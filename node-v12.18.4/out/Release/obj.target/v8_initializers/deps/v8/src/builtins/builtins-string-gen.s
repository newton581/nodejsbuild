	.file	"builtins-string-gen.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal22StringIndexOfAssembler25GenerateStringIndexOfImplEvEUlNS2_8compiler5TNodeINS2_3SmiEEEE_E10_M_managerERSt9_Any_dataRKSA_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal22StringIndexOfAssembler25GenerateStringIndexOfImplEvEUlNS2_8compiler5TNodeINS2_3SmiEEEE_E10_M_managerERSt9_Any_dataRKSA_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal22StringIndexOfAssembler25GenerateStringIndexOfImplEvEUlNS2_8compiler5TNodeINS2_3SmiEEEE_E10_M_managerERSt9_Any_dataRKSA_St18_Manager_operation:
.LFB28499:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L4
	cmpl	$3, %edx
	je	.L5
	cmpl	$1, %edx
	je	.L9
.L5:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE28499:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal22StringIndexOfAssembler25GenerateStringIndexOfImplEvEUlNS2_8compiler5TNodeINS2_3SmiEEEE_E10_M_managerERSt9_Any_dataRKSA_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal22StringIndexOfAssembler25GenerateStringIndexOfImplEvEUlNS2_8compiler5TNodeINS2_3SmiEEEE_E10_M_managerERSt9_Any_dataRKSA_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_3SmiEZNS2_23StringBuiltinsAssembler13StringToArrayENS2_8compiler5TNodeINS2_13NativeContextEEENS8_INS2_6StringEEENS8_IS5_EENS8_INS2_6UnionTIS5_NS2_10HeapNumberEEEEEEUlvE_ZNS6_13StringToArrayESA_SC_SD_SH_EUlvE0_EENS8_IT_EENS7_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSX_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_3SmiEZNS2_23StringBuiltinsAssembler13StringToArrayENS2_8compiler5TNodeINS2_13NativeContextEEENS8_INS2_6StringEEENS8_IS5_EENS8_INS2_6UnionTIS5_NS2_10HeapNumberEEEEEEUlvE_ZNS6_13StringToArrayESA_SC_SD_SH_EUlvE0_EENS8_IT_EENS7_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSX_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_3SmiEZNS2_23StringBuiltinsAssembler13StringToArrayENS2_8compiler5TNodeINS2_13NativeContextEEENS8_INS2_6StringEEENS8_IS5_EENS8_INS2_6UnionTIS5_NS2_10HeapNumberEEEEEEUlvE_ZNS6_13StringToArrayESA_SC_SD_SH_EUlvE0_EENS8_IT_EENS7_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSX_St18_Manager_operation:
.LFB29597:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L11
	cmpl	$3, %edx
	je	.L12
	cmpl	$1, %edx
	je	.L16
.L12:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE29597:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_3SmiEZNS2_23StringBuiltinsAssembler13StringToArrayENS2_8compiler5TNodeINS2_13NativeContextEEENS8_INS2_6StringEEENS8_IS5_EENS8_INS2_6UnionTIS5_NS2_10HeapNumberEEEEEEUlvE_ZNS6_13StringToArrayESA_SC_SD_SH_EUlvE0_EENS8_IT_EENS7_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSX_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_3SmiEZNS2_23StringBuiltinsAssembler13StringToArrayENS2_8compiler5TNodeINS2_13NativeContextEEENS8_INS2_6StringEEENS8_IS5_EENS8_INS2_6UnionTIS5_NS2_10HeapNumberEEEEEEUlvE_ZNS6_13StringToArrayESA_SC_SD_SH_EUlvE0_EENS8_IT_EENS7_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSX_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_3SmiEZNS1_23StringBuiltinsAssembler13StringToArrayENS2_5TNodeINS1_13NativeContextEEENSA_INS1_6StringEEENSA_IS8_EENSA_INS1_6UnionTIS8_NS1_10HeapNumberEEEEEEUlvE_ZNS9_13StringToArrayESC_SE_SF_SJ_EUlvE0_EENSA_IT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_3SmiEZNS1_23StringBuiltinsAssembler13StringToArrayENS2_5TNodeINS1_13NativeContextEEENSA_INS1_6StringEEENSA_IS8_EENSA_INS1_6UnionTIS8_NS1_10HeapNumberEEEEEEUlvE_ZNS9_13StringToArrayESC_SE_SF_SJ_EUlvE0_EENSA_IT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_3SmiEZNS1_23StringBuiltinsAssembler13StringToArrayENS2_5TNodeINS1_13NativeContextEEENSA_INS1_6StringEEENSA_IS8_EENSA_INS1_6UnionTIS8_NS1_10HeapNumberEEEEEEUlvE_ZNS9_13StringToArrayESC_SE_SF_SJ_EUlvE0_EENSA_IT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data:
.LFB29600:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE29600:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_3SmiEZNS1_23StringBuiltinsAssembler13StringToArrayENS2_5TNodeINS1_13NativeContextEEENSA_INS1_6StringEEENSA_IS8_EENSA_INS1_6UnionTIS8_NS1_10HeapNumberEEEEEEUlvE_ZNS9_13StringToArrayESC_SE_SF_SJ_EUlvE0_EENSA_IT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_3SmiEZNS1_23StringBuiltinsAssembler13StringToArrayENS2_5TNodeINS1_13NativeContextEEENSA_INS1_6StringEEENSA_IS8_EENSA_INS1_6UnionTIS8_NS1_10HeapNumberEEEEEEUlvE_ZNS9_13StringToArrayESC_SE_SF_SJ_EUlvE0_EENSA_IT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_3SmiEZNS2_23StringBuiltinsAssembler13StringToArrayENS2_8compiler5TNodeINS2_13NativeContextEEENS8_INS2_6StringEEENS8_IS5_EENS8_INS2_6UnionTIS5_NS2_10HeapNumberEEEEEEUlvE_ZNS6_13StringToArrayESA_SC_SD_SH_EUlvE0_EENS8_IT_EENS7_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSX_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_3SmiEZNS2_23StringBuiltinsAssembler13StringToArrayENS2_8compiler5TNodeINS2_13NativeContextEEENS8_INS2_6StringEEENS8_IS5_EENS8_INS2_6UnionTIS5_NS2_10HeapNumberEEEEEEUlvE_ZNS6_13StringToArrayESA_SC_SD_SH_EUlvE0_EENS8_IT_EENS7_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSX_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_3SmiEZNS2_23StringBuiltinsAssembler13StringToArrayENS2_8compiler5TNodeINS2_13NativeContextEEENS8_INS2_6StringEEENS8_IS5_EENS8_INS2_6UnionTIS5_NS2_10HeapNumberEEEEEEUlvE_ZNS6_13StringToArrayESA_SC_SD_SH_EUlvE0_EENS8_IT_EENS7_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSX_St18_Manager_operation:
.LFB29601:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L19
	cmpl	$3, %edx
	je	.L20
	cmpl	$1, %edx
	je	.L24
.L20:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE29601:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_3SmiEZNS2_23StringBuiltinsAssembler13StringToArrayENS2_8compiler5TNodeINS2_13NativeContextEEENS8_INS2_6StringEEENS8_IS5_EENS8_INS2_6UnionTIS5_NS2_10HeapNumberEEEEEEUlvE_ZNS6_13StringToArrayESA_SC_SD_SH_EUlvE0_EENS8_IT_EENS7_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSX_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_3SmiEZNS2_23StringBuiltinsAssembler13StringToArrayENS2_8compiler5TNodeINS2_13NativeContextEEENS8_INS2_6StringEEENS8_IS5_EENS8_INS2_6UnionTIS5_NS2_10HeapNumberEEEEEEUlvE_ZNS6_13StringToArrayESA_SC_SD_SH_EUlvE0_EENS8_IT_EENS7_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSX_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6UnionTINS2_3SmiENS2_10HeapNumberEEEZNS2_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE1_ZNS9_32GenerateStringPrototypeSplitImplEvEUlvE2_EENS2_8compiler5TNodeIT_EENSC_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSR_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6UnionTINS2_3SmiENS2_10HeapNumberEEEZNS2_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE1_ZNS9_32GenerateStringPrototypeSplitImplEvEUlvE2_EENS2_8compiler5TNodeIT_EENSC_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSR_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6UnionTINS2_3SmiENS2_10HeapNumberEEEZNS2_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE1_ZNS9_32GenerateStringPrototypeSplitImplEvEUlvE2_EENS2_8compiler5TNodeIT_EENSC_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSR_St18_Manager_operation:
.LFB29627:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L26
	cmpl	$3, %edx
	je	.L27
	cmpl	$1, %edx
	je	.L31
.L27:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE29627:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6UnionTINS2_3SmiENS2_10HeapNumberEEEZNS2_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE1_ZNS9_32GenerateStringPrototypeSplitImplEvEUlvE2_EENS2_8compiler5TNodeIT_EENSC_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSR_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6UnionTINS2_3SmiENS2_10HeapNumberEEEZNS2_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE1_ZNS9_32GenerateStringPrototypeSplitImplEvEUlvE2_EENS2_8compiler5TNodeIT_EENSC_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSR_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6UnionTINS2_3SmiENS2_10HeapNumberEEEZNS2_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE1_ZNS9_32GenerateStringPrototypeSplitImplEvEUlvE2_EENS2_8compiler5TNodeIT_EENSC_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSR_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6UnionTINS2_3SmiENS2_10HeapNumberEEEZNS2_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE1_ZNS9_32GenerateStringPrototypeSplitImplEvEUlvE2_EENS2_8compiler5TNodeIT_EENSC_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSR_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6UnionTINS2_3SmiENS2_10HeapNumberEEEZNS2_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE1_ZNS9_32GenerateStringPrototypeSplitImplEvEUlvE2_EENS2_8compiler5TNodeIT_EENSC_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSR_St18_Manager_operation:
.LFB29631:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L33
	cmpl	$3, %edx
	je	.L34
	cmpl	$1, %edx
	je	.L38
.L34:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE29631:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6UnionTINS2_3SmiENS2_10HeapNumberEEEZNS2_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE1_ZNS9_32GenerateStringPrototypeSplitImplEvEUlvE2_EENS2_8compiler5TNodeIT_EENSC_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSR_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6UnionTINS2_3SmiENS2_10HeapNumberEEEZNS2_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE1_ZNS9_32GenerateStringPrototypeSplitImplEvEUlvE2_EENS2_8compiler5TNodeIT_EENSC_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSR_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFvN2v88internal8compiler5TNodeINS1_3SmiEEEEZNS1_22StringIndexOfAssembler25GenerateStringIndexOfImplEvEUlS5_E_E9_M_invokeERKSt9_Any_dataOS5_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvN2v88internal8compiler5TNodeINS1_3SmiEEEEZNS1_22StringIndexOfAssembler25GenerateStringIndexOfImplEvEUlS5_E_E9_M_invokeERKSt9_Any_dataOS5_, @function
_ZNSt17_Function_handlerIFvN2v88internal8compiler5TNodeINS1_3SmiEEEEZNS1_22StringIndexOfAssembler25GenerateStringIndexOfImplEvEUlS5_E_E9_M_invokeERKSt9_Any_dataOS5_:
.LFB28498:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE28498:
	.size	_ZNSt17_Function_handlerIFvN2v88internal8compiler5TNodeINS1_3SmiEEEEZNS1_22StringIndexOfAssembler25GenerateStringIndexOfImplEvEUlS5_E_E9_M_invokeERKSt9_Any_dataOS5_, .-_ZNSt17_Function_handlerIFvN2v88internal8compiler5TNodeINS1_3SmiEEEEZNS1_22StringIndexOfAssembler25GenerateStringIndexOfImplEvEUlS5_E_E9_M_invokeERKSt9_Any_dataOS5_
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_EZNS1_19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorES4_S4_S4_PNS2_26TypedCodeAssemblerVariableINS1_7IntPtrTEEENS2_5TNodeIS8_EEiPNS2_18CodeAssemblerLabelEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_EZNS1_19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorES4_S4_S4_PNS2_26TypedCodeAssemblerVariableINS1_7IntPtrTEEENS2_5TNodeIS8_EEiPNS2_18CodeAssemblerLabelEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_EZNS1_19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorES4_S4_S4_PNS2_26TypedCodeAssemblerVariableINS1_7IntPtrTEEENS2_5TNodeIS8_EEiPNS2_18CodeAssemblerLabelEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB28601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rbx
	movq	(%rsi), %rsi
	movq	8(%rbx), %rax
	movq	16(%rbx), %r12
	movq	(%rax), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movl	$2, %r8d
	movl	$770, %esi
	movq	%rax, %rcx
	movq	(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	(%rax), %rdx
	jmp	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	.cfi_endproc
.LFE28601:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_EZNS1_19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorES4_S4_S4_PNS2_26TypedCodeAssemblerVariableINS1_7IntPtrTEEENS2_5TNodeIS8_EEiPNS2_18CodeAssemblerLabelEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_EZNS1_19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorES4_S4_S4_PNS2_26TypedCodeAssemblerVariableINS1_7IntPtrTEEENS2_5TNodeIS8_EEiPNS2_18CodeAssemblerLabelEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEvEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEvEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEvEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB28483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rbx
	movq	(%r8), %rdx
	movq	8(%rbx), %rsi
	movq	(%rbx), %rdi
	call	_ZN2v88internal17CodeStubAssembler22TruncateTaggedToWord32EPNS0_8compiler4NodeES4_@PLT
	movq	(%rbx), %r13
	movl	$65535, %esi
	movq	%rax, %r12
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	24(%rbx), %rdi
	movq	(%rbx), %r13
	movq	%rax, %r12
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdi
	movl	$15, %r8d
	movl	$1, %ecx
	movq	%rax, %rsi
	movl	$19, %edx
	call	_ZN2v88internal17CodeStubAssembler22ElementOffsetFromIndexEPNS0_8compiler4NodeENS0_12ElementsKindENS1_13ParameterModeEi@PLT
	movq	16(%rbx), %rdx
	movq	(%rbx), %rdi
	movq	%r12, %r8
	movq	%rax, %rcx
	movl	$3, %esi
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	(%rbx), %r13
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	24(%rbx), %rdi
	movq	%rax, %r12
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	24(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	.cfi_endproc
.LFE28483:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEvEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEvEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB28479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %rbx
	movq	(%r8), %rdx
	movq	8(%rbx), %rsi
	movq	(%rbx), %rdi
	call	_ZN2v88internal17CodeStubAssembler22TruncateTaggedToWord32EPNS0_8compiler4NodeES4_@PLT
	movq	(%rbx), %r13
	movl	$65535, %esi
	movq	%rax, %r12
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	32(%rbx), %rdx
	movl	$255, %esi
	movq	%rax, (%rdx)
	movq	(%rbx), %r12
	movq	16(%rbx), %r13
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	32(%rbx), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16Int32GreaterThanENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	24(%rbx), %rdi
	movq	(%rbx), %r12
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movl	$15, %r8d
	movl	$1, %ecx
	movq	%rax, %rsi
	movl	$17, %edx
	call	_ZN2v88internal17CodeStubAssembler22ElementOffsetFromIndexEPNS0_8compiler4NodeENS0_12ElementsKindENS1_13ParameterModeEi@PLT
	movq	40(%rbx), %rdx
	movq	(%rbx), %rdi
	movl	$2, %esi
	movq	%rax, %rcx
	movq	32(%rbx), %rax
	movq	(%rax), %r8
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movq	(%rbx), %r13
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	24(%rbx), %rdi
	movq	%rax, %r12
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	24(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	.cfi_endproc
.LFE28479:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZNSt17_Function_handlerIFvN2v88internal8compiler5TNodeINS1_3SmiEEEEZNS1_30StringIncludesIndexOfAssembler8GenerateENS7_13SearchVariantENS3_INS1_7IntPtrTEEENS3_INS1_7ContextEEEEUlS5_E_E9_M_invokeERKSt9_Any_dataOS5_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvN2v88internal8compiler5TNodeINS1_3SmiEEEEZNS1_30StringIncludesIndexOfAssembler8GenerateENS7_13SearchVariantENS3_INS1_7IntPtrTEEENS3_INS1_7ContextEEEEUlS5_E_E9_M_invokeERKSt9_Any_dataOS5_, @function
_ZNSt17_Function_handlerIFvN2v88internal8compiler5TNodeINS1_3SmiEEEEZNS1_30StringIncludesIndexOfAssembler8GenerateENS7_13SearchVariantENS3_INS1_7IntPtrTEEENS3_INS1_7ContextEEEEUlS5_E_E9_M_invokeERKSt9_Any_dataOS5_:
.LFB28504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	(%rdi), %rax
	movq	(%rsi), %r15
	movq	(%rax), %rdx
	movq	8(%rax), %r14
	movq	%r15, %rsi
	cmpl	$1, (%rdx)
	je	.L52
	movq	16(%rax), %r12
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler24IntPtrGreaterThanOrEqualENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler21SelectBooleanConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEE@PLT
	movq	%rax, %rsi
.L52:
	popq	%r12
	movq	%r14, %rdi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	.cfi_endproc
.LFE28504:
	.size	_ZNSt17_Function_handlerIFvN2v88internal8compiler5TNodeINS1_3SmiEEEEZNS1_30StringIncludesIndexOfAssembler8GenerateENS7_13SearchVariantENS3_INS1_7IntPtrTEEENS3_INS1_7ContextEEEEUlS5_E_E9_M_invokeERKSt9_Any_dataOS5_, .-_ZNSt17_Function_handlerIFvN2v88internal8compiler5TNodeINS1_3SmiEEEEZNS1_30StringIncludesIndexOfAssembler8GenerateENS7_13SearchVariantENS3_INS1_7IntPtrTEEENS3_INS1_7ContextEEEEUlS5_E_E9_M_invokeERKSt9_Any_dataOS5_
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZNSt17_Function_handlerIFvvEZN2v88internal31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEvEUlvE_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvvEZN2v88internal31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEvEUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEZN2v88internal31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEvEUlvE_E9_M_invokeERKSt9_Any_data:
.LFB28515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	32(%rax), %r12
	movq	24(%rax), %r15
	movq	8(%rax), %rbx
	movq	(%rax), %r14
	movq	%r12, %rdi
	movq	16(%rax), %r13
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$869, %edx
	leaq	-112(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-80(%rbp), %rsi
	leaq	-128(%rbp), %rdx
	movq	%r14, %r9
	movl	$3, %edi
	movq	%rax, %r8
	movq	%r13, %xmm1
	movl	$1, %ecx
	pushq	%rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rbx, %xmm0
	movq	%r12, %rdi
	pushq	%rsi
	punpcklqdq	%xmm1, %xmm0
	xorl	%esi, %esi
	movq	%rax, -128(%rbp)
	movq	-96(%rbp), %rax
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -120(%rbp)
	movq	%r15, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L57
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L57:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28515:
	.size	_ZNSt17_Function_handlerIFvvEZN2v88internal31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEvEUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEZN2v88internal31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEvEUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt17_Function_handlerIFvvEZN2v88internal26StringMatchSearchAssembler8GenerateENS3_7VariantEPKcNS2_8compiler5TNodeINS2_6ObjectEEESA_NS8_INS2_7ContextEEEEUlvE_E9_M_invokeERKSt9_Any_data,"axG",@progbits,_ZNSt17_Function_handlerIFvvEZN2v88internal26StringMatchSearchAssembler8GenerateENS3_7VariantEPKcNS2_8compiler5TNodeINS2_6ObjectEEESA_NS8_INS2_7ContextEEEEUlvE_E9_M_invokeERKSt9_Any_data,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFvvEZN2v88internal26StringMatchSearchAssembler8GenerateENS3_7VariantEPKcNS2_8compiler5TNodeINS2_6ObjectEEESA_NS8_INS2_7ContextEEEEUlvE_E9_M_invokeERKSt9_Any_data
	.type	_ZNSt17_Function_handlerIFvvEZN2v88internal26StringMatchSearchAssembler8GenerateENS3_7VariantEPKcNS2_8compiler5TNodeINS2_6ObjectEEESA_NS8_INS2_7ContextEEEEUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEZN2v88internal26StringMatchSearchAssembler8GenerateENS3_7VariantEPKcNS2_8compiler5TNodeINS2_6ObjectEEESA_NS8_INS2_7ContextEEEEUlvE_E9_M_invokeERKSt9_Any_data:
.LFB28531:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	32(%rax), %r12
	movl	(%rax), %r15d
	movq	8(%rax), %r14
	movq	16(%rax), %rbx
	movq	%r12, %rdi
	movq	24(%rax), %r13
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	%r15d, %edx
	leaq	-112(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-80(%rbp), %rsi
	leaq	-128(%rbp), %rdx
	movq	%r14, %r9
	movl	$2, %edi
	movq	%rax, %r8
	movq	%r13, %xmm1
	movl	$1, %ecx
	pushq	%rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rbx, %xmm0
	movq	%r12, %rdi
	pushq	%rsi
	punpcklqdq	%xmm1, %xmm0
	xorl	%esi, %esi
	movq	%rax, -128(%rbp)
	movq	-96(%rbp), %rax
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L61
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L61:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28531:
	.size	_ZNSt17_Function_handlerIFvvEZN2v88internal26StringMatchSearchAssembler8GenerateENS3_7VariantEPKcNS2_8compiler5TNodeINS2_6ObjectEEESA_NS8_INS2_7ContextEEEEUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEZN2v88internal26StringMatchSearchAssembler8GenerateENS3_7VariantEPKcNS2_8compiler5TNodeINS2_6ObjectEEESA_NS8_INS2_7ContextEEEEUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt17_Function_handlerIFvvEZN2v88internal29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvvEZN2v88internal29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEZN2v88internal29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE_E9_M_invokeERKSt9_Any_data:
.LFB28572:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	8(%rax), %rdx
	movq	40(%rax), %r12
	movq	(%rax), %r13
	movq	(%rdx), %r15
	movq	16(%rax), %rdx
	movq	%r12, %rdi
	movq	(%rdx), %rbx
	movq	24(%rax), %rdx
	movq	(%rdx), %rcx
	movq	32(%rax), %rdx
	movq	%rcx, -136(%rbp)
	movq	(%rdx), %r14
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$560, %edx
	leaq	-112(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-80(%rbp), %rsi
	leaq	-128(%rbp), %rdx
	movq	%r15, %r9
	movl	$3, %edi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	pushq	%rdi
	movq	%rbx, %xmm0
	movq	%r12, %rdi
	pushq	%rsi
	movhps	-136(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%rax, -128(%rbp)
	movq	-96(%rbp), %rax
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -120(%rbp)
	movq	%r14, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L65:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28572:
	.size	_ZNSt17_Function_handlerIFvvEZN2v88internal29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEZN2v88internal29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEvEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEvEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEvEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB28519:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rbx), %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	leaq	-128(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	(%rbx), %r12
	movq	16(%rbx), %rax
	movl	$2, %esi
	movq	32(%rbx), %r13
	movq	8(%rbx), %r15
	movq	%r12, %rdi
	movq	24(%rbx), %rbx
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r14, %xmm0
	leaq	-96(%rbp), %rcx
	xorl	%esi, %esi
	movq	%rbx, %xmm1
	movl	$5, %ebx
	movq	%rax, %r8
	movq	%r15, %r9
	movhps	-160(%rbp), %xmm0
	pushq	%rbx
	movq	-112(%rbp), %rax
	movq	%r12, %rdi
	pushq	%rcx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdx
	movl	$1, %ecx
	movaps	%xmm0, -96(%rbp)
	movq	-152(%rbp), %xmm0
	movq	%rdx, -144(%rbp)
	leaq	-144(%rbp), %rdx
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -136(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%r13, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L69
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L69:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28519:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEvEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEvEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEvEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEvEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEvEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB28547:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rbx), %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	leaq	-128(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	(%rbx), %r12
	movl	$1, %esi
	movq	16(%rbx), %r13
	movq	8(%rbx), %r15
	movq	24(%rbx), %rbx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm1
	leaq	-96(%rbp), %rcx
	xorl	%esi, %esi
	movq	%r14, %xmm0
	movl	$4, %ebx
	movq	%rax, %r8
	movq	%r15, %r9
	pushq	%rbx
	movhps	-152(%rbp), %xmm0
	movq	%r12, %rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rcx
	leaq	-144(%rbp), %rdx
	movl	$1, %ecx
	movq	%rax, -144(%rbp)
	movq	-112(%rbp), %rax
	movaps	%xmm0, -96(%rbp)
	movq	%r13, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -136(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L73
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L73:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28547:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEvEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEvEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB28576:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rbx), %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-128(%rbp), %rdi
	movl	$2, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	24(%rbx), %rax
	movq	(%rbx), %r15
	movl	$2, %esi
	movq	8(%rbx), %r14
	movq	(%rax), %rax
	movq	%r15, %rdi
	movq	%rax, -152(%rbp)
	movq	16(%rbx), %rax
	movq	(%rax), %r12
	movq	32(%rbx), %rax
	movq	(%rax), %rdx
	movq	40(%rbx), %rax
	movq	%rdx, -160(%rbp)
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-128(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r13, %xmm0
	movq	%rbx, -64(%rbp)
	xorl	%esi, %esi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movq	%rax, %r8
	movq	%r12, %r9
	movq	%r15, %rdi
	movhps	-168(%rbp), %xmm0
	movq	%rcx, -144(%rbp)
	movl	$5, %ebx
	leaq	-96(%rbp), %rcx
	pushq	%rbx
	movq	-112(%rbp), %rax
	leaq	-144(%rbp), %rdx
	pushq	%rcx
	movl	$1, %ecx
	movaps	%xmm0, -96(%rbp)
	movq	-152(%rbp), %xmm0
	movq	%rax, -136(%rbp)
	movhps	-160(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L77
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L77:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28576:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZNSt17_Function_handlerIFvvEZN2v88internal32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEvEUlvE_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvvEZN2v88internal32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEvEUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEZN2v88internal32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEvEUlvE_E9_M_invokeERKSt9_Any_data:
.LFB28543:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rbx
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rbx), %rax
	movq	(%rax), %r13
	movq	8(%rbx), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	24(%rbx), %rax
	movq	32(%rbx), %rcx
	movq	%r13, %r8
	movq	%r12, %rdi
	movq	(%rax), %rdx
	movq	16(%rbx), %rax
	movq	(%rcx), %rcx
	movq	(%rax), %rsi
	call	_ZN2v88internal23RegExpMatchAllAssembler8GenerateENS0_8compiler5TNodeINS0_7ContextEEES5_NS3_INS0_6ObjectEEES7_@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L81
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L81:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28543:
	.size	_ZNSt17_Function_handlerIFvvEZN2v88internal32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEvEUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEZN2v88internal32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEvEUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_3SmiEZNS1_23StringBuiltinsAssembler13StringToArrayENS2_5TNodeINS1_13NativeContextEEENSA_INS1_6StringEEENSA_IS8_EENSA_INS1_6UnionTIS8_NS1_10HeapNumberEEEEEEUlvE_ZNS9_13StringToArrayESC_SE_SF_SJ_EUlvE0_EENSA_IT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_3SmiEZNS1_23StringBuiltinsAssembler13StringToArrayENS2_5TNodeINS1_13NativeContextEEENSA_INS1_6StringEEENSA_IS8_EENSA_INS1_6UnionTIS8_NS1_10HeapNumberEEEEEEUlvE_ZNS9_13StringToArrayESC_SE_SF_SJ_EUlvE0_EENSA_IT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_3SmiEZNS1_23StringBuiltinsAssembler13StringToArrayENS2_5TNodeINS1_13NativeContextEEENSA_INS1_6StringEEENSA_IS8_EENSA_INS1_6UnionTIS8_NS1_10HeapNumberEEEEEEUlvE_ZNS9_13StringToArrayESC_SE_SF_SJ_EUlvE0_EENSA_IT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB29596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rax), %rsi
	movq	16(%rax), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal17CodeStubAssembler6SmiMinENS0_8compiler5TNodeINS0_3SmiEEES5_@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE29596:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_3SmiEZNS1_23StringBuiltinsAssembler13StringToArrayENS2_5TNodeINS1_13NativeContextEEENSA_INS1_6StringEEENSA_IS8_EENSA_INS1_6UnionTIS8_NS1_10HeapNumberEEEEEEUlvE_ZNS9_13StringToArrayESC_SE_SF_SJ_EUlvE0_EENSA_IT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_3SmiEZNS1_23StringBuiltinsAssembler13StringToArrayENS2_5TNodeINS1_13NativeContextEEENSA_INS1_6StringEEENSA_IS8_EENSA_INS1_6UnionTIS8_NS1_10HeapNumberEEEEEEUlvE_ZNS9_13StringToArrayESC_SE_SF_SJ_EUlvE0_EENSA_IT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_23StringBuiltinsAssembler13StringToArrayENS2_5TNodeINS1_13NativeContextEEENS7_INS1_6StringEEENS7_INS1_3SmiEEENS7_INS1_6UnionTISC_NS1_10HeapNumberEEEEEEUlS4_E1_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_23StringBuiltinsAssembler13StringToArrayENS2_5TNodeINS1_13NativeContextEEENS7_INS1_6StringEEENS7_INS1_3SmiEEENS7_INS1_6UnionTISC_NS1_10HeapNumberEEEEEEUlS4_E1_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_23StringBuiltinsAssembler13StringToArrayENS2_5TNodeINS1_13NativeContextEEENS7_INS1_6StringEEENS7_INS1_3SmiEEENS7_INS1_6UnionTISC_NS1_10HeapNumberEEEEEEUlS4_E1_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB28563:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	(%rsi), %r13
	movq	8(%rbx), %rax
	movq	16(%rbx), %r12
	movq	%r13, %rsi
	movq	(%rax), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movl	$2, %r8d
	movq	%r12, %rdi
	movl	$770, %esi
	movq	%rax, %rcx
	movq	(%rbx), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	16(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18ChangeUint32ToWordENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	subq	$8, %rsp
	movq	16(%rbx), %rdi
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	movq	24(%rbx), %rax
	pushq	$0
	movl	$2, %r9d
	movl	$1, %r8d
	movq	(%rax), %rsi
	call	_ZN2v88internal17CodeStubAssembler21LoadFixedArrayElementENS0_8compiler5TNodeINS0_10FixedArrayEEEPNS2_4NodeEiNS1_13ParameterModeENS0_15LoadSensitivityENS0_11CheckBoundsE@PLT
	movq	16(%rbx), %r14
	movq	32(%rbx), %r15
	movq	%rax, %rsi
	movq	%rax, %r12
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	40(%rbx), %rax
	movq	16(%rbx), %r14
	movq	%r13, %rdx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	(%rax), %r15
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubAssembler21FixedArrayBoundsCheckENS0_8compiler5TNodeINS0_14FixedArrayBaseEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	xorl	%r9d, %r9d
	movl	$4, %r8d
	movl	$1, (%rsp)
	call	_ZN2v88internal17CodeStubAssembler37StoreFixedArrayOrPropertyArrayElementEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28563:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_23StringBuiltinsAssembler13StringToArrayENS2_5TNodeINS1_13NativeContextEEENS7_INS1_6StringEEENS7_INS1_3SmiEEENS7_INS1_6UnionTISC_NS1_10HeapNumberEEEEEEUlS4_E1_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_23StringBuiltinsAssembler13StringToArrayENS2_5TNodeINS1_13NativeContextEEENS7_INS1_6StringEEENS7_INS1_3SmiEEENS7_INS1_6UnionTISC_NS1_10HeapNumberEEEEEEUlS4_E1_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6UnionTINS1_3SmiENS1_10HeapNumberEEEZNS1_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE1_ZNSC_32GenerateStringPrototypeSplitImplEvEUlvE2_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6UnionTINS1_3SmiENS1_10HeapNumberEEEZNS1_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE1_ZNSC_32GenerateStringPrototypeSplitImplEvEUlvE2_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6UnionTINS1_3SmiENS1_10HeapNumberEEEZNS1_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE1_ZNSC_32GenerateStringPrototypeSplitImplEvEUlvE2_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data:
.LFB29630:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	16(%rax), %rdi
	movq	8(%rax), %rdx
	movq	(%rax), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal17CodeStubAssembler8ToUint32ENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE29630:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6UnionTINS1_3SmiENS1_10HeapNumberEEEZNS1_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE1_ZNSC_32GenerateStringPrototypeSplitImplEvEUlvE2_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6UnionTINS1_3SmiENS1_10HeapNumberEEEZNS1_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE1_ZNSC_32GenerateStringPrototypeSplitImplEvEUlvE2_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6UnionTINS1_3SmiENS1_10HeapNumberEEEZNS1_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE1_ZNSC_32GenerateStringPrototypeSplitImplEvEUlvE2_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6UnionTINS1_3SmiENS1_10HeapNumberEEEZNS1_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE1_ZNSC_32GenerateStringPrototypeSplitImplEvEUlvE2_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6UnionTINS1_3SmiENS1_10HeapNumberEEEZNS1_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE1_ZNSC_32GenerateStringPrototypeSplitImplEvEUlvE2_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB29626:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movsd	.LC1(%rip), %xmm0
	movq	(%rax), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8compiler13CodeAssembler14NumberConstantEd@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE29626:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6UnionTINS1_3SmiENS1_10HeapNumberEEEZNS1_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE1_ZNSC_32GenerateStringPrototypeSplitImplEvEUlvE2_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6UnionTINS1_3SmiENS1_10HeapNumberEEEZNS1_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE1_ZNSC_32GenerateStringPrototypeSplitImplEvEUlvE2_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_EZNS1_19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorES4_S4_S4_PNS2_26TypedCodeAssemblerVariableINS1_7IntPtrTEEENS2_5TNodeIS8_EEiPNS2_18CodeAssemblerLabelEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_EZNS1_19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorES4_S4_S4_PNS2_26TypedCodeAssemblerVariableINS1_7IntPtrTEEENS2_5TNodeIS8_EEiPNS2_18CodeAssemblerLabelEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_EZNS1_19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorES4_S4_S4_PNS2_26TypedCodeAssemblerVariableINS1_7IntPtrTEEENS2_5TNodeIS8_EEiPNS2_18CodeAssemblerLabelEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB28597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rbx
	movq	(%rsi), %r14
	movl	$1, %esi
	movq	16(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	16(%rbx), %rdi
	movq	%r14, %rsi
	movq	%rax, %r13
	movq	8(%rbx), %rax
	movq	(%rax), %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEENS3_INS0_9IntegralTEEE@PLT
	movq	%r12, %rdi
	movl	$2, %r8d
	movl	$771, %esi
	movq	%rax, %rcx
	movq	(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	movq	(%rax), %rdx
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	.cfi_endproc
.LFE28597:
	.size	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_EZNS1_19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorES4_S4_S4_PNS2_26TypedCodeAssemblerVariableINS1_7IntPtrTEEENS2_5TNodeIS8_EEiPNS2_18CodeAssemblerLabelEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_EZNS1_19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorES4_S4_S4_PNS2_26TypedCodeAssemblerVariableINS1_7IntPtrTEEENS2_5TNodeIS8_EEiPNS2_18CodeAssemblerLabelEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation:
.LFB28480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L93
	cmpl	$3, %edx
	je	.L94
	cmpl	$1, %edx
	je	.L100
.L95:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$48, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%r12), %xmm1
	movups	%xmm1, 16(%rax)
	movdqu	32(%r12), %xmm2
	movq	%rax, (%rbx)
	movups	%xmm2, 32(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L95
	movl	$48, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28480:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEvEUlPNS2_8compiler4NodeEE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEvEUlPNS2_8compiler4NodeEE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEvEUlPNS2_8compiler4NodeEE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation:
.LFB28484:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L102
	cmpl	$3, %edx
	je	.L103
	cmpl	$1, %edx
	je	.L109
.L104:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$32, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%r12), %xmm1
	movq	%rax, (%rbx)
	movups	%xmm1, 16(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L104
	movl	$32, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28484:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEvEUlPNS2_8compiler4NodeEE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEvEUlPNS2_8compiler4NodeEE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal30StringIncludesIndexOfAssembler8GenerateENS3_13SearchVariantENS2_8compiler5TNodeINS2_7IntPtrTEEENS6_INS2_7ContextEEEEUlNS6_INS2_3SmiEEEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal30StringIncludesIndexOfAssembler8GenerateENS3_13SearchVariantENS2_8compiler5TNodeINS2_7IntPtrTEEENS6_INS2_7ContextEEEEUlNS6_INS2_3SmiEEEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal30StringIncludesIndexOfAssembler8GenerateENS3_13SearchVariantENS2_8compiler5TNodeINS2_7IntPtrTEEENS6_INS2_7ContextEEEEUlNS6_INS2_3SmiEEEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation:
.LFB28505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L111
	cmpl	$3, %edx
	je	.L112
	cmpl	$1, %edx
	je	.L118
.L113:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$24, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movq	16(%r12), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 16(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L113
	movl	$24, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28505:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal30StringIncludesIndexOfAssembler8GenerateENS3_13SearchVariantENS2_8compiler5TNodeINS2_7IntPtrTEEENS6_INS2_7ContextEEEEUlNS6_INS2_3SmiEEEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal30StringIncludesIndexOfAssembler8GenerateENS3_13SearchVariantENS2_8compiler5TNodeINS2_7IntPtrTEEENS6_INS2_7ContextEEEEUlNS6_INS2_3SmiEEEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEvEUlPNS2_8compiler4NodeEE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEvEUlPNS2_8compiler4NodeEE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEvEUlPNS2_8compiler4NodeEE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation:
.LFB28520:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L120
	cmpl	$3, %edx
	je	.L121
	cmpl	$1, %edx
	je	.L127
.L122:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$40, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%r12), %xmm1
	movups	%xmm1, 16(%rax)
	movq	32(%r12), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 32(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L122
	movl	$40, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28520:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEvEUlPNS2_8compiler4NodeEE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEvEUlPNS2_8compiler4NodeEE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation:
.LFB28516:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L129
	cmpl	$3, %edx
	je	.L130
	cmpl	$1, %edx
	je	.L136
.L131:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$40, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%r12), %xmm1
	movups	%xmm1, 16(%rax)
	movq	32(%r12), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 32(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L131
	movl	$40, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28516:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEvEUlPNS2_8compiler4NodeEE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEvEUlPNS2_8compiler4NodeEE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEvEUlPNS2_8compiler4NodeEE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation:
.LFB28548:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L138
	cmpl	$3, %edx
	je	.L139
	cmpl	$1, %edx
	je	.L145
.L140:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$32, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%r12), %xmm1
	movq	%rax, (%rbx)
	movups	%xmm1, 16(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L140
	movl	$32, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28548:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEvEUlPNS2_8compiler4NodeEE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEvEUlPNS2_8compiler4NodeEE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation:
.LFB28544:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L147
	cmpl	$3, %edx
	je	.L148
	cmpl	$1, %edx
	je	.L154
.L149:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$40, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%r12), %xmm1
	movups	%xmm1, 16(%rax)
	movq	32(%r12), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 32(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L149
	movl	$40, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28544:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal23StringBuiltinsAssembler13StringToArrayENS2_8compiler5TNodeINS2_13NativeContextEEENS5_INS2_6StringEEENS5_INS2_3SmiEEENS5_INS2_6UnionTISA_NS2_10HeapNumberEEEEEEUlPNS4_4NodeEE1_E10_M_managerERSt9_Any_dataRKSK_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal23StringBuiltinsAssembler13StringToArrayENS2_8compiler5TNodeINS2_13NativeContextEEENS5_INS2_6StringEEENS5_INS2_3SmiEEENS5_INS2_6UnionTISA_NS2_10HeapNumberEEEEEEUlPNS4_4NodeEE1_E10_M_managerERSt9_Any_dataRKSK_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal23StringBuiltinsAssembler13StringToArrayENS2_8compiler5TNodeINS2_13NativeContextEEENS5_INS2_6StringEEENS5_INS2_3SmiEEENS5_INS2_6UnionTISA_NS2_10HeapNumberEEEEEEUlPNS4_4NodeEE1_E10_M_managerERSt9_Any_dataRKSK_St18_Manager_operation:
.LFB28564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L156
	cmpl	$3, %edx
	je	.L157
	cmpl	$1, %edx
	je	.L163
.L158:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$48, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%r12), %xmm1
	movups	%xmm1, 16(%rax)
	movdqu	32(%r12), %xmm2
	movq	%rax, (%rbx)
	movups	%xmm2, 32(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L158
	movl	$48, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28564:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal23StringBuiltinsAssembler13StringToArrayENS2_8compiler5TNodeINS2_13NativeContextEEENS5_INS2_6StringEEENS5_INS2_3SmiEEENS5_INS2_6UnionTISA_NS2_10HeapNumberEEEEEEUlPNS4_4NodeEE1_E10_M_managerERSt9_Any_dataRKSK_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal23StringBuiltinsAssembler13StringToArrayENS2_8compiler5TNodeINS2_13NativeContextEEENS5_INS2_6StringEEENS5_INS2_3SmiEEENS5_INS2_6UnionTISA_NS2_10HeapNumberEEEEEEUlPNS4_4NodeEE1_E10_M_managerERSt9_Any_dataRKSK_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlPNS2_8compiler4NodeEE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlPNS2_8compiler4NodeEE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlPNS2_8compiler4NodeEE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation:
.LFB28577:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L165
	cmpl	$3, %edx
	je	.L166
	cmpl	$1, %edx
	je	.L172
.L167:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$48, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%r12), %xmm1
	movups	%xmm1, 16(%rax)
	movdqu	32(%r12), %xmm2
	movq	%rax, (%rbx)
	movups	%xmm2, 32(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L167
	movl	$48, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28577:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlPNS2_8compiler4NodeEE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlPNS2_8compiler4NodeEE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation:
.LFB28573:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L174
	cmpl	$3, %edx
	je	.L175
	cmpl	$1, %edx
	je	.L181
.L176:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$48, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%r12), %xmm1
	movups	%xmm1, 16(%rax)
	movdqu	32(%r12), %xmm2
	movq	%rax, (%rbx)
	movups	%xmm2, 32(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L176
	movl	$48, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28573:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorEPNS2_8compiler4NodeES6_S6_PNS4_26TypedCodeAssemblerVariableINS2_7IntPtrTEEENS4_5TNodeIS8_EEiPNS4_18CodeAssemblerLabelEEUlS6_E_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorEPNS2_8compiler4NodeES6_S6_PNS4_26TypedCodeAssemblerVariableINS2_7IntPtrTEEENS4_5TNodeIS8_EEiPNS4_18CodeAssemblerLabelEEUlS6_E_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorEPNS2_8compiler4NodeES6_S6_PNS4_26TypedCodeAssemblerVariableINS2_7IntPtrTEEENS4_5TNodeIS8_EEiPNS4_18CodeAssemblerLabelEEUlS6_E_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation:
.LFB28598:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L183
	cmpl	$3, %edx
	je	.L184
	cmpl	$1, %edx
	je	.L190
.L185:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$24, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movq	16(%r12), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 16(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L185
	movl	$24, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28598:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorEPNS2_8compiler4NodeES6_S6_PNS4_26TypedCodeAssemblerVariableINS2_7IntPtrTEEENS4_5TNodeIS8_EEiPNS4_18CodeAssemblerLabelEEUlS6_E_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorEPNS2_8compiler4NodeES6_S6_PNS4_26TypedCodeAssemblerVariableINS2_7IntPtrTEEENS4_5TNodeIS8_EEiPNS4_18CodeAssemblerLabelEEUlS6_E_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorEPNS2_8compiler4NodeES6_S6_PNS4_26TypedCodeAssemblerVariableINS2_7IntPtrTEEENS4_5TNodeIS8_EEiPNS4_18CodeAssemblerLabelEEUlS6_E0_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorEPNS2_8compiler4NodeES6_S6_PNS4_26TypedCodeAssemblerVariableINS2_7IntPtrTEEENS4_5TNodeIS8_EEiPNS4_18CodeAssemblerLabelEEUlS6_E0_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorEPNS2_8compiler4NodeES6_S6_PNS4_26TypedCodeAssemblerVariableINS2_7IntPtrTEEENS4_5TNodeIS8_EEiPNS4_18CodeAssemblerLabelEEUlS6_E0_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation:
.LFB28602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L192
	cmpl	$3, %edx
	je	.L193
	cmpl	$1, %edx
	je	.L199
.L194:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$24, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movq	16(%r12), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 16(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L194
	movl	$24, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28602:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorEPNS2_8compiler4NodeES6_S6_PNS4_26TypedCodeAssemblerVariableINS2_7IntPtrTEEENS4_5TNodeIS8_EEiPNS4_18CodeAssemblerLabelEEUlS6_E0_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorEPNS2_8compiler4NodeES6_S6_PNS4_26TypedCodeAssemblerVariableINS2_7IntPtrTEEENS4_5TNodeIS8_EEiPNS4_18CodeAssemblerLabelEEUlS6_E0_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal26StringMatchSearchAssembler8GenerateENS3_7VariantEPKcNS2_8compiler5TNodeINS2_6ObjectEEESA_NS8_INS2_7ContextEEEEUlvE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIZN2v88internal26StringMatchSearchAssembler8GenerateENS3_7VariantEPKcNS2_8compiler5TNodeINS2_6ObjectEEESA_NS8_INS2_7ContextEEEEUlvE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIZN2v88internal26StringMatchSearchAssembler8GenerateENS3_7VariantEPKcNS2_8compiler5TNodeINS2_6ObjectEEESA_NS8_INS2_7ContextEEEEUlvE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal26StringMatchSearchAssembler8GenerateENS3_7VariantEPKcNS2_8compiler5TNodeINS2_6ObjectEEESA_NS8_INS2_7ContextEEEEUlvE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal26StringMatchSearchAssembler8GenerateENS3_7VariantEPKcNS2_8compiler5TNodeINS2_6ObjectEEESA_NS8_INS2_7ContextEEEEUlvE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation:
.LFB28532:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L201
	cmpl	$3, %edx
	je	.L202
	cmpl	$1, %edx
	je	.L208
.L203:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L201:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$40, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%r12), %xmm1
	movups	%xmm1, 16(%rax)
	movq	32(%r12), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 32(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L203
	movl	$40, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28532:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal26StringMatchSearchAssembler8GenerateENS3_7VariantEPKcNS2_8compiler5TNodeINS2_6ObjectEEESA_NS8_INS2_7ContextEEEEUlvE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal26StringMatchSearchAssembler8GenerateENS3_7VariantEPKcNS2_8compiler5TNodeINS2_6ObjectEEESA_NS8_INS2_7ContextEEEEUlvE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal26StringMatchSearchAssembler8GenerateENS3_7VariantEPKcNS2_8compiler5TNodeINS2_6ObjectEEESA_NS8_INS2_7ContextEEEEUlPNS7_4NodeEE0_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIZN2v88internal26StringMatchSearchAssembler8GenerateENS3_7VariantEPKcNS2_8compiler5TNodeINS2_6ObjectEEESA_NS8_INS2_7ContextEEEEUlPNS7_4NodeEE0_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIZN2v88internal26StringMatchSearchAssembler8GenerateENS3_7VariantEPKcNS2_8compiler5TNodeINS2_6ObjectEEESA_NS8_INS2_7ContextEEEEUlPNS7_4NodeEE0_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal26StringMatchSearchAssembler8GenerateENS3_7VariantEPKcNS2_8compiler5TNodeINS2_6ObjectEEESA_NS8_INS2_7ContextEEEEUlPNS7_4NodeEE0_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal26StringMatchSearchAssembler8GenerateENS3_7VariantEPKcNS2_8compiler5TNodeINS2_6ObjectEEESA_NS8_INS2_7ContextEEEEUlPNS7_4NodeEE0_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation:
.LFB28536:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L210
	cmpl	$3, %edx
	je	.L211
	cmpl	$1, %edx
	je	.L217
.L212:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L210:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$32, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%r12), %xmm1
	movq	%rax, (%rbx)
	movups	%xmm1, 16(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L217:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L212
	movl	$32, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE28536:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal26StringMatchSearchAssembler8GenerateENS3_7VariantEPKcNS2_8compiler5TNodeINS2_6ObjectEEESA_NS8_INS2_7ContextEEEEUlPNS7_4NodeEE0_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal26StringMatchSearchAssembler8GenerateENS3_7VariantEPKcNS2_8compiler5TNodeINS2_6ObjectEEESA_NS8_INS2_7ContextEEEEUlPNS7_4NodeEE0_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26StringMatchSearchAssembler8GenerateENS6_7VariantEPKcNS2_5TNodeINS1_6ObjectEEESC_NSA_INS1_7ContextEEEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_,"axG",@progbits,_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26StringMatchSearchAssembler8GenerateENS6_7VariantEPKcNS2_5TNodeINS1_6ObjectEEESC_NSA_INS1_7ContextEEEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26StringMatchSearchAssembler8GenerateENS6_7VariantEPKcNS2_5TNodeINS1_6ObjectEEESC_NSA_INS1_7ContextEEEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_
	.type	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26StringMatchSearchAssembler8GenerateENS6_7VariantEPKcNS2_5TNodeINS1_6ObjectEEESC_NSA_INS1_7ContextEEEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_, @function
_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26StringMatchSearchAssembler8GenerateENS6_7VariantEPKcNS2_5TNodeINS1_6ObjectEEESC_NSA_INS1_7ContextEEEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_:
.LFB28535:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rbx), %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	leaq	-128(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	(%rbx), %r12
	movl	$1, %esi
	movq	16(%rbx), %r13
	movq	8(%rbx), %r15
	movq	24(%rbx), %rbx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-128(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm1
	leaq	-96(%rbp), %rcx
	xorl	%esi, %esi
	movq	%r14, %xmm0
	movl	$4, %ebx
	movq	%rax, %r8
	movq	%r15, %r9
	pushq	%rbx
	movhps	-152(%rbp), %xmm0
	movq	%r12, %rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rcx
	leaq	-144(%rbp), %rdx
	movl	$1, %ecx
	movq	%rax, -144(%rbp)
	movq	-112(%rbp), %rax
	movaps	%xmm0, -96(%rbp)
	movq	%r13, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -136(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L221
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L221:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28535:
	.size	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26StringMatchSearchAssembler8GenerateENS6_7VariantEPKcNS2_5TNodeINS1_6ObjectEEESC_NSA_INS1_7ContextEEEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_, .-_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26StringMatchSearchAssembler8GenerateENS6_7VariantEPKcNS2_5TNodeINS1_6ObjectEEESC_NSA_INS1_7ContextEEEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_
	.section	.text._ZN2v88internal23StringBuiltinsAssembler16DirectStringDataEPNS0_8compiler4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23StringBuiltinsAssembler16DirectStringDataEPNS0_8compiler4NodeES4_
	.type	_ZN2v88internal23StringBuiltinsAssembler16DirectStringDataEPNS0_8compiler4NodeES4_, @function
_ZN2v88internal23StringBuiltinsAssembler16DirectStringDataEPNS0_8compiler4NodeES4_:
.LFB23593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-448(%rbp), %r15
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-464(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-320(%rbp), %rbx
	subq	$456, %rsp
	movq	%rsi, -488(%rbp)
	movq	%rdi, %rsi
	movq	%r13, %rdi
	movq	%rdx, -480(%rbp)
	movl	$5, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r12, %rdi
	movl	$7, %esi
	movq	%rax, -472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-480(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-472(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-488(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -480(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19BitcastTaggedToWordENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	movl	$15, %esi
	movq	%rax, -472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-472(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-480(%rbp), %r9
	movl	$5, %ecx
	movq	%r12, %rdi
	movl	$24, %edx
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L225
	addq	$456, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L225:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23593:
	.size	_ZN2v88internal23StringBuiltinsAssembler16DirectStringDataEPNS0_8compiler4NodeES4_, .-_ZN2v88internal23StringBuiltinsAssembler16DirectStringDataEPNS0_8compiler4NodeES4_
	.section	.text._ZN2v88internal23StringBuiltinsAssembler16StringEqual_LoopEPNS0_8compiler4NodeES4_NS0_11MachineTypeES4_S4_S5_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESA_.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal23StringBuiltinsAssembler16StringEqual_LoopEPNS0_8compiler4NodeES4_NS0_11MachineTypeES4_S4_S5_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESA_.constprop.0, @function
_ZN2v88internal23StringBuiltinsAssembler16StringEqual_LoopEPNS0_8compiler4NodeES4_NS0_11MachineTypeES4_S4_S5_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESA_.constprop.0:
.LFB32082:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	32(%rbp), %r9
	movq	40(%rbp), %rbx
	movl	%r10d, -244(%rbp)
	movq	%r9, -240(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal23StringBuiltinsAssembler16DirectStringDataEPNS0_8compiler4NodeES4_
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	leaq	-208(%rbp), %r13
	leaq	-192(%rbp), %r14
	call	_ZN2v88internal23StringBuiltinsAssembler16DirectStringDataEPNS0_8compiler4NodeES4_
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-216(%rbp), %rcx
	movl	$1, %r8d
	movq	%r13, -216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	24(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-240(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movq	-232(%rbp), %r11
	movl	-244(%rbp), %r10d
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	movq	%r11, %rdx
	movl	%r10d, %esi
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%rax, -232(%rbp)
	movzbl	16(%rbp), %eax
	subl	$1, %eax
	cmpb	$13, %al
	ja	.L227
	movzbl	%al, %eax
	leaq	CSWTCH.340(%rip), %rdx
	movq	%r13, %rdi
	movl	(%rdx,%rax,4), %edx
	movl	%edx, -240(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	-240(%rbp), %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movzwl	16(%rbp), %esi
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	-232(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L231
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L227:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L231:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE32082:
	.size	_ZN2v88internal23StringBuiltinsAssembler16StringEqual_LoopEPNS0_8compiler4NodeES4_NS0_11MachineTypeES4_S4_S5_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESA_.constprop.0, .-_ZN2v88internal23StringBuiltinsAssembler16StringEqual_LoopEPNS0_8compiler4NodeES4_NS0_11MachineTypeES4_S4_S5_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESA_.constprop.0
	.section	.text._ZN2v88internal23StringBuiltinsAssembler16StringEqual_LoopEPNS0_8compiler4NodeES4_NS0_11MachineTypeES4_S4_S5_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESA_.constprop.1,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal23StringBuiltinsAssembler16StringEqual_LoopEPNS0_8compiler4NodeES4_NS0_11MachineTypeES4_S4_S5_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESA_.constprop.1, @function
_ZN2v88internal23StringBuiltinsAssembler16StringEqual_LoopEPNS0_8compiler4NodeES4_NS0_11MachineTypeES4_S4_S5_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESA_.constprop.1:
.LFB32081:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	32(%rbp), %r9
	movq	40(%rbp), %rbx
	movl	%r10d, -244(%rbp)
	movq	%r9, -240(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal23StringBuiltinsAssembler16DirectStringDataEPNS0_8compiler4NodeES4_
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	leaq	-208(%rbp), %r13
	leaq	-192(%rbp), %r14
	call	_ZN2v88internal23StringBuiltinsAssembler16DirectStringDataEPNS0_8compiler4NodeES4_
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-216(%rbp), %rcx
	movl	$1, %r8d
	movq	%r13, -216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	24(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-240(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movq	-232(%rbp), %r11
	movl	-244(%rbp), %r10d
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	movq	%r11, %rdx
	movl	%r10d, %esi
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%rax, -232(%rbp)
	movzbl	16(%rbp), %eax
	subl	$1, %eax
	cmpb	$13, %al
	ja	.L233
	movzbl	%al, %eax
	leaq	CSWTCH.340(%rip), %rdx
	movq	%r13, %rdi
	movl	(%rdx,%rax,4), %edx
	movl	%edx, -240(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	-240(%rbp), %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movzwl	16(%rbp), %esi
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	-232(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L237
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L233:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L237:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE32081:
	.size	_ZN2v88internal23StringBuiltinsAssembler16StringEqual_LoopEPNS0_8compiler4NodeES4_NS0_11MachineTypeES4_S4_S5_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESA_.constprop.1, .-_ZN2v88internal23StringBuiltinsAssembler16StringEqual_LoopEPNS0_8compiler4NodeES4_NS0_11MachineTypeES4_S4_S5_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESA_.constprop.1
	.section	.text._ZN2v88internal23StringBuiltinsAssembler25DispatchOnStringEncodingsEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelES6_S6_S6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23StringBuiltinsAssembler25DispatchOnStringEncodingsEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelES6_S6_S6_
	.type	_ZN2v88internal23StringBuiltinsAssembler25DispatchOnStringEncodingsEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelES6_S6_S6_, @function
_ZN2v88internal23StringBuiltinsAssembler25DispatchOnStringEncodingsEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelES6_S6_S6_:
.LFB23594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r9, %xmm0
	movq	%rcx, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$8, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$224, %rsp
	movq	%r8, -240(%rbp)
	movhps	16(%rbp), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movhps	-240(%rbp), %xmm1
	movaps	%xmm0, -240(%rbp)
	movaps	%xmm1, -256(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	leaq	-224(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32ShrENS1_11SloppyTNodeINS0_7Word32TEEEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler8Word32OrENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-96(%rbp), %rcx
	leaq	-80(%rbp), %r8
	movq	%r13, %rdx
	movdqa	.LC2(%rip), %xmm2
	movl	$4, %r9d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movdqa	-256(%rbp), %xmm1
	movdqa	-240(%rbp), %xmm0
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6SwitchEPNS1_4NodeEPNS1_18CodeAssemblerLabelEPKiPS6_m@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L241
	addq	$224, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L241:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23594:
	.size	_ZN2v88internal23StringBuiltinsAssembler25DispatchOnStringEncodingsEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelES6_S6_S6_, .-_ZN2v88internal23StringBuiltinsAssembler25DispatchOnStringEncodingsEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelES6_S6_S6_
	.section	.text._ZN2v88internal23StringBuiltinsAssembler26PointerToStringDataAtIndexEPNS0_8compiler4NodeES4_NS0_6String8EncodingE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23StringBuiltinsAssembler26PointerToStringDataAtIndexEPNS0_8compiler4NodeES4_NS0_6String8EncodingE
	.type	_ZN2v88internal23StringBuiltinsAssembler26PointerToStringDataAtIndexEPNS0_8compiler4NodeES4_NS0_6String8EncodingE, @function
_ZN2v88internal23StringBuiltinsAssembler26PointerToStringDataAtIndexEPNS0_8compiler4NodeES4_NS0_6String8EncodingE:
.LFB23596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpl	$1, %ecx
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -32
	sbbl	%edx, %edx
	xorl	%r8d, %r8d
	movq	%rdi, %r12
	andl	$-2, %edx
	addl	$19, %edx
	call	_ZN2v88internal17CodeStubAssembler22ElementOffsetFromIndexEPNS0_8compiler4NodeENS0_12ElementsKindENS1_13ParameterModeEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23596:
	.size	_ZN2v88internal23StringBuiltinsAssembler26PointerToStringDataAtIndexEPNS0_8compiler4NodeES4_NS0_6String8EncodingE, .-_ZN2v88internal23StringBuiltinsAssembler26PointerToStringDataAtIndexEPNS0_8compiler4NodeES4_NS0_6String8EncodingE
	.section	.text._ZN2v88internal23StringBuiltinsAssembler16StringEqual_CoreENS0_8compiler11SloppyTNodeINS0_6StringEEEPNS2_4NodeES5_S7_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESC_SC_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23StringBuiltinsAssembler16StringEqual_CoreENS0_8compiler11SloppyTNodeINS0_6StringEEEPNS2_4NodeES5_S7_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESC_SC_
	.type	_ZN2v88internal23StringBuiltinsAssembler16StringEqual_CoreENS0_8compiler11SloppyTNodeINS0_6StringEEEPNS2_4NodeES5_S7_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESC_SC_, @function
_ZN2v88internal23StringBuiltinsAssembler16StringEqual_CoreENS0_8compiler11SloppyTNodeINS0_6StringEEEPNS2_4NodeES5_S7_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESC_SC_:
.LFB23601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$616, %rsp
	movq	24(%rbp), %rcx
	movq	32(%rbp), %r8
	movq	%r9, -600(%rbp)
	movq	16(%rbp), %rax
	movq	%rcx, -592(%rbp)
	movq	%r8, -632(%rbp)
	movq	%rax, -584(%rbp)
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	movq	%rbx, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-584(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32ShlENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler8Word32OrENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -616(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$8224, %esi
	movq	%r12, %rdi
	movq	%rax, -608(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-616(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-608(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-592(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$4369, %esi
	movq	%r12, %rdi
	movq	%rax, -608(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-616(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	movq	%r9, -624(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-608(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-632(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-576(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rdi, -608(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-448(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsi, %r11
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -616(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-320(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r10, -648(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-192(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r11, -632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$2056, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-624(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movl	$2056, %esi
	movq	%r12, %rdi
	movq	%rax, -624(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-624(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-608(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-624(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-616(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-624(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-632(%rbp), %r11
	movq	-648(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rcx
	movq	%r10, %rdx
	movq	%r11, -640(%rbp)
	movq	%r10, -624(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-608(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %r9
	movq	%rbx, %r8
	movl	$770, %ecx
	pushq	-592(%rbp)
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	pushq	-584(%rbp)
	pushq	-600(%rbp)
	pushq	$770
	call	_ZN2v88internal23StringBuiltinsAssembler16StringEqual_LoopEPNS0_8compiler4NodeES4_NS0_11MachineTypeES4_S4_S5_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESA_.constprop.0
	movq	-616(%rbp), %rsi
	addq	$32, %rsp
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %r9
	movq	%rbx, %r8
	movl	$771, %ecx
	pushq	-592(%rbp)
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	pushq	-584(%rbp)
	pushq	-600(%rbp)
	pushq	$771
	call	_ZN2v88internal23StringBuiltinsAssembler16StringEqual_LoopEPNS0_8compiler4NodeES4_NS0_11MachineTypeES4_S4_S5_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESA_.constprop.1
	movq	-624(%rbp), %r10
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -632(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %r9
	movq	%rbx, %r8
	movl	$770, %ecx
	pushq	-592(%rbp)
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	pushq	-584(%rbp)
	pushq	-600(%rbp)
	pushq	$771
	call	_ZN2v88internal23StringBuiltinsAssembler16StringEqual_LoopEPNS0_8compiler4NodeES4_NS0_11MachineTypeES4_S4_S5_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESA_.constprop.0
	movq	-640(%rbp), %r11
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -624(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %r9
	movq	%rbx, %r8
	movl	$771, %ecx
	pushq	-592(%rbp)
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	pushq	-584(%rbp)
	pushq	-600(%rbp)
	pushq	$770
	call	_ZN2v88internal23StringBuiltinsAssembler16StringEqual_LoopEPNS0_8compiler4NodeES4_NS0_11MachineTypeES4_S4_S5_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESA_.constprop.1
	movq	-624(%rbp), %r11
	addq	$32, %rsp
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-632(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-616(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-608(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L249
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L249:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23601:
	.size	_ZN2v88internal23StringBuiltinsAssembler16StringEqual_CoreENS0_8compiler11SloppyTNodeINS0_6StringEEEPNS2_4NodeES5_S7_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESC_SC_, .-_ZN2v88internal23StringBuiltinsAssembler16StringEqual_CoreENS0_8compiler11SloppyTNodeINS0_6StringEEEPNS2_4NodeES5_S7_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESC_SC_
	.section	.text._ZN2v88internal23StringBuiltinsAssembler19GenerateStringEqualENS0_8compiler5TNodeINS0_6StringEEES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23StringBuiltinsAssembler19GenerateStringEqualENS0_8compiler5TNodeINS0_6StringEEES5_
	.type	_ZN2v88internal23StringBuiltinsAssembler19GenerateStringEqualENS0_8compiler5TNodeINS0_6StringEEES5_, @function
_ZN2v88internal23StringBuiltinsAssembler19GenerateStringEqualENS0_8compiler5TNodeINS0_6StringEEES5_:
.LFB23597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-608(%rbp), %r14
	leaq	-464(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-624(%rbp), %r13
	movq	%r14, %xmm1
	pushq	%r12
	movq	%r13, %xmm0
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movl	$7, %edx
	subq	$648, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -640(%rbp)
	movq	%rdi, %rsi
	movq	%r13, %rdi
	movaps	%xmm0, -672(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	%rbx, %rcx
	movl	$7, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rbx, -632(%rbp)
	leaq	-208(%rbp), %rbx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	leaq	-592(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -648(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-336(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r11, -656(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-80(%rbp), %r10
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movdqa	-672(%rbp), %xmm0
	movq	%r10, %rcx
	movl	$1, %r8d
	movl	$2, %edx
	movq	%r10, -688(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-640(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler22LoadStringLengthAsWordENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	movq	-632(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -640(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22LoadStringLengthAsWordENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	movq	-640(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	movq	%r9, -680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rdi
	movq	%rax, -640(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-640(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -632(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16LoadInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-632(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -672(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16LoadInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-656(%rbp), %r11
	subq	$8, %rsp
	movq	-680(%rbp), %r9
	movq	-632(%rbp), %rcx
	movq	-672(%rbp), %rdx
	movq	%rax, %r8
	movq	%r12, %rdi
	pushq	%r11
	movq	-640(%rbp), %rsi
	pushq	%r15
	pushq	-648(%rbp)
	movq	%rax, -680(%rbp)
	call	_ZN2v88internal23StringBuiltinsAssembler16StringEqual_CoreENS0_8compiler11SloppyTNodeINS0_6StringEEEPNS2_4NodeES5_S7_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESC_SC_
	movq	-656(%rbp), %r11
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %r9
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	-680(%rbp), %r8
	movq	-672(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler25MaybeDerefIndirectStringsEPNS0_8compiler26TypedCodeAssemblerVariableINS0_6StringEEENS2_5TNodeINS0_6Int32TEEES6_S9_PNS2_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -672(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-688(%rbp), %r10
	movl	$2, %r9d
	movq	%r12, %rdi
	movq	-672(%rbp), %rcx
	movq	%rax, %rdx
	movl	$328, %esi
	movq	-640(%rbp), %xmm0
	movq	%r10, %r8
	movhps	-632(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	-648(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-656(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-648(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L253
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L253:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23597:
	.size	_ZN2v88internal23StringBuiltinsAssembler19GenerateStringEqualENS0_8compiler5TNodeINS0_6StringEEES5_, .-_ZN2v88internal23StringBuiltinsAssembler19GenerateStringEqualENS0_8compiler5TNodeINS0_6StringEEES5_
	.section	.text._ZN2v88internal23StringBuiltinsAssembler16StringEqual_LoopEPNS0_8compiler4NodeES4_NS0_11MachineTypeES4_S4_S5_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESA_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23StringBuiltinsAssembler16StringEqual_LoopEPNS0_8compiler4NodeES4_NS0_11MachineTypeES4_S4_S5_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESA_
	.type	_ZN2v88internal23StringBuiltinsAssembler16StringEqual_LoopEPNS0_8compiler4NodeES4_NS0_11MachineTypeES4_S4_S5_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESA_, @function
_ZN2v88internal23StringBuiltinsAssembler16StringEqual_LoopEPNS0_8compiler4NodeES4_NS0_11MachineTypeES4_S4_S5_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESA_:
.LFB23602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	32(%rbp), %r10
	movq	40(%rbp), %rax
	movq	%r10, -232(%rbp)
	movq	%rax, -248(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal23StringBuiltinsAssembler16DirectStringDataEPNS0_8compiler4NodeES4_
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	leaq	-208(%rbp), %r13
	leaq	-192(%rbp), %r14
	call	_ZN2v88internal23StringBuiltinsAssembler16DirectStringDataEPNS0_8compiler4NodeES4_
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-216(%rbp), %rcx
	movl	$1, %r8d
	movq	%r13, -216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	24(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-232(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	leal	-1(%r15), %eax
	cmpb	$13, %al
	ja	.L255
	leaq	CSWTCH.340(%rip), %r9
	movzbl	%al, %eax
	movq	%r13, %rdi
	movl	(%r9,%rax,4), %edx
	movl	%edx, -232(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	-232(%rbp), %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movl	%r15d, %esi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	leaq	CSWTCH.340(%rip), %r9
	movq	%rax, %r15
	movzbl	16(%rbp), %eax
	subl	$1, %eax
	cmpb	$13, %al
	ja	.L255
	movzbl	%al, %eax
	movq	%r13, %rdi
	movl	(%r9,%rax,4), %ebx
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	%ebx, %edx
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movq	-240(%rbp), %rdx
	movzwl	16(%rbp), %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-248(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L259
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L255:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L259:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23602:
	.size	_ZN2v88internal23StringBuiltinsAssembler16StringEqual_LoopEPNS0_8compiler4NodeES4_NS0_11MachineTypeES4_S4_S5_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESA_, .-_ZN2v88internal23StringBuiltinsAssembler16StringEqual_LoopEPNS0_8compiler4NodeES4_NS0_11MachineTypeES4_S4_S5_NS2_5TNodeINS0_7IntPtrTEEEPNS2_18CodeAssemblerLabelESA_
	.section	.rodata._ZN2v88internal8Builtins28Generate_StringAdd_CheckNoneEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"../deps/v8/src/builtins/builtins-string-gen.cc"
	.section	.rodata._ZN2v88internal8Builtins28Generate_StringAdd_CheckNoneEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"StringAdd_CheckNone"
	.section	.text._ZN2v88internal8Builtins28Generate_StringAdd_CheckNoneEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins28Generate_StringAdd_CheckNoneEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins28Generate_StringAdd_CheckNoneEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins28Generate_StringAdd_CheckNoneEPNS0_8compiler18CodeAssemblerStateE:
.LFB23610:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$292, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$706, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L264
.L261:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler9StringAddEPNS0_8compiler4NodeENS2_5TNodeINS0_6StringEEES7_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L265
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L264:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L261
.L265:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23610:
	.size	_ZN2v88internal8Builtins28Generate_StringAdd_CheckNoneEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins28Generate_StringAdd_CheckNoneEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal28StringAdd_CheckNoneAssembler31GenerateStringAdd_CheckNoneImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28StringAdd_CheckNoneAssembler31GenerateStringAdd_CheckNoneImplEv
	.type	_ZN2v88internal28StringAdd_CheckNoneAssembler31GenerateStringAdd_CheckNoneImplEv, @function
_ZN2v88internal28StringAdd_CheckNoneAssembler31GenerateStringAdd_CheckNoneImplEv:
.LFB23614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler9StringAddEPNS0_8compiler4NodeENS2_5TNodeINS0_6StringEEES7_@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE23614:
	.size	_ZN2v88internal28StringAdd_CheckNoneAssembler31GenerateStringAdd_CheckNoneImplEv, .-_ZN2v88internal28StringAdd_CheckNoneAssembler31GenerateStringAdd_CheckNoneImplEv
	.section	.text._ZN2v88internal18SubStringAssembler21GenerateSubStringImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18SubStringAssembler21GenerateSubStringImplEv
	.type	_ZN2v88internal18SubStringAssembler21GenerateSubStringImplEv, @function
_ZN2v88internal18SubStringAssembler21GenerateSubStringImplEv:
.LFB23623:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler9SubStringENS0_8compiler5TNodeINS0_6StringEEENS3_INS0_7IntPtrTEEES7_@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE23623:
	.size	_ZN2v88internal18SubStringAssembler21GenerateSubStringImplEv, .-_ZN2v88internal18SubStringAssembler21GenerateSubStringImplEv
	.section	.rodata._ZN2v88internal8Builtins18Generate_SubStringEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"SubString"
	.section	.text._ZN2v88internal8Builtins18Generate_SubStringEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins18Generate_SubStringEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins18Generate_SubStringEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins18Generate_SubStringEPNS0_8compiler18CodeAssemblerStateE:
.LFB23619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$299, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$707, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L274
.L271:
	movq	%r13, %rdi
	call	_ZN2v88internal18SubStringAssembler21GenerateSubStringImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L275
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L271
.L275:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23619:
	.size	_ZN2v88internal8Builtins18Generate_SubStringEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins18Generate_SubStringEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal23StringBuiltinsAssembler34GenerateStringRelationalComparisonENS0_8compiler5TNodeINS0_6StringEEES5_NS0_9OperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23StringBuiltinsAssembler34GenerateStringRelationalComparisonENS0_8compiler5TNodeINS0_6StringEEES5_NS0_9OperationE
	.type	_ZN2v88internal23StringBuiltinsAssembler34GenerateStringRelationalComparisonENS0_8compiler5TNodeINS0_6StringEEES5_NS0_9OperationE, @function
_ZN2v88internal23StringBuiltinsAssembler34GenerateStringRelationalComparisonENS0_8compiler5TNodeINS0_6StringEEES5_NS0_9OperationE:
.LFB23624:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1536(%rbp), %r15
	leaq	-1552(%rbp), %r14
	pushq	%r13
	movq	%r15, %xmm1
	movq	%r14, %xmm0
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$7, %edx
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	movq	%rsi, %rcx
	movq	%rdi, %rsi
	movq	%r14, %rdi
	subq	$1688, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -1568(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movq	%r13, %rcx
	movq	%r15, %rdi
	movl	$7, %edx
	movq	%r12, %rsi
	movq	%r15, %r13
	leaq	-1376(%rbp), %r15
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	leaq	-1504(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movdqa	-1568(%rbp), %xmm0
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1576(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-1248(%rbp), %rcx
	movq	%rcx, %r10
	xorl	%ecx, %ecx
	movq	%r10, %rdi
	movq	%r10, -1600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1120(%rbp), %rax
	leaq	-96(%rbp), %rcx
	movq	%r12, %rsi
	movl	$1, %r8d
	movl	$2, %edx
	movq	%rax, %rdi
	movq	%rax, -1592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1592(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1592(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	movq	%r14, -1664(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdi
	movq	%r13, -1672(%rbp)
	movq	%rax, -1568(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1568(%rbp), %r13
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %r14
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r13, -1568(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16LoadInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r14, -1608(%rbp)
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler16LoadInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r14, -1688(%rbp)
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32ShlENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r13, -1680(%rbp)
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler8Word32OrENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	leaq	-992(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, %r13
	movq	%r11, -1656(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-864(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r11, -1624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$2056, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$3855, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-1656(%rbp), %r14
	movq	-1624(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r14, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1568(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler22LoadStringLengthAsWordENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	movq	-1608(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler22LoadStringLengthAsWordENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r13, -1648(%rbp)
	movq	%rax, %rdx
	movq	%rax, -1720(%rbp)
	call	_ZN2v88internal17CodeStubAssembler9IntPtrMinENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEES5_@PLT
	movl	$15, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	leaq	-1520(%rbp), %r13
	movq	%rax, %r14
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rcx
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	-736(%rbp), %r14
	movq	%rax, -1640(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-224(%rbp), %rax
	movl	$1, %r8d
	movq	%r13, -224(%rbp)
	movq	%rax, %rcx
	movq	%rax, -1584(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-608(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r11, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r11, -1616(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-480(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r11, %rdi
	movq	%r12, %rsi
	movq	%r11, -1632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1640(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1632(%rbp), %r11
	movq	-1616(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r11, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1632(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	movq	%r11, -1712(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6Uint8TEvE5valueE(%rip), %esi
	movq	-1568(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	movl	%esi, -1632(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	movq	%r13, %rdi
	movq	%rax, -1640(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1608(%rbp), %rdx
	movl	-1632(%rbp), %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$2, %r8d
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeES5_NS0_15LoadSensitivityE@PLT
	leaq	-352(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r9, -1696(%rbp)
	movq	%rax, -1632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	-1584(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1632(%rbp), %rdx
	movq	-1640(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	-1696(%rbp), %r9
	movq	-1584(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1696(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -1704(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdi
	movq	%rax, -1696(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-1696(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1584(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1632(%rbp), %rdx
	movq	-1640(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14Uint32LessThanENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-1600(%rbp), %rcx
	movq	-1576(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1584(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1704(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1616(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1720(%rbp), %r10
	movq	-1648(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r10, %rdx
	movq	%r10, -1584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11IntPtrEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1584(%rbp), %r10
	movq	-1648(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-1600(%rbp), %rcx
	movq	-1576(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-1712(%rbp), %r11
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1616(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-1624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1592(%rbp), %r9
	movq	%r12, %rdi
	movq	-1688(%rbp), %r8
	movq	-1672(%rbp), %rcx
	movq	-1680(%rbp), %rdx
	movq	-1664(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler25MaybeDerefIndirectStringsEPNS0_8compiler26TypedCodeAssemblerVariableINS0_6StringEEENS2_5TNodeINS0_6Int32TEEES6_S9_PNS2_18CodeAssemblerLabelE@PLT
	cmpl	$20, %ebx
	je	.L277
	jg	.L278
	cmpl	$18, %ebx
	je	.L279
	cmpl	$19, %ebx
	jne	.L281
	movq	-1568(%rbp), %xmm0
	movq	%r12, %rdi
	movhps	-1608(%rbp), %xmm0
	movaps	%xmm0, -1568(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-80(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movdqa	-1568(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$2, %r9d
	movl	$337, %esi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
.L283:
	movq	-1576(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpl	$18, %ebx
	je	.L284
	cmpl	$19, %ebx
	jne	.L281
.L287:
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
.L288:
	movq	-1600(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpl	$19, %ebx
	jle	.L304
	leal	-20(%rbx), %ecx
	cmpl	$1, %ecx
	ja	.L281
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler12TrueConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
.L292:
	movq	-1624(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1656(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1592(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1600(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1576(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1672(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-1664(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L305
	addq	$1688, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L304:
	.cfi_restore_state
	cmpl	$17, %ebx
	jg	.L306
.L281:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L278:
	cmpl	$21, %ebx
	jne	.L281
	movq	-1568(%rbp), %xmm0
	movq	%r12, %rdi
	movhps	-1608(%rbp), %xmm0
	movaps	%xmm0, -1568(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-80(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movdqa	-1568(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$2, %r9d
	movl	$331, %esi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	-1576(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L306:
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L277:
	movq	-1568(%rbp), %xmm0
	movq	%r12, %rdi
	movhps	-1608(%rbp), %xmm0
	movaps	%xmm0, -1568(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-80(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movdqa	-1568(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$2, %r9d
	movl	$330, %esi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	-1576(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
.L284:
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L279:
	movq	-1568(%rbp), %xmm0
	movq	%r12, %rdi
	movhps	-1608(%rbp), %xmm0
	movaps	%xmm0, -1568(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	leaq	-80(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movdqa	-1568(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$2, %r9d
	movl	$336, %esi
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	jmp	.L283
.L305:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23624:
	.size	_ZN2v88internal23StringBuiltinsAssembler34GenerateStringRelationalComparisonENS0_8compiler5TNodeINS0_6StringEEES5_NS0_9OperationE, .-_ZN2v88internal23StringBuiltinsAssembler34GenerateStringRelationalComparisonENS0_8compiler5TNodeINS0_6StringEEES5_NS0_9OperationE
	.section	.rodata._ZN2v88internal8Builtins20Generate_StringEqualEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"StringEqual"
	.section	.text._ZN2v88internal8Builtins20Generate_StringEqualEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins20Generate_StringEqualEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins20Generate_StringEqualEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins20Generate_StringEqualEPNS0_8compiler18CodeAssemblerStateE:
.LFB23629:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$476, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$49, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L311
.L308:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal23StringBuiltinsAssembler19GenerateStringEqualENS0_8compiler5TNodeINS0_6StringEEES5_
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L312
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L311:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L308
.L312:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23629:
	.size	_ZN2v88internal8Builtins20Generate_StringEqualEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins20Generate_StringEqualEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal20StringEqualAssembler23GenerateStringEqualImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20StringEqualAssembler23GenerateStringEqualImplEv
	.type	_ZN2v88internal20StringEqualAssembler23GenerateStringEqualImplEv, @function
_ZN2v88internal20StringEqualAssembler23GenerateStringEqualImplEv:
.LFB23633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal23StringBuiltinsAssembler19GenerateStringEqualENS0_8compiler5TNodeINS0_6StringEEES5_
	.cfi_endproc
.LFE23633:
	.size	_ZN2v88internal20StringEqualAssembler23GenerateStringEqualImplEv, .-_ZN2v88internal20StringEqualAssembler23GenerateStringEqualImplEv
	.section	.rodata._ZN2v88internal8Builtins23Generate_StringLessThanEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"StringLessThan"
	.section	.text._ZN2v88internal8Builtins23Generate_StringLessThanEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins23Generate_StringLessThanEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins23Generate_StringLessThanEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins23Generate_StringLessThanEPNS0_8compiler18CodeAssemblerStateE:
.LFB23638:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$482, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$53, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L319
.L316:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$18, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal23StringBuiltinsAssembler34GenerateStringRelationalComparisonENS0_8compiler5TNodeINS0_6StringEEES5_NS0_9OperationE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L320
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L316
.L320:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23638:
	.size	_ZN2v88internal8Builtins23Generate_StringLessThanEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins23Generate_StringLessThanEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal23StringLessThanAssembler26GenerateStringLessThanImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23StringLessThanAssembler26GenerateStringLessThanImplEv
	.type	_ZN2v88internal23StringLessThanAssembler26GenerateStringLessThanImplEv, @function
_ZN2v88internal23StringLessThanAssembler26GenerateStringLessThanImplEv:
.LFB23642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$18, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal23StringBuiltinsAssembler34GenerateStringRelationalComparisonENS0_8compiler5TNodeINS0_6StringEEES5_NS0_9OperationE
	.cfi_endproc
.LFE23642:
	.size	_ZN2v88internal23StringLessThanAssembler26GenerateStringLessThanImplEv, .-_ZN2v88internal23StringLessThanAssembler26GenerateStringLessThanImplEv
	.section	.rodata._ZN2v88internal8Builtins30Generate_StringLessThanOrEqualEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"StringLessThanOrEqual"
	.section	.text._ZN2v88internal8Builtins30Generate_StringLessThanOrEqualEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins30Generate_StringLessThanOrEqualEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins30Generate_StringLessThanOrEqualEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins30Generate_StringLessThanOrEqualEPNS0_8compiler18CodeAssemblerStateE:
.LFB23647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$488, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$54, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L327
.L324:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$19, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal23StringBuiltinsAssembler34GenerateStringRelationalComparisonENS0_8compiler5TNodeINS0_6StringEEES5_NS0_9OperationE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L328
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L324
.L328:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23647:
	.size	_ZN2v88internal8Builtins30Generate_StringLessThanOrEqualEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins30Generate_StringLessThanOrEqualEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal30StringLessThanOrEqualAssembler33GenerateStringLessThanOrEqualImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30StringLessThanOrEqualAssembler33GenerateStringLessThanOrEqualImplEv
	.type	_ZN2v88internal30StringLessThanOrEqualAssembler33GenerateStringLessThanOrEqualImplEv, @function
_ZN2v88internal30StringLessThanOrEqualAssembler33GenerateStringLessThanOrEqualImplEv:
.LFB23651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$19, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal23StringBuiltinsAssembler34GenerateStringRelationalComparisonENS0_8compiler5TNodeINS0_6StringEEES5_NS0_9OperationE
	.cfi_endproc
.LFE23651:
	.size	_ZN2v88internal30StringLessThanOrEqualAssembler33GenerateStringLessThanOrEqualImplEv, .-_ZN2v88internal30StringLessThanOrEqualAssembler33GenerateStringLessThanOrEqualImplEv
	.section	.rodata._ZN2v88internal8Builtins26Generate_StringGreaterThanEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"StringGreaterThan"
	.section	.text._ZN2v88internal8Builtins26Generate_StringGreaterThanEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins26Generate_StringGreaterThanEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins26Generate_StringGreaterThanEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins26Generate_StringGreaterThanEPNS0_8compiler18CodeAssemblerStateE:
.LFB23656:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$494, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$50, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L335
.L332:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$20, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal23StringBuiltinsAssembler34GenerateStringRelationalComparisonENS0_8compiler5TNodeINS0_6StringEEES5_NS0_9OperationE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L336
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L332
.L336:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23656:
	.size	_ZN2v88internal8Builtins26Generate_StringGreaterThanEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins26Generate_StringGreaterThanEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal26StringGreaterThanAssembler29GenerateStringGreaterThanImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26StringGreaterThanAssembler29GenerateStringGreaterThanImplEv
	.type	_ZN2v88internal26StringGreaterThanAssembler29GenerateStringGreaterThanImplEv, @function
_ZN2v88internal26StringGreaterThanAssembler29GenerateStringGreaterThanImplEv:
.LFB23660:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$20, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal23StringBuiltinsAssembler34GenerateStringRelationalComparisonENS0_8compiler5TNodeINS0_6StringEEES5_NS0_9OperationE
	.cfi_endproc
.LFE23660:
	.size	_ZN2v88internal26StringGreaterThanAssembler29GenerateStringGreaterThanImplEv, .-_ZN2v88internal26StringGreaterThanAssembler29GenerateStringGreaterThanImplEv
	.section	.rodata._ZN2v88internal8Builtins33Generate_StringGreaterThanOrEqualEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"StringGreaterThanOrEqual"
	.section	.text._ZN2v88internal8Builtins33Generate_StringGreaterThanOrEqualEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins33Generate_StringGreaterThanOrEqualEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins33Generate_StringGreaterThanOrEqualEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins33Generate_StringGreaterThanOrEqualEPNS0_8compiler18CodeAssemblerStateE:
.LFB23665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$500, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC10(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$51, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L343
.L340:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$21, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal23StringBuiltinsAssembler34GenerateStringRelationalComparisonENS0_8compiler5TNodeINS0_6StringEEES5_NS0_9OperationE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L344
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L343:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L340
.L344:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23665:
	.size	_ZN2v88internal8Builtins33Generate_StringGreaterThanOrEqualEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins33Generate_StringGreaterThanOrEqualEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal33StringGreaterThanOrEqualAssembler36GenerateStringGreaterThanOrEqualImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33StringGreaterThanOrEqualAssembler36GenerateStringGreaterThanOrEqualImplEv
	.type	_ZN2v88internal33StringGreaterThanOrEqualAssembler36GenerateStringGreaterThanOrEqualImplEv, @function
_ZN2v88internal33StringGreaterThanOrEqualAssembler36GenerateStringGreaterThanOrEqualImplEv:
.LFB23669:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$21, %ecx
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal23StringBuiltinsAssembler34GenerateStringRelationalComparisonENS0_8compiler5TNodeINS0_6StringEEES5_NS0_9OperationE
	.cfi_endproc
.LFE23669:
	.size	_ZN2v88internal33StringGreaterThanOrEqualAssembler36GenerateStringGreaterThanOrEqualImplEv, .-_ZN2v88internal33StringGreaterThanOrEqualAssembler36GenerateStringGreaterThanOrEqualImplEv
	.section	.rodata._ZN2v88internal8Builtins21Generate_StringCharAtEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"StringCharAt"
	.section	.text._ZN2v88internal8Builtins21Generate_StringCharAtEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins21Generate_StringCharAtEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins21Generate_StringCharAtEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins21Generate_StringCharAtEPNS0_8compiler18CodeAssemblerStateE:
.LFB23674:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$507, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$46, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L351
.L348:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler16StringCharCodeAtENS0_8compiler11SloppyTNodeINS0_6StringEEENS3_INS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler24StringFromSingleCharCodeENS0_8compiler5TNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L352
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L351:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L348
.L352:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23674:
	.size	_ZN2v88internal8Builtins21Generate_StringCharAtEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins21Generate_StringCharAtEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal21StringCharAtAssembler24GenerateStringCharAtImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21StringCharAtAssembler24GenerateStringCharAtImplEv
	.type	_ZN2v88internal21StringCharAtAssembler24GenerateStringCharAtImplEv, @function
_ZN2v88internal21StringCharAtAssembler24GenerateStringCharAtImplEv:
.LFB23678:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler16StringCharCodeAtENS0_8compiler11SloppyTNodeINS0_6StringEEENS3_INS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler24StringFromSingleCharCodeENS0_8compiler5TNodeINS0_6Int32TEEE@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE23678:
	.size	_ZN2v88internal21StringCharAtAssembler24GenerateStringCharAtImplEv, .-_ZN2v88internal21StringCharAtAssembler24GenerateStringCharAtImplEv
	.section	.text._ZN2v88internal27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEv
	.type	_ZN2v88internal27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEv, @function
_ZN2v88internal27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEv:
.LFB23705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-528(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-352(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-480(%rbp), %rbx
	subq	$616, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -624(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubArgumentsC1EPNS0_17CodeStubAssemblerEPNS0_8compiler4NodeES6_NS2_13ParameterModeENS1_12ReceiverModeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r13, -632(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNK2v88internal17CodeStubArguments7AtIndexEi@PLT
	movq	-624(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler22TruncateTaggedToWord32EPNS0_8compiler4NodeES4_@PLT
	movl	$65535, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	-576(%rbp), %r13
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler24StringFromSingleCharCodeENS0_8compiler5TNodeINS0_6Int32TEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	-632(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -584(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-224(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -608(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler24AllocateSeqOneByteStringENS0_8compiler5TNodeINS0_7Uint32TEEENS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -600(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler4zoneEv@PLT
	movq	$0, -536(%rbp)
	movq	$0, -544(%rbp)
	movq	24(%rax), %rcx
	movq	16(%rax), %rdx
	movq	%rax, -560(%rbp)
	movq	$0, -552(%rbp)
	subq	%rdx, %rcx
	cmpq	$7, %rcx
	jbe	.L368
	leaq	8(%rdx), %rcx
	movq	%rcx, 16(%rax)
.L357:
	movq	%rdx, -552(%rbp)
	movq	%r12, %xmm1
	movl	$48, %edi
	movq	%rcx, -536(%rbp)
	movhps	-624(%rbp), %xmm1
	movq	%r13, (%rdx)
	movaps	%xmm1, -624(%rbp)
	movq	%rcx, -544(%rbp)
	movq	$0, -80(%rbp)
	call	_Znwm@PLT
	movq	%r13, %xmm2
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	-600(%rbp), %rcx
	movq	%rax, -96(%rbp)
	leaq	-96(%rbp), %r10
	leaq	-584(%rbp), %rdx
	movq	-608(%rbp), %xmm0
	movdqa	-624(%rbp), %xmm1
	movq	%rdx, 32(%rax)
	movq	%r10, %rdx
	movq	%rcx, 40(%rax)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEvEUlPNS2_8compiler4NodeEE_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rcx
	leaq	-560(%rbp), %rsi
	movl	$1, %r9d
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm1, (%rax)
	movups	%xmm0, 16(%rax)
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEvEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	movq	%rcx, %xmm0
	xorl	%ecx, %ecx
	movq	%rax, %xmm3
	movq	%r10, -648(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movq	%rsi, -640(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal17CodeStubArguments7ForEachERKNS0_10ZoneVectorIPNS0_8compiler21CodeAssemblerVariableEEERKSt8functionIFvPNS3_4NodeEEESB_SB_NS0_17CodeStubAssembler13ParameterModeE@PLT
	movq	-80(%rbp), %rax
	movq	-648(%rbp), %r10
	testq	%rax, %rax
	je	.L358
	movq	%r10, %rsi
	movq	%r10, %rdi
	movl	$3, %edx
	call	*%rax
	movq	-648(%rbp), %r10
.L358:
	movq	-600(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r10, -656(%rbp)
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	-608(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler24AllocateSeqTwoByteStringENS0_8compiler5TNodeINS0_7Uint32TEEENS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdi
	movq	%rax, -648(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	pushq	$1
	movq	%r15, %rdx
	movq	%r12, %rdi
	pushq	$0
	movq	-648(%rbp), %rcx
	movq	%rax, %r9
	movq	-600(%rbp), %rsi
	movq	%rcx, %r8
	call	_ZN2v88internal17CodeStubAssembler20CopyStringCharactersEPNS0_8compiler4NodeES4_NS2_5TNodeINS0_7IntPtrTEEES7_S7_NS0_6String8EncodingES9_@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movl	$15, %r8d
	movl	$1, %ecx
	movq	%rax, %rsi
	movl	$19, %edx
	call	_ZN2v88internal17CodeStubAssembler22ElementOffsetFromIndexEPNS0_8compiler4NodeENS0_12ElementsKindENS1_13ParameterModeEi@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	$3, %esi
	movq	-584(%rbp), %r8
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler19StoreNoWriteBarrierENS0_21MachineRepresentationEPNS1_4NodeES5_S5_@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdi
	movq	%rax, -600(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-600(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	$32, %edi
	movq	$0, -80(%rbp)
	movq	%rax, %r12
	call	_Znwm@PLT
	movdqa	-624(%rbp), %xmm4
	xorl	%r8d, %r8d
	movq	-656(%rbp), %r10
	movq	%r15, 16(%rax)
	movl	$1, %r9d
	movq	%r14, %rdi
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEvEUlPNS2_8compiler4NodeEE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rcx
	movq	%r13, 24(%rax)
	movq	%rcx, %xmm0
	movq	%r10, %rdx
	movq	%r12, %rcx
	movq	%rax, -96(%rbp)
	movq	-640(%rbp), %rsi
	movups	%xmm4, (%rax)
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEvEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	movq	%rax, %xmm5
	movq	%r10, -600(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal17CodeStubArguments7ForEachERKNS0_10ZoneVectorIPNS0_8compiler21CodeAssemblerVariableEEERKSt8functionIFvPNS3_4NodeEEESB_SB_NS0_17CodeStubAssembler13ParameterModeE@PLT
	movq	-80(%rbp), %rax
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L359
	movq	-600(%rbp), %r10
	movl	$3, %edx
	movq	%r10, %rsi
	movq	%r10, %rdi
	call	*%rax
.L359:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-608(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-632(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L369
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	movl	$8, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdx
	leaq	8(%rax), %rcx
	jmp	.L357
.L369:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23705:
	.size	_ZN2v88internal27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEv, .-_ZN2v88internal27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEv
	.section	.rodata._ZN2v88internal8Builtins27Generate_StringFromCharCodeEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"StringFromCharCode"
	.section	.text._ZN2v88internal8Builtins27Generate_StringFromCharCodeEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins27Generate_StringFromCharCodeEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins27Generate_StringFromCharCodeEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins27Generate_StringFromCharCodeEPNS0_8compiler18CodeAssemblerStateE:
.LFB23701:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$554, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$589, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L374
.L371:
	movq	%r13, %rdi
	call	_ZN2v88internal27StringFromCharCodeAssembler30GenerateStringFromCharCodeImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L375
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L374:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L371
.L375:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23701:
	.size	_ZN2v88internal8Builtins27Generate_StringFromCharCodeEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins27Generate_StringFromCharCodeEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal23StringBuiltinsAssembler13StringIndexOfENS0_8compiler5TNodeINS0_6StringEEES5_NS3_INS0_3SmiEEERKSt8functionIFvS7_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23StringBuiltinsAssembler13StringIndexOfENS0_8compiler5TNodeINS0_6StringEEES5_NS3_INS0_3SmiEEERKSt8functionIFvS7_EE
	.type	_ZN2v88internal23StringBuiltinsAssembler13StringIndexOfENS0_8compiler5TNodeINS0_6StringEEES5_NS3_INS0_3SmiEEERKSt8functionIFvS7_EE, @function
_ZN2v88internal23StringBuiltinsAssembler13StringIndexOfENS0_8compiler5TNodeINS0_6StringEEES5_NS3_INS0_3SmiEEERKSt8functionIFvS7_EE:
.LFB23711:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	xorl	%esi, %esi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$1912, %rsp
	movq	%rcx, -1880(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r13, -1784(%rbp)
	movq	%rax, %r14
	leaq	-1312(%rbp), %r13
	call	_ZN2v88internal17CodeStubAssembler22LoadStringLengthAsWordENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r15, -1872(%rbp)
	movq	%rax, -1736(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22LoadStringLengthAsWordENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	movq	-1880(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -1744(%rbp)
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler9IntPtrMaxENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEES5_@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-1440(%rbp), %rcx
	movq	%rax, %r15
	movq	%rcx, %rdi
	xorl	%ecx, %ecx
	movq	%rdi, -1832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1736(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11IntPtrEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1832(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1744(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r15, -1728(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-1736(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler21IntPtrLessThanOrEqualENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r13, -1808(%rbp)
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1184(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, %r13
	movq	%rax, -1888(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1872(%rbp), %r15
	movq	-1784(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	(%r12), %rsi
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	leaq	-1632(%rbp), %rax
	leaq	-1056(%rbp), %r15
	movq	%rax, %rdi
	movq	%rax, %r13
	movq	%rax, -1936(%rbp)
	call	_ZN2v88internal23ToDirectStringAssemblerC1EPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_6StringEEENS_4base5FlagsINS1_4FlagEiEE@PLT
	leaq	-1536(%rbp), %rax
	movq	-1784(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	(%r12), %rsi
	movq	%rax, %rdi
	movq	%rax, -1792(%rbp)
	call	_ZN2v88internal23ToDirectStringAssemblerC1EPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_6StringEEENS_4base5FlagsINS1_4FlagEiEE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%r15, -1896(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23ToDirectStringAssembler11TryToDirectEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1792(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal23ToDirectStringAssembler11TryToDirectEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal23ToDirectStringAssembler15TryToSequentialENS1_17StringPointerKindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1792(%rbp), %rdi
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, -1768(%rbp)
	call	_ZN2v88internal23ToDirectStringAssembler15TryToSequentialENS1_17StringPointerKindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-1584(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -1952(%rbp)
	movq	%rax, -1776(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	leaq	-1488(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -1920(%rbp)
	movq	%rax, -1760(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-928(%rbp), %rcx
	movq	%rax, -1752(%rbp)
	movq	%rcx, %r15
	movq	%rcx, -1928(%rbp)
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-800(%rbp), %rcx
	movq	%rcx, %rdi
	xorl	%ecx, %ecx
	movq	%rdi, -1800(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-672(%rbp), %rcx
	movq	%rcx, %r10
	xorl	%ecx, %ecx
	movq	%r10, %rdi
	movq	%r10, -1824(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-544(%rbp), %rcx
	movq	%rcx, %r11
	xorl	%ecx, %ecx
	movq	%r11, %rdi
	movq	%r11, -1816(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1504(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -1912(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	leaq	-1600(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rax, %r13
	movq	%rcx, -1944(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	subq	$8, %rsp
	movq	%r15, %rcx
	movq	%r13, %rdx
	pushq	-1816(%rbp)
	movq	-1824(%rbp), %r9
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	-1800(%rbp), %r8
	call	_ZN2v88internal23StringBuiltinsAssembler25DispatchOnStringEncodingsEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelES6_S6_S6_
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	-416(%rbp), %r15
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	-1760(%rbp), %rsi
	movl	$17, %edx
	call	_ZN2v88internal17CodeStubAssembler22ElementOffsetFromIndexEPNS0_8compiler4NodeENS0_12ElementsKindENS1_13ParameterModeEi@PLT
	movq	-1768(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	-1752(%rbp), %rsi
	movl	$17, %edx
	movq	%rax, -1856(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22ElementOffsetFromIndexEPNS0_8compiler4NodeENS0_12ElementsKindENS1_13ParameterModeEi@PLT
	movq	-1776(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, -1864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	-288(%rbp), %rcx
	movq	%rcx, %r13
	movq	%rcx, -1840(%rbp)
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-1736(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11IntPtrEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-1728(%rbp), %r13
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-1744(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r13, -1728(%rbp)
	movq	%rax, -1848(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-1864(%rbp), %rdx
	movl	$2, %ecx
	movq	%r12, %rdi
	movl	$770, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler4LoadENS0_11MachineTypeEPNS1_4NodeENS0_15LoadSensitivityE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%rax, -1904(%rbp)
	call	_ZN2v88internal17ExternalReference20libc_memchr_functionEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	-1904(%rbp), %rdx
	movq	%r13, -120(%rbp)
	leaq	-160(%rbp), %r13
	movq	%rax, %rsi
	movq	-1848(%rbp), %rax
	movq	%r13, %rcx
	movq	%r12, %rdi
	movl	$5, %r10d
	movl	$1029, %r11d
	movl	$3, %r8d
	movq	%rdx, -136(%rbp)
	movq	%rax, -152(%rbp)
	movl	$5, %edx
	movl	$1285, %eax
	movw	%r10w, -160(%rbp)
	movw	%r11w, -144(%rbp)
	movw	%ax, -128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallCFunctionEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS5_S4_EE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-1808(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1904(%rbp), %r8
	movq	-1848(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-1728(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	cmpq	$0, 16(%rbx)
	movq	%rax, -1640(%rbp)
	popq	%rax
	popq	%rdx
	je	.L378
	leaq	-1640(%rbp), %rsi
	movq	%rbx, %rdi
	call	*24(%rbx)
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	call	_ZN2v88internal17ExternalReference17search_string_rawIKhS3_EES1_v@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movl	$5, %ecx
	movl	$5, %edx
	movl	$1029, %esi
	movl	$5, %edi
	movl	$1029, %r8d
	movw	%si, -128(%rbp)
	movl	$1029, %r9d
	movw	%cx, -144(%rbp)
	movq	-1864(%rbp), %rcx
	movq	%r14, %rsi
	movq	%rax, -152(%rbp)
	movq	-1856(%rbp), %rax
	movw	%dx, -160(%rbp)
	movq	-1728(%rbp), %rdx
	movq	%rcx, -104(%rbp)
	movq	-1736(%rbp), %rcx
	movq	%rax, -136(%rbp)
	movq	-1744(%rbp), %rax
	movw	%di, -112(%rbp)
	movq	%r12, %rdi
	movw	%r8w, -96(%rbp)
	movl	$6, %r8d
	movq	%rcx, -88(%rbp)
	movq	%r13, %rcx
	movq	%rdx, -72(%rbp)
	movl	$1029, %edx
	movw	%r9w, -80(%rbp)
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallCFunctionEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS5_S4_EE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	cmpq	$0, 16(%rbx)
	movq	%rax, -1648(%rbp)
	je	.L378
	leaq	-1648(%rbp), %rsi
	movq	%rbx, %rdi
	call	*24(%rbx)
	movq	-1840(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1800(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	-1760(%rbp), %rsi
	movl	$17, %edx
	call	_ZN2v88internal17CodeStubAssembler22ElementOffsetFromIndexEPNS0_8compiler4NodeENS0_12ElementsKindENS1_13ParameterModeEi@PLT
	movq	-1768(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	-1752(%rbp), %rsi
	movl	$19, %edx
	movq	%rax, -1848(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22ElementOffsetFromIndexEPNS0_8compiler4NodeENS0_12ElementsKindENS1_13ParameterModeEi@PLT
	movq	-1776(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%rax, %r15
	call	_ZN2v88internal17ExternalReference17search_string_rawIKhKtEES1_v@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	-1736(%rbp), %rcx
	movq	%r14, %rsi
	movq	-1728(%rbp), %rdx
	movq	%rax, -152(%rbp)
	movl	$1029, %eax
	movq	%r12, %rdi
	movq	-1848(%rbp), %r9
	movw	%ax, -128(%rbp)
	movl	$5, %r10d
	movl	$5, %r11d
	movq	-1744(%rbp), %rax
	movq	%rcx, -88(%rbp)
	movl	$6, %r8d
	movq	%r13, %rcx
	movq	%rax, -120(%rbp)
	movl	$5, %eax
	movw	%ax, -112(%rbp)
	movl	$1029, %eax
	movw	%ax, -96(%rbp)
	movl	$1029, %eax
	movq	%rdx, -72(%rbp)
	movl	$1029, %edx
	movw	%r10w, -160(%rbp)
	movw	%r11w, -144(%rbp)
	movq	%r9, -136(%rbp)
	movq	%r15, -104(%rbp)
	movw	%ax, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallCFunctionEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS5_S4_EE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	cmpq	$0, 16(%rbx)
	movq	%rax, -1656(%rbp)
	je	.L378
	leaq	-1656(%rbp), %rsi
	movq	%rbx, %rdi
	call	*24(%rbx)
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	-1760(%rbp), %rsi
	movl	$19, %edx
	call	_ZN2v88internal17CodeStubAssembler22ElementOffsetFromIndexEPNS0_8compiler4NodeENS0_12ElementsKindENS1_13ParameterModeEi@PLT
	movq	-1768(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	-1752(%rbp), %rsi
	movl	$17, %edx
	movq	%rax, -1848(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22ElementOffsetFromIndexEPNS0_8compiler4NodeENS0_12ElementsKindENS1_13ParameterModeEi@PLT
	movq	-1776(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%rax, %r15
	call	_ZN2v88internal17ExternalReference17search_string_rawIKtKhEES1_v@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movl	$5, %edx
	movl	$5, %ecx
	movq	-1848(%rbp), %r9
	movl	$1029, %esi
	movl	$5, %edi
	movl	$1029, %r8d
	movw	%dx, -160(%rbp)
	movw	%cx, -144(%rbp)
	movq	-1728(%rbp), %rdx
	movq	-1736(%rbp), %rcx
	movq	%rax, -152(%rbp)
	movq	-1744(%rbp), %rax
	movq	%r9, -136(%rbp)
	movl	$1029, %r9d
	movw	%si, -128(%rbp)
	movq	%r14, %rsi
	movw	%di, -112(%rbp)
	movq	%r12, %rdi
	movw	%r8w, -96(%rbp)
	movl	$6, %r8d
	movq	%rcx, -88(%rbp)
	movq	%r13, %rcx
	movq	%rdx, -72(%rbp)
	movl	$1029, %edx
	movw	%r9w, -80(%rbp)
	movq	%rax, -120(%rbp)
	movq	%r15, -104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallCFunctionEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS5_S4_EE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	cmpq	$0, 16(%rbx)
	movq	%rax, -1664(%rbp)
	je	.L378
	leaq	-1664(%rbp), %rsi
	movq	%rbx, %rdi
	call	*24(%rbx)
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	-1760(%rbp), %rsi
	movl	$19, %edx
	call	_ZN2v88internal17CodeStubAssembler22ElementOffsetFromIndexEPNS0_8compiler4NodeENS0_12ElementsKindENS1_13ParameterModeEi@PLT
	movq	-1768(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	-1752(%rbp), %rsi
	movl	$19, %edx
	movq	%rax, -1760(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22ElementOffsetFromIndexEPNS0_8compiler4NodeENS0_12ElementsKindENS1_13ParameterModeEi@PLT
	movq	-1776(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%rax, %r15
	call	_ZN2v88internal17ExternalReference17search_string_rawIKtS3_EES1_v@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal17ExternalReference15isolate_addressEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	-1760(%rbp), %r9
	movl	$5, %esi
	movq	-1736(%rbp), %rcx
	movq	-1728(%rbp), %rdx
	movl	$5, %edi
	movq	%r15, -104(%rbp)
	movl	$1029, %r8d
	movl	$1029, %r10d
	movq	%rax, -152(%rbp)
	movq	-1744(%rbp), %rax
	movl	$1029, %r11d
	movw	%si, -160(%rbp)
	movq	%r14, %rsi
	movw	%di, -144(%rbp)
	movq	%r12, %rdi
	movq	%r9, -136(%rbp)
	movl	$5, %r9d
	movw	%r8w, -128(%rbp)
	movl	$6, %r8d
	movq	%rcx, -88(%rbp)
	movq	%r13, %rcx
	movq	%rdx, -72(%rbp)
	movl	$1029, %edx
	movw	%r9w, -112(%rbp)
	movw	%r10w, -96(%rbp)
	movw	%r11w, -80(%rbp)
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallCFunctionEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS5_S4_EE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	cmpq	$0, 16(%rbx)
	movq	%rax, -1672(%rbp)
	je	.L378
	leaq	-1672(%rbp), %rsi
	movq	%rbx, %rdi
	call	*24(%rbx)
	movq	-1808(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$-1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	cmpq	$0, 16(%rbx)
	movq	%rax, -1680(%rbp)
	je	.L378
	leaq	-1680(%rbp), %rsi
	movq	%rbx, %rdi
	call	*24(%rbx)
	movq	-1888(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	cmpq	$0, 16(%rbx)
	movq	%rax, -1688(%rbp)
	je	.L378
	leaq	-1688(%rbp), %rsi
	movq	%rbx, %rdi
	call	*24(%rbx)
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L385
.L379:
	movq	-1728(%rbp), %rdx
	movq	-1744(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler9IntPtrMinENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	cmpq	$0, 16(%rbx)
	movq	%rax, -1696(%rbp)
	je	.L378
	leaq	-1696(%rbp), %rsi
	movq	%rbx, %rdi
	call	*24(%rbx)
	movq	-1896(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L386
.L381:
	movq	-1872(%rbp), %xmm0
	movq	%r12, %rdi
	movhps	-1784(%rbp), %xmm0
	movaps	%xmm0, -1728(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17NoContextConstantEv@PLT
	movq	%r13, %rcx
	movl	$334, %esi
	movq	%r12, %rdi
	movdqa	-1728(%rbp), %xmm0
	movq	%rax, %rdx
	movq	-1880(%rbp), %rax
	movl	$3, %r8d
	movq	%rax, -144(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	cmpq	$0, 16(%rbx)
	movq	%rax, -1704(%rbp)
	je	.L378
	leaq	-1704(%rbp), %rsi
	movq	%rbx, %rdi
	call	*24(%rbx)
	movq	-1816(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1824(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1800(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1928(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1896(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	leaq	-1472(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-1920(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-1912(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	leaq	-1520(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-1792(%rbp), %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-1568(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-1952(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-1944(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	leaq	-1616(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-1936(%rbp), %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1888(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1808(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1832(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L387
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L385:
	.cfi_restore_state
	movq	-1840(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	leaq	-144(%rbp), %r14
	movq	%r14, -160(%rbp)
	movq	$22, -288(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movl	$26478, %ecx
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	-288(%rbp), %rdx
	movdqa	.LC13(%rip), %xmm0
	movq	%rax, -160(%rbp)
	movq	%rdx, -144(%rbp)
	movl	$1769108595, 16(%rax)
	movw	%cx, 20(%rax)
	movups	%xmm0, (%rax)
	movq	-288(%rbp), %rax
	movq	-160(%rbp), %rdx
	movq	%rax, -152(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-160(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L379
	call	_ZdlPv@PLT
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L386:
	movq	-1840(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	leaq	-144(%rbp), %r14
	movq	%r14, -160(%rbp)
	movq	$22, -288(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-288(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r13, %rsi
	movdqa	.LC14(%rip), %xmm0
	movq	%rax, -160(%rbp)
	movq	%rdx, -144(%rbp)
	movl	$25701, %edx
	movl	$1801676136, 16(%rax)
	movw	%dx, 20(%rax)
	movups	%xmm0, (%rax)
	movq	-288(%rbp), %rax
	movq	-160(%rbp), %rdx
	movq	%rax, -152(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-160(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L381
	call	_ZdlPv@PLT
	jmp	.L381
.L378:
	call	_ZSt25__throw_bad_function_callv@PLT
.L387:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23711:
	.size	_ZN2v88internal23StringBuiltinsAssembler13StringIndexOfENS0_8compiler5TNodeINS0_6StringEEES5_NS3_INS0_3SmiEEERKSt8functionIFvS7_EE, .-_ZN2v88internal23StringBuiltinsAssembler13StringIndexOfENS0_8compiler5TNodeINS0_6StringEEES5_NS3_INS0_3SmiEEERKSt8functionIFvS7_EE
	.section	.text._ZN2v88internal22StringIndexOfAssembler25GenerateStringIndexOfImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22StringIndexOfAssembler25GenerateStringIndexOfImplEv
	.type	_ZN2v88internal22StringIndexOfAssembler25GenerateStringIndexOfImplEv, @function
_ZN2v88internal22StringIndexOfAssembler25GenerateStringIndexOfImplEv:
.LFB23744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r15, %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal22StringIndexOfAssembler25GenerateStringIndexOfImplEvEUlNS2_8compiler5TNodeINS2_3SmiEEEE_E10_M_managerERSt9_Any_dataRKSA_St18_Manager_operation(%rip), %rdx
	leaq	_ZNSt17_Function_handlerIFvN2v88internal8compiler5TNodeINS1_3SmiEEEEZNS1_22StringIndexOfAssembler25GenerateStringIndexOfImplEvEUlS5_E_E9_M_invokeERKSt9_Any_dataOS5_(%rip), %rax
	movq	%r12, -80(%rbp)
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	movq	%r14, %rdx
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal23StringBuiltinsAssembler13StringIndexOfENS0_8compiler5TNodeINS0_6StringEEES5_NS3_INS0_3SmiEEERKSt8functionIFvS7_EE
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L388
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
.L388:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L395
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L395:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23744:
	.size	_ZN2v88internal22StringIndexOfAssembler25GenerateStringIndexOfImplEv, .-_ZN2v88internal22StringIndexOfAssembler25GenerateStringIndexOfImplEv
	.section	.rodata._ZN2v88internal8Builtins22Generate_StringIndexOfEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"StringIndexOf"
	.section	.text._ZN2v88internal8Builtins22Generate_StringIndexOfEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins22Generate_StringIndexOfEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins22Generate_StringIndexOfEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins22Generate_StringIndexOfEPNS0_8compiler18CodeAssemblerStateE:
.LFB23740:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$834, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC15(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$52, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L400
.L397:
	movq	%r13, %rdi
	call	_ZN2v88internal22StringIndexOfAssembler25GenerateStringIndexOfImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L401
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L397
.L401:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23740:
	.size	_ZN2v88internal8Builtins22Generate_StringIndexOfEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins22Generate_StringIndexOfEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal30StringIncludesIndexOfAssembler8GenerateENS1_13SearchVariantENS0_8compiler5TNodeINS0_7IntPtrTEEENS4_INS0_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30StringIncludesIndexOfAssembler8GenerateENS1_13SearchVariantENS0_8compiler5TNodeINS0_7IntPtrTEEENS4_INS0_7ContextEEE
	.type	_ZN2v88internal30StringIncludesIndexOfAssembler8GenerateENS1_13SearchVariantENS0_8compiler5TNodeINS0_7IntPtrTEEENS4_INS0_7ContextEEE, @function
_ZN2v88internal30StringIncludesIndexOfAssembler8GenerateENS1_13SearchVariantENS0_8compiler5TNodeINS0_7IntPtrTEEENS4_INS0_7ContextEEE:
.LFB23770:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-672(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-352(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$744, %rsp
	movl	%esi, -692(%rbp)
	movq	%rdi, %rsi
	movq	%rcx, -744(%rbp)
	xorl	%ecx, %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-656(%rbp), %rax
	movq	%rax, %r15
	movq	%rax, %rdi
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal17CodeStubArgumentsC1EPNS0_17CodeStubAssemblerEPNS0_8compiler4NodeES6_NS2_13ParameterModeENS1_12ReceiverModeE@PLT
	movq	%r15, %rdi
	leaq	-688(%rbp), %r15
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, -712(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-608(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -728(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-480(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -736(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-224(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -720(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11IntPtrEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-728(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	-96(%rbp), %rbx
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler17IntPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-736(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L422
.L403:
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, -752(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-752(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-728(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L423
.L405:
	movq	-704(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZNK2v88internal17CodeStubArguments7AtIndexEi@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-720(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-736(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L424
.L407:
	movq	-704(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZNK2v88internal17CodeStubArguments7AtIndexEi@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-704(%rbp), %rdi
	movl	$1, %esi
	call	_ZNK2v88internal17CodeStubArguments7AtIndexEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-720(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-720(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L425
.L409:
	leaq	-692(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, %xmm0
	movhps	-704(%rbp), %xmm0
	movaps	%xmm0, -784(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rdi
	movq	%rax, -752(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -760(%rbp)
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-752(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler8IsStringENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-752(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler8IsStringENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$24, %edi
	movq	$0, -80(%rbp)
	call	_Znwm@PLT
	movdqa	-784(%rbp), %xmm0
	movq	%rbx, %r8
	movq	-752(%rbp), %r9
	movq	%r12, 16(%rax)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal30StringIncludesIndexOfAssembler8GenerateENS3_13SearchVariantENS2_8compiler5TNodeINS2_7IntPtrTEEENS6_INS2_7ContextEEEEUlNS6_INS2_3SmiEEEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation(%rip), %rcx
	movq	%r12, %rdi
	movq	-712(%rbp), %rsi
	movups	%xmm0, (%rax)
	movq	%rcx, %xmm0
	movq	-760(%rbp), %rcx
	movq	%r9, %rdx
	movq	%rax, -96(%rbp)
	leaq	_ZNSt17_Function_handlerIFvN2v88internal8compiler5TNodeINS1_3SmiEEEEZNS1_30StringIncludesIndexOfAssembler8GenerateENS7_13SearchVariantENS3_INS1_7IntPtrTEEENS3_INS1_7ContextEEEEUlS5_E_E9_M_invokeERKSt9_Any_dataOS5_(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal23StringBuiltinsAssembler13StringIndexOfENS0_8compiler5TNodeINS0_6StringEEES5_NS3_INS0_3SmiEEERKSt8functionIFvS7_EE
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L411
	movl	$3, %edx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	*%rax
.L411:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L426
.L412:
	xorl	%esi, %esi
	cmpl	$1, -692(%rbp)
	movq	%r14, %rdi
	sete	%sil
	addl	$332, %esi
	movl	%esi, -760(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r15, %rdi
	movq	%rax, -752(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-752(%rbp), %rdx
	movq	%rbx, %rcx
	movq	-712(%rbp), %xmm0
	movq	%rax, %xmm2
	movl	-760(%rbp), %esi
	movl	$3, %r8d
	movq	%r12, %rdi
	movq	%rdx, -80(%rbp)
	movq	-744(%rbp), %rdx
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	-704(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	-720(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-736(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-728(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L427
	addq	$744, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	.cfi_restore_state
	leaq	-80(%rbp), %rax
	movl	$29537, %ecx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%rax, -96(%rbp)
	movq	%rax, -752(%rbp)
	movabsq	$7308626857451135024, %rax
	movq	%rax, -80(%rbp)
	movl	$1663071342, -72(%rbp)
	movw	%cx, -68(%rbp)
	movb	$101, -66(%rbp)
	movq	$15, -88(%rbp)
	movb	$0, -65(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-752(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L403
	call	_ZdlPv@PLT
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L426:
	leaq	-80(%rbp), %rax
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movb	$0, -68(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rax, -752(%rbp)
	movabsq	$7959358215658692931, %rax
	movq	%rax, -80(%rbp)
	movl	$1701669236, -72(%rbp)
	movq	$12, -88(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-752(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L412
	call	_ZdlPv@PLT
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L425:
	leaq	-80(%rbp), %rax
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movb	$104, -72(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rax, -752(%rbp)
	movabsq	$8386072081463468358, %rax
	movq	%rax, -80(%rbp)
	movq	$9, -88(%rbp)
	movb	$0, -71(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-752(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L409
	call	_ZdlPv@PLT
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L424:
	leaq	-80(%rbp), %rax
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movb	$101, -66(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rax, -752(%rbp)
	movabsq	$7308626857451135026, %rax
	movq	%rax, -80(%rbp)
	movl	$29537, %eax
	movw	%ax, -68(%rbp)
	movl	$1663071342, -72(%rbp)
	movq	$15, -88(%rbp)
	movb	$0, -65(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-752(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L407
	call	_ZdlPv@PLT
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L423:
	leaq	-80(%rbp), %rax
	movl	$29537, %edx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%rax, -96(%rbp)
	movq	%rax, -752(%rbp)
	movabsq	$7308626857451135025, %rax
	movq	%rax, -80(%rbp)
	movl	$1663071342, -72(%rbp)
	movw	%dx, -68(%rbp)
	movb	$101, -66(%rbp)
	movq	$15, -88(%rbp)
	movb	$0, -65(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7CommentENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	movq	-752(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L405
	call	_ZdlPv@PLT
	jmp	.L405
.L427:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23770:
	.size	_ZN2v88internal30StringIncludesIndexOfAssembler8GenerateENS1_13SearchVariantENS0_8compiler5TNodeINS0_7IntPtrTEEENS4_INS0_7ContextEEE, .-_ZN2v88internal30StringIncludesIndexOfAssembler8GenerateENS1_13SearchVariantENS0_8compiler5TNodeINS0_7IntPtrTEEENS4_INS0_7ContextEEE
	.section	.text._ZN2v88internal32StringPrototypeIncludesAssembler35GenerateStringPrototypeIncludesImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32StringPrototypeIncludesAssembler35GenerateStringPrototypeIncludesImplEv
	.type	_ZN2v88internal32StringPrototypeIncludesAssembler35GenerateStringPrototypeIncludesImplEv, @function
_ZN2v88internal32StringPrototypeIncludesAssembler35GenerateStringPrototypeIncludesImplEv:
.LFB23760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%r12
	movq	%rax, %rcx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal30StringIncludesIndexOfAssembler8GenerateENS1_13SearchVariantENS0_8compiler5TNodeINS0_7IntPtrTEEENS4_INS0_7ContextEEE
	.cfi_endproc
.LFE23760:
	.size	_ZN2v88internal32StringPrototypeIncludesAssembler35GenerateStringPrototypeIncludesImplEv, .-_ZN2v88internal32StringPrototypeIncludesAssembler35GenerateStringPrototypeIncludesImplEv
	.section	.text._ZN2v88internal31StringPrototypeIndexOfAssembler34GenerateStringPrototypeIndexOfImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31StringPrototypeIndexOfAssembler34GenerateStringPrototypeIndexOfImplEv
	.type	_ZN2v88internal31StringPrototypeIndexOfAssembler34GenerateStringPrototypeIndexOfImplEv, @function
_ZN2v88internal31StringPrototypeIndexOfAssembler34GenerateStringPrototypeIndexOfImplEv:
.LFB23769:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$1, %esi
	popq	%r12
	movq	%rax, %rcx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal30StringIncludesIndexOfAssembler8GenerateENS1_13SearchVariantENS0_8compiler5TNodeINS0_7IntPtrTEEENS4_INS0_7ContextEEE
	.cfi_endproc
.LFE23769:
	.size	_ZN2v88internal31StringPrototypeIndexOfAssembler34GenerateStringPrototypeIndexOfImplEv, .-_ZN2v88internal31StringPrototypeIndexOfAssembler34GenerateStringPrototypeIndexOfImplEv
	.section	.rodata._ZN2v88internal8Builtins31Generate_StringPrototypeIndexOfEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"StringPrototypeIndexOf"
	.section	.text._ZN2v88internal8Builtins31Generate_StringPrototypeIndexOfEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins31Generate_StringPrototypeIndexOfEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins31Generate_StringPrototypeIndexOfEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins31Generate_StringPrototypeIndexOfEPNS0_8compiler18CodeAssemblerStateE:
.LFB23765:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$853, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC16(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$591, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L436
.L433:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal30StringIncludesIndexOfAssembler8GenerateENS1_13SearchVariantENS0_8compiler5TNodeINS0_7IntPtrTEEENS4_INS0_7ContextEEE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L437
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L436:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L433
.L437:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23765:
	.size	_ZN2v88internal8Builtins31Generate_StringPrototypeIndexOfEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins31Generate_StringPrototypeIndexOfEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins32Generate_StringPrototypeIncludesEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC17:
	.string	"StringPrototypeIncludes"
	.section	.text._ZN2v88internal8Builtins32Generate_StringPrototypeIncludesEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins32Generate_StringPrototypeIncludesEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins32Generate_StringPrototypeIncludesEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins32Generate_StringPrototypeIncludesEPNS0_8compiler18CodeAssemblerStateE:
.LFB23756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$844, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC17(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$590, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L442
.L439:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal30StringIncludesIndexOfAssembler8GenerateENS1_13SearchVariantENS0_8compiler5TNodeINS0_7IntPtrTEEENS4_INS0_7ContextEEE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L443
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L442:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L439
.L443:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23756:
	.size	_ZN2v88internal8Builtins32Generate_StringPrototypeIncludesEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins32Generate_StringPrototypeIncludesEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal23StringBuiltinsAssembler25MaybeCallFunctionAtSymbolEPNS0_8compiler4NodeES4_S4_NS0_6HandleINS0_6SymbolEEENS0_23PrototypeCheckAssembler24DescriptorIndexNameValueERKSt8functionIFvvEERKSA_IFvS4_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23StringBuiltinsAssembler25MaybeCallFunctionAtSymbolEPNS0_8compiler4NodeES4_S4_NS0_6HandleINS0_6SymbolEEENS0_23PrototypeCheckAssembler24DescriptorIndexNameValueERKSt8functionIFvvEERKSA_IFvS4_EE
	.type	_ZN2v88internal23StringBuiltinsAssembler25MaybeCallFunctionAtSymbolEPNS0_8compiler4NodeES4_S4_NS0_6HandleINS0_6SymbolEEENS0_23PrototypeCheckAssembler24DescriptorIndexNameValueERKSt8functionIFvvEERKSA_IFvS4_EE, @function
_ZN2v88internal23StringBuiltinsAssembler25MaybeCallFunctionAtSymbolEPNS0_8compiler4NodeES4_S4_NS0_6HandleINS0_6SymbolEEENS0_23PrototypeCheckAssembler24DescriptorIndexNameValueERKSt8functionIFvvEERKSA_IFvS4_EE:
.LFB23775:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-464(%rbp), %r14
	leaq	-336(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-208(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	xorl	%edx, %edx
	subq	$504, %rsp
	movq	32(%rbp), %r11
	movq	%r9, -528(%rbp)
	movq	%rsi, -512(%rbp)
	movq	%rdi, %rsi
	movq	%r14, %rdi
	movq	%r11, -520(%rbp)
	movq	%rcx, -504(%rbp)
	xorl	%ecx, %ecx
	movq	%r8, -536(%rbp)
	movl	$1, %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-504(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-504(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler8IsStringENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-496(%rbp), %rax
	movq	(%r12), %rsi
	movq	%rax, %rdi
	movq	%rax, -504(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	16(%rbp), %rax
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movb	$1, -480(%rbp)
	movq	%rax, -476(%rbp)
	movl	24(%rbp), %eax
	movl	%eax, -468(%rbp)
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	subq	$8, %rsp
	movq	%r15, %r9
	movq	%rbx, %rdx
	pushq	%r13
	movq	-512(%rbp), %rsi
	movq	%rax, %rcx
	movl	$1, %r8d
	pushq	-472(%rbp)
	movq	-504(%rbp), %rdi
	pushq	-480(%rbp)
	call	_ZN2v88internal23RegExpBuiltinsAssembler18BranchIfFastRegExpENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10HeapObjectEEENS3_INS0_3MapEEENS_4base5FlagsINS0_23PrototypeCheckAssembler4FlagEiEENSA_8OptionalINSC_24DescriptorIndexNameValueEEEPNS2_18CodeAssemblerLabelESJ_@PLT
	addq	$32, %rsp
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-528(%rbp), %r10
	movq	-520(%rbp), %r11
	cmpq	$0, 16(%r10)
	je	.L446
	movq	%r11, -520(%rbp)
	movq	%r10, %rdi
	call	*24(%r10)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-504(%rbp), %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17IsNullOrUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-536(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -504(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	leaq	-80(%rbp), %rcx
	xorl	%esi, %esi
	movl	$2, %ebx
	movq	%rax, %r8
	movq	%r15, %rdx
	movq	%r12, %rdi
	pushq	%rbx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-512(%rbp), %r9
	movhps	-504(%rbp), %xmm0
	pushq	%rcx
	movl	$1, %ecx
	movq	%rax, -336(%rbp)
	movq	-192(%rbp), %rax
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler6IsNullENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-520(%rbp), %r11
	popq	%rax
	movq	%rbx, -480(%rbp)
	popq	%rdx
	cmpq	$0, 16(%r11)
	je	.L446
	movq	%r11, %rdi
	leaq	-480(%rbp), %rsi
	call	*24(%r11)
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L449
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L446:
	.cfi_restore_state
	call	_ZSt25__throw_bad_function_callv@PLT
.L449:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23775:
	.size	_ZN2v88internal23StringBuiltinsAssembler25MaybeCallFunctionAtSymbolEPNS0_8compiler4NodeES4_S4_NS0_6HandleINS0_6SymbolEEENS0_23PrototypeCheckAssembler24DescriptorIndexNameValueERKSt8functionIFvvEERKSA_IFvS4_EE, .-_ZN2v88internal23StringBuiltinsAssembler25MaybeCallFunctionAtSymbolEPNS0_8compiler4NodeES4_S4_NS0_6HandleINS0_6SymbolEEENS0_23PrototypeCheckAssembler24DescriptorIndexNameValueERKSt8functionIFvvEERKSA_IFvS4_EE
	.section	.text._ZN2v88internal23StringBuiltinsAssembler17IndexOfDollarCharEPNS0_8compiler4NodeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23StringBuiltinsAssembler17IndexOfDollarCharEPNS0_8compiler4NodeES4_
	.type	_ZN2v88internal23StringBuiltinsAssembler17IndexOfDollarCharEPNS0_8compiler4NodeES4_, @function
_ZN2v88internal23StringBuiltinsAssembler17IndexOfDollarCharEPNS0_8compiler4NodeES4_:
.LFB23776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	%rdx, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$36, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$52, %edx
	leaq	-96(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-64(%rbp), %rsi
	leaq	-112(%rbp), %rdx
	movq	%rbx, %r9
	movl	$3, %edi
	movq	-128(%rbp), %xmm0
	movq	%rax, %r8
	movq	%r13, -48(%rbp)
	pushq	%rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	movq	%r12, %rdi
	pushq	%rsi
	movhps	-120(%rbp), %xmm0
	xorl	%esi, %esi
	movq	%rax, -112(%rbp)
	movq	-80(%rbp), %rax
	movaps	%xmm0, -64(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L453
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L453:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23776:
	.size	_ZN2v88internal23StringBuiltinsAssembler17IndexOfDollarCharEPNS0_8compiler4NodeES4_, .-_ZN2v88internal23StringBuiltinsAssembler17IndexOfDollarCharEPNS0_8compiler4NodeES4_
	.section	.text._ZN2v88internal23StringBuiltinsAssembler15GetSubstitutionEPNS0_8compiler4NodeES4_S4_S4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23StringBuiltinsAssembler15GetSubstitutionEPNS0_8compiler4NodeES4_S4_S4_S4_
	.type	_ZN2v88internal23StringBuiltinsAssembler15GetSubstitutionEPNS0_8compiler4NodeES4_S4_S4_S4_, @function
_ZN2v88internal23StringBuiltinsAssembler15GetSubstitutionEPNS0_8compiler4NodeES4_S4_S4_S4_:
.LFB23777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r9, %xmm2
	movq	%rcx, %xmm1
	punpcklqdq	%xmm2, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-416(%rbp), %r15
	movq	%rsi, %r14
	movq	%rdi, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-224(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$440, %rsp
	movq	%rdx, -424(%rbp)
	movl	$8, %edx
	movq	%r8, -472(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rcx, -432(%rbp)
	movq	%r9, %rcx
	movaps	%xmm1, -464(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	leaq	-352(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	movq	%r12, %rsi
	movl	$1, %r8d
	movq	%r10, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal23StringBuiltinsAssembler17IndexOfDollarCharEPNS0_8compiler4NodeES4_
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%rax, -440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-440(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-448(%rbp), %r10
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rcx
	movq	%r10, -440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-440(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	movq	%r10, -448(%rbp)
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-472(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-432(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -440(%rbp)
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -432(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$55, %edx
	leaq	-384(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-384(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-96(%rbp), %r11
	movl	$3, %edi
	xorl	%esi, %esi
	pushq	%rdi
	movq	-440(%rbp), %rcx
	movq	%rax, %r8
	movq	%r14, %r9
	pushq	%r11
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%r12, %rdi
	movq	-424(%rbp), %xmm0
	movq	%rax, -400(%rbp)
	movq	-368(%rbp), %rax
	leaq	-400(%rbp), %rdx
	movhps	-432(%rbp), %xmm0
	movq	%rcx, -80(%rbp)
	movl	$1, %ecx
	movq	%r11, -432(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$323, %esi
	movq	-432(%rbp), %r11
	movq	%rax, %xmm0
	movl	$5, %r8d
	movdqa	-464(%rbp), %xmm1
	movhps	-424(%rbp), %xmm0
	movq	%rbx, -64(%rbp)
	movq	%r11, %rcx
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L457
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L457:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23777:
	.size	_ZN2v88internal23StringBuiltinsAssembler15GetSubstitutionEPNS0_8compiler4NodeES4_S4_S4_S4_, .-_ZN2v88internal23StringBuiltinsAssembler15GetSubstitutionEPNS0_8compiler4NodeES4_S4_S4_S4_
	.section	.rodata._ZN2v88internal31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEv.str1.1,"aMS",@progbits,1
.LC18:
	.string	"String.prototype.replace"
	.section	.text._ZN2v88internal31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEv
	.type	_ZN2v88internal31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEv, @function
_ZN2v88internal31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEv:
.LFB23786:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$696, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-528(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -688(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	%rax, -648(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	leaq	8(%r12), %rdi
	leaq	.LC18(%rip), %rcx
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler22RequireObjectCoercibleENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEEPKc@PLT
	movl	$40, %edi
	movq	$0, -96(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %xmm1
	movq	%r12, %xmm0
	movq	%r13, %xmm2
	punpcklqdq	%xmm1, %xmm0
	movq	%r14, 32(%rax)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEvEUlPNS2_8compiler4NodeEE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rcx
	movl	$40, %edi
	movups	%xmm0, (%rax)
	movq	%r15, %xmm0
	leaq	-144(%rbp), %r14
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, -112(%rbp)
	movups	%xmm0, 16(%rax)
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEvEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	movq	%rcx, %xmm0
	movq	%rax, %xmm3
	movq	$0, -128(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_Znwm@PLT
	movq	%r15, %xmm4
	movq	%rbx, %xmm0
	movq	%r12, %rdi
	punpcklqdq	%xmm4, %xmm0
	movq	%r12, 32(%rax)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation(%rip), %rcx
	movups	%xmm0, (%rax)
	movq	%r13, %xmm0
	movhps	-648(%rbp), %xmm0
	movq	%rax, -144(%rbp)
	movups	%xmm0, 16(%rax)
	movq	%rcx, %xmm0
	leaq	_ZNSt17_Function_handlerIFvvEZN2v88internal31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEvEUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movl	$479, %ecx
	movq	%rax, %xmm5
	movw	%cx, -632(%rbp)
	movl	$15, -636(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -128(%rbp)
	movl	$135, -628(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-112(%rbp), %rcx
	subq	$8, %rsp
	movq	%r14, %r9
	pushq	%rcx
	movq	-636(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rcx, -664(%rbp)
	leaq	3888(%rax), %r8
	movq	%r13, %rcx
	subq	$16, %rsp
	movq	%rdx, (%rsp)
	movl	-628(%rbp), %edx
	movl	%edx, 8(%rsp)
	movq	%r15, %rdx
	call	_ZN2v88internal23StringBuiltinsAssembler25MaybeCallFunctionAtSymbolEPNS0_8compiler4NodeES4_S4_NS0_6HandleINS0_6SymbolEEENS0_23PrototypeCheckAssembler24DescriptorIndexNameValueERKSt8functionIFvvEERKSA_IFvS4_EE
	movq	-128(%rbp), %rax
	addq	$32, %rsp
	testq	%rax, %rax
	je	.L459
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L459:
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L460
	movq	-664(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L460:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	leaq	-272(%rbp), %r13
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ToString_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -656(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15ToString_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	-656(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -680(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22LoadStringLengthAsWordENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	movq	-680(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler22LoadStringLengthAsWordENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$255, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r14, -712(%rbp)
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler17IntPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-648(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler8IsStringENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-656(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler24IsConsStringInstanceTypeENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23StringBuiltinsAssembler17IndexOfDollarCharEPNS0_8compiler4NodeES4_
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler19TaggedIsPositiveSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, -96(%rbp)
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	-680(%rbp), %rcx
	movq	%r14, -648(%rbp)
	movl	$339, %esi
	movl	$3, %r8d
	movq	-664(%rbp), %r14
	movq	-656(%rbp), %rax
	movq	%rcx, -104(%rbp)
	movq	%r14, %rcx
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$52, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-272(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r14, %rsi
	movl	$3, %edi
	movq	%rbx, %r9
	pushq	%rdi
	movq	-704(%rbp), %rdx
	movq	%rax, %r8
	movl	$1, %ecx
	pushq	%rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	-656(%rbp), %xmm0
	movq	%rax, -400(%rbp)
	movq	%r14, -664(%rbp)
	movq	-256(%rbp), %rax
	leaq	-400(%rbp), %r14
	movhps	-680(%rbp), %xmm0
	movq	%rdx, -96(%rbp)
	movq	%r14, %rdx
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-672(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-696(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-648(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-648(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler13IsCallableMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-648(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ToString_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-656(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-672(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	leaq	-624(%rbp), %r15
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -696(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19EmptyStringConstantEv@PLT
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC1EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-704(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-672(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-704(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-672(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -720(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, -704(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$55, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-400(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-664(%rbp), %rsi
	movq	%rbx, %r9
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movl	$3, %edi
	movq	%rcx, -560(%rbp)
	movq	%rax, %r8
	movq	-720(%rbp), %rcx
	pushq	%rdi
	leaq	-560(%rbp), %r10
	movq	%r12, %rdi
	movq	-656(%rbp), %xmm0
	pushq	%rsi
	movq	-384(%rbp), %rax
	movq	%r10, %rdx
	xorl	%esi, %esi
	movhps	-704(%rbp), %xmm0
	movq	%rcx, -96(%rbp)
	movl	$1, %ecx
	movq	%r10, -736(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-648(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-648(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler7LoadMapENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler13IsCallableMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-592(%rbp), %r11
	movl	$2, %edx
	movq	%r11, %rdi
	movq	%rax, %rsi
	movq	%r11, -728(%rbp)
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-592(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -720(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-664(%rbp), %rsi
	movq	%rbx, %r9
	movq	-648(%rbp), %xmm0
	movl	$6, %edi
	movq	-736(%rbp), %r10
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movhps	-720(%rbp), %xmm0
	pushq	%rdi
	movq	-576(%rbp), %rax
	movq	%r12, %rdi
	movaps	%xmm0, -112(%rbp)
	movq	%r10, %rdx
	movq	-704(%rbp), %xmm0
	pushq	%rsi
	xorl	%esi, %esi
	movhps	-680(%rbp), %xmm0
	movq	%rcx, -560(%rbp)
	movl	$1, %ecx
	movaps	%xmm0, -96(%rbp)
	movq	-672(%rbp), %xmm0
	movq	%r10, -720(%rbp)
	movhps	-656(%rbp), %xmm0
	movq	%rax, -552(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler15ToString_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	movq	%rax, -704(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, -680(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-720(%rbp), %r10
	movl	$706, %edx
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-560(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-664(%rbp), %rsi
	movq	%rbx, %r9
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdi
	movq	%rdi, -608(%rbp)
	movl	$2, %edi
	movq	%rax, %r8
	movq	-680(%rbp), %xmm0
	pushq	%rdi
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	-544(%rbp), %rax
	pushq	%rsi
	movhps	-704(%rbp), %xmm0
	leaq	-608(%rbp), %rdx
	xorl	%esi, %esi
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -600(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-688(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-648(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ToString_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	-696(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-672(%rbp), %rcx
	movq	-656(%rbp), %rdx
	movq	%rax, %r9
	call	_ZN2v88internal23StringBuiltinsAssembler15GetSubstitutionEPNS0_8compiler4NodeES4_S4_S4_S4_
	movq	%r15, %rdi
	movq	%rax, -672(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, -648(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-720(%rbp), %r10
	movl	$706, %edx
	movq	%rax, %rsi
	movq	%r10, %rdi
	movq	%r10, -680(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-560(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-664(%rbp), %rsi
	movq	%rbx, %r9
	movl	$1, %ecx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdi
	movq	-728(%rbp), %r11
	movq	%rax, %r8
	movq	-648(%rbp), %xmm0
	movq	%rdi, -592(%rbp)
	movl	$2, %edi
	movq	-544(%rbp), %rax
	pushq	%rdi
	movhps	-672(%rbp), %xmm0
	movq	%r11, %rdx
	movq	%r12, %rdi
	pushq	%rsi
	xorl	%esi, %esi
	movq	%r11, -672(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-688(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-688(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-696(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -648(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-680(%rbp), %r10
	movl	$55, %edx
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-560(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-664(%rbp), %rsi
	movq	%rbx, %r9
	movl	$3, %edi
	movq	%rax, %r8
	movq	-544(%rbp), %rax
	movq	-672(%rbp), %r11
	pushq	%rdi
	movq	-656(%rbp), %xmm0
	pushq	%rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	xorl	%esi, %esi
	movq	%rax, -584(%rbp)
	movq	-712(%rbp), %rax
	movq	%r11, %rdx
	movq	%r12, %rdi
	movhps	-648(%rbp), %xmm0
	movq	%rcx, -592(%rbp)
	movl	$1, %ecx
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r15, %rdi
	movq	%rax, -656(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, -648(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-680(%rbp), %r10
	movl	$706, %edx
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-560(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-664(%rbp), %rsi
	movq	%rbx, %r9
	movl	$2, %edi
	pushq	%rdi
	movq	-672(%rbp), %r11
	movq	%rax, %r8
	movq	%r12, %rdi
	pushq	%rsi
	movq	-544(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	xorl	%esi, %esi
	movq	-648(%rbp), %xmm0
	movq	%r11, %rdx
	movq	%rcx, -592(%rbp)
	movl	$1, %ecx
	movq	%rax, -584(%rbp)
	movhps	-656(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD1Ev@PLT
	movq	-688(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L469
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L469:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23786:
	.size	_ZN2v88internal31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEv, .-_ZN2v88internal31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEv
	.section	.rodata._ZN2v88internal8Builtins31Generate_StringPrototypeReplaceEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC19:
	.string	"StringPrototypeReplace"
	.section	.text._ZN2v88internal8Builtins31Generate_StringPrototypeReplaceEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins31Generate_StringPrototypeReplaceEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins31Generate_StringPrototypeReplaceEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins31Generate_StringPrototypeReplaceEPNS0_8compiler18CodeAssemblerStateE:
.LFB23782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1042, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$596, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L474
.L471:
	movq	%r13, %rdi
	call	_ZN2v88internal31StringPrototypeReplaceAssembler34GenerateStringPrototypeReplaceImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L475
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L474:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L471
.L475:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23782:
	.size	_ZN2v88internal8Builtins31Generate_StringPrototypeReplaceEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins31Generate_StringPrototypeReplaceEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal26StringMatchSearchAssembler8GenerateENS1_7VariantEPKcNS0_8compiler5TNodeINS0_6ObjectEEES8_NS6_INS0_7ContextEEE,"axG",@progbits,_ZN2v88internal26StringMatchSearchAssembler8GenerateENS1_7VariantEPKcNS0_8compiler5TNodeINS0_6ObjectEEES8_NS6_INS0_7ContextEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal26StringMatchSearchAssembler8GenerateENS1_7VariantEPKcNS0_8compiler5TNodeINS0_6ObjectEEES8_NS6_INS0_7ContextEEE
	.type	_ZN2v88internal26StringMatchSearchAssembler8GenerateENS1_7VariantEPKcNS0_8compiler5TNodeINS0_6ObjectEEES8_NS6_INS0_7ContextEEE, @function
_ZN2v88internal26StringMatchSearchAssembler8GenerateENS1_7VariantEPKcNS0_8compiler5TNodeINS0_6ObjectEEES8_NS6_INS0_7ContextEEE:
.LFB23798:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	xorl	%edx, %edx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	movl	$1, %r8d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	xorl	%ecx, %ecx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$648, %rsp
	movl	%esi, -600(%rbp)
	movq	%rdi, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-512(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	-600(%rbp), %eax
	movq	%r12, %rdi
	testl	%eax, %eax
	jne	.L477
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$478, %esi
	movl	$132, -616(%rbp)
	addq	$3880, %rax
	movw	%si, -656(%rbp)
	movq	%rax, -624(%rbp)
	movl	$13, -648(%rbp)
	movl	$867, -640(%rbp)
.L478:
	movq	%r15, %rcx
	movq	%r13, %rdx
	leaq	8(%r12), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler22RequireObjectCoercibleENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEEPKc@PLT
	movl	$32, %edi
	leaq	-128(%rbp), %r15
	movq	$0, -80(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %xmm2
	movq	%r12, %xmm0
	movq	%r13, %xmm3
	punpcklqdq	%xmm2, %xmm0
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal26StringMatchSearchAssembler8GenerateENS3_7VariantEPKcNS2_8compiler5TNodeINS2_6ObjectEEESA_NS8_INS2_7ContextEEEEUlPNS7_4NodeEE0_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation(%rip), %rcx
	movq	%rax, -96(%rbp)
	movl	$40, %edi
	movups	%xmm0, (%rax)
	movq	%r14, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movq	$0, -112(%rbp)
	movups	%xmm0, 16(%rax)
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_26StringMatchSearchAssembler8GenerateENS6_7VariantEPKcNS2_5TNodeINS1_6ObjectEEESC_NSA_INS1_7ContextEEEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	movq	%rcx, %xmm0
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_Znwm@PLT
	movq	%r14, %xmm5
	movq	%rbx, %xmm0
	movl	-640(%rbp), %ecx
	punpcklqdq	%xmm5, %xmm0
	movq	%r12, %xmm6
	movq	%rax, -128(%rbp)
	movq	%r15, %r9
	movups	%xmm0, 8(%rax)
	movq	%r13, %xmm0
	subq	$8, %rsp
	movq	%r14, %rdx
	punpcklqdq	%xmm6, %xmm0
	movl	%ecx, (%rax)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal26StringMatchSearchAssembler8GenerateENS3_7VariantEPKcNS2_8compiler5TNodeINS2_6ObjectEEESA_NS8_INS2_7ContextEEEEUlvE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation(%rip), %rcx
	movq	%rbx, %rsi
	movups	%xmm0, 24(%rax)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v88internal26StringMatchSearchAssembler8GenerateENS3_7VariantEPKcNS2_8compiler5TNodeINS2_6ObjectEEESA_NS8_INS2_7ContextEEEEUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%rcx, %xmm0
	movq	%r12, %rdi
	movq	%rax, %xmm7
	movl	-648(%rbp), %eax
	movq	-624(%rbp), %r8
	movq	%r13, %rcx
	punpcklqdq	%xmm7, %xmm0
	movl	%eax, -588(%rbp)
	movzwl	-656(%rbp), %eax
	movaps	%xmm0, -112(%rbp)
	movw	%ax, -584(%rbp)
	leaq	-96(%rbp), %rax
	pushq	%rax
	movq	%rax, -600(%rbp)
	movq	-588(%rbp), %rax
	subq	$16, %rsp
	movq	%rax, (%rsp)
	movl	-616(%rbp), %eax
	movl	%eax, 8(%rsp)
	call	_ZN2v88internal23StringBuiltinsAssembler25MaybeCallFunctionAtSymbolEPNS0_8compiler4NodeES4_S4_NS0_6HandleINS0_6SymbolEEENS0_23PrototypeCheckAssembler24DescriptorIndexNameValueERKSt8functionIFvvEERKSA_IFvS4_EE
	movq	-112(%rbp), %rax
	addq	$32, %rsp
	testq	%rax, %rax
	je	.L479
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
.L479:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L480
	movq	-600(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L480:
	leaq	-576(%rbp), %r10
	movq	(%r12), %rsi
	movq	%r10, %rdi
	movq	%r10, -608(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ToString_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	-384(%rbp), %r13
	movq	%rax, -664(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$129, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movl	$1800, %ecx
	movl	$56, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler19EmptyStringConstantEv@PLT
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	-608(%rbp), %r10
	movq	%rax, %r8
	leaq	-256(%rbp), %r14
	movq	%r10, %rdi
	movq	%r10, -680(%rbp)
	call	_ZN2v88internal23RegExpBuiltinsAssembler12RegExpCreateENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_3MapEEENS3_INS0_6ObjectEEENS3_INS0_6StringEEE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -608(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	-648(%rbp), %eax
	subq	$8, %rsp
	movq	-680(%rbp), %r10
	pushq	%r14
	movq	-608(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r13, %r9
	movl	%eax, -540(%rbp)
	movzwl	-656(%rbp), %eax
	movq	%r10, %rdi
	movq	%rbx, %rsi
	movb	$1, -544(%rbp)
	movl	$1, %r8d
	leaq	-560(%rbp), %r15
	movw	%ax, -536(%rbp)
	movl	-616(%rbp), %eax
	movq	%r10, -656(%rbp)
	movl	%eax, -532(%rbp)
	pushq	-536(%rbp)
	pushq	-544(%rbp)
	call	_ZN2v88internal23RegExpBuiltinsAssembler18BranchIfFastRegExpENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10HeapObjectEEENS3_INS0_3MapEEENS_4base5FlagsINS0_23PrototypeCheckAssembler4FlagEiEENSA_8OptionalINSC_24DescriptorIndexNameValueEEEPNS2_18CodeAssemblerLabelESJ_@PLT
	addq	$32, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-544(%rbp), %r11
	movl	-640(%rbp), %edx
	movq	%r11, %rdi
	movq	%rax, %rsi
	movq	%r11, -648(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-544(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	movq	%rbx, %r9
	movq	%r15, %rdx
	movq	-600(%rbp), %rsi
	pushq	%rdi
	movq	%rax, %r8
	movl	$1, %ecx
	movq	-608(%rbp), %xmm1
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%r12, %rdi
	pushq	%rsi
	xorl	%esi, %esi
	movq	%rax, -560(%rbp)
	movq	-528(%rbp), %rax
	movhps	-664(%rbp), %xmm1
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm1, -640(%rbp)
	movq	%rax, -552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -616(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-648(%rbp), %r11
	movl	$710, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	movq	%r11, -624(%rbp)
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-544(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	movq	%rbx, %r9
	movq	%r15, %rdx
	movq	-600(%rbp), %rsi
	movq	%rax, %r8
	pushq	%rdi
	movl	$1, %ecx
	movq	-608(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%r12, %rdi
	pushq	%rsi
	xorl	%esi, %esi
	movq	%rax, -560(%rbp)
	movq	-528(%rbp), %rax
	movhps	-616(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, -608(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-624(%rbp), %r11
	movl	$2, %edx
	movq	%rax, %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-544(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -616(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$4, %edi
	movq	%r15, %rdx
	movq	%rbx, %r9
	movq	-600(%rbp), %rsi
	pushq	%rdi
	movq	%rax, %r8
	movl	$1, %ecx
	movq	-608(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movdqa	-640(%rbp), %xmm1
	movq	%r12, %rdi
	pushq	%rsi
	xorl	%esi, %esi
	movq	%rax, -560(%rbp)
	movq	-528(%rbp), %rax
	movhps	-616(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	movq	%rax, -552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-656(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-672(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L489
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L477:
	.cfi_restore_state
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$480, %ecx
	movl	$137, -616(%rbp)
	addq	$3896, %rax
	movw	%cx, -656(%rbp)
	movq	%rax, -624(%rbp)
	movl	$16, -648(%rbp)
	movl	$559, -640(%rbp)
	jmp	.L478
.L489:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23798:
	.size	_ZN2v88internal26StringMatchSearchAssembler8GenerateENS1_7VariantEPKcNS0_8compiler5TNodeINS0_6ObjectEEES8_NS6_INS0_7ContextEEE, .-_ZN2v88internal26StringMatchSearchAssembler8GenerateENS1_7VariantEPKcNS0_8compiler5TNodeINS0_6ObjectEEES8_NS6_INS0_7ContextEEE
	.section	.rodata._ZN2v88internal8Builtins29Generate_StringPrototypeMatchEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC20:
	.string	"StringPrototypeMatch"
.LC21:
	.string	"String.prototype.match"
	.section	.text._ZN2v88internal8Builtins29Generate_StringPrototypeMatchEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins29Generate_StringPrototypeMatchEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins29Generate_StringPrototypeMatchEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins29Generate_StringPrototypeMatchEPNS0_8compiler18CodeAssemblerStateE:
.LFB23808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$1274, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC20(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$593, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L494
.L491:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%rax, %r9
	leaq	.LC21(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal26StringMatchSearchAssembler8GenerateENS1_7VariantEPKcNS0_8compiler5TNodeINS0_6ObjectEEES8_NS6_INS0_7ContextEEE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L495
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L494:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L491
.L495:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23808:
	.size	_ZN2v88internal8Builtins29Generate_StringPrototypeMatchEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins29Generate_StringPrototypeMatchEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal29StringPrototypeMatchAssembler32GenerateStringPrototypeMatchImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29StringPrototypeMatchAssembler32GenerateStringPrototypeMatchImplEv
	.type	_ZN2v88internal29StringPrototypeMatchAssembler32GenerateStringPrototypeMatchImplEv, @function
_ZN2v88internal29StringPrototypeMatchAssembler32GenerateStringPrototypeMatchImplEv:
.LFB23812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	addq	$8, %rsp
	movq	%rbx, %r8
	movq	%r13, %rcx
	popq	%rbx
	movq	%r12, %rdi
	movq	%rax, %r9
	popq	%r12
	leaq	.LC21(%rip), %rdx
	popq	%r13
	xorl	%esi, %esi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal26StringMatchSearchAssembler8GenerateENS1_7VariantEPKcNS0_8compiler5TNodeINS0_6ObjectEEES8_NS6_INS0_7ContextEEE
	.cfi_endproc
.LFE23812:
	.size	_ZN2v88internal29StringPrototypeMatchAssembler32GenerateStringPrototypeMatchImplEv, .-_ZN2v88internal29StringPrototypeMatchAssembler32GenerateStringPrototypeMatchImplEv
	.section	.rodata._ZN2v88internal32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEv.str1.1,"aMS",@progbits,1
.LC22:
	.string	"String.prototype.matchAll"
.LC23:
	.string	"g"
	.section	.text._ZN2v88internal32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEv
	.type	_ZN2v88internal32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEv, @function
_ZN2v88internal32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEv:
.LFB23821:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	-272(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movq	-272(%rbp), %rsi
	leaq	8(%r12), %rdi
	movq	-256(%rbp), %rdx
	leaq	.LC22(%rip), %rcx
	movq	%rax, -248(%rbp)
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler22RequireObjectCoercibleENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEEPKc@PLT
	movq	-272(%rbp), %r14
	movl	$32, %edi
	movq	-264(%rbp), %rbx
	movq	-256(%rbp), %r13
	movq	$0, -80(%rbp)
	call	_Znwm@PLT
	movq	%r14, %xmm1
	movq	%r12, %xmm0
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEvEUlPNS2_8compiler4NodeEE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rcx
	punpcklqdq	%xmm1, %xmm0
	movq	%r13, %xmm2
	movq	%rax, -96(%rbp)
	movl	$40, %edi
	movups	%xmm0, (%rax)
	movq	%rbx, %xmm0
	leaq	-96(%rbp), %rbx
	leaq	-128(%rbp), %r13
	punpcklqdq	%xmm2, %xmm0
	movq	$0, -112(%rbp)
	movups	%xmm0, 16(%rax)
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEvEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	movq	%rcx, %xmm0
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_Znwm@PLT
	leaq	-256(%rbp), %rdx
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation(%rip), %rcx
	movq	%r12, %rdi
	movq	%rdx, (%rax)
	leaq	-272(%rbp), %rdx
	movq	%rcx, %xmm0
	movq	%rdx, 16(%rax)
	leaq	-248(%rbp), %rdx
	movq	%rdx, 24(%rax)
	leaq	-264(%rbp), %rdx
	movq	%rdx, 32(%rax)
	movq	%r12, 8(%rax)
	movq	%rax, -128(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v88internal32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEvEUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movl	$14, -236(%rbp)
	movq	%rax, %xmm4
	movl	$477, %eax
	punpcklqdq	%xmm4, %xmm0
	movw	%ax, -232(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	$131, -228(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	subq	$8, %rsp
	movq	%r13, %r9
	movq	%r12, %rdi
	pushq	%rbx
	movq	-236(%rbp), %rdx
	leaq	3872(%rax), %r8
	movq	-256(%rbp), %rcx
	movq	-272(%rbp), %rsi
	subq	$16, %rsp
	movq	%rdx, (%rsp)
	movl	-228(%rbp), %edx
	movl	%edx, 8(%rsp)
	movq	-264(%rbp), %rdx
	call	_ZN2v88internal23StringBuiltinsAssembler25MaybeCallFunctionAtSymbolEPNS0_8compiler4NodeES4_S4_NS0_6HandleINS0_6SymbolEEENS0_23PrototypeCheckAssembler24DescriptorIndexNameValueERKSt8functionIFvvEERKSA_IFvS4_EE
	movq	-112(%rbp), %rax
	addq	$32, %rsp
	testq	%rax, %rax
	je	.L499
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L499:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L500
	movl	$3, %edx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	*%rax
.L500:
	leaq	-224(%rbp), %r13
	movq	(%r12), %rsi
	leaq	-160(%rbp), %r14
	movq	%r13, %rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r15
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-256(%rbp), %rdx
	movq	-272(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ToString_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14StringConstantEPKc@PLT
	movq	-264(%rbp), %rcx
	movq	-248(%rbp), %rdx
	movq	%r13, %rdi
	movq	-272(%rbp), %rsi
	movq	%rax, %r8
	call	_ZN2v88internal23RegExpBuiltinsAssembler12RegExpCreateENS0_8compiler5TNodeINS0_7ContextEEES5_NS3_INS0_6ObjectEEENS3_INS0_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -280(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	leaq	-192(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	-272(%rbp), %r9
	movq	%r12, %rdi
	leaq	3872(%rax), %rsi
	movq	%r9, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -288(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-160(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	movl	$1, %ecx
	xorl	%esi, %esi
	pushq	%rdi
	movq	%rax, %r8
	movq	%r12, %rdi
	movq	-280(%rbp), %xmm0
	pushq	%rbx
	movq	-144(%rbp), %rax
	leaq	-208(%rbp), %rdx
	movq	-304(%rbp), %r9
	movhps	-288(%rbp), %xmm0
	movq	%r15, -208(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-272(%rbp), %r9
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -288(%rbp)
	movq	%r9, -312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-192(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$4, %edi
	movq	%r14, %rdx
	xorl	%esi, %esi
	pushq	%rdi
	movq	%rax, %r8
	movq	-312(%rbp), %r9
	movl	$1, %ecx
	movq	-288(%rbp), %xmm0
	movq	-176(%rbp), %rax
	pushq	%rbx
	movq	%r12, %rdi
	movq	%r15, -160(%rbp)
	movhps	-304(%rbp), %xmm0
	movq	%rax, -152(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	-280(%rbp), %xmm0
	movhps	-296(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L509
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L509:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23821:
	.size	_ZN2v88internal32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEv, .-_ZN2v88internal32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEv
	.section	.rodata._ZN2v88internal8Builtins32Generate_StringPrototypeMatchAllEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC24:
	.string	"StringPrototypeMatchAll"
	.section	.text._ZN2v88internal8Builtins32Generate_StringPrototypeMatchAllEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins32Generate_StringPrototypeMatchAllEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins32Generate_StringPrototypeMatchAllEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins32Generate_StringPrototypeMatchAllEPNS0_8compiler18CodeAssemblerStateE:
.LFB23817:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1283, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC24(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$594, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L514
.L511:
	movq	%r13, %rdi
	call	_ZN2v88internal32StringPrototypeMatchAllAssembler35GenerateStringPrototypeMatchAllImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L515
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L514:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L511
.L515:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23817:
	.size	_ZN2v88internal8Builtins32Generate_StringPrototypeMatchAllEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins32Generate_StringPrototypeMatchAllEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins30Generate_StringPrototypeSearchEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC25:
	.string	"StringPrototypeSearch"
.LC26:
	.string	"String.prototype.search"
	.section	.text._ZN2v88internal8Builtins30Generate_StringPrototypeSearchEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins30Generate_StringPrototypeSearchEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins30Generate_StringPrototypeSearchEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins30Generate_StringPrototypeSearchEPNS0_8compiler18CodeAssemblerStateE:
.LFB23831:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$1334, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC25(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$597, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L520
.L517:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rbx, %r8
	movq	%r13, %rcx
	movl	$1, %esi
	movq	%rax, %r9
	leaq	.LC26(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal26StringMatchSearchAssembler8GenerateENS1_7VariantEPKcNS0_8compiler5TNodeINS0_6ObjectEEES8_NS6_INS0_7ContextEEE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L521
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L520:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L517
.L521:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23831:
	.size	_ZN2v88internal8Builtins30Generate_StringPrototypeSearchEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins30Generate_StringPrototypeSearchEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal30StringPrototypeSearchAssembler33GenerateStringPrototypeSearchImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30StringPrototypeSearchAssembler33GenerateStringPrototypeSearchImplEv
	.type	_ZN2v88internal30StringPrototypeSearchAssembler33GenerateStringPrototypeSearchImplEv, @function
_ZN2v88internal30StringPrototypeSearchAssembler33GenerateStringPrototypeSearchImplEv:
.LFB23835:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	addq	$8, %rsp
	movq	%rbx, %r8
	movq	%r13, %rcx
	popq	%rbx
	movq	%r12, %rdi
	movq	%rax, %r9
	popq	%r12
	leaq	.LC26(%rip), %rdx
	popq	%r13
	movl	$1, %esi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal26StringMatchSearchAssembler8GenerateENS1_7VariantEPKcNS0_8compiler5TNodeINS0_6ObjectEEES8_NS6_INS0_7ContextEEE
	.cfi_endproc
.LFE23835:
	.size	_ZN2v88internal30StringPrototypeSearchAssembler33GenerateStringPrototypeSearchImplEv, .-_ZN2v88internal30StringPrototypeSearchAssembler33GenerateStringPrototypeSearchImplEv
	.section	.text._ZN2v88internal23StringBuiltinsAssembler13StringToArrayENS0_8compiler5TNodeINS0_13NativeContextEEENS3_INS0_6StringEEENS3_INS0_3SmiEEENS3_INS0_6UnionTIS8_NS0_10HeapNumberEEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23StringBuiltinsAssembler13StringToArrayENS0_8compiler5TNodeINS0_13NativeContextEEENS3_INS0_6StringEEENS3_INS0_3SmiEEENS3_INS0_6UnionTIS8_NS0_10HeapNumberEEEEE
	.type	_ZN2v88internal23StringBuiltinsAssembler13StringToArrayENS0_8compiler5TNodeINS0_13NativeContextEEENS3_INS0_6StringEEENS3_INS0_3SmiEEENS3_INS0_6UnionTIS8_NS0_10HeapNumberEEEEE, @function
_ZN2v88internal23StringBuiltinsAssembler13StringToArrayENS0_8compiler5TNodeINS0_13NativeContextEEENS3_INS0_6StringEEENS3_INS0_3SmiEEENS3_INS0_6UnionTIS8_NS0_10HeapNumberEEEEE:
.LFB23836:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-512(%rbp), %r15
	movq	%rcx, %r14
	xorl	%ecx, %ecx
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-384(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$744, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -744(%rbp)
	movq	%rdi, %rsi
	movq	%r15, %rdi
	movq	%rdx, -736(%rbp)
	xorl	%edx, %edx
	movq	%r8, -752(%rbp)
	movl	$1, %r8d
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	leaq	-256(%rbp), %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%rbx, -696(%rbp)
	xorl	%edx, %edx
	movq	%r12, %rsi
	leaq	-656(%rbp), %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$7, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	-736(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadInstanceTypeENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler27IsOneByteStringInstanceTypeENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-752(%rbp), %rax
	movq	%r12, %rdi
	movq	%r14, -640(%rbp)
	movq	%r14, -592(%rbp)
	leaq	-96(%rbp), %r14
	movq	%rax, %rsi
	movq	%rax, -608(%rbp)
	movq	%r12, -600(%rbp)
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_3SmiEZNS2_23StringBuiltinsAssembler13StringToArrayENS2_8compiler5TNodeINS2_13NativeContextEEENS8_INS2_6StringEEENS8_IS5_EENS8_INS2_6UnionTIS5_NS2_10HeapNumberEEEEEEUlvE_ZNS6_13StringToArrayESA_SC_SD_SH_EUlvE0_EENS8_IT_EENS7_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSX_St18_Manager_operation(%rip), %rcx
	leaq	-128(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rsi
	leaq	-640(%rbp), %rax
	movq	%rcx, %xmm0
	movq	%r9, %rdx
	movq	%rax, -768(%rbp)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_3SmiEZNS2_23StringBuiltinsAssembler13StringToArrayENS2_8compiler5TNodeINS2_13NativeContextEEENS8_INS2_6StringEEENS8_IS5_EENS8_INS2_6UnionTIS5_NS2_10HeapNumberEEEEEEUlvE_ZNS6_13StringToArrayESA_SC_SD_SH_EUlvE0_EENS8_IT_EENS7_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSX_St18_Manager_operation(%rip), %rcx
	movl	$6, %r8d
	movq	%rax, -96(%rbp)
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_3SmiEZNS1_23StringBuiltinsAssembler13StringToArrayENS2_5TNodeINS1_13NativeContextEEENSA_INS1_6StringEEENSA_IS8_EENSA_INS1_6UnionTIS8_NS1_10HeapNumberEEEEEEUlvE_ZNS9_13StringToArrayESC_SE_SF_SJ_EUlvE0_EENSA_IT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%rax, %xmm1
	leaq	-608(%rbp), %rax
	movq	%r9, -712(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -704(%rbp)
	movq	%rax, -128(%rbp)
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_3SmiEZNS1_23StringBuiltinsAssembler13StringToArrayENS2_5TNodeINS1_13NativeContextEEENSA_INS1_6StringEEENSA_IS8_EENSA_INS1_6UnionTIS8_NS1_10HeapNumberEEEEEEUlvE_ZNS9_13StringToArrayESC_SE_SF_SJ_EUlvE0_EENSA_IT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movaps	%xmm0, -80(%rbp)
	movq	%rax, %xmm2
	movq	%rcx, %xmm0
	movq	%r14, %rcx
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal17CodeStubAssembler10SelectImplENS0_8compiler5TNodeINS0_5BoolTEEERKSt8functionIFPNS2_4NodeEvEESC_NS0_21MachineRepresentationE@PLT
	movq	%rax, -720(%rbp)
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L525
	movq	-712(%rbp), %r9
	movl	$3, %edx
	movq	%r9, %rsi
	movq	%r9, %rdi
	call	*%rax
.L525:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L526
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L526:
	leaq	-680(%rbp), %rax
	movq	-720(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %xmm0
	leaq	-672(%rbp), %rax
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -784(%rbp)
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-736(%rbp), %rdx
	movq	(%r12), %rsi
	xorl	%ecx, %ecx
	movq	-704(%rbp), %rdi
	movq	%rax, -712(%rbp)
	call	_ZN2v88internal23ToDirectStringAssemblerC1EPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_6StringEEENS_4base5FlagsINS1_4FlagEiEE@PLT
	movq	-704(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal23ToDirectStringAssembler11TryToDirectEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%r9d, %r9d
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	-712(%rbp), %rdx
	movl	$4, %r8d
	movl	$2, %esi
	call	_ZN2v88internal17CodeStubAssembler18AllocateFixedArrayENS0_12ElementsKindEPNS0_8compiler4NodeENS1_13ParameterModeENS_4base5FlagsINS1_14AllocationFlagEiEENS3_11SloppyTNodeINS0_3MapEEE@PLT
	movq	-696(%rbp), %rdx
	movq	-704(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rax, -688(%rbp)
	call	_ZN2v88internal23ToDirectStringAssembler15TryToSequentialENS1_17StringPointerKindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rax, -680(%rbp)
	leaq	-560(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -760(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, -672(%rbp)
	call	_ZN2v88internal17CodeStubAssembler34SingleCharacterStringCacheConstantEv@PLT
	movl	$48, %edi
	movq	$0, -80(%rbp)
	movq	%rax, -664(%rbp)
	call	_Znwm@PLT
	movdqa	-784(%rbp), %xmm0
	movq	-696(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-664(%rbp), %rdx
	movq	%r12, 16(%rax)
	movq	%rdx, 24(%rax)
	leaq	-688(%rbp), %rdx
	movq	%rdx, 40(%rax)
	movq	%rsi, 32(%rax)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal23StringBuiltinsAssembler13StringToArrayENS2_8compiler5TNodeINS2_13NativeContextEEENS5_INS2_6StringEEENS5_INS2_3SmiEEENS5_INS2_6UnionTISA_NS2_10HeapNumberEEEEEEUlPNS4_4NodeEE1_E10_M_managerERSt9_Any_dataRKSK_St18_Manager_operation(%rip), %rsi
	movups	%xmm0, (%rax)
	movq	%rsi, %xmm0
	xorl	%esi, %esi
	movq	%rax, -96(%rbp)
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_23StringBuiltinsAssembler13StringToArrayENS2_5TNodeINS1_13NativeContextEEENS7_INS1_6StringEEENS7_INS1_3SmiEEENS7_INS1_6UnionTISC_NS1_10HeapNumberEEEEEEUlS4_E1_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rdi
	movq	%rax, -784(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler4zoneEv@PLT
	pushq	$1
	movl	$1, %r9d
	movq	%r14, %r8
	pushq	$1
	movq	-712(%rbp), %rcx
	movq	%r12, %rdi
	movq	-784(%rbp), %rdx
	movq	-768(%rbp), %rsi
	movq	%rax, -640(%rbp)
	movq	$0, -632(%rbp)
	movq	$0, -624(%rbp)
	movq	$0, -616(%rbp)
	call	_ZN2v88internal17CodeStubAssembler13BuildFastLoopERKNS0_10ZoneVectorIPNS0_8compiler21CodeAssemblerVariableEEEPNS3_4NodeESA_RKSt8functionIFvSA_EEiNS1_13ParameterModeENS1_16IndexAdvanceModeE@PLT
	movq	-80(%rbp), %rax
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L527
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L527:
	movq	-744(%rbp), %rdx
	movq	%r12, %rdi
	movl	$2, %esi
	movq	-736(%rbp), %xmm0
	movhps	-752(%rbp), %xmm0
	movaps	%xmm0, -736(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22LoadJSArrayElementsMapENS0_12ElementsKindENS0_8compiler11SloppyTNodeINS0_13NativeContextEEE@PLT
	movq	-720(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-688(%rbp), %rdx
	movl	$32, %r9d
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15AllocateJSArrayENS0_8compiler5TNodeINS0_3MapEEENS3_INS0_14FixedArrayBaseEEENS3_INS0_3SmiEEEPNS2_4NodeEi@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-696(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	movq	-712(%rbp), %r8
	pushq	$1
	movl	$5, %r9d
	movq	%rax, %rcx
	movl	$2, %esi
	movq	-688(%rbp), %rdx
	call	_ZN2v88internal17CodeStubAssembler23FillFixedArrayWithValueENS0_12ElementsKindEPNS0_8compiler4NodeES5_S5_NS0_9RootIndexENS1_13ParameterModeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-544(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-760(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	leaq	-576(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	leaq	-592(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-704(%rbp), %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rcx
	movq	%r12, %rdi
	movl	$342, %esi
	movdqa	-736(%rbp), %xmm0
	movq	-744(%rbp), %rdx
	movl	$2, %r8d
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-696(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L539
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L539:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23836:
	.size	_ZN2v88internal23StringBuiltinsAssembler13StringToArrayENS0_8compiler5TNodeINS0_13NativeContextEEENS3_INS0_6StringEEENS3_INS0_3SmiEEENS3_INS0_6UnionTIS8_NS0_10HeapNumberEEEEE, .-_ZN2v88internal23StringBuiltinsAssembler13StringToArrayENS0_8compiler5TNodeINS0_13NativeContextEEENS3_INS0_6StringEEENS3_INS0_3SmiEEENS3_INS0_6UnionTIS8_NS0_10HeapNumberEEEEE
	.section	.rodata._ZN2v88internal29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEv.str1.1,"aMS",@progbits,1
.LC27:
	.string	"String.prototype.split"
	.section	.text._ZN2v88internal29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEv
	.type	_ZN2v88internal29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEv, @function
_ZN2v88internal29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEv:
.LFB23851:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-456(%rbp), %r15
	leaq	-472(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-432(%rbp), %r13
	movq	%r15, %xmm3
	movq	%r14, %xmm1
	pushq	%r12
	movq	%r13, %xmm2
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-480(%rbp), %rbx
	punpcklqdq	%xmm3, %xmm2
	movq	%rbx, %xmm4
	punpcklqdq	%xmm4, %xmm1
	subq	$520, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm2, -528(%rbp)
	movaps	%xmm1, -512(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	xorl	%r9d, %r9d
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubArgumentsC1EPNS0_17CodeStubAssemblerEPNS0_8compiler4NodeES6_NS2_13ParameterModeENS1_12ReceiverModeE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movq	-432(%rbp), %rdi
	movq	%rax, -480(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubArguments24GetOptionalArgumentValueEiNS0_8compiler5TNodeINS0_6ObjectEEE@PLT
	movq	-432(%rbp), %rdi
	movq	%rax, -472(%rbp)
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubArguments24GetOptionalArgumentValueEiNS0_8compiler5TNodeINS0_6ObjectEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -464(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -456(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	-480(%rbp), %rdx
	movq	-456(%rbp), %rsi
	leaq	.LC27(%rip), %rcx
	movq	%rax, -488(%rbp)
	leaq	8(%r12), %rax
	movq	%rax, %rdi
	movq	%rax, -536(%rbp)
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler22RequireObjectCoercibleENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEEPKc@PLT
	movl	$48, %edi
	movq	$0, -80(%rbp)
	call	_Znwm@PLT
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlPNS2_8compiler4NodeEE0_E10_M_managerERSt9_Any_dataRKS9_St18_Manager_operation(%rip), %rcx
	movl	$48, %edi
	movq	$0, -112(%rbp)
	movq	%rbx, 32(%rax)
	leaq	-464(%rbp), %rbx
	movq	%rcx, %xmm0
	movq	%rbx, 40(%rax)
	movq	%r12, (%rax)
	movq	%r13, 8(%rax)
	movq	%r15, 16(%rax)
	movq	%r14, 24(%rax)
	movq	%rax, -96(%rbp)
	leaq	_ZNSt17_Function_handlerIFvPN2v88internal8compiler4NodeEEZNS1_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	movq	%rax, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_Znwm@PLT
	movdqa	-528(%rbp), %xmm2
	movdqa	-512(%rbp), %xmm1
	movq	%r12, %rdi
	movq	%rbx, 32(%rax)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE_E10_M_managerERSt9_Any_dataRKS6_St18_Manager_operation(%rip), %rbx
	movups	%xmm2, (%rax)
	movq	%rbx, %xmm0
	leaq	-96(%rbp), %rbx
	movups	%xmm1, 16(%rax)
	movq	%r12, 40(%rax)
	movq	%rax, -128(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v88internal29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movl	$17, -444(%rbp)
	movq	%rax, %xmm6
	movl	$482, %eax
	punpcklqdq	%xmm6, %xmm0
	movw	%ax, -440(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	$138, -436(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	subq	$8, %rsp
	leaq	-128(%rbp), %r9
	movq	%r12, %rdi
	pushq	%rbx
	movq	-444(%rbp), %rdx
	leaq	3912(%rax), %r8
	movq	-480(%rbp), %rcx
	movq	-456(%rbp), %rsi
	movq	%r9, -512(%rbp)
	subq	$16, %rsp
	movq	%rdx, (%rsp)
	movl	-436(%rbp), %edx
	movl	%edx, 8(%rsp)
	movq	-472(%rbp), %rdx
	call	_ZN2v88internal23StringBuiltinsAssembler25MaybeCallFunctionAtSymbolEPNS0_8compiler4NodeES4_S4_NS0_6HandleINS0_6SymbolEEENS0_23PrototypeCheckAssembler24DescriptorIndexNameValueERKSt8functionIFvvEERKSA_IFvS4_EE
	movq	-112(%rbp), %rax
	addq	$32, %rsp
	movq	-512(%rbp), %r9
	testq	%rax, %rax
	je	.L541
	movq	%r9, %rsi
	movq	%r9, %rdi
	movl	$3, %edx
	call	*%rax
	movq	-512(%rbp), %r9
.L541:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L542
	movq	%r9, -512(%rbp)
	movl	$3, %edx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	*%rax
	movq	-512(%rbp), %r9
.L542:
	movq	-480(%rbp), %rdx
	movq	-456(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r9, -528(%rbp)
	leaq	-256(%rbp), %r14
	leaq	-384(%rbp), %r15
	call	_ZN2v88internal17CodeStubAssembler15ToString_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	-464(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r12, -240(%rbp)
	movq	%rax, -512(%rbp)
	movq	-456(%rbp), %rax
	movq	%rsi, -248(%rbp)
	movq	%rax, -256(%rbp)
	movq	%r12, -384(%rbp)
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6UnionTINS2_3SmiENS2_10HeapNumberEEEZNS2_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE1_ZNS9_32GenerateStringPrototypeSplitImplEvEUlvE2_EENS2_8compiler5TNodeIT_EENSC_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE0_E10_M_managerERSt9_Any_dataRKSR_St18_Manager_operation(%rip), %rcx
	movq	%r12, %rdi
	movq	%r14, -96(%rbp)
	movq	%rax, %rsi
	movq	%rcx, %xmm0
	movq	%r15, -128(%rbp)
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6UnionTINS1_3SmiENS1_10HeapNumberEEEZNS1_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE1_ZNSC_32GenerateStringPrototypeSplitImplEvEUlvE2_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE0_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%rax, %xmm7
	movq	-528(%rbp), %r9
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeEvEZNS1_17CodeStubAssembler6SelectINS1_6UnionTINS1_3SmiENS1_10HeapNumberEEEZNS1_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE1_ZNSC_32GenerateStringPrototypeSplitImplEvEUlvE2_EENS2_5TNodeIT_EENS2_11SloppyTNodeINS1_5BoolTEEERKT0_RKT1_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal17CodeStubAssembler6SelectINS2_6UnionTINS2_3SmiENS2_10HeapNumberEEEZNS2_29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEvEUlvE1_ZNS9_32GenerateStringPrototypeSplitImplEvEUlvE2_EENS2_8compiler5TNodeIT_EENSC_11SloppyTNodeINS2_5BoolTEEERKT0_RKT1_EUlvE_E10_M_managerERSt9_Any_dataRKSR_St18_Manager_operation(%rip), %rcx
	punpcklqdq	%xmm7, %xmm0
	movq	%rax, %xmm3
	movl	$8, %r8d
	movaps	%xmm0, -80(%rbp)
	movq	%rcx, %xmm0
	movq	%r9, %rdx
	movq	%rbx, %rcx
	punpcklqdq	%xmm3, %xmm0
	movq	%r9, -496(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal17CodeStubAssembler10SelectImplENS0_8compiler5TNodeINS0_5BoolTEEERKSt8functionIFPNS2_4NodeEvEESC_NS0_21MachineRepresentationE@PLT
	movq	%rax, -528(%rbp)
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L543
	movq	-496(%rbp), %r9
	movl	$3, %edx
	movq	%r9, %rsi
	movq	%r9, %rdi
	call	*%rax
.L543:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L544
	movl	$3, %edx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	*%rax
.L544:
	movq	-472(%rbp), %rdx
	movq	-456(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ToString_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, -496(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-488(%rbp), %rdx
	movq	-528(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-472(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-456(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler22LoadJSArrayElementsMapENS0_12ElementsKindENS0_8compiler11SloppyTNodeINS0_13NativeContextEEE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -544(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	pushq	$0
	xorl	%r9d, %r9d
	movl	$2, %esi
	pushq	$1
	movq	-544(%rbp), %r8
	movq	%rax, %rcx
	movq	%r12, %rdi
	movq	-552(%rbp), %rdx
	call	_ZN2v88internal17CodeStubAssembler15AllocateJSArrayENS0_12ElementsKindENS0_8compiler5TNodeINS0_3MapEEEPNS3_4NodeENS4_INS0_3SmiEEES8_NS1_13ParameterModeENS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	-536(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, -552(%rbp)
	call	_ZN2v88internal38TorqueGeneratedExportedMacrosAssembler20LoadJSObjectElementsENS0_8compiler5TNodeINS0_8JSObjectEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -536(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	-536(%rbp), %r11
	movq	%rax, %rdx
	movq	%rax, -544(%rbp)
	movq	%r11, %rsi
	call	_ZN2v88internal17CodeStubAssembler21FixedArrayBoundsCheckENS0_8compiler5TNodeINS0_14FixedArrayBaseEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	movq	-536(%rbp), %r11
	xorl	%r9d, %r9d
	movq	%r12, %rdi
	movq	-512(%rbp), %rcx
	movq	-544(%rbp), %rdx
	movl	$4, %r8d
	movl	$1, (%rsp)
	movq	%r11, %rsi
	call	_ZN2v88internal17CodeStubAssembler37StoreFixedArrayOrPropertyArrayElementEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	movq	-552(%rbp), %r10
	movq	%r13, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-496(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler21LoadStringLengthAsSmiENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	movq	-488(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -544(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-544(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, -536(%rbp)
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-536(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-512(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler21LoadStringLengthAsSmiENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	movq	-488(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -536(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-536(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rax, -544(%rbp)
	movq	%rcx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-544(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-528(%rbp), %r8
	movq	%r12, %rdi
	movq	-536(%rbp), %rcx
	movq	-512(%rbp), %rdx
	movq	-456(%rbp), %rsi
	call	_ZN2v88internal23StringBuiltinsAssembler13StringToArrayENS0_8compiler5TNodeINS0_13NativeContextEEENS3_INS0_6StringEEENS3_INS0_3SmiEEENS3_INS0_6UnionTIS8_NS0_10HeapNumberEEEEE
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-512(%rbp), %rax
	movl	$298, %esi
	movq	-496(%rbp), %rcx
	movq	-456(%rbp), %rdx
	movl	$3, %r8d
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	-528(%rbp), %rax
	movq	%rcx, -88(%rbp)
	movq	%rbx, %rcx
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-456(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler22LoadJSArrayElementsMapENS0_12ElementsKindENS0_8compiler11SloppyTNodeINS0_13NativeContextEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	pushq	$0
	xorl	%r9d, %r9d
	movq	%r14, %rdx
	pushq	$1
	movq	-488(%rbp), %r8
	movq	%rax, %rcx
	movq	%r12, %rdi
	movl	$2, %esi
	call	_ZN2v88internal17CodeStubAssembler15AllocateJSArrayENS0_12ElementsKindENS0_8compiler5TNodeINS0_3MapEEEPNS3_4NodeENS4_INS0_3SmiEEES8_NS1_13ParameterModeENS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	addq	$32, %rsp
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L559
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L559:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23851:
	.size	_ZN2v88internal29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEv, .-_ZN2v88internal29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEv
	.section	.rodata._ZN2v88internal8Builtins29Generate_StringPrototypeSplitEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC28:
	.string	"StringPrototypeSplit"
	.section	.text._ZN2v88internal8Builtins29Generate_StringPrototypeSplitEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins29Generate_StringPrototypeSplitEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins29Generate_StringPrototypeSplitEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins29Generate_StringPrototypeSplitEPNS0_8compiler18CodeAssemblerStateE:
.LFB23847:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1416, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC28(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$598, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L564
.L561:
	movq	%r13, %rdi
	call	_ZN2v88internal29StringPrototypeSplitAssembler32GenerateStringPrototypeSplitImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L565
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L564:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L561
.L565:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23847:
	.size	_ZN2v88internal8Builtins29Generate_StringPrototypeSplitEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins29Generate_StringPrototypeSplitEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal30StringPrototypeSubstrAssembler33GenerateStringPrototypeSubstrImplEv.str1.1,"aMS",@progbits,1
.LC29:
	.string	"String.prototype.substr"
	.section	.text._ZN2v88internal30StringPrototypeSubstrAssembler33GenerateStringPrototypeSubstrImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30StringPrototypeSubstrAssembler33GenerateStringPrototypeSubstrImplEv
	.type	_ZN2v88internal30StringPrototypeSubstrAssembler33GenerateStringPrototypeSubstrImplEv, @function
_ZN2v88internal30StringPrototypeSubstrAssembler33GenerateStringPrototypeSubstrImplEv:
.LFB23864:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-752(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-704(%rbp), %rbx
	subq	$840, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubArgumentsC1EPNS0_17CodeStubAssemblerEPNS0_8compiler4NodeES6_NS2_13ParameterModeENS1_12ReceiverModeE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movq	-752(%rbp), %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubArguments24GetOptionalArgumentValueEiNS0_8compiler5TNodeINS0_6ObjectEEE@PLT
	movq	-752(%rbp), %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubArguments24GetOptionalArgumentValueEiNS0_8compiler5TNodeINS0_6ObjectEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -848(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rbx, -824(%rbp)
	movq	%r12, %rsi
	leaq	-800(%rbp), %rbx
	movq	%rax, -816(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-784(%rbp), %rcx
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -808(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	leaq	.LC29(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -840(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14StringConstantEPKc@PLT
	movq	%r15, %rdx
	movq	-816(%rbp), %r15
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubAssembler12ToThisStringENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -872(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22LoadStringLengthAsWordENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	%r15, -816(%rbp)
	leaq	-320(%rbp), %r14
	leaq	-192(%rbp), %r15
	movq	%rax, -864(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22ConvertToRelativeIndexENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_7IntPtrTEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%r12, %rsi
	leaq	-576(%rbp), %rdx
	movq	%rdx, %r9
	xorl	%edx, %edx
	movq	%r9, %rdi
	movq	%r9, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-448(%rbp), %r10
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r10, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r10, -856(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-848(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler11IsUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-864(%rbp), %r11
	movq	%r12, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal17CodeStubAssembler6SmiTagENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	-808(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-832(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-848(%rbp), %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	-816(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler16ToInteger_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_23ToIntegerTruncationModeE@PLT
	movq	-808(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	leaq	-768(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rdx, %r9
	movl	$5, %edx
	movq	%r9, %rdi
	movq	%r9, -816(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	-808(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	-856(%rbp), %r10
	movq	-832(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-832(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-808(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-840(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler9IntPtrMaxENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEES5_@PLT
	movq	%rbx, %rdi
	movq	%rax, -848(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-864(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-848(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler9IntPtrMinENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEES5_@PLT
	movq	-816(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-816(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-840(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler21IntPtrLessThanOrEqualENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-824(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19EmptyStringConstantEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	-856(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15Float64ConstantEd@PLT
	movq	-808(%rbp), %rdi
	movq	%rax, -848(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler19LoadHeapNumberValueENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	-848(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler15Float64LessThanENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19EmptyStringConstantEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-864(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-816(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-816(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-840(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler21IntPtrLessThanOrEqualENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-824(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19EmptyStringConstantEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	-816(%rbp), %r15
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-872(%rbp), %r9
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler9SubStringENS0_8compiler5TNodeINS0_6StringEEENS3_INS0_7IntPtrTEEES7_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-856(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-808(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-824(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L569
	addq	$840, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L569:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23864:
	.size	_ZN2v88internal30StringPrototypeSubstrAssembler33GenerateStringPrototypeSubstrImplEv, .-_ZN2v88internal30StringPrototypeSubstrAssembler33GenerateStringPrototypeSubstrImplEv
	.section	.rodata._ZN2v88internal8Builtins30Generate_StringPrototypeSubstrEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC31:
	.string	"StringPrototypeSubstr"
	.section	.text._ZN2v88internal8Builtins30Generate_StringPrototypeSubstrEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins30Generate_StringPrototypeSubstrEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins30Generate_StringPrototypeSubstrEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins30Generate_StringPrototypeSubstrEPNS0_8compiler18CodeAssemblerStateE:
.LFB23860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1520, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC31(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$599, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L574
.L571:
	movq	%r13, %rdi
	call	_ZN2v88internal30StringPrototypeSubstrAssembler33GenerateStringPrototypeSubstrImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L575
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L574:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L571
.L575:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23860:
	.size	_ZN2v88internal8Builtins30Generate_StringPrototypeSubstrEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins30Generate_StringPrototypeSubstrEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins24Generate_StringSubstringEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC32:
	.string	"StringSubstring"
	.section	.text._ZN2v88internal8Builtins24Generate_StringSubstringEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins24Generate_StringSubstringEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins24Generate_StringSubstringEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins24Generate_StringSubstringEPNS0_8compiler18CodeAssemblerStateE:
.LFB23872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$1617, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC32(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$55, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L580
.L577:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler9SubStringENS0_8compiler5TNodeINS0_6StringEEENS3_INS0_7IntPtrTEEES7_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L581
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L580:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L577
.L581:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23872:
	.size	_ZN2v88internal8Builtins24Generate_StringSubstringEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins24Generate_StringSubstringEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal24StringSubstringAssembler27GenerateStringSubstringImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24StringSubstringAssembler27GenerateStringSubstringImplEv
	.type	_ZN2v88internal24StringSubstringAssembler27GenerateStringSubstringImplEv, @function
_ZN2v88internal24StringSubstringAssembler27GenerateStringSubstringImplEv:
.LFB23876:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler9SubStringENS0_8compiler5TNodeINS0_6StringEEENS3_INS0_7IntPtrTEEES7_@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE23876:
	.size	_ZN2v88internal24StringSubstringAssembler27GenerateStringSubstringImplEv, .-_ZN2v88internal24StringSubstringAssembler27GenerateStringSubstringImplEv
	.section	.text._ZN2v88internal19StringTrimAssembler35GotoIfNotWhiteSpaceOrLineTerminatorENS0_8compiler5TNodeINS0_7Word32TEEEPNS2_18CodeAssemblerLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19StringTrimAssembler35GotoIfNotWhiteSpaceOrLineTerminatorENS0_8compiler5TNodeINS0_7Word32TEEEPNS2_18CodeAssemblerLabelE
	.type	_ZN2v88internal19StringTrimAssembler35GotoIfNotWhiteSpaceOrLineTerminatorENS0_8compiler5TNodeINS0_7Word32TEEEPNS2_18CodeAssemblerLabelE, @function
_ZN2v88internal19StringTrimAssembler35GotoIfNotWhiteSpaceOrLineTerminatorENS0_8compiler5TNodeINS0_7Word32TEEEPNS2_18CodeAssemblerLabelE:
.LFB23915:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	xorl	%edx, %edx
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-176(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	subq	$144, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$9, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Uint32LessThanENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$13, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler21Uint32LessThanOrEqualENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$160, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Uint32LessThanENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$160, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$5760, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$8192, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Uint32LessThanENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$8202, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler21Uint32LessThanOrEqualENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$8232, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$8233, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$8239, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$8287, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$65279, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$12288, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L587
	addq	$144, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L587:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23915:
	.size	_ZN2v88internal19StringTrimAssembler35GotoIfNotWhiteSpaceOrLineTerminatorENS0_8compiler5TNodeINS0_7Word32TEEEPNS2_18CodeAssemblerLabelE, .-_ZN2v88internal19StringTrimAssembler35GotoIfNotWhiteSpaceOrLineTerminatorENS0_8compiler5TNodeINS0_7Word32TEEEPNS2_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal19StringTrimAssembler9BuildLoopEPNS0_8compiler26TypedCodeAssemblerVariableINS0_7IntPtrTEEENS2_5TNodeIS4_EEiPNS2_18CodeAssemblerLabelESA_RKSt8functionIFPNS2_4NodeESD_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19StringTrimAssembler9BuildLoopEPNS0_8compiler26TypedCodeAssemblerVariableINS0_7IntPtrTEEENS2_5TNodeIS4_EEiPNS2_18CodeAssemblerLabelESA_RKSt8functionIFPNS2_4NodeESD_EE
	.type	_ZN2v88internal19StringTrimAssembler9BuildLoopEPNS0_8compiler26TypedCodeAssemblerVariableINS0_7IntPtrTEEENS2_5TNodeIS4_EEiPNS2_18CodeAssemblerLabelESA_RKSt8functionIFPNS2_4NodeESD_EE, @function
_ZN2v88internal19StringTrimAssembler9BuildLoopEPNS0_8compiler26TypedCodeAssemblerVariableINS0_7IntPtrTEEENS2_5TNodeIS4_EEiPNS2_18CodeAssemblerLabelESA_RKSt8functionIFPNS2_4NodeESD_EE:
.LFB23914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-200(%rbp), %r15
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	16(%rbp), %r9
	movq	%rdx, -216(%rbp)
	movl	$1, %edx
	movl	%ecx, -236(%rbp)
	movq	%r15, %rcx
	movq	%r9, -224(%rbp)
	movq	%r8, -232(%rbp)
	movl	$1, %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -200(%rbp)
	movq	%rdi, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-216(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -216(%rbp)
	movq	%r11, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11IntPtrEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	-232(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-224(%rbp), %r9
	movq	-216(%rbp), %r8
	cmpq	$0, 16(%r9)
	movq	%r8, -200(%rbp)
	je	.L592
	movq	%r9, %rdi
	movq	%r15, %rsi
	call	*24(%r9)
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal19StringTrimAssembler35GotoIfNotWhiteSpaceOrLineTerminatorENS0_8compiler5TNodeINS0_7Word32TEEEPNS2_18CodeAssemblerLabelE
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	-236(%rbp), %edx
	call	_ZN2v88internal17CodeStubAssembler9IncrementEPNS0_8compiler21CodeAssemblerVariableEiNS1_13ParameterModeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L593
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L592:
	.cfi_restore_state
	call	_ZSt25__throw_bad_function_callv@PLT
.L593:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23914:
	.size	_ZN2v88internal19StringTrimAssembler9BuildLoopEPNS0_8compiler26TypedCodeAssemblerVariableINS0_7IntPtrTEEENS2_5TNodeIS4_EEiPNS2_18CodeAssemblerLabelESA_RKSt8functionIFPNS2_4NodeESD_EE, .-_ZN2v88internal19StringTrimAssembler9BuildLoopEPNS0_8compiler26TypedCodeAssemblerVariableINS0_7IntPtrTEEENS2_5TNodeIS4_EEiPNS2_18CodeAssemblerLabelESA_RKSt8functionIFPNS2_4NodeESD_EE
	.section	.text._ZN2v88internal19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorEPNS0_8compiler4NodeES4_S4_PNS2_26TypedCodeAssemblerVariableINS0_7IntPtrTEEENS2_5TNodeIS6_EEiPNS2_18CodeAssemblerLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorEPNS0_8compiler4NodeES4_S4_PNS2_26TypedCodeAssemblerVariableINS0_7IntPtrTEEENS2_5TNodeIS6_EEiPNS2_18CodeAssemblerLabelE
	.type	_ZN2v88internal19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorEPNS0_8compiler4NodeES4_S4_PNS2_26TypedCodeAssemblerVariableINS0_7IntPtrTEEENS2_5TNodeIS6_EEiPNS2_18CodeAssemblerLabelE, @function
_ZN2v88internal19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorEPNS0_8compiler4NodeES4_S4_PNS2_26TypedCodeAssemblerVariableINS0_7IntPtrTEEENS2_5TNodeIS6_EEiPNS2_18CodeAssemblerLabelE:
.LFB23908:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	xorl	%ecx, %ecx
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-352(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-224(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 3, -56
	movq	%r9, -400(%rbp)
	movq	24(%rbp), %rbx
	movq	%rsi, -360(%rbp)
	movq	%rdi, %rsi
	movq	%r14, %rdi
	movq	%rdx, -368(%rbp)
	leaq	-360(%rbp), %rdx
	movq	%r8, -392(%rbp)
	movq	%rdx, %xmm1
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-368(%rbp), %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm1, -384(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$24, %edi
	leaq	-96(%rbp), %r15
	movq	$0, -80(%rbp)
	call	_Znwm@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	movq	%r13, %r9
	movdqa	-384(%rbp), %xmm1
	movq	%r12, 16(%rax)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorEPNS2_8compiler4NodeES6_S6_PNS4_26TypedCodeAssemblerVariableINS2_7IntPtrTEEENS4_5TNodeIS8_EEiPNS4_18CodeAssemblerLabelEEUlS6_E_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation(%rip), %rcx
	movq	%rbx, %r8
	movq	%rax, -96(%rbp)
	movq	-392(%rbp), %rsi
	movq	%rcx, %xmm0
	movups	%xmm1, (%rax)
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_EZNS1_19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorES4_S4_S4_PNS2_26TypedCodeAssemblerVariableINS1_7IntPtrTEEENS2_5TNodeIS8_EEiPNS2_18CodeAssemblerLabelEEUlS4_E_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	movl	16(%rbp), %ecx
	movq	-400(%rbp), %rdx
	pushq	%r15
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal19StringTrimAssembler9BuildLoopEPNS0_8compiler26TypedCodeAssemblerVariableINS0_7IntPtrTEEENS2_5TNodeIS4_EEiPNS2_18CodeAssemblerLabelESA_RKSt8functionIFPNS2_4NodeESD_EE
	movq	-80(%rbp), %rax
	popq	%rsi
	popq	%rdi
	testq	%rax, %rax
	je	.L595
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
.L595:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$24, %edi
	movq	$0, -80(%rbp)
	call	_Znwm@PLT
	subq	$8, %rsp
	movq	%r13, %r9
	movq	%rbx, %r8
	movdqa	-384(%rbp), %xmm4
	movq	%r12, 16(%rax)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorEPNS2_8compiler4NodeES6_S6_PNS4_26TypedCodeAssemblerVariableINS2_7IntPtrTEEENS4_5TNodeIS8_EEiPNS4_18CodeAssemblerLabelEEUlS6_E0_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation(%rip), %rcx
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	-400(%rbp), %rdx
	movq	%rcx, %xmm0
	movups	%xmm4, (%rax)
	leaq	_ZNSt17_Function_handlerIFPN2v88internal8compiler4NodeES4_EZNS1_19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorES4_S4_S4_PNS2_26TypedCodeAssemblerVariableINS1_7IntPtrTEEENS2_5TNodeIS8_EEiPNS2_18CodeAssemblerLabelEEUlS4_E0_E9_M_invokeERKSt9_Any_dataOS4_(%rip), %rax
	movl	16(%rbp), %ecx
	movq	-392(%rbp), %rsi
	pushq	%r15
	movq	%rax, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal19StringTrimAssembler9BuildLoopEPNS0_8compiler26TypedCodeAssemblerVariableINS0_7IntPtrTEEENS2_5TNodeIS4_EEiPNS2_18CodeAssemblerLabelESA_RKSt8functionIFPNS2_4NodeESD_EE
	movq	-80(%rbp), %rax
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L596
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
.L596:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L605
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L605:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23908:
	.size	_ZN2v88internal19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorEPNS0_8compiler4NodeES4_S4_PNS2_26TypedCodeAssemblerVariableINS0_7IntPtrTEEENS2_5TNodeIS6_EEiPNS2_18CodeAssemblerLabelE, .-_ZN2v88internal19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorEPNS0_8compiler4NodeES4_S4_PNS2_26TypedCodeAssemblerVariableINS0_7IntPtrTEEENS2_5TNodeIS6_EEiPNS2_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal19StringTrimAssembler8GenerateENS0_6String8TrimModeEPKcNS0_8compiler5TNodeINS0_7IntPtrTEEENS7_INS0_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19StringTrimAssembler8GenerateENS0_6String8TrimModeEPKcNS0_8compiler5TNodeINS0_7IntPtrTEEENS7_INS0_7ContextEEE
	.type	_ZN2v88internal19StringTrimAssembler8GenerateENS0_6String8TrimModeEPKcNS0_8compiler5TNodeINS0_7IntPtrTEEENS7_INS0_7ContextEEE, @function
_ZN2v88internal19StringTrimAssembler8GenerateENS0_6String8TrimModeEPKcNS0_8compiler5TNodeINS0_7IntPtrTEEENS7_INS0_7ContextEEE:
.LFB23907:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	leaq	-336(%rbp), %rax
	.cfi_offset 14, -32
	leaq	-480(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-208(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	xorl	%edx, %edx
	subq	$568, %rsp
	movl	%esi, -516(%rbp)
	movq	%rdi, %rsi
	movq	%rax, %rdi
	movq	%r8, -544(%rbp)
	movl	$1, %r8d
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -552(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r9d, %r9d
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubArgumentsC1EPNS0_17CodeStubAssemblerEPNS0_8compiler4NodeES6_NS2_13ParameterModeENS1_12ReceiverModeE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler14StringConstantEPKc@PLT
	movq	-544(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rcx
	leaq	-432(%rbp), %r15
	call	_ZN2v88internal17CodeStubAssembler12ToThisStringENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler22LoadStringLengthAsWordENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	movq	(%r12), %rsi
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movq	%rax, -560(%rbp)
	call	_ZN2v88internal23ToDirectStringAssemblerC1EPNS0_8compiler18CodeAssemblerStateENS2_5TNodeINS0_6StringEEENS_4base5FlagsINS1_4FlagEiEE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal23ToDirectStringAssembler11TryToDirectEPNS0_8compiler18CodeAssemblerLabelE@PLT
	xorl	%esi, %esi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal23ToDirectStringAssembler15TryToSequentialENS1_17StringPointerKindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	leaq	-400(%rbp), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -568(%rbp)
	movq	%rax, -592(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler27IsOneByteStringInstanceTypeENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	leaq	-384(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -576(%rbp)
	movq	%rax, -600(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	leaq	-512(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rax, %rcx
	movq	%rdx, %rdi
	movq	%rdx, -536(%rbp)
	movl	$5, %edx
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-560(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%rax, %rcx
	leaq	-496(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -528(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationEPNS1_4NodeE@PLT
	cmpl	$1, -516(%rbp)
	jbe	.L611
.L607:
	testl	$-3, -516(%rbp)
	je	.L612
.L608:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-528(%rbp), %rdi
	movq	%rax, -560(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-560(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-536(%rbp), %rdi
	movq	%rax, -560(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-560(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler9SubStringENS0_8compiler5TNodeINS0_6StringEEENS3_INS0_7IntPtrTEEES7_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	-516(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	leaq	-80(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rbx, -80(%rbp)
	movq	-544(%rbp), %rdx
	movl	$2, %r8d
	movl	$343, %esi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	-552(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler19EmptyStringConstantEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	movq	-528(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-536(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	leaq	-368(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-576(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-568(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	leaq	-416(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L613
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L612:
	.cfi_restore_state
	movq	$-1, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	pushq	-552(%rbp)
	movq	%r12, %rdi
	movq	-584(%rbp), %rdx
	movq	-528(%rbp), %r8
	pushq	$-1
	movq	%rax, %r9
	movq	-600(%rbp), %rcx
	movq	-592(%rbp), %rsi
	call	_ZN2v88internal19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorEPNS0_8compiler4NodeES4_S4_PNS2_26TypedCodeAssemblerVariableINS0_7IntPtrTEEENS2_5TNodeIS6_EEiPNS2_18CodeAssemblerLabelE
	popq	%rax
	popq	%rdx
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L611:
	pushq	-552(%rbp)
	movq	-600(%rbp), %rcx
	movq	%r12, %rdi
	movq	-592(%rbp), %rsi
	pushq	$1
	movq	-560(%rbp), %r9
	movq	-536(%rbp), %r8
	movq	-584(%rbp), %rdx
	call	_ZN2v88internal19StringTrimAssembler36ScanForNonWhiteSpaceOrLineTerminatorEPNS0_8compiler4NodeES4_S4_PNS2_26TypedCodeAssemblerVariableINS0_7IntPtrTEEENS2_5TNodeIS6_EEiPNS2_18CodeAssemblerLabelE
	popq	%rcx
	popq	%rsi
	jmp	.L607
.L613:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23907:
	.size	_ZN2v88internal19StringTrimAssembler8GenerateENS0_6String8TrimModeEPKcNS0_8compiler5TNodeINS0_7IntPtrTEEENS7_INS0_7ContextEEE, .-_ZN2v88internal19StringTrimAssembler8GenerateENS0_6String8TrimModeEPKcNS0_8compiler5TNodeINS0_7IntPtrTEEENS7_INS0_7ContextEEE
	.section	.rodata._ZN2v88internal28StringPrototypeTrimAssembler31GenerateStringPrototypeTrimImplEv.str1.1,"aMS",@progbits,1
.LC33:
	.string	"String.prototype.trim"
	.section	.text._ZN2v88internal28StringPrototypeTrimAssembler31GenerateStringPrototypeTrimImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28StringPrototypeTrimAssembler31GenerateStringPrototypeTrimImplEv
	.type	_ZN2v88internal28StringPrototypeTrimAssembler31GenerateStringPrototypeTrimImplEv, @function
_ZN2v88internal28StringPrototypeTrimAssembler31GenerateStringPrototypeTrimImplEv:
.LFB23888:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rcx
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%r12
	movq	%rax, %r8
	leaq	.LC33(%rip), %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal19StringTrimAssembler8GenerateENS0_6String8TrimModeEPKcNS0_8compiler5TNodeINS0_7IntPtrTEEENS7_INS0_7ContextEEE
	.cfi_endproc
.LFE23888:
	.size	_ZN2v88internal28StringPrototypeTrimAssembler31GenerateStringPrototypeTrimImplEv, .-_ZN2v88internal28StringPrototypeTrimAssembler31GenerateStringPrototypeTrimImplEv
	.section	.rodata._ZN2v88internal33StringPrototypeTrimStartAssembler36GenerateStringPrototypeTrimStartImplEv.str1.1,"aMS",@progbits,1
.LC34:
	.string	"String.prototype.trimLeft"
	.section	.text._ZN2v88internal33StringPrototypeTrimStartAssembler36GenerateStringPrototypeTrimStartImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33StringPrototypeTrimStartAssembler36GenerateStringPrototypeTrimStartImplEv
	.type	_ZN2v88internal33StringPrototypeTrimStartAssembler36GenerateStringPrototypeTrimStartImplEv, @function
_ZN2v88internal33StringPrototypeTrimStartAssembler36GenerateStringPrototypeTrimStartImplEv:
.LFB23897:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rcx
	movq	%r12, %rdi
	movl	$1, %esi
	popq	%r12
	movq	%rax, %r8
	leaq	.LC34(%rip), %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal19StringTrimAssembler8GenerateENS0_6String8TrimModeEPKcNS0_8compiler5TNodeINS0_7IntPtrTEEENS7_INS0_7ContextEEE
	.cfi_endproc
.LFE23897:
	.size	_ZN2v88internal33StringPrototypeTrimStartAssembler36GenerateStringPrototypeTrimStartImplEv, .-_ZN2v88internal33StringPrototypeTrimStartAssembler36GenerateStringPrototypeTrimStartImplEv
	.section	.rodata._ZN2v88internal31StringPrototypeTrimEndAssembler34GenerateStringPrototypeTrimEndImplEv.str1.1,"aMS",@progbits,1
.LC35:
	.string	"String.prototype.trimRight"
	.section	.text._ZN2v88internal31StringPrototypeTrimEndAssembler34GenerateStringPrototypeTrimEndImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31StringPrototypeTrimEndAssembler34GenerateStringPrototypeTrimEndImplEv
	.type	_ZN2v88internal31StringPrototypeTrimEndAssembler34GenerateStringPrototypeTrimEndImplEv, @function
_ZN2v88internal31StringPrototypeTrimEndAssembler34GenerateStringPrototypeTrimEndImplEv:
.LFB23906:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rcx
	movq	%r12, %rdi
	movl	$2, %esi
	popq	%r12
	movq	%rax, %r8
	leaq	.LC35(%rip), %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal19StringTrimAssembler8GenerateENS0_6String8TrimModeEPKcNS0_8compiler5TNodeINS0_7IntPtrTEEENS7_INS0_7ContextEEE
	.cfi_endproc
.LFE23906:
	.size	_ZN2v88internal31StringPrototypeTrimEndAssembler34GenerateStringPrototypeTrimEndImplEv, .-_ZN2v88internal31StringPrototypeTrimEndAssembler34GenerateStringPrototypeTrimEndImplEv
	.section	.rodata._ZN2v88internal8Builtins28Generate_StringPrototypeTrimEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC36:
	.string	"StringPrototypeTrim"
	.section	.text._ZN2v88internal8Builtins28Generate_StringPrototypeTrimEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins28Generate_StringPrototypeTrimEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins28Generate_StringPrototypeTrimEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins28Generate_StringPrototypeTrimEPNS0_8compiler18CodeAssemblerStateE:
.LFB23884:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$1626, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC36(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$600, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L624
.L621:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %r8
	leaq	.LC33(%rip), %rdx
	call	_ZN2v88internal19StringTrimAssembler8GenerateENS0_6String8TrimModeEPKcNS0_8compiler5TNodeINS0_7IntPtrTEEENS7_INS0_7ContextEEE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L625
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L624:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L621
.L625:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23884:
	.size	_ZN2v88internal8Builtins28Generate_StringPrototypeTrimEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins28Generate_StringPrototypeTrimEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins33Generate_StringPrototypeTrimStartEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC37:
	.string	"StringPrototypeTrimStart"
	.section	.text._ZN2v88internal8Builtins33Generate_StringPrototypeTrimStartEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins33Generate_StringPrototypeTrimStartEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins33Generate_StringPrototypeTrimStartEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins33Generate_StringPrototypeTrimStartEPNS0_8compiler18CodeAssemblerStateE:
.LFB23893:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$1635, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC37(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$602, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L630
.L627:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rcx
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r8
	leaq	.LC34(%rip), %rdx
	call	_ZN2v88internal19StringTrimAssembler8GenerateENS0_6String8TrimModeEPKcNS0_8compiler5TNodeINS0_7IntPtrTEEENS7_INS0_7ContextEEE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L631
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L630:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L627
.L631:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23893:
	.size	_ZN2v88internal8Builtins33Generate_StringPrototypeTrimStartEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins33Generate_StringPrototypeTrimStartEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins31Generate_StringPrototypeTrimEndEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC38:
	.string	"StringPrototypeTrimEnd"
	.section	.text._ZN2v88internal8Builtins31Generate_StringPrototypeTrimEndEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins31Generate_StringPrototypeTrimEndEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins31Generate_StringPrototypeTrimEndEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins31Generate_StringPrototypeTrimEndEPNS0_8compiler18CodeAssemblerStateE:
.LFB23902:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$1644, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC38(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$601, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L636
.L633:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rcx
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, %r8
	leaq	.LC35(%rip), %rdx
	call	_ZN2v88internal19StringTrimAssembler8GenerateENS0_6String8TrimModeEPKcNS0_8compiler5TNodeINS0_7IntPtrTEEENS7_INS0_7ContextEEE
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L637
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L636:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L633
.L637:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23902:
	.size	_ZN2v88internal8Builtins31Generate_StringPrototypeTrimEndEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins31Generate_StringPrototypeTrimEndEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal23StringBuiltinsAssembler19LoadSurrogatePairAtENS0_8compiler11SloppyTNodeINS0_6StringEEENS3_INS0_7IntPtrTEEES7_NS0_15UnicodeEncodingE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23StringBuiltinsAssembler19LoadSurrogatePairAtENS0_8compiler11SloppyTNodeINS0_6StringEEENS3_INS0_7IntPtrTEEES7_NS0_15UnicodeEncodingE
	.type	_ZN2v88internal23StringBuiltinsAssembler19LoadSurrogatePairAtENS0_8compiler11SloppyTNodeINS0_6StringEEENS3_INS0_7IntPtrTEEES7_NS0_15UnicodeEncodingE, @function
_ZN2v88internal23StringBuiltinsAssembler19LoadSurrogatePairAtENS0_8compiler11SloppyTNodeINS0_6StringEEENS3_INS0_7IntPtrTEEES7_NS0_15UnicodeEncodingE:
.LFB23916:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-352(%rbp), %r14
	leaq	-336(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-320(%rbp), %rbx
	subq	$360, %rsp
	movq	%rsi, -360(%rbp)
	movq	%rdi, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -400(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -368(%rbp)
	xorl	%ecx, %ecx
	movl	%r8d, -388(%rbp)
	movl	$1, %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movq	-368(%rbp), %r11
	movq	-360(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r11, %rdx
	movq	%r11, -384(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16StringCharCodeAtENS0_8compiler11SloppyTNodeINS0_6StringEEENS3_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movl	$55296, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$64512, %esi
	movq	%r12, %rdi
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r14, %rdi
	movq	%rax, -368(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-368(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-376(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-384(%rbp), %r11
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-400(%rbp), %r10
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -368(%rbp)
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-368(%rbp), %r8
	movq	-360(%rbp), %rsi
	movq	%r12, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler16StringCharCodeAtENS0_8compiler11SloppyTNodeINS0_6StringEEENS3_INS0_7IntPtrTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movl	$56320, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$64512, %esi
	movq	%r12, %rdi
	movq	%rax, -368(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r15, %rdi
	movq	%rax, -360(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-360(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-368(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r15, %rdi
	movq	%rax, -360(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movl	-388(%rbp), %r9d
	testb	%r9b, %r9b
	je	.L639
	cmpb	$1, %r9b
	jne	.L641
	movl	$-56613888, %esi
	movq	%r12, %rdi
	movq	%rax, -368(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-368(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler8Int32AddENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movl	$10, %esi
	movq	%r12, %rdi
	movq	%rax, -368(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-360(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32ShlENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-368(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler8Int32AddENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
.L641:
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L644
	addq	$360, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L639:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$16, %esi
	movq	%rax, -368(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-368(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32ShlENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	-360(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler8Word32OrENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	jmp	.L641
.L644:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23916:
	.size	_ZN2v88internal23StringBuiltinsAssembler19LoadSurrogatePairAtENS0_8compiler11SloppyTNodeINS0_6StringEEENS3_INS0_7IntPtrTEEES7_NS0_15UnicodeEncodingE, .-_ZN2v88internal23StringBuiltinsAssembler19LoadSurrogatePairAtENS0_8compiler11SloppyTNodeINS0_6StringEEENS3_INS0_7IntPtrTEEES7_NS0_15UnicodeEncodingE
	.section	.text._ZN2v88internal30StringFromCodePointAtAssembler33GenerateStringFromCodePointAtImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30StringFromCodePointAtAssembler33GenerateStringFromCodePointAtImplEv
	.type	_ZN2v88internal30StringFromCodePointAtAssembler33GenerateStringFromCodePointAtImplEv, @function
_ZN2v88internal30StringFromCodePointAtAssembler33GenerateStringFromCodePointAtImplEv:
.LFB23696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler22LoadStringLengthAsWordENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%r8d, %r8d
	call	_ZN2v88internal23StringBuiltinsAssembler19LoadSurrogatePairAtENS0_8compiler11SloppyTNodeINS0_6StringEEENS3_INS0_7IntPtrTEEES7_NS0_15UnicodeEncodingE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler37StringFromSingleUTF16EncodedCodePointENS0_8compiler5TNodeINS0_6Int32TEEE@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE23696:
	.size	_ZN2v88internal30StringFromCodePointAtAssembler33GenerateStringFromCodePointAtImplEv, .-_ZN2v88internal30StringFromCodePointAtAssembler33GenerateStringFromCodePointAtImplEv
	.section	.rodata._ZN2v88internal8Builtins30Generate_StringFromCodePointAtEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC39:
	.string	"StringFromCodePointAt"
	.section	.text._ZN2v88internal8Builtins30Generate_StringFromCodePointAtEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins30Generate_StringFromCodePointAtEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins30Generate_StringFromCodePointAtEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins30Generate_StringFromCodePointAtEPNS0_8compiler18CodeAssemblerStateE:
.LFB23692:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$535, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC39(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$48, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L651
.L648:
	movq	%r13, %rdi
	call	_ZN2v88internal30StringFromCodePointAtAssembler33GenerateStringFromCodePointAtImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L652
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L651:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L648
.L652:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23692:
	.size	_ZN2v88internal8Builtins30Generate_StringFromCodePointAtEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins30Generate_StringFromCodePointAtEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal26StringCodePointAtAssembler29GenerateStringCodePointAtImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26StringCodePointAtAssembler29GenerateStringCodePointAtImplEv
	.type	_ZN2v88internal26StringCodePointAtAssembler29GenerateStringCodePointAtImplEv, @function
_ZN2v88internal26StringCodePointAtAssembler29GenerateStringCodePointAtImplEv:
.LFB23687:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler22LoadStringLengthAsWordENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$1, %r8d
	call	_ZN2v88internal23StringBuiltinsAssembler19LoadSurrogatePairAtENS0_8compiler11SloppyTNodeINS0_6StringEEENS3_INS0_7IntPtrTEEES7_NS0_15UnicodeEncodingE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12SmiFromInt32ENS0_8compiler11SloppyTNodeINS0_6Int32TEEE@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE23687:
	.size	_ZN2v88internal26StringCodePointAtAssembler29GenerateStringCodePointAtImplEv, .-_ZN2v88internal26StringCodePointAtAssembler29GenerateStringCodePointAtImplEv
	.section	.rodata._ZN2v88internal8Builtins26Generate_StringCodePointAtEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC40:
	.string	"StringCodePointAt"
	.section	.text._ZN2v88internal8Builtins26Generate_StringCodePointAtEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins26Generate_StringCodePointAtEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins26Generate_StringCodePointAtEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins26Generate_StringCodePointAtEPNS0_8compiler18CodeAssemblerStateE:
.LFB23683:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$520, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC40(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$47, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L659
.L656:
	movq	%r13, %rdi
	call	_ZN2v88internal26StringCodePointAtAssembler29GenerateStringCodePointAtImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L660
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L659:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L656
.L660:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23683:
	.size	_ZN2v88internal8Builtins26Generate_StringCodePointAtEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins26Generate_StringCodePointAtEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal23StringBuiltinsAssembler44BranchIfStringPrimitiveWithNoCustomIterationENS0_8compiler5TNodeINS0_6ObjectEEENS3_INS0_7ContextEEEPNS2_18CodeAssemblerLabelES9_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23StringBuiltinsAssembler44BranchIfStringPrimitiveWithNoCustomIterationENS0_8compiler5TNodeINS0_6ObjectEEENS3_INS0_7ContextEEEPNS2_18CodeAssemblerLabelES9_
	.type	_ZN2v88internal23StringBuiltinsAssembler44BranchIfStringPrimitiveWithNoCustomIterationENS0_8compiler5TNodeINS0_6ObjectEEENS3_INS0_7ContextEEEPNS2_18CodeAssemblerLabelES9_, @function
_ZN2v88internal23StringBuiltinsAssembler44BranchIfStringPrimitiveWithNoCustomIterationENS0_8compiler5TNodeINS0_6ObjectEEENS3_INS0_7ContextEEEPNS2_18CodeAssemblerLabelES9_:
.LFB23917:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler8IsStringENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9GotoIfNotENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler31StringIteratorProtectorConstantEv@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$1800, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$24, %edx
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	addq	$8, %rsp
	movq	%r13, %rcx
	movq	%r15, %rdx
	popq	%rbx
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	.cfi_endproc
.LFE23917:
	.size	_ZN2v88internal23StringBuiltinsAssembler44BranchIfStringPrimitiveWithNoCustomIterationENS0_8compiler5TNodeINS0_6ObjectEEENS3_INS0_7ContextEEEPNS2_18CodeAssemblerLabelES9_, .-_ZN2v88internal23StringBuiltinsAssembler44BranchIfStringPrimitiveWithNoCustomIterationENS0_8compiler5TNodeINS0_6ObjectEEENS3_INS0_7ContextEEEPNS2_18CodeAssemblerLabelES9_
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal23StringBuiltinsAssembler16DirectStringDataEPNS0_8compiler4NodeES4_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal23StringBuiltinsAssembler16DirectStringDataEPNS0_8compiler4NodeES4_, @function
_GLOBAL__sub_I__ZN2v88internal23StringBuiltinsAssembler16DirectStringDataEPNS0_8compiler4NodeES4_:
.LFB31847:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE31847:
	.size	_GLOBAL__sub_I__ZN2v88internal23StringBuiltinsAssembler16DirectStringDataEPNS0_8compiler4NodeES4_, .-_GLOBAL__sub_I__ZN2v88internal23StringBuiltinsAssembler16DirectStringDataEPNS0_8compiler4NodeES4_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal23StringBuiltinsAssembler16DirectStringDataEPNS0_8compiler4NodeES4_
	.section	.rodata.CSWTCH.340,"a"
	.align 32
	.type	CSWTCH.340, @object
	.size	CSWTCH.340, 56
CSWTCH.340:
	.long	0
	.long	0
	.long	1
	.long	2
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	2
	.long	3
	.long	4
	.weak	_ZN2v88internal13MachineTypeOfINS0_6Uint8TEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_6Uint8TEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_6Uint8TEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_6Uint8TEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_6Uint8TEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_6Uint8TEvE5valueE:
	.byte	2
	.byte	3
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4292870144
	.long	1106247679
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	12
	.long	8
	.long	4
	.long	0
	.align 16
.LC13:
	.quad	7526754601088593200
	.quad	6874854174092522272
	.align 16
.LC14:
	.quad	7959358215658692931
	.quad	7164757654822021492
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
