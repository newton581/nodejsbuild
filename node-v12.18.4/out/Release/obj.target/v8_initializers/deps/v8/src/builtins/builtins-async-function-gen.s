	.file	"builtins-async-function-gen.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZN2v88internal30AsyncFunctionBuiltinsAssembler31AsyncFunctionAwaitResumeClosureEPNS0_8compiler4NodeES4_NS0_17JSGeneratorObject10ResumeModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30AsyncFunctionBuiltinsAssembler31AsyncFunctionAwaitResumeClosureEPNS0_8compiler4NodeES4_NS0_17JSGeneratorObject10ResumeModeE
	.type	_ZN2v88internal30AsyncFunctionBuiltinsAssembler31AsyncFunctionAwaitResumeClosureEPNS0_8compiler4NodeES4_NS0_17JSGeneratorObject10ResumeModeE, @function
_ZN2v88internal30AsyncFunctionBuiltinsAssembler31AsyncFunctionAwaitResumeClosureEPNS0_8compiler4NodeES4_NS0_17JSGeneratorObject10ResumeModeE:
.LFB21899:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-336(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-208(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$376, %rsp
	movq	%rdx, -392(%rbp)
	movl	$2, %edx
	movl	%ecx, -404(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13IsDebugActiveEv@PLT
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$80, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_9JSPromiseEvE5valueE(%rip), %ecx
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	leaq	-80(%rbp), %r9
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%r9, %rcx
	movl	$67, %esi
	movq	%rax, -80(%rbp)
	movl	$1, %r8d
	movq	%r9, -400(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	-404(%rbp), %r10d
	movq	%r12, %rdi
	movl	%r10d, %esi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$56, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$6, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	leaq	-368(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory15ResumeGeneratorEPNS0_7IsolateE@PLT
	movq	-368(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-400(%rbp), %r9
	xorl	%esi, %esi
	movl	$2, %edi
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%r14, %xmm1
	pushq	%r9
	movl	$1, %ecx
	movq	%rbx, %r9
	movq	%r12, %rdi
	movq	-392(%rbp), %xmm0
	movq	%rax, -384(%rbp)
	leaq	-384(%rbp), %rdx
	movq	-352(%rbp), %rax
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L10
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L10:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21899:
	.size	_ZN2v88internal30AsyncFunctionBuiltinsAssembler31AsyncFunctionAwaitResumeClosureEPNS0_8compiler4NodeES4_NS0_17JSGeneratorObject10ResumeModeE, .-_ZN2v88internal30AsyncFunctionBuiltinsAssembler31AsyncFunctionAwaitResumeClosureEPNS0_8compiler4NodeES4_NS0_17JSGeneratorObject10ResumeModeE
	.section	.text._ZN2v88internal27AsyncFunctionEnterAssembler30GenerateAsyncFunctionEnterImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27AsyncFunctionEnterAssembler30GenerateAsyncFunctionEnterImplEv
	.type	_ZN2v88internal27AsyncFunctionEnterAssembler30GenerateAsyncFunctionEnterImplEv, @function
_ZN2v88internal27AsyncFunctionEnterAssembler30GenerateAsyncFunctionEnterImplEv:
.LFB21917:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	-328(%rbp), %r10
	movl	$24, %edx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE(%rip), %ecx
	movq	%rax, %r15
	movq	%r10, %rsi
	movq	%r10, -344(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$771, %ecx
	movl	$42, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssembler35LoadSharedFunctionInfoBytecodeArrayENS0_8compiler11SloppyTNodeINS0_18SharedFunctionInfoEEE@PLT
	movl	$516, %ecx
	movl	$40, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler7WordSarENS1_11SloppyTNodeINS0_5WordTEEENS3_INS0_9IntegralTEEE@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	xorl	%r9d, %r9d
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$4, %r8d
	movq	%rax, %rdx
	movl	$3, %esi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssembler18AllocateFixedArrayENS0_12ElementsKindEPNS0_8compiler4NodeENS1_13ParameterModeENS_4base5FlagsINS1_14AllocationFlagEiEENS3_11SloppyTNodeINS0_3MapEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	subq	$8, %rsp
	movq	%rbx, %r8
	movq	%r14, %rdx
	pushq	$1
	movl	$4, %r9d
	movq	%rax, %rcx
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler23FillFixedArrayWithValueENS0_12ElementsKindEPNS0_8compiler4NodeES5_S5_NS0_9RootIndexENS1_13ParameterModeE@PLT
	movl	$136, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler18AllocateInNewSpaceENS0_8compiler5TNodeINS0_7IntPtrTEEENS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssembler17LoadNativeContextENS0_8compiler11SloppyTNodeINS0_7ContextEEE@PLT
	movl	$212, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -336(%rbp)
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE(%rip), %ecx
	movl	$56, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movl	$88, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal17CodeStubAssembler13InnerAllocateENS0_8compiler5TNodeINS0_10HeapObjectEEEi@PLT
	movq	-328(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rbx
	movq	%rax, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeES4_@PLT
	movl	$29, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$8, %edx
	call	_ZN2v88internal17CodeStubAssembler20StoreObjectFieldRootEPNS0_8compiler4NodeEiNS0_9RootIndexE@PLT
	movl	$29, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$16, %edx
	call	_ZN2v88internal17CodeStubAssembler20StoreObjectFieldRootEPNS0_8compiler4NodeEiNS0_9RootIndexE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24PromiseBuiltinsAssembler11PromiseInitEPNS0_8compiler4NodeE@PLT
	movq	-336(%rbp), %r8
	movl	$17, %edx
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler22StoreMapNoWriteBarrierEPNS0_8compiler4NodeES4_@PLT
	movl	$29, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$8, %edx
	call	_ZN2v88internal17CodeStubAssembler20StoreObjectFieldRootEPNS0_8compiler4NodeEiNS0_9RootIndexE@PLT
	movl	$29, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$16, %edx
	call	_ZN2v88internal17CodeStubAssembler20StoreObjectFieldRootEPNS0_8compiler4NodeEiNS0_9RootIndexE@PLT
	movl	$24, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-344(%rbp), %r10
	movl	$7, %r8d
	movq	%r10, %rcx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r15, %rcx
	movl	$32, %edx
	movq	%r13, %rsi
	movl	$7, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movl	$40, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-352(%rbp), %r11
	movl	$8, %r8d
	movq	%r11, %rcx
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$48, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$6, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$56, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$6, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movl	$-2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movl	$64, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$6, %r8d
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%r14, %rcx
	movl	$72, %edx
	movq	%r13, %rsi
	movl	$7, %r8d
	movq	%r12, %rdi
	leaq	-192(%rbp), %r14
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	movq	%rbx, %rcx
	movl	$80, %edx
	movq	%r13, %rsi
	movl	$7, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler30StoreObjectFieldNoWriteBarrierEPNS0_8compiler4NodeEiS4_NS0_21MachineRepresentationE@PLT
	leaq	-320(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r9, %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	%r9, -328(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler58IsPromiseHookEnabledOrDebugIsActiveOrHasAsyncEventDelegateEv@PLT
	movq	-328(%rbp), %r9
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	-328(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdx
	leaq	-64(%rbp), %rcx
	movq	%r12, %rdi
	movl	$1, %r8d
	movl	$65, %esi
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-328(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L14
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21917:
	.size	_ZN2v88internal27AsyncFunctionEnterAssembler30GenerateAsyncFunctionEnterImplEv, .-_ZN2v88internal27AsyncFunctionEnterAssembler30GenerateAsyncFunctionEnterImplEv
	.section	.rodata._ZN2v88internal8Builtins27Generate_AsyncFunctionEnterEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/v8/src/builtins/builtins-async-function-gen.cc"
	.section	.rodata._ZN2v88internal8Builtins27Generate_AsyncFunctionEnterEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"AsyncFunctionEnter"
	.section	.text._ZN2v88internal8Builtins27Generate_AsyncFunctionEnterEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins27Generate_AsyncFunctionEnterEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins27Generate_AsyncFunctionEnterEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins27Generate_AsyncFunctionEnterEPNS0_8compiler18CodeAssemblerStateE:
.LFB21913:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$79, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$226, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L19
.L16:
	movq	%r13, %rdi
	call	_ZN2v88internal27AsyncFunctionEnterAssembler30GenerateAsyncFunctionEnterImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L20
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L16
.L20:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21913:
	.size	_ZN2v88internal8Builtins27Generate_AsyncFunctionEnterEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins27Generate_AsyncFunctionEnterEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal28AsyncFunctionRejectAssembler31GenerateAsyncFunctionRejectImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28AsyncFunctionRejectAssembler31GenerateAsyncFunctionRejectImplEv
	.type	_ZN2v88internal28AsyncFunctionRejectAssembler31GenerateAsyncFunctionRejectImplEv, @function
_ZN2v88internal28AsyncFunctionRejectAssembler31GenerateAsyncFunctionRejectImplEv:
.LFB21926:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$208, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movl	$80, %edx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_9JSPromiseEvE5valueE(%rip), %ecx
	movq	%rax, %r14
	leaq	-192(%rbp), %r13
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal17CodeStubAssembler13FalseConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$494, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -48(%rbp)
	movl	$3, %edi
	xorl	%esi, %esi
	leaq	-64(%rbp), %rbx
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rbx
	movq	%r14, %r9
	leaq	-208(%rbp), %rdx
	movl	$1, %ecx
	movq	-216(%rbp), %xmm0
	movq	%rax, -208(%rbp)
	movq	%r12, %rdi
	movq	-176(%rbp), %rax
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler21HasAsyncEventDelegateEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13IsDebugActiveEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-216(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%rbx, %r8
	movq	%r14, %rcx
	movl	$68, %esi
	movq	-224(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$2, %r9d
	movq	%r12, %rdi
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L24
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L24:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21926:
	.size	_ZN2v88internal28AsyncFunctionRejectAssembler31GenerateAsyncFunctionRejectImplEv, .-_ZN2v88internal28AsyncFunctionRejectAssembler31GenerateAsyncFunctionRejectImplEv
	.section	.rodata._ZN2v88internal8Builtins28Generate_AsyncFunctionRejectEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"AsyncFunctionReject"
	.section	.text._ZN2v88internal8Builtins28Generate_AsyncFunctionRejectEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins28Generate_AsyncFunctionRejectEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins28Generate_AsyncFunctionRejectEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins28Generate_AsyncFunctionRejectEPNS0_8compiler18CodeAssemblerStateE:
.LFB21922:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$177, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$227, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L29
.L26:
	movq	%r13, %rdi
	call	_ZN2v88internal28AsyncFunctionRejectAssembler31GenerateAsyncFunctionRejectImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L30
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L26
.L30:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21922:
	.size	_ZN2v88internal8Builtins28Generate_AsyncFunctionRejectEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins28Generate_AsyncFunctionRejectEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal29AsyncFunctionResolveAssembler32GenerateAsyncFunctionResolveImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29AsyncFunctionResolveAssembler32GenerateAsyncFunctionResolveImplEv
	.type	_ZN2v88internal29AsyncFunctionResolveAssembler32GenerateAsyncFunctionResolveImplEv, @function
_ZN2v88internal29AsyncFunctionResolveAssembler32GenerateAsyncFunctionResolveImplEv:
.LFB21935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	-64(%rbp), %rbx
	subq	$208, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rsi
	movl	$80, %edx
	movq	%r12, %rdi
	movzwl	_ZN2v88internal13MachineTypeOfINS0_9JSPromiseEvE5valueE(%rip), %ecx
	movq	%rax, %r14
	leaq	-192(%rbp), %r13
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, -216(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$495, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	movq	%r14, %r9
	xorl	%esi, %esi
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	pushq	%rbx
	leaq	-208(%rbp), %rdx
	movq	%r12, %rdi
	movq	-216(%rbp), %xmm0
	movq	%rax, -208(%rbp)
	movq	-176(%rbp), %rax
	movhps	-232(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler21HasAsyncEventDelegateEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler13IsDebugActiveEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-216(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%rbx, %r8
	movq	%r14, %rcx
	movl	$68, %esi
	movq	-224(%rbp), %xmm0
	movq	%rax, %rdx
	movl	$2, %r9d
	movq	%r12, %rdi
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19TailCallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6Int32TEEENS5_INS0_6ObjectEEESt16initializer_listIS9_E@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L34
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L34:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21935:
	.size	_ZN2v88internal29AsyncFunctionResolveAssembler32GenerateAsyncFunctionResolveImplEv, .-_ZN2v88internal29AsyncFunctionResolveAssembler32GenerateAsyncFunctionResolveImplEv
	.section	.rodata._ZN2v88internal8Builtins29Generate_AsyncFunctionResolveEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"AsyncFunctionResolve"
	.section	.text._ZN2v88internal8Builtins29Generate_AsyncFunctionResolveEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins29Generate_AsyncFunctionResolveEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins29Generate_AsyncFunctionResolveEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins29Generate_AsyncFunctionResolveEPNS0_8compiler18CodeAssemblerStateE:
.LFB21931:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$202, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$228, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L39
.L36:
	movq	%r13, %rdi
	call	_ZN2v88internal29AsyncFunctionResolveAssembler32GenerateAsyncFunctionResolveImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L40
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L36
.L40:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21931:
	.size	_ZN2v88internal8Builtins29Generate_AsyncFunctionResolveEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins29Generate_AsyncFunctionResolveEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal8Builtins43Generate_AsyncFunctionLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"AsyncFunctionLazyDeoptContinuation"
	.section	.text._ZN2v88internal8Builtins43Generate_AsyncFunctionLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins43Generate_AsyncFunctionLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins43Generate_AsyncFunctionLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins43Generate_AsyncFunctionLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE:
.LFB21940:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$226, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$229, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L45
.L42:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L46
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L42
.L46:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21940:
	.size	_ZN2v88internal8Builtins43Generate_AsyncFunctionLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins43Generate_AsyncFunctionLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal43AsyncFunctionLazyDeoptContinuationAssembler46GenerateAsyncFunctionLazyDeoptContinuationImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal43AsyncFunctionLazyDeoptContinuationAssembler46GenerateAsyncFunctionLazyDeoptContinuationImplEv
	.type	_ZN2v88internal43AsyncFunctionLazyDeoptContinuationAssembler46GenerateAsyncFunctionLazyDeoptContinuationImplEv, @function
_ZN2v88internal43AsyncFunctionLazyDeoptContinuationAssembler46GenerateAsyncFunctionLazyDeoptContinuationImplEv:
.LFB21944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE21944:
	.size	_ZN2v88internal43AsyncFunctionLazyDeoptContinuationAssembler46GenerateAsyncFunctionLazyDeoptContinuationImplEv, .-_ZN2v88internal43AsyncFunctionLazyDeoptContinuationAssembler46GenerateAsyncFunctionLazyDeoptContinuationImplEv
	.section	.rodata._ZN2v88internal8Builtins40Generate_AsyncFunctionAwaitRejectClosureEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"AsyncFunctionAwaitRejectClosure"
	.section	.text._ZN2v88internal8Builtins40Generate_AsyncFunctionAwaitRejectClosureEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins40Generate_AsyncFunctionAwaitRejectClosureEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins40Generate_AsyncFunctionAwaitRejectClosureEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins40Generate_AsyncFunctionAwaitRejectClosureEPNS0_8compiler18CodeAssemblerStateE:
.LFB21949:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$231, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$232, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L53
.L50:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal30AsyncFunctionBuiltinsAssembler31AsyncFunctionAwaitResumeClosureEPNS0_8compiler4NodeES4_NS0_17JSGeneratorObject10ResumeModeE
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L54
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L50
.L54:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21949:
	.size	_ZN2v88internal8Builtins40Generate_AsyncFunctionAwaitRejectClosureEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins40Generate_AsyncFunctionAwaitRejectClosureEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal40AsyncFunctionAwaitRejectClosureAssembler43GenerateAsyncFunctionAwaitRejectClosureImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal40AsyncFunctionAwaitRejectClosureAssembler43GenerateAsyncFunctionAwaitRejectClosureImplEv
	.type	_ZN2v88internal40AsyncFunctionAwaitRejectClosureAssembler43GenerateAsyncFunctionAwaitRejectClosureImplEv, @function
_ZN2v88internal40AsyncFunctionAwaitRejectClosureAssembler43GenerateAsyncFunctionAwaitRejectClosureImplEv:
.LFB21953:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$2, %ecx
	movq	%rax, %rsi
	call	_ZN2v88internal30AsyncFunctionBuiltinsAssembler31AsyncFunctionAwaitResumeClosureEPNS0_8compiler4NodeES4_NS0_17JSGeneratorObject10ResumeModeE
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE21953:
	.size	_ZN2v88internal40AsyncFunctionAwaitRejectClosureAssembler43GenerateAsyncFunctionAwaitRejectClosureImplEv, .-_ZN2v88internal40AsyncFunctionAwaitRejectClosureAssembler43GenerateAsyncFunctionAwaitRejectClosureImplEv
	.section	.rodata._ZN2v88internal8Builtins41Generate_AsyncFunctionAwaitResolveClosureEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"AsyncFunctionAwaitResolveClosure"
	.section	.text._ZN2v88internal8Builtins41Generate_AsyncFunctionAwaitResolveClosureEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins41Generate_AsyncFunctionAwaitResolveClosureEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins41Generate_AsyncFunctionAwaitResolveClosureEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins41Generate_AsyncFunctionAwaitResolveClosureEPNS0_8compiler18CodeAssemblerStateE:
.LFB21958:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	$241, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$233, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L61
.L58:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal30AsyncFunctionBuiltinsAssembler31AsyncFunctionAwaitResumeClosureEPNS0_8compiler4NodeES4_NS0_17JSGeneratorObject10ResumeModeE
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L62
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L58
.L62:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21958:
	.size	_ZN2v88internal8Builtins41Generate_AsyncFunctionAwaitResolveClosureEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins41Generate_AsyncFunctionAwaitResolveClosureEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal41AsyncFunctionAwaitResolveClosureAssembler44GenerateAsyncFunctionAwaitResolveClosureImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal41AsyncFunctionAwaitResolveClosureAssembler44GenerateAsyncFunctionAwaitResolveClosureImplEv
	.type	_ZN2v88internal41AsyncFunctionAwaitResolveClosureAssembler44GenerateAsyncFunctionAwaitResolveClosureImplEv, @function
_ZN2v88internal41AsyncFunctionAwaitResolveClosureAssembler44GenerateAsyncFunctionAwaitResolveClosureImplEv:
.LFB21962:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movq	%rax, %rsi
	call	_ZN2v88internal30AsyncFunctionBuiltinsAssembler31AsyncFunctionAwaitResumeClosureEPNS0_8compiler4NodeES4_NS0_17JSGeneratorObject10ResumeModeE
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler17UndefinedConstantEv@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	movq	%rax, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE21962:
	.size	_ZN2v88internal41AsyncFunctionAwaitResolveClosureAssembler44GenerateAsyncFunctionAwaitResolveClosureImplEv, .-_ZN2v88internal41AsyncFunctionAwaitResolveClosureAssembler44GenerateAsyncFunctionAwaitResolveClosureImplEv
	.section	.text._ZN2v88internal30AsyncFunctionBuiltinsAssembler18AsyncFunctionAwaitINS0_34AsyncFunctionAwaitCaughtDescriptorEEEvb,"axG",@progbits,_ZN2v88internal30AsyncFunctionBuiltinsAssembler18AsyncFunctionAwaitINS0_34AsyncFunctionAwaitCaughtDescriptorEEEvb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal30AsyncFunctionBuiltinsAssembler18AsyncFunctionAwaitINS0_34AsyncFunctionAwaitCaughtDescriptorEEEvb
	.type	_ZN2v88internal30AsyncFunctionBuiltinsAssembler18AsyncFunctionAwaitINS0_34AsyncFunctionAwaitCaughtDescriptorEEEvb, @function
_ZN2v88internal30AsyncFunctionBuiltinsAssembler18AsyncFunctionAwaitINS0_34AsyncFunctionAwaitCaughtDescriptorEEEvb:
.LFB24494:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 3, -56
	movl	%esi, -328(%rbp)
	xorl	%esi, %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	-336(%rbp), %r10
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$80, %edx
	movq	%rax, %r15
	movq	%r10, %rsi
	movq	%r10, -344(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler21HasAsyncEventDelegateEv@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movzbl	-328(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15BooleanConstantEb@PLT
	movl	$14, %esi
	movq	%r12, %rdi
	movq	%rax, -336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$15, %esi
	movq	%r12, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-336(%rbp), %rcx
	movq	%rbx, %r8
	movq	-328(%rbp), %rdx
	movq	-352(%rbp), %r11
	movq	%rax, %r9
	movq	%r15, %rsi
	movq	%r12, %rdi
	pushq	%rcx
	movq	-344(%rbp), %r10
	pushq	%rdx
	movq	%r11, %rcx
	movq	%r10, %rdx
	call	_ZN2v88internal22AsyncBuiltinsAssembler5AwaitEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdx
	leaq	-64(%rbp), %rcx
	movq	%r12, %rdi
	movl	$1, %r8d
	movl	$66, %esi
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L68
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L68:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24494:
	.size	_ZN2v88internal30AsyncFunctionBuiltinsAssembler18AsyncFunctionAwaitINS0_34AsyncFunctionAwaitCaughtDescriptorEEEvb, .-_ZN2v88internal30AsyncFunctionBuiltinsAssembler18AsyncFunctionAwaitINS0_34AsyncFunctionAwaitCaughtDescriptorEEEvb
	.section	.text._ZN2v88internal33AsyncFunctionAwaitCaughtAssembler36GenerateAsyncFunctionAwaitCaughtImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33AsyncFunctionAwaitCaughtAssembler36GenerateAsyncFunctionAwaitCaughtImplEv
	.type	_ZN2v88internal33AsyncFunctionAwaitCaughtAssembler36GenerateAsyncFunctionAwaitCaughtImplEv, @function
_ZN2v88internal33AsyncFunctionAwaitCaughtAssembler36GenerateAsyncFunctionAwaitCaughtImplEv:
.LFB21972:
	.cfi_startproc
	endbr64
	movl	$1, %esi
	jmp	_ZN2v88internal30AsyncFunctionBuiltinsAssembler18AsyncFunctionAwaitINS0_34AsyncFunctionAwaitCaughtDescriptorEEEvb
	.cfi_endproc
.LFE21972:
	.size	_ZN2v88internal33AsyncFunctionAwaitCaughtAssembler36GenerateAsyncFunctionAwaitCaughtImplEv, .-_ZN2v88internal33AsyncFunctionAwaitCaughtAssembler36GenerateAsyncFunctionAwaitCaughtImplEv
	.section	.rodata._ZN2v88internal8Builtins33Generate_AsyncFunctionAwaitCaughtEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"AsyncFunctionAwaitCaught"
	.section	.text._ZN2v88internal8Builtins33Generate_AsyncFunctionAwaitCaughtEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins33Generate_AsyncFunctionAwaitCaughtEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins33Generate_AsyncFunctionAwaitCaughtEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins33Generate_AsyncFunctionAwaitCaughtEPNS0_8compiler18CodeAssemblerStateE:
.LFB21968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$290, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$230, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L74
.L71:
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal30AsyncFunctionBuiltinsAssembler18AsyncFunctionAwaitINS0_34AsyncFunctionAwaitCaughtDescriptorEEEvb
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L75
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L71
.L75:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21968:
	.size	_ZN2v88internal8Builtins33Generate_AsyncFunctionAwaitCaughtEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins33Generate_AsyncFunctionAwaitCaughtEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal30AsyncFunctionBuiltinsAssembler18AsyncFunctionAwaitINS0_36AsyncFunctionAwaitUncaughtDescriptorEEEvb,"axG",@progbits,_ZN2v88internal30AsyncFunctionBuiltinsAssembler18AsyncFunctionAwaitINS0_36AsyncFunctionAwaitUncaughtDescriptorEEEvb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal30AsyncFunctionBuiltinsAssembler18AsyncFunctionAwaitINS0_36AsyncFunctionAwaitUncaughtDescriptorEEEvb
	.type	_ZN2v88internal30AsyncFunctionBuiltinsAssembler18AsyncFunctionAwaitINS0_36AsyncFunctionAwaitUncaughtDescriptorEEEvb, @function
_ZN2v88internal30AsyncFunctionBuiltinsAssembler18AsyncFunctionAwaitINS0_36AsyncFunctionAwaitUncaughtDescriptorEEEvb:
.LFB24495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 3, -56
	movl	%esi, -328(%rbp)
	xorl	%esi, %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rax, -336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	-336(%rbp), %r10
	movl	$1800, %ecx
	movq	%r12, %rdi
	movl	$80, %edx
	movq	%rax, %r15
	movq	%r10, %rsi
	movq	%r10, -344(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15LoadObjectFieldENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEEiNS0_11MachineTypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler21HasAsyncEventDelegateEv@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movzbl	-328(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15BooleanConstantEb@PLT
	movl	$14, %esi
	movq	%r12, %rdi
	movq	%rax, -336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$15, %esi
	movq	%r12, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-336(%rbp), %rcx
	movq	%rbx, %r8
	movq	-328(%rbp), %rdx
	movq	-352(%rbp), %r11
	movq	%rax, %r9
	movq	%r15, %rsi
	movq	%r12, %rdi
	pushq	%rcx
	movq	-344(%rbp), %r10
	pushq	%rdx
	movq	%r11, %rcx
	movq	%r10, %rdx
	call	_ZN2v88internal22AsyncBuiltinsAssembler5AwaitEPNS0_8compiler4NodeES4_S4_S4_S4_S4_S4_@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler4BindEPNS0_8compiler18CodeAssemblerLabelE@PLT
	movq	%r15, %rdx
	leaq	-64(%rbp), %rcx
	movq	%r12, %rdi
	movl	$1, %r8d
	movl	$66, %esi
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	popq	%rax
	popq	%rdx
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L79
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L79:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24495:
	.size	_ZN2v88internal30AsyncFunctionBuiltinsAssembler18AsyncFunctionAwaitINS0_36AsyncFunctionAwaitUncaughtDescriptorEEEvb, .-_ZN2v88internal30AsyncFunctionBuiltinsAssembler18AsyncFunctionAwaitINS0_36AsyncFunctionAwaitUncaughtDescriptorEEEvb
	.section	.text._ZN2v88internal35AsyncFunctionAwaitUncaughtAssembler38GenerateAsyncFunctionAwaitUncaughtImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal35AsyncFunctionAwaitUncaughtAssembler38GenerateAsyncFunctionAwaitUncaughtImplEv
	.type	_ZN2v88internal35AsyncFunctionAwaitUncaughtAssembler38GenerateAsyncFunctionAwaitUncaughtImplEv, @function
_ZN2v88internal35AsyncFunctionAwaitUncaughtAssembler38GenerateAsyncFunctionAwaitUncaughtImplEv:
.LFB21981:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	_ZN2v88internal30AsyncFunctionBuiltinsAssembler18AsyncFunctionAwaitINS0_36AsyncFunctionAwaitUncaughtDescriptorEEEvb
	.cfi_endproc
.LFE21981:
	.size	_ZN2v88internal35AsyncFunctionAwaitUncaughtAssembler38GenerateAsyncFunctionAwaitUncaughtImplEv, .-_ZN2v88internal35AsyncFunctionAwaitUncaughtAssembler38GenerateAsyncFunctionAwaitUncaughtImplEv
	.section	.rodata._ZN2v88internal8Builtins35Generate_AsyncFunctionAwaitUncaughtEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"AsyncFunctionAwaitUncaught"
	.section	.text._ZN2v88internal8Builtins35Generate_AsyncFunctionAwaitUncaughtEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins35Generate_AsyncFunctionAwaitUncaughtEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins35Generate_AsyncFunctionAwaitUncaughtEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins35Generate_AsyncFunctionAwaitUncaughtEPNS0_8compiler18CodeAssemblerStateE:
.LFB21977:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$297, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$231, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L85
.L82:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal30AsyncFunctionBuiltinsAssembler18AsyncFunctionAwaitINS0_36AsyncFunctionAwaitUncaughtDescriptorEEEvb
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L86
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L82
.L86:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21977:
	.size	_ZN2v88internal8Builtins35Generate_AsyncFunctionAwaitUncaughtEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins35Generate_AsyncFunctionAwaitUncaughtEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal30AsyncFunctionBuiltinsAssembler31AsyncFunctionAwaitResumeClosureEPNS0_8compiler4NodeES4_NS0_17JSGeneratorObject10ResumeModeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal30AsyncFunctionBuiltinsAssembler31AsyncFunctionAwaitResumeClosureEPNS0_8compiler4NodeES4_NS0_17JSGeneratorObject10ResumeModeE, @function
_GLOBAL__sub_I__ZN2v88internal30AsyncFunctionBuiltinsAssembler31AsyncFunctionAwaitResumeClosureEPNS0_8compiler4NodeES4_NS0_17JSGeneratorObject10ResumeModeE:
.LFB28173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28173:
	.size	_GLOBAL__sub_I__ZN2v88internal30AsyncFunctionBuiltinsAssembler31AsyncFunctionAwaitResumeClosureEPNS0_8compiler4NodeES4_NS0_17JSGeneratorObject10ResumeModeE, .-_GLOBAL__sub_I__ZN2v88internal30AsyncFunctionBuiltinsAssembler31AsyncFunctionAwaitResumeClosureEPNS0_8compiler4NodeES4_NS0_17JSGeneratorObject10ResumeModeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal30AsyncFunctionBuiltinsAssembler31AsyncFunctionAwaitResumeClosureEPNS0_8compiler4NodeES4_NS0_17JSGeneratorObject10ResumeModeE
	.weak	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZN2v88internal13MachineTypeOfINS0_9JSPromiseEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_9JSPromiseEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_9JSPromiseEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_9JSPromiseEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_9JSPromiseEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_9JSPromiseEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
