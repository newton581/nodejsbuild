	.file	"array-tq-csa.cc"
	.text
	.section	.rodata._ZN2v88internal30EnsureWriteableFastElements_50EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../../deps/v8/../../deps/v8/src/builtins/array.tq"
	.section	.text._ZN2v88internal30EnsureWriteableFastElements_50EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30EnsureWriteableFastElements_50EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEE
	.type	_ZN2v88internal30EnsureWriteableFastElements_50EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEE, @function
_ZN2v88internal30EnsureWriteableFastElements_50EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEE:
.LFB22410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-1824(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1952(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1992(%rbp), %r12
	pushq	%rbx
	subq	$2088, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -2096(%rbp)
	movq	%rdx, -2112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1992(%rbp)
	movq	%rdi, -1824(%rbp)
	movl	$48, %edi
	movq	$0, -1816(%rbp)
	movq	$0, -1808(%rbp)
	movq	$0, -1800(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1816(%rbp)
	leaq	-1768(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1800(%rbp)
	movq	%rdx, -1808(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1784(%rbp)
	movq	%rax, -2008(%rbp)
	movq	$0, -1792(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1992(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1624(%rbp)
	movq	$0, -1616(%rbp)
	movq	%rax, -1632(%rbp)
	movq	$0, -1608(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1624(%rbp)
	leaq	-1576(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1608(%rbp)
	movq	%rdx, -1616(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1592(%rbp)
	movq	%rax, -2048(%rbp)
	movq	$0, -1600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1992(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1432(%rbp)
	movq	$0, -1424(%rbp)
	movq	%rax, -1440(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1432(%rbp)
	leaq	-1384(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -2040(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1992(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -2072(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1992(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -2032(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1992(%rbp), %rax
	movl	$72, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -2064(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1992(%rbp), %rax
	movl	$96, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -2056(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1992(%rbp), %rax
	movl	$48, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -2024(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1992(%rbp), %rax
	movl	$48, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -280(%rbp)
	leaq	-232(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -2016(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-2096(%rbp), %xmm1
	movaps	%xmm0, -1952(%rbp)
	movhps	-2112(%rbp), %xmm1
	movq	$0, -1936(%rbp)
	movaps	%xmm1, -2096(%rbp)
	call	_Znwm@PLT
	movdqa	-2096(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1952(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2
	call	_ZdlPv@PLT
.L2:
	movq	-2008(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1760(%rbp)
	jne	.L208
	cmpq	$0, -1568(%rbp)
	jne	.L209
.L7:
	cmpq	$0, -1376(%rbp)
	jne	.L210
.L10:
	cmpq	$0, -1184(%rbp)
	jne	.L211
.L15:
	cmpq	$0, -992(%rbp)
	jne	.L212
.L18:
	cmpq	$0, -800(%rbp)
	jne	.L213
.L21:
	cmpq	$0, -608(%rbp)
	jne	.L214
.L23:
	cmpq	$0, -416(%rbp)
	leaq	-288(%rbp), %r14
	jne	.L215
.L26:
	movq	-2016(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L29
	call	_ZdlPv@PLT
.L29:
	movq	-2016(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L30
	call	_ZdlPv@PLT
.L30:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L31
	.p2align 4,,10
	.p2align 3
.L35:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L32
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L35
.L33:
	movq	-280(%rbp), %r13
.L31:
	testq	%r13, %r13
	je	.L36
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L36:
	movq	-2024(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L37
	call	_ZdlPv@PLT
.L37:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L38
	.p2align 4,,10
	.p2align 3
.L42:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L39
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L42
.L40:
	movq	-472(%rbp), %r13
.L38:
	testq	%r13, %r13
	je	.L43
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L43:
	movq	-2056(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L44
	call	_ZdlPv@PLT
.L44:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L45
	.p2align 4,,10
	.p2align 3
.L49:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L46
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L49
.L47:
	movq	-664(%rbp), %r13
.L45:
	testq	%r13, %r13
	je	.L50
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L50:
	movq	-2064(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L51
	call	_ZdlPv@PLT
.L51:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L52
	.p2align 4,,10
	.p2align 3
.L56:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L53
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L56
.L54:
	movq	-856(%rbp), %r13
.L52:
	testq	%r13, %r13
	je	.L57
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L57:
	movq	-2032(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L58
	call	_ZdlPv@PLT
.L58:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L59
	.p2align 4,,10
	.p2align 3
.L63:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L60
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L63
.L61:
	movq	-1048(%rbp), %r13
.L59:
	testq	%r13, %r13
	je	.L64
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L64:
	movq	-2072(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L65
	call	_ZdlPv@PLT
.L65:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L66
	.p2align 4,,10
	.p2align 3
.L70:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L67
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L70
.L68:
	movq	-1240(%rbp), %r13
.L66:
	testq	%r13, %r13
	je	.L71
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L71:
	movq	-2040(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L72
	call	_ZdlPv@PLT
.L72:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L73
	.p2align 4,,10
	.p2align 3
.L77:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L74
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L77
.L75:
	movq	-1432(%rbp), %r13
.L73:
	testq	%r13, %r13
	je	.L78
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L78:
	movq	-2048(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L79
	call	_ZdlPv@PLT
.L79:
	movq	-1616(%rbp), %rbx
	movq	-1624(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L80
	.p2align 4,,10
	.p2align 3
.L84:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L81
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L84
.L82:
	movq	-1624(%rbp), %r13
.L80:
	testq	%r13, %r13
	je	.L85
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L85:
	movq	-2008(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L86
	call	_ZdlPv@PLT
.L86:
	movq	-1808(%rbp), %rbx
	movq	-1816(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L87
	.p2align 4,,10
	.p2align 3
.L91:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L88
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L91
.L89:
	movq	-1816(%rbp), %r13
.L87:
	testq	%r13, %r13
	je	.L92
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L92:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L216
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L91
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L81:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L84
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L74:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L77
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L67:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L70
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L60:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L63
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L53:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L56
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L46:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L49
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L32:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L35
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L39:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L42
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L208:
	movq	-2008(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r10d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r10w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L4
	call	_ZdlPv@PLT
.L4:
	movq	(%rbx), %rax
	movl	$22, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	%rcx, -2096(%rbp)
	movq	%rbx, -2112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE(%rip), %esi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$23, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE(%rip), %esi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal11kCOWMap_210EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -2080(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2080(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	-2096(%rbp), %xmm2
	movaps	%xmm0, -1952(%rbp)
	movhps	-2112(%rbp), %xmm2
	movq	%rbx, -80(%rbp)
	movaps	%xmm2, -2096(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	$0, -1936(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-1632(%rbp), %rdi
	movq	%rax, -1952(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5
	call	_ZdlPv@PLT
.L5:
	movdqa	-2096(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	%rbx, -80(%rbp)
	movaps	%xmm0, -1952(%rbp)
	movaps	%xmm6, -96(%rbp)
	movq	$0, -1936(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-1440(%rbp), %rdi
	movq	%rax, -1952(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6
	call	_ZdlPv@PLT
.L6:
	movq	-2040(%rbp), %rcx
	movq	-2048(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1568(%rbp)
	je	.L7
.L209:
	movq	-2048(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1632(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r9d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L8
	call	_ZdlPv@PLT
.L8:
	movq	(%rbx), %rax
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movdqu	(%rax), %xmm0
	movaps	%xmm1, -1952(%rbp)
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -2096(%rbp)
	call	_Znwm@PLT
	movdqa	-2096(%rbp), %xmm0
	leaq	-480(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1952(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L9
	call	_ZdlPv@PLT
.L9:
	movq	-2024(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1376(%rbp)
	je	.L10
.L210:
	movq	-2040(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1440(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r8d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L11
	call	_ZdlPv@PLT
.L11:
	movq	(%rbx), %rax
	movl	$29, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %r15
	movq	16(%rax), %rax
	movq	%rcx, -2096(%rbp)
	movq	%rax, -2112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE(%rip), %esi
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal13Cast5ATSmi_83EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %xmm6
	movq	%rbx, %xmm5
	movq	-2112(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	%rax, -64(%rbp)
	movq	-2096(%rbp), %xmm4
	punpcklqdq	%xmm5, %xmm3
	movaps	%xmm0, -1984(%rbp)
	leaq	-1984(%rbp), %r15
	punpcklqdq	%xmm6, %xmm4
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm3, -2112(%rbp)
	movaps	%xmm4, -2096(%rbp)
	movaps	%xmm4, -96(%rbp)
	movq	$0, -1968(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm7
	movdqa	-80(%rbp), %xmm5
	leaq	40(%rax), %rdx
	leaq	-1056(%rbp), %rdi
	movq	%rax, -1984(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1968(%rbp)
	movq	%rdx, -1976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L12
	call	_ZdlPv@PLT
.L12:
	movq	-2032(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1944(%rbp)
	jne	.L217
.L13:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1184(%rbp)
	je	.L15
.L211:
	movq	-2072(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1248(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$134678279, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L16
	call	_ZdlPv@PLT
.L16:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm6
	movaps	%xmm0, -1952(%rbp)
	movq	$0, -1936(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm6, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -1952(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L17
	call	_ZdlPv@PLT
.L17:
	movq	-2064(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -992(%rbp)
	je	.L18
.L212:
	movq	-2032(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$134678279, (%rax)
	leaq	5(%rax), %rdx
	movb	$6, 4(%rax)
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L19
	call	_ZdlPv@PLT
.L19:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	8(%rax), %rsi
	movq	16(%rax), %rcx
	movq	32(%rax), %rdx
	movq	(%rax), %rax
	movaps	%xmm0, -1952(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -1936(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -1952(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L20
	call	_ZdlPv@PLT
.L20:
	movq	-2056(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L21
.L213:
	movq	-2064(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movl	$1799, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rdi
	movb	$7, 2(%rax)
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L22
	call	_ZdlPv@PLT
.L22:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -608(%rbp)
	je	.L23
.L214:
	movq	-2056(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$101123847, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L24
	call	_ZdlPv@PLT
.L24:
	movq	(%rbx), %rax
	movl	$30, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rcx, -2112(%rbp)
	movq	8(%rax), %rcx
	movq	%rcx, -2096(%rbp)
	movq	24(%rax), %rcx
	movq	%rcx, -2120(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$31, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -2080(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	-2120(%rbp), %rcx
	pushq	$0
	movl	$1, %r9d
	pushq	$0
	movq	-2080(%rbp), %rdx
	pushq	$0
	movq	%rcx, %r8
	call	_ZN2v88internal17CodeStubAssembler17ExtractFixedArrayEPNS0_8compiler4NodeES4_S4_S4_NS_4base5FlagsINS1_21ExtractFixedArrayFlagEiEENS1_13ParameterModeEPNS2_26TypedCodeAssemblerVariableINS0_5BoolTEEES4_@PLT
	addq	$32, %rsp
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$30, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%rbx, %r8
	movl	$7, %esi
	movq	%r13, %rdi
	movq	-2096(%rbp), %rdx
	movq	%rax, %rcx
	movl	$2, %r9d
	call	_ZN2v88internal8compiler13CodeAssembler13StoreToObjectENS0_21MachineRepresentationENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEEPNS1_4NodeENS0_25StoreToObjectWriteBarrierE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$19, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movq	-2112(%rbp), %xmm0
	movaps	%xmm1, -1952(%rbp)
	movhps	-2096(%rbp), %xmm0
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -2096(%rbp)
	call	_Znwm@PLT
	movdqa	-2096(%rbp), %xmm0
	leaq	-480(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1952(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L25
	call	_ZdlPv@PLT
.L25:
	movq	-2024(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L215:
	movq	-2024(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movq	%r14, %rdi
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L27
	call	_ZdlPv@PLT
.L27:
	movq	(%rbx), %rax
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	leaq	-288(%rbp), %r14
	movdqu	(%rax), %xmm0
	movaps	%xmm1, -1952(%rbp)
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -2096(%rbp)
	call	_Znwm@PLT
	movdqa	-2096(%rbp), %xmm0
	movq	%r14, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1952(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L28
	call	_ZdlPv@PLT
.L28:
	movq	-2016(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L217:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-2096(%rbp), %xmm7
	movaps	%xmm0, -1984(%rbp)
	movaps	%xmm7, -96(%rbp)
	movdqa	-2112(%rbp), %xmm7
	movq	$0, -1968(%rbp)
	movaps	%xmm7, -80(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm7
	leaq	-1248(%rbp), %rdi
	movq	%r15, %rsi
	leaq	32(%rax), %rdx
	movq	%rax, -1984(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-80(%rbp), %xmm7
	movq	%rdx, -1968(%rbp)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L14
	call	_ZdlPv@PLT
.L14:
	movq	-2072(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L13
.L216:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22410:
	.size	_ZN2v88internal30EnsureWriteableFastElements_50EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEE, .-_ZN2v88internal30EnsureWriteableFastElements_50EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEE
	.section	.text._ZN2v88internal25LoadElementOrUndefined_52EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_16FixedDoubleArrayEEENS4_INS0_3SmiEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25LoadElementOrUndefined_52EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_16FixedDoubleArrayEEENS4_INS0_3SmiEEE
	.type	_ZN2v88internal25LoadElementOrUndefined_52EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_16FixedDoubleArrayEEENS4_INS0_3SmiEEE, @function
_ZN2v88internal25LoadElementOrUndefined_52EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_16FixedDoubleArrayEEENS4_INS0_3SmiEEE:
.LFB22459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1384(%rbp), %r14
	leaq	-232(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1568(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1608(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1440(%rbp), %rbx
	subq	$1656, %rsp
	movq	%rdi, -1688(%rbp)
	movq	%rsi, -1680(%rbp)
	movq	%rdx, -1664(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1608(%rbp)
	movq	%rdi, -1440(%rbp)
	movl	$48, %edi
	movq	$0, -1432(%rbp)
	movq	$0, -1424(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -1432(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1608(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -1656(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1608(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1648(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1608(%rbp), %rax
	movl	$48, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1640(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1608(%rbp), %rax
	movl	$72, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1632(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1608(%rbp), %rax
	movl	$72, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -1624(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1608(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1680(%rbp), %xmm1
	movaps	%xmm0, -1568(%rbp)
	movhps	-1664(%rbp), %xmm1
	movq	$0, -1552(%rbp)
	movaps	%xmm1, -1680(%rbp)
	call	_Znwm@PLT
	movdqa	-1680(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1568(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L219
	call	_ZdlPv@PLT
.L219:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1376(%rbp)
	jne	.L380
	cmpq	$0, -1184(%rbp)
	jne	.L381
.L225:
	cmpq	$0, -992(%rbp)
	jne	.L382
.L228:
	cmpq	$0, -800(%rbp)
	jne	.L383
.L231:
	cmpq	$0, -608(%rbp)
	jne	.L384
.L234:
	cmpq	$0, -416(%rbp)
	leaq	-288(%rbp), %rbx
	jne	.L385
.L237:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movl	$1543, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L240
	call	_ZdlPv@PLT
.L240:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L241
	call	_ZdlPv@PLT
.L241:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L242
	.p2align 4,,10
	.p2align 3
.L246:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L243
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L246
.L244:
	movq	-280(%rbp), %r15
.L242:
	testq	%r15, %r15
	je	.L247
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L247:
	movq	-1624(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L248
	call	_ZdlPv@PLT
.L248:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L249
	.p2align 4,,10
	.p2align 3
.L253:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L250
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L253
.L251:
	movq	-472(%rbp), %r15
.L249:
	testq	%r15, %r15
	je	.L254
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L254:
	movq	-1632(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L255
	call	_ZdlPv@PLT
.L255:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L256
	.p2align 4,,10
	.p2align 3
.L260:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L257
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L260
.L258:
	movq	-664(%rbp), %r15
.L256:
	testq	%r15, %r15
	je	.L261
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L261:
	movq	-1640(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L262
	call	_ZdlPv@PLT
.L262:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L263
	.p2align 4,,10
	.p2align 3
.L267:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L264
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L267
.L265:
	movq	-856(%rbp), %r15
.L263:
	testq	%r15, %r15
	je	.L268
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L268:
	movq	-1648(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L269
	call	_ZdlPv@PLT
.L269:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L270
	.p2align 4,,10
	.p2align 3
.L274:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L271
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L274
.L272:
	movq	-1048(%rbp), %r15
.L270:
	testq	%r15, %r15
	je	.L275
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L275:
	movq	-1656(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L276
	call	_ZdlPv@PLT
.L276:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L277
	.p2align 4,,10
	.p2align 3
.L281:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L278
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L281
.L279:
	movq	-1240(%rbp), %r15
.L277:
	testq	%r15, %r15
	je	.L282
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L282:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L283
	call	_ZdlPv@PLT
.L283:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L284
	.p2align 4,,10
	.p2align 3
.L288:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L285
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L288
.L286:
	movq	-1432(%rbp), %r14
.L284:
	testq	%r14, %r14
	je	.L289
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L289:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L386
	addq	$1656, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L288
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L278:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L281
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L271:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L274
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L264:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L267
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L257:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L260
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L243:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L246
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L250:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L253
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L380:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movl	$1543, %r9d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L221
	call	_ZdlPv@PLT
.L221:
	movq	(%rbx), %rax
	movl	$42, %edx
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rsi
	leaq	-1600(%rbp), %rbx
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rcx, -1680(%rbp)
	movq	%rax, -1664(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1688(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1664(%rbp), %rdx
	movq	%r13, %rcx
	movq	%rbx, %rdi
	movq	-1680(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler23LoadDoubleWithHoleCheckENS0_8compiler5TNodeINS0_16FixedDoubleArrayEEENS3_INS0_3SmiEEEPNS2_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	movq	%rax, -1696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1696(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	-1680(%rbp), %xmm2
	movl	$40, %edi
	movaps	%xmm0, -1600(%rbp)
	movhps	-1664(%rbp), %xmm2
	movq	%rax, -64(%rbp)
	movaps	%xmm2, -1680(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm2, -80(%rbp)
	movq	$0, -1584(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rsi
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	leaq	40(%rax), %rdx
	leaq	-1056(%rbp), %rdi
	movq	%rax, -1600(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L222
	call	_ZdlPv@PLT
.L222:
	movq	-1648(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1560(%rbp)
	jne	.L387
.L223:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1184(%rbp)
	je	.L225
.L381:
	movq	-1656(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1248(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$101123591, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L226
	call	_ZdlPv@PLT
.L226:
	movq	(%rbx), %rax
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movdqu	(%rax), %xmm0
	movaps	%xmm1, -1568(%rbp)
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1680(%rbp)
	call	_Znwm@PLT
	movdqa	-1680(%rbp), %xmm0
	leaq	-864(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1568(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L227
	call	_ZdlPv@PLT
.L227:
	movq	-1640(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -992(%rbp)
	je	.L228
.L382:
	movq	-1648(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$101123591, (%rax)
	leaq	5(%rax), %rdx
	movb	$13, 4(%rax)
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L229
	call	_ZdlPv@PLT
.L229:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	32(%rax), %rdx
	movdqu	(%rax), %xmm5
	movaps	%xmm0, -1568(%rbp)
	movq	$0, -1552(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm5, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -1568(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L230
	call	_ZdlPv@PLT
.L230:
	movq	-1632(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L231
.L383:
	movq	-1640(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movl	$1543, %r8d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L232
	call	_ZdlPv@PLT
.L232:
	movq	(%rbx), %rax
	movq	-1688(%rbp), %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -1680(%rbp)
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	$0, -1552(%rbp)
	movhps	-1680(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -1568(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L233
	call	_ZdlPv@PLT
.L233:
	movq	-1624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -608(%rbp)
	je	.L234
.L384:
	movq	-1632(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movl	$1543, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%rbx, %rdi
	movb	$13, 2(%rax)
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L235
	call	_ZdlPv@PLT
.L235:
	movq	(%rbx), %rax
	movl	$43, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -1664(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -1680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1688(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler27AllocateHeapNumberWithValueENS0_8compiler11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$24, %edi
	movq	%rbx, -80(%rbp)
	movq	-1680(%rbp), %xmm0
	movq	$0, -1552(%rbp)
	movhps	-1664(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm3
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -1568(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L236
	call	_ZdlPv@PLT
.L236:
	movq	-1624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L385:
	movq	-1624(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movl	$1543, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$8, 2(%rax)
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L238
	call	_ZdlPv@PLT
.L238:
	movq	(%rbx), %rax
	movl	$41, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -1664(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -1680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %edi
	movq	%rbx, -80(%rbp)
	movq	-1680(%rbp), %xmm0
	movq	$0, -1552(%rbp)
	leaq	-288(%rbp), %rbx
	movhps	-1664(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm4
	leaq	24(%rax), %rdx
	movq	%rax, -1568(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L239
	call	_ZdlPv@PLT
.L239:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L387:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-1680(%rbp), %xmm5
	movaps	%xmm0, -1600(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movq	$0, -1584(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%rbx, %rsi
	leaq	32(%rax), %rdx
	leaq	-1248(%rbp), %rdi
	movq	%rax, -1600(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L224
	call	_ZdlPv@PLT
.L224:
	movq	-1656(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L223
.L386:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22459:
	.size	_ZN2v88internal25LoadElementOrUndefined_52EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_16FixedDoubleArrayEEENS4_INS0_3SmiEEE, .-_ZN2v88internal25LoadElementOrUndefined_52EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_16FixedDoubleArrayEEENS4_INS0_3SmiEEE
	.section	.text._ZN2v88internal17StoreArrayHole_53EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_16FixedDoubleArrayEEENS4_INS0_3SmiEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal17StoreArrayHole_53EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_16FixedDoubleArrayEEENS4_INS0_3SmiEEE
	.type	_ZN2v88internal17StoreArrayHole_53EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_16FixedDoubleArrayEEENS4_INS0_3SmiEEE, @function
_ZN2v88internal17StoreArrayHole_53EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_16FixedDoubleArrayEEENS4_INS0_3SmiEEE:
.LFB22475:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-568(%rbp), %r14
	leaq	-184(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-656(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-664(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-376(%rbp), %rbx
	subq	$680, %rsp
	movq	%rdi, -720(%rbp)
	movq	%rsi, -704(%rbp)
	movq	%rdx, -712(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -664(%rbp)
	movq	%rdi, -624(%rbp)
	movl	$48, %edi
	movq	$0, -616(%rbp)
	movq	$0, -608(%rbp)
	movq	$0, -600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -616(%rbp)
	leaq	-624(%rbp), %rax
	movq	%rdx, -600(%rbp)
	movq	%rdx, -608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -584(%rbp)
	movq	%rax, -688(%rbp)
	movq	$0, -592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$48, %edi
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	%rax, -432(%rbp)
	movq	$0, -408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -408(%rbp)
	movq	%rdx, -416(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -392(%rbp)
	movq	%rax, -424(%rbp)
	movq	$0, -400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r15, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-704(%rbp), %xmm1
	movaps	%xmm0, -656(%rbp)
	movhps	-712(%rbp), %xmm1
	movq	$0, -640(%rbp)
	movaps	%xmm1, -704(%rbp)
	call	_Znwm@PLT
	movdqa	-704(%rbp), %xmm1
	movq	-688(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -656(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L389
	call	_ZdlPv@PLT
.L389:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -560(%rbp)
	jne	.L457
.L390:
	leaq	-240(%rbp), %rax
	cmpq	$0, -368(%rbp)
	movq	%rax, -688(%rbp)
	jne	.L458
.L393:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movl	$1543, %ecx
	movq	-688(%rbp), %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L396
	call	_ZdlPv@PLT
.L396:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L397
	call	_ZdlPv@PLT
.L397:
	movq	-224(%rbp), %r15
	movq	-232(%rbp), %r13
	cmpq	%r13, %r15
	je	.L398
	.p2align 4,,10
	.p2align 3
.L402:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L399
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %r15
	jne	.L402
.L400:
	movq	-232(%rbp), %r13
.L398:
	testq	%r13, %r13
	je	.L403
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L403:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L404
	call	_ZdlPv@PLT
.L404:
	movq	-416(%rbp), %rbx
	movq	-424(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L405
	.p2align 4,,10
	.p2align 3
.L409:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L406
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L409
.L407:
	movq	-424(%rbp), %r13
.L405:
	testq	%r13, %r13
	je	.L410
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L410:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L411
	call	_ZdlPv@PLT
.L411:
	movq	-608(%rbp), %rbx
	movq	-616(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L412
	.p2align 4,,10
	.p2align 3
.L416:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L413
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L416
.L414:
	movq	-616(%rbp), %r13
.L412:
	testq	%r13, %r13
	je	.L417
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L417:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L459
	addq	$680, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L416
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L399:
	addq	$24, %r13
	cmpq	%r13, %r15
	jne	.L402
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L406:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L409
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L457:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movl	$1543, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	movq	-688(%rbp), %rdi
	leaq	2(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L391
	movq	%rax, -688(%rbp)
	call	_ZdlPv@PLT
	movq	-688(%rbp), %rax
.L391:
	movq	(%rax), %rax
	movl	$47, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rcx, -688(%rbp)
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-720(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-704(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	-688(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler25StoreFixedDoubleArrayHoleENS0_8compiler5TNodeINS0_16FixedDoubleArrayEEEPNS2_4NodeENS1_13ParameterModeE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$46, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movq	-688(%rbp), %xmm0
	movaps	%xmm1, -656(%rbp)
	movhps	-704(%rbp), %xmm0
	movq	$0, -640(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movdqa	-688(%rbp), %xmm0
	leaq	-432(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -656(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L392
	call	_ZdlPv@PLT
.L392:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L458:
	leaq	-432(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r8, -688(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movl	$1543, %esi
	movq	-688(%rbp), %r8
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r13, %rsi
	movq	%r8, %rdi
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L394
	movq	%rax, -688(%rbp)
	call	_ZdlPv@PLT
	movq	-688(%rbp), %rax
.L394:
	movq	(%rax), %rax
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movdqu	(%rax), %xmm0
	movaps	%xmm1, -656(%rbp)
	movq	$0, -640(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movdqa	-688(%rbp), %xmm0
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -656(%rbp)
	movups	%xmm0, (%rax)
	leaq	-240(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	movq	%rax, -688(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L395
	call	_ZdlPv@PLT
.L395:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L393
.L459:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22475:
	.size	_ZN2v88internal17StoreArrayHole_53EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_16FixedDoubleArrayEEENS4_INS0_3SmiEEE, .-_ZN2v88internal17StoreArrayHole_53EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_16FixedDoubleArrayEEENS4_INS0_3SmiEEE
	.section	.rodata._ZN2v88internal17StoreArrayHole_54EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10FixedArrayEEENS4_INS0_3SmiEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../../deps/v8/../../deps/v8/src/builtins/torque-internal.tq"
	.section	.text._ZN2v88internal17StoreArrayHole_54EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10FixedArrayEEENS4_INS0_3SmiEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal17StoreArrayHole_54EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10FixedArrayEEENS4_INS0_3SmiEEE
	.type	_ZN2v88internal17StoreArrayHole_54EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10FixedArrayEEENS4_INS0_3SmiEEE, @function
_ZN2v88internal17StoreArrayHole_54EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10FixedArrayEEENS4_INS0_3SmiEEE:
.LFB22476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-1944(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1936(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1904(%rbp), %r12
	pushq	%rbx
	subq	$2120, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -2048(%rbp)
	movq	%rdx, -2064(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1944(%rbp)
	movq	%rdi, -1904(%rbp)
	movl	$48, %edi
	movq	$0, -1896(%rbp)
	movq	$0, -1888(%rbp)
	movq	$0, -1880(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1896(%rbp)
	leaq	-1848(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1880(%rbp)
	movq	%rdx, -1888(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1864(%rbp)
	movq	%rax, -1960(%rbp)
	movq	$0, -1872(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1944(%rbp), %rax
	movl	$312, %edi
	movq	$0, -1704(%rbp)
	movq	$0, -1696(%rbp)
	movq	%rax, -1712(%rbp)
	movq	$0, -1688(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rax, -1704(%rbp)
	leaq	-1656(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1688(%rbp)
	movq	%rdx, -1696(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1672(%rbp)
	movq	%rax, -1976(%rbp)
	movq	$0, -1680(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1944(%rbp), %rax
	movl	$312, %edi
	movq	$0, -1512(%rbp)
	movq	$0, -1504(%rbp)
	movq	%rax, -1520(%rbp)
	movq	$0, -1496(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rax, -1512(%rbp)
	leaq	-1464(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1496(%rbp)
	movq	%rdx, -1504(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1480(%rbp)
	movq	%rax, -1992(%rbp)
	movq	$0, -1488(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1944(%rbp), %rax
	movl	$360, %edi
	movq	$0, -1320(%rbp)
	movq	$0, -1312(%rbp)
	movq	%rax, -1328(%rbp)
	movq	$0, -1304(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	360(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 352(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movq	%rax, -1320(%rbp)
	leaq	-1272(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1304(%rbp)
	movq	%rdx, -1312(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1288(%rbp)
	movq	%rax, -2000(%rbp)
	movq	$0, -1296(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1944(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1128(%rbp)
	movq	$0, -1120(%rbp)
	movq	%rax, -1136(%rbp)
	movq	$0, -1112(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1128(%rbp)
	leaq	-1080(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1112(%rbp)
	movq	%rdx, -1120(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1096(%rbp)
	movq	%rax, -1984(%rbp)
	movq	$0, -1104(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1944(%rbp), %rax
	movl	$240, %edi
	movq	$0, -936(%rbp)
	movq	$0, -928(%rbp)
	movq	%rax, -944(%rbp)
	movq	$0, -920(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -936(%rbp)
	leaq	-888(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -920(%rbp)
	movq	%rdx, -928(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -904(%rbp)
	movq	%rax, -2008(%rbp)
	movq	$0, -912(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1944(%rbp), %rax
	movl	$216, %edi
	movq	$0, -744(%rbp)
	movq	$0, -736(%rbp)
	movq	%rax, -752(%rbp)
	movq	$0, -728(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -744(%rbp)
	leaq	-696(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -728(%rbp)
	movq	%rdx, -736(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -712(%rbp)
	movq	%rax, -2016(%rbp)
	movq	$0, -720(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1944(%rbp), %rax
	movl	$48, %edi
	movq	$0, -552(%rbp)
	movq	$0, -544(%rbp)
	movq	%rax, -560(%rbp)
	movq	$0, -536(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -552(%rbp)
	leaq	-504(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -536(%rbp)
	movq	%rdx, -544(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -520(%rbp)
	movq	%rax, -2024(%rbp)
	movq	$0, -528(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1944(%rbp), %rax
	movl	$48, %edi
	movq	$0, -360(%rbp)
	movq	$0, -352(%rbp)
	movq	%rax, -368(%rbp)
	movq	$0, -344(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -360(%rbp)
	leaq	-312(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -344(%rbp)
	movq	%rdx, -352(%rbp)
	xorl	%edx, %edx
	movq	%rax, -1968(%rbp)
	movups	%xmm0, -328(%rbp)
	movq	$0, -336(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-2048(%rbp), %xmm1
	movaps	%xmm0, -1936(%rbp)
	movhps	-2064(%rbp), %xmm1
	movq	$0, -1920(%rbp)
	movaps	%xmm1, -2048(%rbp)
	call	_Znwm@PLT
	movdqa	-2048(%rbp), %xmm1
	movq	%r12, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1936(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1920(%rbp)
	movq	%rdx, -1928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L461
	call	_ZdlPv@PLT
.L461:
	movq	-1960(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1840(%rbp)
	jne	.L661
	cmpq	$0, -1648(%rbp)
	jne	.L662
.L466:
	cmpq	$0, -1456(%rbp)
	jne	.L663
.L469:
	cmpq	$0, -1264(%rbp)
	jne	.L664
.L472:
	cmpq	$0, -1072(%rbp)
	jne	.L665
.L475:
	cmpq	$0, -880(%rbp)
	jne	.L666
.L477:
	cmpq	$0, -688(%rbp)
	jne	.L667
.L480:
	cmpq	$0, -496(%rbp)
	leaq	-368(%rbp), %r12
	jne	.L668
.L483:
	movq	-1968(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1920(%rbp)
	movaps	%xmm0, -1936(%rbp)
	call	_Znwm@PLT
	movl	$1543, %ecx
	movq	%r12, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1936(%rbp)
	movq	%rdx, -1920(%rbp)
	movq	%rdx, -1928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L486
	call	_ZdlPv@PLT
.L486:
	movq	-1968(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L487
	call	_ZdlPv@PLT
.L487:
	movq	-352(%rbp), %rbx
	movq	-360(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L488
	.p2align 4,,10
	.p2align 3
.L492:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L489
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L492
.L490:
	movq	-360(%rbp), %r12
.L488:
	testq	%r12, %r12
	je	.L493
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L493:
	movq	-2024(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L494
	call	_ZdlPv@PLT
.L494:
	movq	-544(%rbp), %rbx
	movq	-552(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L495
	.p2align 4,,10
	.p2align 3
.L499:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L496
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L499
.L497:
	movq	-552(%rbp), %r12
.L495:
	testq	%r12, %r12
	je	.L500
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L500:
	movq	-2016(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-720(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L501
	call	_ZdlPv@PLT
.L501:
	movq	-736(%rbp), %rbx
	movq	-744(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L502
	.p2align 4,,10
	.p2align 3
.L506:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L503
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L506
.L504:
	movq	-744(%rbp), %r12
.L502:
	testq	%r12, %r12
	je	.L507
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L507:
	movq	-2008(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-912(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L508
	call	_ZdlPv@PLT
.L508:
	movq	-928(%rbp), %rbx
	movq	-936(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L509
	.p2align 4,,10
	.p2align 3
.L513:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L510
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L513
.L511:
	movq	-936(%rbp), %r12
.L509:
	testq	%r12, %r12
	je	.L514
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L514:
	movq	-1984(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L515
	call	_ZdlPv@PLT
.L515:
	movq	-1120(%rbp), %rbx
	movq	-1128(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L516
	.p2align 4,,10
	.p2align 3
.L520:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L517
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L520
.L518:
	movq	-1128(%rbp), %r12
.L516:
	testq	%r12, %r12
	je	.L521
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L521:
	movq	-2000(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1296(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L522
	call	_ZdlPv@PLT
.L522:
	movq	-1312(%rbp), %rbx
	movq	-1320(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L523
	.p2align 4,,10
	.p2align 3
.L527:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L524
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L527
.L525:
	movq	-1320(%rbp), %r12
.L523:
	testq	%r12, %r12
	je	.L528
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L528:
	movq	-1992(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1488(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L529
	call	_ZdlPv@PLT
.L529:
	movq	-1504(%rbp), %rbx
	movq	-1512(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L530
	.p2align 4,,10
	.p2align 3
.L534:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L531
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L534
.L532:
	movq	-1512(%rbp), %r12
.L530:
	testq	%r12, %r12
	je	.L535
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L535:
	movq	-1976(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1680(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L536
	call	_ZdlPv@PLT
.L536:
	movq	-1696(%rbp), %rbx
	movq	-1704(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L537
	.p2align 4,,10
	.p2align 3
.L541:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L538
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L541
.L539:
	movq	-1704(%rbp), %r12
.L537:
	testq	%r12, %r12
	je	.L542
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L542:
	movq	-1960(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1872(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L543
	call	_ZdlPv@PLT
.L543:
	movq	-1888(%rbp), %rbx
	movq	-1896(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L544
	.p2align 4,,10
	.p2align 3
.L548:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L545
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L548
.L546:
	movq	-1896(%rbp), %r12
.L544:
	testq	%r12, %r12
	je	.L549
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L549:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L669
	addq	$2120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L545:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L548
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L538:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L541
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L531:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L534
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L524:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L527
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L517:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L520
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L510:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L513
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L503:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L506
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L489:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L492
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L496:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L499
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L661:
	movq	-1960(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1920(%rbp)
	movaps	%xmm0, -1936(%rbp)
	call	_Znwm@PLT
	movl	$1543, %r9d
	movq	%r12, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1936(%rbp)
	movq	%rdx, -1920(%rbp)
	movq	%rdx, -1928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1936(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L463
	call	_ZdlPv@PLT
.L463:
	movq	(%rbx), %rax
	movl	$51, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	%rcx, -2048(%rbp)
	movq	%rbx, -2064(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$8, %esi
	movq	%r14, %rdi
	movq	%rax, -2032(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-2048(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$55, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -2080(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$56, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$37, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	-2080(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -2096(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2096(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm2
	pxor	%xmm0, %xmm0
	movq	-2048(%rbp), %xmm6
	movq	-2032(%rbp), %rax
	movl	$104, %edi
	movaps	%xmm0, -1936(%rbp)
	movq	-2064(%rbp), %xmm5
	movq	-2080(%rbp), %xmm7
	movdqa	%xmm6, %xmm3
	movq	%xmm6, -112(%rbp)
	punpcklqdq	%xmm5, %xmm3
	movdqa	%xmm5, %xmm4
	movq	%rax, -104(%rbp)
	movdqa	%xmm7, %xmm5
	punpcklqdq	%xmm2, %xmm4
	movq	%xmm6, -160(%rbp)
	movdqa	%xmm7, %xmm6
	punpcklqdq	%xmm2, %xmm6
	movhps	-2064(%rbp), %xmm5
	movaps	%xmm3, -2112(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -2096(%rbp)
	movaps	%xmm5, -2064(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm6, -2080(%rbp)
	movaps	%xmm6, -96(%rbp)
	movq	%rax, -152(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -1920(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm7
	movq	-80(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm3
	movdqa	-160(%rbp), %xmm2
	leaq	104(%rax), %rdx
	leaq	-1712(%rbp), %rdi
	movups	%xmm7, (%rax)
	movdqa	-144(%rbp), %xmm7
	movups	%xmm2, 16(%rax)
	movdqa	-128(%rbp), %xmm2
	movups	%xmm7, 32(%rax)
	movdqa	-112(%rbp), %xmm7
	movq	%rcx, 96(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm3, 80(%rax)
	movq	%rax, -1936(%rbp)
	movq	%rdx, -1920(%rbp)
	movq	%rdx, -1928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L464
	call	_ZdlPv@PLT
.L464:
	movq	-2048(%rbp), %xmm0
	movdqa	-2112(%rbp), %xmm4
	movl	$104, %edi
	movq	%rbx, -80(%rbp)
	movdqa	-2064(%rbp), %xmm5
	movdqa	-2096(%rbp), %xmm6
	movq	$0, -1920(%rbp)
	movhps	-2032(%rbp), %xmm0
	movdqa	-2080(%rbp), %xmm2
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -1936(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-176(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm6
	leaq	104(%rax), %rdx
	leaq	-1520(%rbp), %rdi
	movdqa	-160(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm4
	movq	%rcx, 96(%rax)
	movdqa	-96(%rbp), %xmm2
	movups	%xmm7, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	movups	%xmm2, 80(%rax)
	movq	%rax, -1936(%rbp)
	movq	%rdx, -1920(%rbp)
	movq	%rdx, -1928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L465
	call	_ZdlPv@PLT
.L465:
	movq	-1992(%rbp), %rcx
	movq	-1976(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1648(%rbp)
	je	.L466
.L662:
	movq	-1976(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-1712(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$13, %edi
	movq	$0, -1920(%rbp)
	movaps	%xmm0, -1936(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	movabsq	$361983438678853127, %rcx
	movq	%rcx, (%rax)
	leaq	13(%rax), %rdx
	movl	$84215047, 8(%rax)
	movb	$5, 12(%rax)
	movq	%rax, -1936(%rbp)
	movq	%rdx, -1920(%rbp)
	movq	%rdx, -1928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1936(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L467
	call	_ZdlPv@PLT
.L467:
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	(%rax), %rcx
	movq	24(%rax), %rbx
	movq	88(%rax), %r10
	movq	%rsi, -2080(%rbp)
	movq	16(%rax), %rsi
	movq	%rdx, -2120(%rbp)
	movq	%rdi, -2136(%rbp)
	movq	48(%rax), %rdx
	movq	80(%rax), %rdi
	movq	%rcx, -2064(%rbp)
	movq	%rbx, -2096(%rbp)
	movq	72(%rax), %rcx
	movq	32(%rax), %rbx
	movq	%rsi, -2032(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rdx, -2128(%rbp)
	movl	$39, %edx
	movq	64(%rax), %r12
	movq	%rdi, -2144(%rbp)
	movq	%r14, %rdi
	movq	%r10, -2152(%rbp)
	movq	%rcx, -2048(%rbp)
	movq	%rbx, -2112(%rbp)
	movq	96(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -2160(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2160(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrMulENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, -2160(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2160(%rbp), %rdx
	movq	-2048(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, -2160(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$38, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	-2160(%rbp), %rcx
	call	_ZN2v88internal45UnsafeNewReference20UT5ATSmi10HeapObject_1411EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	-1936(%rbp), %rax
	movl	$120, %edi
	movq	-2064(%rbp), %xmm0
	movq	%rbx, -80(%rbp)
	movhps	-2080(%rbp), %xmm0
	movq	%rax, -72(%rbp)
	movq	-1928(%rbp), %rax
	movaps	%xmm0, -176(%rbp)
	movq	-2032(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movhps	-2096(%rbp), %xmm0
	movq	$0, -1920(%rbp)
	movaps	%xmm0, -160(%rbp)
	movq	-2112(%rbp), %xmm0
	movhps	-2120(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-2128(%rbp), %xmm0
	movhps	-2136(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%r12, %xmm0
	movhps	-2048(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-2144(%rbp), %xmm0
	movhps	-2152(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1936(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm7
	movq	%r13, %rsi
	movq	-64(%rbp), %rcx
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm6
	leaq	120(%rax), %rdx
	leaq	-1328(%rbp), %rdi
	movdqa	-160(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm4
	movups	%xmm7, (%rax)
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm7
	movq	%rcx, 112(%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	movups	%xmm2, 80(%rax)
	movups	%xmm7, 96(%rax)
	movq	%rax, -1936(%rbp)
	movq	%rdx, -1920(%rbp)
	movq	%rdx, -1928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L468
	call	_ZdlPv@PLT
.L468:
	movq	-2000(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1456(%rbp)
	je	.L469
.L663:
	movq	-1992(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-1520(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$13, %edi
	movq	$0, -1920(%rbp)
	movaps	%xmm0, -1936(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	movabsq	$361983438678853127, %rcx
	movq	%rcx, (%rax)
	leaq	13(%rax), %rdx
	movl	$84215047, 8(%rax)
	movb	$5, 12(%rax)
	movq	%rax, -1936(%rbp)
	movq	%rdx, -1920(%rbp)
	movq	%rdx, -1928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1936(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L470
	call	_ZdlPv@PLT
.L470:
	movq	(%rbx), %rax
	movl	$41, %edx
	movq	%r14, %rdi
	movq	24(%rax), %rbx
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	32(%rax), %r12
	movq	%rbx, -2032(%rbp)
	movq	40(%rax), %rbx
	movq	%rsi, -2064(%rbp)
	movq	16(%rax), %rsi
	movq	%rbx, -2096(%rbp)
	movq	48(%rax), %rbx
	movq	56(%rax), %rax
	movq	%rsi, -2080(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rcx, -2048(%rbp)
	movq	%rax, -2112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$64, %edi
	movq	-2048(%rbp), %xmm0
	movq	$0, -1920(%rbp)
	movhps	-2064(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-2080(%rbp), %xmm0
	movhps	-2032(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%r12, %xmm0
	movhps	-2096(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	movhps	-2112(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1936(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-160(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm5
	leaq	64(%rax), %rdx
	leaq	-1136(%rbp), %rdi
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movq	%rax, -1936(%rbp)
	movq	%rdx, -1920(%rbp)
	movq	%rdx, -1928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L471
	call	_ZdlPv@PLT
.L471:
	movq	-1984(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1264(%rbp)
	je	.L472
.L664:
	movq	-2000(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-1328(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$15, %edi
	movq	$0, -1920(%rbp)
	movaps	%xmm0, -1936(%rbp)
	call	_Znwm@PLT
	movl	$1797, %r8d
	movq	%r12, %rdi
	movq	%r13, %rsi
	movabsq	$361983438678853127, %rcx
	movl	$84215047, 8(%rax)
	leaq	15(%rax), %rdx
	movq	%rcx, (%rax)
	movw	%r8w, 12(%rax)
	movb	$5, 14(%rax)
	movq	%rax, -1936(%rbp)
	movq	%rdx, -1920(%rbp)
	movq	%rdx, -1928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1936(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L473
	call	_ZdlPv@PLT
.L473:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	24(%rax), %rbx
	movq	8(%rax), %rsi
	movq	40(%rax), %rdx
	movq	(%rax), %rcx
	movq	%rbx, -2032(%rbp)
	movq	32(%rax), %rbx
	movq	%rsi, -2064(%rbp)
	movq	16(%rax), %rsi
	movq	%rdx, -2112(%rbp)
	movq	56(%rax), %rdx
	movq	48(%rax), %r12
	movq	%rbx, -2096(%rbp)
	movq	104(%rax), %rbx
	movq	112(%rax), %rax
	movq	%rsi, -2080(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rdx, -2120(%rbp)
	movl	$56, %edx
	movq	%rcx, -2048(%rbp)
	movq	%rax, -2128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$80, %edi
	movq	-2048(%rbp), %xmm0
	movq	$0, -1920(%rbp)
	movhps	-2064(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-2080(%rbp), %xmm0
	movhps	-2032(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-2096(%rbp), %xmm0
	movhps	-2112(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%r12, %xmm0
	movhps	-2120(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%rbx, %xmm0
	movhps	-2128(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1936(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-160(%rbp), %xmm7
	leaq	80(%rax), %rdx
	leaq	-944(%rbp), %rdi
	movdqa	-144(%rbp), %xmm3
	movups	%xmm2, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rax, -1936(%rbp)
	movq	%rdx, -1920(%rbp)
	movq	%rdx, -1928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L474
	call	_ZdlPv@PLT
.L474:
	movq	-2008(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1072(%rbp)
	je	.L475
.L665:
	movq	-1984(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-1136(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -1920(%rbp)
	movaps	%xmm0, -1936(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	movabsq	$361983438678853127, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -1936(%rbp)
	movq	%rdx, -1920(%rbp)
	movq	%rdx, -1928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L476
	call	_ZdlPv@PLT
.L476:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -880(%rbp)
	je	.L477
.L666:
	movq	-2008(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-944(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -1920(%rbp)
	movaps	%xmm0, -1936(%rbp)
	call	_Znwm@PLT
	movl	$1287, %edi
	movq	%r13, %rsi
	movabsq	$361983438678853127, %rcx
	movw	%di, 8(%rax)
	leaq	10(%rax), %rdx
	movq	%r12, %rdi
	movq	%rcx, (%rax)
	movq	%rax, -1936(%rbp)
	movq	%rdx, -1920(%rbp)
	movq	%rdx, -1928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1936(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L478
	call	_ZdlPv@PLT
.L478:
	movq	(%rbx), %rax
	movl	$72, %edi
	movdqu	64(%rax), %xmm6
	movdqu	48(%rax), %xmm0
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	72(%rax), %rdx
	movdqu	(%rax), %xmm7
	punpcklqdq	%xmm6, %xmm0
	movq	$0, -1920(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -112(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -1936(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-176(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm6
	leaq	72(%rax), %rdx
	leaq	-752(%rbp), %rdi
	movdqa	-144(%rbp), %xmm3
	movq	%rcx, 64(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm4, 48(%rax)
	movq	%rax, -1936(%rbp)
	movq	%rdx, -1920(%rbp)
	movq	%rdx, -1928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L479
	call	_ZdlPv@PLT
.L479:
	movq	-2016(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -688(%rbp)
	je	.L480
.L667:
	movq	-2016(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-752(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -1920(%rbp)
	movaps	%xmm0, -1936(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	movabsq	$506098626754708999, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$5, 8(%rax)
	movq	%rax, -1936(%rbp)
	movq	%rdx, -1920(%rbp)
	movq	%rdx, -1928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1936(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L481
	call	_ZdlPv@PLT
.L481:
	movq	(%rbx), %rax
	movl	$51, %edx
	movq	%r14, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	64(%rax), %r8
	movq	56(%rax), %r12
	movq	%rsi, -2064(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -2048(%rbp)
	movq	%r8, -2080(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal10TheHole_62EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-2080(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%rbx, %r8
	movq	%r12, %rdx
	movl	$7, %esi
	movq	%rax, %rcx
	movl	$2, %r9d
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13StoreToObjectENS0_21MachineRepresentationENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEEPNS1_4NodeENS0_25StoreToObjectWriteBarrierE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$50, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movq	-2048(%rbp), %xmm0
	movaps	%xmm1, -1936(%rbp)
	movhps	-2064(%rbp), %xmm0
	movq	$0, -1920(%rbp)
	movaps	%xmm0, -2048(%rbp)
	call	_Znwm@PLT
	movdqa	-2048(%rbp), %xmm0
	leaq	-560(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1936(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -1920(%rbp)
	movq	%rdx, -1928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L482
	call	_ZdlPv@PLT
.L482:
	movq	-2024(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L668:
	movq	-2024(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-560(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1920(%rbp)
	movaps	%xmm0, -1936(%rbp)
	call	_Znwm@PLT
	movl	$1543, %esi
	movq	%r12, %rdi
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -1936(%rbp)
	movq	%rdx, -1920(%rbp)
	movq	%rdx, -1928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1936(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L484
	call	_ZdlPv@PLT
.L484:
	movq	(%rbx), %rax
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	leaq	-368(%rbp), %r12
	movdqu	(%rax), %xmm0
	movaps	%xmm1, -1936(%rbp)
	movq	$0, -1920(%rbp)
	movaps	%xmm0, -2048(%rbp)
	call	_Znwm@PLT
	movdqa	-2048(%rbp), %xmm0
	movq	%r12, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1936(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -1920(%rbp)
	movq	%rdx, -1928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L485
	call	_ZdlPv@PLT
.L485:
	movq	-1968(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L483
.L669:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22476:
	.size	_ZN2v88internal17StoreArrayHole_54EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10FixedArrayEEENS4_INS0_3SmiEEE, .-_ZN2v88internal17StoreArrayHole_54EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10FixedArrayEEENS4_INS0_3SmiEEE
	.section	.rodata._ZN2v88internal118UnsafeCast100UT8ATBigInt7ATFalse6ATNull5ATSmi9ATTheHole6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_1440EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../../deps/v8/../../deps/v8/src/builtins/base.tq"
	.section	.text._ZN2v88internal118UnsafeCast100UT8ATBigInt7ATFalse6ATNull5ATSmi9ATTheHole6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_1440EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal118UnsafeCast100UT8ATBigInt7ATFalse6ATNull5ATSmi9ATTheHole6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_1440EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal118UnsafeCast100UT8ATBigInt7ATFalse6ATNull5ATSmi9ATTheHole6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_1440EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal118UnsafeCast100UT8ATBigInt7ATFalse6ATNull5ATSmi9ATTheHole6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_1440EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE:
.LFB22495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-216(%rbp), %r14
	leaq	-688(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-600(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-696(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-656(%rbp), %rbx
	subq	$696, %rsp
	movq	%rsi, -736(%rbp)
	movq	%rdx, -720(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -696(%rbp)
	movq	%rdi, -656(%rbp)
	movl	$48, %edi
	movq	$0, -648(%rbp)
	movq	$0, -640(%rbp)
	movq	$0, -632(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -632(%rbp)
	movq	%rdx, -640(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -616(%rbp)
	movq	%rax, -648(%rbp)
	movq	$0, -624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	%rax, -464(%rbp)
	movq	$0, -440(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -456(%rbp)
	leaq	-408(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -440(%rbp)
	movq	%rdx, -448(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -424(%rbp)
	movq	%rax, -712(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	%rax, -272(%rbp)
	movq	$0, -248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -248(%rbp)
	movq	%rdx, -256(%rbp)
	xorl	%edx, %edx
	movq	%rax, -264(%rbp)
	movups	%xmm0, -232(%rbp)
	movq	$0, -240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-736(%rbp), %xmm1
	movaps	%xmm0, -688(%rbp)
	movhps	-720(%rbp), %xmm1
	movq	$0, -672(%rbp)
	movaps	%xmm1, -736(%rbp)
	call	_Znwm@PLT
	movdqa	-736(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -688(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L671
	call	_ZdlPv@PLT
.L671:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -592(%rbp)
	jne	.L739
.L672:
	cmpq	$0, -400(%rbp)
	leaq	-272(%rbp), %rbx
	jne	.L740
.L675:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L678
	call	_ZdlPv@PLT
.L678:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	16(%rax), %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L679
	call	_ZdlPv@PLT
.L679:
	movq	-256(%rbp), %rbx
	movq	-264(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L680
	.p2align 4,,10
	.p2align 3
.L684:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L681
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L684
.L682:
	movq	-264(%rbp), %r14
.L680:
	testq	%r14, %r14
	je	.L685
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L685:
	movq	-712(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L686
	call	_ZdlPv@PLT
.L686:
	movq	-448(%rbp), %rbx
	movq	-456(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L687
	.p2align 4,,10
	.p2align 3
.L691:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L688
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L691
.L689:
	movq	-456(%rbp), %r14
.L687:
	testq	%r14, %r14
	je	.L692
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L692:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L693
	call	_ZdlPv@PLT
.L693:
	movq	-640(%rbp), %rbx
	movq	-648(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L694
	.p2align 4,,10
	.p2align 3
.L698:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L695
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L698
.L696:
	movq	-648(%rbp), %r13
.L694:
	testq	%r13, %r13
	je	.L699
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L699:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L741
	addq	$696, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L695:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L698
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L681:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L684
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L688:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L691
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L739:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r15, %rsi
	movw	%di, (%rax)
	leaq	2(%rax), %rdx
	movq	%rbx, %rdi
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L673
	call	_ZdlPv@PLT
.L673:
	movq	(%rbx), %rax
	movl	$2790, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	%rcx, -736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm2
	movl	$24, %edi
	movq	-736(%rbp), %xmm0
	movq	%rbx, -64(%rbp)
	movq	$0, -672(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm3
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-464(%rbp), %rdi
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L674
	call	_ZdlPv@PLT
.L674:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L740:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-464(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rsi
	movb	$8, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L676
	call	_ZdlPv@PLT
.L676:
	movq	(%rbx), %rax
	movl	$37, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -720(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %edi
	movq	%rbx, -64(%rbp)
	movq	-736(%rbp), %xmm0
	movq	$0, -672(%rbp)
	leaq	-272(%rbp), %rbx
	movhps	-720(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movdqa	-80(%rbp), %xmm4
	leaq	24(%rax), %rdx
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L677
	call	_ZdlPv@PLT
.L677:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L675
.L741:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22495:
	.size	_ZN2v88internal118UnsafeCast100UT8ATBigInt7ATFalse6ATNull5ATSmi9ATTheHole6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_1440EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal118UnsafeCast100UT8ATBigInt7ATFalse6ATNull5ATSmi9ATTheHole6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_1440EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal25LoadElementOrUndefined_51EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_3SmiEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25LoadElementOrUndefined_51EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_3SmiEEE
	.type	_ZN2v88internal25LoadElementOrUndefined_51EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_3SmiEEE, @function
_ZN2v88internal25LoadElementOrUndefined_51EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_3SmiEEE:
.LFB22429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-1960(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1920(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$2120, %rsp
	movq	%rsi, -2048(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1960(%rbp)
	movq	%rdi, -1920(%rbp)
	movl	$72, %edi
	movq	$0, -1912(%rbp)
	movq	$0, -1904(%rbp)
	movq	$0, -1896(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1912(%rbp)
	leaq	-1864(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1896(%rbp)
	movq	%rdx, -1904(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1880(%rbp)
	movq	%rax, -1976(%rbp)
	movq	$0, -1888(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1960(%rbp), %rax
	movl	$336, %edi
	movq	$0, -1720(%rbp)
	movq	$0, -1712(%rbp)
	movq	%rax, -1728(%rbp)
	movq	$0, -1704(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -1720(%rbp)
	leaq	-1672(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1704(%rbp)
	movq	%rdx, -1712(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1688(%rbp)
	movq	%rax, -2000(%rbp)
	movq	$0, -1696(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1960(%rbp), %rax
	movl	$336, %edi
	movq	$0, -1528(%rbp)
	movq	$0, -1520(%rbp)
	movq	%rax, -1536(%rbp)
	movq	$0, -1512(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -1528(%rbp)
	leaq	-1480(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1512(%rbp)
	movq	%rdx, -1520(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1496(%rbp)
	movq	%rax, -2016(%rbp)
	movq	$0, -1504(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1960(%rbp), %rax
	movl	$384, %edi
	movq	$0, -1336(%rbp)
	movq	$0, -1328(%rbp)
	movq	%rax, -1344(%rbp)
	movq	$0, -1320(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	384(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movq	%rax, -1336(%rbp)
	leaq	-1288(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1320(%rbp)
	movq	%rdx, -1328(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1304(%rbp)
	movq	%rax, -1992(%rbp)
	movq	$0, -1312(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1960(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1144(%rbp)
	movq	$0, -1136(%rbp)
	movq	%rax, -1152(%rbp)
	movq	$0, -1128(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1144(%rbp)
	leaq	-1096(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1128(%rbp)
	movq	%rdx, -1136(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1112(%rbp)
	movq	%rax, -2008(%rbp)
	movq	$0, -1120(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1960(%rbp), %rax
	movl	$264, %edi
	movq	$0, -952(%rbp)
	movq	$0, -944(%rbp)
	movq	%rax, -960(%rbp)
	movq	$0, -936(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -952(%rbp)
	leaq	-904(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -936(%rbp)
	movq	%rdx, -944(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -920(%rbp)
	movq	%rax, -2024(%rbp)
	movq	$0, -928(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1960(%rbp), %rax
	movl	$240, %edi
	movq	$0, -760(%rbp)
	movq	$0, -752(%rbp)
	movq	%rax, -768(%rbp)
	movq	$0, -744(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -760(%rbp)
	leaq	-712(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -744(%rbp)
	movq	%rdx, -752(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -728(%rbp)
	movq	%rax, -2032(%rbp)
	movq	$0, -736(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1960(%rbp), %rax
	movl	$96, %edi
	movq	$0, -568(%rbp)
	movq	$0, -560(%rbp)
	movq	%rax, -576(%rbp)
	movq	$0, -552(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -568(%rbp)
	leaq	-520(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -552(%rbp)
	movq	%rdx, -560(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -536(%rbp)
	movq	%rax, -2040(%rbp)
	movq	$0, -544(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1960(%rbp), %rax
	movl	$96, %edi
	movq	$0, -376(%rbp)
	movq	$0, -368(%rbp)
	movq	%rax, -384(%rbp)
	movq	$0, -360(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -376(%rbp)
	leaq	-328(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -360(%rbp)
	movq	%rdx, -368(%rbp)
	xorl	%edx, %edx
	movq	%rax, -1984(%rbp)
	movups	%xmm0, -344(%rbp)
	movq	$0, -352(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2048(%rbp), %r9
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	%r13, -184(%rbp)
	leaq	-1952(%rbp), %r13
	movq	%r9, -192(%rbp)
	movaps	%xmm0, -1952(%rbp)
	movq	%rbx, -176(%rbp)
	movq	$0, -1936(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r13, %rsi
	movdqa	-192(%rbp), %xmm2
	leaq	24(%rax), %rdx
	movq	%rax, -1952(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L743
	call	_ZdlPv@PLT
.L743:
	movq	-1976(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1856(%rbp)
	jne	.L943
	cmpq	$0, -1664(%rbp)
	jne	.L944
.L748:
	cmpq	$0, -1472(%rbp)
	jne	.L945
.L751:
	cmpq	$0, -1280(%rbp)
	jne	.L946
.L754:
	cmpq	$0, -1088(%rbp)
	jne	.L947
.L757:
	cmpq	$0, -896(%rbp)
	jne	.L948
.L759:
	cmpq	$0, -704(%rbp)
	jne	.L949
.L762:
	cmpq	$0, -512(%rbp)
	leaq	-384(%rbp), %r12
	jne	.L950
.L765:
	movq	-1984(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	movl	$134612743, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L768
	call	_ZdlPv@PLT
.L768:
	movq	(%rbx), %rax
	movq	-1984(%rbp), %rdi
	movq	24(%rax), %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L769
	call	_ZdlPv@PLT
.L769:
	movq	-368(%rbp), %rbx
	movq	-376(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L770
	.p2align 4,,10
	.p2align 3
.L774:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L771
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L774
.L772:
	movq	-376(%rbp), %r13
.L770:
	testq	%r13, %r13
	je	.L775
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L775:
	movq	-2040(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-544(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L776
	call	_ZdlPv@PLT
.L776:
	movq	-560(%rbp), %rbx
	movq	-568(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L777
	.p2align 4,,10
	.p2align 3
.L781:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L778
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L781
.L779:
	movq	-568(%rbp), %r13
.L777:
	testq	%r13, %r13
	je	.L782
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L782:
	movq	-2032(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-736(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L783
	call	_ZdlPv@PLT
.L783:
	movq	-752(%rbp), %rbx
	movq	-760(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L784
	.p2align 4,,10
	.p2align 3
.L788:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L785
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L788
.L786:
	movq	-760(%rbp), %r13
.L784:
	testq	%r13, %r13
	je	.L789
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L789:
	movq	-2024(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-928(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L790
	call	_ZdlPv@PLT
.L790:
	movq	-944(%rbp), %rbx
	movq	-952(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L791
	.p2align 4,,10
	.p2align 3
.L795:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L792
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L795
.L793:
	movq	-952(%rbp), %r13
.L791:
	testq	%r13, %r13
	je	.L796
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L796:
	movq	-2008(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L797
	call	_ZdlPv@PLT
.L797:
	movq	-1136(%rbp), %rbx
	movq	-1144(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L798
	.p2align 4,,10
	.p2align 3
.L802:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L799
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L802
.L800:
	movq	-1144(%rbp), %r13
.L798:
	testq	%r13, %r13
	je	.L803
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L803:
	movq	-1992(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L804
	call	_ZdlPv@PLT
.L804:
	movq	-1328(%rbp), %rbx
	movq	-1336(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L805
	.p2align 4,,10
	.p2align 3
.L809:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L806
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L809
.L807:
	movq	-1336(%rbp), %r13
.L805:
	testq	%r13, %r13
	je	.L810
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L810:
	movq	-2016(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1504(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L811
	call	_ZdlPv@PLT
.L811:
	movq	-1520(%rbp), %rbx
	movq	-1528(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L812
	.p2align 4,,10
	.p2align 3
.L816:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L813
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L816
.L814:
	movq	-1528(%rbp), %r13
.L812:
	testq	%r13, %r13
	je	.L817
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L817:
	movq	-2000(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1696(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L818
	call	_ZdlPv@PLT
.L818:
	movq	-1712(%rbp), %rbx
	movq	-1720(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L819
	.p2align 4,,10
	.p2align 3
.L823:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L820
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L823
.L821:
	movq	-1720(%rbp), %r13
.L819:
	testq	%r13, %r13
	je	.L824
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L824:
	movq	-1976(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1888(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L825
	call	_ZdlPv@PLT
.L825:
	movq	-1904(%rbp), %rbx
	movq	-1912(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L826
	.p2align 4,,10
	.p2align 3
.L830:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L827
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L830
.L828:
	movq	-1912(%rbp), %r13
.L826:
	testq	%r13, %r13
	je	.L831
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L831:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L951
	addq	$2120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L827:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L830
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L820:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L823
	jmp	.L821
	.p2align 4,,10
	.p2align 3
.L813:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L816
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L806:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L809
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L799:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L802
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L792:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L795
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L785:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L788
	jmp	.L786
	.p2align 4,,10
	.p2align 3
.L771:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L774
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L778:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L781
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L943:
	movq	-1976(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r9d
	movq	%r12, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	3(%rax), %rdx
	movb	$6, 2(%rax)
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L745
	call	_ZdlPv@PLT
.L745:
	movq	(%rbx), %rax
	movl	$37, %edx
	movq	%r15, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rax
	movq	%rsi, -2056(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -2112(%rbp)
	movq	%rax, -2064(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$8, %esi
	movq	%r15, %rdi
	movq	%rax, -2072(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-2056(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$55, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2064(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$56, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -2048(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$37, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2048(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -2096(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2096(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-2072(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	-2064(%rbp), %xmm6
	movq	-2056(%rbp), %xmm5
	movl	$112, %edi
	movq	%r12, -104(%rbp)
	movq	%rax, -160(%rbp)
	movq	-2048(%rbp), %rax
	movdqa	%xmm6, %xmm3
	movq	-2112(%rbp), %xmm4
	punpcklqdq	%xmm5, %xmm3
	movq	%xmm6, -144(%rbp)
	movq	%rax, -128(%rbp)
	movq	-2072(%rbp), %rax
	punpcklqdq	%xmm5, %xmm4
	movaps	%xmm3, -2096(%rbp)
	movq	%rax, -112(%rbp)
	movq	-2048(%rbp), %rax
	movaps	%xmm4, -2112(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movq	%xmm6, -136(%rbp)
	movq	%xmm5, -120(%rbp)
	movaps	%xmm0, -1952(%rbp)
	movq	%r12, -152(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	$0, -1936(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-176(%rbp), %xmm7
	leaq	112(%rax), %rdx
	leaq	-1728(%rbp), %rdi
	movups	%xmm6, (%rax)
	movdqa	-160(%rbp), %xmm6
	movups	%xmm7, 16(%rax)
	movdqa	-144(%rbp), %xmm7
	movups	%xmm6, 32(%rax)
	movdqa	-96(%rbp), %xmm6
	movups	%xmm7, 48(%rax)
	movups	%xmm5, 64(%rax)
	movups	%xmm2, 80(%rax)
	movups	%xmm6, 96(%rax)
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L746
	call	_ZdlPv@PLT
.L746:
	movq	-2072(%rbp), %xmm1
	movq	%r12, %xmm7
	movq	-2064(%rbp), %xmm0
	movl	$112, %edi
	movdqa	-2112(%rbp), %xmm5
	movdqa	-2096(%rbp), %xmm2
	movq	$0, -1936(%rbp)
	punpcklqdq	%xmm7, %xmm1
	punpcklqdq	%xmm0, %xmm0
	movq	-2048(%rbp), %xmm7
	movaps	%xmm0, -144(%rbp)
	movdqa	%xmm7, %xmm0
	movaps	%xmm5, -192(%rbp)
	movhps	-2056(%rbp), %xmm0
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm0, -128(%rbp)
	movdqa	%xmm7, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm1
	movdqa	-128(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	leaq	112(%rax), %rdx
	leaq	-1536(%rbp), %rdi
	movdqa	-176(%rbp), %xmm6
	movdqa	-160(%rbp), %xmm7
	movups	%xmm1, (%rax)
	movdqa	-144(%rbp), %xmm1
	movups	%xmm3, 64(%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm1, 48(%rax)
	movups	%xmm4, 80(%rax)
	movups	%xmm5, 96(%rax)
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L747
	call	_ZdlPv@PLT
.L747:
	movq	-2016(%rbp), %rcx
	movq	-2000(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1664(%rbp)
	je	.L748
.L944:
	movq	-2000(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-1728(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$14, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movl	$1285, %r8d
	movq	%r12, %rdi
	movq	%r13, %rsi
	movabsq	$434039933238642439, %rcx
	movl	$84215557, 8(%rax)
	leaq	14(%rax), %rdx
	movq	%rcx, (%rax)
	movw	%r8w, 12(%rax)
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L749
	call	_ZdlPv@PLT
.L749:
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	(%rax), %rcx
	movq	24(%rax), %rbx
	movq	88(%rax), %r10
	movq	%rsi, -2064(%rbp)
	movq	16(%rax), %rsi
	movq	96(%rax), %r11
	movq	%rdx, -2080(%rbp)
	movq	%rdi, -2128(%rbp)
	movq	48(%rax), %rdx
	movq	64(%rax), %rdi
	movq	%rcx, -2056(%rbp)
	movq	%rbx, -2096(%rbp)
	movq	72(%rax), %rcx
	movq	32(%rax), %rbx
	movq	%rsi, -2072(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rdx, -2120(%rbp)
	movl	$39, %edx
	movq	80(%rax), %r12
	movq	%rdi, -2136(%rbp)
	movq	%r15, %rdi
	movq	%r10, -2144(%rbp)
	movq	%r11, -2152(%rbp)
	movq	%rcx, -2048(%rbp)
	movq	%rbx, -2112(%rbp)
	movq	104(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -2160(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2160(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrMulENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, -2160(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2160(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, -2160(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$38, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2160(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	-2048(%rbp), %rdx
	call	_ZN2v88internal45UnsafeNewReference20UT5ATSmi10HeapObject_1411EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%rbx, %xmm2
	movl	$128, %edi
	movq	-2056(%rbp), %xmm0
	movdqa	-1952(%rbp), %xmm6
	movq	$0, -1936(%rbp)
	movhps	-2064(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-2072(%rbp), %xmm0
	movaps	%xmm6, -80(%rbp)
	movhps	-2096(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-2112(%rbp), %xmm0
	movhps	-2080(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-2120(%rbp), %xmm0
	movhps	-2128(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-2136(%rbp), %xmm0
	movhps	-2048(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%r12, %xmm0
	movhps	-2144(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-2152(%rbp), %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-176(%rbp), %xmm1
	movdqa	-160(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm4
	leaq	128(%rax), %rdx
	leaq	-1344(%rbp), %rdi
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm2
	movups	%xmm7, (%rax)
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movups	%xmm1, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm5, 64(%rax)
	movups	%xmm2, 80(%rax)
	movups	%xmm6, 96(%rax)
	movups	%xmm7, 112(%rax)
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L750
	call	_ZdlPv@PLT
.L750:
	movq	-1992(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1472(%rbp)
	je	.L751
.L945:
	movq	-2016(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-1536(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$14, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movl	$1285, %edi
	movq	%r13, %rsi
	movabsq	$434039933238642439, %rcx
	movw	%di, 12(%rax)
	leaq	14(%rax), %rdx
	movq	%r12, %rdi
	movq	%rcx, (%rax)
	movl	$84215557, 8(%rax)
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L752
	call	_ZdlPv@PLT
.L752:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rbx
	movq	8(%rax), %rsi
	movq	56(%rax), %rdx
	movq	(%rax), %rcx
	movq	%rbx, -2072(%rbp)
	movq	40(%rax), %rbx
	movq	%rsi, -2056(%rbp)
	movq	16(%rax), %rsi
	movq	%rbx, -2096(%rbp)
	movq	48(%rax), %rbx
	movq	%rsi, -2064(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	32(%rax), %r12
	movq	%rdx, -2080(%rbp)
	movl	$41, %edx
	movq	%rcx, -2048(%rbp)
	movq	%rbx, -2112(%rbp)
	movq	64(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$72, %edi
	movq	-2048(%rbp), %xmm0
	movq	$0, -1936(%rbp)
	movq	%rbx, -128(%rbp)
	movhps	-2056(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-2064(%rbp), %xmm0
	movhps	-2072(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	%r12, %xmm0
	movhps	-2096(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-2112(%rbp), %xmm0
	movhps	-2080(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-192(%rbp), %xmm1
	movdqa	-176(%rbp), %xmm3
	movdqa	-160(%rbp), %xmm4
	leaq	72(%rax), %rdx
	leaq	-1152(%rbp), %rdi
	movdqa	-144(%rbp), %xmm5
	movq	%rcx, 64(%rax)
	movups	%xmm1, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L753
	call	_ZdlPv@PLT
.L753:
	movq	-2008(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1280(%rbp)
	je	.L754
.L946:
	movq	-1992(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$362263814143805189, %rbx
	leaq	-1344(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	movabsq	$434039933238642439, %rcx
	movq	%rbx, 8(%rax)
	leaq	16(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L755
	call	_ZdlPv@PLT
.L755:
	movq	(%rbx), %rax
	movq	40(%rax), %rdx
	movq	8(%rax), %rsi
	movq	24(%rax), %rbx
	movq	112(%rax), %rdi
	movq	(%rax), %rcx
	movq	48(%rax), %r12
	movq	%rdx, -2112(%rbp)
	movq	56(%rax), %rdx
	movq	%rsi, -2056(%rbp)
	movq	16(%rax), %rsi
	movq	%rbx, -2072(%rbp)
	movq	%rdx, -2080(%rbp)
	movq	64(%rax), %rdx
	movq	32(%rax), %rbx
	movq	%rsi, -2064(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rdx, -2120(%rbp)
	movl	$56, %edx
	movq	%rdi, -2128(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -2048(%rbp)
	movq	%rbx, -2096(%rbp)
	movq	120(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$88, %edi
	movq	-2048(%rbp), %xmm0
	movq	$0, -1936(%rbp)
	movq	%rbx, -112(%rbp)
	movhps	-2056(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-2064(%rbp), %xmm0
	movhps	-2072(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-2096(%rbp), %xmm0
	movhps	-2112(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%r12, %xmm0
	movhps	-2080(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-2120(%rbp), %xmm0
	movhps	-2128(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-192(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm3
	leaq	88(%rax), %rdx
	leaq	-960(%rbp), %rdi
	movdqa	-176(%rbp), %xmm6
	movdqa	-160(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm1
	movq	%rcx, 80(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm1, 48(%rax)
	movups	%xmm3, 64(%rax)
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L756
	call	_ZdlPv@PLT
.L756:
	movq	-2024(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1088(%rbp)
	je	.L757
.L947:
	movq	-2008(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-1152(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	movabsq	$434039933238642439, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$5, 8(%rax)
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L758
	call	_ZdlPv@PLT
.L758:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -896(%rbp)
	je	.L759
.L948:
	movq	-2024(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-960(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$11, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movl	$1797, %esi
	movq	%r12, %rdi
	movabsq	$434039933238642439, %rcx
	movw	%si, 8(%rax)
	leaq	11(%rax), %rdx
	movq	%r13, %rsi
	movq	%rcx, (%rax)
	movb	$5, 10(%rax)
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L760
	call	_ZdlPv@PLT
.L760:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	48(%rax), %rdi
	movq	16(%rax), %r11
	movq	24(%rax), %r10
	movq	32(%rax), %r9
	movq	40(%rax), %r8
	movq	56(%rax), %rsi
	movq	%rdi, -144(%rbp)
	movl	$80, %edi
	movq	72(%rax), %rcx
	movq	80(%rax), %rdx
	movq	%r11, -176(%rbp)
	movq	8(%rax), %rbx
	movq	(%rax), %rax
	movq	%r10, -168(%rbp)
	movq	%r9, -160(%rbp)
	movq	%r8, -152(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movaps	%xmm0, -1952(%rbp)
	movq	%rax, -192(%rbp)
	movq	%rbx, -184(%rbp)
	movq	$0, -1936(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-176(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm2
	leaq	80(%rax), %rdx
	leaq	-768(%rbp), %rdi
	movdqa	-144(%rbp), %xmm6
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L761
	call	_ZdlPv@PLT
.L761:
	movq	-2032(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -704(%rbp)
	je	.L762
.L949:
	movq	-2032(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-768(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	movabsq	$434039933238642439, %rcx
	movq	%rcx, (%rax)
	movl	$1287, %ecx
	leaq	10(%rax), %rdx
	movw	%cx, 8(%rax)
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L763
	call	_ZdlPv@PLT
.L763:
	movq	(%rbx), %rax
	movl	$37, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	movq	8(%rax), %rcx
	movq	64(%rax), %r8
	movq	72(%rax), %r9
	movq	(%rax), %rbx
	movq	%rcx, -2048(%rbp)
	movq	16(%rax), %r12
	movq	%r8, -2064(%rbp)
	movq	%r9, -2056(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-2056(%rbp), %r9
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-2064(%rbp), %r8
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6ObjectEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -2056(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-2056(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal118UnsafeCast100UT8ATBigInt7ATFalse6ATNull5ATSmi9ATTheHole6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_1440EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	movl	$38, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -2056(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2056(%rbp), %r8
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal31ReplaceTheHoleWithUndefined_255EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %xmm0
	movl	$32, %edi
	movq	%r12, -176(%rbp)
	movhps	-2048(%rbp), %xmm0
	movq	%rax, -168(%rbp)
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1952(%rbp)
	movq	$0, -1936(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm4
	movdqa	-176(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-576(%rbp), %rdi
	movq	%rax, -1952(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L764
	call	_ZdlPv@PLT
.L764:
	movq	-2040(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L950:
	movq	-2040(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-576(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	movl	$134612743, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L766
	call	_ZdlPv@PLT
.L766:
	movq	(%rbx), %rax
	movl	$35, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	movq	(%rax), %r12
	movq	8(%rax), %rcx
	movq	16(%rax), %rbx
	movq	24(%rax), %r14
	movq	%rcx, -2048(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %xmm0
	movq	%r14, %xmm1
	movl	$32, %edi
	movhps	-2048(%rbp), %xmm0
	leaq	-384(%rbp), %r12
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -192(%rbp)
	movq	%rbx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -176(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm3
	movq	%r12, %rdi
	movq	%r13, %rsi
	movdqa	-176(%rbp), %xmm2
	leaq	32(%rax), %rdx
	movq	%rax, -1952(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L767
	call	_ZdlPv@PLT
.L767:
	movq	-1984(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L765
.L951:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22429:
	.size	_ZN2v88internal25LoadElementOrUndefined_51EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_3SmiEEE, .-_ZN2v88internal25LoadElementOrUndefined_51EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_3SmiEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal30EnsureWriteableFastElements_50EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal30EnsureWriteableFastElements_50EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEE, @function
_GLOBAL__sub_I__ZN2v88internal30EnsureWriteableFastElements_50EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEE:
.LFB29337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29337:
	.size	_GLOBAL__sub_I__ZN2v88internal30EnsureWriteableFastElements_50EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEE, .-_GLOBAL__sub_I__ZN2v88internal30EnsureWriteableFastElements_50EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal30EnsureWriteableFastElements_50EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEE
	.weak	_ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE:
	.byte	8
	.byte	7
	.weak	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE:
	.byte	7
	.byte	7
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
