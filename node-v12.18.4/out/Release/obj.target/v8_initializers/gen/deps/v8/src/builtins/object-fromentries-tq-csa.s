	.file	"object-fromentries-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0:
.LFB29759:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	%rsi, %r13
	pushq	%rbx
	movq	%r13, %rax
	sarq	$3, %rax
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%r13, %r13
	js	.L14
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%rdx, %r14
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.L9
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%rax, %rdi
.L9:
	leaq	(%rdi,%r13), %r15
	movq	%rdi, (%rbx)
	movq	%r15, 16(%rbx)
	cmpq	%r12, %r14
	je	.L10
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
.L10:
	movq	%r15, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29759:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0:
.LFB29758:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	subq	%rsi, %rdx
	js	.L21
	movq	%rdi, %rbx
	je	.L17
	movq	%rdx, %rdi
	movq	%rdx, %r12
	movq	%rsi, %r13
	call	_Znwm@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	leaq	(%rax,%r12), %r14
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	%r14, 16(%rbx)
	call	memcpy@PLT
	movq	%r14, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	xorl	%r14d, %r14d
	movq	$0, (%rdi)
	movq	$0, 16(%rdi)
	movq	%r14, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29758:
	.size	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev,"axG",@progbits,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev:
.LFB22413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$56, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L23
	call	_ZdlPv@PLT
.L23:
	movq	16(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	%r12, %r13
	je	.L24
	.p2align 4,,10
	.p2align 3
.L28:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L25
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L28
.L26:
	movq	8(%rbx), %r12
.L24:
	testq	%r12, %r12
	je	.L22
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L28
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L22:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22413:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev
	.set	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.section	.rodata._ZN2v88internal45Cast34ATFastJSArrayWithNoCustomIteration_1463EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../../deps/v8/../../deps/v8/src/builtins/base.tq"
	.align 8
.LC3:
	.string	"../../deps/v8/../../deps/v8/src/builtins/object-fromentries.tq"
	.section	.text._ZN2v88internal45Cast34ATFastJSArrayWithNoCustomIteration_1463EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal45Cast34ATFastJSArrayWithNoCustomIteration_1463EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal45Cast34ATFastJSArrayWithNoCustomIteration_1463EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal45Cast34ATFastJSArrayWithNoCustomIteration_1463EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE:
.LFB22520:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	leaq	-1632(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1760(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1800(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1864, %rsp
	movq	%rsi, -1888(%rbp)
	movq	%rdx, -1904(%rbp)
	movq	%rcx, -1872(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1800(%rbp)
	movq	%rdi, -1632(%rbp)
	movl	$48, %edi
	movq	$0, -1624(%rbp)
	movq	$0, -1616(%rbp)
	movq	$0, -1608(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1624(%rbp)
	leaq	-1576(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1608(%rbp)
	movq	%rdx, -1616(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1592(%rbp)
	movq	%rax, -1816(%rbp)
	movq	$0, -1600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1432(%rbp)
	movq	$0, -1424(%rbp)
	movq	%rax, -1440(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1432(%rbp)
	leaq	-1384(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -1832(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1864(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$96, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1840(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1848(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	$0, -472(%rbp)
	movq	%rax, -480(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1824(%rbp)
	movq	$0, -464(%rbp)
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	$0, -440(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1888(%rbp), %xmm1
	movaps	%xmm0, -1760(%rbp)
	movhps	-1904(%rbp), %xmm1
	movq	$0, -1744(%rbp)
	movaps	%xmm1, -1888(%rbp)
	call	_Znwm@PLT
	movdqa	-1888(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L35
	call	_ZdlPv@PLT
.L35:
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1568(%rbp)
	jne	.L219
	cmpq	$0, -1376(%rbp)
	jne	.L220
.L41:
	cmpq	$0, -1184(%rbp)
	jne	.L221
.L44:
	cmpq	$0, -992(%rbp)
	jne	.L222
.L49:
	cmpq	$0, -800(%rbp)
	jne	.L223
.L52:
	cmpq	$0, -608(%rbp)
	leaq	-288(%rbp), %r15
	jne	.L224
	cmpq	$0, -416(%rbp)
	jne	.L225
.L58:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L60
	call	_ZdlPv@PLT
.L60:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L61
	call	_ZdlPv@PLT
.L61:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L62
	.p2align 4,,10
	.p2align 3
.L66:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L63
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L66
.L64:
	movq	-280(%rbp), %r14
.L62:
	testq	%r14, %r14
	je	.L67
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L67:
	movq	-1824(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L68
	call	_ZdlPv@PLT
.L68:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L69
	.p2align 4,,10
	.p2align 3
.L73:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L70
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L73
.L71:
	movq	-472(%rbp), %r14
.L69:
	testq	%r14, %r14
	je	.L74
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L74:
	movq	-1848(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L75
	call	_ZdlPv@PLT
.L75:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L76
	.p2align 4,,10
	.p2align 3
.L80:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L77
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L80
.L78:
	movq	-664(%rbp), %r14
.L76:
	testq	%r14, %r14
	je	.L81
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L81:
	movq	-1840(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L82
	call	_ZdlPv@PLT
.L82:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L83
	.p2align 4,,10
	.p2align 3
.L87:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L84
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L87
.L85:
	movq	-856(%rbp), %r14
.L83:
	testq	%r14, %r14
	je	.L88
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L88:
	movq	-1864(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L89
	call	_ZdlPv@PLT
.L89:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L90
	.p2align 4,,10
	.p2align 3
.L94:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L91
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L94
.L92:
	movq	-1048(%rbp), %r14
.L90:
	testq	%r14, %r14
	je	.L95
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L95:
	movq	-1832(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L96
	call	_ZdlPv@PLT
.L96:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L97
	.p2align 4,,10
	.p2align 3
.L101:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L98
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L101
.L99:
	movq	-1240(%rbp), %r14
.L97:
	testq	%r14, %r14
	je	.L102
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L102:
	movq	-1856(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L103
	call	_ZdlPv@PLT
.L103:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L104
	.p2align 4,,10
	.p2align 3
.L108:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L105
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L108
.L106:
	movq	-1432(%rbp), %r14
.L104:
	testq	%r14, %r14
	je	.L109
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L109:
	movq	-1816(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L110
	call	_ZdlPv@PLT
.L110:
	movq	-1616(%rbp), %rbx
	movq	-1624(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L111
	.p2align 4,,10
	.p2align 3
.L115:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L112
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L115
.L113:
	movq	-1624(%rbp), %r14
.L111:
	testq	%r14, %r14
	je	.L116
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L116:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L226
	addq	$1864, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L115
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L105:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L108
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L98:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L101
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L91:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L94
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L84:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L87
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L77:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L80
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L63:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L66
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L70:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L73
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L219:
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r9d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L37
	call	_ZdlPv@PLT
.L37:
	movq	(%r15), %rax
	movl	$2026, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	leaq	-1792(%rbp), %r15
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rcx, -1904(%rbp)
	movq	%rax, -1888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1888(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1888(%rbp), %rax
	movl	$32, %edi
	movq	-1904(%rbp), %xmm2
	movq	$0, -1776(%rbp)
	movq	%rax, %xmm0
	movq	%rax, %xmm6
	punpcklqdq	%xmm0, %xmm0
	punpcklqdq	%xmm6, %xmm2
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -1904(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -1792(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movq	%r15, %rsi
	leaq	32(%rax), %rdx
	leaq	-1248(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L38
	call	_ZdlPv@PLT
.L38:
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1752(%rbp)
	jne	.L227
.L39:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1376(%rbp)
	je	.L41
.L220:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1440(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r8d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L42
	call	_ZdlPv@PLT
.L42:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-480(%rbp), %rdi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L43
	call	_ZdlPv@PLT
.L43:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1184(%rbp)
	je	.L44
.L221:
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1248(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L45
	call	_ZdlPv@PLT
.L45:
	movq	(%r15), %rax
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	24(%rax), %rax
	movq	%rcx, -1904(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -1888(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r13, %rcx
	movq	%rbx, %rdi
	movq	-1888(%rbp), %rdx
	call	_ZN2v88internal44Cast34ATFastJSArrayWithNoCustomIteration_138EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rcx
	movq	%r15, %xmm3
	pxor	%xmm0, %xmm0
	movhps	-1904(%rbp), %xmm3
	movl	$32, %edi
	movq	%rax, -72(%rbp)
	leaq	-1792(%rbp), %r15
	movq	%rcx, -80(%rbp)
	movaps	%xmm3, -1904(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -1792(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	32(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L46
	call	_ZdlPv@PLT
.L46:
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1752(%rbp)
	jne	.L228
.L47:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -992(%rbp)
	je	.L49
.L222:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rdi
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L50
	call	_ZdlPv@PLT
.L50:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-480(%rbp), %rdi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L51
	call	_ZdlPv@PLT
.L51:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L52
.L223:
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L53
	call	_ZdlPv@PLT
.L53:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	24(%rax), %rdx
	movdqu	(%rax), %xmm4
	movaps	%xmm0, -1760(%rbp)
	movq	$0, -1744(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm4, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -1760(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L54
	call	_ZdlPv@PLT
.L54:
	movq	-1848(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L224:
	movq	-1848(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%r15, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L56
	call	_ZdlPv@PLT
.L56:
	movq	(%rbx), %rax
	movl	$10, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r15
	movq	%rcx, -1888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r15, -80(%rbp)
	movhps	-1888(%rbp), %xmm0
	leaq	-288(%rbp), %r15
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm6
	leaq	24(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L57
	call	_ZdlPv@PLT
.L57:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -416(%rbp)
	je	.L58
.L225:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L59
	call	_ZdlPv@PLT
.L59:
	movq	-1872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L227:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1904(%rbp), %xmm7
	movl	$24, %edi
	movaps	%xmm0, -1792(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-1440(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L40
	call	_ZdlPv@PLT
.L40:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L228:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1904(%rbp), %xmm7
	movl	$24, %edi
	movaps	%xmm0, -1792(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-1056(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L48
	call	_ZdlPv@PLT
.L48:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L47
.L226:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22520:
	.size	_ZN2v88internal45Cast34ATFastJSArrayWithNoCustomIteration_1463EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, .-_ZN2v88internal45Cast34ATFastJSArrayWithNoCustomIteration_1463EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal111Is10JSReceiver90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_1464EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal111Is10JSReceiver90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_1464EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal111Is10JSReceiver90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_1464EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal111Is10JSReceiver90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_1464EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE:
.LFB22533:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1384(%rbp), %r14
	leaq	-232(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1568(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1608(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1440(%rbp), %rbx
	subq	$1656, %rsp
	movq	%rdi, -1664(%rbp)
	movq	%rsi, -1680(%rbp)
	movq	%rdx, -1696(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1608(%rbp)
	movq	%rdi, -1440(%rbp)
	movl	$48, %edi
	movq	$0, -1432(%rbp)
	movq	$0, -1424(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -1432(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1608(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -1656(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1608(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1640(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1608(%rbp), %rax
	movl	$48, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1632(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1608(%rbp), %rax
	movl	$72, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1648(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1608(%rbp), %rax
	movl	$72, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -1624(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1608(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1680(%rbp), %xmm1
	movaps	%xmm0, -1568(%rbp)
	movhps	-1696(%rbp), %xmm1
	movq	$0, -1552(%rbp)
	movaps	%xmm1, -1680(%rbp)
	call	_Znwm@PLT
	movdqa	-1680(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1568(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L230
	call	_ZdlPv@PLT
.L230:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1376(%rbp)
	jne	.L391
	cmpq	$0, -1184(%rbp)
	jne	.L392
.L236:
	cmpq	$0, -992(%rbp)
	jne	.L393
.L239:
	cmpq	$0, -800(%rbp)
	jne	.L394
.L242:
	cmpq	$0, -608(%rbp)
	jne	.L395
.L245:
	cmpq	$0, -416(%rbp)
	leaq	-288(%rbp), %rbx
	jne	.L396
.L248:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$4, 2(%rax)
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L251
	call	_ZdlPv@PLT
.L251:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L252
	call	_ZdlPv@PLT
.L252:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L253
	.p2align 4,,10
	.p2align 3
.L257:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L254
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L257
.L255:
	movq	-280(%rbp), %r15
.L253:
	testq	%r15, %r15
	je	.L258
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L258:
	movq	-1624(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L259
	call	_ZdlPv@PLT
.L259:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L260
	.p2align 4,,10
	.p2align 3
.L264:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L261
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L264
.L262:
	movq	-472(%rbp), %r15
.L260:
	testq	%r15, %r15
	je	.L265
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L265:
	movq	-1648(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L266
	call	_ZdlPv@PLT
.L266:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L267
	.p2align 4,,10
	.p2align 3
.L271:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L268
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L271
.L269:
	movq	-664(%rbp), %r15
.L267:
	testq	%r15, %r15
	je	.L272
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L272:
	movq	-1632(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L273
	call	_ZdlPv@PLT
.L273:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L274
	.p2align 4,,10
	.p2align 3
.L278:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L275
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L278
.L276:
	movq	-856(%rbp), %r15
.L274:
	testq	%r15, %r15
	je	.L279
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L279:
	movq	-1640(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L280
	call	_ZdlPv@PLT
.L280:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L281
	.p2align 4,,10
	.p2align 3
.L285:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L282
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L285
.L283:
	movq	-1048(%rbp), %r15
.L281:
	testq	%r15, %r15
	je	.L286
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L286:
	movq	-1656(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L287
	call	_ZdlPv@PLT
.L287:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L288
	.p2align 4,,10
	.p2align 3
.L292:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L289
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L292
.L290:
	movq	-1240(%rbp), %r15
.L288:
	testq	%r15, %r15
	je	.L293
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L293:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L294
	call	_ZdlPv@PLT
.L294:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L295
	.p2align 4,,10
	.p2align 3
.L299:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L296
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L299
.L297:
	movq	-1432(%rbp), %r14
.L295:
	testq	%r14, %r14
	je	.L300
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L300:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L397
	addq	$1656, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L296:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L299
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L289:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L292
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L282:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L285
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L275:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L278
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L268:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L271
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L254:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L257
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L261:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L264
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L391:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r10d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r10w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L232
	call	_ZdlPv@PLT
.L232:
	movq	(%rbx), %rax
	movl	$2784, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	%rcx, -1680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1680(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rcx
	movq	-1664(%rbp), %rdi
	call	_ZN2v88internal21Cast10JSReceiver_1404EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1680(%rbp), %rcx
	movq	%rbx, %xmm2
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	%rax, -72(%rbp)
	leaq	-1600(%rbp), %rbx
	movq	%rcx, %xmm7
	movq	%rcx, -80(%rbp)
	punpcklqdq	%xmm7, %xmm2
	movaps	%xmm0, -1600(%rbp)
	movaps	%xmm2, -1696(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	$0, -1584(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movq	%rbx, %rsi
	leaq	32(%rax), %rdx
	leaq	-1056(%rbp), %rdi
	movq	%rax, -1600(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L233
	call	_ZdlPv@PLT
.L233:
	movq	-1640(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1560(%rbp)
	jne	.L398
.L234:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1184(%rbp)
	je	.L236
.L392:
	movq	-1656(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1248(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r9d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L237
	call	_ZdlPv@PLT
.L237:
	movq	(%rbx), %rax
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movdqu	(%rax), %xmm0
	movaps	%xmm1, -1568(%rbp)
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1680(%rbp)
	call	_Znwm@PLT
	movdqa	-1680(%rbp), %xmm0
	leaq	-864(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1568(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L238
	call	_ZdlPv@PLT
.L238:
	movq	-1632(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -992(%rbp)
	je	.L239
.L393:
	movq	-1640(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L240
	call	_ZdlPv@PLT
.L240:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	24(%rax), %rdx
	movdqu	(%rax), %xmm5
	movaps	%xmm0, -1568(%rbp)
	movq	$0, -1552(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm5, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -1568(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L241
	call	_ZdlPv@PLT
.L241:
	movq	-1648(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L242
.L394:
	movq	-1632(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r8d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L243
	call	_ZdlPv@PLT
.L243:
	movq	(%rbx), %rax
	movq	-1664(%rbp), %rdi
	xorl	%esi, %esi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -1680(%rbp)
	call	_ZN2v88internal42FromConstexpr6ATbool16ATconstexpr_bool_165EPNS0_8compiler18CodeAssemblerStateEb@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	$0, -1552(%rbp)
	movhps	-1680(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -1568(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L244
	call	_ZdlPv@PLT
.L244:
	movq	-1624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -608(%rbp)
	je	.L245
.L395:
	movq	-1648(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%rbx, %rdi
	movb	$7, 2(%rax)
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L246
	call	_ZdlPv@PLT
.L246:
	movq	(%rbx), %rax
	movl	$2785, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -1680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1664(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal42FromConstexpr6ATbool16ATconstexpr_bool_165EPNS0_8compiler18CodeAssemblerStateEb@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	$0, -1552(%rbp)
	movhps	-1680(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm3
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -1568(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L247
	call	_ZdlPv@PLT
.L247:
	movq	-1624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L396:
	movq	-1624(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$4, 2(%rax)
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L249
	call	_ZdlPv@PLT
.L249:
	movq	(%rbx), %rax
	movl	$24, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -1696(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%rcx, -1680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %edi
	movq	%rbx, -80(%rbp)
	movq	-1680(%rbp), %xmm0
	movq	$0, -1552(%rbp)
	leaq	-288(%rbp), %rbx
	movhps	-1696(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm4
	leaq	24(%rax), %rdx
	movq	%rax, -1568(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L250
	call	_ZdlPv@PLT
.L250:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L398:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1680(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1696(%rbp), %xmm5
	movl	$24, %edi
	movaps	%xmm0, -1600(%rbp)
	movaps	%xmm5, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1584(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%rbx, %rsi
	leaq	24(%rax), %rdx
	leaq	-1248(%rbp), %rdi
	movq	%rax, -1600(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L235
	call	_ZdlPv@PLT
.L235:
	movq	-1656(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L234
.L397:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22533:
	.size	_ZN2v88internal111Is10JSReceiver90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_1464EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal111Is10JSReceiver90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_1464EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_10FixedArrayENS0_3SmiENS0_8JSObjectES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESK_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_10FixedArrayENS0_3SmiENS0_8JSObjectES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESK_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_10FixedArrayENS0_3SmiENS0_8JSObjectES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESK_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_10FixedArrayENS0_3SmiENS0_8JSObjectES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESK_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_10FixedArrayENS0_3SmiENS0_8JSObjectES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESK_:
.LFB26867:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$8, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$434322516317046791, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L400
	movq	%rax, -120(%rbp)
	call	_ZdlPv@PLT
	movq	-120(%rbp), %rax
.L400:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L401
	movq	%rdx, (%r15)
.L401:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L402
	movq	%rdx, (%r14)
.L402:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L403
	movq	%rdx, 0(%r13)
.L403:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L404
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L404:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L405
	movq	%rdx, (%rbx)
.L405:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L406
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L406:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L407
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L407:
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L399
	movq	-112(%rbp), %rbx
	movq	%rax, (%rbx)
.L399:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L438
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L438:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26867:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_10FixedArrayENS0_3SmiENS0_8JSObjectES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESK_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_10FixedArrayENS0_3SmiENS0_8JSObjectES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESK_
	.section	.text._ZN2v88internal29ObjectFromEntriesFastCase_310EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29ObjectFromEntriesFastCase_310EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal29ObjectFromEntriesFastCase_310EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal29ObjectFromEntriesFastCase_310EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE:
.LFB22410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-3664(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-3552(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-3424(%rbp), %r12
	pushq	%rbx
	subq	$3912, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -3824(%rbp)
	movq	%rdx, -3840(%rbp)
	movq	%rcx, -3808(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -3664(%rbp)
	movq	%rdi, -3424(%rbp)
	movl	$48, %edi
	movq	$0, -3416(%rbp)
	movq	$0, -3408(%rbp)
	movq	$0, -3400(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -3416(%rbp)
	leaq	-3368(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3400(%rbp)
	movq	%rdx, -3408(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3384(%rbp)
	movq	%rax, -3680(%rbp)
	movq	$0, -3392(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$96, %edi
	movq	$0, -3224(%rbp)
	movq	$0, -3216(%rbp)
	movq	%rax, -3232(%rbp)
	movq	$0, -3208(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -3224(%rbp)
	leaq	-3176(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3208(%rbp)
	movq	%rdx, -3216(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3192(%rbp)
	movq	%rax, -3784(%rbp)
	movq	$0, -3200(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$120, %edi
	movq	$0, -3032(%rbp)
	movq	$0, -3024(%rbp)
	movq	%rax, -3040(%rbp)
	movq	$0, -3016(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -3032(%rbp)
	leaq	-2984(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3016(%rbp)
	movq	%rdx, -3024(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3000(%rbp)
	movq	%rax, -3744(%rbp)
	movq	$0, -3008(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$120, %edi
	movq	$0, -2840(%rbp)
	movq	$0, -2832(%rbp)
	movq	%rax, -2848(%rbp)
	movq	$0, -2824(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -2840(%rbp)
	leaq	-2792(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2824(%rbp)
	movq	%rdx, -2832(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2808(%rbp)
	movq	%rax, -3800(%rbp)
	movq	$0, -2816(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2648(%rbp)
	movq	$0, -2640(%rbp)
	movq	%rax, -2656(%rbp)
	movq	$0, -2632(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2648(%rbp)
	leaq	-2600(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2632(%rbp)
	movq	%rdx, -2640(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2616(%rbp)
	movq	%rax, -3704(%rbp)
	movq	$0, -2624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$192, %edi
	movq	$0, -2456(%rbp)
	movq	$0, -2448(%rbp)
	movq	%rax, -2464(%rbp)
	movq	$0, -2440(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -2456(%rbp)
	leaq	-2408(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2440(%rbp)
	movq	%rdx, -2448(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2424(%rbp)
	movq	%rax, -3696(%rbp)
	movq	$0, -2432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$192, %edi
	movq	$0, -2264(%rbp)
	movq	$0, -2256(%rbp)
	movq	%rax, -2272(%rbp)
	movq	$0, -2248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -2264(%rbp)
	leaq	-2216(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2248(%rbp)
	movq	%rdx, -2256(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2232(%rbp)
	movq	%rax, -3736(%rbp)
	movq	$0, -2240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$240, %edi
	movq	$0, -2072(%rbp)
	movq	$0, -2064(%rbp)
	movq	%rax, -2080(%rbp)
	movq	$0, -2056(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -2072(%rbp)
	leaq	-2024(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2056(%rbp)
	movq	%rdx, -2064(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2040(%rbp)
	movq	%rax, -3792(%rbp)
	movq	$0, -2048(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$288, %edi
	movq	$0, -1880(%rbp)
	movq	$0, -1872(%rbp)
	movq	%rax, -1888(%rbp)
	movq	$0, -1864(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -1880(%rbp)
	leaq	-1832(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1864(%rbp)
	movq	%rdx, -1872(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1848(%rbp)
	movq	%rax, -3760(%rbp)
	movq	$0, -1856(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$264, %edi
	movq	$0, -1688(%rbp)
	movq	$0, -1680(%rbp)
	movq	%rax, -1696(%rbp)
	movq	$0, -1672(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -1688(%rbp)
	leaq	-1640(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1672(%rbp)
	movq	%rdx, -1680(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1656(%rbp)
	movq	%rax, -3712(%rbp)
	movq	$0, -1664(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$264, %edi
	movq	$0, -1496(%rbp)
	movq	$0, -1488(%rbp)
	movq	%rax, -1504(%rbp)
	movq	$0, -1480(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -1496(%rbp)
	leaq	-1448(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1480(%rbp)
	movq	%rdx, -1488(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1464(%rbp)
	movq	%rax, -3728(%rbp)
	movq	$0, -1472(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1304(%rbp)
	movq	$0, -1296(%rbp)
	movq	%rax, -1312(%rbp)
	movq	$0, -1288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1304(%rbp)
	leaq	-1256(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1288(%rbp)
	movq	%rdx, -1296(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1272(%rbp)
	movq	%rax, -3776(%rbp)
	movq	$0, -1280(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1112(%rbp)
	movq	$0, -1104(%rbp)
	movq	%rax, -1120(%rbp)
	movq	$0, -1096(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1112(%rbp)
	leaq	-1064(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1096(%rbp)
	movq	%rdx, -1104(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1080(%rbp)
	movq	%rax, -3752(%rbp)
	movq	$0, -1088(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$72, %edi
	movq	$0, -920(%rbp)
	movq	$0, -912(%rbp)
	movq	%rax, -928(%rbp)
	movq	$0, -904(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -920(%rbp)
	leaq	-872(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -904(%rbp)
	movq	%rdx, -912(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -888(%rbp)
	movq	%rax, -3768(%rbp)
	movq	$0, -896(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$72, %edi
	movq	$0, -728(%rbp)
	movq	$0, -720(%rbp)
	movq	%rax, -736(%rbp)
	movq	$0, -712(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -728(%rbp)
	leaq	-680(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -712(%rbp)
	movq	%rdx, -720(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -696(%rbp)
	movq	%rax, -3720(%rbp)
	movq	$0, -704(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r15, %rsi
	movq	$0, -536(%rbp)
	movq	%rax, -544(%rbp)
	leaq	-488(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -3672(%rbp)
	movq	$0, -528(%rbp)
	movq	$0, -520(%rbp)
	movq	$0, -512(%rbp)
	movq	$0, -504(%rbp)
	movq	$0, -496(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$72, %edi
	movq	$0, -344(%rbp)
	movq	$0, -336(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -328(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -344(%rbp)
	leaq	-296(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -328(%rbp)
	movq	%rdx, -336(%rbp)
	xorl	%edx, %edx
	movq	%rax, -3688(%rbp)
	movups	%xmm0, -312(%rbp)
	movq	$0, -320(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-3824(%rbp), %xmm1
	movaps	%xmm0, -3552(%rbp)
	movhps	-3840(%rbp), %xmm1
	movq	$0, -3536(%rbp)
	movaps	%xmm1, -3824(%rbp)
	call	_Znwm@PLT
	movdqa	-3824(%rbp), %xmm1
	movq	%r12, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -3552(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -3536(%rbp)
	movq	%rdx, -3544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L440
	call	_ZdlPv@PLT
.L440:
	movq	-3680(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3360(%rbp)
	jne	.L819
	cmpq	$0, -3168(%rbp)
	jne	.L820
.L446:
	cmpq	$0, -2976(%rbp)
	jne	.L821
.L449:
	cmpq	$0, -2784(%rbp)
	jne	.L822
.L454:
	cmpq	$0, -2592(%rbp)
	jne	.L823
.L457:
	cmpq	$0, -2400(%rbp)
	jne	.L824
.L460:
	cmpq	$0, -2208(%rbp)
	jne	.L825
.L463:
	cmpq	$0, -2016(%rbp)
	jne	.L826
.L467:
	cmpq	$0, -1824(%rbp)
	jne	.L827
.L470:
	cmpq	$0, -1632(%rbp)
	jne	.L828
.L474:
	cmpq	$0, -1440(%rbp)
	jne	.L829
.L477:
	cmpq	$0, -1248(%rbp)
	jne	.L830
.L480:
	cmpq	$0, -1056(%rbp)
	jne	.L831
.L482:
	cmpq	$0, -864(%rbp)
	jne	.L832
.L484:
	cmpq	$0, -672(%rbp)
	leaq	-352(%rbp), %r12
	jne	.L833
	cmpq	$0, -480(%rbp)
	jne	.L834
.L490:
	movq	-3688(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -3536(%rbp)
	movaps	%xmm0, -3552(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r12, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -3552(%rbp)
	movq	%rdx, -3536(%rbp)
	movq	%rdx, -3544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L492
	call	_ZdlPv@PLT
.L492:
	movq	(%rbx), %rax
	movq	-3688(%rbp), %rdi
	movq	16(%rax), %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-320(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L493
	call	_ZdlPv@PLT
.L493:
	movq	-336(%rbp), %rbx
	movq	-344(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L494
	.p2align 4,,10
	.p2align 3
.L498:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L495
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L498
.L496:
	movq	-344(%rbp), %r13
.L494:
	testq	%r13, %r13
	je	.L499
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L499:
	movq	-3672(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-512(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L500
	call	_ZdlPv@PLT
.L500:
	movq	-528(%rbp), %rbx
	movq	-536(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L501
	.p2align 4,,10
	.p2align 3
.L505:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L502
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L505
.L503:
	movq	-536(%rbp), %r13
.L501:
	testq	%r13, %r13
	je	.L506
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L506:
	movq	-3720(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L507
	call	_ZdlPv@PLT
.L507:
	movq	-720(%rbp), %rbx
	movq	-728(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L508
	.p2align 4,,10
	.p2align 3
.L512:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L509
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L512
.L510:
	movq	-728(%rbp), %r13
.L508:
	testq	%r13, %r13
	je	.L513
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L513:
	movq	-3768(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-896(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L514
	call	_ZdlPv@PLT
.L514:
	movq	-912(%rbp), %rbx
	movq	-920(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L515
	.p2align 4,,10
	.p2align 3
.L519:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L516
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L519
.L517:
	movq	-920(%rbp), %r13
.L515:
	testq	%r13, %r13
	je	.L520
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L520:
	movq	-3752(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L521
	call	_ZdlPv@PLT
.L521:
	movq	-1104(%rbp), %rbx
	movq	-1112(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L522
	.p2align 4,,10
	.p2align 3
.L526:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L523
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L526
.L524:
	movq	-1112(%rbp), %r13
.L522:
	testq	%r13, %r13
	je	.L527
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L527:
	movq	-3776(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L528
	call	_ZdlPv@PLT
.L528:
	movq	-1296(%rbp), %rbx
	movq	-1304(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L529
	.p2align 4,,10
	.p2align 3
.L533:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L530
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L533
.L531:
	movq	-1304(%rbp), %r13
.L529:
	testq	%r13, %r13
	je	.L534
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L534:
	movq	-3728(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1472(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L535
	call	_ZdlPv@PLT
.L535:
	movq	-1488(%rbp), %rbx
	movq	-1496(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L536
	.p2align 4,,10
	.p2align 3
.L540:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L537
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L540
.L538:
	movq	-1496(%rbp), %r13
.L536:
	testq	%r13, %r13
	je	.L541
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L541:
	movq	-3712(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L542
	call	_ZdlPv@PLT
.L542:
	movq	-1680(%rbp), %rbx
	movq	-1688(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L543
	.p2align 4,,10
	.p2align 3
.L547:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L544
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L547
.L545:
	movq	-1688(%rbp), %r13
.L543:
	testq	%r13, %r13
	je	.L548
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L548:
	movq	-3760(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1856(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L549
	call	_ZdlPv@PLT
.L549:
	movq	-1872(%rbp), %rbx
	movq	-1880(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L550
	.p2align 4,,10
	.p2align 3
.L554:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L551
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L554
.L552:
	movq	-1880(%rbp), %r13
.L550:
	testq	%r13, %r13
	je	.L555
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L555:
	movq	-3792(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L556
	call	_ZdlPv@PLT
.L556:
	movq	-2064(%rbp), %rbx
	movq	-2072(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L557
	.p2align 4,,10
	.p2align 3
.L561:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L558
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L561
.L559:
	movq	-2072(%rbp), %r13
.L557:
	testq	%r13, %r13
	je	.L562
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L562:
	movq	-3736(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L563
	call	_ZdlPv@PLT
.L563:
	movq	-2256(%rbp), %rbx
	movq	-2264(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L564
	.p2align 4,,10
	.p2align 3
.L568:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L565
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L568
.L566:
	movq	-2264(%rbp), %r13
.L564:
	testq	%r13, %r13
	je	.L569
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L569:
	movq	-3696(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L570
	call	_ZdlPv@PLT
.L570:
	movq	-2448(%rbp), %rbx
	movq	-2456(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L571
	.p2align 4,,10
	.p2align 3
.L575:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L572
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L575
.L573:
	movq	-2456(%rbp), %r13
.L571:
	testq	%r13, %r13
	je	.L576
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L576:
	movq	-3704(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L577
	call	_ZdlPv@PLT
.L577:
	movq	-2640(%rbp), %rbx
	movq	-2648(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L578
	.p2align 4,,10
	.p2align 3
.L582:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L579
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L582
.L580:
	movq	-2648(%rbp), %r13
.L578:
	testq	%r13, %r13
	je	.L583
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L583:
	movq	-3800(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2816(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L584
	call	_ZdlPv@PLT
.L584:
	movq	-2832(%rbp), %rbx
	movq	-2840(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L585
	.p2align 4,,10
	.p2align 3
.L589:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L586
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L589
.L587:
	movq	-2840(%rbp), %r13
.L585:
	testq	%r13, %r13
	je	.L590
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L590:
	movq	-3744(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3008(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L591
	call	_ZdlPv@PLT
.L591:
	movq	-3024(%rbp), %rbx
	movq	-3032(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L592
	.p2align 4,,10
	.p2align 3
.L596:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L593
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L596
.L594:
	movq	-3032(%rbp), %r13
.L592:
	testq	%r13, %r13
	je	.L597
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L597:
	movq	-3784(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L598
	call	_ZdlPv@PLT
.L598:
	movq	-3216(%rbp), %rbx
	movq	-3224(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L599
	.p2align 4,,10
	.p2align 3
.L603:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L600
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L603
.L601:
	movq	-3224(%rbp), %r13
.L599:
	testq	%r13, %r13
	je	.L604
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L604:
	movq	-3680(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L605
	call	_ZdlPv@PLT
.L605:
	movq	-3408(%rbp), %rbx
	movq	-3416(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L606
	.p2align 4,,10
	.p2align 3
.L610:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L607
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L610
.L608:
	movq	-3416(%rbp), %r13
.L606:
	testq	%r13, %r13
	je	.L611
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L611:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L835
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L607:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L610
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L600:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L603
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L593:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L596
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L586:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L589
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L579:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L582
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L572:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L575
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L565:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L568
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L558:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L561
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L551:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L554
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L544:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L547
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L537:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L540
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L530:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L533
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L523:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L526
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L516:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L519
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L509:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L512
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L495:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L498
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L502:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L505
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L819:
	movq	-3680(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -3536(%rbp)
	movaps	%xmm0, -3552(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r12, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -3552(%rbp)
	movq	%rdx, -3536(%rbp)
	movq	%rdx, -3544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L442
	call	_ZdlPv@PLT
.L442:
	movq	(%rbx), %rax
	movl	$9, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	movq	8(%rax), %r12
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$10, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal45Cast34ATFastJSArrayWithNoCustomIteration_1463EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	movq	%r12, %xmm6
	movq	%r12, %xmm4
	movq	%rbx, %xmm5
	punpcklqdq	%xmm4, %xmm4
	punpcklqdq	%xmm6, %xmm5
	pxor	%xmm0, %xmm0
	movq	%rax, -128(%rbp)
	leaq	-3584(%rbp), %r12
	leaq	-160(%rbp), %rbx
	movaps	%xmm4, -3840(%rbp)
	leaq	-120(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movaps	%xmm5, -3824(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm0, -3584(%rbp)
	movq	$0, -3568(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3040(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L443
	call	_ZdlPv@PLT
.L443:
	movq	-3744(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3544(%rbp)
	jne	.L836
.L444:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -3168(%rbp)
	je	.L446
.L820:
	movq	-3784(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-160(%rbp), %rbx
	leaq	-3232(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r13, %rdi
	leaq	-156(%rbp), %rdx
	movaps	%xmm0, -3552(%rbp)
	movl	$134744071, -160(%rbp)
	movq	$0, -3536(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3552(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L447
	call	_ZdlPv@PLT
.L447:
	movq	(%r12), %rax
	leaq	-136(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movdqu	(%rax), %xmm0
	movq	16(%rax), %rax
	movq	$0, -3536(%rbp)
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -144(%rbp)
	movaps	%xmm0, -3552(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-928(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L448
	call	_ZdlPv@PLT
.L448:
	movq	-3768(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2976(%rbp)
	je	.L449
.L821:
	movq	-3744(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-160(%rbp), %rbx
	leaq	-3040(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r13, %rdi
	leaq	-155(%rbp), %rdx
	movaps	%xmm0, -3552(%rbp)
	movl	$134744071, -160(%rbp)
	movb	$7, -156(%rbp)
	movq	$0, -3536(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3552(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L450
	call	_ZdlPv@PLT
.L450:
	movq	(%r12), %rax
	movl	$12, %edx
	movq	%r15, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	%rsi, -3856(%rbp)
	movq	16(%rax), %rsi
	movq	32(%rax), %rax
	movq	%rcx, -3840(%rbp)
	movq	%rsi, -3872(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%rax, -3824(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-3824(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	movq	%rax, -3888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal20Cast10FixedArray_103EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movq	-3840(%rbp), %xmm7
	movq	-3872(%rbp), %xmm6
	movq	%r12, -128(%rbp)
	leaq	-3584(%rbp), %r12
	movhps	-3856(%rbp), %xmm7
	movq	%rax, -120(%rbp)
	movhps	-3824(%rbp), %xmm6
	movaps	%xmm7, -3840(%rbp)
	movaps	%xmm6, -3824(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm0, -3584(%rbp)
	movq	$0, -3568(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm6
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	leaq	-2656(%rbp), %rdi
	movq	%rax, -3584(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-128(%rbp), %xmm7
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -3568(%rbp)
	movq	%rdx, -3576(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L451
	call	_ZdlPv@PLT
.L451:
	movq	-3704(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3544(%rbp)
	jne	.L837
.L452:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2784(%rbp)
	je	.L454
.L822:
	movq	-3800(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-2848(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-160(%rbp), %rsi
	leaq	-155(%rbp), %rdx
	movq	%r13, %rdi
	movaps	%xmm0, -3552(%rbp)
	movl	$117966855, -160(%rbp)
	movb	$7, -156(%rbp)
	movq	$0, -3536(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L455
	call	_ZdlPv@PLT
.L455:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-544(%rbp), %rdi
	movq	$0, -3536(%rbp)
	movaps	%xmm0, -3552(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L456
	call	_ZdlPv@PLT
.L456:
	movq	-3672(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2592(%rbp)
	je	.L457
.L823:
	movq	-3704(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-2656(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1799, %eax
	pxor	%xmm0, %xmm0
	leaq	-160(%rbp), %rsi
	leaq	-154(%rbp), %rdx
	movq	%r13, %rdi
	movw	%ax, -156(%rbp)
	movaps	%xmm0, -3552(%rbp)
	movl	$117966855, -160(%rbp)
	movq	$0, -3536(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L458
	call	_ZdlPv@PLT
.L458:
	movq	(%rbx), %rax
	movl	$11, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	movq	8(%rax), %rcx
	movq	24(%rax), %r12
	movq	(%rax), %rbx
	movq	%rcx, -3824(%rbp)
	movq	16(%rax), %rcx
	movq	40(%rax), %rax
	movq	%r12, -3856(%rbp)
	movq	%rcx, -3840(%rbp)
	movq	%rax, -3872(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$13, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler21LoadFastJSArrayLengthENS0_8compiler11SloppyTNodeINS0_7JSArrayEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -3888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$14, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14NewJSObject_55EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEE@PLT
	movl	$16, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %xmm0
	movl	$64, %edi
	movq	%r12, -112(%rbp)
	movhps	-3824(%rbp), %xmm0
	movq	%rax, -104(%rbp)
	movaps	%xmm0, -160(%rbp)
	movq	-3840(%rbp), %xmm0
	movq	$0, -3536(%rbp)
	movhps	-3856(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-3872(%rbp), %xmm0
	movhps	-3888(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3552(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm6
	movdqa	-144(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	64(%rax), %rdx
	leaq	-2464(%rbp), %rdi
	movq	%rax, -3552(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-128(%rbp), %xmm6
	movups	%xmm7, 16(%rax)
	movdqa	-112(%rbp), %xmm7
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rdx, -3536(%rbp)
	movq	%rdx, -3544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L459
	call	_ZdlPv@PLT
.L459:
	movq	-3696(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2400(%rbp)
	je	.L460
.L824:
	movq	-3696(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -3648(%rbp)
	leaq	-2464(%rbp), %r12
	movq	$0, -3640(%rbp)
	movq	$0, -3632(%rbp)
	movq	$0, -3624(%rbp)
	movq	$0, -3616(%rbp)
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	movq	$0, -3584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-3584(%rbp), %rax
	pushq	%rax
	leaq	-3600(%rbp), %rax
	leaq	-3632(%rbp), %rcx
	pushq	%rax
	leaq	-3608(%rbp), %rax
	leaq	-3616(%rbp), %r9
	pushq	%rax
	leaq	-3624(%rbp), %r8
	leaq	-3640(%rbp), %rdx
	leaq	-3648(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_10FixedArrayENS0_3SmiENS0_8JSObjectES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESK_
	addq	$32, %rsp
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3584(%rbp), %rbx
	movq	-3608(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	-3600(%rbp), %xmm3
	movq	-3616(%rbp), %xmm2
	movaps	%xmm0, -3552(%rbp)
	movq	-3632(%rbp), %xmm4
	movq	-3648(%rbp), %xmm5
	movhps	-3584(%rbp), %xmm3
	movq	$0, -3536(%rbp)
	movhps	-3608(%rbp), %xmm2
	movhps	-3624(%rbp), %xmm4
	movaps	%xmm3, -3872(%rbp)
	movhps	-3640(%rbp), %xmm5
	movaps	%xmm2, -3840(%rbp)
	movaps	%xmm4, -3856(%rbp)
	movaps	%xmm5, -3824(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-144(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm7
	leaq	64(%rax), %rdx
	leaq	-2272(%rbp), %rdi
	movq	%rax, -3552(%rbp)
	movups	%xmm3, (%rax)
	movdqa	-112(%rbp), %xmm3
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rdx, -3536(%rbp)
	movq	%rdx, -3544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L461
	call	_ZdlPv@PLT
.L461:
	movdqa	-3824(%rbp), %xmm4
	movdqa	-3856(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movdqa	-3840(%rbp), %xmm6
	movdqa	-3872(%rbp), %xmm7
	movaps	%xmm0, -3552(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movq	$0, -3536(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-144(%rbp), %xmm4
	leaq	64(%rax), %rdx
	leaq	-1120(%rbp), %rdi
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movq	%rax, -3552(%rbp)
	movq	%rdx, -3536(%rbp)
	movq	%rdx, -3544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L462
	call	_ZdlPv@PLT
.L462:
	movq	-3752(%rbp), %rcx
	movq	-3736(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2208(%rbp)
	je	.L463
.L825:
	movq	-3736(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -3656(%rbp)
	leaq	-2272(%rbp), %r12
	movq	$0, -3648(%rbp)
	movq	$0, -3640(%rbp)
	movq	$0, -3632(%rbp)
	movq	$0, -3624(%rbp)
	movq	$0, -3616(%rbp)
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-3600(%rbp), %rax
	pushq	%rax
	leaq	-3608(%rbp), %rax
	leaq	-3624(%rbp), %r9
	pushq	%rax
	leaq	-3616(%rbp), %rax
	leaq	-3632(%rbp), %r8
	pushq	%rax
	leaq	-3640(%rbp), %rcx
	leaq	-3648(%rbp), %rdx
	leaq	-3656(%rbp), %rsi
	leaq	-3584(%rbp), %r12
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_10FixedArrayENS0_3SmiENS0_8JSObjectES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESK_
	addq	$32, %rsp
	movl	$17, %edx
	movq	%r15, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3600(%rbp), %rcx
	movq	-3624(%rbp), %rdx
	movq	%r14, %rdi
	movq	-3656(%rbp), %rsi
	call	_ZN2v88internal25LoadElementOrUndefined_51EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_3SmiEEE@PLT
	movl	$19, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	-3656(%rbp), %rsi
	call	_ZN2v88internal33LoadKeyValuePairNoSideEffects_267EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm6
	movq	%r12, %rdi
	movq	-3608(%rbp), %xmm0
	movq	-3624(%rbp), %xmm1
	movq	%rax, -3824(%rbp)
	punpcklqdq	%xmm6, %xmm6
	leaq	-160(%rbp), %rbx
	movq	-3640(%rbp), %xmm2
	movq	%rbx, %rsi
	movq	-3656(%rbp), %xmm3
	movq	%rdx, -3816(%rbp)
	movhps	-3600(%rbp), %xmm0
	movdqa	-3824(%rbp), %xmm7
	leaq	-64(%rbp), %rdx
	movhps	-3616(%rbp), %xmm1
	movhps	-3632(%rbp), %xmm2
	movhps	-3648(%rbp), %xmm3
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm6, -3840(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	movaps	%xmm0, -3584(%rbp)
	movq	$0, -3568(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1888(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L464
	call	_ZdlPv@PLT
.L464:
	movq	-3760(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3544(%rbp)
	jne	.L838
.L465:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2016(%rbp)
	je	.L467
.L826:
	movq	-3792(%rbp), %rsi
	movq	%r15, %rdi
	movl	$2056, %ebx
	leaq	-2080(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-160(%rbp), %rsi
	movabsq	$434322516317046791, %rax
	leaq	-150(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rax, -160(%rbp)
	movaps	%xmm0, -3552(%rbp)
	movw	%bx, -152(%rbp)
	movq	$0, -3536(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L468
	call	_ZdlPv@PLT
.L468:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-544(%rbp), %rdi
	movq	$0, -3536(%rbp)
	movaps	%xmm0, -3552(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L469
	call	_ZdlPv@PLT
.L469:
	movq	-3672(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1824(%rbp)
	je	.L470
.L827:
	movq	-3760(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-160(%rbp), %rbx
	leaq	-1888(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r13, %rdi
	leaq	-148(%rbp), %rdx
	movabsq	$434322516317046791, %rax
	movaps	%xmm0, -3552(%rbp)
	movq	%rax, -160(%rbp)
	movl	$134744072, -152(%rbp)
	movq	$0, -3536(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3552(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L471
	call	_ZdlPv@PLT
.L471:
	movq	(%r12), %rax
	movq	8(%rax), %rcx
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	%rcx, -3888(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -3912(%rbp)
	movq	32(%rax), %rsi
	movq	%rdx, -3920(%rbp)
	movq	48(%rax), %rdx
	movq	%rdi, -3952(%rbp)
	movq	64(%rax), %rdi
	movq	(%rax), %r12
	movq	%rcx, -3904(%rbp)
	movq	80(%rax), %rcx
	movq	88(%rax), %rax
	movq	%rsi, -3936(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%rdx, -3944(%rbp)
	movl	$18, %edx
	movq	%rdi, -3872(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -3824(%rbp)
	movq	%rax, -3840(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3824(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal111Is10JSReceiver90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_1464EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	movq	%r12, %xmm5
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	-3872(%rbp), %xmm7
	movq	%rax, -3856(%rbp)
	leaq	-72(%rbp), %r12
	movhps	-3888(%rbp), %xmm5
	movq	-3936(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movq	-3904(%rbp), %xmm4
	movq	%r12, %rdx
	movq	-3944(%rbp), %xmm3
	movq	-3840(%rbp), %rax
	movhps	-3824(%rbp), %xmm7
	movaps	%xmm5, -3888(%rbp)
	movhps	-3920(%rbp), %xmm2
	movhps	-3912(%rbp), %xmm4
	movaps	%xmm7, -96(%rbp)
	movhps	-3952(%rbp), %xmm3
	movq	%rax, -80(%rbp)
	movaps	%xmm7, -3872(%rbp)
	movaps	%xmm3, -3824(%rbp)
	movaps	%xmm2, -3936(%rbp)
	movaps	%xmm4, -3904(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm0, -3552(%rbp)
	movq	$0, -3536(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1696(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L472
	call	_ZdlPv@PLT
.L472:
	movdqa	-3888(%rbp), %xmm3
	movdqa	-3904(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movq	%r12, %rdx
	movdqa	-3936(%rbp), %xmm5
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movdqa	-3824(%rbp), %xmm6
	movdqa	-3872(%rbp), %xmm7
	movq	-3840(%rbp), %rax
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movq	%rax, -80(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm0, -3552(%rbp)
	movq	$0, -3536(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1504(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L473
	call	_ZdlPv@PLT
.L473:
	movq	-3728(%rbp), %rcx
	movq	-3712(%rbp), %rdx
	movq	%r15, %rdi
	movq	-3856(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1632(%rbp)
	je	.L474
.L828:
	movq	-3712(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-1696(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$11, %edi
	movq	$0, -3536(%rbp)
	movaps	%xmm0, -3552(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r11d
	movq	%r12, %rdi
	movq	%r13, %rsi
	movabsq	$434322516317046791, %rcx
	movw	%r11w, 8(%rax)
	leaq	11(%rax), %rdx
	movq	%rcx, (%rax)
	movb	$8, 10(%rax)
	movq	%rax, -3552(%rbp)
	movq	%rdx, -3536(%rbp)
	movq	%rdx, -3544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L475
	call	_ZdlPv@PLT
.L475:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-544(%rbp), %rdi
	movq	$0, -3536(%rbp)
	movaps	%xmm0, -3552(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L476
	call	_ZdlPv@PLT
.L476:
	movq	-3672(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1440(%rbp)
	je	.L477
.L829:
	movq	-3728(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-1504(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$11, %edi
	movq	$0, -3536(%rbp)
	movaps	%xmm0, -3552(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r10d
	movq	%r12, %rdi
	movq	%r13, %rsi
	movabsq	$434322516317046791, %rcx
	movw	%r10w, 8(%rax)
	leaq	11(%rax), %rdx
	movq	%rcx, (%rax)
	movb	$8, 10(%rax)
	movq	%rax, -3552(%rbp)
	movq	%rdx, -3536(%rbp)
	movq	%rdx, -3544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L478
	call	_ZdlPv@PLT
.L478:
	movq	(%rbx), %rax
	leaq	-3600(%rbp), %r12
	movq	%r15, %rdi
	movq	40(%rax), %rdx
	movq	8(%rax), %rcx
	movq	24(%rax), %rsi
	movq	(%rax), %rbx
	movq	%rdx, -3904(%rbp)
	movq	48(%rax), %rdx
	movq	%rcx, -3840(%rbp)
	movq	16(%rax), %rcx
	movq	%rdx, -3824(%rbp)
	movq	56(%rax), %rdx
	movq	%rsi, -3872(%rbp)
	movq	32(%rax), %rsi
	movq	%rdx, -3912(%rbp)
	movq	72(%rax), %rdx
	movq	%rcx, -3856(%rbp)
	movq	80(%rax), %rcx
	movq	%rsi, -3888(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%rdx, -3936(%rbp)
	movl	$25, %edx
	movq	%rcx, -3920(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$786, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-3552(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-160(%rbp), %rsi
	movl	$3, %edi
	movq	%rbx, %r9
	pushq	%rdi
	movq	%rax, %r8
	movq	%r12, %rdi
	movq	-3920(%rbp), %rcx
	pushq	%rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	leaq	-3584(%rbp), %rdx
	xorl	%esi, %esi
	movq	-3824(%rbp), %xmm0
	movq	%rax, -3584(%rbp)
	movq	-3536(%rbp), %rax
	movq	%rcx, -144(%rbp)
	movl	$1, %ecx
	movhps	-3936(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%rax, -3576(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$16, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$64, %edi
	movq	$0, -3536(%rbp)
	movhps	-3840(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-3856(%rbp), %xmm0
	movhps	-3872(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-3888(%rbp), %xmm0
	movhps	-3904(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-3824(%rbp), %xmm0
	movhps	-3912(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3552(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-144(%rbp), %xmm3
	leaq	64(%rax), %rdx
	leaq	-1312(%rbp), %rdi
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rax, -3552(%rbp)
	movq	%rdx, -3536(%rbp)
	movq	%rdx, -3544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3552(%rbp), %rdi
	popq	%r8
	popq	%r9
	testq	%rdi, %rdi
	je	.L479
	call	_ZdlPv@PLT
.L479:
	movq	-3776(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1248(%rbp)
	je	.L480
.L830:
	movq	-3776(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -3648(%rbp)
	leaq	-1312(%rbp), %r12
	movq	$0, -3640(%rbp)
	movq	$0, -3632(%rbp)
	movq	$0, -3624(%rbp)
	movq	$0, -3616(%rbp)
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	movq	$0, -3584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-3584(%rbp), %rax
	pushq	%rax
	leaq	-3600(%rbp), %rax
	leaq	-3632(%rbp), %rcx
	pushq	%rax
	leaq	-3608(%rbp), %rax
	leaq	-3616(%rbp), %r9
	pushq	%rax
	leaq	-3624(%rbp), %r8
	leaq	-3640(%rbp), %rdx
	leaq	-3648(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_10FixedArrayENS0_3SmiENS0_8JSObjectES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESK_
	addq	$32, %rsp
	movq	%r14, %rdi
	movl	$1, %esi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3584(%rbp), %r14
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3600(%rbp), %rax
	movl	$64, %edi
	movq	-3616(%rbp), %xmm0
	movq	-3632(%rbp), %xmm1
	movq	%rbx, -104(%rbp)
	movq	-3648(%rbp), %xmm2
	movq	%rax, -112(%rbp)
	movhps	-3608(%rbp), %xmm0
	movhps	-3624(%rbp), %xmm1
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3640(%rbp), %xmm2
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm0, -3552(%rbp)
	movq	$0, -3536(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm2
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm7
	leaq	64(%rax), %rdx
	leaq	-2464(%rbp), %rdi
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rax, -3552(%rbp)
	movq	%rdx, -3536(%rbp)
	movq	%rdx, -3544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L481
	call	_ZdlPv@PLT
.L481:
	movq	-3696(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1056(%rbp)
	je	.L482
.L831:
	movq	-3752(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -3648(%rbp)
	leaq	-1120(%rbp), %r12
	movq	$0, -3640(%rbp)
	movq	$0, -3632(%rbp)
	movq	$0, -3624(%rbp)
	movq	$0, -3616(%rbp)
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	movq	$0, -3584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-3584(%rbp), %rax
	pushq	%rax
	leaq	-3600(%rbp), %rax
	leaq	-3632(%rbp), %rcx
	pushq	%rax
	leaq	-3608(%rbp), %rax
	leaq	-3616(%rbp), %r9
	pushq	%rax
	leaq	-3624(%rbp), %r8
	leaq	-3640(%rbp), %rdx
	leaq	-3648(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_10FixedArrayENS0_3SmiENS0_8JSObjectES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESK_
	addq	$32, %rsp
	movl	$27, %edx
	movq	%r15, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3648(%rbp), %rax
	pxor	%xmm0, %xmm0
	leaq	-160(%rbp), %rsi
	leaq	-136(%rbp), %rdx
	movq	%r13, %rdi
	movaps	%xmm0, -3552(%rbp)
	movq	%rax, -160(%rbp)
	movq	-3640(%rbp), %rax
	movq	$0, -3536(%rbp)
	movq	%rax, -152(%rbp)
	movq	-3600(%rbp), %rax
	movq	%rax, -144(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-736(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L483
	call	_ZdlPv@PLT
.L483:
	movq	-3720(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -864(%rbp)
	je	.L484
.L832:
	movq	-3768(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-928(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -3536(%rbp)
	movaps	%xmm0, -3552(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r12, %rdi
	movb	$8, 2(%rax)
	movq	%rax, -3552(%rbp)
	movq	%rdx, -3536(%rbp)
	movq	%rdx, -3544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L485
	call	_ZdlPv@PLT
.L485:
	movl	$29, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$30, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm0, %xmm0
	leaq	-544(%rbp), %rdi
	movq	%r13, %rsi
	movq	$0, -3536(%rbp)
	movaps	%xmm0, -3552(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L486
	call	_ZdlPv@PLT
.L486:
	movq	-3672(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L833:
	movq	-3720(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-736(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -3536(%rbp)
	movaps	%xmm0, -3552(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%r12, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$7, 2(%rax)
	movq	%rax, -3552(%rbp)
	movq	%rdx, -3536(%rbp)
	movq	%rdx, -3544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L488
	call	_ZdlPv@PLT
.L488:
	movq	(%rbx), %rax
	movl	$7, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %r14
	movq	16(%rax), %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %xmm4
	movq	%r13, %rdi
	movq	%rbx, %xmm0
	punpcklqdq	%xmm4, %xmm0
	movq	%r12, -144(%rbp)
	leaq	-160(%rbp), %rsi
	leaq	-136(%rbp), %rdx
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	leaq	-352(%rbp), %r12
	movaps	%xmm0, -3552(%rbp)
	movq	$0, -3536(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L489
	call	_ZdlPv@PLT
.L489:
	movq	-3688(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -480(%rbp)
	je	.L490
.L834:
	movq	-3672(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-544(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	$0, -3536(%rbp)
	movaps	%xmm0, -3552(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L491
	call	_ZdlPv@PLT
.L491:
	movq	-3808(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L836:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %rdx
	movq	%rbx, %rsi
	movdqa	-3824(%rbp), %xmm5
	movdqa	-3840(%rbp), %xmm6
	movq	%r12, %rdi
	movaps	%xmm0, -3584(%rbp)
	movq	$0, -3568(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3232(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L445
	call	_ZdlPv@PLT
.L445:
	movq	-3784(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L838:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-80(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-3608(%rbp), %xmm0
	movq	-3624(%rbp), %xmm1
	movq	$0, -3568(%rbp)
	movq	-3640(%rbp), %xmm2
	movdqa	-3840(%rbp), %xmm6
	movq	-3656(%rbp), %xmm3
	movhps	-3600(%rbp), %xmm0
	movhps	-3616(%rbp), %xmm1
	movhps	-3632(%rbp), %xmm2
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3648(%rbp), %xmm3
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm0, -3584(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2080(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L466
	call	_ZdlPv@PLT
.L466:
	movq	-3792(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L837:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-120(%rbp), %rdx
	movq	%rbx, %rsi
	movdqa	-3840(%rbp), %xmm7
	movdqa	-3824(%rbp), %xmm5
	movq	%r12, %rdi
	movaps	%xmm0, -3584(%rbp)
	movq	-3888(%rbp), %rax
	movq	$0, -3568(%rbp)
	movaps	%xmm7, -160(%rbp)
	movq	%rax, -128(%rbp)
	movaps	%xmm5, -144(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2848(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L453
	call	_ZdlPv@PLT
.L453:
	movq	-3800(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L452
.L835:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22410:
	.size	_ZN2v88internal29ObjectFromEntriesFastCase_310EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, .-_ZN2v88internal29ObjectFromEntriesFastCase_310EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES6_EE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESG_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES6_EE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESG_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES6_EE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESG_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES6_EE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESG_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES6_EE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESG_:
.LFB26907:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$6, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$2056, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$117769477, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L840
	movq	%rax, -104(%rbp)
	call	_ZdlPv@PLT
	movq	-104(%rbp), %rax
.L840:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L841
	movq	%rdx, (%r15)
.L841:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L842
	movq	%rdx, (%r14)
.L842:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L843
	movq	%rdx, 0(%r13)
.L843:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L844
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L844:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L845
	movq	%rdx, (%rbx)
.L845:
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L839
	movq	-96(%rbp), %rbx
	movq	%rax, (%rbx)
.L839:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L870
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L870:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26907:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES6_EE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESG_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES6_EE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESG_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES6_NS0_8JSObjectENS0_3MapENS0_10JSReceiverES6_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESJ_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES6_NS0_8JSObjectENS0_3MapENS0_10JSReceiverES6_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESJ_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES6_NS0_8JSObjectENS0_3MapENS0_10JSReceiverES6_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESJ_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES6_NS0_8JSObjectENS0_3MapENS0_10JSReceiverES6_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESJ_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES6_NS0_8JSObjectENS0_3MapENS0_10JSReceiverES6_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESJ_:
.LFB26923:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$10, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$506382313673000197, %rcx
	movq	%rcx, (%rax)
	movl	$2055, %ecx
	leaq	10(%rax), %rdx
	movw	%cx, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L872
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %rax
.L872:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L873
	movq	%rdx, (%r15)
.L873:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L874
	movq	%rdx, (%r14)
.L874:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L875
	movq	%rdx, 0(%r13)
.L875:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L876
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L876:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L877
	movq	%rdx, (%rbx)
.L877:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L878
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L878:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L879
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L879:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L880
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L880:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L881
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L881:
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L871
	movq	-128(%rbp), %rsi
	movq	%rax, (%rsi)
.L871:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L918
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L918:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26923:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES6_NS0_8JSObjectENS0_3MapENS0_10JSReceiverES6_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESJ_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES6_NS0_8JSObjectENS0_3MapENS0_10JSReceiverES6_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESJ_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EE:
.LFB26924:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$1, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -48(%rbp)
	movq	$0, -32(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	movb	$8, (%rax)
	leaq	1(%rax), %rdx
	movq	%rax, -48(%rbp)
	movq	%rdx, -32(%rbp)
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-48(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L920
	call	_ZdlPv@PLT
.L920:
	movq	(%r12), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L919
	movq	%rax, (%rbx)
.L919:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L930
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L930:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26924:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EE
	.section	.text._ZN2v88internal26ObjectFromEntriesAssembler29GenerateObjectFromEntriesImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26ObjectFromEntriesAssembler29GenerateObjectFromEntriesImplEv
	.type	_ZN2v88internal26ObjectFromEntriesAssembler29GenerateObjectFromEntriesImplEv, @function
_ZN2v88internal26ObjectFromEntriesAssembler29GenerateObjectFromEntriesImplEv:
.LFB22466:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$1320, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$1, %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rdi, %r12
	leaq	-5000(%rbp), %r14
	movq	%rax, -5016(%rbp)
	movq	%rax, -5000(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r13, %rcx
	movq	%r12, %rsi
	leaq	-4896(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-4896(%rbp), %r13
	movl	$2, %esi
	movq	%r12, %rdi
	movq	-4888(%rbp), %rax
	movq	-4880(%rbp), %rbx
	movq	%r12, -4800(%rbp)
	leaq	-4568(%rbp), %r12
	movq	%r13, -4768(%rbp)
	movq	%rax, -5088(%rbp)
	movq	$1, -4792(%rbp)
	movq	%rbx, -4784(%rbp)
	movq	%rax, -4776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rax, -5040(%rbp)
	leaq	-4800(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -5256(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -4616(%rbp)
	movq	$0, -4608(%rbp)
	movq	%rax, %r15
	movq	-5000(%rbp), %rax
	movq	$0, -4600(%rbp)
	movq	%rax, -4624(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -4600(%rbp)
	movq	%rdx, -4608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4584(%rbp)
	movq	%rax, -4616(%rbp)
	movq	$0, -4592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5000(%rbp), %rax
	movl	$144, %edi
	movq	$0, -4424(%rbp)
	movq	$0, -4416(%rbp)
	movq	%rax, -4432(%rbp)
	movq	$0, -4408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -4424(%rbp)
	leaq	-4376(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4408(%rbp)
	movq	%rdx, -4416(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4392(%rbp)
	movq	%rax, -5056(%rbp)
	movq	$0, -4400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5000(%rbp), %rax
	movl	$144, %edi
	movq	$0, -4232(%rbp)
	movq	$0, -4224(%rbp)
	movq	%rax, -4240(%rbp)
	movq	$0, -4216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -4232(%rbp)
	leaq	-4184(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4216(%rbp)
	movq	%rdx, -4224(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4200(%rbp)
	movq	%rax, -5064(%rbp)
	movq	$0, -4208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5000(%rbp), %rax
	movl	$168, %edi
	movq	$0, -4040(%rbp)
	movq	$0, -4032(%rbp)
	movq	%rax, -4048(%rbp)
	movq	$0, -4024(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -4040(%rbp)
	leaq	-3992(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4024(%rbp)
	movq	%rdx, -4032(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4008(%rbp)
	movq	%rax, -5144(%rbp)
	movq	$0, -4016(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5000(%rbp), %rax
	movl	$192, %edi
	movq	$0, -3848(%rbp)
	movq	$0, -3840(%rbp)
	movq	%rax, -3856(%rbp)
	movq	$0, -3832(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -3848(%rbp)
	leaq	-3800(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3832(%rbp)
	movq	%rdx, -3840(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3816(%rbp)
	movq	%rax, -5048(%rbp)
	movq	$0, -3824(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5000(%rbp), %rax
	movl	$144, %edi
	movq	$0, -3656(%rbp)
	movq	$0, -3648(%rbp)
	movq	%rax, -3664(%rbp)
	movq	$0, -3640(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -3656(%rbp)
	leaq	-3608(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3640(%rbp)
	movq	%rdx, -3648(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3624(%rbp)
	movq	%rax, -5096(%rbp)
	movq	$0, -3632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5000(%rbp), %rax
	movl	$240, %edi
	movq	$0, -3464(%rbp)
	movq	$0, -3456(%rbp)
	movq	%rax, -3472(%rbp)
	movq	$0, -3448(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -3464(%rbp)
	leaq	-3416(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3448(%rbp)
	movq	%rdx, -3456(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3432(%rbp)
	movq	%rax, -5216(%rbp)
	movq	$0, -3440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5000(%rbp), %rax
	movl	$264, %edi
	movq	$0, -3272(%rbp)
	movq	$0, -3264(%rbp)
	movq	%rax, -3280(%rbp)
	movq	$0, -3256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%r14, %rsi
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -3272(%rbp)
	leaq	-3224(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3256(%rbp)
	movq	%rdx, -3264(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3240(%rbp)
	movq	%rax, -5136(%rbp)
	movq	$0, -3248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5000(%rbp), %rax
	movl	$240, %edi
	movq	$0, -3080(%rbp)
	movq	$0, -3072(%rbp)
	movq	%rax, -3088(%rbp)
	movq	$0, -3064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -3080(%rbp)
	leaq	-3032(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3064(%rbp)
	movq	%rdx, -3072(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3048(%rbp)
	movq	%rax, -5104(%rbp)
	movq	$0, -3056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5000(%rbp), %rax
	movl	$336, %edi
	movq	$0, -2888(%rbp)
	movq	$0, -2880(%rbp)
	movq	%rax, -2896(%rbp)
	movq	$0, -2872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%r14, %rsi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -2888(%rbp)
	leaq	-2840(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2872(%rbp)
	movq	%rdx, -2880(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2856(%rbp)
	movq	%rax, -5328(%rbp)
	movq	$0, -2864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5000(%rbp), %rax
	movl	$312, %edi
	movq	$0, -2696(%rbp)
	movq	$0, -2688(%rbp)
	movq	%rax, -2704(%rbp)
	movq	$0, -2680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rax, -2696(%rbp)
	leaq	-2648(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2680(%rbp)
	movq	%rdx, -2688(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2664(%rbp)
	movq	%rax, -5128(%rbp)
	movq	$0, -2672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5000(%rbp), %rax
	movl	$336, %edi
	movq	$0, -2504(%rbp)
	movq	$0, -2496(%rbp)
	movq	%rax, -2512(%rbp)
	movq	$0, -2488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -2504(%rbp)
	leaq	-2456(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2488(%rbp)
	movq	%rdx, -2496(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2472(%rbp)
	movq	%rax, -5280(%rbp)
	movq	$0, -2480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5000(%rbp), %rax
	movl	$240, %edi
	movq	$0, -2312(%rbp)
	movq	$0, -2304(%rbp)
	movq	%rax, -2320(%rbp)
	movq	$0, -2296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -2312(%rbp)
	leaq	-2264(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2296(%rbp)
	movq	%rdx, -2304(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2280(%rbp)
	movq	%rax, -5296(%rbp)
	movq	$0, -2288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5000(%rbp), %rax
	movl	$264, %edi
	movq	$0, -2120(%rbp)
	movq	$0, -2112(%rbp)
	movq	%rax, -2128(%rbp)
	movq	$0, -2104(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -2120(%rbp)
	leaq	-2072(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2104(%rbp)
	movq	%rdx, -2112(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2088(%rbp)
	movq	%rax, -5312(%rbp)
	movq	$0, -2096(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5000(%rbp), %rax
	movl	$336, %edi
	movq	$0, -1928(%rbp)
	movq	$0, -1920(%rbp)
	movq	%rax, -1936(%rbp)
	movq	$0, -1912(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%r14, %rsi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -1928(%rbp)
	leaq	-1880(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1912(%rbp)
	movq	%rdx, -1920(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1896(%rbp)
	movq	%rax, -5160(%rbp)
	movq	$0, -1904(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5000(%rbp), %rax
	movl	$336, %edi
	movq	$0, -1736(%rbp)
	movq	$0, -1728(%rbp)
	movq	%rax, -1744(%rbp)
	movq	$0, -1720(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%r14, %rsi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -1736(%rbp)
	leaq	-1688(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1720(%rbp)
	movq	%rdx, -1728(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1704(%rbp)
	movq	%rax, -5168(%rbp)
	movq	$0, -1712(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5000(%rbp), %rax
	movl	$432, %edi
	movq	$0, -1544(%rbp)
	movq	$0, -1536(%rbp)
	movq	%rax, -1552(%rbp)
	movq	$0, -1528(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movups	%xmm0, (%rax)
	leaq	432(%rax), %rdx
	movq	%r14, %rsi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movq	%rax, -1544(%rbp)
	leaq	-1496(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1528(%rbp)
	movq	%rdx, -1536(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1512(%rbp)
	movq	$0, -1520(%rbp)
	movq	%rax, -5152(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5000(%rbp), %rax
	movl	$240, %edi
	movq	$0, -1352(%rbp)
	movq	$0, -1344(%rbp)
	movq	%rax, -1360(%rbp)
	movq	$0, -1336(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -1352(%rbp)
	leaq	-1304(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1336(%rbp)
	movq	%rdx, -1344(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1320(%rbp)
	movq	%rax, -5288(%rbp)
	movq	$0, -1328(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5000(%rbp), %rax
	movl	$264, %edi
	movq	$0, -1160(%rbp)
	movq	$0, -1152(%rbp)
	movq	%rax, -1168(%rbp)
	movq	$0, -1144(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%r14, %rsi
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -1160(%rbp)
	leaq	-1112(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1144(%rbp)
	movq	%rdx, -1152(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1128(%rbp)
	movq	%rax, -5024(%rbp)
	movq	$0, -1136(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5000(%rbp), %rax
	movl	$144, %edi
	movq	$0, -968(%rbp)
	movq	$0, -960(%rbp)
	movq	%rax, -976(%rbp)
	movq	$0, -952(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%r14, %rsi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -968(%rbp)
	leaq	-920(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -952(%rbp)
	movq	%rdx, -960(%rbp)
	xorl	%edx, %edx
	movq	%rax, -5072(%rbp)
	movups	%xmm0, -936(%rbp)
	movq	$0, -944(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %xmm1
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movhps	-5088(%rbp), %xmm1
	movaps	%xmm0, -400(%rbp)
	leaq	-400(%rbp), %r13
	movaps	%xmm1, -208(%rbp)
	movq	%rbx, %xmm1
	movhps	-5040(%rbp), %xmm1
	movq	%r15, -176(%rbp)
	movaps	%xmm1, -192(%rbp)
	movq	$0, -384(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rcx
	movdqa	-208(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-192(%rbp), %xmm6
	leaq	40(%rax), %rdx
	movq	%rax, -400(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	leaq	-4624(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -384(%rbp)
	movq	%rdx, -392(%rbp)
	movq	%rax, -5208(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L932
	call	_ZdlPv@PLT
.L932:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -4560(%rbp)
	jne	.L1255
	cmpq	$0, -4368(%rbp)
	jne	.L1256
.L937:
	cmpq	$0, -4176(%rbp)
	jne	.L1257
.L939:
	leaq	-3664(%rbp), %rax
	cmpq	$0, -3984(%rbp)
	movq	%rax, -5088(%rbp)
	jne	.L1258
	cmpq	$0, -3792(%rbp)
	jne	.L1259
.L946:
	leaq	-3472(%rbp), %rax
	cmpq	$0, -3600(%rbp)
	movq	%rax, -5040(%rbp)
	jne	.L1260
.L948:
	leaq	-3088(%rbp), %rax
	cmpq	$0, -3408(%rbp)
	movq	%rax, -5096(%rbp)
	leaq	-1360(%rbp), %rax
	movq	%rax, -5120(%rbp)
	jne	.L1261
	cmpq	$0, -3216(%rbp)
	jne	.L1262
.L955:
	cmpq	$0, -3024(%rbp)
	jne	.L1251
.L1277:
	leaq	-2896(%rbp), %rax
	cmpq	$0, -2832(%rbp)
	movq	%rax, -5248(%rbp)
	leaq	-2512(%rbp), %rax
	movq	%rax, -5232(%rbp)
	leaq	-2704(%rbp), %rax
	movq	%rax, -5184(%rbp)
	jne	.L1263
.L964:
	leaq	-2320(%rbp), %rax
	cmpq	$0, -2640(%rbp)
	movq	%rax, -5104(%rbp)
	jne	.L1264
.L967:
	leaq	-2128(%rbp), %rax
	cmpq	$0, -2448(%rbp)
	movq	%rax, -5128(%rbp)
	jne	.L1265
.L970:
	cmpq	$0, -2256(%rbp)
	jne	.L1266
	cmpq	$0, -2064(%rbp)
	jne	.L1267
.L974:
	cmpq	$0, -1872(%rbp)
	jne	.L1268
.L983:
	cmpq	$0, -1680(%rbp)
	jne	.L1269
.L986:
	cmpq	$0, -1488(%rbp)
	jne	.L1270
.L989:
	cmpq	$0, -1296(%rbp)
	jne	.L1271
.L992:
	cmpq	$0, -1104(%rbp)
	jne	.L1272
.L993:
	cmpq	$0, -912(%rbp)
	jne	.L1273
.L996:
	movq	-5072(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L997
	call	_ZdlPv@PLT
.L997:
	movq	-960(%rbp), %rbx
	movq	-968(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L998
	.p2align 4,,10
	.p2align 3
.L1002:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L999
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L1002
.L1000:
	movq	-968(%rbp), %r12
.L998:
	testq	%r12, %r12
	je	.L1003
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1003:
	movq	-5024(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1004
	call	_ZdlPv@PLT
.L1004:
	movq	-1152(%rbp), %rbx
	movq	-1160(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1005
	.p2align 4,,10
	.p2align 3
.L1009:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1006
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1009
.L1007:
	movq	-1160(%rbp), %r12
.L1005:
	testq	%r12, %r12
	je	.L1010
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1010:
	movq	-5120(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5152(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1011
	call	_ZdlPv@PLT
.L1011:
	movq	-1536(%rbp), %rbx
	movq	-1544(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1012
	.p2align 4,,10
	.p2align 3
.L1016:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1013
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L1016
.L1014:
	movq	-1544(%rbp), %r12
.L1012:
	testq	%r12, %r12
	je	.L1017
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1017:
	movq	-5168(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1018
	call	_ZdlPv@PLT
.L1018:
	movq	-1728(%rbp), %rbx
	movq	-1736(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1019
	.p2align 4,,10
	.p2align 3
.L1023:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1020
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L1023
.L1021:
	movq	-1736(%rbp), %r12
.L1019:
	testq	%r12, %r12
	je	.L1024
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1024:
	movq	-5160(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1904(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1025
	call	_ZdlPv@PLT
.L1025:
	movq	-1920(%rbp), %rbx
	movq	-1928(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1026
	.p2align 4,,10
	.p2align 3
.L1030:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1027
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L1030
.L1028:
	movq	-1928(%rbp), %r12
.L1026:
	testq	%r12, %r12
	je	.L1031
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1031:
	movq	-5128(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5104(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5232(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5184(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5248(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5096(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5136(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1032
	call	_ZdlPv@PLT
.L1032:
	movq	-3264(%rbp), %rbx
	movq	-3272(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1033
	.p2align 4,,10
	.p2align 3
.L1037:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1034
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L1037
.L1035:
	movq	-3272(%rbp), %r12
.L1033:
	testq	%r12, %r12
	je	.L1038
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1038:
	movq	-5040(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5088(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5048(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1039
	call	_ZdlPv@PLT
.L1039:
	movq	-3840(%rbp), %rbx
	movq	-3848(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1040
	.p2align 4,,10
	.p2align 3
.L1044:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1041
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L1044
.L1042:
	movq	-3848(%rbp), %r12
.L1040:
	testq	%r12, %r12
	je	.L1045
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1045:
	movq	-5144(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-4016(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1046
	call	_ZdlPv@PLT
.L1046:
	movq	-4032(%rbp), %rbx
	movq	-4040(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1047
	.p2align 4,,10
	.p2align 3
.L1051:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1048
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L1051
.L1049:
	movq	-4040(%rbp), %r12
.L1047:
	testq	%r12, %r12
	je	.L1052
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1052:
	movq	-5064(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-4208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1053
	call	_ZdlPv@PLT
.L1053:
	movq	-4224(%rbp), %rbx
	movq	-4232(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1054
	.p2align 4,,10
	.p2align 3
.L1058:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1055
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1058
.L1056:
	movq	-4232(%rbp), %r12
.L1054:
	testq	%r12, %r12
	je	.L1059
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1059:
	movq	-5056(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-4400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1060
	call	_ZdlPv@PLT
.L1060:
	movq	-4416(%rbp), %rbx
	movq	-4424(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1061
	.p2align 4,,10
	.p2align 3
.L1065:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1062
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1065
.L1063:
	movq	-4424(%rbp), %r12
.L1061:
	testq	%r12, %r12
	je	.L1066
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1066:
	movq	-5208(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1274
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1062:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1065
	jmp	.L1063
	.p2align 4,,10
	.p2align 3
.L1055:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1058
	jmp	.L1056
	.p2align 4,,10
	.p2align 3
.L1048:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1051
	jmp	.L1049
	.p2align 4,,10
	.p2align 3
.L1041:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1044
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L1034:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1037
	jmp	.L1035
	.p2align 4,,10
	.p2align 3
.L1027:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1030
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L1020:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1023
	jmp	.L1021
	.p2align 4,,10
	.p2align 3
.L1013:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1016
	jmp	.L1014
	.p2align 4,,10
	.p2align 3
.L999:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1002
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L1006:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1009
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L1255:
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-208(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r13, %rdi
	leaq	-203(%rbp), %rdx
	movaps	%xmm0, -400(%rbp)
	movl	$117769477, -208(%rbp)
	movb	$8, -204(%rbp)
	movq	$0, -384(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5208(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L934
	call	_ZdlPv@PLT
.L934:
	movq	(%rbx), %rax
	movl	$38, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r12
	movq	16(%rax), %rbx
	movq	%rcx, -5040(%rbp)
	movq	24(%rax), %rcx
	movq	32(%rax), %rax
	movq	%rcx, -5088(%rbp)
	movq	%rax, -5120(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-5016(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	-5016(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %xmm7
	movq	%r15, %rsi
	movq	%r13, %rdi
	movhps	-5040(%rbp), %xmm7
	subq	$8, %rsp
	movq	%rbx, -4848(%rbp)
	movaps	%xmm7, -4864(%rbp)
	pushq	-4848(%rbp)
	pushq	-4856(%rbp)
	pushq	-4864(%rbp)
	movaps	%xmm7, -5040(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$40, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-5016(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler17IsNullOrUndefinedENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %xmm5
	pxor	%xmm0, %xmm0
	movq	-5120(%rbp), %xmm4
	movdqa	-5040(%rbp), %xmm7
	movl	$48, %edi
	movaps	%xmm0, -400(%rbp)
	movq	$0, -384(%rbp)
	punpcklqdq	%xmm5, %xmm4
	movq	%rbx, %xmm5
	movhps	-5088(%rbp), %xmm5
	movaps	%xmm4, -5120(%rbp)
	movaps	%xmm5, -5088(%rbp)
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm5, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-192(%rbp), %xmm4
	movdqa	-176(%rbp), %xmm6
	leaq	48(%rax), %rdx
	leaq	-4432(%rbp), %rdi
	movq	%rax, -400(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -384(%rbp)
	movq	%rdx, -392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L935
	call	_ZdlPv@PLT
.L935:
	movdqa	-5040(%rbp), %xmm7
	movdqa	-5088(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movdqa	-5120(%rbp), %xmm5
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movq	$0, -384(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-192(%rbp), %xmm3
	movdqa	-176(%rbp), %xmm7
	leaq	48(%rax), %rdx
	leaq	-4240(%rbp), %rdi
	movq	%rax, -400(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -384(%rbp)
	movq	%rdx, -392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L936
	call	_ZdlPv@PLT
.L936:
	movq	-5064(%rbp), %rcx
	movq	-5056(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -4368(%rbp)
	je	.L937
.L1256:
	movq	-5056(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4928(%rbp)
	leaq	-4832(%rbp), %r12
	movq	$0, -4912(%rbp)
	leaq	-4432(%rbp), %r15
	movq	$0, -4832(%rbp)
	movq	$0, -4752(%rbp)
	movq	$0, -784(%rbp)
	movq	$0, -592(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rcx
	movq	%r15, %rdi
	leaq	-592(%rbp), %rax
	leaq	-4912(%rbp), %rdx
	pushq	%rax
	leaq	-4928(%rbp), %rsi
	leaq	-784(%rbp), %r9
	leaq	-4752(%rbp), %r8
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES6_EE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESG_
	movl	$48, %edi
	movq	-784(%rbp), %xmm0
	movq	-4832(%rbp), %xmm1
	movq	-4928(%rbp), %xmm2
	movq	$0, -384(%rbp)
	movhps	-592(%rbp), %xmm0
	movhps	-4752(%rbp), %xmm1
	movhps	-4912(%rbp), %xmm2
	movaps	%xmm0, -176(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -208(%rbp)
	movaps	%xmm1, -192(%rbp)
	movaps	%xmm0, -400(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-192(%rbp), %xmm5
	movdqa	-176(%rbp), %xmm6
	leaq	48(%rax), %rdx
	leaq	-976(%rbp), %rdi
	movq	%rax, -400(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -384(%rbp)
	movq	%rdx, -392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-400(%rbp), %rdi
	popq	%rbx
	popq	%r12
	testq	%rdi, %rdi
	je	.L938
	call	_ZdlPv@PLT
.L938:
	movq	-5072(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -4176(%rbp)
	je	.L939
.L1257:
	movq	-5064(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4936(%rbp)
	leaq	-4240(%rbp), %r12
	movq	$0, -4928(%rbp)
	leaq	-592(%rbp), %r15
	leaq	-208(%rbp), %rbx
	movq	$0, -4912(%rbp)
	movq	$0, -4832(%rbp)
	movq	$0, -4752(%rbp)
	movq	$0, -784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-784(%rbp), %rax
	pushq	%rax
	leaq	-4752(%rbp), %r9
	leaq	-4912(%rbp), %rcx
	leaq	-4832(%rbp), %r8
	leaq	-4928(%rbp), %rdx
	leaq	-4936(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES6_EE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESG_
	movl	$41, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-784(%rbp), %rdx
	movq	-4832(%rbp), %rsi
	movq	%r13, %rcx
	movq	-5016(%rbp), %rdi
	call	_ZN2v88internal29ObjectFromEntriesFastCase_310EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	leaq	-144(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	-4752(%rbp), %xmm0
	movq	%rax, %xmm7
	movq	-784(%rbp), %xmm3
	movq	$0, -576(%rbp)
	movq	-4912(%rbp), %xmm1
	movq	-4936(%rbp), %xmm2
	movhps	-784(%rbp), %xmm0
	punpcklqdq	%xmm7, %xmm3
	movhps	-4832(%rbp), %xmm1
	movhps	-4928(%rbp), %xmm2
	movaps	%xmm0, -176(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -208(%rbp)
	movaps	%xmm1, -192(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm0, -592(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3856(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-592(%rbp), %rdi
	popq	%r10
	popq	%r11
	testq	%rdi, %rdi
	je	.L940
	call	_ZdlPv@PLT
.L940:
	movq	-5048(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -392(%rbp)
	jne	.L1275
.L941:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L939
	.p2align 4,,10
	.p2align 3
.L1258:
	movq	-5144(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-208(%rbp), %rbx
	leaq	-4048(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movl	$2056, %r9d
	leaq	-201(%rbp), %rdx
	movaps	%xmm0, -400(%rbp)
	movw	%r9w, -204(%rbp)
	movl	$117769477, -208(%rbp)
	movb	$8, -202(%rbp)
	movq	$0, -384(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L944
	call	_ZdlPv@PLT
.L944:
	movq	(%rbx), %rax
	movl	$48, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movdqu	(%rax), %xmm3
	movq	$0, -384(%rbp)
	movaps	%xmm0, -176(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm1, -192(%rbp)
	movaps	%xmm0, -400(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm4
	movdqa	-192(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-176(%rbp), %xmm6
	leaq	48(%rax), %rdx
	movq	%rax, -400(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	leaq	-3664(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -384(%rbp)
	movq	%rdx, -392(%rbp)
	movq	%rax, -5088(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L945
	call	_ZdlPv@PLT
.L945:
	movq	-5096(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3792(%rbp)
	je	.L946
.L1259:
	movq	-5048(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-208(%rbp), %rbx
	leaq	-3856(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r13, %rdi
	leaq	-200(%rbp), %rdx
	movabsq	$506663788649710853, %rax
	movaps	%xmm0, -400(%rbp)
	movq	%rax, -208(%rbp)
	movq	$0, -384(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L947
	call	_ZdlPv@PLT
.L947:
	movq	(%rbx), %rax
	movq	-5256(%rbp), %rdi
	movq	56(%rax), %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	jmp	.L946
	.p2align 4,,10
	.p2align 3
.L1260:
	movq	-5096(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4928(%rbp)
	leaq	-4832(%rbp), %r12
	movq	$0, -4912(%rbp)
	movq	$0, -4832(%rbp)
	movq	$0, -4752(%rbp)
	movq	$0, -784(%rbp)
	movq	$0, -592(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rcx
	leaq	-592(%rbp), %rax
	pushq	%rax
	movq	-5088(%rbp), %rdi
	leaq	-784(%rbp), %r9
	leaq	-4752(%rbp), %r8
	leaq	-4912(%rbp), %rdx
	leaq	-4928(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES6_EE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESG_
	movl	$44, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-5016(%rbp), %rbx
	movq	-4752(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal14NewJSObject_55EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEE@PLT
	movl	$45, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -5120(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4752(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal24GetIteratorResultMap_225EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEE@PLT
	movl	$46, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -5096(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	-4752(%rbp), %rsi
	movq	-592(%rbp), %rdx
	call	_ZN2v88internal25IteratorBuiltinsAssembler11GetIteratorEPNS0_8compiler4NodeES4_PNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE@PLT
	movq	%r13, %rdi
	movq	%rax, -5184(%rbp)
	movq	%rdx, -5200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$49, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$80, %edi
	movq	-784(%rbp), %xmm0
	movq	-4832(%rbp), %xmm1
	movq	-4928(%rbp), %xmm2
	movq	$0, -384(%rbp)
	movhps	-592(%rbp), %xmm0
	movhps	-4752(%rbp), %xmm1
	movaps	%xmm0, -176(%rbp)
	movhps	-4912(%rbp), %xmm2
	movq	-5120(%rbp), %xmm0
	movaps	%xmm2, -208(%rbp)
	movhps	-5096(%rbp), %xmm0
	movaps	%xmm1, -192(%rbp)
	movaps	%xmm0, -160(%rbp)
	movq	-5184(%rbp), %xmm0
	movhps	-5200(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-192(%rbp), %xmm7
	movdqa	-176(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm2
	leaq	80(%rax), %rdx
	movq	%rax, -400(%rbp)
	movdqa	-144(%rbp), %xmm5
	movq	-5040(%rbp), %rdi
	movups	%xmm3, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rdx, -384(%rbp)
	movq	%rdx, -392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-400(%rbp), %rdi
	popq	%rsi
	popq	%r8
	testq	%rdi, %rdi
	je	.L949
	call	_ZdlPv@PLT
.L949:
	movq	-5216(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L948
	.p2align 4,,10
	.p2align 3
.L1261:
	movq	-5216(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-4832(%rbp), %r12
	movq	$0, -4984(%rbp)
	movq	$0, -4976(%rbp)
	leaq	-344(%rbp), %rbx
	leaq	-592(%rbp), %r15
	movq	$0, -4968(%rbp)
	movq	$0, -4960(%rbp)
	movq	$0, -4952(%rbp)
	movq	$0, -4944(%rbp)
	movq	$0, -4936(%rbp)
	movq	$0, -4928(%rbp)
	movq	$0, -4912(%rbp)
	movq	$0, -4832(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-4912(%rbp), %rax
	movq	-5040(%rbp), %rdi
	pushq	%r12
	leaq	-4952(%rbp), %r9
	leaq	-4968(%rbp), %rcx
	pushq	%rax
	leaq	-4928(%rbp), %rax
	leaq	-4976(%rbp), %rdx
	pushq	%rax
	leaq	-4936(%rbp), %rax
	leaq	-4984(%rbp), %rsi
	pushq	%rax
	leaq	-4944(%rbp), %rax
	leaq	-4960(%rbp), %r8
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES6_NS0_8JSObjectENS0_3MapENS0_10JSReceiverES6_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESJ_
	movq	-5000(%rbp), %rax
	addq	$48, %rsp
	movl	$24, %edi
	movq	$0, -392(%rbp)
	movq	%rax, -400(%rbp)
	movq	$0, -384(%rbp)
	movq	$0, -376(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	$0, 16(%rax)
	movq	%rdx, -376(%rbp)
	movq	%rdx, -384(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -360(%rbp)
	movq	%rax, -392(%rbp)
	movq	$0, -368(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerScopedExceptionHandlerC1EPNS1_13CodeAssemblerEPNS1_31CodeAssemblerParameterizedLabelIJNS0_6ObjectEEEE@PLT
	movq	-5016(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal42FromConstexpr6ATbool16ATconstexpr_bool_165EPNS0_8compiler18CodeAssemblerStateEb@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler35CodeAssemblerScopedExceptionHandlerD1Ev@PLT
	cmpq	$0, -336(%rbp)
	jne	.L1276
.L951:
	movq	-4912(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movq	-4936(%rbp), %xmm7
	movl	$80, %edi
	movq	-4952(%rbp), %xmm3
	movaps	%xmm0, -592(%rbp)
	movq	-4968(%rbp), %xmm4
	movq	-4984(%rbp), %xmm5
	movhps	-4832(%rbp), %xmm6
	movhps	-4928(%rbp), %xmm7
	movq	$0, -576(%rbp)
	movhps	-4944(%rbp), %xmm3
	movhps	-4960(%rbp), %xmm4
	movaps	%xmm6, -5184(%rbp)
	movhps	-4976(%rbp), %xmm5
	movaps	%xmm7, -5232(%rbp)
	movaps	%xmm3, -5200(%rbp)
	movaps	%xmm4, -5120(%rbp)
	movaps	%xmm5, -5248(%rbp)
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-192(%rbp), %xmm3
	movdqa	-176(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm4
	leaq	80(%rax), %rdx
	movq	%rax, -592(%rbp)
	movdqa	-144(%rbp), %xmm2
	movups	%xmm6, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm2, 64(%rax)
	leaq	-3088(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -576(%rbp)
	movq	%rdx, -584(%rbp)
	movq	%rax, -5096(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L953
	call	_ZdlPv@PLT
.L953:
	movdqa	-5248(%rbp), %xmm5
	movdqa	-5120(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$80, %edi
	movdqa	-5200(%rbp), %xmm3
	movdqa	-5232(%rbp), %xmm7
	movaps	%xmm0, -592(%rbp)
	movdqa	-5184(%rbp), %xmm4
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movq	$0, -576(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm2
	movq	%r15, %rsi
	movdqa	-192(%rbp), %xmm5
	movdqa	-176(%rbp), %xmm6
	movdqa	-160(%rbp), %xmm3
	leaq	80(%rax), %rdx
	movq	%rax, -592(%rbp)
	movdqa	-144(%rbp), %xmm7
	movups	%xmm2, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm7, 64(%rax)
	leaq	-1360(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -576(%rbp)
	movq	%rdx, -584(%rbp)
	movq	%rax, -5120(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L954
	call	_ZdlPv@PLT
.L954:
	movq	-5288(%rbp), %rcx
	movq	-5104(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	cmpq	$0, -3216(%rbp)
	je	.L955
.L1262:
	movq	-5136(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-3280(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$11, %edi
	movq	$0, -384(%rbp)
	movaps	%xmm0, -400(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	movabsq	$506382313673000197, %rcx
	movq	%rcx, (%rax)
	movl	$2055, %ecx
	leaq	11(%rax), %rdx
	movw	%cx, 8(%rax)
	movb	$8, 10(%rax)
	movq	%rax, -400(%rbp)
	movq	%rdx, -384(%rbp)
	movq	%rdx, -392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L956
	call	_ZdlPv@PLT
.L956:
	movq	(%rbx), %rax
	movl	$88, %edi
	movdqu	64(%rax), %xmm0
	movdqu	16(%rax), %xmm3
	movdqu	32(%rax), %xmm2
	movdqu	48(%rax), %xmm1
	movq	80(%rax), %rdx
	movdqu	(%rax), %xmm4
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -192(%rbp)
	movq	%rdx, -128(%rbp)
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -400(%rbp)
	movq	$0, -384(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-208(%rbp), %xmm2
	movdqa	-192(%rbp), %xmm5
	movdqa	-176(%rbp), %xmm6
	leaq	88(%rax), %rdx
	leaq	-1168(%rbp), %rdi
	movdqa	-160(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm7
	movq	%rcx, 80(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm7, 64(%rax)
	movq	%rax, -400(%rbp)
	movq	%rdx, -384(%rbp)
	movq	%rdx, -392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L957
	call	_ZdlPv@PLT
.L957:
	movq	-5024(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3024(%rbp)
	je	.L1277
.L1251:
	movq	-5104(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4992(%rbp)
	leaq	-344(%rbp), %rbx
	movq	$0, -4984(%rbp)
	leaq	-592(%rbp), %r15
	leaq	-4832(%rbp), %r12
	movq	$0, -4976(%rbp)
	movq	$0, -4968(%rbp)
	movq	$0, -4960(%rbp)
	movq	$0, -4952(%rbp)
	movq	$0, -4944(%rbp)
	movq	$0, -4936(%rbp)
	movq	$0, -4928(%rbp)
	movq	$0, -4912(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-4912(%rbp), %rax
	movq	-5096(%rbp), %rdi
	pushq	%rax
	leaq	-4928(%rbp), %rax
	leaq	-4960(%rbp), %r9
	pushq	%rax
	leaq	-4936(%rbp), %rax
	leaq	-4976(%rbp), %rcx
	pushq	%rax
	leaq	-4944(%rbp), %rax
	leaq	-4968(%rbp), %r8
	pushq	%rax
	leaq	-4952(%rbp), %rax
	leaq	-4984(%rbp), %rdx
	pushq	%rax
	leaq	-4992(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES6_NS0_8JSObjectENS0_3MapENS0_10JSReceiverES6_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESJ_
	addq	$48, %rsp
	movl	$51, %edx
	movq	%r14, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-784(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -5264(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5000(%rbp), %rax
	movl	$24, %edi
	movq	$0, -392(%rbp)
	movq	$0, -384(%rbp)
	movq	%rax, -400(%rbp)
	movq	$0, -376(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	$0, 16(%rax)
	movq	%rdx, -376(%rbp)
	movq	%rdx, -384(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -360(%rbp)
	movq	%rax, -392(%rbp)
	movq	$0, -368(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerScopedExceptionHandlerC1EPNS1_13CodeAssemblerEPNS1_31CodeAssemblerParameterizedLabelIJNS0_6ObjectEEEE@PLT
	movq	-5016(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4928(%rbp), %rax
	pushq	$0
	movl	$1, %r8d
	pushq	$0
	movq	-4936(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, -4752(%rbp)
	movq	-4912(%rbp), %rax
	movq	-5264(%rbp), %rcx
	movq	-4968(%rbp), %rsi
	movq	%rax, -4744(%rbp)
	leaq	-4752(%rbp), %rax
	movq	%rax, %rdx
	movq	%rax, -5200(%rbp)
	call	_ZN2v88internal25IteratorBuiltinsAssembler12IteratorStepENS0_8compiler5TNodeINS0_7ContextEEERKNS0_26TorqueStructIteratorRecordEPNS2_18CodeAssemblerLabelENS_4base8OptionalINS3_INS0_3MapEEEEESA_PNS2_21CodeAssemblerVariableE@PLT
	movq	%r12, %rdi
	movq	%rax, -5104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerScopedExceptionHandlerD1Ev@PLT
	cmpq	$0, -336(%rbp)
	popq	%rax
	popq	%rdx
	jne	.L1252
	leaq	-2896(%rbp), %rax
	leaq	-208(%rbp), %rbx
	movq	%rax, -5248(%rbp)
	leaq	-96(%rbp), %r12
.L959:
	movq	-4936(%rbp), %rax
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	-4928(%rbp), %xmm0
	movq	-4944(%rbp), %xmm1
	movq	$0, -576(%rbp)
	movq	-4960(%rbp), %xmm2
	movq	%rax, -112(%rbp)
	movq	-4976(%rbp), %xmm3
	movhps	-4912(%rbp), %xmm0
	movq	-4992(%rbp), %xmm4
	movhps	-4936(%rbp), %xmm1
	movq	-5104(%rbp), %rax
	movhps	-4952(%rbp), %xmm2
	movaps	%xmm0, -128(%rbp)
	movhps	-4968(%rbp), %xmm3
	movhps	-4984(%rbp), %xmm4
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -104(%rbp)
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -592(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2512(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -5232(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L961
	call	_ZdlPv@PLT
.L961:
	movq	-5280(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2704(%rbp), %rax
	cmpq	$0, -776(%rbp)
	movq	%rax, -5184(%rbp)
	jne	.L1278
.L962:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5264(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2832(%rbp)
	je	.L964
.L1263:
	movq	-5328(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-208(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-194(%rbp), %rdx
	movabsq	$506382313673000197, %rax
	movaps	%xmm0, -400(%rbp)
	movq	%rax, -208(%rbp)
	movl	$2055, %eax
	movw	%ax, -196(%rbp)
	movl	$134678535, -200(%rbp)
	movq	$0, -384(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5248(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L965
	call	_ZdlPv@PLT
.L965:
	movq	(%rbx), %rax
	movl	$88, %edi
	movdqu	64(%rax), %xmm0
	movdqu	16(%rax), %xmm3
	movdqu	32(%rax), %xmm2
	movdqu	48(%rax), %xmm1
	movq	104(%rax), %rdx
	movdqu	(%rax), %xmm4
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -192(%rbp)
	movq	%rdx, -128(%rbp)
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -400(%rbp)
	movq	$0, -384(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-208(%rbp), %xmm2
	movdqa	-192(%rbp), %xmm5
	movdqa	-176(%rbp), %xmm6
	leaq	88(%rax), %rdx
	leaq	-1168(%rbp), %rdi
	movdqa	-160(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm7
	movq	%rcx, 80(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm7, 64(%rax)
	movq	%rax, -400(%rbp)
	movq	%rdx, -384(%rbp)
	movq	%rdx, -392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L966
	call	_ZdlPv@PLT
.L966:
	movq	-5024(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L1264:
	movq	-5128(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-208(%rbp), %rsi
	movabsq	$506382313673000197, %rax
	leaq	-195(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rax, -208(%rbp)
	movaps	%xmm0, -400(%rbp)
	movl	$134678535, -200(%rbp)
	movb	$7, -196(%rbp)
	movq	$0, -384(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5184(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L968
	call	_ZdlPv@PLT
.L968:
	movq	(%rbx), %rax
	movl	$80, %edi
	movdqu	64(%rax), %xmm0
	movdqu	16(%rax), %xmm3
	movdqu	32(%rax), %xmm2
	movdqu	48(%rax), %xmm1
	movdqu	(%rax), %xmm4
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -400(%rbp)
	movq	$0, -384(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm2
	movq	%r13, %rsi
	movdqa	-192(%rbp), %xmm5
	movdqa	-176(%rbp), %xmm6
	movdqa	-160(%rbp), %xmm3
	leaq	80(%rax), %rdx
	movq	%rax, -400(%rbp)
	movdqa	-144(%rbp), %xmm7
	movups	%xmm2, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm7, 64(%rax)
	leaq	-2320(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -384(%rbp)
	movq	%rdx, -392(%rbp)
	movq	%rax, -5104(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L969
	call	_ZdlPv@PLT
.L969:
	movq	-5296(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L1266:
	movq	-5296(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4952(%rbp)
	leaq	-4832(%rbp), %r12
	movq	$0, -4944(%rbp)
	movq	$0, -4936(%rbp)
	movq	$0, -4928(%rbp)
	movq	$0, -4912(%rbp)
	movq	$0, -4832(%rbp)
	movq	$0, -4752(%rbp)
	movq	$0, -784(%rbp)
	movq	$0, -592(%rbp)
	movq	$0, -400(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-592(%rbp), %rax
	movq	-5104(%rbp), %rdi
	pushq	%r13
	leaq	-4936(%rbp), %rcx
	leaq	-4912(%rbp), %r9
	pushq	%rax
	leaq	-784(%rbp), %rax
	leaq	-4928(%rbp), %r8
	pushq	%rax
	leaq	-4752(%rbp), %rax
	leaq	-4944(%rbp), %rdx
	pushq	%rax
	leaq	-4952(%rbp), %rsi
	pushq	%r12
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES6_NS0_8JSObjectENS0_3MapENS0_10JSReceiverES6_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESJ_
	addq	$48, %rsp
	movl	$52, %edx
	movq	%r14, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4752(%rbp), %rsi
	movq	-5256(%rbp), %rdi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	cmpq	$0, -2064(%rbp)
	je	.L974
.L1267:
	movq	-5312(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-208(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r13, %rdi
	leaq	-197(%rbp), %rdx
	movaps	%xmm0, -400(%rbp)
	movabsq	$506382313673000197, %rax
	movq	%rax, -208(%rbp)
	movl	$2055, %eax
	movw	%ax, -200(%rbp)
	movb	$7, -198(%rbp)
	movq	$0, -384(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-400(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L975
	call	_ZdlPv@PLT
.L975:
	movq	(%r12), %rax
	leaq	-728(%rbp), %r12
	movq	8(%rax), %rsi
	movq	32(%rax), %rdx
	movq	40(%rax), %rdi
	movq	(%rax), %rcx
	movq	64(%rax), %r11
	movq	72(%rax), %r10
	movq	%rsi, -5360(%rbp)
	movq	%rdx, -5392(%rbp)
	movq	16(%rax), %rsi
	movq	56(%rax), %rdx
	movq	%rdi, -5408(%rbp)
	movq	48(%rax), %rdi
	movq	%rcx, -5344(%rbp)
	movq	24(%rax), %rcx
	movq	80(%rax), %rax
	movq	%rsi, -5376(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%rdi, -5312(%rbp)
	movq	%r14, %rdi
	movq	%rdx, -5296(%rbp)
	movl	$50, %edx
	movq	%r11, -5424(%rbp)
	movq	%r10, -5432(%rbp)
	movq	%rcx, -5280(%rbp)
	movq	%rax, -5328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$54, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-5000(%rbp), %rax
	movl	$24, %edi
	movq	$0, -776(%rbp)
	movq	$0, -768(%rbp)
	movq	%rax, -784(%rbp)
	movq	$0, -760(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	$0, 16(%rax)
	leaq	24(%rax), %rdx
	leaq	-784(%rbp), %rdi
	movq	%r14, %rsi
	movups	%xmm0, (%rax)
	movq	%rdi, %r15
	movq	%rdx, -760(%rbp)
	movq	%rdx, -768(%rbp)
	xorl	%edx, %edx
	movq	%rdi, -5264(%rbp)
	movq	%r12, %rdi
	movups	%xmm0, -744(%rbp)
	movq	%rax, -776(%rbp)
	movq	$0, -752(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerScopedExceptionHandlerC1EPNS1_13CodeAssemblerEPNS1_31CodeAssemblerParameterizedLabelIJNS0_6ObjectEEEE@PLT
	leaq	-592(%rbp), %r15
	movq	-5016(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movq	%r15, %rdi
	pushq	$0
	movq	-5296(%rbp), %r8
	movl	$1, %ecx
	movq	-5328(%rbp), %rdx
	movq	-5280(%rbp), %rsi
	call	_ZN2v88internal25IteratorBuiltinsAssembler13IteratorValueENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS_4base8OptionalINS3_INS0_3MapEEEEEPNS2_18CodeAssemblerLabelEPNS2_21CodeAssemblerVariableE@PLT
	movq	%r15, %rdi
	movq	%rax, -5416(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerScopedExceptionHandlerD1Ev@PLT
	cmpq	$0, -720(%rbp)
	popq	%r11
	popq	%rax
	jne	.L1253
	movq	-5344(%rbp), %xmm7
	leaq	-4752(%rbp), %rax
	movq	-5376(%rbp), %xmm6
	movq	-5312(%rbp), %xmm5
	movq	%rax, -5200(%rbp)
	movhps	-5360(%rbp), %xmm7
	movhps	-5280(%rbp), %xmm6
	movaps	%xmm7, -5344(%rbp)
	movhps	-5296(%rbp), %xmm5
	movq	-5392(%rbp), %xmm7
	movaps	%xmm6, -5360(%rbp)
	movq	-5424(%rbp), %xmm6
	movhps	-5408(%rbp), %xmm7
	movaps	%xmm5, -5392(%rbp)
	movhps	-5432(%rbp), %xmm6
	movaps	%xmm7, -5376(%rbp)
	movaps	%xmm6, -5408(%rbp)
.L976:
	movl	$53, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$56, %edx
	movq	%r14, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-5000(%rbp), %rax
	movl	$24, %edi
	movq	$0, -584(%rbp)
	movq	$0, -576(%rbp)
	leaq	-4832(%rbp), %r12
	movq	%rax, -592(%rbp)
	movq	$0, -568(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	$0, 16(%rax)
	leaq	24(%rax), %rdx
	leaq	-536(%rbp), %r9
	movq	%r14, %rsi
	movq	%r9, %rdi
	movups	%xmm0, (%rax)
	movq	%rdx, -568(%rbp)
	movq	%rdx, -576(%rbp)
	xorl	%edx, %edx
	movq	%r9, -5432(%rbp)
	movups	%xmm0, -552(%rbp)
	movq	%rax, -584(%rbp)
	movq	$0, -560(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerScopedExceptionHandlerC1EPNS1_13CodeAssemblerEPNS1_31CodeAssemblerParameterizedLabelIJNS0_6ObjectEEEE@PLT
	movq	-5280(%rbp), %rsi
	movq	-5016(%rbp), %rdi
	movq	-5416(%rbp), %rdx
	call	_ZN2v88internal20LoadKeyValuePair_268EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -5424(%rbp)
	movq	%rdx, -5296(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerScopedExceptionHandlerD1Ev@PLT
	cmpq	$0, -528(%rbp)
	jne	.L1279
.L978:
	movl	$55, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	movq	-5312(%rbp), %xmm6
	movhps	-5424(%rbp), %xmm6
	movaps	%xmm6, -5312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$57, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-5000(%rbp), %rax
	movl	$24, %edi
	movq	$0, -392(%rbp)
	movq	$0, -384(%rbp)
	movq	%rax, -400(%rbp)
	movq	$0, -376(%rbp)
	call	_Znwm@PLT
	pxor	%xmm1, %xmm1
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	$0, 16(%rax)
	leaq	-344(%rbp), %r11
	leaq	24(%rax), %rdx
	movq	%r14, %rsi
	movq	%r11, %rdi
	movups	%xmm1, (%rax)
	movq	%rdx, -376(%rbp)
	movq	%rdx, -384(%rbp)
	xorl	%edx, %edx
	movq	%r11, -5440(%rbp)
	movups	%xmm1, -360(%rbp)
	movq	%rax, -392(%rbp)
	movq	$0, -368(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5200(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerScopedExceptionHandlerC1EPNS1_13CodeAssemblerEPNS1_31CodeAssemblerParameterizedLabelIJNS0_6ObjectEEEE@PLT
	leaq	-4928(%rbp), %r10
	movq	-5016(%rbp), %rsi
	movq	%r10, %rdi
	movq	%r10, -5432(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5432(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$786, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-5432(%rbp), %r10
	movq	-4832(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$3, %edi
	xorl	%esi, %esi
	movq	-5432(%rbp), %r10
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rdi
	movl	$1, %ecx
	pushq	%rbx
	movq	-5280(%rbp), %r9
	movq	%r10, %rdi
	movq	%rax, -4912(%rbp)
	movq	-4816(%rbp), %rax
	movdqa	-5312(%rbp), %xmm6
	movq	%r10, -5280(%rbp)
	movq	%rax, -4904(%rbp)
	movq	-5296(%rbp), %rax
	movaps	%xmm6, -208(%rbp)
	movq	%rax, -192(%rbp)
	leaq	-4912(%rbp), %rax
	movq	%rax, %rdx
	movq	%rax, -5448(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-5280(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-5200(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerScopedExceptionHandlerD1Ev@PLT
	cmpq	$0, -336(%rbp)
	popq	%r9
	movq	-5440(%rbp), %r11
	popq	%r10
	jne	.L1280
.L980:
	movl	$49, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movdqa	-5344(%rbp), %xmm4
	movdqa	-5360(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movdqa	-5376(%rbp), %xmm5
	movdqa	-5392(%rbp), %xmm6
	movl	$80, %edi
	movaps	%xmm0, -4752(%rbp)
	movdqa	-5408(%rbp), %xmm3
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movq	$0, -4736(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm7
	movdqa	-192(%rbp), %xmm4
	movdqa	-176(%rbp), %xmm2
	movdqa	-160(%rbp), %xmm5
	leaq	80(%rax), %rdx
	movq	%rax, -4752(%rbp)
	movdqa	-144(%rbp), %xmm6
	movq	-5040(%rbp), %rdi
	movups	%xmm7, (%rax)
	movq	-5200(%rbp), %rsi
	movups	%xmm4, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	movq	%rdx, -4736(%rbp)
	movq	%rdx, -4744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L982
	call	_ZdlPv@PLT
.L982:
	movq	-5216(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5264(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	cmpq	$0, -1872(%rbp)
	je	.L983
.L1268:
	movq	-5160(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-1936(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$2055, %r8d
	pxor	%xmm0, %xmm0
	movabsq	$506382313673000197, %rax
	leaq	-208(%rbp), %rsi
	leaq	-194(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rax, -208(%rbp)
	movw	%r8w, -196(%rbp)
	movaps	%xmm0, -400(%rbp)
	movl	$117901319, -200(%rbp)
	movq	$0, -384(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L984
	call	_ZdlPv@PLT
.L984:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	40(%rax), %rsi
	movq	8(%rax), %rcx
	movq	24(%rax), %rbx
	movq	72(%rax), %rdx
	movq	(%rax), %r15
	movq	48(%rax), %r12
	movq	%rsi, -5296(%rbp)
	movq	56(%rax), %rsi
	movq	%rcx, -5200(%rbp)
	movq	%rbx, -5264(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -5312(%rbp)
	movq	64(%rax), %rsi
	movq	32(%rax), %rbx
	movq	%rdx, -5344(%rbp)
	movl	$54, %edx
	movq	%rsi, -5328(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%rcx, -5216(%rbp)
	movq	%rbx, -5280(%rbp)
	movq	104(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %xmm0
	movl	$88, %edi
	movq	$0, -384(%rbp)
	movhps	-5200(%rbp), %xmm0
	movq	%rbx, -128(%rbp)
	movaps	%xmm0, -208(%rbp)
	movq	-5216(%rbp), %xmm0
	movhps	-5264(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-5280(%rbp), %xmm0
	movhps	-5296(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	%r12, %xmm0
	movhps	-5312(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-5328(%rbp), %xmm0
	movhps	-5344(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-208(%rbp), %xmm3
	movdqa	-192(%rbp), %xmm7
	movdqa	-176(%rbp), %xmm4
	leaq	88(%rax), %rdx
	leaq	-1168(%rbp), %rdi
	movdqa	-160(%rbp), %xmm2
	movdqa	-144(%rbp), %xmm5
	movq	%rcx, 80(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rax, -400(%rbp)
	movq	%rdx, -384(%rbp)
	movq	%rdx, -392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L985
	call	_ZdlPv@PLT
.L985:
	movq	-5024(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1680(%rbp)
	je	.L986
.L1269:
	movq	-5168(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-1744(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$2056, %edi
	pxor	%xmm0, %xmm0
	movabsq	$506382313673000197, %rax
	movw	%di, -196(%rbp)
	leaq	-208(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-194(%rbp), %rdx
	movq	%rax, -208(%rbp)
	movaps	%xmm0, -400(%rbp)
	movl	$134678535, -200(%rbp)
	movq	$0, -384(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L987
	call	_ZdlPv@PLT
.L987:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	40(%rax), %rsi
	movq	8(%rax), %rcx
	movq	24(%rax), %rbx
	movq	72(%rax), %rdx
	movq	(%rax), %r15
	movq	48(%rax), %r12
	movq	%rsi, -5296(%rbp)
	movq	56(%rax), %rsi
	movq	%rcx, -5200(%rbp)
	movq	%rbx, -5264(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -5312(%rbp)
	movq	64(%rax), %rsi
	movq	32(%rax), %rbx
	movq	%rdx, -5344(%rbp)
	movl	$56, %edx
	movq	%rsi, -5328(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%rcx, -5216(%rbp)
	movq	%rbx, -5280(%rbp)
	movq	104(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %xmm0
	movl	$88, %edi
	movq	$0, -384(%rbp)
	movhps	-5200(%rbp), %xmm0
	movq	%rbx, -128(%rbp)
	movaps	%xmm0, -208(%rbp)
	movq	-5216(%rbp), %xmm0
	movhps	-5264(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-5280(%rbp), %xmm0
	movhps	-5296(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	%r12, %xmm0
	movhps	-5312(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-5328(%rbp), %xmm0
	movhps	-5344(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-208(%rbp), %xmm6
	movdqa	-192(%rbp), %xmm3
	movdqa	-176(%rbp), %xmm7
	leaq	88(%rax), %rdx
	leaq	-1168(%rbp), %rdi
	movdqa	-160(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm2
	movq	%rcx, 80(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm2, 64(%rax)
	movq	%rax, -400(%rbp)
	movq	%rdx, -384(%rbp)
	movq	%rdx, -392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L988
	call	_ZdlPv@PLT
.L988:
	movq	-5024(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1488(%rbp)
	je	.L989
.L1270:
	movq	-5152(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-1552(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC4(%rip), %xmm0
	movl	$2056, %esi
	movq	%r13, %rdi
	movw	%si, -192(%rbp)
	leaq	-190(%rbp), %rdx
	leaq	-208(%rbp), %rsi
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -400(%rbp)
	movq	$0, -384(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L990
	call	_ZdlPv@PLT
.L990:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	40(%rax), %rsi
	movq	8(%rax), %rcx
	movq	24(%rax), %rbx
	movq	72(%rax), %rdx
	movq	(%rax), %r15
	movq	48(%rax), %r12
	movq	%rsi, -5296(%rbp)
	movq	56(%rax), %rsi
	movq	%rcx, -5200(%rbp)
	movq	%rbx, -5264(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -5312(%rbp)
	movq	64(%rax), %rsi
	movq	32(%rax), %rbx
	movq	%rdx, -5344(%rbp)
	movl	$57, %edx
	movq	%rsi, -5328(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%rcx, -5216(%rbp)
	movq	%rbx, -5280(%rbp)
	movq	136(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %xmm0
	movl	$88, %edi
	movq	$0, -384(%rbp)
	movhps	-5200(%rbp), %xmm0
	movq	%rbx, -128(%rbp)
	movaps	%xmm0, -208(%rbp)
	movq	-5216(%rbp), %xmm0
	movhps	-5264(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-5280(%rbp), %xmm0
	movhps	-5296(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	%r12, %xmm0
	movhps	-5312(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-5328(%rbp), %xmm0
	movhps	-5344(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-208(%rbp), %xmm5
	movdqa	-192(%rbp), %xmm6
	movdqa	-176(%rbp), %xmm3
	leaq	88(%rax), %rdx
	leaq	-1168(%rbp), %rdi
	movdqa	-160(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm4
	movq	%rcx, 80(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm4, 64(%rax)
	movq	%rax, -400(%rbp)
	movq	%rdx, -384(%rbp)
	movq	%rdx, -392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L991
	call	_ZdlPv@PLT
.L991:
	movq	-5024(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1296(%rbp)
	je	.L992
.L1271:
	movq	-5288(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4952(%rbp)
	movq	$0, -4944(%rbp)
	movq	$0, -4936(%rbp)
	movq	$0, -4928(%rbp)
	movq	$0, -4912(%rbp)
	movq	$0, -4832(%rbp)
	movq	$0, -4752(%rbp)
	movq	$0, -784(%rbp)
	movq	$0, -592(%rbp)
	movq	$0, -400(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-592(%rbp), %rax
	movq	-5120(%rbp), %rdi
	pushq	%r13
	leaq	-4936(%rbp), %rcx
	leaq	-4912(%rbp), %r9
	pushq	%rax
	leaq	-784(%rbp), %rax
	leaq	-4928(%rbp), %r8
	pushq	%rax
	leaq	-4752(%rbp), %rax
	leaq	-4944(%rbp), %rdx
	pushq	%rax
	leaq	-4832(%rbp), %rax
	leaq	-4952(%rbp), %rsi
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES6_NS0_8JSObjectENS0_3MapENS0_10JSReceiverES6_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESJ_
	addq	$48, %rsp
	movl	$59, %edx
	movq	%r14, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4752(%rbp), %rsi
	movq	-5256(%rbp), %rdi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	cmpq	$0, -1104(%rbp)
	je	.L993
.L1272:
	movq	-5024(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-1168(%rbp), %r15
	xorl	%r12d, %r12d
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$11, %edi
	movq	$0, -384(%rbp)
	movaps	%xmm0, -400(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movabsq	$506382313673000197, %rcx
	movq	%rcx, (%rax)
	movl	$2055, %ecx
	leaq	11(%rax), %rdx
	movw	%cx, 8(%rax)
	movb	$8, 10(%rax)
	movq	%rax, -400(%rbp)
	movq	%rdx, -384(%rbp)
	movq	%rdx, -392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L994
	call	_ZdlPv@PLT
.L994:
	movq	(%rbx), %rax
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	leaq	-592(%rbp), %r15
	movq	80(%rax), %rdx
	movq	64(%rax), %xmm0
	movq	24(%rax), %rbx
	testq	%rdx, %rdx
	movhps	72(%rax), %xmm0
	cmovne	%rdx, %r12
	movl	$61, %edx
	movaps	%xmm0, -5200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-5016(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movdqa	-5200(%rbp), %xmm0
	movq	%r15, %rdi
	movaps	%xmm0, -400(%rbp)
	call	_ZN2v88internal25IteratorBuiltinsAssembler24IteratorCloseOnExceptionEPNS0_8compiler4NodeERKNS0_26TorqueStructIteratorRecordENS2_5TNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -912(%rbp)
	je	.L996
.L1273:
	movq	-5072(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4928(%rbp)
	leaq	-976(%rbp), %r12
	movq	$0, -4912(%rbp)
	movq	$0, -4832(%rbp)
	movq	$0, -4752(%rbp)
	movq	$0, -784(%rbp)
	movq	$0, -592(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-592(%rbp), %rax
	pushq	%rax
	leaq	-784(%rbp), %r9
	leaq	-4832(%rbp), %rcx
	leaq	-4752(%rbp), %r8
	leaq	-4912(%rbp), %rdx
	leaq	-4928(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES6_EE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESG_
	movl	$65, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-5016(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$96, %edx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	-4752(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1265:
	movq	-5280(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-208(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r13, %rdi
	leaq	-194(%rbp), %rdx
	movabsq	$506382313673000197, %rax
	movaps	%xmm0, -400(%rbp)
	movq	%rax, -208(%rbp)
	movl	$1799, %eax
	movw	%ax, -196(%rbp)
	movl	$134678535, -200(%rbp)
	movq	$0, -384(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5232(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-400(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L971
	call	_ZdlPv@PLT
.L971:
	movq	(%r12), %rax
	leaq	-120(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movdqu	64(%rax), %xmm0
	movdqu	(%rax), %xmm4
	movdqu	16(%rax), %xmm3
	movdqu	32(%rax), %xmm2
	movdqu	48(%rax), %xmm1
	movq	104(%rax), %rax
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -208(%rbp)
	movq	%rax, -128(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -400(%rbp)
	movq	$0, -384(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2128(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -5128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L972
	call	_ZdlPv@PLT
.L972:
	movq	-5312(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L970
	.p2align 4,,10
	.p2align 3
.L1275:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	-4936(%rbp), %rdx
	movq	-784(%rbp), %rax
	movaps	%xmm0, -592(%rbp)
	movq	$0, -576(%rbp)
	movq	%rdx, -208(%rbp)
	movq	-4928(%rbp), %rdx
	movq	%rax, -168(%rbp)
	movq	%rdx, -200(%rbp)
	movq	-4912(%rbp), %rdx
	movq	%rax, -160(%rbp)
	movq	%rdx, -192(%rbp)
	movq	-4832(%rbp), %rdx
	movq	%rdx, -184(%rbp)
	movq	-4752(%rbp), %rdx
	movq	%rdx, -176(%rbp)
	leaq	-152(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-4048(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L942
	call	_ZdlPv@PLT
.L942:
	movq	-5144(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L1276:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	$0, -4752(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4752(%rbp), %rax
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EE
	movq	-4984(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$88, %edi
	movaps	%xmm0, -784(%rbp)
	movq	%rax, -208(%rbp)
	movq	-4976(%rbp), %rax
	movq	$0, -768(%rbp)
	movq	%rax, -200(%rbp)
	movq	-4968(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-4960(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-4952(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-4944(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-4936(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-4928(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-4912(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-4832(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-4752(%rbp), %rax
	movq	%rax, -128(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movdqa	-208(%rbp), %xmm7
	leaq	-3280(%rbp), %rdi
	movdqa	-192(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm5
	leaq	88(%rax), %rdx
	leaq	-784(%rbp), %rsi
	movups	%xmm7, (%rax)
	movdqa	-176(%rbp), %xmm7
	movups	%xmm4, 16(%rax)
	movdqa	-160(%rbp), %xmm4
	movq	%rcx, 80(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rax, -784(%rbp)
	movq	%rdx, -768(%rbp)
	movq	%rdx, -776(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L952
	call	_ZdlPv@PLT
.L952:
	movq	-5136(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L951
	.p2align 4,,10
	.p2align 3
.L1278:
	movq	-5264(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-4992(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movq	-4912(%rbp), %rdx
	movq	-4936(%rbp), %rax
	movq	-4928(%rbp), %rcx
	movq	%r15, %rdi
	movaps	%xmm0, -592(%rbp)
	movq	%rsi, -208(%rbp)
	movq	-4984(%rbp), %rsi
	movq	%rdx, -136(%rbp)
	movq	%rsi, -200(%rbp)
	movq	-4976(%rbp), %rsi
	movq	%rdx, -120(%rbp)
	leaq	-104(%rbp), %rdx
	movq	%rsi, -192(%rbp)
	movq	-4968(%rbp), %rsi
	movq	%rax, -152(%rbp)
	movq	%rsi, -184(%rbp)
	movq	-4960(%rbp), %rsi
	movq	%rcx, -144(%rbp)
	movq	%rsi, -176(%rbp)
	movq	-4952(%rbp), %rsi
	movq	%rcx, -128(%rbp)
	movq	%rsi, -168(%rbp)
	movq	-4944(%rbp), %rsi
	movq	%rax, -112(%rbp)
	movq	%rsi, -160(%rbp)
	movq	%rbx, %rsi
	movq	$0, -576(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5184(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L963
	call	_ZdlPv@PLT
.L963:
	movq	-5128(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L1252:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	$0, -4832(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	-208(%rbp), %rbx
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EE
	leaq	-96(%rbp), %r12
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	-4936(%rbp), %xmm6
	movq	-5200(%rbp), %rdi
	movq	%r12, %rdx
	movaps	%xmm0, -4752(%rbp)
	movq	-4928(%rbp), %xmm1
	movq	-4944(%rbp), %xmm2
	movq	$0, -4736(%rbp)
	movq	-4960(%rbp), %xmm3
	movhps	-4832(%rbp), %xmm6
	movq	-4976(%rbp), %xmm4
	movq	-4992(%rbp), %xmm5
	movhps	-4912(%rbp), %xmm1
	movhps	-4936(%rbp), %xmm2
	movaps	%xmm6, -112(%rbp)
	movhps	-4952(%rbp), %xmm3
	movhps	-4968(%rbp), %xmm4
	movaps	%xmm1, -128(%rbp)
	movhps	-4984(%rbp), %xmm5
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2896(%rbp), %rax
	movq	-5200(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -5248(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L960
	call	_ZdlPv@PLT
.L960:
	movq	-5328(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L959
	.p2align 4,,10
	.p2align 3
.L1280:
	movq	-5328(%rbp), %xmm0
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	-5424(%rbp), %xmm2
	movq	-5200(%rbp), %rdi
	movl	$1, %r8d
	movq	%r11, -5432(%rbp)
	movhps	-5416(%rbp), %xmm0
	movhps	-5296(%rbp), %xmm2
	movaps	%xmm0, -5328(%rbp)
	movaps	%xmm2, -5280(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5200(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-5432(%rbp), %r11
	movq	%r14, %rdi
	movq	$0, -4912(%rbp)
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-5448(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EE
	movdqa	-5280(%rbp), %xmm2
	movq	%rbx, %rsi
	movq	-5296(%rbp), %rax
	movdqa	-5344(%rbp), %xmm7
	movdqa	-5360(%rbp), %xmm6
	pxor	%xmm1, %xmm1
	leaq	-64(%rbp), %rdx
	movdqa	-5376(%rbp), %xmm3
	movq	%rax, -80(%rbp)
	movq	%r12, %rdi
	movdqa	-5392(%rbp), %xmm4
	movdqa	-5408(%rbp), %xmm5
	movaps	%xmm2, -112(%rbp)
	movdqa	-5328(%rbp), %xmm0
	movdqa	-5312(%rbp), %xmm2
	movaps	%xmm7, -208(%rbp)
	movq	-4912(%rbp), %rax
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movq	%rax, -72(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm1, -4832(%rbp)
	movq	$0, -4816(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1552(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L981
	call	_ZdlPv@PLT
.L981:
	movq	-5152(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-5200(%rbp), %rbx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L980
	.p2align 4,,10
	.p2align 3
.L1279:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	-5432(%rbp), %r9
	movq	%r14, %rdi
	movq	$0, -4832(%rbp)
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EE
	movq	-5416(%rbp), %rax
	movq	%rbx, %rsi
	movdqa	-5344(%rbp), %xmm7
	movq	-5328(%rbp), %xmm1
	movdqa	-5360(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	leaq	-96(%rbp), %rdx
	movdqa	-5376(%rbp), %xmm3
	movdqa	-5392(%rbp), %xmm4
	movq	%rax, -112(%rbp)
	movdqa	-5408(%rbp), %xmm5
	movq	-5200(%rbp), %rdi
	movaps	%xmm7, -208(%rbp)
	movq	%rax, %xmm7
	movq	-4832(%rbp), %rax
	punpcklqdq	%xmm7, %xmm1
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movq	%rax, -104(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -4752(%rbp)
	movq	$0, -4736(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5200(%rbp), %rsi
	leaq	-1744(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L979
	call	_ZdlPv@PLT
.L979:
	movq	-5168(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L1253:
	movq	-5344(%rbp), %xmm2
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	-5376(%rbp), %xmm5
	movl	$1, %r8d
	movq	%r13, %rdi
	movq	-5392(%rbp), %xmm6
	movq	-5312(%rbp), %xmm3
	movhps	-5360(%rbp), %xmm2
	movq	-5424(%rbp), %xmm7
	movhps	-5408(%rbp), %xmm6
	movhps	-5280(%rbp), %xmm5
	movaps	%xmm2, -5344(%rbp)
	movhps	-5296(%rbp), %xmm3
	movhps	-5432(%rbp), %xmm7
	movaps	%xmm5, -5360(%rbp)
	movaps	%xmm6, -5376(%rbp)
	movaps	%xmm3, -5392(%rbp)
	movaps	%xmm7, -5408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	$0, -4752(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4752(%rbp), %rax
	movq	-5264(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, -5200(%rbp)
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EE
	movq	-5296(%rbp), %rax
	movq	%rbx, %rsi
	movq	-5328(%rbp), %xmm1
	movdqa	-5344(%rbp), %xmm2
	movdqa	-5360(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	leaq	-96(%rbp), %rdx
	movdqa	-5376(%rbp), %xmm6
	movq	%rax, -112(%rbp)
	punpcklqdq	%xmm1, %xmm1
	movq	%r15, %rdi
	movdqa	-5392(%rbp), %xmm3
	movdqa	-5408(%rbp), %xmm7
	movaps	%xmm2, -208(%rbp)
	movq	-4752(%rbp), %rax
	movaps	%xmm5, -192(%rbp)
	movaps	%xmm6, -176(%rbp)
	movq	%rax, -104(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -592(%rbp)
	movq	$0, -576(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1936(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L977
	call	_ZdlPv@PLT
.L977:
	movq	-5160(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L976
.L1274:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22466:
	.size	_ZN2v88internal26ObjectFromEntriesAssembler29GenerateObjectFromEntriesImplEv, .-_ZN2v88internal26ObjectFromEntriesAssembler29GenerateObjectFromEntriesImplEv
	.section	.rodata._ZN2v88internal8Builtins26Generate_ObjectFromEntriesEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/object-fromentries-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins26Generate_ObjectFromEntriesEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"ObjectFromEntries"
	.section	.text._ZN2v88internal8Builtins26Generate_ObjectFromEntriesEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins26Generate_ObjectFromEntriesEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins26Generate_ObjectFromEntriesEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins26Generate_ObjectFromEntriesEPNS0_8compiler18CodeAssemblerStateE:
.LFB22462:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$431, %ecx
	leaq	.LC5(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$845, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L1285
.L1282:
	movq	%r13, %rdi
	call	_ZN2v88internal26ObjectFromEntriesAssembler29GenerateObjectFromEntriesImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1286
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1285:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L1282
.L1286:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22462:
	.size	_ZN2v88internal8Builtins26Generate_ObjectFromEntriesEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins26Generate_ObjectFromEntriesEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal29ObjectFromEntriesFastCase_310EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal29ObjectFromEntriesFastCase_310EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, @function
_GLOBAL__sub_I__ZN2v88internal29ObjectFromEntriesFastCase_310EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE:
.LFB29500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29500:
	.size	_GLOBAL__sub_I__ZN2v88internal29ObjectFromEntriesFastCase_310EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, .-_GLOBAL__sub_I__ZN2v88internal29ObjectFromEntriesFastCase_310EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal29ObjectFromEntriesFastCase_310EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.weak	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.byte	5
	.byte	5
	.byte	5
	.byte	7
	.byte	8
	.byte	8
	.byte	7
	.byte	7
	.byte	7
	.byte	8
	.byte	7
	.byte	8
	.byte	8
	.byte	8
	.byte	7
	.byte	8
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
