	.file	"typed-array-slice-tq-csa.cc"
	.text
	.section	.rodata._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0:
.LFB29452:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	movabsq	$384307168202282325, %rax
	cmpq	%rax, %rdx
	ja	.L19
	pxor	%xmm0, %xmm0
	leaq	(%rdx,%rdx,2), %r14
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	%rdx, %r12
	movups	%xmm0, 8(%rdi)
	salq	$3, %r14
	xorl	%esi, %esi
	movq	$0, 24(%rdi)
	testq	%rdx, %rdx
	je	.L7
	movq	%r14, %rdi
	call	_Znwm@PLT
	leaq	-1(%r12), %rdx
	leaq	(%rax,%r14), %rsi
	movq	%rax, 8(%rbx)
	movq	%rsi, 24(%rbx)
	cmpq	$2, %rdx
	jbe	.L8
	movq	%r12, %rdi
	movq	%r12, %rcx
	pxor	%xmm0, %xmm0
	movq	%rax, %rdx
	shrq	%rdi
	andq	$-2, %rcx
	addq	%rdi, %rcx
	salq	$4, %rcx
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L5:
	movups	%xmm0, (%rdx)
	addq	$48, %rdx
	movups	%xmm0, -32(%rdx)
	movups	%xmm0, -16(%rdx)
	cmpq	%rcx, %rdx
	jne	.L5
	movq	%r12, %rdx
	movq	%r12, %rcx
	andq	$-2, %rdx
	andl	$1, %ecx
	leaq	(%rdx,%rdx,2), %rdi
	leaq	(%rax,%rdi,8), %rax
	cmpq	%rdx, %r12
	je	.L7
.L4:
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	cmpq	$1, %rcx
	je	.L7
	movq	$0, 40(%rax)
	movups	%xmm0, 24(%rax)
	cmpq	$2, %rcx
	je	.L7
	movq	$0, 64(%rax)
	movups	%xmm0, 48(%rax)
.L7:
	movq	%rsi, 16(%rbx)
	pxor	%xmm0, %xmm0
	leaq	56(%rbx), %rdi
	movq	%r13, %rsi
	movups	%xmm0, 32(%rbx)
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	$0, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movq	%r12, %rcx
	jmp	.L4
.L19:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29452:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev,"axG",@progbits,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev:
.LFB22413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$56, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L21
	call	_ZdlPv@PLT
.L21:
	movq	16(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	%r12, %r13
	je	.L22
	.p2align 4,,10
	.p2align 3
.L26:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L23
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L26
.L24:
	movq	8(%rbx), %r12
.L22:
	testq	%r12, %r12
	je	.L20
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L26
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L20:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22413:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev
	.set	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.section	.rodata._ZN2v88internal16kBuiltinName_364EPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"%TypedArray%.prototype.slice"
	.section	.text._ZN2v88internal16kBuiltinName_364EPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal16kBuiltinName_364EPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal16kBuiltinName_364EPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal16kBuiltinName_364EPNS0_8compiler18CodeAssemblerStateE:
.LFB22410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-272(%rbp), %r15
	leaq	-240(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-280(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-184(%rbp), %r12
	movq	%r13, %rsi
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -280(%rbp)
	movq	%rdi, -240(%rbp)
	movq	%r12, %rdi
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -192(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L33
	call	_ZdlPv@PLT
.L33:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L34
	call	_ZdlPv@PLT
.L34:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L35
	call	_ZdlPv@PLT
.L35:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L36
	.p2align 4,,10
	.p2align 3
.L40:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L37
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L40
.L38:
	movq	-232(%rbp), %r12
.L36:
	testq	%r12, %r12
	je	.L41
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L41:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L56
	addq	$248, %rsp
	leaq	.LC1(%rip), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L40
	jmp	.L38
.L56:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22410:
	.size	_ZN2v88internal16kBuiltinName_364EPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal16kBuiltinName_364EPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal12SlowCopy_366EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEES8_NS4_INS0_7IntPtrTEEESA_.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../../deps/v8/../../deps/v8/src/builtins/typed-array-slice.tq"
	.section	.text._ZN2v88internal12SlowCopy_366EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEES8_NS4_INS0_7IntPtrTEEESA_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal12SlowCopy_366EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEES8_NS4_INS0_7IntPtrTEEESA_
	.type	_ZN2v88internal12SlowCopy_366EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEES8_NS4_INS0_7IntPtrTEEESA_, @function
_ZN2v88internal12SlowCopy_366EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEES8_NS4_INS0_7IntPtrTEEESA_:
.LFB22453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-1056(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1096(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-232(%rbp), %rbx
	subq	$1160, %rsp
	movq	%r9, -1160(%rbp)
	movq	%rdx, -1184(%rbp)
	movq	%rcx, -1152(%rbp)
	movq	%rsi, -1168(%rbp)
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	movq	%rdi, -1096(%rbp)
	movq	%rdi, -1056(%rbp)
	movl	$120, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1112(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1096(%rbp), %rax
	movl	$120, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	$0, 112(%rax)
	leaq	120(%rax), %rdx
	movq	%r12, %rsi
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1128(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1096(%rbp), %rax
	movl	$120, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1120(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1096(%rbp), %rax
	movl	$120, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -1136(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1096(%rbp), %rax
	movl	$120, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1168(%rbp), %r11
	pxor	%xmm0, %xmm0
	movq	-1184(%rbp), %r10
	movq	-1160(%rbp), %r9
	movq	-1152(%rbp), %rax
	movl	$40, %edi
	movq	%r13, -72(%rbp)
	movq	%r11, -96(%rbp)
	leaq	-1088(%rbp), %r13
	movq	%r10, -88(%rbp)
	movq	%r9, -64(%rbp)
	movaps	%xmm0, -1088(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1072(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm1
	movdqa	-80(%rbp), %xmm2
	leaq	40(%rax), %rdx
	movq	%rax, -1088(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm1, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L58
	call	_ZdlPv@PLT
.L58:
	movq	-1112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -992(%rbp)
	jne	.L170
	cmpq	$0, -800(%rbp)
	jne	.L171
.L63:
	cmpq	$0, -608(%rbp)
	jne	.L172
.L65:
	cmpq	$0, -416(%rbp)
	leaq	-288(%rbp), %r14
	jne	.L173
.L68:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1072(%rbp)
	movaps	%xmm0, -1088(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$84346631, (%rax)
	leaq	5(%rax), %rdx
	movb	$5, 4(%rax)
	movq	%rax, -1088(%rbp)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L71
	call	_ZdlPv@PLT
.L71:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L72
	call	_ZdlPv@PLT
.L72:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L73
	.p2align 4,,10
	.p2align 3
.L77:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L74
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L77
.L75:
	movq	-280(%rbp), %r13
.L73:
	testq	%r13, %r13
	je	.L78
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L78:
	movq	-1136(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L79
	call	_ZdlPv@PLT
.L79:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L80
	.p2align 4,,10
	.p2align 3
.L84:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L81
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L84
.L82:
	movq	-472(%rbp), %r13
.L80:
	testq	%r13, %r13
	je	.L85
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L85:
	movq	-1120(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L86
	call	_ZdlPv@PLT
.L86:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L87
	.p2align 4,,10
	.p2align 3
.L91:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L88
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L91
.L89:
	movq	-664(%rbp), %r13
.L87:
	testq	%r13, %r13
	je	.L92
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L92:
	movq	-1128(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L93
	call	_ZdlPv@PLT
.L93:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L94
	.p2align 4,,10
	.p2align 3
.L98:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L95
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L98
.L96:
	movq	-856(%rbp), %r13
.L94:
	testq	%r13, %r13
	je	.L99
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L99:
	movq	-1112(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L100
	call	_ZdlPv@PLT
.L100:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L101
	.p2align 4,,10
	.p2align 3
.L105:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L102
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L105
.L103:
	movq	-1048(%rbp), %r13
.L101:
	testq	%r13, %r13
	je	.L106
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L106:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L174
	addq	$1160, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L105
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L95:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L98
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L88:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L91
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L74:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L77
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L81:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L84
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L170:
	movq	-1112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1072(%rbp)
	movaps	%xmm0, -1088(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$84346631, (%rax)
	leaq	5(%rax), %rdx
	movb	$5, 4(%rax)
	movq	%rax, -1088(%rbp)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1088(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L60
	call	_ZdlPv@PLT
.L60:
	movq	(%r15), %rax
	movl	$45, %edx
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	24(%rax), %rsi
	movq	8(%rax), %r15
	movq	%rcx, -1184(%rbp)
	movq	16(%rax), %rcx
	movq	32(%rax), %rax
	movq	%rsi, -1192(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -1152(%rbp)
	movq	%rax, -1160(%rbp)
	movq	%r15, -1168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadElementsKindENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal27TypedArrayBuiltinsAssembler22IsBigInt64ElementsKindENS0_8compiler5TNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$46, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1152(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadElementsKindENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -1200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1200(%rbp), %r8
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal27TypedArrayBuiltinsAssembler22IsBigInt64ElementsKindENS0_8compiler5TNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -1200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$45, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1200(%rbp), %r8
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1160(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	-1152(%rbp), %xmm3
	movq	-1184(%rbp), %xmm4
	movl	$40, %edi
	movaps	%xmm0, -1088(%rbp)
	movhps	-1192(%rbp), %xmm3
	movq	%rax, -64(%rbp)
	movhps	-1168(%rbp), %xmm4
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm3, -1152(%rbp)
	movaps	%xmm4, -1184(%rbp)
	movaps	%xmm4, -96(%rbp)
	movq	$0, -1072(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm6
	leaq	40(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -1088(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L61
	call	_ZdlPv@PLT
.L61:
	movq	-1160(%rbp), %rax
	movdqa	-1184(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movdqa	-1152(%rbp), %xmm5
	movaps	%xmm0, -1088(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movq	%rax, -64(%rbp)
	movq	$0, -1072(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	leaq	40(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -1088(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L62
	call	_ZdlPv@PLT
.L62:
	movq	-1120(%rbp), %rcx
	movq	-1128(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -800(%rbp)
	je	.L63
.L171:
	movq	-1128(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1072(%rbp)
	movaps	%xmm0, -1088(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$84346631, (%rax)
	leaq	5(%rax), %rdx
	movb	$5, 4(%rax)
	movq	%rax, -1088(%rbp)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1088(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L64
	call	_ZdlPv@PLT
.L64:
	movq	(%r15), %rax
	movl	$48, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$21, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -608(%rbp)
	je	.L65
.L172:
	movq	-1120(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1072(%rbp)
	movaps	%xmm0, -1088(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$84346631, (%rax)
	leaq	5(%rax), %rdx
	movb	$5, 4(%rax)
	movq	%rax, -1088(%rbp)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1088(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L66
	call	_ZdlPv@PLT
.L66:
	movq	(%r15), %rax
	leaq	.LC2(%rip), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rdx
	movq	24(%rax), %rdi
	movq	32(%rax), %r15
	movq	%rcx, -1184(%rbp)
	movq	8(%rax), %rcx
	movq	%rdx, -1168(%rbp)
	movl	$51, %edx
	movq	%rdi, -1160(%rbp)
	movq	%r12, %rdi
	movq	%rcx, -1152(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1168(%rbp), %r14
	movq	%r15, %r8
	movq	%r13, %rdi
	movq	-1160(%rbp), %rcx
	movq	-1152(%rbp), %rsi
	movq	%r14, %rdx
	call	_ZN2v88internal27TypedArrayBuiltinsAssembler32CallCCopyTypedArrayElementsSliceENS0_8compiler5TNodeINS0_12JSTypedArrayEEES5_NS3_INS0_7IntPtrTEEES7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$43, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$40, %edi
	movq	%r15, -64(%rbp)
	movq	-1184(%rbp), %xmm0
	movq	$0, -1072(%rbp)
	movhps	-1152(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%r14, %xmm0
	movhps	-1160(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1088(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm6
	leaq	40(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -1088(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L67
	call	_ZdlPv@PLT
.L67:
	movq	-1136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L173:
	movq	-1136(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1072(%rbp)
	movaps	%xmm0, -1088(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$84346631, (%rax)
	leaq	5(%rax), %rdx
	movb	$5, 4(%rax)
	movq	%rax, -1088(%rbp)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1088(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L69
	call	_ZdlPv@PLT
.L69:
	movq	(%r14), %rax
	movl	$40, %edi
	leaq	-288(%rbp), %r14
	movdqu	16(%rax), %xmm0
	movq	32(%rax), %rdx
	movdqu	(%rax), %xmm7
	movq	$0, -1072(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -64(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm0, -1088(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm6
	leaq	40(%rax), %rdx
	movq	%rax, -1088(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L70
	call	_ZdlPv@PLT
.L70:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L68
.L174:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22453:
	.size	_ZN2v88internal12SlowCopy_366EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEES8_NS4_INS0_7IntPtrTEEESA_, .-_ZN2v88internal12SlowCopy_366EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEES8_NS4_INS0_7IntPtrTEEESA_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EES9_PNS7_IS4_EEPNS7_IS5_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EES9_PNS7_IS4_EEPNS7_IS5_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EES9_PNS7_IS4_EEPNS7_IS5_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EES9_PNS7_IS4_EEPNS7_IS5_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EES9_PNS7_IS4_EEPNS7_IS5_EE:
.LFB26708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	$4, %edi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	leaq	-80(%rbp), %rsi
	movl	$100992775, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L176
	call	_ZdlPv@PLT
.L176:
	movq	(%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L177
	movq	%rdx, (%r14)
.L177:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L178
	movq	%rdx, 0(%r13)
.L178:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L179
	movq	%rdx, (%r12)
.L179:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L175
	movq	%rax, (%rbx)
.L175:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L198
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L198:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26708:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EES9_PNS7_IS4_EEPNS7_IS5_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EES9_PNS7_IS4_EEPNS7_IS5_EE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiENS0_6Int32TENS0_8UintPtrTES6_EE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESH_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiENS0_6Int32TENS0_8UintPtrTES6_EE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESH_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiENS0_6Int32TENS0_8UintPtrTES6_EE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESH_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiENS0_6Int32TENS0_8UintPtrTES6_EE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESH_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiENS0_6Int32TENS0_8UintPtrTES6_EE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESH_:
.LFB26729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$7, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$1284, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$100992775, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$4, 6(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L200
	movq	%rax, -112(%rbp)
	call	_ZdlPv@PLT
	movq	-112(%rbp), %rax
.L200:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L201
	movq	%rdx, (%r15)
.L201:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L202
	movq	%rdx, (%r14)
.L202:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L203
	movq	%rdx, 0(%r13)
.L203:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L204
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L204:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L205
	movq	%rdx, (%rbx)
.L205:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L206
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L206:
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L199
	movq	-104(%rbp), %rbx
	movq	%rax, (%rbx)
.L199:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L234
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L234:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26729:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiENS0_6Int32TENS0_8UintPtrTES6_EE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESH_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiENS0_6Int32TENS0_8UintPtrTES6_EE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESH_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiENS0_6Int32TENS0_8UintPtrTES6_S7_S7_S6_S5_S5_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESH_SJ_SJ_SH_SF_SF_SJ_SJ_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiENS0_6Int32TENS0_8UintPtrTES6_S7_S7_S6_S5_S5_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESH_SJ_SJ_SH_SF_SF_SJ_SJ_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiENS0_6Int32TENS0_8UintPtrTES6_S7_S7_S6_S5_S5_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESH_SJ_SJ_SH_SF_SF_SJ_SJ_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiENS0_6Int32TENS0_8UintPtrTES6_S7_S7_S6_S5_S5_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESH_SJ_SJ_SH_SF_SF_SJ_SJ_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiENS0_6Int32TENS0_8UintPtrTES6_S7_S7_S6_S5_S5_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESH_SJ_SJ_SH_SF_SF_SJ_SJ_:
.LFB26741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$14, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$136, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$361419384935483143, %rcx
	movq	%rcx, (%rax)
	movl	$1285, %ecx
	leaq	14(%rax), %rdx
	movl	$101057541, 8(%rax)
	movw	%cx, 12(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L236
	movq	%rax, -168(%rbp)
	call	_ZdlPv@PLT
	movq	-168(%rbp), %rax
.L236:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L237
	movq	%rdx, (%r15)
.L237:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L238
	movq	%rdx, (%r14)
.L238:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L239
	movq	%rdx, 0(%r13)
.L239:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L240
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L240:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L241
	movq	%rdx, (%rbx)
.L241:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L242
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L242:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L243
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L243:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L244
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L244:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L245
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L245:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L246
	movq	-128(%rbp), %rsi
	movq	%rdx, (%rsi)
.L246:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L247
	movq	-136(%rbp), %rcx
	movq	%rdx, (%rcx)
.L247:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L248
	movq	-144(%rbp), %rbx
	movq	%rdx, (%rbx)
.L248:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L249
	movq	-152(%rbp), %rsi
	movq	%rdx, (%rsi)
.L249:
	movq	104(%rax), %rax
	testq	%rax, %rax
	je	.L235
	movq	-160(%rbp), %rcx
	movq	%rax, (%rcx)
.L235:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L298
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L298:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26741:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiENS0_6Int32TENS0_8UintPtrTES6_S7_S7_S6_S5_S5_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESH_SJ_SJ_SH_SF_SF_SJ_SJ_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiENS0_6Int32TENS0_8UintPtrTES6_S7_S7_S6_S5_S5_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESH_SJ_SJ_SH_SF_SF_SJ_SJ_
	.section	.rodata._ZN2v88internal12FastCopy_365EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_12JSTypedArrayEEES6_NS4_INS0_7IntPtrTEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"../../deps/v8/../../deps/v8/src/builtins/typed-array.tq"
	.section	.text._ZN2v88internal12FastCopy_365EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_12JSTypedArrayEEES6_NS4_INS0_7IntPtrTEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal12FastCopy_365EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_12JSTypedArrayEEES6_NS4_INS0_7IntPtrTEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal12FastCopy_365EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_12JSTypedArrayEEES6_NS4_INS0_7IntPtrTEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal12FastCopy_365EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_12JSTypedArrayEEES6_NS4_INS0_7IntPtrTEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE:
.LFB22417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$424, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, -4424(%rbp)
	movq	%rdi, %rbx
	leaq	-4168(%rbp), %r15
	leaq	-3960(%rbp), %r12
	movq	%rsi, -4208(%rbp)
	leaq	-4016(%rbp), %r13
	leaq	-4048(%rbp), %r14
	movq	%rdx, -4304(%rbp)
	movq	%rcx, -4320(%rbp)
	movq	%r8, -4336(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -4168(%rbp)
	movq	%rdi, -4016(%rbp)
	movl	$96, %edi
	movq	$0, -4008(%rbp)
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r12, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -3992(%rbp)
	movq	%rdx, -4000(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3976(%rbp)
	movq	%rax, -4008(%rbp)
	movq	$0, -3984(%rbp)
	movq	%r13, -4352(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-3824(%rbp), %rax
	movl	$4, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -4240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3632(%rbp), %rax
	movl	$4, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -4256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3440(%rbp), %rax
	movl	$7, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -4192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3248(%rbp), %rax
	movl	$7, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -4224(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3056(%rbp), %rax
	movl	$7, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -4248(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2864(%rbp), %rax
	movl	$7, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -4264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2672(%rbp), %rax
	movl	$13, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -4216(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2480(%rbp), %rax
	movl	$13, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -4232(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2288(%rbp), %rax
	movl	$12, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -4272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-4168(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2088(%rbp)
	movq	$0, -2080(%rbp)
	movq	%rax, -2096(%rbp)
	movq	$0, -2072(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -2088(%rbp)
	leaq	-2040(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2072(%rbp)
	movq	%rdx, -2080(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2056(%rbp)
	movq	%rax, -4400(%rbp)
	movq	$0, -2064(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4168(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1896(%rbp)
	movq	$0, -1888(%rbp)
	movq	%rax, -1904(%rbp)
	movq	$0, -1880(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1896(%rbp)
	leaq	-1848(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1880(%rbp)
	movq	%rdx, -1888(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1864(%rbp)
	movq	%rax, -4344(%rbp)
	movq	$0, -1872(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4168(%rbp), %rax
	movl	$336, %edi
	movq	$0, -1704(%rbp)
	movq	$0, -1696(%rbp)
	movq	%rax, -1712(%rbp)
	movq	$0, -1688(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movups	%xmm0, (%rax)
	leaq	336(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -1704(%rbp)
	leaq	-1656(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1688(%rbp)
	movq	%rdx, -1696(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1672(%rbp)
	movq	%rax, -4376(%rbp)
	movq	$0, -1680(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1520(%rbp), %rax
	movl	$14, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -4280(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1328(%rbp), %rax
	movl	$13, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -4288(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-4168(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1128(%rbp)
	movq	$0, -1120(%rbp)
	movq	%rax, -1136(%rbp)
	movq	$0, -1112(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1128(%rbp)
	leaq	-1080(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1112(%rbp)
	movq	%rdx, -1120(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1096(%rbp)
	movq	%rax, -4368(%rbp)
	movq	$0, -1104(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4168(%rbp), %rax
	movl	$216, %edi
	movq	$0, -936(%rbp)
	movq	$0, -928(%rbp)
	movq	%rax, -944(%rbp)
	movq	$0, -920(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -936(%rbp)
	leaq	-888(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -920(%rbp)
	movq	%rdx, -928(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -904(%rbp)
	movq	%rax, -4416(%rbp)
	movq	$0, -912(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4168(%rbp), %rax
	movl	$96, %edi
	movq	$0, -744(%rbp)
	movq	$0, -736(%rbp)
	movq	%rax, -752(%rbp)
	movq	$0, -728(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movups	%xmm0, (%rax)
	leaq	96(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -744(%rbp)
	leaq	-696(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -728(%rbp)
	movq	%rdx, -736(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -712(%rbp)
	movq	%rax, -4384(%rbp)
	movq	$0, -720(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4168(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r15, %rsi
	movq	$0, -552(%rbp)
	movq	%rax, -560(%rbp)
	leaq	-504(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -4360(%rbp)
	movq	$0, -544(%rbp)
	movq	$0, -536(%rbp)
	movq	$0, -528(%rbp)
	movq	$0, -520(%rbp)
	movq	$0, -512(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-368(%rbp), %rax
	movl	$4, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -4184(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-4320(%rbp), %xmm1
	movq	-4208(%rbp), %xmm2
	movaps	%xmm0, -4048(%rbp)
	movhps	-4336(%rbp), %xmm1
	movq	$0, -4032(%rbp)
	movhps	-4304(%rbp), %xmm2
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm2, -176(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm4
	movq	%r13, %rdi
	movq	%r14, %rsi
	movdqa	-160(%rbp), %xmm5
	leaq	32(%rax), %rdx
	movq	%rax, -4048(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L300
	call	_ZdlPv@PLT
.L300:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3952(%rbp)
	jne	.L459
.L301:
	leaq	-560(%rbp), %rax
	cmpq	$0, -3760(%rbp)
	movq	%rax, -4208(%rbp)
	jne	.L460
.L304:
	cmpq	$0, -3568(%rbp)
	jne	.L461
	cmpq	$0, -3376(%rbp)
	jne	.L462
.L309:
	cmpq	$0, -3184(%rbp)
	jne	.L463
.L311:
	cmpq	$0, -2992(%rbp)
	jne	.L464
.L314:
	cmpq	$0, -2800(%rbp)
	jne	.L465
.L316:
	leaq	-2096(%rbp), %rax
	cmpq	$0, -2608(%rbp)
	movq	%rax, -4304(%rbp)
	jne	.L466
	cmpq	$0, -2416(%rbp)
	jne	.L467
.L322:
	leaq	-1904(%rbp), %rax
	cmpq	$0, -2224(%rbp)
	movq	%rax, -4320(%rbp)
	jne	.L468
	cmpq	$0, -2032(%rbp)
	jne	.L469
.L328:
	leaq	-1712(%rbp), %rax
	cmpq	$0, -1840(%rbp)
	movq	%rax, -4336(%rbp)
	jne	.L470
.L329:
	leaq	-1136(%rbp), %rax
	cmpq	$0, -1648(%rbp)
	movq	%rax, -4344(%rbp)
	jne	.L471
	cmpq	$0, -1456(%rbp)
	jne	.L472
.L335:
	cmpq	$0, -1264(%rbp)
	leaq	-944(%rbp), %r12
	jne	.L473
	cmpq	$0, -1072(%rbp)
	jne	.L474
.L340:
	cmpq	$0, -880(%rbp)
	leaq	-752(%rbp), %r13
	jne	.L475
.L342:
	cmpq	$0, -688(%rbp)
	leaq	-312(%rbp), %rbx
	jne	.L476
	cmpq	$0, -496(%rbp)
	jne	.L477
.L349:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -4032(%rbp)
	movaps	%xmm0, -4048(%rbp)
	call	_Znwm@PLT
	movq	-4184(%rbp), %rdi
	movq	%r14, %rsi
	movl	$100992775, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -4048(%rbp)
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L351
	call	_ZdlPv@PLT
.L351:
	movq	-4184(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4208(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4344(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4288(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4280(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4336(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4320(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4304(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4272(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4232(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4216(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4264(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4248(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4224(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4192(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4256(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4240(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4352(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L478
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L459:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	$0, -4080(%rbp)
	movq	$0, -4072(%rbp)
	movq	$0, -4064(%rbp)
	movq	$0, -4056(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-4352(%rbp), %rdi
	leaq	-4064(%rbp), %rcx
	leaq	-4056(%rbp), %r8
	leaq	-4072(%rbp), %rdx
	leaq	-4080(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EES9_PNS7_IS4_EEPNS7_IS5_EE
	movl	$16, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal19IsForceSlowPath_239EPNS0_8compiler18CodeAssemblerStateE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-4064(%rbp), %xmm6
	movq	-4080(%rbp), %xmm7
	movaps	%xmm0, -4048(%rbp)
	movq	%rax, %r12
	movhps	-4056(%rbp), %xmm6
	movq	$0, -4032(%rbp)
	movhps	-4072(%rbp), %xmm7
	movaps	%xmm6, -4304(%rbp)
	movaps	%xmm7, -4208(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm6
	movdqa	-160(%rbp), %xmm7
	movq	%r14, %rsi
	movq	-4240(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -4048(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L302
	call	_ZdlPv@PLT
.L302:
	movdqa	-4208(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-4304(%rbp), %xmm7
	movaps	%xmm0, -4048(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm7, -160(%rbp)
	movq	$0, -4032(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm6
	movdqa	-160(%rbp), %xmm7
	movq	%r14, %rsi
	movq	-4256(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -4048(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L303
	call	_ZdlPv@PLT
.L303:
	leaq	-3576(%rbp), %rcx
	leaq	-3768(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L461:
	leaq	-3576(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4080(%rbp)
	movq	$0, -4072(%rbp)
	movq	$0, -4064(%rbp)
	movq	$0, -4056(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-4256(%rbp), %rdi
	leaq	-4064(%rbp), %rcx
	leaq	-4056(%rbp), %r8
	leaq	-4072(%rbp), %rdx
	leaq	-4080(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EES9_PNS7_IS4_EEPNS7_IS5_EE
	movl	$18, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4080(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadElementsKindENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$19, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4072(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal27TypedArrayBuiltinsAssembler25GetTypedArrayElementsInfoENS0_8compiler5TNodeINS0_12JSTypedArrayEEE@PLT
	movq	%r14, %rdi
	movq	%rdx, %r12
	movq	%rax, -4448(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$25, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movq	%r13, -4336(%rbp)
	call	_ZN2v88internal23ElementsKindNotEqual_72EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6Int32TEEES6_@PLT
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movq	-4336(%rbp), %xmm5
	movq	-4064(%rbp), %xmm3
	movaps	%xmm0, -4048(%rbp)
	movq	%rax, %r13
	movq	-4080(%rbp), %xmm4
	movhps	-4448(%rbp), %xmm5
	movq	%r12, -128(%rbp)
	movhps	-4056(%rbp), %xmm3
	movhps	-4072(%rbp), %xmm4
	movaps	%xmm5, -4336(%rbp)
	movaps	%xmm3, -4304(%rbp)
	movaps	%xmm4, -4320(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movq	$0, -4032(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm6
	movdqa	-160(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm4
	leaq	56(%rax), %rdx
	movq	%rax, -4048(%rbp)
	movq	%rcx, 48(%rax)
	movq	-4192(%rbp), %rdi
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L307
	call	_ZdlPv@PLT
.L307:
	movdqa	-4320(%rbp), %xmm5
	movdqa	-4304(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movdqa	-4336(%rbp), %xmm6
	movq	%r12, -128(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm0, -4048(%rbp)
	movq	$0, -4032(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm5
	leaq	56(%rax), %rdx
	movq	%rax, -4048(%rbp)
	movq	%rcx, 48(%rax)
	movq	-4224(%rbp), %rdi
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L308
	call	_ZdlPv@PLT
.L308:
	leaq	-3192(%rbp), %rcx
	leaq	-3384(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -3376(%rbp)
	je	.L309
.L462:
	leaq	-3384(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4104(%rbp)
	movq	$0, -4096(%rbp)
	movq	$0, -4088(%rbp)
	movq	$0, -4080(%rbp)
	movq	$0, -4072(%rbp)
	movq	$0, -4064(%rbp)
	movq	$0, -4056(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4056(%rbp), %rax
	movq	-4192(%rbp), %rdi
	leaq	-4088(%rbp), %rcx
	pushq	%rax
	leaq	-4064(%rbp), %rax
	leaq	-4096(%rbp), %rdx
	pushq	%rax
	leaq	-4104(%rbp), %rsi
	leaq	-4072(%rbp), %r9
	leaq	-4080(%rbp), %r8
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiENS0_6Int32TENS0_8UintPtrTES6_EE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESH_
	movq	-4208(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	$0, -4032(%rbp)
	movaps	%xmm0, -4048(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4048(%rbp), %rdi
	popq	%r12
	popq	%r13
	testq	%rdi, %rdi
	je	.L310
	call	_ZdlPv@PLT
.L310:
	movq	-4360(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3184(%rbp)
	je	.L311
.L463:
	leaq	-3192(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4104(%rbp)
	movq	$0, -4096(%rbp)
	movq	$0, -4088(%rbp)
	movq	$0, -4080(%rbp)
	movq	$0, -4072(%rbp)
	movq	$0, -4064(%rbp)
	movq	$0, -4056(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4056(%rbp), %rax
	movq	-4224(%rbp), %rdi
	leaq	-4072(%rbp), %r9
	pushq	%rax
	leaq	-4064(%rbp), %rax
	leaq	-4080(%rbp), %r8
	pushq	%rax
	leaq	-4088(%rbp), %rcx
	leaq	-4096(%rbp), %rdx
	leaq	-4104(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiENS0_6Int32TENS0_8UintPtrTES6_EE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESH_
	movl	$26, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	movq	-4096(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE(%rip), %r13d
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rax, %rcx
	movl	%r13d, %esi
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19BitcastTaggedToWordENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, -4320(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4104(%rbp), %r8
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%r8, -4304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-4320(%rbp), %r9
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-4304(%rbp), %r8
	movl	%r13d, %esi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19BitcastTaggedToWordENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-4072(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	-4064(%rbp), %r10
	movq	-4096(%rbp), %rcx
	movq	-4088(%rbp), %rsi
	movaps	%xmm0, -4048(%rbp)
	movq	-4080(%rbp), %rdx
	movq	-4104(%rbp), %rax
	movq	%rdi, -4448(%rbp)
	movq	-4056(%rbp), %r13
	movq	%rdi, -144(%rbp)
	movl	$56, %edi
	movq	%r10, -4336(%rbp)
	movq	%r10, -136(%rbp)
	movq	%rcx, -4320(%rbp)
	movq	%rsi, -4464(%rbp)
	movq	%rdx, -4304(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%rsi, -160(%rbp)
	movq	%rdx, -152(%rbp)
	movq	%rax, -4480(%rbp)
	movq	%rax, -176(%rbp)
	movq	%r13, -128(%rbp)
	movq	$0, -4032(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm2
	movdqa	-160(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm6
	leaq	56(%rax), %rdx
	movq	%rax, -4048(%rbp)
	movq	%rcx, 48(%rax)
	movq	-4248(%rbp), %rdi
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4048(%rbp), %rdi
	popq	%r10
	popq	%r11
	testq	%rdi, %rdi
	je	.L312
	call	_ZdlPv@PLT
.L312:
	movq	-4480(%rbp), %xmm0
	movl	$56, %edi
	movq	%r13, -128(%rbp)
	movq	$0, -4032(%rbp)
	movhps	-4320(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-4464(%rbp), %xmm0
	movhps	-4304(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-4448(%rbp), %xmm0
	movhps	-4336(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -4048(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm5
	leaq	56(%rax), %rdx
	movq	%rax, -4048(%rbp)
	movq	%rcx, 48(%rax)
	movq	-4264(%rbp), %rdi
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L313
	call	_ZdlPv@PLT
.L313:
	leaq	-2808(%rbp), %rcx
	leaq	-3000(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2992(%rbp)
	je	.L314
.L464:
	leaq	-3000(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4104(%rbp)
	movq	$0, -4096(%rbp)
	movq	$0, -4088(%rbp)
	movq	$0, -4080(%rbp)
	movq	$0, -4072(%rbp)
	movq	$0, -4064(%rbp)
	movq	$0, -4056(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4056(%rbp), %rax
	movq	-4248(%rbp), %rdi
	leaq	-4072(%rbp), %r9
	pushq	%rax
	leaq	-4064(%rbp), %rax
	leaq	-4080(%rbp), %r8
	pushq	%rax
	leaq	-4088(%rbp), %rcx
	leaq	-4096(%rbp), %rdx
	leaq	-4104(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiENS0_6Int32TENS0_8UintPtrTES6_EE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESH_
	movl	$27, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4208(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	$0, -4032(%rbp)
	movaps	%xmm0, -4048(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4048(%rbp), %rdi
	popq	%r8
	popq	%r9
	testq	%rdi, %rdi
	je	.L315
	call	_ZdlPv@PLT
.L315:
	movq	-4360(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2800(%rbp)
	je	.L316
.L465:
	leaq	-2808(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4104(%rbp)
	movq	$0, -4096(%rbp)
	movq	$0, -4088(%rbp)
	movq	$0, -4080(%rbp)
	movq	$0, -4072(%rbp)
	movq	$0, -4064(%rbp)
	movq	$0, -4056(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4056(%rbp), %rax
	movq	-4264(%rbp), %rdi
	leaq	-4088(%rbp), %rcx
	pushq	%rax
	leaq	-4064(%rbp), %rax
	leaq	-4072(%rbp), %r9
	pushq	%rax
	leaq	-4080(%rbp), %r8
	leaq	-4096(%rbp), %rdx
	leaq	-4104(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiENS0_6Int32TENS0_8UintPtrTES6_EE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESH_
	movl	$31, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$26, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4080(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal36Convert9ATuintptr13ATPositiveSmi_187EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$27, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4064(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r12, -4304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEENS3_INS0_9IntegralTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$30, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4064(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShrENS1_11SloppyTNodeINS0_5WordTEEENS3_INS0_9IntegralTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4304(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-4304(%rbp), %rax
	pxor	%xmm1, %xmm1
	movq	-4080(%rbp), %xmm0
	movq	-4056(%rbp), %xmm6
	movl	$104, %edi
	movq	-4056(%rbp), %xmm7
	movaps	%xmm1, -4048(%rbp)
	movq	-4072(%rbp), %xmm3
	movq	%xmm0, -96(%rbp)
	movq	-4088(%rbp), %xmm4
	punpcklqdq	%xmm0, %xmm6
	movhps	-4064(%rbp), %xmm7
	movq	-4104(%rbp), %xmm5
	movq	%xmm0, -4496(%rbp)
	punpcklqdq	%xmm0, %xmm4
	movhps	-4064(%rbp), %xmm3
	movaps	%xmm7, -128(%rbp)
	movhps	-4096(%rbp), %xmm5
	movaps	%xmm6, -4480(%rbp)
	movaps	%xmm7, -4464(%rbp)
	movaps	%xmm3, -4448(%rbp)
	movaps	%xmm4, -4336(%rbp)
	movaps	%xmm5, -4320(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm6, -112(%rbp)
	movq	%rax, -88(%rbp)
	movq	%r12, -80(%rbp)
	movq	$0, -4032(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm4
	leaq	104(%rax), %rdx
	movq	%rax, -4048(%rbp)
	movdqa	-160(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm6
	movq	%rcx, 96(%rax)
	movdqa	-96(%rbp), %xmm5
	movq	-4216(%rbp), %rdi
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm5, 80(%rax)
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4048(%rbp), %rdi
	popq	%rcx
	movq	-4496(%rbp), %xmm0
	popq	%rsi
	testq	%rdi, %rdi
	je	.L317
	call	_ZdlPv@PLT
	movq	-4496(%rbp), %xmm0
.L317:
	movdqa	-4320(%rbp), %xmm2
	movdqa	-4336(%rbp), %xmm3
	movhps	-4304(%rbp), %xmm0
	movl	$104, %edi
	movdqa	-4448(%rbp), %xmm6
	movdqa	-4464(%rbp), %xmm7
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movdqa	-4480(%rbp), %xmm4
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm0, -4048(%rbp)
	movq	%r12, -80(%rbp)
	movq	$0, -4032(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	leaq	104(%rax), %rdx
	movq	%rax, -4048(%rbp)
	movdqa	-160(%rbp), %xmm2
	movdqa	-144(%rbp), %xmm3
	movq	%rcx, 96(%rax)
	movdqa	-96(%rbp), %xmm4
	movq	-4232(%rbp), %rdi
	movups	%xmm5, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm4, 80(%rax)
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L318
	call	_ZdlPv@PLT
.L318:
	leaq	-2424(%rbp), %rcx
	leaq	-2616(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L460:
	leaq	-3768(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4080(%rbp)
	movq	$0, -4072(%rbp)
	movq	$0, -4064(%rbp)
	movq	$0, -4056(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-4240(%rbp), %rdi
	leaq	-4064(%rbp), %rcx
	leaq	-4072(%rbp), %rdx
	leaq	-4080(%rbp), %rsi
	leaq	-4056(%rbp), %r8
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EES9_PNS7_IS4_EEPNS7_IS5_EE
	movq	-4208(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	$0, -4032(%rbp)
	movaps	%xmm0, -4048(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L305
	call	_ZdlPv@PLT
.L305:
	movq	-4360(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L466:
	leaq	-2616(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$13, %edi
	movq	$0, -4032(%rbp)
	movaps	%xmm0, -4048(%rbp)
	call	_Znwm@PLT
	movq	-4216(%rbp), %rdi
	movq	%r14, %rsi
	movabsq	$361419384935483143, %rcx
	movq	%rcx, (%rax)
	leaq	13(%rax), %rdx
	movl	$84280836, 8(%rax)
	movb	$5, 12(%rax)
	movq	%rax, -4048(%rbp)
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4048(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L320
	call	_ZdlPv@PLT
.L320:
	movq	(%r12), %rax
	movl	$56, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movq	48(%rax), %rdx
	movdqu	(%rax), %xmm5
	movq	$0, -4032(%rbp)
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -128(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -4048(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm2
	movdqa	-160(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm6
	leaq	56(%rax), %rdx
	movq	%rax, -4048(%rbp)
	movq	%rcx, 48(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm6, 32(%rax)
	leaq	-2096(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	movq	%rax, -4304(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L321
	call	_ZdlPv@PLT
.L321:
	movq	-4400(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2416(%rbp)
	je	.L322
.L467:
	leaq	-2424(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$13, %edi
	movq	$0, -4032(%rbp)
	movaps	%xmm0, -4048(%rbp)
	call	_Znwm@PLT
	movq	-4232(%rbp), %rdi
	movq	%r14, %rsi
	movabsq	$361419384935483143, %rcx
	movq	%rcx, (%rax)
	leaq	13(%rax), %rdx
	movl	$84280836, 8(%rax)
	movb	$5, 12(%rax)
	movq	%rax, -4048(%rbp)
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4048(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L323
	call	_ZdlPv@PLT
.L323:
	movq	(%r12), %rax
	movq	24(%rax), %rdx
	movq	8(%rax), %rsi
	movq	72(%rax), %rdi
	movq	(%rax), %rcx
	movq	32(%rax), %r13
	movq	48(%rax), %r12
	movq	%rdx, -4464(%rbp)
	movq	40(%rax), %rdx
	movq	%rsi, -4336(%rbp)
	movq	%rdi, -4528(%rbp)
	movq	16(%rax), %rsi
	movq	%rdx, -4480(%rbp)
	movq	56(%rax), %rdx
	movq	80(%rax), %rdi
	movq	%rsi, -4448(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%rdx, -4496(%rbp)
	movq	64(%rax), %rdx
	movq	96(%rax), %rax
	movq	%rdi, -4544(%rbp)
	movq	%r15, %rdi
	movq	%rdx, -4512(%rbp)
	movl	$31, %edx
	movq	%rcx, -4320(%rbp)
	movq	%rax, -4560(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$96, %edi
	movq	-4320(%rbp), %xmm0
	movq	$0, -4032(%rbp)
	movhps	-4336(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-4448(%rbp), %xmm0
	movhps	-4464(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%r13, %xmm0
	movhps	-4480(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%r12, %xmm0
	movhps	-4496(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-4512(%rbp), %xmm0
	movhps	-4528(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-4544(%rbp), %xmm0
	movhps	-4560(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -4048(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm2
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-160(%rbp), %xmm4
	leaq	96(%rax), %rdx
	movq	%rax, -4048(%rbp)
	movdqa	-144(%rbp), %xmm5
	movups	%xmm7, (%rax)
	movdqa	-96(%rbp), %xmm7
	movq	-4272(%rbp), %rdi
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm3, 64(%rax)
	movups	%xmm7, 80(%rax)
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L324
	call	_ZdlPv@PLT
.L324:
	leaq	-2232(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L468:
	leaq	-2232(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$12, %edi
	movq	$0, -4032(%rbp)
	movaps	%xmm0, -4048(%rbp)
	call	_Znwm@PLT
	movq	-4272(%rbp), %rdi
	movq	%r14, %rsi
	movabsq	$361419384935483143, %rcx
	movq	%rcx, (%rax)
	leaq	12(%rax), %rdx
	movl	$84280836, 8(%rax)
	movq	%rax, -4048(%rbp)
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4048(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L326
	call	_ZdlPv@PLT
.L326:
	movq	(%r12), %rax
	movl	$31, %edx
	movq	%r15, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %r13
	movq	32(%rax), %r12
	movq	%rsi, -4336(%rbp)
	movq	24(%rax), %rsi
	movq	%rcx, -4320(%rbp)
	movq	%rsi, -4448(%rbp)
	movq	40(%rax), %rsi
	movq	%rsi, -4464(%rbp)
	movq	48(%rax), %rsi
	movq	88(%rax), %rax
	movq	%rsi, -4480(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rax, -4496(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$64, %edi
	movq	-4320(%rbp), %xmm0
	movq	$0, -4032(%rbp)
	movhps	-4336(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	%r13, %xmm0
	movhps	-4448(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%r12, %xmm0
	movhps	-4464(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-4480(%rbp), %xmm0
	movhps	-4496(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -4048(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm2
	movq	%r14, %rsi
	movdqa	-160(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm5
	leaq	64(%rax), %rdx
	movq	%rax, -4048(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm2, 48(%rax)
	leaq	-1904(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	movq	%rax, -4320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L327
	call	_ZdlPv@PLT
.L327:
	movq	-4344(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2032(%rbp)
	je	.L328
.L469:
	movq	-4400(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4104(%rbp)
	movq	$0, -4096(%rbp)
	movq	$0, -4088(%rbp)
	movq	$0, -4080(%rbp)
	movq	$0, -4072(%rbp)
	movq	$0, -4064(%rbp)
	movq	$0, -4056(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4056(%rbp), %rax
	movq	-4304(%rbp), %rdi
	leaq	-4096(%rbp), %rdx
	pushq	%rax
	leaq	-4064(%rbp), %rax
	leaq	-4088(%rbp), %rcx
	pushq	%rax
	leaq	-4072(%rbp), %r9
	leaq	-4080(%rbp), %r8
	leaq	-4104(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiENS0_6Int32TENS0_8UintPtrTES6_EE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESH_
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L470:
	movq	-4344(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -4032(%rbp)
	movaps	%xmm0, -4048(%rbp)
	call	_Znwm@PLT
	movq	-4320(%rbp), %rdi
	movq	%r14, %rsi
	movabsq	$361419384935483143, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -4048(%rbp)
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4048(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L330
	call	_ZdlPv@PLT
.L330:
	movq	(%r12), %rax
	movq	%r15, %rdi
	movq	8(%rax), %rsi
	movq	48(%rax), %rdx
	movq	(%rax), %rcx
	movq	40(%rax), %r13
	movq	%rsi, -4448(%rbp)
	movq	24(%rax), %rsi
	movq	16(%rax), %r12
	movq	%rdx, -4512(%rbp)
	movl	$30, %edx
	movq	%rsi, -4480(%rbp)
	movq	32(%rax), %rsi
	movq	56(%rax), %rax
	movq	%rcx, -4400(%rbp)
	movq	%rsi, -4496(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rax, -4528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$33, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r12, -4464(%rbp)
	call	_ZN2v88internal35Convert13ATPositiveSmi8ATintptr_189EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movl	$26, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r12, -4544(%rbp)
	call	_ZN2v88internal36Convert9ATuintptr13ATPositiveSmi_187EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$27, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEENS3_INS0_9IntegralTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -4336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$30, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4336(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShrENS1_11SloppyTNodeINS0_5WordTEEENS3_INS0_9IntegralTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -4344(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4344(%rbp), %r8
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -4344(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-4512(%rbp), %rax
	movq	%r13, %xmm3
	movq	%r13, %xmm5
	movq	%r12, %xmm6
	pxor	%xmm0, %xmm0
	movl	$112, %edi
	movq	-4544(%rbp), %xmm7
	movq	%rax, %xmm4
	movq	%rax, %xmm2
	movq	-4400(%rbp), %xmm1
	movhps	-4336(%rbp), %xmm6
	punpcklqdq	%xmm4, %xmm3
	punpcklqdq	%xmm7, %xmm7
	movaps	%xmm6, -80(%rbp)
	movq	-4496(%rbp), %xmm4
	movhps	-4528(%rbp), %xmm2
	movhps	-4448(%rbp), %xmm1
	movaps	%xmm6, -4560(%rbp)
	punpcklqdq	%xmm5, %xmm4
	movaps	%xmm7, -4544(%rbp)
	movq	-4464(%rbp), %xmm5
	movaps	%xmm3, -4512(%rbp)
	movhps	-4480(%rbp), %xmm5
	movaps	%xmm2, -4528(%rbp)
	movaps	%xmm4, -4496(%rbp)
	movaps	%xmm5, -4464(%rbp)
	movaps	%xmm1, -4400(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm0, -4048(%rbp)
	movq	$0, -4032(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm4
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-160(%rbp), %xmm7
	leaq	112(%rax), %rdx
	movq	%rax, -4048(%rbp)
	movups	%xmm3, (%rax)
	movdqa	-144(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm3
	movups	%xmm7, 16(%rax)
	movdqa	-80(%rbp), %xmm7
	movups	%xmm6, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm2, 64(%rax)
	movups	%xmm3, 80(%rax)
	movups	%xmm7, 96(%rax)
	leaq	-1712(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	movq	%rax, -4336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L331
	call	_ZdlPv@PLT
.L331:
	movdqa	-4400(%rbp), %xmm6
	movdqa	-4464(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movl	$112, %edi
	movdqa	-4496(%rbp), %xmm5
	movdqa	-4528(%rbp), %xmm2
	movaps	%xmm0, -4048(%rbp)
	movdqa	-4512(%rbp), %xmm3
	movdqa	-4544(%rbp), %xmm7
	movaps	%xmm6, -176(%rbp)
	movdqa	-4560(%rbp), %xmm6
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm6, -80(%rbp)
	movq	$0, -4032(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm4
	movdqa	-128(%rbp), %xmm3
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm6
	leaq	112(%rax), %rdx
	movq	%rax, -4048(%rbp)
	movdqa	-160(%rbp), %xmm5
	movdqa	-144(%rbp), %xmm2
	movups	%xmm4, (%rax)
	movdqa	-80(%rbp), %xmm4
	movq	-4280(%rbp), %rdi
	movups	%xmm3, 48(%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm6, 80(%rax)
	movups	%xmm4, 96(%rax)
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L332
	call	_ZdlPv@PLT
.L332:
	movq	-4376(%rbp), %rdx
	movq	-4344(%rbp), %rsi
	leaq	-1464(%rbp), %rcx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L471:
	movq	-4376(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4160(%rbp)
	movq	$0, -4152(%rbp)
	movq	$0, -4144(%rbp)
	movq	$0, -4136(%rbp)
	movq	$0, -4128(%rbp)
	movq	$0, -4120(%rbp)
	movq	$0, -4112(%rbp)
	movq	$0, -4104(%rbp)
	movq	$0, -4096(%rbp)
	movq	$0, -4088(%rbp)
	movq	$0, -4080(%rbp)
	movq	$0, -4072(%rbp)
	movq	$0, -4064(%rbp)
	movq	$0, -4056(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-4056(%rbp), %rax
	movq	-4336(%rbp), %rdi
	pushq	%rax
	leaq	-4064(%rbp), %rax
	leaq	-4144(%rbp), %rcx
	pushq	%rax
	leaq	-4072(%rbp), %rax
	leaq	-4152(%rbp), %rdx
	pushq	%rax
	leaq	-4080(%rbp), %rax
	leaq	-4160(%rbp), %rsi
	pushq	%rax
	leaq	-4088(%rbp), %rax
	leaq	-4128(%rbp), %r9
	pushq	%rax
	leaq	-4096(%rbp), %rax
	leaq	-4136(%rbp), %r8
	pushq	%rax
	leaq	-4104(%rbp), %rax
	pushq	%rax
	leaq	-4112(%rbp), %rax
	pushq	%rax
	leaq	-4120(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiENS0_6Int32TENS0_8UintPtrTES6_S7_S7_S6_S5_S5_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESH_SJ_SJ_SH_SF_SF_SJ_SJ_
	addq	$80, %rsp
	movl	$64, %edi
	movq	-4112(%rbp), %xmm0
	movq	-4128(%rbp), %xmm1
	movq	-4144(%rbp), %xmm2
	movq	$0, -4032(%rbp)
	movq	-4160(%rbp), %xmm3
	movhps	-4104(%rbp), %xmm0
	movhps	-4120(%rbp), %xmm1
	movhps	-4136(%rbp), %xmm2
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-4152(%rbp), %xmm3
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -4048(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm3
	movq	%r14, %rsi
	movdqa	-160(%rbp), %xmm1
	movdqa	-144(%rbp), %xmm2
	leaq	64(%rax), %rdx
	movq	%rax, -4048(%rbp)
	movq	-4344(%rbp), %rdi
	movups	%xmm5, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L334
	call	_ZdlPv@PLT
.L334:
	movq	-4368(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1456(%rbp)
	je	.L335
.L472:
	leaq	-1464(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4160(%rbp)
	movq	$0, -4152(%rbp)
	movq	$0, -4144(%rbp)
	movq	$0, -4136(%rbp)
	movq	$0, -4128(%rbp)
	movq	$0, -4120(%rbp)
	movq	$0, -4112(%rbp)
	movq	$0, -4104(%rbp)
	movq	$0, -4096(%rbp)
	movq	$0, -4088(%rbp)
	movq	$0, -4080(%rbp)
	movq	$0, -4072(%rbp)
	movq	$0, -4064(%rbp)
	movq	$0, -4056(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-4056(%rbp), %rax
	movq	-4280(%rbp), %rdi
	pushq	%rax
	leaq	-4064(%rbp), %rax
	leaq	-4144(%rbp), %rcx
	pushq	%rax
	leaq	-4072(%rbp), %rax
	leaq	-4128(%rbp), %r9
	pushq	%rax
	leaq	-4080(%rbp), %rax
	leaq	-4136(%rbp), %r8
	pushq	%rax
	leaq	-4088(%rbp), %rax
	leaq	-4152(%rbp), %rdx
	pushq	%rax
	leaq	-4096(%rbp), %rax
	leaq	-4160(%rbp), %rsi
	pushq	%rax
	leaq	-4104(%rbp), %rax
	pushq	%rax
	leaq	-4112(%rbp), %rax
	pushq	%rax
	leaq	-4120(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiENS0_6Int32TENS0_8UintPtrTES6_S7_S7_S6_S5_S5_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESH_SJ_SJ_SH_SF_SF_SJ_SJ_
	addq	$80, %rsp
	movl	$31, %edx
	movq	%r15, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4160(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$104, %edi
	movaps	%xmm0, -4048(%rbp)
	movq	%rax, -176(%rbp)
	movq	-4152(%rbp), %rax
	movq	$0, -4032(%rbp)
	movq	%rax, -168(%rbp)
	movq	-4144(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-4136(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-4128(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-4120(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-4112(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-4104(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-4096(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-4088(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-4080(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-4072(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-4056(%rbp), %rax
	movq	%rax, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm1
	leaq	104(%rax), %rdx
	movq	%rax, -4048(%rbp)
	movdqa	-160(%rbp), %xmm6
	movdqa	-144(%rbp), %xmm4
	movq	%rcx, 96(%rax)
	movdqa	-96(%rbp), %xmm2
	movq	-4288(%rbp), %rdi
	movups	%xmm7, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm1, 64(%rax)
	movups	%xmm2, 80(%rax)
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L336
	call	_ZdlPv@PLT
.L336:
	leaq	-1272(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L473:
	leaq	-1272(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$13, %edi
	movq	$0, -4032(%rbp)
	movaps	%xmm0, -4048(%rbp)
	call	_Znwm@PLT
	movq	-4288(%rbp), %rdi
	movq	%r14, %rsi
	movabsq	$361419384935483143, %rcx
	movq	%rcx, (%rax)
	leaq	13(%rax), %rdx
	movl	$101057541, 8(%rax)
	movb	$5, 12(%rax)
	movq	%rax, -4048(%rbp)
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4048(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L338
	call	_ZdlPv@PLT
.L338:
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	(%rax), %rcx
	movq	96(%rax), %r12
	movq	16(%rax), %r13
	movq	%rsi, -4400(%rbp)
	movq	24(%rax), %rsi
	movq	%rdx, -4480(%rbp)
	movq	48(%rax), %rdx
	movq	%rdi, -4512(%rbp)
	movq	%r15, %rdi
	movq	%rsi, -4448(%rbp)
	movq	32(%rax), %rsi
	movq	%rdx, -4496(%rbp)
	movl	$33, %edx
	movq	%rsi, -4464(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -4376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$72, %edi
	movq	%r12, -112(%rbp)
	movq	-4376(%rbp), %xmm0
	movq	$0, -4032(%rbp)
	leaq	-944(%rbp), %r12
	movhps	-4400(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	%r13, %xmm0
	movhps	-4448(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-4464(%rbp), %xmm0
	movhps	-4480(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-4496(%rbp), %xmm0
	movhps	-4512(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -4048(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movdqa	-128(%rbp), %xmm4
	leaq	72(%rax), %rdx
	movq	%rax, -4048(%rbp)
	movdqa	-176(%rbp), %xmm3
	movdqa	-160(%rbp), %xmm7
	movq	%rcx, 64(%rax)
	movdqa	-144(%rbp), %xmm6
	movups	%xmm4, 48(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L339
	call	_ZdlPv@PLT
.L339:
	movq	-4416(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1072(%rbp)
	je	.L340
.L474:
	movq	-4368(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -4032(%rbp)
	movaps	%xmm0, -4048(%rbp)
	call	_Znwm@PLT
	movq	-4344(%rbp), %rdi
	movq	%r14, %rsi
	movabsq	$361419384935483143, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -4048(%rbp)
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L341
	call	_ZdlPv@PLT
.L341:
	movl	$34, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L475:
	movq	-4416(%rbp), %rsi
	movq	%r15, %rdi
	xorl	%r13d, %r13d
	movq	$0, -4368(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -4032(%rbp)
	movaps	%xmm0, -4048(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movabsq	$361419384935483143, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$5, 8(%rax)
	movq	%rax, -4048(%rbp)
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L343
	movq	%rax, -4376(%rbp)
	call	_ZdlPv@PLT
	movq	-4376(%rbp), %rax
.L343:
	movq	(%rax), %rax
	movq	%r15, %rdi
	movq	56(%rax), %rdx
	movq	8(%rax), %rsi
	movq	(%rax), %r8
	movq	16(%rax), %xmm0
	testq	%rdx, %rdx
	cmove	-4368(%rbp), %rdx
	movq	%rsi, -4376(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%r8, %xmm1
	movhps	24(%rax), %xmm0
	movq	%r8, -4400(%rbp)
	movq	%rdx, -4368(%rbp)
	movq	64(%rax), %rdx
	movhps	-4376(%rbp), %xmm1
	movaps	%xmm0, -4416(%rbp)
	testq	%rdx, %rdx
	movaps	%xmm1, -4448(%rbp)
	cmovne	%rdx, %r13
	movl	$32, %edx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$35, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4400(%rbp), %r8
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler28LoadJSTypedArrayBackingStoreENS0_8compiler5TNodeINS0_12JSTypedArrayEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -4400(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal30Convert8ATintptr9ATuintptr_188EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8UintPtrTEEE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4400(%rbp), %r8
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -4400(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$40, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4376(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler28LoadJSTypedArrayBackingStoreENS0_8compiler5TNodeINS0_12JSTypedArrayEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4400(%rbp), %r8
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-4368(%rbp), %rcx
	leaq	-752(%rbp), %r13
	movq	%r8, %rdx
	call	_ZN2v88internal27TypedArrayBuiltinsAssembler12CallCMemmoveENS0_8compiler5TNodeINS0_7RawPtrTEEES5_NS3_INS0_8UintPtrTEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$15, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$13, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movdqa	-4416(%rbp), %xmm0
	movl	$32, %edi
	movdqa	-4448(%rbp), %xmm1
	movq	$0, -4032(%rbp)
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -4048(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm2
	movq	%r13, %rdi
	movq	%r14, %rsi
	movdqa	-160(%rbp), %xmm3
	leaq	32(%rax), %rdx
	movq	%rax, -4048(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L346
	call	_ZdlPv@PLT
.L346:
	movq	-4384(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L476:
	movq	-4384(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4080(%rbp)
	movq	$0, -4072(%rbp)
	movq	$0, -4064(%rbp)
	movq	$0, -4056(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4064(%rbp), %rcx
	movq	%r13, %rdi
	leaq	-4072(%rbp), %rdx
	leaq	-4080(%rbp), %rsi
	leaq	-4056(%rbp), %r8
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_12JSTypedArrayES3_NS0_7IntPtrTENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EES9_PNS7_IS4_EEPNS7_IS5_EE
	movl	$32, %edi
	movq	-4064(%rbp), %xmm0
	movq	-4080(%rbp), %xmm1
	movq	$0, -4032(%rbp)
	movhps	-4056(%rbp), %xmm0
	movhps	-4072(%rbp), %xmm1
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -4048(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm5
	movdqa	-160(%rbp), %xmm1
	movq	%r14, %rsi
	movq	-4184(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -4048(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm1, 16(%rax)
	movq	%rdx, -4032(%rbp)
	movq	%rdx, -4040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L348
	call	_ZdlPv@PLT
.L348:
	leaq	-312(%rbp), %rbx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -496(%rbp)
	je	.L349
.L477:
	movq	-4360(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-4208(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	$0, -4032(%rbp)
	movaps	%xmm0, -4048(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L350
	call	_ZdlPv@PLT
.L350:
	movq	-4424(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L349
.L478:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22417:
	.size	_ZN2v88internal12FastCopy_365EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_12JSTypedArrayEEES6_NS4_INS0_7IntPtrTEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE, .-_ZN2v88internal12FastCopy_365EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_12JSTypedArrayEEES6_NS4_INS0_7IntPtrTEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayES4_S6_S4_S6_S4_NS0_3SmiES7_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESE_SI_SE_SI_SE_PNSA_IS8_EESK_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayES4_S6_S4_S6_S4_NS0_3SmiES7_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESE_SI_SE_SI_SE_PNSA_IS8_EESK_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayES4_S6_S4_S6_S4_NS0_3SmiES7_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESE_SI_SE_SI_SE_PNSA_IS8_EESK_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayES4_S6_S4_S6_S4_NS0_3SmiES7_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESE_SI_SE_SI_SE_PNSA_IS8_EESK_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayES4_S6_S4_S6_S4_NS0_3SmiES7_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESE_SI_SE_SI_SE_PNSA_IS8_EESK_:
.LFB26767:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$13, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$120, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$577875858245879045, %rcx
	movq	%rcx, (%rax)
	leaq	13(%rax), %rdx
	movl	$100993029, 8(%rax)
	movb	$7, 12(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L480
	movq	%rax, -160(%rbp)
	call	_ZdlPv@PLT
	movq	-160(%rbp), %rax
.L480:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L481
	movq	%rdx, (%r15)
.L481:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L482
	movq	%rdx, (%r14)
.L482:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L483
	movq	%rdx, 0(%r13)
.L483:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L484
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L484:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L485
	movq	%rdx, (%rbx)
.L485:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L486
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L486:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L487
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L487:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L488
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L488:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L489
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L489:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L490
	movq	-128(%rbp), %rsi
	movq	%rdx, (%rsi)
.L490:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L491
	movq	-136(%rbp), %rcx
	movq	%rdx, (%rcx)
.L491:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L492
	movq	-144(%rbp), %rbx
	movq	%rdx, (%rbx)
.L492:
	movq	96(%rax), %rax
	testq	%rax, %rax
	je	.L479
	movq	-152(%rbp), %rsi
	movq	%rax, (%rsi)
.L479:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L538
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L538:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26767:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayES4_S6_S4_S6_S4_NS0_3SmiES7_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESE_SI_SE_SI_SE_PNSA_IS8_EESK_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayES4_S6_S4_S6_S4_NS0_3SmiES7_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESE_SI_SE_SI_SE_PNSA_IS8_EESK_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayES4_S6_S4_S6_S4_NS0_3SmiES7_S7_S7_S7_S4_S8_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESE_SI_SE_SI_SE_PNSA_IS8_EESK_SK_SK_SK_SE_SM_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayES4_S6_S4_S6_S4_NS0_3SmiES7_S7_S7_S7_S4_S8_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESE_SI_SE_SI_SE_PNSA_IS8_EESK_SK_SK_SK_SE_SM_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayES4_S6_S4_S6_S4_NS0_3SmiES7_S7_S7_S7_S4_S8_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESE_SI_SE_SI_SE_PNSA_IS8_EESK_SK_SK_SK_SE_SM_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayES4_S6_S4_S6_S4_NS0_3SmiES7_S7_S7_S7_S4_S8_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESE_SI_SE_SI_SE_PNSA_IS8_EESK_SK_SK_SK_SE_SM_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayES4_S6_S4_S6_S4_NS0_3SmiES7_S7_S7_S7_S4_S8_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESE_SI_SE_SI_SE_PNSA_IS8_EESK_SK_SK_SK_SE_SM_:
.LFB26773:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$18, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$168, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -88(%rbp)
	movq	%r9, -96(%rbp)
	movq	%rax, -104(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	88(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	96(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	104(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	112(%rbp), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$1541, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movdqa	.LC4(%rip), %xmm0
	movw	%cx, 16(%rax)
	leaq	18(%rax), %rdx
	movq	%rax, -80(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L540
	movq	%rax, -200(%rbp)
	call	_ZdlPv@PLT
	movq	-200(%rbp), %rax
.L540:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L541
	movq	%rdx, (%r14)
.L541:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L542
	movq	%rdx, 0(%r13)
.L542:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L543
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L543:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L544
	movq	%rdx, (%rbx)
.L544:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L545
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L545:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L546
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L546:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L547
	movq	-112(%rbp), %rcx
	movq	%rdx, (%rcx)
.L547:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L548
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L548:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L549
	movq	-128(%rbp), %rcx
	movq	%rdx, (%rcx)
.L549:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L550
	movq	-136(%rbp), %rbx
	movq	%rdx, (%rbx)
.L550:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L551
	movq	-144(%rbp), %rsi
	movq	%rdx, (%rsi)
.L551:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L552
	movq	-152(%rbp), %rcx
	movq	%rdx, (%rcx)
.L552:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L553
	movq	-160(%rbp), %rbx
	movq	%rdx, (%rbx)
.L553:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L554
	movq	-168(%rbp), %rsi
	movq	%rdx, (%rsi)
.L554:
	movq	112(%rax), %rdx
	testq	%rdx, %rdx
	je	.L555
	movq	-176(%rbp), %rdi
	movq	%rdx, (%rdi)
.L555:
	movq	120(%rax), %rdx
	testq	%rdx, %rdx
	je	.L556
	movq	-184(%rbp), %rcx
	movq	%rdx, (%rcx)
.L556:
	movq	128(%rax), %rdx
	testq	%rdx, %rdx
	je	.L557
	movq	-192(%rbp), %rbx
	movq	%rdx, (%rbx)
.L557:
	movq	136(%rax), %rax
	testq	%rax, %rax
	je	.L539
	movq	%rax, (%r15)
.L539:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L618
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L618:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26773:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayES4_S6_S4_S6_S4_NS0_3SmiES7_S7_S7_S7_S4_S8_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESE_SI_SE_SI_SE_PNSA_IS8_EESK_SK_SK_SK_SE_SM_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayES4_S6_S4_S6_S4_NS0_3SmiES7_S7_S7_S7_S4_S8_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESE_SI_SE_SI_SE_PNSA_IS8_EESK_SK_SK_SK_SE_SM_
	.section	.text._ZN2v88internal33TypedArrayPrototypeSliceAssembler36GenerateTypedArrayPrototypeSliceImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33TypedArrayPrototypeSliceAssembler36GenerateTypedArrayPrototypeSliceImplEv
	.type	_ZN2v88internal33TypedArrayPrototypeSliceAssembler36GenerateTypedArrayPrototypeSliceImplEv, @function
_ZN2v88internal33TypedArrayPrototypeSliceAssembler36GenerateTypedArrayPrototypeSliceImplEv:
.LFB22465:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$600, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$1, %esi
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %r12
	movq	%r13, -4304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r14, %rcx
	movq	%r12, %rsi
	leaq	-4160(%rbp), %rdi
	movq	%rax, %rdx
	leaq	-4304(%rbp), %r14
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-4152(%rbp), %rbx
	movl	$2, %esi
	movq	%r12, %rdi
	movq	-4144(%rbp), %rcx
	movq	-4160(%rbp), %rax
	movq	%r12, -4032(%rbp)
	leaq	-3800(%rbp), %r12
	movq	%rbx, -4352(%rbp)
	movq	%rbx, -4008(%rbp)
	leaq	-4032(%rbp), %rbx
	movq	%rcx, -4336(%rbp)
	movq	%rcx, -4016(%rbp)
	movq	$1, -4024(%rbp)
	movq	%rax, -4368(%rbp)
	movq	%rax, -4000(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rbx, %rdi
	movq	%rbx, -4584(%rbp)
	leaq	-3608(%rbp), %rbx
	movq	%rax, -4320(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -3848(%rbp)
	movq	$0, -3840(%rbp)
	movq	%rax, %r15
	movq	-4304(%rbp), %rax
	movq	$0, -3832(%rbp)
	movq	%rax, -3856(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -3832(%rbp)
	movq	%rdx, -3840(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3816(%rbp)
	movq	%rax, -3848(%rbp)
	movq	$0, -3824(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4304(%rbp), %rax
	movl	$192, %edi
	movq	$0, -3656(%rbp)
	movq	$0, -3648(%rbp)
	movq	%rax, -3664(%rbp)
	movq	$0, -3640(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -3640(%rbp)
	movq	%rdx, -3648(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3624(%rbp)
	movq	%rax, -3656(%rbp)
	movq	$0, -3632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4304(%rbp), %rax
	movl	$192, %edi
	movq	$0, -3464(%rbp)
	movq	$0, -3456(%rbp)
	movq	%rax, -3472(%rbp)
	movq	$0, -3448(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-3416(%rbp), %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -3448(%rbp)
	movq	%rdx, -3456(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -4416(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -3432(%rbp)
	movq	%rax, -3464(%rbp)
	movq	$0, -3440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4304(%rbp), %rax
	movl	$216, %edi
	movq	$0, -3272(%rbp)
	movq	$0, -3264(%rbp)
	movq	%rax, -3280(%rbp)
	movq	$0, -3256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-3224(%rbp), %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -3256(%rbp)
	movq	%rdx, -3264(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -4432(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -3240(%rbp)
	movq	%rax, -3272(%rbp)
	movq	$0, -3248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4304(%rbp), %rax
	movl	$216, %edi
	movq	$0, -3080(%rbp)
	movq	$0, -3072(%rbp)
	movq	%rax, -3088(%rbp)
	movq	$0, -3064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-3032(%rbp), %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -3064(%rbp)
	movq	%rdx, -3072(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -4384(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -3048(%rbp)
	movq	%rax, -3080(%rbp)
	movq	$0, -3056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4304(%rbp), %rax
	movl	$240, %edi
	movq	$0, -2888(%rbp)
	movq	$0, -2880(%rbp)
	movq	%rax, -2896(%rbp)
	movq	$0, -2872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2840(%rbp), %rsi
	xorl	%ecx, %ecx
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rdx, -2872(%rbp)
	movq	%rdx, -2880(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -4480(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -2856(%rbp)
	movq	%rax, -2888(%rbp)
	movq	$0, -2864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4304(%rbp), %rax
	movl	$240, %edi
	movq	$0, -2696(%rbp)
	movq	$0, -2688(%rbp)
	movq	%rax, -2704(%rbp)
	movq	$0, -2680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-2648(%rbp), %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rdx, -2680(%rbp)
	movq	%rdx, -2688(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -4448(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -2664(%rbp)
	movq	%rax, -2696(%rbp)
	movq	$0, -2672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4304(%rbp), %rax
	movl	$264, %edi
	movq	$0, -2504(%rbp)
	movq	$0, -2496(%rbp)
	movq	%rax, -2512(%rbp)
	movq	$0, -2488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-2456(%rbp), %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rdx, -2488(%rbp)
	movq	%rdx, -2496(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -4464(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -2472(%rbp)
	movq	%rax, -2504(%rbp)
	movq	$0, -2480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4304(%rbp), %rax
	movl	$264, %edi
	movq	$0, -2312(%rbp)
	movq	$0, -2304(%rbp)
	movq	%rax, -2320(%rbp)
	movq	$0, -2296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-2264(%rbp), %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rdx, -2296(%rbp)
	movq	%rdx, -2304(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -4456(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -2280(%rbp)
	movq	%rax, -2312(%rbp)
	movq	$0, -2288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4304(%rbp), %rax
	movl	$312, %edi
	movq	$0, -2120(%rbp)
	movq	$0, -2112(%rbp)
	movq	%rax, -2128(%rbp)
	movq	$0, -2104(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-2072(%rbp), %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rdx, -2104(%rbp)
	movq	%rdx, -2112(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -4520(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -2088(%rbp)
	movq	%rax, -2120(%rbp)
	movq	$0, -2096(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4304(%rbp), %rax
	movl	$336, %edi
	movq	$0, -1928(%rbp)
	movq	$0, -1920(%rbp)
	movq	%rax, -1936(%rbp)
	movq	$0, -1912(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-1880(%rbp), %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rdx, -1912(%rbp)
	movq	%rdx, -1920(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -4576(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -1896(%rbp)
	movq	%rax, -1928(%rbp)
	movq	$0, -1904(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4304(%rbp), %rax
	movl	$360, %edi
	movq	$0, -1736(%rbp)
	movq	$0, -1728(%rbp)
	movq	%rax, -1744(%rbp)
	movq	$0, -1720(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-1688(%rbp), %rsi
	leaq	360(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 352(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movq	%rdx, -1720(%rbp)
	movq	%rdx, -1728(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -4488(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -1704(%rbp)
	movq	%rax, -1736(%rbp)
	movq	$0, -1712(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4304(%rbp), %rax
	movl	$432, %edi
	movq	$0, -1544(%rbp)
	movq	$0, -1536(%rbp)
	movq	%rax, -1552(%rbp)
	movq	$0, -1528(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-1496(%rbp), %rsi
	leaq	432(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, -1512(%rbp)
	movq	%rax, -1544(%rbp)
	movq	%rdx, -1528(%rbp)
	movq	%rdx, -1536(%rbp)
	xorl	%edx, %edx
	movq	$0, -1520(%rbp)
	movq	%rsi, -4560(%rbp)
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4304(%rbp), %rax
	movl	$432, %edi
	movq	$0, -1352(%rbp)
	movq	$0, -1344(%rbp)
	movq	%rax, -1360(%rbp)
	movq	$0, -1336(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-1304(%rbp), %rsi
	leaq	432(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, -1320(%rbp)
	movq	%rax, -1352(%rbp)
	movq	%rdx, -1336(%rbp)
	movq	%rdx, -1344(%rbp)
	xorl	%edx, %edx
	movq	$0, -1328(%rbp)
	movq	%rsi, -4528(%rbp)
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4304(%rbp), %rax
	movl	$312, %edi
	movq	$0, -1160(%rbp)
	movq	$0, -1152(%rbp)
	movq	%rax, -1168(%rbp)
	movq	$0, -1144(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1112(%rbp), %rsi
	xorl	%r8d, %r8d
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	xorl	%ecx, %ecx
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rdx, -1144(%rbp)
	movq	%rdx, -1152(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -4536(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -1128(%rbp)
	movq	%rax, -1160(%rbp)
	movq	$0, -1136(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4304(%rbp), %rax
	movl	$312, %edi
	movq	$0, -968(%rbp)
	movq	$0, -960(%rbp)
	movq	%rax, -976(%rbp)
	movq	$0, -952(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-920(%rbp), %rsi
	xorl	%ecx, %ecx
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rdx, -952(%rbp)
	movq	%rdx, -960(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -4544(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -936(%rbp)
	movq	%rax, -968(%rbp)
	movq	$0, -944(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4304(%rbp), %rax
	movl	$312, %edi
	movq	$0, -776(%rbp)
	movq	$0, -768(%rbp)
	movq	%rax, -784(%rbp)
	movq	$0, -760(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	312(%rax), %rdx
	leaq	-728(%rbp), %rsi
	movups	%xmm0, (%rax)
	movq	$0, 304(%rax)
	movq	%rsi, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rdx, -760(%rbp)
	movq	%rdx, -768(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -4552(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -744(%rbp)
	movq	%rax, -776(%rbp)
	movq	$0, -752(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4304(%rbp), %rax
	movl	$312, %edi
	movq	$0, -584(%rbp)
	movq	$0, -576(%rbp)
	movq	%rax, -592(%rbp)
	movq	$0, -568(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-536(%rbp), %rsi
	xorl	%ecx, %ecx
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rdx, -568(%rbp)
	movq	%rdx, -576(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -4504(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -552(%rbp)
	movq	%rax, -584(%rbp)
	movq	$0, -560(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4304(%rbp), %rax
	movl	$312, %edi
	movq	$0, -392(%rbp)
	movq	$0, -384(%rbp)
	movq	%rax, -400(%rbp)
	movq	$0, -376(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-344(%rbp), %rsi
	xorl	%ecx, %ecx
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rdx, -376(%rbp)
	movq	%rdx, -384(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -4512(%rbp)
	movq	%r14, %rsi
	movq	%rax, -392(%rbp)
	movups	%xmm0, -360(%rbp)
	movq	$0, -368(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movq	-4368(%rbp), %xmm1
	movl	$40, %edi
	movq	%r15, -176(%rbp)
	leaq	-3984(%rbp), %r15
	movhps	-4352(%rbp), %xmm1
	movaps	%xmm0, -3984(%rbp)
	movaps	%xmm1, -208(%rbp)
	movq	-4336(%rbp), %xmm1
	movq	$0, -3968(%rbp)
	movhps	-4320(%rbp), %xmm1
	movaps	%xmm1, -192(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rcx
	movdqa	-208(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-192(%rbp), %xmm4
	leaq	40(%rax), %rdx
	movq	%rax, -3984(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm4, 16(%rax)
	leaq	-3856(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	movq	%rax, -4496(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L620
	call	_ZdlPv@PLT
.L620:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3664(%rbp), %rax
	cmpq	$0, -3792(%rbp)
	movq	%rax, -4392(%rbp)
	leaq	-3472(%rbp), %rax
	movq	%rax, -4400(%rbp)
	jne	.L798
.L621:
	leaq	-3280(%rbp), %rax
	cmpq	$0, -3600(%rbp)
	movq	%rax, -4408(%rbp)
	jne	.L799
.L625:
	leaq	-3088(%rbp), %rax
	cmpq	$0, -3408(%rbp)
	movq	%rax, -4320(%rbp)
	jne	.L800
	cmpq	$0, -3216(%rbp)
	jne	.L801
.L631:
	leaq	-2896(%rbp), %rax
	cmpq	$0, -3024(%rbp)
	movq	%rax, -4416(%rbp)
	leaq	-2704(%rbp), %rax
	movq	%rax, -4432(%rbp)
	jne	.L802
.L634:
	leaq	-2512(%rbp), %rax
	cmpq	$0, -2832(%rbp)
	movq	%rax, -4440(%rbp)
	jne	.L803
.L638:
	leaq	-2320(%rbp), %rax
	cmpq	$0, -2640(%rbp)
	movq	%rax, -4336(%rbp)
	jne	.L804
	cmpq	$0, -2448(%rbp)
	jne	.L805
.L644:
	leaq	-2128(%rbp), %rax
	cmpq	$0, -2256(%rbp)
	leaq	-400(%rbp), %rbx
	movq	%rax, -4448(%rbp)
	jne	.L806
.L647:
	leaq	-1744(%rbp), %rax
	cmpq	$0, -2064(%rbp)
	movq	%rax, -4456(%rbp)
	leaq	-1936(%rbp), %rax
	movq	%rax, -4368(%rbp)
	jne	.L807
.L651:
	leaq	-1168(%rbp), %rax
	cmpq	$0, -1872(%rbp)
	movq	%rax, -4464(%rbp)
	jne	.L808
.L655:
	leaq	-1360(%rbp), %rax
	cmpq	$0, -1680(%rbp)
	movq	%rax, -4480(%rbp)
	leaq	-1552(%rbp), %rax
	movq	%rax, -4384(%rbp)
	jne	.L809
.L658:
	leaq	-784(%rbp), %rax
	cmpq	$0, -1488(%rbp)
	movq	%rax, -4488(%rbp)
	jne	.L810
.L663:
	cmpq	$0, -1296(%rbp)
	leaq	-976(%rbp), %r12
	jne	.L811
	cmpq	$0, -1104(%rbp)
	jne	.L812
.L668:
	leaq	-592(%rbp), %rax
	cmpq	$0, -912(%rbp)
	movq	%rax, -4352(%rbp)
	jne	.L813
	cmpq	$0, -720(%rbp)
	jne	.L814
.L671:
	cmpq	$0, -528(%rbp)
	jne	.L815
.L673:
	cmpq	$0, -336(%rbp)
	jne	.L816
.L675:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4352(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4488(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4464(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4480(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4384(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4456(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4368(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4448(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4336(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4440(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4432(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4416(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4320(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4408(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4400(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4392(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4496(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L817
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L798:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -3968(%rbp)
	movaps	%xmm0, -3984(%rbp)
	call	_Znwm@PLT
	movq	-4496(%rbp), %rdi
	movq	%r15, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -3984(%rbp)
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3984(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L622
	call	_ZdlPv@PLT
.L622:
	movq	(%r12), %rax
	movl	$63, %edx
	movq	%r14, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	movq	24(%rax), %r12
	movq	%rcx, -4352(%rbp)
	movq	16(%rax), %rcx
	movq	32(%rax), %rax
	movq	%rsi, -4368(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -4320(%rbp)
	movq	%rax, -4336(%rbp)
	movq	%r12, -4400(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal16kBuiltinName_364EPNS0_8compiler18CodeAssemblerStateE
	movq	-4336(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal27TypedArrayBuiltinsAssembler18ValidateTypedArrayENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEEPKc@PLT
	movq	%r15, %rdi
	movq	%rax, -4392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$62, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$66, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$48, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-4392(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE(%rip), %esi
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal30Convert8ATintptr9ATuintptr_188EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8UintPtrTEEE@PLT
	movl	$71, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -4408(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	-4352(%rbp), %xmm7
	movq	-4320(%rbp), %rcx
	movhps	-4368(%rbp), %xmm7
	movq	%rcx, -4112(%rbp)
	movaps	%xmm7, -4128(%rbp)
	pushq	-4112(%rbp)
	pushq	-4120(%rbp)
	pushq	-4128(%rbp)
	movaps	%xmm7, -4352(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r15, %rdi
	movq	%rax, -4368(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$73, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4368(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	-4336(%rbp), %xmm6
	movq	-4320(%rbp), %xmm2
	movdqa	-4352(%rbp), %xmm7
	movaps	%xmm0, -3984(%rbp)
	movq	-4408(%rbp), %xmm3
	movhps	-4392(%rbp), %xmm6
	movq	$0, -3968(%rbp)
	movhps	-4400(%rbp), %xmm2
	movaps	%xmm6, -4336(%rbp)
	movhps	-4368(%rbp), %xmm3
	movaps	%xmm2, -4320(%rbp)
	movaps	%xmm3, -4368(%rbp)
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm5
	movq	%r15, %rsi
	leaq	64(%rax), %rdx
	movq	%rax, -3984(%rbp)
	movups	%xmm5, (%rax)
	movdqa	-192(%rbp), %xmm5
	movq	%rdx, -3968(%rbp)
	movups	%xmm5, 16(%rax)
	movdqa	-176(%rbp), %xmm5
	movq	%rdx, -3976(%rbp)
	movups	%xmm5, 32(%rax)
	movdqa	-160(%rbp), %xmm5
	movups	%xmm5, 48(%rax)
	leaq	-3664(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -4392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L623
	call	_ZdlPv@PLT
.L623:
	movdqa	-4352(%rbp), %xmm2
	movdqa	-4320(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movdqa	-4336(%rbp), %xmm5
	movaps	%xmm0, -3984(%rbp)
	movaps	%xmm2, -208(%rbp)
	movdqa	-4368(%rbp), %xmm2
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movq	$0, -3968(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm4
	movq	%r15, %rsi
	movdqa	-192(%rbp), %xmm7
	movdqa	-176(%rbp), %xmm5
	movdqa	-160(%rbp), %xmm2
	leaq	64(%rax), %rdx
	movq	%rax, -3984(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm2, 48(%rax)
	leaq	-3472(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	movq	%rax, -4400(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L624
	call	_ZdlPv@PLT
.L624:
	movq	-4416(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L799:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movabsq	$577875858245879045, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3968(%rbp)
	movaps	%xmm0, -3984(%rbp)
	call	_Znwm@PLT
	movq	-4392(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3984(%rbp)
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3984(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L626
	call	_ZdlPv@PLT
.L626:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	32(%rax), %rsi
	movq	24(%rax), %r12
	movq	40(%rax), %rdx
	movq	%rbx, -4336(%rbp)
	movq	%rcx, -4352(%rbp)
	movq	48(%rax), %rbx
	movq	16(%rax), %rcx
	movq	56(%rax), %rax
	movq	%rsi, -4440(%rbp)
	movq	%r13, %rsi
	movq	%r12, -4408(%rbp)
	movq	%rcx, -4368(%rbp)
	movq	%rdx, -4608(%rbp)
	movq	%rax, -4320(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rcx
	movq	%r15, %rdi
	movq	-4320(%rbp), %rdx
	call	_ZN2v88internal17CodeStubAssembler22ConvertToRelativeIndexENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_7IntPtrTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$72, %edi
	movq	-4336(%rbp), %xmm0
	movq	%r12, -144(%rbp)
	movq	$0, -3968(%rbp)
	movhps	-4352(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-4368(%rbp), %xmm0
	movhps	-4408(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-4440(%rbp), %xmm0
	movhps	-4608(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	%rbx, %xmm0
	movhps	-4320(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3984(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-208(%rbp), %xmm4
	movdqa	-192(%rbp), %xmm7
	movdqa	-176(%rbp), %xmm5
	leaq	72(%rax), %rdx
	movq	%rax, -3984(%rbp)
	movdqa	-160(%rbp), %xmm2
	movq	%rcx, 64(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm2, 48(%rax)
	leaq	-3280(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	movq	%rax, -4408(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L627
	call	_ZdlPv@PLT
.L627:
	movq	-4432(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L800:
	movq	-4416(%rbp), %rsi
	movq	%r14, %rdi
	movabsq	$577875858245879045, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3968(%rbp)
	movaps	%xmm0, -3984(%rbp)
	call	_Znwm@PLT
	movq	-4400(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3984(%rbp)
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3984(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L629
	call	_ZdlPv@PLT
.L629:
	movq	(%rbx), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rsi
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	32(%rax), %r12
	movq	%rsi, -4368(%rbp)
	movq	40(%rax), %rsi
	movq	%rbx, -4320(%rbp)
	movq	48(%rax), %rbx
	movq	%rcx, -4336(%rbp)
	movq	16(%rax), %rcx
	movq	56(%rax), %rax
	movq	%rsi, -4416(%rbp)
	xorl	%esi, %esi
	movq	%rcx, -4352(%rbp)
	movq	%rax, -4440(%rbp)
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$72, %edi
	movq	-4320(%rbp), %xmm0
	movq	$0, -3968(%rbp)
	movq	%rax, -144(%rbp)
	movhps	-4336(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-4352(%rbp), %xmm0
	movhps	-4368(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	%r12, %xmm0
	movhps	-4416(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	%rbx, %xmm0
	movhps	-4440(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3984(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-208(%rbp), %xmm4
	movdqa	-192(%rbp), %xmm7
	movdqa	-176(%rbp), %xmm3
	leaq	72(%rax), %rdx
	movq	%rax, -3984(%rbp)
	movdqa	-160(%rbp), %xmm5
	movq	%rcx, 64(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm5, 48(%rax)
	leaq	-3088(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	movq	%rax, -4320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L630
	call	_ZdlPv@PLT
.L630:
	movq	-4384(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3216(%rbp)
	je	.L631
.L801:
	movq	-4432(%rbp), %rsi
	movq	%r14, %rdi
	movabsq	$577875858245879045, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -3968(%rbp)
	movaps	%xmm0, -3984(%rbp)
	call	_Znwm@PLT
	movq	-4408(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, (%rax)
	leaq	9(%rax), %rdx
	movb	$5, 8(%rax)
	movq	%rax, -3984(%rbp)
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3984(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L632
	call	_ZdlPv@PLT
.L632:
	movq	(%rbx), %rax
	movl	$72, %edi
	movdqu	48(%rax), %xmm0
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	64(%rax), %rdx
	movdqu	(%rax), %xmm4
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -144(%rbp)
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -3984(%rbp)
	movq	$0, -3968(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-208(%rbp), %xmm2
	movdqa	-192(%rbp), %xmm6
	movdqa	-176(%rbp), %xmm7
	leaq	72(%rax), %rdx
	movq	%rax, -3984(%rbp)
	movdqa	-160(%rbp), %xmm3
	movq	-4320(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L633
	call	_ZdlPv@PLT
.L633:
	movq	-4384(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L802:
	movq	-4384(%rbp), %rsi
	movq	%r14, %rdi
	movabsq	$577875858245879045, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -3968(%rbp)
	movaps	%xmm0, -3984(%rbp)
	call	_Znwm@PLT
	movq	-4320(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, (%rax)
	leaq	9(%rax), %rdx
	movb	$5, 8(%rax)
	movq	%rax, -3984(%rbp)
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3984(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L635
	call	_ZdlPv@PLT
.L635:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	40(%rax), %rsi
	movq	56(%rax), %rdx
	movq	8(%rax), %rbx
	movq	24(%rax), %rcx
	movq	%rsi, -4416(%rbp)
	movq	48(%rax), %rsi
	movq	%rdx, -4440(%rbp)
	movq	64(%rax), %rdx
	movq	%rbx, -4352(%rbp)
	movq	16(%rax), %rbx
	movq	%rcx, -4384(%rbp)
	movq	32(%rax), %rcx
	movq	%rsi, -4432(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	(%rax), %r12
	movq	%rdx, -4608(%rbp)
	movl	$72, %edx
	movq	%rcx, -4368(%rbp)
	movq	%rbx, -4336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$79, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %xmm5
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	-4336(%rbp), %rax
	movhps	-4352(%rbp), %xmm5
	subq	$8, %rsp
	movaps	%xmm5, -4096(%rbp)
	movq	%rax, -4080(%rbp)
	pushq	-4080(%rbp)
	pushq	-4088(%rbp)
	pushq	-4096(%rbp)
	movaps	%xmm5, -4352(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$81, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %xmm2
	pxor	%xmm0, %xmm0
	movq	-4608(%rbp), %xmm4
	movq	-4432(%rbp), %xmm7
	movdqa	-4352(%rbp), %xmm5
	movl	$80, %edi
	movaps	%xmm0, -3984(%rbp)
	movq	-4368(%rbp), %xmm3
	punpcklqdq	%xmm2, %xmm4
	movq	-4336(%rbp), %xmm6
	movq	$0, -3968(%rbp)
	movhps	-4440(%rbp), %xmm7
	movaps	%xmm4, -4608(%rbp)
	movhps	-4416(%rbp), %xmm3
	movhps	-4384(%rbp), %xmm6
	movaps	%xmm7, -4432(%rbp)
	movaps	%xmm3, -4368(%rbp)
	movaps	%xmm6, -4336(%rbp)
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm5
	movq	%r15, %rsi
	movdqa	-192(%rbp), %xmm4
	movdqa	-176(%rbp), %xmm6
	movdqa	-160(%rbp), %xmm7
	leaq	80(%rax), %rdx
	movq	%rax, -3984(%rbp)
	movdqa	-144(%rbp), %xmm3
	movups	%xmm5, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm3, 64(%rax)
	leaq	-2896(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	movq	%rax, -4416(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L636
	call	_ZdlPv@PLT
.L636:
	movdqa	-4352(%rbp), %xmm5
	movdqa	-4336(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movl	$80, %edi
	movdqa	-4368(%rbp), %xmm4
	movdqa	-4432(%rbp), %xmm6
	movaps	%xmm0, -3984(%rbp)
	movdqa	-4608(%rbp), %xmm7
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movq	$0, -3968(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm3
	movq	%r15, %rsi
	movdqa	-192(%rbp), %xmm5
	movdqa	-176(%rbp), %xmm2
	movdqa	-160(%rbp), %xmm4
	leaq	80(%rax), %rdx
	movq	%rax, -3984(%rbp)
	movdqa	-144(%rbp), %xmm6
	movups	%xmm3, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm6, 64(%rax)
	leaq	-2704(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	movq	%rax, -4432(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L637
	call	_ZdlPv@PLT
.L637:
	movq	-4448(%rbp), %rcx
	movq	-4480(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L803:
	movq	-4480(%rbp), %rsi
	movq	%r14, %rdi
	movabsq	$577875858245879045, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -3968(%rbp)
	movaps	%xmm0, -3984(%rbp)
	call	_Znwm@PLT
	movl	$2053, %r11d
	movq	-4416(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, (%rax)
	leaq	10(%rax), %rdx
	movw	%r11w, 8(%rax)
	movq	%rax, -3984(%rbp)
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3984(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L639
	call	_ZdlPv@PLT
.L639:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	40(%rax), %rdx
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	32(%rax), %rsi
	movq	%rdx, -4608(%rbp)
	movq	56(%rax), %rdx
	movq	24(%rax), %r12
	movq	%rbx, -4352(%rbp)
	movq	%rcx, -4368(%rbp)
	movq	48(%rax), %rbx
	movq	16(%rax), %rcx
	movq	%rdx, -4624(%rbp)
	movq	64(%rax), %rdx
	movq	72(%rax), %rax
	movq	%rsi, -4480(%rbp)
	movq	%r13, %rsi
	movq	%rcx, -4384(%rbp)
	movq	%r12, -4440(%rbp)
	movq	%rdx, -4592(%rbp)
	movq	%rax, -4336(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rcx
	movq	%r15, %rdi
	movq	-4336(%rbp), %rdx
	call	_ZN2v88internal17CodeStubAssembler22ConvertToRelativeIndexENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_7IntPtrTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$88, %edi
	movq	%r12, -128(%rbp)
	movq	-4352(%rbp), %xmm0
	movq	$0, -3968(%rbp)
	movhps	-4368(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-4384(%rbp), %xmm0
	movhps	-4440(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-4480(%rbp), %xmm0
	movhps	-4608(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	%rbx, %xmm0
	movhps	-4624(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-4592(%rbp), %xmm0
	movhps	-4336(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3984(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-208(%rbp), %xmm7
	movdqa	-192(%rbp), %xmm3
	movdqa	-176(%rbp), %xmm5
	leaq	88(%rax), %rdx
	movq	%rax, -3984(%rbp)
	movdqa	-160(%rbp), %xmm2
	movdqa	-144(%rbp), %xmm4
	movq	%rcx, 80(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm4, 64(%rax)
	leaq	-2512(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	movq	%rax, -4440(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L640
	call	_ZdlPv@PLT
.L640:
	movq	-4464(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L804:
	movq	-4448(%rbp), %rsi
	movq	%r14, %rdi
	movabsq	$577875858245879045, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -3968(%rbp)
	movaps	%xmm0, -3984(%rbp)
	call	_Znwm@PLT
	movl	$2053, %r10d
	movq	-4432(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, (%rax)
	leaq	10(%rax), %rdx
	movw	%r10w, 8(%rax)
	movq	%rax, -3984(%rbp)
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3984(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L642
	call	_ZdlPv@PLT
.L642:
	movq	(%rbx), %rax
	movl	$88, %edi
	movdqu	64(%rax), %xmm0
	movq	48(%rax), %rdx
	movdqu	16(%rax), %xmm3
	movdqu	32(%rax), %xmm2
	movdqu	48(%rax), %xmm1
	movdqu	(%rax), %xmm6
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -128(%rbp)
	movaps	%xmm6, -208(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -3984(%rbp)
	movq	$0, -3968(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-208(%rbp), %xmm7
	movdqa	-192(%rbp), %xmm3
	movdqa	-176(%rbp), %xmm5
	leaq	88(%rax), %rdx
	movq	%rax, -3984(%rbp)
	movdqa	-160(%rbp), %xmm2
	movdqa	-144(%rbp), %xmm4
	movq	%rcx, 80(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm4, 64(%rax)
	leaq	-2320(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	movq	%rax, -4336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L643
	call	_ZdlPv@PLT
.L643:
	movq	-4456(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2448(%rbp)
	je	.L644
.L805:
	movq	-4464(%rbp), %rsi
	movq	%r14, %rdi
	movabsq	$577875858245879045, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$11, %edi
	movq	$0, -3968(%rbp)
	movaps	%xmm0, -3984(%rbp)
	call	_Znwm@PLT
	movl	$2053, %r9d
	movq	-4440(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, (%rax)
	leaq	11(%rax), %rdx
	movw	%r9w, 8(%rax)
	movb	$5, 10(%rax)
	movq	%rax, -3984(%rbp)
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3984(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L645
	call	_ZdlPv@PLT
.L645:
	movq	(%rbx), %rax
	movl	$88, %edi
	movdqu	64(%rax), %xmm0
	movdqu	16(%rax), %xmm3
	movdqu	32(%rax), %xmm2
	movdqu	48(%rax), %xmm1
	movq	80(%rax), %rdx
	movdqu	(%rax), %xmm6
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -192(%rbp)
	movq	%rdx, -128(%rbp)
	movaps	%xmm6, -208(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -3984(%rbp)
	movq	$0, -3968(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-208(%rbp), %xmm7
	movdqa	-192(%rbp), %xmm3
	movdqa	-176(%rbp), %xmm5
	leaq	88(%rax), %rdx
	movq	%rax, -3984(%rbp)
	movdqa	-160(%rbp), %xmm2
	movdqa	-144(%rbp), %xmm4
	movq	%rcx, 80(%rax)
	movq	-4336(%rbp), %rdi
	movups	%xmm7, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm4, 64(%rax)
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L646
	call	_ZdlPv@PLT
.L646:
	movq	-4456(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L644
	.p2align 4,,10
	.p2align 3
.L806:
	movq	-4456(%rbp), %rsi
	movq	%r14, %rdi
	movabsq	$577875858245879045, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$11, %edi
	movq	$0, -3968(%rbp)
	movaps	%xmm0, -3984(%rbp)
	call	_Znwm@PLT
	movl	$2053, %r8d
	movq	-4336(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, (%rax)
	leaq	11(%rax), %rdx
	movw	%r8w, 8(%rax)
	movb	$5, 10(%rax)
	movq	%rax, -3984(%rbp)
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3984(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L648
	call	_ZdlPv@PLT
.L648:
	movq	(%rbx), %rax
	leaq	.LC2(%rip), %rsi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	32(%rax), %rdx
	movq	48(%rax), %rdi
	movq	56(%rax), %r10
	movq	72(%rax), %r11
	movq	%rcx, -4448(%rbp)
	movq	16(%rax), %rcx
	movq	64(%rax), %r12
	movq	%rbx, -4384(%rbp)
	movq	24(%rax), %rbx
	movq	%rdx, -4464(%rbp)
	movl	$80, %edx
	movq	%rcx, -4456(%rbp)
	movq	40(%rax), %rcx
	movq	80(%rax), %rax
	movq	%rdi, -4480(%rbp)
	movq	%r14, %rdi
	movq	%r10, -4608(%rbp)
	movq	%r11, -4592(%rbp)
	movq	%rcx, -4368(%rbp)
	movq	%rbx, -4352(%rbp)
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$84, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%r12, -4624(%rbp)
	movq	%rbx, -4640(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler9IntPtrMaxENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal35Convert13ATPositiveSmi8ATintptr_189EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movl	$89, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$88, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal16kBuiltinName_364EPNS0_8compiler18CodeAssemblerStateE
	movq	-4368(%rbp), %rcx
	movq	%r12, %r8
	movq	%r13, %rdi
	movq	-4352(%rbp), %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal35TypedArraySpeciesCreateByLength_350EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEPKcNS4_INS0_12JSTypedArrayEEENS4_INS0_3SmiEEE@PLT
	movl	$87, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$91, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, -4648(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4648(%rbp), %r8
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, -4648(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-4648(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17IntPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, -4648(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %xmm6
	pxor	%xmm0, %xmm0
	movq	-4640(%rbp), %xmm5
	movq	-4624(%rbp), %xmm2
	movl	$104, %edi
	movq	-4480(%rbp), %xmm4
	movaps	%xmm0, -3984(%rbp)
	punpcklqdq	%xmm6, %xmm5
	movq	%rbx, -112(%rbp)
	movq	-4384(%rbp), %xmm6
	movq	-4464(%rbp), %xmm7
	movhps	-4592(%rbp), %xmm2
	movaps	%xmm5, -128(%rbp)
	movq	-4456(%rbp), %xmm3
	movhps	-4608(%rbp), %xmm4
	movhps	-4448(%rbp), %xmm6
	movaps	%xmm5, -4640(%rbp)
	movhps	-4368(%rbp), %xmm7
	movhps	-4352(%rbp), %xmm3
	movaps	%xmm2, -4624(%rbp)
	movaps	%xmm4, -4480(%rbp)
	movaps	%xmm7, -4368(%rbp)
	movaps	%xmm3, -4352(%rbp)
	movaps	%xmm6, -4384(%rbp)
	movaps	%xmm6, -208(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movq	$0, -3968(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-208(%rbp), %xmm7
	movdqa	-192(%rbp), %xmm3
	movdqa	-176(%rbp), %xmm5
	leaq	104(%rax), %rdx
	movq	%rax, -3984(%rbp)
	movdqa	-160(%rbp), %xmm2
	movdqa	-144(%rbp), %xmm4
	movq	%rcx, 96(%rax)
	movdqa	-128(%rbp), %xmm6
	movups	%xmm7, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm6, 80(%rax)
	leaq	-2128(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	movq	%rax, -4448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L649
	call	_ZdlPv@PLT
.L649:
	movdqa	-4384(%rbp), %xmm1
	movdqa	-4352(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movl	$104, %edi
	movdqa	-4368(%rbp), %xmm3
	movdqa	-4480(%rbp), %xmm5
	movq	%rbx, -112(%rbp)
	leaq	-400(%rbp), %rbx
	movdqa	-4624(%rbp), %xmm2
	movdqa	-4640(%rbp), %xmm4
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm0, -3984(%rbp)
	movq	$0, -3968(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movdqa	-208(%rbp), %xmm6
	movdqa	-192(%rbp), %xmm1
	leaq	104(%rax), %rdx
	movq	%rax, -3984(%rbp)
	movdqa	-176(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm2
	movq	%rcx, 96(%rax)
	movdqa	-160(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm5
	movups	%xmm6, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm5, 64(%rax)
	movups	%xmm2, 80(%rax)
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L650
	call	_ZdlPv@PLT
.L650:
	movq	-4512(%rbp), %rcx
	movq	-4520(%rbp), %rdx
	movq	%r14, %rdi
	movq	-4648(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L807:
	movq	-4520(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4264(%rbp)
	leaq	-4064(%rbp), %r12
	movq	$0, -4256(%rbp)
	movq	$0, -4248(%rbp)
	movq	$0, -4240(%rbp)
	movq	$0, -4232(%rbp)
	movq	$0, -4224(%rbp)
	movq	$0, -4216(%rbp)
	movq	$0, -4208(%rbp)
	movq	$0, -4200(%rbp)
	movq	$0, -4192(%rbp)
	movq	$0, -4184(%rbp)
	movq	$0, -4176(%rbp)
	movq	$0, -4168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4168(%rbp), %rax
	movq	-4448(%rbp), %rdi
	leaq	-4232(%rbp), %r9
	pushq	%rax
	leaq	-4176(%rbp), %rax
	leaq	-4248(%rbp), %rcx
	pushq	%rax
	leaq	-4184(%rbp), %rax
	leaq	-4240(%rbp), %r8
	pushq	%rax
	leaq	-4192(%rbp), %rax
	leaq	-4256(%rbp), %rdx
	pushq	%rax
	leaq	-4200(%rbp), %rax
	leaq	-4264(%rbp), %rsi
	pushq	%rax
	leaq	-4208(%rbp), %rax
	pushq	%rax
	leaq	-4216(%rbp), %rax
	pushq	%rax
	leaq	-4224(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayES4_S6_S4_S6_S4_NS0_3SmiES7_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESE_SI_SE_SI_SE_PNSA_IS8_EESK_
	addq	$64, %rsp
	movl	$93, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4224(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal18EnsureAttached_369EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_12JSTypedArrayEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-4264(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movl	$120, %edi
	movq	%rax, %r8
	movq	-4224(%rbp), %rax
	movaps	%xmm0, -4064(%rbp)
	movq	%rdx, -208(%rbp)
	movq	-4256(%rbp), %rdx
	movq	%r8, -96(%rbp)
	movq	%rdx, -200(%rbp)
	movq	-4248(%rbp), %rdx
	movq	%rax, -168(%rbp)
	movq	%rdx, -192(%rbp)
	movq	-4240(%rbp), %rdx
	movq	%rax, -104(%rbp)
	movq	%rdx, -184(%rbp)
	movq	-4232(%rbp), %rdx
	movq	$0, -4048(%rbp)
	movq	%rdx, -176(%rbp)
	movq	-4216(%rbp), %rdx
	movq	%rdx, -160(%rbp)
	movq	-4208(%rbp), %rdx
	movq	%rdx, -152(%rbp)
	movq	-4200(%rbp), %rdx
	movq	%rdx, -144(%rbp)
	movq	-4192(%rbp), %rdx
	movq	%rdx, -136(%rbp)
	movq	-4184(%rbp), %rdx
	movq	%rdx, -128(%rbp)
	movq	-4176(%rbp), %rdx
	movq	%rdx, -120(%rbp)
	movq	-4168(%rbp), %rdx
	movq	%rdx, -112(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r12, %rsi
	movdqa	-208(%rbp), %xmm4
	movdqa	-192(%rbp), %xmm6
	movdqa	-176(%rbp), %xmm1
	leaq	120(%rax), %rdx
	movq	%rax, -4064(%rbp)
	movdqa	-160(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm3
	movq	%rcx, 112(%rax)
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm2
	movups	%xmm4, (%rax)
	movq	-4456(%rbp), %rdi
	movups	%xmm6, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm3, 64(%rax)
	movups	%xmm5, 80(%rax)
	movups	%xmm2, 96(%rax)
	movq	%rdx, -4048(%rbp)
	movq	%rdx, -4056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L652
	call	_ZdlPv@PLT
.L652:
	movq	-4488(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1936(%rbp), %rax
	cmpq	$0, -3976(%rbp)
	movq	%rax, -4368(%rbp)
	jne	.L818
.L653:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L808:
	movq	-4576(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$14, %edi
	movq	$0, -3968(%rbp)
	movaps	%xmm0, -3984(%rbp)
	call	_Znwm@PLT
	movl	$1799, %edi
	movq	%r15, %rsi
	movabsq	$577875858245879045, %rcx
	movw	%di, 12(%rax)
	movq	-4368(%rbp), %rdi
	leaq	14(%rax), %rdx
	movq	%rcx, (%rax)
	movl	$100993029, 8(%rax)
	movq	%rax, -3984(%rbp)
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3984(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L656
	call	_ZdlPv@PLT
.L656:
	movq	(%r12), %rax
	movl	$104, %edi
	movdqu	80(%rax), %xmm0
	movdqu	16(%rax), %xmm4
	movdqu	32(%rax), %xmm3
	movdqu	48(%rax), %xmm2
	movdqu	64(%rax), %xmm1
	movq	96(%rax), %rdx
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movdqu	(%rax), %xmm6
	movaps	%xmm4, -192(%rbp)
	movq	%rdx, -112(%rbp)
	movaps	%xmm6, -208(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -3984(%rbp)
	movq	$0, -3968(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-208(%rbp), %xmm4
	movdqa	-192(%rbp), %xmm1
	movdqa	-176(%rbp), %xmm7
	leaq	104(%rax), %rdx
	movq	%rax, -3984(%rbp)
	movdqa	-160(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm5
	movq	%rcx, 96(%rax)
	movdqa	-128(%rbp), %xmm2
	movups	%xmm4, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm5, 64(%rax)
	movups	%xmm2, 80(%rax)
	leaq	-1168(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	movq	%rax, -4464(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L657
	call	_ZdlPv@PLT
.L657:
	movq	-4536(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L809:
	movq	-4488(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$15, %edi
	movq	$0, -3968(%rbp)
	movaps	%xmm0, -3984(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movabsq	$577875858245879045, %rcx
	movq	-4456(%rbp), %rdi
	movw	%si, 12(%rax)
	leaq	15(%rax), %rdx
	movq	%r15, %rsi
	movq	%rcx, (%rax)
	movl	$100993029, 8(%rax)
	movb	$7, 14(%rax)
	movq	%rax, -3984(%rbp)
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3984(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L659
	call	_ZdlPv@PLT
.L659:
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	movq	24(%rax), %rdx
	movq	40(%rax), %rdi
	movq	(%rax), %rcx
	movq	56(%rax), %r10
	movq	72(%rax), %r11
	movq	%rsi, -4520(%rbp)
	movq	16(%rax), %rsi
	movq	%rdx, -4608(%rbp)
	movq	32(%rax), %rdx
	movq	%rdi, -4592(%rbp)
	movq	48(%rax), %rdi
	movq	%rsi, -4576(%rbp)
	movq	88(%rax), %rsi
	movq	%rdx, -4624(%rbp)
	movq	96(%rax), %rdx
	movq	%rcx, -4488(%rbp)
	movq	64(%rax), %rcx
	movq	%r10, -4648(%rbp)
	movq	80(%rax), %r10
	movq	%rdi, -4640(%rbp)
	movq	%r14, %rdi
	movq	%rsi, -4352(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	112(%rax), %r12
	movq	%rdx, -4480(%rbp)
	movl	$95, %edx
	movq	%r11, -4672(%rbp)
	movq	%r10, -4688(%rbp)
	movq	%rcx, -4384(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rsi
	movq	%r15, %r9
	movq	%r13, %rdi
	movq	-4352(%rbp), %r8
	movq	-4384(%rbp), %rcx
	movq	-4480(%rbp), %rdx
	call	_ZN2v88internal12FastCopy_365EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_12JSTypedArrayEEES6_NS4_INS0_7IntPtrTEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE
	movq	%r12, %xmm6
	movq	%r12, %xmm2
	movq	-4480(%rbp), %rax
	movq	-4384(%rbp), %xmm1
	movl	$144, %edi
	movq	-4576(%rbp), %xmm0
	leaq	-4064(%rbp), %r12
	movq	%rax, %xmm3
	movq	%rax, %xmm4
	movq	-4688(%rbp), %xmm7
	movq	-4488(%rbp), %xmm8
	punpcklqdq	%xmm3, %xmm2
	punpcklqdq	%xmm6, %xmm4
	movdqa	%xmm1, %xmm5
	movq	-4640(%rbp), %xmm6
	movdqa	%xmm1, %xmm3
	movhps	-4608(%rbp), %xmm0
	movq	-4624(%rbp), %xmm1
	movhps	-4352(%rbp), %xmm5
	movhps	-4352(%rbp), %xmm7
	movhps	-4672(%rbp), %xmm3
	movhps	-4648(%rbp), %xmm6
	movaps	%xmm0, -4576(%rbp)
	movhps	-4592(%rbp), %xmm1
	movhps	-4520(%rbp), %xmm8
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -4736(%rbp)
	movaps	%xmm2, -4720(%rbp)
	movaps	%xmm4, -4704(%rbp)
	movaps	%xmm7, -4688(%rbp)
	movaps	%xmm3, -4672(%rbp)
	movaps	%xmm6, -4640(%rbp)
	movaps	%xmm1, -4624(%rbp)
	movaps	%xmm8, -4352(%rbp)
	movaps	%xmm8, -208(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm0, -4064(%rbp)
	movq	$0, -4048(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm4
	movq	%r12, %rsi
	movdqa	-192(%rbp), %xmm1
	movdqa	-176(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm3
	leaq	144(%rax), %rdx
	movq	%rax, -4064(%rbp)
	movups	%xmm4, (%rax)
	movdqa	-128(%rbp), %xmm2
	movdqa	-112(%rbp), %xmm4
	movups	%xmm1, 16(%rax)
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm1
	movdqa	-144(%rbp), %xmm5
	movups	%xmm7, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm5, 64(%rax)
	movups	%xmm2, 80(%rax)
	movups	%xmm4, 96(%rax)
	movups	%xmm6, 112(%rax)
	movups	%xmm1, 128(%rax)
	leaq	-1360(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4048(%rbp)
	movq	%rdx, -4056(%rbp)
	movq	%rax, -4480(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L660
	call	_ZdlPv@PLT
.L660:
	movq	-4528(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1552(%rbp), %rax
	cmpq	$0, -3976(%rbp)
	movq	%rax, -4384(%rbp)
	jne	.L819
.L661:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L810:
	movq	-4560(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4296(%rbp)
	movq	$0, -4288(%rbp)
	movq	$0, -4280(%rbp)
	movq	$0, -4272(%rbp)
	movq	$0, -4264(%rbp)
	movq	$0, -4256(%rbp)
	movq	$0, -4248(%rbp)
	movq	$0, -4240(%rbp)
	movq	$0, -4232(%rbp)
	movq	$0, -4224(%rbp)
	movq	$0, -4216(%rbp)
	movq	$0, -4208(%rbp)
	movq	$0, -4200(%rbp)
	movq	$0, -4192(%rbp)
	movq	$0, -4184(%rbp)
	movq	$0, -4176(%rbp)
	movq	$0, -4168(%rbp)
	movq	$0, -4064(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-4064(%rbp), %rax
	movq	-4384(%rbp), %rdi
	pushq	%rax
	leaq	-4168(%rbp), %rax
	leaq	-4280(%rbp), %rcx
	pushq	%rax
	leaq	-4176(%rbp), %rax
	leaq	-4288(%rbp), %rdx
	pushq	%rax
	leaq	-4184(%rbp), %rax
	leaq	-4296(%rbp), %rsi
	pushq	%rax
	leaq	-4192(%rbp), %rax
	leaq	-4264(%rbp), %r9
	pushq	%rax
	leaq	-4200(%rbp), %rax
	leaq	-4272(%rbp), %r8
	pushq	%rax
	leaq	-4208(%rbp), %rax
	pushq	%rax
	leaq	-4216(%rbp), %rax
	pushq	%rax
	leaq	-4224(%rbp), %rax
	pushq	%rax
	leaq	-4232(%rbp), %rax
	pushq	%rax
	leaq	-4240(%rbp), %rax
	pushq	%rax
	leaq	-4248(%rbp), %rax
	pushq	%rax
	leaq	-4256(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayES4_S6_S4_S6_S4_NS0_3SmiES7_S7_S7_S7_S4_S8_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESE_SI_SE_SI_SE_PNSA_IS8_EESK_SK_SK_SK_SE_SM_
	movq	-4296(%rbp), %rax
	pxor	%xmm0, %xmm0
	addq	$112, %rsp
	movl	$104, %edi
	movaps	%xmm0, -3984(%rbp)
	movq	%rax, -208(%rbp)
	movq	-4288(%rbp), %rax
	movq	$0, -3968(%rbp)
	movq	%rax, -200(%rbp)
	movq	-4280(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-4272(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-4264(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-4256(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-4248(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-4240(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-4232(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-4224(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-4216(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-4208(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-4200(%rbp), %rax
	movq	%rax, -112(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-208(%rbp), %xmm7
	movdqa	-192(%rbp), %xmm3
	movdqa	-176(%rbp), %xmm5
	leaq	104(%rax), %rdx
	movq	%rax, -3984(%rbp)
	movdqa	-160(%rbp), %xmm2
	movdqa	-144(%rbp), %xmm4
	movq	%rcx, 96(%rax)
	movdqa	-128(%rbp), %xmm6
	movq	-4488(%rbp), %rdi
	movups	%xmm7, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm6, 80(%rax)
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L664
	call	_ZdlPv@PLT
.L664:
	movq	-4552(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L811:
	movq	-4528(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$18, %edi
	movq	$0, -3968(%rbp)
	movaps	%xmm0, -3984(%rbp)
	call	_Znwm@PLT
	movl	$1541, %ecx
	movdqa	.LC4(%rip), %xmm0
	movq	%r15, %rsi
	movw	%cx, 16(%rax)
	movq	-4480(%rbp), %rdi
	leaq	18(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -3984(%rbp)
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3984(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L666
	call	_ZdlPv@PLT
.L666:
	movq	(%r12), %rax
	movq	56(%rax), %r11
	movq	8(%rax), %rsi
	movq	24(%rax), %rdx
	movq	40(%rax), %rdi
	movq	(%rax), %rcx
	movq	64(%rax), %r10
	movq	%r11, -4592(%rbp)
	movq	72(%rax), %r11
	movq	%rsi, -4520(%rbp)
	movq	16(%rax), %rsi
	movq	88(%rax), %r9
	movq	%rdx, -4576(%rbp)
	movq	32(%rax), %rdx
	movq	96(%rax), %r12
	movq	%rdi, -4608(%rbp)
	movq	48(%rax), %rdi
	movq	%r11, -4648(%rbp)
	movq	80(%rax), %r11
	movq	%rsi, -4528(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -4560(%rbp)
	movl	$92, %edx
	movq	%rdi, -4624(%rbp)
	movq	%r14, %rdi
	movq	%rcx, -4352(%rbp)
	movq	%r10, -4640(%rbp)
	movq	%r11, -4672(%rbp)
	movq	%r9, -4688(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$97, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$104, %edi
	movq	%r12, -112(%rbp)
	movq	-4352(%rbp), %xmm0
	movq	$0, -3968(%rbp)
	leaq	-976(%rbp), %r12
	movhps	-4520(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-4528(%rbp), %xmm0
	movhps	-4576(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-4560(%rbp), %xmm0
	movhps	-4608(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-4624(%rbp), %xmm0
	movhps	-4592(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-4640(%rbp), %xmm0
	movhps	-4648(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-4672(%rbp), %xmm0
	movhps	-4688(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3984(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r15, %rsi
	movdqa	-208(%rbp), %xmm1
	movdqa	-192(%rbp), %xmm7
	leaq	104(%rax), %rdx
	movq	%rax, -3984(%rbp)
	movdqa	-176(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm4
	movq	%rcx, 96(%rax)
	movdqa	-160(%rbp), %xmm5
	movdqa	-144(%rbp), %xmm2
	movups	%xmm1, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm2, 64(%rax)
	movups	%xmm4, 80(%rax)
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L667
	call	_ZdlPv@PLT
.L667:
	movq	-4544(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1104(%rbp)
	je	.L668
.L812:
	movq	-4536(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4256(%rbp)
	movq	$0, -4248(%rbp)
	movq	$0, -4240(%rbp)
	movq	$0, -4232(%rbp)
	movq	$0, -4224(%rbp)
	movq	$0, -4216(%rbp)
	movq	$0, -4208(%rbp)
	movq	$0, -4200(%rbp)
	movq	$0, -4192(%rbp)
	movq	$0, -4184(%rbp)
	movq	$0, -4176(%rbp)
	movq	$0, -4168(%rbp)
	movq	$0, -4064(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4064(%rbp), %rax
	movq	-4464(%rbp), %rdi
	leaq	-4240(%rbp), %rcx
	pushq	%rax
	leaq	-4168(%rbp), %rax
	leaq	-4224(%rbp), %r9
	pushq	%rax
	leaq	-4176(%rbp), %rax
	leaq	-4232(%rbp), %r8
	pushq	%rax
	leaq	-4184(%rbp), %rax
	leaq	-4248(%rbp), %rdx
	pushq	%rax
	leaq	-4192(%rbp), %rax
	leaq	-4256(%rbp), %rsi
	pushq	%rax
	leaq	-4200(%rbp), %rax
	pushq	%rax
	leaq	-4208(%rbp), %rax
	pushq	%rax
	leaq	-4216(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayES4_S6_S4_S6_S4_NS0_3SmiES7_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESE_SI_SE_SI_SE_PNSA_IS8_EESK_
	addq	$64, %rsp
	movq	%r14, %rdi
	movl	$98, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal16kBuiltinName_364EPNS0_8compiler18CodeAssemblerStateE
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movl	$50, %edx
	movq	-4232(%rbp), %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L813:
	movq	-4544(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4256(%rbp)
	movq	$0, -4248(%rbp)
	movq	$0, -4240(%rbp)
	movq	$0, -4232(%rbp)
	movq	$0, -4224(%rbp)
	movq	$0, -4216(%rbp)
	movq	$0, -4208(%rbp)
	movq	$0, -4200(%rbp)
	movq	$0, -4192(%rbp)
	movq	$0, -4184(%rbp)
	movq	$0, -4176(%rbp)
	movq	$0, -4168(%rbp)
	movq	$0, -4064(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4064(%rbp), %rax
	movq	%r12, %rdi
	leaq	-4240(%rbp), %rcx
	pushq	%rax
	leaq	-4168(%rbp), %rax
	leaq	-4224(%rbp), %r9
	pushq	%rax
	leaq	-4176(%rbp), %rax
	leaq	-4232(%rbp), %r8
	pushq	%rax
	leaq	-4184(%rbp), %rax
	leaq	-4248(%rbp), %rdx
	pushq	%rax
	leaq	-4192(%rbp), %rax
	leaq	-4256(%rbp), %rsi
	pushq	%rax
	leaq	-4200(%rbp), %rax
	pushq	%rax
	leaq	-4208(%rbp), %rax
	pushq	%rax
	leaq	-4216(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayES4_S6_S4_S6_S4_NS0_3SmiES7_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESE_SI_SE_SI_SE_PNSA_IS8_EESK_
	addq	$64, %rsp
	movl	$100, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4256(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$104, %edi
	movaps	%xmm0, -3984(%rbp)
	movq	%rax, -208(%rbp)
	movq	-4248(%rbp), %rax
	movq	$0, -3968(%rbp)
	movq	%rax, -200(%rbp)
	movq	-4240(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-4232(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-4224(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-4216(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-4208(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-4200(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-4192(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-4184(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-4176(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-4168(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-4064(%rbp), %rax
	movq	%rax, -112(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-208(%rbp), %xmm6
	movdqa	-192(%rbp), %xmm1
	movdqa	-176(%rbp), %xmm7
	leaq	104(%rax), %rdx
	movq	%rax, -3984(%rbp)
	movdqa	-160(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm5
	movq	%rcx, 96(%rax)
	movdqa	-128(%rbp), %xmm2
	movq	-4352(%rbp), %rdi
	movups	%xmm6, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm5, 64(%rax)
	movups	%xmm2, 80(%rax)
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L670
	call	_ZdlPv@PLT
.L670:
	movq	-4504(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -720(%rbp)
	je	.L671
.L814:
	movq	-4552(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4256(%rbp)
	movq	$0, -4248(%rbp)
	movq	$0, -4240(%rbp)
	movq	$0, -4232(%rbp)
	movq	$0, -4224(%rbp)
	movq	$0, -4216(%rbp)
	movq	$0, -4208(%rbp)
	movq	$0, -4200(%rbp)
	movq	$0, -4192(%rbp)
	movq	$0, -4184(%rbp)
	movq	$0, -4176(%rbp)
	movq	$0, -4168(%rbp)
	movq	$0, -4064(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4064(%rbp), %rax
	movq	-4488(%rbp), %rdi
	leaq	-4240(%rbp), %rcx
	pushq	%rax
	leaq	-4168(%rbp), %rax
	leaq	-4224(%rbp), %r9
	pushq	%rax
	leaq	-4176(%rbp), %rax
	leaq	-4232(%rbp), %r8
	pushq	%rax
	leaq	-4184(%rbp), %rax
	leaq	-4248(%rbp), %rdx
	pushq	%rax
	leaq	-4192(%rbp), %rax
	leaq	-4256(%rbp), %rsi
	pushq	%rax
	leaq	-4200(%rbp), %rax
	pushq	%rax
	leaq	-4208(%rbp), %rax
	pushq	%rax
	leaq	-4216(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayES4_S6_S4_S6_S4_NS0_3SmiES7_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESE_SI_SE_SI_SE_PNSA_IS8_EESK_
	addq	$64, %rsp
	movl	$101, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4176(%rbp), %r9
	movq	%r13, %rdi
	movq	-4192(%rbp), %r8
	movq	-4064(%rbp), %rcx
	movq	-4216(%rbp), %rdx
	movq	-4232(%rbp), %rsi
	call	_ZN2v88internal12SlowCopy_366EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEES8_NS4_INS0_7IntPtrTEEESA_
	movl	$92, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4256(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$104, %edi
	movaps	%xmm0, -3984(%rbp)
	movq	%rax, -208(%rbp)
	movq	-4248(%rbp), %rax
	movq	$0, -3968(%rbp)
	movq	%rax, -200(%rbp)
	movq	-4240(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-4232(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-4224(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-4216(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-4208(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-4200(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-4192(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-4184(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-4176(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-4168(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-4064(%rbp), %rax
	movq	%rax, -112(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-208(%rbp), %xmm4
	movdqa	-192(%rbp), %xmm6
	movdqa	-176(%rbp), %xmm1
	leaq	104(%rax), %rdx
	movq	%rax, -3984(%rbp)
	movdqa	-160(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm3
	movq	%rcx, 96(%rax)
	movdqa	-128(%rbp), %xmm5
	movq	-4352(%rbp), %rdi
	movups	%xmm4, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm3, 64(%rax)
	movups	%xmm5, 80(%rax)
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L672
	call	_ZdlPv@PLT
.L672:
	movq	-4504(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -528(%rbp)
	je	.L673
.L815:
	movq	-4504(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4256(%rbp)
	movq	$0, -4248(%rbp)
	movq	$0, -4240(%rbp)
	movq	$0, -4232(%rbp)
	movq	$0, -4224(%rbp)
	movq	$0, -4216(%rbp)
	movq	$0, -4208(%rbp)
	movq	$0, -4200(%rbp)
	movq	$0, -4192(%rbp)
	movq	$0, -4184(%rbp)
	movq	$0, -4176(%rbp)
	movq	$0, -4168(%rbp)
	movq	$0, -4064(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4064(%rbp), %rax
	movq	-4352(%rbp), %rdi
	leaq	-4240(%rbp), %rcx
	pushq	%rax
	leaq	-4168(%rbp), %rax
	leaq	-4224(%rbp), %r9
	pushq	%rax
	leaq	-4176(%rbp), %rax
	leaq	-4232(%rbp), %r8
	pushq	%rax
	leaq	-4184(%rbp), %rax
	leaq	-4248(%rbp), %rdx
	pushq	%rax
	leaq	-4192(%rbp), %rax
	leaq	-4256(%rbp), %rsi
	pushq	%rax
	leaq	-4200(%rbp), %rax
	pushq	%rax
	leaq	-4208(%rbp), %rax
	pushq	%rax
	leaq	-4216(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayES4_S6_S4_S6_S4_NS0_3SmiES7_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESE_SI_SE_SI_SE_PNSA_IS8_EESK_
	addq	$64, %rsp
	movl	$91, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4256(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$104, %edi
	movaps	%xmm0, -3984(%rbp)
	movq	%rax, -208(%rbp)
	movq	-4248(%rbp), %rax
	movq	$0, -3968(%rbp)
	movq	%rax, -200(%rbp)
	movq	-4240(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-4232(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-4224(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-4216(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-4208(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-4200(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-4192(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-4184(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-4176(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-4168(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-4064(%rbp), %rax
	movq	%rax, -112(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movdqa	-208(%rbp), %xmm2
	movdqa	-192(%rbp), %xmm4
	leaq	104(%rax), %rdx
	movq	%rax, -3984(%rbp)
	movdqa	-176(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm3
	movq	%rcx, 96(%rax)
	movdqa	-160(%rbp), %xmm1
	movdqa	-144(%rbp), %xmm7
	movups	%xmm2, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm1, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm3, 80(%rax)
	movq	%rdx, -3968(%rbp)
	movq	%rdx, -3976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L674
	call	_ZdlPv@PLT
.L674:
	movq	-4512(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -336(%rbp)
	je	.L675
.L816:
	movq	-4512(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4248(%rbp)
	movq	$0, -4240(%rbp)
	movq	$0, -4232(%rbp)
	movq	$0, -4224(%rbp)
	movq	$0, -4216(%rbp)
	movq	$0, -4208(%rbp)
	movq	$0, -4200(%rbp)
	movq	$0, -4192(%rbp)
	movq	$0, -4184(%rbp)
	movq	$0, -4176(%rbp)
	movq	$0, -4168(%rbp)
	movq	$0, -4064(%rbp)
	movq	$0, -3984(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4064(%rbp), %rax
	pushq	%r15
	leaq	-4232(%rbp), %rcx
	pushq	%rax
	leaq	-4168(%rbp), %rax
	leaq	-4240(%rbp), %rdx
	movq	%rbx, %rdi
	pushq	%rax
	leaq	-4176(%rbp), %rax
	leaq	-4248(%rbp), %rsi
	pushq	%rax
	leaq	-4184(%rbp), %rax
	leaq	-4216(%rbp), %r9
	pushq	%rax
	leaq	-4192(%rbp), %rax
	leaq	-4224(%rbp), %r8
	pushq	%rax
	leaq	-4200(%rbp), %rax
	pushq	%rax
	leaq	-4208(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayES4_S6_S4_S6_S4_NS0_3SmiES7_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESE_SI_SE_SI_SE_PNSA_IS8_EESK_
	movq	%r14, %rdi
	addq	$64, %rsp
	movl	$105, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3984(%rbp), %rsi
	movq	-4584(%rbp), %rdi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L818:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$112, %edi
	movq	-4168(%rbp), %xmm0
	movq	-4184(%rbp), %xmm1
	movq	-4200(%rbp), %xmm2
	movq	-4216(%rbp), %xmm3
	movq	$0, -4048(%rbp)
	movq	-4232(%rbp), %xmm4
	movhps	-4224(%rbp), %xmm0
	movq	-4248(%rbp), %xmm5
	movhps	-4176(%rbp), %xmm1
	movq	-4264(%rbp), %xmm6
	movhps	-4192(%rbp), %xmm2
	movhps	-4208(%rbp), %xmm3
	movaps	%xmm0, -112(%rbp)
	movhps	-4224(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movhps	-4240(%rbp), %xmm5
	movaps	%xmm3, -160(%rbp)
	movhps	-4256(%rbp), %xmm6
	movaps	%xmm5, -192(%rbp)
	movaps	%xmm6, -208(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -4064(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm7
	movq	%r12, %rsi
	movdqa	-192(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm1
	movq	-4368(%rbp), %rdi
	leaq	112(%rax), %rdx
	movq	%rax, -4064(%rbp)
	movdqa	-176(%rbp), %xmm2
	movdqa	-160(%rbp), %xmm4
	movups	%xmm7, (%rax)
	movdqa	-144(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm7
	movups	%xmm5, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm6, 64(%rax)
	movups	%xmm7, 80(%rax)
	movups	%xmm1, 96(%rax)
	movq	%rdx, -4048(%rbp)
	movq	%rdx, -4056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L654
	call	_ZdlPv@PLT
.L654:
	movq	-4576(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L819:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-4352(%rbp), %xmm5
	movdqa	-4576(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movdqa	-4624(%rbp), %xmm4
	movdqa	-4640(%rbp), %xmm6
	movl	$144, %edi
	movaps	%xmm0, -4064(%rbp)
	movdqa	-4672(%rbp), %xmm7
	movdqa	-4688(%rbp), %xmm1
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm2, -192(%rbp)
	movdqa	-4704(%rbp), %xmm5
	movdqa	-4720(%rbp), %xmm2
	movaps	%xmm4, -176(%rbp)
	movdqa	-4736(%rbp), %xmm4
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movq	$0, -4048(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm6
	movq	%r12, %rsi
	movdqa	-192(%rbp), %xmm7
	movdqa	-176(%rbp), %xmm1
	movdqa	-160(%rbp), %xmm5
	leaq	144(%rax), %rdx
	movq	%rax, -4064(%rbp)
	movdqa	-144(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm2
	movups	%xmm6, (%rax)
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm6
	movups	%xmm7, 16(%rax)
	movdqa	-80(%rbp), %xmm7
	movq	-4384(%rbp), %rdi
	movups	%xmm1, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm3, 64(%rax)
	movups	%xmm2, 80(%rax)
	movups	%xmm4, 96(%rax)
	movups	%xmm6, 112(%rax)
	movups	%xmm7, 128(%rax)
	movq	%rdx, -4048(%rbp)
	movq	%rdx, -4056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L662
	call	_ZdlPv@PLT
.L662:
	movq	-4560(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L661
.L817:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22465:
	.size	_ZN2v88internal33TypedArrayPrototypeSliceAssembler36GenerateTypedArrayPrototypeSliceImplEv, .-_ZN2v88internal33TypedArrayPrototypeSliceAssembler36GenerateTypedArrayPrototypeSliceImplEv
	.section	.rodata._ZN2v88internal8Builtins33Generate_TypedArrayPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/typed-array-slice-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins33Generate_TypedArrayPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"TypedArrayPrototypeSlice"
	.section	.text._ZN2v88internal8Builtins33Generate_TypedArrayPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins33Generate_TypedArrayPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins33Generate_TypedArrayPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins33Generate_TypedArrayPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE:
.LFB22461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$609, %ecx
	leaq	.LC5(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$923, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L824
.L821:
	movq	%r13, %rdi
	call	_ZN2v88internal33TypedArrayPrototypeSliceAssembler36GenerateTypedArrayPrototypeSliceImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L825
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L824:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L821
.L825:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22461:
	.size	_ZN2v88internal8Builtins33Generate_TypedArrayPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins33Generate_TypedArrayPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal16kBuiltinName_364EPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal16kBuiltinName_364EPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal16kBuiltinName_364EPNS0_8compiler18CodeAssemblerStateE:
.LFB29257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29257:
	.size	_GLOBAL__sub_I__ZN2v88internal16kBuiltinName_364EPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal16kBuiltinName_364EPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal16kBuiltinName_364EPNS0_8compiler18CodeAssemblerStateE
	.weak	_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE:
	.byte	5
	.byte	5
	.weak	_ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE:
	.byte	7
	.byte	7
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.byte	5
	.byte	5
	.byte	5
	.byte	7
	.byte	8
	.byte	7
	.byte	5
	.byte	8
	.byte	5
	.byte	8
	.byte	5
	.byte	6
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
