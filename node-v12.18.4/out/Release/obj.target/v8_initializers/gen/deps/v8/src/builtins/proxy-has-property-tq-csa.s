	.file	"proxy-has-property-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_10JSReceiverES6_S6_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESH_SH_PNS9_IS7_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_10JSReceiverES6_S6_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESH_SH_PNS9_IS7_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_10JSReceiverES6_S6_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESH_SH_PNS9_IS7_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_10JSReceiverES6_S6_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESH_SH_PNS9_IS7_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_10JSReceiverES6_S6_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESH_SH_PNS9_IS7_EE:
.LFB26593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$7, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$117901063, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$8, 6(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L8
	movq	%rax, -112(%rbp)
	call	_ZdlPv@PLT
	movq	-112(%rbp), %rax
.L8:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L9
	movq	%rdx, (%r15)
.L9:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L10
	movq	%rdx, (%r14)
.L10:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L11
	movq	%rdx, 0(%r13)
.L11:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L12
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L12:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L13
	movq	%rdx, (%rbx)
.L13:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L14
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L14:
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L7
	movq	-104(%rbp), %rbx
	movq	%rax, (%rbx)
.L7:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L42
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L42:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26593:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_10JSReceiverES6_S6_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESH_SH_PNS9_IS7_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_10JSReceiverES6_S6_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESH_SH_PNS9_IS7_EE
	.section	.rodata._ZN2v88internal25ProxyHasPropertyAssembler28GenerateProxyHasPropertyImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../../deps/v8/../../deps/v8/src/builtins/proxy-has-property.tq"
	.section	.rodata._ZN2v88internal25ProxyHasPropertyAssembler28GenerateProxyHasPropertyImplEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	"has"
	.section	.text._ZN2v88internal25ProxyHasPropertyAssembler28GenerateProxyHasPropertyImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25ProxyHasPropertyAssembler28GenerateProxyHasPropertyImplEv
	.type	_ZN2v88internal25ProxyHasPropertyAssembler28GenerateProxyHasPropertyImplEv, @function
_ZN2v88internal25ProxyHasPropertyAssembler28GenerateProxyHasPropertyImplEv:
.LFB22418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-2992(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-3120(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$3384, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -3216(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -3360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	leaq	-3216(%rbp), %r12
	movq	%rax, -3376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$72, %edi
	movq	$0, -2984(%rbp)
	movq	$0, -2976(%rbp)
	movq	%rax, %rbx
	movq	-3216(%rbp), %rax
	movq	$0, -2968(%rbp)
	movq	%rax, -2992(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -2984(%rbp)
	leaq	-2936(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2968(%rbp)
	movq	%rdx, -2976(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2952(%rbp)
	movq	%rax, -3224(%rbp)
	movq	$0, -2960(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3216(%rbp), %rax
	movl	$96, %edi
	movq	$0, -2792(%rbp)
	movq	$0, -2784(%rbp)
	movq	%rax, -2800(%rbp)
	movq	$0, -2776(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -2792(%rbp)
	leaq	-2744(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2776(%rbp)
	movq	%rdx, -2784(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2760(%rbp)
	movq	%rax, -3328(%rbp)
	movq	$0, -2768(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3216(%rbp), %rax
	movl	$120, %edi
	movq	$0, -2600(%rbp)
	movq	$0, -2592(%rbp)
	movq	%rax, -2608(%rbp)
	movq	$0, -2584(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -2600(%rbp)
	leaq	-2552(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2584(%rbp)
	movq	%rdx, -2592(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2568(%rbp)
	movq	%rax, -3232(%rbp)
	movq	$0, -2576(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3216(%rbp), %rax
	movl	$120, %edi
	movq	$0, -2408(%rbp)
	movq	$0, -2400(%rbp)
	movq	%rax, -2416(%rbp)
	movq	$0, -2392(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -2408(%rbp)
	leaq	-2360(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2392(%rbp)
	movq	%rdx, -2400(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2376(%rbp)
	movq	%rax, -3320(%rbp)
	movq	$0, -2384(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3216(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2216(%rbp)
	movq	$0, -2208(%rbp)
	movq	%rax, -2224(%rbp)
	movq	$0, -2200(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2216(%rbp)
	leaq	-2168(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2200(%rbp)
	movq	%rdx, -2208(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2184(%rbp)
	movq	%rax, -3240(%rbp)
	movq	$0, -2192(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3216(%rbp), %rax
	movl	$96, %edi
	movq	$0, -2024(%rbp)
	movq	$0, -2016(%rbp)
	movq	%rax, -2032(%rbp)
	movq	$0, -2008(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -2024(%rbp)
	leaq	-1976(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2008(%rbp)
	movq	%rdx, -2016(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1992(%rbp)
	movq	%rax, -3248(%rbp)
	movq	$0, -2000(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3216(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1832(%rbp)
	movq	$0, -1824(%rbp)
	movq	%rax, -1840(%rbp)
	movq	$0, -1816(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1832(%rbp)
	leaq	-1784(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1816(%rbp)
	movq	%rdx, -1824(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1800(%rbp)
	movq	%rax, -3256(%rbp)
	movq	$0, -1808(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3216(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1640(%rbp)
	movq	$0, -1632(%rbp)
	movq	%rax, -1648(%rbp)
	movq	$0, -1624(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1640(%rbp)
	leaq	-1592(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1624(%rbp)
	movq	%rdx, -1632(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1608(%rbp)
	movq	%rax, -3336(%rbp)
	movq	$0, -1616(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3216(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1448(%rbp)
	movq	$0, -1440(%rbp)
	movq	%rax, -1456(%rbp)
	movq	$0, -1432(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1448(%rbp)
	leaq	-1400(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1432(%rbp)
	movq	%rdx, -1440(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1416(%rbp)
	movq	%rax, -3264(%rbp)
	movq	$0, -1424(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3216(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1256(%rbp)
	movq	$0, -1248(%rbp)
	movq	%rax, -1264(%rbp)
	movq	$0, -1240(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1256(%rbp)
	leaq	-1208(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1240(%rbp)
	movq	%rdx, -1248(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1224(%rbp)
	movq	%rax, -3280(%rbp)
	movq	$0, -1232(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3216(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1064(%rbp)
	movq	$0, -1056(%rbp)
	movq	%rax, -1072(%rbp)
	movq	$0, -1048(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1064(%rbp)
	leaq	-1016(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1048(%rbp)
	movq	%rdx, -1056(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1032(%rbp)
	movq	%rax, -3296(%rbp)
	movq	$0, -1040(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3216(%rbp), %rax
	movl	$168, %edi
	movq	$0, -872(%rbp)
	movq	$0, -864(%rbp)
	movq	%rax, -880(%rbp)
	movq	$0, -856(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -872(%rbp)
	leaq	-824(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -856(%rbp)
	movq	%rdx, -864(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -840(%rbp)
	movq	%rax, -3304(%rbp)
	movq	$0, -848(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3216(%rbp), %rax
	movl	$168, %edi
	movq	$0, -680(%rbp)
	movq	$0, -672(%rbp)
	movq	%rax, -688(%rbp)
	movq	$0, -664(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -680(%rbp)
	leaq	-632(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -664(%rbp)
	movq	%rdx, -672(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -648(%rbp)
	movq	%rax, -3312(%rbp)
	movq	$0, -656(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3216(%rbp), %rax
	movl	$96, %edi
	movq	$0, -488(%rbp)
	movq	$0, -480(%rbp)
	movq	%rax, -496(%rbp)
	movq	$0, -472(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -488(%rbp)
	leaq	-440(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -472(%rbp)
	movq	%rdx, -480(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -456(%rbp)
	movq	%rax, -3272(%rbp)
	movq	$0, -464(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3216(%rbp), %rax
	movl	$72, %edi
	movq	$0, -296(%rbp)
	movq	$0, -288(%rbp)
	movq	%rax, -304(%rbp)
	movq	$0, -280(%rbp)
	call	_Znwm@PLT
	xorl	%r8d, %r8d
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r12, %rsi
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -296(%rbp)
	leaq	-248(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -280(%rbp)
	movq	%rdx, -288(%rbp)
	xorl	%edx, %edx
	movq	%rax, -3288(%rbp)
	movups	%xmm0, -264(%rbp)
	movq	$0, -272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	-3360(%rbp), %xmm1
	movaps	%xmm0, -3120(%rbp)
	movhps	-3376(%rbp), %xmm1
	movq	%rbx, -96(%rbp)
	movaps	%xmm1, -112(%rbp)
	movq	$0, -3104(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm2
	leaq	24(%rax), %rdx
	movq	%rax, -3120(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -3104(%rbp)
	movq	%rdx, -3112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L44
	call	_ZdlPv@PLT
.L44:
	movq	-3224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2928(%rbp)
	jne	.L379
	cmpq	$0, -2736(%rbp)
	jne	.L380
.L50:
	cmpq	$0, -2544(%rbp)
	jne	.L381
.L53:
	cmpq	$0, -2352(%rbp)
	jne	.L382
.L58:
	cmpq	$0, -2160(%rbp)
	jne	.L383
.L61:
	cmpq	$0, -1968(%rbp)
	jne	.L384
.L64:
	cmpq	$0, -1776(%rbp)
	jne	.L385
.L66:
	cmpq	$0, -1584(%rbp)
	jne	.L386
.L71:
	cmpq	$0, -1392(%rbp)
	jne	.L387
.L74:
	cmpq	$0, -1200(%rbp)
	jne	.L388
.L77:
	cmpq	$0, -1008(%rbp)
	jne	.L389
.L80:
	cmpq	$0, -816(%rbp)
	jne	.L390
.L87:
	cmpq	$0, -624(%rbp)
	jne	.L391
.L89:
	cmpq	$0, -432(%rbp)
	jne	.L392
.L90:
	cmpq	$0, -240(%rbp)
	jne	.L393
.L92:
	movq	-3288(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L94
	call	_ZdlPv@PLT
.L94:
	movq	-288(%rbp), %rbx
	movq	-296(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L95
	.p2align 4,,10
	.p2align 3
.L99:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L96
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L99
.L97:
	movq	-296(%rbp), %r13
.L95:
	testq	%r13, %r13
	je	.L100
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L100:
	movq	-3272(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-464(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L101
	call	_ZdlPv@PLT
.L101:
	movq	-480(%rbp), %rbx
	movq	-488(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L102
	.p2align 4,,10
	.p2align 3
.L106:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L103
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L106
.L104:
	movq	-488(%rbp), %r13
.L102:
	testq	%r13, %r13
	je	.L107
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L107:
	movq	-3312(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L108
	call	_ZdlPv@PLT
.L108:
	movq	-672(%rbp), %rbx
	movq	-680(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L109
	.p2align 4,,10
	.p2align 3
.L113:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L110
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L113
.L111:
	movq	-680(%rbp), %r13
.L109:
	testq	%r13, %r13
	je	.L114
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L114:
	movq	-3304(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-848(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L115
	call	_ZdlPv@PLT
.L115:
	movq	-864(%rbp), %rbx
	movq	-872(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L116
	.p2align 4,,10
	.p2align 3
.L120:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L117
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L120
.L118:
	movq	-872(%rbp), %r13
.L116:
	testq	%r13, %r13
	je	.L121
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L121:
	movq	-3296(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1040(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L122
	call	_ZdlPv@PLT
.L122:
	movq	-1056(%rbp), %rbx
	movq	-1064(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L123
	.p2align 4,,10
	.p2align 3
.L127:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L124
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L127
.L125:
	movq	-1064(%rbp), %r13
.L123:
	testq	%r13, %r13
	je	.L128
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L128:
	movq	-3280(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L129
	call	_ZdlPv@PLT
.L129:
	movq	-1248(%rbp), %rbx
	movq	-1256(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L130
	.p2align 4,,10
	.p2align 3
.L134:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L131
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L134
.L132:
	movq	-1256(%rbp), %r13
.L130:
	testq	%r13, %r13
	je	.L135
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L135:
	movq	-3264(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L136
	call	_ZdlPv@PLT
.L136:
	movq	-1440(%rbp), %rbx
	movq	-1448(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L137
	.p2align 4,,10
	.p2align 3
.L141:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L138
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L141
.L139:
	movq	-1448(%rbp), %r13
.L137:
	testq	%r13, %r13
	je	.L142
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L142:
	movq	-3336(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1616(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L143
	call	_ZdlPv@PLT
.L143:
	movq	-1632(%rbp), %rbx
	movq	-1640(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L144
	.p2align 4,,10
	.p2align 3
.L148:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L145
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L148
.L146:
	movq	-1640(%rbp), %r13
.L144:
	testq	%r13, %r13
	je	.L149
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L149:
	movq	-3256(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1808(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L150
	call	_ZdlPv@PLT
.L150:
	movq	-1824(%rbp), %rbx
	movq	-1832(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L151
	.p2align 4,,10
	.p2align 3
.L155:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L152
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L155
.L153:
	movq	-1832(%rbp), %r13
.L151:
	testq	%r13, %r13
	je	.L156
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L156:
	movq	-3248(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2000(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L157
	call	_ZdlPv@PLT
.L157:
	movq	-2016(%rbp), %rbx
	movq	-2024(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L158
	.p2align 4,,10
	.p2align 3
.L162:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L159
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L162
.L160:
	movq	-2024(%rbp), %r13
.L158:
	testq	%r13, %r13
	je	.L163
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L163:
	movq	-3240(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L164
	call	_ZdlPv@PLT
.L164:
	movq	-2208(%rbp), %rbx
	movq	-2216(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L165
	.p2align 4,,10
	.p2align 3
.L169:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L166
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L169
.L167:
	movq	-2216(%rbp), %r13
.L165:
	testq	%r13, %r13
	je	.L170
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L170:
	movq	-3320(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L171
	call	_ZdlPv@PLT
.L171:
	movq	-2400(%rbp), %rbx
	movq	-2408(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L172
	.p2align 4,,10
	.p2align 3
.L176:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L173
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L176
.L174:
	movq	-2408(%rbp), %r13
.L172:
	testq	%r13, %r13
	je	.L177
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L177:
	movq	-3232(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2576(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L178
	call	_ZdlPv@PLT
.L178:
	movq	-2592(%rbp), %rbx
	movq	-2600(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L179
	.p2align 4,,10
	.p2align 3
.L183:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L180
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L183
.L181:
	movq	-2600(%rbp), %r13
.L179:
	testq	%r13, %r13
	je	.L184
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L184:
	movq	-3328(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2768(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L185
	call	_ZdlPv@PLT
.L185:
	movq	-2784(%rbp), %rbx
	movq	-2792(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L186
	.p2align 4,,10
	.p2align 3
.L190:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L187
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L190
.L188:
	movq	-2792(%rbp), %r13
.L186:
	testq	%r13, %r13
	je	.L191
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L191:
	movq	-3224(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2960(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L192
	call	_ZdlPv@PLT
.L192:
	movq	-2976(%rbp), %rbx
	movq	-2984(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L193
	.p2align 4,,10
	.p2align 3
.L197:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L194
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L197
.L195:
	movq	-2984(%rbp), %r13
.L193:
	testq	%r13, %r13
	je	.L198
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L198:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L394
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L197
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L187:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L190
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L180:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L183
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L173:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L176
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L166:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L169
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L159:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L162
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L152:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L155
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L145:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L148
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L138:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L141
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L131:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L134
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L124:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L127
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L117:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L120
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L110:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L113
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L96:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L99
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L103:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L106
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L379:
	movq	-3224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -3104(%rbp)
	movaps	%xmm0, -3120(%rbp)
	call	_Znwm@PLT
	movl	$1799, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r14, %rdi
	movb	$7, 2(%rax)
	movq	%rax, -3120(%rbp)
	movq	%rdx, -3104(%rbp)
	movq	%rdx, -3112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3120(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L46
	call	_ZdlPv@PLT
.L46:
	movq	(%rbx), %rax
	movl	$15, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %rax
	movq	%rcx, -3360(%rbp)
	movq	%rax, -3376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$27, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-3360(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal20Cast10JSReceiver_140EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm6
	movq	%rbx, %xmm4
	movq	-3376(%rbp), %xmm3
	movhps	-3360(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	%rax, -80(%rbp)
	punpcklqdq	%xmm6, %xmm3
	movaps	%xmm4, -3360(%rbp)
	leaq	-3152(%rbp), %r14
	movaps	%xmm3, -3376(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -3152(%rbp)
	movq	$0, -3136(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm6
	movq	-80(%rbp), %rcx
	movq	%r14, %rsi
	leaq	40(%rax), %rdx
	leaq	-2608(%rbp), %rdi
	movq	%rax, -3152(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-96(%rbp), %xmm6
	movq	%rcx, 32(%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -3136(%rbp)
	movq	%rdx, -3144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L47
	call	_ZdlPv@PLT
.L47:
	movq	-3232(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3112(%rbp)
	jne	.L395
.L48:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2736(%rbp)
	je	.L50
.L380:
	movq	-3328(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2800(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -3104(%rbp)
	movaps	%xmm0, -3120(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -3120(%rbp)
	movq	%rdx, -3104(%rbp)
	movq	%rdx, -3112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3120(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L51
	call	_ZdlPv@PLT
.L51:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm6
	movaps	%xmm0, -3120(%rbp)
	movq	$0, -3104(%rbp)
	movq	%rdx, -96(%rbp)
	movaps	%xmm6, -112(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-112(%rbp), %xmm2
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-304(%rbp), %rdi
	movq	%rax, -3120(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -3104(%rbp)
	movq	%rdx, -3112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L52
	call	_ZdlPv@PLT
.L52:
	movq	-3288(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2544(%rbp)
	je	.L53
.L381:
	movq	-3232(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2608(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -3104(%rbp)
	movaps	%xmm0, -3120(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -3120(%rbp)
	movq	%rdx, -3104(%rbp)
	movq	%rdx, -3112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3120(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L54
	call	_ZdlPv@PLT
.L54:
	movq	(%rbx), %rax
	movl	$30, %edx
	movq	%r12, %rdi
	movq	16(%rax), %rsi
	movq	(%rax), %rcx
	movq	8(%rax), %r14
	movq	32(%rax), %rax
	movq	%rsi, -3376(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rcx, -3360(%rbp)
	movq	%rax, -3392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE(%rip), %esi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal20Cast10JSReceiver_140EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm7
	pxor	%xmm0, %xmm0
	movq	-3376(%rbp), %xmm5
	movl	$48, %edi
	movq	%rbx, -80(%rbp)
	movq	-3360(%rbp), %xmm6
	leaq	-3152(%rbp), %r14
	movhps	-3392(%rbp), %xmm5
	movq	%rax, -72(%rbp)
	punpcklqdq	%xmm7, %xmm6
	movaps	%xmm5, -3376(%rbp)
	movaps	%xmm6, -3360(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm0, -3152(%rbp)
	movq	$0, -3136(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm4
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm6
	leaq	48(%rax), %rdx
	leaq	-2224(%rbp), %rdi
	movq	%rax, -3152(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -3136(%rbp)
	movq	%rdx, -3144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L55
	call	_ZdlPv@PLT
.L55:
	movq	-3240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3112(%rbp)
	jne	.L396
.L56:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2352(%rbp)
	je	.L58
.L382:
	movq	-3320(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2416(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -3104(%rbp)
	movaps	%xmm0, -3120(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -3120(%rbp)
	movq	%rdx, -3104(%rbp)
	movq	%rdx, -3112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3120(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L59
	call	_ZdlPv@PLT
.L59:
	movq	(%rbx), %rax
	movl	$32, %edi
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm2
	movq	$0, -3104(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm0, -3120(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm4
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-2032(%rbp), %rdi
	movq	%rax, -3120(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -3104(%rbp)
	movq	%rdx, -3112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L60
	call	_ZdlPv@PLT
.L60:
	movq	-3248(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2160(%rbp)
	je	.L61
.L383:
	movq	-3240(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2224(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -3104(%rbp)
	movaps	%xmm0, -3120(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movq	%r14, %rdi
	movw	%si, 4(%rax)
	leaq	6(%rax), %rdx
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	movq	%rax, -3120(%rbp)
	movq	%rdx, -3104(%rbp)
	movq	%rdx, -3112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3120(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L62
	call	_ZdlPv@PLT
.L62:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	40(%rax), %rdx
	movdqu	(%rax), %xmm5
	movq	$0, -3104(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -80(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm0, -3120(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm6
	leaq	40(%rax), %rdx
	leaq	-1840(%rbp), %rdi
	movq	%rax, -3120(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -3104(%rbp)
	movq	%rdx, -3112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L63
	call	_ZdlPv@PLT
.L63:
	movq	-3256(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1968(%rbp)
	je	.L64
.L384:
	movq	-3248(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2032(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -3104(%rbp)
	movaps	%xmm0, -3120(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -3120(%rbp)
	movq	%rdx, -3104(%rbp)
	movq	%rdx, -3112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L65
	call	_ZdlPv@PLT
.L65:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -1776(%rbp)
	je	.L66
.L385:
	movq	-3256(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1840(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -3104(%rbp)
	movaps	%xmm0, -3120(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -3120(%rbp)
	movq	%rdx, -3104(%rbp)
	movq	%rdx, -3112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3120(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L67
	call	_ZdlPv@PLT
.L67:
	movq	(%rbx), %rax
	movl	$34, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	24(%rax), %r14
	movq	(%rax), %rbx
	movq	%rcx, -3360(%rbp)
	movq	16(%rax), %rcx
	movq	32(%rax), %rax
	movq	%rcx, -3376(%rbp)
	movq	%rax, -3392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rdx
	movq	%r13, %r8
	movq	%rbx, %rsi
	leaq	.LC2(%rip), %rcx
	movq	%r15, %rdi
	call	_ZN2v88internal13GetMethod_245EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPKcPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm2
	movq	%r14, %xmm5
	movq	-3392(%rbp), %xmm7
	movq	%rbx, %xmm3
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movq	%rax, -64(%rbp)
	punpcklqdq	%xmm2, %xmm7
	movq	-3376(%rbp), %xmm2
	movhps	-3360(%rbp), %xmm3
	movaps	%xmm0, -3152(%rbp)
	movaps	%xmm7, -3392(%rbp)
	leaq	-3152(%rbp), %r14
	punpcklqdq	%xmm5, %xmm2
	movaps	%xmm3, -3360(%rbp)
	movaps	%xmm2, -3376(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	movq	$0, -3136(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-112(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm1
	leaq	56(%rax), %rdx
	leaq	-1456(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm1, 32(%rax)
	movq	%rax, -3152(%rbp)
	movq	%rdx, -3136(%rbp)
	movq	%rdx, -3144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L68
	call	_ZdlPv@PLT
.L68:
	movq	-3264(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3112(%rbp)
	jne	.L397
.L69:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1584(%rbp)
	je	.L71
.L386:
	movq	-3336(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1648(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -3104(%rbp)
	movaps	%xmm0, -3120(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -3120(%rbp)
	movq	%rdx, -3104(%rbp)
	movq	%rdx, -3112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3120(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L72
	call	_ZdlPv@PLT
.L72:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	32(%rax), %rdx
	movdqu	(%rax), %xmm3
	movq	$0, -3104(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -80(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm0, -3120(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm2
	leaq	40(%rax), %rdx
	leaq	-1264(%rbp), %rdi
	movq	%rax, -3120(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -3104(%rbp)
	movq	%rdx, -3112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L73
	call	_ZdlPv@PLT
.L73:
	movq	-3280(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1392(%rbp)
	je	.L74
.L387:
	movq	-3264(%rbp), %rsi
	movq	%r12, %rdi
	movl	$1799, %ebx
	leaq	-1456(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -3104(%rbp)
	movaps	%xmm0, -3120(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%bx, 4(%rax)
	leaq	7(%rax), %rdx
	movl	$117901063, (%rax)
	movb	$7, 6(%rax)
	movq	%rax, -3120(%rbp)
	movq	%rdx, -3104(%rbp)
	movq	%rdx, -3112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3120(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L75
	call	_ZdlPv@PLT
.L75:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	16(%rax), %rdi
	movq	8(%rax), %r8
	movq	24(%rax), %rsi
	movq	32(%rax), %rcx
	movq	48(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdi, -96(%rbp)
	movl	$48, %edi
	movq	%r8, -104(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movaps	%xmm0, -3120(%rbp)
	movq	%rax, -112(%rbp)
	movq	$0, -3104(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm4
	leaq	48(%rax), %rdx
	leaq	-1072(%rbp), %rdi
	movq	%rax, -3120(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm4, 32(%rax)
	movq	%rdx, -3104(%rbp)
	movq	%rdx, -3112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L76
	call	_ZdlPv@PLT
.L76:
	movq	-3296(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1200(%rbp)
	je	.L77
.L388:
	movq	-3280(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1264(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -3104(%rbp)
	movaps	%xmm0, -3120(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -3120(%rbp)
	movq	%rdx, -3104(%rbp)
	movq	%rdx, -3112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3120(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L78
	call	_ZdlPv@PLT
.L78:
	movq	(%rbx), %rax
	movl	$35, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	16(%rax), %rbx
	movq	32(%rax), %rax
	movq	%rcx, -3360(%rbp)
	movq	%rax, -3376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %xmm0
	movl	$32, %edi
	movq	$0, -3104(%rbp)
	movhps	-3360(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%rbx, %xmm0
	movhps	-3376(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3120(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm1
	movdqa	-96(%rbp), %xmm3
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-496(%rbp), %rdi
	movq	%rax, -3120(%rbp)
	movups	%xmm1, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -3104(%rbp)
	movq	%rdx, -3112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L79
	call	_ZdlPv@PLT
.L79:
	movq	-3272(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1008(%rbp)
	je	.L80
.L389:
	movq	-3296(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1072(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -3104(%rbp)
	movaps	%xmm0, -3120(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r11d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	6(%rax), %rdx
	movw	%r11w, 4(%rax)
	movq	%rax, -3120(%rbp)
	movq	%rdx, -3104(%rbp)
	movq	%rdx, -3112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3120(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L81
	call	_ZdlPv@PLT
.L81:
	movq	(%rbx), %rax
	movl	$34, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	leaq	-3168(%rbp), %r14
	movq	(%rax), %rcx
	movq	24(%rax), %rbx
	movq	%rcx, -3376(%rbp)
	movq	8(%rax), %rcx
	movq	%rbx, -3360(%rbp)
	movq	%rcx, -3408(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -3392(%rbp)
	movq	32(%rax), %rcx
	movq	40(%rax), %rax
	movq	%rcx, -3344(%rbp)
	movq	%rax, -3400(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$42, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19IsUndefinedConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	je	.L82
.L84:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	xorl	%edx, %edx
	movq	%rax, %rsi
.L378:
	movq	%r13, %rdi
	movl	$5, %ebx
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	%r14, %rdi
	movl	$2, %esi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-3120(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -3416(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-112(%rbp), %rcx
	pushq	%rbx
	xorl	%esi, %esi
	movq	-3400(%rbp), %xmm0
	movq	%rax, %r8
	pushq	%rcx
	movq	%r14, %rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-3376(%rbp), %r9
	movl	$1, %ecx
	leaq	-3152(%rbp), %rdx
	movhps	-3416(%rbp), %xmm0
	movq	%rax, -3152(%rbp)
	movq	-3104(%rbp), %rax
	movaps	%xmm0, -112(%rbp)
	movq	-3360(%rbp), %xmm0
	movq	%rax, -3144(%rbp)
	movq	-3392(%rbp), %rax
	movhps	-3344(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	popq	%r9
	movq	%r14, %rdi
	popq	%r10
	movq	-3376(%rbp), %xmm7
	movq	%rax, %rbx
	movq	-3392(%rbp), %xmm4
	movq	-3344(%rbp), %xmm5
	movhps	-3408(%rbp), %xmm7
	movhps	-3360(%rbp), %xmm4
	movhps	-3400(%rbp), %xmm5
	movaps	%xmm7, -3376(%rbp)
	movaps	%xmm4, -3360(%rbp)
	movaps	%xmm5, -3392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$43, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal13ToBoolean_240EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEE@PLT
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movdqa	-3376(%rbp), %xmm7
	movdqa	-3360(%rbp), %xmm4
	movdqa	-3392(%rbp), %xmm5
	movq	%rbx, -64(%rbp)
	movq	%rax, %r14
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm0, -3120(%rbp)
	movq	$0, -3104(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-112(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm7
	leaq	56(%rax), %rdx
	leaq	-880(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rax, -3120(%rbp)
	movq	%rdx, -3104(%rbp)
	movq	%rdx, -3112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L85
	call	_ZdlPv@PLT
.L85:
	movdqa	-3376(%rbp), %xmm5
	movdqa	-3360(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movdqa	-3392(%rbp), %xmm1
	movq	%rbx, -64(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	movaps	%xmm0, -3120(%rbp)
	movq	$0, -3104(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-112(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm2
	leaq	56(%rax), %rdx
	leaq	-688(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm2, 32(%rax)
	movq	%rax, -3120(%rbp)
	movq	%rdx, -3104(%rbp)
	movq	%rdx, -3112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L86
	call	_ZdlPv@PLT
.L86:
	movq	-3312(%rbp), %rcx
	movq	-3304(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -816(%rbp)
	je	.L87
.L390:
	movq	-3304(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-880(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -3104(%rbp)
	movaps	%xmm0, -3120(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r8d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	7(%rax), %rdx
	movw	%r8w, 4(%rax)
	movb	$8, 6(%rax)
	movq	%rax, -3120(%rbp)
	movq	%rdx, -3104(%rbp)
	movq	%rdx, -3112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L88
	call	_ZdlPv@PLT
.L88:
	movl	$44, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -624(%rbp)
	je	.L89
.L391:
	movq	-3312(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3208(%rbp)
	leaq	-688(%rbp), %r14
	movq	$0, -3200(%rbp)
	movq	$0, -3192(%rbp)
	movq	$0, -3184(%rbp)
	movq	$0, -3176(%rbp)
	movq	$0, -3168(%rbp)
	movq	$0, -3152(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3152(%rbp), %rax
	movq	%r14, %rdi
	leaq	-3176(%rbp), %r9
	pushq	%rax
	leaq	-3168(%rbp), %rax
	leaq	-3192(%rbp), %rcx
	pushq	%rax
	leaq	-3184(%rbp), %r8
	leaq	-3200(%rbp), %rdx
	leaq	-3208(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_10JSReceiverES6_S6_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESH_SH_PNS9_IS7_EE
	movl	$46, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3192(%rbp), %r8
	movq	%r13, %rdi
	movq	-3200(%rbp), %rcx
	movq	-3176(%rbp), %rdx
	movq	-3208(%rbp), %rsi
	call	_ZN2v88internal24ProxiesCodeStubAssembler18CheckHasTrapResultENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_7JSProxyEEENS3_INS0_4NameEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$47, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8False_66EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -432(%rbp)
	popq	%rsi
	popq	%rdi
	je	.L90
.L392:
	movq	-3272(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-496(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -3104(%rbp)
	movaps	%xmm0, -3120(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134678279, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -3120(%rbp)
	movq	%rdx, -3104(%rbp)
	movq	%rdx, -3112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3120(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L91
	call	_ZdlPv@PLT
.L91:
	movq	(%rbx), %rax
	movl	$51, %edx
	movq	%r12, %rdi
	leaq	-3168(%rbp), %r14
	leaq	.LC1(%rip), %rsi
	movq	16(%rax), %rcx
	movq	(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rcx, -3360(%rbp)
	movq	%rax, -3376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$159, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-3120(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	leaq	-112(%rbp), %r8
	movq	-3376(%rbp), %xmm0
	movq	%rax, %rdx
	movq	-3104(%rbp), %rax
	movq	%r14, %rdi
	movq	%rcx, -3152(%rbp)
	movhps	-3360(%rbp), %xmm0
	leaq	-3152(%rbp), %rsi
	movl	$2, %r9d
	movq	%rbx, %rcx
	movq	%rax, -3144(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -240(%rbp)
	je	.L92
.L393:
	movq	-3288(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-304(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -3104(%rbp)
	movaps	%xmm0, -3120(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -3120(%rbp)
	movq	%rdx, -3104(%rbp)
	movq	%rdx, -3112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3120(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L93
	call	_ZdlPv@PLT
.L93:
	movq	(%rbx), %rax
	movl	$54, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movl	$145, %edx
	leaq	.LC2(%rip), %rcx
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L82:
	movq	-3360(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IsNullConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L84
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	movq	%rax, %rsi
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L395:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-3360(%rbp), %xmm7
	movdqa	-3376(%rbp), %xmm5
	movaps	%xmm0, -3152(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movq	$0, -3136(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm1
	movq	%r14, %rsi
	leaq	32(%rax), %rdx
	leaq	-2800(%rbp), %rdi
	movq	%rax, -3152(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm1, 16(%rax)
	movq	%rdx, -3136(%rbp)
	movq	%rdx, -3144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L49
	call	_ZdlPv@PLT
.L49:
	movq	-3328(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L396:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movdqa	-3360(%rbp), %xmm7
	movdqa	-3376(%rbp), %xmm5
	movq	%rbx, -80(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm0, -3152(%rbp)
	movq	$0, -3136(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm1
	leaq	40(%rax), %rdx
	leaq	-2416(%rbp), %rdi
	movq	%rax, -3152(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm1, 16(%rax)
	movq	%rdx, -3136(%rbp)
	movq	%rdx, -3144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L57
	call	_ZdlPv@PLT
.L57:
	movq	-3320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L397:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movdqa	-3360(%rbp), %xmm3
	movdqa	-3376(%rbp), %xmm6
	movdqa	-3392(%rbp), %xmm2
	movaps	%xmm0, -3152(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm2, -80(%rbp)
	movq	$0, -3136(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm4
	leaq	48(%rax), %rdx
	leaq	-1648(%rbp), %rdi
	movq	%rax, -3152(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm4, 32(%rax)
	movq	%rdx, -3136(%rbp)
	movq	%rdx, -3144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L70
	call	_ZdlPv@PLT
.L70:
	movq	-3336(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L69
.L394:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22418:
	.size	_ZN2v88internal25ProxyHasPropertyAssembler28GenerateProxyHasPropertyImplEv, .-_ZN2v88internal25ProxyHasPropertyAssembler28GenerateProxyHasPropertyImplEv
	.section	.rodata._ZN2v88internal8Builtins25Generate_ProxyHasPropertyEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/proxy-has-property-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins25Generate_ProxyHasPropertyEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"ProxyHasProperty"
	.section	.text._ZN2v88internal8Builtins25Generate_ProxyHasPropertyEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins25Generate_ProxyHasPropertyEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins25Generate_ProxyHasPropertyEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins25Generate_ProxyHasPropertyEPNS0_8compiler18CodeAssemblerStateE:
.LFB22414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$146, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$854, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L402
.L399:
	movq	%r13, %rdi
	call	_ZN2v88internal25ProxyHasPropertyAssembler28GenerateProxyHasPropertyImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L403
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L402:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L399
.L403:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22414:
	.size	_ZN2v88internal8Builtins25Generate_ProxyHasPropertyEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins25Generate_ProxyHasPropertyEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins25Generate_ProxyHasPropertyEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins25Generate_ProxyHasPropertyEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins25Generate_ProxyHasPropertyEPNS0_8compiler18CodeAssemblerStateE:
.LFB29042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29042:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins25Generate_ProxyHasPropertyEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins25Generate_ProxyHasPropertyEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins25Generate_ProxyHasPropertyEPNS0_8compiler18CodeAssemblerStateE
	.weak	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
