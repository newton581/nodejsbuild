	.file	"array-copywithin-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.rodata._ZN2v88internal24ConvertToRelativeIndex_1EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../../deps/v8/../../deps/v8/src/builtins/array-copywithin.tq"
	.section	.text._ZN2v88internal24ConvertToRelativeIndex_1EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24ConvertToRelativeIndex_1EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_
	.type	_ZN2v88internal24ConvertToRelativeIndex_1EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_, @function
_ZN2v88internal24ConvertToRelativeIndex_1EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_:
.LFB22410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-1368(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1456(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1464(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1424(%rbp), %rbx
	subq	$1512, %rsp
	movq	%rsi, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1464(%rbp)
	movq	%rdi, -1424(%rbp)
	movl	$48, %edi
	movq	$0, -1416(%rbp)
	movq	$0, -1408(%rbp)
	movq	$0, -1400(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r15, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -1400(%rbp)
	movq	%rdx, -1408(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1384(%rbp)
	movq	%rax, -1416(%rbp)
	movq	$0, -1392(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1464(%rbp), %rax
	movl	$48, %edi
	movq	$0, -1224(%rbp)
	movq	$0, -1216(%rbp)
	movq	%rax, -1232(%rbp)
	movq	$0, -1208(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1224(%rbp)
	leaq	-1176(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1208(%rbp)
	movq	%rdx, -1216(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1192(%rbp)
	movq	%rax, -1512(%rbp)
	movq	$0, -1200(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1464(%rbp), %rax
	movl	$48, %edi
	movq	$0, -1032(%rbp)
	movq	$0, -1024(%rbp)
	movq	%rax, -1040(%rbp)
	movq	$0, -1016(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1032(%rbp)
	leaq	-984(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1016(%rbp)
	movq	%rdx, -1024(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1000(%rbp)
	movq	%rax, -1504(%rbp)
	movq	$0, -1008(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1464(%rbp), %rax
	movl	$72, %edi
	movq	$0, -840(%rbp)
	movq	$0, -832(%rbp)
	movq	%rax, -848(%rbp)
	movq	$0, -824(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -840(%rbp)
	leaq	-792(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -824(%rbp)
	movq	%rdx, -832(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -808(%rbp)
	movq	%rax, -1496(%rbp)
	movq	$0, -816(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1464(%rbp), %rax
	movl	$72, %edi
	movq	$0, -648(%rbp)
	movq	$0, -640(%rbp)
	movq	%rax, -656(%rbp)
	movq	$0, -632(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -648(%rbp)
	leaq	-600(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -632(%rbp)
	movq	%rdx, -640(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -616(%rbp)
	movq	%rax, -1488(%rbp)
	movq	$0, -624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1464(%rbp), %rax
	movl	$72, %edi
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	%rax, -464(%rbp)
	movq	$0, -440(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -456(%rbp)
	leaq	-408(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -440(%rbp)
	movq	%rdx, -448(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -424(%rbp)
	movq	%rax, -1520(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1464(%rbp), %rax
	movl	$72, %edi
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	%rax, -272(%rbp)
	movq	$0, -248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -264(%rbp)
	leaq	-216(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -248(%rbp)
	movq	%rdx, -256(%rbp)
	xorl	%edx, %edx
	movq	%rax, -1480(%rbp)
	movups	%xmm0, -232(%rbp)
	movq	$0, -240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1536(%rbp), %xmm1
	movaps	%xmm0, -1456(%rbp)
	movhps	-1544(%rbp), %xmm1
	movq	$0, -1440(%rbp)
	movaps	%xmm1, -1536(%rbp)
	call	_Znwm@PLT
	movdqa	-1536(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1456(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1440(%rbp)
	movq	%rdx, -1448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1456(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L8
	call	_ZdlPv@PLT
.L8:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1360(%rbp)
	jne	.L168
	cmpq	$0, -1168(%rbp)
	jne	.L169
.L13:
	cmpq	$0, -976(%rbp)
	jne	.L170
.L16:
	cmpq	$0, -784(%rbp)
	jne	.L171
.L19:
	cmpq	$0, -592(%rbp)
	jne	.L172
.L22:
	cmpq	$0, -400(%rbp)
	leaq	-272(%rbp), %r14
	jne	.L173
.L25:
	movq	-1480(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1440(%rbp)
	movaps	%xmm0, -1456(%rbp)
	call	_Znwm@PLT
	movl	$2056, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -1456(%rbp)
	movq	%rdx, -1440(%rbp)
	movq	%rdx, -1448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1456(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L28
	call	_ZdlPv@PLT
.L28:
	movq	(%rbx), %rax
	movq	-1480(%rbp), %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L29
	call	_ZdlPv@PLT
.L29:
	movq	-256(%rbp), %rbx
	movq	-264(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L30
	.p2align 4,,10
	.p2align 3
.L34:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L31
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L34
.L32:
	movq	-264(%rbp), %r14
.L30:
	testq	%r14, %r14
	je	.L35
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L35:
	movq	-1520(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L36
	call	_ZdlPv@PLT
.L36:
	movq	-448(%rbp), %rbx
	movq	-456(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L37
	.p2align 4,,10
	.p2align 3
.L41:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L38
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L41
.L39:
	movq	-456(%rbp), %r14
.L37:
	testq	%r14, %r14
	je	.L42
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L42:
	movq	-1488(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L43
	call	_ZdlPv@PLT
.L43:
	movq	-640(%rbp), %rbx
	movq	-648(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L44
	.p2align 4,,10
	.p2align 3
.L48:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L45
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L48
.L46:
	movq	-648(%rbp), %r14
.L44:
	testq	%r14, %r14
	je	.L49
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L49:
	movq	-1496(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-816(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L50
	call	_ZdlPv@PLT
.L50:
	movq	-832(%rbp), %rbx
	movq	-840(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L51
	.p2align 4,,10
	.p2align 3
.L55:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L52
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L55
.L53:
	movq	-840(%rbp), %r14
.L51:
	testq	%r14, %r14
	je	.L56
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L56:
	movq	-1504(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1008(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L57
	call	_ZdlPv@PLT
.L57:
	movq	-1024(%rbp), %rbx
	movq	-1032(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L58
	.p2align 4,,10
	.p2align 3
.L62:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L59
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L62
.L60:
	movq	-1032(%rbp), %r14
.L58:
	testq	%r14, %r14
	je	.L63
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L63:
	movq	-1512(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L64
	call	_ZdlPv@PLT
.L64:
	movq	-1216(%rbp), %rbx
	movq	-1224(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L65
	.p2align 4,,10
	.p2align 3
.L69:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L66
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L69
.L67:
	movq	-1224(%rbp), %r14
.L65:
	testq	%r14, %r14
	je	.L70
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L70:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L71
	call	_ZdlPv@PLT
.L71:
	movq	-1408(%rbp), %rbx
	movq	-1416(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L72
	.p2align 4,,10
	.p2align 3
.L76:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L73
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L76
.L74:
	movq	-1416(%rbp), %r14
.L72:
	testq	%r14, %r14
	je	.L77
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L77:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L174
	addq	$1512, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L76
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L66:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L69
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L59:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L62
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L52:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L55
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L45:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L48
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L31:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L34
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L38:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L41
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L168:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1440(%rbp)
	movaps	%xmm0, -1456(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r11d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r11w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1456(%rbp)
	movq	%rdx, -1440(%rbp)
	movq	%rdx, -1448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1456(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	movq	(%rbx), %rax
	movl	$7, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -1544(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rbx, -1536(%rbp)
	movq	%rax, %rdx
	call	_ZN2v88internal19NumberIsLessThan_75EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1536(%rbp), %xmm2
	movaps	%xmm0, -1456(%rbp)
	movq	%rax, %rbx
	movhps	-1544(%rbp), %xmm2
	movq	$0, -1440(%rbp)
	movaps	%xmm2, -1536(%rbp)
	call	_Znwm@PLT
	movdqa	-1536(%rbp), %xmm3
	leaq	-1232(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1456(%rbp)
	movups	%xmm3, (%rax)
	movq	%rdx, -1440(%rbp)
	movq	%rdx, -1448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1456(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L11
	call	_ZdlPv@PLT
.L11:
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	$0, -1440(%rbp)
	movaps	%xmm0, -1456(%rbp)
	call	_Znwm@PLT
	movdqa	-1536(%rbp), %xmm4
	leaq	-1040(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1456(%rbp)
	movups	%xmm4, (%rax)
	movq	%rdx, -1440(%rbp)
	movq	%rdx, -1448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1456(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L12
	call	_ZdlPv@PLT
.L12:
	movq	-1504(%rbp), %rcx
	movq	-1512(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1168(%rbp)
	je	.L13
.L169:
	movq	-1512(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1232(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1440(%rbp)
	movaps	%xmm0, -1456(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r10d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r10w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1456(%rbp)
	movq	%rdx, -1440(%rbp)
	movq	%rdx, -1448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1456(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L14
	call	_ZdlPv@PLT
.L14:
	movq	(%rbx), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -1536(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1536(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberAddENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r13, %rdi
	movq	%rax, -1544(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	-1544(%rbp), %r8
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal6Max_81EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	$0, -1440(%rbp)
	movhps	-1536(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1456(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-848(%rbp), %rdi
	movq	%rax, -1456(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -1440(%rbp)
	movq	%rdx, -1448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1456(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L15
	call	_ZdlPv@PLT
.L15:
	movq	-1496(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -976(%rbp)
	je	.L16
.L170:
	movq	-1504(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1040(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1440(%rbp)
	movaps	%xmm0, -1456(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r9d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1456(%rbp)
	movq	%rdx, -1440(%rbp)
	movq	%rdx, -1448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1456(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L17
	call	_ZdlPv@PLT
.L17:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rbx, %rsi
	movq	%rax, %rdx
	movq	%rax, -1536(%rbp)
	call	_ZN2v88internal6Min_80EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	$0, -1440(%rbp)
	movhps	-1536(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1456(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-656(%rbp), %rdi
	movq	%rax, -1456(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1440(%rbp)
	movq	%rdx, -1448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1456(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L18
	call	_ZdlPv@PLT
.L18:
	movq	-1488(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -784(%rbp)
	je	.L19
.L171:
	movq	-1496(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-848(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1440(%rbp)
	movaps	%xmm0, -1456(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r8d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -1456(%rbp)
	movq	%rdx, -1440(%rbp)
	movq	%rdx, -1448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1456(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L20
	call	_ZdlPv@PLT
.L20:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm7
	movaps	%xmm0, -1456(%rbp)
	movq	$0, -1440(%rbp)
	movq	%rdx, -64(%rbp)
	movaps	%xmm7, -80(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm3
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-656(%rbp), %rdi
	movq	%rax, -1456(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -1440(%rbp)
	movq	%rdx, -1448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1456(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L21
	call	_ZdlPv@PLT
.L21:
	movq	-1488(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -592(%rbp)
	je	.L22
.L172:
	movq	-1488(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-656(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1440(%rbp)
	movaps	%xmm0, -1456(%rbp)
	call	_Znwm@PLT
	movl	$2056, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r14, %rdi
	movb	$8, 2(%rax)
	movq	%rax, -1456(%rbp)
	movq	%rdx, -1440(%rbp)
	movq	%rdx, -1448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1456(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L23
	call	_ZdlPv@PLT
.L23:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm4
	movaps	%xmm0, -1456(%rbp)
	movq	$0, -1440(%rbp)
	movq	%rdx, -64(%rbp)
	movaps	%xmm4, -80(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-464(%rbp), %rdi
	movq	%rax, -1456(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -1440(%rbp)
	movq	%rdx, -1448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1456(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L24
	call	_ZdlPv@PLT
.L24:
	movq	-1520(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L173:
	movq	-1520(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-464(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1440(%rbp)
	movaps	%xmm0, -1456(%rbp)
	call	_Znwm@PLT
	movl	$2056, %esi
	movq	%r14, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$8, 2(%rax)
	movq	%rax, -1456(%rbp)
	movq	%rdx, -1440(%rbp)
	movq	%rdx, -1448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1456(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L26
	call	_ZdlPv@PLT
.L26:
	movq	(%rbx), %rax
	movl	$6, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r14
	movq	%rcx, -1536(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r14, -64(%rbp)
	movhps	-1536(%rbp), %xmm0
	leaq	-272(%rbp), %r14
	movq	$0, -1440(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1456(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-80(%rbp), %xmm6
	leaq	24(%rax), %rdx
	movq	%rax, -1456(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1440(%rbp)
	movq	%rdx, -1448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1456(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L27
	call	_ZdlPv@PLT
.L27:
	movq	-1480(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L25
.L174:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22410:
	.size	_ZN2v88internal24ConvertToRelativeIndex_1EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_, .-_ZN2v88internal24ConvertToRelativeIndex_1EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEESB_SB_SB_SB_SB_SB_SB_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESP_SP_SP_SP_SP_SP_SP_SP_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEESB_SB_SB_SB_SB_SB_SB_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESP_SP_SP_SP_SP_SP_SP_SP_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEESB_SB_SB_SB_SB_SB_SB_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESP_SP_SP_SP_SP_SP_SP_SP_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEESB_SB_SB_SB_SB_SB_SB_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESP_SP_SP_SP_SP_SP_SP_SP_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEESB_SB_SB_SB_SB_SB_SB_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESP_SP_SP_SP_SP_SP_SP_SP_:
.LFB26570:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$15, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$136, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -88(%rbp)
	movq	%r9, -96(%rbp)
	movq	88(%rbp), %r15
	movq	%rax, -104(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$578720283176011013, %rcx
	movq	%rcx, (%rax)
	movl	$2056, %ecx
	leaq	15(%rax), %rdx
	movl	$134744072, 8(%rax)
	movw	%cx, 12(%rax)
	movb	$8, 14(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L176
	movq	%rax, -176(%rbp)
	call	_ZdlPv@PLT
	movq	-176(%rbp), %rax
.L176:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L177
	movq	%rdx, (%r14)
.L177:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L178
	movq	%rdx, 0(%r13)
.L178:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L179
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L179:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L180
	movq	%rdx, (%rbx)
.L180:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L181
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L181:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L182
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L182:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L183
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L183:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L184
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L184:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L185
	movq	-128(%rbp), %rsi
	movq	%rdx, (%rsi)
.L185:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L186
	movq	-136(%rbp), %rcx
	movq	%rdx, (%rcx)
.L186:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L187
	movq	-144(%rbp), %rbx
	movq	%rdx, (%rbx)
.L187:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L188
	movq	-152(%rbp), %rsi
	movq	%rdx, (%rsi)
.L188:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L189
	movq	-160(%rbp), %rcx
	movq	%rdx, (%rcx)
.L189:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L190
	movq	-168(%rbp), %rdi
	movq	%rdx, (%rdi)
.L190:
	movq	112(%rax), %rax
	testq	%rax, %rax
	je	.L175
	movq	%rax, (%r15)
.L175:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L242
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L242:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26570:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEESB_SB_SB_SB_SB_SB_SB_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESP_SP_SP_SP_SP_SP_SP_SP_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEESB_SB_SB_SB_SB_SB_SB_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESP_SP_SP_SP_SP_SP_SP_SP_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEESB_SB_SB_SB_SB_SB_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EESG_PNSE_IS4_EEPNSE_IS5_EEPNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EESQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_PNSE_ISC_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEESB_SB_SB_SB_SB_SB_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EESG_PNSE_IS4_EEPNSE_IS5_EEPNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EESQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_PNSE_ISC_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEESB_SB_SB_SB_SB_SB_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EESG_PNSE_IS4_EEPNSE_IS5_EEPNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EESQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_PNSE_ISC_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEESB_SB_SB_SB_SB_SB_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EESG_PNSE_IS4_EEPNSE_IS5_EEPNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EESQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_PNSE_ISC_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEESB_SB_SB_SB_SB_SB_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EESG_PNSE_IS4_EEPNSE_IS5_EEPNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EESQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_PNSE_ISC_EE:
.LFB26574:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$16, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$152, %rsp
	movq	16(%rbp), %rax
	movq	%rdx, -88(%rbp)
	movq	88(%rbp), %r15
	movq	96(%rbp), %r14
	movq	%r8, -96(%rbp)
	movq	%rax, -112(%rbp)
	movq	24(%rbp), %rax
	movq	%r9, -104(%rbp)
	movq	%rax, -120(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movabsq	$578720283176011013, %rsi
	movabsq	$506663788666685448, %rdi
	movq	%rsi, (%rax)
	leaq	16(%rax), %rdx
	leaq	-80(%rbp), %rsi
	movq	%rdi, 8(%rax)
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L244
	movq	%rax, -184(%rbp)
	call	_ZdlPv@PLT
	movq	-184(%rbp), %rax
.L244:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L245
	movq	%rdx, 0(%r13)
.L245:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L246
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L246:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L247
	movq	%rdx, (%rbx)
.L247:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L248
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L248:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L249
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L249:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L250
	movq	-112(%rbp), %rcx
	movq	%rdx, (%rcx)
.L250:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L251
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L251:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L252
	movq	-128(%rbp), %rcx
	movq	%rdx, (%rcx)
.L252:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L253
	movq	-136(%rbp), %rbx
	movq	%rdx, (%rbx)
.L253:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L254
	movq	-144(%rbp), %rcx
	movq	%rdx, (%rcx)
.L254:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L255
	movq	-152(%rbp), %rbx
	movq	%rdx, (%rbx)
.L255:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L256
	movq	-160(%rbp), %rsi
	movq	%rdx, (%rsi)
.L256:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L257
	movq	-168(%rbp), %rcx
	movq	%rdx, (%rcx)
.L257:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L258
	movq	-176(%rbp), %rbx
	movq	%rdx, (%rbx)
.L258:
	movq	112(%rax), %rdx
	testq	%rdx, %rdx
	je	.L259
	movq	%rdx, (%r15)
.L259:
	movq	120(%rax), %rax
	testq	%rax, %rax
	je	.L243
	movq	%rax, (%r14)
.L243:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L314
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L314:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26574:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEESB_SB_SB_SB_SB_SB_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EESG_PNSE_IS4_EEPNSE_IS5_EEPNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EESQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_PNSE_ISC_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEESB_SB_SB_SB_SB_SB_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EESG_PNSE_IS4_EEPNSE_IS5_EEPNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EESQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_PNSE_ISC_EE
	.section	.text._ZN2v88internal33ArrayPrototypeCopyWithinAssembler36GenerateArrayPrototypeCopyWithinImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33ArrayPrototypeCopyWithinAssembler36GenerateArrayPrototypeCopyWithinImplEv
	.type	_ZN2v88internal33ArrayPrototypeCopyWithinAssembler36GenerateArrayPrototypeCopyWithinImplEv, @function
_ZN2v88internal33ArrayPrototypeCopyWithinAssembler36GenerateArrayPrototypeCopyWithinImplEv:
.LFB22428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-3288(%rbp), %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$3576, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r13, -3288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r12, %rcx
	movq	%r14, %rsi
	leaq	-3120(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-3120(%rbp), %r12
	movl	$2, %esi
	movq	%r14, %rdi
	movq	-3112(%rbp), %rax
	movq	-3104(%rbp), %rbx
	movq	%r14, -2928(%rbp)
	movq	%r12, -2896(%rbp)
	movq	%rax, -3440(%rbp)
	movq	$1, -2920(%rbp)
	movq	%rbx, -2912(%rbp)
	movq	%rax, -2904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rax, -3424(%rbp)
	leaq	-2928(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -3448(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -2872(%rbp)
	movq	$0, -2864(%rbp)
	movq	%rax, %r14
	movq	-3288(%rbp), %rax
	movq	$0, -2856(%rbp)
	movq	%rax, -2880(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -2872(%rbp)
	leaq	-2824(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2856(%rbp)
	movq	%rdx, -2864(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2840(%rbp)
	movq	%rax, -3304(%rbp)
	movq	$0, -2848(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3288(%rbp), %rax
	movl	$288, %edi
	movq	$0, -2680(%rbp)
	movq	$0, -2672(%rbp)
	movq	%rax, -2688(%rbp)
	movq	$0, -2664(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -2680(%rbp)
	leaq	-2632(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2664(%rbp)
	movq	%rdx, -2672(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2648(%rbp)
	movq	%rax, -3352(%rbp)
	movq	$0, -2656(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3288(%rbp), %rax
	movl	$288, %edi
	movq	$0, -2488(%rbp)
	movq	$0, -2480(%rbp)
	movq	%rax, -2496(%rbp)
	movq	$0, -2472(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -2488(%rbp)
	leaq	-2440(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2472(%rbp)
	movq	%rdx, -2480(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2456(%rbp)
	movq	%rax, -3312(%rbp)
	movq	$0, -2464(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3288(%rbp), %rax
	movl	$384, %edi
	movq	$0, -2296(%rbp)
	movq	$0, -2288(%rbp)
	movq	%rax, -2304(%rbp)
	movq	$0, -2280(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	384(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movq	%rax, -2296(%rbp)
	leaq	-2248(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2280(%rbp)
	movq	%rdx, -2288(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2264(%rbp)
	movq	%rax, -3360(%rbp)
	movq	$0, -2272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3288(%rbp), %rax
	movl	$384, %edi
	movq	$0, -2104(%rbp)
	movq	$0, -2096(%rbp)
	movq	%rax, -2112(%rbp)
	movq	$0, -2088(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	384(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movq	%rax, -2104(%rbp)
	leaq	-2056(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2088(%rbp)
	movq	%rdx, -2096(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2072(%rbp)
	movq	%rax, -3368(%rbp)
	movq	$0, -2080(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3288(%rbp), %rax
	movl	$408, %edi
	movq	$0, -1912(%rbp)
	movq	$0, -1904(%rbp)
	movq	%rax, -1920(%rbp)
	movq	$0, -1896(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	408(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 400(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movq	%rax, -1912(%rbp)
	leaq	-1864(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1896(%rbp)
	movq	%rdx, -1904(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1880(%rbp)
	movq	%rax, -3328(%rbp)
	movq	$0, -1888(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3288(%rbp), %rax
	movl	$360, %edi
	movq	$0, -1720(%rbp)
	movq	$0, -1712(%rbp)
	movq	%rax, -1728(%rbp)
	movq	$0, -1704(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	360(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 352(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movq	%rax, -1720(%rbp)
	leaq	-1672(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1704(%rbp)
	movq	%rdx, -1712(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1688(%rbp)
	movq	%rax, -3376(%rbp)
	movq	$0, -1696(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3288(%rbp), %rax
	movl	$360, %edi
	movq	$0, -1528(%rbp)
	movq	$0, -1520(%rbp)
	movq	%rax, -1536(%rbp)
	movq	$0, -1512(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	360(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 352(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movq	%rax, -1528(%rbp)
	leaq	-1480(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1512(%rbp)
	movq	%rdx, -1520(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1496(%rbp)
	movq	%rax, -3320(%rbp)
	movq	$0, -1504(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3288(%rbp), %rax
	movl	$360, %edi
	movq	$0, -1336(%rbp)
	movq	$0, -1328(%rbp)
	movq	%rax, -1344(%rbp)
	movq	$0, -1320(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	360(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 352(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movq	%rax, -1336(%rbp)
	leaq	-1288(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1320(%rbp)
	movq	%rdx, -1328(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1304(%rbp)
	movq	%rax, -3336(%rbp)
	movq	$0, -1312(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3288(%rbp), %rax
	movl	$360, %edi
	movq	$0, -1144(%rbp)
	movq	$0, -1136(%rbp)
	movq	%rax, -1152(%rbp)
	movq	$0, -1128(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	360(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 352(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movq	%rax, -1144(%rbp)
	leaq	-1096(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1128(%rbp)
	movq	%rdx, -1136(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1112(%rbp)
	movq	%rax, -3384(%rbp)
	movq	$0, -1120(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3288(%rbp), %rax
	movl	$384, %edi
	movq	$0, -952(%rbp)
	movq	$0, -944(%rbp)
	movq	%rax, -960(%rbp)
	movq	$0, -936(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	384(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movq	%rax, -952(%rbp)
	leaq	-904(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -936(%rbp)
	movq	%rdx, -944(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -920(%rbp)
	movq	%rax, -3392(%rbp)
	movq	$0, -928(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3288(%rbp), %rax
	movl	$384, %edi
	movq	$0, -760(%rbp)
	movq	$0, -752(%rbp)
	movq	%rax, -768(%rbp)
	movq	$0, -744(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	384(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movq	%rax, -760(%rbp)
	leaq	-712(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -744(%rbp)
	movq	%rdx, -752(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -728(%rbp)
	movq	%rax, -3400(%rbp)
	movq	$0, -736(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3288(%rbp), %rax
	movl	$384, %edi
	movq	$0, -568(%rbp)
	movq	$0, -560(%rbp)
	movq	%rax, -576(%rbp)
	movq	$0, -552(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	384(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movq	%rax, -568(%rbp)
	leaq	-520(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -552(%rbp)
	movq	%rdx, -560(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -536(%rbp)
	movq	%rax, -3344(%rbp)
	movq	$0, -544(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3288(%rbp), %rax
	movl	$360, %edi
	movq	$0, -376(%rbp)
	movq	$0, -368(%rbp)
	movq	%rax, -384(%rbp)
	movq	$0, -360(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	360(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 352(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movq	%rax, -376(%rbp)
	leaq	-328(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -360(%rbp)
	movq	%rdx, -368(%rbp)
	xorl	%edx, %edx
	movq	%rax, -3408(%rbp)
	movups	%xmm0, -344(%rbp)
	movq	$0, -352(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %xmm1
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movhps	-3440(%rbp), %xmm1
	movq	%r14, -160(%rbp)
	leaq	-2880(%rbp), %r12
	leaq	-2960(%rbp), %r14
	movaps	%xmm1, -192(%rbp)
	movq	%rbx, %xmm1
	movhps	-3424(%rbp), %xmm1
	movaps	%xmm0, -2960(%rbp)
	movaps	%xmm1, -176(%rbp)
	movq	$0, -2944(%rbp)
	call	_Znwm@PLT
	movq	-160(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm7
	movdqa	-176(%rbp), %xmm4
	leaq	40(%rax), %rdx
	movq	%rax, -2960(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2944(%rbp)
	movq	%rdx, -2952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2960(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L316
	call	_ZdlPv@PLT
.L316:
	movq	-3304(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2816(%rbp)
	jne	.L618
	cmpq	$0, -2624(%rbp)
	jne	.L619
.L321:
	cmpq	$0, -2432(%rbp)
	jne	.L620
.L324:
	cmpq	$0, -2240(%rbp)
	jne	.L621
.L328:
	cmpq	$0, -2048(%rbp)
	jne	.L622
.L331:
	cmpq	$0, -1856(%rbp)
	jne	.L623
.L334:
	cmpq	$0, -1664(%rbp)
	jne	.L624
.L338:
	cmpq	$0, -1472(%rbp)
	jne	.L625
.L340:
	cmpq	$0, -1280(%rbp)
	jne	.L626
.L342:
	cmpq	$0, -1088(%rbp)
	jne	.L627
.L346:
	cmpq	$0, -896(%rbp)
	jne	.L628
.L349:
	cmpq	$0, -704(%rbp)
	jne	.L629
.L351:
	cmpq	$0, -512(%rbp)
	jne	.L630
.L353:
	cmpq	$0, -320(%rbp)
	jne	.L631
.L355:
	movq	-3408(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L356
	call	_ZdlPv@PLT
.L356:
	movq	-368(%rbp), %rbx
	movq	-376(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L357
	.p2align 4,,10
	.p2align 3
.L361:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L358
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L361
.L359:
	movq	-376(%rbp), %r12
.L357:
	testq	%r12, %r12
	je	.L362
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L362:
	movq	-3344(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-544(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L363
	call	_ZdlPv@PLT
.L363:
	movq	-560(%rbp), %rbx
	movq	-568(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L364
	.p2align 4,,10
	.p2align 3
.L368:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L365
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L368
.L366:
	movq	-568(%rbp), %r12
.L364:
	testq	%r12, %r12
	je	.L369
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L369:
	movq	-3400(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-736(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L370
	call	_ZdlPv@PLT
.L370:
	movq	-752(%rbp), %rbx
	movq	-760(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L371
	.p2align 4,,10
	.p2align 3
.L375:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L372
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L375
.L373:
	movq	-760(%rbp), %r12
.L371:
	testq	%r12, %r12
	je	.L376
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L376:
	movq	-3392(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-928(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L377
	call	_ZdlPv@PLT
.L377:
	movq	-944(%rbp), %rbx
	movq	-952(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L378
	.p2align 4,,10
	.p2align 3
.L382:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L379
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L382
.L380:
	movq	-952(%rbp), %r12
.L378:
	testq	%r12, %r12
	je	.L383
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L383:
	movq	-3384(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L384
	call	_ZdlPv@PLT
.L384:
	movq	-1136(%rbp), %rbx
	movq	-1144(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L385
	.p2align 4,,10
	.p2align 3
.L389:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L386
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L389
.L387:
	movq	-1144(%rbp), %r12
.L385:
	testq	%r12, %r12
	je	.L390
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L390:
	movq	-3336(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L391
	call	_ZdlPv@PLT
.L391:
	movq	-1328(%rbp), %rbx
	movq	-1336(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L392
	.p2align 4,,10
	.p2align 3
.L396:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L393
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L396
.L394:
	movq	-1336(%rbp), %r12
.L392:
	testq	%r12, %r12
	je	.L397
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L397:
	movq	-3320(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1504(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L398
	call	_ZdlPv@PLT
.L398:
	movq	-1520(%rbp), %rbx
	movq	-1528(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L399
	.p2align 4,,10
	.p2align 3
.L403:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L400
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L403
.L401:
	movq	-1528(%rbp), %r12
.L399:
	testq	%r12, %r12
	je	.L404
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L404:
	movq	-3376(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1696(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L405
	call	_ZdlPv@PLT
.L405:
	movq	-1712(%rbp), %rbx
	movq	-1720(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L406
	.p2align 4,,10
	.p2align 3
.L410:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L407
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L410
.L408:
	movq	-1720(%rbp), %r12
.L406:
	testq	%r12, %r12
	je	.L411
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L411:
	movq	-3328(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1888(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L412
	call	_ZdlPv@PLT
.L412:
	movq	-1904(%rbp), %rbx
	movq	-1912(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L413
	.p2align 4,,10
	.p2align 3
.L417:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L414
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L417
.L415:
	movq	-1912(%rbp), %r12
.L413:
	testq	%r12, %r12
	je	.L418
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L418:
	movq	-3368(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2080(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L419
	call	_ZdlPv@PLT
.L419:
	movq	-2096(%rbp), %rbx
	movq	-2104(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L420
	.p2align 4,,10
	.p2align 3
.L424:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L421
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L424
.L422:
	movq	-2104(%rbp), %r12
.L420:
	testq	%r12, %r12
	je	.L425
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L425:
	movq	-3360(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L426
	call	_ZdlPv@PLT
.L426:
	movq	-2288(%rbp), %rbx
	movq	-2296(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L427
	.p2align 4,,10
	.p2align 3
.L431:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L428
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L431
.L429:
	movq	-2296(%rbp), %r12
.L427:
	testq	%r12, %r12
	je	.L432
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L432:
	movq	-3312(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2464(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L433
	call	_ZdlPv@PLT
.L433:
	movq	-2480(%rbp), %rbx
	movq	-2488(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L434
	.p2align 4,,10
	.p2align 3
.L438:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L435
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L438
.L436:
	movq	-2488(%rbp), %r12
.L434:
	testq	%r12, %r12
	je	.L439
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L439:
	movq	-3352(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L440
	call	_ZdlPv@PLT
.L440:
	movq	-2672(%rbp), %rbx
	movq	-2680(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L441
	.p2align 4,,10
	.p2align 3
.L445:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L442
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L445
.L443:
	movq	-2680(%rbp), %r12
.L441:
	testq	%r12, %r12
	je	.L446
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L446:
	movq	-3304(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2848(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L447
	call	_ZdlPv@PLT
.L447:
	movq	-2864(%rbp), %rbx
	movq	-2872(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L448
	.p2align 4,,10
	.p2align 3
.L452:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L449
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L452
.L450:
	movq	-2872(%rbp), %r12
.L448:
	testq	%r12, %r12
	je	.L453
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L453:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L632
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L449:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L452
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L442:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L445
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L435:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L438
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L428:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L431
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L421:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L424
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L414:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L417
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L407:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L410
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L400:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L403
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L393:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L396
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L386:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L389
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L379:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L382
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L372:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L375
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L358:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L361
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L365:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L368
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L618:
	movq	-3304(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2944(%rbp)
	movaps	%xmm0, -2960(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2960(%rbp)
	movq	%rdx, -2944(%rbp)
	movq	%rdx, -2952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2960(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L318
	call	_ZdlPv@PLT
.L318:
	movq	(%rbx), %rax
	movl	$14, %edx
	movq	%r15, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	movq	24(%rax), %rbx
	movq	%rcx, -3440(%rbp)
	movq	16(%rax), %rcx
	movq	32(%rax), %rax
	movq	%rsi, -3488(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rcx, -3424(%rbp)
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%r12, -3504(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15ToObject_InlineENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	movq	%rax, -3568(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$17, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal21GetLengthProperty_244EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$20, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -3472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-3440(%rbp), %xmm6
	movq	-3424(%rbp), %rcx
	movhps	-3488(%rbp), %xmm6
	movq	%rcx, -3072(%rbp)
	movaps	%xmm6, -3088(%rbp)
	pushq	-3072(%rbp)
	pushq	-3080(%rbp)
	pushq	-3088(%rbp)
	movaps	%xmm6, -3440(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler16ToInteger_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_23ToIntegerTruncationModeE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	movq	%rax, -3456(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$24, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3472(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal24ConvertToRelativeIndex_1EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_
	movl	$27, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -3552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqa	-3440(%rbp), %xmm6
	movq	-3424(%rbp), %rcx
	movaps	%xmm6, -3056(%rbp)
	movq	%rcx, -3040(%rbp)
	pushq	-3040(%rbp)
	pushq	-3048(%rbp)
	pushq	-3056(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler16ToInteger_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_23ToIntegerTruncationModeE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	movq	%rax, -3520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$31, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3472(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal24ConvertToRelativeIndex_1EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_
	movl	$35, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -3488(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$36, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$2, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-3424(%rbp), %rcx
	movdqa	-3440(%rbp), %xmm6
	movq	%rcx, -3008(%rbp)
	movaps	%xmm6, -3024(%rbp)
	pushq	-3008(%rbp)
	pushq	-3016(%rbp)
	pushq	-3024(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -3536(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3536(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3472(%rbp), %rax
	movq	%rbx, %xmm1
	movq	-3488(%rbp), %xmm5
	movq	-3424(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movl	$96, %edi
	movq	-3552(%rbp), %xmm2
	movq	%rax, %xmm6
	movq	%rax, %xmm3
	movq	-3504(%rbp), %xmm7
	movaps	%xmm0, -2960(%rbp)
	punpcklqdq	%xmm6, %xmm5
	movdqa	-3440(%rbp), %xmm6
	movhps	-3520(%rbp), %xmm2
	punpcklqdq	%xmm1, %xmm4
	movhps	-3456(%rbp), %xmm3
	movhps	-3568(%rbp), %xmm7
	movaps	%xmm5, -3536(%rbp)
	movaps	%xmm2, -3520(%rbp)
	movaps	%xmm3, -3488(%rbp)
	movaps	%xmm7, -3472(%rbp)
	movaps	%xmm4, -3424(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movq	$0, -2944(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-144(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm7
	leaq	96(%rax), %rdx
	leaq	-2688(%rbp), %rdi
	movq	%rax, -2960(%rbp)
	movups	%xmm5, (%rax)
	movdqa	-176(%rbp), %xmm5
	movups	%xmm6, 48(%rax)
	movups	%xmm5, 16(%rax)
	movdqa	-160(%rbp), %xmm5
	movups	%xmm7, 64(%rax)
	movups	%xmm5, 32(%rax)
	movdqa	-112(%rbp), %xmm5
	movq	%rdx, -2944(%rbp)
	movups	%xmm5, 80(%rax)
	movq	%rdx, -2952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2960(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L319
	call	_ZdlPv@PLT
.L319:
	movdqa	-3440(%rbp), %xmm6
	movdqa	-3424(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movl	$96, %edi
	movdqa	-3472(%rbp), %xmm7
	movdqa	-3488(%rbp), %xmm5
	movaps	%xmm0, -2960(%rbp)
	movaps	%xmm6, -192(%rbp)
	movdqa	-3520(%rbp), %xmm6
	movaps	%xmm4, -176(%rbp)
	movdqa	-3536(%rbp), %xmm4
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movq	$0, -2944(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm5
	movdqa	-160(%rbp), %xmm6
	movdqa	-144(%rbp), %xmm4
	leaq	96(%rax), %rdx
	leaq	-2496(%rbp), %rdi
	movups	%xmm7, (%rax)
	movdqa	-128(%rbp), %xmm7
	movups	%xmm5, 16(%rax)
	movdqa	-112(%rbp), %xmm5
	movups	%xmm6, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm5, 80(%rax)
	movq	%rax, -2960(%rbp)
	movq	%rdx, -2944(%rbp)
	movq	%rdx, -2952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2960(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L320
	call	_ZdlPv@PLT
.L320:
	movq	-3312(%rbp), %rcx
	movq	-3352(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2624(%rbp)
	je	.L321
.L619:
	movq	-3352(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-2688(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$12, %edi
	movq	$0, -2944(%rbp)
	movaps	%xmm0, -2960(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movabsq	$578720283176011013, %rcx
	movq	%rcx, (%rax)
	leaq	12(%rax), %rdx
	movl	$134744072, 8(%rax)
	movq	%rax, -2960(%rbp)
	movq	%rdx, -2944(%rbp)
	movq	%rdx, -2952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2960(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L322
	call	_ZdlPv@PLT
.L322:
	movq	(%rbx), %rax
	movq	40(%rax), %rdi
	movq	(%rax), %rcx
	movq	56(%rax), %r10
	movq	8(%rax), %rsi
	movq	32(%rax), %rdx
	movq	64(%rax), %r11
	movq	%rdi, -3504(%rbp)
	movq	48(%rax), %rdi
	movq	16(%rax), %rbx
	movq	%rcx, -3440(%rbp)
	movq	%r10, -3536(%rbp)
	movq	24(%rax), %rcx
	movq	72(%rax), %r10
	movq	80(%rax), %rax
	movq	%rsi, -3472(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rdx, -3488(%rbp)
	movl	$37, %edx
	movq	%rdi, -3520(%rbp)
	movq	%r15, %rdi
	movq	%r11, -3552(%rbp)
	movq	%r10, -3456(%rbp)
	movq	%rcx, -3424(%rbp)
	movq	%rax, -3568(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$2, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-3440(%rbp), %xmm0
	movq	%rbx, -2976(%rbp)
	pushq	-2976(%rbp)
	movhps	-3472(%rbp), %xmm0
	movaps	%xmm0, -2992(%rbp)
	pushq	-2984(%rbp)
	pushq	-2992(%rbp)
	movaps	%xmm0, -3472(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	-3424(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler16ToInteger_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_23ToIntegerTruncationModeE@PLT
	movq	%r14, %rdi
	movq	%rax, -3440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$36, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movdqa	-3472(%rbp), %xmm0
	movl	$96, %edi
	movq	$0, -2944(%rbp)
	movaps	%xmm0, -192(%rbp)
	movq	%rbx, %xmm0
	movhps	-3424(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-3488(%rbp), %xmm0
	movhps	-3504(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-3520(%rbp), %xmm0
	movhps	-3536(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-3552(%rbp), %xmm0
	movhps	-3456(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-3568(%rbp), %xmm0
	movhps	-3440(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2960(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm6
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm4
	movdqa	-128(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm5
	leaq	96(%rax), %rdx
	leaq	-2496(%rbp), %rdi
	movdqa	-160(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm2
	movups	%xmm6, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm3, 64(%rax)
	movups	%xmm5, 80(%rax)
	movq	%rax, -2960(%rbp)
	movq	%rdx, -2944(%rbp)
	movq	%rdx, -2952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2960(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L323
	call	_ZdlPv@PLT
.L323:
	movq	-3312(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2432(%rbp)
	je	.L324
.L620:
	movq	-3312(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-2496(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$12, %edi
	movq	$0, -2944(%rbp)
	movaps	%xmm0, -2960(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movabsq	$578720283176011013, %rcx
	movq	%rcx, (%rax)
	leaq	12(%rax), %rdx
	movl	$134744072, 8(%rax)
	movq	%rax, -2960(%rbp)
	movq	%rdx, -2944(%rbp)
	movq	%rdx, -2952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2960(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L325
	call	_ZdlPv@PLT
.L325:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	40(%rax), %rdx
	movq	24(%rax), %rbx
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	%rdx, -3576(%rbp)
	movq	56(%rax), %rdx
	movq	%rbx, -3456(%rbp)
	movq	32(%rax), %rbx
	movq	%rsi, -3536(%rbp)
	movq	16(%rax), %rsi
	movq	%rdx, -3584(%rbp)
	movq	72(%rax), %rdx
	movq	64(%rax), %r12
	movq	%rcx, -3520(%rbp)
	movq	%rbx, -3568(%rbp)
	movq	80(%rax), %rcx
	movq	48(%rax), %rbx
	movq	88(%rax), %rax
	movq	%rsi, -3552(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rdx, -3600(%rbp)
	movl	$42, %edx
	movq	%rcx, -3424(%rbp)
	movq	%rax, -3472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3472(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal24ConvertToRelativeIndex_1EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_
	movl	$45, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -3488(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3424(%rbp), %rdx
	movq	-3488(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberSubENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r14, %rdi
	movq	%rax, -3504(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberSubENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r14, %rdi
	movq	%rax, -3440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3504(%rbp), %r8
	movq	-3440(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal6Min_80EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	movl	$48, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -3608(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$50, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -3440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3424(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal19NumberIsLessThan_75EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	movq	-3440(%rbp), %rcx
	movq	%r12, %xmm3
	movq	-3488(%rbp), %xmm5
	movq	-3424(%rbp), %xmm2
	movq	%rbx, %xmm6
	movq	-3568(%rbp), %xmm7
	movhps	-3600(%rbp), %xmm3
	movhps	-3608(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movl	$128, %edi
	movq	-3552(%rbp), %xmm4
	movq	-3520(%rbp), %xmm1
	movhps	-3472(%rbp), %xmm2
	movq	%rcx, -80(%rbp)
	movhps	-3584(%rbp), %xmm6
	movhps	-3576(%rbp), %xmm7
	movhps	-3456(%rbp), %xmm4
	movaps	%xmm5, -3488(%rbp)
	movhps	-3536(%rbp), %xmm1
	movaps	%xmm2, -3424(%rbp)
	movaps	%xmm3, -3600(%rbp)
	movaps	%xmm6, -3472(%rbp)
	movaps	%xmm7, -3568(%rbp)
	movaps	%xmm4, -3552(%rbp)
	movaps	%xmm1, -3520(%rbp)
	movaps	%xmm1, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm0, -2960(%rbp)
	movq	%rax, -3504(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -2944(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm6
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm2
	leaq	128(%rax), %rdx
	leaq	-2304(%rbp), %rdi
	movdqa	-128(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm5
	movups	%xmm6, (%rax)
	movups	%xmm4, 16(%rax)
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm4
	movups	%xmm7, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm3, 64(%rax)
	movups	%xmm5, 80(%rax)
	movups	%xmm6, 96(%rax)
	movups	%xmm4, 112(%rax)
	movq	%rax, -2960(%rbp)
	movq	%rdx, -2944(%rbp)
	movq	%rdx, -2952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2960(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L326
	call	_ZdlPv@PLT
.L326:
	movdqa	-3520(%rbp), %xmm7
	movdqa	-3552(%rbp), %xmm2
	movl	$128, %edi
	movq	$0, -2944(%rbp)
	movdqa	-3568(%rbp), %xmm3
	movdqa	-3472(%rbp), %xmm5
	movq	-3440(%rbp), %xmm0
	movdqa	-3600(%rbp), %xmm6
	movaps	%xmm7, -192(%rbp)
	movdqa	-3424(%rbp), %xmm4
	movdqa	-3488(%rbp), %xmm7
	movaps	%xmm2, -176(%rbp)
	movhps	-3504(%rbp), %xmm0
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm0, -2960(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm2
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm3
	movdqa	-160(%rbp), %xmm5
	movdqa	-144(%rbp), %xmm6
	leaq	128(%rax), %rdx
	leaq	-2112(%rbp), %rdi
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm7
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm7, 80(%rax)
	movups	%xmm2, 96(%rax)
	movups	%xmm3, 112(%rax)
	movq	%rax, -2960(%rbp)
	movq	%rdx, -2944(%rbp)
	movq	%rdx, -2952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2960(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L327
	call	_ZdlPv@PLT
.L327:
	movq	-3368(%rbp), %rcx
	movq	-3360(%rbp), %rdx
	movq	%r15, %rdi
	movq	-3504(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2240(%rbp)
	je	.L328
.L621:
	movq	-3360(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$290491006552901640, %rbx
	leaq	-2304(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	$0, -2944(%rbp)
	movaps	%xmm0, -2960(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movabsq	$578720283176011013, %rcx
	movq	%rbx, 8(%rax)
	leaq	16(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%rax, -2960(%rbp)
	movq	%rdx, -2944(%rbp)
	movq	%rdx, -2952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2960(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L329
	call	_ZdlPv@PLT
.L329:
	movq	(%rbx), %rax
	movq	56(%rax), %rdi
	movq	24(%rax), %rbx
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	40(%rax), %rdx
	movq	112(%rax), %r11
	movq	%rdi, -3456(%rbp)
	movq	72(%rax), %rdi
	movq	80(%rax), %r12
	movq	%rbx, -3504(%rbp)
	movq	32(%rax), %rbx
	movq	%rsi, -3472(%rbp)
	movq	%rdi, -3568(%rbp)
	movq	88(%rax), %rdi
	movq	16(%rax), %rsi
	movq	%rcx, -3440(%rbp)
	movq	%rdi, -3576(%rbp)
	movq	96(%rax), %rdi
	movq	104(%rax), %rcx
	movq	%rbx, -3520(%rbp)
	movq	%rdx, -3536(%rbp)
	movq	64(%rax), %rbx
	movq	48(%rax), %rdx
	movq	120(%rax), %rax
	movq	%rsi, -3488(%rbp)
	movq	%r13, %rsi
	movq	%rdi, -3584(%rbp)
	movq	%r14, %rdi
	movq	%rcx, -3424(%rbp)
	movq	%r11, -3600(%rbp)
	movq	%rdx, -3552(%rbp)
	movq	%rax, -3608(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3424(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberAddENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r14, %rdi
	movq	%rax, -3616(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3616(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal19NumberIsLessThan_75EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	movl	$136, %edi
	movq	-3440(%rbp), %xmm0
	movq	$0, -2944(%rbp)
	movq	%rax, -64(%rbp)
	movhps	-3472(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-3488(%rbp), %xmm0
	movhps	-3504(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-3520(%rbp), %xmm0
	movhps	-3536(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-3552(%rbp), %xmm0
	movhps	-3456(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	movhps	-3568(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%r12, %xmm0
	movhps	-3576(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-3584(%rbp), %xmm0
	movhps	-3424(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-3600(%rbp), %xmm0
	movhps	-3608(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2960(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm6
	movq	-64(%rbp), %rcx
	movdqa	-160(%rbp), %xmm4
	leaq	136(%rax), %rdx
	leaq	-1920(%rbp), %rdi
	movdqa	-144(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm2
	movups	%xmm5, (%rax)
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm5
	movups	%xmm6, 16(%rax)
	movq	%rcx, 128(%rax)
	movdqa	-80(%rbp), %xmm6
	movups	%xmm4, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm2, 64(%rax)
	movups	%xmm3, 80(%rax)
	movups	%xmm5, 96(%rax)
	movups	%xmm6, 112(%rax)
	movq	%rax, -2960(%rbp)
	movq	%rdx, -2944(%rbp)
	movq	%rdx, -2952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2960(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L330
	call	_ZdlPv@PLT
.L330:
	movq	-3328(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2048(%rbp)
	je	.L331
.L622:
	movq	-3368(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$290491006552901640, %rbx
	leaq	-2112(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	$0, -2944(%rbp)
	movaps	%xmm0, -2960(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movabsq	$578720283176011013, %rcx
	movq	%rbx, 8(%rax)
	leaq	16(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%rax, -2960(%rbp)
	movq	%rdx, -2944(%rbp)
	movq	%rdx, -2952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2960(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L332
	call	_ZdlPv@PLT
.L332:
	movq	(%rbx), %rax
	movq	40(%rax), %rdx
	movq	24(%rax), %rbx
	movq	8(%rax), %rsi
	movq	88(%rax), %rdi
	movq	(%rax), %rcx
	movq	104(%rax), %r10
	movq	%rdx, -3520(%rbp)
	movq	56(%rax), %rdx
	movq	%rbx, -3488(%rbp)
	movq	32(%rax), %rbx
	movq	112(%rax), %r11
	movq	%rsi, -3440(%rbp)
	movq	16(%rax), %rsi
	movq	48(%rax), %r12
	movq	%rdx, -3536(%rbp)
	movq	72(%rax), %rdx
	movq	%rdi, -3568(%rbp)
	movq	96(%rax), %rdi
	movq	%rbx, -3504(%rbp)
	movq	%rdx, -3552(%rbp)
	movq	64(%rax), %rbx
	movq	80(%rax), %rdx
	movq	120(%rax), %rax
	movq	%rsi, -3472(%rbp)
	xorl	%esi, %esi
	movq	%rdi, -3576(%rbp)
	movq	%r13, %rdi
	movq	%rcx, -3424(%rbp)
	movq	%rdx, -3456(%rbp)
	movq	%r10, -3584(%rbp)
	movq	%r11, -3600(%rbp)
	movq	%rax, -3608(%rbp)
	call	_ZN2v88internal42FromConstexpr6ATbool16ATconstexpr_bool_165EPNS0_8compiler18CodeAssemblerStateEb@PLT
	movl	$136, %edi
	movq	-3424(%rbp), %xmm0
	movq	$0, -2944(%rbp)
	movq	%rax, -64(%rbp)
	movhps	-3440(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-3472(%rbp), %xmm0
	movhps	-3488(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-3504(%rbp), %xmm0
	movhps	-3520(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%r12, %xmm0
	movhps	-3536(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	movhps	-3552(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-3456(%rbp), %xmm0
	movhps	-3568(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-3576(%rbp), %xmm0
	movhps	-3584(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-3600(%rbp), %xmm0
	movhps	-3608(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2960(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm4
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm7
	movq	-64(%rbp), %rcx
	movdqa	-160(%rbp), %xmm2
	leaq	136(%rax), %rdx
	leaq	-1920(%rbp), %rdi
	movdqa	-144(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm5
	movups	%xmm4, (%rax)
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm4
	movups	%xmm7, 16(%rax)
	movq	%rcx, 128(%rax)
	movdqa	-80(%rbp), %xmm7
	movups	%xmm2, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm5, 64(%rax)
	movups	%xmm6, 80(%rax)
	movups	%xmm4, 96(%rax)
	movups	%xmm7, 112(%rax)
	movq	%rax, -2960(%rbp)
	movq	%rdx, -2944(%rbp)
	movq	%rdx, -2952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2960(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L333
	call	_ZdlPv@PLT
.L333:
	movq	-3328(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1856(%rbp)
	je	.L334
.L623:
	movq	-3328(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-1920(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$17, %edi
	movq	$0, -2944(%rbp)
	movaps	%xmm0, -2960(%rbp)
	call	_Znwm@PLT
	movdqa	.LC2(%rip), %xmm0
	movq	%r12, %rdi
	movq	%r14, %rsi
	movb	$4, 16(%rax)
	leaq	17(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -2960(%rbp)
	movq	%rdx, -2944(%rbp)
	movq	%rdx, -2952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2960(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L335
	call	_ZdlPv@PLT
.L335:
	movq	(%rbx), %rax
	movl	$120, %edi
	movq	(%rax), %rcx
	movdqu	16(%rax), %xmm5
	movdqu	32(%rax), %xmm4
	movdqu	48(%rax), %xmm3
	movq	%rcx, -3424(%rbp)
	movq	8(%rax), %rcx
	movdqu	64(%rax), %xmm2
	movdqu	80(%rax), %xmm1
	movq	%rcx, -3440(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -3472(%rbp)
	movq	24(%rax), %rcx
	movq	%rcx, -3488(%rbp)
	movq	32(%rax), %rcx
	movq	%rcx, -3504(%rbp)
	movq	40(%rax), %rcx
	movq	%rcx, -3520(%rbp)
	movq	48(%rax), %rcx
	movq	%rcx, -3536(%rbp)
	movq	56(%rax), %rcx
	movq	%rcx, -3552(%rbp)
	movq	64(%rax), %rcx
	movq	%rcx, -3456(%rbp)
	movq	72(%rax), %rcx
	movq	%rcx, -3568(%rbp)
	movq	80(%rax), %rcx
	movq	%rcx, -3576(%rbp)
	movq	88(%rax), %rcx
	movq	%rcx, -3584(%rbp)
	movq	96(%rax), %rcx
	movq	%rcx, -3600(%rbp)
	movdqu	96(%rax), %xmm0
	movq	104(%rax), %rcx
	movdqu	(%rax), %xmm6
	movq	112(%rax), %rbx
	movq	128(%rax), %r12
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rcx, -3608(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -2960(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -2944(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm2
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm3
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm7
	leaq	120(%rax), %rdx
	leaq	-1728(%rbp), %rdi
	movdqa	-160(%rbp), %xmm5
	movdqa	-144(%rbp), %xmm4
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movq	%rcx, 112(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm2, 80(%rax)
	movups	%xmm3, 96(%rax)
	movq	%rax, -2960(%rbp)
	movq	%rdx, -2944(%rbp)
	movq	%rdx, -2952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2960(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L336
	call	_ZdlPv@PLT
.L336:
	movq	-3424(%rbp), %xmm0
	movl	$120, %edi
	movq	%rbx, -80(%rbp)
	movq	$0, -2944(%rbp)
	movhps	-3440(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-3472(%rbp), %xmm0
	movhps	-3488(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-3504(%rbp), %xmm0
	movhps	-3520(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-3536(%rbp), %xmm0
	movhps	-3552(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-3456(%rbp), %xmm0
	movhps	-3568(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-3576(%rbp), %xmm0
	movhps	-3584(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-3600(%rbp), %xmm0
	movhps	-3608(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2960(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm5
	movq	%r14, %rsi
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm2
	movdqa	-112(%rbp), %xmm3
	leaq	120(%rax), %rdx
	leaq	-1536(%rbp), %rdi
	movdqa	-176(%rbp), %xmm6
	movdqa	-160(%rbp), %xmm4
	movups	%xmm5, (%rax)
	movdqa	-144(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm5
	movq	%rcx, 112(%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm2, 64(%rax)
	movups	%xmm3, 80(%rax)
	movups	%xmm5, 96(%rax)
	movq	%rax, -2960(%rbp)
	movq	%rdx, -2944(%rbp)
	movq	%rdx, -2952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2960(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L337
	call	_ZdlPv@PLT
.L337:
	movq	-3320(%rbp), %rcx
	movq	-3376(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1664(%rbp)
	je	.L338
.L624:
	movq	-3376(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -3256(%rbp)
	leaq	-1728(%rbp), %r12
	movq	$0, -3248(%rbp)
	movq	$0, -3240(%rbp)
	movq	$0, -3232(%rbp)
	movq	$0, -3224(%rbp)
	movq	$0, -3216(%rbp)
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	$0, -3192(%rbp)
	movq	$0, -3184(%rbp)
	movq	$0, -3176(%rbp)
	movq	$0, -3168(%rbp)
	movq	$0, -3160(%rbp)
	movq	$0, -3152(%rbp)
	movq	$0, -3136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3136(%rbp), %rax
	movq	%r12, %rdi
	leaq	-3240(%rbp), %rcx
	pushq	%rax
	leaq	-3152(%rbp), %rax
	leaq	-3224(%rbp), %r9
	pushq	%rax
	leaq	-3160(%rbp), %rax
	leaq	-3232(%rbp), %r8
	pushq	%rax
	leaq	-3168(%rbp), %rax
	leaq	-3248(%rbp), %rdx
	pushq	%rax
	leaq	-3176(%rbp), %rax
	leaq	-3256(%rbp), %rsi
	pushq	%rax
	leaq	-3184(%rbp), %rax
	pushq	%rax
	leaq	-3192(%rbp), %rax
	pushq	%rax
	leaq	-3200(%rbp), %rax
	pushq	%rax
	leaq	-3208(%rbp), %rax
	pushq	%rax
	leaq	-3216(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEESB_SB_SB_SB_SB_SB_SB_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESP_SP_SP_SP_SP_SP_SP_SP_
	addq	$80, %rsp
	movl	$52, %edx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$-1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$55, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3152(%rbp), %rdx
	movq	-3176(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberAddENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -3424(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3424(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberSubENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$58, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3152(%rbp), %rdx
	movq	-3192(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberAddENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r14, %rdi
	movq	%rax, -3440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -3424(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3440(%rbp), %r8
	movq	-3424(%rbp), %rdx
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler9NumberSubENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r14, %rdi
	movq	%rax, -3424(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$50, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3424(%rbp), %rax
	movl	$120, %edi
	movq	-3208(%rbp), %xmm0
	movq	-3224(%rbp), %xmm1
	movq	%r12, -112(%rbp)
	movq	%rax, -128(%rbp)
	movq	-3184(%rbp), %rax
	movhps	-3200(%rbp), %xmm0
	movq	-3240(%rbp), %xmm2
	movhps	-3216(%rbp), %xmm1
	movq	%rbx, -80(%rbp)
	movq	-3256(%rbp), %xmm3
	movq	%rax, -120(%rbp)
	movq	-3168(%rbp), %rax
	movhps	-3232(%rbp), %xmm2
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3248(%rbp), %xmm3
	movaps	%xmm2, -176(%rbp)
	movq	%rax, -104(%rbp)
	movq	-3160(%rbp), %rax
	movaps	%xmm3, -192(%rbp)
	movq	%rax, -96(%rbp)
	movq	-3152(%rbp), %rax
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -2960(%rbp)
	movq	%rax, -88(%rbp)
	movq	$0, -2944(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm6
	movq	%r14, %rsi
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm5
	leaq	120(%rax), %rdx
	leaq	-1536(%rbp), %rdi
	movdqa	-176(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm7
	movups	%xmm6, (%rax)
	movdqa	-144(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm6
	movq	%rcx, 112(%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm3, 64(%rax)
	movups	%xmm5, 80(%rax)
	movups	%xmm6, 96(%rax)
	movq	%rax, -2960(%rbp)
	movq	%rdx, -2944(%rbp)
	movq	%rdx, -2952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2960(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L339
	call	_ZdlPv@PLT
.L339:
	movq	-3320(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1472(%rbp)
	je	.L340
.L625:
	movq	-3320(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -3256(%rbp)
	leaq	-1536(%rbp), %r12
	movq	$0, -3248(%rbp)
	movq	$0, -3240(%rbp)
	movq	$0, -3232(%rbp)
	movq	$0, -3224(%rbp)
	movq	$0, -3216(%rbp)
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	$0, -3192(%rbp)
	movq	$0, -3184(%rbp)
	movq	$0, -3176(%rbp)
	movq	$0, -3168(%rbp)
	movq	$0, -3160(%rbp)
	movq	$0, -3152(%rbp)
	movq	$0, -3136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3136(%rbp), %rax
	movq	%r12, %rdi
	leaq	-3240(%rbp), %rcx
	pushq	%rax
	leaq	-3152(%rbp), %rax
	leaq	-3224(%rbp), %r9
	pushq	%rax
	leaq	-3160(%rbp), %rax
	leaq	-3232(%rbp), %r8
	pushq	%rax
	leaq	-3168(%rbp), %rax
	leaq	-3248(%rbp), %rdx
	pushq	%rax
	leaq	-3176(%rbp), %rax
	leaq	-3256(%rbp), %rsi
	pushq	%rax
	leaq	-3184(%rbp), %rax
	pushq	%rax
	leaq	-3192(%rbp), %rax
	pushq	%rax
	leaq	-3200(%rbp), %rax
	pushq	%rax
	leaq	-3208(%rbp), %rax
	pushq	%rax
	leaq	-3216(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEESB_SB_SB_SB_SB_SB_SB_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESP_SP_SP_SP_SP_SP_SP_SP_
	addq	$80, %rsp
	movl	$62, %edx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3256(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$120, %edi
	movaps	%xmm0, -2960(%rbp)
	movq	%rax, -192(%rbp)
	movq	-3248(%rbp), %rax
	movq	$0, -2944(%rbp)
	movq	%rax, -184(%rbp)
	movq	-3240(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-3232(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-3224(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-3216(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-3208(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-3200(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-3192(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-3184(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-3176(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-3168(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-3160(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-3152(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-3136(%rbp), %rax
	movq	%rax, -80(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm4
	movq	%r14, %rsi
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm6
	leaq	120(%rax), %rdx
	leaq	-1344(%rbp), %rdi
	movdqa	-176(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm2
	movups	%xmm4, (%rax)
	movdqa	-144(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm4
	movq	%rcx, 112(%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm5, 64(%rax)
	movups	%xmm6, 80(%rax)
	movups	%xmm4, 96(%rax)
	movq	%rax, -2960(%rbp)
	movq	%rdx, -2944(%rbp)
	movq	%rdx, -2952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2960(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L341
	call	_ZdlPv@PLT
.L341:
	movq	-3336(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1280(%rbp)
	je	.L342
.L626:
	movq	-3336(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-1344(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$15, %edi
	movq	$0, -2944(%rbp)
	movaps	%xmm0, -2960(%rbp)
	call	_Znwm@PLT
	movl	$2056, %edi
	movq	%r14, %rsi
	movabsq	$578720283176011013, %rcx
	movw	%di, 12(%rax)
	leaq	15(%rax), %rdx
	movq	%r12, %rdi
	movq	%rcx, (%rax)
	movl	$134744072, 8(%rax)
	movb	$8, 14(%rax)
	movq	%rax, -2960(%rbp)
	movq	%rdx, -2944(%rbp)
	movq	%rdx, -2952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2960(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L343
	call	_ZdlPv@PLT
.L343:
	movq	(%rbx), %rax
	movq	72(%rax), %r10
	movq	8(%rax), %rsi
	movq	56(%rax), %rdi
	movq	24(%rax), %rbx
	movq	40(%rax), %rdx
	movq	(%rax), %rcx
	movq	%r10, -3584(%rbp)
	movq	88(%rax), %r10
	movq	80(%rax), %r11
	movq	%rsi, -3504(%rbp)
	movq	16(%rax), %rsi
	movq	%rdi, -3576(%rbp)
	movq	64(%rax), %rdi
	movq	104(%rax), %r12
	movq	%rbx, -3536(%rbp)
	movq	%rdx, -3456(%rbp)
	movq	32(%rax), %rbx
	movq	48(%rax), %rdx
	movq	%r10, -3600(%rbp)
	movq	96(%rax), %r10
	movq	%rsi, -3520(%rbp)
	xorl	%esi, %esi
	movq	%rdi, -3424(%rbp)
	movq	%r13, %rdi
	movq	%rcx, -3488(%rbp)
	movq	%r11, -3440(%rbp)
	movq	%r10, -3472(%rbp)
	movq	%rbx, -3552(%rbp)
	movq	112(%rax), %rbx
	movq	%rdx, -3568(%rbp)
	movq	%r12, -3608(%rbp)
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal22NumberIsGreaterThan_77EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	pxor	%xmm0, %xmm0
	movl	$120, %edi
	movq	-3472(%rbp), %xmm5
	movq	-3440(%rbp), %xmm2
	movq	%rbx, -80(%rbp)
	movq	%rax, %r12
	movq	-3424(%rbp), %xmm3
	movhps	-3608(%rbp), %xmm5
	movq	-3568(%rbp), %xmm6
	movaps	%xmm0, -2960(%rbp)
	movq	-3552(%rbp), %xmm7
	movhps	-3600(%rbp), %xmm2
	movaps	%xmm5, -96(%rbp)
	movq	-3520(%rbp), %xmm4
	movq	-3488(%rbp), %xmm1
	movhps	-3584(%rbp), %xmm3
	movhps	-3576(%rbp), %xmm6
	movaps	%xmm5, -3472(%rbp)
	movhps	-3456(%rbp), %xmm7
	movhps	-3536(%rbp), %xmm4
	movaps	%xmm2, -3440(%rbp)
	movhps	-3504(%rbp), %xmm1
	movaps	%xmm3, -3424(%rbp)
	movaps	%xmm6, -3568(%rbp)
	movaps	%xmm7, -3552(%rbp)
	movaps	%xmm4, -3520(%rbp)
	movaps	%xmm1, -3488(%rbp)
	movaps	%xmm1, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movq	$0, -2944(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm7
	movq	%r14, %rsi
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm4
	leaq	120(%rax), %rdx
	leaq	-1152(%rbp), %rdi
	movdqa	-176(%rbp), %xmm2
	movdqa	-160(%rbp), %xmm3
	movups	%xmm7, (%rax)
	movdqa	-144(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm7
	movq	%rcx, 112(%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	movups	%xmm4, 80(%rax)
	movups	%xmm7, 96(%rax)
	movq	%rax, -2960(%rbp)
	movq	%rdx, -2944(%rbp)
	movq	%rdx, -2952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2960(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L344
	call	_ZdlPv@PLT
.L344:
	movdqa	-3488(%rbp), %xmm2
	movdqa	-3520(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movl	$120, %edi
	movdqa	-3552(%rbp), %xmm5
	movdqa	-3568(%rbp), %xmm6
	movaps	%xmm0, -2960(%rbp)
	movdqa	-3424(%rbp), %xmm4
	movdqa	-3440(%rbp), %xmm7
	movq	%rbx, -80(%rbp)
	movaps	%xmm2, -192(%rbp)
	movdqa	-3472(%rbp), %xmm2
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	$0, -2944(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm3
	movq	%r14, %rsi
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm2
	leaq	120(%rax), %rdx
	leaq	-384(%rbp), %rdi
	movdqa	-176(%rbp), %xmm5
	movdqa	-160(%rbp), %xmm6
	movups	%xmm3, (%rax)
	movdqa	-144(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm3
	movq	%rcx, 112(%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm2, 80(%rax)
	movups	%xmm3, 96(%rax)
	movq	%rax, -2960(%rbp)
	movq	%rdx, -2944(%rbp)
	movq	%rdx, -2952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2960(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L345
	call	_ZdlPv@PLT
.L345:
	movq	-3408(%rbp), %rcx
	movq	-3384(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1088(%rbp)
	je	.L346
.L627:
	movq	-3384(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -3272(%rbp)
	leaq	-1152(%rbp), %r12
	movq	$0, -3264(%rbp)
	movq	$0, -3256(%rbp)
	movq	$0, -3248(%rbp)
	movq	$0, -3240(%rbp)
	movq	$0, -3232(%rbp)
	movq	$0, -3224(%rbp)
	movq	$0, -3216(%rbp)
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	$0, -3192(%rbp)
	movq	$0, -3184(%rbp)
	movq	$0, -3176(%rbp)
	movq	$0, -3168(%rbp)
	movq	$0, -3160(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3160(%rbp), %rax
	movq	%r12, %rdi
	leaq	-3256(%rbp), %rcx
	pushq	%rax
	leaq	-3168(%rbp), %rax
	leaq	-3248(%rbp), %r8
	pushq	%rax
	leaq	-3176(%rbp), %rax
	leaq	-3240(%rbp), %r9
	pushq	%rax
	leaq	-3184(%rbp), %rax
	leaq	-3264(%rbp), %rdx
	pushq	%rax
	leaq	-3192(%rbp), %rax
	leaq	-3272(%rbp), %rsi
	pushq	%rax
	leaq	-3200(%rbp), %rax
	leaq	-3152(%rbp), %r12
	pushq	%rax
	leaq	-3208(%rbp), %rax
	pushq	%rax
	leaq	-3216(%rbp), %rax
	pushq	%rax
	leaq	-3224(%rbp), %rax
	pushq	%rax
	leaq	-3232(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEESB_SB_SB_SB_SB_SB_SB_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESP_SP_SP_SP_SP_SP_SP_SP_
	addq	$80, %rsp
	movl	$66, %edx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3248(%rbp), %r9
	movq	%r12, %rdi
	movq	-3192(%rbp), %rax
	movq	-3232(%rbp), %rbx
	movq	%r9, -3440(%rbp)
	movq	%rax, -3424(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$159, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-2960(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	-192(%rbp), %rcx
	movl	$2, %ebx
	movq	%rax, %r8
	movq	-3440(%rbp), %r9
	pushq	%rbx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movhps	-3424(%rbp), %xmm0
	leaq	-3136(%rbp), %rdx
	pushq	%rcx
	movl	$1, %ecx
	movq	%rax, -3136(%rbp)
	movq	-2944(%rbp), %rax
	movaps	%xmm0, -192(%rbp)
	movq	%rax, -3128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$69, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3160(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	-3176(%rbp), %xmm5
	movq	-3192(%rbp), %xmm2
	movl	$128, %edi
	movq	-3208(%rbp), %xmm3
	movq	%rbx, -72(%rbp)
	movq	-3224(%rbp), %xmm6
	movhps	-3168(%rbp), %xmm5
	movq	-3240(%rbp), %xmm7
	movq	%rax, -3552(%rbp)
	movq	-3256(%rbp), %xmm4
	movhps	-3184(%rbp), %xmm2
	movaps	%xmm5, -96(%rbp)
	movq	-3272(%rbp), %xmm1
	movhps	-3200(%rbp), %xmm3
	movhps	-3216(%rbp), %xmm6
	movhps	-3232(%rbp), %xmm7
	movaps	%xmm5, -3536(%rbp)
	movhps	-3248(%rbp), %xmm4
	movhps	-3264(%rbp), %xmm1
	movaps	%xmm2, -3520(%rbp)
	movaps	%xmm3, -3504(%rbp)
	movaps	%xmm6, -3488(%rbp)
	movaps	%xmm7, -3472(%rbp)
	movaps	%xmm4, -3440(%rbp)
	movaps	%xmm1, -3424(%rbp)
	movaps	%xmm1, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -2960(%rbp)
	movq	$0, -2944(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm6
	movdqa	-160(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm7
	leaq	128(%rax), %rdx
	leaq	-960(%rbp), %rdi
	movdqa	-128(%rbp), %xmm2
	movdqa	-112(%rbp), %xmm3
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm6
	movups	%xmm4, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm2, 64(%rax)
	movups	%xmm3, 80(%rax)
	movups	%xmm5, 96(%rax)
	movups	%xmm6, 112(%rax)
	movq	%rax, -2960(%rbp)
	movq	%rdx, -2944(%rbp)
	movq	%rdx, -2952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2960(%rbp), %rdi
	popq	%rcx
	popq	%rsi
	testq	%rdi, %rdi
	je	.L347
	call	_ZdlPv@PLT
.L347:
	movdqa	-3440(%rbp), %xmm7
	movdqa	-3424(%rbp), %xmm4
	movl	$128, %edi
	movq	$0, -2944(%rbp)
	movdqa	-3472(%rbp), %xmm2
	movdqa	-3488(%rbp), %xmm3
	movq	-3552(%rbp), %xmm0
	movaps	%xmm7, -176(%rbp)
	movq	%rbx, %xmm7
	movdqa	-3504(%rbp), %xmm5
	movdqa	-3520(%rbp), %xmm6
	movaps	%xmm4, -192(%rbp)
	movdqa	-3536(%rbp), %xmm4
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm0, -2960(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm1
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm2
	movdqa	-160(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm5
	leaq	128(%rax), %rdx
	leaq	-768(%rbp), %rdi
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm4
	movups	%xmm1, (%rax)
	movdqa	-96(%rbp), %xmm7
	movdqa	-80(%rbp), %xmm1
	movups	%xmm2, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	movups	%xmm4, 80(%rax)
	movups	%xmm7, 96(%rax)
	movups	%xmm1, 112(%rax)
	movq	%rax, -2960(%rbp)
	movq	%rdx, -2944(%rbp)
	movq	%rdx, -2952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2960(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L348
	call	_ZdlPv@PLT
.L348:
	movq	-3400(%rbp), %rcx
	movq	-3392(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -896(%rbp)
	je	.L349
.L628:
	movq	-3392(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -3280(%rbp)
	leaq	-960(%rbp), %r12
	movq	$0, -3272(%rbp)
	movq	$0, -3264(%rbp)
	movq	$0, -3256(%rbp)
	movq	$0, -3248(%rbp)
	movq	$0, -3240(%rbp)
	movq	$0, -3232(%rbp)
	movq	$0, -3224(%rbp)
	movq	$0, -3216(%rbp)
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	$0, -3192(%rbp)
	movq	$0, -3184(%rbp)
	movq	$0, -3176(%rbp)
	movq	$0, -3168(%rbp)
	movq	$0, -3160(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-3160(%rbp), %rax
	pushq	%rax
	leaq	-3168(%rbp), %rax
	leaq	-3264(%rbp), %rcx
	pushq	%rax
	leaq	-3176(%rbp), %rax
	leaq	-3256(%rbp), %r8
	pushq	%rax
	leaq	-3184(%rbp), %rax
	leaq	-3248(%rbp), %r9
	pushq	%rax
	leaq	-3192(%rbp), %rax
	leaq	-3272(%rbp), %rdx
	pushq	%rax
	leaq	-3200(%rbp), %rax
	leaq	-3280(%rbp), %rsi
	pushq	%rax
	leaq	-3208(%rbp), %rax
	leaq	-3152(%rbp), %r12
	pushq	%rax
	leaq	-3216(%rbp), %rax
	pushq	%rax
	leaq	-3224(%rbp), %rax
	pushq	%rax
	leaq	-3232(%rbp), %rax
	pushq	%rax
	leaq	-3240(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEESB_SB_SB_SB_SB_SB_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EESG_PNSE_IS4_EEPNSE_IS5_EEPNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EESQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_PNSE_ISC_EE
	addq	$96, %rsp
	movl	$71, %edx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3256(%rbp), %r9
	movq	%r12, %rdi
	movq	-3200(%rbp), %rax
	movq	-3240(%rbp), %rbx
	movq	%r9, -3440(%rbp)
	movq	%rax, -3424(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-2960(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r8
	movq	-2944(%rbp), %rax
	movl	$2, %ebx
	leaq	-3136(%rbp), %r10
	pushq	%rbx
	movq	-3440(%rbp), %r9
	movq	%r10, %rdx
	movl	$1, %ecx
	movq	%rax, -3128(%rbp)
	leaq	-192(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r11
	movhps	-3424(%rbp), %xmm0
	pushq	%rax
	movq	%r11, -3136(%rbp)
	movq	%r10, -3504(%rbp)
	movaps	%xmm0, -192(%rbp)
	movq	%rax, -3472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$74, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3256(%rbp), %r9
	movq	-3240(%rbp), %rcx
	movq	%r12, %rdi
	movq	-3216(%rbp), %rsi
	movq	%r9, -3488(%rbp)
	movq	%rcx, -3424(%rbp)
	movq	%rsi, -3440(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$712, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-2960(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-3472(%rbp), %rcx
	xorl	%esi, %esi
	movq	%rbx, -176(%rbp)
	movl	$3, %ebx
	movq	-3504(%rbp), %r10
	movq	%rax, %r8
	movq	%r12, %rdi
	pushq	%rbx
	movq	-3488(%rbp), %r9
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r11
	pushq	%rcx
	movq	-2944(%rbp), %rax
	movq	%r10, %rdx
	movl	$1, %ecx
	movq	-3424(%rbp), %xmm0
	movq	%r11, -3136(%rbp)
	movq	%rax, -3128(%rbp)
	movhps	-3440(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$69, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$128, %edi
	movq	-3168(%rbp), %xmm0
	movq	-3184(%rbp), %xmm1
	movq	-3200(%rbp), %xmm2
	movq	-3216(%rbp), %xmm3
	movq	$0, -2944(%rbp)
	movq	-3232(%rbp), %xmm4
	movhps	-3160(%rbp), %xmm0
	movq	-3248(%rbp), %xmm5
	movhps	-3176(%rbp), %xmm1
	movq	-3264(%rbp), %xmm6
	movhps	-3192(%rbp), %xmm2
	movaps	%xmm0, -80(%rbp)
	movq	-3280(%rbp), %xmm7
	movhps	-3208(%rbp), %xmm3
	movhps	-3224(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -112(%rbp)
	movhps	-3240(%rbp), %xmm5
	movhps	-3256(%rbp), %xmm6
	movaps	%xmm3, -128(%rbp)
	movhps	-3272(%rbp), %xmm7
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -2960(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm2
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm3
	movdqa	-160(%rbp), %xmm5
	movdqa	-144(%rbp), %xmm6
	leaq	128(%rax), %rdx
	leaq	-576(%rbp), %rdi
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm7
	movups	%xmm2, (%rax)
	movdqa	-96(%rbp), %xmm1
	movdqa	-80(%rbp), %xmm2
	movups	%xmm3, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm7, 80(%rax)
	movups	%xmm1, 96(%rax)
	movups	%xmm2, 112(%rax)
	movq	%rax, -2960(%rbp)
	movq	%rdx, -2944(%rbp)
	movq	%rdx, -2952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2960(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L350
	call	_ZdlPv@PLT
.L350:
	movq	-3344(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -704(%rbp)
	je	.L351
.L629:
	movq	-3400(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -3280(%rbp)
	leaq	-768(%rbp), %r12
	movq	$0, -3272(%rbp)
	movq	$0, -3264(%rbp)
	movq	$0, -3256(%rbp)
	movq	$0, -3248(%rbp)
	movq	$0, -3240(%rbp)
	movq	$0, -3232(%rbp)
	movq	$0, -3224(%rbp)
	movq	$0, -3216(%rbp)
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	$0, -3192(%rbp)
	movq	$0, -3184(%rbp)
	movq	$0, -3176(%rbp)
	movq	$0, -3168(%rbp)
	movq	$0, -3160(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-3160(%rbp), %rax
	pushq	%rax
	leaq	-3168(%rbp), %rax
	leaq	-3256(%rbp), %r8
	pushq	%rax
	leaq	-3176(%rbp), %rax
	leaq	-3264(%rbp), %rcx
	pushq	%rax
	leaq	-3184(%rbp), %rax
	leaq	-3248(%rbp), %r9
	pushq	%rax
	leaq	-3192(%rbp), %rax
	leaq	-3272(%rbp), %rdx
	pushq	%rax
	leaq	-3200(%rbp), %rax
	leaq	-3280(%rbp), %rsi
	pushq	%rax
	leaq	-3208(%rbp), %rax
	leaq	-3152(%rbp), %r12
	pushq	%rax
	leaq	-3216(%rbp), %rax
	pushq	%rax
	leaq	-3224(%rbp), %rax
	pushq	%rax
	leaq	-3232(%rbp), %rax
	pushq	%rax
	leaq	-3240(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEESB_SB_SB_SB_SB_SB_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EESG_PNSE_IS4_EEPNSE_IS5_EEPNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EESQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_PNSE_ISC_EE
	addq	$96, %rsp
	movl	$77, %edx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal59FromConstexpr14ATLanguageMode24ATconstexpr_LanguageMode_166EPNS0_8compiler18CodeAssemblerStateENS0_12LanguageModeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3256(%rbp), %r9
	movq	-3216(%rbp), %rcx
	movq	%r12, %rdi
	movq	-3240(%rbp), %rax
	movq	%r9, -3472(%rbp)
	movq	%rcx, -3440(%rbp)
	movq	%rax, -3424(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$160, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-2960(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	xorl	%esi, %esi
	movq	%rbx, -176(%rbp)
	movq	%rcx, -3136(%rbp)
	movl	$3, %ebx
	movq	%rax, %r8
	movq	%r12, %rdi
	leaq	-192(%rbp), %rcx
	pushq	%rbx
	movq	-3424(%rbp), %xmm0
	leaq	-3136(%rbp), %rdx
	movq	-3472(%rbp), %r9
	movq	-2944(%rbp), %rax
	pushq	%rcx
	movl	$1, %ecx
	movhps	-3440(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	%rax, -3128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$69, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$128, %edi
	movq	-3168(%rbp), %xmm0
	movq	-3184(%rbp), %xmm1
	movq	-3200(%rbp), %xmm2
	movq	-3216(%rbp), %xmm3
	movq	$0, -2944(%rbp)
	movq	-3232(%rbp), %xmm4
	movhps	-3160(%rbp), %xmm0
	movq	-3248(%rbp), %xmm5
	movhps	-3176(%rbp), %xmm1
	movq	-3264(%rbp), %xmm6
	movhps	-3192(%rbp), %xmm2
	movaps	%xmm0, -80(%rbp)
	movq	-3280(%rbp), %xmm7
	movhps	-3208(%rbp), %xmm3
	movhps	-3224(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -112(%rbp)
	movhps	-3240(%rbp), %xmm5
	movhps	-3256(%rbp), %xmm6
	movaps	%xmm3, -128(%rbp)
	movhps	-3272(%rbp), %xmm7
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -2960(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm3
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm5
	movdqa	-160(%rbp), %xmm6
	movdqa	-144(%rbp), %xmm4
	leaq	128(%rax), %rdx
	leaq	-576(%rbp), %rdi
	movups	%xmm3, (%rax)
	movdqa	-128(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm1
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm1, 80(%rax)
	movups	%xmm2, 96(%rax)
	movups	%xmm3, 112(%rax)
	movq	%rax, -2960(%rbp)
	movq	%rdx, -2944(%rbp)
	movq	%rdx, -2952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2960(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L352
	call	_ZdlPv@PLT
.L352:
	movq	-3344(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -512(%rbp)
	je	.L353
.L630:
	movq	-3344(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -3264(%rbp)
	leaq	-576(%rbp), %r12
	movq	$0, -3256(%rbp)
	movq	$0, -3248(%rbp)
	movq	$0, -3240(%rbp)
	movq	$0, -3232(%rbp)
	movq	$0, -3224(%rbp)
	movq	$0, -3216(%rbp)
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	$0, -3192(%rbp)
	movq	$0, -3184(%rbp)
	movq	$0, -3176(%rbp)
	movq	$0, -3168(%rbp)
	movq	$0, -3160(%rbp)
	movq	$0, -3152(%rbp)
	movq	$0, -3136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-3136(%rbp), %rax
	pushq	%rax
	leaq	-3152(%rbp), %rax
	leaq	-3232(%rbp), %r9
	pushq	%rax
	leaq	-3160(%rbp), %rax
	leaq	-3240(%rbp), %r8
	pushq	%rax
	leaq	-3168(%rbp), %rax
	leaq	-3248(%rbp), %rcx
	pushq	%rax
	leaq	-3176(%rbp), %rax
	leaq	-3256(%rbp), %rdx
	pushq	%rax
	leaq	-3184(%rbp), %rax
	leaq	-3264(%rbp), %rsi
	pushq	%rax
	leaq	-3192(%rbp), %rax
	pushq	%rax
	leaq	-3200(%rbp), %rax
	pushq	%rax
	leaq	-3208(%rbp), %rax
	pushq	%rax
	leaq	-3216(%rbp), %rax
	pushq	%rax
	leaq	-3224(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEESB_SB_SB_SB_SB_SB_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EESG_PNSE_IS4_EEPNSE_IS5_EEPNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EESQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_PNSE_ISC_EE
	addq	$96, %rsp
	movl	$81, %edx
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3152(%rbp), %rdx
	movq	-3184(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberAddENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$84, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3152(%rbp), %rdx
	movq	-3200(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberAddENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r14, %rdi
	movq	%rax, -3424(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$87, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3160(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberSubENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$62, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3192(%rbp), %rax
	movq	-3424(%rbp), %rcx
	movl	$120, %edi
	movq	-3216(%rbp), %xmm0
	movq	%r12, -112(%rbp)
	movq	-3232(%rbp), %xmm1
	movq	%rax, -120(%rbp)
	movq	-3176(%rbp), %rax
	movhps	-3208(%rbp), %xmm0
	movq	%rcx, -128(%rbp)
	movq	-3248(%rbp), %xmm2
	movhps	-3224(%rbp), %xmm1
	movq	%rbx, -88(%rbp)
	movq	-3264(%rbp), %xmm3
	movq	%rax, -104(%rbp)
	movq	-3168(%rbp), %rax
	movhps	-3240(%rbp), %xmm2
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3256(%rbp), %xmm3
	movaps	%xmm2, -176(%rbp)
	movq	%rax, -96(%rbp)
	movq	-3152(%rbp), %rax
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -2960(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -2944(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm1
	movdqa	-112(%rbp), %xmm2
	leaq	120(%rax), %rdx
	leaq	-1344(%rbp), %rdi
	movdqa	-176(%rbp), %xmm6
	movdqa	-160(%rbp), %xmm4
	movq	%rcx, 112(%rax)
	movdqa	-144(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm3
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm1, 64(%rax)
	movups	%xmm2, 80(%rax)
	movups	%xmm3, 96(%rax)
	movq	%rax, -2960(%rbp)
	movq	%rdx, -2944(%rbp)
	movq	%rdx, -2952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2960(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L354
	call	_ZdlPv@PLT
.L354:
	movq	-3336(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -320(%rbp)
	je	.L355
.L631:
	movq	-3408(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -3248(%rbp)
	leaq	-384(%rbp), %r12
	movq	$0, -3240(%rbp)
	movq	$0, -3232(%rbp)
	movq	$0, -3224(%rbp)
	movq	$0, -3216(%rbp)
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	$0, -3192(%rbp)
	movq	$0, -3184(%rbp)
	movq	$0, -3176(%rbp)
	movq	$0, -3168(%rbp)
	movq	$0, -3160(%rbp)
	movq	$0, -3152(%rbp)
	movq	$0, -3136(%rbp)
	movq	$0, -2960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3136(%rbp), %rax
	pushq	%r14
	leaq	-3232(%rbp), %rcx
	pushq	%rax
	leaq	-3152(%rbp), %rax
	leaq	-3240(%rbp), %rdx
	movq	%r12, %rdi
	pushq	%rax
	leaq	-3160(%rbp), %rax
	leaq	-3248(%rbp), %rsi
	pushq	%rax
	leaq	-3168(%rbp), %rax
	leaq	-3216(%rbp), %r9
	pushq	%rax
	leaq	-3176(%rbp), %rax
	leaq	-3224(%rbp), %r8
	pushq	%rax
	leaq	-3184(%rbp), %rax
	pushq	%rax
	leaq	-3192(%rbp), %rax
	pushq	%rax
	leaq	-3200(%rbp), %rax
	pushq	%rax
	leaq	-3208(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEESB_SB_SB_SB_SB_SB_SB_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESP_SP_SP_SP_SP_SP_SP_SP_
	movq	%r15, %rdi
	addq	$80, %rsp
	movl	$91, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3208(%rbp), %rsi
	movq	-3448(%rbp), %rdi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	jmp	.L355
.L632:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22428:
	.size	_ZN2v88internal33ArrayPrototypeCopyWithinAssembler36GenerateArrayPrototypeCopyWithinImplEv, .-_ZN2v88internal33ArrayPrototypeCopyWithinAssembler36GenerateArrayPrototypeCopyWithinImplEv
	.section	.rodata._ZN2v88internal8Builtins33Generate_ArrayPrototypeCopyWithinEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/array-copywithin-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins33Generate_ArrayPrototypeCopyWithinEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"ArrayPrototypeCopyWithin"
	.section	.text._ZN2v88internal8Builtins33Generate_ArrayPrototypeCopyWithinEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins33Generate_ArrayPrototypeCopyWithinEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins33Generate_ArrayPrototypeCopyWithinEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins33Generate_ArrayPrototypeCopyWithinEPNS0_8compiler18CodeAssemblerStateE:
.LFB22424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$229, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$726, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L637
.L634:
	movq	%r13, %rdi
	call	_ZN2v88internal33ArrayPrototypeCopyWithinAssembler36GenerateArrayPrototypeCopyWithinImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L638
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L637:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L634
.L638:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22424:
	.size	_ZN2v88internal8Builtins33Generate_ArrayPrototypeCopyWithinEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins33Generate_ArrayPrototypeCopyWithinEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal24ConvertToRelativeIndex_1EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal24ConvertToRelativeIndex_1EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_, @function
_GLOBAL__sub_I__ZN2v88internal24ConvertToRelativeIndex_1EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_:
.LFB29015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29015:
	.size	_GLOBAL__sub_I__ZN2v88internal24ConvertToRelativeIndex_1EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_, .-_GLOBAL__sub_I__ZN2v88internal24ConvertToRelativeIndex_1EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal24ConvertToRelativeIndex_1EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.byte	5
	.byte	5
	.byte	5
	.byte	7
	.byte	8
	.byte	7
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	4
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
