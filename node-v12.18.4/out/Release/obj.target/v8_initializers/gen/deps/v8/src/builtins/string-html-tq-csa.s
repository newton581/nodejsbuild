	.file	"string-html-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.rodata._ZN2v88internal19CreateHTMLAssembler22GenerateCreateHTMLImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../../deps/v8/../../deps/v8/src/builtins/string-html.tq"
	.section	.rodata._ZN2v88internal19CreateHTMLAssembler22GenerateCreateHTMLImplEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	"<"
.LC3:
	.string	" "
.LC4:
	.string	"=\""
.LC5:
	.string	"\""
.LC6:
	.string	">"
.LC7:
	.string	"</"
	.section	.text._ZN2v88internal19CreateHTMLAssembler22GenerateCreateHTMLImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19CreateHTMLAssembler22GenerateCreateHTMLImplEv
	.type	_ZN2v88internal19CreateHTMLAssembler22GenerateCreateHTMLImplEv, @function
_ZN2v88internal19CreateHTMLAssembler22GenerateCreateHTMLImplEv:
.LFB22418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$5, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-736(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-704(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-264(%rbp), %rbx
	subq	$840, %rsp
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -744(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, -784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -800(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, -808(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, -832(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	leaq	-744(%rbp), %r12
	movq	%rax, -816(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$144, %edi
	movq	$0, -696(%rbp)
	movq	%rax, -848(%rbp)
	movq	-744(%rbp), %rax
	movq	$0, -688(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -696(%rbp)
	leaq	-648(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -680(%rbp)
	movq	%rdx, -688(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -664(%rbp)
	movq	%rax, -760(%rbp)
	movq	$0, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-744(%rbp), %rax
	movl	$192, %edi
	movq	$0, -504(%rbp)
	movq	$0, -496(%rbp)
	movq	%rax, -512(%rbp)
	movq	$0, -488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -504(%rbp)
	leaq	-456(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -488(%rbp)
	movq	%rdx, -496(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -472(%rbp)
	movq	%rax, -768(%rbp)
	movq	$0, -480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-744(%rbp), %rax
	movl	$192, %edi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	xorl	%edx, %edx
	movq	%rax, -312(%rbp)
	movups	%xmm0, -280(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movq	-784(%rbp), %xmm1
	movaps	%xmm0, -736(%rbp)
	movhps	-800(%rbp), %xmm1
	movq	$0, -720(%rbp)
	movaps	%xmm1, -128(%rbp)
	movq	-808(%rbp), %xmm1
	movhps	-832(%rbp), %xmm1
	movaps	%xmm1, -112(%rbp)
	movq	-816(%rbp), %xmm1
	movhps	-848(%rbp), %xmm1
	movaps	%xmm1, -96(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm3
	movq	%r13, %rdi
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	leaq	48(%rax), %rdx
	movq	%rax, -736(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rdx, -720(%rbp)
	movq	%rdx, -728(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-736(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L8
	call	_ZdlPv@PLT
.L8:
	movq	-760(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -640(%rbp)
	jne	.L95
	cmpq	$0, -448(%rbp)
	jne	.L96
.L13:
	cmpq	$0, -256(%rbp)
	jne	.L97
.L17:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L23
	call	_ZdlPv@PLT
.L23:
	movq	-304(%rbp), %rbx
	movq	-312(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L24
	.p2align 4,,10
	.p2align 3
.L28:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L25
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L28
.L26:
	movq	-312(%rbp), %r13
.L24:
	testq	%r13, %r13
	je	.L29
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L29:
	movq	-768(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L30
	call	_ZdlPv@PLT
.L30:
	movq	-496(%rbp), %rbx
	movq	-504(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L31
	.p2align 4,,10
	.p2align 3
.L35:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L32
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L35
.L33:
	movq	-504(%rbp), %r13
.L31:
	testq	%r13, %r13
	je	.L36
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L36:
	movq	-760(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L37
	call	_ZdlPv@PLT
.L37:
	movq	-688(%rbp), %rbx
	movq	-696(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L38
	.p2align 4,,10
	.p2align 3
.L42:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L39
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L42
.L40:
	movq	-696(%rbp), %r13
.L38:
	testq	%r13, %r13
	je	.L43
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L43:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L98
	addq	$840, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L42
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L25:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L28
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L32:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L35
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L95:
	movq	-760(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -720(%rbp)
	movaps	%xmm0, -736(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	$117901319, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -736(%rbp)
	movq	%rdx, -720(%rbp)
	movq	%rdx, -728(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-736(%rbp), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	movq	0(%r13), %rax
	movl	$12, %edx
	movq	16(%rax), %rsi
	movq	24(%rax), %rdi
	movq	32(%rax), %r10
	movq	(%rax), %r13
	movq	8(%rax), %rcx
	movq	40(%rax), %rax
	movq	%rsi, -800(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rdi, -808(%rbp)
	movq	%r12, %rdi
	movq	%r10, -832(%rbp)
	movq	%rcx, -784(%rbp)
	movq	%rax, -848(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-800(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-784(%rbp), %rdx
	call	_ZN2v88internal17CodeStubAssembler12ToThisStringENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_6StringEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -872(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$13, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	movq	-808(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	movq	%r13, -816(%rbp)
	call	_ZN2v88internal12StringAdd_82EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6StringEEES8_@PLT
	movl	$14, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -864(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal15kEmptyString_67EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-832(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	-832(%rbp), %xmm7
	movq	-800(%rbp), %xmm2
	movaps	%xmm0, -736(%rbp)
	movq	-872(%rbp), %xmm6
	movq	-816(%rbp), %xmm3
	movhps	-848(%rbp), %xmm7
	movq	$0, -720(%rbp)
	movhps	-864(%rbp), %xmm6
	movhps	-808(%rbp), %xmm2
	movaps	%xmm7, -832(%rbp)
	movhps	-784(%rbp), %xmm3
	movaps	%xmm6, -864(%rbp)
	movaps	%xmm2, -800(%rbp)
	movaps	%xmm3, -784(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm6, -80(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm5
	movq	%r14, %rsi
	leaq	64(%rax), %rdx
	leaq	-512(%rbp), %rdi
	movq	%rax, -736(%rbp)
	movups	%xmm4, (%rax)
	movdqa	-96(%rbp), %xmm4
	movups	%xmm5, 16(%rax)
	movdqa	-80(%rbp), %xmm5
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rdx, -720(%rbp)
	movq	%rdx, -728(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-736(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L11
	call	_ZdlPv@PLT
.L11:
	movdqa	-784(%rbp), %xmm6
	movdqa	-800(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movdqa	-832(%rbp), %xmm4
	movdqa	-864(%rbp), %xmm5
	movaps	%xmm0, -736(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movq	$0, -720(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	leaq	64(%rax), %rdx
	leaq	-320(%rbp), %rdi
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rax, -736(%rbp)
	movq	%rdx, -720(%rbp)
	movq	%rdx, -728(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-736(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L12
	call	_ZdlPv@PLT
.L12:
	movq	-768(%rbp), %rdx
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -448(%rbp)
	je	.L13
.L96:
	movq	-768(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -784(%rbp)
	leaq	-512(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -720(%rbp)
	movaps	%xmm0, -736(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movabsq	$506382309378164743, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -736(%rbp)
	movq	%rdx, -720(%rbp)
	movq	%rdx, -728(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-736(%rbp), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L14
	call	_ZdlPv@PLT
.L14:
	movq	0(%r13), %rax
	movq	%r12, %rdi
	movq	40(%rax), %rcx
	movq	56(%rax), %rdx
	movq	32(%rax), %r9
	movq	(%rax), %r13
	movq	%rcx, -800(%rbp)
	testq	%rdx, %rdx
	movq	48(%rax), %rsi
	movq	16(%rax), %xmm0
	movq	%r9, %xmm1
	cmove	-784(%rbp), %rdx
	movq	%r13, %xmm2
	movhps	8(%rax), %xmm2
	movq	%rsi, -808(%rbp)
	leaq	.LC1(%rip), %rsi
	movhps	24(%rax), %xmm0
	movhps	-800(%rbp), %xmm1
	movq	%rdx, -784(%rbp)
	movl	$16, %edx
	movaps	%xmm2, -864(%rbp)
	movaps	%xmm0, -848(%rbp)
	movaps	%xmm1, -832(%rbp)
	movq	%r9, -816(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-800(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ToString_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -800(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	leaq	-128(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	-800(%rbp), %rax
	movl	$1, %r8d
	movl	$329, %esi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r14, %rdi
	movq	%rax, -800(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$15, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$17, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	movq	-784(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal12StringAdd_82EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6StringEEES8_@PLT
	movq	-816(%rbp), %r9
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	movq	%r9, %rcx
	call	_ZN2v88internal12StringAdd_82EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6StringEEES8_@PLT
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -784(%rbp)
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	movq	-784(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal12StringAdd_82EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6StringEEES8_@PLT
	movq	-800(%rbp), %r8
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	movq	%r8, %rcx
	call	_ZN2v88internal12StringAdd_82EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6StringEEES8_@PLT
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -784(%rbp)
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	movq	-784(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal12StringAdd_82EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6StringEEES8_@PLT
	movl	$14, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movdqa	-848(%rbp), %xmm0
	movl	$64, %edi
	movdqa	-864(%rbp), %xmm2
	movdqa	-832(%rbp), %xmm1
	movq	$0, -720(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	-808(%rbp), %xmm0
	movaps	%xmm2, -128(%rbp)
	movhps	-784(%rbp), %xmm0
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -736(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	leaq	64(%rax), %rdx
	leaq	-320(%rbp), %rdi
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rax, -736(%rbp)
	movq	%rdx, -720(%rbp)
	movq	%rdx, -728(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-736(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L16
	call	_ZdlPv@PLT
.L16:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -256(%rbp)
	je	.L17
.L97:
	leaq	-320(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	movq	%r8, -832(%rbp)
	movq	$0, -784(%rbp)
	movq	$0, -800(%rbp)
	movq	$0, -808(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -720(%rbp)
	movaps	%xmm0, -736(%rbp)
	call	_Znwm@PLT
	movq	-832(%rbp), %r8
	movq	%r14, %rsi
	movabsq	$506382309378164743, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%r8, %rdi
	movq	%rax, -736(%rbp)
	movq	%rdx, -720(%rbp)
	movq	%rdx, -728(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-736(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L18
	movq	%rax, -832(%rbp)
	call	_ZdlPv@PLT
	movq	-832(%rbp), %rax
.L18:
	movq	(%rax), %rax
	movq	%r12, %rdi
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	cmovne	%rdx, %r13
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	cmove	-784(%rbp), %rdx
	movq	%rdx, -784(%rbp)
	movq	48(%rax), %rdx
	movq	56(%rax), %rax
	testq	%rdx, %rdx
	movq	%rdx, %rsi
	cmove	-800(%rbp), %rsi
	movl	$20, %edx
	testq	%rax, %rax
	cmove	-808(%rbp), %rax
	movq	%rsi, -800(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rax, -808(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	movq	-808(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal12StringAdd_82EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6StringEEES8_@PLT
	movq	-800(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12StringAdd_82EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6StringEEES8_@PLT
	movq	%r15, %rdi
	leaq	.LC7(%rip), %rsi
	movq	%rax, -800(%rbp)
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	movq	-800(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal12StringAdd_82EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6StringEEES8_@PLT
	movq	-784(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal12StringAdd_82EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6StringEEES8_@PLT
	movq	%r15, %rdi
	leaq	.LC6(%rip), %rsi
	movq	%rax, -784(%rbp)
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	movq	-784(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal12StringAdd_82EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6StringEEES8_@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L17
.L98:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22418:
	.size	_ZN2v88internal19CreateHTMLAssembler22GenerateCreateHTMLImplEv, .-_ZN2v88internal19CreateHTMLAssembler22GenerateCreateHTMLImplEv
	.section	.rodata._ZN2v88internal8Builtins19Generate_CreateHTMLEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/string-html-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins19Generate_CreateHTMLEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"CreateHTML"
	.section	.text._ZN2v88internal8Builtins19Generate_CreateHTMLEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins19Generate_CreateHTMLEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins19Generate_CreateHTMLEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins19Generate_CreateHTMLEPNS0_8compiler18CodeAssemblerStateE:
.LFB22414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$146, %ecx
	leaq	.LC8(%rip), %rdx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$892, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L103
.L100:
	movq	%r13, %rdi
	call	_ZN2v88internal19CreateHTMLAssembler22GenerateCreateHTMLImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L104
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L100
.L104:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22414:
	.size	_ZN2v88internal8Builtins19Generate_CreateHTMLEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins19Generate_CreateHTMLEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal30StringPrototypeAnchorAssembler33GenerateStringPrototypeAnchorImplEv.str1.1,"aMS",@progbits,1
.LC10:
	.string	"String.prototype.anchor"
.LC11:
	.string	"a"
.LC12:
	.string	"name"
	.section	.text._ZN2v88internal30StringPrototypeAnchorAssembler33GenerateStringPrototypeAnchorImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30StringPrototypeAnchorAssembler33GenerateStringPrototypeAnchorImplEv
	.type	_ZN2v88internal30StringPrototypeAnchorAssembler33GenerateStringPrototypeAnchorImplEv, @function
_ZN2v88internal30StringPrototypeAnchorAssembler33GenerateStringPrototypeAnchorImplEv:
.LFB22436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$504, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r12, %rcx
	movq	%r13, %rsi
	leaq	-432(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-424(%rbp), %rcx
	movl	$2, %esi
	movq	%r13, %rdi
	movq	-416(%rbp), %rbx
	movq	-432(%rbp), %rax
	movq	%r13, -336(%rbp)
	leaq	-472(%rbp), %r13
	movq	%rcx, -488(%rbp)
	movq	%rcx, -312(%rbp)
	movq	%rbx, -512(%rbp)
	movq	%rbx, -320(%rbp)
	leaq	-288(%rbp), %rbx
	movq	$1, -328(%rbp)
	movq	%rax, -496(%rbp)
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-336(%rbp), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -520(%rbp)
	movq	%rax, -504(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, %r12
	movq	-472(%rbp), %rax
	movq	$0, -264(%rbp)
	movq	%rax, -288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	-496(%rbp), %xmm1
	movq	%r12, -64(%rbp)
	leaq	-368(%rbp), %r12
	movhps	-488(%rbp), %xmm1
	movaps	%xmm0, -368(%rbp)
	movaps	%xmm1, -96(%rbp)
	movq	-512(%rbp), %xmm1
	movq	$0, -352(%rbp)
	movhps	-504(%rbp), %xmm1
	movaps	%xmm1, -80(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	leaq	40(%rax), %rdx
	movq	%rax, -368(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L106
	call	_ZdlPv@PLT
.L106:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -224(%rbp)
	jne	.L130
.L107:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L109
	call	_ZdlPv@PLT
.L109:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L110
	.p2align 4,,10
	.p2align 3
.L114:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L111
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L114
.L112:
	movq	-280(%rbp), %r12
.L110:
	testq	%r12, %r12
	je	.L115
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L115:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L131
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L114
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L130:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -352(%rbp)
	movaps	%xmm0, -368(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -368(%rbp)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-368(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L108
	call	_ZdlPv@PLT
.L108:
	movq	(%rbx), %rax
	movl	$27, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	movq	(%rax), %rcx
	movq	24(%rax), %r9
	movq	8(%rax), %rbx
	movq	%rcx, -488(%rbp)
	movq	16(%rax), %rcx
	movq	32(%rax), %rax
	movq	%r9, -536(%rbp)
	movq	%rcx, -512(%rbp)
	movq	%rbx, -496(%rbp)
	movq	%rax, -504(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-488(%rbp), %xmm0
	movq	-512(%rbp), %rcx
	movhps	-496(%rbp), %xmm0
	movq	%rcx, -384(%rbp)
	movaps	%xmm0, -400(%rbp)
	pushq	-384(%rbp)
	pushq	-392(%rbp)
	pushq	-400(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$26, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	.LC10(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	.LC11(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -512(%rbp)
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	.LC12(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -496(%rbp)
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	-464(%rbp), %r10
	movq	%r15, %rsi
	movq	%r10, %rdi
	movq	%rax, -488(%rbp)
	movq	%r10, -528(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-528(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$892, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-528(%rbp), %r10
	movq	-368(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -64(%rbp)
	leaq	-96(%rbp), %rcx
	movq	-504(%rbp), %xmm0
	movl	$5, %ebx
	movq	-528(%rbp), %r10
	movq	%rax, %r8
	movq	-536(%rbp), %r9
	movhps	-512(%rbp), %xmm0
	pushq	%rbx
	movq	-352(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rsi
	pushq	%rcx
	leaq	-448(%rbp), %rdx
	movl	$1, %ecx
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-496(%rbp), %xmm0
	movq	%rsi, -448(%rbp)
	xorl	%esi, %esi
	movhps	-488(%rbp), %xmm0
	movq	%rax, -440(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%r10, -488(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-488(%rbp), %r10
	movq	%rax, %r12
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-520(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	popq	%rax
	popq	%rdx
	jmp	.L107
.L131:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22436:
	.size	_ZN2v88internal30StringPrototypeAnchorAssembler33GenerateStringPrototypeAnchorImplEv, .-_ZN2v88internal30StringPrototypeAnchorAssembler33GenerateStringPrototypeAnchorImplEv
	.section	.rodata._ZN2v88internal8Builtins30Generate_StringPrototypeAnchorEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"StringPrototypeAnchor"
	.section	.text._ZN2v88internal8Builtins30Generate_StringPrototypeAnchorEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins30Generate_StringPrototypeAnchorEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins30Generate_StringPrototypeAnchorEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins30Generate_StringPrototypeAnchorEPNS0_8compiler18CodeAssemblerStateE:
.LFB22432:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$280, %ecx
	leaq	.LC8(%rip), %rdx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$893, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L136
.L133:
	movq	%r13, %rdi
	call	_ZN2v88internal30StringPrototypeAnchorAssembler33GenerateStringPrototypeAnchorImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L137
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L133
.L137:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22432:
	.size	_ZN2v88internal8Builtins30Generate_StringPrototypeAnchorEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins30Generate_StringPrototypeAnchorEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal27StringPrototypeBigAssembler30GenerateStringPrototypeBigImplEv.str1.1,"aMS",@progbits,1
.LC14:
	.string	"String.prototype.big"
.LC15:
	.string	"big"
	.section	.text._ZN2v88internal27StringPrototypeBigAssembler30GenerateStringPrototypeBigImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27StringPrototypeBigAssembler30GenerateStringPrototypeBigImplEv
	.type	_ZN2v88internal27StringPrototypeBigAssembler30GenerateStringPrototypeBigImplEv, @function
_ZN2v88internal27StringPrototypeBigAssembler30GenerateStringPrototypeBigImplEv:
.LFB22448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$472, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r12, %rcx
	movq	%r13, %rsi
	leaq	-400(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-392(%rbp), %rcx
	movl	$2, %esi
	movq	%r13, %rdi
	movq	-384(%rbp), %rbx
	movq	-400(%rbp), %rax
	movq	%r13, -336(%rbp)
	leaq	-440(%rbp), %r13
	movq	%rcx, -472(%rbp)
	movq	%rbx, -480(%rbp)
	movq	%rbx, -320(%rbp)
	leaq	-288(%rbp), %rbx
	movq	%rcx, -312(%rbp)
	movq	$1, -328(%rbp)
	movq	%rax, -456(%rbp)
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-336(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -488(%rbp)
	movq	%rax, -464(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, %r12
	movq	-440(%rbp), %rax
	movq	$0, -264(%rbp)
	movq	%rax, -288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	-456(%rbp), %xmm1
	movq	%r12, -64(%rbp)
	leaq	-368(%rbp), %r12
	movhps	-472(%rbp), %xmm1
	movaps	%xmm0, -368(%rbp)
	movaps	%xmm1, -96(%rbp)
	movq	-480(%rbp), %xmm1
	movq	$0, -352(%rbp)
	movhps	-464(%rbp), %xmm1
	movaps	%xmm1, -80(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	leaq	40(%rax), %rdx
	movq	%rax, -368(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L139
	call	_ZdlPv@PLT
.L139:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -224(%rbp)
	jne	.L163
.L140:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L142
	call	_ZdlPv@PLT
.L142:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L143
	.p2align 4,,10
	.p2align 3
.L147:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L144
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L147
.L145:
	movq	-280(%rbp), %r12
.L143:
	testq	%r12, %r12
	je	.L148
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L148:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L164
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L147
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L163:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -352(%rbp)
	movaps	%xmm0, -368(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -368(%rbp)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-368(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L141
	call	_ZdlPv@PLT
.L141:
	movq	(%rbx), %rax
	movl	$35, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %r9
	movq	32(%rax), %rax
	movq	%r9, -504(%rbp)
	movq	%rax, -456(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal15kEmptyString_67EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	movq	%rax, -480(%rbp)
	call	_ZN2v88internal15kEmptyString_67EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$34, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	.LC14(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	.LC15(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -472(%rbp)
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	-432(%rbp), %r10
	movq	%r15, %rsi
	movq	%r10, %rdi
	movq	%rax, -464(%rbp)
	movq	%r10, -496(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-496(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$892, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-496(%rbp), %r10
	movq	-368(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -64(%rbp)
	leaq	-96(%rbp), %rcx
	xorl	%esi, %esi
	movl	$5, %ebx
	movq	-496(%rbp), %r10
	movq	%rax, %r8
	movq	-456(%rbp), %xmm0
	pushq	%rbx
	movq	-504(%rbp), %r9
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdx
	movhps	-472(%rbp), %xmm0
	pushq	%rcx
	movq	-352(%rbp), %rax
	movl	$1, %ecx
	movaps	%xmm0, -96(%rbp)
	movq	%r10, %rdi
	movq	-464(%rbp), %xmm0
	movq	%rdx, -416(%rbp)
	leaq	-416(%rbp), %rdx
	movhps	-480(%rbp), %xmm0
	movq	%r10, -456(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -408(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-456(%rbp), %r10
	movq	%rax, %r12
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-488(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	popq	%rax
	popq	%rdx
	jmp	.L140
.L164:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22448:
	.size	_ZN2v88internal27StringPrototypeBigAssembler30GenerateStringPrototypeBigImplEv, .-_ZN2v88internal27StringPrototypeBigAssembler30GenerateStringPrototypeBigImplEv
	.section	.rodata._ZN2v88internal8Builtins27Generate_StringPrototypeBigEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"StringPrototypeBig"
	.section	.text._ZN2v88internal8Builtins27Generate_StringPrototypeBigEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins27Generate_StringPrototypeBigEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins27Generate_StringPrototypeBigEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins27Generate_StringPrototypeBigEPNS0_8compiler18CodeAssemblerStateE:
.LFB22444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$325, %ecx
	leaq	.LC8(%rip), %rdx
	leaq	.LC16(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$894, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L169
.L166:
	movq	%r13, %rdi
	call	_ZN2v88internal27StringPrototypeBigAssembler30GenerateStringPrototypeBigImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L170
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L166
.L170:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22444:
	.size	_ZN2v88internal8Builtins27Generate_StringPrototypeBigEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins27Generate_StringPrototypeBigEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal29StringPrototypeBlinkAssembler32GenerateStringPrototypeBlinkImplEv.str1.1,"aMS",@progbits,1
.LC17:
	.string	"String.prototype.blink"
.LC18:
	.string	"blink"
	.section	.text._ZN2v88internal29StringPrototypeBlinkAssembler32GenerateStringPrototypeBlinkImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29StringPrototypeBlinkAssembler32GenerateStringPrototypeBlinkImplEv
	.type	_ZN2v88internal29StringPrototypeBlinkAssembler32GenerateStringPrototypeBlinkImplEv, @function
_ZN2v88internal29StringPrototypeBlinkAssembler32GenerateStringPrototypeBlinkImplEv:
.LFB22457:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$472, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r13, %rcx
	movq	%r12, %rsi
	leaq	-400(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-392(%rbp), %rcx
	movl	$2, %esi
	movq	%r12, %rdi
	movq	-384(%rbp), %rbx
	movq	-400(%rbp), %rax
	movq	%r12, -336(%rbp)
	leaq	-440(%rbp), %r12
	movq	%rcx, -472(%rbp)
	movq	%rbx, -480(%rbp)
	movq	%rbx, -320(%rbp)
	leaq	-288(%rbp), %rbx
	movq	%rcx, -312(%rbp)
	movq	$1, -328(%rbp)
	movq	%rax, -456(%rbp)
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-336(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -488(%rbp)
	movq	%rax, -464(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, %r13
	movq	-440(%rbp), %rax
	movq	$0, -264(%rbp)
	movq	%rax, -288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	-456(%rbp), %xmm1
	movq	%r13, -64(%rbp)
	leaq	-368(%rbp), %r13
	movhps	-472(%rbp), %xmm1
	movaps	%xmm0, -368(%rbp)
	movaps	%xmm1, -96(%rbp)
	movq	-480(%rbp), %xmm1
	movq	$0, -352(%rbp)
	movhps	-464(%rbp), %xmm1
	movaps	%xmm1, -80(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	leaq	40(%rax), %rdx
	movq	%rax, -368(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L172
	call	_ZdlPv@PLT
.L172:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -224(%rbp)
	jne	.L196
.L173:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L175
	call	_ZdlPv@PLT
.L175:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L176
	.p2align 4,,10
	.p2align 3
.L180:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L177
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L180
.L178:
	movq	-280(%rbp), %r13
.L176:
	testq	%r13, %r13
	je	.L181
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L181:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L197
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L180
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L196:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -352(%rbp)
	movaps	%xmm0, -368(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -368(%rbp)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-368(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L174
	call	_ZdlPv@PLT
.L174:
	movq	(%rbx), %rax
	movl	$43, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	24(%rax), %r9
	movq	32(%rax), %rax
	movq	%r9, -504(%rbp)
	movq	%rax, -456(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal15kEmptyString_67EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$44, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -480(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal15kEmptyString_67EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$42, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	.LC17(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	.LC18(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -472(%rbp)
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	-432(%rbp), %r10
	movq	%r15, %rsi
	movq	%r10, %rdi
	movq	%rax, -464(%rbp)
	movq	%r10, -496(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-496(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$892, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-496(%rbp), %r10
	movq	-368(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -64(%rbp)
	leaq	-96(%rbp), %rcx
	xorl	%esi, %esi
	movl	$5, %ebx
	movq	-496(%rbp), %r10
	movq	%rax, %r8
	movq	-456(%rbp), %xmm0
	pushq	%rbx
	movq	-504(%rbp), %r9
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdx
	movhps	-472(%rbp), %xmm0
	pushq	%rcx
	movq	-352(%rbp), %rax
	movl	$1, %ecx
	movaps	%xmm0, -96(%rbp)
	movq	%r10, %rdi
	movq	-464(%rbp), %xmm0
	movq	%rdx, -416(%rbp)
	leaq	-416(%rbp), %rdx
	movhps	-480(%rbp), %xmm0
	movq	%r10, -456(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -408(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-456(%rbp), %r10
	movq	%rax, %r13
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-488(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	popq	%rax
	popq	%rdx
	jmp	.L173
.L197:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22457:
	.size	_ZN2v88internal29StringPrototypeBlinkAssembler32GenerateStringPrototypeBlinkImplEv, .-_ZN2v88internal29StringPrototypeBlinkAssembler32GenerateStringPrototypeBlinkImplEv
	.section	.rodata._ZN2v88internal8Builtins29Generate_StringPrototypeBlinkEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC19:
	.string	"StringPrototypeBlink"
	.section	.text._ZN2v88internal8Builtins29Generate_StringPrototypeBlinkEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins29Generate_StringPrototypeBlinkEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins29Generate_StringPrototypeBlinkEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins29Generate_StringPrototypeBlinkEPNS0_8compiler18CodeAssemblerStateE:
.LFB22453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$367, %ecx
	leaq	.LC8(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$895, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L202
.L199:
	movq	%r13, %rdi
	call	_ZN2v88internal29StringPrototypeBlinkAssembler32GenerateStringPrototypeBlinkImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L203
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L199
.L203:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22453:
	.size	_ZN2v88internal8Builtins29Generate_StringPrototypeBlinkEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins29Generate_StringPrototypeBlinkEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal28StringPrototypeBoldAssembler31GenerateStringPrototypeBoldImplEv.str1.1,"aMS",@progbits,1
.LC20:
	.string	"String.prototype.bold"
.LC21:
	.string	"b"
	.section	.text._ZN2v88internal28StringPrototypeBoldAssembler31GenerateStringPrototypeBoldImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28StringPrototypeBoldAssembler31GenerateStringPrototypeBoldImplEv
	.type	_ZN2v88internal28StringPrototypeBoldAssembler31GenerateStringPrototypeBoldImplEv, @function
_ZN2v88internal28StringPrototypeBoldAssembler31GenerateStringPrototypeBoldImplEv:
.LFB22466:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$472, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r12, %rcx
	movq	%r13, %rsi
	leaq	-400(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-392(%rbp), %rcx
	movl	$2, %esi
	movq	%r13, %rdi
	movq	-384(%rbp), %rbx
	movq	-400(%rbp), %rax
	movq	%r13, -336(%rbp)
	leaq	-440(%rbp), %r13
	movq	%rcx, -472(%rbp)
	movq	%rbx, -480(%rbp)
	movq	%rbx, -320(%rbp)
	leaq	-288(%rbp), %rbx
	movq	%rcx, -312(%rbp)
	movq	$1, -328(%rbp)
	movq	%rax, -456(%rbp)
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-336(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -488(%rbp)
	movq	%rax, -464(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, %r12
	movq	-440(%rbp), %rax
	movq	$0, -264(%rbp)
	movq	%rax, -288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	-456(%rbp), %xmm1
	movq	%r12, -64(%rbp)
	leaq	-368(%rbp), %r12
	movhps	-472(%rbp), %xmm1
	movaps	%xmm0, -368(%rbp)
	movaps	%xmm1, -96(%rbp)
	movq	-480(%rbp), %xmm1
	movq	$0, -352(%rbp)
	movhps	-464(%rbp), %xmm1
	movaps	%xmm1, -80(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	leaq	40(%rax), %rdx
	movq	%rax, -368(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L205
	call	_ZdlPv@PLT
.L205:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -224(%rbp)
	jne	.L229
.L206:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L208
	call	_ZdlPv@PLT
.L208:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L209
	.p2align 4,,10
	.p2align 3
.L213:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L210
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L213
.L211:
	movq	-280(%rbp), %r12
.L209:
	testq	%r12, %r12
	je	.L214
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L214:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L230
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L210:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L213
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L229:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -352(%rbp)
	movaps	%xmm0, -368(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -368(%rbp)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-368(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L207
	call	_ZdlPv@PLT
.L207:
	movq	(%rbx), %rax
	movl	$52, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %r9
	movq	32(%rax), %rax
	movq	%r9, -504(%rbp)
	movq	%rax, -456(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal15kEmptyString_67EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	movq	%rax, -480(%rbp)
	call	_ZN2v88internal15kEmptyString_67EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$51, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	.LC20(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	.LC21(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -472(%rbp)
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	-432(%rbp), %r10
	movq	%r15, %rsi
	movq	%r10, %rdi
	movq	%rax, -464(%rbp)
	movq	%r10, -496(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-496(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$892, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-496(%rbp), %r10
	movq	-368(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -64(%rbp)
	leaq	-96(%rbp), %rcx
	xorl	%esi, %esi
	movl	$5, %ebx
	movq	-496(%rbp), %r10
	movq	%rax, %r8
	movq	-456(%rbp), %xmm0
	pushq	%rbx
	movq	-504(%rbp), %r9
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdx
	movhps	-472(%rbp), %xmm0
	pushq	%rcx
	movq	-352(%rbp), %rax
	movl	$1, %ecx
	movaps	%xmm0, -96(%rbp)
	movq	%r10, %rdi
	movq	-464(%rbp), %xmm0
	movq	%rdx, -416(%rbp)
	leaq	-416(%rbp), %rdx
	movhps	-480(%rbp), %xmm0
	movq	%r10, -456(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -408(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-456(%rbp), %r10
	movq	%rax, %r12
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-488(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	popq	%rax
	popq	%rdx
	jmp	.L206
.L230:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22466:
	.size	_ZN2v88internal28StringPrototypeBoldAssembler31GenerateStringPrototypeBoldImplEv, .-_ZN2v88internal28StringPrototypeBoldAssembler31GenerateStringPrototypeBoldImplEv
	.section	.rodata._ZN2v88internal8Builtins28Generate_StringPrototypeBoldEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC22:
	.string	"StringPrototypeBold"
	.section	.text._ZN2v88internal8Builtins28Generate_StringPrototypeBoldEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins28Generate_StringPrototypeBoldEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins28Generate_StringPrototypeBoldEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins28Generate_StringPrototypeBoldEPNS0_8compiler18CodeAssemblerStateE:
.LFB22462:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$410, %ecx
	leaq	.LC8(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$896, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L235
.L232:
	movq	%r13, %rdi
	call	_ZN2v88internal28StringPrototypeBoldAssembler31GenerateStringPrototypeBoldImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L236
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L232
.L236:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22462:
	.size	_ZN2v88internal8Builtins28Generate_StringPrototypeBoldEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins28Generate_StringPrototypeBoldEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal33StringPrototypeFontcolorAssembler36GenerateStringPrototypeFontcolorImplEv.str1.1,"aMS",@progbits,1
.LC23:
	.string	"String.prototype.fontcolor"
.LC24:
	.string	"font"
.LC25:
	.string	"color"
	.section	.text._ZN2v88internal33StringPrototypeFontcolorAssembler36GenerateStringPrototypeFontcolorImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33StringPrototypeFontcolorAssembler36GenerateStringPrototypeFontcolorImplEv
	.type	_ZN2v88internal33StringPrototypeFontcolorAssembler36GenerateStringPrototypeFontcolorImplEv, @function
_ZN2v88internal33StringPrototypeFontcolorAssembler36GenerateStringPrototypeFontcolorImplEv:
.LFB22475:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$504, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r12, %rcx
	movq	%r13, %rsi
	leaq	-432(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-424(%rbp), %rcx
	movl	$2, %esi
	movq	%r13, %rdi
	movq	-416(%rbp), %rbx
	movq	-432(%rbp), %rax
	movq	%r13, -336(%rbp)
	leaq	-472(%rbp), %r13
	movq	%rcx, -488(%rbp)
	movq	%rcx, -312(%rbp)
	movq	%rbx, -512(%rbp)
	movq	%rbx, -320(%rbp)
	leaq	-288(%rbp), %rbx
	movq	$1, -328(%rbp)
	movq	%rax, -496(%rbp)
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-336(%rbp), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -520(%rbp)
	movq	%rax, -504(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, %r12
	movq	-472(%rbp), %rax
	movq	$0, -264(%rbp)
	movq	%rax, -288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	-496(%rbp), %xmm1
	movq	%r12, -64(%rbp)
	leaq	-368(%rbp), %r12
	movhps	-488(%rbp), %xmm1
	movaps	%xmm0, -368(%rbp)
	movaps	%xmm1, -96(%rbp)
	movq	-512(%rbp), %xmm1
	movq	$0, -352(%rbp)
	movhps	-504(%rbp), %xmm1
	movaps	%xmm1, -80(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	leaq	40(%rax), %rdx
	movq	%rax, -368(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L238
	call	_ZdlPv@PLT
.L238:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -224(%rbp)
	jne	.L262
.L239:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L241
	call	_ZdlPv@PLT
.L241:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L242
	.p2align 4,,10
	.p2align 3
.L246:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L243
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L246
.L244:
	movq	-280(%rbp), %r12
.L242:
	testq	%r12, %r12
	je	.L247
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L247:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L263
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L246
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L262:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -352(%rbp)
	movaps	%xmm0, -368(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -368(%rbp)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-368(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L240
	call	_ZdlPv@PLT
.L240:
	movq	(%rbx), %rax
	movl	$60, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	movq	(%rax), %rcx
	movq	24(%rax), %r9
	movq	8(%rax), %rbx
	movq	%rcx, -488(%rbp)
	movq	16(%rax), %rcx
	movq	32(%rax), %rax
	movq	%r9, -536(%rbp)
	movq	%rcx, -512(%rbp)
	movq	%rbx, -496(%rbp)
	movq	%rax, -504(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-488(%rbp), %xmm0
	movq	-512(%rbp), %rcx
	movhps	-496(%rbp), %xmm0
	movq	%rcx, -384(%rbp)
	movaps	%xmm0, -400(%rbp)
	pushq	-384(%rbp)
	pushq	-392(%rbp)
	pushq	-400(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$59, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	.LC23(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	.LC24(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -512(%rbp)
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	.LC25(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -496(%rbp)
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	-464(%rbp), %r10
	movq	%r15, %rsi
	movq	%r10, %rdi
	movq	%rax, -488(%rbp)
	movq	%r10, -528(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-528(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$892, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-528(%rbp), %r10
	movq	-368(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -64(%rbp)
	leaq	-96(%rbp), %rcx
	movq	-504(%rbp), %xmm0
	movl	$5, %ebx
	movq	-528(%rbp), %r10
	movq	%rax, %r8
	movq	-536(%rbp), %r9
	movhps	-512(%rbp), %xmm0
	pushq	%rbx
	movq	-352(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rsi
	pushq	%rcx
	leaq	-448(%rbp), %rdx
	movl	$1, %ecx
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-496(%rbp), %xmm0
	movq	%rsi, -448(%rbp)
	xorl	%esi, %esi
	movhps	-488(%rbp), %xmm0
	movq	%rax, -440(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%r10, -488(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-488(%rbp), %r10
	movq	%rax, %r12
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-520(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	popq	%rax
	popq	%rdx
	jmp	.L239
.L263:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22475:
	.size	_ZN2v88internal33StringPrototypeFontcolorAssembler36GenerateStringPrototypeFontcolorImplEv, .-_ZN2v88internal33StringPrototypeFontcolorAssembler36GenerateStringPrototypeFontcolorImplEv
	.section	.rodata._ZN2v88internal8Builtins33Generate_StringPrototypeFontcolorEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC26:
	.string	"StringPrototypeFontcolor"
	.section	.text._ZN2v88internal8Builtins33Generate_StringPrototypeFontcolorEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins33Generate_StringPrototypeFontcolorEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins33Generate_StringPrototypeFontcolorEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins33Generate_StringPrototypeFontcolorEPNS0_8compiler18CodeAssemblerStateE:
.LFB22471:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$452, %ecx
	leaq	.LC8(%rip), %rdx
	leaq	.LC26(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$897, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L268
.L265:
	movq	%r13, %rdi
	call	_ZN2v88internal33StringPrototypeFontcolorAssembler36GenerateStringPrototypeFontcolorImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L269
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L265
.L269:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22471:
	.size	_ZN2v88internal8Builtins33Generate_StringPrototypeFontcolorEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins33Generate_StringPrototypeFontcolorEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal32StringPrototypeFontsizeAssembler35GenerateStringPrototypeFontsizeImplEv.str1.1,"aMS",@progbits,1
.LC27:
	.string	"String.prototype.fontsize"
.LC28:
	.string	"size"
	.section	.text._ZN2v88internal32StringPrototypeFontsizeAssembler35GenerateStringPrototypeFontsizeImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32StringPrototypeFontsizeAssembler35GenerateStringPrototypeFontsizeImplEv
	.type	_ZN2v88internal32StringPrototypeFontsizeAssembler35GenerateStringPrototypeFontsizeImplEv, @function
_ZN2v88internal32StringPrototypeFontsizeAssembler35GenerateStringPrototypeFontsizeImplEv:
.LFB22484:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$504, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r12, %rcx
	movq	%r13, %rsi
	leaq	-432(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-424(%rbp), %rcx
	movl	$2, %esi
	movq	%r13, %rdi
	movq	-416(%rbp), %rbx
	movq	-432(%rbp), %rax
	movq	%r13, -336(%rbp)
	leaq	-472(%rbp), %r13
	movq	%rcx, -488(%rbp)
	movq	%rcx, -312(%rbp)
	movq	%rbx, -512(%rbp)
	movq	%rbx, -320(%rbp)
	leaq	-288(%rbp), %rbx
	movq	$1, -328(%rbp)
	movq	%rax, -496(%rbp)
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-336(%rbp), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -520(%rbp)
	movq	%rax, -504(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, %r12
	movq	-472(%rbp), %rax
	movq	$0, -264(%rbp)
	movq	%rax, -288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	-496(%rbp), %xmm1
	movq	%r12, -64(%rbp)
	leaq	-368(%rbp), %r12
	movhps	-488(%rbp), %xmm1
	movaps	%xmm0, -368(%rbp)
	movaps	%xmm1, -96(%rbp)
	movq	-512(%rbp), %xmm1
	movq	$0, -352(%rbp)
	movhps	-504(%rbp), %xmm1
	movaps	%xmm1, -80(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	leaq	40(%rax), %rdx
	movq	%rax, -368(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L271
	call	_ZdlPv@PLT
.L271:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -224(%rbp)
	jne	.L295
.L272:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L274
	call	_ZdlPv@PLT
.L274:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L275
	.p2align 4,,10
	.p2align 3
.L279:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L276
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L279
.L277:
	movq	-280(%rbp), %r12
.L275:
	testq	%r12, %r12
	je	.L280
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L280:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L296
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L279
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L295:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -352(%rbp)
	movaps	%xmm0, -368(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -368(%rbp)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-368(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L273
	call	_ZdlPv@PLT
.L273:
	movq	(%rbx), %rax
	movl	$68, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	movq	(%rax), %rcx
	movq	24(%rax), %r9
	movq	8(%rax), %rbx
	movq	%rcx, -488(%rbp)
	movq	16(%rax), %rcx
	movq	32(%rax), %rax
	movq	%r9, -536(%rbp)
	movq	%rcx, -512(%rbp)
	movq	%rbx, -496(%rbp)
	movq	%rax, -504(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-488(%rbp), %xmm0
	movq	-512(%rbp), %rcx
	movhps	-496(%rbp), %xmm0
	movq	%rcx, -384(%rbp)
	movaps	%xmm0, -400(%rbp)
	pushq	-384(%rbp)
	pushq	-392(%rbp)
	pushq	-400(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$67, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	.LC27(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	.LC24(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -512(%rbp)
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	.LC28(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -496(%rbp)
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	-464(%rbp), %r10
	movq	%r15, %rsi
	movq	%r10, %rdi
	movq	%rax, -488(%rbp)
	movq	%r10, -528(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-528(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$892, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-528(%rbp), %r10
	movq	-368(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -64(%rbp)
	leaq	-96(%rbp), %rcx
	movq	-504(%rbp), %xmm0
	movl	$5, %ebx
	movq	-528(%rbp), %r10
	movq	%rax, %r8
	movq	-536(%rbp), %r9
	movhps	-512(%rbp), %xmm0
	pushq	%rbx
	movq	-352(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rsi
	pushq	%rcx
	leaq	-448(%rbp), %rdx
	movl	$1, %ecx
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-496(%rbp), %xmm0
	movq	%rsi, -448(%rbp)
	xorl	%esi, %esi
	movhps	-488(%rbp), %xmm0
	movq	%rax, -440(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%r10, -488(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-488(%rbp), %r10
	movq	%rax, %r12
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-520(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	popq	%rax
	popq	%rdx
	jmp	.L272
.L296:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22484:
	.size	_ZN2v88internal32StringPrototypeFontsizeAssembler35GenerateStringPrototypeFontsizeImplEv, .-_ZN2v88internal32StringPrototypeFontsizeAssembler35GenerateStringPrototypeFontsizeImplEv
	.section	.rodata._ZN2v88internal8Builtins32Generate_StringPrototypeFontsizeEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC29:
	.string	"StringPrototypeFontsize"
	.section	.text._ZN2v88internal8Builtins32Generate_StringPrototypeFontsizeEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins32Generate_StringPrototypeFontsizeEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins32Generate_StringPrototypeFontsizeEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins32Generate_StringPrototypeFontsizeEPNS0_8compiler18CodeAssemblerStateE:
.LFB22480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$497, %ecx
	leaq	.LC8(%rip), %rdx
	leaq	.LC29(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$898, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L301
.L298:
	movq	%r13, %rdi
	call	_ZN2v88internal32StringPrototypeFontsizeAssembler35GenerateStringPrototypeFontsizeImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L302
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L301:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L298
.L302:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22480:
	.size	_ZN2v88internal8Builtins32Generate_StringPrototypeFontsizeEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins32Generate_StringPrototypeFontsizeEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal29StringPrototypeFixedAssembler32GenerateStringPrototypeFixedImplEv.str1.1,"aMS",@progbits,1
.LC30:
	.string	"String.prototype.fixed"
.LC31:
	.string	"tt"
	.section	.text._ZN2v88internal29StringPrototypeFixedAssembler32GenerateStringPrototypeFixedImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29StringPrototypeFixedAssembler32GenerateStringPrototypeFixedImplEv
	.type	_ZN2v88internal29StringPrototypeFixedAssembler32GenerateStringPrototypeFixedImplEv, @function
_ZN2v88internal29StringPrototypeFixedAssembler32GenerateStringPrototypeFixedImplEv:
.LFB22493:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$472, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r12, %rcx
	movq	%r13, %rsi
	leaq	-400(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-392(%rbp), %rcx
	movl	$2, %esi
	movq	%r13, %rdi
	movq	-384(%rbp), %rbx
	movq	-400(%rbp), %rax
	movq	%r13, -336(%rbp)
	leaq	-440(%rbp), %r13
	movq	%rcx, -472(%rbp)
	movq	%rbx, -480(%rbp)
	movq	%rbx, -320(%rbp)
	leaq	-288(%rbp), %rbx
	movq	%rcx, -312(%rbp)
	movq	$1, -328(%rbp)
	movq	%rax, -456(%rbp)
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-336(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -488(%rbp)
	movq	%rax, -464(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, %r12
	movq	-440(%rbp), %rax
	movq	$0, -264(%rbp)
	movq	%rax, -288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	-456(%rbp), %xmm1
	movq	%r12, -64(%rbp)
	leaq	-368(%rbp), %r12
	movhps	-472(%rbp), %xmm1
	movaps	%xmm0, -368(%rbp)
	movaps	%xmm1, -96(%rbp)
	movq	-480(%rbp), %xmm1
	movq	$0, -352(%rbp)
	movhps	-464(%rbp), %xmm1
	movaps	%xmm1, -80(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	leaq	40(%rax), %rdx
	movq	%rax, -368(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L304
	call	_ZdlPv@PLT
.L304:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -224(%rbp)
	jne	.L328
.L305:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L307
	call	_ZdlPv@PLT
.L307:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L308
	.p2align 4,,10
	.p2align 3
.L312:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L309
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L312
.L310:
	movq	-280(%rbp), %r12
.L308:
	testq	%r12, %r12
	je	.L313
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L313:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L329
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L309:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L312
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L328:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -352(%rbp)
	movaps	%xmm0, -368(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -368(%rbp)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-368(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L306
	call	_ZdlPv@PLT
.L306:
	movq	(%rbx), %rax
	movl	$76, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %r9
	movq	32(%rax), %rax
	movq	%r9, -504(%rbp)
	movq	%rax, -456(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal15kEmptyString_67EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	movq	%rax, -480(%rbp)
	call	_ZN2v88internal15kEmptyString_67EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$75, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	.LC30(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	.LC31(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -472(%rbp)
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	-432(%rbp), %r10
	movq	%r15, %rsi
	movq	%r10, %rdi
	movq	%rax, -464(%rbp)
	movq	%r10, -496(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-496(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$892, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-496(%rbp), %r10
	movq	-368(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -64(%rbp)
	leaq	-96(%rbp), %rcx
	xorl	%esi, %esi
	movl	$5, %ebx
	movq	-496(%rbp), %r10
	movq	%rax, %r8
	movq	-456(%rbp), %xmm0
	pushq	%rbx
	movq	-504(%rbp), %r9
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdx
	movhps	-472(%rbp), %xmm0
	pushq	%rcx
	movq	-352(%rbp), %rax
	movl	$1, %ecx
	movaps	%xmm0, -96(%rbp)
	movq	%r10, %rdi
	movq	-464(%rbp), %xmm0
	movq	%rdx, -416(%rbp)
	leaq	-416(%rbp), %rdx
	movhps	-480(%rbp), %xmm0
	movq	%r10, -456(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -408(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-456(%rbp), %r10
	movq	%rax, %r12
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-488(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	popq	%rax
	popq	%rdx
	jmp	.L305
.L329:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22493:
	.size	_ZN2v88internal29StringPrototypeFixedAssembler32GenerateStringPrototypeFixedImplEv, .-_ZN2v88internal29StringPrototypeFixedAssembler32GenerateStringPrototypeFixedImplEv
	.section	.rodata._ZN2v88internal8Builtins29Generate_StringPrototypeFixedEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC32:
	.string	"StringPrototypeFixed"
	.section	.text._ZN2v88internal8Builtins29Generate_StringPrototypeFixedEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins29Generate_StringPrototypeFixedEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins29Generate_StringPrototypeFixedEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins29Generate_StringPrototypeFixedEPNS0_8compiler18CodeAssemblerStateE:
.LFB22489:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$542, %ecx
	leaq	.LC8(%rip), %rdx
	leaq	.LC32(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$899, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L334
.L331:
	movq	%r13, %rdi
	call	_ZN2v88internal29StringPrototypeFixedAssembler32GenerateStringPrototypeFixedImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L335
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L331
.L335:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22489:
	.size	_ZN2v88internal8Builtins29Generate_StringPrototypeFixedEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins29Generate_StringPrototypeFixedEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal31StringPrototypeItalicsAssembler34GenerateStringPrototypeItalicsImplEv.str1.1,"aMS",@progbits,1
.LC33:
	.string	"String.prototype.italics"
.LC34:
	.string	"i"
	.section	.text._ZN2v88internal31StringPrototypeItalicsAssembler34GenerateStringPrototypeItalicsImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31StringPrototypeItalicsAssembler34GenerateStringPrototypeItalicsImplEv
	.type	_ZN2v88internal31StringPrototypeItalicsAssembler34GenerateStringPrototypeItalicsImplEv, @function
_ZN2v88internal31StringPrototypeItalicsAssembler34GenerateStringPrototypeItalicsImplEv:
.LFB22502:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$472, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r12, %rcx
	movq	%r13, %rsi
	leaq	-400(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-392(%rbp), %rcx
	movl	$2, %esi
	movq	%r13, %rdi
	movq	-384(%rbp), %rbx
	movq	-400(%rbp), %rax
	movq	%r13, -336(%rbp)
	leaq	-440(%rbp), %r13
	movq	%rcx, -472(%rbp)
	movq	%rbx, -480(%rbp)
	movq	%rbx, -320(%rbp)
	leaq	-288(%rbp), %rbx
	movq	%rcx, -312(%rbp)
	movq	$1, -328(%rbp)
	movq	%rax, -456(%rbp)
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-336(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -488(%rbp)
	movq	%rax, -464(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, %r12
	movq	-440(%rbp), %rax
	movq	$0, -264(%rbp)
	movq	%rax, -288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	-456(%rbp), %xmm1
	movq	%r12, -64(%rbp)
	leaq	-368(%rbp), %r12
	movhps	-472(%rbp), %xmm1
	movaps	%xmm0, -368(%rbp)
	movaps	%xmm1, -96(%rbp)
	movq	-480(%rbp), %xmm1
	movq	$0, -352(%rbp)
	movhps	-464(%rbp), %xmm1
	movaps	%xmm1, -80(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	leaq	40(%rax), %rdx
	movq	%rax, -368(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L337
	call	_ZdlPv@PLT
.L337:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -224(%rbp)
	jne	.L361
.L338:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L340
	call	_ZdlPv@PLT
.L340:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L341
	.p2align 4,,10
	.p2align 3
.L345:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L342
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L345
.L343:
	movq	-280(%rbp), %r12
.L341:
	testq	%r12, %r12
	je	.L346
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L346:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L362
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L342:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L345
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L361:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -352(%rbp)
	movaps	%xmm0, -368(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -368(%rbp)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-368(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L339
	call	_ZdlPv@PLT
.L339:
	movq	(%rbx), %rax
	movl	$84, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %r9
	movq	32(%rax), %rax
	movq	%r9, -504(%rbp)
	movq	%rax, -456(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal15kEmptyString_67EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	movq	%rax, -480(%rbp)
	call	_ZN2v88internal15kEmptyString_67EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$83, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	.LC33(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	.LC34(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -472(%rbp)
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	-432(%rbp), %r10
	movq	%r15, %rsi
	movq	%r10, %rdi
	movq	%rax, -464(%rbp)
	movq	%r10, -496(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-496(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$892, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-496(%rbp), %r10
	movq	-368(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -64(%rbp)
	leaq	-96(%rbp), %rcx
	xorl	%esi, %esi
	movl	$5, %ebx
	movq	-496(%rbp), %r10
	movq	%rax, %r8
	movq	-456(%rbp), %xmm0
	pushq	%rbx
	movq	-504(%rbp), %r9
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdx
	movhps	-472(%rbp), %xmm0
	pushq	%rcx
	movq	-352(%rbp), %rax
	movl	$1, %ecx
	movaps	%xmm0, -96(%rbp)
	movq	%r10, %rdi
	movq	-464(%rbp), %xmm0
	movq	%rdx, -416(%rbp)
	leaq	-416(%rbp), %rdx
	movhps	-480(%rbp), %xmm0
	movq	%r10, -456(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -408(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-456(%rbp), %r10
	movq	%rax, %r12
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-488(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	popq	%rax
	popq	%rdx
	jmp	.L338
.L362:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22502:
	.size	_ZN2v88internal31StringPrototypeItalicsAssembler34GenerateStringPrototypeItalicsImplEv, .-_ZN2v88internal31StringPrototypeItalicsAssembler34GenerateStringPrototypeItalicsImplEv
	.section	.rodata._ZN2v88internal8Builtins31Generate_StringPrototypeItalicsEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC35:
	.string	"StringPrototypeItalics"
	.section	.text._ZN2v88internal8Builtins31Generate_StringPrototypeItalicsEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins31Generate_StringPrototypeItalicsEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins31Generate_StringPrototypeItalicsEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins31Generate_StringPrototypeItalicsEPNS0_8compiler18CodeAssemblerStateE:
.LFB22498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$584, %ecx
	leaq	.LC8(%rip), %rdx
	leaq	.LC35(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$900, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L367
.L364:
	movq	%r13, %rdi
	call	_ZN2v88internal31StringPrototypeItalicsAssembler34GenerateStringPrototypeItalicsImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L368
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L367:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L364
.L368:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22498:
	.size	_ZN2v88internal8Builtins31Generate_StringPrototypeItalicsEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins31Generate_StringPrototypeItalicsEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal28StringPrototypeLinkAssembler31GenerateStringPrototypeLinkImplEv.str1.1,"aMS",@progbits,1
.LC36:
	.string	"String.prototype.link"
.LC37:
	.string	"href"
	.section	.text._ZN2v88internal28StringPrototypeLinkAssembler31GenerateStringPrototypeLinkImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28StringPrototypeLinkAssembler31GenerateStringPrototypeLinkImplEv
	.type	_ZN2v88internal28StringPrototypeLinkAssembler31GenerateStringPrototypeLinkImplEv, @function
_ZN2v88internal28StringPrototypeLinkAssembler31GenerateStringPrototypeLinkImplEv:
.LFB22511:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$504, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r12, %rcx
	movq	%r13, %rsi
	leaq	-432(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-424(%rbp), %rcx
	movl	$2, %esi
	movq	%r13, %rdi
	movq	-416(%rbp), %rbx
	movq	-432(%rbp), %rax
	movq	%r13, -336(%rbp)
	leaq	-472(%rbp), %r13
	movq	%rcx, -488(%rbp)
	movq	%rcx, -312(%rbp)
	movq	%rbx, -512(%rbp)
	movq	%rbx, -320(%rbp)
	leaq	-288(%rbp), %rbx
	movq	$1, -328(%rbp)
	movq	%rax, -496(%rbp)
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-336(%rbp), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -520(%rbp)
	movq	%rax, -504(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, %r12
	movq	-472(%rbp), %rax
	movq	$0, -264(%rbp)
	movq	%rax, -288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	-496(%rbp), %xmm1
	movq	%r12, -64(%rbp)
	leaq	-368(%rbp), %r12
	movhps	-488(%rbp), %xmm1
	movaps	%xmm0, -368(%rbp)
	movaps	%xmm1, -96(%rbp)
	movq	-512(%rbp), %xmm1
	movq	$0, -352(%rbp)
	movhps	-504(%rbp), %xmm1
	movaps	%xmm1, -80(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	leaq	40(%rax), %rdx
	movq	%rax, -368(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L370
	call	_ZdlPv@PLT
.L370:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -224(%rbp)
	jne	.L394
.L371:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L373
	call	_ZdlPv@PLT
.L373:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L374
	.p2align 4,,10
	.p2align 3
.L378:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L375
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L378
.L376:
	movq	-280(%rbp), %r12
.L374:
	testq	%r12, %r12
	je	.L379
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L379:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L395
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L375:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L378
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L394:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -352(%rbp)
	movaps	%xmm0, -368(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -368(%rbp)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-368(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L372
	call	_ZdlPv@PLT
.L372:
	movq	(%rbx), %rax
	movl	$92, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	movq	(%rax), %rcx
	movq	24(%rax), %r9
	movq	8(%rax), %rbx
	movq	%rcx, -488(%rbp)
	movq	16(%rax), %rcx
	movq	32(%rax), %rax
	movq	%r9, -536(%rbp)
	movq	%rcx, -512(%rbp)
	movq	%rbx, -496(%rbp)
	movq	%rax, -504(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-488(%rbp), %xmm0
	movq	-512(%rbp), %rcx
	movhps	-496(%rbp), %xmm0
	movq	%rcx, -384(%rbp)
	movaps	%xmm0, -400(%rbp)
	pushq	-384(%rbp)
	pushq	-392(%rbp)
	pushq	-400(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$91, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	.LC36(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	.LC11(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -512(%rbp)
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	.LC37(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -496(%rbp)
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	-464(%rbp), %r10
	movq	%r15, %rsi
	movq	%r10, %rdi
	movq	%rax, -488(%rbp)
	movq	%r10, -528(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-528(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$892, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-528(%rbp), %r10
	movq	-368(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -64(%rbp)
	leaq	-96(%rbp), %rcx
	movq	-504(%rbp), %xmm0
	movl	$5, %ebx
	movq	-528(%rbp), %r10
	movq	%rax, %r8
	movq	-536(%rbp), %r9
	movhps	-512(%rbp), %xmm0
	pushq	%rbx
	movq	-352(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rsi
	pushq	%rcx
	leaq	-448(%rbp), %rdx
	movl	$1, %ecx
	movq	%r10, %rdi
	movaps	%xmm0, -96(%rbp)
	movq	-496(%rbp), %xmm0
	movq	%rsi, -448(%rbp)
	xorl	%esi, %esi
	movhps	-488(%rbp), %xmm0
	movq	%rax, -440(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%r10, -488(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-488(%rbp), %r10
	movq	%rax, %r12
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-520(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	popq	%rax
	popq	%rdx
	jmp	.L371
.L395:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22511:
	.size	_ZN2v88internal28StringPrototypeLinkAssembler31GenerateStringPrototypeLinkImplEv, .-_ZN2v88internal28StringPrototypeLinkAssembler31GenerateStringPrototypeLinkImplEv
	.section	.rodata._ZN2v88internal8Builtins28Generate_StringPrototypeLinkEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC38:
	.string	"StringPrototypeLink"
	.section	.text._ZN2v88internal8Builtins28Generate_StringPrototypeLinkEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins28Generate_StringPrototypeLinkEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins28Generate_StringPrototypeLinkEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins28Generate_StringPrototypeLinkEPNS0_8compiler18CodeAssemblerStateE:
.LFB22507:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$626, %ecx
	leaq	.LC8(%rip), %rdx
	leaq	.LC38(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$901, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L400
.L397:
	movq	%r13, %rdi
	call	_ZN2v88internal28StringPrototypeLinkAssembler31GenerateStringPrototypeLinkImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L401
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L397
.L401:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22507:
	.size	_ZN2v88internal8Builtins28Generate_StringPrototypeLinkEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins28Generate_StringPrototypeLinkEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal29StringPrototypeSmallAssembler32GenerateStringPrototypeSmallImplEv.str1.1,"aMS",@progbits,1
.LC39:
	.string	"String.prototype.small"
.LC40:
	.string	"small"
	.section	.text._ZN2v88internal29StringPrototypeSmallAssembler32GenerateStringPrototypeSmallImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29StringPrototypeSmallAssembler32GenerateStringPrototypeSmallImplEv
	.type	_ZN2v88internal29StringPrototypeSmallAssembler32GenerateStringPrototypeSmallImplEv, @function
_ZN2v88internal29StringPrototypeSmallAssembler32GenerateStringPrototypeSmallImplEv:
.LFB22520:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$472, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r13, %rcx
	movq	%r12, %rsi
	leaq	-400(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-392(%rbp), %rcx
	movl	$2, %esi
	movq	%r12, %rdi
	movq	-384(%rbp), %rbx
	movq	-400(%rbp), %rax
	movq	%r12, -336(%rbp)
	leaq	-440(%rbp), %r12
	movq	%rcx, -472(%rbp)
	movq	%rbx, -480(%rbp)
	movq	%rbx, -320(%rbp)
	leaq	-288(%rbp), %rbx
	movq	%rcx, -312(%rbp)
	movq	$1, -328(%rbp)
	movq	%rax, -456(%rbp)
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-336(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -488(%rbp)
	movq	%rax, -464(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, %r13
	movq	-440(%rbp), %rax
	movq	$0, -264(%rbp)
	movq	%rax, -288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	-456(%rbp), %xmm1
	movq	%r13, -64(%rbp)
	leaq	-368(%rbp), %r13
	movhps	-472(%rbp), %xmm1
	movaps	%xmm0, -368(%rbp)
	movaps	%xmm1, -96(%rbp)
	movq	-480(%rbp), %xmm1
	movq	$0, -352(%rbp)
	movhps	-464(%rbp), %xmm1
	movaps	%xmm1, -80(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	leaq	40(%rax), %rdx
	movq	%rax, -368(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L403
	call	_ZdlPv@PLT
.L403:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -224(%rbp)
	jne	.L427
.L404:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L406
	call	_ZdlPv@PLT
.L406:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L407
	.p2align 4,,10
	.p2align 3
.L411:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L408
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L411
.L409:
	movq	-280(%rbp), %r13
.L407:
	testq	%r13, %r13
	je	.L412
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L412:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L428
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L408:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L411
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L427:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -352(%rbp)
	movaps	%xmm0, -368(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -368(%rbp)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-368(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L405
	call	_ZdlPv@PLT
.L405:
	movq	(%rbx), %rax
	movl	$100, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	24(%rax), %r9
	movq	32(%rax), %rax
	movq	%r9, -504(%rbp)
	movq	%rax, -456(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal15kEmptyString_67EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$101, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -480(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal15kEmptyString_67EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$99, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	.LC39(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	.LC40(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -472(%rbp)
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	-432(%rbp), %r10
	movq	%r15, %rsi
	movq	%r10, %rdi
	movq	%rax, -464(%rbp)
	movq	%r10, -496(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-496(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$892, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-496(%rbp), %r10
	movq	-368(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -64(%rbp)
	leaq	-96(%rbp), %rcx
	xorl	%esi, %esi
	movl	$5, %ebx
	movq	-496(%rbp), %r10
	movq	%rax, %r8
	movq	-456(%rbp), %xmm0
	pushq	%rbx
	movq	-504(%rbp), %r9
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdx
	movhps	-472(%rbp), %xmm0
	pushq	%rcx
	movq	-352(%rbp), %rax
	movl	$1, %ecx
	movaps	%xmm0, -96(%rbp)
	movq	%r10, %rdi
	movq	-464(%rbp), %xmm0
	movq	%rdx, -416(%rbp)
	leaq	-416(%rbp), %rdx
	movhps	-480(%rbp), %xmm0
	movq	%r10, -456(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -408(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-456(%rbp), %r10
	movq	%rax, %r13
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-488(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	popq	%rax
	popq	%rdx
	jmp	.L404
.L428:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22520:
	.size	_ZN2v88internal29StringPrototypeSmallAssembler32GenerateStringPrototypeSmallImplEv, .-_ZN2v88internal29StringPrototypeSmallAssembler32GenerateStringPrototypeSmallImplEv
	.section	.rodata._ZN2v88internal8Builtins29Generate_StringPrototypeSmallEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC41:
	.string	"StringPrototypeSmall"
	.section	.text._ZN2v88internal8Builtins29Generate_StringPrototypeSmallEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins29Generate_StringPrototypeSmallEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins29Generate_StringPrototypeSmallEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins29Generate_StringPrototypeSmallEPNS0_8compiler18CodeAssemblerStateE:
.LFB22516:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$671, %ecx
	leaq	.LC8(%rip), %rdx
	leaq	.LC41(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$902, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L433
.L430:
	movq	%r13, %rdi
	call	_ZN2v88internal29StringPrototypeSmallAssembler32GenerateStringPrototypeSmallImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L434
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L430
.L434:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22516:
	.size	_ZN2v88internal8Builtins29Generate_StringPrototypeSmallEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins29Generate_StringPrototypeSmallEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal30StringPrototypeStrikeAssembler33GenerateStringPrototypeStrikeImplEv.str1.1,"aMS",@progbits,1
.LC42:
	.string	"String.prototype.strike"
.LC43:
	.string	"strike"
	.section	.text._ZN2v88internal30StringPrototypeStrikeAssembler33GenerateStringPrototypeStrikeImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30StringPrototypeStrikeAssembler33GenerateStringPrototypeStrikeImplEv
	.type	_ZN2v88internal30StringPrototypeStrikeAssembler33GenerateStringPrototypeStrikeImplEv, @function
_ZN2v88internal30StringPrototypeStrikeAssembler33GenerateStringPrototypeStrikeImplEv:
.LFB22529:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$472, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r13, %rcx
	movq	%r12, %rsi
	leaq	-400(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-392(%rbp), %rcx
	movl	$2, %esi
	movq	%r12, %rdi
	movq	-384(%rbp), %rbx
	movq	-400(%rbp), %rax
	movq	%r12, -336(%rbp)
	leaq	-440(%rbp), %r12
	movq	%rcx, -472(%rbp)
	movq	%rbx, -480(%rbp)
	movq	%rbx, -320(%rbp)
	leaq	-288(%rbp), %rbx
	movq	%rcx, -312(%rbp)
	movq	$1, -328(%rbp)
	movq	%rax, -456(%rbp)
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-336(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -488(%rbp)
	movq	%rax, -464(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, %r13
	movq	-440(%rbp), %rax
	movq	$0, -264(%rbp)
	movq	%rax, -288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	-456(%rbp), %xmm1
	movq	%r13, -64(%rbp)
	leaq	-368(%rbp), %r13
	movhps	-472(%rbp), %xmm1
	movaps	%xmm0, -368(%rbp)
	movaps	%xmm1, -96(%rbp)
	movq	-480(%rbp), %xmm1
	movq	$0, -352(%rbp)
	movhps	-464(%rbp), %xmm1
	movaps	%xmm1, -80(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	leaq	40(%rax), %rdx
	movq	%rax, -368(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L436
	call	_ZdlPv@PLT
.L436:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -224(%rbp)
	jne	.L460
.L437:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L439
	call	_ZdlPv@PLT
.L439:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L440
	.p2align 4,,10
	.p2align 3
.L444:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L441
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L444
.L442:
	movq	-280(%rbp), %r13
.L440:
	testq	%r13, %r13
	je	.L445
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L445:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L461
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L441:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L444
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L460:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -352(%rbp)
	movaps	%xmm0, -368(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -368(%rbp)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-368(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L438
	call	_ZdlPv@PLT
.L438:
	movq	(%rbx), %rax
	movl	$109, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	24(%rax), %r9
	movq	32(%rax), %rax
	movq	%r9, -504(%rbp)
	movq	%rax, -456(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal15kEmptyString_67EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$110, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -480(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal15kEmptyString_67EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$108, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	.LC42(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	.LC43(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -472(%rbp)
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	-432(%rbp), %r10
	movq	%r15, %rsi
	movq	%r10, %rdi
	movq	%rax, -464(%rbp)
	movq	%r10, -496(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-496(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$892, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-496(%rbp), %r10
	movq	-368(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -64(%rbp)
	leaq	-96(%rbp), %rcx
	xorl	%esi, %esi
	movl	$5, %ebx
	movq	-496(%rbp), %r10
	movq	%rax, %r8
	movq	-456(%rbp), %xmm0
	pushq	%rbx
	movq	-504(%rbp), %r9
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdx
	movhps	-472(%rbp), %xmm0
	pushq	%rcx
	movq	-352(%rbp), %rax
	movl	$1, %ecx
	movaps	%xmm0, -96(%rbp)
	movq	%r10, %rdi
	movq	-464(%rbp), %xmm0
	movq	%rdx, -416(%rbp)
	leaq	-416(%rbp), %rdx
	movhps	-480(%rbp), %xmm0
	movq	%r10, -456(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -408(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-456(%rbp), %r10
	movq	%rax, %r13
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-488(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	popq	%rax
	popq	%rdx
	jmp	.L437
.L461:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22529:
	.size	_ZN2v88internal30StringPrototypeStrikeAssembler33GenerateStringPrototypeStrikeImplEv, .-_ZN2v88internal30StringPrototypeStrikeAssembler33GenerateStringPrototypeStrikeImplEv
	.section	.rodata._ZN2v88internal8Builtins30Generate_StringPrototypeStrikeEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC44:
	.string	"StringPrototypeStrike"
	.section	.text._ZN2v88internal8Builtins30Generate_StringPrototypeStrikeEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins30Generate_StringPrototypeStrikeEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins30Generate_StringPrototypeStrikeEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins30Generate_StringPrototypeStrikeEPNS0_8compiler18CodeAssemblerStateE:
.LFB22525:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$714, %ecx
	leaq	.LC8(%rip), %rdx
	leaq	.LC44(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$903, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L466
.L463:
	movq	%r13, %rdi
	call	_ZN2v88internal30StringPrototypeStrikeAssembler33GenerateStringPrototypeStrikeImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L467
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L466:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L463
.L467:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22525:
	.size	_ZN2v88internal8Builtins30Generate_StringPrototypeStrikeEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins30Generate_StringPrototypeStrikeEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal27StringPrototypeSubAssembler30GenerateStringPrototypeSubImplEv.str1.1,"aMS",@progbits,1
.LC45:
	.string	"String.prototype.sub"
.LC46:
	.string	"sub"
	.section	.text._ZN2v88internal27StringPrototypeSubAssembler30GenerateStringPrototypeSubImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27StringPrototypeSubAssembler30GenerateStringPrototypeSubImplEv
	.type	_ZN2v88internal27StringPrototypeSubAssembler30GenerateStringPrototypeSubImplEv, @function
_ZN2v88internal27StringPrototypeSubAssembler30GenerateStringPrototypeSubImplEv:
.LFB22538:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$472, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r12, %rcx
	movq	%r13, %rsi
	leaq	-400(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-392(%rbp), %rcx
	movl	$2, %esi
	movq	%r13, %rdi
	movq	-384(%rbp), %rbx
	movq	-400(%rbp), %rax
	movq	%r13, -336(%rbp)
	leaq	-440(%rbp), %r13
	movq	%rcx, -472(%rbp)
	movq	%rbx, -480(%rbp)
	movq	%rbx, -320(%rbp)
	leaq	-288(%rbp), %rbx
	movq	%rcx, -312(%rbp)
	movq	$1, -328(%rbp)
	movq	%rax, -456(%rbp)
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-336(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -488(%rbp)
	movq	%rax, -464(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, %r12
	movq	-440(%rbp), %rax
	movq	$0, -264(%rbp)
	movq	%rax, -288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	-456(%rbp), %xmm1
	movq	%r12, -64(%rbp)
	leaq	-368(%rbp), %r12
	movhps	-472(%rbp), %xmm1
	movaps	%xmm0, -368(%rbp)
	movaps	%xmm1, -96(%rbp)
	movq	-480(%rbp), %xmm1
	movq	$0, -352(%rbp)
	movhps	-464(%rbp), %xmm1
	movaps	%xmm1, -80(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	leaq	40(%rax), %rdx
	movq	%rax, -368(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L469
	call	_ZdlPv@PLT
.L469:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -224(%rbp)
	jne	.L493
.L470:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L472
	call	_ZdlPv@PLT
.L472:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L473
	.p2align 4,,10
	.p2align 3
.L477:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L474
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L477
.L475:
	movq	-280(%rbp), %r12
.L473:
	testq	%r12, %r12
	je	.L478
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L478:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L494
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L474:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L477
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L493:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -352(%rbp)
	movaps	%xmm0, -368(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -368(%rbp)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-368(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L471
	call	_ZdlPv@PLT
.L471:
	movq	(%rbx), %rax
	movl	$118, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %r9
	movq	32(%rax), %rax
	movq	%r9, -504(%rbp)
	movq	%rax, -456(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal15kEmptyString_67EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	movq	%rax, -480(%rbp)
	call	_ZN2v88internal15kEmptyString_67EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$117, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	.LC45(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	.LC46(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -472(%rbp)
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	-432(%rbp), %r10
	movq	%r15, %rsi
	movq	%r10, %rdi
	movq	%rax, -464(%rbp)
	movq	%r10, -496(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-496(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$892, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-496(%rbp), %r10
	movq	-368(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -64(%rbp)
	leaq	-96(%rbp), %rcx
	xorl	%esi, %esi
	movl	$5, %ebx
	movq	-496(%rbp), %r10
	movq	%rax, %r8
	movq	-456(%rbp), %xmm0
	pushq	%rbx
	movq	-504(%rbp), %r9
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdx
	movhps	-472(%rbp), %xmm0
	pushq	%rcx
	movq	-352(%rbp), %rax
	movl	$1, %ecx
	movaps	%xmm0, -96(%rbp)
	movq	%r10, %rdi
	movq	-464(%rbp), %xmm0
	movq	%rdx, -416(%rbp)
	leaq	-416(%rbp), %rdx
	movhps	-480(%rbp), %xmm0
	movq	%r10, -456(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -408(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-456(%rbp), %r10
	movq	%rax, %r12
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-488(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	popq	%rax
	popq	%rdx
	jmp	.L470
.L494:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22538:
	.size	_ZN2v88internal27StringPrototypeSubAssembler30GenerateStringPrototypeSubImplEv, .-_ZN2v88internal27StringPrototypeSubAssembler30GenerateStringPrototypeSubImplEv
	.section	.rodata._ZN2v88internal8Builtins27Generate_StringPrototypeSubEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC47:
	.string	"StringPrototypeSub"
	.section	.text._ZN2v88internal8Builtins27Generate_StringPrototypeSubEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins27Generate_StringPrototypeSubEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins27Generate_StringPrototypeSubEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins27Generate_StringPrototypeSubEPNS0_8compiler18CodeAssemblerStateE:
.LFB22534:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$757, %ecx
	leaq	.LC8(%rip), %rdx
	leaq	.LC47(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$904, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L499
.L496:
	movq	%r13, %rdi
	call	_ZN2v88internal27StringPrototypeSubAssembler30GenerateStringPrototypeSubImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L500
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L499:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L496
.L500:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22534:
	.size	_ZN2v88internal8Builtins27Generate_StringPrototypeSubEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins27Generate_StringPrototypeSubEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal27StringPrototypeSupAssembler30GenerateStringPrototypeSupImplEv.str1.1,"aMS",@progbits,1
.LC48:
	.string	"String.prototype.sup"
.LC49:
	.string	"sup"
	.section	.text._ZN2v88internal27StringPrototypeSupAssembler30GenerateStringPrototypeSupImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27StringPrototypeSupAssembler30GenerateStringPrototypeSupImplEv
	.type	_ZN2v88internal27StringPrototypeSupAssembler30GenerateStringPrototypeSupImplEv, @function
_ZN2v88internal27StringPrototypeSupAssembler30GenerateStringPrototypeSupImplEv:
.LFB22547:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$472, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r12, %rcx
	movq	%r13, %rsi
	leaq	-400(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-392(%rbp), %rcx
	movl	$2, %esi
	movq	%r13, %rdi
	movq	-384(%rbp), %rbx
	movq	-400(%rbp), %rax
	movq	%r13, -336(%rbp)
	leaq	-440(%rbp), %r13
	movq	%rcx, -472(%rbp)
	movq	%rbx, -480(%rbp)
	movq	%rbx, -320(%rbp)
	leaq	-288(%rbp), %rbx
	movq	%rcx, -312(%rbp)
	movq	$1, -328(%rbp)
	movq	%rax, -456(%rbp)
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-336(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -488(%rbp)
	movq	%rax, -464(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, %r12
	movq	-440(%rbp), %rax
	movq	$0, -264(%rbp)
	movq	%rax, -288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	-456(%rbp), %xmm1
	movq	%r12, -64(%rbp)
	leaq	-368(%rbp), %r12
	movhps	-472(%rbp), %xmm1
	movaps	%xmm0, -368(%rbp)
	movaps	%xmm1, -96(%rbp)
	movq	-480(%rbp), %xmm1
	movq	$0, -352(%rbp)
	movhps	-464(%rbp), %xmm1
	movaps	%xmm1, -80(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	leaq	40(%rax), %rdx
	movq	%rax, -368(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L502
	call	_ZdlPv@PLT
.L502:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -224(%rbp)
	jne	.L526
.L503:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L505
	call	_ZdlPv@PLT
.L505:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L506
	.p2align 4,,10
	.p2align 3
.L510:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L507
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L510
.L508:
	movq	-280(%rbp), %r12
.L506:
	testq	%r12, %r12
	je	.L511
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L511:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L527
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L507:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L510
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L526:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -352(%rbp)
	movaps	%xmm0, -368(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -368(%rbp)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-368(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L504
	call	_ZdlPv@PLT
.L504:
	movq	(%rbx), %rax
	movl	$126, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %r9
	movq	32(%rax), %rax
	movq	%r9, -504(%rbp)
	movq	%rax, -456(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal15kEmptyString_67EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	movq	%rax, -480(%rbp)
	call	_ZN2v88internal15kEmptyString_67EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$125, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	.LC48(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	.LC49(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -472(%rbp)
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	-432(%rbp), %r10
	movq	%r15, %rsi
	movq	%r10, %rdi
	movq	%rax, -464(%rbp)
	movq	%r10, -496(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-496(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$892, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-496(%rbp), %r10
	movq	-368(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -64(%rbp)
	leaq	-96(%rbp), %rcx
	xorl	%esi, %esi
	movl	$5, %ebx
	movq	-496(%rbp), %r10
	movq	%rax, %r8
	movq	-456(%rbp), %xmm0
	pushq	%rbx
	movq	-504(%rbp), %r9
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdx
	movhps	-472(%rbp), %xmm0
	pushq	%rcx
	movq	-352(%rbp), %rax
	movl	$1, %ecx
	movaps	%xmm0, -96(%rbp)
	movq	%r10, %rdi
	movq	-464(%rbp), %xmm0
	movq	%rdx, -416(%rbp)
	leaq	-416(%rbp), %rdx
	movhps	-480(%rbp), %xmm0
	movq	%r10, -456(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -408(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-456(%rbp), %r10
	movq	%rax, %r12
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-488(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	popq	%rax
	popq	%rdx
	jmp	.L503
.L527:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22547:
	.size	_ZN2v88internal27StringPrototypeSupAssembler30GenerateStringPrototypeSupImplEv, .-_ZN2v88internal27StringPrototypeSupAssembler30GenerateStringPrototypeSupImplEv
	.section	.rodata._ZN2v88internal8Builtins27Generate_StringPrototypeSupEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC50:
	.string	"StringPrototypeSup"
	.section	.text._ZN2v88internal8Builtins27Generate_StringPrototypeSupEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins27Generate_StringPrototypeSupEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins27Generate_StringPrototypeSupEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins27Generate_StringPrototypeSupEPNS0_8compiler18CodeAssemblerStateE:
.LFB22543:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$799, %ecx
	leaq	.LC8(%rip), %rdx
	leaq	.LC50(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$905, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L532
.L529:
	movq	%r13, %rdi
	call	_ZN2v88internal27StringPrototypeSupAssembler30GenerateStringPrototypeSupImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L533
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L532:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L529
.L533:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22543:
	.size	_ZN2v88internal8Builtins27Generate_StringPrototypeSupEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins27Generate_StringPrototypeSupEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins19Generate_CreateHTMLEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins19Generate_CreateHTMLEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins19Generate_CreateHTMLEPNS0_8compiler18CodeAssemblerStateE:
.LFB29023:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29023:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins19Generate_CreateHTMLEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins19Generate_CreateHTMLEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins19Generate_CreateHTMLEPNS0_8compiler18CodeAssemblerStateE
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
