	.file	"arguments-tq-csa.cc"
	.text
	.section	.rodata._ZN2v88internal27GetArgumentsFrameAndCount_0EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../../deps/v8/../../deps/v8/src/builtins/arguments.tq"
	.section	.text._ZN2v88internal27GetArgumentsFrameAndCount_0EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27GetArgumentsFrameAndCount_0EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEE
	.type	_ZN2v88internal27GetArgumentsFrameAndCount_0EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEE, @function
_ZN2v88internal27GetArgumentsFrameAndCount_0EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEE:
.LFB22410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	-1472(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1600(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1640(%rbp), %r12
	pushq	%rbx
	subq	$1736, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -1672(%rbp)
	movl	$48, %edi
	movq	%rdx, -1728(%rbp)
	movq	%rcx, -1744(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -1640(%rbp)
	movq	%rsi, -1472(%rbp)
	movq	$0, -1464(%rbp)
	movq	$0, -1456(%rbp)
	movq	$0, -1448(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1464(%rbp)
	leaq	-1416(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1448(%rbp)
	movq	%rdx, -1456(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1432(%rbp)
	movq	%rax, -1656(%rbp)
	movq	$0, -1440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1640(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1272(%rbp)
	movq	$0, -1264(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -1256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1272(%rbp)
	leaq	-1224(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1256(%rbp)
	movq	%rdx, -1264(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1240(%rbp)
	movq	%rax, -1712(%rbp)
	movq	$0, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1640(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1080(%rbp)
	movq	$0, -1072(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$0, -1064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1080(%rbp)
	leaq	-1032(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1064(%rbp)
	movq	%rdx, -1072(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1048(%rbp)
	movq	%rax, -1696(%rbp)
	movq	$0, -1056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1640(%rbp), %rax
	movl	$144, %edi
	movq	$0, -888(%rbp)
	movq	$0, -880(%rbp)
	movq	%rax, -896(%rbp)
	movq	$0, -872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -888(%rbp)
	leaq	-840(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -872(%rbp)
	movq	%rdx, -880(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -856(%rbp)
	movq	%rax, -1704(%rbp)
	movq	$0, -864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1640(%rbp), %rax
	movl	$168, %edi
	movq	$0, -696(%rbp)
	movq	$0, -688(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -696(%rbp)
	leaq	-648(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -680(%rbp)
	movq	%rdx, -688(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -664(%rbp)
	movq	%rax, -1688(%rbp)
	movq	$0, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1640(%rbp), %rax
	movl	$120, %edi
	movq	$0, -504(%rbp)
	movq	$0, -496(%rbp)
	movq	%rax, -512(%rbp)
	movq	$0, -488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -504(%rbp)
	leaq	-456(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -488(%rbp)
	movq	%rdx, -496(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -472(%rbp)
	movq	%rax, -1680(%rbp)
	movq	$0, -480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1640(%rbp), %rax
	movl	$120, %edi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -312(%rbp)
	leaq	-264(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	xorl	%edx, %edx
	movq	%rax, -1664(%rbp)
	movups	%xmm0, -280(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1728(%rbp), %xmm1
	movaps	%xmm0, -1600(%rbp)
	movhps	-1744(%rbp), %xmm1
	movq	$0, -1584(%rbp)
	movaps	%xmm1, -1728(%rbp)
	call	_Znwm@PLT
	movdqa	-1728(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1600(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2
	call	_ZdlPv@PLT
.L2:
	movq	-1656(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1408(%rbp)
	jne	.L164
	cmpq	$0, -1216(%rbp)
	jne	.L165
.L8:
	cmpq	$0, -1024(%rbp)
	jne	.L166
.L11:
	cmpq	$0, -832(%rbp)
	jne	.L167
.L14:
	cmpq	$0, -640(%rbp)
	jne	.L168
.L17:
	cmpq	$0, -448(%rbp)
	leaq	-320(%rbp), %r14
	jne	.L169
.L20:
	movq	-1664(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1584(%rbp)
	movaps	%xmm0, -1600(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$84215559, (%rax)
	leaq	5(%rax), %rdx
	movb	$5, 4(%rax)
	movq	%rax, -1600(%rbp)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1600(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L23
	call	_ZdlPv@PLT
.L23:
	movq	(%rbx), %rax
	movq	-1664(%rbp), %rdi
	movdqu	16(%rax), %xmm2
	movq	32(%rax), %rdx
	movq	-1672(%rbp), %rax
	movaps	%xmm2, -1728(%rbp)
	movq	%rdx, 16(%rax)
	movups	%xmm2, (%rax)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L24
	call	_ZdlPv@PLT
.L24:
	movq	-304(%rbp), %rbx
	movq	-312(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L25
	.p2align 4,,10
	.p2align 3
.L29:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L26
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L29
.L27:
	movq	-312(%rbp), %r13
.L25:
	testq	%r13, %r13
	je	.L30
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L30:
	movq	-1680(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L31
	call	_ZdlPv@PLT
.L31:
	movq	-496(%rbp), %rbx
	movq	-504(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L32
	.p2align 4,,10
	.p2align 3
.L36:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L33
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L36
.L34:
	movq	-504(%rbp), %r13
.L32:
	testq	%r13, %r13
	je	.L37
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L37:
	movq	-1688(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L38
	call	_ZdlPv@PLT
.L38:
	movq	-688(%rbp), %rbx
	movq	-696(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L39
	.p2align 4,,10
	.p2align 3
.L43:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L40
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L43
.L41:
	movq	-696(%rbp), %r13
.L39:
	testq	%r13, %r13
	je	.L44
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L44:
	movq	-1704(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-864(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L45
	call	_ZdlPv@PLT
.L45:
	movq	-880(%rbp), %rbx
	movq	-888(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L46
	.p2align 4,,10
	.p2align 3
.L50:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L47
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L50
.L48:
	movq	-888(%rbp), %r13
.L46:
	testq	%r13, %r13
	je	.L51
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L51:
	movq	-1696(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L52
	call	_ZdlPv@PLT
.L52:
	movq	-1072(%rbp), %rbx
	movq	-1080(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L53
	.p2align 4,,10
	.p2align 3
.L57:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L54
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L57
.L55:
	movq	-1080(%rbp), %r13
.L53:
	testq	%r13, %r13
	je	.L58
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L58:
	movq	-1712(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L59
	call	_ZdlPv@PLT
.L59:
	movq	-1264(%rbp), %rbx
	movq	-1272(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L60
	.p2align 4,,10
	.p2align 3
.L64:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L61
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L64
.L62:
	movq	-1272(%rbp), %r13
.L60:
	testq	%r13, %r13
	je	.L65
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L65:
	movq	-1656(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1440(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L66
	call	_ZdlPv@PLT
.L66:
	movq	-1456(%rbp), %rbx
	movq	-1464(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L67
	.p2align 4,,10
	.p2align 3
.L71:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L68
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L71
.L69:
	movq	-1464(%rbp), %r13
.L67:
	testq	%r13, %r13
	je	.L72
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L72:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L170
	movq	-1672(%rbp), %rax
	addq	$1736, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L71
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L61:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L64
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L54:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L57
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L47:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L50
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L40:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L43
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L26:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L29
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L33:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L36
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L164:
	movq	-1656(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1584(%rbp)
	movaps	%xmm0, -1600(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r8d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1600(%rbp)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1600(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L4
	call	_ZdlPv@PLT
.L4:
	movq	(%rbx), %rax
	movl	$37, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	%rcx, -1728(%rbp)
	movq	%rbx, -1752(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler22LoadParentFramePointerEv@PLT
	movq	%r13, %rdi
	movq	%rax, -1744(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$40, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE(%rip), %esi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$42, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$42, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_7Uint16TEvE5valueE(%rip), %esi
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Convert7ATint328ATuint16_181EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7Uint16TEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal26Convert6ATbint7ATint32_206EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6Int32TEEE@PLT
	movl	$41, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -1776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$43, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$46, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1744(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23LoadCallerFromFrame_297EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	-1728(%rbp), %rsi
	call	_ZN2v88internal33Cast23ATArgumentsAdaptorFrame_303EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %xmm7
	pxor	%xmm0, %xmm0
	movq	-1776(%rbp), %xmm4
	movl	$64, %edi
	movaps	%xmm0, -1632(%rbp)
	movq	-1744(%rbp), %xmm5
	movq	-1728(%rbp), %xmm6
	punpcklqdq	%xmm4, %xmm4
	movq	%rbx, -80(%rbp)
	leaq	-1632(%rbp), %r15
	punpcklqdq	%xmm7, %xmm5
	movhps	-1752(%rbp), %xmm6
	movaps	%xmm4, -1776(%rbp)
	movaps	%xmm5, -1744(%rbp)
	movaps	%xmm6, -1728(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -1616(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm7
	movdqa	-80(%rbp), %xmm2
	movq	%r15, %rsi
	leaq	64(%rax), %rdx
	leaq	-1088(%rbp), %rdi
	movq	%rax, -1632(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-112(%rbp), %xmm7
	movups	%xmm2, 48(%rax)
	movups	%xmm7, 16(%rax)
	movdqa	-96(%rbp), %xmm7
	movq	%rdx, -1616(%rbp)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -1624(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1632(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5
	call	_ZdlPv@PLT
.L5:
	movq	-1696(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1592(%rbp)
	jne	.L171
.L6:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1216(%rbp)
	je	.L8
.L165:
	movq	-1712(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1280(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -1584(%rbp)
	movaps	%xmm0, -1600(%rbp)
	call	_Znwm@PLT
	movl	$1285, %edi
	movq	%r13, %rsi
	movw	%di, 4(%rax)
	leaq	7(%rax), %rdx
	movq	%r15, %rdi
	movl	$117769991, (%rax)
	movb	$5, 6(%rax)
	movq	%rax, -1600(%rbp)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1600(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L9
	call	_ZdlPv@PLT
.L9:
	movq	(%rbx), %rax
	movl	$48, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movdqu	(%rax), %xmm3
	movq	$0, -1584(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -1600(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	leaq	48(%rax), %rdx
	leaq	-896(%rbp), %rdi
	movq	%rax, -1600(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm3, 32(%rax)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	movq	-1704(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1024(%rbp)
	je	.L11
.L166:
	movq	-1696(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1088(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -1584(%rbp)
	movaps	%xmm0, -1600(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movabsq	$361700864223938311, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -1600(%rbp)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1600(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L12
	call	_ZdlPv@PLT
.L12:
	movq	(%rbx), %rax
	movl	$56, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movq	56(%rax), %rdx
	movdqu	(%rax), %xmm4
	movq	$0, -1584(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -80(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -1600(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	56(%rax), %rdx
	leaq	-704(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rax, -1600(%rbp)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L13
	call	_ZdlPv@PLT
.L13:
	movq	-1688(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -832(%rbp)
	je	.L14
.L167:
	movq	-1704(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-896(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -1584(%rbp)
	movaps	%xmm0, -1600(%rbp)
	call	_Znwm@PLT
	movl	$1285, %esi
	movq	%r15, %rdi
	movw	%si, 4(%rax)
	leaq	6(%rax), %rdx
	movq	%r13, %rsi
	movl	$117769991, (%rax)
	movq	%rax, -1600(%rbp)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1600(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L15
	call	_ZdlPv@PLT
.L15:
	movq	(%rbx), %rax
	movl	$48, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	32(%rax), %rbx
	movq	%rcx, -1728(%rbp)
	movq	16(%rax), %rcx
	movq	40(%rax), %rax
	movq	%rcx, -1744(%rbp)
	movq	%rax, -1752(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$49, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$50, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$47, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %xmm0
	movl	$40, %edi
	movq	%rbx, -96(%rbp)
	movhps	-1728(%rbp), %xmm0
	movq	$0, -1584(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	-1744(%rbp), %xmm0
	movhps	-1752(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1600(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm2
	movdqa	-112(%rbp), %xmm3
	leaq	40(%rax), %rdx
	leaq	-512(%rbp), %rdi
	movq	%rax, -1600(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L16
	call	_ZdlPv@PLT
.L16:
	movq	-1680(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -640(%rbp)
	je	.L17
.L168:
	movq	-1688(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-704(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -1584(%rbp)
	movaps	%xmm0, -1600(%rbp)
	call	_Znwm@PLT
	movl	$1285, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117769991, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$5, 6(%rax)
	movq	%rax, -1600(%rbp)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1600(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L18
	call	_ZdlPv@PLT
.L18:
	movq	(%rbx), %rax
	movl	$45, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	48(%rax), %rbx
	movq	%rcx, -1728(%rbp)
	movq	32(%rax), %rcx
	movq	%rcx, -1752(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$54, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal30LoadLengthFromAdapterFrame_300EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal24Convert6ATbint5ATSmi_208EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$56, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -1744(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$53, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %xmm0
	movl	$40, %edi
	movq	-1752(%rbp), %rcx
	movhps	-1728(%rbp), %xmm0
	movq	$0, -1584(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	%rbx, %xmm0
	movhps	-1744(%rbp), %xmm0
	movq	%rcx, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1600(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm5
	leaq	40(%rax), %rdx
	leaq	-512(%rbp), %rdi
	movq	%rax, -1600(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L19
	call	_ZdlPv@PLT
.L19:
	movq	-1680(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L169:
	movq	-1680(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-512(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1584(%rbp)
	movaps	%xmm0, -1600(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$84215559, (%rax)
	leaq	5(%rax), %rdx
	movb	$5, 4(%rax)
	movq	%rax, -1600(%rbp)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1600(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L21
	call	_ZdlPv@PLT
.L21:
	movq	(%rbx), %rax
	movl	$34, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	16(%rax), %rbx
	movq	32(%rax), %r15
	movq	%rcx, -1728(%rbp)
	movq	24(%rax), %rcx
	movq	%rcx, -1744(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %xmm0
	movl	$40, %edi
	movq	%r15, -96(%rbp)
	movhps	-1728(%rbp), %xmm0
	leaq	-320(%rbp), %r14
	movq	$0, -1584(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	%rbx, %xmm0
	movhps	-1744(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1600(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	leaq	40(%rax), %rdx
	movq	%rax, -1600(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L22
	call	_ZdlPv@PLT
.L22:
	movq	-1664(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L171:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movdqa	-1728(%rbp), %xmm2
	movdqa	-1744(%rbp), %xmm3
	movq	%rbx, -80(%rbp)
	movaps	%xmm2, -128(%rbp)
	movdqa	-1776(%rbp), %xmm2
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -1632(%rbp)
	movq	$0, -1616(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm3
	movq	-80(%rbp), %rcx
	leaq	-1280(%rbp), %rdi
	movdqa	-112(%rbp), %xmm2
	leaq	56(%rax), %rdx
	movq	%r15, %rsi
	movq	%rax, -1632(%rbp)
	movups	%xmm3, (%rax)
	movdqa	-96(%rbp), %xmm3
	movq	%rcx, 48(%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm3, 32(%rax)
	movq	%rdx, -1616(%rbp)
	movq	%rdx, -1624(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1632(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L7
	call	_ZdlPv@PLT
.L7:
	movq	-1712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L6
.L170:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22410:
	.size	_ZN2v88internal27GetArgumentsFrameAndCount_0EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEE, .-_ZN2v88internal27GetArgumentsFrameAndCount_0EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal27GetArgumentsFrameAndCount_0EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal27GetArgumentsFrameAndCount_0EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEE, @function
_GLOBAL__sub_I__ZN2v88internal27GetArgumentsFrameAndCount_0EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEE:
.LFB28932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28932:
	.size	_GLOBAL__sub_I__ZN2v88internal27GetArgumentsFrameAndCount_0EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEE, .-_GLOBAL__sub_I__ZN2v88internal27GetArgumentsFrameAndCount_0EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal27GetArgumentsFrameAndCount_0EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEE
	.weak	_ZN2v88internal13MachineTypeOfINS0_7Uint16TEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_7Uint16TEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_7Uint16TEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_7Uint16TEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_7Uint16TEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_7Uint16TEvE5valueE:
	.byte	3
	.byte	3
	.weak	_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE:
	.byte	7
	.byte	7
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
