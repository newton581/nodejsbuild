	.file	"typed-array-subarray-tq-csa.cc"
	.text
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0:
.LFB29351:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	%rsi, %r13
	pushq	%rbx
	movq	%r13, %rax
	sarq	$3, %rax
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%r13, %r13
	js	.L9
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%rdx, %r14
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.L3
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%rax, %rdi
.L3:
	leaq	(%rdi,%r13), %r15
	movq	%rdi, (%rbx)
	movq	%r15, 16(%rbx)
	cmpq	%r12, %r14
	je	.L4
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
.L4:
	movq	%r15, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L9:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29351:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev,"axG",@progbits,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev:
.LFB22421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$56, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L11
	call	_ZdlPv@PLT
.L11:
	movq	16(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	%r12, %r13
	je	.L12
	.p2align 4,,10
	.p2align 3
.L16:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L13
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L16
.L14:
	movq	8(%rbx), %r12
.L12:
	testq	%r12, %r12
	je	.L10
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L16
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L10:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22421:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev
	.set	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_:
.LFB26662:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$9, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$362266026085516549, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L23
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
.L23:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L24
	movq	%rdx, (%r15)
.L24:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L25
	movq	%rdx, (%r14)
.L25:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L26
	movq	%rdx, 0(%r13)
.L26:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L27
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L27:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L28
	movq	%rdx, (%rbx)
.L28:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L29
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L29:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L30
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L30:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L31
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L31:
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L22
	movq	-120(%rbp), %rbx
	movq	%rax, (%rbx)
.L22:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L65:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26662:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_:
.LFB26664:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$10, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$362266026085516549, %rcx
	movq	%rcx, (%rax)
	movl	$1288, %ecx
	leaq	10(%rax), %rdx
	movw	%cx, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L67
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %rax
.L67:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L68
	movq	%rdx, (%r15)
.L68:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L69
	movq	%rdx, (%r14)
.L69:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L70
	movq	%rdx, 0(%r13)
.L70:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L71
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L71:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L72
	movq	%rdx, (%rbx)
.L72:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L73
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L73:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L74
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L74:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L75
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L75:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L76
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L76:
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L66
	movq	-128(%rbp), %rsi
	movq	%rax, (%rsi)
.L66:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L113
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L113:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26664:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_SI_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_SI_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_SI_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_SI_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_SI_:
.LFB26666:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$11, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$362266026085516549, %rcx
	movq	%rcx, (%rax)
	movl	$1288, %ecx
	leaq	11(%rax), %rdx
	movw	%cx, 8(%rax)
	movb	$8, 10(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L115
	movq	%rax, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %rax
.L115:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L116
	movq	%rdx, (%r15)
.L116:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L117
	movq	%rdx, (%r14)
.L117:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L118
	movq	%rdx, 0(%r13)
.L118:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L119
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L119:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L120
	movq	%rdx, (%rbx)
.L120:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L121
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L121:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L122
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L122:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L123
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L123:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L124
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L124:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L125
	movq	-128(%rbp), %rsi
	movq	%rdx, (%rsi)
.L125:
	movq	80(%rax), %rax
	testq	%rax, %rax
	je	.L114
	movq	-136(%rbp), %rcx
	movq	%rax, (%rcx)
.L114:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L165
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L165:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26666:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_SI_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_SI_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_SI_SE_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_SI_SE_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_SI_SE_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_SI_SE_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_SI_SE_:
.LFB26668:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$12, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$120, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$362266026085516549, %rcx
	movq	%rcx, (%rax)
	leaq	12(%rax), %rdx
	movl	$84411656, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L167
	movq	%rax, -152(%rbp)
	call	_ZdlPv@PLT
	movq	-152(%rbp), %rax
.L167:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L168
	movq	%rdx, (%r15)
.L168:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L169
	movq	%rdx, (%r14)
.L169:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L170
	movq	%rdx, 0(%r13)
.L170:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L171
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L171:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L172
	movq	%rdx, (%rbx)
.L172:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L173
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L173:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L174
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L174:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L175
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L175:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L176
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L176:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L177
	movq	-128(%rbp), %rsi
	movq	%rdx, (%rsi)
.L177:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L178
	movq	-136(%rbp), %rcx
	movq	%rdx, (%rcx)
.L178:
	movq	88(%rax), %rax
	testq	%rax, %rax
	je	.L166
	movq	-144(%rbp), %rbx
	movq	%rax, (%rbx)
.L166:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L221
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L221:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26668:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_SI_SE_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_SI_SE_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_S4_NS0_3SmiENS0_8UintPtrTENS0_6Int32TESA_SA_SA_SB_S9_S9_SA_SA_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EESH_SL_SH_SL_SH_PNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EEST_ST_ST_SV_SR_SR_ST_ST_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_S4_NS0_3SmiENS0_8UintPtrTENS0_6Int32TESA_SA_SA_SB_S9_S9_SA_SA_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EESH_SL_SH_SL_SH_PNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EEST_ST_ST_SV_SR_SR_ST_ST_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_S4_NS0_3SmiENS0_8UintPtrTENS0_6Int32TESA_SA_SA_SB_S9_S9_SA_SA_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EESH_SL_SH_SL_SH_PNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EEST_ST_ST_SV_SR_SR_ST_ST_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_S4_NS0_3SmiENS0_8UintPtrTENS0_6Int32TESA_SA_SA_SB_S9_S9_SA_SA_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EESH_SL_SH_SL_SH_PNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EEST_ST_ST_SV_SR_SR_ST_ST_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_S4_NS0_3SmiENS0_8UintPtrTENS0_6Int32TESA_SA_SA_SB_S9_S9_SA_SA_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EESH_SL_SH_SL_SH_PNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EEST_ST_ST_SV_SR_SR_ST_ST_:
.LFB26687:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$23, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	88(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	96(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	104(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	112(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	120(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	128(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	136(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	144(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	152(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$1286, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movdqa	.LC1(%rip), %xmm0
	movl	$100926725, 16(%rax)
	leaq	23(%rax), %rdx
	movw	%cx, 20(%rax)
	movb	$5, 22(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L223
	movq	%rax, -240(%rbp)
	call	_ZdlPv@PLT
	movq	-240(%rbp), %rax
.L223:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L224
	movq	%rdx, (%r15)
.L224:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L225
	movq	%rdx, (%r14)
.L225:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L226
	movq	%rdx, 0(%r13)
.L226:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L227
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L227:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L228
	movq	%rdx, (%rbx)
.L228:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L229
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L229:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L230
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L230:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L231
	movq	-112(%rbp), %rcx
	movq	%rdx, (%rcx)
.L231:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L232
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L232:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L233
	movq	-128(%rbp), %rcx
	movq	%rdx, (%rcx)
.L233:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L234
	movq	-136(%rbp), %rbx
	movq	%rdx, (%rbx)
.L234:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L235
	movq	-144(%rbp), %rsi
	movq	%rdx, (%rsi)
.L235:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L236
	movq	-152(%rbp), %rcx
	movq	%rdx, (%rcx)
.L236:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L237
	movq	-160(%rbp), %rbx
	movq	%rdx, (%rbx)
.L237:
	movq	112(%rax), %rdx
	testq	%rdx, %rdx
	je	.L238
	movq	-168(%rbp), %rsi
	movq	%rdx, (%rsi)
.L238:
	movq	120(%rax), %rdx
	testq	%rdx, %rdx
	je	.L239
	movq	-176(%rbp), %rdi
	movq	%rdx, (%rdi)
.L239:
	movq	128(%rax), %rdx
	testq	%rdx, %rdx
	je	.L240
	movq	-184(%rbp), %rcx
	movq	%rdx, (%rcx)
.L240:
	movq	136(%rax), %rdx
	testq	%rdx, %rdx
	je	.L241
	movq	-192(%rbp), %rbx
	movq	%rdx, (%rbx)
.L241:
	movq	144(%rax), %rdx
	testq	%rdx, %rdx
	je	.L242
	movq	-200(%rbp), %rsi
	movq	%rdx, (%rsi)
.L242:
	movq	152(%rax), %rdx
	testq	%rdx, %rdx
	je	.L243
	movq	-208(%rbp), %rdi
	movq	%rdx, (%rdi)
.L243:
	movq	160(%rax), %rdx
	testq	%rdx, %rdx
	je	.L244
	movq	-216(%rbp), %rcx
	movq	%rdx, (%rcx)
.L244:
	movq	168(%rax), %rdx
	testq	%rdx, %rdx
	je	.L245
	movq	-224(%rbp), %rbx
	movq	%rdx, (%rbx)
.L245:
	movq	176(%rax), %rax
	testq	%rax, %rax
	je	.L222
	movq	-232(%rbp), %rsi
	movq	%rax, (%rsi)
.L222:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L321
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L321:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26687:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_S4_NS0_3SmiENS0_8UintPtrTENS0_6Int32TESA_SA_SA_SB_S9_S9_SA_SA_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EESH_SL_SH_SL_SH_PNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EEST_ST_ST_SV_SR_SR_ST_ST_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_S4_NS0_3SmiENS0_8UintPtrTENS0_6Int32TESA_SA_SA_SB_S9_S9_SA_SA_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EESH_SL_SH_SL_SH_PNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EEST_ST_ST_SV_SR_SR_ST_ST_
	.section	.rodata._ZN2v88internal36TypedArrayPrototypeSubArrayAssembler39GenerateTypedArrayPrototypeSubArrayImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../../deps/v8/../../deps/v8/src/builtins/typed-array-subarray.tq"
	.align 8
.LC3:
	.string	"%TypedArray%.prototype.subarray"
	.align 8
.LC4:
	.string	"../../deps/v8/../../deps/v8/src/builtins/typed-array.tq"
	.section	.text._ZN2v88internal36TypedArrayPrototypeSubArrayAssembler39GenerateTypedArrayPrototypeSubArrayImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal36TypedArrayPrototypeSubArrayAssembler39GenerateTypedArrayPrototypeSubArrayImplEv
	.type	_ZN2v88internal36TypedArrayPrototypeSubArrayAssembler39GenerateTypedArrayPrototypeSubArrayImplEv, @function
_ZN2v88internal36TypedArrayPrototypeSubArrayAssembler39GenerateTypedArrayPrototypeSubArrayImplEv:
.LFB22418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$424, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$1, %esi
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %r14
	movq	%r13, -4160(%rbp)
	leaq	-4160(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r12, %rcx
	movq	%r14, %rsi
	leaq	-3968(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-3968(%rbp), %r12
	movl	$2, %esi
	movq	%r14, %rdi
	movq	-3960(%rbp), %rax
	movq	-3952(%rbp), %rbx
	movq	%r14, -3872(%rbp)
	movq	%r12, -3840(%rbp)
	movq	%rax, -4304(%rbp)
	movq	$1, -3864(%rbp)
	movq	%rbx, -3856(%rbp)
	movq	%rax, -3848(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rax, -4192(%rbp)
	leaq	-3872(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -4384(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -3688(%rbp)
	movq	$0, -3680(%rbp)
	movq	%rax, %r14
	movq	-4160(%rbp), %rax
	movq	$0, -3672(%rbp)
	movq	%rax, -3696(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -3688(%rbp)
	leaq	-3640(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3672(%rbp)
	movq	%rdx, -3680(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3656(%rbp)
	movq	%rax, -4168(%rbp)
	movq	$0, -3664(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4160(%rbp), %rax
	movl	$144, %edi
	movq	$0, -3496(%rbp)
	movq	$0, -3488(%rbp)
	movq	%rax, -3504(%rbp)
	movq	$0, -3480(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -3496(%rbp)
	leaq	-3448(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3480(%rbp)
	movq	%rdx, -3488(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3464(%rbp)
	movq	%rax, -4288(%rbp)
	movq	$0, -3472(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4160(%rbp), %rax
	movl	$168, %edi
	movq	$0, -3304(%rbp)
	movq	$0, -3296(%rbp)
	movq	%rax, -3312(%rbp)
	movq	$0, -3288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -3304(%rbp)
	leaq	-3256(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3288(%rbp)
	movq	%rdx, -3296(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3272(%rbp)
	movq	%rax, -4200(%rbp)
	movq	$0, -3280(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4160(%rbp), %rax
	movl	$120, %edi
	movq	$0, -3112(%rbp)
	movq	$0, -3104(%rbp)
	movq	%rax, -3120(%rbp)
	movq	$0, -3096(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -3112(%rbp)
	leaq	-3064(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3096(%rbp)
	movq	%rdx, -3104(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3080(%rbp)
	movq	%rax, -4208(%rbp)
	movq	$0, -3088(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4160(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2920(%rbp)
	movq	$0, -2912(%rbp)
	movq	%rax, -2928(%rbp)
	movq	$0, -2904(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2920(%rbp)
	leaq	-2872(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2904(%rbp)
	movq	%rdx, -2912(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2888(%rbp)
	movq	%rax, -4216(%rbp)
	movq	$0, -2896(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4160(%rbp), %rax
	movl	$216, %edi
	movq	$0, -2728(%rbp)
	movq	$0, -2720(%rbp)
	movq	%rax, -2736(%rbp)
	movq	$0, -2712(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -2728(%rbp)
	leaq	-2680(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2712(%rbp)
	movq	%rdx, -2720(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2696(%rbp)
	movq	%rax, -4224(%rbp)
	movq	$0, -2704(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4160(%rbp), %rax
	movl	$216, %edi
	movq	$0, -2536(%rbp)
	movq	$0, -2528(%rbp)
	movq	%rax, -2544(%rbp)
	movq	$0, -2520(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -2536(%rbp)
	leaq	-2488(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2520(%rbp)
	movq	%rdx, -2528(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2504(%rbp)
	movq	%rax, -4232(%rbp)
	movq	$0, -2512(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4160(%rbp), %rax
	movl	$240, %edi
	movq	$0, -2344(%rbp)
	movq	$0, -2336(%rbp)
	movq	%rax, -2352(%rbp)
	movq	$0, -2328(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -2344(%rbp)
	leaq	-2296(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2328(%rbp)
	movq	%rdx, -2336(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2312(%rbp)
	movq	%rax, -4248(%rbp)
	movq	$0, -2320(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4160(%rbp), %rax
	movl	$240, %edi
	movq	$0, -2152(%rbp)
	movq	$0, -2144(%rbp)
	movq	%rax, -2160(%rbp)
	movq	$0, -2136(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -2152(%rbp)
	leaq	-2104(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2136(%rbp)
	movq	%rdx, -2144(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2120(%rbp)
	movq	%rax, -4336(%rbp)
	movq	$0, -2128(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4160(%rbp), %rax
	movl	$264, %edi
	movq	$0, -1960(%rbp)
	movq	$0, -1952(%rbp)
	movq	%rax, -1968(%rbp)
	movq	$0, -1944(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -1960(%rbp)
	leaq	-1912(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1944(%rbp)
	movq	%rdx, -1952(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1928(%rbp)
	movq	%rax, -4368(%rbp)
	movq	$0, -1936(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4160(%rbp), %rax
	movl	$264, %edi
	movq	$0, -1768(%rbp)
	movq	$0, -1760(%rbp)
	movq	%rax, -1776(%rbp)
	movq	$0, -1752(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -1768(%rbp)
	leaq	-1720(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1752(%rbp)
	movq	%rdx, -1760(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1736(%rbp)
	movq	%rax, -4376(%rbp)
	movq	$0, -1744(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4160(%rbp), %rax
	movl	$288, %edi
	movq	$0, -1576(%rbp)
	movq	$0, -1568(%rbp)
	movq	%rax, -1584(%rbp)
	movq	$0, -1560(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -1576(%rbp)
	leaq	-1528(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1560(%rbp)
	movq	%rdx, -1568(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1544(%rbp)
	movq	%rax, -4352(%rbp)
	movq	$0, -1552(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4160(%rbp), %rax
	movl	$288, %edi
	movq	$0, -1384(%rbp)
	movq	$0, -1376(%rbp)
	movq	%rax, -1392(%rbp)
	movq	$0, -1368(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -1384(%rbp)
	leaq	-1336(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1368(%rbp)
	movq	%rdx, -1376(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1352(%rbp)
	movq	%rax, -4176(%rbp)
	movq	$0, -1360(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4160(%rbp), %rax
	movl	$552, %edi
	movq	$0, -1192(%rbp)
	movq	$0, -1184(%rbp)
	movq	%rax, -1200(%rbp)
	movq	$0, -1176(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	552(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -1192(%rbp)
	movq	%rdx, -1176(%rbp)
	movups	%xmm0, 480(%rax)
	movq	$0, 544(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	leaq	-1144(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1184(%rbp)
	xorl	%edx, %edx
	movq	%rax, -4240(%rbp)
	movq	$0, -1168(%rbp)
	movups	%xmm0, -1160(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4160(%rbp), %rax
	movl	$552, %edi
	movq	$0, -1000(%rbp)
	movq	$0, -992(%rbp)
	movq	%rax, -1008(%rbp)
	movq	$0, -984(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	552(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -1000(%rbp)
	movq	%rdx, -984(%rbp)
	movups	%xmm0, 480(%rax)
	movq	$0, 544(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	leaq	-952(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -992(%rbp)
	xorl	%edx, %edx
	movq	%rax, -4256(%rbp)
	movq	$0, -976(%rbp)
	movups	%xmm0, -968(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4160(%rbp), %rax
	movl	$528, %edi
	movq	$0, -808(%rbp)
	movq	$0, -800(%rbp)
	movq	%rax, -816(%rbp)
	movq	$0, -792(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	528(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -808(%rbp)
	movq	%rdx, -792(%rbp)
	movups	%xmm0, 480(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	leaq	-760(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -800(%rbp)
	xorl	%edx, %edx
	movq	%rax, -4264(%rbp)
	movq	$0, -784(%rbp)
	movups	%xmm0, -776(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4160(%rbp), %rax
	movl	$408, %edi
	movq	$0, -616(%rbp)
	movq	$0, -608(%rbp)
	movq	%rax, -624(%rbp)
	movq	$0, -600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	408(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 400(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movq	%rax, -616(%rbp)
	leaq	-568(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -600(%rbp)
	movq	%rdx, -608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -584(%rbp)
	movq	%rax, -4272(%rbp)
	movq	$0, -592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4160(%rbp), %rax
	movl	$432, %edi
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	%rax, -432(%rbp)
	movq	$0, -408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	432(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movq	%rax, -424(%rbp)
	leaq	-376(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -408(%rbp)
	movq	%rdx, -416(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -392(%rbp)
	movq	$0, -400(%rbp)
	movq	%rax, -4280(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %xmm1
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movhps	-4304(%rbp), %xmm1
	movq	%r14, -208(%rbp)
	leaq	-3696(%rbp), %r12
	leaq	-3824(%rbp), %r14
	movaps	%xmm1, -240(%rbp)
	movq	%rbx, %xmm1
	movhps	-4192(%rbp), %xmm1
	movaps	%xmm0, -3824(%rbp)
	movaps	%xmm1, -224(%rbp)
	movq	$0, -3808(%rbp)
	call	_Znwm@PLT
	movq	-208(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movdqa	-240(%rbp), %xmm7
	movdqa	-224(%rbp), %xmm5
	leaq	40(%rax), %rdx
	movq	%rax, -3824(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -3808(%rbp)
	movq	%rdx, -3816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L323
	call	_ZdlPv@PLT
.L323:
	movq	-4168(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3632(%rbp)
	jne	.L647
	cmpq	$0, -3440(%rbp)
	jne	.L648
.L329:
	cmpq	$0, -3248(%rbp)
	jne	.L649
.L332:
	cmpq	$0, -3056(%rbp)
	jne	.L650
.L335:
	cmpq	$0, -2864(%rbp)
	jne	.L651
.L337:
	cmpq	$0, -2672(%rbp)
	jne	.L652
.L341:
	leaq	-2160(%rbp), %rax
	cmpq	$0, -2480(%rbp)
	movq	%rax, -4192(%rbp)
	jne	.L653
	cmpq	$0, -2288(%rbp)
	jne	.L654
.L345:
	leaq	-1968(%rbp), %rax
	cmpq	$0, -2096(%rbp)
	movq	%rax, -4304(%rbp)
	leaq	-1776(%rbp), %rax
	movq	%rax, -4320(%rbp)
	jne	.L655
.L347:
	leaq	-1584(%rbp), %rax
	cmpq	$0, -1904(%rbp)
	movq	%rax, -4336(%rbp)
	jne	.L656
	cmpq	$0, -1712(%rbp)
	jne	.L657
.L352:
	cmpq	$0, -1520(%rbp)
	jne	.L658
.L354:
	cmpq	$0, -1328(%rbp)
	jne	.L659
.L356:
	cmpq	$0, -1136(%rbp)
	jne	.L660
.L359:
	cmpq	$0, -944(%rbp)
	jne	.L661
.L361:
	cmpq	$0, -752(%rbp)
	jne	.L662
.L363:
	cmpq	$0, -560(%rbp)
	jne	.L663
.L366:
	cmpq	$0, -368(%rbp)
	jne	.L664
.L368:
	movq	-4280(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L372
	call	_ZdlPv@PLT
.L372:
	movq	-416(%rbp), %rbx
	movq	-424(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L373
	.p2align 4,,10
	.p2align 3
.L377:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L374
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L377
.L375:
	movq	-424(%rbp), %r12
.L373:
	testq	%r12, %r12
	je	.L378
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L378:
	movq	-4272(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L379
	call	_ZdlPv@PLT
.L379:
	movq	-608(%rbp), %rbx
	movq	-616(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L380
	.p2align 4,,10
	.p2align 3
.L384:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L381
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L384
.L382:
	movq	-616(%rbp), %r12
.L380:
	testq	%r12, %r12
	je	.L385
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L385:
	movq	-4264(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L386
	call	_ZdlPv@PLT
.L386:
	movq	-800(%rbp), %rbx
	movq	-808(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L387
	.p2align 4,,10
	.p2align 3
.L391:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L388
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L391
.L389:
	movq	-808(%rbp), %r12
.L387:
	testq	%r12, %r12
	je	.L392
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L392:
	movq	-4256(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-976(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L393
	call	_ZdlPv@PLT
.L393:
	movq	-992(%rbp), %rbx
	movq	-1000(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L394
	.p2align 4,,10
	.p2align 3
.L398:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L395
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L398
.L396:
	movq	-1000(%rbp), %r12
.L394:
	testq	%r12, %r12
	je	.L399
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L399:
	movq	-4240(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L400
	call	_ZdlPv@PLT
.L400:
	movq	-1184(%rbp), %rbx
	movq	-1192(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L401
	.p2align 4,,10
	.p2align 3
.L405:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L402
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L405
.L403:
	movq	-1192(%rbp), %r12
.L401:
	testq	%r12, %r12
	je	.L406
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L406:
	movq	-4176(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1360(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L407
	call	_ZdlPv@PLT
.L407:
	movq	-1376(%rbp), %rbx
	movq	-1384(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L408
	.p2align 4,,10
	.p2align 3
.L412:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L409
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L412
.L410:
	movq	-1384(%rbp), %r12
.L408:
	testq	%r12, %r12
	je	.L413
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L413:
	movq	-4336(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4320(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4304(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4192(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4248(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2320(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L414
	call	_ZdlPv@PLT
.L414:
	movq	-2336(%rbp), %rbx
	movq	-2344(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L415
	.p2align 4,,10
	.p2align 3
.L419:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L416
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L419
.L417:
	movq	-2344(%rbp), %r12
.L415:
	testq	%r12, %r12
	je	.L420
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L420:
	movq	-4232(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2512(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L421
	call	_ZdlPv@PLT
.L421:
	movq	-2528(%rbp), %rbx
	movq	-2536(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L422
	.p2align 4,,10
	.p2align 3
.L426:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L423
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L426
.L424:
	movq	-2536(%rbp), %r12
.L422:
	testq	%r12, %r12
	je	.L427
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L427:
	movq	-4224(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L428
	call	_ZdlPv@PLT
.L428:
	movq	-2720(%rbp), %rbx
	movq	-2728(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L429
	.p2align 4,,10
	.p2align 3
.L433:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L430
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L433
.L431:
	movq	-2728(%rbp), %r12
.L429:
	testq	%r12, %r12
	je	.L434
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L434:
	movq	-4216(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2896(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L435
	call	_ZdlPv@PLT
.L435:
	movq	-2912(%rbp), %rbx
	movq	-2920(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L436
	.p2align 4,,10
	.p2align 3
.L440:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L437
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L440
.L438:
	movq	-2920(%rbp), %r12
.L436:
	testq	%r12, %r12
	je	.L441
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L441:
	movq	-4208(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L442
	call	_ZdlPv@PLT
.L442:
	movq	-3104(%rbp), %rbx
	movq	-3112(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L443
	.p2align 4,,10
	.p2align 3
.L447:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L444
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L447
.L445:
	movq	-3112(%rbp), %r12
.L443:
	testq	%r12, %r12
	je	.L448
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L448:
	movq	-4200(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L449
	call	_ZdlPv@PLT
.L449:
	movq	-3296(%rbp), %rbx
	movq	-3304(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L450
	.p2align 4,,10
	.p2align 3
.L454:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L451
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L454
.L452:
	movq	-3304(%rbp), %r12
.L450:
	testq	%r12, %r12
	je	.L455
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L455:
	movq	-4288(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3472(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L456
	call	_ZdlPv@PLT
.L456:
	movq	-3488(%rbp), %rbx
	movq	-3496(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L457
	.p2align 4,,10
	.p2align 3
.L461:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L458
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L461
.L459:
	movq	-3496(%rbp), %r12
.L457:
	testq	%r12, %r12
	je	.L462
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L462:
	movq	-4168(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L463
	call	_ZdlPv@PLT
.L463:
	movq	-3680(%rbp), %rbx
	movq	-3688(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L464
	.p2align 4,,10
	.p2align 3
.L468:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L465
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L468
.L466:
	movq	-3688(%rbp), %r12
.L464:
	testq	%r12, %r12
	je	.L469
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L469:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L665
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L465:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L468
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L458:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L461
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L451:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L454
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L444:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L447
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L437:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L440
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L430:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L433
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L423:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L426
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L416:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L419
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L409:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L412
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L402:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L405
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L395:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L398
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L388:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L391
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L374:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L377
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L381:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L384
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L647:
	movq	-4168(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -3808(%rbp)
	movaps	%xmm0, -3824(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -3824(%rbp)
	movq	%rdx, -3808(%rbp)
	movq	%rdx, -3816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3824(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L325
	call	_ZdlPv@PLT
.L325:
	movq	(%rbx), %rax
	movl	$15, %edx
	movq	%r15, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	24(%rax), %r12
	movq	32(%rax), %rbx
	movq	%rsi, -4304(%rbp)
	movq	16(%rax), %rsi
	movq	%rcx, -4192(%rbp)
	movq	%rsi, -4320(%rbp)
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rsi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal22Cast12JSTypedArray_110EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %xmm6
	movq	%rbx, %xmm7
	movq	-4320(%rbp), %xmm4
	punpcklqdq	%xmm7, %xmm7
	pxor	%xmm0, %xmm0
	movq	-4192(%rbp), %xmm5
	movl	$56, %edi
	punpcklqdq	%xmm6, %xmm4
	movaps	%xmm7, -4400(%rbp)
	leaq	-3904(%rbp), %r12
	movhps	-4304(%rbp), %xmm5
	movaps	%xmm4, -4320(%rbp)
	movaps	%xmm5, -4192(%rbp)
	movaps	%xmm5, -240(%rbp)
	movaps	%xmm4, -224(%rbp)
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm0, -3904(%rbp)
	movq	%rax, -192(%rbp)
	movq	$0, -3888(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm6
	movq	%r12, %rsi
	movq	-192(%rbp), %rcx
	movdqa	-208(%rbp), %xmm7
	leaq	56(%rax), %rdx
	leaq	-3312(%rbp), %rdi
	movq	%rax, -3904(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-224(%rbp), %xmm6
	movq	%rcx, 48(%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3904(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L326
	call	_ZdlPv@PLT
.L326:
	movq	-4200(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3816(%rbp)
	jne	.L666
.L327:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -3440(%rbp)
	je	.L329
.L648:
	movq	-4288(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-3504(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -3808(%rbp)
	movaps	%xmm0, -3824(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r9d
	movq	%r12, %rdi
	movq	%r14, %rsi
	movl	$117769477, (%rax)
	leaq	6(%rax), %rdx
	movw	%r9w, 4(%rax)
	movq	%rax, -3824(%rbp)
	movq	%rdx, -3808(%rbp)
	movq	%rdx, -3816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3824(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L330
	call	_ZdlPv@PLT
.L330:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	32(%rax), %rdx
	movdqu	(%rax), %xmm6
	movq	$0, -3808(%rbp)
	movaps	%xmm0, -224(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -208(%rbp)
	movaps	%xmm6, -240(%rbp)
	movaps	%xmm0, -3824(%rbp)
	call	_Znwm@PLT
	movq	-208(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-240(%rbp), %xmm7
	movdqa	-224(%rbp), %xmm6
	leaq	40(%rax), %rdx
	leaq	-3120(%rbp), %rdi
	movq	%rax, -3824(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -3808(%rbp)
	movq	%rdx, -3816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L331
	call	_ZdlPv@PLT
.L331:
	movq	-4208(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3248(%rbp)
	je	.L332
.L649:
	movq	-4200(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-3312(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -3808(%rbp)
	movaps	%xmm0, -3824(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r8d
	movq	%r12, %rdi
	movq	%r14, %rsi
	movl	$117769477, (%rax)
	leaq	7(%rax), %rdx
	movw	%r8w, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -3824(%rbp)
	movq	%rdx, -3808(%rbp)
	movq	%rdx, -3816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3824(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L333
	call	_ZdlPv@PLT
.L333:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	16(%rax), %rdi
	movq	8(%rax), %r8
	movq	24(%rax), %rsi
	movq	32(%rax), %rcx
	movq	48(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdi, -224(%rbp)
	movl	$48, %edi
	movq	%r8, -232(%rbp)
	movq	%rsi, -216(%rbp)
	movq	%rcx, -208(%rbp)
	movq	%rdx, -200(%rbp)
	movaps	%xmm0, -3824(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -3808(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm7
	movdqa	-224(%rbp), %xmm6
	movq	%r14, %rsi
	leaq	48(%rax), %rdx
	leaq	-2928(%rbp), %rdi
	movq	%rax, -3824(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-208(%rbp), %xmm7
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -3808(%rbp)
	movq	%rdx, -3816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L334
	call	_ZdlPv@PLT
.L334:
	movq	-4216(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3056(%rbp)
	je	.L335
.L650:
	movq	-4208(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-3120(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -3808(%rbp)
	movaps	%xmm0, -3824(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -3824(%rbp)
	movq	%rdx, -3808(%rbp)
	movq	%rdx, -3816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3824(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L336
	call	_ZdlPv@PLT
.L336:
	movq	(%rbx), %rax
	movl	$16, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	24(%rax), %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	movl	$62, %edx
	movq	%r12, %rsi
	leaq	.LC3(%rip), %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -2864(%rbp)
	je	.L337
.L651:
	movq	-4216(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-2928(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -3808(%rbp)
	movaps	%xmm0, -3824(%rbp)
	call	_Znwm@PLT
	movl	$1800, %edi
	movq	%r14, %rsi
	movw	%di, 4(%rax)
	leaq	6(%rax), %rdx
	movq	%r12, %rdi
	movl	$117769477, (%rax)
	movq	%rax, -3824(%rbp)
	movq	%rdx, -3808(%rbp)
	movq	%rdx, -3816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3824(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L338
	call	_ZdlPv@PLT
.L338:
	movq	(%rbx), %rax
	movl	$19, %edx
	movq	%r15, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	24(%rax), %rbx
	movq	40(%rax), %r12
	movq	%rsi, -4320(%rbp)
	movq	32(%rax), %rsi
	movq	%rcx, -4304(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -4416(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -4192(%rbp)
	movq	%rbx, -4400(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal27TypedArrayBuiltinsAssembler9GetBufferENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_12JSTypedArrayEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -4448(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$22, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$48, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE(%rip), %esi
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal30Convert8ATintptr9ATuintptr_188EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8UintPtrTEEE@PLT
	movl	$28, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -4432(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-4304(%rbp), %xmm6
	movq	-4192(%rbp), %rcx
	movhps	-4320(%rbp), %xmm6
	movq	%rcx, -3920(%rbp)
	movaps	%xmm6, -3936(%rbp)
	pushq	-3920(%rbp)
	pushq	-3928(%rbp)
	pushq	-3936(%rbp)
	movaps	%xmm6, -4304(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$30, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -4320(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4320(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -4320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %xmm7
	pxor	%xmm0, %xmm0
	movq	-4416(%rbp), %xmm3
	movq	-4448(%rbp), %xmm2
	movdqa	-4304(%rbp), %xmm6
	movl	$72, %edi
	movaps	%xmm0, -3824(%rbp)
	punpcklqdq	%xmm7, %xmm3
	movq	-4192(%rbp), %xmm7
	movq	%rbx, -176(%rbp)
	movhps	-4432(%rbp), %xmm2
	movaps	%xmm3, -4416(%rbp)
	movhps	-4400(%rbp), %xmm7
	movaps	%xmm2, -4432(%rbp)
	movaps	%xmm7, -4192(%rbp)
	movaps	%xmm6, -240(%rbp)
	movaps	%xmm7, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm2, -192(%rbp)
	movq	$0, -3808(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm6
	movq	%r14, %rsi
	movq	-176(%rbp), %rcx
	movdqa	-208(%rbp), %xmm7
	leaq	72(%rax), %rdx
	leaq	-2736(%rbp), %rdi
	movq	%rax, -3824(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-224(%rbp), %xmm6
	movq	%rcx, 64(%rax)
	movups	%xmm6, 16(%rax)
	movdqa	-192(%rbp), %xmm6
	movups	%xmm7, 32(%rax)
	movups	%xmm6, 48(%rax)
	movq	%rdx, -3808(%rbp)
	movq	%rdx, -3816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L339
	call	_ZdlPv@PLT
.L339:
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movq	%rbx, -176(%rbp)
	movdqa	-4304(%rbp), %xmm5
	movdqa	-4192(%rbp), %xmm6
	movdqa	-4416(%rbp), %xmm7
	movaps	%xmm0, -3824(%rbp)
	movaps	%xmm5, -240(%rbp)
	movdqa	-4432(%rbp), %xmm5
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm5, -192(%rbp)
	movq	$0, -3808(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm6
	movq	%r14, %rsi
	movq	-176(%rbp), %rcx
	movdqa	-224(%rbp), %xmm7
	movdqa	-208(%rbp), %xmm5
	leaq	72(%rax), %rdx
	leaq	-2544(%rbp), %rdi
	movups	%xmm6, (%rax)
	movdqa	-192(%rbp), %xmm6
	movq	%rcx, 64(%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movq	%rax, -3824(%rbp)
	movq	%rdx, -3808(%rbp)
	movq	%rdx, -3816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L340
	call	_ZdlPv@PLT
.L340:
	movq	-4232(%rbp), %rcx
	movq	-4224(%rbp), %rdx
	movq	%r15, %rdi
	movq	-4320(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2672(%rbp)
	je	.L341
.L652:
	movq	-4224(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4032(%rbp)
	leaq	-2736(%rbp), %r12
	movq	$0, -4024(%rbp)
	movq	$0, -4016(%rbp)
	movq	$0, -4008(%rbp)
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	movq	$0, -3904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3904(%rbp), %rax
	movq	%r12, %rdi
	leaq	-4000(%rbp), %r9
	pushq	%rax
	leaq	-3976(%rbp), %rax
	leaq	-4008(%rbp), %r8
	pushq	%rax
	leaq	-3984(%rbp), %rax
	leaq	-4016(%rbp), %rcx
	pushq	%rax
	leaq	-3992(%rbp), %rax
	leaq	-4024(%rbp), %rdx
	pushq	%rax
	leaq	-4032(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_
	addq	$32, %rsp
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3976(%rbp), %rcx
	movq	-3904(%rbp), %rdx
	movq	%r14, %rdi
	movq	-4008(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler22ConvertToRelativeIndexENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3904(%rbp), %rax
	movq	-3984(%rbp), %xmm0
	movl	$80, %edi
	movq	-4000(%rbp), %xmm1
	movq	%rbx, -168(%rbp)
	movq	-4016(%rbp), %xmm2
	movhps	-3976(%rbp), %xmm0
	movq	-4032(%rbp), %xmm3
	movq	%rax, -176(%rbp)
	movhps	-3992(%rbp), %xmm1
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-4008(%rbp), %xmm2
	movhps	-4024(%rbp), %xmm3
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm3, -240(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm0, -3824(%rbp)
	movq	$0, -3808(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm7
	movdqa	-224(%rbp), %xmm6
	movq	%r14, %rsi
	leaq	80(%rax), %rdx
	leaq	-2352(%rbp), %rdi
	movq	%rax, -3824(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-208(%rbp), %xmm7
	movups	%xmm6, 16(%rax)
	movdqa	-192(%rbp), %xmm6
	movups	%xmm7, 32(%rax)
	movdqa	-176(%rbp), %xmm7
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movq	%rdx, -3808(%rbp)
	movq	%rdx, -3816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L342
	call	_ZdlPv@PLT
.L342:
	movq	-4248(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L653:
	movq	-4232(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4032(%rbp)
	leaq	-2544(%rbp), %r12
	movq	$0, -4024(%rbp)
	movq	$0, -4016(%rbp)
	movq	$0, -4008(%rbp)
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	movq	$0, -3904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3904(%rbp), %rax
	movq	%r12, %rdi
	leaq	-4016(%rbp), %rcx
	pushq	%rax
	leaq	-3976(%rbp), %rax
	leaq	-4024(%rbp), %rdx
	pushq	%rax
	leaq	-3984(%rbp), %rax
	leaq	-4000(%rbp), %r9
	pushq	%rax
	leaq	-3992(%rbp), %rax
	leaq	-4008(%rbp), %r8
	pushq	%rax
	leaq	-4032(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_
	addq	$32, %rsp
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$80, %edi
	movq	-3904(%rbp), %xmm0
	movq	-3984(%rbp), %xmm1
	movq	-4000(%rbp), %xmm2
	movq	%rax, %xmm5
	movq	-4016(%rbp), %xmm3
	movq	$0, -3808(%rbp)
	movq	-4032(%rbp), %xmm4
	punpcklqdq	%xmm5, %xmm0
	movhps	-3976(%rbp), %xmm1
	movhps	-3992(%rbp), %xmm2
	movhps	-4008(%rbp), %xmm3
	movaps	%xmm0, -176(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-4024(%rbp), %xmm4
	movaps	%xmm3, -224(%rbp)
	movaps	%xmm4, -240(%rbp)
	movaps	%xmm2, -208(%rbp)
	movaps	%xmm1, -192(%rbp)
	movaps	%xmm0, -3824(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm6
	movq	%r14, %rsi
	movdqa	-224(%rbp), %xmm7
	movdqa	-208(%rbp), %xmm5
	movq	-4192(%rbp), %rdi
	leaq	80(%rax), %rdx
	movq	%rax, -3824(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-192(%rbp), %xmm6
	movups	%xmm7, 16(%rax)
	movdqa	-176(%rbp), %xmm7
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movq	%rdx, -3808(%rbp)
	movq	%rdx, -3816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L344
	call	_ZdlPv@PLT
.L344:
	movq	-4336(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2288(%rbp)
	je	.L345
.L654:
	movq	-4248(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4040(%rbp)
	leaq	-2352(%rbp), %r12
	movq	$0, -4032(%rbp)
	movq	$0, -4024(%rbp)
	movq	$0, -4016(%rbp)
	movq	$0, -4008(%rbp)
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	movq	$0, -3904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-3904(%rbp), %rax
	pushq	%rax
	leaq	-3976(%rbp), %rax
	leaq	-4024(%rbp), %rcx
	pushq	%rax
	leaq	-3984(%rbp), %rax
	leaq	-4032(%rbp), %rdx
	pushq	%rax
	leaq	-3992(%rbp), %rax
	leaq	-4040(%rbp), %rsi
	pushq	%rax
	leaq	-4000(%rbp), %rax
	leaq	-4008(%rbp), %r9
	pushq	%rax
	leaq	-4016(%rbp), %r8
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_
	addq	$48, %rsp
	movl	$80, %edi
	movq	-3976(%rbp), %xmm0
	movq	-3992(%rbp), %xmm1
	movq	-4008(%rbp), %xmm2
	movq	$0, -3808(%rbp)
	movq	-4024(%rbp), %xmm3
	movhps	-3904(%rbp), %xmm0
	movq	-4040(%rbp), %xmm4
	movhps	-3984(%rbp), %xmm1
	movhps	-4000(%rbp), %xmm2
	movaps	%xmm0, -176(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-4016(%rbp), %xmm3
	movhps	-4032(%rbp), %xmm4
	movaps	%xmm2, -208(%rbp)
	movaps	%xmm4, -240(%rbp)
	movaps	%xmm3, -224(%rbp)
	movaps	%xmm1, -192(%rbp)
	movaps	%xmm0, -3824(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-224(%rbp), %xmm5
	movdqa	-208(%rbp), %xmm6
	movdqa	-176(%rbp), %xmm4
	leaq	80(%rax), %rdx
	movq	%rax, -3824(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-192(%rbp), %xmm7
	movq	-4192(%rbp), %rdi
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm4, 64(%rax)
	movq	%rdx, -3808(%rbp)
	movq	%rdx, -3816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L346
	call	_ZdlPv@PLT
.L346:
	movq	-4336(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L655:
	movq	-4336(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4048(%rbp)
	movq	$0, -4040(%rbp)
	movq	$0, -4032(%rbp)
	movq	$0, -4024(%rbp)
	movq	$0, -4016(%rbp)
	movq	$0, -4008(%rbp)
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3976(%rbp), %rax
	movq	-4192(%rbp), %rdi
	pushq	%rax
	leaq	-3984(%rbp), %rax
	leaq	-4032(%rbp), %rcx
	pushq	%rax
	leaq	-3992(%rbp), %rax
	leaq	-4016(%rbp), %r9
	pushq	%rax
	leaq	-4000(%rbp), %rax
	leaq	-4024(%rbp), %r8
	pushq	%rax
	leaq	-4008(%rbp), %rax
	leaq	-4040(%rbp), %rdx
	pushq	%rax
	leaq	-4048(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_
	addq	$48, %rsp
	movl	$29, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$36, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-4040(%rbp), %rdx
	movq	-4048(%rbp), %rax
	movq	%rdx, -3896(%rbp)
	movq	-4032(%rbp), %rdx
	movq	%rax, -3904(%rbp)
	movq	%rdx, -3888(%rbp)
	pushq	-3888(%rbp)
	pushq	-3896(%rbp)
	pushq	%rax
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$38, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movq	-3984(%rbp), %xmm6
	movq	-4000(%rbp), %xmm4
	movq	-4016(%rbp), %xmm5
	movl	$88, %edi
	movq	-4032(%rbp), %xmm2
	movaps	%xmm0, -3824(%rbp)
	movq	-4048(%rbp), %xmm3
	movhps	-3976(%rbp), %xmm6
	movhps	-3992(%rbp), %xmm4
	movq	%rbx, -160(%rbp)
	movhps	-4008(%rbp), %xmm5
	movhps	-4024(%rbp), %xmm2
	movaps	%xmm6, -4432(%rbp)
	movhps	-4040(%rbp), %xmm3
	movaps	%xmm4, -4416(%rbp)
	movaps	%xmm5, -4400(%rbp)
	movaps	%xmm2, -4336(%rbp)
	movaps	%xmm3, -4320(%rbp)
	movaps	%xmm3, -240(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm6, -176(%rbp)
	movq	$0, -3808(%rbp)
	call	_Znwm@PLT
	movq	-160(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-240(%rbp), %xmm3
	movdqa	-224(%rbp), %xmm5
	movdqa	-208(%rbp), %xmm6
	leaq	88(%rax), %rdx
	movq	%rax, -3824(%rbp)
	movdqa	-192(%rbp), %xmm7
	movdqa	-176(%rbp), %xmm4
	movq	%rcx, 80(%rax)
	movq	-4304(%rbp), %rdi
	movups	%xmm3, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm4, 64(%rax)
	movq	%rdx, -3808(%rbp)
	movq	%rdx, -3816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L348
	call	_ZdlPv@PLT
.L348:
	movdqa	-4320(%rbp), %xmm3
	movdqa	-4336(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movl	$88, %edi
	movdqa	-4400(%rbp), %xmm6
	movdqa	-4416(%rbp), %xmm7
	movaps	%xmm0, -3824(%rbp)
	movdqa	-4432(%rbp), %xmm4
	movaps	%xmm3, -240(%rbp)
	movaps	%xmm5, -224(%rbp)
	movaps	%xmm6, -208(%rbp)
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	movq	%rbx, -160(%rbp)
	movq	$0, -3808(%rbp)
	call	_Znwm@PLT
	movq	-160(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-240(%rbp), %xmm3
	movdqa	-224(%rbp), %xmm5
	movdqa	-208(%rbp), %xmm6
	leaq	88(%rax), %rdx
	movq	%rax, -3824(%rbp)
	movdqa	-192(%rbp), %xmm7
	movdqa	-176(%rbp), %xmm4
	movq	%rcx, 80(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm4, 64(%rax)
	leaq	-1776(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3808(%rbp)
	movq	%rdx, -3816(%rbp)
	movq	%rax, -4320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L349
	call	_ZdlPv@PLT
.L349:
	movq	-4376(%rbp), %rcx
	movq	-4368(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L656:
	movq	-4368(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4056(%rbp)
	movq	$0, -4048(%rbp)
	movq	$0, -4040(%rbp)
	movq	$0, -4032(%rbp)
	movq	$0, -4024(%rbp)
	movq	$0, -4016(%rbp)
	movq	$0, -4008(%rbp)
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3976(%rbp), %rax
	movq	-4304(%rbp), %rdi
	leaq	-4024(%rbp), %r9
	pushq	%rax
	leaq	-3984(%rbp), %rax
	leaq	-4032(%rbp), %r8
	pushq	%rax
	leaq	-3992(%rbp), %rax
	leaq	-4040(%rbp), %rcx
	pushq	%rax
	leaq	-4000(%rbp), %rax
	leaq	-4048(%rbp), %rdx
	pushq	%rax
	leaq	-4008(%rbp), %rax
	leaq	-4056(%rbp), %rsi
	pushq	%rax
	leaq	-4016(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_SI_
	addq	$48, %rsp
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4000(%rbp), %rcx
	movq	-3976(%rbp), %rdx
	movq	%r14, %rdi
	movq	-4032(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler22ConvertToRelativeIndexENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3976(%rbp), %rax
	movq	-3992(%rbp), %xmm0
	movl	$96, %edi
	movq	-4008(%rbp), %xmm1
	movq	%rbx, -152(%rbp)
	movq	-4024(%rbp), %xmm2
	movhps	-3984(%rbp), %xmm0
	movq	-4040(%rbp), %xmm3
	movq	%rax, -160(%rbp)
	movq	-4056(%rbp), %xmm4
	movhps	-4000(%rbp), %xmm1
	movaps	%xmm0, -176(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-4016(%rbp), %xmm2
	movhps	-4032(%rbp), %xmm3
	movaps	%xmm1, -192(%rbp)
	movhps	-4048(%rbp), %xmm4
	movaps	%xmm3, -224(%rbp)
	movaps	%xmm4, -240(%rbp)
	movaps	%xmm2, -208(%rbp)
	movaps	%xmm0, -3824(%rbp)
	movq	$0, -3808(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm3
	movq	%r14, %rsi
	movdqa	-224(%rbp), %xmm5
	movdqa	-208(%rbp), %xmm6
	movdqa	-192(%rbp), %xmm7
	leaq	96(%rax), %rdx
	movq	%rax, -3824(%rbp)
	movdqa	-176(%rbp), %xmm4
	movups	%xmm3, (%rax)
	movdqa	-160(%rbp), %xmm3
	movq	-4336(%rbp), %rdi
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm3, 80(%rax)
	movq	%rdx, -3808(%rbp)
	movq	%rdx, -3816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L351
	call	_ZdlPv@PLT
.L351:
	movq	-4352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1712(%rbp)
	je	.L352
.L657:
	movq	-4376(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4056(%rbp)
	movq	$0, -4048(%rbp)
	movq	$0, -4040(%rbp)
	movq	$0, -4032(%rbp)
	movq	$0, -4024(%rbp)
	movq	$0, -4016(%rbp)
	movq	$0, -4008(%rbp)
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3976(%rbp), %rax
	movq	-4320(%rbp), %rdi
	leaq	-4040(%rbp), %rcx
	pushq	%rax
	leaq	-3984(%rbp), %rax
	leaq	-4048(%rbp), %rdx
	pushq	%rax
	leaq	-3992(%rbp), %rax
	leaq	-4056(%rbp), %rsi
	pushq	%rax
	leaq	-4000(%rbp), %rax
	leaq	-4024(%rbp), %r9
	pushq	%rax
	leaq	-4008(%rbp), %rax
	leaq	-4032(%rbp), %r8
	pushq	%rax
	leaq	-4016(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_SI_
	addq	$48, %rsp
	movl	$96, %edi
	movq	-3976(%rbp), %xmm0
	movq	-3992(%rbp), %xmm1
	movq	-4008(%rbp), %xmm2
	movq	$0, -3808(%rbp)
	movq	-4024(%rbp), %xmm3
	movhps	-4000(%rbp), %xmm0
	movq	-4040(%rbp), %xmm4
	movq	-4056(%rbp), %xmm5
	movhps	-3984(%rbp), %xmm1
	movhps	-4000(%rbp), %xmm2
	movaps	%xmm0, -160(%rbp)
	movhps	-4016(%rbp), %xmm3
	movhps	-4032(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -192(%rbp)
	movhps	-4048(%rbp), %xmm5
	movaps	%xmm4, -224(%rbp)
	movaps	%xmm5, -240(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -3824(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-224(%rbp), %xmm6
	movdqa	-208(%rbp), %xmm7
	movdqa	-192(%rbp), %xmm1
	leaq	96(%rax), %rdx
	leaq	-1392(%rbp), %rdi
	movdqa	-176(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm2
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm1, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm2, 80(%rax)
	movq	%rax, -3824(%rbp)
	movq	%rdx, -3808(%rbp)
	movq	%rdx, -3816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L353
	call	_ZdlPv@PLT
.L353:
	movq	-4176(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1520(%rbp)
	je	.L354
.L658:
	movq	-4352(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4064(%rbp)
	movq	$0, -4056(%rbp)
	movq	$0, -4048(%rbp)
	movq	$0, -4040(%rbp)
	movq	$0, -4032(%rbp)
	movq	$0, -4024(%rbp)
	movq	$0, -4016(%rbp)
	movq	$0, -4008(%rbp)
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3976(%rbp), %rax
	movq	-4336(%rbp), %rdi
	pushq	%rax
	leaq	-3984(%rbp), %rax
	leaq	-4048(%rbp), %rcx
	pushq	%rax
	leaq	-3992(%rbp), %rax
	leaq	-4056(%rbp), %rdx
	pushq	%rax
	leaq	-4000(%rbp), %rax
	leaq	-4064(%rbp), %rsi
	pushq	%rax
	leaq	-4008(%rbp), %rax
	leaq	-4032(%rbp), %r9
	pushq	%rax
	leaq	-4016(%rbp), %rax
	leaq	-4040(%rbp), %r8
	pushq	%rax
	leaq	-4024(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_SI_SE_
	addq	$64, %rsp
	movl	$96, %edi
	movq	-3984(%rbp), %xmm0
	movq	-4000(%rbp), %xmm1
	movq	-4016(%rbp), %xmm2
	movq	$0, -3808(%rbp)
	movq	-4032(%rbp), %xmm3
	movhps	-3976(%rbp), %xmm0
	movq	-4048(%rbp), %xmm4
	movq	-4064(%rbp), %xmm5
	movhps	-3992(%rbp), %xmm1
	movhps	-4008(%rbp), %xmm2
	movaps	%xmm0, -160(%rbp)
	movhps	-4024(%rbp), %xmm3
	movhps	-4040(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -192(%rbp)
	movhps	-4056(%rbp), %xmm5
	movaps	%xmm4, -224(%rbp)
	movaps	%xmm5, -240(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -3824(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm3
	movq	%r14, %rsi
	movdqa	-224(%rbp), %xmm5
	movdqa	-208(%rbp), %xmm6
	movdqa	-192(%rbp), %xmm7
	leaq	96(%rax), %rdx
	leaq	-1392(%rbp), %rdi
	movdqa	-176(%rbp), %xmm1
	movdqa	-160(%rbp), %xmm4
	movups	%xmm3, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm1, 64(%rax)
	movups	%xmm4, 80(%rax)
	movq	%rax, -3824(%rbp)
	movq	%rdx, -3808(%rbp)
	movq	%rdx, -3816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L355
	call	_ZdlPv@PLT
.L355:
	movq	-4176(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1328(%rbp)
	je	.L356
.L659:
	movq	-4176(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4064(%rbp)
	leaq	-1392(%rbp), %r12
	movq	$0, -4056(%rbp)
	movq	$0, -4048(%rbp)
	movq	$0, -4040(%rbp)
	movq	$0, -4032(%rbp)
	movq	$0, -4024(%rbp)
	movq	$0, -4016(%rbp)
	movq	$0, -4008(%rbp)
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-3976(%rbp), %rax
	pushq	%rax
	leaq	-3984(%rbp), %rax
	leaq	-4032(%rbp), %r9
	pushq	%rax
	leaq	-3992(%rbp), %rax
	leaq	-4040(%rbp), %r8
	pushq	%rax
	leaq	-4000(%rbp), %rax
	leaq	-4048(%rbp), %rcx
	pushq	%rax
	leaq	-4008(%rbp), %rax
	leaq	-4056(%rbp), %rdx
	pushq	%rax
	leaq	-4016(%rbp), %rax
	leaq	-4064(%rbp), %rsi
	pushq	%rax
	leaq	-4024(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESC_PNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_SI_SE_SI_SE_
	addq	$64, %rsp
	movl	$37, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$41, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3992(%rbp), %rdx
	movq	-3976(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler9IntPtrMaxENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal35Convert13ATPositiveSmi8ATintptr_189EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movl	$46, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -4536(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4024(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal27TypedArrayBuiltinsAssembler25GetTypedArrayElementsInfoENS0_8compiler5TNodeINS0_12JSTypedArrayEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -4352(%rbp)
	movq	%rdx, -4400(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$49, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$32, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	movq	-4024(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE(%rip), %esi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -4432(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$52, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$53, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3992(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal35Convert13ATPositiveSmi8ATintptr_189EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movl	$26, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -4368(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4368(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal36Convert9ATuintptr13ATPositiveSmi_187EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$27, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4352(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEENS3_INS0_9IntegralTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$30, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4352(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShrENS1_11SloppyTNodeINS0_5WordTEEENS3_INS0_9IntegralTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -4376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4376(%rbp), %r8
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -4376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$184, %edi
	movq	-4032(%rbp), %xmm5
	movq	-4048(%rbp), %xmm2
	movq	-3984(%rbp), %xmm6
	movq	-4000(%rbp), %xmm7
	movhps	-4024(%rbp), %xmm5
	movq	-4064(%rbp), %xmm3
	movhps	-4040(%rbp), %xmm2
	movaps	%xmm5, -4480(%rbp)
	movhps	-3976(%rbp), %xmm6
	movq	-4016(%rbp), %xmm4
	movaps	%xmm5, -208(%rbp)
	movhps	-3992(%rbp), %xmm7
	movq	-4368(%rbp), %xmm5
	movhps	-4056(%rbp), %xmm3
	movaps	%xmm2, -4464(%rbp)
	movhps	-4008(%rbp), %xmm4
	movaps	%xmm2, -224(%rbp)
	movq	-4400(%rbp), %xmm2
	movaps	%xmm6, -4528(%rbp)
	movaps	%xmm7, -4512(%rbp)
	movaps	%xmm3, -4448(%rbp)
	movaps	%xmm3, -240(%rbp)
	movq	-4432(%rbp), %xmm3
	movaps	%xmm7, -176(%rbp)
	movq	%r12, %xmm7
	movaps	%xmm6, -160(%rbp)
	movdqa	%xmm5, %xmm6
	punpcklqdq	%xmm7, %xmm6
	movdqa	%xmm2, %xmm7
	movaps	%xmm4, -4496(%rbp)
	punpcklqdq	%xmm5, %xmm7
	movaps	%xmm4, -192(%rbp)
	movdqa	%xmm2, %xmm5
	movdqa	%xmm3, %xmm4
	movhps	-4352(%rbp), %xmm4
	punpcklqdq	%xmm3, %xmm5
	movaps	%xmm6, -4416(%rbp)
	movaps	%xmm7, -4400(%rbp)
	movaps	%xmm4, -4368(%rbp)
	movaps	%xmm5, -4432(%rbp)
	movq	-4536(%rbp), %xmm2
	movaps	%xmm5, -128(%rbp)
	movhps	-4352(%rbp), %xmm2
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm2, -4352(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm6, -80(%rbp)
	movaps	%xmm0, -3824(%rbp)
	movq	%rbx, -64(%rbp)
	movq	$0, -3808(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm2
	movq	%r14, %rsi
	movdqa	-224(%rbp), %xmm3
	movdqa	-208(%rbp), %xmm5
	movdqa	-192(%rbp), %xmm6
	leaq	184(%rax), %rdx
	leaq	-1200(%rbp), %rdi
	movq	-64(%rbp), %rcx
	movdqa	-176(%rbp), %xmm1
	movups	%xmm2, (%rax)
	movdqa	-160(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm2
	movups	%xmm3, 16(%rax)
	movdqa	-80(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm3
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	movq	%rcx, 176(%rax)
	movups	%xmm1, 64(%rax)
	movups	%xmm4, 80(%rax)
	movups	%xmm2, 96(%rax)
	movups	%xmm3, 112(%rax)
	movups	%xmm5, 128(%rax)
	movups	%xmm6, 144(%rax)
	movups	%xmm7, 160(%rax)
	movq	%rax, -3824(%rbp)
	movq	%rdx, -3808(%rbp)
	movq	%rdx, -3816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L357
	call	_ZdlPv@PLT
.L357:
	movdqa	-4448(%rbp), %xmm1
	movdqa	-4464(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movl	$184, %edi
	movdqa	-4480(%rbp), %xmm2
	movdqa	-4496(%rbp), %xmm3
	movaps	%xmm0, -3824(%rbp)
	movdqa	-4512(%rbp), %xmm5
	movdqa	-4528(%rbp), %xmm6
	movaps	%xmm1, -240(%rbp)
	movdqa	-4352(%rbp), %xmm7
	movdqa	-4432(%rbp), %xmm1
	movaps	%xmm4, -224(%rbp)
	movaps	%xmm2, -208(%rbp)
	movdqa	-4368(%rbp), %xmm4
	movdqa	-4400(%rbp), %xmm2
	movaps	%xmm3, -192(%rbp)
	movdqa	-4416(%rbp), %xmm3
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movq	%rbx, -64(%rbp)
	movq	$0, -3808(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-224(%rbp), %xmm6
	movdqa	-208(%rbp), %xmm7
	movdqa	-192(%rbp), %xmm1
	leaq	184(%rax), %rdx
	leaq	-1008(%rbp), %rdi
	movq	-64(%rbp), %rcx
	movdqa	-176(%rbp), %xmm4
	movups	%xmm5, (%rax)
	movdqa	-160(%rbp), %xmm2
	movdqa	-144(%rbp), %xmm3
	movups	%xmm6, 16(%rax)
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm6
	movups	%xmm7, 32(%rax)
	movups	%xmm1, 48(%rax)
	movdqa	-96(%rbp), %xmm7
	movdqa	-80(%rbp), %xmm1
	movq	%rcx, 176(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm2, 80(%rax)
	movups	%xmm3, 96(%rax)
	movups	%xmm5, 112(%rax)
	movups	%xmm6, 128(%rax)
	movups	%xmm7, 144(%rax)
	movups	%xmm1, 160(%rax)
	movq	%rax, -3824(%rbp)
	movq	%rdx, -3808(%rbp)
	movq	%rdx, -3816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L358
	call	_ZdlPv@PLT
.L358:
	movq	-4256(%rbp), %rcx
	movq	-4240(%rbp), %rdx
	movq	%r15, %rdi
	movq	-4376(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1136(%rbp)
	je	.L359
.L660:
	movq	-4240(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4152(%rbp)
	leaq	-1200(%rbp), %r12
	movq	$0, -4144(%rbp)
	movq	$0, -4136(%rbp)
	movq	$0, -4128(%rbp)
	movq	$0, -4120(%rbp)
	movq	$0, -4112(%rbp)
	movq	$0, -4104(%rbp)
	movq	$0, -4096(%rbp)
	movq	$0, -4088(%rbp)
	movq	$0, -4080(%rbp)
	movq	$0, -4072(%rbp)
	movq	$0, -4064(%rbp)
	movq	$0, -4056(%rbp)
	movq	$0, -4048(%rbp)
	movq	$0, -4040(%rbp)
	movq	$0, -4032(%rbp)
	movq	$0, -4024(%rbp)
	movq	$0, -4016(%rbp)
	movq	$0, -4008(%rbp)
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3976(%rbp), %rax
	movq	%r12, %rdi
	leaq	-4136(%rbp), %rcx
	pushq	%rax
	leaq	-3984(%rbp), %rax
	leaq	-4144(%rbp), %rdx
	pushq	%rax
	leaq	-3992(%rbp), %rax
	leaq	-4152(%rbp), %rsi
	pushq	%rax
	leaq	-4000(%rbp), %rax
	leaq	-4120(%rbp), %r9
	pushq	%rax
	leaq	-4008(%rbp), %rax
	leaq	-4128(%rbp), %r8
	pushq	%rax
	leaq	-4016(%rbp), %rax
	pushq	%rax
	leaq	-4024(%rbp), %rax
	pushq	%rax
	leaq	-4032(%rbp), %rax
	pushq	%rax
	leaq	-4040(%rbp), %rax
	pushq	%rax
	leaq	-4048(%rbp), %rax
	pushq	%rax
	leaq	-4056(%rbp), %rax
	pushq	%rax
	leaq	-4064(%rbp), %rax
	pushq	%rax
	leaq	-4072(%rbp), %rax
	pushq	%rax
	leaq	-4080(%rbp), %rax
	pushq	%rax
	leaq	-4088(%rbp), %rax
	pushq	%rax
	leaq	-4096(%rbp), %rax
	pushq	%rax
	leaq	-4104(%rbp), %rax
	pushq	%rax
	leaq	-4112(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_S4_NS0_3SmiENS0_8UintPtrTENS0_6Int32TESA_SA_SA_SB_S9_S9_SA_SA_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EESH_SL_SH_SL_SH_PNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EEST_ST_ST_SV_SR_SR_ST_ST_
	movq	-4152(%rbp), %rax
	pxor	%xmm0, %xmm0
	addq	$144, %rsp
	movl	$136, %edi
	movq	%rax, -240(%rbp)
	movq	-4144(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	-4136(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-4128(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-4120(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-4112(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-4104(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-4096(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-4088(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-4080(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-4072(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-4064(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-4056(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-4048(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-4040(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-4032(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-4024(%rbp), %rax
	movaps	%xmm0, -3824(%rbp)
	movq	%rax, -112(%rbp)
	movq	$0, -3808(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm4
	movq	%r14, %rsi
	movq	-112(%rbp), %rcx
	movdqa	-224(%rbp), %xmm2
	movdqa	-208(%rbp), %xmm3
	leaq	136(%rax), %rdx
	leaq	-624(%rbp), %rdi
	movdqa	-192(%rbp), %xmm5
	movdqa	-176(%rbp), %xmm6
	movups	%xmm4, (%rax)
	movdqa	-160(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm4
	movq	%rcx, 128(%rax)
	movdqa	-144(%rbp), %xmm1
	movups	%xmm2, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	movups	%xmm7, 80(%rax)
	movups	%xmm1, 96(%rax)
	movups	%xmm4, 112(%rax)
	movq	%rax, -3824(%rbp)
	movq	%rdx, -3808(%rbp)
	movq	%rdx, -3816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L360
	call	_ZdlPv@PLT
.L360:
	movq	-4272(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -944(%rbp)
	je	.L361
.L661:
	movq	-4256(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4152(%rbp)
	leaq	-1008(%rbp), %r12
	movq	$0, -4144(%rbp)
	movq	$0, -4136(%rbp)
	movq	$0, -4128(%rbp)
	movq	$0, -4120(%rbp)
	movq	$0, -4112(%rbp)
	movq	$0, -4104(%rbp)
	movq	$0, -4096(%rbp)
	movq	$0, -4088(%rbp)
	movq	$0, -4080(%rbp)
	movq	$0, -4072(%rbp)
	movq	$0, -4064(%rbp)
	movq	$0, -4056(%rbp)
	movq	$0, -4048(%rbp)
	movq	$0, -4040(%rbp)
	movq	$0, -4032(%rbp)
	movq	$0, -4024(%rbp)
	movq	$0, -4016(%rbp)
	movq	$0, -4008(%rbp)
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3976(%rbp), %rax
	movq	%r12, %rdi
	leaq	-4136(%rbp), %rcx
	pushq	%rax
	leaq	-3984(%rbp), %rax
	leaq	-4120(%rbp), %r9
	pushq	%rax
	leaq	-3992(%rbp), %rax
	leaq	-4128(%rbp), %r8
	pushq	%rax
	leaq	-4000(%rbp), %rax
	leaq	-4144(%rbp), %rdx
	pushq	%rax
	leaq	-4008(%rbp), %rax
	leaq	-4152(%rbp), %rsi
	pushq	%rax
	leaq	-4016(%rbp), %rax
	pushq	%rax
	leaq	-4024(%rbp), %rax
	pushq	%rax
	leaq	-4032(%rbp), %rax
	pushq	%rax
	leaq	-4040(%rbp), %rax
	pushq	%rax
	leaq	-4048(%rbp), %rax
	pushq	%rax
	leaq	-4056(%rbp), %rax
	pushq	%rax
	leaq	-4064(%rbp), %rax
	pushq	%rax
	leaq	-4072(%rbp), %rax
	pushq	%rax
	leaq	-4080(%rbp), %rax
	pushq	%rax
	leaq	-4088(%rbp), %rax
	pushq	%rax
	leaq	-4096(%rbp), %rax
	pushq	%rax
	leaq	-4104(%rbp), %rax
	pushq	%rax
	leaq	-4112(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_12JSTypedArrayENS0_13JSArrayBufferES4_S6_S4_S6_S4_NS0_3SmiENS0_8UintPtrTENS0_6Int32TESA_SA_SA_SB_S9_S9_SA_SA_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EESH_SL_SH_SL_SH_PNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EEST_ST_ST_SV_SR_SR_ST_ST_
	addq	$144, %rsp
	movl	$31, %edx
	movq	%r15, %rdi
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$176, %edi
	movq	-3992(%rbp), %xmm0
	movq	-4008(%rbp), %xmm1
	movq	-4024(%rbp), %xmm2
	movq	-4040(%rbp), %xmm3
	movq	-4056(%rbp), %xmm4
	movhps	-4000(%rbp), %xmm1
	movq	-4072(%rbp), %xmm5
	movhps	-3976(%rbp), %xmm0
	movq	-4088(%rbp), %xmm6
	movhps	-4016(%rbp), %xmm2
	movq	-4104(%rbp), %xmm7
	movhps	-4032(%rbp), %xmm3
	movq	-4120(%rbp), %xmm8
	movhps	-4048(%rbp), %xmm4
	movaps	%xmm3, -128(%rbp)
	movq	-4136(%rbp), %xmm9
	movq	-4152(%rbp), %xmm10
	movhps	-4064(%rbp), %xmm5
	movhps	-4080(%rbp), %xmm6
	movhps	-4096(%rbp), %xmm7
	movhps	-4112(%rbp), %xmm8
	movhps	-4128(%rbp), %xmm9
	movaps	%xmm7, -192(%rbp)
	movhps	-4144(%rbp), %xmm10
	movaps	%xmm9, -224(%rbp)
	movaps	%xmm10, -240(%rbp)
	movaps	%xmm8, -208(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3824(%rbp)
	movq	$0, -3808(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm2
	movq	%r14, %rsi
	movdqa	-224(%rbp), %xmm3
	movdqa	-208(%rbp), %xmm5
	movdqa	-192(%rbp), %xmm6
	leaq	176(%rax), %rdx
	leaq	-816(%rbp), %rdi
	movdqa	-176(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm1
	movups	%xmm2, (%rax)
	movdqa	-144(%rbp), %xmm4
	movdqa	-128(%rbp), %xmm2
	movups	%xmm3, 16(%rax)
	movups	%xmm5, 32(%rax)
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm5
	movups	%xmm6, 48(%rax)
	movdqa	-80(%rbp), %xmm6
	movups	%xmm7, 64(%rax)
	movups	%xmm1, 80(%rax)
	movups	%xmm4, 96(%rax)
	movups	%xmm2, 112(%rax)
	movups	%xmm3, 128(%rax)
	movups	%xmm5, 144(%rax)
	movups	%xmm6, 160(%rax)
	movq	%rax, -3824(%rbp)
	movq	%rdx, -3808(%rbp)
	movq	%rdx, -3816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L362
	call	_ZdlPv@PLT
.L362:
	movq	-4264(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -752(%rbp)
	je	.L363
.L662:
	movq	-4264(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-816(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$22, %edi
	movq	$0, -3808(%rbp)
	movaps	%xmm0, -3824(%rbp)
	call	_Znwm@PLT
	movl	$1286, %esi
	movdqa	.LC1(%rip), %xmm0
	movq	%r12, %rdi
	movw	%si, 20(%rax)
	leaq	22(%rax), %rdx
	movq	%r14, %rsi
	movl	$100926725, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, -3824(%rbp)
	movq	%rdx, -3808(%rbp)
	movq	%rdx, -3816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3824(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L364
	call	_ZdlPv@PLT
.L364:
	movq	(%rbx), %rax
	movq	40(%rax), %rdx
	movq	8(%rax), %rsi
	movq	24(%rax), %rbx
	movq	88(%rax), %rdi
	movq	104(%rax), %r11
	movq	120(%rax), %r10
	movq	%rdx, -4432(%rbp)
	movq	56(%rax), %rdx
	movq	(%rax), %rcx
	movq	%rsi, -4368(%rbp)
	movq	%rbx, -4400(%rbp)
	movq	16(%rax), %rsi
	movq	%rdx, -4448(%rbp)
	movq	72(%rax), %rdx
	movq	32(%rax), %rbx
	movq	%rdi, -4496(%rbp)
	movq	%rdx, -4464(%rbp)
	movq	96(%rax), %rdi
	movq	80(%rax), %rdx
	movq	%r11, -4528(%rbp)
	movq	112(%rax), %r11
	movq	48(%rax), %r12
	movq	%r10, -4544(%rbp)
	movq	128(%rax), %r10
	movq	%rcx, -4352(%rbp)
	movq	%r11, -4536(%rbp)
	movq	%rsi, -4376(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rbx, -4416(%rbp)
	movq	64(%rax), %rbx
	movq	%rdx, -4480(%rbp)
	movl	$53, %edx
	movq	%rdi, -4512(%rbp)
	movq	%r15, %rdi
	movq	%r10, -4552(%rbp)
	movq	168(%rax), %rax
	movq	%rax, -4560(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$144, %edi
	movq	-4352(%rbp), %xmm0
	movq	$0, -3808(%rbp)
	movhps	-4368(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-4376(%rbp), %xmm0
	movhps	-4400(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-4416(%rbp), %xmm0
	movhps	-4432(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	%r12, %xmm0
	movhps	-4448(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	%rbx, %xmm0
	movhps	-4464(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-4480(%rbp), %xmm0
	movhps	-4496(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-4512(%rbp), %xmm0
	movhps	-4528(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-4536(%rbp), %xmm0
	movhps	-4544(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-4552(%rbp), %xmm0
	movhps	-4560(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3824(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-224(%rbp), %xmm1
	movdqa	-208(%rbp), %xmm4
	movdqa	-192(%rbp), %xmm2
	leaq	144(%rax), %rdx
	leaq	-432(%rbp), %rdi
	movdqa	-176(%rbp), %xmm3
	movdqa	-160(%rbp), %xmm5
	movups	%xmm7, (%rax)
	movdqa	-144(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm7
	movups	%xmm1, 16(%rax)
	movdqa	-112(%rbp), %xmm1
	movups	%xmm4, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm3, 64(%rax)
	movups	%xmm5, 80(%rax)
	movups	%xmm6, 96(%rax)
	movups	%xmm7, 112(%rax)
	movups	%xmm1, 128(%rax)
	movq	%rax, -3824(%rbp)
	movq	%rdx, -3808(%rbp)
	movq	%rdx, -3816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L365
	call	_ZdlPv@PLT
.L365:
	movq	-4280(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -560(%rbp)
	je	.L366
.L663:
	movq	-4272(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-624(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$17, %edi
	movq	$0, -3808(%rbp)
	movaps	%xmm0, -3824(%rbp)
	call	_Znwm@PLT
	movdqa	.LC1(%rip), %xmm0
	movq	%r12, %rdi
	movq	%r14, %rsi
	movb	$5, 16(%rax)
	leaq	17(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -3824(%rbp)
	movq	%rdx, -3808(%rbp)
	movq	%rdx, -3816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3824(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L367
	call	_ZdlPv@PLT
.L367:
	movq	(%rbx), %rax
	movl	$54, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	24(%rax), %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$186, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ThrowRangeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -368(%rbp)
	je	.L368
.L664:
	movq	-4280(%rbp), %rsi
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	leaq	-432(%rbp), %r8
	movq	%r8, -4352(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$18, %edi
	movq	$0, -3808(%rbp)
	movaps	%xmm0, -3824(%rbp)
	call	_Znwm@PLT
	movl	$1285, %ecx
	movdqa	.LC1(%rip), %xmm0
	movq	%r14, %rsi
	movq	-4352(%rbp), %r8
	movw	%cx, 16(%rax)
	leaq	18(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%r8, %rdi
	movq	%rax, -3824(%rbp)
	movq	%rdx, -3808(%rbp)
	movq	%rdx, -3816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L369
	movq	%rax, -4352(%rbp)
	call	_ZdlPv@PLT
	movq	-4352(%rbp), %rax
.L369:
	movq	(%rax), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	24(%rax), %rdx
	movq	48(%rax), %r9
	movq	96(%rax), %rcx
	movq	128(%rax), %r10
	testq	%rdx, %rdx
	movq	136(%rax), %r8
	movq	%r9, -4400(%rbp)
	cmovne	%rdx, %r12
	movq	40(%rax), %rdx
	movq	%rcx, -4376(%rbp)
	movq	%r10, -4368(%rbp)
	testq	%rdx, %rdx
	movq	%r8, -4352(%rbp)
	cmovne	%rdx, %rbx
	movl	$52, %edx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4368(%rbp), %r10
	movq	-4352(%rbp), %r8
	movq	%r14, %rdi
	movq	%r10, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -4352(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$57, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4352(%rbp), %r8
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATuintptr_201EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8UintPtrTEEE@PLT
	movl	$62, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$61, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	-4376(%rbp), %rcx
	movq	-4400(%rbp), %r9
	leaq	.LC3(%rip), %rdx
	pushq	%rcx
	movl	$3, %ecx
	pushq	%r14
	call	_ZN2v88internal27TypedArraySpeciesCreate_349EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEPKcNS0_7int31_tENS4_INS0_12JSTypedArrayEEENS4_INS0_6ObjectEEESD_SD_@PLT
	movq	-4384(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	popq	%rax
	popq	%rdx
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L666:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-4192(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movdqa	-4320(%rbp), %xmm2
	movdqa	-4400(%rbp), %xmm3
	leaq	-240(%rbp), %rsi
	leaq	-192(%rbp), %rdx
	movaps	%xmm4, -240(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm0, -3904(%rbp)
	movq	$0, -3888(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3504(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3904(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L328
	call	_ZdlPv@PLT
.L328:
	movq	-4288(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L327
.L665:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22418:
	.size	_ZN2v88internal36TypedArrayPrototypeSubArrayAssembler39GenerateTypedArrayPrototypeSubArrayImplEv, .-_ZN2v88internal36TypedArrayPrototypeSubArrayAssembler39GenerateTypedArrayPrototypeSubArrayImplEv
	.section	.rodata._ZN2v88internal8Builtins36Generate_TypedArrayPrototypeSubArrayEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/typed-array-subarray-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins36Generate_TypedArrayPrototypeSubArrayEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"TypedArrayPrototypeSubArray"
	.section	.text._ZN2v88internal8Builtins36Generate_TypedArrayPrototypeSubArrayEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins36Generate_TypedArrayPrototypeSubArrayEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins36Generate_TypedArrayPrototypeSubArrayEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins36Generate_TypedArrayPrototypeSubArrayEPNS0_8compiler18CodeAssemblerStateE:
.LFB22414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$146, %ecx
	leaq	.LC5(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$925, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L671
.L668:
	movq	%r13, %rdi
	call	_ZN2v88internal36TypedArrayPrototypeSubArrayAssembler39GenerateTypedArrayPrototypeSubArrayImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L672
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L671:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L668
.L672:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22414:
	.size	_ZN2v88internal8Builtins36Generate_TypedArrayPrototypeSubArrayEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins36Generate_TypedArrayPrototypeSubArrayEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins36Generate_TypedArrayPrototypeSubArrayEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins36Generate_TypedArrayPrototypeSubArrayEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins36Generate_TypedArrayPrototypeSubArrayEPNS0_8compiler18CodeAssemblerStateE:
.LFB29166:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29166:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins36Generate_TypedArrayPrototypeSubArrayEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins36Generate_TypedArrayPrototypeSubArrayEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins36Generate_TypedArrayPrototypeSubArrayEPNS0_8compiler18CodeAssemblerStateE
	.weak	_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE:
	.byte	5
	.byte	5
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.byte	5
	.byte	5
	.byte	5
	.byte	7
	.byte	8
	.byte	7
	.byte	7
	.byte	5
	.byte	8
	.byte	5
	.byte	8
	.byte	5
	.byte	6
	.byte	5
	.byte	4
	.byte	5
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
