	.file	"proxy-get-prototype-of-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0:
.LFB29218:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	%rsi, %r13
	pushq	%rbx
	movq	%r13, %rax
	sarq	$3, %rax
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%r13, %r13
	js	.L14
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%rdx, %r14
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.L9
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%rax, %rdi
.L9:
	leaq	(%rdi,%r13), %r15
	movq	%rdi, (%rbx)
	movq	%r15, 16(%rbx)
	cmpq	%r12, %r14
	je	.L10
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
.L10:
	movq	%r15, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29218:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev,"axG",@progbits,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev:
.LFB22421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$56, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L16
	call	_ZdlPv@PLT
.L16:
	movq	16(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	%r12, %r13
	je	.L17
	.p2align 4,,10
	.p2align 3
.L21:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L18
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L21
.L19:
	movq	8(%rbx), %r12
.L17:
	testq	%r12, %r12
	je	.L15
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L21
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L15:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22421:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev
	.set	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EE:
.LFB26561:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$2, %edi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -64(%rbp)
	movq	$0, -48(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%r12, %rdi
	leaq	-64(%rbp), %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -64(%rbp)
	movq	%rdx, -48(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-64(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L28
	call	_ZdlPv@PLT
.L28:
	movq	(%r12), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L29
	movq	%rdx, 0(%r13)
.L29:
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L27
	movq	%rax, (%rbx)
.L27:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L42
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L42:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26561:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectENS0_5BoolTEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESG_PNSA_IS7_EEPNSA_IS8_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectENS0_5BoolTEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESG_PNSA_IS7_EEPNSA_IS8_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectENS0_5BoolTEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESG_PNSA_IS7_EEPNSA_IS8_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectENS0_5BoolTEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESG_PNSA_IS7_EEPNSA_IS8_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectENS0_5BoolTEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESG_PNSA_IS7_EEPNSA_IS8_EE:
.LFB26588:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$7, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$117901063, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$4, 6(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L44
	movq	%rax, -112(%rbp)
	call	_ZdlPv@PLT
	movq	-112(%rbp), %rax
.L44:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L45
	movq	%rdx, (%r15)
.L45:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L46
	movq	%rdx, (%r14)
.L46:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L47
	movq	%rdx, 0(%r13)
.L47:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L48
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L48:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L49
	movq	%rdx, (%rbx)
.L49:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L50
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L50:
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L43
	movq	-104(%rbp), %rbx
	movq	%rax, (%rbx)
.L43:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L78
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L78:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26588:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectENS0_5BoolTEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESG_PNSA_IS7_EEPNSA_IS8_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectENS0_5BoolTEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESG_PNSA_IS7_EEPNSA_IS8_EE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_PNS9_IS7_EESJ_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_PNS9_IS7_EESJ_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_PNS9_IS7_EESJ_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_PNS9_IS7_EESJ_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_PNS9_IS7_EESJ_:
.LFB26597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$7, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$117901063, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$8, 6(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L80
	movq	%rax, -112(%rbp)
	call	_ZdlPv@PLT
	movq	-112(%rbp), %rax
.L80:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L81
	movq	%rdx, (%r15)
.L81:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L82
	movq	%rdx, (%r14)
.L82:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L83
	movq	%rdx, 0(%r13)
.L83:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L84
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L84:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L85
	movq	%rdx, (%rbx)
.L85:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L86
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L86:
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L79
	movq	-104(%rbp), %rbx
	movq	%rax, (%rbx)
.L79:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L114
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L114:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26597:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_PNS9_IS7_EESJ_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_PNS9_IS7_EESJ_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_PNS9_IS7_EESJ_SJ_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_PNS9_IS7_EESJ_SJ_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_PNS9_IS7_EESJ_SJ_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_PNS9_IS7_EESJ_SJ_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_PNS9_IS7_EESJ_SJ_:
.LFB26599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$8, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$578721378392803079, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L116
	movq	%rax, -120(%rbp)
	call	_ZdlPv@PLT
	movq	-120(%rbp), %rax
.L116:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L117
	movq	%rdx, (%r15)
.L117:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L118
	movq	%rdx, (%r14)
.L118:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L119
	movq	%rdx, 0(%r13)
.L119:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L120
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L120:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L121
	movq	%rdx, (%rbx)
.L121:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L122
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L122:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L123
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L123:
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L115
	movq	-112(%rbp), %rbx
	movq	%rax, (%rbx)
.L115:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L154
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L154:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26599:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_PNS9_IS7_EESJ_SJ_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_PNS9_IS7_EESJ_SJ_
	.section	.rodata._ZN2v88internal28ProxyGetPrototypeOfAssembler31GenerateProxyGetPrototypeOfImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../../deps/v8/../../deps/v8/src/builtins/proxy-get-prototype-of.tq"
	.section	.rodata._ZN2v88internal28ProxyGetPrototypeOfAssembler31GenerateProxyGetPrototypeOfImplEv.str1.1,"aMS",@progbits,1
.LC3:
	.string	"getPrototypeOf"
	.section	.text._ZN2v88internal28ProxyGetPrototypeOfAssembler31GenerateProxyGetPrototypeOfImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28ProxyGetPrototypeOfAssembler31GenerateProxyGetPrototypeOfImplEv
	.type	_ZN2v88internal28ProxyGetPrototypeOfAssembler31GenerateProxyGetPrototypeOfImplEv, @function
_ZN2v88internal28ProxyGetPrototypeOfAssembler31GenerateProxyGetPrototypeOfImplEv:
.LFB22418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$184, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$1, %esi
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %r12
	movq	%r15, -4008(%rbp)
	leaq	-3720(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	-4008(%rbp), %r12
	movq	%rax, -4048(%rbp)
	leaq	-3528(%rbp), %rbx
	leaq	-3904(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -3768(%rbp)
	movq	%rax, -4032(%rbp)
	movq	-4008(%rbp), %rax
	movq	$0, -3760(%rbp)
	movq	%rax, -3776(%rbp)
	movq	$0, -3752(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -3768(%rbp)
	leaq	-3776(%rbp), %rax
	movq	%rdx, -3752(%rbp)
	movq	%rdx, -3760(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3736(%rbp)
	movq	%rax, -4024(%rbp)
	movq	$0, -3744(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4008(%rbp), %rax
	movl	$72, %edi
	movq	$0, -3576(%rbp)
	movq	$0, -3568(%rbp)
	movq	%rax, -3584(%rbp)
	movq	$0, -3560(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%rbx, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -3560(%rbp)
	movq	%rdx, -3568(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3544(%rbp)
	movq	%rax, -3576(%rbp)
	movq	$0, -3552(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4008(%rbp), %rax
	movl	$96, %edi
	movq	$0, -3384(%rbp)
	movq	$0, -3376(%rbp)
	movq	%rax, -3392(%rbp)
	movq	$0, -3368(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-3336(%rbp), %rcx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -3368(%rbp)
	movq	%rdx, -3376(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4096(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -3352(%rbp)
	movq	%rax, -3384(%rbp)
	movq	$0, -3360(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4008(%rbp), %rax
	movl	$120, %edi
	movq	$0, -3192(%rbp)
	movq	$0, -3184(%rbp)
	movq	%rax, -3200(%rbp)
	movq	$0, -3176(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-3144(%rbp), %rcx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -3176(%rbp)
	movq	%rdx, -3184(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4120(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -3160(%rbp)
	movq	%rax, -3192(%rbp)
	movq	$0, -3168(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4008(%rbp), %rax
	movl	$144, %edi
	movq	$0, -3000(%rbp)
	movq	$0, -2992(%rbp)
	movq	%rax, -3008(%rbp)
	movq	$0, -2984(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2952(%rbp), %rcx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rdx, -2984(%rbp)
	movq	%rdx, -2992(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4128(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2968(%rbp)
	movq	%rax, -3000(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4008(%rbp), %rax
	movl	$96, %edi
	movq	$0, -2808(%rbp)
	movq	$0, -2800(%rbp)
	movq	%rax, -2816(%rbp)
	movq	$0, -2792(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2760(%rbp), %rcx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -2792(%rbp)
	movq	%rdx, -2800(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4160(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2776(%rbp)
	movq	%rax, -2808(%rbp)
	movq	$0, -2784(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4008(%rbp), %rax
	movl	$120, %edi
	movq	$0, -2616(%rbp)
	movq	$0, -2608(%rbp)
	movq	%rax, -2624(%rbp)
	movq	$0, -2600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2568(%rbp), %rcx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -2600(%rbp)
	movq	%rdx, -2608(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4176(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2584(%rbp)
	movq	%rax, -2616(%rbp)
	movq	$0, -2592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4008(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2424(%rbp)
	movq	$0, -2416(%rbp)
	movq	%rax, -2432(%rbp)
	movq	$0, -2408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2376(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -2408(%rbp)
	movq	%rdx, -2416(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4144(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2392(%rbp)
	movq	%rax, -2424(%rbp)
	movq	$0, -2400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4008(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2232(%rbp)
	movq	$0, -2224(%rbp)
	movq	%rax, -2240(%rbp)
	movq	$0, -2216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2184(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -2216(%rbp)
	movq	%rdx, -2224(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4200(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2200(%rbp)
	movq	%rax, -2232(%rbp)
	movq	$0, -2208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4008(%rbp), %rax
	movl	$192, %edi
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movq	%rax, -2048(%rbp)
	movq	$0, -2024(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1992(%rbp), %rcx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -2024(%rbp)
	movq	%rdx, -2032(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4192(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2008(%rbp)
	movq	%rax, -2040(%rbp)
	movq	$0, -2016(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4008(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1848(%rbp)
	movq	$0, -1840(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1832(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1800(%rbp), %rcx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rdx, -1832(%rbp)
	movq	%rdx, -1840(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4224(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1816(%rbp)
	movq	%rax, -1848(%rbp)
	movq	$0, -1824(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4008(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1656(%rbp)
	movq	$0, -1648(%rbp)
	movq	%rax, -1664(%rbp)
	movq	$0, -1640(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1608(%rbp), %rcx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rdx, -1640(%rbp)
	movq	%rdx, -1648(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4240(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1624(%rbp)
	movq	%rax, -1656(%rbp)
	movq	$0, -1632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4008(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1464(%rbp)
	movq	$0, -1456(%rbp)
	movq	%rax, -1472(%rbp)
	movq	$0, -1448(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1416(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -1448(%rbp)
	movq	%rdx, -1456(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4288(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1432(%rbp)
	movq	%rax, -1464(%rbp)
	movq	$0, -1440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4008(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1272(%rbp)
	movq	$0, -1264(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -1256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1224(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -1256(%rbp)
	movq	%rdx, -1264(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4112(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1240(%rbp)
	movq	%rax, -1272(%rbp)
	movq	$0, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4008(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1080(%rbp)
	movq	$0, -1072(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$0, -1064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1032(%rbp), %rcx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -1064(%rbp)
	movq	%rdx, -1072(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4264(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1048(%rbp)
	movq	%rax, -1080(%rbp)
	movq	$0, -1056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4008(%rbp), %rax
	movl	$192, %edi
	movq	$0, -888(%rbp)
	movq	$0, -880(%rbp)
	movq	%rax, -896(%rbp)
	movq	$0, -872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-840(%rbp), %rcx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -872(%rbp)
	movq	%rdx, -880(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4272(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -856(%rbp)
	movq	%rax, -888(%rbp)
	movq	$0, -864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4008(%rbp), %rax
	movl	$72, %edi
	movq	$0, -696(%rbp)
	movq	$0, -688(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -680(%rbp)
	call	_Znwm@PLT
	leaq	-648(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -680(%rbp)
	movq	%rdx, -688(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4248(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -664(%rbp)
	movq	%rax, -696(%rbp)
	movq	$0, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4008(%rbp), %rax
	movl	$48, %edi
	movq	$0, -504(%rbp)
	movq	$0, -496(%rbp)
	movq	%rax, -512(%rbp)
	movq	$0, -488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	leaq	-456(%rbp), %rcx
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movq	%rcx, %rdi
	movups	%xmm0, 32(%rax)
	movq	%rdx, -488(%rbp)
	movq	%rdx, -496(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4208(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -472(%rbp)
	movq	%rax, -504(%rbp)
	movq	$0, -480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4008(%rbp), %rax
	movl	$48, %edi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	leaq	-264(%rbp), %rcx
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movq	%rcx, %rdi
	movups	%xmm0, 32(%rax)
	movq	%rdx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4256(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -312(%rbp)
	movups	%xmm0, -280(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-4048(%rbp), %xmm1
	movaps	%xmm0, -3904(%rbp)
	movhps	-4032(%rbp), %xmm1
	movq	$0, -3888(%rbp)
	movaps	%xmm1, -4048(%rbp)
	call	_Znwm@PLT
	movdqa	-4048(%rbp), %xmm1
	movq	-4024(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -3904(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3904(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L156
	call	_ZdlPv@PLT
.L156:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3392(%rbp), %rax
	cmpq	$0, -3712(%rbp)
	movq	%rax, -4072(%rbp)
	leaq	-3584(%rbp), %rax
	movq	%rax, -4032(%rbp)
	jne	.L344
.L157:
	leaq	-512(%rbp), %rax
	cmpq	$0, -3520(%rbp)
	movq	%rax, -4104(%rbp)
	jne	.L345
.L162:
	leaq	-3008(%rbp), %rax
	cmpq	$0, -3328(%rbp)
	movq	%rax, -4080(%rbp)
	leaq	-3200(%rbp), %rax
	movq	%rax, -4064(%rbp)
	jne	.L346
.L165:
	leaq	-2816(%rbp), %rax
	cmpq	$0, -3136(%rbp)
	movq	%rax, -4096(%rbp)
	jne	.L347
.L170:
	leaq	-2624(%rbp), %rax
	cmpq	$0, -2944(%rbp)
	movq	%rax, -4120(%rbp)
	jne	.L348
.L173:
	leaq	-704(%rbp), %rax
	cmpq	$0, -2752(%rbp)
	movq	%rax, -4136(%rbp)
	jne	.L349
.L176:
	leaq	-2432(%rbp), %rax
	cmpq	$0, -2560(%rbp)
	movq	%rax, -4128(%rbp)
	leaq	-2240(%rbp), %rax
	movq	%rax, -4160(%rbp)
	jne	.L350
.L179:
	leaq	-2048(%rbp), %rax
	cmpq	$0, -2368(%rbp)
	movq	%rax, -4048(%rbp)
	jne	.L351
.L186:
	cmpq	$0, -2176(%rbp)
	jne	.L352
.L188:
	leaq	-1856(%rbp), %rax
	cmpq	$0, -1984(%rbp)
	movq	%rax, -4176(%rbp)
	leaq	-1664(%rbp), %rax
	movq	%rax, -4144(%rbp)
	jne	.L353
.L190:
	leaq	-320(%rbp), %rax
	cmpq	$0, -1792(%rbp)
	movq	%rax, -4192(%rbp)
	jne	.L354
.L194:
	leaq	-1472(%rbp), %rax
	cmpq	$0, -1600(%rbp)
	movq	%rax, -4200(%rbp)
	jne	.L355
	cmpq	$0, -1408(%rbp)
	jne	.L356
.L201:
	cmpq	$0, -1216(%rbp)
	leaq	-1088(%rbp), %rbx
	leaq	-896(%rbp), %r14
	jne	.L357
	cmpq	$0, -1024(%rbp)
	jne	.L358
.L205:
	cmpq	$0, -832(%rbp)
	jne	.L359
.L206:
	cmpq	$0, -640(%rbp)
	jne	.L360
.L207:
	cmpq	$0, -448(%rbp)
	jne	.L361
.L211:
	cmpq	$0, -256(%rbp)
	jne	.L362
.L212:
	movq	-4192(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4104(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4136(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4112(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L214
	call	_ZdlPv@PLT
.L214:
	movq	-1264(%rbp), %rbx
	movq	-1272(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L215
	.p2align 4,,10
	.p2align 3
.L219:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L216
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L219
.L217:
	movq	-1272(%rbp), %r13
.L215:
	testq	%r13, %r13
	je	.L220
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L220:
	movq	-4200(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4144(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4176(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4048(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4160(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4128(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4120(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4096(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4080(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4064(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4072(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4032(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4024(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L363
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L219
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L344:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -3888(%rbp)
	movaps	%xmm0, -3904(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r11d
	movq	-4024(%rbp), %rdi
	movq	%r13, %rsi
	movw	%r11w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -3904(%rbp)
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3904(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L158
	call	_ZdlPv@PLT
.L158:
	movq	(%r14), %rax
	movl	$13, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r14
	movq	8(%rax), %rax
	movq	%rax, -4032(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$21, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -4048(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-4048(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-4032(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -4048(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4048(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal20Cast10JSReceiver_140EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-4048(%rbp), %rcx
	movq	%r14, %xmm4
	pxor	%xmm0, %xmm0
	movhps	-4032(%rbp), %xmm4
	movl	$32, %edi
	movq	%rax, -104(%rbp)
	leaq	-3936(%rbp), %r14
	movq	%rcx, -112(%rbp)
	movaps	%xmm4, -4064(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm0, -3936(%rbp)
	movq	$0, -3920(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm7
	movq	%r14, %rsi
	leaq	32(%rax), %rdx
	movq	%rax, -3936(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-112(%rbp), %xmm7
	movq	%rdx, -3920(%rbp)
	movups	%xmm7, 16(%rax)
	leaq	-3392(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3928(%rbp)
	movq	%rax, -4072(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L159
	call	_ZdlPv@PLT
.L159:
	movq	-4096(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3584(%rbp), %rax
	cmpq	$0, -3896(%rbp)
	movq	%rax, -4032(%rbp)
	jne	.L364
.L160:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L345:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -3888(%rbp)
	movaps	%xmm0, -3904(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r10d
	movq	-4032(%rbp), %rdi
	movq	%r13, %rsi
	movw	%r10w, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -3904(%rbp)
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3904(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L163
	call	_ZdlPv@PLT
.L163:
	movq	(%rbx), %rax
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movdqu	(%rax), %xmm0
	movaps	%xmm1, -3904(%rbp)
	movq	$0, -3888(%rbp)
	movaps	%xmm0, -4048(%rbp)
	call	_Znwm@PLT
	movdqa	-4048(%rbp), %xmm0
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -3904(%rbp)
	movups	%xmm0, (%rax)
	leaq	-512(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	movq	%rax, -4104(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3904(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L164
	call	_ZdlPv@PLT
.L164:
	movq	-4208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L346:
	movq	-4096(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -3888(%rbp)
	movaps	%xmm0, -3904(%rbp)
	call	_Znwm@PLT
	movq	-4072(%rbp), %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -3904(%rbp)
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3904(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L166
	call	_ZdlPv@PLT
.L166:
	movq	(%rbx), %rax
	movl	$24, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rbx
	movq	(%rax), %r14
	movq	%rbx, -4048(%rbp)
	movq	24(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -4064(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-4064(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-4048(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -4064(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$28, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r13, %r8
	movq	%rbx, %rdx
	leaq	.LC3(%rip), %rcx
	movq	%r15, %rdi
	call	_ZN2v88internal13GetMethod_245EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPKcPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm6
	movq	%rbx, %xmm5
	pxor	%xmm0, %xmm0
	movhps	-4064(%rbp), %xmm5
	movl	$48, %edi
	movq	%rbx, -96(%rbp)
	movhps	-4048(%rbp), %xmm6
	movaps	%xmm5, -4096(%rbp)
	leaq	-3936(%rbp), %r14
	movaps	%xmm6, -4048(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm0, -3936(%rbp)
	movq	%rax, -88(%rbp)
	movq	$0, -3920(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm6
	leaq	48(%rax), %rdx
	movq	%rax, -3936(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	leaq	-3008(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3920(%rbp)
	movq	%rdx, -3928(%rbp)
	movq	%rax, -4080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L167
	call	_ZdlPv@PLT
.L167:
	movq	-4128(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3200(%rbp), %rax
	cmpq	$0, -3896(%rbp)
	movq	%rax, -4064(%rbp)
	jne	.L365
.L168:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L347:
	movq	-4120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -3888(%rbp)
	movaps	%xmm0, -3904(%rbp)
	call	_Znwm@PLT
	movq	-4064(%rbp), %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -3904(%rbp)
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3904(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L171
	call	_ZdlPv@PLT
.L171:
	movq	(%rbx), %rax
	movl	$32, %edi
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm7
	movq	$0, -3888(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm0, -3904(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	movq	%rax, -3904(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	leaq	-2816(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	movq	%rax, -4096(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3904(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L172
	call	_ZdlPv@PLT
.L172:
	movq	-4160(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L348:
	movq	-4128(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -3888(%rbp)
	movaps	%xmm0, -3904(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r9d
	movq	-4080(%rbp), %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	6(%rax), %rdx
	movw	%r9w, 4(%rax)
	movq	%rax, -3904(%rbp)
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3904(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L174
	call	_ZdlPv@PLT
.L174:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	40(%rax), %rdx
	movdqu	(%rax), %xmm6
	movq	$0, -3888(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm0, -3904(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm6
	leaq	40(%rax), %rdx
	movq	%rax, -3904(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm6, 16(%rax)
	leaq	-2624(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	movq	%rax, -4120(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3904(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L175
	call	_ZdlPv@PLT
.L175:
	movq	-4176(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L349:
	movq	-4160(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -3888(%rbp)
	movaps	%xmm0, -3904(%rbp)
	call	_Znwm@PLT
	movq	-4096(%rbp), %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -3904(%rbp)
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3904(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L177
	call	_ZdlPv@PLT
.L177:
	movq	(%rbx), %rax
	movl	$29, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	24(%rax), %r14
	movq	%rcx, -4048(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r14, -112(%rbp)
	movhps	-4048(%rbp), %xmm0
	movq	$0, -3888(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3904(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	movq	%rax, -3904(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	leaq	-704(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	movq	%rax, -4136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3904(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L178
	call	_ZdlPv@PLT
.L178:
	movq	-4248(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L350:
	movq	-4176(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -3888(%rbp)
	movaps	%xmm0, -3904(%rbp)
	call	_Znwm@PLT
	movq	-4120(%rbp), %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -3904(%rbp)
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3904(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L180
	call	_ZdlPv@PLT
.L180:
	movq	(%rbx), %rax
	movl	$28, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	leaq	-3952(%rbp), %r14
	movq	(%rax), %rbx
	movq	24(%rax), %rcx
	movq	%rbx, -4048(%rbp)
	movq	8(%rax), %rbx
	movq	%rcx, -4160(%rbp)
	movq	%rbx, -4176(%rbp)
	movq	16(%rax), %rbx
	movq	32(%rax), %rax
	movq	%rax, -4128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$32, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19IsUndefinedConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	je	.L181
.L183:
	movq	%rbx, %xmm2
	movq	%r14, %rdi
	movhps	-4160(%rbp), %xmm2
	movaps	%xmm2, -4160(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-3904(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -4296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-4160(%rbp), %xmm2
	movq	-4128(%rbp), %xmm0
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -3936(%rbp)
	movq	-3888(%rbp), %rax
	movhps	-4296(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%rax, -3928(%rbp)
	movaps	%xmm2, -112(%rbp)
.L343:
	leaq	-128(%rbp), %rcx
	movl	$4, %ebx
	xorl	%esi, %esi
	movq	%r14, %rdi
	pushq	%rbx
	movq	-4048(%rbp), %r9
	leaq	-3936(%rbp), %rdx
	pushq	%rcx
	movl	$1, %ecx
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	popq	%rdi
	movq	%r14, %rdi
	popq	%r8
	movq	-4048(%rbp), %xmm7
	movq	%rax, %rbx
	movhps	-4176(%rbp), %xmm7
	movaps	%xmm7, -4176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$36, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4048(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal111Is10JSReceiver90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_1464EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15Word32BinaryNotENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm5
	pxor	%xmm0, %xmm0
	movdqa	-4176(%rbp), %xmm7
	movq	-4128(%rbp), %xmm3
	movl	$56, %edi
	movaps	%xmm0, -3904(%rbp)
	movaps	%xmm7, -128(%rbp)
	movdqa	-4160(%rbp), %xmm7
	punpcklqdq	%xmm5, %xmm3
	movq	%r14, -80(%rbp)
	movaps	%xmm3, -4048(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movq	$0, -3888(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	leaq	56(%rax), %rdx
	movq	%rax, -3904(%rbp)
	movq	%rcx, 48(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	leaq	-2432(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	movq	%rax, -4128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3904(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L184
	call	_ZdlPv@PLT
.L184:
	movdqa	-4176(%rbp), %xmm3
	movdqa	-4160(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movdqa	-4048(%rbp), %xmm6
	movq	%r14, -80(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm0, -3904(%rbp)
	movq	$0, -3888(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm3
	leaq	56(%rax), %rdx
	movq	%rax, -3904(%rbp)
	movq	%rcx, 48(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm3, 32(%rax)
	leaq	-2240(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	movq	%rax, -4160(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3904(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L185
	call	_ZdlPv@PLT
.L185:
	movq	-4200(%rbp), %rcx
	movq	-4144(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L352:
	movq	-4200(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	movq	$0, -3968(%rbp)
	movq	$0, -3960(%rbp)
	movq	$0, -3952(%rbp)
	movq	$0, -3936(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3936(%rbp), %rax
	movq	-4160(%rbp), %rdi
	leaq	-3976(%rbp), %rcx
	pushq	%rax
	leaq	-3952(%rbp), %rax
	leaq	-3984(%rbp), %rdx
	pushq	%rax
	leaq	-3960(%rbp), %r9
	leaq	-3968(%rbp), %r8
	leaq	-3992(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectENS0_5BoolTEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESG_PNSA_IS7_EEPNSA_IS8_EE
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal42FromConstexpr6ATbool16ATconstexpr_bool_165EPNS0_8compiler18CodeAssemblerStateEb@PLT
	movl	$64, %edi
	movq	-3936(%rbp), %xmm0
	movq	-3960(%rbp), %xmm1
	movq	-3976(%rbp), %xmm2
	movq	%rax, %xmm5
	movq	-3992(%rbp), %xmm3
	movq	$0, -3888(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movhps	-3952(%rbp), %xmm1
	movhps	-3968(%rbp), %xmm2
	movhps	-3984(%rbp), %xmm3
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -3904(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm6
	leaq	64(%rax), %rdx
	movq	%rax, -3904(%rbp)
	movq	-4048(%rbp), %rdi
	movups	%xmm3, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm6, 48(%rax)
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3904(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L189
	call	_ZdlPv@PLT
.L189:
	movq	-4192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L351:
	movq	-4144(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	movq	$0, -3968(%rbp)
	movq	$0, -3960(%rbp)
	movq	$0, -3952(%rbp)
	movq	$0, -3936(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3936(%rbp), %rax
	movq	-4128(%rbp), %rdi
	leaq	-3976(%rbp), %rcx
	pushq	%rax
	leaq	-3952(%rbp), %rax
	leaq	-3960(%rbp), %r9
	pushq	%rax
	leaq	-3968(%rbp), %r8
	leaq	-3984(%rbp), %rdx
	leaq	-3992(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectENS0_5BoolTEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESG_PNSA_IS7_EEPNSA_IS8_EE
	movq	%r15, %rdi
	call	_ZN2v88internal7Null_63EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3952(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3936(%rbp), %rax
	movl	$64, %edi
	movq	-3960(%rbp), %xmm0
	movq	-3976(%rbp), %xmm1
	movq	%rbx, -72(%rbp)
	movq	-3992(%rbp), %xmm2
	movq	%rax, -80(%rbp)
	movhps	-3952(%rbp), %xmm0
	movhps	-3968(%rbp), %xmm1
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3984(%rbp), %xmm2
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm0, -3904(%rbp)
	movq	$0, -3888(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm2
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm4
	leaq	64(%rax), %rdx
	movq	%rax, -3904(%rbp)
	movq	-4048(%rbp), %rdi
	movups	%xmm7, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm4, 48(%rax)
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3904(%rbp), %rdi
	popq	%rcx
	popq	%rsi
	testq	%rdi, %rdi
	je	.L187
	call	_ZdlPv@PLT
.L187:
	movq	-4192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L353:
	movq	-4192(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$289365102334248711, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3888(%rbp)
	movaps	%xmm0, -3904(%rbp)
	call	_Znwm@PLT
	movq	-4048(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3904(%rbp)
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3904(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L191
	call	_ZdlPv@PLT
.L191:
	movq	(%rbx), %rax
	movl	$48, %edi
	movq	(%rax), %rbx
	movdqu	32(%rax), %xmm0
	movq	40(%rax), %rcx
	movdqu	16(%rax), %xmm1
	movdqu	(%rax), %xmm4
	movq	56(%rax), %r14
	movq	%rbx, -4144(%rbp)
	movq	8(%rax), %rbx
	movq	%rcx, -4320(%rbp)
	movq	%rbx, -4192(%rbp)
	movq	16(%rax), %rbx
	movq	%rbx, -4200(%rbp)
	movq	24(%rax), %rbx
	movq	%rbx, -4296(%rbp)
	movq	32(%rax), %rbx
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3904(%rbp)
	movq	$0, -3888(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm7
	leaq	48(%rax), %rdx
	movq	%rax, -3904(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm7, 32(%rax)
	leaq	-1856(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	movq	%rax, -4176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3904(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L192
	call	_ZdlPv@PLT
.L192:
	movq	-4144(%rbp), %xmm0
	movl	$48, %edi
	movq	$0, -3888(%rbp)
	movhps	-4192(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-4200(%rbp), %xmm0
	movhps	-4296(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%rbx, %xmm0
	movhps	-4320(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3904(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm2
	movdqa	-112(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm4
	leaq	48(%rax), %rdx
	movq	%rax, -3904(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm4, 32(%rax)
	leaq	-1664(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	movq	%rax, -4144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3904(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L193
	call	_ZdlPv@PLT
.L193:
	movq	-4240(%rbp), %rcx
	movq	-4224(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L354:
	movq	-4224(%rbp), %rsi
	movq	%r12, %rdi
	movl	$2055, %r14d
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -3888(%rbp)
	movaps	%xmm0, -3904(%rbp)
	call	_Znwm@PLT
	movq	-4176(%rbp), %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	6(%rax), %rdx
	movw	%r14w, 4(%rax)
	movq	%rax, -3904(%rbp)
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3904(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L195
	call	_ZdlPv@PLT
.L195:
	movq	(%rbx), %rax
	movl	$37, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movq	$0, -3888(%rbp)
	movq	%r14, %xmm5
	movq	%rbx, %xmm0
	movaps	%xmm1, -3904(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -4192(%rbp)
	call	_Znwm@PLT
	movdqa	-4192(%rbp), %xmm0
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -3904(%rbp)
	movups	%xmm0, (%rax)
	leaq	-320(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	movq	%rax, -4192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3904(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L196
	call	_ZdlPv@PLT
.L196:
	movq	-4256(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L355:
	movq	-4240(%rbp), %rsi
	movq	%r12, %rdi
	movl	$2055, %ebx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -3888(%rbp)
	movaps	%xmm0, -3904(%rbp)
	call	_Znwm@PLT
	movq	-4144(%rbp), %rdi
	movq	%r13, %rsi
	movw	%bx, 4(%rax)
	leaq	6(%rax), %rdx
	movl	$117901063, (%rax)
	movq	%rax, -3904(%rbp)
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3904(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L198
	call	_ZdlPv@PLT
.L198:
	movq	(%rbx), %rax
	movl	$42, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	16(%rax), %rsi
	movq	(%rax), %rbx
	movq	24(%rax), %r14
	movq	%rcx, -4224(%rbp)
	movq	32(%rax), %rcx
	movq	40(%rax), %rax
	movq	%rsi, -4240(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -4320(%rbp)
	movq	%r14, -4296(%rbp)
	movq	%rax, -4304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rbx, -4200(%rbp)
	call	_ZN2v88internal22ObjectIsExtensible_311EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$44, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movq	-4320(%rbp), %xmm2
	movq	-4240(%rbp), %xmm7
	movaps	%xmm0, -3904(%rbp)
	movq	-4200(%rbp), %xmm4
	movhps	-4304(%rbp), %xmm2
	movq	%rbx, -80(%rbp)
	movhps	-4296(%rbp), %xmm7
	movhps	-4224(%rbp), %xmm4
	movaps	%xmm2, -4320(%rbp)
	movaps	%xmm7, -4240(%rbp)
	movaps	%xmm4, -4224(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	$0, -3888(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm2
	leaq	56(%rax), %rdx
	movq	%rax, -3904(%rbp)
	movq	%rcx, 48(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm2, 32(%rax)
	leaq	-1472(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	movq	%rax, -4200(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3904(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L199
	call	_ZdlPv@PLT
.L199:
	movdqa	-4224(%rbp), %xmm6
	movdqa	-4240(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movdqa	-4320(%rbp), %xmm5
	movq	%rbx, -80(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm0, -3904(%rbp)
	movq	$0, -3888(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm2
	leaq	56(%rax), %rdx
	leaq	-1280(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm2, 32(%rax)
	movq	%rax, -3904(%rbp)
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3904(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L200
	call	_ZdlPv@PLT
.L200:
	movq	-4112(%rbp), %rcx
	movq	-4288(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1408(%rbp)
	je	.L201
.L356:
	movq	-4288(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	movq	$0, -3968(%rbp)
	movq	$0, -3960(%rbp)
	movq	$0, -3952(%rbp)
	movq	$0, -3936(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3936(%rbp), %rax
	movq	-4200(%rbp), %rdi
	leaq	-3976(%rbp), %rcx
	pushq	%rax
	leaq	-3952(%rbp), %rax
	leaq	-3960(%rbp), %r9
	pushq	%rax
	leaq	-3968(%rbp), %r8
	leaq	-3984(%rbp), %rdx
	leaq	-3992(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_PNS9_IS7_EESJ_
	movl	$45, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3952(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%r10
	popq	%r11
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L357:
	movq	-4112(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3992(%rbp)
	leaq	-1280(%rbp), %r14
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	movq	$0, -3968(%rbp)
	movq	$0, -3960(%rbp)
	movq	$0, -3952(%rbp)
	movq	$0, -3936(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3936(%rbp), %rax
	movq	%r14, %rdi
	leaq	-3960(%rbp), %r9
	pushq	%rax
	leaq	-3952(%rbp), %rax
	leaq	-3968(%rbp), %r8
	pushq	%rax
	leaq	-3976(%rbp), %rcx
	leaq	-3984(%rbp), %rdx
	leaq	-3992(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_PNS9_IS7_EESJ_
	movl	$49, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3968(%rbp), %rdx
	movq	-3992(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal24ObjectGetPrototypeOf_314EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$54, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3952(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal13SameValue_242EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEES6_@PLT
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	-3960(%rbp), %xmm5
	movq	-3976(%rbp), %xmm6
	movq	%rax, -4224(%rbp)
	movq	-3992(%rbp), %xmm3
	movq	-3936(%rbp), %rax
	movq	%r14, -72(%rbp)
	movhps	-3952(%rbp), %xmm5
	movhps	-3968(%rbp), %xmm6
	movaps	%xmm5, -4288(%rbp)
	movhps	-3984(%rbp), %xmm3
	movaps	%xmm6, -4240(%rbp)
	movaps	%xmm3, -4320(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm0, -3904(%rbp)
	movq	%rax, -4296(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -3888(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm6
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	leaq	64(%rax), %rdx
	movq	%rax, -3904(%rbp)
	movdqa	-80(%rbp), %xmm3
	movups	%xmm6, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3904(%rbp), %rdi
	popq	%r8
	popq	%r9
	testq	%rdi, %rdi
	je	.L203
	call	_ZdlPv@PLT
.L203:
	movq	-4296(%rbp), %xmm0
	movq	%r14, %xmm4
	movdqa	-4320(%rbp), %xmm7
	movl	$64, %edi
	movdqa	-4240(%rbp), %xmm2
	movdqa	-4288(%rbp), %xmm6
	movq	$0, -3888(%rbp)
	leaq	-896(%rbp), %r14
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm0, -3904(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm5
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm7
	leaq	64(%rax), %rdx
	movq	%rax, -3904(%rbp)
	movdqa	-80(%rbp), %xmm2
	movups	%xmm5, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm2, 48(%rax)
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3904(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L204
	call	_ZdlPv@PLT
.L204:
	movq	-4272(%rbp), %rcx
	movq	-4264(%rbp), %rdx
	movq	%r12, %rdi
	movq	-4224(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1024(%rbp)
	je	.L205
.L358:
	movq	-4264(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	movq	$0, -3968(%rbp)
	movq	$0, -3960(%rbp)
	movq	$0, -3952(%rbp)
	movq	$0, -3936(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%rbx, %rdi
	leaq	-3936(%rbp), %rax
	pushq	%rax
	leaq	-3952(%rbp), %rax
	leaq	-3984(%rbp), %rcx
	pushq	%rax
	leaq	-3960(%rbp), %rax
	leaq	-3968(%rbp), %r9
	pushq	%rax
	leaq	-3976(%rbp), %r8
	leaq	-3992(%rbp), %rdx
	leaq	-4000(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_PNS9_IS7_EESJ_SJ_
	addq	$32, %rsp
	movl	$55, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3960(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -832(%rbp)
	je	.L206
.L359:
	movq	-4272(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	movq	$0, -3968(%rbp)
	movq	$0, -3960(%rbp)
	movq	$0, -3952(%rbp)
	movq	$0, -3936(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r14, %rdi
	leaq	-3936(%rbp), %rax
	pushq	%rax
	leaq	-3952(%rbp), %rax
	leaq	-3968(%rbp), %r9
	pushq	%rax
	leaq	-3960(%rbp), %rax
	leaq	-3984(%rbp), %rcx
	pushq	%rax
	leaq	-3976(%rbp), %r8
	leaq	-3992(%rbp), %rdx
	leaq	-4000(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10JSReceiverENS0_10HeapObjectES5_NS0_6ObjectES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_PNS9_IS7_EESJ_SJ_
	addq	$32, %rsp
	movl	$57, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$134, %edx
	movq	-4000(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -640(%rbp)
	je	.L207
.L360:
	movq	-4248(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -4224(%rbp)
	movq	$0, -4240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -3888(%rbp)
	movaps	%xmm0, -3904(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movq	-4136(%rbp), %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$8, 2(%rax)
	movq	%rax, -3904(%rbp)
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3904(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L208
	movq	%rax, -4248(%rbp)
	call	_ZdlPv@PLT
	movq	-4248(%rbp), %rax
.L208:
	movq	(%rax), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rdx
	movq	16(%rax), %rax
	testq	%rdx, %rdx
	cmove	-4224(%rbp), %rdx
	testq	%rax, %rax
	cmove	-4240(%rbp), %rax
	movq	%rdx, -4224(%rbp)
	movl	$61, %edx
	movq	%rax, -4240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4240(%rbp), %rdx
	movq	-4224(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal24ObjectGetPrototypeOf_314EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -4224(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4224(%rbp), %r8
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -448(%rbp)
	je	.L211
.L361:
	movq	-4208(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3952(%rbp)
	movq	$0, -3936(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-4104(%rbp), %rdi
	leaq	-3936(%rbp), %rdx
	leaq	-3952(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EE
	movl	$64, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	movl	$145, %edx
	movq	%r13, %rdi
	movq	-3952(%rbp), %rsi
	leaq	.LC3(%rip), %rcx
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -256(%rbp)
	je	.L212
.L362:
	movq	-4256(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -3888(%rbp)
	movaps	%xmm0, -3904(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	-4192(%rbp), %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -3904(%rbp)
	movq	%rdx, -3888(%rbp)
	movq	%rdx, -3896(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3904(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L213
	movq	%rax, -4208(%rbp)
	call	_ZdlPv@PLT
	movq	-4208(%rbp), %rax
.L213:
	movq	(%rax), %rax
	movl	$67, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	movq	(%rax), %r9
	movq	%r9, -4208(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	-4208(%rbp), %r9
	movl	$133, %edx
	movq	%r9, %rsi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L181:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IsNullConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L183
	movq	%rbx, %xmm6
	movq	%r14, %rdi
	movhps	-4160(%rbp), %xmm6
	movaps	%xmm6, -4160(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-3904(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -4296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-4160(%rbp), %xmm6
	movq	-4128(%rbp), %xmm0
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -3936(%rbp)
	movq	-3888(%rbp), %rax
	movhps	-4296(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%rax, -3928(%rbp)
	movaps	%xmm6, -112(%rbp)
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L364:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-4048(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-4064(%rbp), %xmm4
	movl	$24, %edi
	movaps	%xmm0, -3936(%rbp)
	movaps	%xmm4, -128(%rbp)
	movq	%rax, -112(%rbp)
	movq	$0, -3920(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movdqa	-128(%rbp), %xmm5
	movq	%r14, %rsi
	movq	-4032(%rbp), %rdi
	leaq	24(%rax), %rdx
	movq	%rax, -3936(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -3920(%rbp)
	movq	%rdx, -3928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L161
	call	_ZdlPv@PLT
.L161:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L365:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %rsi
	movq	%r14, %rdi
	movdqa	-4048(%rbp), %xmm4
	leaq	-88(%rbp), %rdx
	movq	%rbx, -96(%rbp)
	movdqa	-4096(%rbp), %xmm5
	movaps	%xmm0, -3936(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movq	$0, -3920(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4064(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L169
	call	_ZdlPv@PLT
.L169:
	movq	-4120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L168
.L363:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22418:
	.size	_ZN2v88internal28ProxyGetPrototypeOfAssembler31GenerateProxyGetPrototypeOfImplEv, .-_ZN2v88internal28ProxyGetPrototypeOfAssembler31GenerateProxyGetPrototypeOfImplEv
	.section	.rodata._ZN2v88internal8Builtins28Generate_ProxyGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/proxy-get-prototype-of-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins28Generate_ProxyGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"ProxyGetPrototypeOf"
	.section	.text._ZN2v88internal8Builtins28Generate_ProxyGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins28Generate_ProxyGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins28Generate_ProxyGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins28Generate_ProxyGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE:
.LFB22414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$146, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$853, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L370
.L367:
	movq	%r13, %rdi
	call	_ZN2v88internal28ProxyGetPrototypeOfAssembler31GenerateProxyGetPrototypeOfImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L371
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L367
.L371:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22414:
	.size	_ZN2v88internal8Builtins28Generate_ProxyGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins28Generate_ProxyGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_ProxyGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_ProxyGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_ProxyGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE:
.LFB29051:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29051:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_ProxyGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_ProxyGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_ProxyGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE
	.weak	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
