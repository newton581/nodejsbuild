	.file	"proxy-is-extensible-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.rodata._ZN2v88internal26ProxyIsExtensibleAssembler29GenerateProxyIsExtensibleImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../../deps/v8/../../deps/v8/src/builtins/proxy-is-extensible.tq"
	.section	.rodata._ZN2v88internal26ProxyIsExtensibleAssembler29GenerateProxyIsExtensibleImplEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	"isExtensible"
	.section	.text._ZN2v88internal26ProxyIsExtensibleAssembler29GenerateProxyIsExtensibleImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26ProxyIsExtensibleAssembler29GenerateProxyIsExtensibleImplEv
	.type	_ZN2v88internal26ProxyIsExtensibleAssembler29GenerateProxyIsExtensibleImplEv, @function
_ZN2v88internal26ProxyIsExtensibleAssembler29GenerateProxyIsExtensibleImplEv:
.LFB22418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-2224(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2352(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$2536, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -2408(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	-2408(%rbp), %r12
	movq	%rax, -2528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -2216(%rbp)
	movq	%rax, -2544(%rbp)
	movq	-2408(%rbp), %rax
	movq	$0, -2208(%rbp)
	movq	%rax, -2224(%rbp)
	movq	$0, -2200(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -2216(%rbp)
	leaq	-2168(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2200(%rbp)
	movq	%rdx, -2208(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2184(%rbp)
	movq	%rax, -2424(%rbp)
	movq	$0, -2192(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$72, %edi
	movq	$0, -2024(%rbp)
	movq	$0, -2016(%rbp)
	movq	%rax, -2032(%rbp)
	movq	$0, -2008(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -2024(%rbp)
	leaq	-1976(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2008(%rbp)
	movq	%rdx, -2016(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1992(%rbp)
	movq	%rax, -2504(%rbp)
	movq	$0, -2000(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1832(%rbp)
	movq	$0, -1824(%rbp)
	movq	%rax, -1840(%rbp)
	movq	$0, -1816(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1832(%rbp)
	leaq	-1784(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1816(%rbp)
	movq	%rdx, -1824(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1800(%rbp)
	movq	%rax, -2432(%rbp)
	movq	$0, -1808(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1640(%rbp)
	movq	$0, -1632(%rbp)
	movq	%rax, -1648(%rbp)
	movq	$0, -1624(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1640(%rbp)
	leaq	-1592(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1624(%rbp)
	movq	%rdx, -1632(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1608(%rbp)
	movq	%rax, -2496(%rbp)
	movq	$0, -1616(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1448(%rbp)
	movq	$0, -1440(%rbp)
	movq	%rax, -1456(%rbp)
	movq	$0, -1432(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1448(%rbp)
	leaq	-1400(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1432(%rbp)
	movq	%rdx, -1440(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1416(%rbp)
	movq	%rax, -2440(%rbp)
	movq	$0, -1424(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1256(%rbp)
	movq	$0, -1248(%rbp)
	movq	%rax, -1264(%rbp)
	movq	$0, -1240(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1256(%rbp)
	leaq	-1208(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1240(%rbp)
	movq	%rdx, -1248(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1224(%rbp)
	movq	%rax, -2448(%rbp)
	movq	$0, -1232(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1064(%rbp)
	movq	$0, -1056(%rbp)
	movq	%rax, -1072(%rbp)
	movq	$0, -1048(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1064(%rbp)
	leaq	-1016(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1048(%rbp)
	movq	%rdx, -1056(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1032(%rbp)
	movq	%rax, -2480(%rbp)
	movq	$0, -1040(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$168, %edi
	movq	$0, -872(%rbp)
	movq	$0, -864(%rbp)
	movq	%rax, -880(%rbp)
	movq	$0, -856(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -872(%rbp)
	leaq	-824(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -856(%rbp)
	movq	%rdx, -864(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -840(%rbp)
	movq	%rax, -2488(%rbp)
	movq	$0, -848(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$168, %edi
	movq	$0, -680(%rbp)
	movq	$0, -672(%rbp)
	movq	%rax, -688(%rbp)
	movq	$0, -664(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -680(%rbp)
	leaq	-632(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -664(%rbp)
	movq	%rdx, -672(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -648(%rbp)
	movq	%rax, -2464(%rbp)
	movq	$0, -656(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$72, %edi
	movq	$0, -488(%rbp)
	movq	$0, -480(%rbp)
	movq	%rax, -496(%rbp)
	movq	$0, -472(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -488(%rbp)
	leaq	-440(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -472(%rbp)
	movq	%rdx, -480(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -456(%rbp)
	movq	%rax, -2472(%rbp)
	movq	$0, -464(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$48, %edi
	movq	$0, -296(%rbp)
	movq	$0, -288(%rbp)
	movq	%rax, -304(%rbp)
	movq	$0, -280(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%r12, %rsi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -296(%rbp)
	leaq	-248(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -280(%rbp)
	movq	%rdx, -288(%rbp)
	xorl	%edx, %edx
	movq	%rax, -2456(%rbp)
	movups	%xmm0, -264(%rbp)
	movq	$0, -272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-2528(%rbp), %xmm1
	movaps	%xmm0, -2352(%rbp)
	movhps	-2544(%rbp), %xmm1
	movq	$0, -2336(%rbp)
	movaps	%xmm1, -2528(%rbp)
	call	_Znwm@PLT
	movdqa	-2528(%rbp), %xmm1
	movq	%r14, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -2352(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L8
	call	_ZdlPv@PLT
.L8:
	movq	-2424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2160(%rbp)
	jne	.L264
	cmpq	$0, -1968(%rbp)
	jne	.L265
.L14:
	cmpq	$0, -1776(%rbp)
	jne	.L266
.L17:
	cmpq	$0, -1584(%rbp)
	jne	.L267
.L22:
	cmpq	$0, -1392(%rbp)
	jne	.L268
.L25:
	cmpq	$0, -1200(%rbp)
	jne	.L269
.L28:
	cmpq	$0, -1008(%rbp)
	jne	.L270
.L31:
	cmpq	$0, -816(%rbp)
	jne	.L271
.L38:
	cmpq	$0, -624(%rbp)
	jne	.L272
.L40:
	cmpq	$0, -432(%rbp)
	jne	.L273
.L42:
	cmpq	$0, -240(%rbp)
	jne	.L274
.L46:
	movq	-2456(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L48
	call	_ZdlPv@PLT
.L48:
	movq	-288(%rbp), %rbx
	movq	-296(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L49
	.p2align 4,,10
	.p2align 3
.L53:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L50
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L53
.L51:
	movq	-296(%rbp), %r13
.L49:
	testq	%r13, %r13
	je	.L54
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L54:
	movq	-2472(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-464(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L55
	call	_ZdlPv@PLT
.L55:
	movq	-480(%rbp), %rbx
	movq	-488(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L56
	.p2align 4,,10
	.p2align 3
.L60:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L57
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L60
.L58:
	movq	-488(%rbp), %r13
.L56:
	testq	%r13, %r13
	je	.L61
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L61:
	movq	-2464(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L62
	call	_ZdlPv@PLT
.L62:
	movq	-672(%rbp), %rbx
	movq	-680(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L63
	.p2align 4,,10
	.p2align 3
.L67:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L64
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L67
.L65:
	movq	-680(%rbp), %r13
.L63:
	testq	%r13, %r13
	je	.L68
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L68:
	movq	-2488(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-848(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L69
	call	_ZdlPv@PLT
.L69:
	movq	-864(%rbp), %rbx
	movq	-872(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L70
	.p2align 4,,10
	.p2align 3
.L74:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L71
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L74
.L72:
	movq	-872(%rbp), %r13
.L70:
	testq	%r13, %r13
	je	.L75
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L75:
	movq	-2480(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1040(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L76
	call	_ZdlPv@PLT
.L76:
	movq	-1056(%rbp), %rbx
	movq	-1064(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L77
	.p2align 4,,10
	.p2align 3
.L81:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L78
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L81
.L79:
	movq	-1064(%rbp), %r13
.L77:
	testq	%r13, %r13
	je	.L82
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L82:
	movq	-2448(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L83
	call	_ZdlPv@PLT
.L83:
	movq	-1248(%rbp), %rbx
	movq	-1256(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L84
	.p2align 4,,10
	.p2align 3
.L88:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L85
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L88
.L86:
	movq	-1256(%rbp), %r13
.L84:
	testq	%r13, %r13
	je	.L89
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L89:
	movq	-2440(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L90
	call	_ZdlPv@PLT
.L90:
	movq	-1440(%rbp), %rbx
	movq	-1448(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L91
	.p2align 4,,10
	.p2align 3
.L95:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L92
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L95
.L93:
	movq	-1448(%rbp), %r13
.L91:
	testq	%r13, %r13
	je	.L96
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L96:
	movq	-2496(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1616(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L97
	call	_ZdlPv@PLT
.L97:
	movq	-1632(%rbp), %rbx
	movq	-1640(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L98
	.p2align 4,,10
	.p2align 3
.L102:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L99
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L102
.L100:
	movq	-1640(%rbp), %r13
.L98:
	testq	%r13, %r13
	je	.L103
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L103:
	movq	-2432(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1808(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L104
	call	_ZdlPv@PLT
.L104:
	movq	-1824(%rbp), %rbx
	movq	-1832(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L105
	.p2align 4,,10
	.p2align 3
.L109:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L106
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L109
.L107:
	movq	-1832(%rbp), %r13
.L105:
	testq	%r13, %r13
	je	.L110
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L110:
	movq	-2504(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2000(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L111
	call	_ZdlPv@PLT
.L111:
	movq	-2016(%rbp), %rbx
	movq	-2024(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L112
	.p2align 4,,10
	.p2align 3
.L116:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L113
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L116
.L114:
	movq	-2024(%rbp), %r13
.L112:
	testq	%r13, %r13
	je	.L117
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L117:
	movq	-2424(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L118
	call	_ZdlPv@PLT
.L118:
	movq	-2208(%rbp), %rbx
	movq	-2216(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L119
	.p2align 4,,10
	.p2align 3
.L123:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L120
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L123
.L121:
	movq	-2216(%rbp), %r13
.L119:
	testq	%r13, %r13
	je	.L124
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L124:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L275
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L123
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L113:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L116
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L106:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L109
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L99:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L102
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L92:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L95
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L85:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L88
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L78:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L81
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L71:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L74
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L64:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L67
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L50:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L53
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L57:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L60
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L264:
	movq	-2424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	movq	(%rbx), %rax
	movl	$13, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r14
	movq	8(%rax), %rax
	movq	%rax, -2528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$21, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-2528(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal20Cast10JSReceiver_140EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm2
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movhps	-2528(%rbp), %xmm2
	movq	%rbx, -96(%rbp)
	leaq	-2384(%rbp), %r14
	movaps	%xmm2, -2528(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm0, -2384(%rbp)
	movq	%rax, -88(%rbp)
	movq	$0, -2368(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm5
	movq	%r14, %rsi
	leaq	32(%rax), %rdx
	leaq	-1840(%rbp), %rdi
	movq	%rax, -2384(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -2368(%rbp)
	movq	%rdx, -2376(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L11
	call	_ZdlPv@PLT
.L11:
	movq	-2432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2344(%rbp)
	jne	.L276
.L12:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1968(%rbp)
	je	.L14
.L265:
	movq	-2504(%rbp), %rsi
	movq	%r12, %rdi
	movl	$1799, %ebx
	leaq	-2032(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%bx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L15
	call	_ZdlPv@PLT
.L15:
	movq	(%rbx), %rax
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movdqu	(%rax), %xmm0
	movaps	%xmm1, -2352(%rbp)
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2528(%rbp)
	call	_Znwm@PLT
	movdqa	-2528(%rbp), %xmm0
	leaq	-304(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -2352(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L16
	call	_ZdlPv@PLT
.L16:
	movq	-2456(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1776(%rbp)
	je	.L17
.L266:
	movq	-2432(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1840(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L18
	call	_ZdlPv@PLT
.L18:
	movq	(%rbx), %rax
	movl	$24, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	24(%rax), %rbx
	movq	%rcx, -2528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -2544(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-2544(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-2528(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -2544(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$28, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r13, %r8
	movq	%rbx, %rdx
	leaq	.LC2(%rip), %rcx
	movq	%r15, %rdi
	call	_ZN2v88internal13GetMethod_245EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPKcPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm4
	movq	%rbx, %xmm3
	pxor	%xmm0, %xmm0
	movhps	-2544(%rbp), %xmm3
	movl	$48, %edi
	movq	%rbx, -80(%rbp)
	movhps	-2528(%rbp), %xmm4
	movaps	%xmm3, -2544(%rbp)
	leaq	-2384(%rbp), %r14
	movaps	%xmm4, -2528(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -2384(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -2368(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm5
	movq	%r14, %rsi
	leaq	48(%rax), %rdx
	leaq	-1456(%rbp), %rdi
	movq	%rax, -2384(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-80(%rbp), %xmm7
	movups	%xmm5, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -2368(%rbp)
	movq	%rdx, -2376(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L19
	call	_ZdlPv@PLT
.L19:
	movq	-2440(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2344(%rbp)
	jne	.L277
.L20:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1584(%rbp)
	je	.L22
.L267:
	movq	-2496(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1648(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L23
	call	_ZdlPv@PLT
.L23:
	movq	(%rbx), %rax
	movl	$32, %edi
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm6
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-1264(%rbp), %rdi
	movq	%rax, -2352(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L24
	call	_ZdlPv@PLT
.L24:
	movq	-2448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1392(%rbp)
	je	.L25
.L268:
	movq	-2440(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1456(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r11d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	6(%rax), %rdx
	movw	%r11w, 4(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L26
	call	_ZdlPv@PLT
.L26:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	40(%rax), %rdx
	movdqu	(%rax), %xmm4
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -80(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm7
	leaq	40(%rax), %rdx
	leaq	-1072(%rbp), %rdi
	movq	%rax, -2352(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L27
	call	_ZdlPv@PLT
.L27:
	movq	-2480(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1200(%rbp)
	je	.L28
.L269:
	movq	-2448(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1264(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L29
	call	_ZdlPv@PLT
.L29:
	movq	(%rbx), %rax
	movl	$29, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	24(%rax), %r14
	movq	%rcx, -2528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r14, -96(%rbp)
	movhps	-2528(%rbp), %xmm0
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-112(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-496(%rbp), %rdi
	movq	%rax, -2352(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L30
	call	_ZdlPv@PLT
.L30:
	movq	-2472(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1008(%rbp)
	je	.L31
.L270:
	movq	-2480(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1072(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L32
	call	_ZdlPv@PLT
.L32:
	movq	(%rbx), %rax
	movl	$28, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	leaq	-2400(%rbp), %r14
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rcx, -2528(%rbp)
	movq	8(%rax), %rcx
	movq	%rcx, -2560(%rbp)
	movq	24(%rax), %rcx
	movq	32(%rax), %rax
	movq	%rcx, -2512(%rbp)
	movq	%rax, -2568(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$33, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19IsUndefinedConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	je	.L33
.L35:
	movq	%rbx, %xmm7
	movq	%r14, %rdi
	movhps	-2512(%rbp), %xmm7
	movaps	%xmm7, -2544(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	xorl	%edx, %edx
	movq	%rax, %rsi
.L263:
	movq	%r13, %rdi
	movl	$4, %ebx
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	%r14, %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-2352(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -2576(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-112(%rbp), %rcx
	pushq	%rbx
	xorl	%esi, %esi
	pushq	%rcx
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	movq	-2568(%rbp), %xmm0
	movdqa	-2544(%rbp), %xmm7
	movq	%rax, -2384(%rbp)
	leaq	-2384(%rbp), %rdx
	movq	-2336(%rbp), %rax
	movq	-2528(%rbp), %r9
	movq	%r14, %rdi
	movhps	-2576(%rbp), %xmm0
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -2376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	popq	%r9
	movq	%r14, %rdi
	popq	%r10
	movq	-2528(%rbp), %xmm5
	movq	%rax, %rbx
	movhps	-2560(%rbp), %xmm5
	movaps	%xmm5, -2560(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal13ToBoolean_240EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEE@PLT
	movl	$36, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	%rax, -2576(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2512(%rbp), %rdx
	movq	-2528(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal22ObjectIsExtensible_311EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal13ToBoolean_240EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEE@PLT
	movl	$40, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14Word32NotEqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movq	-2568(%rbp), %xmm6
	movdqa	-2560(%rbp), %xmm5
	movdqa	-2544(%rbp), %xmm4
	movaps	%xmm0, -2352(%rbp)
	movhps	-2576(%rbp), %xmm6
	movq	%rbx, -64(%rbp)
	movaps	%xmm6, -2528(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm6, -80(%rbp)
	movq	$0, -2336(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-112(%rbp), %xmm2
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm5
	leaq	56(%rax), %rdx
	leaq	-880(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L36
	call	_ZdlPv@PLT
.L36:
	movdqa	-2560(%rbp), %xmm7
	movdqa	-2544(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movdqa	-2528(%rbp), %xmm4
	movq	%rbx, -64(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm0, -2352(%rbp)
	movq	$0, -2336(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-112(%rbp), %xmm2
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm5
	leaq	56(%rax), %rdx
	leaq	-688(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L37
	call	_ZdlPv@PLT
.L37:
	movq	-2464(%rbp), %rcx
	movq	-2488(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -816(%rbp)
	je	.L38
.L271:
	movq	-2488(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-880(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movl	$1031, %r8d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	7(%rax), %rdx
	movw	%r8w, 4(%rax)
	movb	$4, 6(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L39
	call	_ZdlPv@PLT
.L39:
	movq	(%rbx), %rax
	movl	$43, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	48(%rax), %r14
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler21SelectBooleanConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$41, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$138, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -624(%rbp)
	je	.L40
.L272:
	movq	-2464(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-688(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movl	$1031, %edi
	movq	%r13, %rsi
	movw	%di, 4(%rax)
	leaq	7(%rax), %rdx
	movq	%r14, %rdi
	movl	$117901063, (%rax)
	movb	$4, 6(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L41
	call	_ZdlPv@PLT
.L41:
	movq	(%rbx), %rax
	movl	$46, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	40(%rax), %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler21SelectBooleanConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -432(%rbp)
	je	.L42
.L273:
	movq	-2472(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	movq	$0, -2528(%rbp)
	leaq	-496(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$8, 2(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L43
	call	_ZdlPv@PLT
.L43:
	movq	(%rbx), %rax
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rdx
	movq	16(%rax), %rax
	testq	%rdx, %rdx
	cmovne	%rdx, %r14
	testq	%rax, %rax
	cmove	-2528(%rbp), %rax
	movl	$50, %edx
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal22ObjectIsExtensible_311EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -240(%rbp)
	je	.L46
.L274:
	movq	-2456(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-304(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L47
	call	_ZdlPv@PLT
.L47:
	movq	(%rbx), %rax
	movl	$53, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movl	$145, %edx
	leaq	.LC2(%rip), %rcx
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IsNullConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L35
	movq	%rbx, %xmm7
	movq	%r14, %rdi
	movhps	-2512(%rbp), %xmm7
	movaps	%xmm7, -2544(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	movq	%rax, %rsi
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L276:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movdqa	-2528(%rbp), %xmm6
	movaps	%xmm0, -2384(%rbp)
	movaps	%xmm6, -112(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -2368(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-112(%rbp), %xmm4
	movq	%r14, %rsi
	leaq	24(%rax), %rdx
	leaq	-2032(%rbp), %rdi
	movq	%rax, -2384(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -2368(%rbp)
	movq	%rdx, -2376(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L13
	call	_ZdlPv@PLT
.L13:
	movq	-2504(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L277:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movdqa	-2528(%rbp), %xmm2
	movdqa	-2544(%rbp), %xmm3
	movq	%rbx, -80(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -2384(%rbp)
	movq	$0, -2368(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm4
	leaq	40(%rax), %rdx
	leaq	-1648(%rbp), %rdi
	movq	%rax, -2384(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2368(%rbp)
	movq	%rdx, -2376(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L21
	call	_ZdlPv@PLT
.L21:
	movq	-2496(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L20
.L275:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22418:
	.size	_ZN2v88internal26ProxyIsExtensibleAssembler29GenerateProxyIsExtensibleImplEv, .-_ZN2v88internal26ProxyIsExtensibleAssembler29GenerateProxyIsExtensibleImplEv
	.section	.rodata._ZN2v88internal8Builtins26Generate_ProxyIsExtensibleEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/proxy-is-extensible-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins26Generate_ProxyIsExtensibleEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"ProxyIsExtensible"
	.section	.text._ZN2v88internal8Builtins26Generate_ProxyIsExtensibleEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins26Generate_ProxyIsExtensibleEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins26Generate_ProxyIsExtensibleEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins26Generate_ProxyIsExtensibleEPNS0_8compiler18CodeAssemblerStateE:
.LFB22414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$146, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$855, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L282
.L279:
	movq	%r13, %rdi
	call	_ZN2v88internal26ProxyIsExtensibleAssembler29GenerateProxyIsExtensibleImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L283
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L279
.L283:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22414:
	.size	_ZN2v88internal8Builtins26Generate_ProxyIsExtensibleEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins26Generate_ProxyIsExtensibleEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins26Generate_ProxyIsExtensibleEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins26Generate_ProxyIsExtensibleEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins26Generate_ProxyIsExtensibleEPNS0_8compiler18CodeAssemblerStateE:
.LFB28983:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28983:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins26Generate_ProxyIsExtensibleEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins26Generate_ProxyIsExtensibleEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins26Generate_ProxyIsExtensibleEPNS0_8compiler18CodeAssemblerStateE
	.weak	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
