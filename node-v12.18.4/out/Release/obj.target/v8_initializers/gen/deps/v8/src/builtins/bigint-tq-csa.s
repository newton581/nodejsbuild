	.file	"bigint-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0:
.LFB29720:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	%rsi, %r13
	pushq	%rbx
	movq	%r13, %rax
	sarq	$3, %rax
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%r13, %r13
	js	.L14
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%rdx, %r14
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.L9
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%rax, %rdi
.L9:
	leaq	(%rdi,%r13), %r15
	movq	%rdi, (%rbx)
	movq	%r15, 16(%rbx)
	cmpq	%r12, %r14
	je	.L10
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
.L10:
	movq	%r15, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29720:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0:
.LFB29719:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	subq	%rsi, %rdx
	js	.L21
	movq	%rdi, %rbx
	je	.L17
	movq	%rdx, %rdi
	movq	%rdx, %r12
	movq	%rsi, %r13
	call	_Znwm@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	leaq	(%rax,%r12), %r14
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	%r14, 16(%rbx)
	call	memcpy@PLT
	movq	%r14, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	xorl	%r14d, %r14d
	movq	$0, (%rdi)
	movq	$0, 16(%rdi)
	movq	%r14, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29719:
	.size	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0:
.LFB29718:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	movabsq	$384307168202282325, %rax
	cmpq	%rax, %rdx
	ja	.L39
	pxor	%xmm0, %xmm0
	leaq	(%rdx,%rdx,2), %r14
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	%rdx, %r12
	movups	%xmm0, 8(%rdi)
	salq	$3, %r14
	xorl	%esi, %esi
	movq	$0, 24(%rdi)
	testq	%rdx, %rdx
	je	.L28
	movq	%r14, %rdi
	call	_Znwm@PLT
	leaq	-1(%r12), %rdx
	leaq	(%rax,%r14), %rsi
	movq	%rax, 8(%rbx)
	movq	%rsi, 24(%rbx)
	cmpq	$2, %rdx
	jbe	.L29
	movq	%r12, %rdi
	movq	%r12, %rcx
	pxor	%xmm0, %xmm0
	movq	%rax, %rdx
	shrq	%rdi
	andq	$-2, %rcx
	addq	%rdi, %rcx
	salq	$4, %rcx
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L26:
	movups	%xmm0, (%rdx)
	addq	$48, %rdx
	movups	%xmm0, -32(%rdx)
	movups	%xmm0, -16(%rdx)
	cmpq	%rcx, %rdx
	jne	.L26
	movq	%r12, %rdx
	movq	%r12, %rcx
	andq	$-2, %rdx
	andl	$1, %ecx
	leaq	(%rdx,%rdx,2), %rdi
	leaq	(%rax,%rdi,8), %rax
	cmpq	%rdx, %r12
	je	.L28
.L25:
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	cmpq	$1, %rcx
	je	.L28
	movq	$0, 40(%rax)
	movups	%xmm0, 24(%rax)
	cmpq	$2, %rcx
	je	.L28
	movq	$0, 64(%rax)
	movups	%xmm0, 48(%rax)
.L28:
	movq	%rsi, 16(%rbx)
	pxor	%xmm0, %xmm0
	leaq	56(%rbx), %rdi
	movq	%r13, %rsi
	movups	%xmm0, 32(%rbx)
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	$0, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	%r12, %rcx
	jmp	.L25
.L39:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29718:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev,"axG",@progbits,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev:
.LFB22413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$56, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L41
	call	_ZdlPv@PLT
.L41:
	movq	16(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	%r12, %r13
	je	.L42
	.p2align 4,,10
	.p2align 3
.L46:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L43
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L46
.L44:
	movq	8(%rbx), %r12
.L42:
	testq	%r12, %r12
	je	.L40
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L46
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L40:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22413:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev
	.set	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.section	.rodata._ZN2v88internal17kPositiveSign_257EPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../../deps/v8/../../deps/v8/src/builtins/bigint.tq"
	.section	.text._ZN2v88internal17kPositiveSign_257EPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal17kPositiveSign_257EPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal17kPositiveSign_257EPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal17kPositiveSign_257EPNS0_8compiler18CodeAssemblerStateE:
.LFB22420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-240(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-280(%rbp), %r13
	pushq	%r12
	movq	%r13, %rsi
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-272(%rbp), %rbx
	subq	$248, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -280(%rbp)
	movq	%rdi, -240(%rbp)
	movq	%r14, %rdi
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -192(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L53
	call	_ZdlPv@PLT
.L53:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L54
	call	_ZdlPv@PLT
.L54:
	movl	$27, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal45FromConstexpr8ATuint3217ATconstexpr_int31_161EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L55
	call	_ZdlPv@PLT
.L55:
	movq	-224(%rbp), %r14
	movq	-232(%rbp), %r12
	cmpq	%r12, %r14
	je	.L56
	.p2align 4,,10
	.p2align 3
.L60:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L57
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %r14
	jne	.L60
.L58:
	movq	-232(%rbp), %r12
.L56:
	testq	%r12, %r12
	je	.L61
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L61:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L76
	addq	$248, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %r14
	jne	.L60
	jmp	.L58
.L76:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22420:
	.size	_ZN2v88internal17kPositiveSign_257EPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal17kPositiveSign_257EPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal17kNegativeSign_258EPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal17kNegativeSign_258EPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal17kNegativeSign_258EPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal17kNegativeSign_258EPNS0_8compiler18CodeAssemblerStateE:
.LFB22424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-240(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-280(%rbp), %r13
	pushq	%r12
	movq	%r13, %rsi
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-272(%rbp), %rbx
	subq	$248, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -280(%rbp)
	movq	%rdi, -240(%rbp)
	movq	%r14, %rdi
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -192(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L78
	call	_ZdlPv@PLT
.L78:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L79
	call	_ZdlPv@PLT
.L79:
	movl	$28, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal45FromConstexpr8ATuint3217ATconstexpr_int31_161EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L80
	call	_ZdlPv@PLT
.L80:
	movq	-224(%rbp), %r14
	movq	-232(%rbp), %r12
	cmpq	%r12, %r14
	je	.L81
	.p2align 4,,10
	.p2align 3
.L85:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L82
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %r14
	jne	.L85
.L83:
	movq	-232(%rbp), %r12
.L81:
	testq	%r12, %r12
	je	.L86
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L86:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L101
	addq	$248, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %r14
	jne	.L85
	jmp	.L83
.L101:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22424:
	.size	_ZN2v88internal17kNegativeSign_258EPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal17kNegativeSign_258EPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal19IsCanonicalized_259EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19IsCanonicalized_259EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE
	.type	_ZN2v88internal19IsCanonicalized_259EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE, @function
_ZN2v88internal19IsCanonicalized_259EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE:
.LFB22425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-952(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1048(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$1080, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1048(%rbp)
	movq	%rdi, -1008(%rbp)
	movl	$24, %edi
	movq	$0, -1000(%rbp)
	movq	$0, -992(%rbp)
	movq	$0, -984(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r15, %rdi
	movq	$0, 16(%rax)
	movq	%rax, -1000(%rbp)
	leaq	-1008(%rbp), %rax
	movq	%rdx, -984(%rbp)
	movq	%rdx, -992(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -968(%rbp)
	movq	%rax, -1104(%rbp)
	movq	$0, -976(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1048(%rbp), %rax
	movl	$48, %edi
	movq	$0, -808(%rbp)
	movq	$0, -800(%rbp)
	movq	%rax, -816(%rbp)
	movq	$0, -792(%rbp)
	call	_Znwm@PLT
	leaq	-760(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movq	%rcx, %rbx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movq	%rbx, %rdi
	movups	%xmm0, 32(%rax)
	movq	%rdx, -792(%rbp)
	movq	%rdx, -800(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1072(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -776(%rbp)
	movq	%rax, -808(%rbp)
	movq	$0, -784(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1048(%rbp), %rax
	movl	$48, %edi
	movq	$0, -616(%rbp)
	movq	$0, -608(%rbp)
	movq	%rax, -624(%rbp)
	movq	$0, -600(%rbp)
	call	_Znwm@PLT
	leaq	-568(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movq	%rcx, %rbx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movq	%rbx, %rdi
	movups	%xmm0, 32(%rax)
	movq	%rdx, -600(%rbp)
	movq	%rdx, -608(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1080(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -584(%rbp)
	movq	%rax, -616(%rbp)
	movq	$0, -592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1048(%rbp), %rax
	movl	$48, %edi
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	%rax, -432(%rbp)
	movq	$0, -408(%rbp)
	call	_Znwm@PLT
	leaq	-376(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movq	%rcx, %rbx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movq	%rbx, %rdi
	leaq	-184(%rbp), %rbx
	movups	%xmm0, 32(%rax)
	movq	%rdx, -408(%rbp)
	movq	%rdx, -416(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1064(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -392(%rbp)
	movq	%rax, -424(%rbp)
	movq	$0, -400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1048(%rbp), %rax
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -1024(%rbp)
	movaps	%xmm0, -1040(%rbp)
	call	_Znwm@PLT
	movq	-1104(%rbp), %rdi
	movq	%r12, (%rax)
	leaq	-1040(%rbp), %r12
	leaq	8(%rax), %rdx
	movq	%r12, %rsi
	movq	%rax, -1040(%rbp)
	movq	%rdx, -1024(%rbp)
	movq	%rdx, -1032(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1040(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L103
	call	_ZdlPv@PLT
.L103:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -944(%rbp)
	jne	.L219
	cmpq	$0, -752(%rbp)
	jne	.L220
.L108:
	cmpq	$0, -560(%rbp)
	jne	.L221
.L111:
	cmpq	$0, -368(%rbp)
	leaq	-240(%rbp), %r14
	jne	.L222
.L114:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1024(%rbp)
	movaps	%xmm0, -1040(%rbp)
	call	_Znwm@PLT
	movl	$1031, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1040(%rbp)
	movq	%rdx, -1024(%rbp)
	movq	%rdx, -1032(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1040(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L117
	call	_ZdlPv@PLT
.L117:
	movq	(%r12), %rax
	movq	%rbx, %rdi
	movq	8(%rax), %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L118
	call	_ZdlPv@PLT
.L118:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L119
	.p2align 4,,10
	.p2align 3
.L123:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L120
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L123
.L121:
	movq	-232(%rbp), %r14
.L119:
	testq	%r14, %r14
	je	.L124
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L124:
	movq	-1064(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L125
	call	_ZdlPv@PLT
.L125:
	movq	-416(%rbp), %rbx
	movq	-424(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L126
	.p2align 4,,10
	.p2align 3
.L130:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L127
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L130
.L128:
	movq	-424(%rbp), %r14
.L126:
	testq	%r14, %r14
	je	.L131
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L131:
	movq	-1080(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L132
	call	_ZdlPv@PLT
.L132:
	movq	-608(%rbp), %rbx
	movq	-616(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L133
	.p2align 4,,10
	.p2align 3
.L137:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L134
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L137
.L135:
	movq	-616(%rbp), %r14
.L133:
	testq	%r14, %r14
	je	.L138
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L138:
	movq	-1072(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L139
	call	_ZdlPv@PLT
.L139:
	movq	-800(%rbp), %rbx
	movq	-808(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L140
	.p2align 4,,10
	.p2align 3
.L144:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L141
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L144
.L142:
	movq	-808(%rbp), %r14
.L140:
	testq	%r14, %r14
	je	.L145
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L145:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-976(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L146
	call	_ZdlPv@PLT
.L146:
	movq	-992(%rbp), %rbx
	movq	-1000(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L147
	.p2align 4,,10
	.p2align 3
.L151:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L148
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L151
.L149:
	movq	-1000(%rbp), %r14
.L147:
	testq	%r14, %r14
	je	.L152
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L152:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L223
	addq	$1080, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L151
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L141:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L144
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L134:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L137
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L120:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L123
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L127:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L130
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L219:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -1024(%rbp)
	movaps	%xmm0, -1040(%rbp)
	call	_Znwm@PLT
	movq	-1104(%rbp), %rdi
	movq	%r12, %rsi
	movb	$7, (%rax)
	leaq	1(%rax), %rdx
	movq	%rax, -1040(%rbp)
	movq	%rdx, -1024(%rbp)
	movq	%rdx, -1032(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1040(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L105
	movq	%rax, -1104(%rbp)
	call	_ZdlPv@PLT
	movq	-1104(%rbp), %rax
.L105:
	movq	(%rax), %rax
	movl	$51, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	%rax, -1104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1104(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadBigIntBitfieldENS0_8compiler5TNodeINS0_6BigIntEEE@PLT
	movl	$2147483646, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12DecodeWord32ENS0_8compiler11SloppyTNodeINS0_7Word32TEEEjj@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -1088(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$53, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -1112(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1112(%rbp), %rdx
	movq	-1088(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, -1112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1104(%rbp), %xmm2
	movaps	%xmm0, -1040(%rbp)
	movhps	-1088(%rbp), %xmm2
	movq	$0, -1024(%rbp)
	movaps	%xmm2, -1104(%rbp)
	call	_Znwm@PLT
	movdqa	-1104(%rbp), %xmm3
	leaq	-816(%rbp), %rdi
	movq	%r12, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1040(%rbp)
	movups	%xmm3, (%rax)
	movq	%rdx, -1024(%rbp)
	movq	%rdx, -1032(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1040(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L106
	call	_ZdlPv@PLT
.L106:
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	$0, -1024(%rbp)
	movaps	%xmm0, -1040(%rbp)
	call	_Znwm@PLT
	movdqa	-1104(%rbp), %xmm4
	leaq	-624(%rbp), %rdi
	movq	%r12, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1040(%rbp)
	movups	%xmm4, (%rax)
	movq	%rdx, -1024(%rbp)
	movq	%rdx, -1032(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1040(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L107
	call	_ZdlPv@PLT
.L107:
	movq	-1080(%rbp), %rcx
	movq	-1072(%rbp), %rdx
	movq	%r13, %rdi
	movq	-1112(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -752(%rbp)
	je	.L108
.L220:
	movq	-1072(%rbp), %rsi
	leaq	-816(%rbp), %r8
	movq	%r13, %rdi
	movq	%r8, -1104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1024(%rbp)
	movaps	%xmm0, -1040(%rbp)
	call	_Znwm@PLT
	movl	$1287, %r8d
	movq	%r12, %rsi
	movw	%r8w, (%rax)
	movq	-1104(%rbp), %r8
	leaq	2(%rax), %rdx
	movq	%rax, -1040(%rbp)
	movq	%r8, %rdi
	movq	%rdx, -1024(%rbp)
	movq	%rdx, -1032(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1040(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L109
	movq	%rax, -1104(%rbp)
	call	_ZdlPv@PLT
	movq	-1104(%rbp), %rax
.L109:
	movq	(%rax), %rax
	movl	$54, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	%rax, -1104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1104(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadBigIntBitfieldENS0_8compiler5TNodeINS0_6BigIntEEE@PLT
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12DecodeWord32ENS0_8compiler11SloppyTNodeINS0_7Word32TEEEjj@PLT
	movq	%r12, %rdi
	movq	%rax, -1112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal17kPositiveSign_257EPNS0_8compiler18CodeAssemblerStateE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -1088(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1112(%rbp), %r8
	movq	-1088(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, -1088(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movq	-1104(%rbp), %xmm0
	movaps	%xmm1, -1040(%rbp)
	movhps	-1088(%rbp), %xmm0
	movq	$0, -1024(%rbp)
	movaps	%xmm0, -1104(%rbp)
	call	_Znwm@PLT
	movdqa	-1104(%rbp), %xmm0
	leaq	-432(%rbp), %rdi
	movq	%r12, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1040(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -1024(%rbp)
	movq	%rdx, -1032(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1040(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L110
	call	_ZdlPv@PLT
.L110:
	movq	-1064(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -560(%rbp)
	je	.L111
.L221:
	movq	-1080(%rbp), %rsi
	leaq	-624(%rbp), %r8
	movq	%r13, %rdi
	movq	%r8, -1104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1024(%rbp)
	movaps	%xmm0, -1040(%rbp)
	call	_Znwm@PLT
	movl	$1287, %edi
	movq	-1104(%rbp), %r8
	movq	%r12, %rsi
	movw	%di, (%rax)
	leaq	2(%rax), %rdx
	movq	%r8, %rdi
	movq	%rax, -1040(%rbp)
	movq	%rdx, -1024(%rbp)
	movq	%rdx, -1032(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1040(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L112
	movq	%rax, -1104(%rbp)
	call	_ZdlPv@PLT
	movq	-1104(%rbp), %rax
.L112:
	movq	(%rax), %rax
	movl	$57, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %r8
	movq	%rcx, -1104(%rbp)
	movq	%r8, -1112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -1088(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1112(%rbp), %r8
	movq	-1088(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, -1088(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1088(%rbp), %rdx
	movq	-1104(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15LoadBigIntDigitENS0_8compiler5TNodeINS0_6BigIntEEENS3_INS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -1112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal46FromConstexpr9ATuintptr17ATconstexpr_int31_162EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -1088(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1112(%rbp), %r8
	movq	-1088(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, -1088(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movq	-1104(%rbp), %xmm0
	movaps	%xmm1, -1040(%rbp)
	movhps	-1088(%rbp), %xmm0
	movq	$0, -1024(%rbp)
	movaps	%xmm0, -1104(%rbp)
	call	_Znwm@PLT
	movdqa	-1104(%rbp), %xmm0
	leaq	-432(%rbp), %rdi
	movq	%r12, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1040(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -1024(%rbp)
	movq	%rdx, -1032(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1040(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L113
	call	_ZdlPv@PLT
.L113:
	movq	-1064(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L222:
	movq	-1064(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-432(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1024(%rbp)
	movaps	%xmm0, -1040(%rbp)
	call	_Znwm@PLT
	movl	$1031, %esi
	movq	%r14, %rdi
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r12, %rsi
	movq	%rax, -1040(%rbp)
	movq	%rdx, -1024(%rbp)
	movq	%rdx, -1032(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1040(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L115
	call	_ZdlPv@PLT
.L115:
	movq	(%r14), %rax
	movl	$47, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	movq	(%rax), %r14
	movq	8(%rax), %rax
	movq	%rax, -1104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %xmm0
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movhps	-1104(%rbp), %xmm0
	movaps	%xmm1, -1040(%rbp)
	leaq	-240(%rbp), %r14
	movaps	%xmm0, -1104(%rbp)
	movq	$0, -1024(%rbp)
	call	_Znwm@PLT
	movdqa	-1104(%rbp), %xmm0
	movq	%r14, %rdi
	movq	%r12, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1040(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -1024(%rbp)
	movq	%rdx, -1032(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1040(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L116
	call	_ZdlPv@PLT
.L116:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L114
.L223:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22425:
	.size	_ZN2v88internal19IsCanonicalized_259EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE, .-_ZN2v88internal19IsCanonicalized_259EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE
	.section	.text._ZN2v88internal14InvertSign_260EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7Uint32TEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal14InvertSign_260EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7Uint32TEEE
	.type	_ZN2v88internal14InvertSign_260EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7Uint32TEEE, @function
_ZN2v88internal14InvertSign_260EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7Uint32TEEE:
.LFB22435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1336(%rbp), %r14
	leaq	-184(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1392(%rbp), %rbx
	subq	$1464, %rsp
	movq	%rdi, -1504(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1432(%rbp)
	movq	%rdi, -1392(%rbp)
	movl	$24, %edi
	movq	$0, -1384(%rbp)
	movq	$0, -1376(%rbp)
	movq	$0, -1368(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	$0, 16(%rax)
	movq	%rdx, -1368(%rbp)
	movq	%rdx, -1376(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1352(%rbp)
	movq	%rax, -1384(%rbp)
	movq	$0, -1360(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1432(%rbp), %rax
	movl	$24, %edi
	movq	$0, -1192(%rbp)
	movq	$0, -1184(%rbp)
	movq	%rax, -1200(%rbp)
	movq	$0, -1176(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 16(%rax)
	movq	%rax, -1192(%rbp)
	leaq	-1144(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1176(%rbp)
	movq	%rdx, -1184(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1160(%rbp)
	movq	%rax, -1464(%rbp)
	movq	$0, -1168(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1432(%rbp), %rax
	movl	$24, %edi
	movq	$0, -1000(%rbp)
	movq	$0, -992(%rbp)
	movq	%rax, -1008(%rbp)
	movq	$0, -984(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 16(%rax)
	movq	%rax, -1000(%rbp)
	leaq	-952(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -984(%rbp)
	movq	%rdx, -992(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -968(%rbp)
	movq	%rax, -1456(%rbp)
	movq	$0, -976(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1432(%rbp), %rax
	movl	$48, %edi
	movq	$0, -808(%rbp)
	movq	$0, -800(%rbp)
	movq	%rax, -816(%rbp)
	movq	$0, -792(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -808(%rbp)
	leaq	-760(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -792(%rbp)
	movq	%rdx, -800(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -776(%rbp)
	movq	%rax, -1472(%rbp)
	movq	$0, -784(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1432(%rbp), %rax
	movl	$48, %edi
	movq	$0, -616(%rbp)
	movq	$0, -608(%rbp)
	movq	%rax, -624(%rbp)
	movq	$0, -600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -616(%rbp)
	leaq	-568(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -600(%rbp)
	movq	%rdx, -608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -584(%rbp)
	movq	%rax, -1448(%rbp)
	movq	$0, -592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1432(%rbp), %rax
	movl	$48, %edi
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	%rax, -432(%rbp)
	movq	$0, -408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -424(%rbp)
	leaq	-376(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -408(%rbp)
	movq	%rdx, -416(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -392(%rbp)
	movq	%rax, -1480(%rbp)
	movq	$0, -400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1432(%rbp), %rax
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r15, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -1408(%rbp)
	movaps	%xmm0, -1424(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, (%rax)
	leaq	-1424(%rbp), %r13
	leaq	8(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -1424(%rbp)
	movq	%rdx, -1408(%rbp)
	movq	%rdx, -1416(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L225
	call	_ZdlPv@PLT
.L225:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1328(%rbp)
	jne	.L385
	cmpq	$0, -1136(%rbp)
	jne	.L386
.L230:
	cmpq	$0, -944(%rbp)
	jne	.L387
.L233:
	cmpq	$0, -752(%rbp)
	jne	.L388
.L236:
	cmpq	$0, -560(%rbp)
	jne	.L389
.L239:
	cmpq	$0, -368(%rbp)
	leaq	-240(%rbp), %rbx
	jne	.L390
.L242:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1408(%rbp)
	movaps	%xmm0, -1424(%rbp)
	call	_Znwm@PLT
	movl	$1028, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1424(%rbp)
	movq	%rdx, -1408(%rbp)
	movq	%rdx, -1416(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1424(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L245
	call	_ZdlPv@PLT
.L245:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	8(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L246
	call	_ZdlPv@PLT
.L246:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L247
	.p2align 4,,10
	.p2align 3
.L251:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L248
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L251
.L249:
	movq	-232(%rbp), %r15
.L247:
	testq	%r15, %r15
	je	.L252
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L252:
	movq	-1480(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L253
	call	_ZdlPv@PLT
.L253:
	movq	-416(%rbp), %rbx
	movq	-424(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L254
	.p2align 4,,10
	.p2align 3
.L258:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L255
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L258
.L256:
	movq	-424(%rbp), %r15
.L254:
	testq	%r15, %r15
	je	.L259
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L259:
	movq	-1448(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L260
	call	_ZdlPv@PLT
.L260:
	movq	-608(%rbp), %rbx
	movq	-616(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L261
	.p2align 4,,10
	.p2align 3
.L265:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L262
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L265
.L263:
	movq	-616(%rbp), %r15
.L261:
	testq	%r15, %r15
	je	.L266
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L266:
	movq	-1472(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L267
	call	_ZdlPv@PLT
.L267:
	movq	-800(%rbp), %rbx
	movq	-808(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L268
	.p2align 4,,10
	.p2align 3
.L272:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L269
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L272
.L270:
	movq	-808(%rbp), %r15
.L268:
	testq	%r15, %r15
	je	.L273
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L273:
	movq	-1456(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-976(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L274
	call	_ZdlPv@PLT
.L274:
	movq	-992(%rbp), %rbx
	movq	-1000(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L275
	.p2align 4,,10
	.p2align 3
.L279:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L276
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L279
.L277:
	movq	-1000(%rbp), %r15
.L275:
	testq	%r15, %r15
	je	.L280
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L280:
	movq	-1464(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L281
	call	_ZdlPv@PLT
.L281:
	movq	-1184(%rbp), %rbx
	movq	-1192(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L282
	.p2align 4,,10
	.p2align 3
.L286:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L283
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L286
.L284:
	movq	-1192(%rbp), %r15
.L282:
	testq	%r15, %r15
	je	.L287
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L287:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1360(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L288
	call	_ZdlPv@PLT
.L288:
	movq	-1376(%rbp), %rbx
	movq	-1384(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L289
	.p2align 4,,10
	.p2align 3
.L293:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L290
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L293
.L291:
	movq	-1384(%rbp), %r14
.L289:
	testq	%r14, %r14
	je	.L294
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L294:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L391
	addq	$1464, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L290:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L293
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L283:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L286
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L276:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L279
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L269:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L272
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L262:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L265
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L248:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L251
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L255:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L258
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L385:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -1408(%rbp)
	movaps	%xmm0, -1424(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movb	$4, (%rax)
	leaq	1(%rax), %rdx
	movq	%rax, -1424(%rbp)
	movq	%rdx, -1408(%rbp)
	movq	%rdx, -1416(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1424(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L227
	call	_ZdlPv@PLT
.L227:
	movq	(%rbx), %rax
	movl	$61, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1504(%rbp), %rdi
	call	_ZN2v88internal17kPositiveSign_257EPNS0_8compiler18CodeAssemblerStateE
	movq	-1504(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -1488(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1488(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, -1488(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -1408(%rbp)
	movaps	%xmm0, -1424(%rbp)
	call	_Znwm@PLT
	leaq	-1200(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -1424(%rbp)
	movq	%rdx, -1408(%rbp)
	movq	%rdx, -1416(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L228
	call	_ZdlPv@PLT
.L228:
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -1408(%rbp)
	movaps	%xmm0, -1424(%rbp)
	call	_Znwm@PLT
	leaq	-1008(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -1424(%rbp)
	movq	%rdx, -1408(%rbp)
	movq	%rdx, -1416(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L229
	call	_ZdlPv@PLT
.L229:
	movq	-1456(%rbp), %rcx
	movq	-1464(%rbp), %rdx
	movq	%r12, %rdi
	movq	-1488(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1136(%rbp)
	je	.L230
.L386:
	movq	-1464(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1200(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -1408(%rbp)
	movaps	%xmm0, -1424(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movb	$4, (%rax)
	leaq	1(%rax), %rdx
	movq	%rax, -1424(%rbp)
	movq	%rdx, -1408(%rbp)
	movq	%rdx, -1416(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1424(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L231
	call	_ZdlPv@PLT
.L231:
	movq	(%rbx), %rax
	movq	-1504(%rbp), %rdi
	movq	(%rax), %rcx
	movq	%rcx, -1488(%rbp)
	call	_ZN2v88internal17kNegativeSign_258EPNS0_8compiler18CodeAssemblerStateE
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	$0, -1408(%rbp)
	movq	%rax, %rbx
	movaps	%xmm0, -1424(%rbp)
	call	_Znwm@PLT
	movq	-1488(%rbp), %rcx
	leaq	-816(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rbx, 8(%rax)
	leaq	16(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%rax, -1424(%rbp)
	movq	%rdx, -1408(%rbp)
	movq	%rdx, -1416(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L232
	call	_ZdlPv@PLT
.L232:
	movq	-1472(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -944(%rbp)
	je	.L233
.L387:
	movq	-1456(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1008(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -1408(%rbp)
	movaps	%xmm0, -1424(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movb	$4, (%rax)
	leaq	1(%rax), %rdx
	movq	%rax, -1424(%rbp)
	movq	%rdx, -1408(%rbp)
	movq	%rdx, -1416(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1424(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L234
	call	_ZdlPv@PLT
.L234:
	movq	(%rbx), %rax
	movq	-1504(%rbp), %rdi
	movq	(%rax), %rcx
	movq	%rcx, -1488(%rbp)
	call	_ZN2v88internal17kPositiveSign_257EPNS0_8compiler18CodeAssemblerStateE
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	$0, -1408(%rbp)
	movq	%rax, %rbx
	movaps	%xmm0, -1424(%rbp)
	call	_Znwm@PLT
	movq	-1488(%rbp), %rcx
	leaq	-624(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rbx, 8(%rax)
	leaq	16(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%rax, -1424(%rbp)
	movq	%rdx, -1408(%rbp)
	movq	%rdx, -1416(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L235
	call	_ZdlPv@PLT
.L235:
	movq	-1448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -752(%rbp)
	je	.L236
.L388:
	movq	-1472(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-816(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1408(%rbp)
	movaps	%xmm0, -1424(%rbp)
	call	_Znwm@PLT
	movl	$1028, %r8d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1424(%rbp)
	movq	%rdx, -1408(%rbp)
	movq	%rdx, -1416(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1424(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L237
	call	_ZdlPv@PLT
.L237:
	movq	(%rbx), %rax
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movdqu	(%rax), %xmm0
	movaps	%xmm1, -1424(%rbp)
	movq	$0, -1408(%rbp)
	movaps	%xmm0, -1504(%rbp)
	call	_Znwm@PLT
	movdqa	-1504(%rbp), %xmm0
	leaq	-624(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1424(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -1408(%rbp)
	movq	%rdx, -1416(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L238
	call	_ZdlPv@PLT
.L238:
	movq	-1448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -560(%rbp)
	je	.L239
.L389:
	movq	-1448(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-624(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1408(%rbp)
	movaps	%xmm0, -1424(%rbp)
	call	_Znwm@PLT
	movl	$1028, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	2(%rax), %rdx
	movq	%rbx, %rdi
	movq	%rax, -1424(%rbp)
	movq	%rdx, -1408(%rbp)
	movq	%rdx, -1416(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1424(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L240
	call	_ZdlPv@PLT
.L240:
	movq	(%rbx), %rax
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movdqu	(%rax), %xmm0
	movaps	%xmm1, -1424(%rbp)
	movq	$0, -1408(%rbp)
	movaps	%xmm0, -1504(%rbp)
	call	_Znwm@PLT
	movdqa	-1504(%rbp), %xmm0
	leaq	-432(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1424(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -1408(%rbp)
	movq	%rdx, -1416(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L241
	call	_ZdlPv@PLT
.L241:
	movq	-1480(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L390:
	movq	-1480(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-432(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1408(%rbp)
	movaps	%xmm0, -1424(%rbp)
	call	_Znwm@PLT
	movl	$1028, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -1424(%rbp)
	movq	%rdx, -1408(%rbp)
	movq	%rdx, -1416(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1424(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L243
	call	_ZdlPv@PLT
.L243:
	movq	(%rbx), %rax
	movl	$60, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -1504(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movhps	-1504(%rbp), %xmm0
	movaps	%xmm1, -1424(%rbp)
	leaq	-240(%rbp), %rbx
	movaps	%xmm0, -1504(%rbp)
	movq	$0, -1408(%rbp)
	call	_Znwm@PLT
	movdqa	-1504(%rbp), %xmm0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1424(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -1408(%rbp)
	movq	%rdx, -1416(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L244
	call	_ZdlPv@PLT
.L244:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L242
.L391:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22435:
	.size	_ZN2v88internal14InvertSign_260EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7Uint32TEEE, .-_ZN2v88internal14InvertSign_260EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7Uint32TEEE
	.section	.text._ZN2v88internal30AllocateEmptyBigIntNoThrow_261EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEENS4_INS0_7IntPtrTEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30AllocateEmptyBigIntNoThrow_261EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEENS4_INS0_7IntPtrTEEEPNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal30AllocateEmptyBigIntNoThrow_261EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEENS4_INS0_7IntPtrTEEEPNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal30AllocateEmptyBigIntNoThrow_261EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEENS4_INS0_7IntPtrTEEEPNS1_18CodeAssemblerLabelE:
.LFB22442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-1248(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1288(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$1352, %rsp
	movq	%rsi, -1360(%rbp)
	movq	%r8, -1368(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1288(%rbp)
	movq	%rdi, -1248(%rbp)
	movl	$72, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -1304(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1288(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1328(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1288(%rbp), %rax
	movl	$72, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1320(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1288(%rbp), %rax
	movl	$96, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movups	%xmm0, (%rax)
	leaq	96(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1344(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1288(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	$0, -472(%rbp)
	movq	%rax, -480(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1336(%rbp)
	movq	$0, -464(%rbp)
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	$0, -440(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1288(%rbp), %rax
	movl	$96, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -280(%rbp)
	leaq	-232(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -1312(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1360(%rbp), %r9
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	%r13, -88(%rbp)
	leaq	-1280(%rbp), %r13
	movq	%r9, -96(%rbp)
	movaps	%xmm0, -1280(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -1264(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm1
	leaq	24(%rax), %rdx
	movq	%rax, -1280(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm1, (%rax)
	movq	%rdx, -1264(%rbp)
	movq	%rdx, -1272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L393
	call	_ZdlPv@PLT
.L393:
	movq	-1304(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1184(%rbp)
	jne	.L527
	cmpq	$0, -992(%rbp)
	jne	.L528
.L398:
	cmpq	$0, -800(%rbp)
	jne	.L529
.L401:
	cmpq	$0, -608(%rbp)
	leaq	-288(%rbp), %r14
	jne	.L530
	cmpq	$0, -416(%rbp)
	jne	.L531
.L407:
	movq	-1312(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1264(%rbp)
	movaps	%xmm0, -1280(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117769223, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1280(%rbp)
	movq	%rdx, -1264(%rbp)
	movq	%rdx, -1272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1280(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L409
	call	_ZdlPv@PLT
.L409:
	movq	(%rbx), %rax
	movq	-1312(%rbp), %rdi
	movq	24(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L410
	call	_ZdlPv@PLT
.L410:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L411
	.p2align 4,,10
	.p2align 3
.L415:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L412
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L415
.L413:
	movq	-280(%rbp), %r14
.L411:
	testq	%r14, %r14
	je	.L416
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L416:
	movq	-1336(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L417
	call	_ZdlPv@PLT
.L417:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L418
	.p2align 4,,10
	.p2align 3
.L422:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L419
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L422
.L420:
	movq	-472(%rbp), %r14
.L418:
	testq	%r14, %r14
	je	.L423
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L423:
	movq	-1344(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L424
	call	_ZdlPv@PLT
.L424:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L425
	.p2align 4,,10
	.p2align 3
.L429:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L426
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L429
.L427:
	movq	-664(%rbp), %r14
.L425:
	testq	%r14, %r14
	je	.L430
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L430:
	movq	-1320(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L431
	call	_ZdlPv@PLT
.L431:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L432
	.p2align 4,,10
	.p2align 3
.L436:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L433
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L436
.L434:
	movq	-856(%rbp), %r14
.L432:
	testq	%r14, %r14
	je	.L437
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L437:
	movq	-1328(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L438
	call	_ZdlPv@PLT
.L438:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L439
	.p2align 4,,10
	.p2align 3
.L443:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L440
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L443
.L441:
	movq	-1048(%rbp), %r14
.L439:
	testq	%r14, %r14
	je	.L444
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L444:
	movq	-1304(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L445
	call	_ZdlPv@PLT
.L445:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L446
	.p2align 4,,10
	.p2align 3
.L450:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L447
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L450
.L448:
	movq	-1240(%rbp), %r14
.L446:
	testq	%r14, %r14
	je	.L451
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L451:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L532
	addq	$1352, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L447:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L450
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L440:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L443
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L433:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L436
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L426:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L429
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L412:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L415
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L419:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L422
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L527:
	movq	-1304(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1264(%rbp)
	movaps	%xmm0, -1280(%rbp)
	call	_Znwm@PLT
	movl	$1031, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rdi
	movb	$5, 2(%rax)
	movq	%rax, -1280(%rbp)
	movq	%rdx, -1264(%rbp)
	movq	%rdx, -1272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1280(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L395
	call	_ZdlPv@PLT
.L395:
	movq	(%rbx), %rax
	movl	$66, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -1376(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -1360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16777216, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal46FromConstexpr8ATintptr18ATconstexpr_intptr_150EPNS0_8compiler18CodeAssemblerStateEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17IntPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	-1360(%rbp), %xmm2
	movaps	%xmm0, -1280(%rbp)
	movhps	-1376(%rbp), %xmm2
	movq	%rbx, -80(%rbp)
	movaps	%xmm2, -1360(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	$0, -1264(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm3
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-1056(%rbp), %rdi
	movq	%rax, -1280(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -1264(%rbp)
	movq	%rdx, -1272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L396
	call	_ZdlPv@PLT
.L396:
	movdqa	-1360(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	%rbx, -80(%rbp)
	movaps	%xmm0, -1280(%rbp)
	movaps	%xmm4, -96(%rbp)
	movq	$0, -1264(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -1280(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -1264(%rbp)
	movq	%rdx, -1272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L397
	call	_ZdlPv@PLT
.L397:
	movq	-1320(%rbp), %rcx
	movq	-1328(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -992(%rbp)
	je	.L398
.L528:
	movq	-1328(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1264(%rbp)
	movaps	%xmm0, -1280(%rbp)
	call	_Znwm@PLT
	movl	$1031, %esi
	movq	%r15, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$5, 2(%rax)
	movq	%rax, -1280(%rbp)
	movq	%rdx, -1264(%rbp)
	movq	%rdx, -1272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L399
	call	_ZdlPv@PLT
.L399:
	movl	$67, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm0, %xmm0
	leaq	-480(%rbp), %rdi
	movq	%r13, %rsi
	movq	$0, -1264(%rbp)
	movaps	%xmm0, -1280(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L400
	call	_ZdlPv@PLT
.L400:
	movq	-1336(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L401
.L529:
	movq	-1320(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1264(%rbp)
	movaps	%xmm0, -1280(%rbp)
	call	_Znwm@PLT
	movl	$1031, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$5, 2(%rax)
	movq	%rax, -1280(%rbp)
	movq	%rdx, -1264(%rbp)
	movq	%rdx, -1272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1280(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L402
	call	_ZdlPv@PLT
.L402:
	movq	(%rbx), %rax
	movl	$69, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	16(%rax), %r15
	movq	%rcx, -1376(%rbp)
	movq	8(%rax), %rcx
	movq	%rcx, -1360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler14AllocateBigIntENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$71, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1360(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9Word32AndENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -1384(%rbp)
	call	_ZN2v88internal17CodeStubAssembler21TruncateIntPtrToInt32ENS0_8compiler11SloppyTNodeINS0_7IntPtrTEEE@PLT
	movq	-1384(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32ShlENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler8Word32OrENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler19StoreBigIntBitfieldENS0_8compiler5TNodeINS0_6BigIntEEENS3_INS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$72, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm6
	movl	$32, %edi
	movq	-1376(%rbp), %xmm0
	movq	$0, -1264(%rbp)
	movhps	-1360(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%r15, %xmm0
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1280(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm7
	movdqa	-80(%rbp), %xmm3
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -1280(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -1264(%rbp)
	movq	%rdx, -1272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L403
	call	_ZdlPv@PLT
.L403:
	movq	-1344(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L530:
	movq	-1344(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1264(%rbp)
	movaps	%xmm0, -1280(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117769223, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1280(%rbp)
	movq	%rdx, -1264(%rbp)
	movq	%rdx, -1272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1280(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L405
	call	_ZdlPv@PLT
.L405:
	movq	(%rbx), %rax
	movl	$64, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r14
	movq	8(%rax), %r15
	movq	16(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rax, -1360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %xmm4
	movq	%r14, %xmm0
	movl	$32, %edi
	punpcklqdq	%xmm4, %xmm0
	leaq	-288(%rbp), %r14
	movq	$0, -1264(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	movhps	-1360(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1280(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm5
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-80(%rbp), %xmm6
	leaq	32(%rax), %rdx
	movq	%rax, -1280(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -1264(%rbp)
	movq	%rdx, -1272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L406
	call	_ZdlPv@PLT
.L406:
	movq	-1312(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -416(%rbp)
	je	.L407
.L531:
	movq	-1336(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	$0, -1264(%rbp)
	movaps	%xmm0, -1280(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L408
	call	_ZdlPv@PLT
.L408:
	movq	-1368(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L407
.L532:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22442:
	.size	_ZN2v88internal30AllocateEmptyBigIntNoThrow_261EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEENS4_INS0_7IntPtrTEEEPNS1_18CodeAssemblerLabelE, .-_ZN2v88internal30AllocateEmptyBigIntNoThrow_261EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEENS4_INS0_7IntPtrTEEEPNS1_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal23AllocateEmptyBigInt_262EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEENS4_INS0_7IntPtrTEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23AllocateEmptyBigInt_262EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEENS4_INS0_7IntPtrTEEE
	.type	_ZN2v88internal23AllocateEmptyBigInt_262EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEENS4_INS0_7IntPtrTEEE, @function
_ZN2v88internal23AllocateEmptyBigInt_262EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEENS4_INS0_7IntPtrTEEE:
.LFB22449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-248(%rbp), %r14
	leaq	-1264(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1480, %rsp
	movq	%rsi, -1488(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1432(%rbp)
	movq	%rdi, -1264(%rbp)
	movl	$72, %edi
	movq	$0, -1256(%rbp)
	movq	$0, -1248(%rbp)
	movq	$0, -1240(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1256(%rbp)
	leaq	-1208(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1240(%rbp)
	movq	%rdx, -1248(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1224(%rbp)
	movq	%rax, -1448(%rbp)
	movq	$0, -1232(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1432(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1064(%rbp)
	movq	$0, -1056(%rbp)
	movq	%rax, -1072(%rbp)
	movq	$0, -1048(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1064(%rbp)
	leaq	-1016(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1048(%rbp)
	movq	%rdx, -1056(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1032(%rbp)
	movq	%rax, -1480(%rbp)
	movq	$0, -1040(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1432(%rbp), %rax
	movl	$144, %edi
	movq	$0, -872(%rbp)
	movq	$0, -864(%rbp)
	movq	%rax, -880(%rbp)
	movq	$0, -856(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -872(%rbp)
	leaq	-824(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -856(%rbp)
	movq	%rdx, -864(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -840(%rbp)
	movq	%rax, -1472(%rbp)
	movq	$0, -848(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1432(%rbp), %rax
	movl	$72, %edi
	movq	$0, -680(%rbp)
	movq	$0, -672(%rbp)
	movq	%rax, -688(%rbp)
	movq	$0, -664(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -680(%rbp)
	leaq	-632(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -664(%rbp)
	movq	%rdx, -672(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -648(%rbp)
	movq	%rax, -1464(%rbp)
	movq	$0, -656(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1432(%rbp), %rax
	movl	$96, %edi
	movq	$0, -488(%rbp)
	movq	$0, -480(%rbp)
	movq	%rax, -496(%rbp)
	movq	$0, -472(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -488(%rbp)
	leaq	-440(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -472(%rbp)
	movq	%rdx, -480(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -456(%rbp)
	movq	%rax, -1456(%rbp)
	movq	$0, -464(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1432(%rbp), %rax
	movl	$96, %edi
	movq	$0, -296(%rbp)
	movq	$0, -288(%rbp)
	movq	%rax, -304(%rbp)
	movq	$0, -280(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -280(%rbp)
	movq	%rdx, -288(%rbp)
	xorl	%edx, %edx
	movq	%rax, -296(%rbp)
	movups	%xmm0, -264(%rbp)
	movq	$0, -272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1488(%rbp), %r10
	pxor	%xmm0, %xmm0
	movq	-1504(%rbp), %r9
	movl	$24, %edi
	movq	%r13, -96(%rbp)
	leaq	-1392(%rbp), %r13
	movq	%r10, -112(%rbp)
	movq	%r9, -104(%rbp)
	movaps	%xmm0, -1392(%rbp)
	movq	$0, -1376(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm1
	leaq	24(%rax), %rdx
	movq	%rax, -1392(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm1, (%rax)
	movq	%rdx, -1376(%rbp)
	movq	%rdx, -1384(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L534
	call	_ZdlPv@PLT
.L534:
	movq	-1448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1200(%rbp)
	jne	.L669
	cmpq	$0, -1008(%rbp)
	jne	.L670
.L540:
	cmpq	$0, -816(%rbp)
	jne	.L671
.L543:
	cmpq	$0, -624(%rbp)
	jne	.L672
.L546:
	cmpq	$0, -432(%rbp)
	leaq	-304(%rbp), %r15
	jne	.L673
.L548:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1376(%rbp)
	movaps	%xmm0, -1392(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117769223, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1392(%rbp)
	movq	%rdx, -1376(%rbp)
	movq	%rdx, -1384(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1392(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L551
	call	_ZdlPv@PLT
.L551:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	24(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L552
	call	_ZdlPv@PLT
.L552:
	movq	-288(%rbp), %rbx
	movq	-296(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L553
	.p2align 4,,10
	.p2align 3
.L557:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L554
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L557
.L555:
	movq	-296(%rbp), %r14
.L553:
	testq	%r14, %r14
	je	.L558
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L558:
	movq	-1456(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-464(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L559
	call	_ZdlPv@PLT
.L559:
	movq	-480(%rbp), %rbx
	movq	-488(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L560
	.p2align 4,,10
	.p2align 3
.L564:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L561
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L564
.L562:
	movq	-488(%rbp), %r14
.L560:
	testq	%r14, %r14
	je	.L565
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L565:
	movq	-1464(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L566
	call	_ZdlPv@PLT
.L566:
	movq	-672(%rbp), %rbx
	movq	-680(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L567
	.p2align 4,,10
	.p2align 3
.L571:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L568
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L571
.L569:
	movq	-680(%rbp), %r14
.L567:
	testq	%r14, %r14
	je	.L572
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L572:
	movq	-1472(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-848(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L573
	call	_ZdlPv@PLT
.L573:
	movq	-864(%rbp), %rbx
	movq	-872(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L574
	.p2align 4,,10
	.p2align 3
.L578:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L575
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L578
.L576:
	movq	-872(%rbp), %r14
.L574:
	testq	%r14, %r14
	je	.L579
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L579:
	movq	-1480(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1040(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L580
	call	_ZdlPv@PLT
.L580:
	movq	-1056(%rbp), %rbx
	movq	-1064(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L581
	.p2align 4,,10
	.p2align 3
.L585:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L582
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L585
.L583:
	movq	-1064(%rbp), %r14
.L581:
	testq	%r14, %r14
	je	.L586
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L586:
	movq	-1448(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L587
	call	_ZdlPv@PLT
.L587:
	movq	-1248(%rbp), %rbx
	movq	-1256(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L588
	.p2align 4,,10
	.p2align 3
.L592:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L589
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L592
.L590:
	movq	-1256(%rbp), %r14
.L588:
	testq	%r14, %r14
	je	.L593
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L593:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L674
	addq	$1480, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L589:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L592
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L582:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L585
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L575:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L578
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L568:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L571
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L554:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L557
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L561:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L564
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L669:
	movq	-1448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1376(%rbp)
	movaps	%xmm0, -1392(%rbp)
	call	_Znwm@PLT
	movl	$1031, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rdi
	movb	$5, 2(%rax)
	movq	%rax, -1392(%rbp)
	movq	%rdx, -1376(%rbp)
	movq	%rdx, -1384(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1392(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L536
	call	_ZdlPv@PLT
.L536:
	movq	(%r15), %rax
	movl	$78, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	%rcx, -1504(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -1488(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r13, %r8
	movq	%rbx, %rdi
	movq	-1504(%rbp), %rdx
	movq	-1488(%rbp), %rcx
	call	_ZN2v88internal30AllocateEmptyBigIntNoThrow_261EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEENS4_INS0_7IntPtrTEEEPNS1_18CodeAssemblerLabelE
	movq	-1488(%rbp), %rcx
	movq	%r15, %xmm3
	pxor	%xmm0, %xmm0
	movhps	-1504(%rbp), %xmm3
	movl	$48, %edi
	movq	%rax, -72(%rbp)
	leaq	-1424(%rbp), %r15
	movq	%rcx, %xmm2
	movq	%rcx, -80(%rbp)
	movhps	-1504(%rbp), %xmm2
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm2, -1520(%rbp)
	movaps	%xmm3, -1504(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -1424(%rbp)
	movq	$0, -1408(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm4
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm6
	leaq	48(%rax), %rdx
	leaq	-880(%rbp), %rdi
	movq	%rax, -1424(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -1408(%rbp)
	movq	%rdx, -1416(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L537
	call	_ZdlPv@PLT
.L537:
	movq	-1472(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1384(%rbp)
	jne	.L675
.L538:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1008(%rbp)
	je	.L540
.L670:
	movq	-1480(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1072(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1376(%rbp)
	movaps	%xmm0, -1392(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$67437575, (%rax)
	leaq	5(%rax), %rdx
	movb	$5, 4(%rax)
	movq	%rax, -1392(%rbp)
	movq	%rdx, -1376(%rbp)
	movq	%rdx, -1384(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1392(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L541
	call	_ZdlPv@PLT
.L541:
	movq	(%r15), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm7
	movaps	%xmm0, -1392(%rbp)
	movq	$0, -1376(%rbp)
	movq	%rdx, -96(%rbp)
	movaps	%xmm7, -112(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-112(%rbp), %xmm4
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-688(%rbp), %rdi
	movq	%rax, -1392(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -1376(%rbp)
	movq	%rdx, -1384(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L542
	call	_ZdlPv@PLT
.L542:
	movq	-1464(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -816(%rbp)
	je	.L543
.L671:
	movq	-1472(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-880(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -1376(%rbp)
	movaps	%xmm0, -1392(%rbp)
	call	_Znwm@PLT
	movl	$1797, %esi
	movq	%r15, %rdi
	movw	%si, 4(%rax)
	leaq	6(%rax), %rdx
	movq	%r13, %rsi
	movl	$67437575, (%rax)
	movq	%rax, -1392(%rbp)
	movq	%rdx, -1376(%rbp)
	movq	%rdx, -1384(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1392(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L544
	call	_ZdlPv@PLT
.L544:
	movq	(%r15), %rax
	movl	$32, %edi
	movdqu	32(%rax), %xmm5
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm6
	movq	$0, -1376(%rbp)
	shufpd	$2, %xmm5, %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm0, -1392(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm4
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-496(%rbp), %rdi
	movq	%rax, -1392(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -1376(%rbp)
	movq	%rdx, -1384(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L545
	call	_ZdlPv@PLT
.L545:
	movq	-1456(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -624(%rbp)
	je	.L546
.L672:
	movq	-1464(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-688(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1376(%rbp)
	movaps	%xmm0, -1392(%rbp)
	call	_Znwm@PLT
	movl	$1031, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$5, 2(%rax)
	movq	%rax, -1392(%rbp)
	movq	%rdx, -1376(%rbp)
	movq	%rdx, -1384(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1392(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L547
	call	_ZdlPv@PLT
.L547:
	movq	(%r15), %rax
	movl	$81, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$183, %edx
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubAssembler15ThrowRangeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L673:
	movq	-1456(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-496(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1376(%rbp)
	movaps	%xmm0, -1392(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117769223, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1392(%rbp)
	movq	%rdx, -1376(%rbp)
	movq	%rdx, -1384(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1392(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L549
	call	_ZdlPv@PLT
.L549:
	movq	(%rbx), %rax
	movl	$75, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	16(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rcx, -1504(%rbp)
	movq	%rax, -1488(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %xmm0
	movl	$32, %edi
	movq	$0, -1376(%rbp)
	movhps	-1504(%rbp), %xmm0
	leaq	-304(%rbp), %r15
	movaps	%xmm0, -112(%rbp)
	movq	%rbx, %xmm0
	movhps	-1488(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1392(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm5
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm6
	leaq	32(%rax), %rdx
	movq	%rax, -1392(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -1376(%rbp)
	movq	%rdx, -1384(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L550
	call	_ZdlPv@PLT
.L550:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L675:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-112(%rbp), %rsi
	movq	%r15, %rdi
	movdqa	-1504(%rbp), %xmm7
	movq	-1488(%rbp), %rax
	leaq	-72(%rbp), %rdx
	movaps	%xmm0, -1424(%rbp)
	movq	$0, -1408(%rbp)
	movaps	%xmm7, -112(%rbp)
	movdqa	-1520(%rbp), %xmm7
	movq	%rax, -80(%rbp)
	movaps	%xmm7, -96(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1072(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L539
	call	_ZdlPv@PLT
.L539:
	movq	-1480(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L538
.L674:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22449:
	.size	_ZN2v88internal23AllocateEmptyBigInt_262EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEENS4_INS0_7IntPtrTEEE, .-_ZN2v88internal23AllocateEmptyBigInt_262EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEENS4_INS0_7IntPtrTEEE
	.section	.text._ZN2v88internal32MutableBigIntAbsoluteCompare_263EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEES6_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32MutableBigIntAbsoluteCompare_263EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEES6_
	.type	_ZN2v88internal32MutableBigIntAbsoluteCompare_263EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEES6_, @function
_ZN2v88internal32MutableBigIntAbsoluteCompare_263EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEES6_:
.LFB22456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-616(%rbp), %r14
	leaq	-232(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-704(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-712(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-672(%rbp), %rbx
	subq	$728, %rsp
	movq	%rdi, -760(%rbp)
	movq	%rsi, -752(%rbp)
	movq	%rdx, -736(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -712(%rbp)
	movq	%rdi, -672(%rbp)
	movl	$48, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -664(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-712(%rbp), %rax
	movl	$72, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -728(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-712(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-752(%rbp), %xmm1
	movaps	%xmm0, -704(%rbp)
	movhps	-736(%rbp), %xmm1
	movq	$0, -688(%rbp)
	movaps	%xmm1, -752(%rbp)
	call	_Znwm@PLT
	movdqa	-752(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -704(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L677
	call	_ZdlPv@PLT
.L677:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -608(%rbp)
	jne	.L745
.L678:
	cmpq	$0, -416(%rbp)
	leaq	-288(%rbp), %rbx
	jne	.L746
.L681:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -688(%rbp)
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$4, 2(%rax)
	movq	%rax, -704(%rbp)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-704(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L684
	call	_ZdlPv@PLT
.L684:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L685
	call	_ZdlPv@PLT
.L685:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L686
	.p2align 4,,10
	.p2align 3
.L690:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L687
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L690
.L688:
	movq	-280(%rbp), %r15
.L686:
	testq	%r15, %r15
	je	.L691
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L691:
	movq	-728(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L692
	call	_ZdlPv@PLT
.L692:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L693
	.p2align 4,,10
	.p2align 3
.L697:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L694
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L697
.L695:
	movq	-472(%rbp), %r15
.L693:
	testq	%r15, %r15
	je	.L698
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L698:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L699
	call	_ZdlPv@PLT
.L699:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L700
	.p2align 4,,10
	.p2align 3
.L704:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L701
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L704
.L702:
	movq	-664(%rbp), %r14
.L700:
	testq	%r14, %r14
	je	.L705
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L705:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L747
	addq	$728, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L701:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L704
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L687:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L690
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L694:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L697
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L745:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -688(%rbp)
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r9d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -704(%rbp)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-704(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L679
	call	_ZdlPv@PLT
.L679:
	movq	(%rbx), %rax
	movl	$86, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -752(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-760(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	call	_ZN2v88internal17ExternalReference41mutable_big_int_absolute_compare_functionEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movl	$1800, %edi
	movl	$1800, %r8d
	leaq	-96(%rbp), %rcx
	movq	%rax, %rsi
	movq	-752(%rbp), %rax
	movl	$516, %edx
	movw	%di, -96(%rbp)
	movw	%r8w, -80(%rbp)
	movq	%r13, %rdi
	movl	$2, %r8d
	movq	%rbx, -88(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallCFunctionEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS5_S4_EE@PLT
	movq	%r13, %rdi
	movq	%rax, -736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	-736(%rbp), %rax
	movhps	-752(%rbp), %xmm0
	movq	$0, -688(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -704(%rbp)
	movq	%rax, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm2
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -704(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L680
	call	_ZdlPv@PLT
.L680:
	movq	-728(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L746:
	movq	-728(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -688(%rbp)
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$4, 2(%rax)
	movq	%rax, -704(%rbp)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-704(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L682
	call	_ZdlPv@PLT
.L682:
	movq	(%rbx), %rax
	movl	$85, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -736(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -752(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %edi
	movq	%rbx, -80(%rbp)
	movq	-752(%rbp), %xmm0
	movq	$0, -688(%rbp)
	leaq	-288(%rbp), %rbx
	movhps	-736(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm3
	leaq	24(%rax), %rdx
	movq	%rax, -704(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L683
	call	_ZdlPv@PLT
.L683:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L681
.L747:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22456:
	.size	_ZN2v88internal32MutableBigIntAbsoluteCompare_263EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEES6_, .-_ZN2v88internal32MutableBigIntAbsoluteCompare_263EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEES6_
	.section	.rodata._ZN2v88internal39Convert10BigIntBase13MutableBigInt_1455EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"../../deps/v8/../../deps/v8/src/builtins/base.tq"
	.section	.text._ZN2v88internal39Convert10BigIntBase13MutableBigInt_1455EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal39Convert10BigIntBase13MutableBigInt_1455EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE
	.type	_ZN2v88internal39Convert10BigIntBase13MutableBigInt_1455EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE, @function
_ZN2v88internal39Convert10BigIntBase13MutableBigInt_1455EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE:
.LFB22565:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-568(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-664(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-624(%rbp), %rbx
	subq	$664, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -664(%rbp)
	movq	%rdi, -624(%rbp)
	movl	$24, %edi
	movq	$0, -616(%rbp)
	movq	$0, -608(%rbp)
	movq	$0, -600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movq	$0, 16(%rax)
	movq	%rdx, -600(%rbp)
	movq	%rdx, -608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -584(%rbp)
	movq	%rax, -616(%rbp)
	movq	$0, -592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$48, %edi
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	%rax, -432(%rbp)
	movq	$0, -408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -424(%rbp)
	leaq	-376(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -408(%rbp)
	movq	%rdx, -416(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -392(%rbp)
	movq	%rax, -680(%rbp)
	movq	$0, -400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r15, (%rax)
	leaq	-656(%rbp), %r15
	leaq	8(%rax), %rdx
	movq	%r15, %rsi
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L749
	call	_ZdlPv@PLT
.L749:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -560(%rbp)
	jne	.L817
.L750:
	cmpq	$0, -368(%rbp)
	leaq	-240(%rbp), %rbx
	jne	.L818
.L753:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L756
	call	_ZdlPv@PLT
.L756:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	8(%rax), %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L757
	call	_ZdlPv@PLT
.L757:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L758
	.p2align 4,,10
	.p2align 3
.L762:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L759
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L762
.L760:
	movq	-232(%rbp), %r14
.L758:
	testq	%r14, %r14
	je	.L763
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L763:
	movq	-680(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L764
	call	_ZdlPv@PLT
.L764:
	movq	-416(%rbp), %rbx
	movq	-424(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L765
	.p2align 4,,10
	.p2align 3
.L769:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L766
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L769
.L767:
	movq	-424(%rbp), %r14
.L765:
	testq	%r14, %r14
	je	.L770
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L770:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L771
	call	_ZdlPv@PLT
.L771:
	movq	-608(%rbp), %rbx
	movq	-616(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L772
	.p2align 4,,10
	.p2align 3
.L776:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L773
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L776
.L774:
	movq	-616(%rbp), %r13
.L772:
	testq	%r13, %r13
	je	.L777
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L777:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L819
	addq	$664, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L773:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L776
	jmp	.L774
	.p2align 4,,10
	.p2align 3
.L759:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L762
	jmp	.L760
	.p2align 4,,10
	.p2align 3
.L766:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L769
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L817:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movb	$7, (%rax)
	leaq	1(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L751
	call	_ZdlPv@PLT
.L751:
	movq	(%rbx), %rax
	movl	$2650, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movq	$0, -640(%rbp)
	movq	%rbx, %xmm0
	movaps	%xmm1, -656(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movdqa	-704(%rbp), %xmm0
	leaq	-432(%rbp), %rdi
	movq	%r15, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -656(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L752
	call	_ZdlPv@PLT
.L752:
	movq	-680(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L750
	.p2align 4,,10
	.p2align 3
.L818:
	movq	-680(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-432(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r15, %rsi
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L754
	call	_ZdlPv@PLT
.L754:
	movq	(%rbx), %rax
	movl	$22, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movhps	-704(%rbp), %xmm0
	movaps	%xmm1, -656(%rbp)
	leaq	-240(%rbp), %rbx
	movaps	%xmm0, -704(%rbp)
	movq	$0, -640(%rbp)
	call	_Znwm@PLT
	movdqa	-704(%rbp), %xmm0
	movq	%rbx, %rdi
	movq	%r15, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -656(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L755
	call	_ZdlPv@PLT
.L755:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L753
.L819:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22565:
	.size	_ZN2v88internal39Convert10BigIntBase13MutableBigInt_1455EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE, .-_ZN2v88internal39Convert10BigIntBase13MutableBigInt_1455EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE
	.section	.text._ZN2v88internal35Convert8ATBigInt13MutableBigInt_256EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal35Convert8ATBigInt13MutableBigInt_256EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE
	.type	_ZN2v88internal35Convert8ATBigInt13MutableBigInt_256EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE, @function
_ZN2v88internal35Convert8ATBigInt13MutableBigInt_256EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE:
.LFB22410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-568(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-664(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$664, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -664(%rbp)
	movq	%rdi, -624(%rbp)
	movl	$24, %edi
	movq	$0, -616(%rbp)
	movq	$0, -608(%rbp)
	movq	$0, -600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movq	$0, 16(%rax)
	movq	%rax, -616(%rbp)
	leaq	-624(%rbp), %rax
	movq	%rdx, -600(%rbp)
	movq	%rdx, -608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -584(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$48, %edi
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	%rax, -432(%rbp)
	movq	$0, -408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-376(%rbp), %rcx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -408(%rbp)
	movq	%rdx, -416(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -680(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -392(%rbp)
	movq	%rax, -424(%rbp)
	movq	$0, -400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-704(%rbp), %rdi
	movq	%r15, (%rax)
	leaq	-656(%rbp), %r15
	leaq	8(%rax), %rdx
	movq	%r15, %rsi
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L821
	call	_ZdlPv@PLT
.L821:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -560(%rbp)
	jne	.L889
.L822:
	cmpq	$0, -368(%rbp)
	leaq	-240(%rbp), %rbx
	jne	.L890
.L825:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L828
	call	_ZdlPv@PLT
.L828:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	8(%rax), %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L829
	call	_ZdlPv@PLT
.L829:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L830
	.p2align 4,,10
	.p2align 3
.L834:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L831
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L834
.L832:
	movq	-232(%rbp), %r14
.L830:
	testq	%r14, %r14
	je	.L835
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L835:
	movq	-680(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L836
	call	_ZdlPv@PLT
.L836:
	movq	-416(%rbp), %rbx
	movq	-424(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L837
	.p2align 4,,10
	.p2align 3
.L841:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L838
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L841
.L839:
	movq	-424(%rbp), %r14
.L837:
	testq	%r14, %r14
	je	.L842
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L842:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L843
	call	_ZdlPv@PLT
.L843:
	movq	-608(%rbp), %rbx
	movq	-616(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L844
	.p2align 4,,10
	.p2align 3
.L848:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L845
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L848
.L846:
	movq	-616(%rbp), %r13
.L844:
	testq	%r13, %r13
	je	.L849
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L849:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L891
	addq	$664, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L845:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L848
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L831:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L834
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L838:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L841
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L889:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-704(%rbp), %rdi
	movq	%r15, %rsi
	movb	$7, (%rax)
	leaq	1(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L823
	movq	%rax, -704(%rbp)
	call	_ZdlPv@PLT
	movq	-704(%rbp), %rax
.L823:
	movq	(%rax), %rax
	movl	$22, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-704(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal39Convert10BigIntBase13MutableBigInt_1455EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	$0, -640(%rbp)
	movq	%rax, %rbx
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-704(%rbp), %rcx
	leaq	-432(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, 8(%rax)
	leaq	16(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L824
	call	_ZdlPv@PLT
.L824:
	movq	-680(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L822
	.p2align 4,,10
	.p2align 3
.L890:
	movq	-680(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-432(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r15, %rsi
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L826
	call	_ZdlPv@PLT
.L826:
	movq	(%rbx), %rax
	movl	$20, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movhps	-704(%rbp), %xmm0
	movaps	%xmm1, -656(%rbp)
	leaq	-240(%rbp), %rbx
	movaps	%xmm0, -704(%rbp)
	movq	$0, -640(%rbp)
	call	_Znwm@PLT
	movdqa	-704(%rbp), %xmm0
	movq	%rbx, %rdi
	movq	%r15, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -656(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L827
	call	_ZdlPv@PLT
.L827:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L825
.L891:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22410:
	.size	_ZN2v88internal35Convert8ATBigInt13MutableBigInt_256EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE, .-_ZN2v88internal35Convert8ATBigInt13MutableBigInt_256EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE
	.section	.text._ZN2v88internal18Cast8ATBigInt_1456EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18Cast8ATBigInt_1456EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal18Cast8ATBigInt_1456EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal18Cast8ATBigInt_1456EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE:
.LFB22566:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	leaq	-1632(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1760(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1800(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1880, %rsp
	movq	%rsi, -1888(%rbp)
	movq	%rdx, -1872(%rbp)
	movq	%rcx, -1896(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1800(%rbp)
	movq	%rdi, -1632(%rbp)
	movl	$48, %edi
	movq	$0, -1624(%rbp)
	movq	$0, -1616(%rbp)
	movq	$0, -1608(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1624(%rbp)
	leaq	-1576(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1608(%rbp)
	movq	%rdx, -1616(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1592(%rbp)
	movq	%rax, -1816(%rbp)
	movq	$0, -1600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1432(%rbp)
	movq	$0, -1424(%rbp)
	movq	%rax, -1440(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1432(%rbp)
	leaq	-1384(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -1840(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1864(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$96, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1848(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1832(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	$0, -472(%rbp)
	movq	%rax, -480(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1824(%rbp)
	movq	$0, -464(%rbp)
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	$0, -440(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1888(%rbp), %xmm1
	movaps	%xmm0, -1760(%rbp)
	movhps	-1872(%rbp), %xmm1
	movq	$0, -1744(%rbp)
	movaps	%xmm1, -1888(%rbp)
	call	_Znwm@PLT
	movdqa	-1888(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L893
	call	_ZdlPv@PLT
.L893:
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1568(%rbp)
	jne	.L1077
	cmpq	$0, -1376(%rbp)
	jne	.L1078
.L899:
	cmpq	$0, -1184(%rbp)
	jne	.L1079
.L902:
	cmpq	$0, -992(%rbp)
	jne	.L1080
.L907:
	cmpq	$0, -800(%rbp)
	jne	.L1081
.L910:
	cmpq	$0, -608(%rbp)
	leaq	-288(%rbp), %r15
	jne	.L1082
	cmpq	$0, -416(%rbp)
	jne	.L1083
.L916:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L918
	call	_ZdlPv@PLT
.L918:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L919
	call	_ZdlPv@PLT
.L919:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L920
	.p2align 4,,10
	.p2align 3
.L924:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L921
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L924
.L922:
	movq	-280(%rbp), %r14
.L920:
	testq	%r14, %r14
	je	.L925
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L925:
	movq	-1824(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L926
	call	_ZdlPv@PLT
.L926:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L927
	.p2align 4,,10
	.p2align 3
.L931:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L928
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L931
.L929:
	movq	-472(%rbp), %r14
.L927:
	testq	%r14, %r14
	je	.L932
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L932:
	movq	-1832(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L933
	call	_ZdlPv@PLT
.L933:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L934
	.p2align 4,,10
	.p2align 3
.L938:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L935
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L938
.L936:
	movq	-664(%rbp), %r14
.L934:
	testq	%r14, %r14
	je	.L939
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L939:
	movq	-1848(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L940
	call	_ZdlPv@PLT
.L940:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L941
	.p2align 4,,10
	.p2align 3
.L945:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L942
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L945
.L943:
	movq	-856(%rbp), %r14
.L941:
	testq	%r14, %r14
	je	.L946
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L946:
	movq	-1864(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L947
	call	_ZdlPv@PLT
.L947:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L948
	.p2align 4,,10
	.p2align 3
.L952:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L949
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L952
.L950:
	movq	-1048(%rbp), %r14
.L948:
	testq	%r14, %r14
	je	.L953
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L953:
	movq	-1840(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L954
	call	_ZdlPv@PLT
.L954:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L955
	.p2align 4,,10
	.p2align 3
.L959:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L956
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L959
.L957:
	movq	-1240(%rbp), %r14
.L955:
	testq	%r14, %r14
	je	.L960
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L960:
	movq	-1856(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L961
	call	_ZdlPv@PLT
.L961:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L962
	.p2align 4,,10
	.p2align 3
.L966:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L963
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L966
.L964:
	movq	-1432(%rbp), %r14
.L962:
	testq	%r14, %r14
	je	.L967
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L967:
	movq	-1816(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L968
	call	_ZdlPv@PLT
.L968:
	movq	-1616(%rbp), %rbx
	movq	-1624(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L969
	.p2align 4,,10
	.p2align 3
.L973:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L970
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L973
.L971:
	movq	-1624(%rbp), %r14
.L969:
	testq	%r14, %r14
	je	.L974
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L974:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1084
	addq	$1880, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L970:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L973
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L963:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L966
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L956:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L959
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L949:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L952
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L942:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L945
	jmp	.L943
	.p2align 4,,10
	.p2align 3
.L935:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L938
	jmp	.L936
	.p2align 4,,10
	.p2align 3
.L921:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L924
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L928:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L931
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L1077:
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r9d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L895
	call	_ZdlPv@PLT
.L895:
	movq	(%r15), %rax
	movl	$2026, %edx
	movq	%r12, %rdi
	leaq	.LC3(%rip), %rsi
	leaq	-1792(%rbp), %r15
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rcx, -1872(%rbp)
	movq	%rax, -1888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1888(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1888(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movq	%r15, %rdi
	movq	-1872(%rbp), %xmm2
	movq	$0, -1776(%rbp)
	movq	%rax, %xmm0
	movq	%rax, %xmm7
	leaq	-96(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	punpcklqdq	%xmm7, %xmm2
	movq	%rax, %rsi
	movq	%rax, -1872(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -1920(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -1792(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1248(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L896
	call	_ZdlPv@PLT
.L896:
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1752(%rbp)
	jne	.L1085
.L897:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1376(%rbp)
	je	.L899
.L1078:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1440(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r8d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L900
	call	_ZdlPv@PLT
.L900:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-480(%rbp), %rdi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L901
	call	_ZdlPv@PLT
.L901:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1184(%rbp)
	je	.L902
.L1079:
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1248(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L903
	call	_ZdlPv@PLT
.L903:
	movq	(%r15), %rax
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %r15
	movq	24(%rax), %rax
	movq	%rcx, -1872(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -1888(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1888(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal17Cast8ATBigInt_129EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %xmm4
	movq	-1888(%rbp), %rcx
	movq	-1872(%rbp), %xmm3
	movq	%rax, -72(%rbp)
	leaq	-96(%rbp), %rax
	pxor	%xmm0, %xmm0
	leaq	-64(%rbp), %rdx
	leaq	-1792(%rbp), %r15
	punpcklqdq	%xmm4, %xmm3
	movq	%rax, %rsi
	movq	%rcx, -80(%rbp)
	movq	%r15, %rdi
	movq	%rax, -1872(%rbp)
	movaps	%xmm3, -1920(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -1792(%rbp)
	movq	$0, -1776(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-864(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L904
	call	_ZdlPv@PLT
.L904:
	movq	-1848(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1752(%rbp)
	jne	.L1086
.L905:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -992(%rbp)
	je	.L907
.L1080:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rdi
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L908
	call	_ZdlPv@PLT
.L908:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-480(%rbp), %rdi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L909
	call	_ZdlPv@PLT
.L909:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L910
.L1081:
	movq	-1848(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L911
	call	_ZdlPv@PLT
.L911:
	movq	(%rbx), %rax
	leaq	-96(%rbp), %rsi
	leaq	-72(%rbp), %rdx
	movq	%r13, %rdi
	movdqu	(%rax), %xmm0
	movq	24(%rax), %rax
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-672(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L912
	call	_ZdlPv@PLT
.L912:
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L910
	.p2align 4,,10
	.p2align 3
.L1082:
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%r15, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L914
	call	_ZdlPv@PLT
.L914:
	movq	(%rbx), %rax
	movl	$177, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	16(%rax), %r15
	movq	%rcx, -1888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, -80(%rbp)
	leaq	-96(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rbx, %xmm0
	leaq	-72(%rbp), %rdx
	leaq	-288(%rbp), %r15
	movq	$0, -1744(%rbp)
	movhps	-1888(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1760(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L915
	call	_ZdlPv@PLT
.L915:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -416(%rbp)
	je	.L916
.L1083:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L917
	call	_ZdlPv@PLT
.L917:
	movq	-1896(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L1085:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-72(%rbp), %rdx
	movq	%r15, %rdi
	movdqa	-1920(%rbp), %xmm5
	movq	-1888(%rbp), %rax
	movaps	%xmm0, -1792(%rbp)
	movq	-1872(%rbp), %rsi
	movq	$0, -1776(%rbp)
	movq	%rax, -80(%rbp)
	movaps	%xmm5, -96(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1440(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L898
	call	_ZdlPv@PLT
.L898:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L897
	.p2align 4,,10
	.p2align 3
.L1086:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-72(%rbp), %rdx
	movq	%r15, %rdi
	movdqa	-1920(%rbp), %xmm6
	movq	-1888(%rbp), %rax
	movaps	%xmm0, -1792(%rbp)
	movq	-1872(%rbp), %rsi
	movq	$0, -1776(%rbp)
	movq	%rax, -80(%rbp)
	movaps	%xmm6, -96(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1056(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L906
	call	_ZdlPv@PLT
.L906:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L905
.L1084:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22566:
	.size	_ZN2v88internal18Cast8ATBigInt_1456EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, .-_ZN2v88internal18Cast8ATBigInt_1456EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SE_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SE_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SE_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SE_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SE_:
.LFB26913:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$7, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$1285, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$67569415, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$4, 6(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1088
	movq	%rax, -112(%rbp)
	call	_ZdlPv@PLT
	movq	-112(%rbp), %rax
.L1088:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1089
	movq	%rdx, (%r15)
.L1089:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1090
	movq	%rdx, (%r14)
.L1090:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1091
	movq	%rdx, 0(%r13)
.L1091:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1092
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1092:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1093
	movq	%rdx, (%rbx)
.L1093:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1094
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1094:
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L1087
	movq	-104(%rbp), %rbx
	movq	%rax, (%rbx)
.L1087:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1122
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1122:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26913:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SE_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SE_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_PNS7_IS5_EESB_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_PNS7_IS5_EESB_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_PNS7_IS5_EESB_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_PNS7_IS5_EESB_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_PNS7_IS5_EESB_:
.LFB26919:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$5, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$56, %rsp
	movq	%r8, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$67569415, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1124
	movq	%rax, -96(%rbp)
	call	_ZdlPv@PLT
	movq	-96(%rbp), %rax
.L1124:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1125
	movq	%rdx, (%r15)
.L1125:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1126
	movq	%rdx, (%r14)
.L1126:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1127
	movq	%rdx, 0(%r13)
.L1127:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1128
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1128:
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L1123
	movq	%rax, (%rbx)
.L1123:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1150
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1150:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26919:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_PNS7_IS5_EESB_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_PNS7_IS5_EESB_
	.section	.text._ZN2v88internal28MutableBigIntAbsoluteSub_264EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6BigIntEEES8_NS4_INS0_7Uint32TEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28MutableBigIntAbsoluteSub_264EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6BigIntEEES8_NS4_INS0_7Uint32TEEE
	.type	_ZN2v88internal28MutableBigIntAbsoluteSub_264EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6BigIntEEES8_NS4_INS0_7Uint32TEEE, @function
_ZN2v88internal28MutableBigIntAbsoluteSub_264EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6BigIntEEES8_NS4_INS0_7Uint32TEEE:
.LFB22460:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-2184(%rbp), %r14
	leaq	-1224(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2272(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-2368(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$2536, %rsp
	movq	%rsi, -2400(%rbp)
	movq	%rdx, -2384(%rbp)
	movq	%rcx, -2408(%rbp)
	movq	%r8, -2416(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -2368(%rbp)
	movq	%rdi, -2240(%rbp)
	movl	$96, %edi
	movq	$0, -2232(%rbp)
	movq	$0, -2224(%rbp)
	movq	$0, -2216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -2232(%rbp)
	leaq	-2240(%rbp), %rax
	movq	%rdx, -2216(%rbp)
	movq	%rdx, -2224(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2200(%rbp)
	movq	%rax, -2376(%rbp)
	movq	$0, -2208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2368(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movq	%rax, -2048(%rbp)
	movq	$0, -2024(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1992(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -2024(%rbp)
	movq	%rdx, -2032(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2432(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2008(%rbp)
	movq	%rax, -2040(%rbp)
	movq	$0, -2016(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2368(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1848(%rbp)
	movq	$0, -1840(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1832(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1800(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -1832(%rbp)
	movq	%rdx, -1840(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2424(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1816(%rbp)
	movq	%rax, -1848(%rbp)
	movq	$0, -1824(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2368(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1656(%rbp)
	movq	$0, -1648(%rbp)
	movq	%rax, -1664(%rbp)
	movq	$0, -1640(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1608(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -1640(%rbp)
	movq	%rdx, -1648(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2448(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1624(%rbp)
	movq	%rax, -1656(%rbp)
	movq	$0, -1632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2368(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1464(%rbp)
	movq	$0, -1456(%rbp)
	movq	%rax, -1472(%rbp)
	movq	$0, -1448(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1416(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -1448(%rbp)
	movq	%rdx, -1456(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2488(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1432(%rbp)
	movq	%rax, -1464(%rbp)
	movq	$0, -1440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2368(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1272(%rbp)
	movq	$0, -1264(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -1256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r15, %rdi
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -1256(%rbp)
	movq	%rdx, -1264(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1240(%rbp)
	movq	%rax, -1272(%rbp)
	movq	$0, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2368(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1080(%rbp)
	movq	$0, -1072(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$0, -1064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1032(%rbp), %rcx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -1064(%rbp)
	movq	%rdx, -1072(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2480(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1048(%rbp)
	movq	%rax, -1080(%rbp)
	movq	$0, -1056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2368(%rbp), %rax
	movl	$192, %edi
	movq	$0, -888(%rbp)
	movq	$0, -880(%rbp)
	movq	%rax, -896(%rbp)
	movq	$0, -872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-840(%rbp), %rcx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -872(%rbp)
	movq	%rdx, -880(%rbp)
	movq	%rcx, -2472(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movups	%xmm0, -856(%rbp)
	movq	%rax, -888(%rbp)
	movq	$0, -864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2368(%rbp), %rax
	movl	$168, %edi
	movq	$0, -696(%rbp)
	movq	$0, -688(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-648(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -680(%rbp)
	movq	%rdx, -688(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2496(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -664(%rbp)
	movq	%rax, -696(%rbp)
	movq	$0, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2368(%rbp), %rax
	movl	$120, %edi
	movq	$0, -504(%rbp)
	movq	$0, -496(%rbp)
	movq	%rax, -512(%rbp)
	movq	$0, -488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-456(%rbp), %rcx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -488(%rbp)
	movq	%rdx, -496(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2464(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -472(%rbp)
	movq	%rax, -504(%rbp)
	movq	$0, -480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2368(%rbp), %rax
	movl	$120, %edi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-264(%rbp), %rcx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2456(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -312(%rbp)
	movups	%xmm0, -280(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-2408(%rbp), %xmm1
	movq	-2400(%rbp), %xmm2
	movaps	%xmm0, -2272(%rbp)
	movhps	-2416(%rbp), %xmm1
	movq	$0, -2256(%rbp)
	movhps	-2384(%rbp), %xmm2
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -128(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm5
	movq	%r13, %rsi
	movq	-2376(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -2272(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -2256(%rbp)
	movq	%rdx, -2264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1152
	call	_ZdlPv@PLT
.L1152:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2048(%rbp), %rax
	cmpq	$0, -2176(%rbp)
	movq	%rax, -2408(%rbp)
	leaq	-1856(%rbp), %rax
	movq	%rax, -2384(%rbp)
	jne	.L1243
.L1153:
	leaq	-512(%rbp), %rax
	cmpq	$0, -1984(%rbp)
	movq	%rax, -2400(%rbp)
	jne	.L1244
.L1157:
	leaq	-1664(%rbp), %rax
	cmpq	$0, -1792(%rbp)
	movq	%rax, -2432(%rbp)
	leaq	-704(%rbp), %rax
	movq	%rax, -2440(%rbp)
	jne	.L1245
.L1159:
	leaq	-1472(%rbp), %rax
	cmpq	$0, -1600(%rbp)
	movq	%rax, -2416(%rbp)
	leaq	-1280(%rbp), %rax
	movq	%rax, -2424(%rbp)
	jne	.L1246
.L1162:
	leaq	-1088(%rbp), %rax
	cmpq	$0, -1408(%rbp)
	movq	%rax, -2448(%rbp)
	jne	.L1247
.L1165:
	cmpq	$0, -1216(%rbp)
	leaq	-896(%rbp), %r14
	jne	.L1248
.L1167:
	cmpq	$0, -1024(%rbp)
	jne	.L1249
.L1169:
	cmpq	$0, -832(%rbp)
	jne	.L1250
	cmpq	$0, -640(%rbp)
	jne	.L1251
.L1175:
	cmpq	$0, -448(%rbp)
	leaq	-320(%rbp), %r15
	jne	.L1252
.L1177:
	movq	-2456(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2256(%rbp)
	movaps	%xmm0, -2272(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	$67569415, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2272(%rbp)
	movq	%rdx, -2256(%rbp)
	movq	%rdx, -2264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2272(%rbp), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L1179
	call	_ZdlPv@PLT
.L1179:
	movq	0(%r13), %rax
	movq	%r15, %rdi
	movq	32(%rax), %r13
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2400(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2440(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2448(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2424(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2416(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2432(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2384(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2408(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2376(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1253
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1243:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2256(%rbp)
	movaps	%xmm0, -2272(%rbp)
	call	_Znwm@PLT
	movq	-2376(%rbp), %rdi
	movq	%r13, %rsi
	movl	$67569415, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2272(%rbp)
	movq	%rdx, -2256(%rbp)
	movq	%rdx, -2264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2272(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L1154
	call	_ZdlPv@PLT
.L1154:
	movq	(%r14), %rax
	movl	$91, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %r14
	movq	%rcx, -2440(%rbp)
	movq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, -2400(%rbp)
	movq	%rax, -2512(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadBigIntBitfieldENS0_8compiler5TNodeINS0_6BigIntEEE@PLT
	movl	$2147483646, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12DecodeWord32ENS0_8compiler11SloppyTNodeINS0_7Word32TEEEjj@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -2408(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$92, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2400(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadBigIntBitfieldENS0_8compiler5TNodeINS0_6BigIntEEE@PLT
	movl	$2147483646, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12DecodeWord32ENS0_8compiler11SloppyTNodeINS0_7Word32TEEEjj@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -2528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$93, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadBigIntBitfieldENS0_8compiler5TNodeINS0_6BigIntEEE@PLT
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12DecodeWord32ENS0_8compiler11SloppyTNodeINS0_7Word32TEEEjj@PLT
	movq	%r13, %rdi
	movq	%rax, -2384(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$96, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, -2416(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2416(%rbp), %rdx
	movq	-2408(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, -2416(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-72(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	-2400(%rbp), %xmm7
	movq	%r14, %xmm4
	leaq	-128(%rbp), %r14
	movq	-2440(%rbp), %xmm3
	movq	-2408(%rbp), %xmm6
	movq	-2384(%rbp), %rax
	movq	%r14, %rsi
	movq	%rdx, -2440(%rbp)
	movhps	-2512(%rbp), %xmm7
	punpcklqdq	%xmm4, %xmm3
	movaps	%xmm0, -2272(%rbp)
	movhps	-2528(%rbp), %xmm6
	movq	%rax, -80(%rbp)
	movaps	%xmm6, -2528(%rbp)
	movaps	%xmm7, -2512(%rbp)
	movaps	%xmm3, -2400(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movq	$0, -2256(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2048(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -2408(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2272(%rbp), %rdi
	movq	-2440(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L1155
	call	_ZdlPv@PLT
	movq	-2440(%rbp), %rdx
.L1155:
	movdqa	-2400(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	%r13, %rdi
	movdqa	-2512(%rbp), %xmm4
	movq	-2384(%rbp), %rax
	movaps	%xmm0, -2272(%rbp)
	movaps	%xmm5, -128(%rbp)
	movdqa	-2528(%rbp), %xmm5
	movq	%rax, -80(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movq	$0, -2256(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1856(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -2384(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1156
	call	_ZdlPv@PLT
.L1156:
	movq	-2424(%rbp), %rcx
	movq	-2432(%rbp), %rdx
	movq	%r12, %rdi
	movq	-2416(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1153
	.p2align 4,,10
	.p2align 3
.L1244:
	movq	-2432(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -2344(%rbp)
	movq	$0, -2336(%rbp)
	movq	$0, -2328(%rbp)
	movq	$0, -2320(%rbp)
	movq	$0, -2312(%rbp)
	movq	$0, -2304(%rbp)
	movq	$0, -2288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2288(%rbp), %rax
	movq	-2408(%rbp), %rdi
	leaq	-2328(%rbp), %rcx
	pushq	%rax
	leaq	-2304(%rbp), %rax
	leaq	-2312(%rbp), %r9
	pushq	%rax
	leaq	-2320(%rbp), %r8
	leaq	-2336(%rbp), %rdx
	leaq	-2344(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SE_
	leaq	.LC2(%rip), %rsi
	movl	$98, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2344(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	-2336(%rbp), %rax
	movl	$40, %edi
	movaps	%xmm0, -2272(%rbp)
	movq	%rdx, -128(%rbp)
	movq	-2328(%rbp), %rdx
	movq	%rax, -120(%rbp)
	movq	%rdx, -112(%rbp)
	movq	-2320(%rbp), %rdx
	movq	%rax, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	$0, -2256(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	movq	-2400(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -2272(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2256(%rbp)
	movq	%rdx, -2264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2272(%rbp), %rdi
	popq	%rcx
	popq	%rsi
	testq	%rdi, %rdi
	je	.L1158
	call	_ZdlPv@PLT
.L1158:
	movq	-2464(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1157
	.p2align 4,,10
	.p2align 3
.L1245:
	movq	-2424(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -2344(%rbp)
	movq	$0, -2336(%rbp)
	movq	$0, -2328(%rbp)
	movq	$0, -2320(%rbp)
	movq	$0, -2312(%rbp)
	movq	$0, -2304(%rbp)
	movq	$0, -2288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2288(%rbp), %rax
	movq	-2384(%rbp), %rdi
	leaq	-2312(%rbp), %r9
	pushq	%rax
	leaq	-2304(%rbp), %rax
	leaq	-2320(%rbp), %r8
	pushq	%rax
	leaq	-2328(%rbp), %rcx
	leaq	-2336(%rbp), %rdx
	leaq	-2344(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SE_
	movl	$101, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2304(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	leaq	-128(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, -2416(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-2328(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movq	-2320(%rbp), %rdx
	movq	-2312(%rbp), %rdi
	movq	-2344(%rbp), %rax
	movaps	%xmm0, -2272(%rbp)
	movq	-2336(%rbp), %rcx
	movq	-2304(%rbp), %r11
	movq	%rsi, -2512(%rbp)
	movq	-2288(%rbp), %r10
	movq	%rdx, -2552(%rbp)
	movq	%rdi, -2544(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%r14, %rsi
	movq	%rdx, -104(%rbp)
	leaq	-72(%rbp), %rdx
	movq	%rdi, -96(%rbp)
	movq	%r13, %rdi
	movq	%rax, -2440(%rbp)
	movq	%rax, -128(%rbp)
	movq	%rdx, -2560(%rbp)
	movq	%rcx, -2528(%rbp)
	movq	%r11, -2424(%rbp)
	movq	%r10, -2536(%rbp)
	movq	%rcx, -120(%rbp)
	movq	%r11, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	$0, -2256(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2432(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2272(%rbp), %rdi
	popq	%rax
	popq	%rdx
	movq	-2560(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L1160
	call	_ZdlPv@PLT
	movq	-2560(%rbp), %rdx
.L1160:
	movq	-2440(%rbp), %xmm0
	movq	-2536(%rbp), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	$0, -2256(%rbp)
	movhps	-2528(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	-2512(%rbp), %xmm0
	movhps	-2552(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-2544(%rbp), %xmm0
	movhps	-2424(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2272(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-704(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -2440(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1161
	call	_ZdlPv@PLT
.L1161:
	movq	-2496(%rbp), %rcx
	movq	-2448(%rbp), %rdx
	movq	%r12, %rdi
	movq	-2416(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1159
	.p2align 4,,10
	.p2align 3
.L1247:
	movq	-2488(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -2344(%rbp)
	movq	$0, -2336(%rbp)
	movq	$0, -2328(%rbp)
	movq	$0, -2320(%rbp)
	movq	$0, -2312(%rbp)
	movq	$0, -2304(%rbp)
	movq	$0, -2288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2288(%rbp), %rax
	movq	-2416(%rbp), %rdi
	leaq	-2312(%rbp), %r9
	pushq	%rax
	leaq	-2304(%rbp), %rax
	leaq	-2328(%rbp), %rcx
	pushq	%rax
	leaq	-2320(%rbp), %r8
	leaq	-2336(%rbp), %rdx
	leaq	-2344(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SE_
	leaq	-128(%rbp), %rsi
	leaq	-64(%rbp), %rdx
	movq	%r13, %rdi
	movq	-2288(%rbp), %xmm0
	movq	-2312(%rbp), %xmm1
	movq	$0, -2256(%rbp)
	movq	-2328(%rbp), %xmm2
	movq	-2344(%rbp), %xmm3
	movhps	-2336(%rbp), %xmm0
	movhps	-2304(%rbp), %xmm1
	movhps	-2320(%rbp), %xmm2
	movhps	-2336(%rbp), %xmm3
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -2272(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2448(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2272(%rbp), %rdi
	popq	%r9
	popq	%r10
	testq	%rdi, %rdi
	je	.L1166
	call	_ZdlPv@PLT
.L1166:
	movq	-2480(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1165
	.p2align 4,,10
	.p2align 3
.L1246:
	movq	-2448(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -2344(%rbp)
	leaq	-128(%rbp), %r14
	movq	$0, -2336(%rbp)
	movq	$0, -2328(%rbp)
	movq	$0, -2320(%rbp)
	movq	$0, -2312(%rbp)
	movq	$0, -2304(%rbp)
	movq	$0, -2288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2288(%rbp), %rax
	movq	-2432(%rbp), %rdi
	leaq	-2320(%rbp), %r8
	pushq	%rax
	leaq	-2304(%rbp), %rax
	leaq	-2328(%rbp), %rcx
	pushq	%rax
	leaq	-2312(%rbp), %r9
	leaq	-2336(%rbp), %rdx
	leaq	-2344(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SE_
	movl	$102, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2288(%rbp), %rdx
	movq	-2320(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, -2448(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-2328(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movq	-2320(%rbp), %rdx
	movq	-2312(%rbp), %rdi
	movq	-2344(%rbp), %rax
	movaps	%xmm0, -2272(%rbp)
	movq	-2304(%rbp), %r11
	movq	-2336(%rbp), %rcx
	movq	%rsi, -2528(%rbp)
	movq	-2288(%rbp), %r9
	movq	%rdx, -2512(%rbp)
	movq	%rdi, -2552(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%r14, %rsi
	movq	%rdx, -104(%rbp)
	leaq	-72(%rbp), %rdx
	movq	%rdi, -96(%rbp)
	movq	%r13, %rdi
	movq	%rax, -2560(%rbp)
	movq	%r11, -2536(%rbp)
	movq	%rax, -128(%rbp)
	movq	%r11, -88(%rbp)
	movq	%rdx, -2568(%rbp)
	movq	%rcx, -2424(%rbp)
	movq	%r9, -2544(%rbp)
	movq	%rcx, -120(%rbp)
	movq	%r9, -80(%rbp)
	movq	$0, -2256(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2416(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2272(%rbp), %rdi
	popq	%r11
	movq	-2568(%rbp), %rdx
	popq	%rax
	testq	%rdi, %rdi
	je	.L1163
	call	_ZdlPv@PLT
	movq	-2568(%rbp), %rdx
.L1163:
	movq	-2560(%rbp), %xmm0
	movq	-2544(%rbp), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	$0, -2256(%rbp)
	movhps	-2424(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	-2528(%rbp), %xmm0
	movhps	-2512(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-2552(%rbp), %xmm0
	movhps	-2536(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2272(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1280(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -2424(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1164
	call	_ZdlPv@PLT
.L1164:
	movq	-2488(%rbp), %rdx
	movq	-2448(%rbp), %rsi
	movq	%r15, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1162
	.p2align 4,,10
	.p2align 3
.L1250:
	movq	-2472(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2256(%rbp)
	movaps	%xmm0, -2272(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$505534577272882951, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2272(%rbp)
	movq	%rdx, -2256(%rbp)
	movq	%rdx, -2264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2272(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L1173
	call	_ZdlPv@PLT
.L1173:
	movq	(%r15), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	56(%rax), %rdx
	movdqu	(%rax), %xmm5
	movq	$0, -2256(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm0, -2272(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	movq	-2400(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -2272(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2256(%rbp)
	movq	%rdx, -2264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1174
	call	_ZdlPv@PLT
.L1174:
	movq	-2464(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -640(%rbp)
	je	.L1175
.L1251:
	movq	-2496(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -2344(%rbp)
	movq	$0, -2336(%rbp)
	movq	$0, -2328(%rbp)
	movq	$0, -2320(%rbp)
	movq	$0, -2312(%rbp)
	movq	$0, -2304(%rbp)
	movq	$0, -2288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2288(%rbp), %rax
	movq	-2440(%rbp), %rdi
	leaq	-2312(%rbp), %r9
	pushq	%rax
	leaq	-2304(%rbp), %rax
	leaq	-2320(%rbp), %r8
	pushq	%rax
	leaq	-2328(%rbp), %rcx
	leaq	-2336(%rbp), %rdx
	leaq	-2344(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SE_
	movl	$105, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2312(%rbp), %rcx
	movq	-2320(%rbp), %rdx
	movq	%rbx, %rdi
	movq	-2344(%rbp), %rsi
	call	_ZN2v88internal23AllocateEmptyBigInt_262EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEENS4_INS0_7IntPtrTEEE
	movl	$106, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2336(%rbp), %rcx
	movq	-2328(%rbp), %rdx
	movq	%rcx, -2480(%rbp)
	movq	%rdx, -2472(%rbp)
	call	_ZN2v88internal17ExternalReference54mutable_big_int_absolute_sub_and_canonicalize_functionEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	-2480(%rbp), %rcx
	movl	$1800, %edx
	movq	%r13, %rdi
	movw	%dx, -112(%rbp)
	movq	-2472(%rbp), %rdx
	movl	$3, %r8d
	movq	%rax, %rsi
	movq	%rcx, -104(%rbp)
	movl	$1800, %ecx
	movl	$1800, %eax
	movw	%cx, -96(%rbp)
	leaq	-128(%rbp), %rcx
	movq	%rdx, -88(%rbp)
	movl	$1800, %edx
	movw	%ax, -128(%rbp)
	movq	%r15, -120(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallCFunctionEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS5_S4_EE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$107, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal35Convert8ATBigInt13MutableBigInt_256EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE
	movq	-2344(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movaps	%xmm0, -2272(%rbp)
	movq	%rdx, -128(%rbp)
	movq	-2336(%rbp), %rdx
	movq	%rax, -96(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-2328(%rbp), %rdx
	movq	$0, -2256(%rbp)
	movq	%rdx, -112(%rbp)
	movq	-2320(%rbp), %rdx
	movq	%rdx, -104(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm5
	movq	-2400(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -2272(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -2256(%rbp)
	movq	%rdx, -2264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2272(%rbp), %rdi
	popq	%rsi
	popq	%r8
	testq	%rdi, %rdi
	je	.L1176
	call	_ZdlPv@PLT
.L1176:
	movq	-2464(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1175
	.p2align 4,,10
	.p2align 3
.L1249:
	movq	-2480(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2256(%rbp)
	movaps	%xmm0, -2272(%rbp)
	call	_Znwm@PLT
	movq	-2448(%rbp), %rdi
	movq	%r13, %rsi
	movabsq	$505534577272882951, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2272(%rbp)
	movq	%rdx, -2256(%rbp)
	movq	%rdx, -2264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2272(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L1170
	call	_ZdlPv@PLT
.L1170:
	movq	(%r15), %rax
	leaq	-128(%rbp), %rsi
	leaq	-64(%rbp), %rdx
	movq	%r13, %rdi
	movdqu	32(%rax), %xmm0
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm1
	movdqu	48(%rax), %xmm4
	movq	$0, -2256(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -2272(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1171
	call	_ZdlPv@PLT
.L1171:
	movq	-2472(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1248:
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	$0, -2360(%rbp)
	leaq	-2304(%rbp), %r15
	movq	$0, -2352(%rbp)
	movq	$0, -2344(%rbp)
	movq	$0, -2336(%rbp)
	movq	$0, -2328(%rbp)
	movq	$0, -2320(%rbp)
	movq	$0, -2312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2312(%rbp), %rax
	movq	-2424(%rbp), %rdi
	leaq	-2336(%rbp), %r8
	pushq	%rax
	leaq	-2320(%rbp), %rax
	leaq	-2352(%rbp), %rdx
	pushq	%rax
	leaq	-2344(%rbp), %rcx
	leaq	-2328(%rbp), %r9
	leaq	-2360(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SE_
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2360(%rbp), %r9
	movq	-2352(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r9, -2528(%rbp)
	movq	%rcx, -2488(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$790, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-2272(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-128(%rbp), %r10
	movl	$1, %edi
	xorl	%esi, %esi
	movq	%rax, %r8
	pushq	%rdi
	movq	-2488(%rbp), %rcx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%r10
	movq	-2528(%rbp), %r9
	movq	%r15, %rdi
	leaq	-2288(%rbp), %rdx
	movq	%rax, -2288(%rbp)
	movq	-2256(%rbp), %rax
	movq	%rcx, -128(%rbp)
	movl	$1, %ecx
	movq	%r10, -2512(%rbp)
	movq	%rax, -2280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r15, %rdi
	movq	%rax, -2488(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-2312(%rbp), %rdx
	movq	-2512(%rbp), %r10
	movq	%r13, %rdi
	movq	-2328(%rbp), %xmm0
	movq	-2488(%rbp), %rax
	movq	$0, -2256(%rbp)
	movq	-2344(%rbp), %xmm1
	movq	%r10, %rsi
	movq	%rdx, -80(%rbp)
	leaq	-64(%rbp), %rdx
	movq	-2360(%rbp), %xmm2
	movhps	-2320(%rbp), %xmm0
	movq	%rax, -72(%rbp)
	movhps	-2336(%rbp), %xmm1
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-2352(%rbp), %xmm2
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm0, -2272(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1168
	call	_ZdlPv@PLT
.L1168:
	movq	-2472(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1167
	.p2align 4,,10
	.p2align 3
.L1252:
	movq	-2464(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -2328(%rbp)
	movq	$0, -2320(%rbp)
	movq	$0, -2312(%rbp)
	movq	$0, -2304(%rbp)
	movq	$0, -2288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2400(%rbp), %rdi
	leaq	-2312(%rbp), %rcx
	leaq	-2288(%rbp), %r9
	leaq	-2304(%rbp), %r8
	leaq	-2320(%rbp), %rdx
	leaq	-2328(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_PNS7_IS5_EESB_
	movl	$89, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2328(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movaps	%xmm0, -2272(%rbp)
	movq	%rax, -128(%rbp)
	movq	-2320(%rbp), %rax
	movq	$0, -2256(%rbp)
	movq	%rax, -120(%rbp)
	movq	-2312(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-2304(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-2288(%rbp), %rax
	movq	%rax, -96(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	leaq	40(%rax), %rdx
	movq	%rax, -2272(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2256(%rbp)
	movq	%rdx, -2264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1178
	call	_ZdlPv@PLT
.L1178:
	movq	-2456(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1177
.L1253:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22460:
	.size	_ZN2v88internal28MutableBigIntAbsoluteSub_264EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6BigIntEEES8_NS4_INS0_7Uint32TEEE, .-_ZN2v88internal28MutableBigIntAbsoluteSub_264EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6BigIntEEES8_NS4_INS0_7Uint32TEEE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SC_SC_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SC_SC_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SC_SC_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SC_SC_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SC_SC_:
.LFB26921:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$8, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$506379002203014919, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1255
	movq	%rax, -120(%rbp)
	call	_ZdlPv@PLT
	movq	-120(%rbp), %rax
.L1255:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1256
	movq	%rdx, (%r15)
.L1256:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1257
	movq	%rdx, (%r14)
.L1257:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1258
	movq	%rdx, 0(%r13)
.L1258:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1259
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1259:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1260
	movq	%rdx, (%rbx)
.L1260:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1261
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1261:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1262
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1262:
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L1254
	movq	-112(%rbp), %rbx
	movq	%rax, (%rbx)
.L1254:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1293
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1293:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26921:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SC_SC_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SC_SC_
	.section	.text._ZN2v88internal28MutableBigIntAbsoluteAdd_265EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6BigIntEEES8_NS4_INS0_7Uint32TEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28MutableBigIntAbsoluteAdd_265EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6BigIntEEES8_NS4_INS0_7Uint32TEEEPNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal28MutableBigIntAbsoluteAdd_265EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6BigIntEEES8_NS4_INS0_7Uint32TEEEPNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal28MutableBigIntAbsoluteAdd_265EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6BigIntEEES8_NS4_INS0_7Uint32TEEEPNS1_18CodeAssemblerLabelE:
.LFB22473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-3344(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-3464(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-3216(%rbp), %rbx
	subq	$3672, %rsp
	movq	%rcx, -3648(%rbp)
	movq	%r8, -3664(%rbp)
	movq	%r9, -3616(%rbp)
	movq	%rsi, -3480(%rbp)
	movq	%r12, %rsi
	movq	%rdx, -3632(%rbp)
	movl	$4, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -3464(%rbp)
	movq	%rbx, %rdi
	movq	%rbx, -3608(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3024(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2832(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3504(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2640(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3536(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2448(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3568(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2256(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2064(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1872(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3576(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1680(%rbp), %rax
	movl	$9, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1488(%rbp), %rax
	movl	$9, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1296(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1104(%rbp), %rax
	movl	$10, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3600(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-912(%rbp), %rax
	movl	$11, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3552(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-720(%rbp), %rax
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3496(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-528(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-336(%rbp), %rax
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3488(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-144(%rbp), %rax
	leaq	-112(%rbp), %rdx
	movq	%r13, %rdi
	movq	-3648(%rbp), %xmm0
	movq	%rax, %rsi
	movq	-3480(%rbp), %xmm1
	movq	$0, -3328(%rbp)
	movq	%rax, -3480(%rbp)
	movhps	-3664(%rbp), %xmm0
	movhps	-3632(%rbp), %xmm1
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -3344(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1295
	call	_ZdlPv@PLT
.L1295:
	leaq	-3160(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3152(%rbp)
	jne	.L1422
	cmpq	$0, -2960(%rbp)
	jne	.L1423
.L1300:
	cmpq	$0, -2768(%rbp)
	jne	.L1424
.L1302:
	cmpq	$0, -2576(%rbp)
	jne	.L1425
.L1305:
	cmpq	$0, -2384(%rbp)
	jne	.L1426
.L1307:
	cmpq	$0, -2192(%rbp)
	jne	.L1427
.L1310:
	cmpq	$0, -2000(%rbp)
	jne	.L1428
.L1313:
	cmpq	$0, -1808(%rbp)
	jne	.L1429
.L1315:
	cmpq	$0, -1616(%rbp)
	jne	.L1430
.L1317:
	cmpq	$0, -1424(%rbp)
	jne	.L1431
.L1320:
	cmpq	$0, -1232(%rbp)
	jne	.L1432
.L1323:
	cmpq	$0, -1040(%rbp)
	jne	.L1433
.L1327:
	cmpq	$0, -848(%rbp)
	jne	.L1434
.L1330:
	cmpq	$0, -656(%rbp)
	leaq	-280(%rbp), %r14
	jne	.L1435
	cmpq	$0, -464(%rbp)
	jne	.L1436
.L1335:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -3328(%rbp)
	movaps	%xmm0, -3344(%rbp)
	call	_Znwm@PLT
	movq	-3488(%rbp), %rdi
	movq	%r13, %rsi
	movl	$67569415, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -3344(%rbp)
	movq	%rdx, -3328(%rbp)
	movq	%rdx, -3336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3344(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1337
	call	_ZdlPv@PLT
.L1337:
	movq	(%rbx), %rax
	movq	-3488(%rbp), %rdi
	movq	32(%rax), %r13
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3592(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3496(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3552(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3600(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3584(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3512(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3528(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3576(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3544(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3520(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3568(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3536(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3504(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3560(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3608(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1437
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1422:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -3328(%rbp)
	movaps	%xmm0, -3344(%rbp)
	call	_Znwm@PLT
	movq	-3608(%rbp), %rdi
	movq	%r13, %rsi
	movl	$67569415, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -3344(%rbp)
	movq	%rdx, -3328(%rbp)
	movq	%rdx, -3336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3344(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1297
	call	_ZdlPv@PLT
.L1297:
	movq	(%rbx), %rax
	movl	$113, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	16(%rax), %r14
	movq	(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rcx, -3632(%rbp)
	movq	%rax, -3688(%rbp)
	movq	%rbx, -3680(%rbp)
	movq	%r14, -3648(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3632(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadBigIntBitfieldENS0_8compiler5TNodeINS0_6BigIntEEE@PLT
	movl	$2147483646, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12DecodeWord32ENS0_8compiler11SloppyTNodeINS0_7Word32TEEEjj@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$114, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadBigIntBitfieldENS0_8compiler5TNodeINS0_6BigIntEEE@PLT
	movl	$2147483646, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12DecodeWord32ENS0_8compiler11SloppyTNodeINS0_7Word32TEEEjj@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	movq	%rax, -3664(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$116, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$117, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$118, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3648(%rbp), %rax
	movq	%rbx, %xmm5
	movq	-3632(%rbp), %xmm4
	movhps	-3664(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	$0, -3328(%rbp)
	movq	%rax, %xmm7
	movq	%rax, %xmm6
	movaps	%xmm5, -112(%rbp)
	punpcklqdq	%xmm7, %xmm4
	movhps	-3688(%rbp), %xmm6
	movq	-3680(%rbp), %xmm7
	movaps	%xmm5, -3664(%rbp)
	movaps	%xmm4, -3712(%rbp)
	movhps	-3632(%rbp), %xmm7
	movaps	%xmm6, -3648(%rbp)
	movaps	%xmm7, -3632(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm0, -3344(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm4
	movdqa	-128(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	64(%rax), %rdx
	movq	%rax, -3344(%rbp)
	movq	-3560(%rbp), %rdi
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rdx, -3328(%rbp)
	movq	%rdx, -3336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1298
	call	_ZdlPv@PLT
.L1298:
	movdqa	-3632(%rbp), %xmm4
	movdqa	-3648(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movdqa	-3664(%rbp), %xmm6
	movdqa	-3712(%rbp), %xmm7
	movaps	%xmm0, -3344(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	$0, -3328(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm4
	movdqa	-128(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	64(%rax), %rdx
	movq	%rax, -3344(%rbp)
	movq	-3504(%rbp), %rdi
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rdx, -3328(%rbp)
	movq	%rdx, -3336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1299
	call	_ZdlPv@PLT
.L1299:
	leaq	-2776(%rbp), %rcx
	leaq	-2968(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2960(%rbp)
	je	.L1300
.L1423:
	leaq	-2968(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3440(%rbp)
	movq	$0, -3432(%rbp)
	movq	$0, -3424(%rbp)
	movq	$0, -3416(%rbp)
	movq	$0, -3408(%rbp)
	movq	$0, -3400(%rbp)
	movq	$0, -3392(%rbp)
	movq	$0, -3376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3376(%rbp), %rax
	movq	-3560(%rbp), %rdi
	pushq	%rax
	leaq	-3392(%rbp), %rax
	leaq	-3424(%rbp), %rcx
	pushq	%rax
	leaq	-3400(%rbp), %rax
	leaq	-3408(%rbp), %r9
	pushq	%rax
	leaq	-3416(%rbp), %r8
	leaq	-3432(%rbp), %rdx
	leaq	-3440(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SC_SC_
	addq	$32, %rsp
	movl	$120, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$121, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$122, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$123, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$124, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$118, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3480(%rbp), %rsi
	leaq	-80(%rbp), %rdx
	movq	%r13, %rdi
	movq	-3400(%rbp), %xmm0
	movq	-3424(%rbp), %xmm2
	movq	$0, -3328(%rbp)
	movq	-3440(%rbp), %xmm1
	movhps	-3408(%rbp), %xmm0
	movhps	-3416(%rbp), %xmm2
	movaps	%xmm0, -112(%rbp)
	movhps	-3432(%rbp), %xmm1
	movq	-3424(%rbp), %xmm0
	movaps	%xmm1, -144(%rbp)
	movhps	-3432(%rbp), %xmm0
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3344(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3504(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1301
	call	_ZdlPv@PLT
.L1301:
	leaq	-2776(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2768(%rbp)
	je	.L1302
.L1424:
	leaq	-2776(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3440(%rbp)
	movq	$0, -3432(%rbp)
	movq	$0, -3424(%rbp)
	movq	$0, -3416(%rbp)
	movq	$0, -3408(%rbp)
	movq	$0, -3400(%rbp)
	movq	$0, -3392(%rbp)
	movq	$0, -3376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3376(%rbp), %rax
	movq	-3504(%rbp), %rdi
	pushq	%rax
	leaq	-3392(%rbp), %rax
	leaq	-3424(%rbp), %rcx
	pushq	%rax
	leaq	-3400(%rbp), %rax
	leaq	-3408(%rbp), %r9
	pushq	%rax
	leaq	-3416(%rbp), %r8
	leaq	-3432(%rbp), %rdx
	leaq	-3440(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SC_SC_
	addq	$32, %rsp
	movl	$128, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3408(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	-3392(%rbp), %xmm3
	movq	-3408(%rbp), %xmm2
	movaps	%xmm0, -3344(%rbp)
	movq	-3424(%rbp), %xmm4
	movq	-3440(%rbp), %xmm5
	movhps	-3376(%rbp), %xmm3
	movq	$0, -3328(%rbp)
	movhps	-3400(%rbp), %xmm2
	movhps	-3416(%rbp), %xmm4
	movaps	%xmm3, -3664(%rbp)
	movhps	-3432(%rbp), %xmm5
	movaps	%xmm2, -3680(%rbp)
	movaps	%xmm4, -3648(%rbp)
	movaps	%xmm5, -3632(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm4
	movdqa	-128(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	64(%rax), %rdx
	movq	%rax, -3344(%rbp)
	movq	-3536(%rbp), %rdi
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rdx, -3328(%rbp)
	movq	%rdx, -3336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1303
	call	_ZdlPv@PLT
.L1303:
	movdqa	-3632(%rbp), %xmm3
	movdqa	-3648(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movdqa	-3680(%rbp), %xmm5
	movdqa	-3664(%rbp), %xmm6
	movaps	%xmm0, -3344(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movq	$0, -3328(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	leaq	64(%rax), %rdx
	movq	%rax, -3344(%rbp)
	movq	-3568(%rbp), %rdi
	movups	%xmm7, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rdx, -3328(%rbp)
	movq	%rdx, -3336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1304
	call	_ZdlPv@PLT
.L1304:
	leaq	-2392(%rbp), %rcx
	leaq	-2584(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2576(%rbp)
	je	.L1305
.L1425:
	leaq	-2584(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3440(%rbp)
	movq	$0, -3432(%rbp)
	movq	$0, -3424(%rbp)
	movq	$0, -3416(%rbp)
	movq	$0, -3408(%rbp)
	movq	$0, -3400(%rbp)
	movq	$0, -3392(%rbp)
	movq	$0, -3376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3376(%rbp), %rax
	movq	-3536(%rbp), %rdi
	pushq	%rax
	leaq	-3392(%rbp), %rax
	leaq	-3424(%rbp), %rcx
	pushq	%rax
	leaq	-3400(%rbp), %rax
	leaq	-3408(%rbp), %r9
	pushq	%rax
	leaq	-3416(%rbp), %r8
	leaq	-3432(%rbp), %rdx
	leaq	-3440(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SC_SC_
	addq	$32, %rsp
	movl	$130, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3440(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movaps	%xmm0, -3344(%rbp)
	movq	%rax, -144(%rbp)
	movq	-3432(%rbp), %rax
	movq	$0, -3328(%rbp)
	movq	%rax, -136(%rbp)
	movq	-3424(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-3416(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-3392(%rbp), %rax
	movq	%rax, -112(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-144(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm7
	movq	-3496(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -3344(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -3328(%rbp)
	movq	%rdx, -3336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1306
	call	_ZdlPv@PLT
.L1306:
	leaq	-664(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2384(%rbp)
	je	.L1307
.L1426:
	leaq	-2392(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3440(%rbp)
	movq	$0, -3432(%rbp)
	movq	$0, -3424(%rbp)
	movq	$0, -3416(%rbp)
	movq	$0, -3408(%rbp)
	movq	$0, -3400(%rbp)
	movq	$0, -3392(%rbp)
	movq	$0, -3376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3376(%rbp), %rax
	movq	-3568(%rbp), %rdi
	pushq	%rax
	leaq	-3392(%rbp), %rax
	leaq	-3424(%rbp), %rcx
	pushq	%rax
	leaq	-3400(%rbp), %rax
	leaq	-3408(%rbp), %r9
	pushq	%rax
	leaq	-3416(%rbp), %r8
	leaq	-3432(%rbp), %rdx
	leaq	-3440(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SC_SC_
	addq	$32, %rsp
	movl	$134, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3400(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	-3392(%rbp), %xmm6
	movq	-3408(%rbp), %xmm7
	movaps	%xmm0, -3344(%rbp)
	movq	-3424(%rbp), %xmm3
	movq	-3440(%rbp), %xmm2
	movhps	-3376(%rbp), %xmm6
	movq	$0, -3328(%rbp)
	movhps	-3400(%rbp), %xmm7
	movhps	-3416(%rbp), %xmm3
	movaps	%xmm6, -3664(%rbp)
	movhps	-3432(%rbp), %xmm2
	movaps	%xmm7, -3632(%rbp)
	movaps	%xmm3, -3648(%rbp)
	movaps	%xmm2, -3680(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	leaq	64(%rax), %rdx
	movq	%rax, -3344(%rbp)
	movq	-3520(%rbp), %rdi
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movq	%rdx, -3328(%rbp)
	movq	%rdx, -3336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1308
	call	_ZdlPv@PLT
.L1308:
	movdqa	-3680(%rbp), %xmm7
	movdqa	-3648(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movdqa	-3632(%rbp), %xmm2
	movdqa	-3664(%rbp), %xmm4
	movaps	%xmm0, -3344(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movq	$0, -3328(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm3
	leaq	64(%rax), %rdx
	movq	%rax, -3344(%rbp)
	movq	-3584(%rbp), %rdi
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rdx, -3328(%rbp)
	movq	%rdx, -3336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1309
	call	_ZdlPv@PLT
.L1309:
	leaq	-1240(%rbp), %rcx
	leaq	-2200(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2192(%rbp)
	je	.L1310
.L1427:
	leaq	-2200(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3440(%rbp)
	movq	$0, -3432(%rbp)
	movq	$0, -3424(%rbp)
	movq	$0, -3416(%rbp)
	movq	$0, -3408(%rbp)
	movq	$0, -3400(%rbp)
	movq	$0, -3392(%rbp)
	movq	$0, -3376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3376(%rbp), %rax
	movq	-3520(%rbp), %rdi
	pushq	%rax
	leaq	-3392(%rbp), %rax
	leaq	-3408(%rbp), %r9
	pushq	%rax
	leaq	-3400(%rbp), %rax
	leaq	-3416(%rbp), %r8
	pushq	%rax
	leaq	-3424(%rbp), %rcx
	leaq	-3432(%rbp), %rdx
	leaq	-3440(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SC_SC_
	addq	$32, %rsp
	movl	$135, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3392(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadBigIntBitfieldENS0_8compiler5TNodeINS0_6BigIntEEE@PLT
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12DecodeWord32ENS0_8compiler11SloppyTNodeINS0_7Word32TEEEjj@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3416(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	-3392(%rbp), %xmm4
	movq	-3408(%rbp), %xmm5
	movaps	%xmm0, -3344(%rbp)
	movq	-3424(%rbp), %xmm6
	movq	-3440(%rbp), %xmm7
	movhps	-3376(%rbp), %xmm4
	movq	$0, -3328(%rbp)
	movhps	-3400(%rbp), %xmm5
	movhps	-3416(%rbp), %xmm6
	movaps	%xmm4, -3648(%rbp)
	movhps	-3432(%rbp), %xmm7
	movaps	%xmm5, -3680(%rbp)
	movaps	%xmm6, -3664(%rbp)
	movaps	%xmm7, -3632(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm1
	movdqa	-128(%rbp), %xmm2
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	leaq	64(%rax), %rdx
	movq	%rax, -3344(%rbp)
	movq	-3544(%rbp), %rdi
	movups	%xmm1, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rdx, -3328(%rbp)
	movq	%rdx, -3336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1311
	call	_ZdlPv@PLT
.L1311:
	movdqa	-3632(%rbp), %xmm6
	movdqa	-3664(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movdqa	-3680(%rbp), %xmm3
	movdqa	-3648(%rbp), %xmm1
	movaps	%xmm0, -3344(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movq	$0, -3328(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	leaq	64(%rax), %rdx
	movq	%rax, -3344(%rbp)
	movq	-3576(%rbp), %rdi
	movups	%xmm2, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movq	%rdx, -3328(%rbp)
	movq	%rdx, -3336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1312
	call	_ZdlPv@PLT
.L1312:
	leaq	-1816(%rbp), %rcx
	leaq	-2008(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2000(%rbp)
	je	.L1313
.L1428:
	leaq	-2008(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3440(%rbp)
	movq	$0, -3432(%rbp)
	movq	$0, -3424(%rbp)
	movq	$0, -3416(%rbp)
	movq	$0, -3408(%rbp)
	movq	$0, -3400(%rbp)
	movq	$0, -3392(%rbp)
	movq	$0, -3376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3376(%rbp), %rax
	movq	-3544(%rbp), %rdi
	pushq	%rax
	leaq	-3392(%rbp), %rax
	leaq	-3424(%rbp), %rcx
	pushq	%rax
	leaq	-3400(%rbp), %rax
	leaq	-3408(%rbp), %r9
	pushq	%rax
	leaq	-3416(%rbp), %r8
	leaq	-3432(%rbp), %rdx
	leaq	-3440(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SC_SC_
	pxor	%xmm0, %xmm0
	addq	$32, %rsp
	movq	%r13, %rdi
	movq	-3440(%rbp), %rdx
	movq	-3392(%rbp), %rax
	movaps	%xmm0, -3344(%rbp)
	movq	-3480(%rbp), %rsi
	movq	$0, -3328(%rbp)
	movq	%rdx, -144(%rbp)
	movq	-3432(%rbp), %rdx
	movq	%rax, -96(%rbp)
	movq	%rdx, -136(%rbp)
	movq	-3424(%rbp), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -128(%rbp)
	movq	-3416(%rbp), %rdx
	movq	%rdx, -120(%rbp)
	movq	-3408(%rbp), %rdx
	movq	%rdx, -112(%rbp)
	movq	-3400(%rbp), %rdx
	movq	%rdx, -104(%rbp)
	movq	-3376(%rbp), %rdx
	movq	%rdx, -88(%rbp)
	leaq	-72(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3528(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1314
	call	_ZdlPv@PLT
.L1314:
	leaq	-1624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1808(%rbp)
	je	.L1315
.L1429:
	leaq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3456(%rbp)
	leaq	-3392(%rbp), %r14
	movq	$0, -3448(%rbp)
	movq	$0, -3440(%rbp)
	movq	$0, -3432(%rbp)
	movq	$0, -3424(%rbp)
	movq	$0, -3416(%rbp)
	movq	$0, -3408(%rbp)
	movq	$0, -3400(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3400(%rbp), %rax
	movq	-3576(%rbp), %rdi
	pushq	%rax
	leaq	-3408(%rbp), %rax
	leaq	-3440(%rbp), %rcx
	pushq	%rax
	leaq	-3416(%rbp), %rax
	leaq	-3432(%rbp), %r8
	pushq	%rax
	leaq	-3448(%rbp), %rdx
	leaq	-3424(%rbp), %r9
	leaq	-3456(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SC_SC_
	addq	$32, %rsp
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3456(%rbp), %r9
	movq	%r14, %rdi
	movq	-3408(%rbp), %rbx
	movq	%r9, -3632(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$790, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-3344(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-3480(%rbp), %rcx
	xorl	%esi, %esi
	movq	%rbx, -144(%rbp)
	movl	$1, %ebx
	movq	%rax, %r8
	movq	%r14, %rdi
	movq	-3632(%rbp), %r9
	pushq	%rbx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	leaq	-3376(%rbp), %rdx
	pushq	%rcx
	movl	$1, %ecx
	movq	%rax, -3376(%rbp)
	movq	-3328(%rbp), %rax
	movq	%rax, -3368(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-72(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rbx, -80(%rbp)
	movq	-3408(%rbp), %xmm0
	movq	-3480(%rbp), %rsi
	movq	$0, -3328(%rbp)
	movq	-3424(%rbp), %xmm1
	movq	-3440(%rbp), %xmm2
	movq	-3456(%rbp), %xmm3
	movhps	-3400(%rbp), %xmm0
	movhps	-3416(%rbp), %xmm1
	movhps	-3432(%rbp), %xmm2
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3448(%rbp), %xmm3
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3344(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3512(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3344(%rbp), %rdi
	popq	%r8
	popq	%r9
	testq	%rdi, %rdi
	je	.L1316
	call	_ZdlPv@PLT
.L1316:
	leaq	-1432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1616(%rbp)
	je	.L1317
.L1430:
	leaq	-1624(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$506379002203014919, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -3328(%rbp)
	movaps	%xmm0, -3344(%rbp)
	call	_Znwm@PLT
	movq	-3528(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%rax, -3344(%rbp)
	movq	%rdx, -3328(%rbp)
	movq	%rdx, -3336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3344(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1318
	call	_ZdlPv@PLT
.L1318:
	movq	(%rbx), %rax
	movq	-3480(%rbp), %rsi
	leaq	-72(%rbp), %rdx
	movq	%r13, %rdi
	movdqu	48(%rax), %xmm0
	movdqu	(%rax), %xmm3
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	64(%rax), %rax
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -144(%rbp)
	movq	%rax, -80(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3344(%rbp)
	movq	$0, -3328(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3512(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1319
	call	_ZdlPv@PLT
.L1319:
	leaq	-1432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1424(%rbp)
	je	.L1320
.L1431:
	leaq	-1432(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$506379002203014919, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -3328(%rbp)
	movaps	%xmm0, -3344(%rbp)
	call	_Znwm@PLT
	movq	-3512(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%rax, -3344(%rbp)
	movq	%rdx, -3328(%rbp)
	movq	%rdx, -3336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3344(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1321
	call	_ZdlPv@PLT
.L1321:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	64(%rax), %rdx
	movdqu	(%rax), %xmm7
	movq	$0, -3328(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -112(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm0, -3344(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-144(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm1
	movq	-3496(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -3344(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm1, 16(%rax)
	movq	%rdx, -3328(%rbp)
	movq	%rdx, -3336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1322
	call	_ZdlPv@PLT
.L1322:
	leaq	-664(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1232(%rbp)
	je	.L1323
.L1432:
	leaq	-1240(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3448(%rbp)
	movq	$0, -3440(%rbp)
	movq	$0, -3432(%rbp)
	movq	$0, -3424(%rbp)
	movq	$0, -3416(%rbp)
	movq	$0, -3408(%rbp)
	movq	$0, -3400(%rbp)
	movq	$0, -3392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3392(%rbp), %rax
	movq	-3584(%rbp), %rdi
	pushq	%rax
	leaq	-3400(%rbp), %rax
	leaq	-3416(%rbp), %r9
	pushq	%rax
	leaq	-3408(%rbp), %rax
	leaq	-3432(%rbp), %rcx
	pushq	%rax
	leaq	-3424(%rbp), %r8
	leaq	-3440(%rbp), %rdx
	leaq	-3448(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TENS0_7IntPtrTES6_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EESC_PNS8_IS5_EEPNS8_IS6_EESG_SC_SC_
	addq	$32, %rsp
	movl	$139, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3416(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	leaq	-3376(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %r8
	movq	%rbx, %rcx
	movq	%r15, %rdi
	movq	-3424(%rbp), %rdx
	movq	-3448(%rbp), %rsi
	call	_ZN2v88internal30AllocateEmptyBigIntNoThrow_261EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEENS4_INS0_7IntPtrTEEEPNS1_18CodeAssemblerLabelE
	leaq	-56(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rbx, -72(%rbp)
	movq	-3400(%rbp), %xmm0
	movq	-3424(%rbp), %rcx
	movq	%rax, -64(%rbp)
	movq	-3416(%rbp), %xmm1
	movq	-3480(%rbp), %rsi
	movq	$0, -3360(%rbp)
	movq	-3432(%rbp), %xmm2
	movhps	-3392(%rbp), %xmm0
	movq	%rcx, -80(%rbp)
	movq	-3448(%rbp), %xmm3
	movhps	-3408(%rbp), %xmm1
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3424(%rbp), %xmm2
	movaps	%xmm1, -112(%rbp)
	movhps	-3440(%rbp), %xmm3
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm0, -3376(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3552(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1324
	call	_ZdlPv@PLT
.L1324:
	leaq	-856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3336(%rbp)
	jne	.L1438
.L1325:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1040(%rbp)
	je	.L1327
.L1433:
	leaq	-1048(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3480(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movl	$1284, %edi
	movw	%di, -136(%rbp)
	leaq	-134(%rbp), %rdx
	movq	%r13, %rdi
	movabsq	$506379002203014919, %rax
	movq	%rax, -144(%rbp)
	movaps	%xmm0, -3344(%rbp)
	movq	$0, -3328(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3600(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1328
	call	_ZdlPv@PLT
.L1328:
	movq	-3592(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	$0, -3328(%rbp)
	movaps	%xmm0, -3344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1329
	call	_ZdlPv@PLT
.L1329:
	leaq	-472(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -848(%rbp)
	je	.L1330
.L1434:
	leaq	-856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1284, %esi
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movw	%si, -136(%rbp)
	movq	-3480(%rbp), %rsi
	movabsq	$506379002203014919, %rax
	leaq	-133(%rbp), %rdx
	movq	%rax, -144(%rbp)
	movaps	%xmm0, -3344(%rbp)
	movb	$7, -134(%rbp)
	movq	$0, -3328(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3552(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3344(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1331
	call	_ZdlPv@PLT
.L1331:
	movq	(%rbx), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	24(%rax), %rdx
	movq	48(%rax), %r8
	movq	(%rax), %rbx
	movq	%rcx, -3648(%rbp)
	movq	16(%rax), %rcx
	movq	%rdx, -3680(%rbp)
	movl	$141, %edx
	movq	80(%rax), %r14
	movq	%rcx, -3664(%rbp)
	movq	%r8, -3688(%rbp)
	movq	%rbx, -3632(%rbp)
	movq	56(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	call	_ZN2v88internal17ExternalReference54mutable_big_int_absolute_add_and_canonicalize_functionEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler16ExternalConstantENS0_17ExternalReferenceE@PLT
	movq	-3688(%rbp), %r8
	movl	$1800, %ecx
	movq	%r13, %rdi
	movw	%cx, -112(%rbp)
	movl	$1800, %edx
	movq	%rax, %rsi
	movq	-3480(%rbp), %rcx
	movw	%dx, -128(%rbp)
	movl	$1800, %eax
	movl	$1800, %edx
	movq	%r8, -120(%rbp)
	movl	$3, %r8d
	movw	%ax, -144(%rbp)
	movq	%r14, -136(%rbp)
	movq	%rbx, -104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallCFunctionEPNS1_4NodeENS0_11MachineTypeESt16initializer_listISt4pairIS5_S4_EE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$142, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal35Convert8ATBigInt13MutableBigInt_256EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE
	movl	$40, %edi
	movq	-3632(%rbp), %xmm0
	movq	$0, -3328(%rbp)
	movq	%rax, -112(%rbp)
	movhps	-3648(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-3664(%rbp), %xmm0
	movhps	-3680(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3344(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-144(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm4
	movq	-3496(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -3344(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -3328(%rbp)
	movq	%rdx, -3336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1332
	call	_ZdlPv@PLT
.L1332:
	leaq	-664(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1330
	.p2align 4,,10
	.p2align 3
.L1435:
	leaq	-664(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3416(%rbp)
	movq	$0, -3408(%rbp)
	movq	$0, -3400(%rbp)
	movq	$0, -3392(%rbp)
	movq	$0, -3376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3496(%rbp), %rdi
	leaq	-3400(%rbp), %rcx
	leaq	-3376(%rbp), %r9
	leaq	-3392(%rbp), %r8
	leaq	-3408(%rbp), %rdx
	leaq	-3416(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_PNS7_IS5_EESB_
	movl	$110, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3416(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movaps	%xmm0, -3344(%rbp)
	movq	%rax, -144(%rbp)
	movq	-3408(%rbp), %rax
	movq	$0, -3328(%rbp)
	movq	%rax, -136(%rbp)
	movq	-3400(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-3392(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-3376(%rbp), %rax
	movq	%rax, -112(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-144(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm6
	movq	-3488(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -3344(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -3328(%rbp)
	movq	%rdx, -3336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1334
	call	_ZdlPv@PLT
.L1334:
	leaq	-280(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -464(%rbp)
	je	.L1335
.L1436:
	leaq	-472(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3592(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	$0, -3328(%rbp)
	movaps	%xmm0, -3344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1336
	call	_ZdlPv@PLT
.L1336:
	movq	-3616(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1335
	.p2align 4,,10
	.p2align 3
.L1438:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-64(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rbx, -72(%rbp)
	movq	-3400(%rbp), %xmm0
	movq	-3424(%rbp), %rax
	movq	$0, -3360(%rbp)
	movq	-3416(%rbp), %xmm1
	movq	-3480(%rbp), %rsi
	movq	-3432(%rbp), %xmm2
	movhps	-3392(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movq	-3448(%rbp), %xmm3
	movhps	-3408(%rbp), %xmm1
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3424(%rbp), %xmm2
	movaps	%xmm1, -112(%rbp)
	movhps	-3440(%rbp), %xmm3
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm0, -3376(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3600(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1326
	call	_ZdlPv@PLT
.L1326:
	leaq	-1048(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1325
.L1437:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22473:
	.size	_ZN2v88internal28MutableBigIntAbsoluteAdd_265EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6BigIntEEES8_NS4_INS0_7Uint32TEEEPNS1_18CodeAssemblerLabelE, .-_ZN2v88internal28MutableBigIntAbsoluteAdd_265EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6BigIntEEES8_NS4_INS0_7Uint32TEEEPNS1_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_PNS7_IS5_EESD_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_PNS7_IS5_EESD_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_PNS7_IS5_EESD_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_PNS7_IS5_EESD_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_PNS7_IS5_EESD_:
.LFB26931:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$5, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$56, %rsp
	movq	%r8, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$67569415, (%rax)
	leaq	5(%rax), %rdx
	movb	$4, 4(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1440
	movq	%rax, -96(%rbp)
	call	_ZdlPv@PLT
	movq	-96(%rbp), %rax
.L1440:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1441
	movq	%rdx, (%r15)
.L1441:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1442
	movq	%rdx, (%r14)
.L1442:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1443
	movq	%rdx, 0(%r13)
.L1443:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1444
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1444:
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L1439
	movq	%rax, (%rbx)
.L1439:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1466
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1466:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26931:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_PNS7_IS5_EESD_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_PNS7_IS5_EESD_
	.section	.text._ZN2v88internal17BigIntAddImpl_266EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6BigIntEEES8_PNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal17BigIntAddImpl_266EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6BigIntEEES8_PNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal17BigIntAddImpl_266EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6BigIntEEES8_PNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal17BigIntAddImpl_266EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6BigIntEEES8_PNS1_18CodeAssemblerLabelE:
.LFB22486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-1992(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-2256(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$2392, %rsp
	movq	%rsi, -2288(%rbp)
	movq	%r8, -2400(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -2256(%rbp)
	movq	%rdi, -2048(%rbp)
	movl	$72, %edi
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movq	$0, -2024(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -2040(%rbp)
	leaq	-2048(%rbp), %rax
	movq	%rdx, -2024(%rbp)
	movq	%rdx, -2032(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2008(%rbp)
	movq	%rax, -2264(%rbp)
	movq	$0, -2016(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2256(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1848(%rbp)
	movq	$0, -1840(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1832(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1800(%rbp), %rcx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -1832(%rbp)
	movq	%rdx, -1840(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2304(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1816(%rbp)
	movq	%rax, -1848(%rbp)
	movq	$0, -1824(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2256(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1656(%rbp)
	movq	$0, -1648(%rbp)
	movq	%rax, -1664(%rbp)
	movq	$0, -1640(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1608(%rbp), %rcx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -1640(%rbp)
	movq	%rdx, -1648(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2392(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1624(%rbp)
	movq	%rax, -1656(%rbp)
	movq	$0, -1632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2256(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1464(%rbp)
	movq	$0, -1456(%rbp)
	movq	%rax, -1472(%rbp)
	movq	$0, -1448(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1416(%rbp), %rcx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -1448(%rbp)
	movq	%rdx, -1456(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2376(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1432(%rbp)
	movq	%rax, -1464(%rbp)
	movq	$0, -1440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2256(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1272(%rbp)
	movq	$0, -1264(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -1256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1224(%rbp), %rcx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -1256(%rbp)
	movq	%rdx, -1264(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2352(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1240(%rbp)
	movq	%rax, -1272(%rbp)
	movq	$0, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2256(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1080(%rbp)
	movq	$0, -1072(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$0, -1064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1032(%rbp), %rcx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -1064(%rbp)
	movq	%rdx, -1072(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2384(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1048(%rbp)
	movq	%rax, -1080(%rbp)
	movq	$0, -1056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2256(%rbp), %rax
	movl	$120, %edi
	movq	$0, -888(%rbp)
	movq	$0, -880(%rbp)
	movq	%rax, -896(%rbp)
	movq	$0, -872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-840(%rbp), %rcx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -872(%rbp)
	movq	%rdx, -880(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2360(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -856(%rbp)
	movq	%rax, -888(%rbp)
	movq	$0, -864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2256(%rbp), %rax
	movl	$96, %edi
	movq	$0, -696(%rbp)
	movq	$0, -688(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-648(%rbp), %rcx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -680(%rbp)
	movq	%rdx, -688(%rbp)
	movq	%rcx, -2344(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movups	%xmm0, -664(%rbp)
	movq	%rax, -696(%rbp)
	movq	$0, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	-2256(%rbp), %rax
	leaq	-456(%rbp), %rcx
	movq	$0, -504(%rbp)
	movq	%rcx, %rdi
	movq	%rcx, -2368(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -512(%rbp)
	movq	$0, -496(%rbp)
	movq	$0, -488(%rbp)
	movq	$0, -480(%rbp)
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2256(%rbp), %rax
	movl	$96, %edi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-264(%rbp), %rcx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2336(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -312(%rbp)
	movups	%xmm0, -280(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2288(%rbp), %r9
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	%r13, -120(%rbp)
	leaq	-2176(%rbp), %r13
	movq	%r9, -128(%rbp)
	movaps	%xmm0, -2176(%rbp)
	movq	%rbx, -112(%rbp)
	movq	$0, -2160(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movdqa	-128(%rbp), %xmm3
	movq	%r13, %rsi
	movq	-2264(%rbp), %rdi
	leaq	24(%rax), %rdx
	movq	%rax, -2176(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -2160(%rbp)
	movq	%rdx, -2168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1468
	call	_ZdlPv@PLT
.L1468:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1856(%rbp), %rax
	cmpq	$0, -1984(%rbp)
	movq	%rax, -2272(%rbp)
	leaq	-1280(%rbp), %rax
	movq	%rax, -2320(%rbp)
	jne	.L1559
.L1469:
	leaq	-1472(%rbp), %rax
	cmpq	$0, -1792(%rbp)
	movq	%rax, -2296(%rbp)
	leaq	-1664(%rbp), %rax
	movq	%rax, -2288(%rbp)
	jne	.L1560
.L1473:
	leaq	-512(%rbp), %rax
	cmpq	$0, -1600(%rbp)
	movq	%rax, -2328(%rbp)
	jne	.L1561
.L1477:
	cmpq	$0, -1408(%rbp)
	leaq	-704(%rbp), %rbx
	jne	.L1562
.L1480:
	leaq	-1088(%rbp), %rax
	cmpq	$0, -1216(%rbp)
	leaq	-896(%rbp), %r14
	movq	%rax, -2304(%rbp)
	jne	.L1563
.L1483:
	cmpq	$0, -1024(%rbp)
	jne	.L1564
.L1486:
	cmpq	$0, -832(%rbp)
	jne	.L1565
.L1488:
	cmpq	$0, -640(%rbp)
	leaq	-320(%rbp), %r15
	jne	.L1566
	cmpq	$0, -448(%rbp)
	jne	.L1567
.L1493:
	movq	-2336(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2160(%rbp)
	movaps	%xmm0, -2176(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	$117901063, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2176(%rbp)
	movq	%rdx, -2160(%rbp)
	movq	%rdx, -2168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2176(%rbp), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L1495
	call	_ZdlPv@PLT
.L1495:
	movq	0(%r13), %rax
	movq	%r15, %rdi
	movq	24(%rax), %r13
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2328(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2304(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2320(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2296(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2288(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2272(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2264(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1568
	addq	$2392, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1559:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2160(%rbp)
	movaps	%xmm0, -2176(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	-2264(%rbp), %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -2176(%rbp)
	movq	%rdx, -2160(%rbp)
	movq	%rdx, -2168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2176(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1470
	call	_ZdlPv@PLT
.L1470:
	movq	(%rbx), %rax
	movl	$147, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	16(%rax), %r14
	movq	%rbx, -2272(%rbp)
	movq	8(%rax), %rbx
	movq	%rbx, -2320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadBigIntBitfieldENS0_8compiler5TNodeINS0_6BigIntEEE@PLT
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12DecodeWord32ENS0_8compiler11SloppyTNodeINS0_7Word32TEEEjj@PLT
	movq	%r13, %rdi
	movq	%rax, -2288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$148, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadBigIntBitfieldENS0_8compiler5TNodeINS0_6BigIntEEE@PLT
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12DecodeWord32ENS0_8compiler11SloppyTNodeINS0_7Word32TEEEjj@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$149, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2288(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, -2296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-88(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	-2272(%rbp), %xmm5
	movq	%r14, %xmm4
	leaq	-128(%rbp), %r14
	movq	%rdx, -2328(%rbp)
	movhps	-2288(%rbp), %xmm4
	movq	%r14, %rsi
	movaps	%xmm0, -2176(%rbp)
	movhps	-2320(%rbp), %xmm5
	movaps	%xmm4, -2288(%rbp)
	movaps	%xmm5, -2320(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -2160(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1856(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -2272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2176(%rbp), %rdi
	movq	-2328(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L1471
	call	_ZdlPv@PLT
	movq	-2328(%rbp), %rdx
.L1471:
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rbx, -96(%rbp)
	movdqa	-2320(%rbp), %xmm6
	movdqa	-2288(%rbp), %xmm7
	movaps	%xmm0, -2176(%rbp)
	movq	$0, -2160(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1280(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -2320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1472
	call	_ZdlPv@PLT
.L1472:
	movq	-2352(%rbp), %rcx
	movq	-2304(%rbp), %rdx
	movq	%r12, %rdi
	movq	-2296(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1566:
	movq	-2344(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2160(%rbp)
	movaps	%xmm0, -2176(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2176(%rbp)
	movq	%rdx, -2160(%rbp)
	movq	%rdx, -2168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2176(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L1491
	call	_ZdlPv@PLT
.L1491:
	movq	(%r15), %rax
	movl	$145, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	%rcx, -2344(%rbp)
	movq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, -2352(%rbp)
	movq	%rax, -2360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-128(%rbp), %rsi
	leaq	-96(%rbp), %rdx
	movq	%r13, %rdi
	movq	$0, -2160(%rbp)
	movq	%r15, %xmm0
	leaq	-320(%rbp), %r15
	movhps	-2344(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-2352(%rbp), %xmm0
	movhps	-2360(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2176(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1492
	call	_ZdlPv@PLT
.L1492:
	movq	-2336(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -448(%rbp)
	je	.L1493
.L1567:
	movq	-2368(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2328(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	$0, -2160(%rbp)
	movaps	%xmm0, -2176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1494
	call	_ZdlPv@PLT
.L1494:
	movq	-2400(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1493
	.p2align 4,,10
	.p2align 3
.L1565:
	movq	-2360(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -2240(%rbp)
	movq	$0, -2232(%rbp)
	movq	$0, -2224(%rbp)
	movq	$0, -2216(%rbp)
	movq	$0, -2208(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2208(%rbp), %r9
	movq	%r14, %rdi
	leaq	-2224(%rbp), %rcx
	leaq	-2216(%rbp), %r8
	leaq	-2232(%rbp), %rdx
	leaq	-2240(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_PNS7_IS5_EESD_
	movl	$160, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2216(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal14InvertSign_260EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7Uint32TEEE
	movq	-2232(%rbp), %rcx
	movq	-2224(%rbp), %rdx
	movq	%r15, %rdi
	movq	-2240(%rbp), %rsi
	movq	%rax, %r8
	call	_ZN2v88internal28MutableBigIntAbsoluteSub_264EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6BigIntEEES8_NS4_INS0_7Uint32TEEE
	leaq	-128(%rbp), %rsi
	leaq	-96(%rbp), %rdx
	movq	%r13, %rdi
	movq	-2224(%rbp), %xmm0
	movq	%rax, %xmm6
	movq	-2240(%rbp), %xmm1
	movq	$0, -2160(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movhps	-2232(%rbp), %xmm1
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -2176(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1489
	call	_ZdlPv@PLT
.L1489:
	movq	-2344(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1488
	.p2align 4,,10
	.p2align 3
.L1564:
	movq	-2384(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -2240(%rbp)
	movq	$0, -2232(%rbp)
	movq	$0, -2224(%rbp)
	movq	$0, -2216(%rbp)
	movq	$0, -2208(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2304(%rbp), %rdi
	leaq	-2208(%rbp), %r9
	leaq	-2224(%rbp), %rcx
	leaq	-2216(%rbp), %r8
	leaq	-2232(%rbp), %rdx
	leaq	-2240(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_PNS7_IS5_EESD_
	movl	$158, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2216(%rbp), %r8
	movq	%r15, %rdi
	movq	-2224(%rbp), %rcx
	movq	-2232(%rbp), %rdx
	movq	-2240(%rbp), %rsi
	call	_ZN2v88internal28MutableBigIntAbsoluteSub_264EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6BigIntEEES8_NS4_INS0_7Uint32TEEE
	leaq	-128(%rbp), %rsi
	leaq	-96(%rbp), %rdx
	movq	%r13, %rdi
	movq	-2224(%rbp), %xmm0
	movq	%rax, %xmm2
	movq	-2240(%rbp), %xmm1
	movq	$0, -2160(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movhps	-2232(%rbp), %xmm1
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -2176(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1487
	call	_ZdlPv@PLT
.L1487:
	movq	-2344(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L1563:
	movq	-2352(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -2240(%rbp)
	movq	$0, -2232(%rbp)
	movq	$0, -2224(%rbp)
	movq	$0, -2216(%rbp)
	movq	$0, -2208(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2320(%rbp), %rdi
	leaq	-2208(%rbp), %r9
	leaq	-2216(%rbp), %r8
	leaq	-2224(%rbp), %rcx
	leaq	-2232(%rbp), %rdx
	leaq	-2240(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_PNS7_IS5_EESD_
	movl	$157, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2224(%rbp), %rdx
	movq	-2232(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal32MutableBigIntAbsoluteCompare_263EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEES6_
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal44FromConstexpr7ATint3217ATconstexpr_int31_146EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -2352(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2352(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	-128(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler23Int32GreaterThanOrEqualENS1_11SloppyTNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, -2352(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-2224(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movq	-2216(%rbp), %rdx
	movq	-2208(%rbp), %rdi
	movq	-2240(%rbp), %rax
	movaps	%xmm0, -2176(%rbp)
	movq	-2232(%rbp), %rcx
	movq	%rsi, -2424(%rbp)
	movq	%rdx, -2392(%rbp)
	movq	%rdi, -2376(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%r14, %rsi
	movq	%rdx, -104(%rbp)
	leaq	-88(%rbp), %rdx
	movq	%rdi, -96(%rbp)
	movq	%r13, %rdi
	movq	%rdx, -2432(%rbp)
	movq	%rax, -2416(%rbp)
	movq	%rcx, -2408(%rbp)
	movq	%rax, -128(%rbp)
	movq	%rcx, -120(%rbp)
	movq	$0, -2160(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2304(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2176(%rbp), %rdi
	movq	-2432(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L1484
	call	_ZdlPv@PLT
	movq	-2432(%rbp), %rdx
.L1484:
	movq	-2416(%rbp), %xmm0
	movq	-2376(%rbp), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	-896(%rbp), %r14
	movq	$0, -2160(%rbp)
	movhps	-2408(%rbp), %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	-2424(%rbp), %xmm0
	movhps	-2392(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2176(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1485
	call	_ZdlPv@PLT
.L1485:
	movq	-2360(%rbp), %rcx
	movq	-2384(%rbp), %rdx
	movq	%r12, %rdi
	movq	-2352(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1483
	.p2align 4,,10
	.p2align 3
.L1562:
	movq	-2376(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-128(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-119(%rbp), %rdx
	movq	%r14, %rsi
	movabsq	$290208414817519367, %rax
	movq	%r13, %rdi
	movaps	%xmm0, -2176(%rbp)
	movq	%rax, -128(%rbp)
	movb	$7, -120(%rbp)
	movq	$0, -2160(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2296(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2176(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1481
	call	_ZdlPv@PLT
.L1481:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	leaq	-704(%rbp), %rbx
	movq	(%rax), %rsi
	movq	16(%rax), %rdx
	movq	8(%rax), %rcx
	movq	64(%rax), %rax
	movaps	%xmm0, -2176(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%r14, %rsi
	movq	%rdx, -112(%rbp)
	leaq	-96(%rbp), %rdx
	movq	%rcx, -120(%rbp)
	movq	%rax, -104(%rbp)
	movq	$0, -2160(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1482
	call	_ZdlPv@PLT
.L1482:
	movq	-2344(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1480
	.p2align 4,,10
	.p2align 3
.L1561:
	movq	-2392(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %rsi
	movq	%r13, %rdi
	movabsq	$290208414817519367, %rax
	leaq	-120(%rbp), %rdx
	movaps	%xmm0, -2176(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -2160(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2288(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1478
	call	_ZdlPv@PLT
.L1478:
	leaq	-512(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	$0, -2160(%rbp)
	movq	%rax, %rdi
	movq	%rax, -2328(%rbp)
	movaps	%xmm0, -2176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1479
	call	_ZdlPv@PLT
.L1479:
	movq	-2368(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1477
	.p2align 4,,10
	.p2align 3
.L1560:
	movq	-2304(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -2248(%rbp)
	leaq	-2208(%rbp), %rbx
	movq	$0, -2240(%rbp)
	leaq	-128(%rbp), %r14
	movq	$0, -2232(%rbp)
	movq	$0, -2224(%rbp)
	movq	$0, -2216(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2272(%rbp), %rdi
	leaq	-2216(%rbp), %r9
	leaq	-2232(%rbp), %rcx
	leaq	-2224(%rbp), %r8
	leaq	-2240(%rbp), %rdx
	leaq	-2248(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntES4_NS0_7Uint32TES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_PNS7_IS5_EESD_
	movl	$152, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2224(%rbp), %r8
	movq	%r13, %r9
	movq	-2232(%rbp), %rcx
	movq	-2240(%rbp), %rdx
	movq	-2248(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal28MutableBigIntAbsoluteAdd_265EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6BigIntEEES8_NS4_INS0_7Uint32TEEEPNS1_18CodeAssemblerLabelE
	movq	-2248(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	-2232(%rbp), %rdx
	movq	-2240(%rbp), %rcx
	movq	%rax, %r8
	movaps	%xmm0, -2208(%rbp)
	movq	%rsi, -128(%rbp)
	movq	-2216(%rbp), %rsi
	movq	-2224(%rbp), %rax
	movq	%rdx, -112(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%r14, %rsi
	movq	%rdx, -80(%rbp)
	leaq	-56(%rbp), %rdx
	movq	%rcx, -120(%rbp)
	movq	%rax, -104(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%rax, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	$0, -2192(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2296(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1474
	call	_ZdlPv@PLT
.L1474:
	movq	-2376(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1664(%rbp), %rax
	cmpq	$0, -2168(%rbp)
	movq	%rax, -2288(%rbp)
	jne	.L1569
.L1475:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L1473
	.p2align 4,,10
	.p2align 3
.L1569:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-64(%rbp), %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	-2232(%rbp), %xmm0
	movq	-2216(%rbp), %xmm1
	movq	$0, -2192(%rbp)
	movq	-2248(%rbp), %xmm2
	movhps	-2224(%rbp), %xmm0
	movhps	-2240(%rbp), %xmm1
	movhps	-2240(%rbp), %xmm2
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -2208(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2288(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1476
	call	_ZdlPv@PLT
.L1476:
	movq	-2392(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1475
.L1568:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22486:
	.size	_ZN2v88internal17BigIntAddImpl_266EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6BigIntEEES8_PNS1_18CodeAssemblerLabelE, .-_ZN2v88internal17BigIntAddImpl_266EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6BigIntEEES8_PNS1_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal25BigIntAddNoThrowAssembler28GenerateBigIntAddNoThrowImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25BigIntAddNoThrowAssembler28GenerateBigIntAddNoThrowImplEv
	.type	_ZN2v88internal25BigIntAddNoThrowAssembler28GenerateBigIntAddNoThrowImplEv, @function
_ZN2v88internal25BigIntAddNoThrowAssembler28GenerateBigIntAddNoThrowImplEv:
.LFB22510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-824(%rbp), %r14
	leaq	-248(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-880(%rbp), %rbx
	subq	$1080, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, -1080(%rbp)
	movq	%rax, -1048(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, -1088(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	leaq	-1048(%rbp), %r12
	movq	%rax, -1104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$72, %edi
	movq	$0, -872(%rbp)
	movq	$0, -864(%rbp)
	movq	%rax, %r13
	movq	-1048(%rbp), %rax
	movq	$0, -856(%rbp)
	movq	%rax, -880(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -856(%rbp)
	movq	%rdx, -864(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -840(%rbp)
	movq	%rax, -872(%rbp)
	movq	$0, -848(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1048(%rbp), %rax
	movl	$120, %edi
	movq	$0, -680(%rbp)
	movq	$0, -672(%rbp)
	movq	%rax, -688(%rbp)
	movq	$0, -664(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -680(%rbp)
	leaq	-632(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -664(%rbp)
	movq	%rdx, -672(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -648(%rbp)
	movq	%rax, -1072(%rbp)
	movq	$0, -656(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1048(%rbp), %rax
	movl	$144, %edi
	movq	$0, -488(%rbp)
	movq	$0, -480(%rbp)
	movq	%rax, -496(%rbp)
	movq	$0, -472(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -488(%rbp)
	leaq	-440(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -472(%rbp)
	movq	%rdx, -480(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -456(%rbp)
	movq	%rax, -1064(%rbp)
	movq	$0, -464(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1048(%rbp), %rax
	movl	$72, %edi
	movq	$0, -296(%rbp)
	movq	$0, -288(%rbp)
	movq	%rax, -304(%rbp)
	movq	$0, -280(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -280(%rbp)
	movq	%rdx, -288(%rbp)
	xorl	%edx, %edx
	movq	%rax, -296(%rbp)
	movups	%xmm0, -264(%rbp)
	movq	$0, -272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	-1088(%rbp), %xmm1
	movq	%r13, -96(%rbp)
	leaq	-1008(%rbp), %r13
	movhps	-1104(%rbp), %xmm1
	movaps	%xmm0, -1008(%rbp)
	movaps	%xmm1, -112(%rbp)
	movq	$0, -992(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm2
	leaq	24(%rax), %rdx
	movq	%rax, -1008(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -992(%rbp)
	movq	%rdx, -1000(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1008(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1571
	call	_ZdlPv@PLT
.L1571:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -816(%rbp)
	jne	.L1662
	cmpq	$0, -624(%rbp)
	jne	.L1663
.L1577:
	cmpq	$0, -432(%rbp)
	jne	.L1664
.L1580:
	cmpq	$0, -240(%rbp)
	jne	.L1665
.L1582:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1584
	call	_ZdlPv@PLT
.L1584:
	movq	-288(%rbp), %rbx
	movq	-296(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1585
	.p2align 4,,10
	.p2align 3
.L1589:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1586
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1589
.L1587:
	movq	-296(%rbp), %r13
.L1585:
	testq	%r13, %r13
	je	.L1590
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1590:
	movq	-1064(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-464(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1591
	call	_ZdlPv@PLT
.L1591:
	movq	-480(%rbp), %rbx
	movq	-488(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1592
	.p2align 4,,10
	.p2align 3
.L1596:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1593
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1596
.L1594:
	movq	-488(%rbp), %r13
.L1592:
	testq	%r13, %r13
	je	.L1597
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1597:
	movq	-1072(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1598
	call	_ZdlPv@PLT
.L1598:
	movq	-672(%rbp), %rbx
	movq	-680(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1599
	.p2align 4,,10
	.p2align 3
.L1603:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1600
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1603
.L1601:
	movq	-680(%rbp), %r13
.L1599:
	testq	%r13, %r13
	je	.L1604
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1604:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-848(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1605
	call	_ZdlPv@PLT
.L1605:
	movq	-864(%rbp), %rbx
	movq	-872(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1606
	.p2align 4,,10
	.p2align 3
.L1610:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1607
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1610
.L1608:
	movq	-872(%rbp), %r13
.L1606:
	testq	%r13, %r13
	je	.L1611
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1611:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1666
	addq	$1080, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1607:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1610
	jmp	.L1608
	.p2align 4,,10
	.p2align 3
.L1600:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1603
	jmp	.L1601
	.p2align 4,,10
	.p2align 3
.L1586:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1589
	jmp	.L1587
	.p2align 4,,10
	.p2align 3
.L1593:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1596
	jmp	.L1594
	.p2align 4,,10
	.p2align 3
.L1662:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -992(%rbp)
	movaps	%xmm0, -1008(%rbp)
	call	_Znwm@PLT
	movl	$1799, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%rbx, %rdi
	movb	$7, 2(%rax)
	movq	%rax, -1008(%rbp)
	movq	%rdx, -992(%rbp)
	movq	%rdx, -1000(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1008(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1573
	call	_ZdlPv@PLT
.L1573:
	movq	(%rbx), %rax
	movl	$166, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	%rcx, -1104(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -1088(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1104(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %r8
	movq	-1088(%rbp), %rcx
	movq	-1080(%rbp), %rdi
	call	_ZN2v88internal17BigIntAddImpl_266EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6BigIntEEES8_PNS1_18CodeAssemblerLabelE
	movq	-1088(%rbp), %rcx
	movq	%rbx, %xmm4
	pxor	%xmm0, %xmm0
	movhps	-1104(%rbp), %xmm4
	movl	$48, %edi
	movq	%rax, -72(%rbp)
	leaq	-1040(%rbp), %rbx
	movq	%rcx, %xmm3
	movq	%rcx, -80(%rbp)
	movhps	-1104(%rbp), %xmm3
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm3, -1120(%rbp)
	movaps	%xmm4, -1104(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -1040(%rbp)
	movq	$0, -1024(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm5
	movq	%rbx, %rsi
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	leaq	48(%rax), %rdx
	leaq	-496(%rbp), %rdi
	movq	%rax, -1040(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -1024(%rbp)
	movq	%rdx, -1032(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1040(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1574
	call	_ZdlPv@PLT
.L1574:
	movq	-1064(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1000(%rbp)
	jne	.L1667
.L1575:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -624(%rbp)
	je	.L1577
.L1663:
	movq	-1072(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-688(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -992(%rbp)
	movaps	%xmm0, -1008(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -1008(%rbp)
	movq	%rdx, -992(%rbp)
	movq	%rdx, -1000(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1008(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1578
	call	_ZdlPv@PLT
.L1578:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm5
	movaps	%xmm0, -1008(%rbp)
	movq	$0, -992(%rbp)
	movq	%rdx, -96(%rbp)
	movaps	%xmm5, -112(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-112(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-304(%rbp), %rdi
	movq	%rax, -1008(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -992(%rbp)
	movq	%rdx, -1000(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1008(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1579
	call	_ZdlPv@PLT
.L1579:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -432(%rbp)
	je	.L1580
.L1664:
	movq	-1064(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-496(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -992(%rbp)
	movaps	%xmm0, -1008(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movq	%rbx, %rdi
	movw	%si, 4(%rax)
	leaq	6(%rax), %rdx
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	movq	%rax, -1008(%rbp)
	movq	%rdx, -992(%rbp)
	movq	%rdx, -1000(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1008(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1581
	call	_ZdlPv@PLT
.L1581:
	movq	(%rbx), %rax
	movq	-1080(%rbp), %rsi
	movq	%r13, %rdi
	movq	40(%rax), %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -240(%rbp)
	je	.L1582
.L1665:
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	-304(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -992(%rbp)
	movaps	%xmm0, -1008(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -1008(%rbp)
	movq	%rdx, -992(%rbp)
	movq	%rdx, -1000(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1008(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1583
	call	_ZdlPv@PLT
.L1583:
	movl	$170, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1080(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal37Convert5ATSmi17ATconstexpr_int31_1406EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	-1080(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L1582
	.p2align 4,,10
	.p2align 3
.L1667:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-112(%rbp), %rsi
	movq	%rbx, %rdi
	movdqa	-1104(%rbp), %xmm7
	movq	-1088(%rbp), %rax
	leaq	-72(%rbp), %rdx
	movaps	%xmm0, -1040(%rbp)
	movq	$0, -1024(%rbp)
	movaps	%xmm7, -112(%rbp)
	movdqa	-1120(%rbp), %xmm7
	movq	%rax, -80(%rbp)
	movaps	%xmm7, -96(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-688(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1040(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1576
	call	_ZdlPv@PLT
.L1576:
	movq	-1072(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1575
.L1666:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22510:
	.size	_ZN2v88internal25BigIntAddNoThrowAssembler28GenerateBigIntAddNoThrowImplEv, .-_ZN2v88internal25BigIntAddNoThrowAssembler28GenerateBigIntAddNoThrowImplEv
	.section	.rodata._ZN2v88internal8Builtins25Generate_BigIntAddNoThrowEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/bigint-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins25Generate_BigIntAddNoThrowEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"BigIntAddNoThrow"
	.section	.text._ZN2v88internal8Builtins25Generate_BigIntAddNoThrowEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins25Generate_BigIntAddNoThrowEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins25Generate_BigIntAddNoThrowEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins25Generate_BigIntAddNoThrowEPNS0_8compiler18CodeAssemblerStateE:
.LFB22506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1160, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$788, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L1672
.L1669:
	movq	%r13, %rdi
	call	_ZN2v88internal25BigIntAddNoThrowAssembler28GenerateBigIntAddNoThrowImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1673
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1672:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L1669
.L1673:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22506:
	.size	_ZN2v88internal8Builtins25Generate_BigIntAddNoThrowEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins25Generate_BigIntAddNoThrowEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal18BigIntAddAssembler21GenerateBigIntAddImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18BigIntAddAssembler21GenerateBigIntAddImplEv
	.type	_ZN2v88internal18BigIntAddAssembler21GenerateBigIntAddImplEv, @function
_ZN2v88internal18BigIntAddAssembler21GenerateBigIntAddImplEv:
.LFB22525:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1800(%rbp), %r15
	leaq	-1608(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1984(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$2168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, -2048(%rbp)
	movq	%rax, -2024(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -2064(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	leaq	-2024(%rbp), %r12
	movq	%rax, -2080(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$72, %edi
	movq	$0, -1848(%rbp)
	movq	$0, -1840(%rbp)
	movq	%rax, %rbx
	movq	-2024(%rbp), %rax
	movq	$0, -1832(%rbp)
	movq	%rax, -1856(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1848(%rbp)
	leaq	-1856(%rbp), %rax
	movq	%rdx, -1832(%rbp)
	movq	%rdx, -1840(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1816(%rbp)
	movq	%rax, -2040(%rbp)
	movq	$0, -1824(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2024(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1656(%rbp)
	movq	$0, -1648(%rbp)
	movq	%rax, -1664(%rbp)
	movq	$0, -1640(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -1640(%rbp)
	movq	%rdx, -1648(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1624(%rbp)
	movq	%rax, -1656(%rbp)
	movq	$0, -1632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2024(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1464(%rbp)
	movq	$0, -1456(%rbp)
	movq	%rax, -1472(%rbp)
	movq	$0, -1448(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1416(%rbp), %rcx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -1448(%rbp)
	movq	%rdx, -1456(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2144(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1432(%rbp)
	movq	%rax, -1464(%rbp)
	movq	$0, -1440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2024(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1272(%rbp)
	movq	$0, -1264(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -1256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1224(%rbp), %rcx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -1256(%rbp)
	movq	%rdx, -1264(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2192(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1240(%rbp)
	movq	%rax, -1272(%rbp)
	movq	$0, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2024(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1080(%rbp)
	movq	$0, -1072(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$0, -1064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1032(%rbp), %rcx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rdx, -1064(%rbp)
	movq	%rdx, -1072(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2160(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1048(%rbp)
	movq	%rax, -1080(%rbp)
	movq	$0, -1056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2024(%rbp), %rax
	movl	$168, %edi
	movq	$0, -888(%rbp)
	movq	$0, -880(%rbp)
	movq	%rax, -896(%rbp)
	movq	$0, -872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-840(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -872(%rbp)
	movq	%rdx, -880(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2176(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -856(%rbp)
	movq	%rax, -888(%rbp)
	movq	$0, -864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2024(%rbp), %rax
	movl	$192, %edi
	movq	$0, -696(%rbp)
	movq	$0, -688(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-648(%rbp), %rcx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -680(%rbp)
	movq	%rdx, -688(%rbp)
	movq	%rcx, -2128(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movups	%xmm0, -664(%rbp)
	movq	%rax, -696(%rbp)
	movq	$0, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2024(%rbp), %rax
	movl	$72, %edi
	movq	$0, -504(%rbp)
	movq	$0, -496(%rbp)
	movq	%rax, -512(%rbp)
	movq	$0, -488(%rbp)
	call	_Znwm@PLT
	leaq	-456(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -488(%rbp)
	movq	%rdx, -496(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2120(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -472(%rbp)
	movq	%rax, -504(%rbp)
	movq	$0, -480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2024(%rbp), %rax
	movl	$72, %edi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -296(%rbp)
	call	_Znwm@PLT
	leaq	-264(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2168(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -312(%rbp)
	movups	%xmm0, -280(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	-2064(%rbp), %xmm1
	movaps	%xmm0, -1984(%rbp)
	movhps	-2080(%rbp), %xmm1
	movq	%rbx, -112(%rbp)
	movaps	%xmm1, -128(%rbp)
	movq	$0, -1968(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movdqa	-128(%rbp), %xmm2
	movq	%r13, %rsi
	movq	-2040(%rbp), %rdi
	leaq	24(%rax), %rdx
	movq	%rax, -1984(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -1968(%rbp)
	movq	%rdx, -1976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1675
	call	_ZdlPv@PLT
.L1675:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1472(%rbp), %rax
	cmpq	$0, -1792(%rbp)
	movq	%rax, -2104(%rbp)
	leaq	-1664(%rbp), %rax
	movq	%rax, -2088(%rbp)
	jne	.L1773
.L1676:
	leaq	-512(%rbp), %rax
	cmpq	$0, -1600(%rbp)
	movq	%rax, -2064(%rbp)
	jne	.L1774
.L1681:
	leaq	-1088(%rbp), %rax
	cmpq	$0, -1408(%rbp)
	movq	%rax, -2112(%rbp)
	leaq	-1280(%rbp), %rax
	movq	%rax, -2096(%rbp)
	jne	.L1775
.L1684:
	cmpq	$0, -1216(%rbp)
	jne	.L1776
.L1689:
	leaq	-896(%rbp), %rax
	cmpq	$0, -1024(%rbp)
	leaq	-704(%rbp), %r14
	movq	%rax, -2080(%rbp)
	jne	.L1777
.L1692:
	cmpq	$0, -832(%rbp)
	leaq	-320(%rbp), %r15
	jne	.L1778
.L1697:
	cmpq	$0, -640(%rbp)
	jne	.L1779
.L1700:
	cmpq	$0, -448(%rbp)
	jne	.L1780
	cmpq	$0, -256(%rbp)
	jne	.L1781
.L1704:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2064(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2080(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2112(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2096(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2104(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2088(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2040(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1782
	addq	$2168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1773:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1968(%rbp)
	movaps	%xmm0, -1984(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r9d
	movq	-2040(%rbp), %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -1984(%rbp)
	movq	%rdx, -1968(%rbp)
	movq	%rdx, -1976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1984(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1677
	call	_ZdlPv@PLT
.L1677:
	movq	(%rbx), %rax
	movl	$177, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %r15
	movq	(%rax), %rbx
	movq	16(%rax), %rax
	movq	%rax, -2064(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	-2048(%rbp), %rdi
	call	_ZN2v88internal18Cast8ATBigInt_1456EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	movq	%r15, %xmm4
	movq	%r15, %xmm5
	movq	-2064(%rbp), %xmm3
	leaq	-2016(%rbp), %r15
	pxor	%xmm0, %xmm0
	leaq	-88(%rbp), %rdx
	movq	%rax, -96(%rbp)
	punpcklqdq	%xmm4, %xmm3
	movq	%rbx, %xmm4
	leaq	-128(%rbp), %rbx
	movq	%r15, %rdi
	punpcklqdq	%xmm5, %xmm4
	movq	%rbx, %rsi
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm3, -2064(%rbp)
	movaps	%xmm4, -2080(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm0, -2016(%rbp)
	movq	$0, -2000(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1472(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -2104(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2016(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1678
	call	_ZdlPv@PLT
.L1678:
	movq	-2144(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1664(%rbp), %rax
	cmpq	$0, -1976(%rbp)
	movq	%rax, -2088(%rbp)
	jne	.L1783
.L1679:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L1676
	.p2align 4,,10
	.p2align 3
.L1780:
	movq	-2120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1968(%rbp)
	movaps	%xmm0, -1984(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	-2064(%rbp), %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$8, 2(%rax)
	movq	%rax, -1984(%rbp)
	movq	%rdx, -1968(%rbp)
	movq	%rdx, -1976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1984(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1703
	call	_ZdlPv@PLT
.L1703:
	movq	(%rbx), %rax
	movl	$183, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2048(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$21, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -256(%rbp)
	je	.L1704
.L1781:
	movq	-2168(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1968(%rbp)
	movaps	%xmm0, -1984(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -1984(%rbp)
	movq	%rdx, -1968(%rbp)
	movq	%rdx, -1976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1984(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1705
	call	_ZdlPv@PLT
.L1705:
	movq	(%rbx), %rax
	movl	$186, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2048(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$183, %edx
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler15ThrowRangeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L1704
	.p2align 4,,10
	.p2align 3
.L1779:
	movq	-2128(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %rsi
	movq	%r13, %rdi
	movabsq	$506381209866602503, %rax
	leaq	-120(%rbp), %rdx
	movaps	%xmm0, -1984(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -1968(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1984(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1701
	call	_ZdlPv@PLT
.L1701:
	movq	(%rbx), %rax
	movq	-2048(%rbp), %rsi
	movq	%r13, %rdi
	movq	56(%rax), %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L1700
	.p2align 4,,10
	.p2align 3
.L1778:
	movq	-2176(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-128(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-121(%rbp), %rdx
	movq	%rbx, %rsi
	movl	$1799, %edi
	movaps	%xmm0, -1984(%rbp)
	movw	%di, -124(%rbp)
	movq	%r13, %rdi
	movl	$117966855, -128(%rbp)
	movb	$7, -122(%rbp)
	movq	$0, -1968(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2080(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1984(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L1698
	call	_ZdlPv@PLT
.L1698:
	movq	(%r15), %rax
	leaq	-104(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	leaq	-320(%rbp), %r15
	movdqu	(%rax), %xmm0
	movq	16(%rax), %rax
	movq	$0, -1968(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -112(%rbp)
	movaps	%xmm0, -1984(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1699
	call	_ZdlPv@PLT
.L1699:
	movq	-2168(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1697
	.p2align 4,,10
	.p2align 3
.L1777:
	movq	-2160(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-128(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-122(%rbp), %rdx
	movq	%rbx, %rsi
	movl	$1800, %r8d
	movq	%r13, %rdi
	movaps	%xmm0, -1984(%rbp)
	movw	%r8w, -124(%rbp)
	movl	$117966855, -128(%rbp)
	movq	$0, -1968(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2112(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1984(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L1693
	call	_ZdlPv@PLT
.L1693:
	movq	(%r14), %rax
	movl	$180, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	24(%rax), %r15
	movq	%rcx, -2080(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -2160(%rbp)
	movq	40(%rax), %rcx
	movq	%rcx, -2144(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %r8
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	-2144(%rbp), %rcx
	movq	-2048(%rbp), %rdi
	call	_ZN2v88internal17BigIntAddImpl_266EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6BigIntEEES8_PNS1_18CodeAssemblerLabelE
	pxor	%xmm0, %xmm0
	leaq	-64(%rbp), %rdx
	movq	%rbx, %rsi
	movq	-2144(%rbp), %rcx
	movq	%r15, %xmm6
	movq	%rax, -72(%rbp)
	movq	%r14, %xmm3
	movq	-2160(%rbp), %xmm2
	leaq	-2016(%rbp), %r15
	movhps	-2080(%rbp), %xmm3
	leaq	-704(%rbp), %r14
	movq	%rcx, %xmm7
	movq	%r15, %rdi
	movq	%rcx, -80(%rbp)
	punpcklqdq	%xmm6, %xmm7
	punpcklqdq	%xmm6, %xmm2
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm7, -2208(%rbp)
	movaps	%xmm2, -2160(%rbp)
	movaps	%xmm3, -2192(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm0, -2016(%rbp)
	movq	$0, -2000(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2016(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1694
	call	_ZdlPv@PLT
.L1694:
	movq	-2128(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-896(%rbp), %rax
	cmpq	$0, -1976(%rbp)
	movq	%rax, -2080(%rbp)
	jne	.L1784
.L1695:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L1692
	.p2align 4,,10
	.p2align 3
.L1776:
	movq	-2192(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-128(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-123(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movaps	%xmm0, -1984(%rbp)
	movl	$117966855, -128(%rbp)
	movb	$8, -124(%rbp)
	movq	$0, -1968(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2096(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1984(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L1690
	call	_ZdlPv@PLT
.L1690:
	movq	(%r14), %rax
	leaq	-104(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movdqu	(%rax), %xmm0
	movq	16(%rax), %rax
	movq	$0, -1968(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -112(%rbp)
	movaps	%xmm0, -1984(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2064(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1691
	call	_ZdlPv@PLT
.L1691:
	movq	-2120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1689
	.p2align 4,,10
	.p2align 3
.L1775:
	movq	-2144(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-128(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-123(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movaps	%xmm0, -1984(%rbp)
	movl	$134744071, -128(%rbp)
	movb	$7, -124(%rbp)
	movq	$0, -1968(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2104(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1984(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L1685
	call	_ZdlPv@PLT
.L1685:
	movq	(%r14), %rax
	movl	$178, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r15
	movq	16(%rax), %r14
	movq	8(%rax), %rcx
	movq	32(%rax), %rax
	movq	%rcx, -2080(%rbp)
	movq	%rax, -2096(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%r14, %rdx
	movq	-2048(%rbp), %rdi
	call	_ZN2v88internal18Cast8ATBigInt_1456EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %xmm6
	movq	%r14, %xmm5
	leaq	-2016(%rbp), %r15
	movq	%rax, -88(%rbp)
	movhps	-2096(%rbp), %xmm5
	movq	%r15, %rdi
	movq	%r14, -96(%rbp)
	movhps	-2080(%rbp), %xmm6
	movaps	%xmm5, -2144(%rbp)
	movaps	%xmm6, -2080(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm0, -2016(%rbp)
	movq	$0, -2000(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1088(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -2112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2016(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1686
	call	_ZdlPv@PLT
.L1686:
	movq	-2160(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1280(%rbp), %rax
	cmpq	$0, -1976(%rbp)
	movq	%rax, -2096(%rbp)
	jne	.L1785
.L1687:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L1684
	.p2align 4,,10
	.p2align 3
.L1774:
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	-128(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-124(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movaps	%xmm0, -1984(%rbp)
	movl	$134744071, -128(%rbp)
	movq	$0, -1968(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2088(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1984(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L1682
	call	_ZdlPv@PLT
.L1682:
	movq	(%r14), %rax
	leaq	-104(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movdqu	(%rax), %xmm0
	movq	16(%rax), %rax
	movq	$0, -1968(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -112(%rbp)
	movaps	%xmm0, -1984(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-512(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -2064(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1683
	call	_ZdlPv@PLT
.L1683:
	movq	-2120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1681
	.p2align 4,,10
	.p2align 3
.L1783:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-96(%rbp), %rdx
	movq	%rbx, %rsi
	movdqa	-2080(%rbp), %xmm7
	movq	%r15, %rdi
	movaps	%xmm0, -2016(%rbp)
	movq	$0, -2000(%rbp)
	movaps	%xmm7, -128(%rbp)
	movdqa	-2064(%rbp), %xmm7
	movaps	%xmm7, -112(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2088(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2016(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1680
	call	_ZdlPv@PLT
.L1680:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1679
	.p2align 4,,10
	.p2align 3
.L1785:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-88(%rbp), %rdx
	movq	%rbx, %rsi
	movdqa	-2080(%rbp), %xmm7
	movq	%r15, %rdi
	movaps	%xmm0, -2016(%rbp)
	movq	%r14, -96(%rbp)
	movaps	%xmm7, -128(%rbp)
	movdqa	-2144(%rbp), %xmm7
	movq	$0, -2000(%rbp)
	movaps	%xmm7, -112(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2096(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2016(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1688
	call	_ZdlPv@PLT
.L1688:
	movq	-2192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1687
	.p2align 4,,10
	.p2align 3
.L1784:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-2192(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movdqa	-2160(%rbp), %xmm6
	movdqa	-2208(%rbp), %xmm7
	movq	-2144(%rbp), %rax
	movq	%rbx, %rsi
	movq	%r15, %rdi
	leaq	-72(%rbp), %rdx
	movaps	%xmm5, -128(%rbp)
	movq	%rax, -80(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm0, -2016(%rbp)
	movq	$0, -2000(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2080(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2016(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1696
	call	_ZdlPv@PLT
.L1696:
	movq	-2176(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1695
.L1782:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22525:
	.size	_ZN2v88internal18BigIntAddAssembler21GenerateBigIntAddImplEv, .-_ZN2v88internal18BigIntAddAssembler21GenerateBigIntAddImplEv
	.section	.rodata._ZN2v88internal8Builtins18Generate_BigIntAddEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"BigIntAdd"
	.section	.text._ZN2v88internal8Builtins18Generate_BigIntAddEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins18Generate_BigIntAddEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins18Generate_BigIntAddEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins18Generate_BigIntAddEPNS0_8compiler18CodeAssemblerStateE:
.LFB22521:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1225, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$789, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L1790
.L1787:
	movq	%r13, %rdi
	call	_ZN2v88internal18BigIntAddAssembler21GenerateBigIntAddImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1791
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1790:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L1787
.L1791:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22521:
	.size	_ZN2v88internal8Builtins18Generate_BigIntAddEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins18Generate_BigIntAddEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntENS0_7IntPtrTES4_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESB_SD_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntENS0_7IntPtrTES4_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESB_SD_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntENS0_7IntPtrTES4_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESB_SD_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntENS0_7IntPtrTES4_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESB_SD_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntENS0_7IntPtrTES4_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESB_SD_:
.LFB26970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$5, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$56, %rsp
	movq	%r8, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$117769991, (%rax)
	leaq	5(%rax), %rdx
	movb	$5, 4(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1793
	movq	%rax, -96(%rbp)
	call	_ZdlPv@PLT
	movq	-96(%rbp), %rax
.L1793:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1794
	movq	%rdx, (%r15)
.L1794:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1795
	movq	%rdx, (%r14)
.L1795:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1796
	movq	%rdx, 0(%r13)
.L1796:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1797
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1797:
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L1792
	movq	%rax, (%rbx)
.L1792:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1819
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1819:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26970:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntENS0_7IntPtrTES4_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESB_SD_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntENS0_7IntPtrTES4_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESB_SD_
	.section	.text._ZN2v88internal25BigIntUnaryMinusAssembler28GenerateBigIntUnaryMinusImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25BigIntUnaryMinusAssembler28GenerateBigIntUnaryMinusImplEv
	.type	_ZN2v88internal25BigIntUnaryMinusAssembler28GenerateBigIntUnaryMinusImplEv, @function
_ZN2v88internal25BigIntUnaryMinusAssembler28GenerateBigIntUnaryMinusImplEv:
.LFB22555:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1440(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1520(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1608, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, -1520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	-1472(%rbp), %r12
	movq	%rax, -1600(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -1432(%rbp)
	movq	%rax, -1616(%rbp)
	movq	-1520(%rbp), %rax
	movq	$0, -1424(%rbp)
	movq	%rax, -1440(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1432(%rbp)
	leaq	-1384(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -1528(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1520(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -1544(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1520(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1560(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1520(%rbp), %rax
	movl	$120, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1536(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1520(%rbp), %rax
	movl	$120, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1568(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1520(%rbp), %rax
	movl	$120, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -1552(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1520(%rbp), %rax
	movl	$120, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -280(%rbp)
	leaq	-232(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -1576(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1600(%rbp), %xmm1
	movaps	%xmm0, -1472(%rbp)
	movhps	-1616(%rbp), %xmm1
	movq	$0, -1456(%rbp)
	movaps	%xmm1, -1600(%rbp)
	call	_Znwm@PLT
	movdqa	-1600(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%r12, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1472(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1456(%rbp)
	movq	%rdx, -1464(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1472(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1821
	call	_ZdlPv@PLT
.L1821:
	movq	-1528(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1376(%rbp)
	jne	.L1965
	cmpq	$0, -1184(%rbp)
	jne	.L1966
.L1826:
	cmpq	$0, -992(%rbp)
	jne	.L1967
.L1828:
	cmpq	$0, -800(%rbp)
	jne	.L1968
.L1831:
	cmpq	$0, -608(%rbp)
	jne	.L1969
.L1834:
	cmpq	$0, -416(%rbp)
	jne	.L1970
.L1836:
	cmpq	$0, -224(%rbp)
	jne	.L1971
.L1838:
	movq	-1576(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1839
	call	_ZdlPv@PLT
.L1839:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1840
	.p2align 4,,10
	.p2align 3
.L1844:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1841
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1844
.L1842:
	movq	-280(%rbp), %r12
.L1840:
	testq	%r12, %r12
	je	.L1845
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1845:
	movq	-1552(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1846
	call	_ZdlPv@PLT
.L1846:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1847
	.p2align 4,,10
	.p2align 3
.L1851:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1848
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1851
.L1849:
	movq	-472(%rbp), %r12
.L1847:
	testq	%r12, %r12
	je	.L1852
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1852:
	movq	-1568(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1853
	call	_ZdlPv@PLT
.L1853:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1854
	.p2align 4,,10
	.p2align 3
.L1858:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1855
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1858
.L1856:
	movq	-664(%rbp), %r12
.L1854:
	testq	%r12, %r12
	je	.L1859
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1859:
	movq	-1536(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1860
	call	_ZdlPv@PLT
.L1860:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1861
	.p2align 4,,10
	.p2align 3
.L1865:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1862
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1865
.L1863:
	movq	-856(%rbp), %r12
.L1861:
	testq	%r12, %r12
	je	.L1866
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1866:
	movq	-1560(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1867
	call	_ZdlPv@PLT
.L1867:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1868
	.p2align 4,,10
	.p2align 3
.L1872:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1869
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1872
.L1870:
	movq	-1048(%rbp), %r12
.L1868:
	testq	%r12, %r12
	je	.L1873
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1873:
	movq	-1544(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1874
	call	_ZdlPv@PLT
.L1874:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1875
	.p2align 4,,10
	.p2align 3
.L1879:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1876
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1879
.L1877:
	movq	-1240(%rbp), %r12
.L1875:
	testq	%r12, %r12
	je	.L1880
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1880:
	movq	-1528(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1881
	call	_ZdlPv@PLT
.L1881:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1882
	.p2align 4,,10
	.p2align 3
.L1886:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1883
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1886
.L1884:
	movq	-1432(%rbp), %r12
.L1882:
	testq	%r12, %r12
	je	.L1887
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1887:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1972
	addq	$1608, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1883:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1886
	jmp	.L1884
	.p2align 4,,10
	.p2align 3
.L1876:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1879
	jmp	.L1877
	.p2align 4,,10
	.p2align 3
.L1869:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1872
	jmp	.L1870
	.p2align 4,,10
	.p2align 3
.L1862:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1865
	jmp	.L1863
	.p2align 4,,10
	.p2align 3
.L1855:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1858
	jmp	.L1856
	.p2align 4,,10
	.p2align 3
.L1841:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1844
	jmp	.L1842
	.p2align 4,,10
	.p2align 3
.L1848:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1851
	jmp	.L1849
	.p2align 4,,10
	.p2align 3
.L1965:
	movq	-1528(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-96(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movl	$1799, %ecx
	movq	%rax, %rsi
	leaq	-94(%rbp), %rdx
	movq	%rax, -1600(%rbp)
	movw	%cx, -96(%rbp)
	movaps	%xmm0, -1472(%rbp)
	movq	$0, -1456(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1472(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1823
	call	_ZdlPv@PLT
.L1823:
	movq	(%rbx), %rax
	movl	$191, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rcx, -1616(%rbp)
	movq	%rax, %rbx
	movq	%rax, -1584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadBigIntBitfieldENS0_8compiler5TNodeINS0_6BigIntEEE@PLT
	movl	$2147483646, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12DecodeWord32ENS0_8compiler11SloppyTNodeINS0_7Word32TEEEjj@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$194, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-72(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	-1616(%rbp), %xmm2
	movq	-1600(%rbp), %rsi
	movaps	%xmm0, -1472(%rbp)
	movq	%rbx, -80(%rbp)
	movhps	-1584(%rbp), %xmm2
	movq	$0, -1456(%rbp)
	movq	%rdx, -1584(%rbp)
	movaps	%xmm2, -1616(%rbp)
	movaps	%xmm2, -96(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1248(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1472(%rbp), %rdi
	movq	-1584(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L1824
	call	_ZdlPv@PLT
	movq	-1584(%rbp), %rdx
.L1824:
	movdqa	-1616(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rbx, -80(%rbp)
	movq	-1600(%rbp), %rsi
	movaps	%xmm0, -1472(%rbp)
	movaps	%xmm3, -96(%rbp)
	movq	$0, -1456(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1056(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1472(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1825
	call	_ZdlPv@PLT
.L1825:
	movq	-1560(%rbp), %rcx
	movq	-1544(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1184(%rbp)
	je	.L1826
.L1966:
	movq	-1544(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-1248(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movl	$1799, %edx
	movb	$5, -94(%rbp)
	movw	%dx, -96(%rbp)
	leaq	-93(%rbp), %rdx
	movaps	%xmm0, -1472(%rbp)
	movq	$0, -1456(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1472(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1827
	call	_ZdlPv@PLT
.L1827:
	movq	(%rbx), %rax
	movl	$195, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	movq	8(%rax), %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -992(%rbp)
	je	.L1828
.L1967:
	movq	-1560(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-1056(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-93(%rbp), %rdx
	movq	%r12, %rdi
	movl	$1799, %eax
	movb	$5, -94(%rbp)
	movw	%ax, -96(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -1600(%rbp)
	movaps	%xmm0, -1472(%rbp)
	movq	$0, -1456(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1472(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1829
	call	_ZdlPv@PLT
.L1829:
	movq	(%rbx), %rax
	movl	$199, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r15
	movq	%rcx, -1616(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1616(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadBigIntBitfieldENS0_8compiler5TNodeINS0_6BigIntEEE@PLT
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler12DecodeWord32ENS0_8compiler11SloppyTNodeINS0_7Word32TEEEjj@PLT
	movq	%r12, %rdi
	movq	%rax, -1584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1584(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14InvertSign_260EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7Uint32TEEE
	movq	%r15, %rcx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal23AllocateEmptyBigInt_262EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7Uint32TEEENS4_INS0_7IntPtrTEEE
	movl	$200, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, -1584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %xmm0
	leaq	-56(%rbp), %rdx
	movq	%r12, %rdi
	movhps	-1616(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movq	-1600(%rbp), %rsi
	movaps	%xmm0, -96(%rbp)
	movq	%r15, %xmm0
	movhps	-1584(%rbp), %xmm0
	movq	$0, -1456(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1472(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-864(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1472(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1830
	call	_ZdlPv@PLT
.L1830:
	movq	-1536(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L1831
.L1968:
	movq	-1536(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-864(%rbp), %r15
	movq	$0, -1512(%rbp)
	movq	$0, -1504(%rbp)
	movq	$0, -1496(%rbp)
	movq	$0, -1488(%rbp)
	movq	$0, -1480(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1480(%rbp), %r9
	movq	%r15, %rdi
	leaq	-1488(%rbp), %r8
	leaq	-1496(%rbp), %rcx
	leaq	-1504(%rbp), %rdx
	leaq	-1512(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntENS0_7IntPtrTES4_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESB_SD_
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1496(%rbp), %rdx
	movq	-1480(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, -1616(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1512(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	-1488(%rbp), %rbx
	movq	-1496(%rbp), %rsi
	movq	-1504(%rbp), %rcx
	movq	%r12, %rdi
	movaps	%xmm0, -1472(%rbp)
	movq	%rax, -1584(%rbp)
	movq	-1480(%rbp), %r15
	movq	%rbx, -1624(%rbp)
	movq	%rax, -96(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rbx, -72(%rbp)
	leaq	-56(%rbp), %rbx
	movq	%rsi, -1632(%rbp)
	movq	%rbx, %rdx
	movq	%rsi, -80(%rbp)
	movq	%rax, %rsi
	movq	%rcx, -1640(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%rax, -1600(%rbp)
	movq	%r15, -64(%rbp)
	movq	$0, -1456(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-672(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1472(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1832
	call	_ZdlPv@PLT
.L1832:
	movq	-1584(%rbp), %xmm0
	movq	-1600(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%r15, -64(%rbp)
	movhps	-1640(%rbp), %xmm0
	movq	$0, -1456(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	-1632(%rbp), %xmm0
	movhps	-1624(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1472(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-288(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1472(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1833
	call	_ZdlPv@PLT
.L1833:
	movq	-1576(%rbp), %rcx
	movq	-1568(%rbp), %rdx
	movq	%r13, %rdi
	movq	-1616(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -608(%rbp)
	je	.L1834
.L1969:
	movq	-1568(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-672(%rbp), %r15
	movq	$0, -1512(%rbp)
	movq	$0, -1504(%rbp)
	movq	$0, -1496(%rbp)
	movq	$0, -1488(%rbp)
	movq	$0, -1480(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1480(%rbp), %r9
	leaq	-1488(%rbp), %r8
	movq	%r15, %rdi
	leaq	-1496(%rbp), %rcx
	leaq	-1504(%rbp), %rdx
	leaq	-1512(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntENS0_7IntPtrTES4_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESB_SD_
	movl	$201, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1480(%rbp), %rdx
	movq	-1504(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler15LoadBigIntDigitENS0_8compiler5TNodeINS0_6BigIntEEENS3_INS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1480(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	-1488(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler16StoreBigIntDigitENS0_8compiler5TNodeINS0_6BigIntEEENS3_INS0_7IntPtrTEEENS3_INS0_8UintPtrTEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$200, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm0, %xmm0
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movq	-1512(%rbp), %rax
	leaq	-56(%rbp), %rdx
	movaps	%xmm0, -1472(%rbp)
	movq	$0, -1456(%rbp)
	movq	%rax, -96(%rbp)
	movq	-1504(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-1496(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-1488(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-1480(%rbp), %rax
	movq	%rax, -64(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-480(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1472(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1835
	call	_ZdlPv@PLT
.L1835:
	movq	-1552(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -416(%rbp)
	je	.L1836
.L1970:
	movq	-1552(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-480(%rbp), %r15
	movq	$0, -1512(%rbp)
	movq	$0, -1504(%rbp)
	movq	$0, -1496(%rbp)
	movq	$0, -1488(%rbp)
	movq	$0, -1480(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1496(%rbp), %rcx
	movq	%r15, %rdi
	leaq	-1480(%rbp), %r9
	leaq	-1488(%rbp), %r8
	leaq	-1504(%rbp), %rdx
	leaq	-1512(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntENS0_7IntPtrTES4_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESB_SD_
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1480(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-96(%rbp), %rsi
	leaq	-56(%rbp), %rdx
	movq	%r12, %rdi
	movq	-1496(%rbp), %xmm0
	movq	%rbx, -64(%rbp)
	movq	-1512(%rbp), %xmm1
	movq	$0, -1456(%rbp)
	movhps	-1488(%rbp), %xmm0
	movhps	-1504(%rbp), %xmm1
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -1472(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-864(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1472(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1837
	call	_ZdlPv@PLT
.L1837:
	movq	-1536(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -224(%rbp)
	je	.L1838
.L1971:
	movq	-1576(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-288(%rbp), %r15
	movq	$0, -1512(%rbp)
	movq	$0, -1504(%rbp)
	movq	$0, -1496(%rbp)
	movq	$0, -1488(%rbp)
	movq	$0, -1480(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1496(%rbp), %rcx
	movq	%r15, %rdi
	leaq	-1480(%rbp), %r9
	leaq	-1488(%rbp), %r8
	leaq	-1504(%rbp), %rdx
	leaq	-1512(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6BigIntENS0_7IntPtrTES4_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESB_SD_
	movl	$203, %edx
	movq	%r13, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1488(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal35Convert8ATBigInt13MutableBigInt_256EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L1838
.L1972:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22555:
	.size	_ZN2v88internal25BigIntUnaryMinusAssembler28GenerateBigIntUnaryMinusImplEv, .-_ZN2v88internal25BigIntUnaryMinusAssembler28GenerateBigIntUnaryMinusImplEv
	.section	.rodata._ZN2v88internal8Builtins25Generate_BigIntUnaryMinusEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"BigIntUnaryMinus"
	.section	.text._ZN2v88internal8Builtins25Generate_BigIntUnaryMinusEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins25Generate_BigIntUnaryMinusEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins25Generate_BigIntUnaryMinusEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins25Generate_BigIntUnaryMinusEPNS0_8compiler18CodeAssemblerStateE:
.LFB22551:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1363, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$790, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L1977
.L1974:
	movq	%r13, %rdi
	call	_ZN2v88internal25BigIntUnaryMinusAssembler28GenerateBigIntUnaryMinusImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1978
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1977:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L1974
.L1978:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22551:
	.size	_ZN2v88internal8Builtins25Generate_BigIntUnaryMinusEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins25Generate_BigIntUnaryMinusEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal35Convert8ATBigInt13MutableBigInt_256EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal35Convert8ATBigInt13MutableBigInt_256EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE, @function
_GLOBAL__sub_I__ZN2v88internal35Convert8ATBigInt13MutableBigInt_256EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE:
.LFB29496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29496:
	.size	_GLOBAL__sub_I__ZN2v88internal35Convert8ATBigInt13MutableBigInt_256EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE, .-_GLOBAL__sub_I__ZN2v88internal35Convert8ATBigInt13MutableBigInt_256EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal35Convert8ATBigInt13MutableBigInt_256EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6BigIntEEE
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
