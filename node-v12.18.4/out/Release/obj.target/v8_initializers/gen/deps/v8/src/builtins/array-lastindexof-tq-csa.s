	.file	"array-lastindexof-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0:
.LFB29906:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	%rsi, %r13
	pushq	%rbx
	movq	%r13, %rax
	sarq	$3, %rax
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%r13, %r13
	js	.L14
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%rdx, %r14
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.L9
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%rax, %rdi
.L9:
	leaq	(%rdi,%r13), %r15
	movq	%rdi, (%rbx)
	movq	%r15, 16(%rbx)
	cmpq	%r12, %r14
	je	.L10
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
.L10:
	movq	%r15, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29906:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0:
.LFB29905:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	subq	%rsi, %rdx
	js	.L21
	movq	%rdi, %rbx
	je	.L17
	movq	%rdx, %rdi
	movq	%rdx, %r12
	movq	%rsi, %r13
	call	_Znwm@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	leaq	(%rax,%r12), %r14
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	%r14, 16(%rbx)
	call	memcpy@PLT
	movq	%r14, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	xorl	%r14d, %r14d
	movq	$0, (%rdi)
	movq	$0, 16(%rdi)
	movq	%r14, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29905:
	.size	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev,"axG",@progbits,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev:
.LFB22413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$56, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L23
	call	_ZdlPv@PLT
.L23:
	movq	16(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	%r12, %r13
	je	.L24
	.p2align 4,,10
	.p2align 3
.L28:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L25
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L28
.L26:
	movq	8(%rbx), %r12
.L24:
	testq	%r12, %r12
	je	.L22
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L28
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L22:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22413:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev
	.set	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.section	.rodata._ZN2v88internal32LoadWithHoleCheck10FixedArray_19EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../../deps/v8/../../deps/v8/src/builtins/array-lastindexof.tq"
	.align 8
.LC3:
	.string	"../../deps/v8/../../deps/v8/src/builtins/torque-internal.tq"
	.section	.text._ZN2v88internal32LoadWithHoleCheck10FixedArray_19EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32LoadWithHoleCheck10FixedArray_19EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal32LoadWithHoleCheck10FixedArray_19EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal32LoadWithHoleCheck10FixedArray_19EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE:
.LFB22410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	-2536(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-2440(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$2776, %rsp
	movq	%rdx, -2624(%rbp)
	movq	%r8, -2680(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -2536(%rbp)
	movq	%rdi, -2496(%rbp)
	movl	$72, %edi
	movq	$0, -2488(%rbp)
	movq	$0, -2480(%rbp)
	movq	$0, -2472(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r12, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -2488(%rbp)
	leaq	-2496(%rbp), %rax
	movq	%rdx, -2472(%rbp)
	movq	%rdx, -2480(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2456(%rbp)
	movq	%rax, -2552(%rbp)
	movq	$0, -2464(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2536(%rbp), %rax
	movl	$360, %edi
	movq	$0, -2296(%rbp)
	movq	$0, -2288(%rbp)
	movq	%rax, -2304(%rbp)
	movq	$0, -2280(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2248(%rbp), %rcx
	movq	%r15, %rsi
	leaq	360(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 352(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movq	%rdx, -2280(%rbp)
	movq	%rdx, -2288(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2560(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2264(%rbp)
	movq	%rax, -2296(%rbp)
	movq	$0, -2272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2536(%rbp), %rax
	movl	$360, %edi
	movq	$0, -2104(%rbp)
	movq	$0, -2096(%rbp)
	movq	%rax, -2112(%rbp)
	movq	$0, -2088(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2056(%rbp), %rcx
	movq	%r15, %rsi
	leaq	360(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 352(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movq	%rdx, -2088(%rbp)
	movq	%rdx, -2096(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2584(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2072(%rbp)
	movq	%rax, -2104(%rbp)
	movq	$0, -2080(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2536(%rbp), %rax
	movl	$408, %edi
	movq	$0, -1912(%rbp)
	movq	$0, -1904(%rbp)
	movq	%rax, -1920(%rbp)
	movq	$0, -1896(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1864(%rbp), %rcx
	movq	%r15, %rsi
	leaq	408(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 400(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movq	%rdx, -1896(%rbp)
	movq	%rdx, -1904(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2608(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1880(%rbp)
	movq	%rax, -1912(%rbp)
	movq	$0, -1888(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2536(%rbp), %rax
	movl	$240, %edi
	movq	$0, -1720(%rbp)
	movq	$0, -1712(%rbp)
	movq	%rax, -1728(%rbp)
	movq	$0, -1704(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1672(%rbp), %rcx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rdx, -1704(%rbp)
	movq	%rdx, -1712(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2656(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1688(%rbp)
	movq	%rax, -1720(%rbp)
	movq	$0, -1696(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2536(%rbp), %rax
	movl	$288, %edi
	movq	$0, -1528(%rbp)
	movq	$0, -1520(%rbp)
	movq	%rax, -1536(%rbp)
	movq	$0, -1512(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1480(%rbp), %rcx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rdx, -1512(%rbp)
	movq	%rdx, -1520(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2576(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1496(%rbp)
	movq	%rax, -1528(%rbp)
	movq	$0, -1504(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2536(%rbp), %rax
	movl	$264, %edi
	movq	$0, -1336(%rbp)
	movq	$0, -1328(%rbp)
	movq	%rax, -1344(%rbp)
	movq	$0, -1320(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1288(%rbp), %rcx
	movq	%r15, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rdx, -1320(%rbp)
	movq	%rdx, -1328(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2600(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1304(%rbp)
	movq	%rax, -1336(%rbp)
	movq	$0, -1312(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2536(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1144(%rbp)
	movq	$0, -1136(%rbp)
	movq	%rax, -1152(%rbp)
	movq	$0, -1128(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1096(%rbp), %rcx
	movq	%r15, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -1128(%rbp)
	movq	%rdx, -1136(%rbp)
	movq	%rcx, -2672(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movups	%xmm0, -1112(%rbp)
	movq	%rax, -1144(%rbp)
	movq	$0, -1120(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2536(%rbp), %rax
	movl	$120, %edi
	movq	$0, -952(%rbp)
	movq	$0, -944(%rbp)
	movq	%rax, -960(%rbp)
	movq	$0, -936(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-904(%rbp), %rcx
	movq	%r15, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -936(%rbp)
	movq	%rdx, -944(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2568(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -920(%rbp)
	movq	%rax, -952(%rbp)
	movq	$0, -928(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2536(%rbp), %rax
	movl	$96, %edi
	movq	$0, -760(%rbp)
	movq	$0, -752(%rbp)
	movq	%rax, -768(%rbp)
	movq	$0, -744(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-712(%rbp), %rcx
	movq	%r15, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -744(%rbp)
	movq	%rdx, -752(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2592(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -728(%rbp)
	movq	%rax, -760(%rbp)
	movq	$0, -736(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	-2536(%rbp), %rax
	leaq	-520(%rbp), %rcx
	movq	$0, -568(%rbp)
	movq	%rcx, %rdi
	movq	%rcx, -2648(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -576(%rbp)
	movq	$0, -560(%rbp)
	movq	$0, -552(%rbp)
	movq	$0, -544(%rbp)
	movq	$0, -536(%rbp)
	movq	$0, -528(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2536(%rbp), %rax
	movl	$96, %edi
	movq	$0, -376(%rbp)
	movq	$0, -368(%rbp)
	movq	%rax, -384(%rbp)
	movq	$0, -360(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-328(%rbp), %rcx
	movq	%r15, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -360(%rbp)
	movq	%rdx, -368(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2640(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -376(%rbp)
	movups	%xmm0, -344(%rbp)
	movq	$0, -352(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2624(%rbp), %r9
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	%r14, -192(%rbp)
	leaq	-2528(%rbp), %r14
	movq	%r9, -184(%rbp)
	movaps	%xmm0, -2528(%rbp)
	movq	%rbx, -176(%rbp)
	movq	$0, -2512(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rcx
	movdqa	-192(%rbp), %xmm5
	movq	%r14, %rsi
	movq	-2552(%rbp), %rdi
	leaq	24(%rax), %rdx
	movq	%rax, -2528(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L35
	call	_ZdlPv@PLT
.L35:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2432(%rbp)
	jne	.L242
	cmpq	$0, -2240(%rbp)
	jne	.L243
.L40:
	leaq	-1728(%rbp), %rax
	cmpq	$0, -2048(%rbp)
	movq	%rax, -2632(%rbp)
	jne	.L244
	cmpq	$0, -1856(%rbp)
	jne	.L245
.L46:
	cmpq	$0, -1664(%rbp)
	jne	.L246
.L49:
	cmpq	$0, -1472(%rbp)
	jne	.L247
.L51:
	leaq	-1152(%rbp), %rax
	cmpq	$0, -1280(%rbp)
	movq	%rax, -2624(%rbp)
	jne	.L248
.L54:
	cmpq	$0, -1088(%rbp)
	leaq	-576(%rbp), %r12
	jne	.L249
	cmpq	$0, -896(%rbp)
	jne	.L250
.L61:
	cmpq	$0, -704(%rbp)
	leaq	-384(%rbp), %r13
	jne	.L251
	cmpq	$0, -512(%rbp)
	jne	.L252
.L68:
	movq	-2640(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2512(%rbp)
	movaps	%xmm0, -2528(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	$134612743, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2528(%rbp)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2528(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L70
	call	_ZdlPv@PLT
.L70:
	movq	(%rbx), %rax
	movq	%r13, %rdi
	movq	24(%rax), %r14
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2592(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-736(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L71
	call	_ZdlPv@PLT
.L71:
	movq	-752(%rbp), %rbx
	movq	-760(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L72
	.p2align 4,,10
	.p2align 3
.L76:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L73
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L76
.L74:
	movq	-760(%rbp), %r12
.L72:
	testq	%r12, %r12
	je	.L77
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L77:
	movq	-2568(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-928(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L78
	call	_ZdlPv@PLT
.L78:
	movq	-944(%rbp), %rbx
	movq	-952(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L79
	.p2align 4,,10
	.p2align 3
.L83:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L80
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L83
.L81:
	movq	-952(%rbp), %r12
.L79:
	testq	%r12, %r12
	je	.L84
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L84:
	movq	-2624(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2600(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L85
	call	_ZdlPv@PLT
.L85:
	movq	-1328(%rbp), %rbx
	movq	-1336(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L86
	.p2align 4,,10
	.p2align 3
.L90:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L87
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L90
.L88:
	movq	-1336(%rbp), %r12
.L86:
	testq	%r12, %r12
	je	.L91
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L91:
	movq	-2576(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1504(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L92
	call	_ZdlPv@PLT
.L92:
	movq	-1520(%rbp), %rbx
	movq	-1528(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L93
	.p2align 4,,10
	.p2align 3
.L97:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L94
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L97
.L95:
	movq	-1528(%rbp), %r12
.L93:
	testq	%r12, %r12
	je	.L98
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L98:
	movq	-2632(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2608(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1888(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L99
	call	_ZdlPv@PLT
.L99:
	movq	-1904(%rbp), %rbx
	movq	-1912(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L100
	.p2align 4,,10
	.p2align 3
.L104:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L101
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L104
.L102:
	movq	-1912(%rbp), %r12
.L100:
	testq	%r12, %r12
	je	.L105
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L105:
	movq	-2584(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2080(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L106
	call	_ZdlPv@PLT
.L106:
	movq	-2096(%rbp), %rbx
	movq	-2104(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L107
	.p2align 4,,10
	.p2align 3
.L111:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L108
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L111
.L109:
	movq	-2104(%rbp), %r12
.L107:
	testq	%r12, %r12
	je	.L112
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L112:
	movq	-2560(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L113
	call	_ZdlPv@PLT
.L113:
	movq	-2288(%rbp), %rbx
	movq	-2296(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L114
	.p2align 4,,10
	.p2align 3
.L118:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L115
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L118
.L116:
	movq	-2296(%rbp), %r12
.L114:
	testq	%r12, %r12
	je	.L119
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L119:
	movq	-2552(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L253
	addq	$2776, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L118
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L108:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L111
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L101:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L104
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L94:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L97
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L87:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L90
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L73:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L76
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L80:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L83
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L242:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2512(%rbp)
	movaps	%xmm0, -2528(%rbp)
	call	_Znwm@PLT
	movl	$1799, %edi
	movq	%r14, %rsi
	movw	%di, (%rax)
	movq	-2552(%rbp), %rdi
	leaq	3(%rax), %rdx
	movb	$6, 2(%rax)
	movq	%rax, -2528(%rbp)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2528(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L37
	call	_ZdlPv@PLT
.L37:
	movq	(%rbx), %rax
	movl	$13, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %r12
	movq	16(%rax), %rax
	movq	%r12, -2752(%rbp)
	movq	%rax, -2624(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rbx, -2736(%rbp)
	call	_ZN2v88internal27UnsafeCast10FixedArray_1409EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$14, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -2632(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$8, %esi
	movq	%r15, %rdi
	movq	%rax, -2720(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-2632(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$55, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -2704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2624(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$56, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$37, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	-2704(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -2688(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2688(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-192(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -2688(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-72(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	-2624(%rbp), %xmm2
	movq	%r14, %rdi
	movq	-2632(%rbp), %xmm3
	movq	%rdx, -2776(%rbp)
	movq	-2736(%rbp), %xmm7
	movq	-2720(%rbp), %rax
	movaps	%xmm0, -2528(%rbp)
	movdqa	%xmm2, %xmm6
	movdqa	%xmm2, %xmm1
	movq	%xmm3, -112(%rbp)
	punpcklqdq	%xmm3, %xmm6
	movhps	-2752(%rbp), %xmm7
	movq	%rax, -104(%rbp)
	movaps	%xmm6, -2768(%rbp)
	movaps	%xmm6, -176(%rbp)
	movq	-2704(%rbp), %xmm6
	movaps	%xmm7, -2752(%rbp)
	movdqa	%xmm6, %xmm2
	movaps	%xmm7, -192(%rbp)
	movq	%rbx, %xmm7
	movhps	-2624(%rbp), %xmm2
	punpcklqdq	%xmm7, %xmm1
	movq	%rbx, -80(%rbp)
	movq	%xmm3, -160(%rbp)
	movdqa	%xmm6, %xmm3
	punpcklqdq	%xmm7, %xmm3
	movq	%rax, -152(%rbp)
	movaps	%xmm1, -2736(%rbp)
	movaps	%xmm2, -2704(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm3, -2624(%rbp)
	movaps	%xmm3, -96(%rbp)
	movq	$0, -2512(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2304(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2528(%rbp), %rdi
	movq	-2776(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L38
	call	_ZdlPv@PLT
	movq	-2776(%rbp), %rdx
.L38:
	movq	-2632(%rbp), %xmm0
	movdqa	-2752(%rbp), %xmm3
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqa	-2768(%rbp), %xmm4
	movdqa	-2704(%rbp), %xmm7
	movq	%rbx, -80(%rbp)
	movhps	-2720(%rbp), %xmm0
	movdqa	-2736(%rbp), %xmm6
	movdqa	-2624(%rbp), %xmm2
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -2528(%rbp)
	movq	$0, -2512(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2112(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L39
	call	_ZdlPv@PLT
.L39:
	movq	-2584(%rbp), %rcx
	movq	-2560(%rbp), %rdx
	movq	%r15, %rdi
	movq	-2688(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2240(%rbp)
	je	.L40
.L243:
	movq	-2560(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-2304(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$15, %edi
	movq	$0, -2512(%rbp)
	movaps	%xmm0, -2528(%rbp)
	call	_Znwm@PLT
	movl	$1285, %esi
	movq	%r12, %rdi
	movabsq	$433758466851866375, %rcx
	movw	%si, 12(%rax)
	leaq	15(%rax), %rdx
	movq	%r14, %rsi
	movq	%rcx, (%rax)
	movl	$84346118, 8(%rax)
	movb	$5, 14(%rax)
	movq	%rax, -2528(%rbp)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2528(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L41
	call	_ZdlPv@PLT
.L41:
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	(%rax), %rcx
	movq	24(%rax), %rbx
	movq	72(%rax), %r10
	movq	%rsi, -2704(%rbp)
	movq	16(%rax), %rsi
	movq	96(%rax), %r11
	movq	%rdx, -2752(%rbp)
	movq	%rdi, -2776(%rbp)
	movq	48(%rax), %rdx
	movq	64(%rax), %rdi
	movq	%rcx, -2632(%rbp)
	movq	%rbx, -2688(%rbp)
	movq	88(%rax), %rcx
	movq	32(%rax), %rbx
	movq	%r10, -2792(%rbp)
	movq	104(%rax), %r10
	movq	%rsi, -2720(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%rdx, -2768(%rbp)
	movl	$39, %edx
	movq	80(%rax), %r12
	movq	%rdi, -2784(%rbp)
	movq	%r15, %rdi
	movq	%r11, -2800(%rbp)
	movq	%r10, -2808(%rbp)
	movq	%rcx, -2624(%rbp)
	movq	%rbx, -2736(%rbp)
	movq	112(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -2816(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2816(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrMulENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -2816(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2816(%rbp), %rdx
	movq	-2624(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -2816(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$38, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-2816(%rbp), %rcx
	call	_ZN2v88internal45UnsafeNewReference20UT5ATSmi10HeapObject_1411EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	leaq	-56(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rbx, -80(%rbp)
	movq	-2632(%rbp), %xmm0
	movq	-2528(%rbp), %rax
	leaq	-192(%rbp), %rsi
	movq	$0, -2512(%rbp)
	movhps	-2704(%rbp), %xmm0
	movq	%rax, -72(%rbp)
	movq	-2520(%rbp), %rax
	movaps	%xmm0, -192(%rbp)
	movq	-2720(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movhps	-2688(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-2736(%rbp), %xmm0
	movhps	-2752(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-2768(%rbp), %xmm0
	movhps	-2776(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-2784(%rbp), %xmm0
	movhps	-2792(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%r12, %xmm0
	movhps	-2624(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-2800(%rbp), %xmm0
	movhps	-2808(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2528(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1920(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L42
	call	_ZdlPv@PLT
.L42:
	movq	-2608(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L244:
	movq	-2584(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-2112(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$15, %edi
	movq	$0, -2512(%rbp)
	movaps	%xmm0, -2528(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movabsq	$433758466851866375, %rcx
	movq	%rcx, (%rax)
	movl	$1285, %ecx
	leaq	15(%rax), %rdx
	movl	$84346118, 8(%rax)
	movw	%cx, 12(%rax)
	movb	$5, 14(%rax)
	movq	%rax, -2528(%rbp)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2528(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L44
	call	_ZdlPv@PLT
.L44:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rbx
	movq	8(%rax), %rsi
	movq	40(%rax), %rdx
	movq	(%rax), %rcx
	movq	%rbx, -2720(%rbp)
	movq	32(%rax), %rbx
	movq	%rsi, -2632(%rbp)
	movq	16(%rax), %rsi
	movq	%rdx, -2736(%rbp)
	movq	56(%rax), %rdx
	movq	48(%rax), %r12
	movq	%rbx, -2688(%rbp)
	movq	64(%rax), %rbx
	movq	72(%rax), %rax
	movq	%rsi, -2704(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%rdx, -2752(%rbp)
	movl	$41, %edx
	movq	%rcx, -2624(%rbp)
	movq	%rax, -2768(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-192(%rbp), %rsi
	leaq	-112(%rbp), %rdx
	movq	%r14, %rdi
	movq	-2624(%rbp), %xmm0
	movq	$0, -2512(%rbp)
	movhps	-2632(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-2704(%rbp), %xmm0
	movhps	-2720(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-2688(%rbp), %xmm0
	movhps	-2736(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%r12, %xmm0
	movhps	-2752(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	movhps	-2768(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2528(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1728(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -2632(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L45
	call	_ZdlPv@PLT
.L45:
	movq	-2656(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1856(%rbp)
	je	.L46
.L245:
	movq	-2608(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-192(%rbp), %r12
	leaq	-1920(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC4(%rip), %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-175(%rbp), %rdx
	movb	$5, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2528(%rbp)
	movq	$0, -2512(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2528(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L47
	call	_ZdlPv@PLT
.L47:
	movq	(%rbx), %rax
	movq	56(%rax), %rdi
	movq	24(%rax), %rbx
	movq	8(%rax), %rsi
	movq	40(%rax), %rdx
	movq	%rdi, -2776(%rbp)
	movq	72(%rax), %rdi
	movq	%rbx, -2688(%rbp)
	movq	32(%rax), %rbx
	movq	%rsi, -2704(%rbp)
	movq	16(%rax), %rsi
	movq	%rdx, -2752(%rbp)
	movq	48(%rax), %rdx
	movq	%rdi, -2784(%rbp)
	movq	120(%rax), %rdi
	movq	(%rax), %rcx
	movq	%rbx, -2736(%rbp)
	movq	64(%rax), %rbx
	movq	128(%rax), %rax
	movq	%rsi, -2720(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%rdx, -2768(%rbp)
	movl	$56, %edx
	movq	%rdi, -2792(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -2624(%rbp)
	movq	%rax, -2800(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-96(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-2624(%rbp), %xmm0
	movq	$0, -2512(%rbp)
	movhps	-2704(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-2720(%rbp), %xmm0
	movhps	-2688(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-2736(%rbp), %xmm0
	movhps	-2752(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-2768(%rbp), %xmm0
	movhps	-2776(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	movhps	-2784(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-2792(%rbp), %xmm0
	movhps	-2800(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2528(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1536(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L48
	call	_ZdlPv@PLT
.L48:
	movq	-2576(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1664(%rbp)
	je	.L49
.L246:
	movq	-2656(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1286, %edx
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movw	%dx, -184(%rbp)
	leaq	-192(%rbp), %rsi
	movabsq	$433758466851866375, %rax
	leaq	-182(%rbp), %rdx
	movq	%rax, -192(%rbp)
	movaps	%xmm0, -2528(%rbp)
	movq	$0, -2512(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2632(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L50
	call	_ZdlPv@PLT
.L50:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -1472(%rbp)
	je	.L51
.L247:
	movq	-2576(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-192(%rbp), %r12
	leaq	-1536(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-180(%rbp), %rdx
	movabsq	$433758466851866375, %rax
	movaps	%xmm0, -2528(%rbp)
	movq	%rax, -192(%rbp)
	movl	$84346118, -184(%rbp)
	movq	$0, -2512(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2528(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L52
	call	_ZdlPv@PLT
.L52:
	movq	(%rbx), %rax
	leaq	-104(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqu	80(%rax), %xmm7
	movdqu	64(%rax), %xmm0
	movdqu	(%rax), %xmm4
	movdqu	16(%rax), %xmm3
	movdqu	32(%rax), %xmm2
	movdqu	48(%rax), %xmm1
	punpcklqdq	%xmm7, %xmm0
	movq	88(%rax), %rax
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -192(%rbp)
	movq	%rax, -112(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -2528(%rbp)
	movq	$0, -2512(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1344(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L53
	call	_ZdlPv@PLT
.L53:
	movq	-2600(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L249:
	movq	-2672(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2512(%rbp)
	movaps	%xmm0, -2528(%rbp)
	call	_Znwm@PLT
	movq	-2624(%rbp), %rdi
	movq	%r14, %rsi
	movl	$117835527, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2528(%rbp)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L59
	call	_ZdlPv@PLT
.L59:
	leaq	-576(%rbp), %r12
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	$0, -2512(%rbp)
	movq	%r12, %rdi
	movaps	%xmm0, -2528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L60
	call	_ZdlPv@PLT
.L60:
	movq	-2648(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -896(%rbp)
	je	.L61
.L250:
	movq	-2568(%rbp), %rsi
	leaq	-960(%rbp), %r8
	movq	%r15, %rdi
	xorl	%ebx, %ebx
	movq	%r8, -2672(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2512(%rbp)
	movaps	%xmm0, -2528(%rbp)
	call	_Znwm@PLT
	movq	-2672(%rbp), %r8
	movq	%r14, %rsi
	movl	$117835527, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%r8, %rdi
	movq	%rax, -2528(%rbp)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L62
	movq	%rax, -2672(%rbp)
	call	_ZdlPv@PLT
	movq	-2672(%rbp), %rax
.L62:
	movq	(%rax), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	(%rax), %r8
	movq	32(%rax), %rdx
	movq	16(%rax), %rcx
	testq	%rdx, %rdx
	movq	%r8, %xmm0
	movq	%r8, -2656(%rbp)
	cmovne	%rdx, %rbx
	movhps	8(%rax), %xmm0
	movl	$16, %edx
	movq	%rcx, -2704(%rbp)
	movaps	%xmm0, -2672(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2656(%rbp), %r8
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal107UnsafeCast90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_1412EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movdqa	-2672(%rbp), %xmm0
	movq	%r14, %rdi
	movq	-2704(%rbp), %rcx
	leaq	-192(%rbp), %rsi
	leaq	-160(%rbp), %rdx
	movq	%rax, -168(%rbp)
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rcx, -176(%rbp)
	movaps	%xmm0, -2528(%rbp)
	movq	$0, -2512(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-768(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L64
	call	_ZdlPv@PLT
.L64:
	movq	-2592(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L248:
	movq	-2600(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-192(%rbp), %r12
	leaq	-1344(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-181(%rbp), %rdx
	movaps	%xmm0, -2528(%rbp)
	movabsq	$433758466851866375, %rax
	movq	%rax, -192(%rbp)
	movl	$1798, %eax
	movw	%ax, -184(%rbp)
	movb	$5, -182(%rbp)
	movq	$0, -2512(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2528(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L55
	call	_ZdlPv@PLT
.L55:
	movq	(%rbx), %rax
	movl	$14, %edx
	movq	%r15, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	72(%rax), %r8
	movq	24(%rax), %rbx
	movq	%rsi, -2704(%rbp)
	movq	16(%rax), %rsi
	movq	%rcx, -2624(%rbp)
	movq	%rsi, -2720(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%r8, -2656(%rbp)
	movq	%rbx, -2688(%rbp)
	movq	80(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-2656(%rbp), %r8
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6ObjectEvE5valueE(%rip), %esi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$15, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal10TheHole_62EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -2656(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2656(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -2656(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-2720(%rbp), %xmm4
	leaq	-152(%rbp), %rdx
	movq	-2624(%rbp), %xmm5
	movaps	%xmm0, -2528(%rbp)
	movq	%rbx, -160(%rbp)
	movhps	-2688(%rbp), %xmm4
	movhps	-2704(%rbp), %xmm5
	movq	%rdx, -2688(%rbp)
	movaps	%xmm4, -2720(%rbp)
	movaps	%xmm5, -2704(%rbp)
	movaps	%xmm5, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	movq	$0, -2512(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1152(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -2624(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2528(%rbp), %rdi
	movq	-2688(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L56
	call	_ZdlPv@PLT
	movq	-2688(%rbp), %rdx
.L56:
	movdqa	-2704(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqa	-2720(%rbp), %xmm4
	movaps	%xmm0, -2528(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	movq	%rbx, -160(%rbp)
	movq	$0, -2512(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-960(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L57
	call	_ZdlPv@PLT
.L57:
	movq	-2568(%rbp), %rcx
	movq	-2672(%rbp), %rdx
	movq	%r15, %rdi
	movq	-2656(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L251:
	movq	-2592(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-768(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2512(%rbp)
	movaps	%xmm0, -2528(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	$134612743, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2528(%rbp)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2528(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L66
	call	_ZdlPv@PLT
.L66:
	movq	(%rbx), %rax
	movl	$10, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r13
	movq	16(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rcx, -2672(%rbp)
	movq	%rax, -2656(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %xmm0
	leaq	-192(%rbp), %rsi
	leaq	-160(%rbp), %rdx
	movhps	-2672(%rbp), %xmm0
	movq	%r14, %rdi
	leaq	-384(%rbp), %r13
	movq	$0, -2512(%rbp)
	movaps	%xmm0, -192(%rbp)
	movq	%rbx, %xmm0
	movhps	-2656(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2528(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L67
	call	_ZdlPv@PLT
.L67:
	movq	-2640(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -512(%rbp)
	je	.L68
.L252:
	movq	-2648(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	$0, -2512(%rbp)
	movaps	%xmm0, -2528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L69
	call	_ZdlPv@PLT
.L69:
	movq	-2680(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L68
.L253:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22410:
	.size	_ZN2v88internal32LoadWithHoleCheck10FixedArray_19EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE, .-_ZN2v88internal32LoadWithHoleCheck10FixedArray_19EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal38LoadWithHoleCheck16FixedDoubleArray_20EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal38LoadWithHoleCheck16FixedDoubleArray_20EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal38LoadWithHoleCheck16FixedDoubleArray_20EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal38LoadWithHoleCheck16FixedDoubleArray_20EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE:
.LFB22449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-248(%rbp), %r14
	leaq	-1264(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1432(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1512, %rsp
	movq	%rsi, -1520(%rbp)
	movq	%rcx, -1504(%rbp)
	movq	%r8, -1488(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1432(%rbp)
	movq	%rdi, -1264(%rbp)
	movl	$72, %edi
	movq	$0, -1256(%rbp)
	movq	$0, -1248(%rbp)
	movq	$0, -1240(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1256(%rbp)
	leaq	-1208(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1240(%rbp)
	movq	%rdx, -1248(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1224(%rbp)
	movq	%rax, -1448(%rbp)
	movq	$0, -1232(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1432(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1064(%rbp)
	movq	$0, -1056(%rbp)
	movq	%rax, -1072(%rbp)
	movq	$0, -1048(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1064(%rbp)
	leaq	-1016(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1048(%rbp)
	movq	%rdx, -1056(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1032(%rbp)
	movq	%rax, -1480(%rbp)
	movq	$0, -1040(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1432(%rbp), %rax
	movl	$168, %edi
	movq	$0, -872(%rbp)
	movq	$0, -864(%rbp)
	movq	%rax, -880(%rbp)
	movq	$0, -856(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -872(%rbp)
	leaq	-824(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -856(%rbp)
	movq	%rdx, -864(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -840(%rbp)
	movq	%rax, -1464(%rbp)
	movq	$0, -848(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1432(%rbp), %rax
	movl	$96, %edi
	movq	$0, -680(%rbp)
	movq	$0, -672(%rbp)
	movq	%rax, -688(%rbp)
	movq	$0, -664(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movups	%xmm0, (%rax)
	leaq	96(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -680(%rbp)
	leaq	-632(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -664(%rbp)
	movq	%rdx, -672(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -648(%rbp)
	movq	%rax, -1456(%rbp)
	movq	$0, -656(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1432(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	$0, -488(%rbp)
	movq	%rax, -496(%rbp)
	leaq	-440(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1472(%rbp)
	movq	$0, -480(%rbp)
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1432(%rbp), %rax
	movl	$96, %edi
	movq	$0, -296(%rbp)
	movq	$0, -288(%rbp)
	movq	%rax, -304(%rbp)
	movq	$0, -280(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -280(%rbp)
	movq	%rdx, -288(%rbp)
	xorl	%edx, %edx
	movq	%rax, -296(%rbp)
	movups	%xmm0, -264(%rbp)
	movq	$0, -272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1520(%rbp), %r9
	pxor	%xmm0, %xmm0
	movq	-1504(%rbp), %r10
	movl	$24, %edi
	movq	%r13, -104(%rbp)
	leaq	-1392(%rbp), %r13
	movq	%r9, -112(%rbp)
	movq	%r10, -96(%rbp)
	movaps	%xmm0, -1392(%rbp)
	movq	$0, -1376(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm1
	leaq	24(%rax), %rdx
	movq	%rax, -1392(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm1, (%rax)
	movq	%rdx, -1376(%rbp)
	movq	%rdx, -1384(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L255
	call	_ZdlPv@PLT
.L255:
	movq	-1448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1200(%rbp)
	jne	.L390
	cmpq	$0, -1008(%rbp)
	jne	.L391
.L261:
	cmpq	$0, -816(%rbp)
	jne	.L392
.L264:
	cmpq	$0, -624(%rbp)
	leaq	-304(%rbp), %r15
	jne	.L393
	cmpq	$0, -432(%rbp)
	jne	.L394
.L270:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1376(%rbp)
	movaps	%xmm0, -1392(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$134612743, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1392(%rbp)
	movq	%rdx, -1376(%rbp)
	movq	%rdx, -1384(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1392(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L272
	call	_ZdlPv@PLT
.L272:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	24(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L273
	call	_ZdlPv@PLT
.L273:
	movq	-288(%rbp), %rbx
	movq	-296(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L274
	.p2align 4,,10
	.p2align 3
.L278:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L275
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L278
.L276:
	movq	-296(%rbp), %r14
.L274:
	testq	%r14, %r14
	je	.L279
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L279:
	movq	-1472(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-464(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L280
	call	_ZdlPv@PLT
.L280:
	movq	-480(%rbp), %rbx
	movq	-488(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L281
	.p2align 4,,10
	.p2align 3
.L285:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L282
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L285
.L283:
	movq	-488(%rbp), %r14
.L281:
	testq	%r14, %r14
	je	.L286
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L286:
	movq	-1456(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L287
	call	_ZdlPv@PLT
.L287:
	movq	-672(%rbp), %rbx
	movq	-680(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L288
	.p2align 4,,10
	.p2align 3
.L292:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L289
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L292
.L290:
	movq	-680(%rbp), %r14
.L288:
	testq	%r14, %r14
	je	.L293
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L293:
	movq	-1464(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-848(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L294
	call	_ZdlPv@PLT
.L294:
	movq	-864(%rbp), %rbx
	movq	-872(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L295
	.p2align 4,,10
	.p2align 3
.L299:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L296
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L299
.L297:
	movq	-872(%rbp), %r14
.L295:
	testq	%r14, %r14
	je	.L300
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L300:
	movq	-1480(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1040(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L301
	call	_ZdlPv@PLT
.L301:
	movq	-1056(%rbp), %rbx
	movq	-1064(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L302
	.p2align 4,,10
	.p2align 3
.L306:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L303
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L306
.L304:
	movq	-1064(%rbp), %r14
.L302:
	testq	%r14, %r14
	je	.L307
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L307:
	movq	-1448(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L308
	call	_ZdlPv@PLT
.L308:
	movq	-1248(%rbp), %rbx
	movq	-1256(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L309
	.p2align 4,,10
	.p2align 3
.L313:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L310
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L313
.L311:
	movq	-1256(%rbp), %r14
.L309:
	testq	%r14, %r14
	je	.L314
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L314:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L395
	addq	$1512, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L310:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L313
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L303:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L306
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L296:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L299
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L289:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L292
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L275:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L278
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L282:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L285
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L390:
	movq	-1448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1376(%rbp)
	movaps	%xmm0, -1392(%rbp)
	call	_Znwm@PLT
	movl	$1799, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rdi
	movb	$6, 2(%rax)
	movq	%rax, -1392(%rbp)
	movq	%rdx, -1376(%rbp)
	movq	%rdx, -1384(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1392(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L257
	call	_ZdlPv@PLT
.L257:
	movq	(%r15), %rax
	movl	$22, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	16(%rax), %rax
	movq	%rcx, -1504(%rbp)
	movq	%rax, -1520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1504(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r15, -1544(%rbp)
	leaq	-1424(%rbp), %r15
	call	_ZN2v88internal33UnsafeCast16FixedDoubleArray_1413EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$23, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -1536(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1520(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r15, %rdi
	movq	-1536(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler23LoadDoubleWithHoleCheckENS0_8compiler5TNodeINS0_16FixedDoubleArrayEEENS3_INS0_3SmiEEEPNS2_18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	movq	%rax, -1552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1520(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	-1536(%rbp), %xmm5
	movq	-1544(%rbp), %xmm4
	movl	$56, %edi
	movaps	%xmm0, -1424(%rbp)
	movq	%rax, %xmm7
	movdqa	%xmm5, %xmm2
	movq	%rax, %xmm3
	movq	-1552(%rbp), %rax
	punpcklqdq	%xmm7, %xmm2
	punpcklqdq	%xmm5, %xmm3
	movhps	-1504(%rbp), %xmm4
	movq	$0, -1408(%rbp)
	movaps	%xmm2, -1520(%rbp)
	movaps	%xmm3, -1536(%rbp)
	movaps	%xmm4, -1504(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm2, -80(%rbp)
	movq	%rax, -64(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm6
	movq	-64(%rbp), %rcx
	leaq	-880(%rbp), %rdi
	movdqa	-96(%rbp), %xmm7
	leaq	56(%rax), %rdx
	movq	%r15, %rsi
	movq	%rax, -1424(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-80(%rbp), %xmm6
	movq	%rcx, 48(%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -1408(%rbp)
	movq	%rdx, -1416(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L258
	call	_ZdlPv@PLT
.L258:
	movq	-1464(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1384(%rbp)
	jne	.L396
.L259:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1008(%rbp)
	je	.L261
.L391:
	movq	-1480(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1072(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -1376(%rbp)
	movaps	%xmm0, -1392(%rbp)
	call	_Znwm@PLT
	movl	$1543, %esi
	movq	%r15, %rdi
	movw	%si, 4(%rax)
	leaq	6(%rax), %rdx
	movq	%r13, %rsi
	movl	$117835527, (%rax)
	movq	%rax, -1392(%rbp)
	movq	%rdx, -1376(%rbp)
	movq	%rdx, -1384(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L262
	call	_ZdlPv@PLT
.L262:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-496(%rbp), %rdi
	movq	$0, -1376(%rbp)
	movaps	%xmm0, -1392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L263
	call	_ZdlPv@PLT
.L263:
	movq	-1472(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -816(%rbp)
	je	.L264
.L392:
	movq	-1464(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-880(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -1376(%rbp)
	movaps	%xmm0, -1392(%rbp)
	call	_Znwm@PLT
	movl	$1543, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117835527, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$13, 6(%rax)
	movq	%rax, -1392(%rbp)
	movq	%rdx, -1376(%rbp)
	movq	%rdx, -1384(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1392(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L265
	call	_ZdlPv@PLT
.L265:
	movq	(%r15), %rax
	movl	$25, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	48(%rax), %r15
	movq	%rsi, -1520(%rbp)
	movq	16(%rax), %rsi
	movq	%rcx, -1504(%rbp)
	movq	%rsi, -1536(%rbp)
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler27AllocateHeapNumberWithValueENS0_8compiler11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -1544(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$32, %edi
	movq	-1504(%rbp), %xmm0
	movq	$0, -1376(%rbp)
	movhps	-1520(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-1536(%rbp), %xmm0
	movhps	-1544(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1392(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-688(%rbp), %rdi
	movq	%rax, -1392(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1376(%rbp)
	movq	%rdx, -1384(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L266
	call	_ZdlPv@PLT
.L266:
	movq	-1456(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L393:
	movq	-1456(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-688(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1376(%rbp)
	movaps	%xmm0, -1392(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$134612743, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1392(%rbp)
	movq	%rdx, -1376(%rbp)
	movq	%rdx, -1384(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1392(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L268
	call	_ZdlPv@PLT
.L268:
	movq	(%rbx), %rax
	movl	$19, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	16(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rcx, -1504(%rbp)
	movq	%rax, -1520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %xmm0
	movl	$32, %edi
	movq	$0, -1376(%rbp)
	movhps	-1504(%rbp), %xmm0
	leaq	-304(%rbp), %r15
	movaps	%xmm0, -112(%rbp)
	movq	%rbx, %xmm0
	movhps	-1520(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1392(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm6
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm7
	leaq	32(%rax), %rdx
	movq	%rax, -1392(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1376(%rbp)
	movq	%rdx, -1384(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L269
	call	_ZdlPv@PLT
.L269:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -432(%rbp)
	je	.L270
.L394:
	movq	-1472(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-496(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	$0, -1376(%rbp)
	movaps	%xmm0, -1392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L271
	call	_ZdlPv@PLT
.L271:
	movq	-1488(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L396:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-112(%rbp), %rsi
	movq	%r15, %rdi
	movdqa	-1504(%rbp), %xmm5
	movdqa	-1520(%rbp), %xmm6
	leaq	-64(%rbp), %rdx
	movaps	%xmm0, -1424(%rbp)
	movq	$0, -1408(%rbp)
	movaps	%xmm5, -112(%rbp)
	movdqa	-1536(%rbp), %xmm5
	movaps	%xmm6, -80(%rbp)
	movaps	%xmm5, -96(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1072(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L260
	call	_ZdlPv@PLT
.L260:
	movq	-1480(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L259
.L395:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22449:
	.size	_ZN2v88internal38LoadWithHoleCheck16FixedDoubleArray_20EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE, .-_ZN2v88internal38LoadWithHoleCheck16FixedDoubleArray_20EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal26GenericArrayLastIndexOf_23EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26GenericArrayLastIndexOf_23EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE
	.type	_ZN2v88internal26GenericArrayLastIndexOf_23EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE, @function
_ZN2v88internal26GenericArrayLastIndexOf_23EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE:
.LFB22491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-2048(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2080(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-2120(%rbp), %r12
	pushq	%rbx
	subq	$2264, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -2224(%rbp)
	movq	%rdx, -2240(%rbp)
	movq	%rcx, -2256(%rbp)
	movq	%r8, -2272(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -2120(%rbp)
	movq	%rdi, -2048(%rbp)
	movl	$96, %edi
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movq	$0, -2024(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -2040(%rbp)
	leaq	-1992(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2024(%rbp)
	movq	%rdx, -2032(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2008(%rbp)
	movq	%rax, -2136(%rbp)
	movq	$0, -2016(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2120(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1848(%rbp)
	movq	$0, -1840(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1832(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1848(%rbp)
	leaq	-1800(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1832(%rbp)
	movq	%rdx, -1840(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1816(%rbp)
	movq	%rax, -2160(%rbp)
	movq	$0, -1824(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2120(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1656(%rbp)
	movq	$0, -1648(%rbp)
	movq	%rax, -1664(%rbp)
	movq	$0, -1640(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1656(%rbp)
	leaq	-1608(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1640(%rbp)
	movq	%rdx, -1648(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1624(%rbp)
	movq	%rax, -2176(%rbp)
	movq	$0, -1632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2120(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1464(%rbp)
	movq	$0, -1456(%rbp)
	movq	%rax, -1472(%rbp)
	movq	$0, -1448(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1464(%rbp)
	leaq	-1416(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1448(%rbp)
	movq	%rdx, -1456(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1432(%rbp)
	movq	%rax, -2192(%rbp)
	movq	$0, -1440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2120(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1272(%rbp)
	movq	$0, -1264(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -1256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1272(%rbp)
	leaq	-1224(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1256(%rbp)
	movq	%rdx, -1264(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1240(%rbp)
	movq	%rax, -2200(%rbp)
	movq	$0, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2120(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1080(%rbp)
	movq	$0, -1072(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$0, -1064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1080(%rbp)
	leaq	-1032(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1064(%rbp)
	movq	%rdx, -1072(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1048(%rbp)
	movq	%rax, -2184(%rbp)
	movq	$0, -1056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2120(%rbp), %rax
	movl	$144, %edi
	movq	$0, -888(%rbp)
	movq	$0, -880(%rbp)
	movq	%rax, -896(%rbp)
	movq	$0, -872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -888(%rbp)
	leaq	-840(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -872(%rbp)
	movq	%rdx, -880(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -856(%rbp)
	movq	%rax, -2152(%rbp)
	movq	$0, -864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2120(%rbp), %rax
	movl	$120, %edi
	movq	$0, -696(%rbp)
	movq	$0, -688(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -696(%rbp)
	leaq	-648(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -680(%rbp)
	movq	%rdx, -688(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -664(%rbp)
	movq	%rax, -2208(%rbp)
	movq	$0, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2120(%rbp), %rax
	movl	$120, %edi
	movq	$0, -504(%rbp)
	movq	$0, -496(%rbp)
	movq	%rax, -512(%rbp)
	movq	$0, -488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -504(%rbp)
	leaq	-456(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -488(%rbp)
	movq	%rdx, -496(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -472(%rbp)
	movq	%rax, -2168(%rbp)
	movq	$0, -480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2120(%rbp), %rax
	movl	$120, %edi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -312(%rbp)
	leaq	-264(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	xorl	%edx, %edx
	movq	%rax, -2144(%rbp)
	movups	%xmm0, -280(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-2256(%rbp), %xmm1
	movq	-2224(%rbp), %xmm2
	movaps	%xmm0, -2080(%rbp)
	movhps	-2272(%rbp), %xmm1
	movq	$0, -2064(%rbp)
	movhps	-2240(%rbp), %xmm2
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -128(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm3
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm4
	leaq	32(%rax), %rdx
	movq	%rax, -2080(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2064(%rbp)
	movq	%rdx, -2072(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2080(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L398
	call	_ZdlPv@PLT
.L398:
	movq	-2136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1984(%rbp)
	jne	.L632
	cmpq	$0, -1792(%rbp)
	jne	.L633
.L402:
	cmpq	$0, -1600(%rbp)
	jne	.L634
.L406:
	cmpq	$0, -1408(%rbp)
	jne	.L635
.L410:
	cmpq	$0, -1216(%rbp)
	jne	.L636
.L414:
	cmpq	$0, -1024(%rbp)
	jne	.L637
.L417:
	cmpq	$0, -832(%rbp)
	jne	.L638
.L420:
	cmpq	$0, -640(%rbp)
	jne	.L639
.L423:
	cmpq	$0, -448(%rbp)
	leaq	-320(%rbp), %r14
	jne	.L640
.L426:
	movq	-2144(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2064(%rbp)
	movaps	%xmm0, -2080(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134743815, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2080(%rbp)
	movq	%rdx, -2064(%rbp)
	movq	%rdx, -2072(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2080(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L429
	call	_ZdlPv@PLT
.L429:
	movq	(%rbx), %rax
	movq	-2144(%rbp), %rdi
	movq	32(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L430
	call	_ZdlPv@PLT
.L430:
	movq	-304(%rbp), %rbx
	movq	-312(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L431
	.p2align 4,,10
	.p2align 3
.L435:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L432
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L435
.L433:
	movq	-312(%rbp), %r14
.L431:
	testq	%r14, %r14
	je	.L436
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L436:
	movq	-2168(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L437
	call	_ZdlPv@PLT
.L437:
	movq	-496(%rbp), %rbx
	movq	-504(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L438
	.p2align 4,,10
	.p2align 3
.L442:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L439
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L442
.L440:
	movq	-504(%rbp), %r14
.L438:
	testq	%r14, %r14
	je	.L443
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L443:
	movq	-2208(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L444
	call	_ZdlPv@PLT
.L444:
	movq	-688(%rbp), %rbx
	movq	-696(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L445
	.p2align 4,,10
	.p2align 3
.L449:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L446
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L449
.L447:
	movq	-696(%rbp), %r14
.L445:
	testq	%r14, %r14
	je	.L450
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L450:
	movq	-2152(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-864(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L451
	call	_ZdlPv@PLT
.L451:
	movq	-880(%rbp), %rbx
	movq	-888(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L452
	.p2align 4,,10
	.p2align 3
.L456:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L453
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L456
.L454:
	movq	-888(%rbp), %r14
.L452:
	testq	%r14, %r14
	je	.L457
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L457:
	movq	-2184(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L458
	call	_ZdlPv@PLT
.L458:
	movq	-1072(%rbp), %rbx
	movq	-1080(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L459
	.p2align 4,,10
	.p2align 3
.L463:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L460
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L463
.L461:
	movq	-1080(%rbp), %r14
.L459:
	testq	%r14, %r14
	je	.L464
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L464:
	movq	-2200(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L465
	call	_ZdlPv@PLT
.L465:
	movq	-1264(%rbp), %rbx
	movq	-1272(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L466
	.p2align 4,,10
	.p2align 3
.L470:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L467
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L470
.L468:
	movq	-1272(%rbp), %r14
.L466:
	testq	%r14, %r14
	je	.L471
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L471:
	movq	-2192(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1440(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L472
	call	_ZdlPv@PLT
.L472:
	movq	-1456(%rbp), %rbx
	movq	-1464(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L473
	.p2align 4,,10
	.p2align 3
.L477:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L474
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L477
.L475:
	movq	-1464(%rbp), %r14
.L473:
	testq	%r14, %r14
	je	.L478
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L478:
	movq	-2176(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1632(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L479
	call	_ZdlPv@PLT
.L479:
	movq	-1648(%rbp), %rbx
	movq	-1656(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L480
	.p2align 4,,10
	.p2align 3
.L484:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L481
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L484
.L482:
	movq	-1656(%rbp), %r14
.L480:
	testq	%r14, %r14
	je	.L485
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L485:
	movq	-2160(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L486
	call	_ZdlPv@PLT
.L486:
	movq	-1840(%rbp), %rbx
	movq	-1848(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L487
	.p2align 4,,10
	.p2align 3
.L491:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L488
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L491
.L489:
	movq	-1848(%rbp), %r14
.L487:
	testq	%r14, %r14
	je	.L492
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L492:
	movq	-2136(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2016(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L493
	call	_ZdlPv@PLT
.L493:
	movq	-2032(%rbp), %rbx
	movq	-2040(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L494
	.p2align 4,,10
	.p2align 3
.L498:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L495
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L498
.L496:
	movq	-2040(%rbp), %r14
.L494:
	testq	%r14, %r14
	je	.L499
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L499:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L641
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L495:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L498
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L488:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L491
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L481:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L484
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L474:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L477
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L467:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L470
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L460:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L463
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L453:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L456
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L446:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L449
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L432:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L435
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L439:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L442
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L632:
	movq	-2136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2064(%rbp)
	movaps	%xmm0, -2080(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134743815, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2080(%rbp)
	movq	%rdx, -2064(%rbp)
	movq	%rdx, -2072(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2080(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L400
	call	_ZdlPv@PLT
.L400:
	movq	(%rbx), %rax
	movl	$104, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	24(%rax), %rbx
	movq	%rcx, -2224(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -2240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$107, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-128(%rbp), %rsi
	leaq	-88(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r14, %xmm0
	movq	%rbx, %xmm4
	movq	%rbx, -96(%rbp)
	movhps	-2224(%rbp), %xmm0
	movq	$0, -2064(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	-2240(%rbp), %xmm0
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2080(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1856(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2080(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L401
	call	_ZdlPv@PLT
.L401:
	movq	-2160(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1792(%rbp)
	je	.L402
.L633:
	movq	-2160(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1856(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2064(%rbp)
	movaps	%xmm0, -2080(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134743815, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2080(%rbp)
	movq	%rdx, -2064(%rbp)
	movq	%rdx, -2072(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2080(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L403
	call	_ZdlPv@PLT
.L403:
	movq	(%rbx), %rax
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	16(%rax), %rbx
	movq	%rcx, -2256(%rbp)
	movq	24(%rax), %rcx
	movq	32(%rax), %rax
	movq	%rcx, -2272(%rbp)
	movq	%rax, -2224(%rbp)
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	-2224(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal29NumberIsGreaterThanOrEqual_78EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	movq	%rbx, %xmm5
	movq	%r14, %xmm6
	movq	%r13, %rdi
	movq	%rax, -2240(%rbp)
	leaq	-128(%rbp), %rbx
	movq	-2224(%rbp), %rax
	leaq	-88(%rbp), %r14
	movhps	-2272(%rbp), %xmm5
	movhps	-2256(%rbp), %xmm6
	movq	%r14, %rdx
	movq	%rbx, %rsi
	pxor	%xmm0, %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm5, -2272(%rbp)
	movaps	%xmm6, -2256(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm0, -2080(%rbp)
	movq	$0, -2064(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1664(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2080(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L404
	call	_ZdlPv@PLT
.L404:
	movq	-2224(%rbp), %rax
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movdqa	-2256(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movdqa	-2272(%rbp), %xmm5
	movq	$0, -2064(%rbp)
	movq	%rax, -96(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm0, -2080(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-704(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2080(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L405
	call	_ZdlPv@PLT
.L405:
	movq	-2208(%rbp), %rcx
	movq	-2176(%rbp), %rdx
	movq	%r12, %rdi
	movq	-2240(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1600(%rbp)
	je	.L406
.L634:
	movq	-2176(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1664(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2064(%rbp)
	movaps	%xmm0, -2080(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134743815, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2080(%rbp)
	movq	%rdx, -2064(%rbp)
	movq	%rdx, -2072(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2080(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L407
	call	_ZdlPv@PLT
.L407:
	movq	(%rbx), %rax
	leaq	-2112(%rbp), %r14
	movq	%r12, %rdi
	movq	16(%rax), %rsi
	movq	24(%rax), %rdx
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	32(%rax), %rax
	movq	%rsi, -2288(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -2296(%rbp)
	movl	$109, %edx
	movq	%rcx, -2224(%rbp)
	movq	%rax, -2240(%rbp)
	movq	%rbx, -2280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$159, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-2080(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	leaq	-128(%rbp), %rbx
	xorl	%esi, %esi
	movl	$2, %edi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-2224(%rbp), %r9
	pushq	%rdi
	movhps	-2240(%rbp), %xmm0
	movl	$1, %ecx
	movq	%r14, %rdi
	pushq	%rbx
	leaq	-2096(%rbp), %rdx
	movq	%rax, -2096(%rbp)
	movq	-2064(%rbp), %rax
	movaps	%xmm0, -128(%rbp)
	movq	%rax, -2088(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r14, %rdi
	movq	%rax, -2256(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$112, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2256(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	leaq	-80(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, -2272(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	-2240(%rbp), %xmm7
	movq	%r13, %rdi
	movaps	%xmm0, -2080(%rbp)
	movq	-2224(%rbp), %xmm4
	movq	-2288(%rbp), %xmm3
	movq	$0, -2064(%rbp)
	movhps	-2256(%rbp), %xmm7
	movhps	-2280(%rbp), %xmm4
	movhps	-2296(%rbp), %xmm3
	movaps	%xmm7, -2240(%rbp)
	movaps	%xmm3, -2256(%rbp)
	movaps	%xmm4, -2224(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1472(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2080(%rbp), %rdi
	popq	%r10
	popq	%r11
	testq	%rdi, %rdi
	je	.L408
	call	_ZdlPv@PLT
.L408:
	pxor	%xmm0, %xmm0
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movdqa	-2224(%rbp), %xmm6
	movdqa	-2256(%rbp), %xmm7
	movaps	%xmm0, -2080(%rbp)
	movdqa	-2240(%rbp), %xmm4
	movq	$0, -2064(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-896(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2080(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L409
	call	_ZdlPv@PLT
.L409:
	movq	-2152(%rbp), %rcx
	movq	-2192(%rbp), %rdx
	movq	%r12, %rdi
	movq	-2272(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1408(%rbp)
	je	.L410
.L635:
	movq	-2192(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1472(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2064(%rbp)
	movaps	%xmm0, -2080(%rbp)
	call	_Znwm@PLT
	movl	$1800, %r9d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134743815, (%rax)
	leaq	6(%rax), %rdx
	movw	%r9w, 4(%rax)
	movq	%rax, -2080(%rbp)
	movq	%rdx, -2064(%rbp)
	movq	%rdx, -2072(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2080(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L411
	call	_ZdlPv@PLT
.L411:
	movq	(%rbx), %rax
	leaq	-2112(%rbp), %r14
	movq	%r12, %rdi
	movq	24(%rax), %rdx
	movq	16(%rax), %rsi
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	%rdx, -2296(%rbp)
	movq	32(%rax), %rdx
	movq	40(%rax), %rax
	movq	%rsi, -2240(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -2256(%rbp)
	movl	$114, %edx
	movq	%rcx, -2224(%rbp)
	movq	%rax, -2304(%rbp)
	movq	%rbx, -2288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-2080(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	leaq	-128(%rbp), %rbx
	xorl	%esi, %esi
	movl	$2, %edi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-2224(%rbp), %r9
	pushq	%rdi
	movhps	-2256(%rbp), %xmm0
	movl	$1, %ecx
	movq	%r14, %rdi
	pushq	%rbx
	leaq	-2096(%rbp), %rdx
	movq	%rax, -2096(%rbp)
	movq	-2064(%rbp), %rax
	movaps	%xmm0, -128(%rbp)
	movq	%rax, -2088(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r14, %rdi
	movq	%rax, -2272(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$118, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2272(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	-2240(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler11StrictEqualENS0_8compiler11SloppyTNodeINS0_6ObjectEEES5_PNS2_21CodeAssemblerVariableE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$121, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -2280(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2280(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, -2280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %xmm1
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	-2272(%rbp), %xmm5
	leaq	-64(%rbp), %r14
	movq	-2256(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movq	-2240(%rbp), %xmm7
	movq	%r14, %rdx
	movq	-2224(%rbp), %xmm3
	movaps	%xmm0, -2080(%rbp)
	punpcklqdq	%xmm1, %xmm5
	movhps	-2304(%rbp), %xmm6
	movq	$0, -2064(%rbp)
	movhps	-2296(%rbp), %xmm7
	movhps	-2288(%rbp), %xmm3
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm5, -2272(%rbp)
	movaps	%xmm6, -2256(%rbp)
	movaps	%xmm7, -2240(%rbp)
	movaps	%xmm3, -2224(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm5, -80(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1280(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2080(%rbp), %rdi
	popq	%rsi
	popq	%r8
	testq	%rdi, %rdi
	je	.L412
	call	_ZdlPv@PLT
.L412:
	movdqa	-2224(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movq	%r14, %rdx
	movdqa	-2240(%rbp), %xmm5
	movdqa	-2256(%rbp), %xmm6
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movaps	%xmm0, -2080(%rbp)
	movdqa	-2272(%rbp), %xmm7
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	movq	$0, -2064(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1088(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2080(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L413
	call	_ZdlPv@PLT
.L413:
	movq	-2184(%rbp), %rcx
	movq	-2200(%rbp), %rdx
	movq	%r12, %rdi
	movq	-2280(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1216(%rbp)
	je	.L414
.L636:
	movq	-2200(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1280(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2064(%rbp)
	movaps	%xmm0, -2080(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$506662689155057415, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2080(%rbp)
	movq	%rdx, -2064(%rbp)
	movq	%rdx, -2072(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2080(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L415
	call	_ZdlPv@PLT
.L415:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	32(%rax), %rdx
	movdqu	(%rax), %xmm4
	movq	$0, -2064(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm0, -2080(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm1
	movdqa	-112(%rbp), %xmm2
	leaq	40(%rax), %rdx
	leaq	-512(%rbp), %rdi
	movq	%rax, -2080(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm1, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -2064(%rbp)
	movq	%rdx, -2072(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2080(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L416
	call	_ZdlPv@PLT
.L416:
	movq	-2168(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1024(%rbp)
	je	.L417
.L637:
	movq	-2184(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1088(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2064(%rbp)
	movaps	%xmm0, -2080(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$506662689155057415, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2080(%rbp)
	movq	%rdx, -2064(%rbp)
	movq	%rdx, -2072(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2080(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L418
	call	_ZdlPv@PLT
.L418:
	movq	(%rbx), %rax
	movl	$112, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %r14
	movq	32(%rax), %rbx
	movq	%rsi, -2240(%rbp)
	movq	24(%rax), %rsi
	movq	40(%rax), %rax
	movq	%rcx, -2224(%rbp)
	movq	%rsi, -2256(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rax, -2272(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-128(%rbp), %rsi
	leaq	-80(%rbp), %rdx
	movq	%r13, %rdi
	movq	-2224(%rbp), %xmm0
	movq	$0, -2064(%rbp)
	movhps	-2240(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%r14, %xmm0
	movhps	-2256(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%rbx, %xmm0
	movhps	-2272(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2080(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-896(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2080(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L419
	call	_ZdlPv@PLT
.L419:
	movq	-2152(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -832(%rbp)
	je	.L420
.L638:
	movq	-2152(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-896(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2064(%rbp)
	movaps	%xmm0, -2080(%rbp)
	call	_Znwm@PLT
	movl	$1800, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134743815, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -2080(%rbp)
	movq	%rdx, -2064(%rbp)
	movq	%rdx, -2072(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2080(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L421
	call	_ZdlPv@PLT
.L421:
	movq	(%rbx), %rax
	movl	$125, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	24(%rax), %rbx
	movq	%rsi, -2240(%rbp)
	movq	16(%rax), %rsi
	movq	%rcx, -2224(%rbp)
	movq	%rsi, -2256(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rbx, -2272(%rbp)
	movq	32(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberSubENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$107, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-128(%rbp), %rsi
	leaq	-88(%rbp), %rdx
	movq	%r13, %rdi
	movq	-2224(%rbp), %xmm0
	movq	%rbx, -96(%rbp)
	movq	$0, -2064(%rbp)
	movhps	-2240(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-2256(%rbp), %xmm0
	movhps	-2272(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2080(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1856(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2080(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L422
	call	_ZdlPv@PLT
.L422:
	movq	-2160(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -640(%rbp)
	je	.L423
.L639:
	movq	-2208(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-704(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2064(%rbp)
	movaps	%xmm0, -2080(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134743815, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2080(%rbp)
	movq	%rdx, -2064(%rbp)
	movq	%rdx, -2072(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2080(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L424
	call	_ZdlPv@PLT
.L424:
	movq	(%rbx), %rax
	movl	$129, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	16(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rcx, -2224(%rbp)
	movq	%rax, -2240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$-1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %xmm0
	movl	$40, %edi
	movq	%r15, -96(%rbp)
	movhps	-2224(%rbp), %xmm0
	movq	$0, -2064(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	%rbx, %xmm0
	movhps	-2240(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2080(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm6
	leaq	40(%rax), %rdx
	leaq	-512(%rbp), %rdi
	movq	%rax, -2080(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -2064(%rbp)
	movq	%rdx, -2072(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2080(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L425
	call	_ZdlPv@PLT
.L425:
	movq	-2168(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L640:
	movq	-2168(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-512(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2064(%rbp)
	movaps	%xmm0, -2080(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134743815, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2080(%rbp)
	movq	%rdx, -2064(%rbp)
	movq	%rdx, -2072(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2080(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L427
	call	_ZdlPv@PLT
.L427:
	movq	(%rbx), %rax
	movl	$101, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	16(%rax), %rbx
	movq	32(%rax), %r15
	movq	%rcx, -2224(%rbp)
	movq	24(%rax), %rcx
	movq	%rcx, -2240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %xmm0
	movl	$40, %edi
	movq	%r15, -96(%rbp)
	movhps	-2224(%rbp), %xmm0
	leaq	-320(%rbp), %r14
	movq	$0, -2064(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	%rbx, %xmm0
	movhps	-2240(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2080(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm4
	leaq	40(%rax), %rdx
	movq	%rax, -2080(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2064(%rbp)
	movq	%rdx, -2072(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2080(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L428
	call	_ZdlPv@PLT
.L428:
	movq	-2144(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L426
.L641:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22491:
	.size	_ZN2v88internal26GenericArrayLastIndexOf_23EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE, .-_ZN2v88internal26GenericArrayLastIndexOf_23EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEENS0_7RawPtrTES8_NS0_7IntPtrTES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EESF_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEENS0_7RawPtrTES8_NS0_7IntPtrTES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EESF_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEENS0_7RawPtrTES8_NS0_7IntPtrTES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EESF_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEENS0_7RawPtrTES8_NS0_7IntPtrTES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EESF_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEENS0_7RawPtrTES8_NS0_7IntPtrTES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EESF_:
.LFB26972:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$6, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$2053, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$84215815, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L643
	movq	%rax, -104(%rbp)
	call	_ZdlPv@PLT
	movq	-104(%rbp), %rax
.L643:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L644
	movq	%rdx, (%r15)
.L644:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L645
	movq	%rdx, (%r14)
.L645:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L646
	movq	%rdx, 0(%r13)
.L646:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L647
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L647:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L648
	movq	%rdx, (%rbx)
.L648:
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L642
	movq	-96(%rbp), %rbx
	movq	%rax, (%rbx)
.L642:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L673
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L673:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26972:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEENS0_7RawPtrTES8_NS0_7IntPtrTES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EESF_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEENS0_7RawPtrTES8_NS0_7IntPtrTES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EESF_
	.section	.text._ZN2v88internal15GetFromIndex_21EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEENS0_21TorqueStructArgumentsE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal15GetFromIndex_21EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEENS0_21TorqueStructArgumentsE
	.type	_ZN2v88internal15GetFromIndex_21EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEENS0_21TorqueStructArgumentsE, @function
_ZN2v88internal15GetFromIndex_21EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEENS0_21TorqueStructArgumentsE:
.LFB22456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-2032(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-2152(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$2280, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -2152(%rbp)
	movq	%rdi, -2032(%rbp)
	movl	$120, %edi
	movq	$0, -2024(%rbp)
	movq	$0, -2016(%rbp)
	movq	$0, -2008(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -2024(%rbp)
	leaq	-1976(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2008(%rbp)
	movq	%rdx, -2016(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1992(%rbp)
	movq	%rax, -2168(%rbp)
	movq	$0, -2000(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2152(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1832(%rbp)
	movq	$0, -1824(%rbp)
	movq	%rax, -1840(%rbp)
	movq	$0, -1816(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1832(%rbp)
	leaq	-1784(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1816(%rbp)
	movq	%rdx, -1824(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1800(%rbp)
	movq	%rax, -2200(%rbp)
	movq	$0, -1808(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2152(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1640(%rbp)
	movq	$0, -1632(%rbp)
	movq	%rax, -1648(%rbp)
	movq	$0, -1624(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1640(%rbp)
	leaq	-1592(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1624(%rbp)
	movq	%rdx, -1632(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1608(%rbp)
	movq	%rax, -2208(%rbp)
	movq	$0, -1616(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2152(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1448(%rbp)
	movq	$0, -1440(%rbp)
	movq	%rax, -1456(%rbp)
	movq	$0, -1432(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1448(%rbp)
	leaq	-1400(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1432(%rbp)
	movq	%rdx, -1440(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1416(%rbp)
	movq	%rax, -2224(%rbp)
	movq	$0, -1424(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2152(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1256(%rbp)
	movq	$0, -1248(%rbp)
	movq	%rax, -1264(%rbp)
	movq	$0, -1240(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1256(%rbp)
	leaq	-1208(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1240(%rbp)
	movq	%rdx, -1248(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1224(%rbp)
	movq	%rax, -2184(%rbp)
	movq	$0, -1232(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2152(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1064(%rbp)
	movq	$0, -1056(%rbp)
	movq	%rax, -1072(%rbp)
	movq	$0, -1048(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1064(%rbp)
	leaq	-1016(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1048(%rbp)
	movq	%rdx, -1056(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1032(%rbp)
	movq	%rax, -2216(%rbp)
	movq	$0, -1040(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2152(%rbp), %rax
	movl	$168, %edi
	movq	$0, -872(%rbp)
	movq	$0, -864(%rbp)
	movq	%rax, -880(%rbp)
	movq	$0, -856(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -872(%rbp)
	leaq	-824(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -856(%rbp)
	movq	%rdx, -864(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -840(%rbp)
	movq	%rax, -2232(%rbp)
	movq	$0, -848(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2152(%rbp), %rax
	movl	$168, %edi
	movq	$0, -680(%rbp)
	movq	$0, -672(%rbp)
	movq	%rax, -688(%rbp)
	movq	$0, -664(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -680(%rbp)
	leaq	-632(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -664(%rbp)
	movq	%rdx, -672(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -648(%rbp)
	movq	%rax, -2192(%rbp)
	movq	$0, -656(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2152(%rbp), %rax
	movl	$144, %edi
	movq	$0, -488(%rbp)
	movq	$0, -480(%rbp)
	movq	%rax, -496(%rbp)
	movq	$0, -472(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -488(%rbp)
	leaq	-440(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -472(%rbp)
	movq	%rdx, -480(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -456(%rbp)
	movq	%rax, -2240(%rbp)
	movq	$0, -464(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2152(%rbp), %rax
	movl	$144, %edi
	movq	$0, -296(%rbp)
	movq	$0, -288(%rbp)
	movq	%rax, -304(%rbp)
	movq	$0, -280(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -296(%rbp)
	leaq	-248(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -280(%rbp)
	movq	%rdx, -288(%rbp)
	xorl	%edx, %edx
	movq	%rax, -2176(%rbp)
	movups	%xmm0, -264(%rbp)
	movq	$0, -272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	%r13, -112(%rbp)
	leaq	-2064(%rbp), %r13
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movaps	%xmm0, -2064(%rbp)
	movq	%rax, -88(%rbp)
	movq	32(%rbp), %rax
	movq	%rbx, -104(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -2048(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm4
	leaq	40(%rax), %rdx
	movq	%rax, -2064(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2048(%rbp)
	movq	%rdx, -2056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L675
	call	_ZdlPv@PLT
.L675:
	movq	-2168(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1968(%rbp)
	jne	.L893
	cmpq	$0, -1776(%rbp)
	jne	.L894
.L680:
	cmpq	$0, -1584(%rbp)
	jne	.L895
.L683:
	cmpq	$0, -1392(%rbp)
	jne	.L896
.L686:
	cmpq	$0, -1200(%rbp)
	jne	.L897
.L688:
	cmpq	$0, -1008(%rbp)
	jne	.L898
.L691:
	cmpq	$0, -816(%rbp)
	jne	.L899
.L694:
	cmpq	$0, -624(%rbp)
	jne	.L900
.L697:
	cmpq	$0, -432(%rbp)
	leaq	-304(%rbp), %r14
	jne	.L901
.L700:
	movq	-2176(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2048(%rbp)
	movaps	%xmm0, -2064(%rbp)
	call	_Znwm@PLT
	movl	$2053, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$84215815, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -2064(%rbp)
	movq	%rdx, -2048(%rbp)
	movq	%rdx, -2056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2064(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L702
	call	_ZdlPv@PLT
.L702:
	movq	(%rbx), %rax
	movq	-2176(%rbp), %rdi
	movq	40(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L703
	call	_ZdlPv@PLT
.L703:
	movq	-288(%rbp), %rbx
	movq	-296(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L704
	.p2align 4,,10
	.p2align 3
.L708:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L705
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L708
.L706:
	movq	-296(%rbp), %r14
.L704:
	testq	%r14, %r14
	je	.L709
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L709:
	movq	-2240(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-464(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L710
	call	_ZdlPv@PLT
.L710:
	movq	-480(%rbp), %rbx
	movq	-488(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L711
	.p2align 4,,10
	.p2align 3
.L715:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L712
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L715
.L713:
	movq	-488(%rbp), %r14
.L711:
	testq	%r14, %r14
	je	.L716
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L716:
	movq	-2192(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L717
	call	_ZdlPv@PLT
.L717:
	movq	-672(%rbp), %rbx
	movq	-680(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L718
	.p2align 4,,10
	.p2align 3
.L722:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L719
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L722
.L720:
	movq	-680(%rbp), %r14
.L718:
	testq	%r14, %r14
	je	.L723
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L723:
	movq	-2232(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-848(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L724
	call	_ZdlPv@PLT
.L724:
	movq	-864(%rbp), %rbx
	movq	-872(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L725
	.p2align 4,,10
	.p2align 3
.L729:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L726
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L729
.L727:
	movq	-872(%rbp), %r14
.L725:
	testq	%r14, %r14
	je	.L730
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L730:
	movq	-2216(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1040(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L731
	call	_ZdlPv@PLT
.L731:
	movq	-1056(%rbp), %rbx
	movq	-1064(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L732
	.p2align 4,,10
	.p2align 3
.L736:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L733
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L736
.L734:
	movq	-1064(%rbp), %r14
.L732:
	testq	%r14, %r14
	je	.L737
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L737:
	movq	-2184(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L738
	call	_ZdlPv@PLT
.L738:
	movq	-1248(%rbp), %rbx
	movq	-1256(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L739
	.p2align 4,,10
	.p2align 3
.L743:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L740
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L743
.L741:
	movq	-1256(%rbp), %r14
.L739:
	testq	%r14, %r14
	je	.L744
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L744:
	movq	-2224(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L745
	call	_ZdlPv@PLT
.L745:
	movq	-1440(%rbp), %rbx
	movq	-1448(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L746
	.p2align 4,,10
	.p2align 3
.L750:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L747
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L750
.L748:
	movq	-1448(%rbp), %r14
.L746:
	testq	%r14, %r14
	je	.L751
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L751:
	movq	-2208(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1616(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L752
	call	_ZdlPv@PLT
.L752:
	movq	-1632(%rbp), %rbx
	movq	-1640(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L753
	.p2align 4,,10
	.p2align 3
.L757:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L754
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L757
.L755:
	movq	-1640(%rbp), %r14
.L753:
	testq	%r14, %r14
	je	.L758
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L758:
	movq	-2200(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1808(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L759
	call	_ZdlPv@PLT
.L759:
	movq	-1824(%rbp), %rbx
	movq	-1832(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L760
	.p2align 4,,10
	.p2align 3
.L764:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L761
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L764
.L762:
	movq	-1832(%rbp), %r14
.L760:
	testq	%r14, %r14
	je	.L765
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L765:
	movq	-2168(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2000(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L766
	call	_ZdlPv@PLT
.L766:
	movq	-2016(%rbp), %rbx
	movq	-2024(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L767
	.p2align 4,,10
	.p2align 3
.L771:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L768
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L771
.L769:
	movq	-2024(%rbp), %r14
.L767:
	testq	%r14, %r14
	je	.L772
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L772:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L902
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L768:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L771
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L761:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L764
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L754:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L757
	jmp	.L755
	.p2align 4,,10
	.p2align 3
.L747:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L750
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L740:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L743
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L733:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L736
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L726:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L729
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L719:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L722
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L705:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L708
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L712:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L715
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L893:
	movq	-2168(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2048(%rbp)
	movaps	%xmm0, -2064(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$84215815, (%rax)
	leaq	5(%rax), %rdx
	movb	$5, 4(%rax)
	movq	%rax, -2064(%rbp)
	movq	%rdx, -2048(%rbp)
	movq	%rdx, -2056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2064(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L677
	call	_ZdlPv@PLT
.L677:
	movq	(%rbx), %rax
	movl	$65, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	32(%rax), %r14
	movq	16(%rax), %rbx
	movq	%rsi, -2304(%rbp)
	movq	24(%rax), %rsi
	movq	%rcx, -2288(%rbp)
	movq	%rsi, -2320(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%r14, -2248(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$2, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdx
	movq	-2248(%rbp), %r14
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, -2272(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, -80(%rbp)
	leaq	-72(%rbp), %r14
	movq	%r13, %rdi
	movq	-2288(%rbp), %xmm6
	movq	%rbx, %xmm5
	leaq	-112(%rbp), %rbx
	pxor	%xmm0, %xmm0
	movhps	-2320(%rbp), %xmm5
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movaps	%xmm0, -2064(%rbp)
	movhps	-2304(%rbp), %xmm6
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm5, -2320(%rbp)
	movaps	%xmm6, -2288(%rbp)
	movaps	%xmm6, -112(%rbp)
	movq	$0, -2048(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1840(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L678
	call	_ZdlPv@PLT
.L678:
	movq	-2248(%rbp), %rax
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movdqa	-2288(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movdqa	-2320(%rbp), %xmm4
	movq	$0, -2048(%rbp)
	movq	%rax, -80(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm0, -2064(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1648(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L679
	call	_ZdlPv@PLT
.L679:
	movq	-2208(%rbp), %rcx
	movq	-2200(%rbp), %rdx
	movq	%r12, %rdi
	movq	-2272(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1776(%rbp)
	je	.L680
.L894:
	movq	-2200(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1840(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2048(%rbp)
	movaps	%xmm0, -2064(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$84215815, (%rax)
	leaq	5(%rax), %rdx
	movb	$5, 4(%rax)
	movq	%rax, -2064(%rbp)
	movq	%rdx, -2048(%rbp)
	movq	%rdx, -2056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2064(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L681
	call	_ZdlPv@PLT
.L681:
	movq	(%rbx), %rax
	movl	$66, %edx
	movq	%r12, %rdi
	movq	16(%rax), %rsi
	movq	(%rax), %rcx
	movq	8(%rax), %r14
	movq	24(%rax), %rbx
	movq	32(%rax), %rax
	movq	%rsi, -2272(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -2248(%rbp)
	movq	%rbx, -2288(%rbp)
	movq	%rax, -2304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberSubENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r13, %rdi
	movq	%rax, -2320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$65, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %xmm3
	movl	$48, %edi
	movq	-2248(%rbp), %xmm0
	movq	$0, -2048(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-2272(%rbp), %xmm0
	movhps	-2288(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-2304(%rbp), %xmm0
	movhps	-2320(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2064(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm6
	leaq	48(%rax), %rdx
	leaq	-1456(%rbp), %rdi
	movq	%rax, -2064(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -2048(%rbp)
	movq	%rdx, -2056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L682
	call	_ZdlPv@PLT
.L682:
	movq	-2224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1584(%rbp)
	je	.L683
.L895:
	movq	-2208(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1648(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2048(%rbp)
	movaps	%xmm0, -2064(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$84215815, (%rax)
	leaq	5(%rax), %rdx
	movb	$5, 4(%rax)
	movq	%rax, -2064(%rbp)
	movq	%rdx, -2048(%rbp)
	movq	%rdx, -2056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2064(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L684
	call	_ZdlPv@PLT
.L684:
	movq	(%rbx), %rax
	movl	$67, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	24(%rax), %rsi
	movq	(%rax), %r14
	movq	32(%rax), %rbx
	movq	%rcx, -2248(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -2288(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -2272(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -2304(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2304(%rbp), %r8
	subq	$8, %rsp
	movq	%r13, %rdi
	movq	-2272(%rbp), %xmm0
	movq	%rbx, -2080(%rbp)
	pushq	-2080(%rbp)
	movq	%r8, %rsi
	movhps	-2288(%rbp), %xmm0
	movaps	%xmm0, -2096(%rbp)
	pushq	-2088(%rbp)
	pushq	-2096(%rbp)
	movaps	%xmm0, -2288(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r13, %rdi
	movq	%rax, -2272(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	-2272(%rbp), %rdx
	call	_ZN2v88internal17CodeStubAssembler16ToInteger_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_23ToIntegerTruncationModeE@PLT
	movq	%r13, %rdi
	movq	%rax, -2272(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$65, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %xmm1
	movl	$48, %edi
	movdqa	-2288(%rbp), %xmm0
	movhps	-2248(%rbp), %xmm1
	movq	$0, -2048(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	movhps	-2272(%rbp), %xmm0
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2064(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	leaq	48(%rax), %rdx
	leaq	-1264(%rbp), %rdi
	movq	%rax, -2064(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm4, 32(%rax)
	movq	%rdx, -2048(%rbp)
	movq	%rdx, -2056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L685
	call	_ZdlPv@PLT
.L685:
	movq	-2184(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1392(%rbp)
	je	.L686
.L896:
	movq	-2224(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -2144(%rbp)
	leaq	-1456(%rbp), %r14
	movq	$0, -2136(%rbp)
	movq	$0, -2128(%rbp)
	movq	$0, -2120(%rbp)
	movq	$0, -2112(%rbp)
	movq	$0, -2104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r14, %rdi
	leaq	-2104(%rbp), %rax
	pushq	%rax
	leaq	-2128(%rbp), %rcx
	leaq	-2136(%rbp), %rdx
	leaq	-2144(%rbp), %rsi
	leaq	-2112(%rbp), %r9
	leaq	-2120(%rbp), %r8
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEENS0_7RawPtrTES8_NS0_7IntPtrTES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EESF_
	movl	$48, %edi
	movq	-2112(%rbp), %xmm0
	movq	-2128(%rbp), %xmm1
	movq	-2144(%rbp), %xmm2
	movq	$0, -2048(%rbp)
	movhps	-2104(%rbp), %xmm0
	movhps	-2120(%rbp), %xmm1
	movhps	-2136(%rbp), %xmm2
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -2064(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	leaq	48(%rax), %rdx
	leaq	-1264(%rbp), %rdi
	movq	%rax, -2064(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -2048(%rbp)
	movq	%rdx, -2056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2064(%rbp), %rdi
	popq	%rcx
	popq	%rsi
	testq	%rdi, %rdi
	je	.L687
	call	_ZdlPv@PLT
.L687:
	movq	-2184(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1200(%rbp)
	je	.L688
.L897:
	movq	-2184(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -2144(%rbp)
	leaq	-1264(%rbp), %r14
	movq	$0, -2136(%rbp)
	leaq	-112(%rbp), %rbx
	movq	$0, -2128(%rbp)
	movq	$0, -2120(%rbp)
	movq	$0, -2112(%rbp)
	movq	$0, -2104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r14, %rdi
	leaq	-2104(%rbp), %rax
	pushq	%rax
	leaq	-2128(%rbp), %rcx
	leaq	-2112(%rbp), %r9
	leaq	-2120(%rbp), %r8
	leaq	-2136(%rbp), %rdx
	leaq	-2144(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEENS0_7RawPtrTES8_NS0_7IntPtrTES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EESF_
	movl	$70, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$71, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	-2104(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal29NumberIsGreaterThanOrEqual_78EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	leaq	-56(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	-2112(%rbp), %xmm7
	movq	%r13, %rdi
	movq	%r14, -64(%rbp)
	movq	-2128(%rbp), %xmm2
	movq	%rax, -2248(%rbp)
	movq	-2144(%rbp), %xmm1
	movhps	-2104(%rbp), %xmm7
	movq	%rdx, -2320(%rbp)
	movhps	-2120(%rbp), %xmm2
	movaps	%xmm7, -2304(%rbp)
	movhps	-2136(%rbp), %xmm1
	movaps	%xmm2, -2288(%rbp)
	movaps	%xmm1, -2272(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	movaps	%xmm0, -2064(%rbp)
	movq	$0, -2048(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1072(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2064(%rbp), %rdi
	popq	%rax
	popq	%rdx
	movq	-2320(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L689
	call	_ZdlPv@PLT
	movq	-2320(%rbp), %rdx
.L689:
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r14, -64(%rbp)
	movdqa	-2272(%rbp), %xmm3
	movdqa	-2288(%rbp), %xmm4
	movaps	%xmm0, -2064(%rbp)
	movdqa	-2304(%rbp), %xmm5
	movq	$0, -2048(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-880(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L690
	call	_ZdlPv@PLT
.L690:
	movq	-2232(%rbp), %rcx
	movq	-2216(%rbp), %rdx
	movq	%r12, %rdi
	movq	-2248(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1008(%rbp)
	je	.L691
.L898:
	movq	-2216(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1072(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2048(%rbp)
	movaps	%xmm0, -2064(%rbp)
	call	_Znwm@PLT
	movl	$2053, %r11d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$84215815, (%rax)
	leaq	7(%rax), %rdx
	movw	%r11w, 4(%rax)
	movb	$8, 6(%rax)
	movq	%rax, -2064(%rbp)
	movq	%rdx, -2048(%rbp)
	movq	%rdx, -2056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2064(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L692
	call	_ZdlPv@PLT
.L692:
	movq	(%rbx), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	24(%rax), %rdx
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	40(%rax), %r14
	movq	%rdx, -2304(%rbp)
	movq	32(%rax), %rdx
	movq	%rcx, -2272(%rbp)
	movq	8(%rax), %rcx
	movq	%rdx, -2320(%rbp)
	movl	$74, %edx
	movq	%rcx, -2248(%rbp)
	movq	%rbx, -2288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2248(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberSubENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Min_80EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	movl	$71, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-112(%rbp), %rsi
	leaq	-56(%rbp), %rdx
	movq	%r13, %rdi
	movq	-2272(%rbp), %xmm0
	movq	%r14, %xmm6
	movq	%rbx, -64(%rbp)
	movq	$0, -2048(%rbp)
	movhps	-2248(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-2288(%rbp), %xmm0
	movhps	-2304(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-2320(%rbp), %xmm0
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2064(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-688(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L693
	call	_ZdlPv@PLT
.L693:
	movq	-2192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -816(%rbp)
	je	.L694
.L899:
	movq	-2232(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-880(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2048(%rbp)
	movaps	%xmm0, -2064(%rbp)
	call	_Znwm@PLT
	movl	$2053, %r10d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$84215815, (%rax)
	leaq	7(%rax), %rdx
	movw	%r10w, 4(%rax)
	movb	$8, 6(%rax)
	movq	%rax, -2064(%rbp)
	movq	%rdx, -2048(%rbp)
	movq	%rdx, -2056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2064(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L695
	call	_ZdlPv@PLT
.L695:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rdx
	movq	16(%rax), %rsi
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	%rdx, -2304(%rbp)
	movq	32(%rax), %rdx
	movq	40(%rax), %r14
	movq	%rsi, -2288(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -2320(%rbp)
	movl	$77, %edx
	movq	%rcx, -2248(%rbp)
	movq	%rbx, -2272(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberAddENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$71, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-112(%rbp), %rsi
	leaq	-56(%rbp), %rdx
	movq	%r13, %rdi
	movq	-2248(%rbp), %xmm0
	movq	%r14, %xmm7
	movq	%rbx, -64(%rbp)
	movq	$0, -2048(%rbp)
	movhps	-2272(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-2288(%rbp), %xmm0
	movhps	-2304(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-2320(%rbp), %xmm0
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2064(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-688(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L696
	call	_ZdlPv@PLT
.L696:
	movq	-2192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -624(%rbp)
	je	.L697
.L900:
	movq	-2192(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-688(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2048(%rbp)
	movaps	%xmm0, -2064(%rbp)
	call	_Znwm@PLT
	movl	$2053, %r9d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$84215815, (%rax)
	leaq	7(%rax), %rdx
	movw	%r9w, 4(%rax)
	movb	$8, 6(%rax)
	movq	%rax, -2064(%rbp)
	movq	%rdx, -2048(%rbp)
	movq	%rdx, -2056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2064(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L698
	call	_ZdlPv@PLT
.L698:
	movq	(%rbx), %rax
	movl	$79, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	16(%rax), %r14
	movq	32(%rax), %rbx
	movq	%rcx, -2248(%rbp)
	movq	24(%rax), %rcx
	movq	48(%rax), %rax
	movq	%rcx, -2272(%rbp)
	movq	%rax, -2288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %xmm0
	movl	$48, %edi
	movq	$0, -2048(%rbp)
	movhps	-2248(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%r14, %xmm0
	movhps	-2272(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	movhps	-2288(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2064(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	leaq	48(%rax), %rdx
	leaq	-496(%rbp), %rdi
	movq	%rax, -2064(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rdx, -2048(%rbp)
	movq	%rdx, -2056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L699
	call	_ZdlPv@PLT
.L699:
	movq	-2240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L901:
	movq	-2240(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -2144(%rbp)
	leaq	-496(%rbp), %r15
	movq	$0, -2136(%rbp)
	movq	$0, -2128(%rbp)
	movq	$0, -2120(%rbp)
	movq	$0, -2112(%rbp)
	movq	$0, -2104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r15, %rdi
	leaq	-2104(%rbp), %rax
	pushq	%rax
	leaq	-2120(%rbp), %r8
	leaq	-2128(%rbp), %rcx
	leaq	-2112(%rbp), %r9
	leaq	-2136(%rbp), %rdx
	leaq	-2144(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEENS0_7RawPtrTES8_NS0_7IntPtrTES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EESF_
	movl	$61, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$48, %edi
	movq	-2112(%rbp), %xmm0
	movq	-2128(%rbp), %xmm1
	movq	-2144(%rbp), %xmm2
	movq	$0, -2048(%rbp)
	movhps	-2104(%rbp), %xmm0
	movhps	-2120(%rbp), %xmm1
	movhps	-2136(%rbp), %xmm2
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -2064(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm6
	movq	%r13, %rsi
	movq	%r14, %rdi
	movdqa	-96(%rbp), %xmm7
	movdqa	-80(%rbp), %xmm3
	leaq	48(%rax), %rdx
	movq	%rax, -2064(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm3, 32(%rax)
	movq	%rdx, -2048(%rbp)
	movq	%rdx, -2056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2064(%rbp), %rdi
	popq	%rsi
	popq	%r8
	testq	%rdi, %rdi
	je	.L701
	call	_ZdlPv@PLT
.L701:
	movq	-2176(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L700
.L902:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22456:
	.size	_ZN2v88internal15GetFromIndex_21EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEENS0_21TorqueStructArgumentsE, .-_ZN2v88internal15GetFromIndex_21EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEENS0_21TorqueStructArgumentsE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE9AddInputsENS1_5TNodeIS3_EENS9_IS4_EENS9_IS5_EENS9_IS6_EENS9_IS7_EESC_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE9AddInputsENS1_5TNodeIS3_EENS9_IS4_EENS9_IS5_EENS9_IS6_EENS9_IS7_EESC_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE9AddInputsENS1_5TNodeIS3_EENS9_IS4_EENS9_IS5_EENS9_IS6_EENS9_IS7_EESC_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE9AddInputsENS1_5TNodeIS3_EENS9_IS4_EENS9_IS5_EENS9_IS6_EENS9_IS7_EESC_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE9AddInputsENS1_5TNodeIS3_EENS9_IS4_EENS9_IS5_EENS9_IS6_EENS9_IS7_EESC_:
.LFB27025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r9, %xmm0
	movq	%rdx, %xmm3
	movq	%rcx, %xmm1
	movq	%rsi, %xmm2
	punpcklqdq	%xmm3, %xmm2
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$48, %edi
	subq	$120, %rsp
	movq	%r8, -120(%rbp)
	movhps	16(%rbp), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movhps	-120(%rbp), %xmm1
	movaps	%xmm0, -48(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -80(%rbp)
	movaps	%xmm1, -64(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	$0, -96(%rbp)
	call	_Znwm@PLT
	movdqa	-80(%rbp), %xmm4
	movdqa	-64(%rbp), %xmm5
	movq	%r12, %rdi
	movdqa	-48(%rbp), %xmm6
	leaq	48(%rax), %rdx
	leaq	-112(%rbp), %rsi
	movq	%rax, -112(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -96(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L903
	call	_ZdlPv@PLT
.L903:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L910
	addq	$120, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L910:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27025:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE9AddInputsENS1_5TNodeIS3_EENS9_IS4_EENS9_IS5_EENS9_IS6_EENS9_IS7_EESC_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE9AddInputsENS1_5TNodeIS3_EENS9_IS4_EENS9_IS5_EENS9_IS6_EENS9_IS7_EESC_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_:
.LFB27026:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$6, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$1543, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$134612743, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L912
	movq	%rax, -104(%rbp)
	call	_ZdlPv@PLT
	movq	-104(%rbp), %rax
.L912:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L913
	movq	%rdx, (%r15)
.L913:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L914
	movq	%rdx, (%r14)
.L914:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L915
	movq	%rdx, 0(%r13)
.L915:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L916
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L916:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L917
	movq	%rdx, (%rbx)
.L917:
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L911
	movq	-96(%rbp), %rbx
	movq	%rax, (%rbx)
.L911:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L942
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L942:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27026:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_S6_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SI_PNSA_IS8_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_S6_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SI_PNSA_IS8_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_S6_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SI_PNSA_IS8_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_S6_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SI_PNSA_IS8_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_S6_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SI_PNSA_IS8_EE:
.LFB27032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$8, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$506661585348331271, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L944
	movq	%rax, -120(%rbp)
	call	_ZdlPv@PLT
	movq	-120(%rbp), %rax
.L944:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L945
	movq	%rdx, (%r15)
.L945:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L946
	movq	%rdx, (%r14)
.L946:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L947
	movq	%rdx, 0(%r13)
.L947:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L948
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L948:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L949
	movq	%rdx, (%rbx)
.L949:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L950
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L950:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L951
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L951:
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L943
	movq	-112(%rbp), %rbx
	movq	%rax, (%rbx)
.L943:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L982
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L982:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27032:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_S6_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SI_PNSA_IS8_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_S6_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SI_PNSA_IS8_EE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESE_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESE_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESE_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESE_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESE_:
.LFB27034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$5, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$56, %rsp
	movq	%r8, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$134612743, (%rax)
	leaq	5(%rax), %rdx
	movb	$6, 4(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L984
	movq	%rax, -96(%rbp)
	call	_ZdlPv@PLT
	movq	-96(%rbp), %rax
.L984:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L985
	movq	%rdx, (%r15)
.L985:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L986
	movq	%rdx, (%r14)
.L986:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L987
	movq	%rdx, 0(%r13)
.L987:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L988
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L988:
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L983
	movq	%rax, (%rbx)
.L983:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1010
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1010:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27034:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESE_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESE_
	.section	.text._ZN2v88internal37FastArrayLastIndexOf10FixedArray_1423EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEENS4_INS0_3SmiEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal37FastArrayLastIndexOf10FixedArray_1423EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEENS4_INS0_3SmiEEENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal37FastArrayLastIndexOf10FixedArray_1423EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEENS4_INS0_3SmiEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal37FastArrayLastIndexOf10FixedArray_1423EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEENS4_INS0_3SmiEEENS4_INS0_6ObjectEEE:
.LFB22525:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-2760(%rbp), %r14
	leaq	-2816(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2944(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-3040(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$3272, %rsp
	movq	%rsi, -3056(%rbp)
	movq	%rdx, -3072(%rbp)
	movq	%rcx, -3088(%rbp)
	movq	%r8, -3096(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -3040(%rbp)
	movq	%rdi, -2816(%rbp)
	movl	$96, %edi
	movq	$0, -2808(%rbp)
	movq	$0, -2800(%rbp)
	movq	$0, -2792(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -2792(%rbp)
	movq	%rdx, -2800(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2776(%rbp)
	movq	%rax, -2808(%rbp)
	movq	$0, -2784(%rbp)
	movq	%r15, -3176(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3040(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2616(%rbp)
	movq	$0, -2608(%rbp)
	movq	%rax, -2624(%rbp)
	movq	$0, -2600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2616(%rbp)
	leaq	-2568(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2600(%rbp)
	movq	%rdx, -2608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2584(%rbp)
	movq	%rax, -3136(%rbp)
	movq	$0, -2592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3040(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2424(%rbp)
	movq	$0, -2416(%rbp)
	movq	%rax, -2432(%rbp)
	movq	$0, -2408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2424(%rbp)
	leaq	-2376(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2408(%rbp)
	movq	%rdx, -2416(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2392(%rbp)
	movq	%rax, -3120(%rbp)
	movq	$0, -2400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3040(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2232(%rbp)
	movq	$0, -2224(%rbp)
	movq	%rax, -2240(%rbp)
	movq	$0, -2216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2232(%rbp)
	leaq	-2184(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2216(%rbp)
	movq	%rdx, -2224(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2200(%rbp)
	movq	%rax, -3200(%rbp)
	movq	$0, -2208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3040(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movq	%rax, -2048(%rbp)
	movq	$0, -2024(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2040(%rbp)
	leaq	-1992(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2024(%rbp)
	movq	%rdx, -2032(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2008(%rbp)
	movq	%rax, -3104(%rbp)
	movq	$0, -2016(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3040(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1848(%rbp)
	movq	$0, -1840(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1832(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1848(%rbp)
	leaq	-1800(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1832(%rbp)
	movq	%rdx, -1840(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1816(%rbp)
	movq	%rax, -3144(%rbp)
	movq	$0, -1824(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3040(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1656(%rbp)
	movq	$0, -1648(%rbp)
	movq	%rax, -1664(%rbp)
	movq	$0, -1640(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1656(%rbp)
	leaq	-1608(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1640(%rbp)
	movq	%rdx, -1648(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1624(%rbp)
	movq	%rax, -3240(%rbp)
	movq	$0, -1632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3040(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1464(%rbp)
	movq	$0, -1456(%rbp)
	movq	%rax, -1472(%rbp)
	movq	$0, -1448(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1464(%rbp)
	leaq	-1416(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1448(%rbp)
	movq	%rdx, -1456(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1432(%rbp)
	movq	%rax, -3248(%rbp)
	movq	$0, -1440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3040(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1272(%rbp)
	movq	$0, -1264(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -1256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1272(%rbp)
	leaq	-1224(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1256(%rbp)
	movq	%rdx, -1264(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1240(%rbp)
	movq	%rax, -3224(%rbp)
	movq	$0, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3040(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1080(%rbp)
	movq	$0, -1072(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$0, -1064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1080(%rbp)
	leaq	-1032(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1064(%rbp)
	movq	%rdx, -1072(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1048(%rbp)
	movq	%rax, -3232(%rbp)
	movq	$0, -1056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3040(%rbp), %rax
	movl	$144, %edi
	movq	$0, -888(%rbp)
	movq	$0, -880(%rbp)
	movq	%rax, -896(%rbp)
	movq	$0, -872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -888(%rbp)
	leaq	-840(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -872(%rbp)
	movq	%rdx, -880(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -856(%rbp)
	movq	%rax, -3208(%rbp)
	movq	$0, -864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3040(%rbp), %rax
	movl	$144, %edi
	movq	$0, -696(%rbp)
	movq	$0, -688(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -696(%rbp)
	leaq	-648(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -680(%rbp)
	movq	%rdx, -688(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -664(%rbp)
	movq	%rax, -3216(%rbp)
	movq	$0, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3040(%rbp), %rax
	movl	$120, %edi
	movq	$0, -504(%rbp)
	movq	$0, -496(%rbp)
	movq	%rax, -512(%rbp)
	movq	$0, -488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -504(%rbp)
	leaq	-456(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -488(%rbp)
	movq	%rdx, -496(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -472(%rbp)
	movq	%rax, -3192(%rbp)
	movq	$0, -480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3040(%rbp), %rax
	movl	$120, %edi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -312(%rbp)
	leaq	-264(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	xorl	%edx, %edx
	movq	%rax, -3184(%rbp)
	movups	%xmm0, -280(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-3088(%rbp), %xmm1
	movq	-3056(%rbp), %xmm2
	movaps	%xmm0, -2944(%rbp)
	movhps	-3096(%rbp), %xmm1
	movq	$0, -2928(%rbp)
	movhps	-3072(%rbp), %xmm2
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -128(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm3
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm4
	leaq	32(%rax), %rdx
	movq	%rax, -2944(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1012
	call	_ZdlPv@PLT
.L1012:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2624(%rbp), %rax
	cmpq	$0, -2752(%rbp)
	movq	%rax, -3096(%rbp)
	leaq	-2432(%rbp), %rax
	movq	%rax, -3056(%rbp)
	jne	.L1102
	cmpq	$0, -2560(%rbp)
	jne	.L1103
.L1017:
	leaq	-2240(%rbp), %rax
	cmpq	$0, -2368(%rbp)
	movq	%rax, -3072(%rbp)
	jne	.L1104
.L1018:
	leaq	-2048(%rbp), %rax
	cmpq	$0, -2176(%rbp)
	movq	%rax, -3168(%rbp)
	leaq	-704(%rbp), %rax
	movq	%rax, -3152(%rbp)
	jne	.L1105
.L1019:
	leaq	-1664(%rbp), %rax
	cmpq	$0, -1984(%rbp)
	movq	%rax, -3136(%rbp)
	leaq	-1856(%rbp), %rax
	movq	%rax, -3088(%rbp)
	jne	.L1106
.L1022:
	leaq	-1088(%rbp), %rax
	cmpq	$0, -1792(%rbp)
	movq	%rax, -3104(%rbp)
	jne	.L1107
.L1026:
	leaq	-1472(%rbp), %rax
	cmpq	$0, -1600(%rbp)
	movq	%rax, -3144(%rbp)
	leaq	-1280(%rbp), %rax
	movq	%rax, -3120(%rbp)
	jne	.L1108
.L1028:
	cmpq	$0, -1408(%rbp)
	leaq	-512(%rbp), %r15
	jne	.L1109
.L1032:
	cmpq	$0, -1216(%rbp)
	leaq	-896(%rbp), %r14
	jne	.L1110
	cmpq	$0, -1024(%rbp)
	jne	.L1111
.L1035:
	cmpq	$0, -832(%rbp)
	jne	.L1112
.L1036:
	cmpq	$0, -640(%rbp)
	jne	.L1113
.L1037:
	cmpq	$0, -448(%rbp)
	leaq	-320(%rbp), %rbx
	jne	.L1114
.L1039:
	movq	-3184(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2928(%rbp)
	movaps	%xmm0, -2944(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	$134612743, (%rax)
	leaq	5(%rax), %rdx
	movb	$6, 4(%rax)
	movq	%rax, -2944(%rbp)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2944(%rbp), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L1041
	call	_ZdlPv@PLT
.L1041:
	movq	0(%r13), %rax
	movq	%rbx, %rdi
	movq	32(%rax), %r13
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3152(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3104(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3120(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3144(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3136(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3088(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3168(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3072(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3056(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3096(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3176(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1115
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1102:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2928(%rbp)
	movaps	%xmm0, -2944(%rbp)
	call	_Znwm@PLT
	movq	-3176(%rbp), %rdi
	movq	%r13, %rsi
	movl	$134612743, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2944(%rbp)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2944(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L1014
	call	_ZdlPv@PLT
.L1014:
	movq	(%r14), %rax
	movl	$30, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %r14
	movq	%rcx, -3072(%rbp)
	movq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%r14, -3088(%rbp)
	movq	%rcx, -3056(%rbp)
	movq	%rax, -3096(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE(%rip), %esi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$31, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$37, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-3056(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler24IntPtrGreaterThanOrEqualENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3056(%rbp), %rax
	movq	%r14, %xmm5
	movq	-3072(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movq	$0, -2928(%rbp)
	movq	%rax, %xmm6
	movhps	-3088(%rbp), %xmm7
	movaps	%xmm0, -2944(%rbp)
	punpcklqdq	%xmm6, %xmm5
	movhps	-3096(%rbp), %xmm6
	movaps	%xmm7, -3072(%rbp)
	movaps	%xmm5, -3056(%rbp)
	movaps	%xmm6, -3168(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm7
	leaq	48(%rax), %rdx
	movq	%rax, -2944(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	leaq	-2624(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	movq	%rax, -3096(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1015
	call	_ZdlPv@PLT
.L1015:
	movdqa	-3072(%rbp), %xmm1
	movdqa	-3168(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movdqa	-3056(%rbp), %xmm5
	movaps	%xmm0, -2944(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movq	$0, -2928(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm1
	leaq	48(%rax), %rdx
	movq	%rax, -2944(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm1, 32(%rax)
	leaq	-2432(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	movq	%rax, -3056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1016
	call	_ZdlPv@PLT
.L1016:
	movq	-3120(%rbp), %rcx
	movq	-3136(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2560(%rbp)
	je	.L1017
.L1103:
	movq	-3136(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3016(%rbp)
	movq	$0, -3008(%rbp)
	movq	$0, -3000(%rbp)
	movq	$0, -2992(%rbp)
	movq	$0, -2984(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-2976(%rbp), %rax
	movq	-3096(%rbp), %rdi
	pushq	%rax
	leaq	-2984(%rbp), %r9
	leaq	-2992(%rbp), %r8
	leaq	-3000(%rbp), %rcx
	leaq	-3008(%rbp), %rdx
	leaq	-3016(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_
	movl	$38, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	movq	-2984(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$37, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2984(%rbp), %r9
	movq	%r14, (%rsp)
	movq	-2992(%rbp), %r8
	movq	-3000(%rbp), %rcx
	movq	-3008(%rbp), %rdx
	movq	-3016(%rbp), %rsi
	movq	-3056(%rbp), %rdi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE9AddInputsENS1_5TNodeIS3_EENS9_IS4_EENS9_IS5_EENS9_IS6_EENS9_IS7_EESC_
	movq	-3120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	popq	%r10
	popq	%r11
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1105:
	movq	-3200(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3016(%rbp)
	movq	$0, -3008(%rbp)
	movq	$0, -3000(%rbp)
	movq	$0, -2992(%rbp)
	movq	$0, -2984(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-2976(%rbp), %rax
	movq	-3072(%rbp), %rdi
	pushq	%rax
	leaq	-3000(%rbp), %rcx
	leaq	-2984(%rbp), %r9
	leaq	-2992(%rbp), %r8
	leaq	-3008(%rbp), %rdx
	leaq	-3016(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2976(%rbp), %r15
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler24IntPtrGreaterThanOrEqualENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movq	-2984(%rbp), %xmm3
	movq	-3000(%rbp), %xmm4
	movq	-3016(%rbp), %xmm5
	movaps	%xmm0, -2944(%rbp)
	movhps	-2976(%rbp), %xmm3
	movq	$0, -2928(%rbp)
	movhps	-2992(%rbp), %xmm4
	movhps	-3008(%rbp), %xmm5
	movaps	%xmm3, -3088(%rbp)
	movaps	%xmm4, -3136(%rbp)
	movaps	%xmm5, -3120(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm2
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	movq	-3168(%rbp), %rdi
	leaq	48(%rax), %rdx
	movq	%rax, -2944(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	popq	%rcx
	popq	%rsi
	testq	%rdi, %rdi
	je	.L1020
	call	_ZdlPv@PLT
.L1020:
	movdqa	-3120(%rbp), %xmm7
	movdqa	-3136(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movdqa	-3088(%rbp), %xmm2
	movaps	%xmm0, -2944(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	$0, -2928(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm7
	leaq	48(%rax), %rdx
	movq	%rax, -2944(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	leaq	-704(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	movq	%rax, -3152(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1021
	call	_ZdlPv@PLT
.L1021:
	movq	-3216(%rbp), %rcx
	movq	-3104(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1104:
	movq	-3120(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3008(%rbp)
	movq	$0, -3000(%rbp)
	movq	$0, -2992(%rbp)
	movq	$0, -2984(%rbp)
	movq	$0, -2976(%rbp)
	movq	$0, -2944(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	-3056(%rbp), %rdi
	leaq	-2992(%rbp), %rcx
	pushq	%r13
	leaq	-2976(%rbp), %r9
	leaq	-2984(%rbp), %r8
	leaq	-3000(%rbp), %rdx
	leaq	-3008(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_
	movl	$41, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	popq	%rdi
	pushq	-2944(%rbp)
	movq	-2976(%rbp), %r9
	movq	-2984(%rbp), %r8
	movq	-2992(%rbp), %rcx
	movq	-3000(%rbp), %rdx
	movq	-3008(%rbp), %rsi
	movq	-3072(%rbp), %rdi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE9AddInputsENS1_5TNodeIS3_EENS9_IS4_EENS9_IS5_EENS9_IS6_EENS9_IS7_EESC_
	movq	-3200(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	popq	%r8
	popq	%r9
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L1106:
	movq	-3104(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3024(%rbp)
	leaq	-2976(%rbp), %r15
	movq	$0, -3016(%rbp)
	leaq	-128(%rbp), %r14
	movq	$0, -3008(%rbp)
	movq	$0, -3000(%rbp)
	movq	$0, -2992(%rbp)
	movq	$0, -2984(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-2984(%rbp), %rax
	movq	-3168(%rbp), %rdi
	pushq	%rax
	leaq	-2992(%rbp), %r9
	leaq	-3008(%rbp), %rcx
	leaq	-3000(%rbp), %r8
	leaq	-3016(%rbp), %rdx
	leaq	-3024(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_
	movl	$43, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2984(%rbp), %rcx
	movq	%r13, %r8
	movq	%rbx, %rdi
	movq	-2992(%rbp), %rdx
	movq	-3024(%rbp), %rsi
	call	_ZN2v88internal32LoadWithHoleCheck10FixedArray_19EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	-3024(%rbp), %rcx
	movq	-2992(%rbp), %rdx
	movq	%rax, %r8
	movaps	%xmm0, -2976(%rbp)
	movq	-2984(%rbp), %rax
	movq	%r8, -64(%rbp)
	movq	%rcx, -128(%rbp)
	movq	-3016(%rbp), %rcx
	movq	%rdx, -96(%rbp)
	movq	%rcx, -120(%rbp)
	movq	-3008(%rbp), %rcx
	movq	%rdx, -80(%rbp)
	leaq	-56(%rbp), %rdx
	movq	%rcx, -112(%rbp)
	movq	-3000(%rbp), %rcx
	movq	%rax, -88(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rcx, -104(%rbp)
	movq	$0, -2960(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3136(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2976(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L1023
	call	_ZdlPv@PLT
.L1023:
	movq	-3240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1856(%rbp), %rax
	cmpq	$0, -2936(%rbp)
	movq	%rax, -3088(%rbp)
	jne	.L1116
.L1024:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1107:
	movq	-3144(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2928(%rbp)
	movaps	%xmm0, -2944(%rbp)
	call	_Znwm@PLT
	movq	-3088(%rbp), %rdi
	movq	%r13, %rsi
	movabsq	$434322516333692679, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2944(%rbp)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2944(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L1027
	call	_ZdlPv@PLT
.L1027:
	movq	(%r14), %rax
	subq	$8, %rsp
	leaq	-1088(%rbp), %rdi
	movq	%rdi, -3104(%rbp)
	movq	(%rax), %rsi
	movq	8(%rax), %rdx
	movq	16(%rax), %rcx
	movq	24(%rax), %r8
	movq	32(%rax), %r9
	pushq	40(%rax)
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE9AddInputsENS1_5TNodeIS3_EENS9_IS4_EENS9_IS5_EENS9_IS6_EENS9_IS7_EESC_
	movq	-3232(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	popq	%r14
	popq	%r15
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1108:
	movq	-3240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -2928(%rbp)
	movaps	%xmm0, -2944(%rbp)
	call	_Znwm@PLT
	movq	-3136(%rbp), %rdi
	movq	%r13, %rsi
	movabsq	$434322516333692679, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -2944(%rbp)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2944(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L1029
	call	_ZdlPv@PLT
.L1029:
	movq	(%r14), %rax
	movq	8(%rax), %rsi
	movq	32(%rax), %rdx
	movq	40(%rax), %rdi
	movq	24(%rax), %r15
	movq	(%rax), %rcx
	movq	%rsi, -3144(%rbp)
	movq	16(%rax), %rsi
	movq	64(%rax), %r14
	movq	%rdx, -3280(%rbp)
	movl	$46, %edx
	movq	%rdi, -3296(%rbp)
	movq	%r12, %rdi
	movq	%rsi, -3264(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -3120(%rbp)
	movq	%r15, -3288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler11StrictEqualENS0_8compiler11SloppyTNodeINS0_6ObjectEEES5_PNS2_21CodeAssemblerVariableE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$47, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, -3240(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3240(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, -3240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %xmm3
	movq	%r14, %xmm6
	movq	%r13, %rdi
	punpcklqdq	%xmm3, %xmm6
	leaq	-128(%rbp), %r14
	leaq	-64(%rbp), %r15
	movq	-3280(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movq	%r15, %rdx
	movq	%r14, %rsi
	movaps	%xmm6, -80(%rbp)
	movq	-3264(%rbp), %xmm3
	movhps	-3296(%rbp), %xmm7
	movq	-3120(%rbp), %xmm4
	movaps	%xmm6, -3312(%rbp)
	movaps	%xmm7, -3280(%rbp)
	movhps	-3288(%rbp), %xmm3
	movhps	-3144(%rbp), %xmm4
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm3, -3264(%rbp)
	movaps	%xmm4, -3120(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm0, -2944(%rbp)
	movq	$0, -2928(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1472(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -3144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1030
	call	_ZdlPv@PLT
.L1030:
	movdqa	-3120(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movq	%r15, %rdx
	movdqa	-3264(%rbp), %xmm1
	movdqa	-3280(%rbp), %xmm2
	movq	%r14, %rsi
	movq	%r13, %rdi
	movaps	%xmm0, -2944(%rbp)
	movdqa	-3312(%rbp), %xmm5
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movq	$0, -2928(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1280(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -3120(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1031
	call	_ZdlPv@PLT
.L1031:
	movq	-3224(%rbp), %rcx
	movq	-3248(%rbp), %rdx
	movq	%r12, %rdi
	movq	-3240(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L1109:
	movq	-3248(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3032(%rbp)
	movq	$0, -3024(%rbp)
	movq	$0, -3016(%rbp)
	movq	$0, -3008(%rbp)
	movq	$0, -3000(%rbp)
	movq	$0, -2992(%rbp)
	movq	$0, -2984(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-2976(%rbp), %rax
	movq	-3144(%rbp), %rdi
	pushq	%rax
	leaq	-2984(%rbp), %rax
	leaq	-3016(%rbp), %rcx
	pushq	%rax
	leaq	-2992(%rbp), %rax
	leaq	-3000(%rbp), %r9
	pushq	%rax
	leaq	-3008(%rbp), %r8
	leaq	-3024(%rbp), %rdx
	leaq	-3032(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_S6_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SI_PNSA_IS8_EE
	addq	$32, %rsp
	movl	$49, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3032(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movaps	%xmm0, -2944(%rbp)
	movq	%rax, -128(%rbp)
	movq	-3024(%rbp), %rax
	movq	$0, -2928(%rbp)
	movq	%rax, -120(%rbp)
	movq	-3016(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-3008(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-2992(%rbp), %rax
	movq	%rax, -96(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	leaq	40(%rax), %rdx
	movq	%rax, -2944(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1033
	call	_ZdlPv@PLT
.L1033:
	movq	-3192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1110:
	movq	-3224(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3024(%rbp)
	movq	$0, -3016(%rbp)
	movq	$0, -3008(%rbp)
	movq	$0, -3000(%rbp)
	movq	$0, -2992(%rbp)
	movq	$0, -2984(%rbp)
	movq	$0, -2976(%rbp)
	movq	$0, -2944(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-2976(%rbp), %rax
	movq	-3120(%rbp), %rdi
	pushq	%r13
	leaq	-3008(%rbp), %rcx
	leaq	-2992(%rbp), %r9
	pushq	%rax
	leaq	-2984(%rbp), %rax
	leaq	-3000(%rbp), %r8
	pushq	%rax
	leaq	-3016(%rbp), %rdx
	leaq	-3024(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_S6_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SI_PNSA_IS8_EE
	addq	$32, %rsp
	movl	$42, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$52, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	subq	$8, %rsp
	movq	%r14, %rdi
	pushq	-2984(%rbp)
	movq	-2992(%rbp), %r9
	movq	-3000(%rbp), %r8
	movq	-3008(%rbp), %rcx
	movq	-3016(%rbp), %rdx
	movq	-3024(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE9AddInputsENS1_5TNodeIS3_EENS9_IS4_EENS9_IS5_EENS9_IS6_EENS9_IS7_EESC_
	movq	-3208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1024(%rbp)
	popq	%r10
	popq	%r11
	je	.L1035
.L1111:
	movq	-3232(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3008(%rbp)
	movq	$0, -3000(%rbp)
	movq	$0, -2992(%rbp)
	movq	$0, -2984(%rbp)
	movq	$0, -2976(%rbp)
	movq	$0, -2944(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	-3104(%rbp), %rdi
	leaq	-2992(%rbp), %rcx
	pushq	%r13
	leaq	-2976(%rbp), %r9
	leaq	-2984(%rbp), %r8
	leaq	-3000(%rbp), %rdx
	leaq	-3008(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_
	movl	$42, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	popq	%rdi
	movq	%r14, %rdi
	pushq	-2944(%rbp)
	movq	-2976(%rbp), %r9
	movq	-2984(%rbp), %r8
	movq	-2992(%rbp), %rcx
	movq	-3000(%rbp), %rdx
	movq	-3008(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE9AddInputsENS1_5TNodeIS3_EENS9_IS4_EENS9_IS5_EENS9_IS6_EENS9_IS7_EESC_
	movq	-3208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -832(%rbp)
	popq	%r8
	popq	%r9
	je	.L1036
.L1112:
	movq	-3208(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3016(%rbp)
	movq	$0, -3008(%rbp)
	movq	$0, -3000(%rbp)
	movq	$0, -2992(%rbp)
	movq	$0, -2984(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r14, %rdi
	leaq	-2976(%rbp), %rax
	pushq	%rax
	leaq	-3000(%rbp), %rcx
	leaq	-2984(%rbp), %r9
	leaq	-2992(%rbp), %r8
	leaq	-3008(%rbp), %rdx
	leaq	-3016(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_
	movl	$54, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, -3208(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3208(%rbp), %r9
	movq	-2976(%rbp), %r8
	movq	%r13, %rdi
	movq	%r9, %rsi
	movq	%r8, -3224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-3224(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, -3208(%rbp)
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-3208(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -3208(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$41, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3208(%rbp), %rax
	movq	-3000(%rbp), %rcx
	movq	-2984(%rbp), %r9
	movq	-2992(%rbp), %r8
	movq	-3008(%rbp), %rdx
	movq	-3016(%rbp), %rsi
	movq	%rax, (%rsp)
	movq	-3072(%rbp), %rdi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE9AddInputsENS1_5TNodeIS3_EENS9_IS4_EENS9_IS5_EENS9_IS6_EENS9_IS7_EESC_
	movq	-3200(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -640(%rbp)
	popq	%rcx
	popq	%rsi
	je	.L1037
.L1113:
	movq	-3216(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3016(%rbp)
	movq	$0, -3008(%rbp)
	movq	$0, -3000(%rbp)
	movq	$0, -2992(%rbp)
	movq	$0, -2984(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-2976(%rbp), %rax
	movq	-3152(%rbp), %rdi
	pushq	%rax
	leaq	-3000(%rbp), %rcx
	leaq	-2984(%rbp), %r9
	leaq	-2992(%rbp), %r8
	leaq	-3008(%rbp), %rdx
	leaq	-3016(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_
	movl	$58, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$-1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	-3016(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movaps	%xmm0, -2944(%rbp)
	movq	%rdx, -128(%rbp)
	movq	-3008(%rbp), %rdx
	movq	%rax, -96(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-3000(%rbp), %rdx
	movq	$0, -2928(%rbp)
	movq	%rdx, -112(%rbp)
	movq	-2992(%rbp), %rdx
	movq	%rdx, -104(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm4
	leaq	40(%rax), %rdx
	movq	%rax, -2944(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L1038
	call	_ZdlPv@PLT
.L1038:
	movq	-3192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1037
	.p2align 4,,10
	.p2align 3
.L1114:
	movq	-3192(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3008(%rbp)
	movq	$0, -3000(%rbp)
	movq	$0, -2992(%rbp)
	movq	$0, -2984(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2992(%rbp), %rcx
	movq	%r15, %rdi
	leaq	-2976(%rbp), %r9
	leaq	-2984(%rbp), %r8
	leaq	-3000(%rbp), %rdx
	leaq	-3008(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESE_
	movl	$93, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3008(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movaps	%xmm0, -2944(%rbp)
	movq	%rax, -128(%rbp)
	movq	-3000(%rbp), %rax
	movq	$0, -2928(%rbp)
	movq	%rax, -120(%rbp)
	movq	-2992(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-2984(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-2976(%rbp), %rax
	movq	%rax, -96(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm1
	movdqa	-112(%rbp), %xmm2
	leaq	40(%rax), %rdx
	movq	%rax, -2944(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm1, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1040
	call	_ZdlPv@PLT
.L1040:
	movq	-3184(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1039
	.p2align 4,,10
	.p2align 3
.L1116:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-64(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	-2992(%rbp), %xmm0
	movq	-3008(%rbp), %xmm1
	movq	$0, -2960(%rbp)
	movq	-3024(%rbp), %xmm2
	movhps	-2984(%rbp), %xmm0
	movhps	-3000(%rbp), %xmm1
	movhps	-3016(%rbp), %xmm2
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -2976(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3088(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2976(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1025
	call	_ZdlPv@PLT
.L1025:
	movq	-3144(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1024
.L1115:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22525:
	.size	_ZN2v88internal37FastArrayLastIndexOf10FixedArray_1423EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEENS4_INS0_3SmiEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal37FastArrayLastIndexOf10FixedArray_1423EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEENS4_INS0_3SmiEEENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal43FastArrayLastIndexOf16FixedDoubleArray_1424EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEENS4_INS0_3SmiEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal43FastArrayLastIndexOf16FixedDoubleArray_1424EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEENS4_INS0_3SmiEEENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal43FastArrayLastIndexOf16FixedDoubleArray_1424EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEENS4_INS0_3SmiEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal43FastArrayLastIndexOf16FixedDoubleArray_1424EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEENS4_INS0_3SmiEEENS4_INS0_6ObjectEEE:
.LFB22544:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-2760(%rbp), %r14
	leaq	-2816(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2944(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-3040(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$3272, %rsp
	movq	%rsi, -3056(%rbp)
	movq	%rdx, -3072(%rbp)
	movq	%rcx, -3088(%rbp)
	movq	%r8, -3096(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -3040(%rbp)
	movq	%rdi, -2816(%rbp)
	movl	$96, %edi
	movq	$0, -2808(%rbp)
	movq	$0, -2800(%rbp)
	movq	$0, -2792(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -2792(%rbp)
	movq	%rdx, -2800(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2776(%rbp)
	movq	%rax, -2808(%rbp)
	movq	$0, -2784(%rbp)
	movq	%r15, -3176(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3040(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2616(%rbp)
	movq	$0, -2608(%rbp)
	movq	%rax, -2624(%rbp)
	movq	$0, -2600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2616(%rbp)
	leaq	-2568(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2600(%rbp)
	movq	%rdx, -2608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2584(%rbp)
	movq	%rax, -3136(%rbp)
	movq	$0, -2592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3040(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2424(%rbp)
	movq	$0, -2416(%rbp)
	movq	%rax, -2432(%rbp)
	movq	$0, -2408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2424(%rbp)
	leaq	-2376(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2408(%rbp)
	movq	%rdx, -2416(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2392(%rbp)
	movq	%rax, -3120(%rbp)
	movq	$0, -2400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3040(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2232(%rbp)
	movq	$0, -2224(%rbp)
	movq	%rax, -2240(%rbp)
	movq	$0, -2216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2232(%rbp)
	leaq	-2184(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2216(%rbp)
	movq	%rdx, -2224(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2200(%rbp)
	movq	%rax, -3200(%rbp)
	movq	$0, -2208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3040(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movq	%rax, -2048(%rbp)
	movq	$0, -2024(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2040(%rbp)
	leaq	-1992(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2024(%rbp)
	movq	%rdx, -2032(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2008(%rbp)
	movq	%rax, -3104(%rbp)
	movq	$0, -2016(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3040(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1848(%rbp)
	movq	$0, -1840(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1832(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1848(%rbp)
	leaq	-1800(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1832(%rbp)
	movq	%rdx, -1840(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1816(%rbp)
	movq	%rax, -3144(%rbp)
	movq	$0, -1824(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3040(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1656(%rbp)
	movq	$0, -1648(%rbp)
	movq	%rax, -1664(%rbp)
	movq	$0, -1640(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1656(%rbp)
	leaq	-1608(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1640(%rbp)
	movq	%rdx, -1648(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1624(%rbp)
	movq	%rax, -3240(%rbp)
	movq	$0, -1632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3040(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1464(%rbp)
	movq	$0, -1456(%rbp)
	movq	%rax, -1472(%rbp)
	movq	$0, -1448(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1464(%rbp)
	leaq	-1416(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1448(%rbp)
	movq	%rdx, -1456(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1432(%rbp)
	movq	%rax, -3248(%rbp)
	movq	$0, -1440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3040(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1272(%rbp)
	movq	$0, -1264(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -1256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1272(%rbp)
	leaq	-1224(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1256(%rbp)
	movq	%rdx, -1264(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1240(%rbp)
	movq	%rax, -3224(%rbp)
	movq	$0, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3040(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1080(%rbp)
	movq	$0, -1072(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$0, -1064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1080(%rbp)
	leaq	-1032(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1064(%rbp)
	movq	%rdx, -1072(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1048(%rbp)
	movq	%rax, -3232(%rbp)
	movq	$0, -1056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3040(%rbp), %rax
	movl	$144, %edi
	movq	$0, -888(%rbp)
	movq	$0, -880(%rbp)
	movq	%rax, -896(%rbp)
	movq	$0, -872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -888(%rbp)
	leaq	-840(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -872(%rbp)
	movq	%rdx, -880(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -856(%rbp)
	movq	%rax, -3208(%rbp)
	movq	$0, -864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3040(%rbp), %rax
	movl	$144, %edi
	movq	$0, -696(%rbp)
	movq	$0, -688(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -696(%rbp)
	leaq	-648(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -680(%rbp)
	movq	%rdx, -688(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -664(%rbp)
	movq	%rax, -3216(%rbp)
	movq	$0, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3040(%rbp), %rax
	movl	$120, %edi
	movq	$0, -504(%rbp)
	movq	$0, -496(%rbp)
	movq	%rax, -512(%rbp)
	movq	$0, -488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -504(%rbp)
	leaq	-456(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -488(%rbp)
	movq	%rdx, -496(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -472(%rbp)
	movq	%rax, -3192(%rbp)
	movq	$0, -480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3040(%rbp), %rax
	movl	$120, %edi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -312(%rbp)
	leaq	-264(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	xorl	%edx, %edx
	movq	%rax, -3184(%rbp)
	movups	%xmm0, -280(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-3088(%rbp), %xmm1
	movq	-3056(%rbp), %xmm2
	movaps	%xmm0, -2944(%rbp)
	movhps	-3096(%rbp), %xmm1
	movq	$0, -2928(%rbp)
	movhps	-3072(%rbp), %xmm2
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -128(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm3
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm4
	leaq	32(%rax), %rdx
	movq	%rax, -2944(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1118
	call	_ZdlPv@PLT
.L1118:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2624(%rbp), %rax
	cmpq	$0, -2752(%rbp)
	movq	%rax, -3096(%rbp)
	leaq	-2432(%rbp), %rax
	movq	%rax, -3056(%rbp)
	jne	.L1208
	cmpq	$0, -2560(%rbp)
	jne	.L1209
.L1123:
	leaq	-2240(%rbp), %rax
	cmpq	$0, -2368(%rbp)
	movq	%rax, -3072(%rbp)
	jne	.L1210
.L1124:
	leaq	-2048(%rbp), %rax
	cmpq	$0, -2176(%rbp)
	movq	%rax, -3168(%rbp)
	leaq	-704(%rbp), %rax
	movq	%rax, -3152(%rbp)
	jne	.L1211
.L1125:
	leaq	-1664(%rbp), %rax
	cmpq	$0, -1984(%rbp)
	movq	%rax, -3136(%rbp)
	leaq	-1856(%rbp), %rax
	movq	%rax, -3088(%rbp)
	jne	.L1212
.L1128:
	leaq	-1088(%rbp), %rax
	cmpq	$0, -1792(%rbp)
	movq	%rax, -3104(%rbp)
	jne	.L1213
.L1132:
	leaq	-1472(%rbp), %rax
	cmpq	$0, -1600(%rbp)
	movq	%rax, -3144(%rbp)
	leaq	-1280(%rbp), %rax
	movq	%rax, -3120(%rbp)
	jne	.L1214
.L1134:
	cmpq	$0, -1408(%rbp)
	leaq	-512(%rbp), %r15
	jne	.L1215
.L1138:
	cmpq	$0, -1216(%rbp)
	leaq	-896(%rbp), %r14
	jne	.L1216
	cmpq	$0, -1024(%rbp)
	jne	.L1217
.L1141:
	cmpq	$0, -832(%rbp)
	jne	.L1218
.L1142:
	cmpq	$0, -640(%rbp)
	jne	.L1219
.L1143:
	cmpq	$0, -448(%rbp)
	leaq	-320(%rbp), %rbx
	jne	.L1220
.L1145:
	movq	-3184(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2928(%rbp)
	movaps	%xmm0, -2944(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	$134612743, (%rax)
	leaq	5(%rax), %rdx
	movb	$6, 4(%rax)
	movq	%rax, -2944(%rbp)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2944(%rbp), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L1147
	call	_ZdlPv@PLT
.L1147:
	movq	0(%r13), %rax
	movq	%rbx, %rdi
	movq	32(%rax), %r13
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3152(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3104(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3120(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3144(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3136(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3088(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3168(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3072(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3056(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3096(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3176(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1221
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1208:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2928(%rbp)
	movaps	%xmm0, -2944(%rbp)
	call	_Znwm@PLT
	movq	-3176(%rbp), %rdi
	movq	%r13, %rsi
	movl	$134612743, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2944(%rbp)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2944(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L1120
	call	_ZdlPv@PLT
.L1120:
	movq	(%r14), %rax
	movl	$30, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %r14
	movq	%rcx, -3072(%rbp)
	movq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%r14, -3088(%rbp)
	movq	%rcx, -3056(%rbp)
	movq	%rax, -3096(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE(%rip), %esi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$31, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$37, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-3056(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler24IntPtrGreaterThanOrEqualENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3056(%rbp), %rax
	movq	%r14, %xmm5
	movq	-3072(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movq	$0, -2928(%rbp)
	movq	%rax, %xmm6
	movhps	-3088(%rbp), %xmm7
	movaps	%xmm0, -2944(%rbp)
	punpcklqdq	%xmm6, %xmm5
	movhps	-3096(%rbp), %xmm6
	movaps	%xmm7, -3072(%rbp)
	movaps	%xmm5, -3056(%rbp)
	movaps	%xmm6, -3168(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm7
	leaq	48(%rax), %rdx
	movq	%rax, -2944(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	leaq	-2624(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	movq	%rax, -3096(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1121
	call	_ZdlPv@PLT
.L1121:
	movdqa	-3072(%rbp), %xmm1
	movdqa	-3168(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movdqa	-3056(%rbp), %xmm5
	movaps	%xmm0, -2944(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movq	$0, -2928(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm1
	leaq	48(%rax), %rdx
	movq	%rax, -2944(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm1, 32(%rax)
	leaq	-2432(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	movq	%rax, -3056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1122
	call	_ZdlPv@PLT
.L1122:
	movq	-3120(%rbp), %rcx
	movq	-3136(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2560(%rbp)
	je	.L1123
.L1209:
	movq	-3136(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3016(%rbp)
	movq	$0, -3008(%rbp)
	movq	$0, -3000(%rbp)
	movq	$0, -2992(%rbp)
	movq	$0, -2984(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-2976(%rbp), %rax
	movq	-3096(%rbp), %rdi
	pushq	%rax
	leaq	-2984(%rbp), %r9
	leaq	-2992(%rbp), %r8
	leaq	-3000(%rbp), %rcx
	leaq	-3008(%rbp), %rdx
	leaq	-3016(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_
	movl	$38, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	movq	-2984(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$37, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2984(%rbp), %r9
	movq	%r14, (%rsp)
	movq	-2992(%rbp), %r8
	movq	-3000(%rbp), %rcx
	movq	-3008(%rbp), %rdx
	movq	-3016(%rbp), %rsi
	movq	-3056(%rbp), %rdi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE9AddInputsENS1_5TNodeIS3_EENS9_IS4_EENS9_IS5_EENS9_IS6_EENS9_IS7_EESC_
	movq	-3120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	popq	%r10
	popq	%r11
	jmp	.L1123
	.p2align 4,,10
	.p2align 3
.L1211:
	movq	-3200(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3016(%rbp)
	movq	$0, -3008(%rbp)
	movq	$0, -3000(%rbp)
	movq	$0, -2992(%rbp)
	movq	$0, -2984(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-2976(%rbp), %rax
	movq	-3072(%rbp), %rdi
	pushq	%rax
	leaq	-3000(%rbp), %rcx
	leaq	-2984(%rbp), %r9
	leaq	-2992(%rbp), %r8
	leaq	-3008(%rbp), %rdx
	leaq	-3016(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2976(%rbp), %r15
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler24IntPtrGreaterThanOrEqualENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movq	-2984(%rbp), %xmm3
	movq	-3000(%rbp), %xmm4
	movq	-3016(%rbp), %xmm5
	movaps	%xmm0, -2944(%rbp)
	movhps	-2976(%rbp), %xmm3
	movq	$0, -2928(%rbp)
	movhps	-2992(%rbp), %xmm4
	movhps	-3008(%rbp), %xmm5
	movaps	%xmm3, -3088(%rbp)
	movaps	%xmm4, -3136(%rbp)
	movaps	%xmm5, -3120(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm2
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	movq	-3168(%rbp), %rdi
	leaq	48(%rax), %rdx
	movq	%rax, -2944(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	popq	%rcx
	popq	%rsi
	testq	%rdi, %rdi
	je	.L1126
	call	_ZdlPv@PLT
.L1126:
	movdqa	-3120(%rbp), %xmm7
	movdqa	-3136(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movdqa	-3088(%rbp), %xmm2
	movaps	%xmm0, -2944(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	$0, -2928(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm7
	leaq	48(%rax), %rdx
	movq	%rax, -2944(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	leaq	-704(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	movq	%rax, -3152(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1127
	call	_ZdlPv@PLT
.L1127:
	movq	-3216(%rbp), %rcx
	movq	-3104(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1125
	.p2align 4,,10
	.p2align 3
.L1210:
	movq	-3120(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3008(%rbp)
	movq	$0, -3000(%rbp)
	movq	$0, -2992(%rbp)
	movq	$0, -2984(%rbp)
	movq	$0, -2976(%rbp)
	movq	$0, -2944(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	-3056(%rbp), %rdi
	leaq	-2992(%rbp), %rcx
	pushq	%r13
	leaq	-2976(%rbp), %r9
	leaq	-2984(%rbp), %r8
	leaq	-3000(%rbp), %rdx
	leaq	-3008(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_
	movl	$41, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	popq	%rdi
	pushq	-2944(%rbp)
	movq	-2976(%rbp), %r9
	movq	-2984(%rbp), %r8
	movq	-2992(%rbp), %rcx
	movq	-3000(%rbp), %rdx
	movq	-3008(%rbp), %rsi
	movq	-3072(%rbp), %rdi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE9AddInputsENS1_5TNodeIS3_EENS9_IS4_EENS9_IS5_EENS9_IS6_EENS9_IS7_EESC_
	movq	-3200(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	popq	%r8
	popq	%r9
	jmp	.L1124
	.p2align 4,,10
	.p2align 3
.L1212:
	movq	-3104(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3024(%rbp)
	leaq	-2976(%rbp), %r15
	movq	$0, -3016(%rbp)
	leaq	-128(%rbp), %r14
	movq	$0, -3008(%rbp)
	movq	$0, -3000(%rbp)
	movq	$0, -2992(%rbp)
	movq	$0, -2984(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-2984(%rbp), %rax
	movq	-3168(%rbp), %rdi
	pushq	%rax
	leaq	-2992(%rbp), %r9
	leaq	-3008(%rbp), %rcx
	leaq	-3000(%rbp), %r8
	leaq	-3016(%rbp), %rdx
	leaq	-3024(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_
	movl	$43, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2984(%rbp), %rcx
	movq	%r13, %r8
	movq	%rbx, %rdi
	movq	-2992(%rbp), %rdx
	movq	-3024(%rbp), %rsi
	call	_ZN2v88internal38LoadWithHoleCheck16FixedDoubleArray_20EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	-3024(%rbp), %rcx
	movq	-2992(%rbp), %rdx
	movq	%rax, %r8
	movaps	%xmm0, -2976(%rbp)
	movq	-2984(%rbp), %rax
	movq	%r8, -64(%rbp)
	movq	%rcx, -128(%rbp)
	movq	-3016(%rbp), %rcx
	movq	%rdx, -96(%rbp)
	movq	%rcx, -120(%rbp)
	movq	-3008(%rbp), %rcx
	movq	%rdx, -80(%rbp)
	leaq	-56(%rbp), %rdx
	movq	%rcx, -112(%rbp)
	movq	-3000(%rbp), %rcx
	movq	%rax, -88(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rcx, -104(%rbp)
	movq	$0, -2960(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3136(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2976(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L1129
	call	_ZdlPv@PLT
.L1129:
	movq	-3240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1856(%rbp), %rax
	cmpq	$0, -2936(%rbp)
	movq	%rax, -3088(%rbp)
	jne	.L1222
.L1130:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1213:
	movq	-3144(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2928(%rbp)
	movaps	%xmm0, -2944(%rbp)
	call	_Znwm@PLT
	movq	-3088(%rbp), %rdi
	movq	%r13, %rsi
	movabsq	$434322516333692679, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2944(%rbp)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2944(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L1133
	call	_ZdlPv@PLT
.L1133:
	movq	(%r14), %rax
	subq	$8, %rsp
	leaq	-1088(%rbp), %rdi
	movq	%rdi, -3104(%rbp)
	movq	(%rax), %rsi
	movq	8(%rax), %rdx
	movq	16(%rax), %rcx
	movq	24(%rax), %r8
	movq	32(%rax), %r9
	pushq	40(%rax)
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE9AddInputsENS1_5TNodeIS3_EENS9_IS4_EENS9_IS5_EENS9_IS6_EENS9_IS7_EESC_
	movq	-3232(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	popq	%r14
	popq	%r15
	jmp	.L1132
	.p2align 4,,10
	.p2align 3
.L1214:
	movq	-3240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -2928(%rbp)
	movaps	%xmm0, -2944(%rbp)
	call	_Znwm@PLT
	movq	-3136(%rbp), %rdi
	movq	%r13, %rsi
	movabsq	$434322516333692679, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -2944(%rbp)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2944(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L1135
	call	_ZdlPv@PLT
.L1135:
	movq	(%r14), %rax
	movq	8(%rax), %rsi
	movq	32(%rax), %rdx
	movq	40(%rax), %rdi
	movq	24(%rax), %r15
	movq	(%rax), %rcx
	movq	%rsi, -3144(%rbp)
	movq	16(%rax), %rsi
	movq	64(%rax), %r14
	movq	%rdx, -3280(%rbp)
	movl	$46, %edx
	movq	%rdi, -3296(%rbp)
	movq	%r12, %rdi
	movq	%rsi, -3264(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -3120(%rbp)
	movq	%r15, -3288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler11StrictEqualENS0_8compiler11SloppyTNodeINS0_6ObjectEEES5_PNS2_21CodeAssemblerVariableE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$47, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, -3240(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3240(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, -3240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %xmm3
	movq	%r14, %xmm6
	movq	%r13, %rdi
	punpcklqdq	%xmm3, %xmm6
	leaq	-128(%rbp), %r14
	leaq	-64(%rbp), %r15
	movq	-3280(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movq	%r15, %rdx
	movq	%r14, %rsi
	movaps	%xmm6, -80(%rbp)
	movq	-3264(%rbp), %xmm3
	movhps	-3296(%rbp), %xmm7
	movq	-3120(%rbp), %xmm4
	movaps	%xmm6, -3312(%rbp)
	movaps	%xmm7, -3280(%rbp)
	movhps	-3288(%rbp), %xmm3
	movhps	-3144(%rbp), %xmm4
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm3, -3264(%rbp)
	movaps	%xmm4, -3120(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm0, -2944(%rbp)
	movq	$0, -2928(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1472(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -3144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1136
	call	_ZdlPv@PLT
.L1136:
	movdqa	-3120(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movq	%r15, %rdx
	movdqa	-3264(%rbp), %xmm1
	movdqa	-3280(%rbp), %xmm2
	movq	%r14, %rsi
	movq	%r13, %rdi
	movaps	%xmm0, -2944(%rbp)
	movdqa	-3312(%rbp), %xmm5
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movq	$0, -2928(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1280(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -3120(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1137
	call	_ZdlPv@PLT
.L1137:
	movq	-3224(%rbp), %rcx
	movq	-3248(%rbp), %rdx
	movq	%r12, %rdi
	movq	-3240(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1134
	.p2align 4,,10
	.p2align 3
.L1215:
	movq	-3248(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3032(%rbp)
	movq	$0, -3024(%rbp)
	movq	$0, -3016(%rbp)
	movq	$0, -3008(%rbp)
	movq	$0, -3000(%rbp)
	movq	$0, -2992(%rbp)
	movq	$0, -2984(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-2976(%rbp), %rax
	movq	-3144(%rbp), %rdi
	pushq	%rax
	leaq	-2984(%rbp), %rax
	leaq	-3016(%rbp), %rcx
	pushq	%rax
	leaq	-2992(%rbp), %rax
	leaq	-3000(%rbp), %r9
	pushq	%rax
	leaq	-3008(%rbp), %r8
	leaq	-3024(%rbp), %rdx
	leaq	-3032(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_S6_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SI_PNSA_IS8_EE
	addq	$32, %rsp
	movl	$49, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3032(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movaps	%xmm0, -2944(%rbp)
	movq	%rax, -128(%rbp)
	movq	-3024(%rbp), %rax
	movq	$0, -2928(%rbp)
	movq	%rax, -120(%rbp)
	movq	-3016(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-3008(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-2992(%rbp), %rax
	movq	%rax, -96(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	leaq	40(%rax), %rdx
	movq	%rax, -2944(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1139
	call	_ZdlPv@PLT
.L1139:
	movq	-3192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1216:
	movq	-3224(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3024(%rbp)
	movq	$0, -3016(%rbp)
	movq	$0, -3008(%rbp)
	movq	$0, -3000(%rbp)
	movq	$0, -2992(%rbp)
	movq	$0, -2984(%rbp)
	movq	$0, -2976(%rbp)
	movq	$0, -2944(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-2976(%rbp), %rax
	movq	-3120(%rbp), %rdi
	pushq	%r13
	leaq	-3008(%rbp), %rcx
	leaq	-2992(%rbp), %r9
	pushq	%rax
	leaq	-2984(%rbp), %rax
	leaq	-3000(%rbp), %r8
	pushq	%rax
	leaq	-3016(%rbp), %rdx
	leaq	-3024(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_S6_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SI_PNSA_IS8_EE
	addq	$32, %rsp
	movl	$42, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$52, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	subq	$8, %rsp
	movq	%r14, %rdi
	pushq	-2984(%rbp)
	movq	-2992(%rbp), %r9
	movq	-3000(%rbp), %r8
	movq	-3008(%rbp), %rcx
	movq	-3016(%rbp), %rdx
	movq	-3024(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE9AddInputsENS1_5TNodeIS3_EENS9_IS4_EENS9_IS5_EENS9_IS6_EENS9_IS7_EESC_
	movq	-3208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1024(%rbp)
	popq	%r10
	popq	%r11
	je	.L1141
.L1217:
	movq	-3232(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3008(%rbp)
	movq	$0, -3000(%rbp)
	movq	$0, -2992(%rbp)
	movq	$0, -2984(%rbp)
	movq	$0, -2976(%rbp)
	movq	$0, -2944(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	-3104(%rbp), %rdi
	leaq	-2992(%rbp), %rcx
	pushq	%r13
	leaq	-2976(%rbp), %r9
	leaq	-2984(%rbp), %r8
	leaq	-3000(%rbp), %rdx
	leaq	-3008(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_
	movl	$42, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	popq	%rdi
	movq	%r14, %rdi
	pushq	-2944(%rbp)
	movq	-2976(%rbp), %r9
	movq	-2984(%rbp), %r8
	movq	-2992(%rbp), %rcx
	movq	-3000(%rbp), %rdx
	movq	-3008(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE9AddInputsENS1_5TNodeIS3_EENS9_IS4_EENS9_IS5_EENS9_IS6_EENS9_IS7_EESC_
	movq	-3208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -832(%rbp)
	popq	%r8
	popq	%r9
	je	.L1142
.L1218:
	movq	-3208(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3016(%rbp)
	movq	$0, -3008(%rbp)
	movq	$0, -3000(%rbp)
	movq	$0, -2992(%rbp)
	movq	$0, -2984(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r14, %rdi
	leaq	-2976(%rbp), %rax
	pushq	%rax
	leaq	-3000(%rbp), %rcx
	leaq	-2984(%rbp), %r9
	leaq	-2992(%rbp), %r8
	leaq	-3008(%rbp), %rdx
	leaq	-3016(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_
	movl	$54, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, -3208(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3208(%rbp), %r9
	movq	-2976(%rbp), %r8
	movq	%r13, %rdi
	movq	%r9, %rsi
	movq	%r8, -3224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-3224(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, -3208(%rbp)
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-3208(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -3208(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$41, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3208(%rbp), %rax
	movq	-3000(%rbp), %rcx
	movq	-2984(%rbp), %r9
	movq	-2992(%rbp), %r8
	movq	-3008(%rbp), %rdx
	movq	-3016(%rbp), %rsi
	movq	%rax, (%rsp)
	movq	-3072(%rbp), %rdi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE9AddInputsENS1_5TNodeIS3_EENS9_IS4_EENS9_IS5_EENS9_IS6_EENS9_IS7_EESC_
	movq	-3200(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -640(%rbp)
	popq	%rcx
	popq	%rsi
	je	.L1143
.L1219:
	movq	-3216(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3016(%rbp)
	movq	$0, -3008(%rbp)
	movq	$0, -3000(%rbp)
	movq	$0, -2992(%rbp)
	movq	$0, -2984(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-2976(%rbp), %rax
	movq	-3152(%rbp), %rdi
	pushq	%rax
	leaq	-3000(%rbp), %rcx
	leaq	-2984(%rbp), %r9
	leaq	-2992(%rbp), %r8
	leaq	-3008(%rbp), %rdx
	leaq	-3016(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectENS0_14FixedArrayBaseES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_
	movl	$58, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$-1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	-3016(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movaps	%xmm0, -2944(%rbp)
	movq	%rdx, -128(%rbp)
	movq	-3008(%rbp), %rdx
	movq	%rax, -96(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-3000(%rbp), %rdx
	movq	$0, -2928(%rbp)
	movq	%rdx, -112(%rbp)
	movq	-2992(%rbp), %rdx
	movq	%rdx, -104(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm4
	leaq	40(%rax), %rdx
	movq	%rax, -2944(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L1144
	call	_ZdlPv@PLT
.L1144:
	movq	-3192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1143
	.p2align 4,,10
	.p2align 3
.L1220:
	movq	-3192(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3008(%rbp)
	movq	$0, -3000(%rbp)
	movq	$0, -2992(%rbp)
	movq	$0, -2984(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2992(%rbp), %rcx
	movq	%r15, %rdi
	leaq	-2976(%rbp), %r9
	leaq	-2984(%rbp), %r8
	leaq	-3000(%rbp), %rdx
	leaq	-3008(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSArrayENS0_3SmiENS0_6ObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESE_
	movl	$97, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3008(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movaps	%xmm0, -2944(%rbp)
	movq	%rax, -128(%rbp)
	movq	-3000(%rbp), %rax
	movq	$0, -2928(%rbp)
	movq	%rax, -120(%rbp)
	movq	-2992(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-2984(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-2976(%rbp), %rax
	movq	%rax, -96(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm1
	movdqa	-112(%rbp), %xmm2
	leaq	40(%rax), %rdx
	movq	%rax, -2944(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm1, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1146
	call	_ZdlPv@PLT
.L1146:
	movq	-3184(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1145
	.p2align 4,,10
	.p2align 3
.L1222:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-64(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	-2992(%rbp), %xmm0
	movq	-3008(%rbp), %xmm1
	movq	$0, -2960(%rbp)
	movq	-3024(%rbp), %xmm2
	movhps	-2984(%rbp), %xmm0
	movhps	-3000(%rbp), %xmm1
	movhps	-3016(%rbp), %xmm2
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -2976(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3088(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2976(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1131
	call	_ZdlPv@PLT
.L1131:
	movq	-3144(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1130
.L1221:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22544:
	.size	_ZN2v88internal43FastArrayLastIndexOf16FixedDoubleArray_1424EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEENS4_INS0_3SmiEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal43FastArrayLastIndexOf16FixedDoubleArray_1424EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEENS4_INS0_3SmiEEENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal26TryFastArrayLastIndexOf_22EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26TryFastArrayLastIndexOf_22EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEEPNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal26TryFastArrayLastIndexOf_22EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEEPNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal26TryFastArrayLastIndexOf_22EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEEPNS1_18CodeAssemblerLabelE:
.LFB22466:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-2432(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2560(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-2600(%rbp), %r12
	pushq	%rbx
	subq	$2792, %rsp
	.cfi_offset 3, -56
	movq	%r9, -2720(%rbp)
	movq	%rdi, -2616(%rbp)
	movq	%rsi, -2736(%rbp)
	movq	%rdx, -2752(%rbp)
	movq	%rcx, -2768(%rbp)
	movq	%r8, -2784(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -2600(%rbp)
	movq	%rdi, -2432(%rbp)
	movl	$96, %edi
	movq	$0, -2424(%rbp)
	movq	$0, -2416(%rbp)
	movq	$0, -2408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -2424(%rbp)
	leaq	-2376(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2408(%rbp)
	movq	%rdx, -2416(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2392(%rbp)
	movq	%rax, -2624(%rbp)
	movq	$0, -2400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2600(%rbp), %rax
	movl	$120, %edi
	movq	$0, -2232(%rbp)
	movq	$0, -2224(%rbp)
	movq	%rax, -2240(%rbp)
	movq	$0, -2216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -2232(%rbp)
	leaq	-2184(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2216(%rbp)
	movq	%rdx, -2224(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2200(%rbp)
	movq	%rax, -2704(%rbp)
	movq	$0, -2208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2600(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movq	%rax, -2048(%rbp)
	movq	$0, -2024(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2040(%rbp)
	leaq	-1992(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2024(%rbp)
	movq	%rdx, -2032(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2008(%rbp)
	movq	%rax, -2664(%rbp)
	movq	$0, -2016(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2600(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1848(%rbp)
	movq	$0, -1840(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1832(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1848(%rbp)
	leaq	-1800(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1832(%rbp)
	movq	%rdx, -1840(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1816(%rbp)
	movq	%rax, -2680(%rbp)
	movq	$0, -1824(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2600(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1656(%rbp)
	movq	$0, -1648(%rbp)
	movq	%rax, -1664(%rbp)
	movq	$0, -1640(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1656(%rbp)
	leaq	-1608(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1640(%rbp)
	movq	%rdx, -1648(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1624(%rbp)
	movq	%rax, -2696(%rbp)
	movq	$0, -1632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2600(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1464(%rbp)
	movq	$0, -1456(%rbp)
	movq	%rax, -1472(%rbp)
	movq	$0, -1448(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1464(%rbp)
	leaq	-1416(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1448(%rbp)
	movq	%rdx, -1456(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1432(%rbp)
	movq	%rax, -2712(%rbp)
	movq	$0, -1440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2600(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1272(%rbp)
	movq	$0, -1264(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -1256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1272(%rbp)
	leaq	-1224(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1256(%rbp)
	movq	%rdx, -1264(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1240(%rbp)
	movq	%rax, -2672(%rbp)
	movq	$0, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2600(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1080(%rbp)
	movq	$0, -1072(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$0, -1064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1080(%rbp)
	leaq	-1032(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1064(%rbp)
	movq	%rdx, -1072(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1048(%rbp)
	movq	%rax, -2688(%rbp)
	movq	$0, -1056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2600(%rbp), %rax
	movl	$192, %edi
	movq	$0, -888(%rbp)
	movq	$0, -880(%rbp)
	movq	%rax, -896(%rbp)
	movq	$0, -872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -888(%rbp)
	leaq	-840(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -872(%rbp)
	movq	%rdx, -880(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -856(%rbp)
	movq	%rax, -2656(%rbp)
	movq	$0, -864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2600(%rbp), %rax
	movl	$120, %edi
	movq	$0, -696(%rbp)
	movq	$0, -688(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movups	%xmm0, (%rax)
	leaq	120(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -696(%rbp)
	leaq	-648(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -680(%rbp)
	movq	%rdx, -688(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -664(%rbp)
	movq	%rax, -2640(%rbp)
	movq	$0, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2600(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	$0, -504(%rbp)
	movq	%rax, -512(%rbp)
	leaq	-456(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -2648(%rbp)
	movq	$0, -496(%rbp)
	movq	$0, -488(%rbp)
	movq	$0, -480(%rbp)
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2600(%rbp), %rax
	movl	$120, %edi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -312(%rbp)
	leaq	-264(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	xorl	%edx, %edx
	movq	%rax, -2632(%rbp)
	movups	%xmm0, -280(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-2768(%rbp), %xmm1
	movq	-2736(%rbp), %xmm2
	movaps	%xmm0, -2560(%rbp)
	movhps	-2784(%rbp), %xmm1
	movq	$0, -2544(%rbp)
	movhps	-2752(%rbp), %xmm2
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -128(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm3
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm4
	leaq	32(%rax), %rdx
	movq	%rax, -2560(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2544(%rbp)
	movq	%rdx, -2552(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2560(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1224
	call	_ZdlPv@PLT
.L1224:
	movq	-2624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2368(%rbp)
	jne	.L1516
	cmpq	$0, -2176(%rbp)
	jne	.L1517
.L1230:
	cmpq	$0, -1984(%rbp)
	jne	.L1518
.L1233:
	cmpq	$0, -1792(%rbp)
	jne	.L1519
.L1237:
	cmpq	$0, -1600(%rbp)
	jne	.L1520
.L1240:
	cmpq	$0, -1408(%rbp)
	jne	.L1521
.L1245:
	cmpq	$0, -1216(%rbp)
	jne	.L1522
.L1248:
	cmpq	$0, -1024(%rbp)
	jne	.L1523
.L1252:
	cmpq	$0, -832(%rbp)
	jne	.L1524
.L1257:
	cmpq	$0, -640(%rbp)
	leaq	-320(%rbp), %r14
	jne	.L1525
	cmpq	$0, -448(%rbp)
	jne	.L1526
.L1265:
	movq	-2632(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2544(%rbp)
	movaps	%xmm0, -2560(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134743815, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2560(%rbp)
	movq	%rdx, -2544(%rbp)
	movq	%rdx, -2552(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2560(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1267
	call	_ZdlPv@PLT
.L1267:
	movq	(%rbx), %rax
	movq	-2632(%rbp), %rdi
	movq	32(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1268
	call	_ZdlPv@PLT
.L1268:
	movq	-304(%rbp), %rbx
	movq	-312(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1269
	.p2align 4,,10
	.p2align 3
.L1273:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1270
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1273
.L1271:
	movq	-312(%rbp), %r14
.L1269:
	testq	%r14, %r14
	je	.L1274
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1274:
	movq	-2648(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1275
	call	_ZdlPv@PLT
.L1275:
	movq	-496(%rbp), %rbx
	movq	-504(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1276
	.p2align 4,,10
	.p2align 3
.L1280:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1277
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1280
.L1278:
	movq	-504(%rbp), %r14
.L1276:
	testq	%r14, %r14
	je	.L1281
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1281:
	movq	-2640(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1282
	call	_ZdlPv@PLT
.L1282:
	movq	-688(%rbp), %rbx
	movq	-696(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1283
	.p2align 4,,10
	.p2align 3
.L1287:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1284
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1287
.L1285:
	movq	-696(%rbp), %r14
.L1283:
	testq	%r14, %r14
	je	.L1288
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1288:
	movq	-2656(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-864(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1289
	call	_ZdlPv@PLT
.L1289:
	movq	-880(%rbp), %rbx
	movq	-888(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1290
	.p2align 4,,10
	.p2align 3
.L1294:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1291
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1294
.L1292:
	movq	-888(%rbp), %r14
.L1290:
	testq	%r14, %r14
	je	.L1295
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1295:
	movq	-2688(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1296
	call	_ZdlPv@PLT
.L1296:
	movq	-1072(%rbp), %rbx
	movq	-1080(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1297
	.p2align 4,,10
	.p2align 3
.L1301:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1298
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1301
.L1299:
	movq	-1080(%rbp), %r14
.L1297:
	testq	%r14, %r14
	je	.L1302
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1302:
	movq	-2672(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1303
	call	_ZdlPv@PLT
.L1303:
	movq	-1264(%rbp), %rbx
	movq	-1272(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1304
	.p2align 4,,10
	.p2align 3
.L1308:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1305
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1308
.L1306:
	movq	-1272(%rbp), %r14
.L1304:
	testq	%r14, %r14
	je	.L1309
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1309:
	movq	-2712(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1440(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1310
	call	_ZdlPv@PLT
.L1310:
	movq	-1456(%rbp), %rbx
	movq	-1464(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1311
	.p2align 4,,10
	.p2align 3
.L1315:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1312
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1315
.L1313:
	movq	-1464(%rbp), %r14
.L1311:
	testq	%r14, %r14
	je	.L1316
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1316:
	movq	-2696(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1632(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1317
	call	_ZdlPv@PLT
.L1317:
	movq	-1648(%rbp), %rbx
	movq	-1656(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1318
	.p2align 4,,10
	.p2align 3
.L1322:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1319
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1322
.L1320:
	movq	-1656(%rbp), %r14
.L1318:
	testq	%r14, %r14
	je	.L1323
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1323:
	movq	-2680(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1324
	call	_ZdlPv@PLT
.L1324:
	movq	-1840(%rbp), %rbx
	movq	-1848(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1325
	.p2align 4,,10
	.p2align 3
.L1329:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1326
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1329
.L1327:
	movq	-1848(%rbp), %r14
.L1325:
	testq	%r14, %r14
	je	.L1330
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1330:
	movq	-2664(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2016(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1331
	call	_ZdlPv@PLT
.L1331:
	movq	-2032(%rbp), %rbx
	movq	-2040(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1332
	.p2align 4,,10
	.p2align 3
.L1336:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1333
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1336
.L1334:
	movq	-2040(%rbp), %r14
.L1332:
	testq	%r14, %r14
	je	.L1337
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1337:
	movq	-2704(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1338
	call	_ZdlPv@PLT
.L1338:
	movq	-2224(%rbp), %rbx
	movq	-2232(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1339
	.p2align 4,,10
	.p2align 3
.L1343:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1340
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1343
.L1341:
	movq	-2232(%rbp), %r14
.L1339:
	testq	%r14, %r14
	je	.L1344
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1344:
	movq	-2624(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1345
	call	_ZdlPv@PLT
.L1345:
	movq	-2416(%rbp), %rbx
	movq	-2424(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1346
	.p2align 4,,10
	.p2align 3
.L1350:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1347
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%rbx, %r14
	jne	.L1350
.L1348:
	movq	-2424(%rbp), %r14
.L1346:
	testq	%r14, %r14
	je	.L1351
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1351:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1527
	addq	$2792, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1347:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1350
	jmp	.L1348
	.p2align 4,,10
	.p2align 3
.L1340:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1343
	jmp	.L1341
	.p2align 4,,10
	.p2align 3
.L1333:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1336
	jmp	.L1334
	.p2align 4,,10
	.p2align 3
.L1326:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1329
	jmp	.L1327
	.p2align 4,,10
	.p2align 3
.L1319:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1322
	jmp	.L1320
	.p2align 4,,10
	.p2align 3
.L1312:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1315
	jmp	.L1313
	.p2align 4,,10
	.p2align 3
.L1305:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1308
	jmp	.L1306
	.p2align 4,,10
	.p2align 3
.L1298:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1301
	jmp	.L1299
	.p2align 4,,10
	.p2align 3
.L1291:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1294
	jmp	.L1292
	.p2align 4,,10
	.p2align 3
.L1284:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1287
	jmp	.L1285
	.p2align 4,,10
	.p2align 3
.L1270:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1273
	jmp	.L1271
	.p2align 4,,10
	.p2align 3
.L1277:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1280
	jmp	.L1278
	.p2align 4,,10
	.p2align 3
.L1516:
	movq	-2624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2544(%rbp)
	movaps	%xmm0, -2560(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134743815, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2560(%rbp)
	movq	%rdx, -2544(%rbp)
	movq	%rdx, -2552(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2560(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1226
	call	_ZdlPv@PLT
.L1226:
	movq	(%rbx), %rax
	movl	$86, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r14
	movq	8(%rax), %rbx
	movq	16(%rax), %r15
	movq	24(%rax), %rax
	movq	%rax, -2736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%rbx, %rdx
	movq	-2616(%rbp), %rdi
	call	_ZN2v88internal23Cast13ATFastJSArray_135EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm7
	movq	%r15, %xmm5
	movq	%r14, %xmm6
	leaq	-128(%rbp), %r14
	punpcklqdq	%xmm7, %xmm6
	pxor	%xmm0, %xmm0
	movq	%rax, -88(%rbp)
	leaq	-2592(%rbp), %r15
	leaq	-80(%rbp), %rdx
	movq	%r14, %rsi
	movaps	%xmm6, -128(%rbp)
	movhps	-2736(%rbp), %xmm5
	movq	%r15, %rdi
	movq	%rbx, -96(%rbp)
	movaps	%xmm5, -2736(%rbp)
	movaps	%xmm6, -2752(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm0, -2592(%rbp)
	movq	$0, -2576(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2048(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1227
	call	_ZdlPv@PLT
.L1227:
	movq	-2664(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2552(%rbp)
	jne	.L1528
.L1228:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2176(%rbp)
	je	.L1230
.L1517:
	movq	-2704(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2240(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-123(%rbp), %rdx
	movaps	%xmm0, -2560(%rbp)
	movl	$134743815, -128(%rbp)
	movb	$7, -124(%rbp)
	movq	$0, -2544(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2560(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1231
	call	_ZdlPv@PLT
.L1231:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-512(%rbp), %rdi
	movq	$0, -2544(%rbp)
	movaps	%xmm0, -2560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2560(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1232
	call	_ZdlPv@PLT
.L1232:
	movq	-2648(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1984(%rbp)
	je	.L1233
.L1518:
	movq	-2664(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-128(%rbp), %r14
	leaq	-2048(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-122(%rbp), %rdx
	movq	%r14, %rsi
	movl	$1799, %edi
	movaps	%xmm0, -2560(%rbp)
	movw	%di, -124(%rbp)
	movq	%r13, %rdi
	movl	$134743815, -128(%rbp)
	movq	$0, -2544(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2560(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1234
	call	_ZdlPv@PLT
.L1234:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	24(%rax), %rdx
	movq	(%rax), %rcx
	movq	40(%rax), %r15
	movq	%rsi, -2768(%rbp)
	movq	16(%rax), %rsi
	movq	%rdx, -2792(%rbp)
	movl	$87, %edx
	movq	%rsi, -2784(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -2752(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2616(%rbp), %rbx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler21LoadFastJSArrayLengthENS0_8compiler11SloppyTNodeINS0_7JSArrayEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -2736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$88, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rdi
	xorl	%esi, %esi
	movq	%rbx, -2616(%rbp)
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	-2616(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-2736(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %xmm7
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	-80(%rbp), %r15
	movhps	-2736(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movq	-2784(%rbp), %xmm3
	movq	-2752(%rbp), %xmm4
	movq	%r15, %rdx
	movaps	%xmm7, -96(%rbp)
	movhps	-2792(%rbp), %xmm3
	movaps	%xmm7, -2736(%rbp)
	movhps	-2768(%rbp), %xmm4
	movaps	%xmm3, -2784(%rbp)
	movaps	%xmm4, -2752(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm0, -2560(%rbp)
	movq	$0, -2544(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1856(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2560(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1235
	call	_ZdlPv@PLT
.L1235:
	pxor	%xmm0, %xmm0
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movdqa	-2752(%rbp), %xmm2
	movdqa	-2784(%rbp), %xmm7
	movaps	%xmm0, -2560(%rbp)
	movq	$0, -2544(%rbp)
	movaps	%xmm2, -128(%rbp)
	movdqa	-2736(%rbp), %xmm2
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1664(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2560(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1236
	call	_ZdlPv@PLT
.L1236:
	movq	-2696(%rbp), %rcx
	movq	-2680(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1792(%rbp)
	je	.L1237
.L1519:
	movq	-2680(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1856(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2544(%rbp)
	movaps	%xmm0, -2560(%rbp)
	call	_Znwm@PLT
	movl	$1543, %esi
	movq	%r14, %rdi
	movw	%si, 4(%rax)
	leaq	6(%rax), %rdx
	movq	%r13, %rsi
	movl	$134743815, (%rax)
	movq	%rax, -2560(%rbp)
	movq	%rdx, -2544(%rbp)
	movq	%rdx, -2552(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2560(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1238
	call	_ZdlPv@PLT
.L1238:
	movq	(%rbx), %rax
	movq	-2616(%rbp), %rsi
	movq	%r13, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	16(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rcx, -2736(%rbp)
	movq	%rax, -2752(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$-1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %xmm0
	movl	$40, %edi
	movq	%r15, -96(%rbp)
	movhps	-2736(%rbp), %xmm0
	movq	$0, -2544(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	%rbx, %xmm0
	movhps	-2752(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2560(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm2
	leaq	40(%rax), %rdx
	leaq	-704(%rbp), %rdi
	movq	%rax, -2560(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -2544(%rbp)
	movq	%rdx, -2552(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2560(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1239
	call	_ZdlPv@PLT
.L1239:
	movq	-2640(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1600(%rbp)
	je	.L1240
.L1520:
	movq	-2696(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1664(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2544(%rbp)
	movaps	%xmm0, -2560(%rbp)
	call	_Znwm@PLT
	movl	$1543, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134743815, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -2560(%rbp)
	movq	%rdx, -2544(%rbp)
	movq	%rdx, -2552(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2560(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1241
	call	_ZdlPv@PLT
.L1241:
	movq	(%rbx), %rax
	movl	$90, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	24(%rax), %rbx
	movq	(%rax), %r15
	movq	16(%rax), %r14
	movq	%rcx, -2736(%rbp)
	movq	32(%rax), %rcx
	movq	40(%rax), %rax
	movq	%rcx, -2752(%rbp)
	movq	%rax, -2768(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2616(%rbp), %rdi
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal13Cast5ATSmi_83EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm6
	movq	%rbx, %xmm7
	movq	-2752(%rbp), %xmm5
	punpcklqdq	%xmm7, %xmm6
	leaq	-128(%rbp), %r14
	pxor	%xmm0, %xmm0
	movq	%rax, -72(%rbp)
	movq	%r15, %xmm7
	leaq	-64(%rbp), %rdx
	movq	%r14, %rsi
	movaps	%xmm6, -112(%rbp)
	leaq	-2592(%rbp), %r15
	movhps	-2768(%rbp), %xmm5
	movhps	-2736(%rbp), %xmm7
	movaps	%xmm6, -2752(%rbp)
	movq	%r15, %rdi
	movaps	%xmm5, -2768(%rbp)
	movaps	%xmm7, -2736(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm0, -2592(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -2576(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1280(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1242
	call	_ZdlPv@PLT
.L1242:
	movq	-2672(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2552(%rbp)
	jne	.L1529
.L1243:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1408(%rbp)
	je	.L1245
.L1521:
	movq	-2712(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1472(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %rsi
	movq	%r13, %rdi
	movl	$1543, %eax
	leaq	-121(%rbp), %rdx
	movb	$8, -122(%rbp)
	movw	%ax, -124(%rbp)
	movaps	%xmm0, -2560(%rbp)
	movl	$134743815, -128(%rbp)
	movq	$0, -2544(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2560(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1246
	call	_ZdlPv@PLT
.L1246:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-512(%rbp), %rdi
	movq	$0, -2544(%rbp)
	movaps	%xmm0, -2560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2560(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1247
	call	_ZdlPv@PLT
.L1247:
	movq	-2648(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1216(%rbp)
	je	.L1248
.L1522:
	movq	-2672(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-128(%rbp), %r14
	leaq	-1280(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-120(%rbp), %rdx
	movq	%r14, %rsi
	movabsq	$434603991310534407, %rax
	movq	%r13, %rdi
	movaps	%xmm0, -2560(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -2544(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2560(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1249
	call	_ZdlPv@PLT
.L1249:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	24(%rax), %rdx
	movq	(%rax), %rcx
	movq	%rsi, -2768(%rbp)
	movq	16(%rax), %rsi
	movq	%rdx, -2792(%rbp)
	movq	40(%rax), %rdx
	movq	%rcx, -2752(%rbp)
	movq	32(%rax), %rcx
	movq	56(%rax), %rax
	movq	%rsi, -2784(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -2816(%rbp)
	movl	$91, %edx
	movq	%rcx, -2736(%rbp)
	movq	%rax, -2832(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-2616(%rbp), %rbx
	movq	%r13, %rdi
	movq	%rax, %r15
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-2736(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler19LoadMapElementsKindENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$92, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler29IsFastSmiOrTaggedElementsKindENS0_8compiler5TNodeINS0_6Int32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %xmm4
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	-2832(%rbp), %xmm3
	leaq	-64(%rbp), %r15
	pxor	%xmm0, %xmm0
	movq	-2784(%rbp), %xmm5
	movq	-2752(%rbp), %xmm6
	movq	%r15, %rdx
	movaps	%xmm0, -2560(%rbp)
	punpcklqdq	%xmm4, %xmm3
	movq	-2736(%rbp), %xmm4
	movhps	-2792(%rbp), %xmm5
	movq	$0, -2544(%rbp)
	movhps	-2768(%rbp), %xmm6
	movaps	%xmm3, -2832(%rbp)
	movhps	-2816(%rbp), %xmm4
	movaps	%xmm5, -2784(%rbp)
	movaps	%xmm4, -2816(%rbp)
	movaps	%xmm6, -2736(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1088(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2560(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1250
	call	_ZdlPv@PLT
.L1250:
	pxor	%xmm0, %xmm0
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movdqa	-2736(%rbp), %xmm3
	movdqa	-2784(%rbp), %xmm2
	movaps	%xmm0, -2560(%rbp)
	movdqa	-2816(%rbp), %xmm7
	movq	$0, -2544(%rbp)
	movaps	%xmm3, -128(%rbp)
	movdqa	-2832(%rbp), %xmm3
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-896(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2560(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1251
	call	_ZdlPv@PLT
.L1251:
	movq	-2656(%rbp), %rcx
	movq	-2688(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1024(%rbp)
	je	.L1252
.L1523:
	movq	-2688(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	leaq	-1088(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2544(%rbp)
	movaps	%xmm0, -2560(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movabsq	$289925853281257223, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2560(%rbp)
	movq	%rdx, -2544(%rbp)
	movq	%rdx, -2552(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2560(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1253
	call	_ZdlPv@PLT
.L1253:
	movq	(%rbx), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	32(%rax), %rdx
	movq	16(%rax), %r8
	movq	(%rax), %rbx
	testq	%rdx, %rdx
	movq	%r8, %xmm0
	movq	%r8, -2752(%rbp)
	cmovne	%rdx, %r14
	movq	48(%rax), %rdx
	movq	%rbx, %xmm1
	movhps	24(%rax), %xmm0
	movhps	8(%rax), %xmm1
	movaps	%xmm0, -2736(%rbp)
	testq	%rdx, %rdx
	movaps	%xmm1, -2768(%rbp)
	cmovne	%rdx, %r15
	movl	$94, %edx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$93, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	-2752(%rbp), %r8
	movq	-2616(%rbp), %rdi
	call	_ZN2v88internal37FastArrayLastIndexOf10FixedArray_1423EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEENS4_INS0_3SmiEEENS4_INS0_6ObjectEEE
	movdqa	-2736(%rbp), %xmm0
	movl	$40, %edi
	movdqa	-2768(%rbp), %xmm1
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -2560(%rbp)
	movq	$0, -2544(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm2
	leaq	40(%rax), %rdx
	leaq	-704(%rbp), %rdi
	movq	%rax, -2560(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -2544(%rbp)
	movq	%rdx, -2552(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2560(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1256
	call	_ZdlPv@PLT
.L1256:
	movq	-2640(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -832(%rbp)
	je	.L1257
.L1524:
	movq	-2656(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	leaq	-896(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2544(%rbp)
	movaps	%xmm0, -2560(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movabsq	$289925853281257223, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2560(%rbp)
	movq	%rdx, -2544(%rbp)
	movq	%rdx, -2552(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2560(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1258
	call	_ZdlPv@PLT
.L1258:
	movq	(%rbx), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	32(%rax), %rdx
	movq	16(%rax), %r8
	movq	(%rax), %rbx
	testq	%rdx, %rdx
	movq	%r8, %xmm0
	movq	%r8, -2752(%rbp)
	cmovne	%rdx, %r14
	movq	48(%rax), %rdx
	movq	%rbx, %xmm1
	movhps	24(%rax), %xmm0
	movhps	8(%rax), %xmm1
	movaps	%xmm0, -2736(%rbp)
	testq	%rdx, %rdx
	movaps	%xmm1, -2768(%rbp)
	cmovne	%rdx, %r15
	movl	$98, %edx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$97, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	-2752(%rbp), %r8
	movq	-2616(%rbp), %rdi
	call	_ZN2v88internal43FastArrayLastIndexOf16FixedDoubleArray_1424EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEENS4_INS0_3SmiEEENS4_INS0_6ObjectEEE
	movdqa	-2736(%rbp), %xmm0
	movl	$40, %edi
	movdqa	-2768(%rbp), %xmm1
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -2560(%rbp)
	movq	$0, -2544(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm6
	leaq	40(%rax), %rdx
	leaq	-704(%rbp), %rdi
	movq	%rax, -2560(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -2544(%rbp)
	movq	%rdx, -2552(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2560(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1261
	call	_ZdlPv@PLT
.L1261:
	movq	-2640(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1257
	.p2align 4,,10
	.p2align 3
.L1525:
	movq	-2640(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-704(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2544(%rbp)
	movaps	%xmm0, -2560(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134743815, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2560(%rbp)
	movq	%rdx, -2544(%rbp)
	movq	%rdx, -2552(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2560(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1263
	call	_ZdlPv@PLT
.L1263:
	movq	(%rbx), %rax
	movl	$82, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	16(%rax), %rbx
	movq	32(%rax), %r15
	movq	%rcx, -2616(%rbp)
	movq	24(%rax), %rcx
	movq	%rcx, -2736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %xmm0
	movl	$40, %edi
	movq	%r15, -96(%rbp)
	movhps	-2616(%rbp), %xmm0
	leaq	-320(%rbp), %r14
	movq	$0, -2544(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	%rbx, %xmm0
	movhps	-2736(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2560(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm3
	leaq	40(%rax), %rdx
	movq	%rax, -2560(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -2544(%rbp)
	movq	%rdx, -2552(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2560(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1264
	call	_ZdlPv@PLT
.L1264:
	movq	-2632(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -448(%rbp)
	je	.L1265
.L1526:
	movq	-2648(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-512(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	$0, -2544(%rbp)
	movaps	%xmm0, -2560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2560(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1266
	call	_ZdlPv@PLT
.L1266:
	movq	-2720(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1528:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-88(%rbp), %rdx
	movq	%r14, %rsi
	movdqa	-2752(%rbp), %xmm4
	movq	%r15, %rdi
	movaps	%xmm0, -2592(%rbp)
	movq	%rbx, -96(%rbp)
	movaps	%xmm4, -128(%rbp)
	movdqa	-2736(%rbp), %xmm4
	movq	$0, -2576(%rbp)
	movaps	%xmm4, -112(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2240(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1229
	call	_ZdlPv@PLT
.L1229:
	movq	-2704(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1228
	.p2align 4,,10
	.p2align 3
.L1529:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-72(%rbp), %rdx
	movq	%r14, %rsi
	movdqa	-2736(%rbp), %xmm2
	movq	%r15, %rdi
	movq	%rbx, -80(%rbp)
	movdqa	-2752(%rbp), %xmm4
	movaps	%xmm0, -2592(%rbp)
	movaps	%xmm2, -128(%rbp)
	movdqa	-2768(%rbp), %xmm2
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	$0, -2576(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1472(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1244
	call	_ZdlPv@PLT
.L1244:
	movq	-2712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1243
.L1527:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22466:
	.size	_ZN2v88internal26TryFastArrayLastIndexOf_22EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEEPNS1_18CodeAssemblerLabelE, .-_ZN2v88internal26TryFastArrayLastIndexOf_22EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEEPNS1_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal34ArrayPrototypeLastIndexOfAssembler37GenerateArrayPrototypeLastIndexOfImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal34ArrayPrototypeLastIndexOfAssembler37GenerateArrayPrototypeLastIndexOfImplEv
	.type	_ZN2v88internal34ArrayPrototypeLastIndexOfAssembler37GenerateArrayPrototypeLastIndexOfImplEv, @function
_ZN2v88internal34ArrayPrototypeLastIndexOfAssembler37GenerateArrayPrototypeLastIndexOfImplEv:
.LFB22509:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1784, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, -1640(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r13, %rcx
	movq	%r12, %rsi
	leaq	-1632(%rbp), %rdi
	movq	%rax, %rdx
	leaq	-1328(%rbp), %r13
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-1624(%rbp), %rcx
	movl	$2, %esi
	movq	%r12, %rdi
	movq	-1632(%rbp), %rax
	movq	-1616(%rbp), %rbx
	movq	%r12, -1504(%rbp)
	leaq	-1640(%rbp), %r12
	movq	%rcx, -1728(%rbp)
	movq	%rcx, -1480(%rbp)
	movq	$1, -1496(%rbp)
	movq	%rbx, -1488(%rbp)
	movq	%rax, -1744(%rbp)
	movq	%rax, -1472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-1504(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -1752(%rbp)
	movq	%rax, -1712(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -1320(%rbp)
	movq	$0, -1312(%rbp)
	movq	%rax, %r15
	movq	-1640(%rbp), %rax
	movq	$0, -1304(%rbp)
	movq	%rax, -1328(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1272(%rbp), %rcx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -1304(%rbp)
	movq	%rdx, -1312(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1656(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1288(%rbp)
	movq	%rax, -1320(%rbp)
	movq	$0, -1296(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1640(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1128(%rbp)
	movq	$0, -1120(%rbp)
	movq	%rax, -1136(%rbp)
	movq	$0, -1112(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1080(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -1112(%rbp)
	movq	%rdx, -1120(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1680(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1096(%rbp)
	movq	%rax, -1128(%rbp)
	movq	$0, -1104(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1640(%rbp), %rax
	movl	$168, %edi
	movq	$0, -936(%rbp)
	movq	$0, -928(%rbp)
	movq	%rax, -944(%rbp)
	movq	$0, -920(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-888(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -920(%rbp)
	movq	%rdx, -928(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1664(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -904(%rbp)
	movq	%rax, -936(%rbp)
	movq	$0, -912(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1640(%rbp), %rax
	movl	$312, %edi
	movq	$0, -744(%rbp)
	movq	$0, -736(%rbp)
	movq	%rax, -752(%rbp)
	movq	$0, -728(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-696(%rbp), %rcx
	movq	%r12, %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rdx, -728(%rbp)
	movq	%rdx, -736(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1696(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -712(%rbp)
	movq	%rax, -744(%rbp)
	movq	$0, -720(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1640(%rbp), %rax
	movl	$336, %edi
	movq	$0, -552(%rbp)
	movq	$0, -544(%rbp)
	movq	%rax, -560(%rbp)
	movq	$0, -536(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-504(%rbp), %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rdx, -536(%rbp)
	movq	%rdx, -544(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -1672(%rbp)
	movq	%r12, %rsi
	movups	%xmm0, -520(%rbp)
	movq	%rax, -552(%rbp)
	movq	$0, -528(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1640(%rbp), %rax
	movl	$216, %edi
	movq	$0, -360(%rbp)
	movq	$0, -352(%rbp)
	movq	%rax, -368(%rbp)
	movq	$0, -344(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-312(%rbp), %rcx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -344(%rbp)
	movq	%rdx, -352(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1688(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -360(%rbp)
	movups	%xmm0, -328(%rbp)
	movq	$0, -336(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movq	-1744(%rbp), %xmm1
	movl	$40, %edi
	movq	%r15, -144(%rbp)
	leaq	-1456(%rbp), %r15
	movhps	-1728(%rbp), %xmm1
	movaps	%xmm0, -1456(%rbp)
	movaps	%xmm1, -176(%rbp)
	movq	%rbx, %xmm1
	movhps	-1712(%rbp), %xmm1
	movq	$0, -1440(%rbp)
	movaps	%xmm1, -160(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r13, %rdi
	movq	%r15, %rsi
	movdqa	-176(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm5
	leaq	40(%rax), %rdx
	movq	%rax, -1456(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1440(%rbp)
	movq	%rdx, -1448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1456(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1531
	call	_ZdlPv@PLT
.L1531:
	movq	-1656(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1264(%rbp)
	jne	.L1678
	cmpq	$0, -1072(%rbp)
	jne	.L1679
.L1536:
	cmpq	$0, -880(%rbp)
	jne	.L1680
.L1538:
	cmpq	$0, -688(%rbp)
	jne	.L1681
.L1543:
	cmpq	$0, -496(%rbp)
	jne	.L1682
.L1546:
	cmpq	$0, -304(%rbp)
	jne	.L1683
.L1548:
	movq	-1688(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1554
	call	_ZdlPv@PLT
.L1554:
	movq	-352(%rbp), %rbx
	movq	-360(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1555
	.p2align 4,,10
	.p2align 3
.L1559:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1556
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1559
.L1557:
	movq	-360(%rbp), %r13
.L1555:
	testq	%r13, %r13
	je	.L1560
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1560:
	movq	-1672(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1561
	call	_ZdlPv@PLT
.L1561:
	movq	-544(%rbp), %rbx
	movq	-552(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1562
	.p2align 4,,10
	.p2align 3
.L1566:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1563
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1566
.L1564:
	movq	-552(%rbp), %r13
.L1562:
	testq	%r13, %r13
	je	.L1567
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1567:
	movq	-1696(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-720(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1568
	call	_ZdlPv@PLT
.L1568:
	movq	-736(%rbp), %rbx
	movq	-744(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1569
	.p2align 4,,10
	.p2align 3
.L1573:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1570
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1573
.L1571:
	movq	-744(%rbp), %r13
.L1569:
	testq	%r13, %r13
	je	.L1574
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1574:
	movq	-1664(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-912(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1575
	call	_ZdlPv@PLT
.L1575:
	movq	-928(%rbp), %rbx
	movq	-936(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1576
	.p2align 4,,10
	.p2align 3
.L1580:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1577
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1580
.L1578:
	movq	-936(%rbp), %r13
.L1576:
	testq	%r13, %r13
	je	.L1581
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1581:
	movq	-1680(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1582
	call	_ZdlPv@PLT
.L1582:
	movq	-1120(%rbp), %rbx
	movq	-1128(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1583
	.p2align 4,,10
	.p2align 3
.L1587:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1584
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1587
.L1585:
	movq	-1128(%rbp), %r13
.L1583:
	testq	%r13, %r13
	je	.L1588
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1588:
	movq	-1656(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1296(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1589
	call	_ZdlPv@PLT
.L1589:
	movq	-1312(%rbp), %rbx
	movq	-1320(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1590
	.p2align 4,,10
	.p2align 3
.L1594:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1591
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1594
.L1592:
	movq	-1320(%rbp), %r13
.L1590:
	testq	%r13, %r13
	je	.L1595
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1595:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1684
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1591:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1594
	jmp	.L1592
	.p2align 4,,10
	.p2align 3
.L1584:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1587
	jmp	.L1585
	.p2align 4,,10
	.p2align 3
.L1577:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1580
	jmp	.L1578
	.p2align 4,,10
	.p2align 3
.L1570:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1573
	jmp	.L1571
	.p2align 4,,10
	.p2align 3
.L1556:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1559
	jmp	.L1557
	.p2align 4,,10
	.p2align 3
.L1563:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1566
	jmp	.L1564
	.p2align 4,,10
	.p2align 3
.L1678:
	movq	-1656(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1440(%rbp)
	movaps	%xmm0, -1456(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -1456(%rbp)
	movq	%rdx, -1440(%rbp)
	movq	%rdx, -1448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1456(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1533
	call	_ZdlPv@PLT
.L1533:
	movq	(%rbx), %rax
	movl	$136, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rsi
	movq	24(%rax), %r13
	movq	%rcx, -1776(%rbp)
	movq	16(%rax), %rcx
	movq	32(%rax), %rax
	movq	%rsi, -1744(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -1792(%rbp)
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rbx, -1728(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15ToObject_InlineENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	movq	%rax, -1808(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$139, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal21GetLengthProperty_244EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$142, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r15, %rdi
	movq	%rax, -1712(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1712(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal16IsNumberEqual_73EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	movq	%r13, %xmm3
	pxor	%xmm0, %xmm0
	movq	-1728(%rbp), %xmm6
	movq	-1792(%rbp), %xmm7
	movl	$56, %edi
	movq	%rbx, -128(%rbp)
	movq	-1744(%rbp), %xmm2
	movhps	-1808(%rbp), %xmm6
	movaps	%xmm0, -1456(%rbp)
	punpcklqdq	%xmm3, %xmm7
	movaps	%xmm6, -1728(%rbp)
	movhps	-1776(%rbp), %xmm2
	movaps	%xmm7, -1792(%rbp)
	movaps	%xmm2, -1744(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movq	%rax, -1712(%rbp)
	movq	$0, -1440(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-176(%rbp), %xmm3
	movdqa	-160(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm5
	leaq	56(%rax), %rdx
	leaq	-1136(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rax, -1456(%rbp)
	movq	%rdx, -1440(%rbp)
	movq	%rdx, -1448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1456(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1534
	call	_ZdlPv@PLT
.L1534:
	movdqa	-1744(%rbp), %xmm3
	movdqa	-1792(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movdqa	-1728(%rbp), %xmm5
	movq	%rbx, -128(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm0, -1456(%rbp)
	movq	$0, -1440(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-176(%rbp), %xmm3
	movdqa	-160(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm4
	leaq	56(%rax), %rdx
	leaq	-944(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movq	%rax, -1456(%rbp)
	movq	%rdx, -1440(%rbp)
	movq	%rdx, -1448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1456(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1535
	call	_ZdlPv@PLT
.L1535:
	movq	-1664(%rbp), %rcx
	movq	-1680(%rbp), %rdx
	movq	%r12, %rdi
	movq	-1712(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1072(%rbp)
	je	.L1536
.L1679:
	movq	-1680(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1136(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -1440(%rbp)
	movaps	%xmm0, -1456(%rbp)
	call	_Znwm@PLT
	movl	$1800, %edi
	movq	%r15, %rsi
	movw	%di, 4(%rax)
	leaq	7(%rax), %rdx
	movq	%r13, %rdi
	movl	$117769477, (%rax)
	movb	$8, 6(%rax)
	movq	%rax, -1456(%rbp)
	movq	%rdx, -1440(%rbp)
	movq	%rdx, -1448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1456(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1537
	call	_ZdlPv@PLT
.L1537:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$-1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1752(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	cmpq	$0, -880(%rbp)
	je	.L1538
.L1680:
	movq	-1664(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-944(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -1440(%rbp)
	movaps	%xmm0, -1456(%rbp)
	call	_Znwm@PLT
	movl	$1800, %esi
	movq	%r13, %rdi
	movw	%si, 4(%rax)
	leaq	7(%rax), %rdx
	movq	%r15, %rsi
	movl	$117769477, (%rax)
	movb	$8, 6(%rax)
	movq	%rax, -1456(%rbp)
	movq	%rdx, -1440(%rbp)
	movq	%rdx, -1448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1456(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1539
	call	_ZdlPv@PLT
.L1539:
	movq	(%rbx), %rax
	movq	32(%rax), %rdx
	movq	16(%rax), %rsi
	movq	40(%rax), %rdi
	movq	24(%rax), %rcx
	movq	(%rax), %r13
	movq	%rdx, -1760(%rbp)
	movq	48(%rax), %rdx
	movq	8(%rax), %rbx
	movq	%rsi, -1728(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdi, -1776(%rbp)
	movq	%r12, %rdi
	movq	%rdx, -1792(%rbp)
	movl	$145, %edx
	movq	%rcx, -1712(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm5
	subq	$8, %rsp
	movq	%r14, %rdi
	movq	-1728(%rbp), %rsi
	movq	%r13, %xmm3
	movq	-1792(%rbp), %rdx
	punpcklqdq	%xmm5, %xmm3
	movaps	%xmm3, -1600(%rbp)
	movq	%rsi, -1584(%rbp)
	movq	-1712(%rbp), %rsi
	pushq	-1584(%rbp)
	pushq	-1592(%rbp)
	pushq	-1600(%rbp)
	movaps	%xmm3, -1744(%rbp)
	call	_ZN2v88internal15GetFromIndex_21EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEENS0_21TorqueStructArgumentsE
	movl	$147, %edx
	addq	$32, %rsp
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r13, %rsi
	movq	%r15, %rdi
	movdqa	-1744(%rbp), %xmm3
	movq	-1728(%rbp), %rax
	movaps	%xmm3, -1568(%rbp)
	movq	%rax, -1552(%rbp)
	pushq	-1552(%rbp)
	pushq	-1560(%rbp)
	pushq	-1568(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r15, %rdi
	movq	%rax, -1808(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$150, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %r9
	movq	%rbx, %r8
	movq	%r14, %rdi
	movq	-1776(%rbp), %r13
	movq	-1808(%rbp), %rcx
	movq	-1712(%rbp), %rsi
	movq	%r13, %rdx
	call	_ZN2v88internal26TryFastArrayLastIndexOf_22EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEEPNS1_18CodeAssemblerLabelE
	movq	%r13, %xmm4
	movq	%rbx, %xmm2
	movq	-1808(%rbp), %xmm6
	movdqa	-1744(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movl	$112, %edi
	movq	-1760(%rbp), %xmm7
	punpcklqdq	%xmm6, %xmm4
	movdqa	%xmm6, %xmm5
	movaps	%xmm0, -1536(%rbp)
	movq	-1792(%rbp), %xmm6
	movhps	-1712(%rbp), %xmm5
	movhps	-1776(%rbp), %xmm7
	movaps	%xmm4, -96(%rbp)
	leaq	-1536(%rbp), %r13
	punpcklqdq	%xmm2, %xmm6
	movaps	%xmm4, -1824(%rbp)
	movq	-1728(%rbp), %xmm2
	movaps	%xmm5, -1808(%rbp)
	movhps	-1712(%rbp), %xmm2
	movaps	%xmm6, -1792(%rbp)
	movaps	%xmm7, -1776(%rbp)
	movaps	%xmm2, -1712(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movq	%rbx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -1520(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm2
	leaq	112(%rax), %rdx
	leaq	-560(%rbp), %rdi
	movdqa	-160(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm4
	movups	%xmm3, (%rax)
	movdqa	-80(%rbp), %xmm3
	movups	%xmm5, 48(%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm6, 64(%rax)
	movups	%xmm2, 80(%rax)
	movups	%xmm3, 96(%rax)
	movq	%rax, -1536(%rbp)
	movq	%rdx, -1520(%rbp)
	movq	%rdx, -1528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1540
	call	_ZdlPv@PLT
.L1540:
	movq	-1672(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1448(%rbp)
	jne	.L1685
.L1541:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -688(%rbp)
	je	.L1543
.L1681:
	movq	-1696(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-752(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$13, %edi
	movq	$0, -1440(%rbp)
	movaps	%xmm0, -1456(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movabsq	$578720283176011013, %rsi
	movq	%rsi, (%rax)
	leaq	13(%rax), %rdx
	movq	%r15, %rsi
	movl	$134678280, 8(%rax)
	movb	$8, 12(%rax)
	movq	%rax, -1456(%rbp)
	movq	%rdx, -1440(%rbp)
	movq	%rdx, -1448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1456(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1544
	call	_ZdlPv@PLT
.L1544:
	movq	(%rbx), %rax
	movl	$72, %edi
	movdqu	48(%rax), %xmm0
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	64(%rax), %rdx
	movdqu	(%rax), %xmm7
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -112(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -1456(%rbp)
	movq	$0, -1440(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-176(%rbp), %xmm4
	movdqa	-128(%rbp), %xmm2
	movdqa	-160(%rbp), %xmm5
	leaq	72(%rax), %rdx
	leaq	-368(%rbp), %rdi
	movdqa	-144(%rbp), %xmm6
	movq	%rcx, 64(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm2, 48(%rax)
	movq	%rax, -1456(%rbp)
	movq	%rdx, -1440(%rbp)
	movq	%rdx, -1448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1456(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1545
	call	_ZdlPv@PLT
.L1545:
	movq	-1688(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -496(%rbp)
	je	.L1546
.L1682:
	movq	-1672(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-560(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$14, %edi
	movq	$0, -1440(%rbp)
	movaps	%xmm0, -1456(%rbp)
	call	_Znwm@PLT
	movl	$2056, %ecx
	movq	%r13, %rdi
	movabsq	$578720283176011013, %rsi
	movq	%rsi, (%rax)
	leaq	14(%rax), %rdx
	movq	%r15, %rsi
	movl	$134678280, 8(%rax)
	movw	%cx, 12(%rax)
	movq	%rax, -1456(%rbp)
	movq	%rdx, -1440(%rbp)
	movq	%rdx, -1448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1456(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1547
	call	_ZdlPv@PLT
.L1547:
	movq	(%rbx), %rax
	movq	-1752(%rbp), %rdi
	movq	104(%rax), %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	cmpq	$0, -304(%rbp)
	je	.L1548
.L1683:
	movq	-1688(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	leaq	-368(%rbp), %r8
	movq	$0, -1744(%rbp)
	movq	%r8, -1728(%rbp)
	movq	$0, -1712(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -1440(%rbp)
	movaps	%xmm0, -1456(%rbp)
	call	_Znwm@PLT
	movq	-1728(%rbp), %r8
	movabsq	$578720283176011013, %rsi
	movq	%rsi, (%rax)
	leaq	9(%rax), %rdx
	movq	%r15, %rsi
	movb	$8, 8(%rax)
	movq	%r8, %rdi
	movq	%rax, -1456(%rbp)
	movq	%rdx, -1440(%rbp)
	movq	%rdx, -1448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1456(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1549
	movq	%rax, -1728(%rbp)
	call	_ZdlPv@PLT
	movq	-1728(%rbp), %rax
.L1549:
	movq	(%rax), %rax
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	cmovne	%rdx, %r13
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	cmove	-1744(%rbp), %rdx
	movq	%rdx, %r15
	movq	56(%rax), %rdx
	movq	64(%rax), %rax
	testq	%rdx, %rdx
	cmovne	%rdx, %rbx
	testq	%rax, %rax
	cmove	-1712(%rbp), %rax
	movl	$154, %edx
	movq	%rax, -1712(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rbx, %r8
	movq	-1712(%rbp), %rcx
	movq	%r15, %rdx
	call	_ZN2v88internal26GenericArrayLastIndexOf_23EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE
	movq	-1752(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	jmp	.L1548
	.p2align 4,,10
	.p2align 3
.L1685:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-1744(%rbp), %xmm3
	movq	%r13, %rdi
	movdqa	-1712(%rbp), %xmm7
	movdqa	-1776(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	leaq	-72(%rbp), %rdx
	movdqa	-1792(%rbp), %xmm5
	movaps	%xmm3, -176(%rbp)
	movdqa	-1808(%rbp), %xmm3
	leaq	-176(%rbp), %rsi
	movaps	%xmm7, -160(%rbp)
	movdqa	-1824(%rbp), %xmm7
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm0, -1536(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -1520(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-752(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1542
	call	_ZdlPv@PLT
.L1542:
	movq	-1696(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1541
.L1684:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22509:
	.size	_ZN2v88internal34ArrayPrototypeLastIndexOfAssembler37GenerateArrayPrototypeLastIndexOfImplEv, .-_ZN2v88internal34ArrayPrototypeLastIndexOfAssembler37GenerateArrayPrototypeLastIndexOfImplEv
	.section	.rodata._ZN2v88internal8Builtins34Generate_ArrayPrototypeLastIndexOfEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/array-lastindexof-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins34Generate_ArrayPrototypeLastIndexOfEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"ArrayPrototypeLastIndexOf"
	.section	.text._ZN2v88internal8Builtins34Generate_ArrayPrototypeLastIndexOfEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins34Generate_ArrayPrototypeLastIndexOfEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins34Generate_ArrayPrototypeLastIndexOfEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins34Generate_ArrayPrototypeLastIndexOfEPNS0_8compiler18CodeAssemblerStateE:
.LFB22505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1006, %ecx
	leaq	.LC5(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$760, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L1690
.L1687:
	movq	%r13, %rdi
	call	_ZN2v88internal34ArrayPrototypeLastIndexOfAssembler37GenerateArrayPrototypeLastIndexOfImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1691
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1690:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L1687
.L1691:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22505:
	.size	_ZN2v88internal8Builtins34Generate_ArrayPrototypeLastIndexOfEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins34Generate_ArrayPrototypeLastIndexOfEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal32LoadWithHoleCheck10FixedArray_19EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal32LoadWithHoleCheck10FixedArray_19EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE, @function
_GLOBAL__sub_I__ZN2v88internal32LoadWithHoleCheck10FixedArray_19EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE:
.LFB29599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29599:
	.size	_GLOBAL__sub_I__ZN2v88internal32LoadWithHoleCheck10FixedArray_19EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE, .-_GLOBAL__sub_I__ZN2v88internal32LoadWithHoleCheck10FixedArray_19EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal32LoadWithHoleCheck10FixedArray_19EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE
	.weak	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.byte	7
	.byte	7
	.byte	6
	.byte	7
	.byte	7
	.byte	5
	.byte	5
	.byte	6
	.byte	6
	.byte	5
	.byte	7
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	7
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
