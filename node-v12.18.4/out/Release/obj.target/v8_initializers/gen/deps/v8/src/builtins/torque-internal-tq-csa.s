	.file	"torque-internal-tq-csa.cc"
	.text
	.section	.rodata._ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../../deps/v8/../../deps/v8/src/builtins/torque-internal.tq"
	.section	.text._ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE:
.LFB22410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-656(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-568(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-664(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -56
	leaq	-624(%rbp), %rbx
	subq	$648, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -664(%rbp)
	movq	%rdi, -624(%rbp)
	movq	%r13, %rdi
	movq	$0, -616(%rbp)
	movq	$0, -608(%rbp)
	movq	$0, -600(%rbp)
	movq	$0, -592(%rbp)
	movq	$0, -584(%rbp)
	movq	$0, -576(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	$0, -424(%rbp)
	movq	%rax, -432(%rbp)
	leaq	-376(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -680(%rbp)
	movq	$0, -416(%rbp)
	movq	$0, -408(%rbp)
	movq	$0, -400(%rbp)
	movq	$0, -392(%rbp)
	movq	$0, -384(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	$0, -232(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -192(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2
	call	_ZdlPv@PLT
.L2:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -560(%rbp)
	jne	.L71
.L3:
	cmpq	$0, -368(%rbp)
	leaq	-240(%rbp), %rbx
	jne	.L72
.L6:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L9
	call	_ZdlPv@PLT
.L9:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L11
	.p2align 4,,10
	.p2align 3
.L15:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L12
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L15
.L13:
	movq	-232(%rbp), %r14
.L11:
	testq	%r14, %r14
	je	.L16
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L16:
	movq	-680(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L17
	call	_ZdlPv@PLT
.L17:
	movq	-416(%rbp), %rbx
	movq	-424(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L18
	.p2align 4,,10
	.p2align 3
.L22:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L19
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L22
.L20:
	movq	-424(%rbp), %r14
.L18:
	testq	%r14, %r14
	je	.L23
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L23:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L24
	call	_ZdlPv@PLT
.L24:
	movq	-608(%rbp), %rbx
	movq	-616(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L25
	.p2align 4,,10
	.p2align 3
.L29:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L26
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L29
.L27:
	movq	-616(%rbp), %r13
.L25:
	testq	%r13, %r13
	je	.L30
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L30:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L73
	addq	$648, %rsp
	movl	$8, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L29
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L19:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L22
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L12:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L15
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L71:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4
	call	_ZdlPv@PLT
.L4:
	movl	$10, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm0, %xmm0
	leaq	-432(%rbp), %rdi
	movq	%r15, %rsi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5
	call	_ZdlPv@PLT
.L5:
	movq	-680(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L72:
	movq	-680(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-432(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L7
	call	_ZdlPv@PLT
.L7:
	movl	$9, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-240(%rbp), %rbx
	pxor	%xmm0, %xmm0
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movaps	%xmm0, -656(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L8
	call	_ZdlPv@PLT
.L8:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L6
.L73:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22410:
	.size	_ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal20SizeOf9ATfloat64_340EPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20SizeOf9ATfloat64_340EPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal20SizeOf9ATfloat64_340EPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal20SizeOf9ATfloat64_340EPNS0_8compiler18CodeAssemblerStateE:
.LFB22417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-656(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-568(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-664(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -56
	leaq	-624(%rbp), %rbx
	subq	$648, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -664(%rbp)
	movq	%rdi, -624(%rbp)
	movq	%r13, %rdi
	movq	$0, -616(%rbp)
	movq	$0, -608(%rbp)
	movq	$0, -600(%rbp)
	movq	$0, -592(%rbp)
	movq	$0, -584(%rbp)
	movq	$0, -576(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	$0, -424(%rbp)
	movq	%rax, -432(%rbp)
	leaq	-376(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -680(%rbp)
	movq	$0, -416(%rbp)
	movq	$0, -408(%rbp)
	movq	$0, -400(%rbp)
	movq	$0, -392(%rbp)
	movq	$0, -384(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	$0, -232(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -192(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L75
	call	_ZdlPv@PLT
.L75:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -560(%rbp)
	jne	.L143
.L76:
	cmpq	$0, -368(%rbp)
	leaq	-240(%rbp), %rbx
	jne	.L144
.L79:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L82
	call	_ZdlPv@PLT
.L82:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L83
	call	_ZdlPv@PLT
.L83:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L84
	.p2align 4,,10
	.p2align 3
.L88:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L85
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L88
.L86:
	movq	-232(%rbp), %r14
.L84:
	testq	%r14, %r14
	je	.L89
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L89:
	movq	-680(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L90
	call	_ZdlPv@PLT
.L90:
	movq	-416(%rbp), %rbx
	movq	-424(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L91
	.p2align 4,,10
	.p2align 3
.L95:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L92
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L95
.L93:
	movq	-424(%rbp), %r14
.L91:
	testq	%r14, %r14
	je	.L96
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L96:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L97
	call	_ZdlPv@PLT
.L97:
	movq	-608(%rbp), %rbx
	movq	-616(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L98
	.p2align 4,,10
	.p2align 3
.L102:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L99
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L102
.L100:
	movq	-616(%rbp), %r13
.L98:
	testq	%r13, %r13
	je	.L103
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L103:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L145
	addq	$648, %rsp
	movl	$8, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L102
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L92:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L95
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L85:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L88
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L143:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L77
	call	_ZdlPv@PLT
.L77:
	movl	$13, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm0, %xmm0
	leaq	-432(%rbp), %rdi
	movq	%r15, %rsi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L78
	call	_ZdlPv@PLT
.L78:
	movq	-680(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L144:
	movq	-680(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-432(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L80
	call	_ZdlPv@PLT
.L80:
	movl	$12, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-240(%rbp), %rbx
	pxor	%xmm0, %xmm0
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movaps	%xmm0, -656(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L81
	call	_ZdlPv@PLT
.L81:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L79
.L145:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22417:
	.size	_ZN2v88internal20SizeOf9ATfloat64_340EPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal20SizeOf9ATfloat64_340EPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal45UnsafeNewReference20UT5ATSmi10HeapObject_1411EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal45UnsafeNewReference20UT5ATSmi10HeapObject_1411EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE
	.type	_ZN2v88internal45UnsafeNewReference20UT5ATSmi10HeapObject_1411EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE, @function
_ZN2v88internal45UnsafeNewReference20UT5ATSmi10HeapObject_1411EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE:
.LFB22418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	leaq	-672(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-616(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-712(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-704(%rbp), %rbx
	subq	$728, %rsp
	movq	%rdi, -728(%rbp)
	movl	$48, %edi
	movq	%rdx, -752(%rbp)
	movq	%rcx, -760(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -712(%rbp)
	movq	%rsi, -672(%rbp)
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -664(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-712(%rbp), %rax
	movl	$96, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -736(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-712(%rbp), %rax
	movl	$96, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-752(%rbp), %xmm1
	movaps	%xmm0, -704(%rbp)
	movhps	-760(%rbp), %xmm1
	movq	$0, -688(%rbp)
	movaps	%xmm1, -752(%rbp)
	call	_Znwm@PLT
	movdqa	-752(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -704(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L147
	call	_ZdlPv@PLT
.L147:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -608(%rbp)
	jne	.L215
.L148:
	cmpq	$0, -416(%rbp)
	leaq	-288(%rbp), %r15
	jne	.L216
.L151:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -688(%rbp)
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	$84346119, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -704(%rbp)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-704(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L154
	call	_ZdlPv@PLT
.L154:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movdqu	16(%rax), %xmm2
	movq	-728(%rbp), %rax
	movups	%xmm2, (%rax)
	movaps	%xmm2, -752(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L155
	call	_ZdlPv@PLT
.L155:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L156
	.p2align 4,,10
	.p2align 3
.L160:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L157
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L160
.L158:
	movq	-280(%rbp), %r14
.L156:
	testq	%r14, %r14
	je	.L161
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L161:
	movq	-736(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L162
	call	_ZdlPv@PLT
.L162:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L163
	.p2align 4,,10
	.p2align 3
.L167:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L164
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L167
.L165:
	movq	-472(%rbp), %r14
.L163:
	testq	%r14, %r14
	je	.L168
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L168:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L169
	call	_ZdlPv@PLT
.L169:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L170
	.p2align 4,,10
	.p2align 3
.L174:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L171
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L174
.L172:
	movq	-664(%rbp), %r13
.L170:
	testq	%r13, %r13
	je	.L175
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L175:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L217
	movq	-728(%rbp), %rax
	addq	$728, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L174
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L157:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L160
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L164:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L167
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L215:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -688(%rbp)
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movl	$1287, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -704(%rbp)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-704(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L149
	call	_ZdlPv@PLT
.L149:
	movq	(%r15), %rax
	movl	$29, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r15
	movq	8(%rax), %rax
	movq	%rax, -752(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$30, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$28, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %xmm0
	movl	$32, %edi
	movq	$0, -688(%rbp)
	movhps	-752(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movq	%rbx, %rsi
	leaq	32(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -704(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L150
	call	_ZdlPv@PLT
.L150:
	movq	-736(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L216:
	movq	-736(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -688(%rbp)
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movl	$84346119, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -704(%rbp)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-704(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L152
	call	_ZdlPv@PLT
.L152:
	movq	(%r15), %rax
	movl	$38, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	%rcx, -752(%rbp)
	movq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, -760(%rbp)
	movq	%rax, -768(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %xmm0
	movl	$32, %edi
	movq	$0, -688(%rbp)
	movhps	-752(%rbp), %xmm0
	leaq	-288(%rbp), %r15
	movaps	%xmm0, -96(%rbp)
	movq	-760(%rbp), %xmm0
	movhps	-768(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movdqa	-80(%rbp), %xmm7
	leaq	32(%rax), %rdx
	movq	%rax, -704(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L153
	call	_ZdlPv@PLT
.L153:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L151
.L217:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22418:
	.size	_ZN2v88internal45UnsafeNewReference20UT5ATSmi10HeapObject_1411EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE, .-_ZN2v88internal45UnsafeNewReference20UT5ATSmi10HeapObject_1411EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE
	.section	.rodata._ZN2v88internal40Convert8ATintptr17ATconstexpr_int31_1415EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../../deps/v8/../../deps/v8/src/builtins/base.tq"
	.section	.text._ZN2v88internal40Convert8ATintptr17ATconstexpr_int31_1415EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal40Convert8ATintptr17ATconstexpr_int31_1415EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE
	.type	_ZN2v88internal40Convert8ATintptr17ATconstexpr_int31_1415EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE, @function
_ZN2v88internal40Convert8ATintptr17ATconstexpr_int31_1415EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE:
.LFB22425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-656(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-568(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-664(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-376(%rbp), %rbx
	subq	$664, %rsp
	movq	%rdi, -688(%rbp)
	movl	%esi, -692(%rbp)
	movq	%r12, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-624(%rbp), %rax
	movq	%rdi, -664(%rbp)
	movq	%rdi, -624(%rbp)
	movq	%r13, %rdi
	movq	%rax, -680(%rbp)
	movq	$0, -616(%rbp)
	movq	$0, -608(%rbp)
	movq	$0, -600(%rbp)
	movq	$0, -592(%rbp)
	movq	$0, -584(%rbp)
	movq	$0, -576(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$24, %edi
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	%rax, -432(%rbp)
	movq	$0, -408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movq	$0, 16(%rax)
	movq	%rdx, -408(%rbp)
	movq	%rdx, -416(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -392(%rbp)
	movq	%rax, -424(%rbp)
	movq	$0, -400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$24, %edi
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	$0, 16(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-680(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r15, %rsi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L219
	call	_ZdlPv@PLT
.L219:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -560(%rbp)
	jne	.L287
.L220:
	leaq	-240(%rbp), %rax
	cmpq	$0, -368(%rbp)
	movq	%rax, -680(%rbp)
	jne	.L288
.L223:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-680(%rbp), %rdi
	movq	%r15, %rsi
	movb	$5, (%rax)
	leaq	1(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L226
	call	_ZdlPv@PLT
.L226:
	movq	(%r15), %rax
	movq	%r14, %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L227
	call	_ZdlPv@PLT
.L227:
	movq	-224(%rbp), %rax
	movq	-232(%rbp), %r14
	cmpq	%r14, %rax
	je	.L228
	.p2align 4,,10
	.p2align 3
.L232:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L229
	movq	%rax, -680(%rbp)
	addq	$24, %r14
	call	_ZdlPv@PLT
	movq	-680(%rbp), %rax
	cmpq	%r14, %rax
	jne	.L232
.L230:
	movq	-232(%rbp), %r14
.L228:
	testq	%r14, %r14
	je	.L233
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L233:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L234
	call	_ZdlPv@PLT
.L234:
	movq	-416(%rbp), %rbx
	movq	-424(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L235
	.p2align 4,,10
	.p2align 3
.L239:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L236
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L239
.L237:
	movq	-424(%rbp), %r14
.L235:
	testq	%r14, %r14
	je	.L240
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L240:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L241
	call	_ZdlPv@PLT
.L241:
	movq	-608(%rbp), %rbx
	movq	-616(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L242
	.p2align 4,,10
	.p2align 3
.L246:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L243
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L246
.L244:
	movq	-616(%rbp), %r13
.L242:
	testq	%r13, %r13
	je	.L247
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L247:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L289
	addq	$664, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L246
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L229:
	addq	$24, %r14
	cmpq	%r14, %rax
	jne	.L232
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L236:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L239
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L287:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-680(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r15, %rsi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L221
	call	_ZdlPv@PLT
.L221:
	movl	$2650, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	-692(%rbp), %esi
	movq	-688(%rbp), %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	movq	%rax, -680(%rbp)
	call	_Znwm@PLT
	movq	-680(%rbp), %rdx
	leaq	-432(%rbp), %rdi
	movq	%r15, %rsi
	leaq	8(%rax), %rcx
	movq	%rax, -656(%rbp)
	movq	%rdx, (%rax)
	movq	%rcx, -640(%rbp)
	movq	%rcx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L222
	call	_ZdlPv@PLT
.L222:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L288:
	leaq	-432(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r8, -680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-680(%rbp), %r8
	movq	%r15, %rsi
	movb	$5, (%rax)
	leaq	1(%rax), %rdx
	movq	%r8, %rdi
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L224
	movq	%rax, -680(%rbp)
	call	_ZdlPv@PLT
	movq	-680(%rbp), %rax
.L224:
	movq	(%rax), %rax
	movl	$50, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	%rcx, -680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-680(%rbp), %rcx
	movq	%r15, %rsi
	leaq	8(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rcx, (%rax)
	leaq	-240(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	movq	%rax, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L225
	call	_ZdlPv@PLT
.L225:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L223
.L289:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22425:
	.size	_ZN2v88internal40Convert8ATintptr17ATconstexpr_int31_1415EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE, .-_ZN2v88internal40Convert8ATintptr17ATconstexpr_int31_1415EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE
	.section	.text._ZN2v88internal33UnsafeNewReference9ATfloat64_1426EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33UnsafeNewReference9ATfloat64_1426EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE
	.type	_ZN2v88internal33UnsafeNewReference9ATfloat64_1426EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE, @function
_ZN2v88internal33UnsafeNewReference9ATfloat64_1426EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE:
.LFB22429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	leaq	-672(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-616(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-712(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-704(%rbp), %rbx
	subq	$728, %rsp
	movq	%rdi, -728(%rbp)
	movl	$48, %edi
	movq	%rdx, -752(%rbp)
	movq	%rcx, -760(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -712(%rbp)
	movq	%rsi, -672(%rbp)
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -664(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-712(%rbp), %rax
	movl	$96, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -736(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-712(%rbp), %rax
	movl	$96, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-752(%rbp), %xmm1
	movaps	%xmm0, -704(%rbp)
	movhps	-760(%rbp), %xmm1
	movq	$0, -688(%rbp)
	movaps	%xmm1, -752(%rbp)
	call	_Znwm@PLT
	movdqa	-752(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -704(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L291
	call	_ZdlPv@PLT
.L291:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -608(%rbp)
	jne	.L359
.L292:
	cmpq	$0, -416(%rbp)
	leaq	-288(%rbp), %r15
	jne	.L360
.L295:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -688(%rbp)
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	$84346119, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -704(%rbp)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-704(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L298
	call	_ZdlPv@PLT
.L298:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movdqu	16(%rax), %xmm2
	movq	-728(%rbp), %rax
	movups	%xmm2, (%rax)
	movaps	%xmm2, -752(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L299
	call	_ZdlPv@PLT
.L299:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L300
	.p2align 4,,10
	.p2align 3
.L304:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L301
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L304
.L302:
	movq	-280(%rbp), %r14
.L300:
	testq	%r14, %r14
	je	.L305
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L305:
	movq	-736(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L306
	call	_ZdlPv@PLT
.L306:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L307
	.p2align 4,,10
	.p2align 3
.L311:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L308
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L311
.L309:
	movq	-472(%rbp), %r14
.L307:
	testq	%r14, %r14
	je	.L312
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L312:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L313
	call	_ZdlPv@PLT
.L313:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L314
	.p2align 4,,10
	.p2align 3
.L318:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L315
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L318
.L316:
	movq	-664(%rbp), %r13
.L314:
	testq	%r13, %r13
	je	.L319
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L319:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L361
	movq	-728(%rbp), %rax
	addq	$728, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L318
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L301:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L304
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L308:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L311
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L359:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -688(%rbp)
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movl	$1287, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -704(%rbp)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-704(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L293
	call	_ZdlPv@PLT
.L293:
	movq	(%r15), %rax
	movl	$29, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r15
	movq	8(%rax), %rax
	movq	%rax, -752(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$30, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$28, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %xmm0
	movl	$32, %edi
	movq	$0, -688(%rbp)
	movhps	-752(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movq	%rbx, %rsi
	leaq	32(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -704(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L294
	call	_ZdlPv@PLT
.L294:
	movq	-736(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L360:
	movq	-736(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -688(%rbp)
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movl	$84346119, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -704(%rbp)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-704(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L296
	call	_ZdlPv@PLT
.L296:
	movq	(%r15), %rax
	movl	$38, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	%rcx, -752(%rbp)
	movq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, -760(%rbp)
	movq	%rax, -768(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %xmm0
	movl	$32, %edi
	movq	$0, -688(%rbp)
	movhps	-752(%rbp), %xmm0
	leaq	-288(%rbp), %r15
	movaps	%xmm0, -96(%rbp)
	movq	-760(%rbp), %xmm0
	movhps	-768(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movdqa	-80(%rbp), %xmm7
	leaq	32(%rax), %rdx
	movq	%rax, -704(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L297
	call	_ZdlPv@PLT
.L297:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L295
.L361:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22429:
	.size	_ZN2v88internal33UnsafeNewReference9ATfloat64_1426EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE, .-_ZN2v88internal33UnsafeNewReference9ATfloat64_1426EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE:
.LFB28862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28862:
	.size	_GLOBAL__sub_I__ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
