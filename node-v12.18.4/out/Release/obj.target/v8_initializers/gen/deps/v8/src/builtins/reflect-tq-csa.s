	.file	"reflect-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0:
.LFB29349:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	%rsi, %r13
	pushq	%rbx
	movq	%r13, %rax
	sarq	$3, %rax
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%r13, %r13
	js	.L14
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%rdx, %r14
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.L9
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%rax, %rdi
.L9:
	leaq	(%rdi,%r13), %r15
	movq	%rdi, (%rbx)
	movq	%r15, 16(%rbx)
	cmpq	%r12, %r14
	je	.L10
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
.L10:
	movq	%r15, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29349:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0:
.LFB29347:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	movabsq	$384307168202282325, %rax
	cmpq	%rax, %rdx
	ja	.L32
	pxor	%xmm0, %xmm0
	leaq	(%rdx,%rdx,2), %r14
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	%rdx, %r12
	movups	%xmm0, 8(%rdi)
	salq	$3, %r14
	xorl	%esi, %esi
	movq	$0, 24(%rdi)
	testq	%rdx, %rdx
	je	.L21
	movq	%r14, %rdi
	call	_Znwm@PLT
	leaq	-1(%r12), %rdx
	leaq	(%rax,%r14), %rsi
	movq	%rax, 8(%rbx)
	movq	%rsi, 24(%rbx)
	cmpq	$2, %rdx
	jbe	.L22
	movq	%r12, %rdi
	movq	%r12, %rcx
	pxor	%xmm0, %xmm0
	movq	%rax, %rdx
	shrq	%rdi
	andq	$-2, %rcx
	addq	%rdi, %rcx
	salq	$4, %rcx
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L19:
	movups	%xmm0, (%rdx)
	addq	$48, %rdx
	movups	%xmm0, -32(%rdx)
	movups	%xmm0, -16(%rdx)
	cmpq	%rcx, %rdx
	jne	.L19
	movq	%r12, %rdx
	movq	%r12, %rcx
	andq	$-2, %rdx
	andl	$1, %ecx
	leaq	(%rdx,%rdx,2), %rdi
	leaq	(%rax,%rdi,8), %rax
	cmpq	%rdx, %r12
	je	.L21
.L18:
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	cmpq	$1, %rcx
	je	.L21
	movq	$0, 40(%rax)
	movups	%xmm0, 24(%rax)
	cmpq	$2, %rcx
	je	.L21
	movq	$0, 64(%rax)
	movups	%xmm0, 48(%rax)
.L21:
	movq	%rsi, 16(%rbx)
	pxor	%xmm0, %xmm0
	leaq	56(%rbx), %rdi
	movq	%r13, %rsi
	movups	%xmm0, 32(%rbx)
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	$0, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movq	%r12, %rcx
	jmp	.L18
.L32:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29347:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev,"axG",@progbits,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev:
.LFB22421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$56, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L34
	call	_ZdlPv@PLT
.L34:
	movq	16(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	%r12, %r13
	je	.L35
	.p2align 4,,10
	.p2align 3
.L39:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L36
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L39
.L37:
	movq	8(%rbx), %r12
.L35:
	testq	%r12, %r12
	je	.L33
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L39
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L33:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22421:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev
	.set	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.section	.rodata._ZN2v88internal28ReflectIsExtensibleAssembler31GenerateReflectIsExtensibleImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../../deps/v8/../../deps/v8/src/builtins/reflect.tq"
	.section	.rodata._ZN2v88internal28ReflectIsExtensibleAssembler31GenerateReflectIsExtensibleImplEv.str1.1,"aMS",@progbits,1
.LC3:
	.string	"Reflect.isExtensible"
	.section	.text._ZN2v88internal28ReflectIsExtensibleAssembler31GenerateReflectIsExtensibleImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28ReflectIsExtensibleAssembler31GenerateReflectIsExtensibleImplEv
	.type	_ZN2v88internal28ReflectIsExtensibleAssembler31GenerateReflectIsExtensibleImplEv, @function
_ZN2v88internal28ReflectIsExtensibleAssembler31GenerateReflectIsExtensibleImplEv:
.LFB22418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1000(%rbp), %r14
	leaq	-232(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1184(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1056(%rbp), %rbx
	subq	$1256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, -1264(%rbp)
	movq	%rax, -1224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	leaq	-1224(%rbp), %r12
	movq	%rax, -1280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -1048(%rbp)
	movq	%rax, -1296(%rbp)
	movq	-1224(%rbp), %rax
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1048(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1224(%rbp), %rax
	movl	$72, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1256(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1224(%rbp), %rax
	movl	$96, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1224(%rbp), %rax
	movl	$48, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -1240(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1224(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1280(%rbp), %xmm1
	movaps	%xmm0, -1184(%rbp)
	movhps	-1296(%rbp), %xmm1
	movq	$0, -1168(%rbp)
	movaps	%xmm1, -1280(%rbp)
	call	_Znwm@PLT
	movdqa	-1280(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1184(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L46
	call	_ZdlPv@PLT
.L46:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -992(%rbp)
	jne	.L162
	cmpq	$0, -800(%rbp)
	jne	.L163
.L52:
	cmpq	$0, -608(%rbp)
	jne	.L164
.L55:
	cmpq	$0, -416(%rbp)
	jne	.L165
.L58:
	cmpq	$0, -224(%rbp)
	jne	.L166
.L60:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L63
	call	_ZdlPv@PLT
.L63:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L64
	.p2align 4,,10
	.p2align 3
.L68:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L65
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L68
.L66:
	movq	-280(%rbp), %r13
.L64:
	testq	%r13, %r13
	je	.L69
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L69:
	movq	-1240(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L70
	call	_ZdlPv@PLT
.L70:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L71
	.p2align 4,,10
	.p2align 3
.L75:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L72
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L75
.L73:
	movq	-472(%rbp), %r13
.L71:
	testq	%r13, %r13
	je	.L76
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L76:
	movq	-1248(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L77
	call	_ZdlPv@PLT
.L77:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L78
	.p2align 4,,10
	.p2align 3
.L82:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L79
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L82
.L80:
	movq	-664(%rbp), %r13
.L78:
	testq	%r13, %r13
	je	.L83
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L83:
	movq	-1256(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L84
	call	_ZdlPv@PLT
.L84:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L85
	.p2align 4,,10
	.p2align 3
.L89:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L86
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L89
.L87:
	movq	-856(%rbp), %r13
.L85:
	testq	%r13, %r13
	je	.L90
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L90:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L91
	call	_ZdlPv@PLT
.L91:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L92
	.p2align 4,,10
	.p2align 3
.L96:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L93
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L96
.L94:
	movq	-1048(%rbp), %r13
.L92:
	testq	%r13, %r13
	je	.L97
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L97:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L167
	addq	$1256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L96
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L86:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L89
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L79:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L82
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L65:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L68
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L72:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L75
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L162:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r8d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L48
	call	_ZdlPv@PLT
.L48:
	movq	(%rbx), %rax
	movl	$13, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	%rcx, -1280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1280(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rcx
	movq	-1264(%rbp), %rdi
	call	_ZN2v88internal21Cast10JSReceiver_1404EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1280(%rbp), %rcx
	movq	%rbx, %xmm2
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	%rax, -72(%rbp)
	leaq	-1216(%rbp), %rbx
	movq	%rcx, %xmm7
	movq	%rcx, -80(%rbp)
	punpcklqdq	%xmm7, %xmm2
	movaps	%xmm0, -1216(%rbp)
	movaps	%xmm2, -1296(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	$0, -1200(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movq	%rbx, %rsi
	leaq	32(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -1216(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -1200(%rbp)
	movq	%rdx, -1208(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L49
	call	_ZdlPv@PLT
.L49:
	movq	-1248(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1176(%rbp)
	jne	.L168
.L50:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -800(%rbp)
	je	.L52
.L163:
	movq	-1256(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%rbx, %rdi
	movb	$8, 2(%rax)
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L53
	call	_ZdlPv@PLT
.L53:
	movq	(%rbx), %rax
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movdqu	(%rax), %xmm0
	movaps	%xmm1, -1184(%rbp)
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1280(%rbp)
	call	_Znwm@PLT
	movdqa	-1280(%rbp), %xmm0
	leaq	-480(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1184(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L54
	call	_ZdlPv@PLT
.L54:
	movq	-1240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -608(%rbp)
	je	.L55
.L164:
	movq	-1248(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L56
	call	_ZdlPv@PLT
.L56:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	24(%rax), %rdx
	movdqu	(%rax), %xmm5
	movaps	%xmm0, -1184(%rbp)
	movq	$0, -1168(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm5, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-288(%rbp), %rdi
	movq	%rax, -1184(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L57
	call	_ZdlPv@PLT
.L57:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -416(%rbp)
	je	.L58
.L165:
	movq	-1240(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L59
	call	_ZdlPv@PLT
.L59:
	movq	(%rbx), %rax
	movl	$14, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1264(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	movl	$26, %edx
	movq	%rbx, %rsi
	leaq	.LC3(%rip), %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -224(%rbp)
	je	.L60
.L166:
	leaq	-288(%rbp), %r8
	movq	%r15, %rsi
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	movq	%r8, -1280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	-1280(%rbp), %r8
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%r8, %rdi
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L61
	movq	%rax, -1280(%rbp)
	call	_ZdlPv@PLT
	movq	-1280(%rbp), %rax
.L61:
	movq	(%rax), %rax
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	movq	(%rax), %rdx
	movq	16(%rax), %r8
	testq	%rdx, %rdx
	movq	%r8, -1280(%rbp)
	cmovne	%rdx, %rbx
	movl	$15, %edx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1280(%rbp), %r8
	movq	-1264(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal22ObjectIsExtensible_311EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movq	-1264(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L168:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1280(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1296(%rbp), %xmm7
	movl	$24, %edi
	movaps	%xmm0, -1216(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1200(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%rbx, %rsi
	leaq	24(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -1216(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1200(%rbp)
	movq	%rdx, -1208(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L51
	call	_ZdlPv@PLT
.L51:
	movq	-1256(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L50
.L167:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22418:
	.size	_ZN2v88internal28ReflectIsExtensibleAssembler31GenerateReflectIsExtensibleImplEv, .-_ZN2v88internal28ReflectIsExtensibleAssembler31GenerateReflectIsExtensibleImplEv
	.section	.rodata._ZN2v88internal8Builtins28Generate_ReflectIsExtensibleEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/reflect-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins28Generate_ReflectIsExtensibleEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"ReflectIsExtensible"
	.section	.text._ZN2v88internal8Builtins28Generate_ReflectIsExtensibleEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins28Generate_ReflectIsExtensibleEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins28Generate_ReflectIsExtensibleEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins28Generate_ReflectIsExtensibleEPNS0_8compiler18CodeAssemblerStateE:
.LFB22414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$146, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$861, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L173
.L170:
	movq	%r13, %rdi
	call	_ZN2v88internal28ReflectIsExtensibleAssembler31GenerateReflectIsExtensibleImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L174
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L170
.L174:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22414:
	.size	_ZN2v88internal8Builtins28Generate_ReflectIsExtensibleEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins28Generate_ReflectIsExtensibleEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal33ReflectPreventExtensionsAssembler36GenerateReflectPreventExtensionsImplEv.str1.1,"aMS",@progbits,1
.LC6:
	.string	"Reflect.preventExtensions"
	.section	.text._ZN2v88internal33ReflectPreventExtensionsAssembler36GenerateReflectPreventExtensionsImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33ReflectPreventExtensionsAssembler36GenerateReflectPreventExtensionsImplEv
	.type	_ZN2v88internal33ReflectPreventExtensionsAssembler36GenerateReflectPreventExtensionsImplEv, @function
_ZN2v88internal33ReflectPreventExtensionsAssembler36GenerateReflectPreventExtensionsImplEv:
.LFB22442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1000(%rbp), %r14
	leaq	-232(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1184(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1056(%rbp), %rbx
	subq	$1256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, -1264(%rbp)
	movq	%rax, -1224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	leaq	-1224(%rbp), %r12
	movq	%rax, -1280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -1048(%rbp)
	movq	%rax, -1296(%rbp)
	movq	-1224(%rbp), %rax
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1048(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1224(%rbp), %rax
	movl	$72, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1256(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1224(%rbp), %rax
	movl	$96, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1224(%rbp), %rax
	movl	$48, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -1240(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1224(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1280(%rbp), %xmm1
	movaps	%xmm0, -1184(%rbp)
	movhps	-1296(%rbp), %xmm1
	movq	$0, -1168(%rbp)
	movaps	%xmm1, -1280(%rbp)
	call	_Znwm@PLT
	movdqa	-1280(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1184(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L176
	call	_ZdlPv@PLT
.L176:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -992(%rbp)
	jne	.L292
	cmpq	$0, -800(%rbp)
	jne	.L293
.L182:
	cmpq	$0, -608(%rbp)
	jne	.L294
.L185:
	cmpq	$0, -416(%rbp)
	jne	.L295
.L188:
	cmpq	$0, -224(%rbp)
	jne	.L296
.L190:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L193
	call	_ZdlPv@PLT
.L193:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L194
	.p2align 4,,10
	.p2align 3
.L198:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L195
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L198
.L196:
	movq	-280(%rbp), %r13
.L194:
	testq	%r13, %r13
	je	.L199
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L199:
	movq	-1240(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L200
	call	_ZdlPv@PLT
.L200:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L201
	.p2align 4,,10
	.p2align 3
.L205:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L202
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L205
.L203:
	movq	-472(%rbp), %r13
.L201:
	testq	%r13, %r13
	je	.L206
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L206:
	movq	-1248(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L207
	call	_ZdlPv@PLT
.L207:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L208
	.p2align 4,,10
	.p2align 3
.L212:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L209
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L212
.L210:
	movq	-664(%rbp), %r13
.L208:
	testq	%r13, %r13
	je	.L213
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L213:
	movq	-1256(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L214
	call	_ZdlPv@PLT
.L214:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L215
	.p2align 4,,10
	.p2align 3
.L219:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L216
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L219
.L217:
	movq	-856(%rbp), %r13
.L215:
	testq	%r13, %r13
	je	.L220
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L220:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L221
	call	_ZdlPv@PLT
.L221:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L222
	.p2align 4,,10
	.p2align 3
.L226:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L223
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L226
.L224:
	movq	-1048(%rbp), %r13
.L222:
	testq	%r13, %r13
	je	.L227
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L227:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L297
	addq	$1256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L226
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L216:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L219
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L209:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L212
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L195:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L198
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L202:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L205
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L292:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r8d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L178
	call	_ZdlPv@PLT
.L178:
	movq	(%rbx), %rax
	movl	$21, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	%rcx, -1280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1280(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rcx
	movq	-1264(%rbp), %rdi
	call	_ZN2v88internal21Cast10JSReceiver_1404EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1280(%rbp), %rcx
	movq	%rbx, %xmm2
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	%rax, -72(%rbp)
	leaq	-1216(%rbp), %rbx
	movq	%rcx, %xmm7
	movq	%rcx, -80(%rbp)
	punpcklqdq	%xmm7, %xmm2
	movaps	%xmm0, -1216(%rbp)
	movaps	%xmm2, -1296(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	$0, -1200(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movq	%rbx, %rsi
	leaq	32(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -1216(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -1200(%rbp)
	movq	%rdx, -1208(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L179
	call	_ZdlPv@PLT
.L179:
	movq	-1248(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1176(%rbp)
	jne	.L298
.L180:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -800(%rbp)
	je	.L182
.L293:
	movq	-1256(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%rbx, %rdi
	movb	$8, 2(%rax)
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L183
	call	_ZdlPv@PLT
.L183:
	movq	(%rbx), %rax
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movdqu	(%rax), %xmm0
	movaps	%xmm1, -1184(%rbp)
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1280(%rbp)
	call	_Znwm@PLT
	movdqa	-1280(%rbp), %xmm0
	leaq	-480(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1184(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L184
	call	_ZdlPv@PLT
.L184:
	movq	-1240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -608(%rbp)
	je	.L185
.L294:
	movq	-1248(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L186
	call	_ZdlPv@PLT
.L186:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	24(%rax), %rdx
	movdqu	(%rax), %xmm5
	movaps	%xmm0, -1184(%rbp)
	movq	$0, -1168(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm5, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-288(%rbp), %rdi
	movq	%rax, -1184(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L187
	call	_ZdlPv@PLT
.L187:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -416(%rbp)
	je	.L188
.L295:
	movq	-1240(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L189
	call	_ZdlPv@PLT
.L189:
	movq	(%rbx), %rax
	movl	$22, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1264(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	movl	$26, %edx
	movq	%rbx, %rsi
	leaq	.LC6(%rip), %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -224(%rbp)
	je	.L190
.L296:
	leaq	-288(%rbp), %r8
	movq	%r15, %rsi
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	movq	%r8, -1280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	-1280(%rbp), %r8
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%r8, %rdi
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L191
	movq	%rax, -1280(%rbp)
	call	_ZdlPv@PLT
	movq	-1280(%rbp), %rax
.L191:
	movq	(%rax), %rax
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	movq	(%rax), %rdx
	movq	16(%rax), %r8
	testq	%rdx, %rdx
	movq	%r8, -1280(%rbp)
	cmovne	%rdx, %rbx
	movl	$23, %edx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1280(%rbp), %r8
	movq	-1264(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%r8, %rdx
	call	_ZN2v88internal36ObjectPreventExtensionsDontThrow_313EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movq	-1264(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L298:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1280(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1296(%rbp), %xmm7
	movl	$24, %edi
	movaps	%xmm0, -1216(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1200(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%rbx, %rsi
	leaq	24(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -1216(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1200(%rbp)
	movq	%rdx, -1208(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L181
	call	_ZdlPv@PLT
.L181:
	movq	-1256(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L180
.L297:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22442:
	.size	_ZN2v88internal33ReflectPreventExtensionsAssembler36GenerateReflectPreventExtensionsImplEv, .-_ZN2v88internal33ReflectPreventExtensionsAssembler36GenerateReflectPreventExtensionsImplEv
	.section	.rodata._ZN2v88internal8Builtins33Generate_ReflectPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"ReflectPreventExtensions"
	.section	.text._ZN2v88internal8Builtins33Generate_ReflectPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins33Generate_ReflectPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins33Generate_ReflectPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins33Generate_ReflectPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE:
.LFB22438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$213, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$862, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L303
.L300:
	movq	%r13, %rdi
	call	_ZN2v88internal33ReflectPreventExtensionsAssembler36GenerateReflectPreventExtensionsImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L304
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L303:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L300
.L304:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22438:
	.size	_ZN2v88internal8Builtins33Generate_ReflectPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins33Generate_ReflectPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal30ReflectGetPrototypeOfAssembler33GenerateReflectGetPrototypeOfImplEv.str1.1,"aMS",@progbits,1
.LC8:
	.string	"Reflect.getPrototypeOf"
	.section	.text._ZN2v88internal30ReflectGetPrototypeOfAssembler33GenerateReflectGetPrototypeOfImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30ReflectGetPrototypeOfAssembler33GenerateReflectGetPrototypeOfImplEv
	.type	_ZN2v88internal30ReflectGetPrototypeOfAssembler33GenerateReflectGetPrototypeOfImplEv, @function
_ZN2v88internal30ReflectGetPrototypeOfAssembler33GenerateReflectGetPrototypeOfImplEv:
.LFB22451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1000(%rbp), %r14
	leaq	-232(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1184(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1056(%rbp), %rbx
	subq	$1256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, -1264(%rbp)
	movq	%rax, -1224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	leaq	-1224(%rbp), %r12
	movq	%rax, -1280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -1048(%rbp)
	movq	%rax, -1296(%rbp)
	movq	-1224(%rbp), %rax
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1048(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1224(%rbp), %rax
	movl	$72, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1256(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1224(%rbp), %rax
	movl	$96, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1224(%rbp), %rax
	movl	$48, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -1240(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1224(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1280(%rbp), %xmm1
	movaps	%xmm0, -1184(%rbp)
	movhps	-1296(%rbp), %xmm1
	movq	$0, -1168(%rbp)
	movaps	%xmm1, -1280(%rbp)
	call	_Znwm@PLT
	movdqa	-1280(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1184(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L306
	call	_ZdlPv@PLT
.L306:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -992(%rbp)
	jne	.L425
	cmpq	$0, -800(%rbp)
	jne	.L426
.L312:
	cmpq	$0, -608(%rbp)
	jne	.L427
.L315:
	cmpq	$0, -416(%rbp)
	jne	.L428
.L318:
	cmpq	$0, -224(%rbp)
	jne	.L429
.L320:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L324
	call	_ZdlPv@PLT
.L324:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L325
	.p2align 4,,10
	.p2align 3
.L329:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L326
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L329
.L327:
	movq	-280(%rbp), %r13
.L325:
	testq	%r13, %r13
	je	.L330
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L330:
	movq	-1240(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L331
	call	_ZdlPv@PLT
.L331:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L332
	.p2align 4,,10
	.p2align 3
.L336:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L333
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L336
.L334:
	movq	-472(%rbp), %r13
.L332:
	testq	%r13, %r13
	je	.L337
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L337:
	movq	-1248(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L338
	call	_ZdlPv@PLT
.L338:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L339
	.p2align 4,,10
	.p2align 3
.L343:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L340
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L343
.L341:
	movq	-664(%rbp), %r13
.L339:
	testq	%r13, %r13
	je	.L344
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L344:
	movq	-1256(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L345
	call	_ZdlPv@PLT
.L345:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L346
	.p2align 4,,10
	.p2align 3
.L350:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L347
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L350
.L348:
	movq	-856(%rbp), %r13
.L346:
	testq	%r13, %r13
	je	.L351
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L351:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L352
	call	_ZdlPv@PLT
.L352:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L353
	.p2align 4,,10
	.p2align 3
.L357:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L354
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L357
.L355:
	movq	-1048(%rbp), %r13
.L353:
	testq	%r13, %r13
	je	.L358
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L358:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L430
	addq	$1256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L354:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L357
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L347:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L350
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L340:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L343
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L326:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L329
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L333:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L336
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L425:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r8d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L308
	call	_ZdlPv@PLT
.L308:
	movq	(%rbx), %rax
	movl	$29, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	%rcx, -1280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1280(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rcx
	movq	-1264(%rbp), %rdi
	call	_ZN2v88internal21Cast10JSReceiver_1404EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1280(%rbp), %rcx
	movq	%rbx, %xmm2
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	%rax, -72(%rbp)
	leaq	-1216(%rbp), %rbx
	movq	%rcx, %xmm7
	movq	%rcx, -80(%rbp)
	punpcklqdq	%xmm7, %xmm2
	movaps	%xmm0, -1216(%rbp)
	movaps	%xmm2, -1296(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	$0, -1200(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movq	%rbx, %rsi
	leaq	32(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -1216(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -1200(%rbp)
	movq	%rdx, -1208(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L309
	call	_ZdlPv@PLT
.L309:
	movq	-1248(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1176(%rbp)
	jne	.L431
.L310:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -800(%rbp)
	je	.L312
.L426:
	movq	-1256(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%rbx, %rdi
	movb	$8, 2(%rax)
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L313
	call	_ZdlPv@PLT
.L313:
	movq	(%rbx), %rax
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movdqu	(%rax), %xmm0
	movaps	%xmm1, -1184(%rbp)
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1280(%rbp)
	call	_Znwm@PLT
	movdqa	-1280(%rbp), %xmm0
	leaq	-480(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1184(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L314
	call	_ZdlPv@PLT
.L314:
	movq	-1240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -608(%rbp)
	je	.L315
.L427:
	movq	-1248(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L316
	call	_ZdlPv@PLT
.L316:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	24(%rax), %rdx
	movdqu	(%rax), %xmm5
	movaps	%xmm0, -1184(%rbp)
	movq	$0, -1168(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm5, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-288(%rbp), %rdi
	movq	%rax, -1184(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L317
	call	_ZdlPv@PLT
.L317:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -416(%rbp)
	je	.L318
.L428:
	movq	-1240(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L319
	call	_ZdlPv@PLT
.L319:
	movq	(%rbx), %rax
	movl	$30, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1264(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	movl	$26, %edx
	movq	%rbx, %rsi
	leaq	.LC8(%rip), %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -224(%rbp)
	je	.L320
.L429:
	leaq	-288(%rbp), %r8
	movq	%r15, %rsi
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	movq	%r8, -1296(%rbp)
	movq	$0, -1280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	-1296(%rbp), %r8
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%r8, %rdi
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L321
	movq	%rax, -1296(%rbp)
	call	_ZdlPv@PLT
	movq	-1296(%rbp), %rax
.L321:
	movq	(%rax), %rax
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	movq	(%rax), %rdx
	movq	16(%rax), %rax
	testq	%rdx, %rdx
	cmove	-1280(%rbp), %rdx
	testq	%rax, %rax
	cmovne	%rax, %rbx
	movq	%rdx, -1280(%rbp)
	movl	$31, %edx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1280(%rbp), %rsi
	movq	-1264(%rbp), %rdi
	movq	%rbx, %rdx
	call	_ZN2v88internal28JSReceiverGetPrototypeOf_315EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEE@PLT
	movq	-1264(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L431:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1280(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1296(%rbp), %xmm7
	movl	$24, %edi
	movaps	%xmm0, -1216(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1200(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%rbx, %rsi
	leaq	24(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -1216(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1200(%rbp)
	movq	%rdx, -1208(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L311
	call	_ZdlPv@PLT
.L311:
	movq	-1256(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L310
.L430:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22451:
	.size	_ZN2v88internal30ReflectGetPrototypeOfAssembler33GenerateReflectGetPrototypeOfImplEv, .-_ZN2v88internal30ReflectGetPrototypeOfAssembler33GenerateReflectGetPrototypeOfImplEv
	.section	.rodata._ZN2v88internal8Builtins30Generate_ReflectGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"ReflectGetPrototypeOf"
	.section	.text._ZN2v88internal8Builtins30Generate_ReflectGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins30Generate_ReflectGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins30Generate_ReflectGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins30Generate_ReflectGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE:
.LFB22447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$280, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$863, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L436
.L433:
	movq	%r13, %rdi
	call	_ZN2v88internal30ReflectGetPrototypeOfAssembler33GenerateReflectGetPrototypeOfImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L437
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L436:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L433
.L437:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22447:
	.size	_ZN2v88internal8Builtins30Generate_ReflectGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins30Generate_ReflectGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal30ReflectSetPrototypeOfAssembler33GenerateReflectSetPrototypeOfImplEv.str1.1,"aMS",@progbits,1
.LC10:
	.string	"Reflect.setPrototypeOf"
	.section	.text._ZN2v88internal30ReflectSetPrototypeOfAssembler33GenerateReflectSetPrototypeOfImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30ReflectSetPrototypeOfAssembler33GenerateReflectSetPrototypeOfImplEv
	.type	_ZN2v88internal30ReflectSetPrototypeOfAssembler33GenerateReflectSetPrototypeOfImplEv, @function
_ZN2v88internal30ReflectSetPrototypeOfAssembler33GenerateReflectSetPrototypeOfImplEv:
.LFB22460:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$5, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1592(%rbp), %r14
	leaq	-1208(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1648(%rbp), %rbx
	subq	$1896, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, -1832(%rbp)
	movq	%rax, -1816(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -1904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	leaq	-1816(%rbp), %r12
	movq	%rax, -1920(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$72, %edi
	movq	$0, -1640(%rbp)
	movq	$0, -1632(%rbp)
	movq	%rax, %r13
	movq	-1816(%rbp), %rax
	movq	$0, -1624(%rbp)
	movq	%rax, -1648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -1624(%rbp)
	movq	%rdx, -1632(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1608(%rbp)
	movq	%rax, -1640(%rbp)
	movq	$0, -1616(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1816(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1448(%rbp)
	movq	$0, -1440(%rbp)
	movq	%rax, -1456(%rbp)
	movq	$0, -1432(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1448(%rbp)
	leaq	-1400(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1432(%rbp)
	movq	%rdx, -1440(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1416(%rbp)
	movq	%rax, -1880(%rbp)
	movq	$0, -1424(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1816(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1256(%rbp)
	movq	$0, -1248(%rbp)
	movq	%rax, -1264(%rbp)
	movq	$0, -1240(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r15, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -1240(%rbp)
	movq	%rdx, -1248(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1224(%rbp)
	movq	%rax, -1256(%rbp)
	movq	$0, -1232(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1816(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1064(%rbp)
	movq	$0, -1056(%rbp)
	movq	%rax, -1072(%rbp)
	movq	$0, -1048(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1064(%rbp)
	leaq	-1016(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1048(%rbp)
	movq	%rdx, -1056(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1032(%rbp)
	movq	%rax, -1848(%rbp)
	movq	$0, -1040(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1816(%rbp), %rax
	movl	$96, %edi
	movq	$0, -872(%rbp)
	movq	$0, -864(%rbp)
	movq	%rax, -880(%rbp)
	movq	$0, -856(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -872(%rbp)
	leaq	-824(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -856(%rbp)
	movq	%rdx, -864(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -840(%rbp)
	movq	%rax, -1840(%rbp)
	movq	$0, -848(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1816(%rbp), %rax
	movl	$144, %edi
	movq	$0, -680(%rbp)
	movq	$0, -672(%rbp)
	movq	%rax, -688(%rbp)
	movq	$0, -664(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -680(%rbp)
	leaq	-632(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -664(%rbp)
	movq	%rdx, -672(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -648(%rbp)
	movq	%rax, -1872(%rbp)
	movq	$0, -656(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1816(%rbp), %rax
	movl	$168, %edi
	movq	$0, -488(%rbp)
	movq	$0, -480(%rbp)
	movq	%rax, -496(%rbp)
	movq	$0, -472(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -488(%rbp)
	leaq	-440(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -472(%rbp)
	movq	%rdx, -480(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -456(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -464(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1816(%rbp), %rax
	movl	$120, %edi
	movq	$0, -296(%rbp)
	movq	$0, -288(%rbp)
	movq	%rax, -304(%rbp)
	movq	$0, -280(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -296(%rbp)
	leaq	-248(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -280(%rbp)
	movq	%rdx, -288(%rbp)
	xorl	%edx, %edx
	movq	%rax, -1864(%rbp)
	movups	%xmm0, -264(%rbp)
	movq	$0, -272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	-1904(%rbp), %xmm1
	movq	%r13, -96(%rbp)
	leaq	-1776(%rbp), %r13
	movhps	-1920(%rbp), %xmm1
	movaps	%xmm0, -1776(%rbp)
	movaps	%xmm1, -112(%rbp)
	movq	$0, -1760(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm2
	leaq	24(%rax), %rdx
	movq	%rax, -1776(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -1760(%rbp)
	movq	%rdx, -1768(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1776(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L439
	call	_ZdlPv@PLT
.L439:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1584(%rbp)
	jne	.L625
	cmpq	$0, -1392(%rbp)
	jne	.L626
.L445:
	cmpq	$0, -1200(%rbp)
	jne	.L627
.L448:
	cmpq	$0, -1008(%rbp)
	jne	.L628
.L451:
	cmpq	$0, -816(%rbp)
	jne	.L629
.L453:
	cmpq	$0, -624(%rbp)
	jne	.L630
.L458:
	cmpq	$0, -432(%rbp)
	jne	.L631
.L461:
	cmpq	$0, -240(%rbp)
	jne	.L632
.L465:
	movq	-1864(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L467
	call	_ZdlPv@PLT
.L467:
	movq	-288(%rbp), %rbx
	movq	-296(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L468
	.p2align 4,,10
	.p2align 3
.L472:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L469
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L472
.L470:
	movq	-296(%rbp), %r13
.L468:
	testq	%r13, %r13
	je	.L473
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L473:
	movq	-1856(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-464(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L474
	call	_ZdlPv@PLT
.L474:
	movq	-480(%rbp), %rbx
	movq	-488(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L475
	.p2align 4,,10
	.p2align 3
.L479:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L476
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L479
.L477:
	movq	-488(%rbp), %r13
.L475:
	testq	%r13, %r13
	je	.L480
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L480:
	movq	-1872(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L481
	call	_ZdlPv@PLT
.L481:
	movq	-672(%rbp), %rbx
	movq	-680(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L482
	.p2align 4,,10
	.p2align 3
.L486:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L483
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L486
.L484:
	movq	-680(%rbp), %r13
.L482:
	testq	%r13, %r13
	je	.L487
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L487:
	movq	-1840(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-848(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L488
	call	_ZdlPv@PLT
.L488:
	movq	-864(%rbp), %rbx
	movq	-872(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L489
	.p2align 4,,10
	.p2align 3
.L493:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L490
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L493
.L491:
	movq	-872(%rbp), %r13
.L489:
	testq	%r13, %r13
	je	.L494
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L494:
	movq	-1848(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1040(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L495
	call	_ZdlPv@PLT
.L495:
	movq	-1056(%rbp), %rbx
	movq	-1064(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L496
	.p2align 4,,10
	.p2align 3
.L500:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L497
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L500
.L498:
	movq	-1064(%rbp), %r13
.L496:
	testq	%r13, %r13
	je	.L501
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L501:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L502
	call	_ZdlPv@PLT
.L502:
	movq	-1248(%rbp), %rbx
	movq	-1256(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L503
	.p2align 4,,10
	.p2align 3
.L507:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L504
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L507
.L505:
	movq	-1256(%rbp), %r13
.L503:
	testq	%r13, %r13
	je	.L508
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L508:
	movq	-1880(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L509
	call	_ZdlPv@PLT
.L509:
	movq	-1440(%rbp), %rbx
	movq	-1448(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L510
	.p2align 4,,10
	.p2align 3
.L514:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L511
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L514
.L512:
	movq	-1448(%rbp), %r13
.L510:
	testq	%r13, %r13
	je	.L515
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L515:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1616(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L516
	call	_ZdlPv@PLT
.L516:
	movq	-1632(%rbp), %rbx
	movq	-1640(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L517
	.p2align 4,,10
	.p2align 3
.L521:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L518
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L521
.L519:
	movq	-1640(%rbp), %r13
.L517:
	testq	%r13, %r13
	je	.L522
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L522:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L633
	addq	$1896, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L518:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L521
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L511:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L514
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L504:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L507
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L497:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L500
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L490:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L493
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L483:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L486
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L469:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L472
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L476:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L479
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L625:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1760(%rbp)
	movaps	%xmm0, -1776(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r8d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -1776(%rbp)
	movq	%rdx, -1760(%rbp)
	movq	%rdx, -1768(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1776(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L441
	call	_ZdlPv@PLT
.L441:
	movq	(%rbx), %rax
	movl	$37, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	16(%rax), %rax
	movq	%rcx, -1904(%rbp)
	movq	%rax, -1920(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1904(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rcx
	movq	-1832(%rbp), %rdi
	call	_ZN2v88internal21Cast10JSReceiver_1404EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm4
	pxor	%xmm0, %xmm0
	movq	-1920(%rbp), %xmm3
	movhps	-1904(%rbp), %xmm4
	movl	$40, %edi
	movq	%rax, -80(%rbp)
	leaq	-1808(%rbp), %rbx
	movhps	-1904(%rbp), %xmm3
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm3, -1920(%rbp)
	movaps	%xmm4, -1904(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -1808(%rbp)
	movq	$0, -1792(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm2
	movq	-80(%rbp), %rcx
	movq	%rbx, %rsi
	leaq	40(%rax), %rdx
	leaq	-1264(%rbp), %rdi
	movq	%rax, -1808(%rbp)
	movups	%xmm2, (%rax)
	movdqa	-96(%rbp), %xmm2
	movq	%rcx, 32(%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -1792(%rbp)
	movq	%rdx, -1800(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1808(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L442
	call	_ZdlPv@PLT
.L442:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1768(%rbp)
	jne	.L634
.L443:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1392(%rbp)
	je	.L445
.L626:
	movq	-1880(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1456(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1760(%rbp)
	movaps	%xmm0, -1776(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1776(%rbp)
	movq	%rdx, -1760(%rbp)
	movq	%rdx, -1768(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1776(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L446
	call	_ZdlPv@PLT
.L446:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm3
	movaps	%xmm0, -1776(%rbp)
	movq	$0, -1760(%rbp)
	movq	%rdx, -96(%rbp)
	movaps	%xmm3, -112(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-112(%rbp), %xmm4
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-1072(%rbp), %rdi
	movq	%rax, -1776(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -1760(%rbp)
	movq	%rdx, -1768(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1776(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L447
	call	_ZdlPv@PLT
.L447:
	movq	-1848(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1200(%rbp)
	je	.L448
.L627:
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	-1264(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1760(%rbp)
	movaps	%xmm0, -1776(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -1776(%rbp)
	movq	%rdx, -1760(%rbp)
	movq	%rdx, -1768(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1776(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L449
	call	_ZdlPv@PLT
.L449:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	8(%rax), %rsi
	movq	16(%rax), %rcx
	movq	32(%rax), %rdx
	movq	(%rax), %rax
	movaps	%xmm0, -1776(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rax, -112(%rbp)
	movq	$0, -1760(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-880(%rbp), %rdi
	movq	%rax, -1776(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -1760(%rbp)
	movq	%rdx, -1768(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1776(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L450
	call	_ZdlPv@PLT
.L450:
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1008(%rbp)
	je	.L451
.L628:
	movq	-1848(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1072(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1760(%rbp)
	movaps	%xmm0, -1776(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%rbx, %rdi
	movb	$8, 2(%rax)
	movq	%rax, -1776(%rbp)
	movq	%rdx, -1760(%rbp)
	movq	%rdx, -1768(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1776(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L452
	call	_ZdlPv@PLT
.L452:
	movq	(%rbx), %rax
	movl	$38, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1832(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	movl	$26, %edx
	movq	%rbx, %rsi
	leaq	.LC10(%rip), %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -816(%rbp)
	je	.L453
.L629:
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-880(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1760(%rbp)
	movaps	%xmm0, -1776(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1776(%rbp)
	movq	%rdx, -1760(%rbp)
	movq	%rdx, -1768(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1776(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L454
	call	_ZdlPv@PLT
.L454:
	movq	(%rbx), %rax
	movl	$39, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	%rcx, -1920(%rbp)
	movq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, -1904(%rbp)
	movq	%rax, -1936(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$40, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1904(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rcx
	movq	-1832(%rbp), %rdi
	call	_ZN2v88internal32Cast21UT6ATNull10JSReceiver_1465EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm7
	pxor	%xmm0, %xmm0
	movq	-1904(%rbp), %xmm3
	movhps	-1920(%rbp), %xmm7
	movl	$56, %edi
	movq	%rax, -64(%rbp)
	leaq	-1808(%rbp), %rbx
	movdqa	%xmm3, %xmm5
	movdqa	%xmm3, %xmm6
	movaps	%xmm7, -112(%rbp)
	punpcklqdq	%xmm5, %xmm5
	movhps	-1936(%rbp), %xmm6
	movaps	%xmm7, -1920(%rbp)
	movaps	%xmm5, -1904(%rbp)
	movaps	%xmm6, -1936(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm0, -1808(%rbp)
	movq	$0, -1792(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-112(%rbp), %xmm7
	movq	%rbx, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	leaq	56(%rax), %rdx
	leaq	-496(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm3, 32(%rax)
	movq	%rax, -1808(%rbp)
	movq	%rdx, -1792(%rbp)
	movq	%rdx, -1800(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1808(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L455
	call	_ZdlPv@PLT
.L455:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1768(%rbp)
	jne	.L635
.L456:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -624(%rbp)
	je	.L458
.L630:
	movq	-1872(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-688(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -1760(%rbp)
	movaps	%xmm0, -1776(%rbp)
	call	_Znwm@PLT
	movl	$2056, %esi
	movq	%rbx, %rdi
	movw	%si, 4(%rax)
	leaq	6(%rax), %rdx
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	movq	%rax, -1776(%rbp)
	movq	%rdx, -1760(%rbp)
	movq	%rdx, -1768(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1776(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L459
	call	_ZdlPv@PLT
.L459:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	32(%rax), %rdx
	movdqu	(%rax), %xmm4
	movq	$0, -1760(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -80(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm0, -1776(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	leaq	40(%rax), %rdx
	leaq	-304(%rbp), %rdi
	movq	%rax, -1776(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -1760(%rbp)
	movq	%rdx, -1768(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1776(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L460
	call	_ZdlPv@PLT
.L460:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -432(%rbp)
	je	.L461
.L631:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	leaq	-496(%rbp), %r8
	movq	%r8, -1920(%rbp)
	movq	$0, -1904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -1760(%rbp)
	movaps	%xmm0, -1776(%rbp)
	call	_Znwm@PLT
	movl	$2056, %ecx
	movq	-1920(%rbp), %r8
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%r8, %rdi
	movb	$7, 6(%rax)
	movq	%rax, -1776(%rbp)
	movq	%rdx, -1760(%rbp)
	movq	%rdx, -1768(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1776(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L462
	movq	%rax, -1920(%rbp)
	call	_ZdlPv@PLT
	movq	-1920(%rbp), %rax
.L462:
	movq	(%rax), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rdx
	movq	24(%rax), %r8
	movq	48(%rax), %rax
	testq	%rdx, %rdx
	cmove	-1904(%rbp), %rdx
	movq	%r8, -1920(%rbp)
	testq	%rax, %rax
	movq	%rdx, -1904(%rbp)
	movl	$41, %edx
	cmovne	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1920(%rbp), %r8
	movq	-1904(%rbp), %rsi
	movq	-1832(%rbp), %rdi
	movq	%rbx, %rcx
	movq	%r8, %rdx
	call	_ZN2v88internal33ObjectSetPrototypeOfDontThrow_317EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_10HeapObjectEEE@PLT
	movq	-1832(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -240(%rbp)
	je	.L465
.L632:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-304(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1760(%rbp)
	movaps	%xmm0, -1776(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -1776(%rbp)
	movq	%rdx, -1760(%rbp)
	movq	%rdx, -1768(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1776(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L466
	call	_ZdlPv@PLT
.L466:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movl	$43, %edx
	leaq	.LC2(%rip), %rsi
	movq	16(%rax), %rcx
	movq	(%rax), %rbx
	movq	%rcx, -1904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$44, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1832(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	-1904(%rbp), %rcx
	movl	$116, %edx
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L634:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-1904(%rbp), %xmm7
	movdqa	-1920(%rbp), %xmm2
	movaps	%xmm0, -1808(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	$0, -1792(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm2
	movq	%rbx, %rsi
	leaq	32(%rax), %rdx
	leaq	-1456(%rbp), %rdi
	movq	%rax, -1808(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -1792(%rbp)
	movq	%rdx, -1800(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1808(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L444
	call	_ZdlPv@PLT
.L444:
	movq	-1880(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L635:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movdqa	-1920(%rbp), %xmm7
	movdqa	-1936(%rbp), %xmm2
	movaps	%xmm0, -1808(%rbp)
	movaps	%xmm7, -112(%rbp)
	movdqa	-1904(%rbp), %xmm7
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	movq	$0, -1792(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm4
	movq	%rbx, %rsi
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm6
	leaq	48(%rax), %rdx
	leaq	-688(%rbp), %rdi
	movq	%rax, -1808(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -1792(%rbp)
	movq	%rdx, -1800(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1808(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L457
	call	_ZdlPv@PLT
.L457:
	movq	-1872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L456
.L633:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22460:
	.size	_ZN2v88internal30ReflectSetPrototypeOfAssembler33GenerateReflectSetPrototypeOfImplEv, .-_ZN2v88internal30ReflectSetPrototypeOfAssembler33GenerateReflectSetPrototypeOfImplEv
	.section	.rodata._ZN2v88internal8Builtins30Generate_ReflectSetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"ReflectSetPrototypeOf"
	.section	.text._ZN2v88internal8Builtins30Generate_ReflectSetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins30Generate_ReflectSetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins30Generate_ReflectSetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins30Generate_ReflectSetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE:
.LFB22456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$347, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$864, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L640
.L637:
	movq	%r13, %rdi
	call	_ZN2v88internal30ReflectSetPrototypeOfAssembler33GenerateReflectSetPrototypeOfImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L641
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L640:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L637
.L641:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22456:
	.size	_ZN2v88internal8Builtins30Generate_ReflectSetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins30Generate_ReflectSetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal30ReflectDeletePropertyAssembler33GenerateReflectDeletePropertyImplEv.str1.1,"aMS",@progbits,1
.LC12:
	.string	"Reflect.deleteProperty"
	.section	.text._ZN2v88internal30ReflectDeletePropertyAssembler33GenerateReflectDeletePropertyImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30ReflectDeletePropertyAssembler33GenerateReflectDeletePropertyImplEv
	.type	_ZN2v88internal30ReflectDeletePropertyAssembler33GenerateReflectDeletePropertyImplEv, @function
_ZN2v88internal30ReflectDeletePropertyAssembler33GenerateReflectDeletePropertyImplEv:
.LFB22520:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$5, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1000(%rbp), %r14
	leaq	-232(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1056(%rbp), %rbx
	subq	$1288, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, -1272(%rbp)
	movq	%rax, -1240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -1296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	leaq	-1240(%rbp), %r12
	movq	%rax, -1312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$72, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, %r13
	movq	-1240(%rbp), %rax
	movq	$0, -1032(%rbp)
	movq	%rax, -1056(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1048(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1240(%rbp), %rax
	movl	$96, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1240(%rbp), %rax
	movl	$120, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1264(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1240(%rbp), %rax
	movl	$72, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -1256(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1240(%rbp), %rax
	movl	$96, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r15, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	-1296(%rbp), %xmm1
	movq	%r13, -80(%rbp)
	leaq	-1184(%rbp), %r13
	movhps	-1312(%rbp), %xmm1
	movaps	%xmm0, -1184(%rbp)
	movaps	%xmm1, -96(%rbp)
	movq	$0, -1168(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm2
	leaq	24(%rax), %rdx
	movq	%rax, -1184(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L643
	call	_ZdlPv@PLT
.L643:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -992(%rbp)
	jne	.L756
	cmpq	$0, -800(%rbp)
	jne	.L757
.L649:
	cmpq	$0, -608(%rbp)
	jne	.L758
.L652:
	cmpq	$0, -416(%rbp)
	jne	.L759
.L655:
	cmpq	$0, -224(%rbp)
	jne	.L760
.L657:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L659
	call	_ZdlPv@PLT
.L659:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L660
	.p2align 4,,10
	.p2align 3
.L664:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L661
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L664
.L662:
	movq	-280(%rbp), %r13
.L660:
	testq	%r13, %r13
	je	.L665
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L665:
	movq	-1256(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L666
	call	_ZdlPv@PLT
.L666:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L667
	.p2align 4,,10
	.p2align 3
.L671:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L668
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L671
.L669:
	movq	-472(%rbp), %r13
.L667:
	testq	%r13, %r13
	je	.L672
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L672:
	movq	-1264(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L673
	call	_ZdlPv@PLT
.L673:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L674
	.p2align 4,,10
	.p2align 3
.L678:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L675
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L678
.L676:
	movq	-664(%rbp), %r13
.L674:
	testq	%r13, %r13
	je	.L679
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L679:
	movq	-1280(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L680
	call	_ZdlPv@PLT
.L680:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L681
	.p2align 4,,10
	.p2align 3
.L685:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L682
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L685
.L683:
	movq	-856(%rbp), %r13
.L681:
	testq	%r13, %r13
	je	.L686
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L686:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L687
	call	_ZdlPv@PLT
.L687:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L688
	.p2align 4,,10
	.p2align 3
.L692:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L689
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L692
.L690:
	movq	-1048(%rbp), %r13
.L688:
	testq	%r13, %r13
	je	.L693
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L693:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L761
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L689:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L692
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L682:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L685
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L675:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L678
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L661:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L664
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L668:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L671
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L756:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$8, 2(%rax)
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L645
	call	_ZdlPv@PLT
.L645:
	movq	(%rbx), %rax
	movl	$75, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	16(%rax), %rax
	movq	%rcx, -1296(%rbp)
	movq	%rax, -1312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1296(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rcx
	movq	-1272(%rbp), %rdi
	call	_ZN2v88internal21Cast10JSReceiver_1404EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm4
	pxor	%xmm0, %xmm0
	movq	-1312(%rbp), %xmm3
	movhps	-1296(%rbp), %xmm4
	movl	$40, %edi
	movq	%rax, -64(%rbp)
	leaq	-1216(%rbp), %rbx
	movhps	-1296(%rbp), %xmm3
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm3, -1312(%rbp)
	movaps	%xmm4, -1296(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm0, -1216(%rbp)
	movq	$0, -1200(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rsi
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm6
	leaq	40(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -1216(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -1200(%rbp)
	movq	%rdx, -1208(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L646
	call	_ZdlPv@PLT
.L646:
	movq	-1264(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1176(%rbp)
	jne	.L762
.L647:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -800(%rbp)
	je	.L649
.L757:
	movq	-1280(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L650
	call	_ZdlPv@PLT
.L650:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm7
	movaps	%xmm0, -1184(%rbp)
	movq	$0, -1168(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm7, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -1184(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L651
	call	_ZdlPv@PLT
.L651:
	movq	-1256(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -608(%rbp)
	je	.L652
.L758:
	movq	-1264(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L653
	call	_ZdlPv@PLT
.L653:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	8(%rax), %rsi
	movq	16(%rax), %rcx
	movq	32(%rax), %rdx
	movq	(%rax), %rax
	movaps	%xmm0, -1184(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -1168(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-288(%rbp), %rdi
	movq	%rax, -1184(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L654
	call	_ZdlPv@PLT
.L654:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -416(%rbp)
	je	.L655
.L759:
	movq	-1256(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L656
	call	_ZdlPv@PLT
.L656:
	movq	(%rbx), %rax
	movl	$76, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1272(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	movl	$26, %edx
	movq	%rbx, %rsi
	leaq	.LC12(%rip), %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -224(%rbp)
	je	.L657
.L760:
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	-288(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L658
	call	_ZdlPv@PLT
.L658:
	movq	(%rbx), %rax
	movl	$77, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r9
	movq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%r9, -1328(%rbp)
	movq	%rcx, -1296(%rbp)
	movq	%rax, -1312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1272(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal59FromConstexpr14ATLanguageMode24ATconstexpr_LanguageMode_166EPNS0_8compiler18CodeAssemblerStateENS0_12LanguageModeE@PLT
	leaq	-1232(%rbp), %r10
	movq	-1272(%rbp), %rsi
	movq	%r10, %rdi
	movq	%rax, %rbx
	movq	%r10, -1320(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1320(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$160, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-1320(%rbp), %r10
	movq	-1184(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movq	%rbx, -80(%rbp)
	xorl	%esi, %esi
	movq	%rcx, -1216(%rbp)
	leaq	-96(%rbp), %rcx
	movq	%rax, %r8
	movl	$3, %ebx
	pushq	%rbx
	movq	-1320(%rbp), %r10
	leaq	-1216(%rbp), %rdx
	movq	-1328(%rbp), %r9
	movq	-1168(%rbp), %rax
	pushq	%rcx
	movl	$1, %ecx
	movq	-1312(%rbp), %xmm0
	movq	%r10, %rdi
	movq	%rax, -1208(%rbp)
	movhps	-1296(%rbp), %xmm0
	movq	%r10, -1296(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-1296(%rbp), %r10
	movq	%rax, %rbx
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1272(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L762:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-1296(%rbp), %xmm5
	movdqa	-1312(%rbp), %xmm6
	movaps	%xmm0, -1216(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm6, -80(%rbp)
	movq	$0, -1200(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm7
	movdqa	-80(%rbp), %xmm5
	movq	%rbx, %rsi
	leaq	32(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -1216(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1200(%rbp)
	movq	%rdx, -1208(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L648
	call	_ZdlPv@PLT
.L648:
	movq	-1280(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L647
.L761:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22520:
	.size	_ZN2v88internal30ReflectDeletePropertyAssembler33GenerateReflectDeletePropertyImplEv, .-_ZN2v88internal30ReflectDeletePropertyAssembler33GenerateReflectDeletePropertyImplEv
	.section	.rodata._ZN2v88internal8Builtins30Generate_ReflectDeletePropertyEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"ReflectDeleteProperty"
	.section	.text._ZN2v88internal8Builtins30Generate_ReflectDeletePropertyEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins30Generate_ReflectDeletePropertyEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins30Generate_ReflectDeletePropertyEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins30Generate_ReflectDeletePropertyEPNS0_8compiler18CodeAssemblerStateE:
.LFB22516:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$773, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$866, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L767
.L764:
	movq	%r13, %rdi
	call	_ZN2v88internal30ReflectDeletePropertyAssembler33GenerateReflectDeletePropertyImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L768
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L767:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L764
.L768:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22516:
	.size	_ZN2v88internal8Builtins30Generate_ReflectDeletePropertyEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins30Generate_ReflectDeletePropertyEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextES4_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EESC_PNS8_IS6_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextES4_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EESC_PNS8_IS6_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextES4_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EESC_PNS8_IS6_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextES4_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EESC_PNS8_IS6_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextES4_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EESC_PNS8_IS6_EE:
.LFB26704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$6, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$2053, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$117769477, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L770
	movq	%rax, -104(%rbp)
	call	_ZdlPv@PLT
	movq	-104(%rbp), %rax
.L770:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L771
	movq	%rdx, (%r15)
.L771:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L772
	movq	%rdx, (%r14)
.L772:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L773
	movq	%rdx, 0(%r13)
.L773:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L774
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L774:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L775
	movq	%rdx, (%rbx)
.L775:
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L769
	movq	-96(%rbp), %rbx
	movq	%rax, (%rbx)
.L769:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L800
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L800:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26704:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextES4_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EESC_PNS8_IS6_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextES4_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EESC_PNS8_IS6_EE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextES4_NS0_6ObjectENS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EESD_PNS9_IS6_EEPNS9_IS7_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextES4_NS0_6ObjectENS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EESD_PNS9_IS6_EEPNS9_IS7_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextES4_NS0_6ObjectENS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EESD_PNS9_IS6_EEPNS9_IS7_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextES4_NS0_6ObjectENS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EESD_PNS9_IS6_EEPNS9_IS7_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextES4_NS0_6ObjectENS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EESD_PNS9_IS6_EEPNS9_IS7_EE:
.LFB26710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$7, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$2053, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$117769477, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L802
	movq	%rax, -112(%rbp)
	call	_ZdlPv@PLT
	movq	-112(%rbp), %rax
.L802:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L803
	movq	%rdx, (%r15)
.L803:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L804
	movq	%rdx, (%r14)
.L804:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L805
	movq	%rdx, 0(%r13)
.L805:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L806
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L806:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L807
	movq	%rdx, (%rbx)
.L807:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L808
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L808:
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L801
	movq	-104(%rbp), %rbx
	movq	%rax, (%rbx)
.L801:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L836
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L836:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26710:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextES4_NS0_6ObjectENS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EESD_PNS9_IS6_EEPNS9_IS7_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextES4_NS0_6ObjectENS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EESD_PNS9_IS6_EEPNS9_IS7_EE
	.section	.rodata._ZN2v88internal19ReflectGetAssembler22GenerateReflectGetImplEv.str1.1,"aMS",@progbits,1
.LC14:
	.string	"Reflect.get"
	.section	.text._ZN2v88internal19ReflectGetAssembler22GenerateReflectGetImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19ReflectGetAssembler22GenerateReflectGetImplEv
	.type	_ZN2v88internal19ReflectGetAssembler22GenerateReflectGetImplEv, @function
_ZN2v88internal19ReflectGetAssembler22GenerateReflectGetImplEv:
.LFB22484:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$4008, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -3784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r13, %rcx
	movq	%r12, %rsi
	leaq	-3712(%rbp), %rdi
	movq	%rax, %rdx
	leaq	-3352(%rbp), %r13
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-3712(%rbp), %r14
	movl	$2, %esi
	movq	%r12, %rdi
	movq	-3704(%rbp), %rax
	movq	-3696(%rbp), %rbx
	movq	%r12, -3584(%rbp)
	leaq	-3784(%rbp), %r12
	movq	%r14, -3552(%rbp)
	movq	%rax, -3840(%rbp)
	movq	$1, -3576(%rbp)
	movq	%rbx, -3568(%rbp)
	movq	%rax, -3560(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$96, %edi
	movq	$0, -3400(%rbp)
	movq	%rax, -3824(%rbp)
	movq	-3784(%rbp), %rax
	movq	$0, -3392(%rbp)
	movq	%rax, -3408(%rbp)
	movq	$0, -3384(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -3400(%rbp)
	leaq	-3408(%rbp), %rax
	movq	%rdx, -3384(%rbp)
	movq	%rdx, -3392(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3368(%rbp)
	movq	%rax, -3800(%rbp)
	movq	$0, -3376(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3784(%rbp), %rax
	movl	$120, %edi
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	%rax, -3216(%rbp)
	movq	$0, -3192(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-3160(%rbp), %rcx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -3192(%rbp)
	movq	%rdx, -3200(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3936(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -3176(%rbp)
	movq	%rax, -3208(%rbp)
	movq	$0, -3184(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3784(%rbp), %rax
	movl	$120, %edi
	movq	$0, -3016(%rbp)
	movq	$0, -3008(%rbp)
	movq	%rax, -3024(%rbp)
	movq	$0, -3000(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2968(%rbp), %rcx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -3000(%rbp)
	movq	%rdx, -3008(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3904(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2984(%rbp)
	movq	%rax, -3016(%rbp)
	movq	$0, -2992(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3784(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2824(%rbp)
	movq	$0, -2816(%rbp)
	movq	%rax, -2832(%rbp)
	movq	$0, -2808(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2776(%rbp), %rcx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rdx, -2808(%rbp)
	movq	%rdx, -2816(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3912(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2792(%rbp)
	movq	%rax, -2824(%rbp)
	movq	$0, -2800(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3784(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2632(%rbp)
	movq	$0, -2624(%rbp)
	movq	%rax, -2640(%rbp)
	movq	$0, -2616(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2584(%rbp), %rcx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rcx, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rdx, -2616(%rbp)
	movq	%rdx, -2624(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3896(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2600(%rbp)
	movq	%rax, -2632(%rbp)
	movq	$0, -2608(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-2448(%rbp), %rcx
	movl	$7, %edx
	movq	%r12, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -3920(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2256(%rbp), %rcx
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -3808(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2064(%rbp), %rcx
	movl	$6, %edx
	movq	%r12, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -3888(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-3784(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1864(%rbp)
	movq	$0, -1856(%rbp)
	movq	%rax, -1872(%rbp)
	movq	$0, -1848(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1816(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -1848(%rbp)
	movq	%rdx, -1856(%rbp)
	movq	%rcx, -4000(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movups	%xmm0, -1832(%rbp)
	movq	%rax, -1864(%rbp)
	movq	$0, -1840(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3784(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1672(%rbp)
	movq	$0, -1664(%rbp)
	movq	%rax, -1680(%rbp)
	movq	$0, -1656(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1624(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rcx, %rdi
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -1656(%rbp)
	movq	%rdx, -1664(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3984(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1640(%rbp)
	movq	%rax, -1672(%rbp)
	movq	$0, -1648(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1488(%rbp), %rcx
	movl	$7, %edx
	movq	%r12, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -3872(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-3784(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1288(%rbp)
	movq	$0, -1280(%rbp)
	movq	%rax, -1296(%rbp)
	movq	$0, -1272(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1240(%rbp), %rcx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -1272(%rbp)
	movq	%rdx, -1280(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3968(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1256(%rbp)
	movq	%rax, -1288(%rbp)
	movq	$0, -1264(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3784(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1096(%rbp)
	movq	$0, -1088(%rbp)
	movq	%rax, -1104(%rbp)
	movq	$0, -1080(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1048(%rbp), %rcx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rcx, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -1080(%rbp)
	movq	%rdx, -1088(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3952(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1064(%rbp)
	movq	%rax, -1096(%rbp)
	movq	$0, -1072(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-912(%rbp), %rcx
	movl	$9, %edx
	movq	%r12, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -3864(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-3784(%rbp), %rax
	movl	$216, %edi
	movq	$0, -712(%rbp)
	movq	$0, -704(%rbp)
	movq	%rax, -720(%rbp)
	movq	$0, -696(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-664(%rbp), %rcx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -696(%rbp)
	movq	%rdx, -704(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4008(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -680(%rbp)
	movq	%rax, -712(%rbp)
	movq	$0, -688(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3784(%rbp), %rax
	movl	$240, %edi
	movq	$0, -520(%rbp)
	movq	$0, -512(%rbp)
	movq	%rax, -528(%rbp)
	movq	$0, -504(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-472(%rbp), %rcx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rdx, -504(%rbp)
	movq	%rdx, -512(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4016(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -488(%rbp)
	movq	%rax, -520(%rbp)
	movq	$0, -496(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3784(%rbp), %rax
	movl	$240, %edi
	movq	$0, -328(%rbp)
	movq	$0, -320(%rbp)
	movq	%rax, -336(%rbp)
	movq	$0, -312(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-280(%rbp), %rsi
	xorl	%ecx, %ecx
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rdx, -312(%rbp)
	movq	%rdx, -320(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -3960(%rbp)
	movq	%r12, %rsi
	movq	%rax, -328(%rbp)
	movups	%xmm0, -296(%rbp)
	movq	$0, -304(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %xmm1
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movhps	-3840(%rbp), %xmm1
	movaps	%xmm0, -3536(%rbp)
	leaq	-3536(%rbp), %r14
	movaps	%xmm1, -144(%rbp)
	movq	%rbx, %xmm1
	movhps	-3824(%rbp), %xmm1
	movq	$0, -3520(%rbp)
	movaps	%xmm1, -128(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm4
	movdqa	-128(%rbp), %xmm5
	movq	%r14, %rsi
	movq	-3800(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L838
	call	_ZdlPv@PLT
.L838:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3216(%rbp), %rax
	cmpq	$0, -3344(%rbp)
	movq	%rax, -3856(%rbp)
	leaq	-3024(%rbp), %rax
	movq	%rax, -3848(%rbp)
	jne	.L992
.L839:
	leaq	-2832(%rbp), %rax
	cmpq	$0, -3152(%rbp)
	movq	%rax, -3880(%rbp)
	jne	.L993
.L843:
	leaq	-2640(%rbp), %rax
	cmpq	$0, -2960(%rbp)
	movq	%rax, -3824(%rbp)
	jne	.L994
	cmpq	$0, -2768(%rbp)
	jne	.L995
.L849:
	cmpq	$0, -2576(%rbp)
	jne	.L996
.L851:
	cmpq	$0, -2384(%rbp)
	jne	.L997
.L855:
	leaq	-1872(%rbp), %rax
	cmpq	$0, -2192(%rbp)
	movq	%rax, -3904(%rbp)
	jne	.L998
	cmpq	$0, -2000(%rbp)
	jne	.L999
.L861:
	leaq	-1680(%rbp), %rax
	cmpq	$0, -1808(%rbp)
	movq	%rax, -3896(%rbp)
	jne	.L1000
.L862:
	leaq	-1296(%rbp), %rax
	cmpq	$0, -1616(%rbp)
	movq	%rax, -3912(%rbp)
	jne	.L1001
.L865:
	leaq	-1104(%rbp), %rax
	cmpq	$0, -1424(%rbp)
	movq	%rax, -3840(%rbp)
	jne	.L1002
	cmpq	$0, -1232(%rbp)
	jne	.L1003
.L869:
	leaq	-720(%rbp), %rax
	cmpq	$0, -1040(%rbp)
	movq	%rax, -3936(%rbp)
	jne	.L1004
.L872:
	leaq	-528(%rbp), %rax
	cmpq	$0, -848(%rbp)
	movq	%rax, -3952(%rbp)
	jne	.L1005
.L876:
	cmpq	$0, -656(%rbp)
	leaq	-336(%rbp), %r13
	jne	.L1006
	cmpq	$0, -464(%rbp)
	jne	.L1007
.L882:
	cmpq	$0, -272(%rbp)
	jne	.L1008
.L885:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3952(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3936(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3864(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3840(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3912(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3872(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3896(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3904(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3888(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3808(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3920(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3824(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3880(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3848(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3856(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3800(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1009
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L992:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	-3800(%rbp), %rdi
	movq	%r14, %rsi
	movl	$117769477, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L840
	call	_ZdlPv@PLT
.L840:
	movq	(%rbx), %rax
	movl	$61, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	%rbx, -3840(%rbp)
	movq	16(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rcx, -3848(%rbp)
	movq	%rax, -3824(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$62, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17IntPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm6
	pxor	%xmm0, %xmm0
	movq	-3840(%rbp), %xmm7
	movhps	-3824(%rbp), %xmm6
	movl	$40, %edi
	movaps	%xmm0, -3536(%rbp)
	movhps	-3848(%rbp), %xmm7
	movaps	%xmm6, -3824(%rbp)
	movaps	%xmm7, -3840(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movq	%rbx, -112(%rbp)
	movq	$0, -3520(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm7
	movq	-112(%rbp), %rcx
	movq	%r14, %rsi
	leaq	40(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-128(%rbp), %xmm7
	movq	%rcx, 32(%rax)
	movups	%xmm7, 16(%rax)
	leaq	-3216(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	movq	%rax, -3856(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L841
	call	_ZdlPv@PLT
.L841:
	movdqa	-3840(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	%rbx, -112(%rbp)
	movaps	%xmm0, -3536(%rbp)
	movaps	%xmm7, -144(%rbp)
	movdqa	-3824(%rbp), %xmm7
	movq	$0, -3520(%rbp)
	movaps	%xmm7, -128(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movdqa	-128(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-144(%rbp), %xmm4
	leaq	40(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	leaq	-3024(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	movq	%rax, -3848(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L842
	call	_ZdlPv@PLT
.L842:
	movq	-3904(%rbp), %rcx
	movq	-3936(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L993:
	movq	-3936(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	-3856(%rbp), %rdi
	movq	%r14, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$5, 4(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L844
	call	_ZdlPv@PLT
.L844:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	(%rax), %rbx
	movq	24(%rax), %rsi
	movq	8(%rax), %rcx
	movq	%rbx, -3824(%rbp)
	movq	16(%rax), %rbx
	movq	32(%rax), %rax
	movq	%rsi, -3880(%rbp)
	xorl	%esi, %esi
	movq	%rcx, -3840(%rbp)
	movq	%rax, -3936(%rbp)
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-3824(%rbp), %xmm0
	movq	%rbx, -3664(%rbp)
	pushq	-3664(%rbp)
	movhps	-3840(%rbp), %xmm0
	movaps	%xmm0, -3680(%rbp)
	pushq	-3672(%rbp)
	pushq	-3680(%rbp)
	movaps	%xmm0, -3840(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r14, %rdi
	movq	%rax, -3824(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movdqa	-3840(%rbp), %xmm0
	movl	$48, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	movhps	-3880(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-3936(%rbp), %xmm0
	movhps	-3824(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm4
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm5
	leaq	48(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	leaq	-2832(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	movq	%rax, -3880(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L845
	call	_ZdlPv@PLT
.L845:
	movq	-3912(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L994:
	movq	-3904(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	-3848(%rbp), %rdi
	movq	%r14, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$5, 4(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L847
	call	_ZdlPv@PLT
.L847:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	8(%rax), %rbx
	movq	24(%rax), %rcx
	movq	(%rax), %r13
	movq	%rbx, -3824(%rbp)
	movq	16(%rax), %rbx
	movq	%rcx, -3904(%rbp)
	movq	%rbx, -3840(%rbp)
	movq	32(%rax), %rbx
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %xmm0
	movl	$48, %edi
	movq	$0, -3520(%rbp)
	movhps	-3824(%rbp), %xmm0
	movq	%rbx, -112(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	-3840(%rbp), %xmm0
	movq	%rax, -104(%rbp)
	movhps	-3904(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm4
	leaq	48(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	leaq	-2640(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	movq	%rax, -3824(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L848
	call	_ZdlPv@PLT
.L848:
	movq	-3896(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2768(%rbp)
	je	.L849
.L995:
	movq	-3912(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3760(%rbp)
	movq	$0, -3752(%rbp)
	movq	$0, -3744(%rbp)
	movq	$0, -3728(%rbp)
	movq	$0, -3648(%rbp)
	movq	$0, -3616(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3616(%rbp), %rax
	movq	-3880(%rbp), %rdi
	pushq	%rax
	leaq	-3744(%rbp), %rcx
	leaq	-3752(%rbp), %rdx
	leaq	-3760(%rbp), %rsi
	leaq	-3648(%rbp), %r9
	leaq	-3728(%rbp), %r8
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextES4_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EESC_PNS8_IS6_EE
	movl	$48, %edi
	movq	-3648(%rbp), %xmm0
	movq	-3744(%rbp), %xmm1
	movq	-3760(%rbp), %xmm2
	movq	$0, -3520(%rbp)
	movhps	-3616(%rbp), %xmm0
	movhps	-3728(%rbp), %xmm1
	movhps	-3752(%rbp), %xmm2
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	movq	-3824(%rbp), %rdi
	leaq	48(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	popq	%r10
	popq	%r11
	testq	%rdi, %rdi
	je	.L850
	call	_ZdlPv@PLT
.L850:
	movq	-3896(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2576(%rbp)
	je	.L851
.L996:
	movq	-3896(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3768(%rbp)
	leaq	-3616(%rbp), %r13
	movq	$0, -3760(%rbp)
	movq	$0, -3752(%rbp)
	movq	$0, -3744(%rbp)
	movq	$0, -3728(%rbp)
	movq	$0, -3648(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3648(%rbp), %rax
	movq	-3824(%rbp), %rdi
	pushq	%rax
	leaq	-3728(%rbp), %r9
	leaq	-3752(%rbp), %rcx
	leaq	-3744(%rbp), %r8
	leaq	-3760(%rbp), %rdx
	leaq	-3768(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextES4_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EESC_PNS8_IS6_EE
	movl	$63, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3648(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r15, %rdi
	movq	-3744(%rbp), %rsi
	call	_ZN2v88internal21Cast10JSReceiver_1404EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$64, %edi
	movq	-3728(%rbp), %xmm0
	movq	-3648(%rbp), %xmm3
	movq	-3752(%rbp), %xmm1
	movq	%rax, %xmm4
	movq	-3768(%rbp), %xmm2
	movq	$0, -3600(%rbp)
	movhps	-3648(%rbp), %xmm0
	punpcklqdq	%xmm4, %xmm3
	movhps	-3744(%rbp), %xmm1
	movhps	-3760(%rbp), %xmm2
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -3616(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm4
	leaq	64(%rax), %rdx
	movq	%rax, -3616(%rbp)
	movq	-3808(%rbp), %rdi
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm4, 48(%rax)
	movq	%rdx, -3600(%rbp)
	movq	%rdx, -3608(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3616(%rbp), %rdi
	popq	%r8
	popq	%r9
	testq	%rdi, %rdi
	je	.L852
	call	_ZdlPv@PLT
.L852:
	leaq	-2200(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3528(%rbp)
	jne	.L1010
.L853:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2384(%rbp)
	je	.L855
.L997:
	leaq	-2392(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movl	$2053, %edi
	movq	%r14, %rsi
	movw	%di, 4(%rax)
	movq	-3920(%rbp), %rdi
	leaq	7(%rax), %rdx
	movl	$117769477, (%rax)
	movb	$8, 6(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L856
	call	_ZdlPv@PLT
.L856:
	movq	(%rbx), %rax
	movl	$48, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movdqu	(%rax), %xmm5
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm3
	movq	%r14, %rsi
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	movq	-3888(%rbp), %rdi
	leaq	48(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L857
	call	_ZdlPv@PLT
.L857:
	leaq	-2008(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L998:
	leaq	-2200(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$506663775764808965, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	-3808(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L859
	call	_ZdlPv@PLT
.L859:
	movq	(%rbx), %rax
	movl	$56, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movq	56(%rax), %rdx
	movdqu	(%rax), %xmm5
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm3
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-144(%rbp), %xmm4
	leaq	56(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movq	%rcx, 48(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm6, 32(%rax)
	leaq	-1872(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	movq	%rax, -3904(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L860
	call	_ZdlPv@PLT
.L860:
	movq	-4000(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2000(%rbp)
	je	.L861
.L999:
	leaq	-2008(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3760(%rbp)
	movq	$0, -3752(%rbp)
	movq	$0, -3744(%rbp)
	movq	$0, -3728(%rbp)
	movq	$0, -3648(%rbp)
	movq	$0, -3616(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3616(%rbp), %rax
	movq	-3888(%rbp), %rdi
	pushq	%rax
	leaq	-3648(%rbp), %r9
	leaq	-3744(%rbp), %rcx
	leaq	-3728(%rbp), %r8
	leaq	-3752(%rbp), %rdx
	leaq	-3760(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextES4_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EESC_PNS8_IS6_EE
	movl	$64, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	movl	$26, %edx
	movq	%r14, %rdi
	movq	-3728(%rbp), %rsi
	leaq	.LC14(%rip), %rcx
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L1000:
	movq	-4000(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3768(%rbp)
	movq	$0, -3760(%rbp)
	movq	$0, -3752(%rbp)
	movq	$0, -3744(%rbp)
	movq	$0, -3728(%rbp)
	movq	$0, -3648(%rbp)
	movq	$0, -3616(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3616(%rbp), %rax
	movq	-3904(%rbp), %rdi
	leaq	-3728(%rbp), %r9
	pushq	%rax
	leaq	-3648(%rbp), %rax
	leaq	-3744(%rbp), %r8
	pushq	%rax
	leaq	-3752(%rbp), %rcx
	leaq	-3760(%rbp), %rdx
	leaq	-3768(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextES4_NS0_6ObjectENS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EESD_PNS9_IS6_EEPNS9_IS7_EE
	movl	$65, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3728(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17IntPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3728(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	-3760(%rbp), %rcx
	movq	-3752(%rbp), %rsi
	movq	-3744(%rbp), %rdx
	movaps	%xmm0, -3536(%rbp)
	movq	-3648(%rbp), %r11
	movq	-3768(%rbp), %rax
	movq	%rdi, -112(%rbp)
	movq	-3616(%rbp), %rbx
	movq	%rdi, -4024(%rbp)
	movl	$56, %edi
	movq	%rcx, -3912(%rbp)
	movq	%rsi, -3936(%rbp)
	movq	%rdx, -4000(%rbp)
	movq	%r11, -4032(%rbp)
	movq	%rcx, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%r11, -104(%rbp)
	movq	%rax, -3840(%rbp)
	movq	%rax, -144(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -3520(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm7
	leaq	56(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movq	%rcx, 48(%rax)
	movq	-3896(%rbp), %rdi
	movups	%xmm7, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm4, 32(%rax)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L863
	call	_ZdlPv@PLT
.L863:
	movq	-3840(%rbp), %xmm0
	movl	$56, %edi
	movq	%rbx, -96(%rbp)
	movq	$0, -3520(%rbp)
	movhps	-3912(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-3936(%rbp), %xmm0
	movhps	-4000(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-4024(%rbp), %xmm0
	movhps	-4032(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm6
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm3
	leaq	56(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movq	%rcx, 48(%rax)
	movq	-3872(%rbp), %rdi
	movups	%xmm3, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L864
	call	_ZdlPv@PLT
.L864:
	movq	-3984(%rbp), %rdx
	leaq	-1432(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L862
	.p2align 4,,10
	.p2align 3
.L1001:
	movq	-3984(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3776(%rbp)
	movq	$0, -3768(%rbp)
	movq	$0, -3760(%rbp)
	movq	$0, -3752(%rbp)
	movq	$0, -3744(%rbp)
	movq	$0, -3728(%rbp)
	movq	$0, -3616(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3616(%rbp), %rax
	movq	-3896(%rbp), %rdi
	leaq	-3760(%rbp), %rcx
	pushq	%rax
	leaq	-3728(%rbp), %rax
	leaq	-3744(%rbp), %r9
	pushq	%rax
	leaq	-3752(%rbp), %r8
	leaq	-3768(%rbp), %rdx
	leaq	-3776(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextES4_NS0_6ObjectENS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EESD_PNS9_IS6_EEPNS9_IS7_EE
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3768(%rbp), %rdx
	popq	%rbx
	movq	%r13, %rsi
	movq	-3776(%rbp), %rax
	movq	%r14, %rdi
	movq	%rdx, -3640(%rbp)
	movq	-3760(%rbp), %rdx
	movq	%rax, -3648(%rbp)
	movq	%rdx, -3632(%rbp)
	pushq	-3632(%rbp)
	pushq	-3640(%rbp)
	pushq	%rax
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3616(%rbp), %rax
	movl	$64, %edi
	movq	-3744(%rbp), %xmm0
	movq	-3760(%rbp), %xmm1
	movq	%rbx, -88(%rbp)
	movq	-3776(%rbp), %xmm2
	movq	%rax, -96(%rbp)
	movhps	-3728(%rbp), %xmm0
	movhps	-3752(%rbp), %xmm1
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3768(%rbp), %xmm2
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm0, -3536(%rbp)
	movq	$0, -3520(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm4
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm2
	leaq	64(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movq	-3912(%rbp), %rdi
	movups	%xmm5, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm2, 48(%rax)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L866
	call	_ZdlPv@PLT
.L866:
	movq	-3968(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L1002:
	leaq	-1432(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3776(%rbp)
	movq	$0, -3768(%rbp)
	movq	$0, -3760(%rbp)
	movq	$0, -3752(%rbp)
	movq	$0, -3744(%rbp)
	movq	$0, -3728(%rbp)
	movq	$0, -3616(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3616(%rbp), %rax
	movq	-3872(%rbp), %rdi
	leaq	-3760(%rbp), %rcx
	pushq	%rax
	leaq	-3728(%rbp), %rax
	leaq	-3768(%rbp), %rdx
	pushq	%rax
	leaq	-3776(%rbp), %rsi
	leaq	-3744(%rbp), %r9
	leaq	-3752(%rbp), %r8
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextES4_NS0_6ObjectENS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EESB_PNS9_IS4_EEPNS9_IS5_EESD_PNS9_IS6_EEPNS9_IS7_EE
	movq	%r15, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$64, %edi
	movq	-3616(%rbp), %xmm0
	movq	-3744(%rbp), %xmm1
	movq	-3760(%rbp), %xmm2
	movq	%rax, %xmm6
	movq	-3776(%rbp), %xmm3
	movq	$0, -3520(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movhps	-3728(%rbp), %xmm1
	movhps	-3752(%rbp), %xmm2
	movhps	-3768(%rbp), %xmm3
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm3
	leaq	64(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movq	-3840(%rbp), %rdi
	movups	%xmm7, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	popq	%r10
	popq	%r11
	testq	%rdi, %rdi
	je	.L868
	call	_ZdlPv@PLT
.L868:
	movq	-3952(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1232(%rbp)
	je	.L869
.L1003:
	movq	-3968(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$578439894826026245, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	-3912(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L870
	call	_ZdlPv@PLT
.L870:
	movq	(%rbx), %rax
	movl	$64, %edi
	movdqu	48(%rax), %xmm0
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movdqu	(%rax), %xmm6
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm4
	leaq	64(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movq	-3840(%rbp), %rdi
	movups	%xmm2, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm4, 48(%rax)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L871
	call	_ZdlPv@PLT
.L871:
	movq	-3952(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L1004:
	movq	-3952(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$578439894826026245, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	-3840(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L873
	call	_ZdlPv@PLT
.L873:
	movq	(%rbx), %rax
	leaq	-3728(%rbp), %r13
	movl	$66, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rbx
	movq	16(%rax), %rcx
	movq	%rsi, -3968(%rbp)
	movq	40(%rax), %rsi
	movq	%rbx, -3984(%rbp)
	movq	24(%rax), %rbx
	movq	%rsi, -4032(%rbp)
	movq	48(%rax), %rsi
	movq	%rcx, -4000(%rbp)
	movq	32(%rax), %rcx
	movq	56(%rax), %rax
	movq	%rsi, -4040(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -3936(%rbp)
	movq	%rax, -3952(%rbp)
	movq	%rbx, -4024(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$99, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-3536(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-144(%rbp), %rsi
	movl	$1, %edi
	movq	%rbx, %r9
	movq	%rax, %r8
	movq	-3520(%rbp), %rax
	pushq	%rdi
	movl	$1, %ecx
	pushq	%rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, -3608(%rbp)
	movq	-3952(%rbp), %rax
	movq	%rdx, -3616(%rbp)
	leaq	-3616(%rbp), %rdx
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$67, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$2, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3936(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17IntPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movq	-3936(%rbp), %xmm4
	movq	-4000(%rbp), %xmm5
	movaps	%xmm0, -3536(%rbp)
	movq	-3984(%rbp), %xmm6
	movq	-4040(%rbp), %xmm3
	movq	%rbx, -80(%rbp)
	movhps	-4032(%rbp), %xmm4
	movhps	-4024(%rbp), %xmm5
	movhps	-3968(%rbp), %xmm6
	movaps	%xmm4, -3936(%rbp)
	movhps	-3952(%rbp), %xmm3
	movaps	%xmm5, -4000(%rbp)
	movaps	%xmm3, -3952(%rbp)
	movaps	%xmm6, -3984(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movq	$0, -3520(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm2
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	72(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movdqa	-144(%rbp), %xmm3
	movq	-3864(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	popq	%r8
	popq	%r9
	testq	%rdi, %rdi
	je	.L874
	call	_ZdlPv@PLT
.L874:
	movdqa	-3984(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movq	%rbx, -80(%rbp)
	movl	$72, %edi
	movdqa	-4000(%rbp), %xmm4
	movdqa	-3936(%rbp), %xmm3
	movaps	%xmm0, -3536(%rbp)
	movdqa	-3952(%rbp), %xmm2
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	$0, -3520(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-144(%rbp), %xmm6
	leaq	72(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movdqa	-96(%rbp), %xmm4
	movq	%rcx, 64(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm4, 48(%rax)
	leaq	-720(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	movq	%rax, -3936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L875
	call	_ZdlPv@PLT
.L875:
	movq	-4008(%rbp), %rcx
	leaq	-856(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L1005:
	leaq	-856(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$578439894826026245, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	-3864(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L877
	call	_ZdlPv@PLT
.L877:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	32(%rax), %rsi
	movq	56(%rax), %rdx
	movq	%rcx, -3984(%rbp)
	movq	24(%rax), %rcx
	movq	%rbx, -3952(%rbp)
	movq	16(%rax), %rbx
	movq	%rcx, -3968(%rbp)
	movq	40(%rax), %rcx
	movq	%rsi, -4000(%rbp)
	movl	$2, %esi
	movq	%rcx, -4024(%rbp)
	movq	48(%rax), %rcx
	movq	64(%rax), %rax
	movq	%rdx, -4040(%rbp)
	movq	%rcx, -4032(%rbp)
	movq	%rax, -4048(%rbp)
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-3952(%rbp), %xmm0
	movq	%rbx, -3600(%rbp)
	pushq	-3600(%rbp)
	movhps	-3984(%rbp), %xmm0
	movaps	%xmm0, -3616(%rbp)
	pushq	-3608(%rbp)
	pushq	-3616(%rbp)
	movaps	%xmm0, -3984(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r14, %rdi
	movq	%rax, -3952(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movdqa	-3984(%rbp), %xmm0
	movl	$80, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	movhps	-3968(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-4000(%rbp), %xmm0
	movhps	-4024(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-4032(%rbp), %xmm0
	movhps	-4040(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-4048(%rbp), %xmm0
	movhps	-3952(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm2
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	80(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movdqa	-80(%rbp), %xmm5
	movups	%xmm3, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm5, 64(%rax)
	leaq	-528(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	movq	%rax, -3952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L878
	call	_ZdlPv@PLT
.L878:
	movq	-4016(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L1006:
	movq	-4008(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$578439894826026245, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	-3936(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L880
	call	_ZdlPv@PLT
.L880:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	leaq	-336(%rbp), %r13
	movq	40(%rax), %rdi
	movq	8(%rax), %r11
	movq	16(%rax), %r10
	movq	24(%rax), %r9
	movq	32(%rax), %r8
	movq	48(%rax), %rdx
	movq	56(%rax), %rsi
	movq	64(%rax), %rcx
	movq	(%rax), %rax
	movq	%rdi, -104(%rbp)
	movl	$80, %edi
	movq	%r11, -136(%rbp)
	movq	%r10, -128(%rbp)
	movq	%r9, -120(%rbp)
	movq	%r8, -112(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movaps	%xmm0, -3536(%rbp)
	movq	%rax, -144(%rbp)
	movq	$0, -3520(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm3
	movq	%r13, %rdi
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm6
	leaq	80(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movdqa	-144(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm7
	movups	%xmm3, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L881
	call	_ZdlPv@PLT
.L881:
	movq	-3960(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -464(%rbp)
	je	.L882
.L1007:
	movq	-4016(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$578439894826026245, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	-3952(%rbp), %rdi
	movq	%rbx, (%rax)
	leaq	10(%rax), %rdx
	movw	%si, 8(%rax)
	movq	%r14, %rsi
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L883
	call	_ZdlPv@PLT
.L883:
	movq	(%rbx), %rax
	movl	$80, %edi
	movdqu	64(%rax), %xmm0
	movdqu	16(%rax), %xmm3
	movdqu	32(%rax), %xmm2
	movdqu	48(%rax), %xmm1
	movdqu	(%rax), %xmm5
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -3536(%rbp)
	movq	$0, -3520(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm3
	movq	%r13, %rdi
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm6
	leaq	80(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movdqa	-144(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm7
	movups	%xmm3, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L884
	call	_ZdlPv@PLT
.L884:
	movq	-3960(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -272(%rbp)
	je	.L885
.L1008:
	movq	-3960(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$578439894826026245, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	10(%rax), %rdx
	movw	%cx, 8(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L886
	call	_ZdlPv@PLT
.L886:
	movq	(%rbx), %rax
	movl	$69, %edx
	movq	%r12, %rdi
	movq	64(%rax), %rsi
	movq	24(%rax), %r9
	movq	48(%rax), %rbx
	movq	%rsi, -3984(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%r9, -4008(%rbp)
	movq	%rbx, -3960(%rbp)
	movq	72(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r14, %rdi
	movq	%rax, -3968(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$68, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-3744(%rbp), %r10
	movq	%r15, %rsi
	movq	%r10, %rdi
	movq	%r10, -4000(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4000(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$711, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-4000(%rbp), %r10
	movq	-3536(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-144(%rbp), %rcx
	xorl	%esi, %esi
	movq	-3960(%rbp), %xmm0
	movq	-4000(%rbp), %r10
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-4008(%rbp), %r9
	movhps	-3984(%rbp), %xmm0
	movq	%rax, -3728(%rbp)
	movq	-3520(%rbp), %rax
	leaq	-3728(%rbp), %rdx
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	movq	%r10, %rdi
	movl	$4, %ebx
	pushq	%rbx
	movhps	-3968(%rbp), %xmm0
	pushq	%rcx
	movl	$1, %ecx
	movaps	%xmm0, -128(%rbp)
	movq	%r10, -3960(%rbp)
	movq	%rax, -3720(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-3960(%rbp), %r10
	movq	%rax, %rbx
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-3584(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	popq	%rax
	popq	%rdx
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L1010:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3768(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	-3648(%rbp), %rax
	leaq	-144(%rbp), %rsi
	movq	%r13, %rdi
	movaps	%xmm0, -3616(%rbp)
	movq	%rdx, -144(%rbp)
	movq	-3760(%rbp), %rdx
	movq	%rax, -104(%rbp)
	movq	%rdx, -136(%rbp)
	movq	-3752(%rbp), %rdx
	movq	%rax, -96(%rbp)
	movq	%rdx, -128(%rbp)
	movq	-3744(%rbp), %rdx
	movq	$0, -3600(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-3728(%rbp), %rdx
	movq	%rdx, -112(%rbp)
	leaq	-88(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3920(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3616(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L854
	call	_ZdlPv@PLT
.L854:
	leaq	-2392(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L853
.L1009:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22484:
	.size	_ZN2v88internal19ReflectGetAssembler22GenerateReflectGetImplEv, .-_ZN2v88internal19ReflectGetAssembler22GenerateReflectGetImplEv
	.section	.rodata._ZN2v88internal8Builtins19Generate_ReflectGetEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"ReflectGet"
	.section	.text._ZN2v88internal8Builtins19Generate_ReflectGetEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins19Generate_ReflectGetEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins19Generate_ReflectGetEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins19Generate_ReflectGetEPNS0_8compiler18CodeAssemblerStateE:
.LFB22480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$469, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC15(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$865, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L1015
.L1012:
	movq	%r13, %rdi
	call	_ZN2v88internal19ReflectGetAssembler22GenerateReflectGetImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1016
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1015:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L1012
.L1016:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22480:
	.size	_ZN2v88internal8Builtins19Generate_ReflectGetEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins19Generate_ReflectGetEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_ReflectIsExtensibleEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_ReflectIsExtensibleEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_ReflectIsExtensibleEPNS0_8compiler18CodeAssemblerStateE:
.LFB29189:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29189:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_ReflectIsExtensibleEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_ReflectIsExtensibleEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_ReflectIsExtensibleEPNS0_8compiler18CodeAssemblerStateE
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
