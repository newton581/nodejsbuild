	.file	"array-foreach-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0:
.LFB30261:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	%rsi, %r13
	pushq	%rbx
	movq	%r13, %rax
	sarq	$3, %rax
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%r13, %r13
	js	.L14
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%rdx, %r14
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.L9
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%rax, %rdi
.L9:
	leaq	(%rdi,%r13), %r15
	movq	%rdi, (%rbx)
	movq	%r15, 16(%rbx)
	cmpq	%r12, %r14
	je	.L10
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
.L10:
	movq	%r15, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE30261:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0:
.LFB30260:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	subq	%rsi, %rdx
	js	.L21
	movq	%rdi, %rbx
	je	.L17
	movq	%rdx, %rdi
	movq	%rdx, %r12
	movq	%rsi, %r13
	call	_Znwm@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	leaq	(%rax,%r12), %r14
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	%r14, 16(%rbx)
	call	memcpy@PLT
	movq	%r14, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	xorl	%r14d, %r14d
	movq	$0, (%rdi)
	movq	$0, 16(%rdi)
	movq	%r14, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE30260:
	.size	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0:
.LFB30259:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	movabsq	$384307168202282325, %rax
	cmpq	%rax, %rdx
	ja	.L39
	pxor	%xmm0, %xmm0
	leaq	(%rdx,%rdx,2), %r14
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	%rdx, %r12
	movups	%xmm0, 8(%rdi)
	salq	$3, %r14
	xorl	%esi, %esi
	movq	$0, 24(%rdi)
	testq	%rdx, %rdx
	je	.L28
	movq	%r14, %rdi
	call	_Znwm@PLT
	leaq	-1(%r12), %rdx
	leaq	(%rax,%r14), %rsi
	movq	%rax, 8(%rbx)
	movq	%rsi, 24(%rbx)
	cmpq	$2, %rdx
	jbe	.L29
	movq	%r12, %rdi
	movq	%r12, %rcx
	pxor	%xmm0, %xmm0
	movq	%rax, %rdx
	shrq	%rdi
	andq	$-2, %rcx
	addq	%rdi, %rcx
	salq	$4, %rcx
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L26:
	movups	%xmm0, (%rdx)
	addq	$48, %rdx
	movups	%xmm0, -32(%rdx)
	movups	%xmm0, -16(%rdx)
	cmpq	%rcx, %rdx
	jne	.L26
	movq	%r12, %rdx
	movq	%r12, %rcx
	andq	$-2, %rdx
	andl	$1, %ecx
	leaq	(%rdx,%rdx,2), %rdi
	leaq	(%rax,%rdi,8), %rax
	cmpq	%rdx, %r12
	je	.L28
.L25:
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	cmpq	$1, %rcx
	je	.L28
	movq	$0, 40(%rax)
	movups	%xmm0, 24(%rax)
	cmpq	$2, %rcx
	je	.L28
	movq	$0, 64(%rax)
	movups	%xmm0, 48(%rax)
.L28:
	movq	%rsi, 16(%rbx)
	pxor	%xmm0, %xmm0
	leaq	56(%rbx), %rdi
	movq	%r13, %rsi
	movups	%xmm0, 32(%rbx)
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	$0, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	%r12, %rcx
	jmp	.L25
.L39:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE30259:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev,"axG",@progbits,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev:
.LFB22421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$56, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L41
	call	_ZdlPv@PLT
.L41:
	movq	16(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	%r12, %r13
	je	.L42
	.p2align 4,,10
	.p2align 3
.L46:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L43
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L46
.L44:
	movq	8(%rbx), %r12
.L42:
	testq	%r12, %r12
	je	.L40
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L46
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L40:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22421:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev
	.set	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.section	.rodata._ZN2v88internal37ArrayForEachLoopContinuationAssembler40GenerateArrayForEachLoopContinuationImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../../deps/v8/../../deps/v8/src/builtins/array-foreach.tq"
	.section	.text._ZN2v88internal37ArrayForEachLoopContinuationAssembler40GenerateArrayForEachLoopContinuationImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal37ArrayForEachLoopContinuationAssembler40GenerateArrayForEachLoopContinuationImplEv
	.type	_ZN2v88internal37ArrayForEachLoopContinuationAssembler40GenerateArrayForEachLoopContinuationImplEv, @function
_ZN2v88internal37ArrayForEachLoopContinuationAssembler40GenerateArrayForEachLoopContinuationImplEv:
.LFB22511:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1520(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1560(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1768, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, -1576(%rbp)
	movq	%rax, -1560(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -1728(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -1648(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, -1664(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, -1680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%rax, -1696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$5, %esi
	movq	%rax, -1712(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$6, %esi
	movq	%rax, -1720(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$7, %esi
	movq	%r12, %rdi
	leaq	-1488(%rbp), %r12
	movq	%rax, -1744(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$216, %edi
	movq	$0, -1480(%rbp)
	movq	$0, -1472(%rbp)
	movq	%rax, %rbx
	movq	-1560(%rbp), %rax
	movq	$0, -1464(%rbp)
	movq	%rax, -1488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1480(%rbp)
	leaq	-1432(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1464(%rbp)
	movq	%rdx, -1472(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1448(%rbp)
	movq	%rax, -1584(%rbp)
	movq	$0, -1456(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1560(%rbp), %rax
	movl	$240, %edi
	movq	$0, -1288(%rbp)
	movq	$0, -1280(%rbp)
	movq	%rax, -1296(%rbp)
	movq	$0, -1272(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -1288(%rbp)
	leaq	-1240(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1272(%rbp)
	movq	%rdx, -1280(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1256(%rbp)
	movq	%rax, -1592(%rbp)
	movq	$0, -1264(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1560(%rbp), %rax
	movl	$240, %edi
	movq	$0, -1096(%rbp)
	movq	$0, -1088(%rbp)
	movq	%rax, -1104(%rbp)
	movq	$0, -1080(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -1096(%rbp)
	leaq	-1048(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1080(%rbp)
	movq	%rdx, -1088(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1064(%rbp)
	movq	%rax, -1608(%rbp)
	movq	$0, -1072(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1560(%rbp), %rax
	movl	$264, %edi
	movq	$0, -904(%rbp)
	movq	$0, -896(%rbp)
	movq	%rax, -912(%rbp)
	movq	$0, -888(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -904(%rbp)
	leaq	-856(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -888(%rbp)
	movq	%rdx, -896(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -872(%rbp)
	movq	%rax, -1616(%rbp)
	movq	$0, -880(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1560(%rbp), %rax
	movl	$264, %edi
	movq	$0, -712(%rbp)
	movq	$0, -704(%rbp)
	movq	%rax, -720(%rbp)
	movq	$0, -696(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -712(%rbp)
	leaq	-664(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -696(%rbp)
	movq	%rdx, -704(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -680(%rbp)
	movq	%rax, -1600(%rbp)
	movq	$0, -688(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1560(%rbp), %rax
	movl	$240, %edi
	movq	$0, -520(%rbp)
	movq	$0, -512(%rbp)
	movq	%rax, -528(%rbp)
	movq	$0, -504(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -520(%rbp)
	leaq	-472(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -504(%rbp)
	movq	%rdx, -512(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -488(%rbp)
	movq	%rax, -1624(%rbp)
	movq	$0, -496(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1560(%rbp), %rax
	movl	$240, %edi
	movq	$0, -328(%rbp)
	movq	$0, -320(%rbp)
	movq	%rax, -336(%rbp)
	movq	$0, -312(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -328(%rbp)
	leaq	-280(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -312(%rbp)
	movq	%rdx, -320(%rbp)
	xorl	%edx, %edx
	movq	%rax, -1632(%rbp)
	movups	%xmm0, -296(%rbp)
	movq	$0, -304(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movq	-1728(%rbp), %xmm1
	movaps	%xmm0, -1520(%rbp)
	movhps	-1648(%rbp), %xmm1
	movq	%rbx, -80(%rbp)
	movaps	%xmm1, -144(%rbp)
	movq	-1664(%rbp), %xmm1
	movq	$0, -1504(%rbp)
	movhps	-1680(%rbp), %xmm1
	movaps	%xmm1, -128(%rbp)
	movq	-1696(%rbp), %xmm1
	movhps	-1712(%rbp), %xmm1
	movaps	%xmm1, -112(%rbp)
	movq	-1720(%rbp), %xmm1
	movhps	-1744(%rbp), %xmm1
	movaps	%xmm1, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	leaq	72(%rax), %rdx
	movq	%rax, -1520(%rbp)
	movdqa	-144(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm2
	movq	%rcx, 64(%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm2, 48(%rax)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L53
	call	_ZdlPv@PLT
.L53:
	movq	-1584(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1424(%rbp)
	jne	.L221
	cmpq	$0, -1232(%rbp)
	jne	.L222
.L57:
	cmpq	$0, -1040(%rbp)
	jne	.L223
.L61:
	cmpq	$0, -848(%rbp)
	jne	.L224
.L65:
	cmpq	$0, -656(%rbp)
	jne	.L225
.L71:
	cmpq	$0, -464(%rbp)
	jne	.L226
.L74:
	cmpq	$0, -272(%rbp)
	jne	.L227
.L77:
	movq	-1632(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L79
	call	_ZdlPv@PLT
.L79:
	movq	-320(%rbp), %rbx
	movq	-328(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L80
	.p2align 4,,10
	.p2align 3
.L84:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L81
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L84
.L82:
	movq	-328(%rbp), %r12
.L80:
	testq	%r12, %r12
	je	.L85
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L85:
	movq	-1624(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-496(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L86
	call	_ZdlPv@PLT
.L86:
	movq	-512(%rbp), %rbx
	movq	-520(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L87
	.p2align 4,,10
	.p2align 3
.L91:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L88
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L91
.L89:
	movq	-520(%rbp), %r12
.L87:
	testq	%r12, %r12
	je	.L92
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L92:
	movq	-1600(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L93
	call	_ZdlPv@PLT
.L93:
	movq	-704(%rbp), %rbx
	movq	-712(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L94
	.p2align 4,,10
	.p2align 3
.L98:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L95
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L98
.L96:
	movq	-712(%rbp), %r12
.L94:
	testq	%r12, %r12
	je	.L99
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L99:
	movq	-1616(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L100
	call	_ZdlPv@PLT
.L100:
	movq	-896(%rbp), %rbx
	movq	-904(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L101
	.p2align 4,,10
	.p2align 3
.L105:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L102
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L105
.L103:
	movq	-904(%rbp), %r12
.L101:
	testq	%r12, %r12
	je	.L106
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L106:
	movq	-1608(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L107
	call	_ZdlPv@PLT
.L107:
	movq	-1088(%rbp), %rbx
	movq	-1096(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L108
	.p2align 4,,10
	.p2align 3
.L112:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L109
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L112
.L110:
	movq	-1096(%rbp), %r12
.L108:
	testq	%r12, %r12
	je	.L113
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L113:
	movq	-1592(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L114
	call	_ZdlPv@PLT
.L114:
	movq	-1280(%rbp), %rbx
	movq	-1288(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L115
	.p2align 4,,10
	.p2align 3
.L119:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L116
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L119
.L117:
	movq	-1288(%rbp), %r12
.L115:
	testq	%r12, %r12
	je	.L120
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L120:
	movq	-1584(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1456(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L121
	call	_ZdlPv@PLT
.L121:
	movq	-1472(%rbp), %rbx
	movq	-1480(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L122
	.p2align 4,,10
	.p2align 3
.L126:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L123
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L126
.L124:
	movq	-1480(%rbp), %r12
.L122:
	testq	%r12, %r12
	je	.L127
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L127:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L228
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L126
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L116:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L119
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L109:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L112
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L102:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L105
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L95:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L98
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L81:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L84
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L88:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L91
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L221:
	movq	-1584(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -1504(%rbp)
	movaps	%xmm0, -1520(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movabsq	$578720283192919815, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1520(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L55
	call	_ZdlPv@PLT
.L55:
	movq	(%rbx), %rax
	movl	$49, %edx
	movq	%r13, %rdi
	movq	24(%rax), %rsi
	movq	8(%rax), %rcx
	movq	(%rax), %r12
	movq	32(%rax), %r15
	movq	%rsi, -1696(%rbp)
	movq	40(%rax), %rsi
	movq	%rcx, -1664(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -1712(%rbp)
	movq	56(%rax), %rsi
	movq	%rcx, -1680(%rbp)
	movq	48(%rax), %rcx
	movq	%rsi, -1720(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	64(%rax), %rbx
	movq	%rcx, -1648(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %xmm0
	movl	$80, %edi
	movq	$0, -1504(%rbp)
	movhps	-1664(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-1680(%rbp), %xmm0
	movhps	-1696(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%r15, %xmm0
	movhps	-1712(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-1648(%rbp), %xmm0
	movhps	-1720(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	movhps	-1648(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1520(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm7
	leaq	-1296(%rbp), %rdi
	movq	%r14, %rsi
	leaq	80(%rax), %rdx
	movq	%rax, -1520(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-128(%rbp), %xmm7
	movq	%rdx, -1504(%rbp)
	movups	%xmm7, 16(%rax)
	movdqa	-112(%rbp), %xmm7
	movq	%rdx, -1512(%rbp)
	movups	%xmm7, 32(%rax)
	movdqa	-96(%rbp), %xmm7
	movups	%xmm7, 48(%rax)
	movdqa	-80(%rbp), %xmm7
	movups	%xmm7, 64(%rax)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L56
	call	_ZdlPv@PLT
.L56:
	movq	-1592(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1232(%rbp)
	je	.L57
.L222:
	movq	-1592(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-1296(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -1504(%rbp)
	movaps	%xmm0, -1520(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movabsq	$578720283192919815, %rcx
	movq	%rcx, (%rax)
	movl	$2056, %ecx
	leaq	10(%rax), %rdx
	movw	%cx, 8(%rax)
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1520(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L58
	call	_ZdlPv@PLT
.L58:
	movq	(%rbx), %rax
	movq	40(%rax), %rbx
	movq	8(%rax), %rsi
	movq	24(%rax), %rdx
	movq	64(%rax), %rdi
	movq	72(%rax), %r15
	movq	(%rax), %rcx
	movq	%rbx, -1728(%rbp)
	movq	48(%rax), %rbx
	movq	%rsi, -1696(%rbp)
	movq	%rdx, -1720(%rbp)
	movq	16(%rax), %rsi
	movq	32(%rax), %rdx
	movq	%rbx, -1680(%rbp)
	movq	56(%rax), %rbx
	movq	%rdi, -1664(%rbp)
	movq	-1576(%rbp), %rdi
	movq	%rsi, -1712(%rbp)
	movq	%r15, %rsi
	movq	%rdx, -1744(%rbp)
	movq	%rbx, %rdx
	movq	%rcx, -1648(%rbp)
	call	_ZN2v88internal19NumberIsLessThan_75EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	movq	%rbx, %xmm5
	movq	%r15, %xmm2
	movq	-1680(%rbp), %xmm4
	movq	-1664(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movl	$80, %edi
	movq	%rax, %r12
	punpcklqdq	%xmm5, %xmm4
	movaps	%xmm0, -1520(%rbp)
	movq	-1744(%rbp), %xmm5
	movq	-1712(%rbp), %xmm6
	movq	-1648(%rbp), %xmm7
	punpcklqdq	%xmm2, %xmm3
	movaps	%xmm4, -1680(%rbp)
	movhps	-1728(%rbp), %xmm5
	movhps	-1720(%rbp), %xmm6
	movaps	%xmm3, -1664(%rbp)
	movhps	-1696(%rbp), %xmm7
	movaps	%xmm5, -1744(%rbp)
	movaps	%xmm6, -1712(%rbp)
	movaps	%xmm7, -1648(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movq	$0, -1504(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm5
	leaq	80(%rax), %rdx
	leaq	-1104(%rbp), %rdi
	movups	%xmm6, (%rax)
	movdqa	-80(%rbp), %xmm6
	movups	%xmm7, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L59
	call	_ZdlPv@PLT
.L59:
	movdqa	-1648(%rbp), %xmm7
	movdqa	-1712(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movl	$80, %edi
	movdqa	-1744(%rbp), %xmm4
	movdqa	-1680(%rbp), %xmm2
	movaps	%xmm0, -1520(%rbp)
	movdqa	-1664(%rbp), %xmm5
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movq	$0, -1504(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm7
	leaq	-336(%rbp), %rdi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm4
	leaq	80(%rax), %rdx
	movq	%r14, %rsi
	movdqa	-80(%rbp), %xmm2
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm2, 64(%rax)
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L60
	call	_ZdlPv@PLT
.L60:
	movq	-1632(%rbp), %rcx
	movq	-1608(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1040(%rbp)
	je	.L61
.L223:
	movq	-1608(%rbp), %rsi
	movq	%r13, %rdi
	movl	$2056, %r15d
	leaq	-1104(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -1504(%rbp)
	movaps	%xmm0, -1520(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movabsq	$578720283192919815, %rcx
	movq	%rcx, (%rax)
	leaq	10(%rax), %rdx
	movw	%r15w, 8(%rax)
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1520(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L62
	call	_ZdlPv@PLT
.L62:
	movq	(%rbx), %rax
	movq	24(%rax), %rsi
	movq	56(%rax), %rdi
	movq	8(%rax), %rcx
	movq	48(%rax), %rdx
	movq	40(%rax), %rbx
	movq	(%rax), %r12
	movq	%rsi, -1696(%rbp)
	movq	%rdi, -1728(%rbp)
	movq	32(%rax), %rsi
	movq	64(%rax), %rdi
	movq	%rcx, -1664(%rbp)
	movq	16(%rax), %rcx
	movq	72(%rax), %rax
	movq	%rsi, -1712(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -1744(%rbp)
	movl	$55, %edx
	movq	%rdi, -1752(%rbp)
	movq	%r13, %rdi
	movq	%rcx, -1680(%rbp)
	movq	%rax, -1648(%rbp)
	movq	%rbx, -1720(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1576(%rbp), %r15
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	-1648(%rbp), %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler11HasPropertyENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_NS1_21HasPropertyLookupModeE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$58, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	movq	%r15, -1576(%rbp)
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1576(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %xmm6
	pxor	%xmm0, %xmm0
	movq	-1744(%rbp), %xmm3
	movhps	-1664(%rbp), %xmm6
	movl	$88, %edi
	movq	%rbx, -64(%rbp)
	movq	-1712(%rbp), %xmm4
	movq	-1680(%rbp), %xmm5
	movhps	-1728(%rbp), %xmm3
	movq	-1752(%rbp), %xmm2
	movaps	%xmm6, -1664(%rbp)
	movhps	-1720(%rbp), %xmm4
	movaps	%xmm3, -1744(%rbp)
	movhps	-1648(%rbp), %xmm2
	movhps	-1696(%rbp), %xmm5
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm2, -1648(%rbp)
	movaps	%xmm4, -1712(%rbp)
	movaps	%xmm5, -1680(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm2, -80(%rbp)
	movaps	%xmm0, -1520(%rbp)
	movq	$0, -1504(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-128(%rbp), %xmm6
	leaq	-912(%rbp), %rdi
	movdqa	-144(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm7
	leaq	88(%rax), %rdx
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movq	%rcx, 80(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm4, 64(%rax)
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L63
	call	_ZdlPv@PLT
.L63:
	movdqa	-1664(%rbp), %xmm2
	movdqa	-1680(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movl	$88, %edi
	movdqa	-1712(%rbp), %xmm6
	movdqa	-1744(%rbp), %xmm7
	movq	%rbx, -64(%rbp)
	movdqa	-1648(%rbp), %xmm3
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm0, -1520(%rbp)
	movq	$0, -1504(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-128(%rbp), %xmm2
	leaq	-720(%rbp), %rdi
	movdqa	-144(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm5
	leaq	88(%rax), %rdx
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%rcx, 80(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L64
	call	_ZdlPv@PLT
.L64:
	movq	-1600(%rbp), %rcx
	movq	-1616(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -848(%rbp)
	je	.L65
.L224:
	movq	-1616(%rbp), %rsi
	movq	%r13, %rdi
	movl	$2056, %ebx
	leaq	-912(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$11, %edi
	movq	$0, -1504(%rbp)
	movaps	%xmm0, -1520(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movabsq	$578720283192919815, %rcx
	movw	%bx, 8(%rax)
	leaq	11(%rax), %rdx
	movq	%rcx, (%rax)
	movb	$7, 10(%rax)
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1520(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L66
	call	_ZdlPv@PLT
.L66:
	movq	(%rbx), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	leaq	-1552(%rbp), %r15
	leaq	-1536(%rbp), %r12
	movq	32(%rax), %rdx
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	%rdx, -1720(%rbp)
	movq	40(%rax), %rdx
	movq	%rcx, -1712(%rbp)
	movq	16(%rax), %rcx
	movq	%rdx, -1664(%rbp)
	movq	48(%rax), %rdx
	movq	%rcx, -1696(%rbp)
	movq	24(%rax), %rcx
	movq	%rdx, -1744(%rbp)
	movq	56(%rax), %rdx
	movq	%rcx, -1648(%rbp)
	movq	%rdx, -1728(%rbp)
	movq	64(%rax), %rdx
	movq	%rdx, -1752(%rbp)
	movq	72(%rax), %rdx
	movq	80(%rax), %rax
	movq	%rdx, -1680(%rbp)
	movl	$60, %edx
	movq	%rax, -1760(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1576(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-1520(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	movq	%rbx, %r9
	movq	%r12, %rdx
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rdi
	movl	$1, %ecx
	movq	%rax, -1536(%rbp)
	movq	-1504(%rbp), %rax
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	-1664(%rbp), %xmm0
	movq	%rax, -1528(%rbp)
	leaq	-144(%rbp), %rax
	pushq	%rax
	movhps	-1680(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%rax, -1784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r15, %rdi
	movq	%rax, -1776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$63, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1576(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1648(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19IsUndefinedConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	popq	%r10
	popq	%r11
	testb	%al, %al
	je	.L67
.L69:
	movq	-1648(%rbp), %xmm0
	movq	%r15, %rdi
	movhps	-1776(%rbp), %xmm0
	movaps	%xmm0, -1808(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	xorl	%edx, %edx
	movq	%rax, %rsi
.L220:
	movq	%r14, %rdi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$3, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1520(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -1776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$6, %edi
	movq	%r12, %rdx
	movq	%rbx, %r9
	movq	-1784(%rbp), %rsi
	pushq	%rdi
	movq	%rax, %r8
	movl	$1, %ecx
	movdqa	-1808(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%r15, %rdi
	movq	-1696(%rbp), %xmm1
	pushq	%rsi
	xorl	%esi, %esi
	movaps	%xmm0, -128(%rbp)
	movhps	-1776(%rbp), %xmm1
	movq	-1680(%rbp), %xmm0
	movq	%rax, -1536(%rbp)
	movq	-1504(%rbp), %rax
	movhps	-1664(%rbp), %xmm0
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -1528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%rbx, %xmm0
	popq	%r8
	movq	%r15, %rdi
	movq	-1696(%rbp), %xmm1
	movhps	-1712(%rbp), %xmm0
	popq	%r9
	movq	-1720(%rbp), %xmm4
	movaps	%xmm0, -1776(%rbp)
	movq	-1744(%rbp), %xmm3
	movq	-1752(%rbp), %xmm2
	movhps	-1648(%rbp), %xmm1
	movhps	-1664(%rbp), %xmm4
	movhps	-1728(%rbp), %xmm3
	movaps	%xmm1, -1712(%rbp)
	movhps	-1680(%rbp), %xmm2
	movaps	%xmm4, -1696(%rbp)
	movaps	%xmm3, -1664(%rbp)
	movaps	%xmm2, -1648(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$58, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movdqa	-1776(%rbp), %xmm0
	movdqa	-1712(%rbp), %xmm1
	movl	$88, %edi
	movdqa	-1696(%rbp), %xmm4
	movdqa	-1664(%rbp), %xmm3
	movq	$0, -1504(%rbp)
	movdqa	-1648(%rbp), %xmm2
	movq	-1760(%rbp), %rax
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm2, -80(%rbp)
	movaps	%xmm0, -1520(%rbp)
	movq	%rax, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	leaq	-720(%rbp), %rdi
	movdqa	-144(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm2
	leaq	88(%rax), %rdx
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm6
	movq	%rcx, 80(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L70
	call	_ZdlPv@PLT
.L70:
	movq	-1600(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -656(%rbp)
	je	.L71
.L225:
	movq	-1600(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-720(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$11, %edi
	movq	$0, -1504(%rbp)
	movaps	%xmm0, -1520(%rbp)
	call	_Znwm@PLT
	movl	$2056, %edi
	movq	%r14, %rsi
	movabsq	$578720283192919815, %rcx
	movw	%di, 8(%rax)
	leaq	11(%rax), %rdx
	movq	%r12, %rdi
	movq	%rcx, (%rax)
	movb	$7, 10(%rax)
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1520(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L72
	call	_ZdlPv@PLT
.L72:
	movq	(%rbx), %rax
	movl	$49, %edx
	movq	%r13, %rdi
	movq	40(%rax), %rbx
	movq	24(%rax), %rsi
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	%rbx, -1712(%rbp)
	movq	56(%rax), %rbx
	movq	%rsi, -1680(%rbp)
	movq	32(%rax), %rsi
	movq	48(%rax), %r12
	movq	%rcx, -1648(%rbp)
	movq	%rbx, -1720(%rbp)
	movq	16(%rax), %rcx
	movq	64(%rax), %rbx
	movq	72(%rax), %rax
	movq	%rsi, -1696(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -1664(%rbp)
	movq	%rax, -1744(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %xmm0
	movl	$80, %edi
	movq	$0, -1504(%rbp)
	movhps	-1648(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-1664(%rbp), %xmm0
	movhps	-1680(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-1696(%rbp), %xmm0
	movhps	-1712(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%r12, %xmm0
	movhps	-1720(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	movhps	-1744(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1520(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm3
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm2
	leaq	80(%rax), %rdx
	leaq	-528(%rbp), %rdi
	movups	%xmm7, (%rax)
	movdqa	-80(%rbp), %xmm7
	movups	%xmm3, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm7, 64(%rax)
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L73
	call	_ZdlPv@PLT
.L73:
	movq	-1624(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -464(%rbp)
	je	.L74
.L226:
	movq	-1624(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-528(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -1504(%rbp)
	movaps	%xmm0, -1520(%rbp)
	call	_Znwm@PLT
	movl	$2056, %esi
	movq	%r12, %rdi
	movabsq	$578720283192919815, %rcx
	movw	%si, 8(%rax)
	leaq	10(%rax), %rdx
	movq	%r14, %rsi
	movq	%rcx, (%rax)
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1520(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L75
	call	_ZdlPv@PLT
.L75:
	movq	(%rbx), %rax
	movq	24(%rax), %rbx
	movq	8(%rax), %rsi
	movq	56(%rax), %rdi
	movq	(%rax), %rcx
	movq	32(%rax), %rdx
	movq	72(%rax), %r12
	movq	%rbx, -1696(%rbp)
	movq	40(%rax), %rbx
	movq	%rsi, -1664(%rbp)
	movq	16(%rax), %rsi
	movq	%rdi, -1728(%rbp)
	movq	%rbx, -1720(%rbp)
	movq	-1576(%rbp), %rdi
	movq	48(%rax), %rbx
	movq	%rsi, -1680(%rbp)
	movl	$1, %esi
	movq	%rcx, -1648(%rbp)
	movq	%rdx, -1712(%rbp)
	movq	%rbx, -1744(%rbp)
	movq	64(%rax), %rbx
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	-1576(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberAddENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r14, %rdi
	movq	%rax, -1752(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$80, %edi
	movq	-1648(%rbp), %xmm0
	movq	$0, -1504(%rbp)
	movhps	-1664(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-1680(%rbp), %xmm0
	movhps	-1696(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-1712(%rbp), %xmm0
	movhps	-1720(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-1744(%rbp), %xmm0
	movhps	-1728(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	movhps	-1752(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1520(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm6
	leaq	-1296(%rbp), %rdi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm4
	leaq	80(%rax), %rdx
	movq	%r14, %rsi
	movdqa	-80(%rbp), %xmm2
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm2, 64(%rax)
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L76
	call	_ZdlPv@PLT
.L76:
	movq	-1592(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -272(%rbp)
	je	.L77
.L227:
	movq	-1632(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-336(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -1504(%rbp)
	movaps	%xmm0, -1520(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movabsq	$578720283192919815, %rcx
	movq	%rcx, (%rax)
	movl	$2056, %ecx
	leaq	10(%rax), %rdx
	movw	%cx, 8(%rax)
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L78
	call	_ZdlPv@PLT
.L78:
	movl	$68, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1576(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L67:
	movq	-1648(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IsNullConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L69
	movq	-1648(%rbp), %xmm0
	movq	%r15, %rdi
	movhps	-1776(%rbp), %xmm0
	movaps	%xmm0, -1808(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	movq	%rax, %rsi
	jmp	.L220
.L228:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22511:
	.size	_ZN2v88internal37ArrayForEachLoopContinuationAssembler40GenerateArrayForEachLoopContinuationImplEv, .-_ZN2v88internal37ArrayForEachLoopContinuationAssembler40GenerateArrayForEachLoopContinuationImplEv
	.section	.rodata._ZN2v88internal8Builtins37Generate_ArrayForEachLoopContinuationEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/array-foreach-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins37Generate_ArrayForEachLoopContinuationEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"ArrayForEachLoopContinuation"
	.section	.text._ZN2v88internal8Builtins37Generate_ArrayForEachLoopContinuationEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins37Generate_ArrayForEachLoopContinuationEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins37Generate_ArrayForEachLoopContinuationEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins37Generate_ArrayForEachLoopContinuationEPNS0_8compiler18CodeAssemblerStateE:
.LFB22507:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$783, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$747, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L233
.L230:
	movq	%r13, %rdi
	call	_ZN2v88internal37ArrayForEachLoopContinuationAssembler40GenerateArrayForEachLoopContinuationImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L234
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L230
.L234:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22507:
	.size	_ZN2v88internal8Builtins37Generate_ArrayForEachLoopContinuationEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins37Generate_ArrayForEachLoopContinuationEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_SA_SA_SA_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_SA_SA_SA_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_SA_SA_SA_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_SA_SA_SA_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_SA_SA_SA_:
.LFB27079:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$7, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$2056, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$134744071, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$8, 6(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L236
	movq	%rax, -112(%rbp)
	call	_ZdlPv@PLT
	movq	-112(%rbp), %rax
.L236:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L237
	movq	%rdx, (%r15)
.L237:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L238
	movq	%rdx, (%r14)
.L238:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L239
	movq	%rdx, 0(%r13)
.L239:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L240
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L240:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L241
	movq	%rdx, (%rbx)
.L241:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L242
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L242:
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L235
	movq	-104(%rbp), %rbx
	movq	%rax, (%rbx)
.L235:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L270
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L270:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27079:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_SA_SA_SA_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_SA_SA_SA_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_NS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_SB_PNS7_IS5_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_NS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_SB_PNS7_IS5_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_NS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_SB_PNS7_IS5_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_NS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_SB_PNS7_IS5_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_NS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_SB_PNS7_IS5_EE:
.LFB27080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$8, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$506663788666685447, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L272
	movq	%rax, -120(%rbp)
	call	_ZdlPv@PLT
	movq	-120(%rbp), %rax
.L272:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L273
	movq	%rdx, (%r15)
.L273:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L274
	movq	%rdx, (%r14)
.L274:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L275
	movq	%rdx, 0(%r13)
.L275:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L276
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L276:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L277
	movq	%rdx, (%rbx)
.L277:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L278
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L278:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L279
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L279:
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L271
	movq	-112(%rbp), %rbx
	movq	%rax, (%rbx)
.L271:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L310
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L310:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27080:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_NS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_SB_PNS7_IS5_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_NS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_SB_PNS7_IS5_EE
	.section	.text._ZN2v88internal47ArrayForEachLoopEagerDeoptContinuationAssembler50GenerateArrayForEachLoopEagerDeoptContinuationImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal47ArrayForEachLoopEagerDeoptContinuationAssembler50GenerateArrayForEachLoopEagerDeoptContinuationImplEv
	.type	_ZN2v88internal47ArrayForEachLoopEagerDeoptContinuationAssembler50GenerateArrayForEachLoopEagerDeoptContinuationImplEv, @function
_ZN2v88internal47ArrayForEachLoopEagerDeoptContinuationAssembler50GenerateArrayForEachLoopEagerDeoptContinuationImplEv:
.LFB22418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$7, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-3352(%rbp), %r14
	leaq	-3408(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-3536(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-2584(%rbp), %rbx
	subq	$3912, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, -3656(%rbp)
	movq	%rax, -3640(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -3664(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -3672(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, -3696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, -3712(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	leaq	-3640(%rbp), %r12
	movq	%rax, -3680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$144, %edi
	movq	$0, -3400(%rbp)
	movq	%rax, -3728(%rbp)
	movq	-3640(%rbp), %rax
	movq	$0, -3392(%rbp)
	movq	%rax, -3408(%rbp)
	movq	$0, -3384(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rdx, -3384(%rbp)
	movq	%rdx, -3392(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3368(%rbp)
	movq	%rax, -3400(%rbp)
	movq	$0, -3376(%rbp)
	movq	%r15, -3848(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3640(%rbp), %rax
	movl	$168, %edi
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	%rax, -3216(%rbp)
	movq	$0, -3192(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -3208(%rbp)
	leaq	-3160(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3192(%rbp)
	movq	%rdx, -3200(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3176(%rbp)
	movq	%rax, -3736(%rbp)
	movq	$0, -3184(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3640(%rbp), %rax
	movl	$192, %edi
	movq	$0, -3016(%rbp)
	movq	$0, -3008(%rbp)
	movq	%rax, -3024(%rbp)
	movq	$0, -3000(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -3016(%rbp)
	leaq	-2968(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3000(%rbp)
	movq	%rdx, -3008(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2984(%rbp)
	movq	%rax, -3760(%rbp)
	movq	$0, -2992(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3640(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2824(%rbp)
	movq	$0, -2816(%rbp)
	movq	%rax, -2832(%rbp)
	movq	$0, -2808(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2824(%rbp)
	leaq	-2776(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2808(%rbp)
	movq	%rdx, -2816(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2792(%rbp)
	movq	%rax, -3744(%rbp)
	movq	$0, -2800(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3640(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2632(%rbp)
	movq	$0, -2624(%rbp)
	movq	%rax, -2640(%rbp)
	movq	$0, -2616(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -2616(%rbp)
	movq	%rdx, -2624(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2600(%rbp)
	movq	%rax, -2632(%rbp)
	movq	$0, -2608(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3640(%rbp), %rax
	movl	$192, %edi
	movq	$0, -2440(%rbp)
	movq	$0, -2432(%rbp)
	movq	%rax, -2448(%rbp)
	movq	$0, -2424(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -2440(%rbp)
	leaq	-2392(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2424(%rbp)
	movq	%rdx, -2432(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2408(%rbp)
	movq	%rax, -3904(%rbp)
	movq	$0, -2416(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3640(%rbp), %rax
	movl	$216, %edi
	movq	$0, -2248(%rbp)
	movq	$0, -2240(%rbp)
	movq	%rax, -2256(%rbp)
	movq	$0, -2232(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -2248(%rbp)
	leaq	-2200(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2232(%rbp)
	movq	%rdx, -2240(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2216(%rbp)
	movq	%rax, -3784(%rbp)
	movq	$0, -2224(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3640(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2056(%rbp)
	movq	$0, -2048(%rbp)
	movq	%rax, -2064(%rbp)
	movq	$0, -2040(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -2056(%rbp)
	leaq	-2008(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2040(%rbp)
	movq	%rdx, -2048(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2024(%rbp)
	movq	%rax, -3824(%rbp)
	movq	$0, -2032(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3640(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1864(%rbp)
	movq	$0, -1856(%rbp)
	movq	%rax, -1872(%rbp)
	movq	$0, -1848(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1864(%rbp)
	leaq	-1816(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1848(%rbp)
	movq	%rdx, -1856(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1832(%rbp)
	movq	%rax, -3776(%rbp)
	movq	$0, -1840(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3640(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1672(%rbp)
	movq	$0, -1664(%rbp)
	movq	%rax, -1680(%rbp)
	movq	$0, -1656(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1672(%rbp)
	leaq	-1624(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1656(%rbp)
	movq	%rdx, -1664(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1640(%rbp)
	movq	%rax, -3920(%rbp)
	movq	$0, -1648(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3640(%rbp), %rax
	movl	$240, %edi
	movq	$0, -1480(%rbp)
	movq	$0, -1472(%rbp)
	movq	%rax, -1488(%rbp)
	movq	$0, -1464(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -1480(%rbp)
	leaq	-1432(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1464(%rbp)
	movq	%rdx, -1472(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1448(%rbp)
	movq	%rax, -3888(%rbp)
	movq	$0, -1456(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3640(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1288(%rbp)
	movq	$0, -1280(%rbp)
	movq	%rax, -1296(%rbp)
	movq	$0, -1272(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1288(%rbp)
	leaq	-1240(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1272(%rbp)
	movq	%rdx, -1280(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1256(%rbp)
	movq	%rax, -3792(%rbp)
	movq	$0, -1264(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3640(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1096(%rbp)
	movq	$0, -1088(%rbp)
	movq	%rax, -1104(%rbp)
	movq	$0, -1080(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1096(%rbp)
	leaq	-1048(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1080(%rbp)
	movq	%rdx, -1088(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1064(%rbp)
	movq	%rax, -3840(%rbp)
	movq	$0, -1072(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3640(%rbp), %rax
	movl	$240, %edi
	movq	$0, -904(%rbp)
	movq	$0, -896(%rbp)
	movq	%rax, -912(%rbp)
	movq	$0, -888(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -904(%rbp)
	leaq	-856(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -888(%rbp)
	movq	%rdx, -896(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -872(%rbp)
	movq	%rax, -3928(%rbp)
	movq	$0, -880(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3640(%rbp), %rax
	movl	$264, %edi
	movq	$0, -712(%rbp)
	movq	$0, -704(%rbp)
	movq	%rax, -720(%rbp)
	movq	$0, -696(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -712(%rbp)
	leaq	-664(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -696(%rbp)
	movq	%rdx, -704(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -680(%rbp)
	movq	%rax, -3872(%rbp)
	movq	$0, -688(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3640(%rbp), %rax
	movl	$216, %edi
	movq	$0, -520(%rbp)
	movq	$0, -512(%rbp)
	movq	%rax, -528(%rbp)
	movq	$0, -504(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -520(%rbp)
	leaq	-472(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -504(%rbp)
	movq	%rdx, -512(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -488(%rbp)
	movq	%rax, -3856(%rbp)
	movq	$0, -496(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3640(%rbp), %rax
	movl	$240, %edi
	movq	$0, -328(%rbp)
	movq	$0, -320(%rbp)
	movq	%rax, -336(%rbp)
	movq	$0, -312(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -328(%rbp)
	leaq	-280(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -312(%rbp)
	movq	%rdx, -320(%rbp)
	xorl	%edx, %edx
	movq	%rax, -3864(%rbp)
	movups	%xmm0, -296(%rbp)
	movq	$0, -304(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movq	-3664(%rbp), %xmm1
	movaps	%xmm0, -3536(%rbp)
	movhps	-3672(%rbp), %xmm1
	movq	$0, -3520(%rbp)
	movaps	%xmm1, -144(%rbp)
	movq	-3696(%rbp), %xmm1
	movhps	-3712(%rbp), %xmm1
	movaps	%xmm1, -128(%rbp)
	movq	-3680(%rbp), %xmm1
	movhps	-3728(%rbp), %xmm1
	movaps	%xmm1, -112(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm5
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-144(%rbp), %xmm4
	leaq	48(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movups	%xmm5, 16(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L312
	call	_ZdlPv@PLT
.L312:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3024(%rbp), %rax
	cmpq	$0, -3344(%rbp)
	movq	%rax, -3680(%rbp)
	leaq	-3216(%rbp), %rax
	movq	%rax, -3664(%rbp)
	jne	.L479
.L313:
	leaq	-2832(%rbp), %rax
	cmpq	$0, -3152(%rbp)
	movq	%rax, -3728(%rbp)
	jne	.L480
.L318:
	leaq	-2640(%rbp), %rax
	cmpq	$0, -2960(%rbp)
	movq	%rax, -3736(%rbp)
	jne	.L481
.L320:
	cmpq	$0, -2768(%rbp)
	jne	.L482
.L322:
	leaq	-2256(%rbp), %rax
	cmpq	$0, -2576(%rbp)
	movq	%rax, -3744(%rbp)
	leaq	-2448(%rbp), %rax
	movq	%rax, -3672(%rbp)
	jne	.L483
.L324:
	leaq	-2064(%rbp), %rax
	cmpq	$0, -2384(%rbp)
	movq	%rax, -3808(%rbp)
	jne	.L484
.L329:
	leaq	-1872(%rbp), %rax
	cmpq	$0, -2192(%rbp)
	movq	%rax, -3760(%rbp)
	jne	.L485
.L332:
	cmpq	$0, -2000(%rbp)
	jne	.L486
.L335:
	leaq	-1488(%rbp), %rax
	cmpq	$0, -1808(%rbp)
	movq	%rax, -3784(%rbp)
	leaq	-1680(%rbp), %rax
	movq	%rax, -3696(%rbp)
	jne	.L487
.L337:
	leaq	-1296(%rbp), %rax
	cmpq	$0, -1616(%rbp)
	movq	%rax, -3824(%rbp)
	jne	.L488
.L342:
	leaq	-1104(%rbp), %rax
	cmpq	$0, -1424(%rbp)
	movq	%rax, -3776(%rbp)
	jne	.L489
.L345:
	cmpq	$0, -1232(%rbp)
	jne	.L490
.L348:
	leaq	-720(%rbp), %rax
	cmpq	$0, -1040(%rbp)
	movq	%rax, -3792(%rbp)
	leaq	-912(%rbp), %rax
	movq	%rax, -3712(%rbp)
	jne	.L491
.L350:
	leaq	-528(%rbp), %rax
	cmpq	$0, -848(%rbp)
	movq	%rax, -3840(%rbp)
	jne	.L492
.L355:
	cmpq	$0, -656(%rbp)
	leaq	-336(%rbp), %r15
	jne	.L493
.L358:
	cmpq	$0, -464(%rbp)
	jne	.L494
	cmpq	$0, -272(%rbp)
	jne	.L495
.L363:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3840(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3792(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3712(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3776(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3824(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3784(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3696(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3760(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3808(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3744(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3672(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3736(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3728(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3680(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3664(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3848(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L496
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L479:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movl	$2056, %ecx
	movq	-3848(%rbp), %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L314
	call	_ZdlPv@PLT
.L314:
	movq	(%r14), %rax
	movl	$13, %edx
	movq	%r12, %rdi
	movq	24(%rax), %rsi
	movq	(%rax), %r14
	movq	8(%rax), %rcx
	movq	16(%rax), %r15
	movq	%rsi, -3664(%rbp)
	movq	32(%rax), %rsi
	movq	40(%rax), %rax
	movq	%rcx, -3672(%rbp)
	movq	%rsi, -3696(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rax, -3712(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3672(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r13, %rcx
	movq	-3656(%rbp), %rdi
	call	_ZN2v88internal21Cast10JSReceiver_1404EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3672(%rbp), %rcx
	movq	%r15, %xmm3
	movq	-3696(%rbp), %xmm7
	movq	%r14, %xmm4
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movq	%rcx, %xmm2
	leaq	-3568(%rbp), %r15
	leaq	-144(%rbp), %r14
	movq	%rcx, -96(%rbp)
	movhps	-3712(%rbp), %xmm7
	movhps	-3664(%rbp), %xmm3
	movq	%r14, %rsi
	movq	%r15, %rdi
	punpcklqdq	%xmm2, %xmm4
	movaps	%xmm7, -3728(%rbp)
	movaps	%xmm3, -3696(%rbp)
	movaps	%xmm4, -3712(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm0, -3568(%rbp)
	movq	$0, -3552(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3024(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -3680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L315
	call	_ZdlPv@PLT
.L315:
	movq	-3760(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3216(%rbp), %rax
	cmpq	$0, -3528(%rbp)
	movq	%rax, -3664(%rbp)
	jne	.L497
.L316:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L482:
	movq	-3744(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r11d
	movq	-3728(%rbp), %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	6(%rax), %rdx
	movw	%r11w, 4(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L323
	call	_ZdlPv@PLT
.L323:
	movq	-3656(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L481:
	movq	-3760(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3632(%rbp)
	movq	$0, -3624(%rbp)
	movq	$0, -3616(%rbp)
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	movq	$0, -3592(%rbp)
	movq	$0, -3584(%rbp)
	movq	$0, -3568(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3568(%rbp), %rax
	movq	-3680(%rbp), %rdi
	pushq	%rax
	leaq	-3584(%rbp), %rax
	leaq	-3616(%rbp), %rcx
	pushq	%rax
	leaq	-3592(%rbp), %rax
	leaq	-3600(%rbp), %r9
	pushq	%rax
	leaq	-3608(%rbp), %r8
	leaq	-3624(%rbp), %rdx
	leaq	-3632(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_NS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_SB_PNS7_IS5_EE
	pxor	%xmm0, %xmm0
	addq	$32, %rsp
	movq	%r13, %rdi
	movq	-3632(%rbp), %rax
	leaq	-144(%rbp), %rsi
	leaq	-88(%rbp), %rdx
	movaps	%xmm0, -3536(%rbp)
	movq	$0, -3520(%rbp)
	movq	%rax, -144(%rbp)
	movq	-3624(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-3616(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-3608(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-3600(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-3592(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-3568(%rbp), %rax
	movq	%rax, -96(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3736(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L321
	call	_ZdlPv@PLT
.L321:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L480:
	movq	-3736(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3624(%rbp)
	movq	$0, -3616(%rbp)
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	movq	$0, -3592(%rbp)
	movq	$0, -3584(%rbp)
	movq	$0, -3568(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3568(%rbp), %rax
	movq	-3664(%rbp), %rdi
	leaq	-3608(%rbp), %rcx
	pushq	%rax
	leaq	-3584(%rbp), %rax
	leaq	-3592(%rbp), %r9
	pushq	%rax
	leaq	-3600(%rbp), %r8
	leaq	-3616(%rbp), %rdx
	leaq	-3624(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_SA_SA_SA_
	leaq	-144(%rbp), %rsi
	leaq	-96(%rbp), %rdx
	movq	%r13, %rdi
	movq	-3592(%rbp), %xmm0
	movq	-3608(%rbp), %xmm1
	movq	$0, -3520(%rbp)
	movq	-3624(%rbp), %xmm2
	movhps	-3584(%rbp), %xmm0
	movhps	-3600(%rbp), %xmm1
	movhps	-3616(%rbp), %xmm2
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3728(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	popq	%r14
	popq	%r15
	testq	%rdi, %rdi
	je	.L319
	call	_ZdlPv@PLT
.L319:
	movq	-3744(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L483:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r10d
	movq	-3736(%rbp), %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	7(%rax), %rdx
	movw	%r10w, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L325
	call	_ZdlPv@PLT
.L325:
	movq	(%rbx), %rax
	movl	$14, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rbx
	movq	40(%rax), %rcx
	movq	16(%rax), %r15
	movq	(%rax), %r14
	movq	%rbx, -3672(%rbp)
	movq	24(%rax), %rbx
	movq	%rcx, -3760(%rbp)
	movq	%rbx, -3696(%rbp)
	movq	32(%rax), %rbx
	movq	%rbx, -3712(%rbp)
	movq	48(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	-3656(%rbp), %rdi
	call	_ZN2v88internal82Cast71UT19ATCallableApiObject17ATCallableJSProxy15JSBoundFunction10JSFunction_1405EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %xmm6
	movq	%rbx, %xmm5
	movq	%r15, %xmm2
	punpcklqdq	%xmm6, %xmm5
	pxor	%xmm0, %xmm0
	leaq	-72(%rbp), %rdx
	movq	%rax, -80(%rbp)
	movq	%r14, %xmm7
	leaq	-3568(%rbp), %r15
	movq	-3712(%rbp), %xmm6
	leaq	-144(%rbp), %r14
	movhps	-3696(%rbp), %xmm2
	movhps	-3672(%rbp), %xmm7
	movq	%r14, %rsi
	movq	%r15, %rdi
	movhps	-3760(%rbp), %xmm6
	movaps	%xmm5, -3808(%rbp)
	movaps	%xmm6, -3712(%rbp)
	movaps	%xmm2, -3760(%rbp)
	movaps	%xmm7, -3696(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm0, -3568(%rbp)
	movq	$0, -3552(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2256(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -3744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L326
	call	_ZdlPv@PLT
.L326:
	movq	-3784(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2448(%rbp), %rax
	cmpq	$0, -3528(%rbp)
	movq	%rax, -3672(%rbp)
	jne	.L498
.L327:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L486:
	movq	-3824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r9d
	movq	-3808(%rbp), %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	7(%rax), %rdx
	movw	%r9w, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L336
	call	_ZdlPv@PLT
.L336:
	movq	-3656(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L485:
	movq	-3784(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	-135(%rbp), %rdx
	movaps	%xmm0, -3536(%rbp)
	movabsq	$578439907727902727, %rax
	movq	%rax, -144(%rbp)
	movb	$7, -136(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3744(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L333
	call	_ZdlPv@PLT
.L333:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	24(%rax), %rdi
	movq	32(%rax), %rsi
	movq	48(%rax), %rdx
	movq	(%rax), %r10
	movq	8(%rax), %r9
	movq	16(%rax), %r8
	movq	40(%rax), %rcx
	movq	64(%rax), %rax
	movq	%rdi, -120(%rbp)
	movq	%r13, %rdi
	movq	%rsi, -112(%rbp)
	movq	%r14, %rsi
	movq	%rdx, -96(%rbp)
	leaq	-80(%rbp), %rdx
	movq	%r10, -144(%rbp)
	movq	%r9, -136(%rbp)
	movq	%r8, -128(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%rax, -88(%rbp)
	movaps	%xmm0, -3536(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1872(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -3760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L334
	call	_ZdlPv@PLT
.L334:
	movq	-3776(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L484:
	movq	-3904(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	-136(%rbp), %rdx
	movabsq	$578439907727902727, %rax
	movaps	%xmm0, -3536(%rbp)
	movq	%rax, -144(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3672(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L330
	call	_ZdlPv@PLT
.L330:
	movq	(%rbx), %rax
	leaq	-88(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movdqu	32(%rax), %xmm0
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm1
	movq	48(%rax), %rax
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2064(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -3808(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L331
	call	_ZdlPv@PLT
.L331:
	movq	-3824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L487:
	movq	-3776(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$506382313689974791, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	-3760(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L338
	call	_ZdlPv@PLT
.L338:
	movq	(%rbx), %rax
	movl	$15, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rbx
	movq	40(%rax), %rcx
	movq	(%rax), %r15
	movq	16(%rax), %r14
	movq	%rbx, -3696(%rbp)
	movq	24(%rax), %rbx
	movq	%rcx, -3776(%rbp)
	movq	48(%rax), %rcx
	movq	%rbx, -3712(%rbp)
	movq	32(%rax), %rbx
	movq	56(%rax), %rax
	movq	%rcx, -3784(%rbp)
	movq	%rax, -3824(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rdi
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal29Cast20UT5ATSmi10HeapNumber_85EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm5
	movq	%r15, %xmm6
	movq	-3784(%rbp), %xmm3
	leaq	-3568(%rbp), %r15
	movq	%rbx, %xmm4
	leaq	-144(%rbp), %r14
	movq	%rax, -72(%rbp)
	movhps	-3824(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	leaq	-64(%rbp), %rdx
	movq	%r14, %rsi
	movhps	-3776(%rbp), %xmm4
	movhps	-3712(%rbp), %xmm5
	movq	%r15, %rdi
	movaps	%xmm3, -3824(%rbp)
	movhps	-3696(%rbp), %xmm6
	movaps	%xmm4, -3776(%rbp)
	movaps	%xmm5, -3712(%rbp)
	movaps	%xmm6, -3904(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -3568(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -3552(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1488(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -3784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L339
	call	_ZdlPv@PLT
.L339:
	movq	-3888(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1680(%rbp), %rax
	cmpq	$0, -3528(%rbp)
	movq	%rax, -3696(%rbp)
	jne	.L499
.L340:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L490:
	movq	-3792(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$506382313689974791, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	-3824(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L349
	call	_ZdlPv@PLT
.L349:
	movq	-3656(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L489:
	movq	-3888(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$506382313689974791, %rax
	movl	$2056, %r8d
	leaq	-134(%rbp), %rdx
	movaps	%xmm0, -3536(%rbp)
	movq	%rax, -144(%rbp)
	movw	%r8w, -136(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3784(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L346
	call	_ZdlPv@PLT
.L346:
	movq	(%rbx), %rax
	leaq	-72(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movdqu	48(%rax), %xmm0
	movdqu	(%rax), %xmm3
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	72(%rax), %rax
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -144(%rbp)
	movq	%rax, -80(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3536(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1104(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -3776(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L347
	call	_ZdlPv@PLT
.L347:
	movq	-3840(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L488:
	movq	-3920(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	-135(%rbp), %rdx
	movaps	%xmm0, -3536(%rbp)
	movabsq	$506382313689974791, %rax
	movq	%rax, -144(%rbp)
	movb	$8, -136(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3696(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L343
	call	_ZdlPv@PLT
.L343:
	movq	(%rbx), %rax
	leaq	-80(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movdqu	32(%rax), %xmm0
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm1
	movdqu	48(%rax), %xmm6
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1296(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -3824(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L344
	call	_ZdlPv@PLT
.L344:
	movq	-3792(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L491:
	movq	-3840(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$506382313689974791, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	-3776(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L351
	call	_ZdlPv@PLT
.L351:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rsi
	movq	8(%rax), %rcx
	movq	56(%rax), %rdx
	movq	(%rax), %rbx
	movq	40(%rax), %r15
	movq	48(%rax), %r14
	movq	%rsi, -3888(%rbp)
	movq	32(%rax), %rsi
	movq	%rcx, -3792(%rbp)
	movq	16(%rax), %rcx
	movq	%rdx, -3920(%rbp)
	movl	$16, %edx
	movq	%rsi, -3904(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rbx, -3712(%rbp)
	movq	64(%rax), %rbx
	movq	%rcx, -3840(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	call	_ZN2v88internal29Cast20UT5ATSmi10HeapNumber_85EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %xmm7
	movq	%rbx, %xmm2
	movq	-3904(%rbp), %xmm3
	punpcklqdq	%xmm7, %xmm2
	pxor	%xmm0, %xmm0
	leaq	-56(%rbp), %rdx
	movq	%rax, -64(%rbp)
	movq	%r15, %xmm6
	movq	%r14, %xmm7
	leaq	-3568(%rbp), %r15
	movq	-3840(%rbp), %xmm4
	movq	-3712(%rbp), %xmm5
	punpcklqdq	%xmm6, %xmm3
	movq	%r15, %rdi
	movaps	%xmm2, -80(%rbp)
	leaq	-144(%rbp), %r14
	movhps	-3888(%rbp), %xmm4
	movhps	-3920(%rbp), %xmm7
	movaps	%xmm2, -3952(%rbp)
	movhps	-3792(%rbp), %xmm5
	movq	%r14, %rsi
	movaps	%xmm7, -3920(%rbp)
	movaps	%xmm3, -3904(%rbp)
	movaps	%xmm4, -3840(%rbp)
	movaps	%xmm5, -3888(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm0, -3568(%rbp)
	movq	$0, -3552(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-720(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -3792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L352
	call	_ZdlPv@PLT
.L352:
	movq	-3872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-912(%rbp), %rax
	cmpq	$0, -3528(%rbp)
	movq	%rax, -3712(%rbp)
	jne	.L500
.L353:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L494:
	movq	-3856(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$506382313689974791, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	-3840(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L362
	call	_ZdlPv@PLT
.L362:
	movq	-3656(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -272(%rbp)
	je	.L363
.L495:
	movq	-3864(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$2056, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$506382313689974791, %rax
	pxor	%xmm0, %xmm0
	leaq	-134(%rbp), %rdx
	movw	%cx, -136(%rbp)
	movq	%rax, -144(%rbp)
	movaps	%xmm0, -3536(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L364
	call	_ZdlPv@PLT
.L364:
	movq	(%rbx), %rax
	movl	$19, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	48(%rax), %rcx
	movq	(%rax), %r9
	movq	24(%rax), %rbx
	movq	%rcx, -3864(%rbp)
	movq	56(%rax), %rcx
	movq	%r9, -3952(%rbp)
	movq	%rcx, -3888(%rbp)
	movq	64(%rax), %rcx
	movq	%rbx, -3856(%rbp)
	movq	72(%rax), %rbx
	movq	%rcx, -3872(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3656(%rbp), %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$20, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -3920(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3656(%rbp), %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$18, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -3904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-3584(%rbp), %r10
	movq	-3656(%rbp), %rsi
	movq	%r10, %rdi
	movq	%r10, -3928(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3928(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$747, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-3928(%rbp), %r10
	movq	-3536(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-3928(%rbp), %r10
	movl	$1, %ecx
	xorl	%esi, %esi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-3952(%rbp), %r9
	leaq	-3568(%rbp), %rdx
	movq	%rax, -3568(%rbp)
	movq	-3520(%rbp), %rax
	movq	%r10, %rdi
	movq	%rax, -3560(%rbp)
	movq	-3864(%rbp), %rax
	movq	%rax, %xmm0
	movhps	-3888(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-3856(%rbp), %xmm0
	movq	%r10, -3856(%rbp)
	movhps	-3920(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%rax, %xmm0
	movhps	-3872(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%rbx, %xmm0
	movl	$8, %ebx
	pushq	%rbx
	movhps	-3904(%rbp), %xmm0
	pushq	%r14
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-3856(%rbp), %r10
	movq	%rax, %r14
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3656(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L493:
	movq	-3872(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$2056, %esi
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movw	%si, -136(%rbp)
	leaq	-133(%rbp), %rdx
	movq	%r14, %rsi
	movabsq	$506382313689974791, %rax
	movq	%rax, -144(%rbp)
	movaps	%xmm0, -3536(%rbp)
	movb	$8, -134(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3792(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L359
	call	_ZdlPv@PLT
.L359:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	leaq	-336(%rbp), %r15
	movq	40(%rax), %rdi
	movq	48(%rax), %rsi
	movq	64(%rax), %rdx
	movq	8(%rax), %r11
	movq	16(%rax), %r10
	movq	24(%rax), %r9
	movq	32(%rax), %r8
	movq	56(%rax), %rcx
	movq	(%rax), %rbx
	movq	80(%rax), %rax
	movq	%rdi, -104(%rbp)
	movq	%r13, %rdi
	movq	%rsi, -96(%rbp)
	movq	%r14, %rsi
	movq	%rdx, -80(%rbp)
	leaq	-64(%rbp), %rdx
	movq	%r11, -136(%rbp)
	movq	%r10, -128(%rbp)
	movq	%r9, -120(%rbp)
	movq	%r8, -112(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%rax, -72(%rbp)
	movaps	%xmm0, -3536(%rbp)
	movq	%rbx, -144(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L360
	call	_ZdlPv@PLT
.L360:
	movq	-3864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L492:
	movq	-3928(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$2056, %edi
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movw	%di, -136(%rbp)
	leaq	-134(%rbp), %rdx
	movq	%r13, %rdi
	movabsq	$506382313689974791, %rax
	movq	%rax, -144(%rbp)
	movaps	%xmm0, -3536(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3712(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L356
	call	_ZdlPv@PLT
.L356:
	movq	(%rbx), %rax
	leaq	-72(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movdqu	48(%rax), %xmm0
	movdqu	(%rax), %xmm3
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	64(%rax), %rax
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -144(%rbp)
	movq	%rax, -80(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3536(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-528(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -3840(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L357
	call	_ZdlPv@PLT
.L357:
	movq	-3856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L497:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-88(%rbp), %rdx
	movq	%r14, %rsi
	movdqa	-3712(%rbp), %xmm7
	movdqa	-3696(%rbp), %xmm6
	movq	%r15, %rdi
	movaps	%xmm0, -3568(%rbp)
	movq	-3672(%rbp), %rax
	movq	$0, -3552(%rbp)
	movaps	%xmm7, -144(%rbp)
	movdqa	-3728(%rbp), %xmm7
	movq	%rax, -96(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3664(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L317
	call	_ZdlPv@PLT
.L317:
	movq	-3736(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L498:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rdx
	movq	%r14, %rsi
	movdqa	-3696(%rbp), %xmm6
	movdqa	-3760(%rbp), %xmm7
	movq	%r15, %rdi
	movaps	%xmm0, -3568(%rbp)
	movq	$0, -3552(%rbp)
	movaps	%xmm6, -144(%rbp)
	movdqa	-3712(%rbp), %xmm6
	movaps	%xmm7, -128(%rbp)
	movdqa	-3808(%rbp), %xmm7
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3672(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L328
	call	_ZdlPv@PLT
.L328:
	movq	-3904(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L499:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-72(%rbp), %rdx
	movq	%r14, %rsi
	movdqa	-3904(%rbp), %xmm3
	movdqa	-3712(%rbp), %xmm6
	movq	%r15, %rdi
	movaps	%xmm0, -3568(%rbp)
	movdqa	-3776(%rbp), %xmm7
	movq	%rbx, -80(%rbp)
	movaps	%xmm3, -144(%rbp)
	movdqa	-3824(%rbp), %xmm3
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movq	$0, -3552(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3696(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L341
	call	_ZdlPv@PLT
.L341:
	movq	-3920(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L500:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-3888(%rbp), %xmm6
	movq	%r14, %rsi
	movdqa	-3840(%rbp), %xmm2
	movdqa	-3904(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	leaq	-64(%rbp), %rdx
	movq	%r15, %rdi
	movdqa	-3920(%rbp), %xmm3
	movaps	%xmm6, -144(%rbp)
	movdqa	-3952(%rbp), %xmm6
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm6, -80(%rbp)
	movaps	%xmm0, -3568(%rbp)
	movq	$0, -3552(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3712(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L354
	call	_ZdlPv@PLT
.L354:
	movq	-3928(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L353
.L496:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22418:
	.size	_ZN2v88internal47ArrayForEachLoopEagerDeoptContinuationAssembler50GenerateArrayForEachLoopEagerDeoptContinuationImplEv, .-_ZN2v88internal47ArrayForEachLoopEagerDeoptContinuationAssembler50GenerateArrayForEachLoopEagerDeoptContinuationImplEv
	.section	.rodata._ZN2v88internal8Builtins47Generate_ArrayForEachLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"ArrayForEachLoopEagerDeoptContinuation"
	.section	.text._ZN2v88internal8Builtins47Generate_ArrayForEachLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins47Generate_ArrayForEachLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins47Generate_ArrayForEachLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins47Generate_ArrayForEachLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE:
.LFB22414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$146, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$745, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L505
.L502:
	movq	%r13, %rdi
	call	_ZN2v88internal47ArrayForEachLoopEagerDeoptContinuationAssembler50GenerateArrayForEachLoopEagerDeoptContinuationImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L506
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L505:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L502
.L506:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22414:
	.size	_ZN2v88internal8Builtins47Generate_ArrayForEachLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins47Generate_ArrayForEachLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal46ArrayForEachLoopLazyDeoptContinuationAssembler49GenerateArrayForEachLoopLazyDeoptContinuationImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal46ArrayForEachLoopLazyDeoptContinuationAssembler49GenerateArrayForEachLoopLazyDeoptContinuationImplEv
	.type	_ZN2v88internal46ArrayForEachLoopLazyDeoptContinuationAssembler49GenerateArrayForEachLoopLazyDeoptContinuationImplEv, @function
_ZN2v88internal46ArrayForEachLoopLazyDeoptContinuationAssembler49GenerateArrayForEachLoopLazyDeoptContinuationImplEv:
.LFB22469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-3664(%rbp), %r14
	leaq	-3552(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-3176(%rbp), %rbx
	subq	$3896, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, -3672(%rbp)
	movq	%rax, -3664(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -3688(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -3696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, -3704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, -3728(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%rax, -3736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$5, %esi
	movq	%r12, %rdi
	leaq	-3368(%rbp), %r12
	movq	%rax, -3744(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$168, %edi
	movq	$0, -3416(%rbp)
	movq	$0, -3408(%rbp)
	movq	%rax, %r13
	movq	-3664(%rbp), %rax
	movq	$0, -3400(%rbp)
	movq	%rax, -3424(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -3416(%rbp)
	leaq	-3424(%rbp), %rax
	movq	%rdx, -3400(%rbp)
	movq	%rdx, -3408(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3384(%rbp)
	movq	%rax, -3680(%rbp)
	movq	$0, -3392(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$192, %edi
	movq	$0, -3224(%rbp)
	movq	$0, -3216(%rbp)
	movq	%rax, -3232(%rbp)
	movq	$0, -3208(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -3208(%rbp)
	movq	%rdx, -3216(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3192(%rbp)
	movq	%rax, -3224(%rbp)
	movq	%rbx, -3768(%rbp)
	leaq	-2984(%rbp), %rbx
	movq	$0, -3200(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$216, %edi
	movq	$0, -3032(%rbp)
	movq	$0, -3024(%rbp)
	movq	%rax, -3040(%rbp)
	movq	$0, -3016(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -3016(%rbp)
	movq	%rdx, -3024(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3000(%rbp)
	movq	%rax, -3032(%rbp)
	movq	%rbx, -3752(%rbp)
	leaq	-2792(%rbp), %rbx
	movq	$0, -3008(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2840(%rbp)
	movq	$0, -2832(%rbp)
	movq	%rax, -2848(%rbp)
	movq	$0, -2824(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -2824(%rbp)
	movq	%rdx, -2832(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2808(%rbp)
	movq	%rax, -2840(%rbp)
	movq	$0, -2816(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-2656(%rbp), %rcx
	movl	$8, %edx
	movq	%r14, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -3712(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-3664(%rbp), %rax
	movl	$216, %edi
	movq	$0, -2456(%rbp)
	movq	$0, -2448(%rbp)
	movq	%rax, -2464(%rbp)
	movq	$0, -2440(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2408(%rbp), %rcx
	movq	%r14, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -2440(%rbp)
	movq	%rdx, -2448(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3776(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2424(%rbp)
	movq	%rax, -2456(%rbp)
	movq	$0, -2432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$240, %edi
	movq	$0, -2264(%rbp)
	movq	$0, -2256(%rbp)
	movq	%rax, -2272(%rbp)
	movq	$0, -2248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2216(%rbp), %rcx
	movq	%r14, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rdx, -2248(%rbp)
	movq	%rdx, -2256(%rbp)
	movq	%rcx, -3792(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movups	%xmm0, -2232(%rbp)
	movq	%rax, -2264(%rbp)
	movq	$0, -2240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$192, %edi
	movq	$0, -2072(%rbp)
	movq	$0, -2064(%rbp)
	movq	%rax, -2080(%rbp)
	movq	$0, -2056(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2024(%rbp), %rcx
	movq	%r14, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -2056(%rbp)
	movq	%rdx, -2064(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3808(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2040(%rbp)
	movq	%rax, -2072(%rbp)
	movq	$0, -2048(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1880(%rbp)
	movq	$0, -1872(%rbp)
	movq	%rax, -1888(%rbp)
	movq	$0, -1864(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1832(%rbp), %rcx
	movq	%r14, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -1864(%rbp)
	movq	%rdx, -1872(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3888(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1848(%rbp)
	movq	%rax, -1880(%rbp)
	movq	$0, -1856(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$240, %edi
	movq	$0, -1688(%rbp)
	movq	$0, -1680(%rbp)
	movq	%rax, -1696(%rbp)
	movq	$0, -1672(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1640(%rbp), %rcx
	movq	%r14, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rdx, -1672(%rbp)
	movq	%rdx, -1680(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3904(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1656(%rbp)
	movq	%rax, -1688(%rbp)
	movq	$0, -1664(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$264, %edi
	movq	$0, -1496(%rbp)
	movq	$0, -1488(%rbp)
	movq	%rax, -1504(%rbp)
	movq	$0, -1480(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1448(%rbp), %rcx
	movq	%r14, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rdx, -1480(%rbp)
	movq	%rdx, -1488(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3816(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1464(%rbp)
	movq	%rax, -1496(%rbp)
	movq	$0, -1472(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1304(%rbp)
	movq	$0, -1296(%rbp)
	movq	%rax, -1312(%rbp)
	movq	$0, -1288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1256(%rbp), %rcx
	movq	%r14, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -1288(%rbp)
	movq	%rdx, -1296(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3872(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1272(%rbp)
	movq	%rax, -1304(%rbp)
	movq	$0, -1280(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$240, %edi
	movq	$0, -1112(%rbp)
	movq	$0, -1104(%rbp)
	movq	%rax, -1120(%rbp)
	movq	$0, -1096(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1064(%rbp), %rcx
	movq	%r14, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rdx, -1096(%rbp)
	movq	%rdx, -1104(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3840(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1080(%rbp)
	movq	%rax, -1112(%rbp)
	movq	$0, -1088(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$264, %edi
	movq	$0, -920(%rbp)
	movq	$0, -912(%rbp)
	movq	%rax, -928(%rbp)
	movq	$0, -904(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-872(%rbp), %rcx
	movq	%r14, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rdx, -904(%rbp)
	movq	%rdx, -912(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3912(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -888(%rbp)
	movq	%rax, -920(%rbp)
	movq	$0, -896(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$288, %edi
	movq	$0, -728(%rbp)
	movq	$0, -720(%rbp)
	movq	%rax, -736(%rbp)
	movq	$0, -712(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-680(%rbp), %rcx
	movq	%r14, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rdx, -712(%rbp)
	movq	%rdx, -720(%rbp)
	movq	%rcx, -3848(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movups	%xmm0, -696(%rbp)
	movq	%rax, -728(%rbp)
	movq	$0, -704(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$240, %edi
	movq	$0, -536(%rbp)
	movq	$0, -528(%rbp)
	movq	%rax, -544(%rbp)
	movq	$0, -520(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-488(%rbp), %rcx
	movq	%r14, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rdx, -520(%rbp)
	movq	%rdx, -528(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3856(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -504(%rbp)
	movq	%rax, -536(%rbp)
	movq	$0, -512(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3664(%rbp), %rax
	movl	$264, %edi
	movq	$0, -344(%rbp)
	movq	$0, -336(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -328(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-296(%rbp), %rcx
	movq	%r14, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rdx, -328(%rbp)
	movq	%rdx, -336(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3824(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -344(%rbp)
	movups	%xmm0, -312(%rbp)
	movq	$0, -320(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movq	-3688(%rbp), %xmm1
	movaps	%xmm0, -3552(%rbp)
	movhps	-3696(%rbp), %xmm1
	movq	%r13, -112(%rbp)
	movaps	%xmm1, -160(%rbp)
	movq	-3704(%rbp), %xmm1
	movq	$0, -3536(%rbp)
	movhps	-3728(%rbp), %xmm1
	movaps	%xmm1, -144(%rbp)
	movq	-3736(%rbp), %xmm1
	movhps	-3744(%rbp), %xmm1
	movaps	%xmm1, -128(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movdqa	-128(%rbp), %xmm7
	movq	%r15, %rsi
	movdqa	-160(%rbp), %xmm5
	movdqa	-144(%rbp), %xmm6
	leaq	56(%rax), %rdx
	movq	%rax, -3552(%rbp)
	movq	%rcx, 48(%rax)
	movq	-3680(%rbp), %rdi
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -3536(%rbp)
	movq	%rdx, -3544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L508
	call	_ZdlPv@PLT
.L508:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3040(%rbp), %rax
	cmpq	$0, -3360(%rbp)
	movq	%rax, -3736(%rbp)
	leaq	-3232(%rbp), %rax
	movq	%rax, -3696(%rbp)
	jne	.L666
.L509:
	leaq	-2848(%rbp), %rax
	cmpq	$0, -3168(%rbp)
	movq	%rax, -3760(%rbp)
	jne	.L667
.L513:
	cmpq	$0, -2976(%rbp)
	jne	.L668
.L516:
	cmpq	$0, -2784(%rbp)
	jne	.L669
.L519:
	leaq	-2272(%rbp), %rax
	cmpq	$0, -2592(%rbp)
	movq	%rax, -3744(%rbp)
	leaq	-2464(%rbp), %rax
	movq	%rax, -3704(%rbp)
	jne	.L670
.L520:
	leaq	-2080(%rbp), %rax
	cmpq	$0, -2400(%rbp)
	movq	%rax, -3768(%rbp)
	jne	.L671
.L524:
	leaq	-1888(%rbp), %rax
	cmpq	$0, -2208(%rbp)
	movq	%rax, -3776(%rbp)
	jne	.L672
.L527:
	cmpq	$0, -2016(%rbp)
	jne	.L673
.L530:
	leaq	-1504(%rbp), %rax
	cmpq	$0, -1824(%rbp)
	movq	%rax, -3752(%rbp)
	leaq	-1696(%rbp), %rax
	movq	%rax, -3688(%rbp)
	jne	.L674
.L531:
	leaq	-1312(%rbp), %rax
	cmpq	$0, -1632(%rbp)
	movq	%rax, -3792(%rbp)
	jne	.L675
.L536:
	leaq	-1120(%rbp), %rax
	cmpq	$0, -1440(%rbp)
	movq	%rax, -3808(%rbp)
	jne	.L676
.L539:
	cmpq	$0, -1248(%rbp)
	jne	.L677
.L542:
	leaq	-736(%rbp), %rax
	cmpq	$0, -1056(%rbp)
	movq	%rax, -3816(%rbp)
	leaq	-928(%rbp), %rax
	movq	%rax, -3728(%rbp)
	jne	.L678
.L544:
	cmpq	$0, -864(%rbp)
	leaq	-544(%rbp), %rbx
	jne	.L679
.L549:
	cmpq	$0, -672(%rbp)
	leaq	-352(%rbp), %r13
	jne	.L680
.L552:
	cmpq	$0, -480(%rbp)
	jne	.L681
	cmpq	$0, -288(%rbp)
	jne	.L682
.L557:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3816(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3728(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3808(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3792(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3752(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3688(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3776(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3768(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3744(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3704(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3712(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3760(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3736(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3696(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3680(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L683
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L666:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	$0, -3648(%rbp)
	leaq	-3584(%rbp), %r13
	movq	$0, -3640(%rbp)
	leaq	-160(%rbp), %r12
	movq	$0, -3632(%rbp)
	movq	$0, -3624(%rbp)
	movq	$0, -3616(%rbp)
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3600(%rbp), %rax
	movq	-3680(%rbp), %rdi
	leaq	-3616(%rbp), %r9
	pushq	%rax
	leaq	-3608(%rbp), %rax
	leaq	-3632(%rbp), %rcx
	pushq	%rax
	leaq	-3624(%rbp), %r8
	leaq	-3640(%rbp), %rdx
	leaq	-3648(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_SA_SA_SA_
	movl	$31, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3640(%rbp), %rdx
	movq	-3648(%rbp), %rsi
	movq	%r15, %rcx
	movq	-3672(%rbp), %rdi
	call	_ZN2v88internal21Cast10JSReceiver_1404EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	-3648(%rbp), %rdx
	movq	%rax, %r8
	movq	-3640(%rbp), %rax
	movaps	%xmm0, -3584(%rbp)
	movq	%r8, -96(%rbp)
	movq	%rdx, -160(%rbp)
	movq	-3632(%rbp), %rdx
	movq	%rax, -152(%rbp)
	movq	%rdx, -144(%rbp)
	movq	-3624(%rbp), %rdx
	movq	%rax, -104(%rbp)
	movq	%rdx, -136(%rbp)
	movq	-3616(%rbp), %rdx
	movq	$0, -3568(%rbp)
	movq	%rdx, -128(%rbp)
	movq	-3608(%rbp), %rdx
	movq	%rdx, -120(%rbp)
	movq	-3600(%rbp), %rdx
	movq	%rdx, -112(%rbp)
	leaq	-88(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3736(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3584(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L510
	call	_ZdlPv@PLT
.L510:
	movq	-3752(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3232(%rbp), %rax
	cmpq	$0, -3544(%rbp)
	movq	%rax, -3696(%rbp)
	jne	.L684
.L511:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L674:
	movq	-3888(%rbp), %rsi
	movq	%r14, %rdi
	movabsq	$506663788666685447, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -3536(%rbp)
	movaps	%xmm0, -3552(%rbp)
	call	_Znwm@PLT
	movq	-3776(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%rax, -3552(%rbp)
	movq	%rdx, -3536(%rbp)
	movq	%rdx, -3544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L532
	call	_ZdlPv@PLT
.L532:
	movq	(%rbx), %rax
	movl	$33, %edx
	movq	%r14, %rdi
	movq	24(%rax), %rsi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	48(%rax), %r13
	movq	%rsi, -3792(%rbp)
	movq	40(%rax), %rsi
	movq	%rcx, -3728(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -3808(%rbp)
	movq	56(%rax), %rsi
	movq	64(%rax), %r12
	movq	%rbx, -3688(%rbp)
	movq	%rsi, -3888(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	32(%rax), %rbx
	movq	%rcx, -3752(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3672(%rbp), %rdi
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal29Cast20UT5ATSmi10HeapNumber_85EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %xmm4
	movq	%rbx, %xmm6
	movq	-3752(%rbp), %xmm7
	punpcklqdq	%xmm6, %xmm4
	pxor	%xmm0, %xmm0
	leaq	-72(%rbp), %rdx
	movq	%rax, -80(%rbp)
	movq	%r13, %xmm5
	leaq	-160(%rbp), %r12
	movq	-3688(%rbp), %xmm3
	leaq	-3584(%rbp), %r13
	movhps	-3888(%rbp), %xmm5
	movhps	-3808(%rbp), %xmm6
	movq	%r12, %rsi
	movq	%r13, %rdi
	movhps	-3792(%rbp), %xmm7
	movhps	-3728(%rbp), %xmm3
	movaps	%xmm4, -3936(%rbp)
	movaps	%xmm5, -3888(%rbp)
	movaps	%xmm6, -3808(%rbp)
	movaps	%xmm7, -3792(%rbp)
	movaps	%xmm3, -3728(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm0, -3584(%rbp)
	movq	$0, -3568(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1504(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -3752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L533
	call	_ZdlPv@PLT
.L533:
	movq	-3816(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1696(%rbp), %rax
	cmpq	$0, -3544(%rbp)
	movq	%rax, -3688(%rbp)
	jne	.L685
.L534:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L673:
	movq	-3808(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -3648(%rbp)
	movq	$0, -3640(%rbp)
	movq	$0, -3632(%rbp)
	movq	$0, -3624(%rbp)
	movq	$0, -3616(%rbp)
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	movq	$0, -3584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3584(%rbp), %rax
	movq	-3768(%rbp), %rdi
	pushq	%rax
	leaq	-3600(%rbp), %rax
	leaq	-3632(%rbp), %rcx
	pushq	%rax
	leaq	-3608(%rbp), %rax
	leaq	-3640(%rbp), %rdx
	pushq	%rax
	leaq	-3616(%rbp), %r9
	leaq	-3624(%rbp), %r8
	leaq	-3648(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_NS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_SB_PNS7_IS5_EE
	movq	-3672(%rbp), %rsi
	addq	$32, %rsp
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L672:
	movq	-3792(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-160(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r15, %rdi
	movabsq	$506663788666685447, %rax
	movl	$1800, %r11d
	leaq	-150(%rbp), %rdx
	movaps	%xmm0, -3552(%rbp)
	movq	%rax, -160(%rbp)
	movw	%r11w, -152(%rbp)
	movq	$0, -3536(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3744(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L528
	call	_ZdlPv@PLT
.L528:
	movq	(%rbx), %rax
	leaq	-88(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movdqu	48(%rax), %xmm0
	movdqu	(%rax), %xmm3
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	72(%rax), %rax
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -160(%rbp)
	movq	%rax, -96(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3552(%rbp)
	movq	$0, -3536(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1888(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -3776(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L529
	call	_ZdlPv@PLT
.L529:
	movq	-3888(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L671:
	movq	-3776(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-160(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	-151(%rbp), %rdx
	movaps	%xmm0, -3552(%rbp)
	movabsq	$506663788666685447, %rax
	movq	%rax, -160(%rbp)
	movb	$8, -152(%rbp)
	movq	$0, -3536(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3704(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L525
	call	_ZdlPv@PLT
.L525:
	movq	(%rbx), %rax
	leaq	-96(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movdqu	32(%rax), %xmm0
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm1
	movdqu	48(%rax), %xmm5
	movq	$0, -3536(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -3552(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2080(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -3768(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L526
	call	_ZdlPv@PLT
.L526:
	movq	-3808(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L670:
	leaq	-2600(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -3656(%rbp)
	leaq	-3584(%rbp), %r13
	movq	$0, -3648(%rbp)
	leaq	-160(%rbp), %r12
	movq	$0, -3640(%rbp)
	movq	$0, -3632(%rbp)
	movq	$0, -3624(%rbp)
	movq	$0, -3616(%rbp)
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3600(%rbp), %rax
	movq	-3712(%rbp), %rdi
	pushq	%rax
	leaq	-3608(%rbp), %rax
	leaq	-3624(%rbp), %r9
	pushq	%rax
	leaq	-3616(%rbp), %rax
	leaq	-3640(%rbp), %rcx
	pushq	%rax
	leaq	-3632(%rbp), %r8
	leaq	-3648(%rbp), %rdx
	leaq	-3656(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_NS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_SB_PNS7_IS5_EE
	addq	$32, %rsp
	movl	$32, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3640(%rbp), %rdx
	movq	-3656(%rbp), %rsi
	movq	%r15, %rcx
	movq	-3672(%rbp), %rdi
	call	_ZN2v88internal82Cast71UT19ATCallableApiObject17ATCallableJSProxy15JSBoundFunction10JSFunction_1405EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-80(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	-3640(%rbp), %xmm4
	movq	%rax, %xmm3
	movq	-3640(%rbp), %xmm0
	movq	$0, -3568(%rbp)
	movq	-3608(%rbp), %xmm1
	movq	-3624(%rbp), %xmm2
	punpcklqdq	%xmm3, %xmm4
	movhps	-3632(%rbp), %xmm0
	movq	-3656(%rbp), %xmm3
	movhps	-3600(%rbp), %xmm1
	movhps	-3616(%rbp), %xmm2
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3648(%rbp), %xmm3
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm0, -3584(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3744(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L521
	call	_ZdlPv@PLT
.L521:
	movq	-3792(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2464(%rbp), %rax
	cmpq	$0, -3544(%rbp)
	movq	%rax, -3704(%rbp)
	jne	.L686
.L522:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L669:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	$0, -3640(%rbp)
	movq	$0, -3632(%rbp)
	movq	$0, -3624(%rbp)
	movq	$0, -3616(%rbp)
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	movq	$0, -3584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3584(%rbp), %rax
	movq	-3760(%rbp), %rdi
	leaq	-3624(%rbp), %rcx
	pushq	%rax
	leaq	-3600(%rbp), %rax
	leaq	-3632(%rbp), %rdx
	pushq	%rax
	leaq	-3608(%rbp), %r9
	leaq	-3616(%rbp), %r8
	leaq	-3640(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_SA_SA_SA_
	movq	-3672(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rbx
	popq	%r12
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L668:
	movq	-3752(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-160(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	-151(%rbp), %rdx
	movaps	%xmm0, -3552(%rbp)
	movabsq	$578721382704613383, %rax
	movq	%rax, -160(%rbp)
	movb	$7, -152(%rbp)
	movq	$0, -3536(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3736(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3552(%rbp), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L517
	call	_ZdlPv@PLT
.L517:
	movq	0(%r13), %rax
	pxor	%xmm0, %xmm0
	movq	24(%rax), %rdi
	movq	32(%rax), %rsi
	movq	48(%rax), %rdx
	movq	(%rax), %r10
	movq	8(%rax), %r9
	movq	16(%rax), %r8
	movq	40(%rax), %rcx
	movq	64(%rax), %rax
	movq	%rdi, -136(%rbp)
	movq	%r15, %rdi
	movq	%rsi, -128(%rbp)
	movq	%r12, %rsi
	movq	%rdx, -112(%rbp)
	leaq	-96(%rbp), %rdx
	movq	%r10, -160(%rbp)
	movq	%r9, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%rcx, -120(%rbp)
	movq	%rax, -104(%rbp)
	movaps	%xmm0, -3552(%rbp)
	movq	$0, -3536(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3712(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L518
	call	_ZdlPv@PLT
.L518:
	leaq	-2600(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L667:
	movq	-3768(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-160(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	-152(%rbp), %rdx
	movabsq	$578721382704613383, %rax
	movaps	%xmm0, -3552(%rbp)
	movq	%rax, -160(%rbp)
	movq	$0, -3536(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3696(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3552(%rbp), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L514
	call	_ZdlPv@PLT
.L514:
	movq	0(%r13), %rax
	leaq	-104(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movdqu	32(%rax), %xmm0
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm1
	movq	48(%rax), %rax
	movq	$0, -3536(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -112(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -3552(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2848(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -3760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L515
	call	_ZdlPv@PLT
.L515:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L677:
	movq	-3872(%rbp), %rsi
	movq	%r14, %rdi
	movabsq	$506663788666685447, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -3536(%rbp)
	movaps	%xmm0, -3552(%rbp)
	call	_Znwm@PLT
	movq	-3792(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%rax, -3552(%rbp)
	movq	%rdx, -3536(%rbp)
	movq	%rdx, -3544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L543
	call	_ZdlPv@PLT
.L543:
	movq	-3672(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L676:
	movq	-3816(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-160(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r15, %rdi
	movabsq	$506663788666685447, %rax
	movl	$2055, %r9d
	leaq	-149(%rbp), %rdx
	movaps	%xmm0, -3552(%rbp)
	movq	%rax, -160(%rbp)
	movw	%r9w, -152(%rbp)
	movb	$8, -150(%rbp)
	movq	$0, -3536(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3752(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L540
	call	_ZdlPv@PLT
.L540:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	40(%rax), %rdi
	movq	48(%rax), %rsi
	movq	64(%rax), %rdx
	movq	8(%rax), %r11
	movq	16(%rax), %r10
	movq	24(%rax), %r9
	movq	32(%rax), %r8
	movq	56(%rax), %rcx
	movq	(%rax), %rbx
	movq	80(%rax), %rax
	movq	%rdi, -120(%rbp)
	movq	%r15, %rdi
	movq	%rsi, -112(%rbp)
	movq	%r12, %rsi
	movq	%rdx, -96(%rbp)
	leaq	-80(%rbp), %rdx
	movq	%r11, -152(%rbp)
	movq	%r10, -144(%rbp)
	movq	%r9, -136(%rbp)
	movq	%r8, -128(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%rax, -88(%rbp)
	movaps	%xmm0, -3552(%rbp)
	movq	%rbx, -160(%rbp)
	movq	$0, -3536(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1120(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -3808(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L541
	call	_ZdlPv@PLT
.L541:
	movq	-3840(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L675:
	movq	-3904(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-160(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r15, %rdi
	movabsq	$506663788666685447, %rax
	movl	$2055, %r10d
	leaq	-150(%rbp), %rdx
	movaps	%xmm0, -3552(%rbp)
	movq	%rax, -160(%rbp)
	movw	%r10w, -152(%rbp)
	movq	$0, -3536(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3688(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L537
	call	_ZdlPv@PLT
.L537:
	movq	(%rbx), %rax
	leaq	-88(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movdqu	48(%rax), %xmm0
	movdqu	(%rax), %xmm3
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	64(%rax), %rax
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -160(%rbp)
	movq	%rax, -96(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3552(%rbp)
	movq	$0, -3536(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1312(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -3792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L538
	call	_ZdlPv@PLT
.L538:
	movq	-3872(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L678:
	movq	-3840(%rbp), %rsi
	movq	%r14, %rdi
	movabsq	$506663788666685447, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -3536(%rbp)
	movaps	%xmm0, -3552(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r8d
	movq	-3808(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, (%rax)
	leaq	10(%rax), %rdx
	movw	%r8w, 8(%rax)
	movq	%rax, -3552(%rbp)
	movq	%rdx, -3536(%rbp)
	movq	%rdx, -3544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L545
	call	_ZdlPv@PLT
.L545:
	movq	(%rbx), %rax
	movl	$34, %edx
	movq	%r14, %rdi
	movq	24(%rax), %rsi
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	32(%rax), %r13
	movq	%rsi, -3872(%rbp)
	movq	56(%rax), %rsi
	movq	48(%rax), %r12
	movq	%rbx, -3728(%rbp)
	movq	%rsi, -3888(%rbp)
	movq	64(%rax), %rsi
	movq	40(%rax), %rbx
	movq	%rcx, -3816(%rbp)
	movq	16(%rax), %rcx
	movq	72(%rax), %rax
	movq	%rsi, -3904(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rax, -3936(%rbp)
	movq	%rcx, -3840(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3672(%rbp), %rdi
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal29Cast20UT5ATSmi10HeapNumber_85EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %xmm5
	movq	%rbx, %xmm7
	movq	-3904(%rbp), %xmm4
	punpcklqdq	%xmm7, %xmm5
	pxor	%xmm0, %xmm0
	leaq	-64(%rbp), %rdx
	movq	%rax, -72(%rbp)
	movq	%r12, %xmm2
	leaq	-3584(%rbp), %r13
	movaps	%xmm5, -128(%rbp)
	movq	-3840(%rbp), %xmm6
	movq	-3728(%rbp), %xmm7
	movq	%r13, %rdi
	movq	%rbx, -80(%rbp)
	leaq	-160(%rbp), %r12
	movhps	-3936(%rbp), %xmm4
	movhps	-3872(%rbp), %xmm6
	movq	%r12, %rsi
	movaps	%xmm5, -3936(%rbp)
	movhps	-3888(%rbp), %xmm2
	movhps	-3816(%rbp), %xmm7
	movaps	%xmm4, -3904(%rbp)
	movaps	%xmm2, -3888(%rbp)
	movaps	%xmm6, -3840(%rbp)
	movaps	%xmm7, -3872(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm0, -3584(%rbp)
	movq	$0, -3568(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-736(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -3816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L546
	call	_ZdlPv@PLT
.L546:
	movq	-3848(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-928(%rbp), %rax
	cmpq	$0, -3544(%rbp)
	movq	%rax, -3728(%rbp)
	jne	.L687
.L547:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L681:
	movq	-3856(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -3536(%rbp)
	movaps	%xmm0, -3552(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%rbx, %rdi
	movabsq	$506663788666685447, %rcx
	movw	%si, 8(%rax)
	leaq	10(%rax), %rdx
	movq	%r15, %rsi
	movq	%rcx, (%rax)
	movq	%rax, -3552(%rbp)
	movq	%rdx, -3536(%rbp)
	movq	%rdx, -3544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L556
	call	_ZdlPv@PLT
.L556:
	movq	-3672(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -288(%rbp)
	je	.L557
.L682:
	movq	-3824(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-160(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$2055, %ecx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movabsq	$506663788666685447, %rax
	pxor	%xmm0, %xmm0
	leaq	-149(%rbp), %rdx
	movw	%cx, -152(%rbp)
	movq	%rax, -160(%rbp)
	movaps	%xmm0, -3552(%rbp)
	movb	$8, -150(%rbp)
	movq	$0, -3536(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L558
	movq	%rax, -3840(%rbp)
	call	_ZdlPv@PLT
	movq	-3840(%rbp), %rax
.L558:
	movq	(%rax), %rax
	movl	$37, %edx
	movq	%r14, %rdi
	movq	56(%rax), %rsi
	movq	(%rax), %r9
	movq	24(%rax), %rcx
	movq	%rsi, -3824(%rbp)
	movq	64(%rax), %rsi
	movq	%r9, -3936(%rbp)
	movq	%rsi, -3848(%rbp)
	movq	72(%rax), %rsi
	movq	80(%rax), %rax
	movq	%rcx, -3840(%rbp)
	movq	%rsi, -3872(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rax, -3856(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3672(%rbp), %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$38, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -3904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3672(%rbp), %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$36, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -3888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-3600(%rbp), %r10
	movq	-3672(%rbp), %rsi
	movq	%r10, %rdi
	movq	%r10, -3912(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3912(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$747, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-3912(%rbp), %r10
	movq	-3552(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$8, %edi
	xorl	%esi, %esi
	movq	-3824(%rbp), %xmm6
	pushq	%rdi
	movq	-3912(%rbp), %r10
	movq	%rax, %r8
	movl	$1, %ecx
	movdqa	%xmm6, %xmm0
	pushq	%r12
	movq	-3936(%rbp), %r9
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rdx
	movhps	-3848(%rbp), %xmm0
	movq	-3536(%rbp), %rax
	movq	%r10, %rdi
	movq	%rdx, -3584(%rbp)
	movaps	%xmm0, -160(%rbp)
	leaq	-3584(%rbp), %rdx
	movq	-3840(%rbp), %xmm0
	movq	%rax, -3576(%rbp)
	movhps	-3904(%rbp), %xmm0
	movq	%r10, -3840(%rbp)
	movaps	%xmm0, -144(%rbp)
	movdqa	%xmm6, %xmm0
	movhps	-3872(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-3856(%rbp), %xmm0
	movhps	-3888(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-3840(%rbp), %r10
	movq	%rax, %r12
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3672(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L680:
	movq	-3848(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-160(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	-148(%rbp), %rdx
	movabsq	$506663788666685447, %rax
	movaps	%xmm0, -3552(%rbp)
	movq	%rax, -160(%rbp)
	movl	$134744071, -152(%rbp)
	movq	$0, -3536(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3816(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3552(%rbp), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L553
	call	_ZdlPv@PLT
.L553:
	movq	0(%r13), %rax
	leaq	-72(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	-352(%rbp), %r13
	movdqu	64(%rax), %xmm0
	movdqu	(%rax), %xmm4
	movdqu	16(%rax), %xmm3
	movdqu	32(%rax), %xmm2
	movdqu	48(%rax), %xmm1
	movq	88(%rax), %rax
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -160(%rbp)
	movq	%rax, -80(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3552(%rbp)
	movq	$0, -3536(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L554
	call	_ZdlPv@PLT
.L554:
	movq	-3824(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L679:
	movq	-3912(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-160(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$2055, %edi
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movw	%di, -152(%rbp)
	leaq	-149(%rbp), %rdx
	movq	%r15, %rdi
	movabsq	$506663788666685447, %rax
	movq	%rax, -160(%rbp)
	movaps	%xmm0, -3552(%rbp)
	movb	$8, -150(%rbp)
	movq	$0, -3536(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3728(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L550
	call	_ZdlPv@PLT
.L550:
	movq	(%rbx), %rax
	leaq	-80(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	-544(%rbp), %rbx
	movdqu	48(%rax), %xmm0
	movdqu	(%rax), %xmm3
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movdqu	64(%rax), %xmm5
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3552(%rbp)
	movq	$0, -3536(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L551
	call	_ZdlPv@PLT
.L551:
	movq	-3856(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L686:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	-3656(%rbp), %rdx
	movq	-3640(%rbp), %rax
	movaps	%xmm0, -3584(%rbp)
	movq	$0, -3568(%rbp)
	movq	%rdx, -160(%rbp)
	movq	-3648(%rbp), %rdx
	movq	%rax, -144(%rbp)
	movq	%rdx, -152(%rbp)
	movq	-3632(%rbp), %rdx
	movq	%rax, -96(%rbp)
	movq	%rdx, -136(%rbp)
	movq	-3624(%rbp), %rdx
	movq	%rdx, -128(%rbp)
	movq	-3616(%rbp), %rdx
	movq	%rdx, -120(%rbp)
	movq	-3608(%rbp), %rdx
	movq	%rdx, -112(%rbp)
	movq	-3600(%rbp), %rdx
	movq	%rdx, -104(%rbp)
	leaq	-88(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3704(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L523
	call	_ZdlPv@PLT
.L523:
	movq	-3776(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L685:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rdx
	movq	%r12, %rsi
	movdqa	-3728(%rbp), %xmm6
	movdqa	-3792(%rbp), %xmm7
	movq	%r13, %rdi
	movaps	%xmm0, -3584(%rbp)
	movdqa	-3936(%rbp), %xmm4
	movq	$0, -3568(%rbp)
	movaps	%xmm6, -160(%rbp)
	movdqa	-3808(%rbp), %xmm6
	movaps	%xmm7, -144(%rbp)
	movdqa	-3888(%rbp), %xmm7
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3688(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L535
	call	_ZdlPv@PLT
.L535:
	movq	-3904(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L684:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-96(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	-3600(%rbp), %xmm0
	movq	-3616(%rbp), %xmm1
	movq	$0, -3568(%rbp)
	movq	-3632(%rbp), %xmm2
	movq	-3648(%rbp), %xmm3
	movhps	-3640(%rbp), %xmm0
	movhps	-3608(%rbp), %xmm1
	movhps	-3624(%rbp), %xmm2
	movhps	-3640(%rbp), %xmm3
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3584(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3696(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L512
	call	_ZdlPv@PLT
.L512:
	movq	-3768(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L687:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-3872(%rbp), %xmm3
	movq	%r12, %rsi
	movdqa	-3840(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	leaq	-72(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rbx, -80(%rbp)
	movdqa	-3936(%rbp), %xmm7
	movdqa	-3888(%rbp), %xmm4
	movaps	%xmm3, -160(%rbp)
	movdqa	-3904(%rbp), %xmm3
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -3584(%rbp)
	movq	$0, -3568(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3728(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L548
	call	_ZdlPv@PLT
.L548:
	movq	-3912(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L547
.L683:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22469:
	.size	_ZN2v88internal46ArrayForEachLoopLazyDeoptContinuationAssembler49GenerateArrayForEachLoopLazyDeoptContinuationImplEv, .-_ZN2v88internal46ArrayForEachLoopLazyDeoptContinuationAssembler49GenerateArrayForEachLoopLazyDeoptContinuationImplEv
	.section	.rodata._ZN2v88internal8Builtins46Generate_ArrayForEachLoopLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"ArrayForEachLoopLazyDeoptContinuation"
	.section	.text._ZN2v88internal8Builtins46Generate_ArrayForEachLoopLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins46Generate_ArrayForEachLoopLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins46Generate_ArrayForEachLoopLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins46Generate_ArrayForEachLoopLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE:
.LFB22465:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$455, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$746, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L692
.L689:
	movq	%r13, %rdi
	call	_ZN2v88internal46ArrayForEachLoopLazyDeoptContinuationAssembler49GenerateArrayForEachLoopLazyDeoptContinuationImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L693
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L692:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L689
.L693:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22465:
	.size	_ZN2v88internal8Builtins46Generate_ArrayForEachLoopLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins46Generate_ArrayForEachLoopLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_:
.LFB27185:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$14, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$136, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$506098639673231111, %rcx
	movq	%rcx, (%rax)
	movl	$1028, %ecx
	leaq	14(%rax), %rdx
	movl	$67569415, 8(%rax)
	movw	%cx, 12(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L695
	movq	%rax, -168(%rbp)
	call	_ZdlPv@PLT
	movq	-168(%rbp), %rax
.L695:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L696
	movq	%rdx, (%r15)
.L696:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L697
	movq	%rdx, (%r14)
.L697:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L698
	movq	%rdx, 0(%r13)
.L698:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L699
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L699:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L700
	movq	%rdx, (%rbx)
.L700:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L701
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L701:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L702
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L702:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L703
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L703:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L704
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L704:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L705
	movq	-128(%rbp), %rsi
	movq	%rdx, (%rsi)
.L705:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L706
	movq	-136(%rbp), %rcx
	movq	%rdx, (%rcx)
.L706:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L707
	movq	-144(%rbp), %rbx
	movq	%rdx, (%rbx)
.L707:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L708
	movq	-152(%rbp), %rsi
	movq	%rdx, (%rsi)
.L708:
	movq	104(%rax), %rax
	testq	%rax, %rax
	je	.L694
	movq	-160(%rbp), %rcx
	movq	%rax, (%rcx)
.L694:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L757
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L757:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27185:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SQ_SO_SM_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SQ_SO_SM_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SQ_SO_SM_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SQ_SO_SM_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SQ_SO_SM_:
.LFB27195:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$20, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$184, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	88(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	96(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	104(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	112(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	120(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	128(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movdqa	.LC7(%rip), %xmm0
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$134612742, 16(%rax)
	leaq	20(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L759
	movq	%rax, -216(%rbp)
	call	_ZdlPv@PLT
	movq	-216(%rbp), %rax
.L759:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L760
	movq	%rdx, (%r15)
.L760:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L761
	movq	%rdx, (%r14)
.L761:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L762
	movq	%rdx, 0(%r13)
.L762:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L763
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L763:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L764
	movq	%rdx, (%rbx)
.L764:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L765
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L765:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L766
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L766:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L767
	movq	-112(%rbp), %rcx
	movq	%rdx, (%rcx)
.L767:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L768
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L768:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L769
	movq	-128(%rbp), %rcx
	movq	%rdx, (%rcx)
.L769:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L770
	movq	-136(%rbp), %rbx
	movq	%rdx, (%rbx)
.L770:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L771
	movq	-144(%rbp), %rsi
	movq	%rdx, (%rsi)
.L771:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L772
	movq	-152(%rbp), %rcx
	movq	%rdx, (%rcx)
.L772:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L773
	movq	-160(%rbp), %rbx
	movq	%rdx, (%rbx)
.L773:
	movq	112(%rax), %rdx
	testq	%rdx, %rdx
	je	.L774
	movq	-168(%rbp), %rsi
	movq	%rdx, (%rsi)
.L774:
	movq	120(%rax), %rdx
	testq	%rdx, %rdx
	je	.L775
	movq	-176(%rbp), %rdi
	movq	%rdx, (%rdi)
.L775:
	movq	128(%rax), %rdx
	testq	%rdx, %rdx
	je	.L776
	movq	-184(%rbp), %rcx
	movq	%rdx, (%rcx)
.L776:
	movq	136(%rax), %rdx
	testq	%rdx, %rdx
	je	.L777
	movq	-192(%rbp), %rbx
	movq	%rdx, (%rbx)
.L777:
	movq	144(%rax), %rdx
	testq	%rdx, %rdx
	je	.L778
	movq	-200(%rbp), %rsi
	movq	%rdx, (%rsi)
.L778:
	movq	152(%rax), %rax
	testq	%rax, %rax
	je	.L758
	movq	-208(%rbp), %rdi
	movq	%rax, (%rdi)
.L758:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L845
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L845:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27195:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SQ_SO_SM_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SQ_SO_SM_
	.section	.rodata._ZN2v88internal18FastArrayForEach_7EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_NS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableISA_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"../../deps/v8/../../deps/v8/src/builtins/base.tq"
	.section	.text._ZN2v88internal18FastArrayForEach_7EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_NS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableISA_EE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18FastArrayForEach_7EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_NS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableISA_EE
	.type	_ZN2v88internal18FastArrayForEach_7EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_NS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableISA_EE, @function
_ZN2v88internal18FastArrayForEach_7EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_NS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableISA_EE:
.LFB22521:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$4088, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%rax, -8024(%rbp)
	movq	24(%rbp), %rax
	movq	%r9, %r14
	movq	%rdx, %rbx
	movq	%rdi, -7480(%rbp)
	leaq	-7464(%rbp), %r13
	leaq	-7080(%rbp), %r12
	movq	%rsi, -7536(%rbp)
	movq	%r13, %r15
	movq	%r8, -7504(%rbp)
	movq	%rcx, -7520(%rbp)
	movq	%rax, -8032(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	%rdi, -7464(%rbp)
	movq	%rdi, -7136(%rbp)
	movl	$120, %edi
	movq	$0, -7128(%rbp)
	movq	$0, -7120(%rbp)
	movq	$0, -7112(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -7112(%rbp)
	movq	%rdx, -7120(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -7096(%rbp)
	movq	%rax, -7128(%rbp)
	movq	$0, -7104(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$168, %edi
	movq	$0, -6936(%rbp)
	movq	$0, -6928(%rbp)
	movq	%rax, -6944(%rbp)
	movq	$0, -6920(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -6936(%rbp)
	leaq	-6888(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6920(%rbp)
	movq	%rdx, -6928(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6904(%rbp)
	movq	%rax, -7632(%rbp)
	movq	$0, -6912(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$192, %edi
	movq	$0, -6744(%rbp)
	movq	$0, -6736(%rbp)
	movq	%rax, -6752(%rbp)
	movq	$0, -6728(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -6744(%rbp)
	leaq	-6696(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6728(%rbp)
	movq	%rdx, -6736(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6712(%rbp)
	movq	%rax, -7576(%rbp)
	movq	$0, -6720(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$144, %edi
	movq	$0, -6552(%rbp)
	movq	$0, -6544(%rbp)
	movq	%rax, -6560(%rbp)
	movq	$0, -6536(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -6552(%rbp)
	leaq	-6504(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6536(%rbp)
	movq	%rdx, -6544(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6520(%rbp)
	movq	%rax, -7584(%rbp)
	movq	$0, -6528(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$168, %edi
	movq	$0, -6360(%rbp)
	movq	$0, -6352(%rbp)
	movq	%rax, -6368(%rbp)
	movq	$0, -6344(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -6360(%rbp)
	leaq	-6312(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6344(%rbp)
	movq	%rdx, -6352(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6328(%rbp)
	movq	%rax, -7664(%rbp)
	movq	$0, -6336(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$192, %edi
	movq	$0, -6168(%rbp)
	movq	$0, -6160(%rbp)
	movq	%rax, -6176(%rbp)
	movq	$0, -6152(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -6168(%rbp)
	leaq	-6120(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6152(%rbp)
	movq	%rdx, -6160(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6136(%rbp)
	movq	%rax, -7672(%rbp)
	movq	$0, -6144(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$216, %edi
	movq	$0, -5976(%rbp)
	movq	$0, -5968(%rbp)
	movq	%rax, -5984(%rbp)
	movq	$0, -5960(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -5976(%rbp)
	leaq	-5928(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5960(%rbp)
	movq	%rdx, -5968(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5944(%rbp)
	movq	%rax, -7600(%rbp)
	movq	$0, -5952(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$168, %edi
	movq	$0, -5784(%rbp)
	movq	$0, -5776(%rbp)
	movq	%rax, -5792(%rbp)
	movq	$0, -5768(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -5784(%rbp)
	leaq	-5736(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5768(%rbp)
	movq	%rdx, -5776(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5752(%rbp)
	movq	%rax, -7616(%rbp)
	movq	$0, -5760(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$192, %edi
	movq	$0, -5592(%rbp)
	movq	$0, -5584(%rbp)
	movq	%rax, -5600(%rbp)
	movq	$0, -5576(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -5592(%rbp)
	leaq	-5544(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5576(%rbp)
	movq	%rdx, -5584(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5560(%rbp)
	movq	%rax, -7680(%rbp)
	movq	$0, -5568(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$336, %edi
	movq	$0, -5400(%rbp)
	movq	$0, -5392(%rbp)
	movq	%rax, -5408(%rbp)
	movq	$0, -5384(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -5400(%rbp)
	leaq	-5352(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5384(%rbp)
	movq	%rdx, -5392(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5368(%rbp)
	movq	%rax, -7888(%rbp)
	movq	$0, -5376(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$336, %edi
	movq	$0, -5208(%rbp)
	movq	$0, -5200(%rbp)
	movq	%rax, -5216(%rbp)
	movq	$0, -5192(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -5208(%rbp)
	leaq	-5160(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5192(%rbp)
	movq	%rdx, -5200(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5176(%rbp)
	movq	%rax, -7776(%rbp)
	movq	$0, -5184(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$336, %edi
	movq	$0, -5016(%rbp)
	movq	$0, -5008(%rbp)
	movq	%rax, -5024(%rbp)
	movq	$0, -5000(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -5016(%rbp)
	leaq	-4968(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5000(%rbp)
	movq	%rdx, -5008(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4984(%rbp)
	movq	%rax, -7792(%rbp)
	movq	$0, -4992(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$336, %edi
	movq	$0, -4824(%rbp)
	movq	$0, -4816(%rbp)
	movq	%rax, -4832(%rbp)
	movq	$0, -4808(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -4824(%rbp)
	leaq	-4776(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4808(%rbp)
	movq	%rdx, -4816(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4792(%rbp)
	movq	%rax, -7808(%rbp)
	movq	$0, -4800(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$336, %edi
	movq	$0, -4632(%rbp)
	movq	$0, -4624(%rbp)
	movq	%rax, -4640(%rbp)
	movq	$0, -4616(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -4632(%rbp)
	leaq	-4584(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4616(%rbp)
	movq	%rdx, -4624(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4600(%rbp)
	movq	%rax, -7744(%rbp)
	movq	$0, -4608(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$336, %edi
	movq	$0, -4440(%rbp)
	movq	$0, -4432(%rbp)
	movq	%rax, -4448(%rbp)
	movq	$0, -4424(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -4440(%rbp)
	leaq	-4392(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4424(%rbp)
	movq	%rdx, -4432(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4408(%rbp)
	movq	%rax, -7840(%rbp)
	movq	$0, -4416(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$336, %edi
	movq	$0, -4248(%rbp)
	movq	$0, -4240(%rbp)
	movq	%rax, -4256(%rbp)
	movq	$0, -4232(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -4248(%rbp)
	leaq	-4200(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4232(%rbp)
	movq	%rdx, -4240(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4216(%rbp)
	movq	%rax, -7816(%rbp)
	movq	$0, -4224(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$336, %edi
	movq	$0, -4056(%rbp)
	movq	$0, -4048(%rbp)
	movq	%rax, -4064(%rbp)
	movq	$0, -4040(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -4056(%rbp)
	leaq	-4008(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4040(%rbp)
	movq	%rdx, -4048(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4024(%rbp)
	movq	%rax, -7552(%rbp)
	movq	$0, -4032(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$336, %edi
	movq	$0, -3864(%rbp)
	movq	$0, -3856(%rbp)
	movq	%rax, -3872(%rbp)
	movq	$0, -3848(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -3864(%rbp)
	leaq	-3816(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3848(%rbp)
	movq	%rdx, -3856(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3832(%rbp)
	movq	%rax, -7824(%rbp)
	movq	$0, -3840(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$384, %edi
	movq	$0, -3672(%rbp)
	movq	$0, -3664(%rbp)
	movq	%rax, -3680(%rbp)
	movq	$0, -3656(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	384(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movq	%rax, -3672(%rbp)
	leaq	-3624(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3656(%rbp)
	movq	%rdx, -3664(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3640(%rbp)
	movq	%rax, -7848(%rbp)
	movq	$0, -3648(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$336, %edi
	movq	$0, -3480(%rbp)
	movq	$0, -3472(%rbp)
	movq	%rax, -3488(%rbp)
	movq	$0, -3464(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -3480(%rbp)
	leaq	-3432(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3464(%rbp)
	movq	%rdx, -3472(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3448(%rbp)
	movq	%rax, -7856(%rbp)
	movq	$0, -3456(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$336, %edi
	movq	$0, -3288(%rbp)
	movq	$0, -3280(%rbp)
	movq	%rax, -3296(%rbp)
	movq	$0, -3272(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -3288(%rbp)
	leaq	-3240(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3272(%rbp)
	movq	%rdx, -3280(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3256(%rbp)
	movq	%rax, -7936(%rbp)
	movq	$0, -3264(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$408, %edi
	movq	$0, -3096(%rbp)
	movq	$0, -3088(%rbp)
	movq	%rax, -3104(%rbp)
	movq	$0, -3080(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	408(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 400(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movq	%rax, -3096(%rbp)
	leaq	-3048(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3080(%rbp)
	movq	%rdx, -3088(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3064(%rbp)
	movq	%rax, -7952(%rbp)
	movq	$0, -3072(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$456, %edi
	movq	$0, -2904(%rbp)
	movq	$0, -2896(%rbp)
	movq	%rax, -2912(%rbp)
	movq	$0, -2888(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movups	%xmm0, (%rax)
	leaq	456(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movq	%rax, -2904(%rbp)
	movq	$0, 448(%rax)
	leaq	-2856(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2888(%rbp)
	movq	%rdx, -2896(%rbp)
	xorl	%edx, %edx
	movq	$0, -2880(%rbp)
	movups	%xmm0, -2872(%rbp)
	movq	%rax, -8000(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$480, %edi
	movq	$0, -2712(%rbp)
	movq	$0, -2704(%rbp)
	movq	%rax, -2720(%rbp)
	movq	$0, -2696(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movups	%xmm0, (%rax)
	leaq	480(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -2712(%rbp)
	leaq	-2664(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2696(%rbp)
	movq	%rdx, -2704(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2680(%rbp)
	movq	%rax, -7968(%rbp)
	movq	$0, -2688(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$408, %edi
	movq	$0, -2520(%rbp)
	movq	$0, -2512(%rbp)
	movq	%rax, -2528(%rbp)
	movq	$0, -2504(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	408(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 400(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movq	%rax, -2520(%rbp)
	leaq	-2472(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2504(%rbp)
	movq	%rdx, -2512(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2488(%rbp)
	movq	%rax, -7984(%rbp)
	movq	$0, -2496(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$456, %edi
	movq	$0, -2328(%rbp)
	movq	$0, -2320(%rbp)
	movq	%rax, -2336(%rbp)
	movq	$0, -2312(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movups	%xmm0, (%rax)
	leaq	456(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movq	%rax, -2328(%rbp)
	movq	$0, 448(%rax)
	leaq	-2280(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2312(%rbp)
	movq	%rdx, -2320(%rbp)
	xorl	%edx, %edx
	movq	$0, -2304(%rbp)
	movups	%xmm0, -2296(%rbp)
	movq	%rax, -8016(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$480, %edi
	movq	$0, -2136(%rbp)
	movq	$0, -2128(%rbp)
	movq	%rax, -2144(%rbp)
	movq	$0, -2120(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movups	%xmm0, (%rax)
	leaq	480(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -2136(%rbp)
	leaq	-2088(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2120(%rbp)
	movq	%rdx, -2128(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2104(%rbp)
	movq	%rax, -7720(%rbp)
	movq	$0, -2112(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$432, %edi
	movq	$0, -1944(%rbp)
	movq	$0, -1936(%rbp)
	movq	%rax, -1952(%rbp)
	movq	$0, -1928(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movups	%xmm0, (%rax)
	leaq	432(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movq	%rax, -1944(%rbp)
	leaq	-1896(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1928(%rbp)
	movq	%rdx, -1936(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1912(%rbp)
	movq	$0, -1920(%rbp)
	movq	%rax, -7560(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$336, %edi
	movq	$0, -1752(%rbp)
	movq	$0, -1744(%rbp)
	movq	%rax, -1760(%rbp)
	movq	$0, -1736(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -1752(%rbp)
	leaq	-1704(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1736(%rbp)
	movq	%rdx, -1744(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1720(%rbp)
	movq	%rax, -7488(%rbp)
	movq	$0, -1728(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$360, %edi
	movq	$0, -1560(%rbp)
	movq	$0, -1552(%rbp)
	movq	%rax, -1568(%rbp)
	movq	$0, -1544(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	360(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 352(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movq	%rax, -1560(%rbp)
	leaq	-1512(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1544(%rbp)
	movq	%rdx, -1552(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1528(%rbp)
	movq	%rax, -7728(%rbp)
	movq	$0, -1536(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$504, %edi
	movq	$0, -1368(%rbp)
	movq	$0, -1360(%rbp)
	movq	%rax, -1376(%rbp)
	movq	$0, -1352(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	504(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -1368(%rbp)
	movq	%rdx, -1352(%rbp)
	movups	%xmm0, 480(%rax)
	movq	$0, 496(%rax)
	leaq	-1320(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1360(%rbp)
	xorl	%edx, %edx
	movq	%rax, -7736(%rbp)
	movq	$0, -1344(%rbp)
	movups	%xmm0, -1336(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$336, %edi
	movq	$0, -1176(%rbp)
	movq	$0, -1168(%rbp)
	movq	%rax, -1184(%rbp)
	movq	$0, -1160(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -1176(%rbp)
	leaq	-1128(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1160(%rbp)
	movq	%rdx, -1168(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1144(%rbp)
	movq	%rax, -7912(%rbp)
	movq	$0, -1152(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$336, %edi
	movq	$0, -984(%rbp)
	movq	$0, -976(%rbp)
	movq	%rax, -992(%rbp)
	movq	$0, -968(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -984(%rbp)
	leaq	-936(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -968(%rbp)
	movq	%rdx, -976(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -952(%rbp)
	movq	%rax, -7920(%rbp)
	movq	$0, -960(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$144, %edi
	movq	$0, -792(%rbp)
	movq	$0, -784(%rbp)
	movq	%rax, -800(%rbp)
	movq	$0, -776(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -792(%rbp)
	leaq	-744(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -776(%rbp)
	movq	%rdx, -784(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -760(%rbp)
	movq	%rax, -7992(%rbp)
	movq	$0, -768(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$24, %edi
	movq	$0, -600(%rbp)
	movq	$0, -592(%rbp)
	movq	%rax, -608(%rbp)
	movq	$0, -584(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 16(%rax)
	movq	%rax, -600(%rbp)
	leaq	-552(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -584(%rbp)
	movq	%rdx, -592(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -568(%rbp)
	movq	%rax, -7880(%rbp)
	movq	$0, -576(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7464(%rbp), %rax
	movl	$144, %edi
	movq	$0, -408(%rbp)
	movq	$0, -400(%rbp)
	movq	%rax, -416(%rbp)
	movq	$0, -392(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	leaq	-7264(%rbp), %r13
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -408(%rbp)
	leaq	-360(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -392(%rbp)
	movq	%rdx, -400(%rbp)
	xorl	%edx, %edx
	movq	%rax, -7904(%rbp)
	movups	%xmm0, -376(%rbp)
	movq	$0, -384(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7536(%rbp), %r10
	movq	-7520(%rbp), %r11
	pxor	%xmm0, %xmm0
	movq	-7504(%rbp), %rax
	movl	$40, %edi
	movaps	%xmm0, -7264(%rbp)
	movq	%r10, -224(%rbp)
	movq	%r11, -208(%rbp)
	movq	%rbx, -216(%rbp)
	movq	%rax, -200(%rbp)
	movq	%r14, -192(%rbp)
	movq	$0, -7248(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm7
	movq	-192(%rbp), %rcx
	movq	%r13, %rsi
	leaq	40(%rax), %rdx
	movq	%rax, -7264(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-208(%rbp), %xmm7
	movq	%rcx, 32(%rax)
	movups	%xmm7, 16(%rax)
	leaq	-7136(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	movq	%rax, -7896(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L847
	call	_ZdlPv@PLT
.L847:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-6752(%rbp), %rax
	cmpq	$0, -7072(%rbp)
	movq	%rax, -7624(%rbp)
	leaq	-6944(%rbp), %rax
	movq	%rax, -7568(%rbp)
	jne	.L1227
.L848:
	leaq	-6560(%rbp), %rax
	cmpq	$0, -6880(%rbp)
	movq	%rax, -7648(%rbp)
	jne	.L1228
.L853:
	leaq	-6368(%rbp), %rax
	cmpq	$0, -6688(%rbp)
	movq	%rax, -7632(%rbp)
	jne	.L1229
.L856:
	leaq	-608(%rbp), %rax
	cmpq	$0, -6496(%rbp)
	movq	%rax, -7504(%rbp)
	jne	.L1230
.L859:
	leaq	-5984(%rbp), %rax
	cmpq	$0, -6304(%rbp)
	movq	%rax, -7584(%rbp)
	leaq	-6176(%rbp), %rax
	movq	%rax, -7576(%rbp)
	jne	.L1231
.L862:
	leaq	-5792(%rbp), %rax
	cmpq	$0, -6112(%rbp)
	movq	%rax, -7664(%rbp)
	jne	.L1232
.L867:
	leaq	-5600(%rbp), %rax
	cmpq	$0, -5920(%rbp)
	movq	%rax, -7672(%rbp)
	jne	.L1233
.L870:
	cmpq	$0, -5728(%rbp)
	jne	.L1234
.L873:
	leaq	-5408(%rbp), %rax
	cmpq	$0, -5536(%rbp)
	movq	%rax, -7520(%rbp)
	jne	.L1235
.L876:
	leaq	-5216(%rbp), %rax
	cmpq	$0, -5344(%rbp)
	movq	%rax, -7680(%rbp)
	leaq	-992(%rbp), %rax
	movq	%rax, -7760(%rbp)
	jne	.L1236
.L879:
	leaq	-5024(%rbp), %rax
	cmpq	$0, -5152(%rbp)
	movq	%rax, -7696(%rbp)
	leaq	-4832(%rbp), %rax
	movq	%rax, -7712(%rbp)
	jne	.L1237
.L882:
	leaq	-4064(%rbp), %rax
	cmpq	$0, -4960(%rbp)
	movq	%rax, -7536(%rbp)
	jne	.L1238
.L885:
	leaq	-4640(%rbp), %rax
	cmpq	$0, -4768(%rbp)
	movq	%rax, -7776(%rbp)
	leaq	-4448(%rbp), %rax
	movq	%rax, -7792(%rbp)
	jne	.L1239
	cmpq	$0, -4576(%rbp)
	jne	.L1240
.L890:
	leaq	-4256(%rbp), %rax
	cmpq	$0, -4384(%rbp)
	movq	%rax, -7808(%rbp)
	jne	.L1241
.L892:
	leaq	-3872(%rbp), %rax
	cmpq	$0, -4192(%rbp)
	movq	%rax, -7744(%rbp)
	jne	.L1242
.L894:
	cmpq	$0, -4000(%rbp)
	jne	.L1243
.L896:
	leaq	-3680(%rbp), %rax
	cmpq	$0, -3808(%rbp)
	movq	%rax, -7816(%rbp)
	jne	.L1244
.L898:
	leaq	-3488(%rbp), %rax
	cmpq	$0, -3616(%rbp)
	movq	%rax, -7824(%rbp)
	leaq	-3296(%rbp), %rax
	movq	%rax, -7840(%rbp)
	jne	.L1245
	cmpq	$0, -3424(%rbp)
	jne	.L1246
.L904:
	leaq	-3104(%rbp), %rax
	cmpq	$0, -3232(%rbp)
	movq	%rax, -7848(%rbp)
	leaq	-2528(%rbp), %rax
	movq	%rax, -7872(%rbp)
	jne	.L1247
.L906:
	leaq	-2720(%rbp), %rax
	cmpq	$0, -3040(%rbp)
	movq	%rax, -7856(%rbp)
	leaq	-2912(%rbp), %rax
	movq	%rax, -7600(%rbp)
	jne	.L1248
	cmpq	$0, -2848(%rbp)
	jne	.L1249
.L914:
	leaq	-1952(%rbp), %rax
	cmpq	$0, -2656(%rbp)
	movq	%rax, -7552(%rbp)
	jne	.L1250
.L917:
	leaq	-2336(%rbp), %rax
	cmpq	$0, -2464(%rbp)
	movq	%rax, -7616(%rbp)
	jne	.L1251
	cmpq	$0, -2272(%rbp)
	jne	.L1252
.L924:
	cmpq	$0, -2080(%rbp)
	jne	.L1253
.L927:
	cmpq	$0, -1888(%rbp)
	jne	.L1254
.L929:
	leaq	-1184(%rbp), %rax
	cmpq	$0, -1696(%rbp)
	movq	%rax, -7560(%rbp)
	jne	.L1255
	cmpq	$0, -1504(%rbp)
	jne	.L1256
.L934:
	cmpq	$0, -1312(%rbp)
	jne	.L1257
.L937:
	cmpq	$0, -1120(%rbp)
	jne	.L1258
.L943:
	leaq	-800(%rbp), %rax
	cmpq	$0, -928(%rbp)
	movq	%rax, -7888(%rbp)
	jne	.L1259
.L945:
	cmpq	$0, -736(%rbp)
	leaq	-416(%rbp), %r12
	jne	.L1260
	cmpq	$0, -544(%rbp)
	jne	.L1261
.L950:
	movq	-7904(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -7248(%rbp)
	movaps	%xmm0, -7264(%rbp)
	call	_Znwm@PLT
	movl	$2056, %ecx
	movq	%r12, %rdi
	movq	%r13, %rsi
	movl	$117966599, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -7264(%rbp)
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7264(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L952
	call	_ZdlPv@PLT
.L952:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	40(%rax), %r13
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7504(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7888(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7760(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7560(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7736(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L953
	call	_ZdlPv@PLT
.L953:
	movq	-1360(%rbp), %rbx
	movq	-1368(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L954
	.p2align 4,,10
	.p2align 3
.L958:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L955
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L958
.L956:
	movq	-1368(%rbp), %r12
.L954:
	testq	%r12, %r12
	je	.L959
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L959:
	movq	-7728(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L960
	call	_ZdlPv@PLT
.L960:
	movq	-1552(%rbp), %rbx
	movq	-1560(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L961
	.p2align 4,,10
	.p2align 3
.L965:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L962
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L965
.L963:
	movq	-1560(%rbp), %r12
.L961:
	testq	%r12, %r12
	je	.L966
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L966:
	movq	-7488(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1728(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L967
	call	_ZdlPv@PLT
.L967:
	movq	-1744(%rbp), %rbx
	movq	-1752(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L968
	.p2align 4,,10
	.p2align 3
.L972:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L969
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L972
.L970:
	movq	-1752(%rbp), %r12
.L968:
	testq	%r12, %r12
	je	.L973
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L973:
	movq	-7552(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7720(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L974
	call	_ZdlPv@PLT
.L974:
	movq	-2128(%rbp), %rbx
	movq	-2136(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L975
	.p2align 4,,10
	.p2align 3
.L979:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L976
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L979
.L977:
	movq	-2136(%rbp), %r12
.L975:
	testq	%r12, %r12
	je	.L980
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L980:
	movq	-7616(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7872(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7856(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7600(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7848(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7840(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7824(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7816(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7744(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7536(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7808(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7792(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7776(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7712(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7696(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7680(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7520(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7672(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7664(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7584(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7576(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7632(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7648(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7624(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7568(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7896(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1262
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L976:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L979
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L969:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L972
	jmp	.L970
	.p2align 4,,10
	.p2align 3
.L955:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L958
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L962:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L965
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L1227:
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	-224(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	-219(%rbp), %rdx
	movaps	%xmm0, -7264(%rbp)
	movl	$117966599, -224(%rbp)
	movb	$8, -220(%rbp)
	movq	$0, -7248(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7896(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7264(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L849
	call	_ZdlPv@PLT
.L849:
	movq	(%rbx), %rax
	movl	$74, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	32(%rax), %r12
	movq	%rcx, -7520(%rbp)
	movq	24(%rax), %rcx
	movq	%rbx, -7504(%rbp)
	movq	16(%rax), %rbx
	movq	%rcx, -7536(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7480(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$75, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -7568(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7480(%rbp), %rdi
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal13Cast5ATSmi_83EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %xmm7
	pxor	%xmm0, %xmm0
	movq	-7504(%rbp), %xmm4
	leaq	-7296(%rbp), %r12
	movq	%rbx, %xmm6
	movq	%r14, %rsi
	movq	%rax, -168(%rbp)
	movhps	-7568(%rbp), %xmm7
	movhps	-7536(%rbp), %xmm6
	movq	%r12, %rdi
	movaps	%xmm0, -7296(%rbp)
	movhps	-7520(%rbp), %xmm4
	leaq	-160(%rbp), %rdx
	movaps	%xmm7, -7648(%rbp)
	movaps	%xmm6, -7536(%rbp)
	movaps	%xmm4, -7504(%rbp)
	movaps	%xmm4, -224(%rbp)
	movaps	%xmm6, -208(%rbp)
	movaps	%xmm7, -192(%rbp)
	movq	%rbx, -176(%rbp)
	movq	$0, -7280(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-6752(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -7624(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7296(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L850
	call	_ZdlPv@PLT
.L850:
	movq	-7576(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-6944(%rbp), %rax
	cmpq	$0, -7256(%rbp)
	movq	%rax, -7568(%rbp)
	jne	.L1263
.L851:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L848
	.p2align 4,,10
	.p2align 3
.L1230:
	movq	-7584(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1544, %eax
	pxor	%xmm0, %xmm0
	leaq	-224(%rbp), %rsi
	leaq	-218(%rbp), %rdx
	movq	%r13, %rdi
	movw	%ax, -220(%rbp)
	movaps	%xmm0, -7264(%rbp)
	movl	$117966599, -224(%rbp)
	movq	$0, -7248(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7648(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7264(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L860
	call	_ZdlPv@PLT
.L860:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	40(%rax), %rbx
	movaps	%xmm0, -7264(%rbp)
	movq	$0, -7248(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -7264(%rbp)
	leaq	-608(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	movq	%rax, -7504(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L861
	call	_ZdlPv@PLT
.L861:
	movq	-7880(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L859
	.p2align 4,,10
	.p2align 3
.L1229:
	movq	-7576(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-224(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	-216(%rbp), %rdx
	movabsq	$434603995588724487, %rax
	movaps	%xmm0, -7264(%rbp)
	movq	%rax, -224(%rbp)
	movq	$0, -7248(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7624(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7264(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L857
	call	_ZdlPv@PLT
.L857:
	movq	(%rbx), %rax
	leaq	-168(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movdqu	32(%rax), %xmm0
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm1
	movq	56(%rax), %rax
	movq	$0, -7248(%rbp)
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -176(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -7264(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-6368(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -7632(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L858
	call	_ZdlPv@PLT
.L858:
	movq	-7664(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L856
	.p2align 4,,10
	.p2align 3
.L1228:
	movq	-7632(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-224(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1544, %eax
	movq	%r14, %rsi
	movq	%r13, %rdi
	pxor	%xmm0, %xmm0
	leaq	-217(%rbp), %rdx
	movw	%ax, -220(%rbp)
	movaps	%xmm0, -7264(%rbp)
	movl	$117966599, -224(%rbp)
	movb	$8, -218(%rbp)
	movq	$0, -7248(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7568(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7264(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L854
	call	_ZdlPv@PLT
.L854:
	movq	(%rbx), %rax
	leaq	-176(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm1
	movdqu	32(%rax), %xmm7
	movq	$0, -7248(%rbp)
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm0, -7264(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-6560(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -7648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L855
	call	_ZdlPv@PLT
.L855:
	movq	-7584(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L1231:
	movq	-7664(%rbp), %rsi
	movq	%r15, %rdi
	movl	$1544, %r14d
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -7248(%rbp)
	movaps	%xmm0, -7264(%rbp)
	call	_Znwm@PLT
	movq	-7632(%rbp), %rdi
	movq	%r13, %rsi
	movl	$117966599, (%rax)
	leaq	7(%rax), %rdx
	movw	%r14w, 4(%rax)
	movb	$6, 6(%rax)
	movq	%rax, -7264(%rbp)
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7264(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L863
	call	_ZdlPv@PLT
.L863:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	16(%rax), %rcx
	movq	8(%rax), %r14
	movq	(%rax), %rbx
	movq	48(%rax), %r12
	movq	%rsi, -7536(%rbp)
	movq	32(%rax), %rsi
	movq	%rdx, -7584(%rbp)
	movl	$76, %edx
	movq	%rcx, -7520(%rbp)
	movq	%rsi, -7576(%rbp)
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	-7480(%rbp), %rdi
	call	_ZN2v88internal23Cast13ATFastJSArray_135EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm7
	movq	%r12, %xmm5
	movq	-7576(%rbp), %xmm3
	punpcklqdq	%xmm7, %xmm5
	movq	%r14, %xmm4
	leaq	-7296(%rbp), %r12
	movq	-7520(%rbp), %xmm7
	movq	%rbx, %xmm6
	leaq	-224(%rbp), %r14
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movhps	-7584(%rbp), %xmm3
	punpcklqdq	%xmm4, %xmm6
	movq	%r14, %rsi
	movq	%rax, -160(%rbp)
	movhps	-7536(%rbp), %xmm7
	leaq	-152(%rbp), %rdx
	movaps	%xmm5, -7696(%rbp)
	movaps	%xmm3, -7664(%rbp)
	movaps	%xmm7, -7536(%rbp)
	movaps	%xmm6, -7520(%rbp)
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm0, -7296(%rbp)
	movq	$0, -7280(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-5984(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -7584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7296(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L864
	call	_ZdlPv@PLT
.L864:
	movq	-7600(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-6176(%rbp), %rax
	cmpq	$0, -7256(%rbp)
	movq	%rax, -7576(%rbp)
	jne	.L1264
.L865:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L862
	.p2align 4,,10
	.p2align 3
.L1234:
	movq	-7616(%rbp), %rsi
	movq	%r15, %rdi
	movl	$1544, %r12d
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -7248(%rbp)
	movaps	%xmm0, -7264(%rbp)
	call	_Znwm@PLT
	movq	-7664(%rbp), %rdi
	movq	%r13, %rsi
	movl	$117966599, (%rax)
	leaq	7(%rax), %rdx
	movw	%r12w, 4(%rax)
	movb	$6, 6(%rax)
	movq	%rax, -7264(%rbp)
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7264(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L874
	call	_ZdlPv@PLT
.L874:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	40(%rax), %rbx
	movaps	%xmm0, -7264(%rbp)
	movq	$0, -7248(%rbp)
	call	_Znwm@PLT
	movq	-7504(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -7264(%rbp)
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L875
	call	_ZdlPv@PLT
.L875:
	movq	-7880(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L1233:
	movq	-7600(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-224(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	-215(%rbp), %rdx
	movaps	%xmm0, -7264(%rbp)
	movabsq	$506098639673231111, %rax
	movq	%rax, -224(%rbp)
	movb	$7, -216(%rbp)
	movq	$0, -7248(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7584(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7264(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L871
	call	_ZdlPv@PLT
.L871:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	24(%rax), %rdi
	movq	32(%rax), %rsi
	movq	48(%rax), %rdx
	movq	(%rax), %r10
	movq	8(%rax), %r9
	movq	16(%rax), %r8
	movq	40(%rax), %rcx
	movq	64(%rax), %rax
	movq	%rdi, -200(%rbp)
	movq	%r13, %rdi
	movq	%rsi, -192(%rbp)
	movq	%r14, %rsi
	movq	%rdx, -176(%rbp)
	leaq	-160(%rbp), %rdx
	movq	%r10, -224(%rbp)
	movq	%r9, -216(%rbp)
	movq	%r8, -208(%rbp)
	movq	%rcx, -184(%rbp)
	movq	%rax, -168(%rbp)
	movaps	%xmm0, -7264(%rbp)
	movq	$0, -7248(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-5600(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -7672(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L872
	call	_ZdlPv@PLT
.L872:
	movq	-7680(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L870
	.p2align 4,,10
	.p2align 3
.L1232:
	movq	-7672(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-224(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	-216(%rbp), %rdx
	movabsq	$506098639673231111, %rax
	movaps	%xmm0, -7264(%rbp)
	movq	%rax, -224(%rbp)
	movq	$0, -7248(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7576(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7264(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L868
	call	_ZdlPv@PLT
.L868:
	movq	(%rbx), %rax
	leaq	-168(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movdqu	32(%rax), %xmm0
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm1
	movq	48(%rax), %rax
	movq	$0, -7248(%rbp)
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -176(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -7264(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-5792(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -7664(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L869
	call	_ZdlPv@PLT
.L869:
	movq	-7616(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L1235:
	movq	-7680(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-224(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-216(%rbp), %rdx
	movabsq	$506098639673231111, %rax
	movaps	%xmm0, -7264(%rbp)
	movq	%rax, -224(%rbp)
	movq	$0, -7248(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7672(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7264(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L877
	call	_ZdlPv@PLT
.L877:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	%rsi, -7760(%rbp)
	movq	32(%rax), %rsi
	movq	%rdx, -8048(%rbp)
	movq	48(%rax), %rdx
	movq	%rcx, -7696(%rbp)
	movq	16(%rax), %rcx
	movq	%rbx, -7680(%rbp)
	movq	56(%rax), %rbx
	movq	%rsi, -7872(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -8064(%rbp)
	movl	$77, %edx
	movq	%rcx, -7712(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7480(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal25NewFastJSArrayWitness_236EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7JSArrayEEE@PLT
	movq	-7240(%rbp), %rdi
	movq	-7224(%rbp), %rax
	movl	$80, %edx
	leaq	.LC2(%rip), %rsi
	movq	-7248(%rbp), %r12
	movq	-7264(%rbp), %r14
	movq	%rdi, -7600(%rbp)
	movq	-7256(%rbp), %rdi
	movq	%rax, -7520(%rbp)
	movq	-7232(%rbp), %rax
	movq	%rdi, -7616(%rbp)
	movq	%r15, %rdi
	movq	%rax, -7536(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm7
	movl	$112, %edi
	movq	-7680(%rbp), %xmm0
	movq	$0, -7248(%rbp)
	movhps	-7696(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-7712(%rbp), %xmm0
	movhps	-7760(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-7872(%rbp), %xmm0
	movhps	-8048(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-8064(%rbp), %xmm0
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	%r14, %xmm0
	movhps	-7616(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%r12, %xmm0
	movhps	-7600(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-7536(%rbp), %xmm0
	movhps	-7520(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7264(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	112(%rax), %rdx
	movq	%rax, -7264(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-208(%rbp), %xmm7
	movq	%rdx, -7248(%rbp)
	movups	%xmm7, 16(%rax)
	movdqa	-192(%rbp), %xmm7
	movq	%rdx, -7256(%rbp)
	movups	%xmm7, 32(%rax)
	movdqa	-176(%rbp), %xmm7
	movups	%xmm7, 48(%rax)
	movdqa	-160(%rbp), %xmm7
	movups	%xmm7, 64(%rax)
	movdqa	-144(%rbp), %xmm7
	movups	%xmm7, 80(%rax)
	movdqa	-128(%rbp), %xmm7
	movups	%xmm7, 96(%rax)
	leaq	-5408(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -7520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L878
	call	_ZdlPv@PLT
.L878:
	movq	-7888(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L1236:
	movq	-7888(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	movq	$0, -7312(%rbp)
	movq	$0, -7296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7296(%rbp), %rax
	movq	-7520(%rbp), %rdi
	pushq	%rax
	leaq	-7312(%rbp), %rax
	leaq	-7392(%rbp), %rcx
	pushq	%rax
	leaq	-7320(%rbp), %rax
	leaq	-7376(%rbp), %r9
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7384(%rbp), %r8
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7400(%rbp), %rdx
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7408(%rbp), %rsi
	pushq	%rax
	leaq	-7352(%rbp), %rax
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	movq	-7480(%rbp), %rsi
	addq	$80, %rsp
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7368(%rbp), %r14
	movq	-7360(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$112, %edi
	movq	-7312(%rbp), %xmm4
	movq	-7328(%rbp), %xmm5
	movaps	%xmm0, -7264(%rbp)
	movq	-7344(%rbp), %xmm3
	movq	-7360(%rbp), %xmm7
	movhps	-7296(%rbp), %xmm4
	movq	-7376(%rbp), %xmm6
	movq	$0, -7248(%rbp)
	movq	-7392(%rbp), %xmm2
	movhps	-7320(%rbp), %xmm5
	movaps	%xmm4, -128(%rbp)
	movq	-7408(%rbp), %xmm1
	movhps	-7336(%rbp), %xmm3
	movhps	-7352(%rbp), %xmm7
	movhps	-7368(%rbp), %xmm6
	movaps	%xmm4, -7712(%rbp)
	movhps	-7384(%rbp), %xmm2
	movhps	-7400(%rbp), %xmm1
	movaps	%xmm5, -7536(%rbp)
	movaps	%xmm3, -7696(%rbp)
	movaps	%xmm7, -7616(%rbp)
	movaps	%xmm6, -7600(%rbp)
	movaps	%xmm2, -7872(%rbp)
	movaps	%xmm1, -7760(%rbp)
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm2, -208(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-208(%rbp), %xmm5
	movdqa	-176(%rbp), %xmm4
	movq	-7680(%rbp), %rdi
	leaq	112(%rax), %rdx
	movq	%rax, -7264(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-192(%rbp), %xmm7
	movups	%xmm5, 16(%rax)
	movdqa	-160(%rbp), %xmm5
	movups	%xmm7, 32(%rax)
	movdqa	-144(%rbp), %xmm7
	movups	%xmm4, 48(%rax)
	movdqa	-128(%rbp), %xmm4
	movups	%xmm5, 64(%rax)
	movups	%xmm7, 80(%rax)
	movups	%xmm4, 96(%rax)
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L880
	call	_ZdlPv@PLT
.L880:
	movdqa	-7760(%rbp), %xmm5
	movdqa	-7872(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movl	$112, %edi
	movdqa	-7600(%rbp), %xmm4
	movdqa	-7696(%rbp), %xmm6
	movaps	%xmm0, -7264(%rbp)
	movdqa	-7536(%rbp), %xmm3
	movaps	%xmm5, -224(%rbp)
	movdqa	-7616(%rbp), %xmm5
	movaps	%xmm7, -208(%rbp)
	movdqa	-7712(%rbp), %xmm7
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm7, -128(%rbp)
	movq	$0, -7248(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-208(%rbp), %xmm5
	movdqa	-192(%rbp), %xmm6
	movdqa	-176(%rbp), %xmm3
	leaq	112(%rax), %rdx
	movq	%rax, -7264(%rbp)
	movups	%xmm4, (%rax)
	movdqa	-160(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm4
	movups	%xmm5, 16(%rax)
	movdqa	-128(%rbp), %xmm5
	movups	%xmm6, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm4, 80(%rax)
	movups	%xmm5, 96(%rax)
	leaq	-992(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	movq	%rax, -7760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L881
	call	_ZdlPv@PLT
.L881:
	movq	-7920(%rbp), %rcx
	movq	-7776(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L1237:
	movq	-7776(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	movq	$0, -7312(%rbp)
	movq	$0, -7296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7296(%rbp), %rax
	movq	-7680(%rbp), %rdi
	pushq	%rax
	leaq	-7312(%rbp), %rax
	leaq	-7376(%rbp), %r9
	pushq	%rax
	leaq	-7320(%rbp), %rax
	leaq	-7384(%rbp), %r8
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7392(%rbp), %rcx
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7400(%rbp), %rdx
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7408(%rbp), %rsi
	pushq	%rax
	leaq	-7352(%rbp), %rax
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	addq	$80, %rsp
	movl	$3097, %edx
	movq	%r15, %rdi
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-7480(%rbp), %rbx
	movq	%r13, %rdi
	movq	%rax, %r12
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	movq	-7344(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE(%rip), %esi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7328(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$112, %edi
	movq	-7312(%rbp), %xmm4
	movq	-7328(%rbp), %xmm5
	movaps	%xmm0, -7264(%rbp)
	movq	-7344(%rbp), %xmm3
	movq	-7360(%rbp), %xmm7
	movhps	-7296(%rbp), %xmm4
	movq	-7376(%rbp), %xmm6
	movq	$0, -7248(%rbp)
	movq	-7392(%rbp), %xmm2
	movhps	-7320(%rbp), %xmm5
	movaps	%xmm4, -128(%rbp)
	movq	-7408(%rbp), %xmm1
	movhps	-7336(%rbp), %xmm3
	movhps	-7352(%rbp), %xmm7
	movhps	-7368(%rbp), %xmm6
	movaps	%xmm4, -7776(%rbp)
	movhps	-7384(%rbp), %xmm2
	movhps	-7400(%rbp), %xmm1
	movaps	%xmm5, -7600(%rbp)
	movaps	%xmm3, -7712(%rbp)
	movaps	%xmm7, -7536(%rbp)
	movaps	%xmm6, -8048(%rbp)
	movaps	%xmm2, -7872(%rbp)
	movaps	%xmm1, -7616(%rbp)
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm2, -208(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-208(%rbp), %xmm3
	movdqa	-192(%rbp), %xmm7
	movdqa	-176(%rbp), %xmm4
	leaq	112(%rax), %rdx
	movq	%rax, -7264(%rbp)
	movdqa	-160(%rbp), %xmm5
	movups	%xmm6, (%rax)
	movdqa	-144(%rbp), %xmm6
	movups	%xmm3, 16(%rax)
	movdqa	-128(%rbp), %xmm3
	movq	-7696(%rbp), %rdi
	movups	%xmm7, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm5, 64(%rax)
	movups	%xmm6, 80(%rax)
	movups	%xmm3, 96(%rax)
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L883
	call	_ZdlPv@PLT
.L883:
	movdqa	-7616(%rbp), %xmm7
	movdqa	-7872(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movl	$112, %edi
	movdqa	-8048(%rbp), %xmm5
	movdqa	-7536(%rbp), %xmm6
	movaps	%xmm0, -7264(%rbp)
	movdqa	-7712(%rbp), %xmm3
	movaps	%xmm7, -224(%rbp)
	movdqa	-7600(%rbp), %xmm7
	movaps	%xmm4, -208(%rbp)
	movdqa	-7776(%rbp), %xmm4
	movaps	%xmm5, -192(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movq	$0, -7248(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-208(%rbp), %xmm6
	movdqa	-192(%rbp), %xmm3
	movdqa	-176(%rbp), %xmm7
	leaq	112(%rax), %rdx
	movq	%rax, -7264(%rbp)
	movups	%xmm5, (%rax)
	movdqa	-160(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm5
	movups	%xmm6, 16(%rax)
	movdqa	-128(%rbp), %xmm6
	movups	%xmm3, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm5, 80(%rax)
	movups	%xmm6, 96(%rax)
	leaq	-4832(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	movq	%rax, -7712(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L884
	call	_ZdlPv@PLT
.L884:
	movq	-7808(%rbp), %rcx
	movq	-7792(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L882
	.p2align 4,,10
	.p2align 3
.L1238:
	movq	-7792(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	movq	$0, -7312(%rbp)
	movq	$0, -7296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7296(%rbp), %rax
	movq	-7696(%rbp), %rdi
	pushq	%rax
	leaq	-7312(%rbp), %rax
	leaq	-7392(%rbp), %rcx
	pushq	%rax
	leaq	-7320(%rbp), %rax
	leaq	-7400(%rbp), %rdx
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7408(%rbp), %rsi
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7376(%rbp), %r9
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7384(%rbp), %r8
	pushq	%rax
	leaq	-7352(%rbp), %rax
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	addq	$80, %rsp
	movl	$112, %edi
	movq	-7312(%rbp), %xmm0
	movq	-7328(%rbp), %xmm1
	movq	-7344(%rbp), %xmm2
	movq	$0, -7248(%rbp)
	movq	-7360(%rbp), %xmm3
	movhps	-7296(%rbp), %xmm0
	movq	-7376(%rbp), %xmm4
	movq	-7392(%rbp), %xmm5
	movhps	-7320(%rbp), %xmm1
	movaps	%xmm0, -128(%rbp)
	movq	-7408(%rbp), %xmm6
	movhps	-7336(%rbp), %xmm2
	movhps	-7352(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -144(%rbp)
	movhps	-7368(%rbp), %xmm4
	movhps	-7384(%rbp), %xmm5
	movhps	-7400(%rbp), %xmm6
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm0, -7264(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-208(%rbp), %xmm7
	movdqa	-192(%rbp), %xmm4
	movdqa	-176(%rbp), %xmm5
	leaq	112(%rax), %rdx
	movq	%rax, -7264(%rbp)
	movdqa	-160(%rbp), %xmm6
	movups	%xmm3, (%rax)
	movdqa	-144(%rbp), %xmm3
	movups	%xmm7, 16(%rax)
	movdqa	-128(%rbp), %xmm7
	movq	-7536(%rbp), %rdi
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	movups	%xmm3, 80(%rax)
	movups	%xmm7, 96(%rax)
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L886
	call	_ZdlPv@PLT
.L886:
	movq	-7552(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L1239:
	movq	-7808(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	movq	$0, -7312(%rbp)
	movq	$0, -7296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7296(%rbp), %rax
	movq	-7712(%rbp), %rdi
	pushq	%rax
	leaq	-7312(%rbp), %rax
	leaq	-7392(%rbp), %rcx
	pushq	%rax
	leaq	-7320(%rbp), %rax
	leaq	-7376(%rbp), %r9
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7384(%rbp), %r8
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7400(%rbp), %rdx
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7408(%rbp), %rsi
	pushq	%rax
	leaq	-7352(%rbp), %rax
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	addq	$80, %rsp
	movl	$3104, %edx
	movq	%r15, %rdi
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7480(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler32IsNoElementsProtectorCellInvalidEv@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$112, %edi
	movq	-7312(%rbp), %xmm4
	movq	-7328(%rbp), %xmm5
	movaps	%xmm0, -7264(%rbp)
	movq	-7344(%rbp), %xmm3
	movq	-7360(%rbp), %xmm7
	movhps	-7296(%rbp), %xmm4
	movq	-7376(%rbp), %xmm6
	movq	$0, -7248(%rbp)
	movq	-7392(%rbp), %xmm2
	movhps	-7320(%rbp), %xmm5
	movaps	%xmm4, -128(%rbp)
	movq	-7408(%rbp), %xmm1
	movhps	-7336(%rbp), %xmm3
	movhps	-7352(%rbp), %xmm7
	movhps	-7368(%rbp), %xmm6
	movaps	%xmm4, -7872(%rbp)
	movhps	-7384(%rbp), %xmm2
	movhps	-7400(%rbp), %xmm1
	movaps	%xmm5, -7808(%rbp)
	movaps	%xmm3, -7616(%rbp)
	movaps	%xmm7, -7600(%rbp)
	movaps	%xmm6, -8048(%rbp)
	movaps	%xmm2, -7792(%rbp)
	movaps	%xmm1, -8064(%rbp)
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm2, -208(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-208(%rbp), %xmm5
	movdqa	-192(%rbp), %xmm6
	movdqa	-176(%rbp), %xmm3
	leaq	112(%rax), %rdx
	movq	%rax, -7264(%rbp)
	movdqa	-160(%rbp), %xmm7
	movups	%xmm4, (%rax)
	movdqa	-144(%rbp), %xmm4
	movups	%xmm5, 16(%rax)
	movdqa	-128(%rbp), %xmm5
	movq	-7776(%rbp), %rdi
	movups	%xmm6, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm4, 80(%rax)
	movups	%xmm5, 96(%rax)
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L888
	call	_ZdlPv@PLT
.L888:
	movdqa	-8064(%rbp), %xmm6
	movdqa	-7792(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movl	$112, %edi
	movdqa	-8048(%rbp), %xmm7
	movdqa	-7600(%rbp), %xmm4
	movaps	%xmm0, -7264(%rbp)
	movdqa	-7616(%rbp), %xmm5
	movaps	%xmm6, -224(%rbp)
	movdqa	-7808(%rbp), %xmm6
	movaps	%xmm3, -208(%rbp)
	movdqa	-7872(%rbp), %xmm3
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movq	$0, -7248(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-208(%rbp), %xmm4
	movdqa	-192(%rbp), %xmm5
	movdqa	-176(%rbp), %xmm6
	leaq	112(%rax), %rdx
	movq	%rax, -7264(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-160(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm7
	movups	%xmm4, 16(%rax)
	movdqa	-128(%rbp), %xmm4
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm3, 64(%rax)
	movups	%xmm7, 80(%rax)
	movups	%xmm4, 96(%rax)
	leaq	-4448(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	movq	%rax, -7792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L889
	call	_ZdlPv@PLT
.L889:
	movq	-7840(%rbp), %rcx
	movq	-7744(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -4576(%rbp)
	je	.L890
.L1240:
	movq	-7744(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	movq	$0, -7312(%rbp)
	movq	$0, -7296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7296(%rbp), %rax
	movq	-7776(%rbp), %rdi
	pushq	%rax
	leaq	-7312(%rbp), %rax
	leaq	-7392(%rbp), %rcx
	pushq	%rax
	leaq	-7320(%rbp), %rax
	leaq	-7400(%rbp), %rdx
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7408(%rbp), %rsi
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7376(%rbp), %r9
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7384(%rbp), %r8
	pushq	%rax
	leaq	-7352(%rbp), %rax
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	addq	$80, %rsp
	movl	$112, %edi
	movq	-7312(%rbp), %xmm0
	movq	-7328(%rbp), %xmm1
	movq	-7344(%rbp), %xmm2
	movq	$0, -7248(%rbp)
	movq	-7360(%rbp), %xmm3
	movhps	-7296(%rbp), %xmm0
	movq	-7376(%rbp), %xmm4
	movq	-7392(%rbp), %xmm5
	movhps	-7320(%rbp), %xmm1
	movaps	%xmm0, -128(%rbp)
	movq	-7408(%rbp), %xmm6
	movhps	-7336(%rbp), %xmm2
	movhps	-7352(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -144(%rbp)
	movhps	-7368(%rbp), %xmm4
	movhps	-7384(%rbp), %xmm5
	movhps	-7400(%rbp), %xmm6
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm0, -7264(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-208(%rbp), %xmm6
	movdqa	-192(%rbp), %xmm3
	movdqa	-176(%rbp), %xmm7
	leaq	112(%rax), %rdx
	movq	%rax, -7264(%rbp)
	movdqa	-160(%rbp), %xmm4
	movups	%xmm5, (%rax)
	movdqa	-144(%rbp), %xmm5
	movups	%xmm6, 16(%rax)
	movdqa	-128(%rbp), %xmm6
	movq	-7536(%rbp), %rdi
	movups	%xmm3, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm5, 80(%rax)
	movups	%xmm6, 96(%rax)
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L891
	call	_ZdlPv@PLT
.L891:
	movq	-7552(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L1241:
	movq	-7840(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	movq	$0, -7312(%rbp)
	movq	$0, -7296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7296(%rbp), %rax
	movq	-7792(%rbp), %rdi
	pushq	%rax
	leaq	-7312(%rbp), %rax
	leaq	-7392(%rbp), %rcx
	pushq	%rax
	leaq	-7320(%rbp), %rax
	leaq	-7376(%rbp), %r9
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7384(%rbp), %r8
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7400(%rbp), %rdx
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7408(%rbp), %rsi
	pushq	%rax
	leaq	-7352(%rbp), %rax
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	addq	$80, %rsp
	movl	$3105, %edx
	movq	%r15, %rdi
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$81, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	movq	-7344(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7344(%rbp), %rax
	movl	$112, %edi
	movq	-7360(%rbp), %xmm0
	movq	-7376(%rbp), %xmm1
	movq	%rbx, -152(%rbp)
	movhps	-7352(%rbp), %xmm0
	movq	%rax, -160(%rbp)
	movq	-7392(%rbp), %xmm2
	movaps	%xmm0, -176(%rbp)
	movq	-7328(%rbp), %xmm0
	movq	-7408(%rbp), %xmm3
	movhps	-7368(%rbp), %xmm1
	movhps	-7384(%rbp), %xmm2
	movaps	%xmm1, -192(%rbp)
	movhps	-7320(%rbp), %xmm0
	movhps	-7400(%rbp), %xmm3
	movaps	%xmm2, -208(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	-7312(%rbp), %xmm0
	movaps	%xmm3, -224(%rbp)
	movhps	-7296(%rbp), %xmm0
	movq	$0, -7248(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7264(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-208(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm1
	movq	-7808(%rbp), %rdi
	leaq	112(%rax), %rdx
	movq	%rax, -7264(%rbp)
	movdqa	-192(%rbp), %xmm4
	movdqa	-176(%rbp), %xmm5
	movups	%xmm3, (%rax)
	movdqa	-160(%rbp), %xmm6
	movdqa	-144(%rbp), %xmm3
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	movups	%xmm3, 80(%rax)
	movups	%xmm1, 96(%rax)
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L893
	call	_ZdlPv@PLT
.L893:
	movq	-7816(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L1243:
	movq	-7552(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	movq	$0, -7312(%rbp)
	movq	$0, -7296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7296(%rbp), %rax
	movq	-7536(%rbp), %rdi
	pushq	%rax
	leaq	-7312(%rbp), %rax
	leaq	-7392(%rbp), %rcx
	pushq	%rax
	leaq	-7320(%rbp), %rax
	leaq	-7400(%rbp), %rdx
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7408(%rbp), %rsi
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7376(%rbp), %r9
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7384(%rbp), %r8
	pushq	%rax
	leaq	-7352(%rbp), %rax
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	pxor	%xmm0, %xmm0
	addq	$80, %rsp
	movq	-7368(%rbp), %rbx
	movl	$8, %edi
	movaps	%xmm0, -7264(%rbp)
	movq	$0, -7248(%rbp)
	call	_Znwm@PLT
	movq	-7504(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -7264(%rbp)
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L897
	call	_ZdlPv@PLT
.L897:
	movq	-7880(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L896
	.p2align 4,,10
	.p2align 3
.L1242:
	movq	-7816(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	movq	$0, -7312(%rbp)
	movq	$0, -7296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7296(%rbp), %rax
	movq	-7808(%rbp), %rdi
	pushq	%rax
	leaq	-7312(%rbp), %rax
	leaq	-7392(%rbp), %rcx
	pushq	%rax
	leaq	-7320(%rbp), %rax
	leaq	-7400(%rbp), %rdx
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7408(%rbp), %rsi
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7376(%rbp), %r9
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7384(%rbp), %r8
	pushq	%rax
	leaq	-7352(%rbp), %rax
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	addq	$80, %rsp
	movl	$112, %edi
	movq	-7312(%rbp), %xmm0
	movq	-7328(%rbp), %xmm1
	movq	-7344(%rbp), %xmm2
	movq	$0, -7248(%rbp)
	movq	-7360(%rbp), %xmm3
	movhps	-7296(%rbp), %xmm0
	movq	-7376(%rbp), %xmm4
	movq	-7392(%rbp), %xmm5
	movhps	-7320(%rbp), %xmm1
	movaps	%xmm0, -128(%rbp)
	movq	-7408(%rbp), %xmm6
	movhps	-7336(%rbp), %xmm2
	movhps	-7352(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -144(%rbp)
	movhps	-7368(%rbp), %xmm4
	movhps	-7384(%rbp), %xmm5
	movhps	-7400(%rbp), %xmm6
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm0, -7264(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-208(%rbp), %xmm4
	movdqa	-192(%rbp), %xmm5
	movdqa	-176(%rbp), %xmm6
	leaq	112(%rax), %rdx
	movq	%rax, -7264(%rbp)
	movdqa	-160(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm1
	movups	%xmm7, (%rax)
	movdqa	-128(%rbp), %xmm7
	movq	-7744(%rbp), %rdi
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm3, 64(%rax)
	movups	%xmm1, 80(%rax)
	movups	%xmm7, 96(%rax)
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L895
	call	_ZdlPv@PLT
.L895:
	movq	-7824(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L1245:
	movq	-7848(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC7(%rip), %xmm0
	leaq	-224(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-208(%rbp), %rdx
	movq	$0, -7248(%rbp)
	movaps	%xmm0, -224(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7264(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7816(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7264(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L901
	call	_ZdlPv@PLT
.L901:
	movq	(%rbx), %rax
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	8(%rax), %rcx
	movq	72(%rax), %r11
	movq	88(%rax), %r10
	movq	%rsi, -7824(%rbp)
	movq	32(%rax), %rsi
	movq	(%rax), %rbx
	movq	%rdx, -7848(%rbp)
	movq	%rdi, -8048(%rbp)
	movq	48(%rax), %rdx
	movq	64(%rax), %rdi
	movq	%rcx, -7600(%rbp)
	movq	%r11, -8080(%rbp)
	movq	16(%rax), %rcx
	movq	80(%rax), %r11
	movq	%r10, -8104(%rbp)
	movq	104(%rax), %r10
	movq	%rsi, -7840(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -7872(%rbp)
	movl	$84, %edx
	movq	120(%rax), %r14
	movq	%rdi, -8064(%rbp)
	movq	%r15, %rdi
	movq	112(%rax), %r12
	movq	%rcx, -7616(%rbp)
	movq	%r11, -8096(%rbp)
	movq	%r10, -8128(%rbp)
	movq	%rbx, -7552(%rbp)
	movq	96(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7480(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler21LoadFastJSArrayLengthENS0_8compiler11SloppyTNodeINS0_7JSArrayEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-7480(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler24IntPtrGreaterThanOrEqualENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm4
	pxor	%xmm0, %xmm0
	movq	-8096(%rbp), %xmm5
	movq	-8064(%rbp), %xmm3
	movhps	-8128(%rbp), %xmm4
	movq	-7872(%rbp), %xmm7
	movl	$112, %edi
	movq	-7840(%rbp), %xmm6
	movhps	-8104(%rbp), %xmm5
	movq	-7616(%rbp), %xmm2
	movaps	%xmm4, -8128(%rbp)
	movq	-7552(%rbp), %xmm1
	movhps	-8080(%rbp), %xmm3
	movhps	-8048(%rbp), %xmm7
	movaps	%xmm5, -8096(%rbp)
	movhps	-7848(%rbp), %xmm6
	movhps	-7824(%rbp), %xmm2
	movaps	%xmm3, -8064(%rbp)
	movhps	-7600(%rbp), %xmm1
	movaps	%xmm7, -7872(%rbp)
	movaps	%xmm6, -7840(%rbp)
	movaps	%xmm2, -7616(%rbp)
	movaps	%xmm1, -7552(%rbp)
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm2, -208(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm0, -7264(%rbp)
	movq	$0, -7248(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-208(%rbp), %xmm5
	movdqa	-192(%rbp), %xmm6
	movdqa	-176(%rbp), %xmm3
	leaq	112(%rax), %rdx
	movq	%rax, -7264(%rbp)
	movups	%xmm4, (%rax)
	movdqa	-160(%rbp), %xmm1
	movdqa	-128(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm7
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm1, 64(%rax)
	movups	%xmm7, 80(%rax)
	movups	%xmm4, 96(%rax)
	leaq	-3488(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	movq	%rax, -7824(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L902
	call	_ZdlPv@PLT
.L902:
	movdqa	-7552(%rbp), %xmm5
	movdqa	-7616(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$112, %edi
	movdqa	-7840(%rbp), %xmm3
	movdqa	-7872(%rbp), %xmm1
	movaps	%xmm0, -7264(%rbp)
	movdqa	-8064(%rbp), %xmm7
	movdqa	-8096(%rbp), %xmm4
	movaps	%xmm5, -224(%rbp)
	movdqa	-8128(%rbp), %xmm5
	movaps	%xmm6, -208(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movq	$0, -7248(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-208(%rbp), %xmm3
	movdqa	-192(%rbp), %xmm1
	movdqa	-176(%rbp), %xmm7
	leaq	112(%rax), %rdx
	movq	%rax, -7264(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-160(%rbp), %xmm4
	movdqa	-128(%rbp), %xmm6
	movdqa	-144(%rbp), %xmm5
	movups	%xmm3, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm5, 80(%rax)
	movups	%xmm6, 96(%rax)
	leaq	-3296(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	movq	%rax, -7840(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L903
	call	_ZdlPv@PLT
.L903:
	movq	-7936(%rbp), %rcx
	movq	-7856(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -3424(%rbp)
	je	.L904
.L1246:
	movq	-7856(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	movq	$0, -7312(%rbp)
	movq	$0, -7296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7296(%rbp), %rax
	movq	-7824(%rbp), %rdi
	pushq	%rax
	leaq	-7312(%rbp), %rax
	leaq	-7392(%rbp), %rcx
	pushq	%rax
	leaq	-7320(%rbp), %rax
	leaq	-7400(%rbp), %rdx
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7408(%rbp), %rsi
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7376(%rbp), %r9
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7384(%rbp), %r8
	pushq	%rax
	leaq	-7352(%rbp), %rax
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	pxor	%xmm0, %xmm0
	addq	$80, %rsp
	movq	-7368(%rbp), %rbx
	movl	$8, %edi
	movaps	%xmm0, -7264(%rbp)
	movq	$0, -7248(%rbp)
	call	_Znwm@PLT
	movq	-7504(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -7264(%rbp)
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L905
	call	_ZdlPv@PLT
.L905:
	movq	-7880(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L904
	.p2align 4,,10
	.p2align 3
.L1244:
	movq	-7824(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	movq	$0, -7312(%rbp)
	movq	$0, -7296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7296(%rbp), %rax
	movq	-7744(%rbp), %rdi
	pushq	%rax
	leaq	-7312(%rbp), %rax
	leaq	-7392(%rbp), %rcx
	pushq	%rax
	leaq	-7320(%rbp), %rax
	leaq	-7376(%rbp), %r9
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7384(%rbp), %r8
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7400(%rbp), %rdx
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7408(%rbp), %rsi
	pushq	%rax
	leaq	-7352(%rbp), %rax
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	addq	$80, %rsp
	movl	$84, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$3093, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-224(%rbp), %rsi
	leaq	-96(%rbp), %rdx
	movq	%r13, %rdi
	movq	-7312(%rbp), %xmm0
	movq	-7328(%rbp), %xmm1
	movq	$0, -7248(%rbp)
	movq	-7344(%rbp), %xmm2
	movq	-7360(%rbp), %xmm3
	movhps	-7296(%rbp), %xmm0
	movhps	-7320(%rbp), %xmm1
	movq	-7376(%rbp), %xmm4
	movq	-7392(%rbp), %xmm5
	movaps	%xmm0, -128(%rbp)
	movhps	-7336(%rbp), %xmm2
	movq	-7368(%rbp), %xmm0
	movq	-7408(%rbp), %xmm6
	movhps	-7352(%rbp), %xmm3
	movhps	-7368(%rbp), %xmm4
	movhps	-7384(%rbp), %xmm5
	movaps	%xmm2, -160(%rbp)
	movhps	-7336(%rbp), %xmm0
	movhps	-7400(%rbp), %xmm6
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -7264(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7816(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L899
	call	_ZdlPv@PLT
.L899:
	movq	-7848(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L898
	.p2align 4,,10
	.p2align 3
.L1247:
	movq	-7936(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	movq	$0, -7312(%rbp)
	movq	$0, -7296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7296(%rbp), %rax
	movq	-7840(%rbp), %rdi
	pushq	%rax
	leaq	-7312(%rbp), %rax
	leaq	-7392(%rbp), %rcx
	pushq	%rax
	leaq	-7320(%rbp), %rax
	leaq	-7376(%rbp), %r9
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7384(%rbp), %r8
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7400(%rbp), %rdx
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7408(%rbp), %rsi
	pushq	%rax
	leaq	-7352(%rbp), %rax
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	addq	$80, %rsp
	movl	$85, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$3110, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7400(%rbp), %rax
	pxor	%xmm2, %xmm2
	movq	-7368(%rbp), %xmm0
	movq	-7408(%rbp), %xmm1
	movq	-7384(%rbp), %rcx
	movq	-7344(%rbp), %r11
	movq	-7336(%rbp), %r10
	movq	%xmm0, -184(%rbp)
	movq	-7328(%rbp), %r9
	movq	-7296(%rbp), %r8
	movq	%xmm1, -224(%rbp)
	movq	-7392(%rbp), %rbx
	movq	-7376(%rbp), %rsi
	movq	%rax, -7552(%rbp)
	movq	-7360(%rbp), %rdx
	movq	-7312(%rbp), %r14
	movq	%rcx, -7616(%rbp)
	movq	-7352(%rbp), %rdi
	movq	%r11, -8048(%rbp)
	movq	%r10, -8064(%rbp)
	movq	-7320(%rbp), %r12
	movq	%r9, -8080(%rbp)
	movq	%r8, -8104(%rbp)
	movq	%rax, -216(%rbp)
	movq	%rcx, -200(%rbp)
	movq	%rbx, -7600(%rbp)
	movq	%rsi, -7856(%rbp)
	movq	%rdx, -7872(%rbp)
	movq	%rdi, -7936(%rbp)
	movq	%r14, -8096(%rbp)
	movq	%rbx, -208(%rbp)
	leaq	-88(%rbp), %rbx
	movq	%rsi, -192(%rbp)
	movq	%rdx, -176(%rbp)
	movq	%rbx, %rdx
	movq	%rdi, -168(%rbp)
	movq	%r13, %rdi
	movq	%r14, -128(%rbp)
	leaq	-224(%rbp), %r14
	movq	%r14, %rsi
	movq	%xmm0, -112(%rbp)
	movq	%xmm1, -104(%rbp)
	movq	%xmm1, -8112(%rbp)
	movq	%xmm0, -96(%rbp)
	movq	%xmm0, -8128(%rbp)
	movq	%r11, -160(%rbp)
	movq	%r10, -152(%rbp)
	movq	%r9, -144(%rbp)
	movq	%r8, -120(%rbp)
	movaps	%xmm2, -7264(%rbp)
	movq	%r12, -136(%rbp)
	movq	$0, -7248(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7848(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	movq	-8128(%rbp), %xmm0
	movq	-8112(%rbp), %xmm1
	testq	%rdi, %rdi
	je	.L907
	movq	%xmm0, -8112(%rbp)
	movq	%xmm1, -8128(%rbp)
	call	_ZdlPv@PLT
	movq	-8112(%rbp), %xmm0
	movq	-8128(%rbp), %xmm1
.L907:
	movdqa	%xmm1, %xmm2
	movq	%r12, %xmm3
	movdqa	%xmm0, %xmm7
	movq	%rbx, %rdx
	punpcklqdq	%xmm1, %xmm7
	movq	%xmm0, -96(%rbp)
	movq	%r14, %rsi
	movq	%r13, %rdi
	movhps	-7552(%rbp), %xmm2
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm2, -224(%rbp)
	movq	-7600(%rbp), %xmm2
	movq	$0, -7248(%rbp)
	movhps	-7616(%rbp), %xmm2
	movaps	%xmm2, -208(%rbp)
	movq	-7856(%rbp), %xmm2
	punpcklqdq	%xmm0, %xmm2
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -192(%rbp)
	movq	-7872(%rbp), %xmm2
	movaps	%xmm0, -7264(%rbp)
	movhps	-7936(%rbp), %xmm2
	movaps	%xmm2, -176(%rbp)
	movq	-8048(%rbp), %xmm2
	movhps	-8064(%rbp), %xmm2
	movaps	%xmm2, -160(%rbp)
	movq	-8080(%rbp), %xmm2
	punpcklqdq	%xmm3, %xmm2
	movaps	%xmm2, -144(%rbp)
	movq	-8096(%rbp), %xmm2
	movhps	-8104(%rbp), %xmm2
	movaps	%xmm2, -128(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2528(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -7872(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L908
	call	_ZdlPv@PLT
.L908:
	movq	-7984(%rbp), %rcx
	movq	-7952(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L1248:
	movq	-7952(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$17, %edi
	movq	$0, -7248(%rbp)
	movaps	%xmm0, -7264(%rbp)
	call	_Znwm@PLT
	movdqa	.LC7(%rip), %xmm0
	movq	-7848(%rbp), %rdi
	movq	%r13, %rsi
	movb	$6, 16(%rax)
	leaq	17(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -7264(%rbp)
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7264(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L910
	call	_ZdlPv@PLT
.L910:
	movq	(%rbx), %rax
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	8(%rax), %rcx
	movq	88(%rax), %r10
	movq	104(%rax), %r9
	movq	%rsi, -7856(%rbp)
	movq	32(%rax), %rsi
	movq	80(%rax), %r11
	movq	%rdx, -7952(%rbp)
	movq	48(%rax), %rdx
	movq	%rdi, -8064(%rbp)
	movq	64(%rax), %rdi
	movq	(%rax), %rbx
	movq	%rcx, -7600(%rbp)
	movq	%r10, -8104(%rbp)
	movq	16(%rax), %rcx
	movq	96(%rax), %r10
	movq	%r9, -8112(%rbp)
	movq	112(%rax), %r9
	movq	%rsi, -7936(%rbp)
	leaq	.LC8(%rip), %rsi
	movq	72(%rax), %r12
	movq	120(%rax), %r14
	movq	%rdx, -8048(%rbp)
	movl	$3111, %edx
	movq	%rdi, -8080(%rbp)
	movq	%r15, %rdi
	movq	%r11, -8096(%rbp)
	movq	%r10, -8128(%rbp)
	movq	%r9, -8144(%rbp)
	movq	%rbx, -7552(%rbp)
	movq	128(%rax), %rbx
	movq	%rcx, -7616(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %r8
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	-7480(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal39LoadElementNoHole16FixedDoubleArray_235EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %xmm1
	movq	%r14, %xmm7
	movq	-8144(%rbp), %xmm5
	movq	%rbx, %xmm4
	leaq	-7296(%rbp), %r12
	movq	-8080(%rbp), %xmm6
	movq	-7616(%rbp), %xmm0
	punpcklqdq	%xmm1, %xmm4
	punpcklqdq	%xmm7, %xmm5
	leaq	-64(%rbp), %rdx
	movq	%r12, %rdi
	punpcklqdq	%xmm1, %xmm6
	movhps	-7856(%rbp), %xmm0
	movq	-8128(%rbp), %xmm3
	movq	-8096(%rbp), %xmm7
	movq	-8048(%rbp), %xmm2
	leaq	-224(%rbp), %r14
	movq	-7936(%rbp), %xmm1
	movaps	%xmm0, -7616(%rbp)
	movq	-7552(%rbp), %xmm9
	movq	%r14, %rsi
	movaps	%xmm5, -112(%rbp)
	movhps	-8112(%rbp), %xmm3
	movhps	-8104(%rbp), %xmm7
	movhps	-8064(%rbp), %xmm2
	movhps	-7952(%rbp), %xmm1
	movaps	%xmm0, -208(%rbp)
	movhps	-7600(%rbp), %xmm9
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -8160(%rbp)
	movaps	%xmm5, -8144(%rbp)
	movaps	%xmm3, -8128(%rbp)
	movaps	%xmm7, -8096(%rbp)
	movaps	%xmm6, -8080(%rbp)
	movaps	%xmm2, -8048(%rbp)
	movaps	%xmm1, -7936(%rbp)
	movaps	%xmm9, -7552(%rbp)
	movaps	%xmm9, -224(%rbp)
	movaps	%xmm1, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm4, -96(%rbp)
	movq	%rbx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movaps	%xmm0, -7296(%rbp)
	movq	$0, -7280(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2720(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -7856(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7296(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L911
	call	_ZdlPv@PLT
.L911:
	movq	-7968(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2912(%rbp), %rax
	cmpq	$0, -7256(%rbp)
	movq	%rax, -7600(%rbp)
	jne	.L1265
.L912:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2848(%rbp)
	je	.L914
.L1249:
	movq	-8000(%rbp), %rsi
	movq	%r15, %rdi
	movl	$1798, %ebx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$19, %edi
	movq	$0, -7248(%rbp)
	movaps	%xmm0, -7264(%rbp)
	call	_Znwm@PLT
	movdqa	.LC7(%rip), %xmm0
	movq	-7600(%rbp), %rdi
	movq	%r13, %rsi
	movw	%bx, 16(%rax)
	leaq	19(%rax), %rdx
	movb	$6, 18(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, -7264(%rbp)
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7264(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L915
	call	_ZdlPv@PLT
.L915:
	movq	(%rbx), %rax
	movl	$112, %edi
	movdqu	96(%rax), %xmm0
	movdqu	16(%rax), %xmm5
	movdqu	32(%rax), %xmm4
	movdqu	48(%rax), %xmm3
	movdqu	64(%rax), %xmm2
	movdqu	80(%rax), %xmm1
	movdqu	(%rax), %xmm6
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -7264(%rbp)
	movq	$0, -7248(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-208(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm6
	movdqa	-192(%rbp), %xmm3
	leaq	112(%rax), %rdx
	leaq	-1760(%rbp), %rdi
	movdqa	-176(%rbp), %xmm7
	movups	%xmm4, (%rax)
	movdqa	-160(%rbp), %xmm4
	movups	%xmm5, 16(%rax)
	movdqa	-144(%rbp), %xmm5
	movups	%xmm3, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm5, 80(%rax)
	movups	%xmm6, 96(%rax)
	movq	%rax, -7264(%rbp)
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L916
	call	_ZdlPv@PLT
.L916:
	movq	-7488(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L914
	.p2align 4,,10
	.p2align 3
.L1251:
	movq	-7984(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$17, %edi
	movq	$0, -7248(%rbp)
	movaps	%xmm0, -7264(%rbp)
	call	_Znwm@PLT
	movdqa	.LC7(%rip), %xmm0
	movq	-7872(%rbp), %rdi
	movq	%r13, %rsi
	movb	$6, 16(%rax)
	leaq	17(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -7264(%rbp)
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7264(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L920
	call	_ZdlPv@PLT
.L920:
	movq	(%rbx), %rax
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	8(%rax), %rcx
	movq	88(%rax), %r10
	movq	104(%rax), %r9
	movq	%rsi, -7968(%rbp)
	movq	32(%rax), %rsi
	movq	80(%rax), %r11
	movq	%rdx, -8000(%rbp)
	movq	48(%rax), %rdx
	movq	%rdi, -8064(%rbp)
	movq	64(%rax), %rdi
	movq	(%rax), %rbx
	movq	%rcx, -7936(%rbp)
	movq	%r10, -8104(%rbp)
	movq	16(%rax), %rcx
	movq	96(%rax), %r10
	movq	%r9, -8112(%rbp)
	movq	112(%rax), %r9
	movq	%rsi, -7984(%rbp)
	leaq	.LC8(%rip), %rsi
	movq	72(%rax), %r12
	movq	120(%rax), %r14
	movq	%rdx, -8048(%rbp)
	movl	$3114, %edx
	movq	%rdi, -8080(%rbp)
	movq	%r15, %rdi
	movq	%r11, -8096(%rbp)
	movq	%r10, -8128(%rbp)
	movq	%r9, -8144(%rbp)
	movq	%rbx, -7616(%rbp)
	movq	128(%rax), %rbx
	movq	%rcx, -7952(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %r8
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	-7480(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal33LoadElementNoHole10FixedArray_234EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm1
	movq	%r12, %xmm3
	movq	-8144(%rbp), %xmm5
	movq	%rbx, %xmm4
	leaq	-224(%rbp), %r14
	movq	-8080(%rbp), %xmm6
	movq	-7952(%rbp), %xmm0
	punpcklqdq	%xmm1, %xmm5
	movq	%r12, %xmm1
	punpcklqdq	%xmm3, %xmm4
	movq	%r14, %rsi
	punpcklqdq	%xmm1, %xmm6
	movhps	-7968(%rbp), %xmm0
	movq	-8128(%rbp), %xmm3
	movq	-8096(%rbp), %xmm7
	leaq	-7296(%rbp), %r12
	leaq	-64(%rbp), %rdx
	movq	-8048(%rbp), %xmm2
	movq	-7984(%rbp), %xmm1
	movhps	-8112(%rbp), %xmm3
	movq	%r12, %rdi
	movaps	%xmm5, -112(%rbp)
	movq	-7616(%rbp), %xmm10
	movhps	-8104(%rbp), %xmm7
	movhps	-8064(%rbp), %xmm2
	movhps	-8000(%rbp), %xmm1
	movaps	%xmm0, -7952(%rbp)
	movhps	-7936(%rbp), %xmm10
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -8160(%rbp)
	movaps	%xmm5, -8144(%rbp)
	movaps	%xmm3, -8128(%rbp)
	movaps	%xmm7, -8096(%rbp)
	movaps	%xmm6, -8080(%rbp)
	movaps	%xmm2, -8048(%rbp)
	movaps	%xmm1, -7984(%rbp)
	movaps	%xmm10, -7936(%rbp)
	movaps	%xmm10, -224(%rbp)
	movaps	%xmm1, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm4, -96(%rbp)
	movq	%rbx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movaps	%xmm0, -7296(%rbp)
	movq	$0, -7280(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2144(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7296(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L921
	call	_ZdlPv@PLT
.L921:
	movq	-7720(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2336(%rbp), %rax
	cmpq	$0, -7256(%rbp)
	movq	%rax, -7616(%rbp)
	jne	.L1266
.L922:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2272(%rbp)
	je	.L924
.L1252:
	movq	-8016(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$19, %edi
	movq	$0, -7248(%rbp)
	movaps	%xmm0, -7264(%rbp)
	call	_Znwm@PLT
	movl	$1798, %r11d
	movdqa	.LC7(%rip), %xmm0
	movq	%r13, %rsi
	movw	%r11w, 16(%rax)
	movq	-7616(%rbp), %rdi
	leaq	19(%rax), %rdx
	movb	$6, 18(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, -7264(%rbp)
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7264(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L925
	call	_ZdlPv@PLT
.L925:
	movq	(%rbx), %rax
	movl	$112, %edi
	movdqu	96(%rax), %xmm0
	movdqu	16(%rax), %xmm5
	movdqu	32(%rax), %xmm4
	movdqu	48(%rax), %xmm3
	movdqu	64(%rax), %xmm2
	movdqu	80(%rax), %xmm1
	movdqu	(%rax), %xmm7
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm7, -224(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -7264(%rbp)
	movq	$0, -7248(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-208(%rbp), %xmm5
	movdqa	-192(%rbp), %xmm6
	movdqa	-176(%rbp), %xmm3
	leaq	112(%rax), %rdx
	leaq	-1760(%rbp), %rdi
	movdqa	-160(%rbp), %xmm7
	movups	%xmm4, (%rax)
	movdqa	-144(%rbp), %xmm4
	movups	%xmm5, 16(%rax)
	movdqa	-128(%rbp), %xmm5
	movups	%xmm6, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm4, 80(%rax)
	movups	%xmm5, 96(%rax)
	movq	%rax, -7264(%rbp)
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L926
	call	_ZdlPv@PLT
.L926:
	movq	-7488(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2080(%rbp)
	je	.L927
.L1253:
	movq	-7720(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7456(%rbp)
	leaq	-2144(%rbp), %r12
	movq	$0, -7448(%rbp)
	movq	$0, -7440(%rbp)
	movq	$0, -7432(%rbp)
	movq	$0, -7424(%rbp)
	movq	$0, -7416(%rbp)
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	movq	$0, -7312(%rbp)
	movq	$0, -7296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-7296(%rbp), %rax
	pushq	%rax
	leaq	-7312(%rbp), %rax
	leaq	-7440(%rbp), %rcx
	pushq	%rax
	leaq	-7320(%rbp), %rax
	leaq	-7424(%rbp), %r9
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7432(%rbp), %r8
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7448(%rbp), %rdx
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7456(%rbp), %rsi
	pushq	%rax
	leaq	-7352(%rbp), %rax
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	leaq	-7376(%rbp), %rax
	pushq	%rax
	leaq	-7384(%rbp), %rax
	pushq	%rax
	leaq	-7392(%rbp), %rax
	pushq	%rax
	leaq	-7400(%rbp), %rax
	pushq	%rax
	leaq	-7408(%rbp), %rax
	pushq	%rax
	leaq	-7416(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SQ_SO_SM_
	subq	$-128, %rsp
	leaq	-80(%rbp), %rdx
	movq	%r13, %rdi
	movq	-7328(%rbp), %xmm0
	leaq	-224(%rbp), %rsi
	movq	-7344(%rbp), %xmm1
	movq	$0, -7248(%rbp)
	movq	-7360(%rbp), %xmm2
	movq	-7376(%rbp), %xmm3
	movq	-7392(%rbp), %xmm4
	movhps	-7296(%rbp), %xmm0
	movq	-7408(%rbp), %xmm5
	movhps	-7336(%rbp), %xmm1
	movq	-7424(%rbp), %xmm6
	movhps	-7352(%rbp), %xmm2
	movaps	%xmm0, -96(%rbp)
	movq	-7440(%rbp), %xmm7
	movhps	-7368(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -128(%rbp)
	movq	-7456(%rbp), %xmm8
	movhps	-7384(%rbp), %xmm4
	movhps	-7400(%rbp), %xmm5
	movhps	-7416(%rbp), %xmm6
	movaps	%xmm3, -144(%rbp)
	movhps	-7432(%rbp), %xmm7
	movhps	-7448(%rbp), %xmm8
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm8, -224(%rbp)
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -7264(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7552(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L928
	call	_ZdlPv@PLT
.L928:
	movq	-7560(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1888(%rbp)
	je	.L929
.L1254:
	movq	-7560(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-224(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC7(%rip), %xmm0
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	$2054, %r10d
	leaq	-206(%rbp), %rdx
	movq	$0, -7248(%rbp)
	movaps	%xmm0, -224(%rbp)
	pxor	%xmm0, %xmm0
	movw	%r10w, -208(%rbp)
	movaps	%xmm0, -7264(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7552(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7264(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L930
	call	_ZdlPv@PLT
.L930:
	movq	(%rbx), %rax
	movq	40(%rax), %rdx
	movq	24(%rax), %rsi
	movq	72(%rax), %rdi
	movq	8(%rax), %rcx
	movq	88(%rax), %r11
	movq	(%rax), %rbx
	movq	%rdx, -8016(%rbp)
	movq	56(%rax), %rdx
	movq	104(%rax), %r10
	movq	%rsi, -7968(%rbp)
	movq	32(%rax), %rsi
	movq	%rdi, -8064(%rbp)
	movq	80(%rax), %rdi
	movq	%rdx, -8000(%rbp)
	movq	64(%rax), %rdx
	movq	%rcx, -7936(%rbp)
	movq	16(%rax), %rcx
	movq	%r11, -8096(%rbp)
	movq	96(%rax), %r11
	movq	%rsi, -7984(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -8048(%rbp)
	movl	$85, %edx
	movq	48(%rax), %r12
	movq	%rdi, -8080(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -7952(%rbp)
	movq	%r11, -8104(%rbp)
	movq	%r10, -8128(%rbp)
	movq	%rbx, -7560(%rbp)
	movq	136(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-104(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	-7560(%rbp), %xmm0
	movq	%rbx, -112(%rbp)
	movq	$0, -7248(%rbp)
	movhps	-7936(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-7952(%rbp), %xmm0
	movhps	-7968(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-7984(%rbp), %xmm0
	movhps	-8016(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	%r12, %xmm0
	movhps	-8000(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-8048(%rbp), %xmm0
	movhps	-8064(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-8080(%rbp), %xmm0
	movhps	-8096(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-8104(%rbp), %xmm0
	movhps	-8128(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7264(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1568(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L931
	call	_ZdlPv@PLT
.L931:
	movq	-7728(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L1250:
	movq	-7968(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7456(%rbp)
	movq	$0, -7448(%rbp)
	movq	$0, -7440(%rbp)
	movq	$0, -7432(%rbp)
	movq	$0, -7424(%rbp)
	movq	$0, -7416(%rbp)
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	movq	$0, -7312(%rbp)
	movq	$0, -7296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7296(%rbp), %rax
	movq	-7856(%rbp), %rdi
	pushq	%rax
	leaq	-7312(%rbp), %rax
	leaq	-7440(%rbp), %rcx
	pushq	%rax
	leaq	-7320(%rbp), %rax
	leaq	-7424(%rbp), %r9
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7432(%rbp), %r8
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7448(%rbp), %rdx
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7456(%rbp), %rsi
	pushq	%rax
	leaq	-7352(%rbp), %rax
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	leaq	-7376(%rbp), %rax
	pushq	%rax
	leaq	-7384(%rbp), %rax
	pushq	%rax
	leaq	-7392(%rbp), %rax
	pushq	%rax
	leaq	-7400(%rbp), %rax
	pushq	%rax
	leaq	-7408(%rbp), %rax
	pushq	%rax
	leaq	-7416(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SQ_SO_SM_
	subq	$-128, %rsp
	leaq	-80(%rbp), %rdx
	movq	%r13, %rdi
	movq	-7328(%rbp), %xmm0
	leaq	-224(%rbp), %rsi
	movq	-7344(%rbp), %xmm1
	movq	$0, -7248(%rbp)
	movq	-7360(%rbp), %xmm2
	movq	-7376(%rbp), %xmm3
	movq	-7392(%rbp), %xmm4
	movhps	-7296(%rbp), %xmm0
	movq	-7408(%rbp), %xmm5
	movhps	-7336(%rbp), %xmm1
	movq	-7424(%rbp), %xmm6
	movhps	-7352(%rbp), %xmm2
	movaps	%xmm0, -96(%rbp)
	movq	-7440(%rbp), %xmm7
	movhps	-7368(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -128(%rbp)
	movq	-7456(%rbp), %xmm8
	movhps	-7384(%rbp), %xmm4
	movhps	-7400(%rbp), %xmm5
	movhps	-7416(%rbp), %xmm6
	movaps	%xmm3, -144(%rbp)
	movhps	-7432(%rbp), %xmm7
	movhps	-7448(%rbp), %xmm8
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm8, -224(%rbp)
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -7264(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7552(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L918
	call	_ZdlPv@PLT
.L918:
	movq	-7560(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L917
	.p2align 4,,10
	.p2align 3
.L1255:
	movq	-7488(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7408(%rbp)
	leaq	-1760(%rbp), %r12
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	movq	$0, -7312(%rbp)
	movq	$0, -7296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-7296(%rbp), %rax
	pushq	%rax
	leaq	-7312(%rbp), %rax
	leaq	-7392(%rbp), %rcx
	pushq	%rax
	leaq	-7320(%rbp), %rax
	leaq	-7376(%rbp), %r9
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7384(%rbp), %r8
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7400(%rbp), %rdx
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7408(%rbp), %rsi
	pushq	%rax
	leaq	-7352(%rbp), %rax
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	addq	$80, %rsp
	movl	$86, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$112, %edi
	movq	-7312(%rbp), %xmm0
	movq	-7328(%rbp), %xmm1
	movq	-7344(%rbp), %xmm2
	movq	-7360(%rbp), %xmm3
	movq	$0, -7248(%rbp)
	movq	-7376(%rbp), %xmm4
	movhps	-7296(%rbp), %xmm0
	movq	-7392(%rbp), %xmm5
	movhps	-7320(%rbp), %xmm1
	movq	-7408(%rbp), %xmm6
	movhps	-7336(%rbp), %xmm2
	movaps	%xmm0, -128(%rbp)
	movhps	-7352(%rbp), %xmm3
	movhps	-7368(%rbp), %xmm4
	movhps	-7384(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -176(%rbp)
	movhps	-7400(%rbp), %xmm6
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -7264(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-208(%rbp), %xmm3
	movdqa	-192(%rbp), %xmm1
	movdqa	-176(%rbp), %xmm7
	leaq	112(%rax), %rdx
	movq	%rax, -7264(%rbp)
	movdqa	-160(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm5
	movups	%xmm6, (%rax)
	movdqa	-128(%rbp), %xmm6
	movq	-7560(%rbp), %rdi
	movups	%xmm3, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm5, 80(%rax)
	movups	%xmm6, 96(%rax)
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L933
	call	_ZdlPv@PLT
.L933:
	movq	-7912(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1504(%rbp)
	je	.L934
.L1256:
	movq	-7728(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-224(%rbp), %r14
	leaq	-1568(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	%r13, %rdi
	movabsq	$506098639673231111, %rax
	movl	$1028, %r9d
	leaq	-209(%rbp), %rdx
	movaps	%xmm0, -7264(%rbp)
	movq	%rax, -224(%rbp)
	movw	%r9w, -212(%rbp)
	movl	$67569415, -216(%rbp)
	movb	$8, -210(%rbp)
	movq	$0, -7248(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7264(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L935
	call	_ZdlPv@PLT
.L935:
	movq	(%rbx), %rax
	movq	24(%rax), %rsi
	movq	8(%rax), %rcx
	movq	56(%rax), %rdx
	movq	(%rax), %rbx
	movq	88(%rax), %r11
	movq	80(%rax), %rdi
	movq	%rsi, -8000(%rbp)
	movq	40(%rax), %rsi
	movq	72(%rax), %r12
	movq	%rcx, -7984(%rbp)
	movq	16(%rax), %rcx
	movq	%rdx, -8064(%rbp)
	movq	%rsi, -7968(%rbp)
	movq	64(%rax), %rdx
	movq	48(%rax), %rsi
	movq	%rbx, -7936(%rbp)
	movq	%rcx, -8016(%rbp)
	movq	96(%rax), %rbx
	movq	32(%rax), %rcx
	movq	%r11, -8104(%rbp)
	movq	104(%rax), %r11
	movq	112(%rax), %rax
	movq	%rsi, -8048(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -8080(%rbp)
	movl	$85, %edx
	movq	%rdi, -8096(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -7952(%rbp)
	movq	%r11, -8128(%rbp)
	movq	%rax, -8112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$87, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$3093, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %xmm3
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	-8112(%rbp), %rax
	leaq	-56(%rbp), %rdx
	movq	%r12, -64(%rbp)
	movq	-7936(%rbp), %xmm0
	movq	-8000(%rbp), %xmm7
	movq	$0, -7248(%rbp)
	movhps	-7984(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-8016(%rbp), %xmm0
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-7952(%rbp), %xmm0
	movhps	-7968(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-8048(%rbp), %xmm0
	movhps	-8064(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-8080(%rbp), %xmm0
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-8096(%rbp), %xmm0
	movhps	-8104(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	movhps	-8128(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%rax, %xmm0
	movhps	-7936(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movdqa	%xmm7, %xmm0
	movhps	-7952(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rax, %xmm0
	movhps	-7968(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7264(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1376(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L936
	call	_ZdlPv@PLT
.L936:
	movq	-7736(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1312(%rbp)
	je	.L937
.L1257:
	movq	-7736(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-224(%rbp), %r14
	leaq	-1376(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC9(%rip), %xmm0
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	-203(%rbp), %rdx
	movl	$101189639, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7264(%rbp)
	movb	$7, -204(%rbp)
	movq	$0, -7248(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7264(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L938
	call	_ZdlPv@PLT
.L938:
	movq	(%rbx), %rax
	movl	$87, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	leaq	-7312(%rbp), %r12
	movq	(%rax), %rbx
	movq	%rbx, -7936(%rbp)
	movq	8(%rax), %rbx
	movq	%rbx, -7952(%rbp)
	movq	16(%rax), %rbx
	movq	%rbx, -7968(%rbp)
	movq	24(%rax), %rbx
	movq	%rbx, -7984(%rbp)
	movq	32(%rax), %rbx
	movq	%rbx, -8016(%rbp)
	movq	40(%rax), %rbx
	movq	%rbx, -8000(%rbp)
	movq	48(%rax), %rbx
	movq	%rbx, -8048(%rbp)
	movq	56(%rax), %rbx
	movq	%rbx, -8064(%rbp)
	movq	64(%rax), %rbx
	movq	%rbx, -8080(%rbp)
	movq	72(%rax), %rbx
	movq	%rbx, -8096(%rbp)
	movq	80(%rax), %rbx
	movq	%rbx, -8104(%rbp)
	movq	88(%rax), %rbx
	movq	%rbx, -8128(%rbp)
	movq	96(%rax), %rbx
	movq	%rbx, -8112(%rbp)
	movq	104(%rax), %rbx
	movq	%rbx, -8144(%rbp)
	movq	120(%rax), %rbx
	movq	%rbx, -8160(%rbp)
	movq	128(%rax), %rbx
	movq	%rbx, -8176(%rbp)
	movq	144(%rax), %rcx
	movq	136(%rax), %rbx
	movq	%rcx, -8184(%rbp)
	movq	152(%rax), %rcx
	movq	160(%rax), %rax
	movq	%rcx, -8208(%rbp)
	movq	%rax, -8192(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7480(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19IsUndefinedConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	je	.L939
.L941:
	movq	-8208(%rbp), %xmm0
	movq	%rbx, %xmm1
	movq	%r12, %rdi
	movhps	-8184(%rbp), %xmm1
	movhps	-8192(%rbp), %xmm0
	movaps	%xmm1, -8224(%rbp)
	movaps	%xmm0, -8208(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-7264(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -8184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-8224(%rbp), %xmm1
	movq	-8176(%rbp), %xmm2
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movdqa	-8208(%rbp), %xmm0
	movq	%rax, -7296(%rbp)
	movq	-7248(%rbp), %rax
	movhps	-8184(%rbp), %xmm2
	movaps	%xmm2, -224(%rbp)
	movq	%rax, -7288(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
.L1226:
	movl	$6, %ebx
	movl	$1, %ecx
	xorl	%esi, %esi
	movq	%r12, %rdi
	pushq	%rbx
	movq	-8160(%rbp), %r9
	leaq	-7296(%rbp), %rdx
	pushq	%r14
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	popq	%rdi
	movq	%r12, %rdi
	popq	%r8
	movq	-7936(%rbp), %xmm6
	movq	-7968(%rbp), %xmm5
	movq	-8016(%rbp), %xmm4
	movq	-8048(%rbp), %xmm3
	movq	-8080(%rbp), %xmm2
	movhps	-7952(%rbp), %xmm6
	movq	-8104(%rbp), %xmm1
	movhps	-7984(%rbp), %xmm5
	movq	-8112(%rbp), %xmm0
	movhps	-8000(%rbp), %xmm4
	movhps	-8064(%rbp), %xmm3
	movaps	%xmm6, -8176(%rbp)
	movhps	-8096(%rbp), %xmm2
	movhps	-8128(%rbp), %xmm1
	movaps	%xmm5, -8160(%rbp)
	movhps	-8144(%rbp), %xmm0
	movaps	%xmm4, -8016(%rbp)
	movaps	%xmm3, -7984(%rbp)
	movaps	%xmm2, -7968(%rbp)
	movaps	%xmm1, -7952(%rbp)
	movaps	%xmm0, -7936(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$80, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movdqa	-7936(%rbp), %xmm0
	movdqa	-8176(%rbp), %xmm6
	movl	$112, %edi
	movdqa	-8160(%rbp), %xmm5
	movdqa	-8016(%rbp), %xmm4
	movq	$0, -7248(%rbp)
	movdqa	-7984(%rbp), %xmm3
	movdqa	-7968(%rbp), %xmm2
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movdqa	-7952(%rbp), %xmm1
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -7264(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm1
	movq	%r13, %rsi
	movdqa	-208(%rbp), %xmm7
	movdqa	-192(%rbp), %xmm4
	movdqa	-176(%rbp), %xmm5
	leaq	112(%rax), %rdx
	movq	%rax, -7264(%rbp)
	movdqa	-160(%rbp), %xmm6
	movdqa	-144(%rbp), %xmm3
	movups	%xmm1, (%rax)
	movdqa	-128(%rbp), %xmm1
	movq	-7560(%rbp), %rdi
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	movups	%xmm3, 80(%rax)
	movups	%xmm1, 96(%rax)
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L942
	call	_ZdlPv@PLT
.L942:
	movq	-7912(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1120(%rbp)
	je	.L943
.L1258:
	movq	-7912(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	movq	$0, -7312(%rbp)
	movq	$0, -7296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7296(%rbp), %rax
	movq	-7560(%rbp), %rdi
	pushq	%rax
	leaq	-7312(%rbp), %rax
	leaq	-7392(%rbp), %rcx
	pushq	%rax
	leaq	-7320(%rbp), %rax
	leaq	-7376(%rbp), %r9
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7384(%rbp), %r8
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7400(%rbp), %rdx
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7408(%rbp), %rsi
	pushq	%rax
	leaq	-7352(%rbp), %rax
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	movq	-7480(%rbp), %rbx
	addq	$80, %rsp
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7368(%rbp), %r14
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-7376(%rbp), %rax
	movq	-7392(%rbp), %xmm0
	movl	$112, %edi
	movq	-7408(%rbp), %xmm1
	movq	%rbx, -184(%rbp)
	movhps	-7384(%rbp), %xmm0
	movq	%rax, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	movq	-7360(%rbp), %xmm0
	movhps	-7400(%rbp), %xmm1
	movaps	%xmm1, -224(%rbp)
	movhps	-7352(%rbp), %xmm0
	movq	$0, -7248(%rbp)
	movaps	%xmm0, -176(%rbp)
	movq	-7344(%rbp), %xmm0
	movhps	-7336(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-7328(%rbp), %xmm0
	movhps	-7320(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-7312(%rbp), %xmm0
	movhps	-7296(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7264(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-208(%rbp), %xmm4
	movdqa	-192(%rbp), %xmm5
	movdqa	-176(%rbp), %xmm6
	leaq	112(%rax), %rdx
	movq	%rax, -7264(%rbp)
	movdqa	-160(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm1
	movups	%xmm7, (%rax)
	movdqa	-128(%rbp), %xmm7
	movq	-7520(%rbp), %rdi
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm3, 64(%rax)
	movups	%xmm1, 80(%rax)
	movups	%xmm7, 96(%rax)
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L944
	call	_ZdlPv@PLT
.L944:
	movq	-7888(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L943
	.p2align 4,,10
	.p2align 3
.L1260:
	movq	-7992(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -7248(%rbp)
	movaps	%xmm0, -7264(%rbp)
	call	_Znwm@PLT
	movl	$2056, %esi
	movq	-7888(%rbp), %rdi
	movw	%si, 4(%rax)
	leaq	6(%rax), %rdx
	movq	%r13, %rsi
	movl	$117966599, (%rax)
	movq	%rax, -7264(%rbp)
	movq	%rdx, -7248(%rbp)
	movq	%rdx, -7256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7264(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L948
	call	_ZdlPv@PLT
.L948:
	movq	(%rbx), %rax
	movl	$71, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	(%rax), %rbx
	movq	24(%rax), %rcx
	movq	16(%rax), %r12
	movq	8(%rax), %r14
	movq	%rbx, -7480(%rbp)
	movq	32(%rax), %rbx
	movq	40(%rax), %rax
	movq	%rcx, -7912(%rbp)
	movq	%rax, -7936(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %xmm5
	leaq	-224(%rbp), %rsi
	movq	%r13, %rdi
	movq	-7480(%rbp), %xmm0
	leaq	-176(%rbp), %rdx
	movq	$0, -7248(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	%r12, %xmm0
	leaq	-416(%rbp), %r12
	movhps	-7912(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	%rbx, %xmm0
	movhps	-7936(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7264(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L949
	call	_ZdlPv@PLT
.L949:
	movq	-7904(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -544(%rbp)
	je	.L950
.L1261:
	movq	-7880(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-224(%rbp), %rsi
	leaq	-223(%rbp), %rdx
	movq	%r13, %rdi
	movaps	%xmm0, -7264(%rbp)
	movb	$6, -224(%rbp)
	movq	$0, -7248(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7504(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7264(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L951
	call	_ZdlPv@PLT
.L951:
	movq	(%rbx), %rax
	movq	-8032(%rbp), %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-8024(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L1259:
	movq	-7920(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	movq	$0, -7312(%rbp)
	movq	$0, -7296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7296(%rbp), %rax
	movq	-7760(%rbp), %rdi
	pushq	%rax
	leaq	-7312(%rbp), %rax
	leaq	-7392(%rbp), %rcx
	pushq	%rax
	leaq	-7320(%rbp), %rax
	leaq	-7376(%rbp), %r9
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7384(%rbp), %r8
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7400(%rbp), %rdx
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7408(%rbp), %rsi
	pushq	%rax
	leaq	-7352(%rbp), %rax
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	addq	$80, %rsp
	movl	$89, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7480(%rbp), %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	leaq	-224(%rbp), %rsi
	leaq	-176(%rbp), %rdx
	movq	%r13, %rdi
	movq	-7376(%rbp), %xmm0
	movq	%rax, %xmm4
	movq	-7392(%rbp), %xmm1
	movq	$0, -7248(%rbp)
	movq	-7408(%rbp), %xmm2
	punpcklqdq	%xmm4, %xmm0
	movhps	-7384(%rbp), %xmm1
	movhps	-7400(%rbp), %xmm2
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -7264(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7888(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L946
	call	_ZdlPv@PLT
.L946:
	movq	-7992(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L945
	.p2align 4,,10
	.p2align 3
.L939:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IsNullConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L941
	movq	-8208(%rbp), %xmm1
	movq	%rbx, %xmm0
	movq	%r12, %rdi
	movhps	-8184(%rbp), %xmm0
	movhps	-8192(%rbp), %xmm1
	movaps	%xmm0, -8224(%rbp)
	movaps	%xmm1, -8208(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-7264(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -8184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-8224(%rbp), %xmm0
	movq	-8176(%rbp), %xmm2
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movdqa	-8208(%rbp), %xmm1
	movq	%rax, -7296(%rbp)
	movq	-7248(%rbp), %rax
	movhps	-8184(%rbp), %xmm2
	movaps	%xmm2, -224(%rbp)
	movq	%rax, -7288(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm1, -192(%rbp)
	jmp	.L1226
	.p2align 4,,10
	.p2align 3
.L1263:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	%r12, %rdi
	movdqa	-7504(%rbp), %xmm6
	movdqa	-7648(%rbp), %xmm3
	leaq	-168(%rbp), %rdx
	movaps	%xmm0, -7296(%rbp)
	movq	%rbx, -176(%rbp)
	movaps	%xmm6, -224(%rbp)
	movdqa	-7536(%rbp), %xmm6
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm6, -208(%rbp)
	movq	$0, -7280(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7568(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7296(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L852
	call	_ZdlPv@PLT
.L852:
	movq	-7632(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L1264:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-7520(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-7536(%rbp), %xmm6
	movdqa	-7664(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movdqa	-7696(%rbp), %xmm1
	leaq	-160(%rbp), %rdx
	movaps	%xmm7, -224(%rbp)
	movaps	%xmm6, -208(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -7296(%rbp)
	movq	$0, -7280(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7576(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7296(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L866
	call	_ZdlPv@PLT
.L866:
	movq	-7672(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L1265:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-7552(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-7616(%rbp), %xmm6
	movdqa	-7936(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	leaq	-72(%rbp), %rdx
	movdqa	-8048(%rbp), %xmm5
	movdqa	-8080(%rbp), %xmm3
	movq	%r12, %rdi
	movq	%rbx, -80(%rbp)
	movdqa	-8096(%rbp), %xmm1
	movaps	%xmm7, -224(%rbp)
	movdqa	-8128(%rbp), %xmm7
	movaps	%xmm6, -208(%rbp)
	movdqa	-8144(%rbp), %xmm6
	movaps	%xmm4, -192(%rbp)
	movdqa	-8160(%rbp), %xmm4
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm0, -7296(%rbp)
	movq	$0, -7280(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7600(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7296(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L913
	call	_ZdlPv@PLT
.L913:
	movq	-8000(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L1266:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-7936(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-7952(%rbp), %xmm3
	movdqa	-7984(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	leaq	-72(%rbp), %rdx
	movdqa	-8048(%rbp), %xmm7
	movdqa	-8080(%rbp), %xmm6
	movq	%r12, %rdi
	movq	%rbx, -80(%rbp)
	movdqa	-8096(%rbp), %xmm4
	movaps	%xmm5, -224(%rbp)
	movdqa	-8128(%rbp), %xmm5
	movaps	%xmm3, -208(%rbp)
	movdqa	-8144(%rbp), %xmm3
	movaps	%xmm1, -192(%rbp)
	movdqa	-8160(%rbp), %xmm1
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -7296(%rbp)
	movq	$0, -7280(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7616(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7296(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L923
	call	_ZdlPv@PLT
.L923:
	movq	-8016(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L922
.L1262:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22521:
	.size	_ZN2v88internal18FastArrayForEach_7EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_NS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableISA_EE, .-_ZN2v88internal18FastArrayForEach_7EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_NS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableISA_EE
	.section	.rodata._ZN2v88internal21ArrayForEachAssembler24GenerateArrayForEachImplEv.str1.1,"aMS",@progbits,1
.LC10:
	.string	"Array.prototype.forEach"
	.section	.text._ZN2v88internal21ArrayForEachAssembler24GenerateArrayForEachImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21ArrayForEachAssembler24GenerateArrayForEachImplEv
	.type	_ZN2v88internal21ArrayForEachAssembler24GenerateArrayForEachImplEv, @function
_ZN2v88internal21ArrayForEachAssembler24GenerateArrayForEachImplEv:
.LFB22597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-3192(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$3480, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, -3192(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r13, %rcx
	movq	%r12, %rsi
	leaq	-3168(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-3152(%rbp), %rcx
	movl	$2, %esi
	movq	%r12, %rdi
	movq	-3168(%rbp), %r13
	movq	-3160(%rbp), %rax
	movq	%r12, -3040(%rbp)
	leaq	-2808(%rbp), %r12
	movq	%rcx, -3232(%rbp)
	movq	%rcx, -3024(%rbp)
	movq	%r13, -3008(%rbp)
	movq	%rax, -3248(%rbp)
	movq	$1, -3032(%rbp)
	movq	%rax, -3016(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rax, -3216(%rbp)
	leaq	-3040(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -3352(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -2856(%rbp)
	movq	$0, -2848(%rbp)
	movq	%rax, %rbx
	movq	-3192(%rbp), %rax
	movq	$0, -2840(%rbp)
	movq	%rax, -2864(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -2856(%rbp)
	leaq	-2864(%rbp), %rax
	movq	%rdx, -2840(%rbp)
	movq	%rdx, -2848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2824(%rbp)
	movq	%rax, -3208(%rbp)
	movq	$0, -2832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3192(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2664(%rbp)
	movq	$0, -2656(%rbp)
	movq	%rax, -2672(%rbp)
	movq	$0, -2648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-2616(%rbp), %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -2648(%rbp)
	movq	%rdx, -2656(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -3312(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -2632(%rbp)
	movq	%rax, -2664(%rbp)
	movq	$0, -2640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3192(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2472(%rbp)
	movq	$0, -2464(%rbp)
	movq	%rax, -2480(%rbp)
	movq	$0, -2456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-2424(%rbp), %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -2456(%rbp)
	movq	%rdx, -2464(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -3296(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -2440(%rbp)
	movq	%rax, -2472(%rbp)
	movq	$0, -2448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3192(%rbp), %rax
	movl	$192, %edi
	movq	$0, -2280(%rbp)
	movq	$0, -2272(%rbp)
	movq	%rax, -2288(%rbp)
	movq	$0, -2264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-2232(%rbp), %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -2264(%rbp)
	movq	%rdx, -2272(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -3320(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -2248(%rbp)
	movq	%rax, -2280(%rbp)
	movq	$0, -2256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3192(%rbp), %rax
	movl	$216, %edi
	movq	$0, -2088(%rbp)
	movq	$0, -2080(%rbp)
	movq	%rax, -2096(%rbp)
	movq	$0, -2072(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-2040(%rbp), %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -2072(%rbp)
	movq	%rdx, -2080(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -3264(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -2056(%rbp)
	movq	%rax, -2088(%rbp)
	movq	$0, -2064(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3192(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1896(%rbp)
	movq	$0, -1888(%rbp)
	movq	%rax, -1904(%rbp)
	movq	$0, -1880(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1848(%rbp), %rsi
	xorl	%ecx, %ecx
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -1880(%rbp)
	movq	%rdx, -1888(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -3424(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -1864(%rbp)
	movq	%rax, -1896(%rbp)
	movq	$0, -1872(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3192(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1704(%rbp)
	movq	$0, -1696(%rbp)
	movq	%rax, -1712(%rbp)
	movq	$0, -1688(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-1656(%rbp), %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -1688(%rbp)
	movq	%rdx, -1696(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -3376(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -1672(%rbp)
	movq	%rax, -1704(%rbp)
	movq	$0, -1680(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3192(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1512(%rbp)
	movq	$0, -1504(%rbp)
	movq	%rax, -1520(%rbp)
	movq	$0, -1496(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-1464(%rbp), %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -1496(%rbp)
	movq	%rdx, -1504(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -3392(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -1480(%rbp)
	movq	%rax, -1512(%rbp)
	movq	$0, -1488(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3192(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1320(%rbp)
	movq	$0, -1312(%rbp)
	movq	%rax, -1328(%rbp)
	movq	$0, -1304(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-1272(%rbp), %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -1304(%rbp)
	movq	%rdx, -1312(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -3328(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -1288(%rbp)
	movq	%rax, -1320(%rbp)
	movq	$0, -1296(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3192(%rbp), %rax
	movl	$360, %edi
	movq	$0, -1128(%rbp)
	movq	$0, -1120(%rbp)
	movq	%rax, -1136(%rbp)
	movq	$0, -1112(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-1080(%rbp), %rsi
	leaq	360(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 352(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movq	%rdx, -1112(%rbp)
	movq	%rdx, -1120(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -3336(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -1096(%rbp)
	movq	%rax, -1128(%rbp)
	movq	$0, -1104(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3192(%rbp), %rax
	movl	$360, %edi
	movq	$0, -936(%rbp)
	movq	$0, -928(%rbp)
	movq	%rax, -944(%rbp)
	movq	$0, -920(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-888(%rbp), %rsi
	leaq	360(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 352(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movq	%rdx, -920(%rbp)
	movq	%rdx, -928(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -3256(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -904(%rbp)
	movq	%rax, -936(%rbp)
	movq	$0, -912(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3192(%rbp), %rax
	movl	$264, %edi
	movq	$0, -744(%rbp)
	movq	$0, -736(%rbp)
	movq	%rax, -752(%rbp)
	movq	$0, -728(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	264(%rax), %rdx
	leaq	-696(%rbp), %rsi
	movups	%xmm0, (%rax)
	movq	$0, 256(%rax)
	movq	%rsi, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rdx, -728(%rbp)
	movq	%rdx, -736(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -3360(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -712(%rbp)
	movq	%rax, -744(%rbp)
	movq	$0, -720(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3192(%rbp), %rax
	movl	$264, %edi
	movq	$0, -552(%rbp)
	movq	$0, -544(%rbp)
	movq	%rax, -560(%rbp)
	movq	$0, -536(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-504(%rbp), %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rdx, -536(%rbp)
	movq	%rdx, -544(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -3400(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -520(%rbp)
	movq	%rax, -552(%rbp)
	movq	$0, -528(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3192(%rbp), %rax
	movl	$120, %edi
	movq	$0, -360(%rbp)
	movq	$0, -352(%rbp)
	movq	%rax, -368(%rbp)
	movq	$0, -344(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	120(%rax), %rdx
	leaq	-312(%rbp), %rsi
	movq	$0, 112(%rax)
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -344(%rbp)
	movq	%rdx, -352(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -3344(%rbp)
	movq	%r15, %rsi
	movq	%rax, -360(%rbp)
	movups	%xmm0, -328(%rbp)
	movq	$0, -336(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %xmm1
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movhps	-3248(%rbp), %xmm1
	movaps	%xmm0, -2992(%rbp)
	leaq	-2992(%rbp), %r13
	movaps	%xmm1, -176(%rbp)
	movq	-3232(%rbp), %xmm1
	movq	%rbx, -144(%rbp)
	movhps	-3216(%rbp), %xmm1
	movq	$0, -2976(%rbp)
	movaps	%xmm1, -160(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-176(%rbp), %xmm6
	movdqa	-160(%rbp), %xmm7
	movq	-3208(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -2992(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2976(%rbp)
	movq	%rdx, -2984(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1268
	call	_ZdlPv@PLT
.L1268:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2672(%rbp), %rax
	cmpq	$0, -2800(%rbp)
	movq	%rax, -3280(%rbp)
	leaq	-2480(%rbp), %rax
	movq	%rax, -3272(%rbp)
	jne	.L1454
.L1269:
	leaq	-368(%rbp), %rax
	cmpq	$0, -2608(%rbp)
	movq	%rax, -3216(%rbp)
	jne	.L1455
.L1273:
	leaq	-2288(%rbp), %rax
	cmpq	$0, -2416(%rbp)
	movq	%rax, -3248(%rbp)
	jne	.L1456
	cmpq	$0, -2224(%rbp)
	jne	.L1457
.L1281:
	leaq	-1904(%rbp), %rax
	cmpq	$0, -2032(%rbp)
	movq	%rax, -3320(%rbp)
	leaq	-1712(%rbp), %rax
	movq	%rax, -3296(%rbp)
	jne	.L1458
.L1284:
	leaq	-1520(%rbp), %rax
	cmpq	$0, -1840(%rbp)
	movq	%rax, -3312(%rbp)
	jne	.L1459
.L1288:
	leaq	-1328(%rbp), %rax
	cmpq	$0, -1648(%rbp)
	movq	%rax, -3232(%rbp)
	jne	.L1460
	cmpq	$0, -1456(%rbp)
	jne	.L1461
.L1294:
	cmpq	$0, -1264(%rbp)
	jne	.L1462
.L1297:
	cmpq	$0, -1072(%rbp)
	leaq	-752(%rbp), %r12
	jne	.L1463
.L1302:
	cmpq	$0, -880(%rbp)
	jne	.L1464
.L1305:
	leaq	-560(%rbp), %rax
	cmpq	$0, -688(%rbp)
	movq	%rax, -3328(%rbp)
	jne	.L1465
	cmpq	$0, -496(%rbp)
	jne	.L1466
.L1310:
	cmpq	$0, -304(%rbp)
	jne	.L1467
.L1312:
	movq	-3216(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3328(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3256(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-912(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1314
	call	_ZdlPv@PLT
.L1314:
	movq	-928(%rbp), %rbx
	movq	-936(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1315
	.p2align 4,,10
	.p2align 3
.L1319:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1316
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L1319
.L1317:
	movq	-936(%rbp), %r12
.L1315:
	testq	%r12, %r12
	je	.L1320
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1320:
	movq	-3336(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1321
	call	_ZdlPv@PLT
.L1321:
	movq	-1120(%rbp), %rbx
	movq	-1128(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1322
	.p2align 4,,10
	.p2align 3
.L1326:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1323
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L1326
.L1324:
	movq	-1128(%rbp), %r12
.L1322:
	testq	%r12, %r12
	je	.L1327
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1327:
	movq	-3232(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3312(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3296(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3320(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3264(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1328
	call	_ZdlPv@PLT
.L1328:
	movq	-2080(%rbp), %rbx
	movq	-2088(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1329
	.p2align 4,,10
	.p2align 3
.L1333:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1330
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L1333
.L1331:
	movq	-2088(%rbp), %r12
.L1329:
	testq	%r12, %r12
	je	.L1334
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1334:
	movq	-3248(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3272(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3280(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3208(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1468
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1330:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1333
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1316:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1319
	jmp	.L1317
	.p2align 4,,10
	.p2align 3
.L1323:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1326
	jmp	.L1324
	.p2align 4,,10
	.p2align 3
.L1454:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2976(%rbp)
	movaps	%xmm0, -2992(%rbp)
	call	_Znwm@PLT
	movq	-3208(%rbp), %rdi
	movq	%r13, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2992(%rbp)
	movq	%rdx, -2976(%rbp)
	movq	%rdx, -2984(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2992(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1270
	call	_ZdlPv@PLT
.L1270:
	movq	(%rbx), %rax
	movl	$97, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	24(%rax), %r12
	movq	%rcx, -3272(%rbp)
	movq	16(%rax), %rcx
	movq	%rbx, -3280(%rbp)
	movq	32(%rax), %rbx
	movq	%rcx, -3232(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	.LC10(%rip), %rcx
	call	_ZN2v88internal26RequireObjectCoercible_241EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPKc@PLT
	movl	$100, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ToObject_InlineENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -3248(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$103, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3248(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal21GetLengthProperty_244EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$106, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -3216(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -3440(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3440(%rbp), %rdx
	movq	-3232(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, -3440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %xmm7
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	-3232(%rbp), %xmm2
	movq	-3216(%rbp), %rax
	movq	%rbx, %xmm4
	leaq	-120(%rbp), %rbx
	movq	-3272(%rbp), %xmm3
	leaq	-176(%rbp), %r12
	movq	%rbx, %rdx
	movhps	-3248(%rbp), %xmm4
	punpcklqdq	%xmm7, %xmm2
	movq	%r12, %rsi
	movq	%rax, -128(%rbp)
	movhps	-3280(%rbp), %xmm3
	movaps	%xmm4, -3456(%rbp)
	movaps	%xmm2, -3232(%rbp)
	movaps	%xmm3, -3248(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm0, -2992(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2672(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -3280(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1271
	call	_ZdlPv@PLT
.L1271:
	movq	-3216(%rbp), %rax
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movdqa	-3248(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movdqa	-3456(%rbp), %xmm4
	movq	$0, -2976(%rbp)
	movq	%rax, -128(%rbp)
	movaps	%xmm7, -176(%rbp)
	movdqa	-3232(%rbp), %xmm7
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm0, -2992(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2480(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -3272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1272
	call	_ZdlPv@PLT
.L1272:
	movq	-3296(%rbp), %rcx
	movq	-3312(%rbp), %rdx
	movq	%r15, %rdi
	movq	-3440(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1269
	.p2align 4,,10
	.p2align 3
.L1455:
	movq	-3312(%rbp), %rsi
	movq	%r15, %rdi
	movl	$1800, %ebx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2976(%rbp)
	movaps	%xmm0, -2992(%rbp)
	call	_Znwm@PLT
	movq	-3280(%rbp), %rdi
	movq	%r13, %rsi
	movw	%bx, 4(%rax)
	leaq	7(%rax), %rdx
	movl	$117769477, (%rax)
	movb	$8, 6(%rax)
	movq	%rax, -2992(%rbp)
	movq	%rdx, -2976(%rbp)
	movq	%rdx, -2984(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2992(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1274
	call	_ZdlPv@PLT
.L1274:
	movq	(%rbx), %rax
	movl	$107, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	8(%rax), %rcx
	movq	24(%rax), %rbx
	movq	(%rax), %r12
	movq	%rcx, -3216(%rbp)
	movq	16(%rax), %rcx
	movq	%rbx, -3248(%rbp)
	movq	32(%rax), %rbx
	movq	%rcx, -3232(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %xmm0
	leaq	-176(%rbp), %rsi
	leaq	-136(%rbp), %rdx
	movhps	-3216(%rbp), %xmm0
	movq	%r13, %rdi
	movq	%rbx, -144(%rbp)
	movaps	%xmm0, -176(%rbp)
	movq	-3232(%rbp), %xmm0
	movq	$0, -2976(%rbp)
	movhps	-3248(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2992(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-368(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -3216(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1275
	call	_ZdlPv@PLT
.L1275:
	movq	-3344(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1273
	.p2align 4,,10
	.p2align 3
.L1456:
	movq	-3296(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2976(%rbp)
	movaps	%xmm0, -2992(%rbp)
	call	_Znwm@PLT
	movl	$1800, %r11d
	movq	-3272(%rbp), %rdi
	movq	%r13, %rsi
	movl	$117769477, (%rax)
	leaq	7(%rax), %rdx
	movw	%r11w, 4(%rax)
	movb	$8, 6(%rax)
	movq	%rax, -2992(%rbp)
	movq	%rdx, -2976(%rbp)
	movq	%rdx, -2984(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2992(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1277
	call	_ZdlPv@PLT
.L1277:
	movq	(%rbx), %rax
	movl	$109, %edx
	movq	%r15, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	40(%rax), %rsi
	movq	24(%rax), %r12
	movq	%rcx, -3248(%rbp)
	movq	16(%rax), %rcx
	movq	%rbx, -3296(%rbp)
	movq	32(%rax), %rbx
	movq	48(%rax), %rax
	movq	%rsi, -3440(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -3232(%rbp)
	movq	%rbx, -3312(%rbp)
	movq	%rax, -3456(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	-3248(%rbp), %xmm5
	movq	-3232(%rbp), %rcx
	movhps	-3296(%rbp), %xmm5
	movq	%rcx, -3120(%rbp)
	movaps	%xmm5, -3136(%rbp)
	pushq	-3120(%rbp)
	pushq	-3128(%rbp)
	pushq	-3136(%rbp)
	movaps	%xmm5, -3296(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal82Cast71UT19ATCallableApiObject17ATCallableJSProxy15JSBoundFunction10JSFunction_1405EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm7
	movq	%r12, %xmm2
	movq	-3456(%rbp), %xmm6
	movdqa	-3296(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	leaq	-104(%rbp), %rdx
	movq	-3232(%rbp), %xmm4
	punpcklqdq	%xmm7, %xmm6
	leaq	-3072(%rbp), %rbx
	movq	%rax, -112(%rbp)
	movq	-3312(%rbp), %xmm7
	leaq	-176(%rbp), %r12
	punpcklqdq	%xmm2, %xmm4
	movq	%rbx, %rdi
	movaps	%xmm6, -3456(%rbp)
	movhps	-3440(%rbp), %xmm7
	movq	%r12, %rsi
	movaps	%xmm4, -3232(%rbp)
	movaps	%xmm7, -3312(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm0, -3072(%rbp)
	movq	$0, -3056(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2096(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1278
	call	_ZdlPv@PLT
.L1278:
	movq	-3264(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2288(%rbp), %rax
	cmpq	$0, -2984(%rbp)
	movq	%rax, -3248(%rbp)
	jne	.L1469
.L1279:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2224(%rbp)
	je	.L1281
.L1457:
	movq	-3320(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-176(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	-168(%rbp), %rdx
	movabsq	$578720283176011013, %rax
	movaps	%xmm0, -2992(%rbp)
	movq	%rax, -176(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3248(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2992(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1282
	call	_ZdlPv@PLT
.L1282:
	movq	(%rbx), %rax
	leaq	-136(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm1
	movq	32(%rax), %rax
	movq	$0, -2976(%rbp)
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -144(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -2992(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3216(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1283
	call	_ZdlPv@PLT
.L1283:
	movq	-3344(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1281
	.p2align 4,,10
	.p2align 3
.L1458:
	movq	-3264(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-176(%rbp), %r12
	leaq	-2096(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	-167(%rbp), %rdx
	movaps	%xmm0, -2992(%rbp)
	movabsq	$578720283176011013, %rax
	movq	%rax, -176(%rbp)
	movb	$7, -168(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2992(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1285
	call	_ZdlPv@PLT
.L1285:
	movq	(%rbx), %rax
	movl	$112, %edx
	movq	%r15, %rdi
	movq	8(%rax), %rbx
	movq	40(%rax), %rsi
	movq	(%rax), %rcx
	movq	%rbx, -3312(%rbp)
	movq	24(%rax), %rbx
	movq	%rsi, -3456(%rbp)
	movq	48(%rax), %rsi
	movq	%rcx, -3296(%rbp)
	movq	16(%rax), %rcx
	movq	%rbx, -3320(%rbp)
	movq	32(%rax), %rbx
	movq	64(%rax), %rax
	movq	%rsi, -3472(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -3232(%rbp)
	movq	%rbx, -3440(%rbp)
	movq	%rax, -3488(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3232(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17IntPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-112(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	-3472(%rbp), %xmm5
	movq	%r13, %rdi
	movq	-3440(%rbp), %xmm2
	movaps	%xmm0, -2992(%rbp)
	movq	-3232(%rbp), %xmm3
	movq	-3296(%rbp), %xmm6
	movq	$0, -2976(%rbp)
	movhps	-3488(%rbp), %xmm5
	movhps	-3456(%rbp), %xmm2
	movhps	-3312(%rbp), %xmm6
	movhps	-3320(%rbp), %xmm3
	movq	%rdx, -3312(%rbp)
	movaps	%xmm5, -3472(%rbp)
	movaps	%xmm2, -3440(%rbp)
	movaps	%xmm3, -3232(%rbp)
	movaps	%xmm6, -3296(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1904(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2992(%rbp), %rdi
	movq	-3312(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L1286
	call	_ZdlPv@PLT
	movq	-3312(%rbp), %rdx
.L1286:
	movdqa	-3296(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r13, %rdi
	movdqa	-3232(%rbp), %xmm3
	movdqa	-3440(%rbp), %xmm4
	movaps	%xmm0, -2992(%rbp)
	movaps	%xmm5, -176(%rbp)
	movdqa	-3472(%rbp), %xmm5
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1712(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -3296(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1287
	call	_ZdlPv@PLT
.L1287:
	movq	-3376(%rbp), %rcx
	movq	-3424(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1284
	.p2align 4,,10
	.p2align 3
.L1459:
	movq	-3424(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2976(%rbp)
	movaps	%xmm0, -2992(%rbp)
	call	_Znwm@PLT
	movq	-3320(%rbp), %rdi
	movq	%r13, %rsi
	movabsq	$506662689138083077, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2992(%rbp)
	movq	%rdx, -2976(%rbp)
	movq	%rdx, -2984(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2992(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1289
	call	_ZdlPv@PLT
.L1289:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	24(%rax), %rsi
	movq	8(%rax), %rbx
	movq	40(%rax), %rdx
	movq	(%rax), %rcx
	movq	%rsi, -3424(%rbp)
	movq	32(%rax), %rsi
	movq	%rbx, -3312(%rbp)
	movq	16(%rax), %rbx
	movq	%rdx, -3456(%rbp)
	movq	48(%rax), %rdx
	movq	56(%rax), %rax
	movq	%rsi, -3440(%rbp)
	movl	$1, %esi
	movq	%rcx, -3232(%rbp)
	movq	%rdx, -3472(%rbp)
	movq	%rax, -3488(%rbp)
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	-3232(%rbp), %xmm0
	movq	%rbx, -3088(%rbp)
	pushq	-3088(%rbp)
	movhps	-3312(%rbp), %xmm0
	movaps	%xmm0, -3104(%rbp)
	pushq	-3096(%rbp)
	pushq	-3104(%rbp)
	movaps	%xmm0, -3232(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-104(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, -112(%rbp)
	movdqa	-3232(%rbp), %xmm0
	leaq	-176(%rbp), %rsi
	movq	$0, -2976(%rbp)
	movaps	%xmm0, -176(%rbp)
	movq	%rbx, %xmm0
	movhps	-3424(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-3440(%rbp), %xmm0
	movhps	-3456(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-3472(%rbp), %xmm0
	movhps	-3488(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2992(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1520(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -3312(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1290
	call	_ZdlPv@PLT
.L1290:
	movq	-3392(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1288
	.p2align 4,,10
	.p2align 3
.L1460:
	movq	-3376(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2976(%rbp)
	movaps	%xmm0, -2992(%rbp)
	call	_Znwm@PLT
	movq	-3296(%rbp), %rdi
	movq	%r13, %rsi
	movabsq	$506662689138083077, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2992(%rbp)
	movq	%rdx, -2976(%rbp)
	movq	%rdx, -2984(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2992(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1292
	call	_ZdlPv@PLT
.L1292:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	8(%rax), %rbx
	movq	24(%rax), %rsi
	movq	(%rax), %rcx
	movq	32(%rax), %r12
	movq	%rbx, -3376(%rbp)
	movq	16(%rax), %rbx
	movq	%rsi, -3440(%rbp)
	movq	40(%rax), %rsi
	movq	%rbx, -3424(%rbp)
	movq	48(%rax), %rbx
	movq	56(%rax), %rax
	movq	%rcx, -3232(%rbp)
	movq	%rsi, -3456(%rbp)
	movq	%rax, -3472(%rbp)
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	leaq	-176(%rbp), %rsi
	leaq	-104(%rbp), %rdx
	movq	%r13, %rdi
	movq	-3232(%rbp), %xmm0
	movq	%rax, -112(%rbp)
	movq	$0, -2976(%rbp)
	movhps	-3376(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-3424(%rbp), %xmm0
	movhps	-3440(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%r12, %xmm0
	movhps	-3456(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	movhps	-3472(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2992(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1328(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -3232(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1293
	call	_ZdlPv@PLT
.L1293:
	movq	-3328(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1456(%rbp)
	je	.L1294
.L1461:
	movq	-3392(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -2976(%rbp)
	movaps	%xmm0, -2992(%rbp)
	call	_Znwm@PLT
	movq	-3312(%rbp), %rdi
	movq	%r13, %rsi
	movabsq	$506662689138083077, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -2992(%rbp)
	movq	%rdx, -2976(%rbp)
	movq	%rdx, -2984(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2992(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1295
	call	_ZdlPv@PLT
.L1295:
	movq	(%rbx), %rax
	leaq	-176(%rbp), %rsi
	leaq	-104(%rbp), %rdx
	movq	%r13, %rdi
	movdqu	48(%rax), %xmm0
	movdqu	(%rax), %xmm3
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	64(%rax), %rax
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -176(%rbp)
	movq	%rax, -112(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -2992(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3232(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1296
	call	_ZdlPv@PLT
.L1296:
	movq	-3328(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1264(%rbp)
	je	.L1297
.L1462:
	movq	-3328(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -2976(%rbp)
	movaps	%xmm0, -2992(%rbp)
	call	_Znwm@PLT
	movq	-3232(%rbp), %rdi
	movq	%r13, %rsi
	movabsq	$506662689138083077, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -2992(%rbp)
	movq	%rdx, -2976(%rbp)
	movq	%rdx, -2984(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2992(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1298
	call	_ZdlPv@PLT
.L1298:
	movq	(%rbx), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	32(%rax), %rdx
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	64(%rax), %r9
	movq	%rdx, -3472(%rbp)
	movq	48(%rax), %rdx
	movq	%rcx, -3424(%rbp)
	movq	24(%rax), %rcx
	movq	%rbx, -3440(%rbp)
	movq	16(%rax), %rbx
	movq	%rdx, -3392(%rbp)
	movl	$115, %edx
	movq	56(%rax), %r12
	movq	%r9, -3504(%rbp)
	movq	%rcx, -3376(%rbp)
	movq	%rbx, -3456(%rbp)
	movq	40(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$117, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -3488(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-3184(%rbp), %rax
	movl	$6, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -3328(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pushq	-3328(%rbp)
	movq	%r12, %r8
	movq	-3392(%rbp), %rcx
	movq	-3504(%rbp), %r9
	pushq	%r13
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	-3376(%rbp), %rsi
	call	_ZN2v88internal18FastArrayForEach_7EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_NS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableISA_EE
	movq	-3504(%rbp), %r9
	movq	%r12, %xmm7
	movq	-3392(%rbp), %xmm3
	movq	%rbx, %xmm4
	movq	%rbx, %xmm6
	movq	%rax, -64(%rbp)
	leaq	-56(%rbp), %rax
	movq	%r9, %xmm2
	punpcklqdq	%xmm3, %xmm4
	pxor	%xmm0, %xmm0
	movq	%rax, %rdx
	punpcklqdq	%xmm2, %xmm7
	movdqa	%xmm3, %xmm2
	movq	%r12, %xmm3
	movq	-3424(%rbp), %xmm1
	punpcklqdq	%xmm3, %xmm2
	movq	%r9, %xmm5
	movq	-3472(%rbp), %xmm3
	leaq	-3072(%rbp), %rbx
	leaq	-176(%rbp), %r12
	movhps	-3440(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%rax, -3440(%rbp)
	punpcklqdq	%xmm6, %xmm3
	movq	%r12, %rsi
	movq	-3456(%rbp), %xmm6
	movhps	-3488(%rbp), %xmm5
	movaps	%xmm7, -3504(%rbp)
	movhps	-3376(%rbp), %xmm6
	movaps	%xmm4, -3392(%rbp)
	movaps	%xmm5, -3488(%rbp)
	movaps	%xmm2, -3520(%rbp)
	movaps	%xmm3, -3472(%rbp)
	movaps	%xmm6, -3376(%rbp)
	movaps	%xmm1, -3424(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	movaps	%xmm0, -3072(%rbp)
	movq	$0, -3056(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-944(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3072(%rbp), %rdi
	popq	%r9
	popq	%r10
	testq	%rdi, %rdi
	je	.L1299
	call	_ZdlPv@PLT
.L1299:
	movq	-3256(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2984(%rbp)
	jne	.L1470
.L1300:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3328(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	jmp	.L1297
	.p2align 4,,10
	.p2align 3
.L1465:
	movq	-3360(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$11, %edi
	movq	$0, -2976(%rbp)
	movaps	%xmm0, -2992(%rbp)
	call	_Znwm@PLT
	movl	$2056, %esi
	movq	%r12, %rdi
	movabsq	$506662689138083077, %rcx
	movw	%si, 8(%rax)
	leaq	11(%rax), %rdx
	movq	%r13, %rsi
	movq	%rcx, (%rax)
	movb	$6, 10(%rax)
	movq	%rax, -2992(%rbp)
	movq	%rdx, -2976(%rbp)
	movq	%rdx, -2984(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2992(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1308
	call	_ZdlPv@PLT
.L1308:
	movq	(%rbx), %rax
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	8(%rax), %rbx
	movq	%rsi, -3360(%rbp)
	movq	32(%rax), %rsi
	movq	%rdx, -3440(%rbp)
	movq	48(%rax), %rdx
	movq	%rdi, -3472(%rbp)
	movq	64(%rax), %rdi
	movq	(%rax), %rcx
	movq	%rbx, -3376(%rbp)
	movq	16(%rax), %rbx
	movq	%rsi, -3424(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -3456(%rbp)
	movl	$121, %edx
	movq	%rdi, -3488(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -3328(%rbp)
	movq	%rbx, -3392(%rbp)
	movq	80(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$116, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-88(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rbx, -96(%rbp)
	movq	-3328(%rbp), %xmm0
	movq	%rbx, %xmm7
	leaq	-176(%rbp), %rsi
	movq	$0, -2976(%rbp)
	movhps	-3376(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-3392(%rbp), %xmm0
	movhps	-3360(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-3424(%rbp), %xmm0
	movhps	-3440(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-3456(%rbp), %xmm0
	movhps	-3472(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-3488(%rbp), %xmm0
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2992(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-560(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -3328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1309
	call	_ZdlPv@PLT
.L1309:
	movq	-3400(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -496(%rbp)
	je	.L1310
.L1466:
	movq	-3400(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$11, %edi
	movq	$0, -2976(%rbp)
	movaps	%xmm0, -2992(%rbp)
	call	_Znwm@PLT
	movq	-3328(%rbp), %rdi
	movq	%r13, %rsi
	movabsq	$506662689138083077, %rcx
	movq	%rcx, (%rax)
	movl	$2056, %ecx
	leaq	11(%rax), %rdx
	movw	%cx, 8(%rax)
	movb	$6, 10(%rax)
	movq	%rax, -2992(%rbp)
	movq	%rdx, -2976(%rbp)
	movq	%rdx, -2984(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2992(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1311
	call	_ZdlPv@PLT
.L1311:
	movq	(%rbx), %rax
	movl	$125, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	40(%rax), %rcx
	movq	24(%rax), %r9
	movq	48(%rax), %rbx
	movq	%rcx, -3376(%rbp)
	movq	56(%rax), %rcx
	movq	%r9, -3472(%rbp)
	movq	%rcx, -3392(%rbp)
	movq	64(%rax), %rcx
	movq	72(%rax), %rax
	movq	%rcx, -3360(%rbp)
	movq	%rax, -3400(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	movq	%rax, -3440(%rbp)
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$124, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -3424(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-3184(%rbp), %r10
	movq	%r14, %rsi
	movq	%r10, %rdi
	movq	%r10, -3456(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3456(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$747, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-3456(%rbp), %r10
	movq	-2992(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-176(%rbp), %rcx
	xorl	%esi, %esi
	movq	-3456(%rbp), %r10
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-3472(%rbp), %r9
	leaq	-3072(%rbp), %rdx
	movq	%rax, -3072(%rbp)
	movq	-2976(%rbp), %rax
	movq	%r10, %rdi
	movq	%rax, -3064(%rbp)
	movq	-3376(%rbp), %rax
	movq	%r10, -3376(%rbp)
	movq	%rax, %xmm0
	movhps	-3392(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-3360(%rbp), %xmm0
	movhps	-3440(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%rax, %xmm0
	movhps	-3400(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	movl	$8, %ebx
	pushq	%rbx
	movhps	-3424(%rbp), %xmm0
	pushq	%rcx
	movl	$1, %ecx
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-3376(%rbp), %r10
	movq	%rax, %rbx
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3352(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	cmpq	$0, -304(%rbp)
	popq	%rax
	popq	%rdx
	je	.L1312
.L1467:
	movq	-3344(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2976(%rbp)
	movaps	%xmm0, -2992(%rbp)
	call	_Znwm@PLT
	movq	-3216(%rbp), %rdi
	movq	%r13, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2992(%rbp)
	movq	%rdx, -2976(%rbp)
	movq	%rdx, -2984(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2992(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1313
	call	_ZdlPv@PLT
.L1313:
	movq	(%rbx), %rax
	movl	$128, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	leaq	-3072(%rbp), %r13
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	%rcx, -3344(%rbp)
	movq	16(%rax), %rcx
	movq	%rbx, -3352(%rbp)
	movq	24(%rax), %rbx
	movq	%rcx, -3392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -3376(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3392(%rbp), %rcx
	subq	$8, %rsp
	movq	%r13, %rdi
	movq	-3344(%rbp), %xmm0
	movq	-3376(%rbp), %r8
	movq	%rcx, -2976(%rbp)
	movhps	-3352(%rbp), %xmm0
	pushq	-2976(%rbp)
	movq	%r8, %rsi
	movaps	%xmm0, -2992(%rbp)
	pushq	-2984(%rbp)
	pushq	-2992(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r13, %rdi
	movq	%rax, -3344(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	-3344(%rbp), %rcx
	movl	$25, %edx
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L1312
	.p2align 4,,10
	.p2align 3
.L1464:
	movq	-3256(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-944(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$2055, %edi
	pxor	%xmm0, %xmm0
	movabsq	$506662689138083077, %rax
	movw	%di, -164(%rbp)
	leaq	-176(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-161(%rbp), %rdx
	movq	%rax, -176(%rbp)
	movaps	%xmm0, -2992(%rbp)
	movl	$134678536, -168(%rbp)
	movb	$8, -162(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2992(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1306
	call	_ZdlPv@PLT
.L1306:
	movq	(%rbx), %rax
	movq	-3352(%rbp), %rdi
	movq	112(%rax), %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	jmp	.L1305
	.p2align 4,,10
	.p2align 3
.L1463:
	movq	-3336(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-176(%rbp), %r12
	leaq	-1136(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r13, %rdi
	movabsq	$506662689138083077, %rax
	movl	$2055, %r8d
	leaq	-161(%rbp), %rdx
	movaps	%xmm0, -2992(%rbp)
	movq	%rax, -176(%rbp)
	movw	%r8w, -164(%rbp)
	movl	$134678536, -168(%rbp)
	movb	$6, -162(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2992(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1303
	call	_ZdlPv@PLT
.L1303:
	movq	(%rbx), %rax
	movq	%r12, %rsi
	leaq	-88(%rbp), %rdx
	movq	%r13, %rdi
	leaq	-752(%rbp), %r12
	movdqu	64(%rax), %xmm0
	movdqu	(%rax), %xmm4
	movdqu	16(%rax), %xmm3
	movdqu	32(%rax), %xmm2
	movdqu	48(%rax), %xmm1
	movq	112(%rax), %rax
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -176(%rbp)
	movq	%rax, -96(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -2992(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1304
	call	_ZdlPv@PLT
.L1304:
	movq	-3360(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1469:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-112(%rbp), %rdx
	movq	%r12, %rsi
	movdqa	-3296(%rbp), %xmm4
	movdqa	-3232(%rbp), %xmm5
	movq	%rbx, %rdi
	movaps	%xmm0, -3072(%rbp)
	movdqa	-3312(%rbp), %xmm7
	movq	$0, -3056(%rbp)
	movaps	%xmm4, -176(%rbp)
	movdqa	-3456(%rbp), %xmm4
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3248(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1280
	call	_ZdlPv@PLT
.L1280:
	movq	-3320(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1279
	.p2align 4,,10
	.p2align 3
.L1470:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3328(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movdqa	-3424(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movdqa	-3376(%rbp), %xmm7
	movdqa	-3472(%rbp), %xmm4
	movq	-3440(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movaps	%xmm5, -176(%rbp)
	movdqa	-3520(%rbp), %xmm5
	movaps	%xmm7, -160(%rbp)
	movdqa	-3488(%rbp), %xmm7
	movaps	%xmm4, -144(%rbp)
	movdqa	-3392(%rbp), %xmm4
	movaps	%xmm5, -128(%rbp)
	movdqa	-3504(%rbp), %xmm5
	movq	%rax, -64(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm0, -3072(%rbp)
	movq	$0, -3056(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1136(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1301
	call	_ZdlPv@PLT
.L1301:
	movq	-3336(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1300
.L1468:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22597:
	.size	_ZN2v88internal21ArrayForEachAssembler24GenerateArrayForEachImplEv, .-_ZN2v88internal21ArrayForEachAssembler24GenerateArrayForEachImplEv
	.section	.rodata._ZN2v88internal8Builtins21Generate_ArrayForEachEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"ArrayForEach"
	.section	.text._ZN2v88internal8Builtins21Generate_ArrayForEachEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins21Generate_ArrayForEachEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins21Generate_ArrayForEachEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins21Generate_ArrayForEachEPNS0_8compiler18CodeAssemblerStateE:
.LFB22593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1736, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$748, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L1475
.L1472:
	movq	%r13, %rdi
	call	_ZN2v88internal21ArrayForEachAssembler24GenerateArrayForEachImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1476
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1475:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L1472
.L1476:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22593:
	.size	_ZN2v88internal8Builtins21Generate_ArrayForEachEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins21Generate_ArrayForEachEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins47Generate_ArrayForEachLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins47Generate_ArrayForEachLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins47Generate_ArrayForEachLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE:
.LFB29884:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29884:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins47Generate_ArrayForEachLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins47Generate_ArrayForEachLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins47Generate_ArrayForEachLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.weak	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC7:
	.byte	7
	.byte	7
	.byte	8
	.byte	7
	.byte	8
	.byte	6
	.byte	6
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	4
	.byte	4
	.byte	4
	.byte	6
	.byte	7
	.align 16
.LC9:
	.byte	7
	.byte	7
	.byte	8
	.byte	7
	.byte	8
	.byte	6
	.byte	6
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	4
	.byte	4
	.byte	4
	.byte	8
	.byte	7
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
