	.file	"regexp-test-tq-csa.cc"
	.text
	.section	.rodata._ZN2v88internal32RegExpPrototypeTestFastAssembler35GenerateRegExpPrototypeTestFastImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../../deps/v8/../../deps/v8/src/builtins/regexp-test.tq"
	.section	.text._ZN2v88internal32RegExpPrototypeTestFastAssembler35GenerateRegExpPrototypeTestFastImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32RegExpPrototypeTestFastAssembler35GenerateRegExpPrototypeTestFastImplEv
	.type	_ZN2v88internal32RegExpPrototypeTestFastAssembler35GenerateRegExpPrototypeTestFastImplEv, @function
_ZN2v88internal32RegExpPrototypeTestFastAssembler35GenerateRegExpPrototypeTestFastImplEv:
.LFB22454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1016(%rbp), %r14
	leaq	-248(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1072(%rbp), %rbx
	subq	$1304, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, -1272(%rbp)
	movq	%rax, -1240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, -1296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	leaq	-1240(%rbp), %r12
	movq	%rax, -1304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$72, %edi
	movq	$0, -1064(%rbp)
	movq	$0, -1056(%rbp)
	movq	%rax, %r13
	movq	-1240(%rbp), %rax
	movq	$0, -1048(%rbp)
	movq	%rax, -1072(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -1048(%rbp)
	movq	%rdx, -1056(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1032(%rbp)
	movq	%rax, -1064(%rbp)
	movq	$0, -1040(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1240(%rbp), %rax
	movl	$120, %edi
	movq	$0, -872(%rbp)
	movq	$0, -864(%rbp)
	movq	%rax, -880(%rbp)
	movq	$0, -856(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -872(%rbp)
	leaq	-824(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -856(%rbp)
	movq	%rdx, -864(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -840(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -848(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1240(%rbp), %rax
	movl	$144, %edi
	movq	$0, -680(%rbp)
	movq	$0, -672(%rbp)
	movq	%rax, -688(%rbp)
	movq	$0, -664(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -680(%rbp)
	leaq	-632(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -664(%rbp)
	movq	%rdx, -672(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -648(%rbp)
	movq	%rax, -1256(%rbp)
	movq	$0, -656(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1240(%rbp), %rax
	movl	$72, %edi
	movq	$0, -488(%rbp)
	movq	$0, -480(%rbp)
	movq	%rax, -496(%rbp)
	movq	$0, -472(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -488(%rbp)
	leaq	-440(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -472(%rbp)
	movq	%rdx, -480(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -456(%rbp)
	movq	%rax, -1264(%rbp)
	movq	$0, -464(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1240(%rbp), %rax
	movl	$96, %edi
	movq	$0, -296(%rbp)
	movq	$0, -288(%rbp)
	movq	%rax, -304(%rbp)
	movq	$0, -280(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r15, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -280(%rbp)
	movq	%rdx, -288(%rbp)
	xorl	%edx, %edx
	movq	%rax, -296(%rbp)
	movups	%xmm0, -264(%rbp)
	movq	$0, -272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	-1296(%rbp), %xmm1
	movq	%r13, -96(%rbp)
	leaq	-1200(%rbp), %r13
	movhps	-1304(%rbp), %xmm1
	movaps	%xmm0, -1200(%rbp)
	movaps	%xmm1, -112(%rbp)
	movq	$0, -1184(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm2
	leaq	24(%rax), %rdx
	movq	%rax, -1200(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -1184(%rbp)
	movq	%rdx, -1192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2
	call	_ZdlPv@PLT
.L2:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1008(%rbp)
	jne	.L116
	cmpq	$0, -816(%rbp)
	jne	.L117
.L8:
	cmpq	$0, -624(%rbp)
	jne	.L118
.L11:
	cmpq	$0, -432(%rbp)
	jne	.L119
.L14:
	cmpq	$0, -240(%rbp)
	jne	.L120
.L16:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L18
	call	_ZdlPv@PLT
.L18:
	movq	-288(%rbp), %rbx
	movq	-296(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L19
	.p2align 4,,10
	.p2align 3
.L23:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L20
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L23
.L21:
	movq	-296(%rbp), %r13
.L19:
	testq	%r13, %r13
	je	.L24
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L24:
	movq	-1264(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-464(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L25
	call	_ZdlPv@PLT
.L25:
	movq	-480(%rbp), %rbx
	movq	-488(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L26
	.p2align 4,,10
	.p2align 3
.L30:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L27
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L30
.L28:
	movq	-488(%rbp), %r13
.L26:
	testq	%r13, %r13
	je	.L31
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L31:
	movq	-1256(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L32
	call	_ZdlPv@PLT
.L32:
	movq	-672(%rbp), %rbx
	movq	-680(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L33
	.p2align 4,,10
	.p2align 3
.L37:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L34
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L37
.L35:
	movq	-680(%rbp), %r13
.L33:
	testq	%r13, %r13
	je	.L38
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L38:
	movq	-1280(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-848(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L39
	call	_ZdlPv@PLT
.L39:
	movq	-864(%rbp), %rbx
	movq	-872(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L40
	.p2align 4,,10
	.p2align 3
.L44:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L41
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L44
.L42:
	movq	-872(%rbp), %r13
.L40:
	testq	%r13, %r13
	je	.L45
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L45:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1040(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L46
	call	_ZdlPv@PLT
.L46:
	movq	-1056(%rbp), %rbx
	movq	-1064(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L47
	.p2align 4,,10
	.p2align 3
.L51:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L48
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L51
.L49:
	movq	-1064(%rbp), %r13
.L47:
	testq	%r13, %r13
	je	.L52
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L52:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L121
	addq	$1304, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L51
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L41:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L44
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L34:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L37
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L20:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L23
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L27:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L30
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L116:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1184(%rbp)
	movaps	%xmm0, -1200(%rbp)
	call	_Znwm@PLT
	movl	$1799, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%rbx, %rdi
	movb	$7, 2(%rax)
	movq	%rax, -1200(%rbp)
	movq	%rdx, -1184(%rbp)
	movq	%rdx, -1192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1200(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L4
	call	_ZdlPv@PLT
.L4:
	movq	(%rbx), %rax
	movl	$33, %edx
	movq	%r12, %rdi
	leaq	-1232(%rbp), %rbx
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rax
	movq	%rsi, -1296(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rax, -1304(%rbp)
	movq	%rcx, -1312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1272(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1304(%rbp), %rcx
	movq	%r13, %r8
	movq	%rbx, %rdi
	movq	-1296(%rbp), %rdx
	movq	-1312(%rbp), %rsi
	call	_ZN2v88internal23RegExpBuiltinsAssembler40RegExpPrototypeExecBodyWithoutResultFastENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_8JSRegExpEEENS3_INS0_6StringEEEPNS2_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	movq	%rax, -1320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1304(%rbp), %rax
	movl	$48, %edi
	movq	-1312(%rbp), %xmm4
	movq	$0, -1216(%rbp)
	movq	%rax, %xmm0
	movq	%rax, %xmm3
	movhps	-1296(%rbp), %xmm4
	movhps	-1320(%rbp), %xmm0
	movhps	-1296(%rbp), %xmm3
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -1344(%rbp)
	movaps	%xmm4, -1296(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -1232(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm5
	movq	%rbx, %rsi
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	leaq	48(%rax), %rdx
	leaq	-688(%rbp), %rdi
	movq	%rax, -1232(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -1216(%rbp)
	movq	%rdx, -1224(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5
	call	_ZdlPv@PLT
.L5:
	movq	-1256(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1192(%rbp)
	jne	.L122
.L6:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -816(%rbp)
	je	.L8
.L117:
	movq	-1280(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-880(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1184(%rbp)
	movaps	%xmm0, -1200(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -1200(%rbp)
	movq	%rdx, -1184(%rbp)
	movq	%rdx, -1192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1200(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L9
	call	_ZdlPv@PLT
.L9:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm5
	movaps	%xmm0, -1200(%rbp)
	movq	$0, -1184(%rbp)
	movq	%rdx, -96(%rbp)
	movaps	%xmm5, -112(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-112(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-496(%rbp), %rdi
	movq	%rax, -1200(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1184(%rbp)
	movq	%rdx, -1192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	movq	-1264(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -624(%rbp)
	je	.L11
.L118:
	movq	-1256(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-688(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -1184(%rbp)
	movaps	%xmm0, -1200(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movq	%rbx, %rdi
	movw	%si, 4(%rax)
	leaq	6(%rax), %rdx
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	movq	%rax, -1200(%rbp)
	movq	%rdx, -1184(%rbp)
	movq	%rdx, -1192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1200(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L12
	call	_ZdlPv@PLT
.L12:
	movq	(%rbx), %rax
	movl	$32, %edi
	movdqu	32(%rax), %xmm7
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm5
	movq	$0, -1184(%rbp)
	shufpd	$2, %xmm7, %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm0, -1200(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-304(%rbp), %rdi
	movq	%rax, -1200(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1184(%rbp)
	movq	%rdx, -1192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L13
	call	_ZdlPv@PLT
.L13:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -432(%rbp)
	je	.L14
.L119:
	movq	-1264(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-496(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1184(%rbp)
	movaps	%xmm0, -1200(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -1200(%rbp)
	movq	%rdx, -1184(%rbp)
	movq	%rdx, -1192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L15
	call	_ZdlPv@PLT
.L15:
	movl	$34, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1272(%rbp), %rdi
	call	_ZN2v88internal8False_66EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1272(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -240(%rbp)
	je	.L16
.L120:
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	-304(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1184(%rbp)
	movaps	%xmm0, -1200(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1200(%rbp)
	movq	%rdx, -1184(%rbp)
	movq	%rdx, -1192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L17
	call	_ZdlPv@PLT
.L17:
	movq	%r12, %rdi
	movl	$33, %edx
	leaq	.LC0(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$35, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1272(%rbp), %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1272(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L122:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1304(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1296(%rbp), %xmm5
	movdqa	-1344(%rbp), %xmm6
	movl	$40, %edi
	movaps	%xmm0, -1232(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1216(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%rbx, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm2
	leaq	40(%rax), %rdx
	leaq	-880(%rbp), %rdi
	movq	%rax, -1232(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -1216(%rbp)
	movq	%rdx, -1224(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L7
	call	_ZdlPv@PLT
.L7:
	movq	-1280(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L6
.L121:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22454:
	.size	_ZN2v88internal32RegExpPrototypeTestFastAssembler35GenerateRegExpPrototypeTestFastImplEv, .-_ZN2v88internal32RegExpPrototypeTestFastAssembler35GenerateRegExpPrototypeTestFastImplEv
	.section	.rodata._ZN2v88internal8Builtins32Generate_RegExpPrototypeTestFastEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/regexp-test-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins32Generate_RegExpPrototypeTestFastEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"RegExpPrototypeTestFast"
	.section	.text._ZN2v88internal8Builtins32Generate_RegExpPrototypeTestFastEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins32Generate_RegExpPrototypeTestFastEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins32Generate_RegExpPrototypeTestFastEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins32Generate_RegExpPrototypeTestFastEPNS0_8compiler18CodeAssemblerStateE:
.LFB22450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$332, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$873, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L127
.L124:
	movq	%r13, %rdi
	call	_ZN2v88internal32RegExpPrototypeTestFastAssembler35GenerateRegExpPrototypeTestFastImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L128
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L124
.L128:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22450:
	.size	_ZN2v88internal8Builtins32Generate_RegExpPrototypeTestFastEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins32Generate_RegExpPrototypeTestFastEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal24UnsafeCast8JSRegExp_1468EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"../../deps/v8/../../deps/v8/src/builtins/base.tq"
	.section	.text._ZN2v88internal24UnsafeCast8JSRegExp_1468EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24UnsafeCast8JSRegExp_1468EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal24UnsafeCast8JSRegExp_1468EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal24UnsafeCast8JSRegExp_1468EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE:
.LFB22467:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-216(%rbp), %r14
	leaq	-688(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-600(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-696(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-656(%rbp), %rbx
	subq	$696, %rsp
	movq	%rsi, -736(%rbp)
	movq	%rdx, -720(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -696(%rbp)
	movq	%rdi, -656(%rbp)
	movl	$48, %edi
	movq	$0, -648(%rbp)
	movq	$0, -640(%rbp)
	movq	$0, -632(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -632(%rbp)
	movq	%rdx, -640(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -616(%rbp)
	movq	%rax, -648(%rbp)
	movq	$0, -624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	%rax, -464(%rbp)
	movq	$0, -440(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -456(%rbp)
	leaq	-408(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -440(%rbp)
	movq	%rdx, -448(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -424(%rbp)
	movq	%rax, -712(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	%rax, -272(%rbp)
	movq	$0, -248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -248(%rbp)
	movq	%rdx, -256(%rbp)
	xorl	%edx, %edx
	movq	%rax, -264(%rbp)
	movups	%xmm0, -232(%rbp)
	movq	$0, -240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-736(%rbp), %xmm1
	movaps	%xmm0, -688(%rbp)
	movhps	-720(%rbp), %xmm1
	movq	$0, -672(%rbp)
	movaps	%xmm1, -736(%rbp)
	call	_Znwm@PLT
	movdqa	-736(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -688(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L130
	call	_ZdlPv@PLT
.L130:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -592(%rbp)
	jne	.L198
.L131:
	cmpq	$0, -400(%rbp)
	leaq	-272(%rbp), %rbx
	jne	.L199
.L134:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L137
	call	_ZdlPv@PLT
.L137:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	16(%rax), %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L138
	call	_ZdlPv@PLT
.L138:
	movq	-256(%rbp), %rbx
	movq	-264(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L139
	.p2align 4,,10
	.p2align 3
.L143:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L140
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L143
.L141:
	movq	-264(%rbp), %r14
.L139:
	testq	%r14, %r14
	je	.L144
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L144:
	movq	-712(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L145
	call	_ZdlPv@PLT
.L145:
	movq	-448(%rbp), %rbx
	movq	-456(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L146
	.p2align 4,,10
	.p2align 3
.L150:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L147
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L150
.L148:
	movq	-456(%rbp), %r14
.L146:
	testq	%r14, %r14
	je	.L151
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L151:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L152
	call	_ZdlPv@PLT
.L152:
	movq	-640(%rbp), %rbx
	movq	-648(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L153
	.p2align 4,,10
	.p2align 3
.L157:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L154
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L157
.L155:
	movq	-648(%rbp), %r13
.L153:
	testq	%r13, %r13
	je	.L158
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L158:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L200
	addq	$696, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L157
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L140:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L143
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L147:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L150
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L198:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r15, %rsi
	movw	%di, (%rax)
	leaq	2(%rax), %rdx
	movq	%rbx, %rdi
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L132
	call	_ZdlPv@PLT
.L132:
	movq	(%rbx), %rax
	movl	$2790, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	%rcx, -736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm2
	movl	$24, %edi
	movq	-736(%rbp), %xmm0
	movq	%rbx, -64(%rbp)
	movq	$0, -672(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm3
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-464(%rbp), %rdi
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L133
	call	_ZdlPv@PLT
.L133:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L199:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-464(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rsi
	movb	$7, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L135
	call	_ZdlPv@PLT
.L135:
	movq	(%rbx), %rax
	movl	$19, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -720(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %edi
	movq	%rbx, -64(%rbp)
	movq	-736(%rbp), %xmm0
	movq	$0, -672(%rbp)
	leaq	-272(%rbp), %rbx
	movhps	-720(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movdqa	-80(%rbp), %xmm4
	leaq	24(%rax), %rdx
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L136
	call	_ZdlPv@PLT
.L136:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L134
.L200:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22467:
	.size	_ZN2v88internal24UnsafeCast8JSRegExp_1468EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal24UnsafeCast8JSRegExp_1468EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.section	.rodata._ZN2v88internal28RegExpPrototypeTestAssembler31GenerateRegExpPrototypeTestImplEv.str1.1,"aMS",@progbits,1
.LC4:
	.string	"RegExp.prototype.test"
	.section	.text._ZN2v88internal28RegExpPrototypeTestAssembler31GenerateRegExpPrototypeTestImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28RegExpPrototypeTestAssembler31GenerateRegExpPrototypeTestImplEv
	.type	_ZN2v88internal28RegExpPrototypeTestAssembler31GenerateRegExpPrototypeTestImplEv, @function
_ZN2v88internal28RegExpPrototypeTestAssembler31GenerateRegExpPrototypeTestImplEv:
.LFB22418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-2240(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2368(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$2552, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -2408(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -2528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	leaq	-2408(%rbp), %r12
	movq	%rax, -2544(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$72, %edi
	movq	$0, -2232(%rbp)
	movq	$0, -2224(%rbp)
	movq	%rax, %rbx
	movq	-2408(%rbp), %rax
	movq	$0, -2216(%rbp)
	movq	%rax, -2240(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -2232(%rbp)
	leaq	-2184(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2216(%rbp)
	movq	%rdx, -2224(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2200(%rbp)
	movq	%rax, -2424(%rbp)
	movq	$0, -2208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$96, %edi
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movq	%rax, -2048(%rbp)
	movq	$0, -2024(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -2040(%rbp)
	leaq	-1992(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2024(%rbp)
	movq	%rdx, -2032(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2008(%rbp)
	movq	%rax, -2496(%rbp)
	movq	$0, -2016(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1848(%rbp)
	movq	$0, -1840(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1832(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1848(%rbp)
	leaq	-1800(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1832(%rbp)
	movq	%rdx, -1840(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1816(%rbp)
	movq	%rax, -2432(%rbp)
	movq	$0, -1824(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1656(%rbp)
	movq	$0, -1648(%rbp)
	movq	%rax, -1664(%rbp)
	movq	$0, -1640(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1656(%rbp)
	leaq	-1608(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1640(%rbp)
	movq	%rdx, -1648(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1624(%rbp)
	movq	%rax, -2440(%rbp)
	movq	$0, -1632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1464(%rbp)
	movq	$0, -1456(%rbp)
	movq	%rax, -1472(%rbp)
	movq	$0, -1448(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1464(%rbp)
	leaq	-1416(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1448(%rbp)
	movq	%rdx, -1456(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1432(%rbp)
	movq	%rax, -2448(%rbp)
	movq	$0, -1440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1272(%rbp)
	movq	$0, -1264(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -1256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1272(%rbp)
	leaq	-1224(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1256(%rbp)
	movq	%rdx, -1264(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1240(%rbp)
	movq	%rax, -2456(%rbp)
	movq	$0, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1080(%rbp)
	movq	$0, -1072(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$0, -1064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1080(%rbp)
	leaq	-1032(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1064(%rbp)
	movq	%rdx, -1072(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1048(%rbp)
	movq	%rax, -2504(%rbp)
	movq	$0, -1056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$192, %edi
	movq	$0, -888(%rbp)
	movq	$0, -880(%rbp)
	movq	%rax, -896(%rbp)
	movq	$0, -872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -888(%rbp)
	leaq	-840(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -872(%rbp)
	movq	%rdx, -880(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -856(%rbp)
	movq	%rax, -2488(%rbp)
	movq	$0, -864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$120, %edi
	movq	$0, -696(%rbp)
	movq	$0, -688(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -696(%rbp)
	leaq	-648(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -680(%rbp)
	movq	%rdx, -688(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -664(%rbp)
	movq	%rax, -2464(%rbp)
	movq	$0, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$144, %edi
	movq	$0, -504(%rbp)
	movq	$0, -496(%rbp)
	movq	%rax, -512(%rbp)
	movq	$0, -488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -504(%rbp)
	leaq	-456(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -488(%rbp)
	movq	%rdx, -496(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -472(%rbp)
	movq	%rax, -2472(%rbp)
	movq	$0, -480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$120, %edi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -312(%rbp)
	leaq	-264(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	xorl	%edx, %edx
	movq	%rax, -2480(%rbp)
	movups	%xmm0, -280(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	-2528(%rbp), %xmm1
	movaps	%xmm0, -2368(%rbp)
	movhps	-2544(%rbp), %xmm1
	movq	%rbx, -112(%rbp)
	movaps	%xmm1, -128(%rbp)
	movq	$0, -2352(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm2
	leaq	24(%rax), %rdx
	movq	%rax, -2368(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L202
	call	_ZdlPv@PLT
.L202:
	movq	-2424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2176(%rbp)
	jne	.L451
	cmpq	$0, -1984(%rbp)
	jne	.L452
.L208:
	cmpq	$0, -1792(%rbp)
	jne	.L453
.L211:
	cmpq	$0, -1600(%rbp)
	jne	.L454
.L214:
	cmpq	$0, -1408(%rbp)
	jne	.L455
.L216:
	cmpq	$0, -1216(%rbp)
	jne	.L456
.L220:
	cmpq	$0, -1024(%rbp)
	jne	.L457
.L225:
	cmpq	$0, -832(%rbp)
	jne	.L458
.L228:
	cmpq	$0, -640(%rbp)
	jne	.L459
.L231:
	cmpq	$0, -448(%rbp)
	jne	.L460
.L233:
	cmpq	$0, -256(%rbp)
	jne	.L461
.L235:
	movq	-2480(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L238
	call	_ZdlPv@PLT
.L238:
	movq	-304(%rbp), %rbx
	movq	-312(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L239
	.p2align 4,,10
	.p2align 3
.L243:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L240
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L243
.L241:
	movq	-312(%rbp), %r13
.L239:
	testq	%r13, %r13
	je	.L244
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L244:
	movq	-2472(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L245
	call	_ZdlPv@PLT
.L245:
	movq	-496(%rbp), %rbx
	movq	-504(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L246
	.p2align 4,,10
	.p2align 3
.L250:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L247
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L250
.L248:
	movq	-504(%rbp), %r13
.L246:
	testq	%r13, %r13
	je	.L251
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L251:
	movq	-2464(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L252
	call	_ZdlPv@PLT
.L252:
	movq	-688(%rbp), %rbx
	movq	-696(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L253
	.p2align 4,,10
	.p2align 3
.L257:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L254
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L257
.L255:
	movq	-696(%rbp), %r13
.L253:
	testq	%r13, %r13
	je	.L258
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L258:
	movq	-2488(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-864(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L259
	call	_ZdlPv@PLT
.L259:
	movq	-880(%rbp), %rbx
	movq	-888(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L260
	.p2align 4,,10
	.p2align 3
.L264:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L261
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L264
.L262:
	movq	-888(%rbp), %r13
.L260:
	testq	%r13, %r13
	je	.L265
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L265:
	movq	-2504(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L266
	call	_ZdlPv@PLT
.L266:
	movq	-1072(%rbp), %rbx
	movq	-1080(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L267
	.p2align 4,,10
	.p2align 3
.L271:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L268
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L271
.L269:
	movq	-1080(%rbp), %r13
.L267:
	testq	%r13, %r13
	je	.L272
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L272:
	movq	-2456(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L273
	call	_ZdlPv@PLT
.L273:
	movq	-1264(%rbp), %rbx
	movq	-1272(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L274
	.p2align 4,,10
	.p2align 3
.L278:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L275
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L278
.L276:
	movq	-1272(%rbp), %r13
.L274:
	testq	%r13, %r13
	je	.L279
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L279:
	movq	-2448(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1440(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L280
	call	_ZdlPv@PLT
.L280:
	movq	-1456(%rbp), %rbx
	movq	-1464(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L281
	.p2align 4,,10
	.p2align 3
.L285:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L282
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L285
.L283:
	movq	-1464(%rbp), %r13
.L281:
	testq	%r13, %r13
	je	.L286
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L286:
	movq	-2440(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1632(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L287
	call	_ZdlPv@PLT
.L287:
	movq	-1648(%rbp), %rbx
	movq	-1656(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L288
	.p2align 4,,10
	.p2align 3
.L292:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L289
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L292
.L290:
	movq	-1656(%rbp), %r13
.L288:
	testq	%r13, %r13
	je	.L293
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L293:
	movq	-2432(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L294
	call	_ZdlPv@PLT
.L294:
	movq	-1840(%rbp), %rbx
	movq	-1848(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L295
	.p2align 4,,10
	.p2align 3
.L299:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L296
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L299
.L297:
	movq	-1848(%rbp), %r13
.L295:
	testq	%r13, %r13
	je	.L300
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L300:
	movq	-2496(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2016(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L301
	call	_ZdlPv@PLT
.L301:
	movq	-2032(%rbp), %rbx
	movq	-2040(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L302
	.p2align 4,,10
	.p2align 3
.L306:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L303
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L306
.L304:
	movq	-2040(%rbp), %r13
.L302:
	testq	%r13, %r13
	je	.L307
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L307:
	movq	-2424(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L308
	call	_ZdlPv@PLT
.L308:
	movq	-2224(%rbp), %rbx
	movq	-2232(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L309
	.p2align 4,,10
	.p2align 3
.L313:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L310
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L313
.L311:
	movq	-2232(%rbp), %r13
.L309:
	testq	%r13, %r13
	je	.L314
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L314:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L462
	addq	$2552, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L310:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L313
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L303:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L306
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L296:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L299
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L289:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L292
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L282:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L285
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L275:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L278
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L268:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L271
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L261:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L264
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L254:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L257
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L240:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L243
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L247:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L250
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L451:
	movq	-2424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2352(%rbp)
	movaps	%xmm0, -2368(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r8d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -2368(%rbp)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2368(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L204
	call	_ZdlPv@PLT
.L204:
	movq	(%rbx), %rax
	movl	$14, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %r14
	movq	(%rax), %rbx
	movq	16(%rax), %rax
	movq	%rax, -2528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rdx
	movq	%r13, %rcx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal21Cast10JSReceiver_1404EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm7
	movq	%rbx, %xmm4
	movq	-2528(%rbp), %xmm3
	punpcklqdq	%xmm7, %xmm4
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	%rax, -96(%rbp)
	punpcklqdq	%xmm7, %xmm3
	movaps	%xmm4, -2528(%rbp)
	leaq	-2400(%rbp), %r14
	movaps	%xmm3, -2544(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm0, -2400(%rbp)
	movq	$0, -2384(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm6
	leaq	40(%rax), %rdx
	leaq	-1856(%rbp), %rdi
	movq	%rax, -2400(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -2384(%rbp)
	movq	%rdx, -2392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L205
	call	_ZdlPv@PLT
.L205:
	movq	-2432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2360(%rbp)
	jne	.L463
.L206:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1984(%rbp)
	je	.L208
.L452:
	movq	-2496(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2048(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2352(%rbp)
	movaps	%xmm0, -2368(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2368(%rbp)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2368(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L209
	call	_ZdlPv@PLT
.L209:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm7
	movaps	%xmm0, -2368(%rbp)
	movq	$0, -2352(%rbp)
	movq	%rdx, -112(%rbp)
	movaps	%xmm7, -128(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-1664(%rbp), %rdi
	movq	%rax, -2368(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L210
	call	_ZdlPv@PLT
.L210:
	movq	-2440(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1792(%rbp)
	je	.L211
.L453:
	movq	-2432(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1856(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2352(%rbp)
	movaps	%xmm0, -2368(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2368(%rbp)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2368(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L212
	call	_ZdlPv@PLT
.L212:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	8(%rax), %rsi
	movq	16(%rax), %rcx
	movq	32(%rax), %rdx
	movq	(%rax), %rax
	movaps	%xmm0, -2368(%rbp)
	movq	%rsi, -120(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -2352(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-1472(%rbp), %rdi
	movq	%rax, -2368(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L213
	call	_ZdlPv@PLT
.L213:
	movq	-2448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1600(%rbp)
	je	.L214
.L454:
	movq	-2440(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1664(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2352(%rbp)
	movaps	%xmm0, -2368(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r14, %rdi
	movb	$8, 2(%rax)
	movq	%rax, -2368(%rbp)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2368(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L215
	call	_ZdlPv@PLT
.L215:
	movq	(%rbx), %rax
	movl	$15, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	movl	$62, %edx
	movq	%r14, %rsi
	leaq	.LC4(%rip), %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -1408(%rbp)
	je	.L216
.L455:
	movq	-2448(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1472(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2352(%rbp)
	movaps	%xmm0, -2368(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2368(%rbp)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2368(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L217
	call	_ZdlPv@PLT
.L217:
	movq	(%rbx), %rax
	movl	$16, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r14
	movq	24(%rax), %rax
	movq	%rcx, -2544(%rbp)
	movq	%rax, -2528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r14, -2560(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15ToString_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$17, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2528(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal26IsFastRegExpPermissive_327EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm6
	pxor	%xmm0, %xmm0
	movq	-2560(%rbp), %xmm5
	movhps	-2544(%rbp), %xmm6
	movl	$40, %edi
	movq	%r14, -96(%rbp)
	movhps	-2528(%rbp), %xmm5
	movaps	%xmm6, -2544(%rbp)
	movaps	%xmm5, -2528(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm0, -2368(%rbp)
	movq	%rax, -2576(%rbp)
	movq	$0, -2352(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm4
	leaq	40(%rax), %rdx
	leaq	-1280(%rbp), %rdi
	movq	%rax, -2368(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L218
	call	_ZdlPv@PLT
.L218:
	movdqa	-2544(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movdqa	-2528(%rbp), %xmm6
	movaps	%xmm0, -2368(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movq	%r14, -96(%rbp)
	movq	$0, -2352(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm5
	leaq	40(%rax), %rdx
	leaq	-320(%rbp), %rdi
	movq	%rax, -2368(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L219
	call	_ZdlPv@PLT
.L219:
	movq	-2480(%rbp), %rcx
	movq	-2456(%rbp), %rdx
	movq	%r12, %rdi
	movq	-2576(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1216(%rbp)
	je	.L220
.L456:
	movq	-2456(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1280(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2352(%rbp)
	movaps	%xmm0, -2368(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2368(%rbp)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2368(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L221
	call	_ZdlPv@PLT
.L221:
	movq	(%rbx), %rax
	movl	$19, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	24(%rax), %r14
	movq	%rcx, -2560(%rbp)
	movq	16(%rax), %rcx
	movq	32(%rax), %rax
	movq	%r14, -2512(%rbp)
	movq	%rcx, -2576(%rbp)
	movq	%rax, -2528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal24UnsafeCast8JSRegExp_1468EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	movl	$18, %edx
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rsi
	movq	%rax, -2544(%rbp)
	leaq	-2400(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %r8
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-2528(%rbp), %rcx
	movq	-2544(%rbp), %rdx
	call	_ZN2v88internal23RegExpBuiltinsAssembler40RegExpPrototypeExecBodyWithoutResultFastENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_8JSRegExpEEENS3_INS0_6StringEEEPNS2_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	movq	%rax, -2584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm3
	movl	$64, %edi
	movq	-2528(%rbp), %rax
	movq	-2576(%rbp), %xmm2
	movhps	-2560(%rbp), %xmm3
	movq	$0, -2384(%rbp)
	movq	%rax, %xmm0
	movq	%rax, %xmm7
	movaps	%xmm3, -2560(%rbp)
	movhps	-2584(%rbp), %xmm0
	movhps	-2544(%rbp), %xmm7
	movhps	-2512(%rbp), %xmm2
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm7, -2544(%rbp)
	movaps	%xmm2, -2576(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm0, -2400(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm4
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm7
	leaq	64(%rax), %rdx
	leaq	-896(%rbp), %rdi
	movups	%xmm6, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rax, -2400(%rbp)
	movq	%rdx, -2384(%rbp)
	movq	%rdx, -2392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L222
	call	_ZdlPv@PLT
.L222:
	movq	-2488(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2360(%rbp)
	jne	.L464
.L223:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1024(%rbp)
	je	.L225
.L457:
	movq	-2504(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1088(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2352(%rbp)
	movaps	%xmm0, -2368(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movq	%r14, %rdi
	movw	%si, 4(%rax)
	leaq	7(%rax), %rdx
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	movb	$7, 6(%rax)
	movq	%rax, -2368(%rbp)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2368(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L226
	call	_ZdlPv@PLT
.L226:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	32(%rax), %rdx
	movdqu	(%rax), %xmm5
	movq	$0, -2352(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm0, -2368(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm4
	leaq	40(%rax), %rdx
	leaq	-704(%rbp), %rdi
	movq	%rax, -2368(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L227
	call	_ZdlPv@PLT
.L227:
	movq	-2464(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -832(%rbp)
	je	.L228
.L458:
	movq	-2488(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-896(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2352(%rbp)
	movaps	%xmm0, -2368(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$506381209866602503, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2368(%rbp)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2368(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L229
	call	_ZdlPv@PLT
.L229:
	movq	(%rbx), %rax
	movl	$48, %edi
	movdqu	48(%rax), %xmm2
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movdqu	(%rax), %xmm7
	movq	$0, -2352(%rbp)
	shufpd	$2, %xmm2, %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -2368(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	leaq	48(%rax), %rdx
	leaq	-512(%rbp), %rdi
	movq	%rax, -2368(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L230
	call	_ZdlPv@PLT
.L230:
	movq	-2472(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -640(%rbp)
	je	.L231
.L459:
	movq	-2464(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-704(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2352(%rbp)
	movaps	%xmm0, -2368(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2368(%rbp)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L232
	call	_ZdlPv@PLT
.L232:
	movl	$20, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8False_66EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -448(%rbp)
	je	.L233
.L460:
	movq	-2472(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-512(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2352(%rbp)
	movaps	%xmm0, -2368(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -2368(%rbp)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L234
	call	_ZdlPv@PLT
.L234:
	movl	$18, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$21, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -256(%rbp)
	je	.L235
.L461:
	movq	-2480(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-320(%rbp), %rbx
	xorl	%r14d, %r14d
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2352(%rbp)
	movaps	%xmm0, -2368(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2368(%rbp)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2368(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L236
	call	_ZdlPv@PLT
.L236:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rsi
	movq	(%rax), %rdx
	movq	24(%rax), %r8
	movq	32(%rax), %rbx
	testq	%rdx, %rdx
	movq	%r8, -2528(%rbp)
	cmovne	%rdx, %r14
	movl	$23, %edx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	-2528(%rbp), %r8
	movq	%r8, %rdx
	call	_ZN2v88internal23RegExpBuiltinsAssembler10RegExpExecENS0_8compiler5TNodeINS0_7ContextEEEPNS2_4NodeES7_@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$24, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal7Null_63EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler21SelectBooleanConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L463:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-2528(%rbp), %xmm4
	movdqa	-2544(%rbp), %xmm2
	movaps	%xmm0, -2400(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movq	$0, -2384(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm2
	movq	%r14, %rsi
	leaq	32(%rax), %rdx
	leaq	-2048(%rbp), %rdi
	movq	%rax, -2400(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -2384(%rbp)
	movq	%rdx, -2392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L207
	call	_ZdlPv@PLT
.L207:
	movq	-2496(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L464:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2528(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-2560(%rbp), %xmm4
	movdqa	-2576(%rbp), %xmm2
	movdqa	-2544(%rbp), %xmm7
	movl	$56, %edi
	movaps	%xmm0, -2400(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -2384(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm5
	leaq	56(%rax), %rdx
	leaq	-1088(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rax, -2400(%rbp)
	movq	%rdx, -2384(%rbp)
	movq	%rdx, -2392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L224
	call	_ZdlPv@PLT
.L224:
	movq	-2504(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L223
.L462:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22418:
	.size	_ZN2v88internal28RegExpPrototypeTestAssembler31GenerateRegExpPrototypeTestImplEv, .-_ZN2v88internal28RegExpPrototypeTestAssembler31GenerateRegExpPrototypeTestImplEv
	.section	.rodata._ZN2v88internal8Builtins28Generate_RegExpPrototypeTestEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"RegExpPrototypeTest"
	.section	.text._ZN2v88internal8Builtins28Generate_RegExpPrototypeTestEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins28Generate_RegExpPrototypeTestEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins28Generate_RegExpPrototypeTestEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins28Generate_RegExpPrototypeTestEPNS0_8compiler18CodeAssemblerStateE:
.LFB22414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$146, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$872, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L469
.L466:
	movq	%r13, %rdi
	call	_ZN2v88internal28RegExpPrototypeTestAssembler31GenerateRegExpPrototypeTestImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L470
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L469:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L466
.L470:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22414:
	.size	_ZN2v88internal8Builtins28Generate_RegExpPrototypeTestEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins28Generate_RegExpPrototypeTestEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_RegExpPrototypeTestEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_RegExpPrototypeTestEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_RegExpPrototypeTestEPNS0_8compiler18CodeAssemblerStateE:
.LFB29092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29092:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_RegExpPrototypeTestEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_RegExpPrototypeTestEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_RegExpPrototypeTestEPNS0_8compiler18CodeAssemblerStateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
