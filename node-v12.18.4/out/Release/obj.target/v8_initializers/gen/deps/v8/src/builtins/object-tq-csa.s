	.file	"object-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.rodata._ZN2v88internal22ObjectIsExtensible_311EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../../deps/v8/../../deps/v8/src/builtins/object.tq"
	.section	.text._ZN2v88internal22ObjectIsExtensible_311EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22ObjectIsExtensible_311EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal22ObjectIsExtensible_311EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal22ObjectIsExtensible_311EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE:
.LFB22410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	leaq	-2208(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2336(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-2392(%rbp), %r12
	pushq	%rbx
	subq	$2488, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -2472(%rbp)
	movq	%rsi, -2512(%rbp)
	movq	%rdx, -2528(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -2392(%rbp)
	movq	%rdi, -2208(%rbp)
	movl	$48, %edi
	movq	$0, -2200(%rbp)
	movq	$0, -2192(%rbp)
	movq	$0, -2184(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -2200(%rbp)
	leaq	-2152(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2184(%rbp)
	movq	%rdx, -2192(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2168(%rbp)
	movq	%rax, -2408(%rbp)
	movq	$0, -2176(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$72, %edi
	movq	$0, -2008(%rbp)
	movq	$0, -2000(%rbp)
	movq	%rax, -2016(%rbp)
	movq	$0, -1992(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -2008(%rbp)
	leaq	-1960(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1992(%rbp)
	movq	%rdx, -2000(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1976(%rbp)
	movq	%rax, -2488(%rbp)
	movq	$0, -1984(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1816(%rbp)
	movq	$0, -1808(%rbp)
	movq	%rax, -1824(%rbp)
	movq	$0, -1800(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1816(%rbp)
	leaq	-1768(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1800(%rbp)
	movq	%rdx, -1808(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1784(%rbp)
	movq	%rax, -2424(%rbp)
	movq	$0, -1792(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$48, %edi
	movq	$0, -1624(%rbp)
	movq	$0, -1616(%rbp)
	movq	%rax, -1632(%rbp)
	movq	$0, -1608(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1624(%rbp)
	leaq	-1576(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1608(%rbp)
	movq	%rdx, -1616(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1592(%rbp)
	movq	%rax, -2432(%rbp)
	movq	$0, -1600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1432(%rbp)
	movq	$0, -1424(%rbp)
	movq	%rax, -1440(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1432(%rbp)
	leaq	-1384(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -2440(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -2480(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -2456(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$72, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -2464(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$96, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -2448(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$72, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -2416(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-2512(%rbp), %xmm1
	movaps	%xmm0, -2336(%rbp)
	movhps	-2528(%rbp), %xmm1
	movq	$0, -2320(%rbp)
	movaps	%xmm1, -2512(%rbp)
	call	_Znwm@PLT
	movdqa	-2512(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L8
	call	_ZdlPv@PLT
.L8:
	movq	-2408(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2144(%rbp)
	jne	.L262
	cmpq	$0, -1952(%rbp)
	jne	.L263
.L14:
	cmpq	$0, -1760(%rbp)
	jne	.L264
.L17:
	cmpq	$0, -1568(%rbp)
	jne	.L265
.L20:
	cmpq	$0, -1376(%rbp)
	jne	.L266
.L23:
	cmpq	$0, -1184(%rbp)
	jne	.L267
.L28:
	cmpq	$0, -992(%rbp)
	jne	.L268
.L31:
	cmpq	$0, -800(%rbp)
	jne	.L269
.L34:
	cmpq	$0, -608(%rbp)
	jne	.L270
.L37:
	cmpq	$0, -416(%rbp)
	leaq	-288(%rbp), %r15
	jne	.L271
.L40:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L43
	call	_ZdlPv@PLT
.L43:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L44
	call	_ZdlPv@PLT
.L44:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L45
	.p2align 4,,10
	.p2align 3
.L49:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L46
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L49
.L47:
	movq	-280(%rbp), %r14
.L45:
	testq	%r14, %r14
	je	.L50
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L50:
	movq	-2416(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L51
	call	_ZdlPv@PLT
.L51:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L52
	.p2align 4,,10
	.p2align 3
.L56:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L53
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L56
.L54:
	movq	-472(%rbp), %r14
.L52:
	testq	%r14, %r14
	je	.L57
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L57:
	movq	-2448(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L58
	call	_ZdlPv@PLT
.L58:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L59
	.p2align 4,,10
	.p2align 3
.L63:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L60
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L63
.L61:
	movq	-664(%rbp), %r14
.L59:
	testq	%r14, %r14
	je	.L64
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L64:
	movq	-2464(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L65
	call	_ZdlPv@PLT
.L65:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L66
	.p2align 4,,10
	.p2align 3
.L70:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L67
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L70
.L68:
	movq	-856(%rbp), %r14
.L66:
	testq	%r14, %r14
	je	.L71
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L71:
	movq	-2456(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L72
	call	_ZdlPv@PLT
.L72:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L73
	.p2align 4,,10
	.p2align 3
.L77:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L74
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L77
.L75:
	movq	-1048(%rbp), %r14
.L73:
	testq	%r14, %r14
	je	.L78
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L78:
	movq	-2480(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L79
	call	_ZdlPv@PLT
.L79:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L80
	.p2align 4,,10
	.p2align 3
.L84:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L81
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L84
.L82:
	movq	-1240(%rbp), %r14
.L80:
	testq	%r14, %r14
	je	.L85
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L85:
	movq	-2440(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L86
	call	_ZdlPv@PLT
.L86:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L87
	.p2align 4,,10
	.p2align 3
.L91:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L88
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L91
.L89:
	movq	-1432(%rbp), %r14
.L87:
	testq	%r14, %r14
	je	.L92
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L92:
	movq	-2432(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L93
	call	_ZdlPv@PLT
.L93:
	movq	-1616(%rbp), %rbx
	movq	-1624(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L94
	.p2align 4,,10
	.p2align 3
.L98:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L95
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L98
.L96:
	movq	-1624(%rbp), %r14
.L94:
	testq	%r14, %r14
	je	.L99
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L99:
	movq	-2424(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L100
	call	_ZdlPv@PLT
.L100:
	movq	-1808(%rbp), %rbx
	movq	-1816(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L101
	.p2align 4,,10
	.p2align 3
.L105:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L102
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L105
.L103:
	movq	-1816(%rbp), %r14
.L101:
	testq	%r14, %r14
	je	.L106
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L106:
	movq	-2488(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L107
	call	_ZdlPv@PLT
.L107:
	movq	-2000(%rbp), %rbx
	movq	-2008(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L108
	.p2align 4,,10
	.p2align 3
.L112:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L109
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L112
.L110:
	movq	-2008(%rbp), %r14
.L108:
	testq	%r14, %r14
	je	.L113
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L113:
	movq	-2408(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L114
	call	_ZdlPv@PLT
.L114:
	movq	-2192(%rbp), %rbx
	movq	-2200(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L115
	.p2align 4,,10
	.p2align 3
.L119:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L116
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L119
.L117:
	movq	-2200(%rbp), %r14
.L115:
	testq	%r14, %r14
	je	.L120
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L120:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L272
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L119
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L109:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L112
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L102:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L105
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L95:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L98
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L88:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L91
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L81:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L84
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L74:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L77
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L67:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L70
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L60:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L63
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L46:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L49
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L53:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L56
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L262:
	movq	-2408(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%r15, %rdi
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	movq	(%rbx), %rax
	movl	$32, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	-2472(%rbp), %rdi
	call	_ZN2v88internal21Cast10JSReceiver_1404EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %xmm5
	movq	%rbx, %xmm2
	pxor	%xmm0, %xmm0
	punpcklqdq	%xmm5, %xmm2
	movl	$32, %edi
	movaps	%xmm0, -2368(%rbp)
	leaq	-2368(%rbp), %rbx
	movaps	%xmm2, -2512(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	%r15, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -2352(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%rbx, %rsi
	leaq	32(%rax), %rdx
	leaq	-1824(%rbp), %rdi
	movq	%rax, -2368(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L11
	call	_ZdlPv@PLT
.L11:
	movq	-2424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2328(%rbp)
	jne	.L273
.L12:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1952(%rbp)
	je	.L14
.L263:
	movq	-2488(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2016(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L15
	call	_ZdlPv@PLT
.L15:
	movq	(%rbx), %rax
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movdqu	(%rax), %xmm0
	movaps	%xmm1, -2336(%rbp)
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2512(%rbp)
	call	_Znwm@PLT
	movdqa	-2512(%rbp), %xmm0
	leaq	-1632(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L16
	call	_ZdlPv@PLT
.L16:
	movq	-2432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1760(%rbp)
	je	.L17
.L264:
	movq	-2424(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1824(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L18
	call	_ZdlPv@PLT
.L18:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	24(%rax), %rdx
	movdqu	(%rax), %xmm5
	movaps	%xmm0, -2336(%rbp)
	movq	$0, -2320(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm5, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-1440(%rbp), %rdi
	movq	%rax, -2336(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L19
	call	_ZdlPv@PLT
.L19:
	movq	-2440(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1568(%rbp)
	je	.L20
.L265:
	movq	-2432(%rbp), %rsi
	movq	%r12, %rdi
	movl	$2055, %ebx
	leaq	-1632(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%bx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L21
	call	_ZdlPv@PLT
.L21:
	movq	(%rbx), %rax
	movq	-2472(%rbp), %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %r15
	call	_ZN2v88internal8False_66EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$24, %edi
	movq	$0, -2320(%rbp)
	movq	%r15, %xmm7
	movq	%rbx, %xmm0
	movq	%rax, -80(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -2336(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L22
	call	_ZdlPv@PLT
.L22:
	movq	-2416(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1376(%rbp)
	je	.L23
.L266:
	movq	-2440(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1440(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r11d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r11w, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L24
	call	_ZdlPv@PLT
.L24:
	movq	(%rbx), %rax
	movl	$33, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	16(%rax), %rbx
	movq	(%rax), %r15
	movq	%rcx, -2512(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2472(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	call	_ZN2v88internal16Cast7JSProxy_107EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm3
	movq	%r15, %xmm4
	pxor	%xmm0, %xmm0
	punpcklqdq	%xmm3, %xmm3
	movl	$40, %edi
	movq	%rax, -64(%rbp)
	movhps	-2512(%rbp), %xmm4
	movaps	%xmm3, -2528(%rbp)
	leaq	-2368(%rbp), %rbx
	movaps	%xmm4, -2512(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm0, -2368(%rbp)
	movq	$0, -2352(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rsi
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	leaq	40(%rax), %rdx
	leaq	-1056(%rbp), %rdi
	movq	%rax, -2368(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L25
	call	_ZdlPv@PLT
.L25:
	movq	-2456(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2328(%rbp)
	jne	.L274
.L26:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1184(%rbp)
	je	.L28
.L267:
	movq	-2480(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1248(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L29
	call	_ZdlPv@PLT
.L29:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm6
	movaps	%xmm0, -2336(%rbp)
	movq	$0, -2320(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm6, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -2336(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L30
	call	_ZdlPv@PLT
.L30:
	movq	-2464(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -992(%rbp)
	je	.L31
.L268:
	movq	-2456(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L32
	call	_ZdlPv@PLT
.L32:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	8(%rax), %rsi
	movq	16(%rax), %rcx
	movq	32(%rax), %rdx
	movq	(%rax), %rax
	movaps	%xmm0, -2336(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -2320(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -2336(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L33
	call	_ZdlPv@PLT
.L33:
	movq	-2448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L34
.L269:
	movq	-2464(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r10d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r10w, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L35
	call	_ZdlPv@PLT
.L35:
	movq	(%rbx), %rax
	movl	$34, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r15
	movq	%rcx, -2512(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2472(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	leaq	-96(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movl	$1, %r8d
	movl	$244, %esi
	movq	%r15, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r15, -80(%rbp)
	movhps	-2512(%rbp), %xmm0
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -2336(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L36
	call	_ZdlPv@PLT
.L36:
	movq	-2416(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -608(%rbp)
	je	.L37
.L270:
	movq	-2448(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L38
	call	_ZdlPv@PLT
.L38:
	movq	(%rbx), %rax
	movl	$35, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	leaq	-2384(%rbp), %r15
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	%rcx, -2512(%rbp)
	movq	24(%rax), %rcx
	movq	%rcx, -2528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2472(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$855, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-2336(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-96(%rbp), %rsi
	movl	$1, %edi
	movq	%rbx, %r9
	pushq	%rdi
	movq	%rax, %r8
	movq	%r15, %rdi
	movq	-2528(%rbp), %rcx
	pushq	%rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	leaq	-2368(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, -2368(%rbp)
	movq	-2320(%rbp), %rax
	movq	%rcx, -96(%rbp)
	movl	$1, %ecx
	movq	%rax, -2360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r15, %rdi
	movq	%rax, -2472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	-2472(%rbp), %rax
	movhps	-2512(%rbp), %xmm0
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2336(%rbp)
	movq	%rax, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -2336(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	popq	%r8
	popq	%r9
	testq	%rdi, %rdi
	je	.L39
	call	_ZdlPv@PLT
.L39:
	movq	-2416(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L271:
	movq	-2416(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%r15, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$8, 2(%rax)
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L41
	call	_ZdlPv@PLT
.L41:
	movq	(%rbx), %rax
	movl	$30, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r15
	movq	%rcx, -2472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r15, -80(%rbp)
	movhps	-2472(%rbp), %xmm0
	leaq	-288(%rbp), %r15
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm3
	leaq	24(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L42
	call	_ZdlPv@PLT
.L42:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L273:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movdqa	-2512(%rbp), %xmm4
	movaps	%xmm0, -2368(%rbp)
	movaps	%xmm4, -96(%rbp)
	movq	%r15, -80(%rbp)
	movq	$0, -2352(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%rbx, %rsi
	leaq	24(%rax), %rdx
	leaq	-2016(%rbp), %rdi
	movq	%rax, -2368(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L13
	call	_ZdlPv@PLT
.L13:
	movq	-2488(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L274:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-2512(%rbp), %xmm7
	movdqa	-2528(%rbp), %xmm5
	movaps	%xmm0, -2368(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movq	$0, -2352(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm4
	movq	%rbx, %rsi
	leaq	32(%rax), %rdx
	leaq	-1248(%rbp), %rdi
	movq	%rax, -2368(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L27
	call	_ZdlPv@PLT
.L27:
	movq	-2480(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L26
.L272:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22410:
	.size	_ZN2v88internal22ObjectIsExtensible_311EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal22ObjectIsExtensible_311EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal32ObjectPreventExtensionsThrow_312EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32ObjectPreventExtensionsThrow_312EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal32ObjectPreventExtensionsThrow_312EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal32ObjectPreventExtensionsThrow_312EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE:
.LFB22435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	leaq	-2208(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2336(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-2392(%rbp), %r12
	pushq	%rbx
	subq	$2504, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -2472(%rbp)
	movq	%rsi, -2512(%rbp)
	movq	%rdx, -2528(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -2392(%rbp)
	movq	%rdi, -2208(%rbp)
	movl	$48, %edi
	movq	$0, -2200(%rbp)
	movq	$0, -2192(%rbp)
	movq	$0, -2184(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -2200(%rbp)
	leaq	-2152(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2184(%rbp)
	movq	%rdx, -2192(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2168(%rbp)
	movq	%rax, -2408(%rbp)
	movq	$0, -2176(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$72, %edi
	movq	$0, -2008(%rbp)
	movq	$0, -2000(%rbp)
	movq	%rax, -2016(%rbp)
	movq	$0, -1992(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -2008(%rbp)
	leaq	-1960(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1992(%rbp)
	movq	%rdx, -2000(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1976(%rbp)
	movq	%rax, -2488(%rbp)
	movq	$0, -1984(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1816(%rbp)
	movq	$0, -1808(%rbp)
	movq	%rax, -1824(%rbp)
	movq	$0, -1800(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1816(%rbp)
	leaq	-1768(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1800(%rbp)
	movq	%rdx, -1808(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1784(%rbp)
	movq	%rax, -2424(%rbp)
	movq	$0, -1792(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$48, %edi
	movq	$0, -1624(%rbp)
	movq	$0, -1616(%rbp)
	movq	%rax, -1632(%rbp)
	movq	$0, -1608(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1624(%rbp)
	leaq	-1576(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1608(%rbp)
	movq	%rdx, -1616(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1592(%rbp)
	movq	%rax, -2432(%rbp)
	movq	$0, -1600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1432(%rbp)
	movq	$0, -1424(%rbp)
	movq	%rax, -1440(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1432(%rbp)
	leaq	-1384(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -2440(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -2480(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -2456(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$72, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -2464(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$96, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -2448(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$72, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -2416(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-2512(%rbp), %xmm1
	movaps	%xmm0, -2336(%rbp)
	movhps	-2528(%rbp), %xmm1
	movq	$0, -2320(%rbp)
	movaps	%xmm1, -2512(%rbp)
	call	_Znwm@PLT
	movdqa	-2512(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L276
	call	_ZdlPv@PLT
.L276:
	movq	-2408(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2144(%rbp)
	jne	.L530
	cmpq	$0, -1952(%rbp)
	jne	.L531
.L282:
	cmpq	$0, -1760(%rbp)
	jne	.L532
.L285:
	cmpq	$0, -1568(%rbp)
	jne	.L533
.L288:
	cmpq	$0, -1376(%rbp)
	jne	.L534
.L291:
	cmpq	$0, -1184(%rbp)
	jne	.L535
.L296:
	cmpq	$0, -992(%rbp)
	jne	.L536
.L299:
	cmpq	$0, -800(%rbp)
	jne	.L537
.L302:
	cmpq	$0, -608(%rbp)
	jne	.L538
.L305:
	cmpq	$0, -416(%rbp)
	leaq	-288(%rbp), %r15
	jne	.L539
.L308:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L311
	call	_ZdlPv@PLT
.L311:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L312
	call	_ZdlPv@PLT
.L312:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L313
	.p2align 4,,10
	.p2align 3
.L317:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L314
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L317
.L315:
	movq	-280(%rbp), %r14
.L313:
	testq	%r14, %r14
	je	.L318
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L318:
	movq	-2416(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L319
	call	_ZdlPv@PLT
.L319:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L320
	.p2align 4,,10
	.p2align 3
.L324:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L321
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L324
.L322:
	movq	-472(%rbp), %r14
.L320:
	testq	%r14, %r14
	je	.L325
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L325:
	movq	-2448(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L326
	call	_ZdlPv@PLT
.L326:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L327
	.p2align 4,,10
	.p2align 3
.L331:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L328
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L331
.L329:
	movq	-664(%rbp), %r14
.L327:
	testq	%r14, %r14
	je	.L332
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L332:
	movq	-2464(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L333
	call	_ZdlPv@PLT
.L333:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L334
	.p2align 4,,10
	.p2align 3
.L338:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L335
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L338
.L336:
	movq	-856(%rbp), %r14
.L334:
	testq	%r14, %r14
	je	.L339
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L339:
	movq	-2456(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L340
	call	_ZdlPv@PLT
.L340:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L341
	.p2align 4,,10
	.p2align 3
.L345:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L342
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L345
.L343:
	movq	-1048(%rbp), %r14
.L341:
	testq	%r14, %r14
	je	.L346
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L346:
	movq	-2480(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L347
	call	_ZdlPv@PLT
.L347:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L348
	.p2align 4,,10
	.p2align 3
.L352:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L349
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L352
.L350:
	movq	-1240(%rbp), %r14
.L348:
	testq	%r14, %r14
	je	.L353
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L353:
	movq	-2440(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L354
	call	_ZdlPv@PLT
.L354:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L355
	.p2align 4,,10
	.p2align 3
.L359:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L356
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L359
.L357:
	movq	-1432(%rbp), %r14
.L355:
	testq	%r14, %r14
	je	.L360
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L360:
	movq	-2432(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L361
	call	_ZdlPv@PLT
.L361:
	movq	-1616(%rbp), %rbx
	movq	-1624(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L362
	.p2align 4,,10
	.p2align 3
.L366:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L363
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L366
.L364:
	movq	-1624(%rbp), %r14
.L362:
	testq	%r14, %r14
	je	.L367
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L367:
	movq	-2424(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L368
	call	_ZdlPv@PLT
.L368:
	movq	-1808(%rbp), %rbx
	movq	-1816(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L369
	.p2align 4,,10
	.p2align 3
.L373:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L370
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L373
.L371:
	movq	-1816(%rbp), %r14
.L369:
	testq	%r14, %r14
	je	.L374
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L374:
	movq	-2488(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L375
	call	_ZdlPv@PLT
.L375:
	movq	-2000(%rbp), %rbx
	movq	-2008(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L376
	.p2align 4,,10
	.p2align 3
.L380:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L377
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L380
.L378:
	movq	-2008(%rbp), %r14
.L376:
	testq	%r14, %r14
	je	.L381
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L381:
	movq	-2408(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L382
	call	_ZdlPv@PLT
.L382:
	movq	-2192(%rbp), %rbx
	movq	-2200(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L383
	.p2align 4,,10
	.p2align 3
.L387:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L384
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L387
.L385:
	movq	-2200(%rbp), %r14
.L383:
	testq	%r14, %r14
	je	.L388
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L388:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L540
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L384:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L387
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L377:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L380
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L370:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L373
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L363:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L366
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L356:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L359
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L349:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L352
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L342:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L345
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L335:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L338
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L328:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L331
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L314:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L317
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L321:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L324
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L530:
	movq	-2408(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%r15, %rdi
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L278
	call	_ZdlPv@PLT
.L278:
	movq	(%rbx), %rax
	movl	$41, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	-2472(%rbp), %rdi
	call	_ZN2v88internal21Cast10JSReceiver_1404EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %xmm5
	movq	%rbx, %xmm2
	pxor	%xmm0, %xmm0
	punpcklqdq	%xmm5, %xmm2
	movl	$32, %edi
	movaps	%xmm0, -2368(%rbp)
	leaq	-2368(%rbp), %rbx
	movaps	%xmm2, -2512(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	%r15, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -2352(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%rbx, %rsi
	leaq	32(%rax), %rdx
	leaq	-1824(%rbp), %rdi
	movq	%rax, -2368(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L279
	call	_ZdlPv@PLT
.L279:
	movq	-2424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2328(%rbp)
	jne	.L541
.L280:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1952(%rbp)
	je	.L282
.L531:
	movq	-2488(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2016(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L283
	call	_ZdlPv@PLT
.L283:
	movq	(%rbx), %rax
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movdqu	(%rax), %xmm0
	movaps	%xmm1, -2336(%rbp)
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2512(%rbp)
	call	_Znwm@PLT
	movdqa	-2512(%rbp), %xmm0
	leaq	-1632(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L284
	call	_ZdlPv@PLT
.L284:
	movq	-2432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1760(%rbp)
	je	.L285
.L532:
	movq	-2424(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1824(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L286
	call	_ZdlPv@PLT
.L286:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	24(%rax), %rdx
	movdqu	(%rax), %xmm5
	movaps	%xmm0, -2336(%rbp)
	movq	$0, -2320(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm5, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-1440(%rbp), %rdi
	movq	%rax, -2336(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L287
	call	_ZdlPv@PLT
.L287:
	movq	-2440(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1568(%rbp)
	je	.L288
.L533:
	movq	-2432(%rbp), %rsi
	movq	%r12, %rdi
	movl	$2055, %ebx
	leaq	-1632(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%bx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L289
	call	_ZdlPv@PLT
.L289:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	8(%rax), %rdx
	movdqu	(%rax), %xmm7
	movaps	%xmm0, -2336(%rbp)
	movq	$0, -2320(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm7, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -2336(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L290
	call	_ZdlPv@PLT
.L290:
	movq	-2416(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1376(%rbp)
	je	.L291
.L534:
	movq	-2440(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1440(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r11d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r11w, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L292
	call	_ZdlPv@PLT
.L292:
	movq	(%rbx), %rax
	movl	$42, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	16(%rax), %rbx
	movq	(%rax), %r15
	movq	%rcx, -2512(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2472(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	call	_ZN2v88internal16Cast7JSProxy_107EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm3
	movq	%r15, %xmm4
	pxor	%xmm0, %xmm0
	punpcklqdq	%xmm3, %xmm3
	movl	$40, %edi
	movq	%rax, -64(%rbp)
	movhps	-2512(%rbp), %xmm4
	movaps	%xmm3, -2528(%rbp)
	leaq	-2368(%rbp), %rbx
	movaps	%xmm4, -2512(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm0, -2368(%rbp)
	movq	$0, -2352(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rsi
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	leaq	40(%rax), %rdx
	leaq	-1056(%rbp), %rdi
	movq	%rax, -2368(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L293
	call	_ZdlPv@PLT
.L293:
	movq	-2456(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2328(%rbp)
	jne	.L542
.L294:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1184(%rbp)
	je	.L296
.L535:
	movq	-2480(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1248(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L297
	call	_ZdlPv@PLT
.L297:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm6
	movaps	%xmm0, -2336(%rbp)
	movq	$0, -2320(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm6, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -2336(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L298
	call	_ZdlPv@PLT
.L298:
	movq	-2464(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -992(%rbp)
	je	.L299
.L536:
	movq	-2456(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L300
	call	_ZdlPv@PLT
.L300:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	8(%rax), %rsi
	movq	16(%rax), %rcx
	movq	32(%rax), %rdx
	movq	(%rax), %rax
	movaps	%xmm0, -2336(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -2320(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -2336(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L301
	call	_ZdlPv@PLT
.L301:
	movq	-2448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L302
.L537:
	movq	-2464(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r10d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r10w, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L303
	call	_ZdlPv@PLT
.L303:
	movq	(%rbx), %rax
	movl	$44, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r15
	movq	%rcx, -2512(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$43, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2472(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	leaq	-96(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movl	$1, %r8d
	movl	$231, %esi
	movq	%r15, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r15, -80(%rbp)
	movhps	-2512(%rbp), %xmm0
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -2336(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L304
	call	_ZdlPv@PLT
.L304:
	movq	-2416(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -608(%rbp)
	je	.L305
.L538:
	movq	-2448(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L306
	call	_ZdlPv@PLT
.L306:
	movq	(%rbx), %rax
	movl	$45, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	leaq	-2384(%rbp), %r15
	movq	16(%rax), %r10
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	24(%rax), %rax
	movq	%r10, -2536(%rbp)
	movq	%rcx, -2512(%rbp)
	movq	%rax, -2528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2472(%rbp), %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2472(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -2496(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$856, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-2336(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-96(%rbp), %rsi
	movl	$2, %edi
	movq	%rbx, %r9
	pushq	%rdi
	movq	%rax, %r8
	movq	%r15, %rdi
	movq	-2528(%rbp), %xmm0
	pushq	%rsi
	movq	-2320(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	xorl	%esi, %esi
	movhps	-2496(%rbp), %xmm0
	leaq	-2368(%rbp), %rdx
	movq	%rcx, -2368(%rbp)
	movl	$1, %ecx
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -2360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$46, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	-2536(%rbp), %r10
	movhps	-2512(%rbp), %xmm0
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	%r10, -80(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -2336(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	popq	%r8
	popq	%r9
	testq	%rdi, %rdi
	je	.L307
	call	_ZdlPv@PLT
.L307:
	movq	-2416(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L539:
	movq	-2416(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%r15, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$8, 2(%rax)
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L309
	call	_ZdlPv@PLT
.L309:
	movq	(%rbx), %rax
	movl	$38, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r15
	movq	%rcx, -2472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r15, -80(%rbp)
	movhps	-2472(%rbp), %xmm0
	leaq	-288(%rbp), %r15
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm3
	leaq	24(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L310
	call	_ZdlPv@PLT
.L310:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L541:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movdqa	-2512(%rbp), %xmm4
	movaps	%xmm0, -2368(%rbp)
	movaps	%xmm4, -96(%rbp)
	movq	%r15, -80(%rbp)
	movq	$0, -2352(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%rbx, %rsi
	leaq	24(%rax), %rdx
	leaq	-2016(%rbp), %rdi
	movq	%rax, -2368(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L281
	call	_ZdlPv@PLT
.L281:
	movq	-2488(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L542:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-2512(%rbp), %xmm7
	movdqa	-2528(%rbp), %xmm5
	movaps	%xmm0, -2368(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movq	$0, -2352(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm4
	movq	%rbx, %rsi
	leaq	32(%rax), %rdx
	leaq	-1248(%rbp), %rdi
	movq	%rax, -2368(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L295
	call	_ZdlPv@PLT
.L295:
	movq	-2480(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L294
.L540:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22435:
	.size	_ZN2v88internal32ObjectPreventExtensionsThrow_312EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal32ObjectPreventExtensionsThrow_312EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal36ObjectPreventExtensionsDontThrow_313EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal36ObjectPreventExtensionsDontThrow_313EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal36ObjectPreventExtensionsDontThrow_313EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal36ObjectPreventExtensionsDontThrow_313EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE:
.LFB22436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-2208(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2336(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-2392(%rbp), %r12
	pushq	%rbx
	subq	$2488, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -2512(%rbp)
	movq	%rdx, -2528(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -2392(%rbp)
	movq	%rdi, -2208(%rbp)
	movl	$48, %edi
	movq	$0, -2200(%rbp)
	movq	$0, -2192(%rbp)
	movq	$0, -2184(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -2200(%rbp)
	leaq	-2152(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2184(%rbp)
	movq	%rdx, -2192(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2168(%rbp)
	movq	%rax, -2408(%rbp)
	movq	$0, -2176(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$72, %edi
	movq	$0, -2008(%rbp)
	movq	$0, -2000(%rbp)
	movq	%rax, -2016(%rbp)
	movq	$0, -1992(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -2008(%rbp)
	leaq	-1960(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1992(%rbp)
	movq	%rdx, -2000(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1976(%rbp)
	movq	%rax, -2488(%rbp)
	movq	$0, -1984(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1816(%rbp)
	movq	$0, -1808(%rbp)
	movq	%rax, -1824(%rbp)
	movq	$0, -1800(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1816(%rbp)
	leaq	-1768(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1800(%rbp)
	movq	%rdx, -1808(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1784(%rbp)
	movq	%rax, -2432(%rbp)
	movq	$0, -1792(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$48, %edi
	movq	$0, -1624(%rbp)
	movq	$0, -1616(%rbp)
	movq	%rax, -1632(%rbp)
	movq	$0, -1608(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1624(%rbp)
	leaq	-1576(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1608(%rbp)
	movq	%rdx, -1616(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1592(%rbp)
	movq	%rax, -2440(%rbp)
	movq	$0, -1600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1432(%rbp)
	movq	$0, -1424(%rbp)
	movq	%rax, -1440(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1432(%rbp)
	leaq	-1384(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -2448(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -2480(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -2464(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$72, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -2472(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$96, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -2456(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$72, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -2424(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -280(%rbp)
	leaq	-232(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -2416(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-2512(%rbp), %xmm1
	movaps	%xmm0, -2336(%rbp)
	movhps	-2528(%rbp), %xmm1
	movq	$0, -2320(%rbp)
	movaps	%xmm1, -2512(%rbp)
	call	_Znwm@PLT
	movdqa	-2512(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L544
	call	_ZdlPv@PLT
.L544:
	movq	-2408(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2144(%rbp)
	jne	.L798
	cmpq	$0, -1952(%rbp)
	jne	.L799
.L550:
	cmpq	$0, -1760(%rbp)
	jne	.L800
.L553:
	cmpq	$0, -1568(%rbp)
	jne	.L801
.L556:
	cmpq	$0, -1376(%rbp)
	jne	.L802
.L559:
	cmpq	$0, -1184(%rbp)
	jne	.L803
.L564:
	cmpq	$0, -992(%rbp)
	jne	.L804
.L567:
	cmpq	$0, -800(%rbp)
	jne	.L805
.L570:
	cmpq	$0, -608(%rbp)
	jne	.L806
.L573:
	cmpq	$0, -416(%rbp)
	leaq	-288(%rbp), %r14
	jne	.L807
.L576:
	movq	-2416(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L579
	call	_ZdlPv@PLT
.L579:
	movq	(%rbx), %rax
	movq	-2416(%rbp), %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L580
	call	_ZdlPv@PLT
.L580:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L581
	.p2align 4,,10
	.p2align 3
.L585:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L582
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L585
.L583:
	movq	-280(%rbp), %r14
.L581:
	testq	%r14, %r14
	je	.L586
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L586:
	movq	-2424(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L587
	call	_ZdlPv@PLT
.L587:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L588
	.p2align 4,,10
	.p2align 3
.L592:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L589
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L592
.L590:
	movq	-472(%rbp), %r14
.L588:
	testq	%r14, %r14
	je	.L593
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L593:
	movq	-2456(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L594
	call	_ZdlPv@PLT
.L594:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L595
	.p2align 4,,10
	.p2align 3
.L599:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L596
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L599
.L597:
	movq	-664(%rbp), %r14
.L595:
	testq	%r14, %r14
	je	.L600
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L600:
	movq	-2472(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L601
	call	_ZdlPv@PLT
.L601:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L602
	.p2align 4,,10
	.p2align 3
.L606:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L603
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L606
.L604:
	movq	-856(%rbp), %r14
.L602:
	testq	%r14, %r14
	je	.L607
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L607:
	movq	-2464(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L608
	call	_ZdlPv@PLT
.L608:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L609
	.p2align 4,,10
	.p2align 3
.L613:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L610
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L613
.L611:
	movq	-1048(%rbp), %r14
.L609:
	testq	%r14, %r14
	je	.L614
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L614:
	movq	-2480(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L615
	call	_ZdlPv@PLT
.L615:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L616
	.p2align 4,,10
	.p2align 3
.L620:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L617
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L620
.L618:
	movq	-1240(%rbp), %r14
.L616:
	testq	%r14, %r14
	je	.L621
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L621:
	movq	-2448(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L622
	call	_ZdlPv@PLT
.L622:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L623
	.p2align 4,,10
	.p2align 3
.L627:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L624
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L627
.L625:
	movq	-1432(%rbp), %r14
.L623:
	testq	%r14, %r14
	je	.L628
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L628:
	movq	-2440(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L629
	call	_ZdlPv@PLT
.L629:
	movq	-1616(%rbp), %rbx
	movq	-1624(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L630
	.p2align 4,,10
	.p2align 3
.L634:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L631
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L634
.L632:
	movq	-1624(%rbp), %r14
.L630:
	testq	%r14, %r14
	je	.L635
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L635:
	movq	-2432(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L636
	call	_ZdlPv@PLT
.L636:
	movq	-1808(%rbp), %rbx
	movq	-1816(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L637
	.p2align 4,,10
	.p2align 3
.L641:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L638
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L641
.L639:
	movq	-1816(%rbp), %r14
.L637:
	testq	%r14, %r14
	je	.L642
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L642:
	movq	-2488(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L643
	call	_ZdlPv@PLT
.L643:
	movq	-2000(%rbp), %rbx
	movq	-2008(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L644
	.p2align 4,,10
	.p2align 3
.L648:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L645
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L648
.L646:
	movq	-2008(%rbp), %r14
.L644:
	testq	%r14, %r14
	je	.L649
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L649:
	movq	-2408(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L650
	call	_ZdlPv@PLT
.L650:
	movq	-2192(%rbp), %rbx
	movq	-2200(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L651
	.p2align 4,,10
	.p2align 3
.L655:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L652
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L655
.L653:
	movq	-2200(%rbp), %r14
.L651:
	testq	%r14, %r14
	je	.L656
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L656:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L808
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L652:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L655
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L645:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L648
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L638:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L641
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L631:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L634
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L624:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L627
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L617:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L620
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L610:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L613
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L603:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L606
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L596:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L599
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L582:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L585
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L589:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L592
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L798:
	movq	-2408(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%r15, %rdi
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L546
	call	_ZdlPv@PLT
.L546:
	movq	(%rbx), %rax
	movl	$52, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal21Cast10JSReceiver_1404EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %xmm5
	movq	%rbx, %xmm2
	pxor	%xmm0, %xmm0
	punpcklqdq	%xmm5, %xmm2
	movl	$32, %edi
	movaps	%xmm0, -2368(%rbp)
	leaq	-2368(%rbp), %rbx
	movaps	%xmm2, -2512(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	%r15, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -2352(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%rbx, %rsi
	leaq	32(%rax), %rdx
	leaq	-1824(%rbp), %rdi
	movq	%rax, -2368(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L547
	call	_ZdlPv@PLT
.L547:
	movq	-2432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2328(%rbp)
	jne	.L809
.L548:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1952(%rbp)
	je	.L550
.L799:
	movq	-2488(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2016(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L551
	call	_ZdlPv@PLT
.L551:
	movq	(%rbx), %rax
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movdqu	(%rax), %xmm0
	movaps	%xmm1, -2336(%rbp)
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2512(%rbp)
	call	_Znwm@PLT
	movdqa	-2512(%rbp), %xmm0
	leaq	-1632(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L552
	call	_ZdlPv@PLT
.L552:
	movq	-2440(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1760(%rbp)
	je	.L553
.L800:
	movq	-2432(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1824(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L554
	call	_ZdlPv@PLT
.L554:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	24(%rax), %rdx
	movdqu	(%rax), %xmm5
	movaps	%xmm0, -2336(%rbp)
	movq	$0, -2320(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm5, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-1440(%rbp), %rdi
	movq	%rax, -2336(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L555
	call	_ZdlPv@PLT
.L555:
	movq	-2448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1568(%rbp)
	je	.L556
.L801:
	movq	-2440(%rbp), %rsi
	movq	%r12, %rdi
	movl	$2055, %ebx
	leaq	-1632(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%bx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L557
	call	_ZdlPv@PLT
.L557:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %r15
	call	_ZN2v88internal8False_66EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$24, %edi
	movq	$0, -2320(%rbp)
	movq	%r15, %xmm7
	movq	%rbx, %xmm0
	movq	%rax, -80(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -2336(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L558
	call	_ZdlPv@PLT
.L558:
	movq	-2424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1376(%rbp)
	je	.L559
.L802:
	movq	-2448(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1440(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r11d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r11w, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L560
	call	_ZdlPv@PLT
.L560:
	movq	(%rbx), %rax
	movl	$53, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	16(%rax), %rbx
	movq	(%rax), %r15
	movq	%rcx, -2512(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal16Cast7JSProxy_107EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm3
	movq	%r15, %xmm4
	pxor	%xmm0, %xmm0
	punpcklqdq	%xmm3, %xmm3
	movl	$40, %edi
	movq	%rax, -64(%rbp)
	movhps	-2512(%rbp), %xmm4
	movaps	%xmm3, -2528(%rbp)
	leaq	-2368(%rbp), %rbx
	movaps	%xmm4, -2512(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm0, -2368(%rbp)
	movq	$0, -2352(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rsi
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	leaq	40(%rax), %rdx
	leaq	-1056(%rbp), %rdi
	movq	%rax, -2368(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L561
	call	_ZdlPv@PLT
.L561:
	movq	-2464(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2328(%rbp)
	jne	.L810
.L562:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1184(%rbp)
	je	.L564
.L803:
	movq	-2480(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1248(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L565
	call	_ZdlPv@PLT
.L565:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm6
	movaps	%xmm0, -2336(%rbp)
	movq	$0, -2320(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm6, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -2336(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L566
	call	_ZdlPv@PLT
.L566:
	movq	-2472(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -992(%rbp)
	je	.L567
.L804:
	movq	-2464(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L568
	call	_ZdlPv@PLT
.L568:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	8(%rax), %rsi
	movq	16(%rax), %rcx
	movq	32(%rax), %rdx
	movq	(%rax), %rax
	movaps	%xmm0, -2336(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -2320(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -2336(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L569
	call	_ZdlPv@PLT
.L569:
	movq	-2456(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L570
.L805:
	movq	-2472(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r10d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r10w, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L571
	call	_ZdlPv@PLT
.L571:
	movq	(%rbx), %rax
	movl	$55, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r15
	movq	%rcx, -2512(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$54, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	leaq	-96(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movl	$1, %r8d
	movl	$230, %esi
	movq	%r15, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r15, -80(%rbp)
	movhps	-2512(%rbp), %xmm0
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -2336(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L572
	call	_ZdlPv@PLT
.L572:
	movq	-2424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -608(%rbp)
	je	.L573
.L806:
	movq	-2456(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L574
	call	_ZdlPv@PLT
.L574:
	movq	(%rbx), %rax
	movl	$56, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	leaq	-2384(%rbp), %r15
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rcx, -2512(%rbp)
	movq	%rax, -2528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8False_66EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, -2496(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$856, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-2336(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-96(%rbp), %rsi
	movl	$2, %edi
	movq	%rbx, %r9
	pushq	%rdi
	movq	%rax, %r8
	movq	%r15, %rdi
	movq	-2528(%rbp), %xmm0
	pushq	%rsi
	movq	-2320(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	xorl	%esi, %esi
	movhps	-2496(%rbp), %xmm0
	leaq	-2368(%rbp), %rdx
	movq	%rcx, -2368(%rbp)
	movl	$1, %ecx
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -2360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r14, -80(%rbp)
	movhps	-2512(%rbp), %xmm0
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -2336(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	popq	%r8
	popq	%r9
	testq	%rdi, %rdi
	je	.L575
	call	_ZdlPv@PLT
.L575:
	movq	-2424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L807:
	movq	-2424(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%r14, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$8, 2(%rax)
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L577
	call	_ZdlPv@PLT
.L577:
	movq	(%rbx), %rax
	movl	$49, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %r14
	movq	(%rax), %rbx
	movq	16(%rax), %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %xmm3
	movq	%rbx, %xmm0
	movl	$24, %edi
	punpcklqdq	%xmm3, %xmm0
	movq	%r15, -80(%rbp)
	leaq	-288(%rbp), %r14
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2336(%rbp)
	movq	$0, -2320(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm4
	leaq	24(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L578
	call	_ZdlPv@PLT
.L578:
	movq	-2416(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L809:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movdqa	-2512(%rbp), %xmm6
	movaps	%xmm0, -2368(%rbp)
	movaps	%xmm6, -96(%rbp)
	movq	%r15, -80(%rbp)
	movq	$0, -2352(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%rbx, %rsi
	leaq	24(%rax), %rdx
	leaq	-2016(%rbp), %rdi
	movq	%rax, -2368(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L549
	call	_ZdlPv@PLT
.L549:
	movq	-2488(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L810:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-2512(%rbp), %xmm5
	movdqa	-2528(%rbp), %xmm2
	movaps	%xmm0, -2368(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm2, -80(%rbp)
	movq	$0, -2352(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%rbx, %rsi
	leaq	32(%rax), %rdx
	leaq	-1248(%rbp), %rdi
	movq	%rax, -2368(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L563
	call	_ZdlPv@PLT
.L563:
	movq	-2480(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L562
.L808:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22436:
	.size	_ZN2v88internal36ObjectPreventExtensionsDontThrow_313EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal36ObjectPreventExtensionsDontThrow_313EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal28JSReceiverGetPrototypeOf_315EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28JSReceiverGetPrototypeOf_315EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEE
	.type	_ZN2v88internal28JSReceiverGetPrototypeOf_315EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEE, @function
_ZN2v88internal28JSReceiverGetPrototypeOf_315EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEE:
.LFB22438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-1384(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1568(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1624(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1440(%rbp), %rbx
	subq	$1688, %rsp
	movq	%rsi, -1696(%rbp)
	movq	%rdx, -1712(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1624(%rbp)
	movq	%rdi, -1440(%rbp)
	movl	$48, %edi
	movq	$0, -1432(%rbp)
	movq	$0, -1424(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r15, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -1432(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1624(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -1680(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1624(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1656(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1624(%rbp), %rax
	movl	$48, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1672(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1624(%rbp), %rax
	movl	$72, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1664(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1624(%rbp), %rax
	movl	$72, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -1648(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1624(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -280(%rbp)
	leaq	-232(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -1640(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1696(%rbp), %xmm1
	movaps	%xmm0, -1568(%rbp)
	movhps	-1712(%rbp), %xmm1
	movq	$0, -1552(%rbp)
	movaps	%xmm1, -1696(%rbp)
	call	_Znwm@PLT
	movdqa	-1696(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1568(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L812
	call	_ZdlPv@PLT
.L812:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1376(%rbp)
	jne	.L973
	cmpq	$0, -1184(%rbp)
	jne	.L974
.L818:
	cmpq	$0, -992(%rbp)
	jne	.L975
.L821:
	cmpq	$0, -800(%rbp)
	jne	.L976
.L824:
	cmpq	$0, -608(%rbp)
	jne	.L977
.L827:
	cmpq	$0, -416(%rbp)
	leaq	-288(%rbp), %r14
	jne	.L978
.L830:
	movq	-1640(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L833
	call	_ZdlPv@PLT
.L833:
	movq	(%rbx), %rax
	movq	-1640(%rbp), %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L834
	call	_ZdlPv@PLT
.L834:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L835
	.p2align 4,,10
	.p2align 3
.L839:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L836
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L839
.L837:
	movq	-280(%rbp), %r14
.L835:
	testq	%r14, %r14
	je	.L840
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L840:
	movq	-1648(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L841
	call	_ZdlPv@PLT
.L841:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L842
	.p2align 4,,10
	.p2align 3
.L846:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L843
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L846
.L844:
	movq	-472(%rbp), %r14
.L842:
	testq	%r14, %r14
	je	.L847
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L847:
	movq	-1664(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L848
	call	_ZdlPv@PLT
.L848:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L849
	.p2align 4,,10
	.p2align 3
.L853:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L850
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L853
.L851:
	movq	-664(%rbp), %r14
.L849:
	testq	%r14, %r14
	je	.L854
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L854:
	movq	-1672(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L855
	call	_ZdlPv@PLT
.L855:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L856
	.p2align 4,,10
	.p2align 3
.L860:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L857
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L860
.L858:
	movq	-856(%rbp), %r14
.L856:
	testq	%r14, %r14
	je	.L861
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L861:
	movq	-1656(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L862
	call	_ZdlPv@PLT
.L862:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L863
	.p2align 4,,10
	.p2align 3
.L867:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L864
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L867
.L865:
	movq	-1048(%rbp), %r14
.L863:
	testq	%r14, %r14
	je	.L868
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L868:
	movq	-1680(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L869
	call	_ZdlPv@PLT
.L869:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L870
	.p2align 4,,10
	.p2align 3
.L874:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L871
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L874
.L872:
	movq	-1240(%rbp), %r14
.L870:
	testq	%r14, %r14
	je	.L875
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L875:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L876
	call	_ZdlPv@PLT
.L876:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L877
	.p2align 4,,10
	.p2align 3
.L881:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L878
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L881
.L879:
	movq	-1432(%rbp), %r14
.L877:
	testq	%r14, %r14
	je	.L882
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L882:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L979
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L878:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L881
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L871:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L874
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L864:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L867
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L857:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L860
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L850:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L853
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L836:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L839
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L843:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L846
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L973:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L814
	call	_ZdlPv@PLT
.L814:
	movq	(%rbx), %rax
	movl	$68, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	%rcx, -1696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1696(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal16Cast7JSProxy_107EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1696(%rbp), %rcx
	movq	%rbx, %xmm2
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	%rax, -72(%rbp)
	leaq	-1600(%rbp), %rbx
	movq	%rcx, %xmm7
	movq	%rcx, -80(%rbp)
	punpcklqdq	%xmm7, %xmm2
	movaps	%xmm0, -1600(%rbp)
	movaps	%xmm2, -1712(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	$0, -1584(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movq	%rbx, %rsi
	leaq	32(%rax), %rdx
	leaq	-1056(%rbp), %rdi
	movq	%rax, -1600(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L815
	call	_ZdlPv@PLT
.L815:
	movq	-1656(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1560(%rbp)
	jne	.L980
.L816:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1184(%rbp)
	je	.L818
.L974:
	movq	-1680(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1248(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L819
	call	_ZdlPv@PLT
.L819:
	movq	(%rbx), %rax
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movdqu	(%rax), %xmm0
	movaps	%xmm1, -1568(%rbp)
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1696(%rbp)
	call	_Znwm@PLT
	movdqa	-1696(%rbp), %xmm0
	leaq	-864(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1568(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L820
	call	_ZdlPv@PLT
.L820:
	movq	-1672(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -992(%rbp)
	je	.L821
.L975:
	movq	-1656(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L822
	call	_ZdlPv@PLT
.L822:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	24(%rax), %rdx
	movdqu	(%rax), %xmm5
	movaps	%xmm0, -1568(%rbp)
	movq	$0, -1552(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm5, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -1568(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L823
	call	_ZdlPv@PLT
.L823:
	movq	-1664(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L824
.L976:
	movq	-1672(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r11d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r11w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L825
	call	_ZdlPv@PLT
.L825:
	movq	(%rbx), %rax
	movl	$69, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -1696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	leaq	-96(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	-1696(%rbp), %rax
	movl	$1, %r8d
	movl	$232, %esi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rdi
	movq	%rax, -1712(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	-1712(%rbp), %rax
	movhps	-1696(%rbp), %xmm0
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1568(%rbp)
	movq	%rax, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -1568(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L826
	call	_ZdlPv@PLT
.L826:
	movq	-1648(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -608(%rbp)
	je	.L827
.L977:
	movq	-1664(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r10d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r10w, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L828
	call	_ZdlPv@PLT
.L828:
	movq	(%rbx), %rax
	movl	$70, %edx
	movq	%r12, %rdi
	leaq	-1616(%rbp), %rbx
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	movq	%rcx, -1696(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -1712(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rcx, -1720(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$853, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-1568(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-96(%rbp), %rsi
	movl	$1, %edi
	movq	-1720(%rbp), %rcx
	pushq	%rdi
	movq	%rax, %r8
	movq	%rbx, %rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rsi
	movq	-1696(%rbp), %r9
	leaq	-1600(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, -1600(%rbp)
	movq	-1552(%rbp), %rax
	movq	%rcx, -96(%rbp)
	movl	$1, %ecx
	movq	%rax, -1592(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$24, %edi
	movq	%r14, -80(%rbp)
	movq	-1696(%rbp), %xmm0
	movq	$0, -1552(%rbp)
	movhps	-1712(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm3
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -1568(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	popq	%r8
	popq	%r9
	testq	%rdi, %rdi
	je	.L829
	call	_ZdlPv@PLT
.L829:
	movq	-1648(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L978:
	movq	-1648(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movq	%r14, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$8, 2(%rax)
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L831
	call	_ZdlPv@PLT
.L831:
	movq	(%rbx), %rax
	movl	$65, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r14
	movq	%rcx, -1696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r14, -80(%rbp)
	movhps	-1696(%rbp), %xmm0
	leaq	-288(%rbp), %r14
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm4
	leaq	24(%rax), %rdx
	movq	%rax, -1568(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L832
	call	_ZdlPv@PLT
.L832:
	movq	-1640(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L980:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1696(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1712(%rbp), %xmm5
	movl	$24, %edi
	movaps	%xmm0, -1600(%rbp)
	movaps	%xmm5, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1584(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%rbx, %rsi
	leaq	24(%rax), %rdx
	leaq	-1248(%rbp), %rdi
	movq	%rax, -1600(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L817
	call	_ZdlPv@PLT
.L817:
	movq	-1680(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L816
.L979:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22438:
	.size	_ZN2v88internal28JSReceiverGetPrototypeOf_315EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEE, .-_ZN2v88internal28JSReceiverGetPrototypeOf_315EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEE
	.section	.text._ZN2v88internal24ObjectGetPrototypeOf_314EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24ObjectGetPrototypeOf_314EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal24ObjectGetPrototypeOf_314EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal24ObjectGetPrototypeOf_314EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE:
.LFB22437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-600(%rbp), %r14
	leaq	-216(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-688(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-696(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$712, %rsp
	movq	%rsi, -736(%rbp)
	movq	%rdx, -744(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -696(%rbp)
	movq	%rdi, -656(%rbp)
	movl	$48, %edi
	movq	$0, -648(%rbp)
	movq	$0, -640(%rbp)
	movq	$0, -632(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -648(%rbp)
	leaq	-656(%rbp), %rax
	movq	%rdx, -632(%rbp)
	movq	%rdx, -640(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -616(%rbp)
	movq	%rax, -720(%rbp)
	movq	$0, -624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	%rax, -464(%rbp)
	movq	$0, -440(%rbp)
	call	_Znwm@PLT
	leaq	-408(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -440(%rbp)
	movq	%rdx, -448(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -712(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -424(%rbp)
	movq	%rax, -456(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	%rax, -272(%rbp)
	movq	$0, -248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -248(%rbp)
	movq	%rdx, -256(%rbp)
	xorl	%edx, %edx
	movq	%rax, -264(%rbp)
	movups	%xmm0, -232(%rbp)
	movq	$0, -240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-736(%rbp), %xmm1
	movaps	%xmm0, -688(%rbp)
	movhps	-744(%rbp), %xmm1
	movq	$0, -672(%rbp)
	movaps	%xmm1, -736(%rbp)
	call	_Znwm@PLT
	movdqa	-736(%rbp), %xmm1
	movq	-720(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -688(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L982
	call	_ZdlPv@PLT
.L982:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -592(%rbp)
	jne	.L1050
.L983:
	cmpq	$0, -400(%rbp)
	leaq	-272(%rbp), %rbx
	jne	.L1051
.L986:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L989
	call	_ZdlPv@PLT
.L989:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L990
	call	_ZdlPv@PLT
.L990:
	movq	-256(%rbp), %rbx
	movq	-264(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L991
	.p2align 4,,10
	.p2align 3
.L995:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L992
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L995
.L993:
	movq	-264(%rbp), %r15
.L991:
	testq	%r15, %r15
	je	.L996
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L996:
	movq	-712(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L997
	call	_ZdlPv@PLT
.L997:
	movq	-448(%rbp), %rbx
	movq	-456(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L998
	.p2align 4,,10
	.p2align 3
.L1002:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L999
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L1002
.L1000:
	movq	-456(%rbp), %r15
.L998:
	testq	%r15, %r15
	je	.L1003
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1003:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1004
	call	_ZdlPv@PLT
.L1004:
	movq	-640(%rbp), %rbx
	movq	-648(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1005
	.p2align 4,,10
	.p2align 3
.L1009:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1006
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1009
.L1007:
	movq	-648(%rbp), %r14
.L1005:
	testq	%r14, %r14
	je	.L1010
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1010:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1052
	addq	$712, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1006:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1009
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L992:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L995
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L999:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L1002
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L1050:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	movq	-720(%rbp), %rdi
	leaq	2(%rax), %rdx
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L984
	movq	%rax, -720(%rbp)
	call	_ZdlPv@PLT
	movq	-720(%rbp), %rax
.L984:
	movq	(%rax), %rax
	movl	$61, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rcx, -720(%rbp)
	movq	%rax, -736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-736(%rbp), %rdx
	movq	-720(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ToObject_InlineENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -744(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$62, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-744(%rbp), %r8
	movq	-720(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal28JSReceiverGetPrototypeOf_315EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEE
	movl	$24, %edi
	movq	-720(%rbp), %xmm0
	movq	$0, -672(%rbp)
	movq	%rax, -64(%rbp)
	movhps	-736(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm2
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-464(%rbp), %rdi
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L985
	call	_ZdlPv@PLT
.L985:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L983
	.p2align 4,,10
	.p2align 3
.L1051:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-464(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$8, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L987
	call	_ZdlPv@PLT
.L987:
	movq	(%rbx), %rax
	movl	$59, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -736(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rcx, -720(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %edi
	movq	%rbx, -64(%rbp)
	movq	-720(%rbp), %xmm0
	movq	$0, -672(%rbp)
	leaq	-272(%rbp), %rbx
	movhps	-736(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	-80(%rbp), %xmm3
	leaq	24(%rax), %rdx
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L988
	call	_ZdlPv@PLT
.L988:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L986
.L1052:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22437:
	.size	_ZN2v88internal24ObjectGetPrototypeOf_314EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal24ObjectGetPrototypeOf_314EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal29ObjectSetPrototypeOfThrow_316EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_10HeapObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29ObjectSetPrototypeOfThrow_316EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_10HeapObjectEEE
	.type	_ZN2v88internal29ObjectSetPrototypeOfThrow_316EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_10HeapObjectEEE, @function
_ZN2v88internal29ObjectSetPrototypeOfThrow_316EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_10HeapObjectEEE:
.LFB22454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-2168(%rbp), %r15
	leaq	-2224(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-2408(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$2536, %rsp
	movq	%rdi, -2488(%rbp)
	movq	%rsi, -2528(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -2408(%rbp)
	movq	%rdi, -2224(%rbp)
	movl	$72, %edi
	movq	$0, -2216(%rbp)
	movq	$0, -2208(%rbp)
	movq	$0, -2200(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -2200(%rbp)
	movq	%rdx, -2208(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2184(%rbp)
	movq	%rax, -2216(%rbp)
	movq	$0, -2192(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$96, %edi
	movq	$0, -2024(%rbp)
	movq	$0, -2016(%rbp)
	movq	%rax, -2032(%rbp)
	movq	$0, -2008(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -2024(%rbp)
	leaq	-1976(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2008(%rbp)
	movq	%rdx, -2016(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1992(%rbp)
	movq	%rax, -2496(%rbp)
	movq	$0, -2000(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1832(%rbp)
	movq	$0, -1824(%rbp)
	movq	%rax, -1840(%rbp)
	movq	$0, -1816(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1832(%rbp)
	leaq	-1784(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1816(%rbp)
	movq	%rdx, -1824(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1800(%rbp)
	movq	%rax, -2448(%rbp)
	movq	$0, -1808(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1640(%rbp)
	movq	$0, -1632(%rbp)
	movq	%rax, -1648(%rbp)
	movq	$0, -1624(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1640(%rbp)
	leaq	-1592(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1624(%rbp)
	movq	%rdx, -1632(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1608(%rbp)
	movq	%rax, -2456(%rbp)
	movq	$0, -1616(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1448(%rbp)
	movq	$0, -1440(%rbp)
	movq	%rax, -1456(%rbp)
	movq	$0, -1432(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1448(%rbp)
	leaq	-1400(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1432(%rbp)
	movq	%rdx, -1440(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1416(%rbp)
	movq	%rax, -2440(%rbp)
	movq	$0, -1424(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1256(%rbp)
	movq	$0, -1248(%rbp)
	movq	%rax, -1264(%rbp)
	movq	$0, -1240(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1256(%rbp)
	leaq	-1208(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1240(%rbp)
	movq	%rdx, -1248(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1224(%rbp)
	movq	%rax, -2504(%rbp)
	movq	$0, -1232(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1064(%rbp)
	movq	$0, -1056(%rbp)
	movq	%rax, -1072(%rbp)
	movq	$0, -1048(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1064(%rbp)
	leaq	-1016(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1048(%rbp)
	movq	%rdx, -1056(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1032(%rbp)
	movq	%rax, -2480(%rbp)
	movq	$0, -1040(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$96, %edi
	movq	$0, -872(%rbp)
	movq	$0, -864(%rbp)
	movq	%rax, -880(%rbp)
	movq	$0, -856(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -872(%rbp)
	leaq	-824(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -856(%rbp)
	movq	%rdx, -864(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -840(%rbp)
	movq	%rax, -2464(%rbp)
	movq	$0, -848(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$120, %edi
	movq	$0, -680(%rbp)
	movq	$0, -672(%rbp)
	movq	%rax, -688(%rbp)
	movq	$0, -664(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -680(%rbp)
	leaq	-632(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -664(%rbp)
	movq	%rdx, -672(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -648(%rbp)
	movq	%rax, -2472(%rbp)
	movq	$0, -656(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$96, %edi
	movq	$0, -488(%rbp)
	movq	$0, -480(%rbp)
	movq	%rax, -496(%rbp)
	movq	$0, -472(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -488(%rbp)
	leaq	-440(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -472(%rbp)
	movq	%rdx, -480(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -456(%rbp)
	movq	%rax, -2432(%rbp)
	movq	$0, -464(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$96, %edi
	movq	$0, -296(%rbp)
	movq	$0, -288(%rbp)
	movq	%rax, -304(%rbp)
	movq	$0, -280(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -296(%rbp)
	leaq	-248(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -280(%rbp)
	movq	%rdx, -288(%rbp)
	xorl	%edx, %edx
	movq	%rax, -2424(%rbp)
	movups	%xmm0, -264(%rbp)
	movq	$0, -272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2528(%rbp), %r9
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	%r13, -104(%rbp)
	leaq	-2352(%rbp), %r13
	movq	%r9, -112(%rbp)
	movaps	%xmm0, -2352(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -2336(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm1
	leaq	24(%rax), %rdx
	movq	%rax, -2352(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm1, (%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1054
	call	_ZdlPv@PLT
.L1054:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2160(%rbp)
	jne	.L1308
	cmpq	$0, -1968(%rbp)
	jne	.L1309
.L1060:
	cmpq	$0, -1776(%rbp)
	jne	.L1310
.L1063:
	cmpq	$0, -1584(%rbp)
	jne	.L1311
.L1066:
	cmpq	$0, -1392(%rbp)
	jne	.L1312
.L1069:
	cmpq	$0, -1200(%rbp)
	jne	.L1313
.L1074:
	cmpq	$0, -1008(%rbp)
	jne	.L1314
.L1077:
	cmpq	$0, -816(%rbp)
	jne	.L1315
.L1080:
	cmpq	$0, -624(%rbp)
	jne	.L1316
.L1083:
	cmpq	$0, -432(%rbp)
	leaq	-304(%rbp), %r14
	jne	.L1317
.L1086:
	movq	-2424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134678535, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1089
	call	_ZdlPv@PLT
.L1089:
	movq	(%rbx), %rax
	movq	-2424(%rbp), %rdi
	movq	24(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1090
	call	_ZdlPv@PLT
.L1090:
	movq	-288(%rbp), %rbx
	movq	-296(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1091
	.p2align 4,,10
	.p2align 3
.L1095:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1092
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1095
.L1093:
	movq	-296(%rbp), %r14
.L1091:
	testq	%r14, %r14
	je	.L1096
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1096:
	movq	-2432(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-464(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1097
	call	_ZdlPv@PLT
.L1097:
	movq	-480(%rbp), %rbx
	movq	-488(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1098
	.p2align 4,,10
	.p2align 3
.L1102:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1099
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1102
.L1100:
	movq	-488(%rbp), %r14
.L1098:
	testq	%r14, %r14
	je	.L1103
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1103:
	movq	-2472(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1104
	call	_ZdlPv@PLT
.L1104:
	movq	-672(%rbp), %rbx
	movq	-680(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1105
	.p2align 4,,10
	.p2align 3
.L1109:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1106
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%rbx, %r14
	jne	.L1109
.L1107:
	movq	-680(%rbp), %r14
.L1105:
	testq	%r14, %r14
	je	.L1110
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1110:
	movq	-2464(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-848(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1111
	call	_ZdlPv@PLT
.L1111:
	movq	-864(%rbp), %rbx
	movq	-872(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1112
	.p2align 4,,10
	.p2align 3
.L1116:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1113
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1116
.L1114:
	movq	-872(%rbp), %r14
.L1112:
	testq	%r14, %r14
	je	.L1117
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1117:
	movq	-2480(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1040(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1118
	call	_ZdlPv@PLT
.L1118:
	movq	-1056(%rbp), %rbx
	movq	-1064(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1119
	.p2align 4,,10
	.p2align 3
.L1123:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1120
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1123
.L1121:
	movq	-1064(%rbp), %r14
.L1119:
	testq	%r14, %r14
	je	.L1124
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1124:
	movq	-2504(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1125
	call	_ZdlPv@PLT
.L1125:
	movq	-1248(%rbp), %rbx
	movq	-1256(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1126
	.p2align 4,,10
	.p2align 3
.L1130:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1127
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1130
.L1128:
	movq	-1256(%rbp), %r14
.L1126:
	testq	%r14, %r14
	je	.L1131
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1131:
	movq	-2440(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1132
	call	_ZdlPv@PLT
.L1132:
	movq	-1440(%rbp), %rbx
	movq	-1448(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1133
	.p2align 4,,10
	.p2align 3
.L1137:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1134
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1137
.L1135:
	movq	-1448(%rbp), %r14
.L1133:
	testq	%r14, %r14
	je	.L1138
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1138:
	movq	-2456(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1616(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1139
	call	_ZdlPv@PLT
.L1139:
	movq	-1632(%rbp), %rbx
	movq	-1640(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1140
	.p2align 4,,10
	.p2align 3
.L1144:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1141
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1144
.L1142:
	movq	-1640(%rbp), %r14
.L1140:
	testq	%r14, %r14
	je	.L1145
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1145:
	movq	-2448(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1808(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1146
	call	_ZdlPv@PLT
.L1146:
	movq	-1824(%rbp), %rbx
	movq	-1832(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1147
	.p2align 4,,10
	.p2align 3
.L1151:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1148
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1151
.L1149:
	movq	-1832(%rbp), %r14
.L1147:
	testq	%r14, %r14
	je	.L1152
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1152:
	movq	-2496(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2000(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1153
	call	_ZdlPv@PLT
.L1153:
	movq	-2016(%rbp), %rbx
	movq	-2024(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1154
	.p2align 4,,10
	.p2align 3
.L1158:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1155
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1158
.L1156:
	movq	-2024(%rbp), %r14
.L1154:
	testq	%r14, %r14
	je	.L1159
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1159:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1160
	call	_ZdlPv@PLT
.L1160:
	movq	-2208(%rbp), %rbx
	movq	-2216(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1161
	.p2align 4,,10
	.p2align 3
.L1165:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1162
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1165
.L1163:
	movq	-2216(%rbp), %r14
.L1161:
	testq	%r14, %r14
	je	.L1166
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1166:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1318
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1162:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1165
	jmp	.L1163
	.p2align 4,,10
	.p2align 3
.L1155:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1158
	jmp	.L1156
	.p2align 4,,10
	.p2align 3
.L1148:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1151
	jmp	.L1149
	.p2align 4,,10
	.p2align 3
.L1141:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1144
	jmp	.L1142
	.p2align 4,,10
	.p2align 3
.L1134:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1137
	jmp	.L1135
	.p2align 4,,10
	.p2align 3
.L1127:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1130
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1120:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1123
	jmp	.L1121
	.p2align 4,,10
	.p2align 3
.L1113:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1116
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1106:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1109
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1092:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1095
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1099:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1102
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1308:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r14, %rdi
	movb	$7, 2(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1056
	call	_ZdlPv@PLT
.L1056:
	movq	(%rbx), %rax
	movl	$76, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %r14
	movq	(%rax), %rbx
	movq	16(%rax), %rax
	movq	%rax, -2528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rdx
	movq	%r13, %rcx
	movq	%rbx, %rsi
	movq	-2488(%rbp), %rdi
	call	_ZN2v88internal21Cast10JSReceiver_1404EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm6
	movq	%rbx, %xmm3
	movq	-2528(%rbp), %xmm2
	punpcklqdq	%xmm6, %xmm3
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	%rax, -80(%rbp)
	punpcklqdq	%xmm6, %xmm2
	movaps	%xmm3, -2528(%rbp)
	leaq	-2384(%rbp), %r14
	movaps	%xmm2, -2544(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -2384(%rbp)
	movq	$0, -2368(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	40(%rax), %rdx
	leaq	-1840(%rbp), %rdi
	movq	%rax, -2384(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2368(%rbp)
	movq	%rdx, -2376(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1057
	call	_ZdlPv@PLT
.L1057:
	movq	-2448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2344(%rbp)
	jne	.L1319
.L1058:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1968(%rbp)
	je	.L1060
.L1309:
	movq	-2496(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2032(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134678535, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1061
	call	_ZdlPv@PLT
.L1061:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm6
	movaps	%xmm0, -2352(%rbp)
	movq	$0, -2336(%rbp)
	movq	%rdx, -96(%rbp)
	movaps	%xmm6, -112(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-112(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-1648(%rbp), %rdi
	movq	%rax, -2352(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1062
	call	_ZdlPv@PLT
.L1062:
	movq	-2456(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1776(%rbp)
	je	.L1063
.L1310:
	movq	-2448(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1840(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134678535, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1064
	call	_ZdlPv@PLT
.L1064:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	8(%rax), %rsi
	movq	16(%rax), %rcx
	movq	32(%rax), %rdx
	movq	(%rax), %rax
	movaps	%xmm0, -2352(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rax, -112(%rbp)
	movq	$0, -2336(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-1456(%rbp), %rdi
	movq	%rax, -2352(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1065
	call	_ZdlPv@PLT
.L1065:
	movq	-2440(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1584(%rbp)
	je	.L1066
.L1311:
	movq	-2456(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1648(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%r14, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$7, 2(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1067
	call	_ZdlPv@PLT
.L1067:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	8(%rax), %rdx
	movq	16(%rax), %rcx
	movq	(%rax), %rax
	movaps	%xmm0, -2352(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rax, -112(%rbp)
	movq	$0, -2336(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm1
	movdqa	-96(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-496(%rbp), %rdi
	movq	%rax, -2352(%rbp)
	movups	%xmm1, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1068
	call	_ZdlPv@PLT
.L1068:
	movq	-2432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1392(%rbp)
	je	.L1069
.L1312:
	movq	-2440(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1456(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1070
	call	_ZdlPv@PLT
.L1070:
	movq	(%rbx), %rax
	movl	$77, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	24(%rax), %rbx
	movq	16(%rax), %r14
	movq	%rsi, -2544(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rcx, -2528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2488(%rbp), %rdi
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal16Cast7JSProxy_107EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm7
	movq	%r14, %xmm4
	movq	-2528(%rbp), %xmm5
	punpcklqdq	%xmm7, %xmm4
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movq	%rbx, -80(%rbp)
	movhps	-2544(%rbp), %xmm5
	movaps	%xmm4, -2560(%rbp)
	leaq	-2384(%rbp), %r14
	movaps	%xmm5, -2528(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm0, -2384(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -2368(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm1
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	leaq	48(%rax), %rdx
	leaq	-1072(%rbp), %rdi
	movq	%rax, -2384(%rbp)
	movups	%xmm1, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rdx, -2368(%rbp)
	movq	%rdx, -2376(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1071
	call	_ZdlPv@PLT
.L1071:
	movq	-2480(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2344(%rbp)
	jne	.L1320
.L1072:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1200(%rbp)
	je	.L1074
.L1313:
	movq	-2504(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1264(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1075
	call	_ZdlPv@PLT
.L1075:
	movq	(%rbx), %rax
	movl	$32, %edi
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm7
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-880(%rbp), %rdi
	movq	%rax, -2352(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1076
	call	_ZdlPv@PLT
.L1076:
	movq	-2464(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1008(%rbp)
	je	.L1077
.L1314:
	movq	-2480(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1072(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1078
	call	_ZdlPv@PLT
.L1078:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	40(%rax), %rdx
	movdqu	(%rax), %xmm6
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -80(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm1
	movdqa	-96(%rbp), %xmm4
	leaq	40(%rax), %rdx
	leaq	-688(%rbp), %rdi
	movq	%rax, -2352(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm1, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1079
	call	_ZdlPv@PLT
.L1079:
	movq	-2472(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -816(%rbp)
	je	.L1080
.L1315:
	movq	-2464(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-880(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1081
	call	_ZdlPv@PLT
.L1081:
	movq	(%rbx), %rax
	movl	$79, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	24(%rax), %r14
	movq	%rcx, -2528(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -2544(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$78, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2488(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %xmm0
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	-2544(%rbp), %r14
	leaq	-112(%rbp), %rcx
	movl	$2, %r8d
	movl	$234, %esi
	movq	%r14, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rdi
	movq	%rax, -2544(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm0
	movl	$32, %edi
	movq	$0, -2336(%rbp)
	movhps	-2528(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%r14, %xmm0
	movhps	-2544(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-496(%rbp), %rdi
	movq	%rax, -2352(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1082
	call	_ZdlPv@PLT
.L1082:
	movq	-2432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -624(%rbp)
	je	.L1083
.L1316:
	movq	-2472(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-688(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1084
	call	_ZdlPv@PLT
.L1084:
	movq	(%rbx), %rax
	movl	$80, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	leaq	-2400(%rbp), %r14
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	%rcx, -2544(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -2528(%rbp)
	movq	24(%rax), %rcx
	movq	32(%rax), %rax
	movq	%rcx, -2560(%rbp)
	movq	%rax, -2512(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2488(%rbp), %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2488(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -2568(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$860, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-2352(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	leaq	-112(%rbp), %rsi
	movq	%rbx, %r9
	movl	$3, %edi
	movq	%rcx, -2384(%rbp)
	movq	%rax, %r8
	movq	-2568(%rbp), %rcx
	pushq	%rdi
	movq	-2336(%rbp), %rax
	movq	%r14, %rdi
	leaq	-2384(%rbp), %rdx
	movq	-2512(%rbp), %xmm0
	pushq	%rsi
	xorl	%esi, %esi
	movq	%rcx, -96(%rbp)
	movl	$1, %ecx
	movhps	-2528(%rbp), %xmm0
	movq	%rax, -2376(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$81, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$32, %edi
	movq	$0, -2336(%rbp)
	movhps	-2544(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-2528(%rbp), %xmm0
	movhps	-2560(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-496(%rbp), %rdi
	movq	%rax, -2352(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L1085
	call	_ZdlPv@PLT
.L1085:
	movq	-2432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L1317:
	movq	-2432(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-496(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134678535, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1087
	call	_ZdlPv@PLT
.L1087:
	movq	(%rbx), %rax
	movl	$73, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	16(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rcx, -2488(%rbp)
	movq	%rax, -2528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %xmm0
	movl	$32, %edi
	movq	$0, -2336(%rbp)
	movhps	-2488(%rbp), %xmm0
	leaq	-304(%rbp), %r14
	movaps	%xmm0, -112(%rbp)
	movq	%rbx, %xmm0
	movhps	-2528(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm6
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm1
	leaq	32(%rax), %rdx
	movq	%rax, -2352(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm1, 16(%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1088
	call	_ZdlPv@PLT
.L1088:
	movq	-2424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1086
	.p2align 4,,10
	.p2align 3
.L1319:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-2528(%rbp), %xmm4
	movdqa	-2544(%rbp), %xmm5
	movaps	%xmm0, -2384(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movq	$0, -2368(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm2
	movq	%r14, %rsi
	leaq	32(%rax), %rdx
	leaq	-2032(%rbp), %rdi
	movq	%rax, -2384(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -2368(%rbp)
	movq	%rdx, -2376(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1059
	call	_ZdlPv@PLT
.L1059:
	movq	-2496(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1058
	.p2align 4,,10
	.p2align 3
.L1320:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movdqa	-2528(%rbp), %xmm3
	movdqa	-2560(%rbp), %xmm6
	movq	%rbx, -80(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm0, -2384(%rbp)
	movq	$0, -2368(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm1
	movdqa	-96(%rbp), %xmm4
	leaq	40(%rax), %rdx
	leaq	-1264(%rbp), %rdi
	movq	%rax, -2384(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm1, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2368(%rbp)
	movq	%rdx, -2376(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1073
	call	_ZdlPv@PLT
.L1073:
	movq	-2504(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1072
.L1318:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22454:
	.size	_ZN2v88internal29ObjectSetPrototypeOfThrow_316EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_10HeapObjectEEE, .-_ZN2v88internal29ObjectSetPrototypeOfThrow_316EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_10HeapObjectEEE
	.section	.text._ZN2v88internal33ObjectSetPrototypeOfDontThrow_317EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_10HeapObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33ObjectSetPrototypeOfDontThrow_317EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_10HeapObjectEEE
	.type	_ZN2v88internal33ObjectSetPrototypeOfDontThrow_317EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_10HeapObjectEEE, @function
_ZN2v88internal33ObjectSetPrototypeOfDontThrow_317EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_10HeapObjectEEE:
.LFB22476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-2168(%rbp), %r15
	leaq	-2224(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-2408(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$2520, %rsp
	movq	%rdi, -2440(%rbp)
	movq	%rsi, -2528(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -2408(%rbp)
	movq	%rdi, -2224(%rbp)
	movl	$72, %edi
	movq	$0, -2216(%rbp)
	movq	$0, -2208(%rbp)
	movq	$0, -2200(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -2200(%rbp)
	movq	%rdx, -2208(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2184(%rbp)
	movq	%rax, -2216(%rbp)
	movq	$0, -2192(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$96, %edi
	movq	$0, -2024(%rbp)
	movq	$0, -2016(%rbp)
	movq	%rax, -2032(%rbp)
	movq	$0, -2008(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -2024(%rbp)
	leaq	-1976(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2008(%rbp)
	movq	%rdx, -2016(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1992(%rbp)
	movq	%rax, -2496(%rbp)
	movq	$0, -2000(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1832(%rbp)
	movq	$0, -1824(%rbp)
	movq	%rax, -1840(%rbp)
	movq	$0, -1816(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1832(%rbp)
	leaq	-1784(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1816(%rbp)
	movq	%rdx, -1824(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1800(%rbp)
	movq	%rax, -2456(%rbp)
	movq	$0, -1808(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1640(%rbp)
	movq	$0, -1632(%rbp)
	movq	%rax, -1648(%rbp)
	movq	$0, -1624(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1640(%rbp)
	leaq	-1592(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1624(%rbp)
	movq	%rdx, -1632(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1608(%rbp)
	movq	%rax, -2464(%rbp)
	movq	$0, -1616(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1448(%rbp)
	movq	$0, -1440(%rbp)
	movq	%rax, -1456(%rbp)
	movq	$0, -1432(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1448(%rbp)
	leaq	-1400(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1432(%rbp)
	movq	%rdx, -1440(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1416(%rbp)
	movq	%rax, -2448(%rbp)
	movq	$0, -1424(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1256(%rbp)
	movq	$0, -1248(%rbp)
	movq	%rax, -1264(%rbp)
	movq	$0, -1240(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1256(%rbp)
	leaq	-1208(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1240(%rbp)
	movq	%rdx, -1248(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1224(%rbp)
	movq	%rax, -2504(%rbp)
	movq	$0, -1232(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1064(%rbp)
	movq	$0, -1056(%rbp)
	movq	%rax, -1072(%rbp)
	movq	$0, -1048(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1064(%rbp)
	leaq	-1016(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1048(%rbp)
	movq	%rdx, -1056(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1032(%rbp)
	movq	%rax, -2488(%rbp)
	movq	$0, -1040(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$96, %edi
	movq	$0, -872(%rbp)
	movq	$0, -864(%rbp)
	movq	%rax, -880(%rbp)
	movq	$0, -856(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -872(%rbp)
	leaq	-824(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -856(%rbp)
	movq	%rdx, -864(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -840(%rbp)
	movq	%rax, -2472(%rbp)
	movq	$0, -848(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$120, %edi
	movq	$0, -680(%rbp)
	movq	$0, -672(%rbp)
	movq	%rax, -688(%rbp)
	movq	$0, -664(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -680(%rbp)
	leaq	-632(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -664(%rbp)
	movq	%rdx, -672(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -648(%rbp)
	movq	%rax, -2480(%rbp)
	movq	$0, -656(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$96, %edi
	movq	$0, -488(%rbp)
	movq	$0, -480(%rbp)
	movq	%rax, -496(%rbp)
	movq	$0, -472(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -488(%rbp)
	leaq	-440(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -472(%rbp)
	movq	%rdx, -480(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -456(%rbp)
	movq	%rax, -2432(%rbp)
	movq	$0, -464(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rax
	movl	$96, %edi
	movq	$0, -296(%rbp)
	movq	$0, -288(%rbp)
	movq	%rax, -304(%rbp)
	movq	$0, -280(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -296(%rbp)
	leaq	-248(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -280(%rbp)
	movq	%rdx, -288(%rbp)
	xorl	%edx, %edx
	movq	%rax, -2424(%rbp)
	movups	%xmm0, -264(%rbp)
	movq	$0, -272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2528(%rbp), %r9
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	%r13, -104(%rbp)
	leaq	-2352(%rbp), %r13
	movq	%r9, -112(%rbp)
	movaps	%xmm0, -2352(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -2336(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm1
	leaq	24(%rax), %rdx
	movq	%rax, -2352(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm1, (%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1322
	call	_ZdlPv@PLT
.L1322:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2160(%rbp)
	jne	.L1576
	cmpq	$0, -1968(%rbp)
	jne	.L1577
.L1328:
	cmpq	$0, -1776(%rbp)
	jne	.L1578
.L1331:
	cmpq	$0, -1584(%rbp)
	jne	.L1579
.L1334:
	cmpq	$0, -1392(%rbp)
	jne	.L1580
.L1337:
	cmpq	$0, -1200(%rbp)
	jne	.L1581
.L1342:
	cmpq	$0, -1008(%rbp)
	jne	.L1582
.L1345:
	cmpq	$0, -816(%rbp)
	jne	.L1583
.L1348:
	cmpq	$0, -624(%rbp)
	jne	.L1584
.L1351:
	cmpq	$0, -432(%rbp)
	leaq	-304(%rbp), %r14
	jne	.L1585
.L1354:
	movq	-2424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134678535, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1357
	call	_ZdlPv@PLT
.L1357:
	movq	(%rbx), %rax
	movq	-2424(%rbp), %rdi
	movq	24(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1358
	call	_ZdlPv@PLT
.L1358:
	movq	-288(%rbp), %rbx
	movq	-296(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1359
	.p2align 4,,10
	.p2align 3
.L1363:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1360
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1363
.L1361:
	movq	-296(%rbp), %r14
.L1359:
	testq	%r14, %r14
	je	.L1364
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1364:
	movq	-2432(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-464(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1365
	call	_ZdlPv@PLT
.L1365:
	movq	-480(%rbp), %rbx
	movq	-488(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1366
	.p2align 4,,10
	.p2align 3
.L1370:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1367
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1370
.L1368:
	movq	-488(%rbp), %r14
.L1366:
	testq	%r14, %r14
	je	.L1371
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1371:
	movq	-2480(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1372
	call	_ZdlPv@PLT
.L1372:
	movq	-672(%rbp), %rbx
	movq	-680(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1373
	.p2align 4,,10
	.p2align 3
.L1377:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1374
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%rbx, %r14
	jne	.L1377
.L1375:
	movq	-680(%rbp), %r14
.L1373:
	testq	%r14, %r14
	je	.L1378
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1378:
	movq	-2472(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-848(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1379
	call	_ZdlPv@PLT
.L1379:
	movq	-864(%rbp), %rbx
	movq	-872(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1380
	.p2align 4,,10
	.p2align 3
.L1384:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1381
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1384
.L1382:
	movq	-872(%rbp), %r14
.L1380:
	testq	%r14, %r14
	je	.L1385
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1385:
	movq	-2488(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1040(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1386
	call	_ZdlPv@PLT
.L1386:
	movq	-1056(%rbp), %rbx
	movq	-1064(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1387
	.p2align 4,,10
	.p2align 3
.L1391:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1388
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1391
.L1389:
	movq	-1064(%rbp), %r14
.L1387:
	testq	%r14, %r14
	je	.L1392
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1392:
	movq	-2504(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1393
	call	_ZdlPv@PLT
.L1393:
	movq	-1248(%rbp), %rbx
	movq	-1256(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1394
	.p2align 4,,10
	.p2align 3
.L1398:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1395
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1398
.L1396:
	movq	-1256(%rbp), %r14
.L1394:
	testq	%r14, %r14
	je	.L1399
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1399:
	movq	-2448(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1400
	call	_ZdlPv@PLT
.L1400:
	movq	-1440(%rbp), %rbx
	movq	-1448(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1401
	.p2align 4,,10
	.p2align 3
.L1405:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1402
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1405
.L1403:
	movq	-1448(%rbp), %r14
.L1401:
	testq	%r14, %r14
	je	.L1406
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1406:
	movq	-2464(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1616(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1407
	call	_ZdlPv@PLT
.L1407:
	movq	-1632(%rbp), %rbx
	movq	-1640(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1408
	.p2align 4,,10
	.p2align 3
.L1412:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1409
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1412
.L1410:
	movq	-1640(%rbp), %r14
.L1408:
	testq	%r14, %r14
	je	.L1413
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1413:
	movq	-2456(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1808(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1414
	call	_ZdlPv@PLT
.L1414:
	movq	-1824(%rbp), %rbx
	movq	-1832(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1415
	.p2align 4,,10
	.p2align 3
.L1419:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1416
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1419
.L1417:
	movq	-1832(%rbp), %r14
.L1415:
	testq	%r14, %r14
	je	.L1420
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1420:
	movq	-2496(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2000(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1421
	call	_ZdlPv@PLT
.L1421:
	movq	-2016(%rbp), %rbx
	movq	-2024(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1422
	.p2align 4,,10
	.p2align 3
.L1426:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1423
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1426
.L1424:
	movq	-2024(%rbp), %r14
.L1422:
	testq	%r14, %r14
	je	.L1427
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1427:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1428
	call	_ZdlPv@PLT
.L1428:
	movq	-2208(%rbp), %rbx
	movq	-2216(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1429
	.p2align 4,,10
	.p2align 3
.L1433:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1430
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1433
.L1431:
	movq	-2216(%rbp), %r14
.L1429:
	testq	%r14, %r14
	je	.L1434
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1434:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1586
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1430:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1433
	jmp	.L1431
	.p2align 4,,10
	.p2align 3
.L1423:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1426
	jmp	.L1424
	.p2align 4,,10
	.p2align 3
.L1416:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1419
	jmp	.L1417
	.p2align 4,,10
	.p2align 3
.L1409:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1412
	jmp	.L1410
	.p2align 4,,10
	.p2align 3
.L1402:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1405
	jmp	.L1403
	.p2align 4,,10
	.p2align 3
.L1395:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1398
	jmp	.L1396
	.p2align 4,,10
	.p2align 3
.L1388:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1391
	jmp	.L1389
	.p2align 4,,10
	.p2align 3
.L1381:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1384
	jmp	.L1382
	.p2align 4,,10
	.p2align 3
.L1374:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1377
	jmp	.L1375
	.p2align 4,,10
	.p2align 3
.L1360:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1363
	jmp	.L1361
	.p2align 4,,10
	.p2align 3
.L1367:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1370
	jmp	.L1368
	.p2align 4,,10
	.p2align 3
.L1576:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r14, %rdi
	movb	$7, 2(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1324
	call	_ZdlPv@PLT
.L1324:
	movq	(%rbx), %rax
	movl	$87, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %r14
	movq	(%rax), %rbx
	movq	16(%rax), %rax
	movq	%rax, -2528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rdx
	movq	%r13, %rcx
	movq	%rbx, %rsi
	movq	-2440(%rbp), %rdi
	call	_ZN2v88internal21Cast10JSReceiver_1404EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm6
	movq	%rbx, %xmm3
	movq	-2528(%rbp), %xmm2
	punpcklqdq	%xmm6, %xmm3
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	%rax, -80(%rbp)
	punpcklqdq	%xmm6, %xmm2
	movaps	%xmm3, -2528(%rbp)
	leaq	-2384(%rbp), %r14
	movaps	%xmm2, -2544(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -2384(%rbp)
	movq	$0, -2368(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	40(%rax), %rdx
	leaq	-1840(%rbp), %rdi
	movq	%rax, -2384(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2368(%rbp)
	movq	%rdx, -2376(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1325
	call	_ZdlPv@PLT
.L1325:
	movq	-2456(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2344(%rbp)
	jne	.L1587
.L1326:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1968(%rbp)
	je	.L1328
.L1577:
	movq	-2496(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2032(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134678535, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1329
	call	_ZdlPv@PLT
.L1329:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm6
	movaps	%xmm0, -2352(%rbp)
	movq	$0, -2336(%rbp)
	movq	%rdx, -96(%rbp)
	movaps	%xmm6, -112(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-112(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-1648(%rbp), %rdi
	movq	%rax, -2352(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1330
	call	_ZdlPv@PLT
.L1330:
	movq	-2464(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1776(%rbp)
	je	.L1331
.L1578:
	movq	-2456(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1840(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134678535, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1332
	call	_ZdlPv@PLT
.L1332:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	8(%rax), %rsi
	movq	16(%rax), %rcx
	movq	32(%rax), %rdx
	movq	(%rax), %rax
	movaps	%xmm0, -2352(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rax, -112(%rbp)
	movq	$0, -2336(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-1456(%rbp), %rdi
	movq	%rax, -2352(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1333
	call	_ZdlPv@PLT
.L1333:
	movq	-2448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1584(%rbp)
	je	.L1334
.L1579:
	movq	-2464(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1648(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%r14, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$7, 2(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1335
	call	_ZdlPv@PLT
.L1335:
	movq	(%rbx), %rax
	movq	-2440(%rbp), %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r14
	movq	%rcx, -2528(%rbp)
	call	_ZN2v88internal8False_66EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %xmm0
	movl	$32, %edi
	movq	%r14, -96(%rbp)
	movhps	-2528(%rbp), %xmm0
	movq	%rax, -88(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2352(%rbp)
	movq	$0, -2336(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm1
	movdqa	-96(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-496(%rbp), %rdi
	movq	%rax, -2352(%rbp)
	movups	%xmm1, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1336
	call	_ZdlPv@PLT
.L1336:
	movq	-2432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1392(%rbp)
	je	.L1337
.L1580:
	movq	-2448(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1456(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1338
	call	_ZdlPv@PLT
.L1338:
	movq	(%rbx), %rax
	movl	$88, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	24(%rax), %rbx
	movq	16(%rax), %r14
	movq	%rsi, -2544(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rcx, -2528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2440(%rbp), %rdi
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal16Cast7JSProxy_107EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm7
	movq	%r14, %xmm4
	movq	-2528(%rbp), %xmm5
	punpcklqdq	%xmm7, %xmm4
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movq	%rbx, -80(%rbp)
	movhps	-2544(%rbp), %xmm5
	movaps	%xmm4, -2560(%rbp)
	leaq	-2384(%rbp), %r14
	movaps	%xmm5, -2528(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm0, -2384(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -2368(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm1
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	leaq	48(%rax), %rdx
	leaq	-1072(%rbp), %rdi
	movq	%rax, -2384(%rbp)
	movups	%xmm1, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rdx, -2368(%rbp)
	movq	%rdx, -2376(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1339
	call	_ZdlPv@PLT
.L1339:
	movq	-2488(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2344(%rbp)
	jne	.L1588
.L1340:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1200(%rbp)
	je	.L1342
.L1581:
	movq	-2504(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1264(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1343
	call	_ZdlPv@PLT
.L1343:
	movq	(%rbx), %rax
	movl	$32, %edi
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm7
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-880(%rbp), %rdi
	movq	%rax, -2352(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1344
	call	_ZdlPv@PLT
.L1344:
	movq	-2472(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1008(%rbp)
	je	.L1345
.L1582:
	movq	-2488(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1072(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1346
	call	_ZdlPv@PLT
.L1346:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	40(%rax), %rdx
	movdqu	(%rax), %xmm6
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -80(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm1
	movdqa	-96(%rbp), %xmm4
	leaq	40(%rax), %rdx
	leaq	-688(%rbp), %rdi
	movq	%rax, -2352(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm1, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1347
	call	_ZdlPv@PLT
.L1347:
	movq	-2480(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -816(%rbp)
	je	.L1348
.L1583:
	movq	-2472(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-880(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1349
	call	_ZdlPv@PLT
.L1349:
	movq	(%rbx), %rax
	movl	$90, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	24(%rax), %r14
	movq	%rcx, -2528(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -2544(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$89, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2440(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %xmm0
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	-2544(%rbp), %r14
	leaq	-112(%rbp), %rcx
	movl	$2, %r8d
	movl	$233, %esi
	movq	%r14, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rdi
	movq	%rax, -2544(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm0
	movl	$32, %edi
	movq	$0, -2336(%rbp)
	movhps	-2528(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%r14, %xmm0
	movhps	-2544(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-496(%rbp), %rdi
	movq	%rax, -2352(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1350
	call	_ZdlPv@PLT
.L1350:
	movq	-2432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -624(%rbp)
	je	.L1351
.L1584:
	movq	-2480(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-688(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1352
	call	_ZdlPv@PLT
.L1352:
	movq	(%rbx), %rax
	movl	$91, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	leaq	-2400(%rbp), %r14
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	%rcx, -2544(%rbp)
	movq	16(%rax), %rcx
	movq	32(%rax), %rax
	movq	%rcx, -2528(%rbp)
	movq	%rax, -2560(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2440(%rbp), %rdi
	call	_ZN2v88internal8False_66EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2440(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -2512(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$860, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-2352(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	leaq	-112(%rbp), %rsi
	movl	$3, %edi
	pushq	%rdi
	movq	%rax, %r8
	movq	-2336(%rbp), %rax
	movq	%rbx, %r9
	pushq	%rsi
	leaq	-2384(%rbp), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	-2560(%rbp), %xmm0
	movq	%rcx, -2384(%rbp)
	movq	-2512(%rbp), %rcx
	movq	%rax, -2376(%rbp)
	movhps	-2528(%rbp), %xmm0
	movq	%rcx, -96(%rbp)
	movl	$1, %ecx
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r14, %rdi
	movq	%rax, -2440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm0
	movl	$32, %edi
	movq	$0, -2336(%rbp)
	movhps	-2544(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-2528(%rbp), %xmm0
	movhps	-2440(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-496(%rbp), %rdi
	movq	%rax, -2352(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L1353
	call	_ZdlPv@PLT
.L1353:
	movq	-2432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1351
	.p2align 4,,10
	.p2align 3
.L1585:
	movq	-2432(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-496(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134678535, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1355
	call	_ZdlPv@PLT
.L1355:
	movq	(%rbx), %rax
	movl	$84, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	16(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rcx, -2440(%rbp)
	movq	%rax, -2528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %xmm0
	movl	$32, %edi
	movq	$0, -2336(%rbp)
	movhps	-2440(%rbp), %xmm0
	leaq	-304(%rbp), %r14
	movaps	%xmm0, -112(%rbp)
	movq	%rbx, %xmm0
	movhps	-2528(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm6
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm1
	leaq	32(%rax), %rdx
	movq	%rax, -2352(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm1, 16(%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1356
	call	_ZdlPv@PLT
.L1356:
	movq	-2424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1354
	.p2align 4,,10
	.p2align 3
.L1587:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-2528(%rbp), %xmm4
	movdqa	-2544(%rbp), %xmm5
	movaps	%xmm0, -2384(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movq	$0, -2368(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm2
	movq	%r14, %rsi
	leaq	32(%rax), %rdx
	leaq	-2032(%rbp), %rdi
	movq	%rax, -2384(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -2368(%rbp)
	movq	%rdx, -2376(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1327
	call	_ZdlPv@PLT
.L1327:
	movq	-2496(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1326
	.p2align 4,,10
	.p2align 3
.L1588:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movdqa	-2528(%rbp), %xmm3
	movdqa	-2560(%rbp), %xmm6
	movq	%rbx, -80(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm0, -2384(%rbp)
	movq	$0, -2368(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm1
	movdqa	-96(%rbp), %xmm4
	leaq	40(%rax), %rdx
	leaq	-1264(%rbp), %rdi
	movq	%rax, -2384(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm1, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2368(%rbp)
	movq	%rdx, -2376(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1341
	call	_ZdlPv@PLT
.L1341:
	movq	-2504(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1340
.L1586:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22476:
	.size	_ZN2v88internal33ObjectSetPrototypeOfDontThrow_317EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_10HeapObjectEEE, .-_ZN2v88internal33ObjectSetPrototypeOfDontThrow_317EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_10HeapObjectEEE
	.section	.text._ZN2v88internal27ObjectIsExtensibleAssembler30GenerateObjectIsExtensibleImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal27ObjectIsExtensibleAssembler30GenerateObjectIsExtensibleImplEv
	.type	_ZN2v88internal27ObjectIsExtensibleAssembler30GenerateObjectIsExtensibleImplEv, @function
_ZN2v88internal27ObjectIsExtensibleAssembler30GenerateObjectIsExtensibleImplEv:
.LFB22485:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-272(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-184(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-240(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	leaq	-280(%rbp), %r12
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -304(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-312(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-304(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1590
	call	_ZdlPv@PLT
.L1590:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L1620
.L1591:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1595
	call	_ZdlPv@PLT
.L1595:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1596
	.p2align 4,,10
	.p2align 3
.L1600:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1597
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1600
.L1598:
	movq	-232(%rbp), %r13
.L1596:
	testq	%r13, %r13
	je	.L1601
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1601:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1621
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1597:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1600
	jmp	.L1598
	.p2align 4,,10
	.p2align 3
.L1620:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1592
	call	_ZdlPv@PLT
.L1592:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	testq	%rdx, %rdx
	movq	%rdx, %rbx
	cmove	-312(%rbp), %rbx
	testq	%rax, %rax
	cmove	-304(%rbp), %rax
	movl	$99, %edx
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-304(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal22ObjectIsExtensible_311EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L1591
.L1621:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22485:
	.size	_ZN2v88internal27ObjectIsExtensibleAssembler30GenerateObjectIsExtensibleImplEv, .-_ZN2v88internal27ObjectIsExtensibleAssembler30GenerateObjectIsExtensibleImplEv
	.section	.rodata._ZN2v88internal8Builtins27Generate_ObjectIsExtensibleEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/object-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins27Generate_ObjectIsExtensibleEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"ObjectIsExtensible"
	.section	.text._ZN2v88internal8Builtins27Generate_ObjectIsExtensibleEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins27Generate_ObjectIsExtensibleEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins27Generate_ObjectIsExtensibleEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins27Generate_ObjectIsExtensibleEPNS0_8compiler18CodeAssemblerStateE:
.LFB22481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$976, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$846, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L1626
.L1623:
	movq	%r13, %rdi
	call	_ZN2v88internal27ObjectIsExtensibleAssembler30GenerateObjectIsExtensibleImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1627
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1626:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L1623
.L1627:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22481:
	.size	_ZN2v88internal8Builtins27Generate_ObjectIsExtensibleEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins27Generate_ObjectIsExtensibleEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal32ObjectPreventExtensionsAssembler35GenerateObjectPreventExtensionsImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32ObjectPreventExtensionsAssembler35GenerateObjectPreventExtensionsImplEv
	.type	_ZN2v88internal32ObjectPreventExtensionsAssembler35GenerateObjectPreventExtensionsImplEv, @function
_ZN2v88internal32ObjectPreventExtensionsAssembler35GenerateObjectPreventExtensionsImplEv:
.LFB22494:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-272(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-184(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-240(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	leaq	-280(%rbp), %r12
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -304(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-312(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-304(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1629
	call	_ZdlPv@PLT
.L1629:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L1659
.L1630:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1634
	call	_ZdlPv@PLT
.L1634:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1635
	.p2align 4,,10
	.p2align 3
.L1639:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1636
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1639
.L1637:
	movq	-232(%rbp), %r13
.L1635:
	testq	%r13, %r13
	je	.L1640
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1640:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1660
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1636:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1639
	jmp	.L1637
	.p2align 4,,10
	.p2align 3
.L1659:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1631
	call	_ZdlPv@PLT
.L1631:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	testq	%rdx, %rdx
	movq	%rdx, %rbx
	cmove	-312(%rbp), %rbx
	testq	%rax, %rax
	cmove	-304(%rbp), %rax
	movl	$107, %edx
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-304(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal32ObjectPreventExtensionsThrow_312EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L1630
.L1660:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22494:
	.size	_ZN2v88internal32ObjectPreventExtensionsAssembler35GenerateObjectPreventExtensionsImplEv, .-_ZN2v88internal32ObjectPreventExtensionsAssembler35GenerateObjectPreventExtensionsImplEv
	.section	.rodata._ZN2v88internal8Builtins32Generate_ObjectPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"ObjectPreventExtensions"
	.section	.text._ZN2v88internal8Builtins32Generate_ObjectPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins32Generate_ObjectPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins32Generate_ObjectPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins32Generate_ObjectPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE:
.LFB22490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$997, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$847, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L1665
.L1662:
	movq	%r13, %rdi
	call	_ZN2v88internal32ObjectPreventExtensionsAssembler35GenerateObjectPreventExtensionsImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1666
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1665:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L1662
.L1666:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22490:
	.size	_ZN2v88internal8Builtins32Generate_ObjectPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins32Generate_ObjectPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal29ObjectGetPrototypeOfAssembler32GenerateObjectGetPrototypeOfImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29ObjectGetPrototypeOfAssembler32GenerateObjectGetPrototypeOfImplEv
	.type	_ZN2v88internal29ObjectGetPrototypeOfAssembler32GenerateObjectGetPrototypeOfImplEv, @function
_ZN2v88internal29ObjectGetPrototypeOfAssembler32GenerateObjectGetPrototypeOfImplEv:
.LFB22503:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-272(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-184(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-240(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	leaq	-280(%rbp), %r12
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -304(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-312(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-304(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1668
	call	_ZdlPv@PLT
.L1668:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L1698
.L1669:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1673
	call	_ZdlPv@PLT
.L1673:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1674
	.p2align 4,,10
	.p2align 3
.L1678:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1675
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1678
.L1676:
	movq	-232(%rbp), %r13
.L1674:
	testq	%r13, %r13
	je	.L1679
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1679:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1699
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1675:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1678
	jmp	.L1676
	.p2align 4,,10
	.p2align 3
.L1698:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1670
	call	_ZdlPv@PLT
.L1670:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	testq	%rdx, %rdx
	movq	%rdx, %rbx
	cmove	-312(%rbp), %rbx
	testq	%rax, %rax
	cmove	-304(%rbp), %rax
	movl	$115, %edx
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-304(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal24ObjectGetPrototypeOf_314EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L1669
.L1699:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22503:
	.size	_ZN2v88internal29ObjectGetPrototypeOfAssembler32GenerateObjectGetPrototypeOfImplEv, .-_ZN2v88internal29ObjectGetPrototypeOfAssembler32GenerateObjectGetPrototypeOfImplEv
	.section	.rodata._ZN2v88internal8Builtins29Generate_ObjectGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"ObjectGetPrototypeOf"
	.section	.text._ZN2v88internal8Builtins29Generate_ObjectGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins29Generate_ObjectGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins29Generate_ObjectGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins29Generate_ObjectGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE:
.LFB22499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1018, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$848, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L1704
.L1701:
	movq	%r13, %rdi
	call	_ZN2v88internal29ObjectGetPrototypeOfAssembler32GenerateObjectGetPrototypeOfImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1705
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1704:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L1701
.L1705:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22499:
	.size	_ZN2v88internal8Builtins29Generate_ObjectGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins29Generate_ObjectGetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal32Cast21UT6ATNull10JSReceiver_1465EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"../../deps/v8/../../deps/v8/src/builtins/base.tq"
	.section	.text._ZN2v88internal32Cast21UT6ATNull10JSReceiver_1465EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Cast21UT6ATNull10JSReceiver_1465EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal32Cast21UT6ATNull10JSReceiver_1465EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal32Cast21UT6ATNull10JSReceiver_1465EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE:
.LFB22522:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	leaq	-1632(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1760(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1800(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1864, %rsp
	movq	%rsi, -1888(%rbp)
	movq	%rdx, -1904(%rbp)
	movq	%rcx, -1872(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1800(%rbp)
	movq	%rdi, -1632(%rbp)
	movl	$48, %edi
	movq	$0, -1624(%rbp)
	movq	$0, -1616(%rbp)
	movq	$0, -1608(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1624(%rbp)
	leaq	-1576(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1608(%rbp)
	movq	%rdx, -1616(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1592(%rbp)
	movq	%rax, -1816(%rbp)
	movq	$0, -1600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1432(%rbp)
	movq	$0, -1424(%rbp)
	movq	%rax, -1440(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1432(%rbp)
	leaq	-1384(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -1832(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1864(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$96, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1840(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1848(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	$0, -472(%rbp)
	movq	%rax, -480(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1824(%rbp)
	movq	$0, -464(%rbp)
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	$0, -440(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1888(%rbp), %xmm1
	movaps	%xmm0, -1760(%rbp)
	movhps	-1904(%rbp), %xmm1
	movq	$0, -1744(%rbp)
	movaps	%xmm1, -1888(%rbp)
	call	_Znwm@PLT
	movdqa	-1888(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1707
	call	_ZdlPv@PLT
.L1707:
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1568(%rbp)
	jne	.L1891
	cmpq	$0, -1376(%rbp)
	jne	.L1892
.L1713:
	cmpq	$0, -1184(%rbp)
	jne	.L1893
.L1716:
	cmpq	$0, -992(%rbp)
	jne	.L1894
.L1721:
	cmpq	$0, -800(%rbp)
	jne	.L1895
.L1724:
	cmpq	$0, -608(%rbp)
	leaq	-288(%rbp), %r15
	jne	.L1896
	cmpq	$0, -416(%rbp)
	jne	.L1897
.L1730:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1732
	call	_ZdlPv@PLT
.L1732:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1733
	call	_ZdlPv@PLT
.L1733:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1734
	.p2align 4,,10
	.p2align 3
.L1738:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1735
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1738
.L1736:
	movq	-280(%rbp), %r14
.L1734:
	testq	%r14, %r14
	je	.L1739
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1739:
	movq	-1824(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1740
	call	_ZdlPv@PLT
.L1740:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1741
	.p2align 4,,10
	.p2align 3
.L1745:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1742
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1745
.L1743:
	movq	-472(%rbp), %r14
.L1741:
	testq	%r14, %r14
	je	.L1746
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1746:
	movq	-1848(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1747
	call	_ZdlPv@PLT
.L1747:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1748
	.p2align 4,,10
	.p2align 3
.L1752:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1749
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1752
.L1750:
	movq	-664(%rbp), %r14
.L1748:
	testq	%r14, %r14
	je	.L1753
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1753:
	movq	-1840(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1754
	call	_ZdlPv@PLT
.L1754:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1755
	.p2align 4,,10
	.p2align 3
.L1759:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1756
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1759
.L1757:
	movq	-856(%rbp), %r14
.L1755:
	testq	%r14, %r14
	je	.L1760
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1760:
	movq	-1864(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1761
	call	_ZdlPv@PLT
.L1761:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1762
	.p2align 4,,10
	.p2align 3
.L1766:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1763
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1766
.L1764:
	movq	-1048(%rbp), %r14
.L1762:
	testq	%r14, %r14
	je	.L1767
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1767:
	movq	-1832(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1768
	call	_ZdlPv@PLT
.L1768:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1769
	.p2align 4,,10
	.p2align 3
.L1773:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1770
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1773
.L1771:
	movq	-1240(%rbp), %r14
.L1769:
	testq	%r14, %r14
	je	.L1774
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1774:
	movq	-1856(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1775
	call	_ZdlPv@PLT
.L1775:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1776
	.p2align 4,,10
	.p2align 3
.L1780:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1777
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1780
.L1778:
	movq	-1432(%rbp), %r14
.L1776:
	testq	%r14, %r14
	je	.L1781
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1781:
	movq	-1816(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1782
	call	_ZdlPv@PLT
.L1782:
	movq	-1616(%rbp), %rbx
	movq	-1624(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1783
	.p2align 4,,10
	.p2align 3
.L1787:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1784
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1787
.L1785:
	movq	-1624(%rbp), %r14
.L1783:
	testq	%r14, %r14
	je	.L1788
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1788:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1898
	addq	$1864, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1784:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1787
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L1777:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1780
	jmp	.L1778
	.p2align 4,,10
	.p2align 3
.L1770:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1773
	jmp	.L1771
	.p2align 4,,10
	.p2align 3
.L1763:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1766
	jmp	.L1764
	.p2align 4,,10
	.p2align 3
.L1756:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1759
	jmp	.L1757
	.p2align 4,,10
	.p2align 3
.L1749:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1752
	jmp	.L1750
	.p2align 4,,10
	.p2align 3
.L1735:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1738
	jmp	.L1736
	.p2align 4,,10
	.p2align 3
.L1742:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1745
	jmp	.L1743
	.p2align 4,,10
	.p2align 3
.L1891:
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r9d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L1709
	call	_ZdlPv@PLT
.L1709:
	movq	(%r15), %rax
	movl	$2026, %edx
	movq	%r12, %rdi
	leaq	.LC6(%rip), %rsi
	leaq	-1792(%rbp), %r15
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rcx, -1904(%rbp)
	movq	%rax, -1888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1888(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1888(%rbp), %rax
	movl	$32, %edi
	movq	-1904(%rbp), %xmm2
	movq	$0, -1776(%rbp)
	movq	%rax, %xmm0
	movq	%rax, %xmm6
	punpcklqdq	%xmm0, %xmm0
	punpcklqdq	%xmm6, %xmm2
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -1904(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -1792(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movq	%r15, %rsi
	leaq	32(%rax), %rdx
	leaq	-1248(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1710
	call	_ZdlPv@PLT
.L1710:
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1752(%rbp)
	jne	.L1899
.L1711:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1376(%rbp)
	je	.L1713
.L1892:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1440(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r8d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1714
	call	_ZdlPv@PLT
.L1714:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-480(%rbp), %rdi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1715
	call	_ZdlPv@PLT
.L1715:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1184(%rbp)
	je	.L1716
.L1893:
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1248(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L1717
	call	_ZdlPv@PLT
.L1717:
	movq	(%r15), %rax
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	24(%rax), %rax
	movq	%rcx, -1904(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -1888(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1888(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal31Cast21UT6ATNull10JSReceiver_144EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rcx
	movq	%r15, %xmm3
	pxor	%xmm0, %xmm0
	movhps	-1904(%rbp), %xmm3
	movl	$32, %edi
	movq	%rax, -72(%rbp)
	leaq	-1792(%rbp), %r15
	movq	%rcx, -80(%rbp)
	movaps	%xmm3, -1904(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -1792(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	32(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1718
	call	_ZdlPv@PLT
.L1718:
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1752(%rbp)
	jne	.L1900
.L1719:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -992(%rbp)
	je	.L1721
.L1894:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rdi
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1722
	call	_ZdlPv@PLT
.L1722:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-480(%rbp), %rdi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1723
	call	_ZdlPv@PLT
.L1723:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L1724
.L1895:
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1725
	call	_ZdlPv@PLT
.L1725:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	24(%rax), %rdx
	movdqu	(%rax), %xmm4
	movaps	%xmm0, -1760(%rbp)
	movq	$0, -1744(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm4, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -1760(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1726
	call	_ZdlPv@PLT
.L1726:
	movq	-1848(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1724
	.p2align 4,,10
	.p2align 3
.L1896:
	movq	-1848(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%r15, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1728
	call	_ZdlPv@PLT
.L1728:
	movq	(%rbx), %rax
	movl	$133, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r15
	movq	%rcx, -1888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r15, -80(%rbp)
	movhps	-1888(%rbp), %xmm0
	leaq	-288(%rbp), %r15
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm6
	leaq	24(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1729
	call	_ZdlPv@PLT
.L1729:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -416(%rbp)
	je	.L1730
.L1897:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1731
	call	_ZdlPv@PLT
.L1731:
	movq	-1872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1730
	.p2align 4,,10
	.p2align 3
.L1899:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1904(%rbp), %xmm7
	movl	$24, %edi
	movaps	%xmm0, -1792(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-1440(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1712
	call	_ZdlPv@PLT
.L1712:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1711
	.p2align 4,,10
	.p2align 3
.L1900:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1904(%rbp), %xmm7
	movl	$24, %edi
	movaps	%xmm0, -1792(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-1056(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1720
	call	_ZdlPv@PLT
.L1720:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1719
.L1898:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22522:
	.size	_ZN2v88internal32Cast21UT6ATNull10JSReceiver_1465EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, .-_ZN2v88internal32Cast21UT6ATNull10JSReceiver_1465EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.section	.rodata._ZN2v88internal29ObjectSetPrototypeOfAssembler32GenerateObjectSetPrototypeOfImplEv.str1.1,"aMS",@progbits,1
.LC7:
	.string	"Object.setPrototypeOf"
	.section	.text._ZN2v88internal29ObjectSetPrototypeOfAssembler32GenerateObjectSetPrototypeOfImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29ObjectSetPrototypeOfAssembler32GenerateObjectSetPrototypeOfImplEv
	.type	_ZN2v88internal29ObjectSetPrototypeOfAssembler32GenerateObjectSetPrototypeOfImplEv, @function
_ZN2v88internal29ObjectSetPrototypeOfAssembler32GenerateObjectSetPrototypeOfImplEv:
.LFB22512:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$5, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-824(%rbp), %r14
	leaq	-248(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-880(%rbp), %rbx
	subq	$1080, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, -1072(%rbp)
	movq	%rax, -1048(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -1088(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	leaq	-1048(%rbp), %r12
	movq	%rax, -1104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$72, %edi
	movq	$0, -872(%rbp)
	movq	$0, -864(%rbp)
	movq	%rax, %r13
	movq	-1048(%rbp), %rax
	movq	$0, -856(%rbp)
	movq	%rax, -880(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -856(%rbp)
	movq	%rdx, -864(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -840(%rbp)
	movq	%rax, -872(%rbp)
	movq	$0, -848(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1048(%rbp), %rax
	movl	$120, %edi
	movq	$0, -680(%rbp)
	movq	$0, -672(%rbp)
	movq	%rax, -688(%rbp)
	movq	$0, -664(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -680(%rbp)
	leaq	-632(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -664(%rbp)
	movq	%rdx, -672(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -648(%rbp)
	movq	%rax, -1080(%rbp)
	movq	$0, -656(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1048(%rbp), %rax
	movl	$144, %edi
	movq	$0, -488(%rbp)
	movq	$0, -480(%rbp)
	movq	%rax, -496(%rbp)
	movq	$0, -472(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -488(%rbp)
	leaq	-440(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -472(%rbp)
	movq	%rdx, -480(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -456(%rbp)
	movq	%rax, -1064(%rbp)
	movq	$0, -464(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1048(%rbp), %rax
	movl	$96, %edi
	movq	$0, -296(%rbp)
	movq	$0, -288(%rbp)
	movq	%rax, -304(%rbp)
	movq	$0, -280(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r15, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -280(%rbp)
	movq	%rdx, -288(%rbp)
	xorl	%edx, %edx
	movq	%rax, -296(%rbp)
	movups	%xmm0, -264(%rbp)
	movq	$0, -272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	-1088(%rbp), %xmm1
	movq	%r13, -96(%rbp)
	leaq	-1008(%rbp), %r13
	movhps	-1104(%rbp), %xmm1
	movaps	%xmm0, -1008(%rbp)
	movaps	%xmm1, -112(%rbp)
	movq	$0, -992(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm2
	leaq	24(%rax), %rdx
	movq	%rax, -1008(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -992(%rbp)
	movq	%rdx, -1000(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1008(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1902
	call	_ZdlPv@PLT
.L1902:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -816(%rbp)
	jne	.L2002
	cmpq	$0, -624(%rbp)
	jne	.L2003
.L1908:
	cmpq	$0, -432(%rbp)
	jne	.L2004
.L1911:
	cmpq	$0, -240(%rbp)
	jne	.L2005
.L1916:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1918
	call	_ZdlPv@PLT
.L1918:
	movq	-288(%rbp), %rbx
	movq	-296(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1919
	.p2align 4,,10
	.p2align 3
.L1923:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1920
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1923
.L1921:
	movq	-296(%rbp), %r13
.L1919:
	testq	%r13, %r13
	je	.L1924
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1924:
	movq	-1064(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-464(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1925
	call	_ZdlPv@PLT
.L1925:
	movq	-480(%rbp), %rbx
	movq	-488(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1926
	.p2align 4,,10
	.p2align 3
.L1930:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1927
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1930
.L1928:
	movq	-488(%rbp), %r13
.L1926:
	testq	%r13, %r13
	je	.L1931
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1931:
	movq	-1080(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1932
	call	_ZdlPv@PLT
.L1932:
	movq	-672(%rbp), %rbx
	movq	-680(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1933
	.p2align 4,,10
	.p2align 3
.L1937:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1934
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1937
.L1935:
	movq	-680(%rbp), %r13
.L1933:
	testq	%r13, %r13
	je	.L1938
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1938:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-848(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1939
	call	_ZdlPv@PLT
.L1939:
	movq	-864(%rbp), %rbx
	movq	-872(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1940
	.p2align 4,,10
	.p2align 3
.L1944:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1941
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1944
.L1942:
	movq	-872(%rbp), %r13
.L1940:
	testq	%r13, %r13
	je	.L1945
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1945:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2006
	addq	$1080, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1941:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1944
	jmp	.L1942
	.p2align 4,,10
	.p2align 3
.L1934:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1937
	jmp	.L1935
	.p2align 4,,10
	.p2align 3
.L1920:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1923
	jmp	.L1921
	.p2align 4,,10
	.p2align 3
.L1927:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1930
	jmp	.L1928
	.p2align 4,,10
	.p2align 3
.L2002:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -992(%rbp)
	movaps	%xmm0, -1008(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$8, 2(%rax)
	movq	%rax, -1008(%rbp)
	movq	%rdx, -992(%rbp)
	movq	%rdx, -1000(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1008(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1904
	call	_ZdlPv@PLT
.L1904:
	movq	(%rbx), %rax
	movl	$124, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	%rcx, -1104(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -1088(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1104(%rbp), %rdx
	movq	-1072(%rbp), %rdi
	movq	%rbx, %rsi
	leaq	.LC7(%rip), %rcx
	call	_ZN2v88internal26RequireObjectCoercible_241EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPKc@PLT
	movl	$132, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$133, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1088(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rcx
	movq	-1072(%rbp), %rdi
	call	_ZN2v88internal32Cast21UT6ATNull10JSReceiver_1465EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	movq	-1088(%rbp), %rcx
	movq	%rbx, %xmm4
	pxor	%xmm0, %xmm0
	movhps	-1104(%rbp), %xmm4
	movl	$48, %edi
	movq	%rax, -72(%rbp)
	leaq	-1040(%rbp), %rbx
	movq	%rcx, %xmm3
	movq	%rcx, -80(%rbp)
	punpcklqdq	%xmm3, %xmm3
	movaps	%xmm4, -1104(%rbp)
	movaps	%xmm3, -1120(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -1040(%rbp)
	movq	$0, -1024(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm5
	movq	%rbx, %rsi
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	leaq	48(%rax), %rdx
	leaq	-496(%rbp), %rdi
	movq	%rax, -1040(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -1024(%rbp)
	movq	%rdx, -1032(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1040(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1905
	call	_ZdlPv@PLT
.L1905:
	movq	-1064(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1000(%rbp)
	jne	.L2007
.L1906:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -624(%rbp)
	je	.L1908
.L2003:
	movq	-1080(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-688(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -992(%rbp)
	movaps	%xmm0, -1008(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -1008(%rbp)
	movq	%rdx, -992(%rbp)
	movq	%rdx, -1000(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1008(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1909
	call	_ZdlPv@PLT
.L1909:
	movq	(%rbx), %rax
	movl	$32, %edi
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm5
	movq	$0, -992(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm0, -1008(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-304(%rbp), %rdi
	movq	%rax, -1008(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -992(%rbp)
	movq	%rdx, -1000(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1008(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1910
	call	_ZdlPv@PLT
.L1910:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -432(%rbp)
	je	.L1911
.L2004:
	movq	-1064(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	leaq	-496(%rbp), %r8
	movq	%r8, -1120(%rbp)
	movq	$0, -1088(%rbp)
	movq	$0, -1104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -992(%rbp)
	movaps	%xmm0, -1008(%rbp)
	call	_Znwm@PLT
	movl	$1800, %ecx
	movq	-1120(%rbp), %r8
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%r8, %rdi
	movq	%rax, -1008(%rbp)
	movq	%rdx, -992(%rbp)
	movq	%rdx, -1000(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1008(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1912
	movq	%rax, -1120(%rbp)
	call	_ZdlPv@PLT
	movq	-1120(%rbp), %rax
.L1912:
	movq	(%rax), %rax
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	cmove	-1088(%rbp), %rdx
	movq	%rdx, -1088(%rbp)
	movq	8(%rax), %rdx
	movq	40(%rax), %rax
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	cmove	-1104(%rbp), %rcx
	movl	$134, %edx
	testq	%rax, %rax
	cmovne	%rax, %rbx
	movq	%rcx, -1104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1104(%rbp), %rdx
	movq	-1088(%rbp), %rsi
	movq	-1072(%rbp), %rdi
	movq	%rbx, %rcx
	call	_ZN2v88internal29ObjectSetPrototypeOfThrow_316EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_10HeapObjectEEE
	movq	-1072(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -240(%rbp)
	je	.L1916
.L2005:
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	-304(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -992(%rbp)
	movaps	%xmm0, -1008(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1008(%rbp)
	movq	%rdx, -992(%rbp)
	movq	%rdx, -1000(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1008(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1917
	call	_ZdlPv@PLT
.L1917:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movl	$136, %edx
	leaq	.LC1(%rip), %rsi
	movq	16(%rax), %rcx
	movq	(%rax), %rbx
	movq	%rcx, -1088(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$137, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1072(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	-1088(%rbp), %rcx
	movl	$116, %edx
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L1916
	.p2align 4,,10
	.p2align 3
.L2007:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1088(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1104(%rbp), %xmm5
	movdqa	-1120(%rbp), %xmm6
	movl	$40, %edi
	movaps	%xmm0, -1040(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1024(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%rbx, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm5
	leaq	40(%rax), %rdx
	leaq	-688(%rbp), %rdi
	movq	%rax, -1040(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1024(%rbp)
	movq	%rdx, -1032(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1040(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1907
	call	_ZdlPv@PLT
.L1907:
	movq	-1080(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1906
.L2006:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22512:
	.size	_ZN2v88internal29ObjectSetPrototypeOfAssembler32GenerateObjectSetPrototypeOfImplEv, .-_ZN2v88internal29ObjectSetPrototypeOfAssembler32GenerateObjectSetPrototypeOfImplEv
	.section	.rodata._ZN2v88internal8Builtins29Generate_ObjectSetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"ObjectSetPrototypeOf"
	.section	.text._ZN2v88internal8Builtins29Generate_ObjectSetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins29Generate_ObjectSetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins29Generate_ObjectSetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins29Generate_ObjectSetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE:
.LFB22508:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1039, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$849, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L2012
.L2009:
	movq	%r13, %rdi
	call	_ZN2v88internal29ObjectSetPrototypeOfAssembler32GenerateObjectSetPrototypeOfImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2013
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2012:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L2009
.L2013:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22508:
	.size	_ZN2v88internal8Builtins29Generate_ObjectSetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins29Generate_ObjectSetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal22ObjectIsExtensible_311EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal22ObjectIsExtensible_311EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, @function
_GLOBAL__sub_I__ZN2v88internal22ObjectIsExtensible_311EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE:
.LFB29243:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29243:
	.size	_GLOBAL__sub_I__ZN2v88internal22ObjectIsExtensible_311EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, .-_GLOBAL__sub_I__ZN2v88internal22ObjectIsExtensible_311EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal22ObjectIsExtensible_311EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
