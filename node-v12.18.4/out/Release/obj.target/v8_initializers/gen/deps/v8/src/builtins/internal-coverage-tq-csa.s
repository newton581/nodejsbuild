	.file	"internal-coverage-tq-csa.cc"
	.text
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0:
.LFB29581:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	%rsi, %r13
	pushq	%rbx
	movq	%r13, %rax
	sarq	$3, %rax
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%r13, %r13
	js	.L9
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%rdx, %r14
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.L3
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%rax, %rdi
.L3:
	leaq	(%rdi,%r13), %r15
	movq	%rdi, (%rbx)
	movq	%r15, 16(%rbx)
	cmpq	%r12, %r14
	je	.L4
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
.L4:
	movq	%r15, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L9:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29581:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0:
.LFB29580:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	subq	%rsi, %rdx
	js	.L16
	movq	%rdi, %rbx
	je	.L12
	movq	%rdx, %rdi
	movq	%rdx, %r12
	movq	%rsi, %r13
	call	_Znwm@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	leaq	(%rax,%r12), %r14
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	%r14, 16(%rbx)
	call	memcpy@PLT
	movq	%r14, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	xorl	%r14d, %r14d
	movq	$0, (%rdi)
	movq	$0, 16(%rdi)
	movq	%r14, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L16:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29580:
	.size	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev,"axG",@progbits,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev:
.LFB22413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$56, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L18
	call	_ZdlPv@PLT
.L18:
	movq	16(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	%r12, %r13
	je	.L19
	.p2align 4,,10
	.p2align 3
.L23:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L20
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L23
.L21:
	movq	8(%rbx), %r12
.L19:
	testq	%r12, %r12
	je	.L17
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L23
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L17:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22413:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev
	.set	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.section	.rodata._ZN2v88internal13SlotCount_307EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10FixedArrayEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../../deps/v8/../../deps/v8/src/builtins/internal-coverage.tq"
	.section	.text._ZN2v88internal13SlotCount_307EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10FixedArrayEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal13SlotCount_307EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10FixedArrayEEE
	.type	_ZN2v88internal13SlotCount_307EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10FixedArrayEEE, @function
_ZN2v88internal13SlotCount_307EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10FixedArrayEEE:
.LFB22435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-568(%rbp), %r14
	leaq	-184(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-664(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$664, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -664(%rbp)
	movq	%rdi, -624(%rbp)
	movl	$24, %edi
	movq	$0, -616(%rbp)
	movq	$0, -608(%rbp)
	movq	$0, -600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	$0, 16(%rax)
	movq	%rax, -616(%rbp)
	leaq	-624(%rbp), %rax
	movq	%rdx, -600(%rbp)
	movq	%rdx, -608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -584(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$48, %edi
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	%rax, -432(%rbp)
	movq	$0, -408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-376(%rbp), %rcx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -408(%rbp)
	movq	%rdx, -416(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -680(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -392(%rbp)
	movq	%rax, -424(%rbp)
	movq	$0, -400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r15, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-704(%rbp), %rdi
	movq	%r13, (%rax)
	leaq	-656(%rbp), %r13
	leaq	8(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L30
	call	_ZdlPv@PLT
.L30:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -560(%rbp)
	jne	.L98
.L31:
	cmpq	$0, -368(%rbp)
	leaq	-240(%rbp), %rbx
	jne	.L99
.L34:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movl	$1543, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L37
	call	_ZdlPv@PLT
.L37:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	8(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L38
	call	_ZdlPv@PLT
.L38:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L39
	.p2align 4,,10
	.p2align 3
.L43:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L40
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L43
.L41:
	movq	-232(%rbp), %r15
.L39:
	testq	%r15, %r15
	je	.L44
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L44:
	movq	-680(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L45
	call	_ZdlPv@PLT
.L45:
	movq	-416(%rbp), %rbx
	movq	-424(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L46
	.p2align 4,,10
	.p2align 3
.L50:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L47
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L50
.L48:
	movq	-424(%rbp), %r15
.L46:
	testq	%r15, %r15
	je	.L51
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L51:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L52
	call	_ZdlPv@PLT
.L52:
	movq	-608(%rbp), %rbx
	movq	-616(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L53
	.p2align 4,,10
	.p2align 3
.L57:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L54
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L57
.L55:
	movq	-616(%rbp), %r14
.L53:
	testq	%r14, %r14
	je	.L58
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L58:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L100
	addq	$664, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L57
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L40:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L43
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L47:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L50
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L98:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-704(%rbp), %rdi
	movq	%r13, %rsi
	movb	$7, (%rax)
	leaq	1(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L32
	movq	%rax, -704(%rbp)
	call	_ZdlPv@PLT
	movq	-704(%rbp), %rax
.L32:
	movq	(%rax), %rax
	movl	$36, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, -688(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-688(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-704(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -688(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$-1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-688(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %rbx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movl	$2, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler7WordSarENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler7WordAndENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -688(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movq	-704(%rbp), %xmm0
	movaps	%xmm1, -656(%rbp)
	movhps	-688(%rbp), %xmm0
	movq	$0, -640(%rbp)
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movdqa	-704(%rbp), %xmm0
	leaq	-432(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -656(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L33
	call	_ZdlPv@PLT
.L33:
	movq	-680(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L99:
	movq	-680(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-432(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movl	$1543, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L35
	call	_ZdlPv@PLT
.L35:
	movq	(%rbx), %rax
	movl	$31, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movhps	-704(%rbp), %xmm0
	movaps	%xmm1, -656(%rbp)
	leaq	-240(%rbp), %rbx
	movaps	%xmm0, -704(%rbp)
	movq	$0, -640(%rbp)
	call	_Znwm@PLT
	movdqa	-704(%rbp), %xmm0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -656(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L36
	call	_ZdlPv@PLT
.L36:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L34
.L100:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22435:
	.size	_ZN2v88internal13SlotCount_307EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10FixedArrayEEE, .-_ZN2v88internal13SlotCount_307EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10FixedArrayEEE
	.section	.text._ZN2v88internal21FirstIndexForSlot_308EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3SmiEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21FirstIndexForSlot_308EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3SmiEEE
	.type	_ZN2v88internal21FirstIndexForSlot_308EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3SmiEEE, @function
_ZN2v88internal21FirstIndexForSlot_308EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3SmiEEE:
.LFB22442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-600(%rbp), %r14
	leaq	-216(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-688(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-696(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$712, %rsp
	movq	%rsi, -736(%rbp)
	movq	%rdx, -744(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -696(%rbp)
	movq	%rdi, -656(%rbp)
	movl	$48, %edi
	movq	$0, -648(%rbp)
	movq	$0, -640(%rbp)
	movq	$0, -632(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -648(%rbp)
	leaq	-656(%rbp), %rax
	movq	%rdx, -632(%rbp)
	movq	%rdx, -640(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -616(%rbp)
	movq	%rax, -720(%rbp)
	movq	$0, -624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	%rax, -464(%rbp)
	movq	$0, -440(%rbp)
	call	_Znwm@PLT
	leaq	-408(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -440(%rbp)
	movq	%rdx, -448(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -712(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -424(%rbp)
	movq	%rax, -456(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	%rax, -272(%rbp)
	movq	$0, -248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -248(%rbp)
	movq	%rdx, -256(%rbp)
	xorl	%edx, %edx
	movq	%rax, -264(%rbp)
	movups	%xmm0, -232(%rbp)
	movq	$0, -240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-736(%rbp), %xmm1
	movaps	%xmm0, -688(%rbp)
	movhps	-744(%rbp), %xmm1
	movq	$0, -672(%rbp)
	movaps	%xmm1, -736(%rbp)
	call	_Znwm@PLT
	movdqa	-736(%rbp), %xmm1
	movq	-720(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -688(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L102
	call	_ZdlPv@PLT
.L102:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -592(%rbp)
	jne	.L170
.L103:
	cmpq	$0, -400(%rbp)
	leaq	-272(%rbp), %rbx
	jne	.L171
.L106:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$1543, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$6, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L109
	call	_ZdlPv@PLT
.L109:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L110
	call	_ZdlPv@PLT
.L110:
	movq	-256(%rbp), %rbx
	movq	-264(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L111
	.p2align 4,,10
	.p2align 3
.L115:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L112
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L115
.L113:
	movq	-264(%rbp), %r15
.L111:
	testq	%r15, %r15
	je	.L116
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L116:
	movq	-712(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L117
	call	_ZdlPv@PLT
.L117:
	movq	-448(%rbp), %rbx
	movq	-456(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L118
	.p2align 4,,10
	.p2align 3
.L122:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L119
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L122
.L120:
	movq	-456(%rbp), %r15
.L118:
	testq	%r15, %r15
	je	.L123
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L123:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L124
	call	_ZdlPv@PLT
.L124:
	movq	-640(%rbp), %rbx
	movq	-648(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L125
	.p2align 4,,10
	.p2align 3
.L129:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L126
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L129
.L127:
	movq	-648(%rbp), %r14
.L125:
	testq	%r14, %r14
	je	.L130
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L130:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L172
	addq	$712, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L129
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L112:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L115
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L119:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L122
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L170:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$1543, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	movq	-720(%rbp), %rdi
	leaq	2(%rax), %rdx
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L104
	movq	%rax, -720(%rbp)
	call	_ZdlPv@PLT
	movq	-720(%rbp), %rax
.L104:
	movq	(%rax), %rax
	movl	$41, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rcx, -736(%rbp)
	movq	%rax, -720(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-720(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movl	$2, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$24, %edi
	movq	%rbx, -64(%rbp)
	movq	-736(%rbp), %xmm0
	movq	$0, -672(%rbp)
	movhps	-720(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm2
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-464(%rbp), %rdi
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L105
	call	_ZdlPv@PLT
.L105:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L171:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-464(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$1543, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$6, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L107
	call	_ZdlPv@PLT
.L107:
	movq	(%rbx), %rax
	movl	$39, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -736(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rcx, -720(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %edi
	movq	%rbx, -64(%rbp)
	movq	-720(%rbp), %xmm0
	movq	$0, -672(%rbp)
	leaq	-272(%rbp), %rbx
	movhps	-736(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	-80(%rbp), %xmm3
	leaq	24(%rax), %rdx
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L108
	call	_ZdlPv@PLT
.L108:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L106
.L172:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22442:
	.size	_ZN2v88internal21FirstIndexForSlot_308EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3SmiEEE, .-_ZN2v88internal21FirstIndexForSlot_308EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3SmiEEE
	.section	.rodata._ZN2v88internal31UnsafeCast14ATCoverageInfo_1462EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../../deps/v8/../../deps/v8/src/builtins/base.tq"
	.section	.text._ZN2v88internal31UnsafeCast14ATCoverageInfo_1462EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31UnsafeCast14ATCoverageInfo_1462EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal31UnsafeCast14ATCoverageInfo_1462EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal31UnsafeCast14ATCoverageInfo_1462EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE:
.LFB22512:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-216(%rbp), %r14
	leaq	-688(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-600(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-696(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-656(%rbp), %rbx
	subq	$696, %rsp
	movq	%rsi, -736(%rbp)
	movq	%rdx, -720(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -696(%rbp)
	movq	%rdi, -656(%rbp)
	movl	$48, %edi
	movq	$0, -648(%rbp)
	movq	$0, -640(%rbp)
	movq	$0, -632(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -632(%rbp)
	movq	%rdx, -640(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -616(%rbp)
	movq	%rax, -648(%rbp)
	movq	$0, -624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	%rax, -464(%rbp)
	movq	$0, -440(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -456(%rbp)
	leaq	-408(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -440(%rbp)
	movq	%rdx, -448(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -424(%rbp)
	movq	%rax, -712(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	%rax, -272(%rbp)
	movq	$0, -248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -248(%rbp)
	movq	%rdx, -256(%rbp)
	xorl	%edx, %edx
	movq	%rax, -264(%rbp)
	movups	%xmm0, -232(%rbp)
	movq	$0, -240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-736(%rbp), %xmm1
	movaps	%xmm0, -688(%rbp)
	movhps	-720(%rbp), %xmm1
	movq	$0, -672(%rbp)
	movaps	%xmm1, -736(%rbp)
	call	_Znwm@PLT
	movdqa	-736(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -688(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L174
	call	_ZdlPv@PLT
.L174:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -592(%rbp)
	jne	.L242
.L175:
	cmpq	$0, -400(%rbp)
	leaq	-272(%rbp), %rbx
	jne	.L243
.L178:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L181
	call	_ZdlPv@PLT
.L181:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	16(%rax), %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L182
	call	_ZdlPv@PLT
.L182:
	movq	-256(%rbp), %rbx
	movq	-264(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L183
	.p2align 4,,10
	.p2align 3
.L187:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L184
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L187
.L185:
	movq	-264(%rbp), %r14
.L183:
	testq	%r14, %r14
	je	.L188
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L188:
	movq	-712(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L189
	call	_ZdlPv@PLT
.L189:
	movq	-448(%rbp), %rbx
	movq	-456(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L190
	.p2align 4,,10
	.p2align 3
.L194:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L191
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L194
.L192:
	movq	-456(%rbp), %r14
.L190:
	testq	%r14, %r14
	je	.L195
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L195:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L196
	call	_ZdlPv@PLT
.L196:
	movq	-640(%rbp), %rbx
	movq	-648(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L197
	.p2align 4,,10
	.p2align 3
.L201:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L198
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L201
.L199:
	movq	-648(%rbp), %r13
.L197:
	testq	%r13, %r13
	je	.L202
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L202:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L244
	addq	$696, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L201
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L184:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L187
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L191:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L194
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L242:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r15, %rsi
	movw	%di, (%rax)
	leaq	2(%rax), %rdx
	movq	%rbx, %rdi
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L176
	call	_ZdlPv@PLT
.L176:
	movq	(%rbx), %rax
	movl	$2790, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	%rcx, -736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm2
	movl	$24, %edi
	movq	-736(%rbp), %xmm0
	movq	%rbx, -64(%rbp)
	movq	$0, -672(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm3
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-464(%rbp), %rdi
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L177
	call	_ZdlPv@PLT
.L177:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L243:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-464(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rsi
	movb	$7, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L179
	call	_ZdlPv@PLT
.L179:
	movq	(%rbx), %rax
	movl	$28, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -720(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rcx, -736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %edi
	movq	%rbx, -64(%rbp)
	movq	-736(%rbp), %xmm0
	movq	$0, -672(%rbp)
	leaq	-272(%rbp), %rbx
	movhps	-720(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movdqa	-80(%rbp), %xmm4
	leaq	24(%rax), %rdx
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L180
	call	_ZdlPv@PLT
.L180:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L178
.L244:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22512:
	.size	_ZN2v88internal31UnsafeCast14ATCoverageInfo_1462EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal31UnsafeCast14ATCoverageInfo_1462EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal19GetCoverageInfo_306EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19GetCoverageInfo_306EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEEPNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal19GetCoverageInfo_306EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEEPNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal19GetCoverageInfo_306EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEEPNS1_18CodeAssemblerLabelE:
.LFB22410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-2016(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2144(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-2184(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$2296, %rsp
	movq	%rsi, -2288(%rbp)
	movq	%rdx, -2304(%rbp)
	movq	%rcx, -2312(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -2184(%rbp)
	movq	%rdi, -2016(%rbp)
	movl	$48, %edi
	movq	$0, -2008(%rbp)
	movq	$0, -2000(%rbp)
	movq	$0, -1992(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -2008(%rbp)
	leaq	-1960(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1992(%rbp)
	movq	%rdx, -2000(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1976(%rbp)
	movq	%rax, -2200(%rbp)
	movq	$0, -1984(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2184(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1816(%rbp)
	movq	$0, -1808(%rbp)
	movq	%rax, -1824(%rbp)
	movq	$0, -1800(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1816(%rbp)
	leaq	-1768(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1800(%rbp)
	movq	%rdx, -1808(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1784(%rbp)
	movq	%rax, -2272(%rbp)
	movq	$0, -1792(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2184(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1624(%rbp)
	movq	$0, -1616(%rbp)
	movq	%rax, -1632(%rbp)
	movq	$0, -1608(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1624(%rbp)
	leaq	-1576(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1608(%rbp)
	movq	%rdx, -1616(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1592(%rbp)
	movq	%rax, -2232(%rbp)
	movq	$0, -1600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2184(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1432(%rbp)
	movq	$0, -1424(%rbp)
	movq	%rax, -1440(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1432(%rbp)
	leaq	-1384(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -2240(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2184(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -2224(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2184(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -2248(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2184(%rbp), %rax
	movl	$96, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -2264(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2184(%rbp), %rax
	movl	$72, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -2256(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2184(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	$0, -472(%rbp)
	movq	%rax, -480(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -2216(%rbp)
	movq	$0, -464(%rbp)
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	$0, -440(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2184(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -280(%rbp)
	leaq	-232(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -2208(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-2288(%rbp), %xmm1
	movaps	%xmm0, -2144(%rbp)
	movhps	-2304(%rbp), %xmm1
	movq	$0, -2128(%rbp)
	movaps	%xmm1, -2288(%rbp)
	call	_Znwm@PLT
	movdqa	-2288(%rbp), %xmm1
	movq	%r14, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -2144(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L246
	call	_ZdlPv@PLT
.L246:
	movq	-2200(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1952(%rbp)
	jne	.L473
	cmpq	$0, -1760(%rbp)
	jne	.L474
.L252:
	cmpq	$0, -1568(%rbp)
	jne	.L475
.L255:
	cmpq	$0, -1376(%rbp)
	jne	.L476
.L258:
	cmpq	$0, -1184(%rbp)
	jne	.L477
.L261:
	cmpq	$0, -992(%rbp)
	jne	.L478
.L265:
	cmpq	$0, -800(%rbp)
	jne	.L479
.L268:
	cmpq	$0, -608(%rbp)
	leaq	-288(%rbp), %r14
	jne	.L480
	cmpq	$0, -416(%rbp)
	jne	.L481
.L274:
	movq	-2208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -2144(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -2144(%rbp)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2144(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L276
	call	_ZdlPv@PLT
.L276:
	movq	(%rbx), %rax
	movq	-2208(%rbp), %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L277
	call	_ZdlPv@PLT
.L277:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L278
	.p2align 4,,10
	.p2align 3
.L282:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L279
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L282
.L280:
	movq	-280(%rbp), %r14
.L278:
	testq	%r14, %r14
	je	.L283
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L283:
	movq	-2216(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L284
	call	_ZdlPv@PLT
.L284:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L285
	.p2align 4,,10
	.p2align 3
.L289:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L286
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L289
.L287:
	movq	-472(%rbp), %r14
.L285:
	testq	%r14, %r14
	je	.L290
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L290:
	movq	-2256(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L291
	call	_ZdlPv@PLT
.L291:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L292
	.p2align 4,,10
	.p2align 3
.L296:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L293
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L296
.L294:
	movq	-664(%rbp), %r14
.L292:
	testq	%r14, %r14
	je	.L297
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L297:
	movq	-2264(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L298
	call	_ZdlPv@PLT
.L298:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L299
	.p2align 4,,10
	.p2align 3
.L303:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L300
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L303
.L301:
	movq	-856(%rbp), %r14
.L299:
	testq	%r14, %r14
	je	.L304
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L304:
	movq	-2248(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L305
	call	_ZdlPv@PLT
.L305:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L306
	.p2align 4,,10
	.p2align 3
.L310:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L307
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L310
.L308:
	movq	-1048(%rbp), %r14
.L306:
	testq	%r14, %r14
	je	.L311
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L311:
	movq	-2224(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L312
	call	_ZdlPv@PLT
.L312:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L313
	.p2align 4,,10
	.p2align 3
.L317:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L314
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L317
.L315:
	movq	-1240(%rbp), %r14
.L313:
	testq	%r14, %r14
	je	.L318
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L318:
	movq	-2240(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L319
	call	_ZdlPv@PLT
.L319:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L320
	.p2align 4,,10
	.p2align 3
.L324:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L321
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L324
.L322:
	movq	-1432(%rbp), %r14
.L320:
	testq	%r14, %r14
	je	.L325
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L325:
	movq	-2232(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L326
	call	_ZdlPv@PLT
.L326:
	movq	-1616(%rbp), %rbx
	movq	-1624(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L327
	.p2align 4,,10
	.p2align 3
.L331:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L328
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L331
.L329:
	movq	-1624(%rbp), %r14
.L327:
	testq	%r14, %r14
	je	.L332
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L332:
	movq	-2272(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L333
	call	_ZdlPv@PLT
.L333:
	movq	-1808(%rbp), %rbx
	movq	-1816(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L334
	.p2align 4,,10
	.p2align 3
.L338:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L335
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L338
.L336:
	movq	-1816(%rbp), %r14
.L334:
	testq	%r14, %r14
	je	.L339
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L339:
	movq	-2200(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L340
	call	_ZdlPv@PLT
.L340:
	movq	-2000(%rbp), %rbx
	movq	-2008(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L341
	.p2align 4,,10
	.p2align 3
.L345:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L342
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L345
.L343:
	movq	-2008(%rbp), %r14
.L341:
	testq	%r14, %r14
	je	.L346
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L346:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L482
	addq	$2296, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L342:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L345
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L335:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L338
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L328:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L331
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L321:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L324
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L314:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L317
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L307:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L310
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L300:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L303
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L293:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L296
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L279:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L282
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L286:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L289
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L473:
	movq	-2200(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -2144(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r8d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -2144(%rbp)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2144(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L248
	call	_ZdlPv@PLT
.L248:
	movq	(%r14), %rax
	movl	$23, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r15
	movq	8(%rax), %rax
	movq	%rax, -2304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-2304(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -2288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$24, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-2288(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rdx
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal18Cast9DebugInfo_142EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm6
	movq	%r15, %xmm3
	movq	-2288(%rbp), %xmm2
	movhps	-2304(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	%rax, -64(%rbp)
	punpcklqdq	%xmm6, %xmm2
	movaps	%xmm3, -2304(%rbp)
	leaq	-2176(%rbp), %r14
	movaps	%xmm2, -2288(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm2, -80(%rbp)
	movaps	%xmm0, -2176(%rbp)
	movq	$0, -2160(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm7
	movdqa	-80(%rbp), %xmm6
	leaq	40(%rax), %rdx
	leaq	-1632(%rbp), %rdi
	movq	%rax, -2176(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -2160(%rbp)
	movq	%rdx, -2168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L249
	call	_ZdlPv@PLT
.L249:
	movq	-2232(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2136(%rbp)
	jne	.L483
.L250:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1760(%rbp)
	je	.L252
.L474:
	movq	-2272(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1824(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -2144(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2144(%rbp)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2144(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L253
	call	_ZdlPv@PLT
.L253:
	movq	(%r14), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm7
	movaps	%xmm0, -2144(%rbp)
	movq	$0, -2128(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm7, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-1440(%rbp), %rdi
	movq	%rax, -2144(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L254
	call	_ZdlPv@PLT
.L254:
	movq	-2240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1568(%rbp)
	je	.L255
.L475:
	movq	-2232(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1632(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -2144(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2144(%rbp)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2144(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L256
	call	_ZdlPv@PLT
.L256:
	movq	(%r14), %rax
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	8(%rax), %rsi
	movq	16(%rax), %rcx
	movq	32(%rax), %rdx
	movq	(%rax), %rax
	movaps	%xmm0, -2144(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -2128(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm7
	movdqa	-80(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-1248(%rbp), %rdi
	movq	%rax, -2144(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L257
	call	_ZdlPv@PLT
.L257:
	movq	-2224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1376(%rbp)
	je	.L258
.L476:
	movq	-2240(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1440(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -2144(%rbp)
	call	_Znwm@PLT
	movl	$1799, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r14, %rdi
	movb	$7, 2(%rax)
	movq	%rax, -2144(%rbp)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L259
	call	_ZdlPv@PLT
.L259:
	movl	$25, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm0, %xmm0
	leaq	-480(%rbp), %rdi
	movq	%r13, %rsi
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -2144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L260
	call	_ZdlPv@PLT
.L260:
	movq	-2216(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1184(%rbp)
	je	.L261
.L477:
	movq	-2224(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1248(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -2144(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2144(%rbp)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2144(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L262
	call	_ZdlPv@PLT
.L262:
	movq	(%r14), %rax
	movl	$27, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	24(%rax), %r14
	movq	%rsi, -2304(%rbp)
	movq	16(%rax), %rsi
	movq	%rcx, -2288(%rbp)
	movq	%rsi, -2336(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%r14, -2320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler7WordAndENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-2336(%rbp), %xmm4
	movq	-2288(%rbp), %xmm5
	movaps	%xmm0, -2144(%rbp)
	movhps	-2320(%rbp), %xmm4
	movq	$0, -2128(%rbp)
	movhps	-2304(%rbp), %xmm5
	movaps	%xmm4, -2336(%rbp)
	movaps	%xmm5, -2288(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm7
	movdqa	-80(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-1056(%rbp), %rdi
	movq	%rax, -2144(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L263
	call	_ZdlPv@PLT
.L263:
	movdqa	-2288(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -2144(%rbp)
	movaps	%xmm7, -96(%rbp)
	movdqa	-2336(%rbp), %xmm7
	movaps	%xmm7, -80(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -2144(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L264
	call	_ZdlPv@PLT
.L264:
	movq	-2264(%rbp), %rcx
	movq	-2248(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -992(%rbp)
	je	.L265
.L478:
	movq	-2248(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -2144(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2144(%rbp)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L266
	call	_ZdlPv@PLT
.L266:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-480(%rbp), %rdi
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -2144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L267
	call	_ZdlPv@PLT
.L267:
	movq	-2216(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L268
.L479:
	movq	-2264(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -2144(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2144(%rbp)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2144(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L269
	call	_ZdlPv@PLT
.L269:
	movq	(%r14), %rax
	movl	$28, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	24(%rax), %r14
	movq	%rcx, -2304(%rbp)
	movq	%r15, -2288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE(%rip), %esi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rdx
	movq	-2288(%rbp), %r15
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal31UnsafeCast14ATCoverageInfo_1462EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	movq	%r15, %xmm0
	movl	$24, %edi
	movq	$0, -2128(%rbp)
	movhps	-2304(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2144(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm4
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -2144(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L270
	call	_ZdlPv@PLT
.L270:
	movq	-2256(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L480:
	movq	-2256(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -2144(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movq	%r14, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$7, 2(%rax)
	movq	%rax, -2144(%rbp)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2144(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L272
	call	_ZdlPv@PLT
.L272:
	movq	(%rbx), %rax
	movl	$21, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %r14
	movq	(%rax), %rbx
	movq	16(%rax), %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %xmm5
	movq	%rbx, %xmm0
	movl	$24, %edi
	punpcklqdq	%xmm5, %xmm0
	movq	%r15, -80(%rbp)
	leaq	-288(%rbp), %r14
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2144(%rbp)
	movq	$0, -2128(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm6
	leaq	24(%rax), %rdx
	movq	%rax, -2144(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L273
	call	_ZdlPv@PLT
.L273:
	movq	-2208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -416(%rbp)
	je	.L274
.L481:
	movq	-2216(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -2144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L275
	call	_ZdlPv@PLT
.L275:
	movq	-2312(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L483:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-2304(%rbp), %xmm7
	movdqa	-2288(%rbp), %xmm2
	movaps	%xmm0, -2176(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm2, -80(%rbp)
	movq	$0, -2160(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movq	%r14, %rsi
	leaq	32(%rax), %rdx
	leaq	-1824(%rbp), %rdi
	movq	%rax, -2176(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2160(%rbp)
	movq	%rdx, -2168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L251
	call	_ZdlPv@PLT
.L251:
	movq	-2272(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L250
.L482:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22410:
	.size	_ZN2v88internal19GetCoverageInfo_306EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEEPNS1_18CodeAssemblerLabelE, .-_ZN2v88internal19GetCoverageInfo_306EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEEPNS1_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_3SmiES5_S5_S4_NS0_7IntPtrTES6_S5_S5_S6_NS0_10HeapObjectES6_S6_S6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_SD_PNS9_IS6_EESH_SF_SF_SH_PNS9_IS7_EESH_SH_SH_SH_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_3SmiES5_S5_S4_NS0_7IntPtrTES6_S5_S5_S6_NS0_10HeapObjectES6_S6_S6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_SD_PNS9_IS6_EESH_SF_SF_SH_PNS9_IS7_EESH_SH_SH_SH_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_3SmiES5_S5_S4_NS0_7IntPtrTES6_S5_S5_S6_NS0_10HeapObjectES6_S6_S6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_SD_PNS9_IS6_EESH_SF_SF_SH_PNS9_IS7_EESH_SH_SH_SH_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_3SmiES5_S5_S4_NS0_7IntPtrTES6_S5_S5_S6_NS0_10HeapObjectES6_S6_S6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_SD_PNS9_IS6_EESH_SF_SF_SH_PNS9_IS7_EESH_SH_SH_SH_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_3SmiES5_S5_S4_NS0_7IntPtrTES6_S5_S5_S6_NS0_10HeapObjectES6_S6_S6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_SD_PNS9_IS6_EESH_SF_SF_SH_PNS9_IS7_EESH_SH_SH_SH_:
.LFB26821:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$16, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$152, %rsp
	movq	16(%rbp), %rax
	movq	%rdx, -88(%rbp)
	movq	88(%rbp), %r15
	movq	96(%rbp), %r14
	movq	%r8, -96(%rbp)
	movq	%rax, -112(%rbp)
	movq	24(%rbp), %rax
	movq	%r9, -104(%rbp)
	movq	%rax, -120(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movabsq	$361703067525449479, %rsi
	movabsq	$361700864223938054, %rdi
	movq	%rsi, (%rax)
	leaq	16(%rax), %rdx
	leaq	-80(%rbp), %rsi
	movq	%rdi, 8(%rax)
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L485
	movq	%rax, -184(%rbp)
	call	_ZdlPv@PLT
	movq	-184(%rbp), %rax
.L485:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L486
	movq	%rdx, 0(%r13)
.L486:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L487
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L487:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L488
	movq	%rdx, (%rbx)
.L488:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L489
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L489:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L490
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L490:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L491
	movq	-112(%rbp), %rcx
	movq	%rdx, (%rcx)
.L491:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L492
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L492:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L493
	movq	-128(%rbp), %rcx
	movq	%rdx, (%rcx)
.L493:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L494
	movq	-136(%rbp), %rbx
	movq	%rdx, (%rbx)
.L494:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L495
	movq	-144(%rbp), %rcx
	movq	%rdx, (%rcx)
.L495:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L496
	movq	-152(%rbp), %rbx
	movq	%rdx, (%rbx)
.L496:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L497
	movq	-160(%rbp), %rsi
	movq	%rdx, (%rsi)
.L497:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L498
	movq	-168(%rbp), %rcx
	movq	%rdx, (%rcx)
.L498:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L499
	movq	-176(%rbp), %rbx
	movq	%rdx, (%rbx)
.L499:
	movq	112(%rax), %rdx
	testq	%rdx, %rdx
	je	.L500
	movq	%rdx, (%r15)
.L500:
	movq	120(%rax), %rax
	testq	%rax, %rax
	je	.L484
	movq	%rax, (%r14)
.L484:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L555
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L555:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26821:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_3SmiES5_S5_S4_NS0_7IntPtrTES6_S5_S5_S6_NS0_10HeapObjectES6_S6_S6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_SD_PNS9_IS6_EESH_SF_SF_SH_PNS9_IS7_EESH_SH_SH_SH_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_3SmiES5_S5_S4_NS0_7IntPtrTES6_S5_S5_S6_NS0_10HeapObjectES6_S6_S6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_SD_PNS9_IS6_EESH_SF_SF_SH_PNS9_IS7_EESH_SH_SH_SH_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_3SmiES5_S5_S4_NS0_7IntPtrTES6_S5_S5_NS0_10HeapObjectES6_S4_S6_S6_S5_S5_S6_S7_S6_S6_S6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_SD_PNS9_IS6_EESH_SF_SF_PNS9_IS7_EESH_SD_SH_SH_SF_SF_SH_SJ_SH_SH_SH_SH_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_3SmiES5_S5_S4_NS0_7IntPtrTES6_S5_S5_NS0_10HeapObjectES6_S4_S6_S6_S5_S5_S6_S7_S6_S6_S6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_SD_PNS9_IS6_EESH_SF_SF_PNS9_IS7_EESH_SD_SH_SH_SF_SF_SH_SJ_SH_SH_SH_SH_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_3SmiES5_S5_S4_NS0_7IntPtrTES6_S5_S5_NS0_10HeapObjectES6_S4_S6_S6_S5_S5_S6_S7_S6_S6_S6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_SD_PNS9_IS6_EESH_SF_SF_PNS9_IS7_EESH_SD_SH_SH_SF_SF_SH_SJ_SH_SH_SH_SH_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_3SmiES5_S5_S4_NS0_7IntPtrTES6_S5_S5_NS0_10HeapObjectES6_S4_S6_S6_S5_S5_S6_S7_S6_S6_S6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_SD_PNS9_IS6_EESH_SF_SF_PNS9_IS7_EESH_SD_SH_SH_SF_SF_SH_SJ_SH_SH_SH_SH_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_3SmiES5_S5_S4_NS0_7IntPtrTES6_S5_S5_NS0_10HeapObjectES6_S4_S6_S6_S5_S5_S6_S7_S6_S6_S6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_SD_PNS9_IS6_EESH_SF_SF_PNS9_IS7_EESH_SD_SH_SH_SF_SF_SH_SJ_SH_SH_SH_SH_:
.LFB26841:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$23, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	88(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	96(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	104(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	112(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	120(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	128(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	136(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	144(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	152(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$1285, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movdqa	.LC3(%rip), %xmm0
	movl	$84346118, 16(%rax)
	leaq	23(%rax), %rdx
	movw	%cx, 20(%rax)
	movb	$5, 22(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L557
	movq	%rax, -240(%rbp)
	call	_ZdlPv@PLT
	movq	-240(%rbp), %rax
.L557:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L558
	movq	%rdx, (%r15)
.L558:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L559
	movq	%rdx, (%r14)
.L559:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L560
	movq	%rdx, 0(%r13)
.L560:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L561
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L561:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L562
	movq	%rdx, (%rbx)
.L562:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L563
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L563:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L564
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L564:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L565
	movq	-112(%rbp), %rcx
	movq	%rdx, (%rcx)
.L565:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L566
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L566:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L567
	movq	-128(%rbp), %rcx
	movq	%rdx, (%rcx)
.L567:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L568
	movq	-136(%rbp), %rbx
	movq	%rdx, (%rbx)
.L568:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L569
	movq	-144(%rbp), %rsi
	movq	%rdx, (%rsi)
.L569:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L570
	movq	-152(%rbp), %rcx
	movq	%rdx, (%rcx)
.L570:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L571
	movq	-160(%rbp), %rbx
	movq	%rdx, (%rbx)
.L571:
	movq	112(%rax), %rdx
	testq	%rdx, %rdx
	je	.L572
	movq	-168(%rbp), %rsi
	movq	%rdx, (%rsi)
.L572:
	movq	120(%rax), %rdx
	testq	%rdx, %rdx
	je	.L573
	movq	-176(%rbp), %rdi
	movq	%rdx, (%rdi)
.L573:
	movq	128(%rax), %rdx
	testq	%rdx, %rdx
	je	.L574
	movq	-184(%rbp), %rcx
	movq	%rdx, (%rcx)
.L574:
	movq	136(%rax), %rdx
	testq	%rdx, %rdx
	je	.L575
	movq	-192(%rbp), %rbx
	movq	%rdx, (%rbx)
.L575:
	movq	144(%rax), %rdx
	testq	%rdx, %rdx
	je	.L576
	movq	-200(%rbp), %rsi
	movq	%rdx, (%rsi)
.L576:
	movq	152(%rax), %rdx
	testq	%rdx, %rdx
	je	.L577
	movq	-208(%rbp), %rdi
	movq	%rdx, (%rdi)
.L577:
	movq	160(%rax), %rdx
	testq	%rdx, %rdx
	je	.L578
	movq	-216(%rbp), %rcx
	movq	%rdx, (%rcx)
.L578:
	movq	168(%rax), %rdx
	testq	%rdx, %rdx
	je	.L579
	movq	-224(%rbp), %rbx
	movq	%rdx, (%rbx)
.L579:
	movq	176(%rax), %rax
	testq	%rax, %rax
	je	.L556
	movq	-232(%rbp), %rsi
	movq	%rax, (%rsi)
.L556:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L655
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L655:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26841:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_3SmiES5_S5_S4_NS0_7IntPtrTES6_S5_S5_NS0_10HeapObjectES6_S4_S6_S6_S5_S5_S6_S7_S6_S6_S6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_SD_PNS9_IS6_EESH_SF_SF_PNS9_IS7_EESH_SD_SH_SH_SF_SF_SH_SJ_SH_SH_SH_SH_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_3SmiES5_S5_S4_NS0_7IntPtrTES6_S5_S5_NS0_10HeapObjectES6_S4_S6_S6_S5_S5_S6_S7_S6_S6_S6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_SD_PNS9_IS6_EESH_SF_SF_PNS9_IS7_EESH_SD_SH_SH_SF_SF_SH_SJ_SH_SH_SH_SH_
	.section	.rodata._ZN2v88internal23IncrementBlockCount_309EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_3SmiEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"../../deps/v8/../../deps/v8/src/builtins/torque-internal.tq"
	.section	.text._ZN2v88internal23IncrementBlockCount_309EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_3SmiEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23IncrementBlockCount_309EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_3SmiEEE
	.type	_ZN2v88internal23IncrementBlockCount_309EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_3SmiEEE, @function
_ZN2v88internal23IncrementBlockCount_309EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_3SmiEEE:
.LFB22449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	leaq	-3360(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-3080(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$3672, %rsp
	movq	%rsi, -3368(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -3360(%rbp)
	movq	%rdi, -3136(%rbp)
	movl	$72, %edi
	movq	$0, -3128(%rbp)
	movq	$0, -3120(%rbp)
	movq	$0, -3112(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r12, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -3112(%rbp)
	movq	%rdx, -3120(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3096(%rbp)
	movq	%rax, -3128(%rbp)
	movq	$0, -3104(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3360(%rbp), %rax
	movl	$384, %edi
	movq	$0, -2936(%rbp)
	movq	$0, -2928(%rbp)
	movq	%rax, -2944(%rbp)
	movq	$0, -2920(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	384(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movq	%rax, -2936(%rbp)
	leaq	-2888(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2920(%rbp)
	movq	%rdx, -2928(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2904(%rbp)
	movq	%rax, -3376(%rbp)
	movq	$0, -2912(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3360(%rbp), %rax
	movl	$384, %edi
	movq	$0, -2744(%rbp)
	movq	$0, -2736(%rbp)
	movq	%rax, -2752(%rbp)
	movq	$0, -2728(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	384(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movq	%rax, -2744(%rbp)
	leaq	-2696(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2728(%rbp)
	movq	%rdx, -2736(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2712(%rbp)
	movq	%rax, -3448(%rbp)
	movq	$0, -2720(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3360(%rbp), %rax
	movl	$432, %edi
	movq	$0, -2552(%rbp)
	movq	$0, -2544(%rbp)
	movq	%rax, -2560(%rbp)
	movq	$0, -2536(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movups	%xmm0, (%rax)
	leaq	432(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movq	%rax, -2552(%rbp)
	leaq	-2504(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2536(%rbp)
	movq	%rdx, -2544(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2520(%rbp)
	movq	$0, -2528(%rbp)
	movq	%rax, -3408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3360(%rbp), %rax
	movl	$264, %edi
	movq	$0, -2360(%rbp)
	movq	$0, -2352(%rbp)
	movq	%rax, -2368(%rbp)
	movq	$0, -2344(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -2360(%rbp)
	leaq	-2312(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2344(%rbp)
	movq	%rdx, -2352(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2328(%rbp)
	movq	%rax, -3424(%rbp)
	movq	$0, -2336(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3360(%rbp), %rax
	movl	$312, %edi
	movq	$0, -2168(%rbp)
	movq	$0, -2160(%rbp)
	movq	%rax, -2176(%rbp)
	movq	$0, -2152(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rax, -2168(%rbp)
	leaq	-2120(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2152(%rbp)
	movq	%rdx, -2160(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2136(%rbp)
	movq	%rax, -3440(%rbp)
	movq	$0, -2144(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3360(%rbp), %rax
	movl	$288, %edi
	movq	$0, -1976(%rbp)
	movq	$0, -1968(%rbp)
	movq	%rax, -1984(%rbp)
	movq	$0, -1960(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -1976(%rbp)
	leaq	-1928(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1960(%rbp)
	movq	%rdx, -1968(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1944(%rbp)
	movq	%rax, -3512(%rbp)
	movq	$0, -1952(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3360(%rbp), %rax
	movl	$552, %edi
	movq	$0, -1784(%rbp)
	movq	$0, -1776(%rbp)
	movq	%rax, -1792(%rbp)
	movq	$0, -1768(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	552(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -1784(%rbp)
	movq	%rdx, -1768(%rbp)
	movups	%xmm0, 480(%rax)
	movq	$0, 544(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	leaq	-1736(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1776(%rbp)
	xorl	%edx, %edx
	movq	%rax, -3528(%rbp)
	movq	$0, -1760(%rbp)
	movups	%xmm0, -1752(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3360(%rbp), %rax
	movl	$552, %edi
	movq	$0, -1592(%rbp)
	movq	$0, -1584(%rbp)
	movq	%rax, -1600(%rbp)
	movq	$0, -1576(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	552(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -1592(%rbp)
	movq	%rdx, -1576(%rbp)
	movups	%xmm0, 480(%rax)
	movq	$0, 544(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	leaq	-1544(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1584(%rbp)
	xorl	%edx, %edx
	movq	%rax, -3464(%rbp)
	movq	$0, -1568(%rbp)
	movups	%xmm0, -1560(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3360(%rbp), %rax
	movl	$600, %edi
	movq	$0, -1400(%rbp)
	movq	$0, -1392(%rbp)
	movq	%rax, -1408(%rbp)
	movq	$0, -1384(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	600(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -1400(%rbp)
	movq	%rdx, -1384(%rbp)
	movups	%xmm0, 480(%rax)
	movq	$0, 592(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	movups	%xmm0, 544(%rax)
	movups	%xmm0, 560(%rax)
	movups	%xmm0, 576(%rax)
	leaq	-1352(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1392(%rbp)
	xorl	%edx, %edx
	movq	%rax, -3496(%rbp)
	movq	$0, -1376(%rbp)
	movups	%xmm0, -1368(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3360(%rbp), %rax
	movl	$432, %edi
	movq	$0, -1208(%rbp)
	movq	$0, -1200(%rbp)
	movq	%rax, -1216(%rbp)
	movq	$0, -1192(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movups	%xmm0, (%rax)
	leaq	432(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movq	%rax, -1208(%rbp)
	leaq	-1160(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1192(%rbp)
	movq	%rdx, -1200(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1176(%rbp)
	movq	$0, -1184(%rbp)
	movq	%rax, -3504(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3360(%rbp), %rax
	movl	$480, %edi
	movq	$0, -1016(%rbp)
	movq	$0, -1008(%rbp)
	movq	%rax, -1024(%rbp)
	movq	$0, -1000(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movups	%xmm0, (%rax)
	leaq	480(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -1016(%rbp)
	leaq	-968(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1000(%rbp)
	movq	%rdx, -1008(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -984(%rbp)
	movq	%rax, -3520(%rbp)
	movq	$0, -992(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3360(%rbp), %rax
	movl	$456, %edi
	movq	$0, -824(%rbp)
	movq	$0, -816(%rbp)
	movq	%rax, -832(%rbp)
	movq	$0, -808(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movups	%xmm0, (%rax)
	leaq	456(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movq	%rax, -824(%rbp)
	movq	$0, 448(%rax)
	leaq	-776(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -808(%rbp)
	movq	%rdx, -816(%rbp)
	xorl	%edx, %edx
	movq	$0, -800(%rbp)
	movups	%xmm0, -792(%rbp)
	movq	%rax, -3488(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3360(%rbp), %rax
	movl	$72, %edi
	movq	$0, -632(%rbp)
	movq	$0, -624(%rbp)
	movq	%rax, -640(%rbp)
	movq	$0, -616(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -632(%rbp)
	leaq	-584(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -616(%rbp)
	movq	%rdx, -624(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -600(%rbp)
	movq	%rax, -3536(%rbp)
	movq	$0, -608(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3360(%rbp), %rax
	movl	$72, %edi
	movq	$0, -440(%rbp)
	movq	$0, -432(%rbp)
	movq	%rax, -448(%rbp)
	movq	$0, -424(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -440(%rbp)
	leaq	-392(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -424(%rbp)
	movq	%rdx, -432(%rbp)
	xorl	%edx, %edx
	movq	%rax, -3480(%rbp)
	movups	%xmm0, -408(%rbp)
	movq	$0, -416(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3368(%rbp), %r9
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	%r14, -248(%rbp)
	leaq	-3168(%rbp), %r14
	movq	%r9, -256(%rbp)
	movaps	%xmm0, -3168(%rbp)
	movq	%rbx, -240(%rbp)
	movq	$0, -3152(%rbp)
	call	_Znwm@PLT
	movq	-240(%rbp), %rcx
	movdqa	-256(%rbp), %xmm2
	movq	%r14, %rsi
	leaq	24(%rax), %rdx
	movq	%rax, -3168(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	leaq	-3136(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3152(%rbp)
	movq	%rdx, -3160(%rbp)
	movq	%rax, -3472(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L657
	call	_ZdlPv@PLT
.L657:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2944(%rbp), %rax
	cmpq	$0, -3072(%rbp)
	movq	%rax, -3384(%rbp)
	leaq	-2752(%rbp), %rax
	movq	%rax, -3392(%rbp)
	jne	.L789
.L658:
	leaq	-2560(%rbp), %rax
	cmpq	$0, -2880(%rbp)
	movq	%rax, -3400(%rbp)
	jne	.L790
.L662:
	leaq	-2368(%rbp), %rax
	cmpq	$0, -2688(%rbp)
	movq	%rax, -3368(%rbp)
	jne	.L791
.L664:
	leaq	-2176(%rbp), %rax
	cmpq	$0, -2496(%rbp)
	movq	%rax, -3376(%rbp)
	jne	.L792
	cmpq	$0, -2304(%rbp)
	jne	.L793
.L669:
	leaq	-1984(%rbp), %rax
	cmpq	$0, -2112(%rbp)
	movq	%rax, -3448(%rbp)
	jne	.L794
.L671:
	leaq	-1792(%rbp), %rax
	cmpq	$0, -1920(%rbp)
	movq	%rax, -3456(%rbp)
	leaq	-1600(%rbp), %rax
	movq	%rax, -3408(%rbp)
	jne	.L795
.L674:
	leaq	-1408(%rbp), %rax
	cmpq	$0, -1728(%rbp)
	movq	%rax, -3424(%rbp)
	jne	.L796
.L678:
	leaq	-1216(%rbp), %rax
	cmpq	$0, -1536(%rbp)
	movq	%rax, -3440(%rbp)
	jne	.L797
.L680:
	cmpq	$0, -1344(%rbp)
	leaq	-1024(%rbp), %rbx
	jne	.L798
.L682:
	cmpq	$0, -1152(%rbp)
	jne	.L799
.L685:
	cmpq	$0, -960(%rbp)
	leaq	-832(%rbp), %r12
	jne	.L800
.L687:
	leaq	-640(%rbp), %rax
	cmpq	$0, -768(%rbp)
	movq	%rax, -3464(%rbp)
	jne	.L801
.L690:
	cmpq	$0, -576(%rbp)
	leaq	-448(%rbp), %r13
	jne	.L802
.L693:
	movq	-3480(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -3152(%rbp)
	movaps	%xmm0, -3168(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%r13, %rdi
	movq	%r14, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$6, 2(%rax)
	movq	%rax, -3168(%rbp)
	movq	%rdx, -3152(%rbp)
	movq	%rdx, -3160(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L696
	call	_ZdlPv@PLT
.L696:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3464(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3440(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3424(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3408(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3456(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3448(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3376(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3368(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3400(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3392(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3384(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3472(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L803
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L789:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -3152(%rbp)
	movaps	%xmm0, -3168(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r11d
	movq	-3472(%rbp), %rdi
	movq	%r14, %rsi
	movw	%r11w, (%rax)
	leaq	3(%rax), %rdx
	movb	$6, 2(%rax)
	movq	%rax, -3168(%rbp)
	movq	%rdx, -3152(%rbp)
	movq	%rdx, -3160(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3168(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L659
	call	_ZdlPv@PLT
.L659:
	movq	(%rbx), %rax
	movl	$47, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %rax
	movq	%rcx, -3392(%rbp)
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r12, -3568(%rbp)
	movq	%rbx, -3384(%rbp)
	call	_ZN2v88internal21FirstIndexForSlot_308EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3SmiEEE
	movl	$48, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	movq	%rax, -3600(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$2, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -3400(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$49, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$8, %esi
	movq	%r15, %rdi
	movq	%rax, -3456(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-3392(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$55, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3400(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$56, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -3368(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$37, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3368(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -3544(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3544(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-256(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -3544(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-128(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	-3456(%rbp), %rax
	movq	%r14, %rdi
	movq	-3400(%rbp), %xmm1
	movq	%rdx, -3616(%rbp)
	movq	-3392(%rbp), %xmm2
	movaps	%xmm0, -3168(%rbp)
	movq	-3568(%rbp), %xmm4
	movq	%rax, -208(%rbp)
	movq	-3368(%rbp), %rax
	movdqa	%xmm1, %xmm3
	movq	-3384(%rbp), %xmm5
	punpcklqdq	%xmm2, %xmm3
	movhps	-3600(%rbp), %xmm4
	movq	%xmm1, -192(%rbp)
	movq	%rax, -176(%rbp)
	movq	-3456(%rbp), %rax
	punpcklqdq	%xmm2, %xmm5
	movaps	%xmm3, -3584(%rbp)
	movq	%rax, -160(%rbp)
	movq	-3368(%rbp), %rax
	movaps	%xmm4, -3600(%rbp)
	movq	%rax, -144(%rbp)
	movq	%rax, -136(%rbp)
	movaps	%xmm5, -3568(%rbp)
	movaps	%xmm5, -256(%rbp)
	movaps	%xmm4, -240(%rbp)
	movaps	%xmm3, -224(%rbp)
	movq	%xmm1, -184(%rbp)
	movq	%xmm2, -168(%rbp)
	movq	%rbx, -200(%rbp)
	movq	%rbx, -152(%rbp)
	movq	$0, -3152(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2944(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -3384(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3168(%rbp), %rdi
	movq	-3616(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L660
	call	_ZdlPv@PLT
	movq	-3616(%rbp), %rdx
.L660:
	movq	-3400(%rbp), %xmm0
	movq	%rbx, %xmm5
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqa	-3568(%rbp), %xmm6
	movdqa	-3600(%rbp), %xmm7
	movq	$0, -3152(%rbp)
	movq	-3368(%rbp), %xmm4
	punpcklqdq	%xmm0, %xmm0
	movq	-3456(%rbp), %xmm1
	movaps	%xmm0, -192(%rbp)
	movdqa	%xmm4, %xmm0
	punpcklqdq	%xmm5, %xmm1
	movdqa	-3584(%rbp), %xmm5
	movaps	%xmm6, -256(%rbp)
	movhps	-3392(%rbp), %xmm0
	movaps	%xmm7, -240(%rbp)
	movaps	%xmm0, -176(%rbp)
	movdqa	%xmm4, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm5, -224(%rbp)
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -3168(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2752(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -3392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L661
	call	_ZdlPv@PLT
.L661:
	movq	-3448(%rbp), %rcx
	movq	-3376(%rbp), %rdx
	movq	%r15, %rdi
	movq	-3544(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L792:
	movq	-3408(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$18, %edi
	movq	$0, -3152(%rbp)
	movaps	%xmm0, -3168(%rbp)
	call	_Znwm@PLT
	movl	$1287, %r10d
	movdqa	.LC5(%rip), %xmm0
	movq	%r14, %rsi
	movw	%r10w, 16(%rax)
	movq	-3400(%rbp), %rdi
	leaq	18(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -3168(%rbp)
	movq	%rdx, -3152(%rbp)
	movq	%rdx, -3160(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3168(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L667
	call	_ZdlPv@PLT
.L667:
	movq	(%rbx), %rax
	movq	40(%rax), %rdx
	movq	24(%rax), %rsi
	movq	72(%rax), %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	%rdx, -3568(%rbp)
	movq	56(%rax), %rdx
	movq	%rsi, -3456(%rbp)
	movq	32(%rax), %rsi
	movq	%rdi, -3616(%rbp)
	movq	80(%rax), %rdi
	movq	%rdx, -3584(%rbp)
	movq	64(%rax), %rdx
	movq	128(%rax), %r10
	movq	%rcx, -3408(%rbp)
	movq	16(%rax), %rcx
	movq	48(%rax), %r12
	movq	%rsi, -3544(%rbp)
	leaq	.LC4(%rip), %rsi
	movq	%rdx, -3600(%rbp)
	movl	$56, %edx
	movq	%rdi, -3632(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -3448(%rbp)
	movq	%r10, -3648(%rbp)
	movq	%rbx, -3376(%rbp)
	movq	136(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-256(%rbp), %rsi
	leaq	-152(%rbp), %rdx
	movq	%r14, %rdi
	movq	-3376(%rbp), %xmm0
	movq	%rbx, -160(%rbp)
	movq	$0, -3152(%rbp)
	movhps	-3408(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	-3448(%rbp), %xmm0
	movhps	-3456(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-3544(%rbp), %xmm0
	movhps	-3568(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	%r12, %xmm0
	movhps	-3584(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-3600(%rbp), %xmm0
	movhps	-3616(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-3632(%rbp), %xmm0
	movhps	-3648(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3168(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2176(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -3376(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L668
	call	_ZdlPv@PLT
.L668:
	movq	-3440(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2304(%rbp)
	je	.L669
.L793:
	movq	-3424(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$361703067525449479, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$11, %edi
	movq	$0, -3152(%rbp)
	movaps	%xmm0, -3168(%rbp)
	call	_Znwm@PLT
	movl	$1542, %r9d
	movq	-3368(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	11(%rax), %rdx
	movw	%r9w, 8(%rax)
	movb	$5, 10(%rax)
	movq	%rax, -3168(%rbp)
	movq	%rdx, -3152(%rbp)
	movq	%rdx, -3160(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L670
	call	_ZdlPv@PLT
.L670:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L791:
	movq	-3448(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -3296(%rbp)
	movq	$0, -3288(%rbp)
	movq	$0, -3280(%rbp)
	movq	$0, -3272(%rbp)
	movq	$0, -3264(%rbp)
	movq	$0, -3256(%rbp)
	movq	$0, -3248(%rbp)
	movq	$0, -3240(%rbp)
	movq	$0, -3232(%rbp)
	movq	$0, -3224(%rbp)
	movq	$0, -3216(%rbp)
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	$0, -3192(%rbp)
	movq	$0, -3184(%rbp)
	movq	$0, -3176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3176(%rbp), %rax
	movq	-3392(%rbp), %rdi
	pushq	%rax
	leaq	-3184(%rbp), %rax
	leaq	-3280(%rbp), %rcx
	pushq	%rax
	leaq	-3192(%rbp), %rax
	leaq	-3264(%rbp), %r9
	pushq	%rax
	leaq	-3200(%rbp), %rax
	leaq	-3272(%rbp), %r8
	pushq	%rax
	leaq	-3208(%rbp), %rax
	leaq	-3288(%rbp), %rdx
	pushq	%rax
	leaq	-3216(%rbp), %rax
	leaq	-3296(%rbp), %rsi
	pushq	%rax
	leaq	-3224(%rbp), %rax
	pushq	%rax
	leaq	-3232(%rbp), %rax
	pushq	%rax
	leaq	-3240(%rbp), %rax
	pushq	%rax
	leaq	-3248(%rbp), %rax
	pushq	%rax
	leaq	-3256(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_3SmiES5_S5_S4_NS0_7IntPtrTES6_S5_S5_S6_NS0_10HeapObjectES6_S6_S6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_SD_PNS9_IS6_EESH_SF_SF_SH_PNS9_IS7_EESH_SH_SH_SH_
	addq	$96, %rsp
	movl	$41, %edx
	movq	%r15, %rdi
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3296(%rbp), %rax
	pxor	%xmm0, %xmm0
	leaq	-256(%rbp), %rsi
	leaq	-168(%rbp), %rdx
	movq	%r14, %rdi
	movaps	%xmm0, -3168(%rbp)
	movq	%rax, -256(%rbp)
	movq	-3288(%rbp), %rax
	movq	$0, -3152(%rbp)
	movq	%rax, -248(%rbp)
	movq	-3280(%rbp), %rax
	movq	%rax, -240(%rbp)
	movq	-3272(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	-3264(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-3256(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-3248(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-3240(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-3232(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-3224(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-3216(%rbp), %rax
	movq	%rax, -176(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3368(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L665
	call	_ZdlPv@PLT
.L665:
	movq	-3424(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L790:
	movq	-3376(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -3296(%rbp)
	movq	$0, -3288(%rbp)
	movq	$0, -3280(%rbp)
	movq	$0, -3272(%rbp)
	movq	$0, -3264(%rbp)
	movq	$0, -3256(%rbp)
	movq	$0, -3248(%rbp)
	movq	$0, -3240(%rbp)
	movq	$0, -3232(%rbp)
	movq	$0, -3224(%rbp)
	movq	$0, -3216(%rbp)
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	$0, -3192(%rbp)
	movq	$0, -3184(%rbp)
	movq	$0, -3176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3176(%rbp), %rax
	movq	-3384(%rbp), %rdi
	pushq	%rax
	leaq	-3184(%rbp), %rax
	leaq	-3264(%rbp), %r9
	pushq	%rax
	leaq	-3192(%rbp), %rax
	leaq	-3272(%rbp), %r8
	pushq	%rax
	leaq	-3200(%rbp), %rax
	leaq	-3280(%rbp), %rcx
	pushq	%rax
	leaq	-3208(%rbp), %rax
	leaq	-3288(%rbp), %rdx
	pushq	%rax
	leaq	-3216(%rbp), %rax
	leaq	-3296(%rbp), %rsi
	pushq	%rax
	leaq	-3224(%rbp), %rax
	pushq	%rax
	leaq	-3232(%rbp), %rax
	pushq	%rax
	leaq	-3240(%rbp), %rax
	pushq	%rax
	leaq	-3248(%rbp), %rax
	pushq	%rax
	leaq	-3256(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_3SmiES5_S5_S4_NS0_7IntPtrTES6_S5_S5_S6_NS0_10HeapObjectES6_S6_S6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_SD_PNS9_IS6_EESH_SF_SF_SH_PNS9_IS7_EESH_SH_SH_SH_
	addq	$96, %rsp
	movl	$39, %edx
	movq	%r15, %rdi
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3176(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrMulENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3200(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$38, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-3208(%rbp), %rdx
	call	_ZN2v88internal45UnsafeNewReference20UT5ATSmi10HeapObject_1411EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	leaq	-256(%rbp), %rsi
	leaq	-112(%rbp), %rdx
	movq	%r14, %rdi
	movq	-3184(%rbp), %xmm0
	movq	-3200(%rbp), %xmm1
	movq	$0, -3152(%rbp)
	movq	-3216(%rbp), %xmm2
	movq	-3232(%rbp), %xmm3
	movhps	-3176(%rbp), %xmm0
	movhps	-3192(%rbp), %xmm1
	movq	-3248(%rbp), %xmm4
	movq	-3264(%rbp), %xmm5
	movaps	%xmm0, -144(%rbp)
	movhps	-3208(%rbp), %xmm2
	movq	-3168(%rbp), %xmm0
	movq	-3280(%rbp), %xmm6
	movq	-3296(%rbp), %xmm7
	movhps	-3224(%rbp), %xmm3
	movhps	-3240(%rbp), %xmm4
	movhps	-3256(%rbp), %xmm5
	movhps	-3160(%rbp), %xmm0
	movhps	-3272(%rbp), %xmm6
	movaps	%xmm5, -224(%rbp)
	movhps	-3288(%rbp), %xmm7
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm7, -256(%rbp)
	movaps	%xmm6, -240(%rbp)
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -3168(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3400(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L663
	call	_ZdlPv@PLT
.L663:
	movq	-3408(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L794:
	movq	-3440(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$361703067525449479, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$13, %edi
	movq	$0, -3152(%rbp)
	movaps	%xmm0, -3168(%rbp)
	call	_Znwm@PLT
	movq	-3376(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	13(%rax), %rdx
	movl	$117769734, 8(%rax)
	movb	$5, 12(%rax)
	movq	%rax, -3168(%rbp)
	movq	%rdx, -3152(%rbp)
	movq	%rdx, -3160(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3168(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L672
	call	_ZdlPv@PLT
.L672:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	16(%rax), %rcx
	movq	(%rax), %rbx
	movq	56(%rax), %rdi
	movq	64(%rax), %rsi
	movq	88(%rax), %rdx
	movq	24(%rax), %r11
	movq	%rcx, -3408(%rbp)
	movq	32(%rax), %r10
	movq	40(%rax), %r9
	movq	48(%rax), %r8
	movq	72(%rax), %rcx
	movq	8(%rax), %r12
	movq	96(%rax), %rax
	movq	%rbx, -256(%rbp)
	movq	-3408(%rbp), %rbx
	movq	%rdi, -200(%rbp)
	movq	%r14, %rdi
	movq	%rsi, -192(%rbp)
	leaq	-256(%rbp), %rsi
	movq	%rdx, -176(%rbp)
	leaq	-160(%rbp), %rdx
	movq	%r11, -232(%rbp)
	movq	%r10, -224(%rbp)
	movq	%r9, -216(%rbp)
	movq	%r8, -208(%rbp)
	movq	%rcx, -184(%rbp)
	movq	%rax, -168(%rbp)
	movaps	%xmm0, -3168(%rbp)
	movq	%r12, -248(%rbp)
	movq	%rbx, -240(%rbp)
	movq	$0, -3152(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1984(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -3448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L673
	call	_ZdlPv@PLT
.L673:
	movq	-3512(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L795:
	movq	-3512(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$361703067525449479, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$12, %edi
	movq	$0, -3152(%rbp)
	movaps	%xmm0, -3168(%rbp)
	call	_Znwm@PLT
	movq	-3448(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	12(%rax), %rdx
	movl	$84346374, 8(%rax)
	movq	%rax, -3168(%rbp)
	movq	%rdx, -3152(%rbp)
	movq	%rdx, -3160(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3168(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L675
	call	_ZdlPv@PLT
.L675:
	movq	(%rbx), %rax
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	(%rax), %rbx
	movq	16(%rax), %rcx
	movq	72(%rax), %r10
	movq	24(%rax), %rsi
	movq	%rdx, -3600(%rbp)
	movq	%rdi, -3632(%rbp)
	movq	48(%rax), %rdx
	movq	64(%rax), %rdi
	movq	%rbx, -3456(%rbp)
	movq	%rcx, -3568(%rbp)
	movq	8(%rax), %rbx
	movq	32(%rax), %rcx
	movq	%r10, -3552(%rbp)
	movq	80(%rax), %r10
	movq	88(%rax), %rax
	movq	%rsi, -3584(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rdx, -3616(%rbp)
	movl	$50, %edx
	movq	%rdi, -3648(%rbp)
	movq	%r15, %rdi
	movq	%r10, -3664(%rbp)
	movq	%rcx, -3424(%rbp)
	movq	%rbx, -3408(%rbp)
	movq	%rax, -3672(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$8, %esi
	movq	%r15, %rdi
	movq	%rax, -3512(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$55, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -3440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3424(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$56, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$37, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	-3440(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -3544(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3544(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-256(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -3544(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-3568(%rbp), %xmm3
	movq	-3424(%rbp), %rdx
	movq	-3664(%rbp), %xmm6
	movq	-3408(%rbp), %rax
	movhps	-3584(%rbp), %xmm3
	movq	-3512(%rbp), %rcx
	movq	-3648(%rbp), %xmm7
	movq	%rdx, %xmm2
	movq	-3616(%rbp), %xmm1
	movaps	%xmm3, -3568(%rbp)
	movhps	-3672(%rbp), %xmm6
	movq	-3456(%rbp), %xmm4
	movaps	%xmm3, -240(%rbp)
	movhps	-3552(%rbp), %xmm7
	movq	-3440(%rbp), %xmm3
	movq	%rdx, %xmm5
	movhps	-3632(%rbp), %xmm1
	movaps	%xmm6, -3664(%rbp)
	movhps	-3600(%rbp), %xmm2
	movhps	-3408(%rbp), %xmm4
	movaps	%xmm7, -3648(%rbp)
	leaq	-72(%rbp), %rdx
	movaps	%xmm1, -3632(%rbp)
	movaps	%xmm1, -208(%rbp)
	movq	%rbx, %xmm1
	movaps	%xmm7, -192(%rbp)
	movdqa	%xmm3, %xmm7
	punpcklqdq	%xmm1, %xmm5
	movaps	%xmm6, -176(%rbp)
	movdqa	%xmm3, %xmm6
	punpcklqdq	%xmm1, %xmm7
	movhps	-3424(%rbp), %xmm6
	movq	%rax, -160(%rbp)
	movq	%rcx, -152(%rbp)
	movaps	%xmm2, -3600(%rbp)
	movaps	%xmm4, -3616(%rbp)
	movaps	%xmm4, -256(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm5, -3584(%rbp)
	movaps	%xmm6, -3440(%rbp)
	movaps	%xmm6, -144(%rbp)
	movq	%rdx, -3552(%rbp)
	movq	%rax, -112(%rbp)
	movq	%rcx, -104(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm7, -3424(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm0, -3168(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -3152(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1792(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -3456(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3168(%rbp), %rdi
	movq	-3552(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L676
	call	_ZdlPv@PLT
	movq	-3552(%rbp), %rdx
.L676:
	movq	-3408(%rbp), %xmm0
	movdqa	-3616(%rbp), %xmm7
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqa	-3568(%rbp), %xmm2
	movdqa	-3600(%rbp), %xmm1
	movq	%rbx, -80(%rbp)
	movhps	-3512(%rbp), %xmm0
	movdqa	-3632(%rbp), %xmm3
	movdqa	-3648(%rbp), %xmm4
	movaps	%xmm7, -256(%rbp)
	movdqa	-3664(%rbp), %xmm5
	movdqa	-3440(%rbp), %xmm6
	movaps	%xmm2, -240(%rbp)
	movdqa	-3584(%rbp), %xmm7
	movdqa	-3424(%rbp), %xmm2
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -3168(%rbp)
	movq	$0, -3152(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1600(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -3408(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L677
	call	_ZdlPv@PLT
.L677:
	movq	-3464(%rbp), %rcx
	movq	-3528(%rbp), %rdx
	movq	%r15, %rdi
	movq	-3544(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L797:
	movq	-3464(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -3352(%rbp)
	movq	$0, -3344(%rbp)
	movq	$0, -3336(%rbp)
	movq	$0, -3328(%rbp)
	movq	$0, -3320(%rbp)
	movq	$0, -3312(%rbp)
	movq	$0, -3304(%rbp)
	movq	$0, -3296(%rbp)
	movq	$0, -3288(%rbp)
	movq	$0, -3280(%rbp)
	movq	$0, -3272(%rbp)
	movq	$0, -3264(%rbp)
	movq	$0, -3256(%rbp)
	movq	$0, -3248(%rbp)
	movq	$0, -3240(%rbp)
	movq	$0, -3232(%rbp)
	movq	$0, -3224(%rbp)
	movq	$0, -3216(%rbp)
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	$0, -3192(%rbp)
	movq	$0, -3184(%rbp)
	movq	$0, -3176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3176(%rbp), %rax
	movq	-3408(%rbp), %rdi
	leaq	-3336(%rbp), %rcx
	pushq	%rax
	leaq	-3184(%rbp), %rax
	leaq	-3320(%rbp), %r9
	pushq	%rax
	leaq	-3192(%rbp), %rax
	leaq	-3328(%rbp), %r8
	pushq	%rax
	leaq	-3200(%rbp), %rax
	leaq	-3344(%rbp), %rdx
	pushq	%rax
	leaq	-3208(%rbp), %rax
	leaq	-3352(%rbp), %rsi
	pushq	%rax
	leaq	-3216(%rbp), %rax
	pushq	%rax
	leaq	-3224(%rbp), %rax
	pushq	%rax
	leaq	-3232(%rbp), %rax
	pushq	%rax
	leaq	-3240(%rbp), %rax
	pushq	%rax
	leaq	-3248(%rbp), %rax
	pushq	%rax
	leaq	-3256(%rbp), %rax
	pushq	%rax
	leaq	-3264(%rbp), %rax
	pushq	%rax
	leaq	-3272(%rbp), %rax
	pushq	%rax
	leaq	-3280(%rbp), %rax
	pushq	%rax
	leaq	-3288(%rbp), %rax
	pushq	%rax
	leaq	-3296(%rbp), %rax
	pushq	%rax
	leaq	-3304(%rbp), %rax
	pushq	%rax
	leaq	-3312(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_3SmiES5_S5_S4_NS0_7IntPtrTES6_S5_S5_NS0_10HeapObjectES6_S4_S6_S6_S5_S5_S6_S7_S6_S6_S6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_SD_PNS9_IS6_EESH_SF_SF_PNS9_IS7_EESH_SD_SH_SH_SF_SF_SH_SJ_SH_SH_SH_SH_
	addq	$144, %rsp
	movl	$41, %edx
	movq	%r15, %rdi
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-256(%rbp), %rsi
	leaq	-112(%rbp), %rdx
	movq	%r14, %rdi
	movq	-3224(%rbp), %xmm0
	movq	-3240(%rbp), %xmm1
	movq	$0, -3152(%rbp)
	movq	-3256(%rbp), %xmm2
	movq	-3272(%rbp), %xmm3
	movq	-3288(%rbp), %xmm4
	movhps	-3216(%rbp), %xmm0
	movq	-3304(%rbp), %xmm5
	movhps	-3232(%rbp), %xmm1
	movq	-3320(%rbp), %xmm6
	movhps	-3248(%rbp), %xmm2
	movq	-3336(%rbp), %xmm7
	movhps	-3264(%rbp), %xmm3
	movq	-3352(%rbp), %xmm8
	movhps	-3280(%rbp), %xmm4
	movaps	%xmm0, -128(%rbp)
	movhps	-3296(%rbp), %xmm5
	movhps	-3312(%rbp), %xmm6
	movhps	-3328(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -208(%rbp)
	movhps	-3344(%rbp), %xmm8
	movaps	%xmm7, -240(%rbp)
	movaps	%xmm8, -256(%rbp)
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -3168(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3440(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L681
	call	_ZdlPv@PLT
.L681:
	movq	-3504(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L796:
	movq	-3528(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -3352(%rbp)
	movq	$0, -3344(%rbp)
	movq	$0, -3336(%rbp)
	movq	$0, -3328(%rbp)
	movq	$0, -3320(%rbp)
	movq	$0, -3312(%rbp)
	movq	$0, -3304(%rbp)
	movq	$0, -3296(%rbp)
	movq	$0, -3288(%rbp)
	movq	$0, -3280(%rbp)
	movq	$0, -3272(%rbp)
	movq	$0, -3264(%rbp)
	movq	$0, -3256(%rbp)
	movq	$0, -3248(%rbp)
	movq	$0, -3240(%rbp)
	movq	$0, -3232(%rbp)
	movq	$0, -3224(%rbp)
	movq	$0, -3216(%rbp)
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	$0, -3192(%rbp)
	movq	$0, -3184(%rbp)
	movq	$0, -3176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3176(%rbp), %rax
	movq	-3456(%rbp), %rdi
	leaq	-3320(%rbp), %r9
	pushq	%rax
	leaq	-3184(%rbp), %rax
	leaq	-3328(%rbp), %r8
	pushq	%rax
	leaq	-3192(%rbp), %rax
	leaq	-3336(%rbp), %rcx
	pushq	%rax
	leaq	-3200(%rbp), %rax
	leaq	-3344(%rbp), %rdx
	pushq	%rax
	leaq	-3208(%rbp), %rax
	leaq	-3352(%rbp), %rsi
	pushq	%rax
	leaq	-3216(%rbp), %rax
	pushq	%rax
	leaq	-3224(%rbp), %rax
	pushq	%rax
	leaq	-3232(%rbp), %rax
	pushq	%rax
	leaq	-3240(%rbp), %rax
	pushq	%rax
	leaq	-3248(%rbp), %rax
	pushq	%rax
	leaq	-3256(%rbp), %rax
	pushq	%rax
	leaq	-3264(%rbp), %rax
	pushq	%rax
	leaq	-3272(%rbp), %rax
	pushq	%rax
	leaq	-3280(%rbp), %rax
	pushq	%rax
	leaq	-3288(%rbp), %rax
	pushq	%rax
	leaq	-3296(%rbp), %rax
	pushq	%rax
	leaq	-3304(%rbp), %rax
	pushq	%rax
	leaq	-3312(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_3SmiES5_S5_S4_NS0_7IntPtrTES6_S5_S5_NS0_10HeapObjectES6_S4_S6_S6_S5_S5_S6_S7_S6_S6_S6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_SD_PNS9_IS6_EESH_SF_SF_PNS9_IS7_EESH_SD_SH_SH_SF_SF_SH_SJ_SH_SH_SH_SH_
	addq	$144, %rsp
	movl	$39, %edx
	movq	%r15, %rdi
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3176(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrMulENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3200(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$38, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-3208(%rbp), %rdx
	call	_ZN2v88internal45UnsafeNewReference20UT5ATSmi10HeapObject_1411EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	pxor	%xmm0, %xmm0
	leaq	-56(%rbp), %rdx
	movq	%r14, %rdi
	movq	-3352(%rbp), %rax
	leaq	-256(%rbp), %rsi
	movq	%rax, -256(%rbp)
	movq	-3344(%rbp), %rax
	movq	%rax, -248(%rbp)
	movq	-3336(%rbp), %rax
	movq	%rax, -240(%rbp)
	movq	-3328(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	-3320(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-3312(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-3304(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-3296(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-3288(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-3280(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-3272(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-3264(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-3256(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-3248(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-3240(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-3232(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-3224(%rbp), %rax
	movq	$0, -3152(%rbp)
	movq	%rax, -128(%rbp)
	movq	-3216(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-3208(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-3200(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-3192(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-3184(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-3176(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-3168(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-3160(%rbp), %rax
	movaps	%xmm0, -3168(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3424(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L679
	call	_ZdlPv@PLT
.L679:
	movq	-3496(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L799:
	movq	-3504(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$18, %edi
	movq	$0, -3152(%rbp)
	movaps	%xmm0, -3168(%rbp)
	call	_Znwm@PLT
	movl	$1286, %r8d
	movdqa	.LC3(%rip), %xmm0
	movq	%r14, %rsi
	movw	%r8w, 16(%rax)
	movq	-3440(%rbp), %rdi
	leaq	18(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -3168(%rbp)
	movq	%rdx, -3152(%rbp)
	movq	%rdx, -3160(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L686
	call	_ZdlPv@PLT
.L686:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L798:
	movq	-3496(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-256(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC3(%rip), %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-231(%rbp), %rdx
	movabsq	$505816052266370310, %rax
	movb	$5, -232(%rbp)
	movaps	%xmm0, -256(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -240(%rbp)
	movaps	%xmm0, -3168(%rbp)
	movq	$0, -3152(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3424(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3168(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L683
	call	_ZdlPv@PLT
.L683:
	movq	(%rbx), %rax
	movq	56(%rax), %rdi
	movq	88(%rax), %r11
	movq	8(%rax), %rcx
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	96(%rax), %r10
	movq	%rdi, -3600(%rbp)
	movq	72(%rax), %rdi
	movq	(%rax), %rbx
	movq	%r11, -3648(%rbp)
	movq	104(%rax), %r11
	movq	120(%rax), %r9
	movq	%rcx, -3496(%rbp)
	movq	%rsi, -3528(%rbp)
	movq	16(%rax), %rcx
	movq	32(%rax), %rsi
	movq	%rdx, -3568(%rbp)
	movq	%rdi, -3616(%rbp)
	movq	48(%rax), %rdx
	movq	80(%rax), %rdi
	movq	%r11, -3664(%rbp)
	movq	112(%rax), %r11
	movq	%rcx, -3512(%rbp)
	movq	%r10, -3552(%rbp)
	movq	%r11, -3672(%rbp)
	movq	%rbx, -3464(%rbp)
	movq	64(%rax), %rbx
	movq	%rsi, -3544(%rbp)
	leaq	.LC4(%rip), %rsi
	movq	%rdx, -3584(%rbp)
	movl	$56, %edx
	movq	%rdi, -3632(%rbp)
	movq	%r15, %rdi
	movq	%r9, -3680(%rbp)
	movq	128(%rax), %r9
	movq	136(%rax), %r8
	movq	%r9, -3688(%rbp)
	movq	%r8, -3696(%rbp)
	movq	184(%rax), %r8
	movq	192(%rax), %rax
	movq	%r8, -3704(%rbp)
	movq	%rax, -3712(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-96(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-3464(%rbp), %xmm0
	movq	$0, -3152(%rbp)
	movhps	-3496(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	-3512(%rbp), %xmm0
	movhps	-3528(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-3544(%rbp), %xmm0
	movhps	-3568(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-3584(%rbp), %xmm0
	movhps	-3600(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	%rbx, %xmm0
	leaq	-1024(%rbp), %rbx
	movhps	-3616(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-3632(%rbp), %xmm0
	movhps	-3648(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-3552(%rbp), %xmm0
	movhps	-3664(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-3672(%rbp), %xmm0
	movhps	-3680(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-3688(%rbp), %xmm0
	movhps	-3696(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-3704(%rbp), %xmm0
	movhps	-3712(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3168(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L684
	call	_ZdlPv@PLT
.L684:
	movq	-3520(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L801:
	movq	-3488(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$19, %edi
	movq	$0, -3152(%rbp)
	movaps	%xmm0, -3168(%rbp)
	call	_Znwm@PLT
	movl	$1798, %edi
	movdqa	.LC3(%rip), %xmm0
	movq	%r14, %rsi
	movw	%di, 16(%rax)
	leaq	19(%rax), %rdx
	movq	%r12, %rdi
	movb	$5, 18(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, -3168(%rbp)
	movq	%rdx, -3152(%rbp)
	movq	%rdx, -3160(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L691
	movq	%rax, -3464(%rbp)
	call	_ZdlPv@PLT
	movq	-3464(%rbp), %rax
.L691:
	movq	(%rax), %rax
	movl	$50, %edx
	movq	%r15, %rdi
	movq	8(%rax), %rsi
	movq	80(%rax), %r10
	movq	88(%rax), %r9
	movq	(%rax), %rcx
	movq	%rsi, -3488(%rbp)
	movq	16(%rax), %rsi
	movq	136(%rax), %r8
	movq	144(%rax), %r11
	movq	%r10, -3528(%rbp)
	movq	%rsi, -3496(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%r9, -3520(%rbp)
	movq	%rcx, -3464(%rbp)
	movq	%r8, -3512(%rbp)
	movq	%r11, -3504(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-3504(%rbp), %r11
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-3512(%rbp), %r8
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6ObjectEvE5valueE(%rip), %esi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -3504(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3504(%rbp), %rdx
	movq	-3464(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal21UnsafeCast5ATSmi_1410EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rax, -3512(%rbp)
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -3504(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3504(%rbp), %r11
	movq	%r14, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-3512(%rbp), %r8
	movq	%r14, %rdi
	movq	%rax, -3504(%rbp)
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-3504(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -3504(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$49, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-3520(%rbp), %r9
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	xorl	%r9d, %r9d
	movl	$6, %esi
	movq	%r14, %rdi
	movq	-3528(%rbp), %r10
	movq	-3504(%rbp), %r8
	movq	%rax, %rcx
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler13StoreToObjectENS0_21MachineRepresentationENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEEPNS1_4NodeENS0_25StoreToObjectWriteBarrierE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$45, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$44, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3496(%rbp), %rsi
	movl	$24, %edi
	movq	-3464(%rbp), %xmm0
	movq	$0, -3152(%rbp)
	movhps	-3488(%rbp), %xmm0
	movq	%rsi, -240(%rbp)
	movaps	%xmm0, -256(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3168(%rbp)
	call	_Znwm@PLT
	movq	-240(%rbp), %rcx
	movdqa	-256(%rbp), %xmm1
	movq	%r14, %rsi
	leaq	24(%rax), %rdx
	movq	%rax, -3168(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm1, (%rax)
	leaq	-640(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3152(%rbp)
	movq	%rdx, -3160(%rbp)
	movq	%rax, -3464(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L692
	call	_ZdlPv@PLT
.L692:
	movq	-3536(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L800:
	movq	-3520(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$20, %edi
	movq	$0, -3152(%rbp)
	movaps	%xmm0, -3168(%rbp)
	call	_Znwm@PLT
	movdqa	.LC3(%rip), %xmm0
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movl	$84346118, 16(%rax)
	leaq	20(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -3168(%rbp)
	movq	%rdx, -3152(%rbp)
	movq	%rdx, -3160(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3168(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L688
	call	_ZdlPv@PLT
.L688:
	movq	(%r12), %rax
	leaq	-256(%rbp), %rsi
	leaq	-104(%rbp), %rdx
	movq	%r14, %rdi
	leaq	-832(%rbp), %r12
	movdqu	144(%rax), %xmm9
	movdqu	128(%rax), %xmm0
	movdqu	(%rax), %xmm8
	movdqu	16(%rax), %xmm7
	movdqu	32(%rax), %xmm6
	movdqu	48(%rax), %xmm5
	punpcklqdq	%xmm9, %xmm0
	movdqu	64(%rax), %xmm4
	movdqu	80(%rax), %xmm3
	movdqu	96(%rax), %xmm2
	movdqu	112(%rax), %xmm1
	movq	152(%rax), %rax
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm8, -256(%rbp)
	movq	%rax, -112(%rbp)
	movaps	%xmm7, -240(%rbp)
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -3168(%rbp)
	movq	$0, -3152(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L689
	call	_ZdlPv@PLT
.L689:
	movq	-3488(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L802:
	movq	-3536(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -3152(%rbp)
	movaps	%xmm0, -3168(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movq	-3464(%rbp), %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r14, %rsi
	movb	$6, 2(%rax)
	movq	%rax, -3168(%rbp)
	movq	%rdx, -3152(%rbp)
	movq	%rdx, -3160(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3168(%rbp), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L694
	call	_ZdlPv@PLT
.L694:
	movq	0(%r13), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	leaq	-448(%rbp), %r13
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm3
	movaps	%xmm0, -3168(%rbp)
	movq	$0, -3152(%rbp)
	movq	%rdx, -240(%rbp)
	movaps	%xmm3, -256(%rbp)
	call	_Znwm@PLT
	movq	-240(%rbp), %rcx
	movq	%r13, %rdi
	movq	%r14, %rsi
	movdqa	-256(%rbp), %xmm4
	leaq	24(%rax), %rdx
	movq	%rax, -3168(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -3152(%rbp)
	movq	%rdx, -3160(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L695
	call	_ZdlPv@PLT
.L695:
	movq	-3480(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L693
.L803:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22449:
	.size	_ZN2v88internal23IncrementBlockCount_309EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_3SmiEEE, .-_ZN2v88internal23IncrementBlockCount_309EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_3SmiEEE
	.section	.text._ZN2v88internal24IncBlockCounterAssembler27GenerateIncBlockCounterImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24IncBlockCounterAssembler27GenerateIncBlockCounterImplEv
	.type	_ZN2v88internal24IncBlockCounterAssembler27GenerateIncBlockCounterImplEv, @function
_ZN2v88internal24IncBlockCounterAssembler27GenerateIncBlockCounterImplEv:
.LFB22499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1000(%rbp), %r15
	leaq	-1056(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1184(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1272, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, -1304(%rbp)
	movq	%rax, -1224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, -1280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	leaq	-1224(%rbp), %r12
	movq	%rax, -1296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$72, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, %rbx
	movq	-1224(%rbp), %rax
	movq	$0, -1032(%rbp)
	movq	%rax, -1056(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1048(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1224(%rbp), %rax
	movl	$96, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1264(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1224(%rbp), %rax
	movl	$120, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1224(%rbp), %rax
	movl	$72, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -1240(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1224(%rbp), %rax
	movl	$96, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -280(%rbp)
	leaq	-232(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -1256(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	-1280(%rbp), %xmm1
	movaps	%xmm0, -1184(%rbp)
	movhps	-1296(%rbp), %xmm1
	movq	%rbx, -80(%rbp)
	movaps	%xmm1, -96(%rbp)
	movq	$0, -1168(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm2
	leaq	24(%rax), %rdx
	movq	%rax, -1184(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L805
	call	_ZdlPv@PLT
.L805:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -992(%rbp)
	jne	.L927
	cmpq	$0, -800(%rbp)
	jne	.L928
.L811:
	cmpq	$0, -608(%rbp)
	jne	.L929
.L814:
	cmpq	$0, -416(%rbp)
	jne	.L930
.L817:
	cmpq	$0, -224(%rbp)
	jne	.L931
.L819:
	movq	-1256(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L824
	call	_ZdlPv@PLT
.L824:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L825
	.p2align 4,,10
	.p2align 3
.L829:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L826
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L829
.L827:
	movq	-280(%rbp), %r13
.L825:
	testq	%r13, %r13
	je	.L830
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L830:
	movq	-1240(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L831
	call	_ZdlPv@PLT
.L831:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L832
	.p2align 4,,10
	.p2align 3
.L836:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L833
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L836
.L834:
	movq	-472(%rbp), %r13
.L832:
	testq	%r13, %r13
	je	.L837
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L837:
	movq	-1248(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L838
	call	_ZdlPv@PLT
.L838:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L839
	.p2align 4,,10
	.p2align 3
.L843:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L840
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L843
.L841:
	movq	-664(%rbp), %r13
.L839:
	testq	%r13, %r13
	je	.L844
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L844:
	movq	-1264(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L845
	call	_ZdlPv@PLT
.L845:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L846
	.p2align 4,,10
	.p2align 3
.L850:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L847
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L850
.L848:
	movq	-856(%rbp), %r13
.L846:
	testq	%r13, %r13
	je	.L851
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L851:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L852
	call	_ZdlPv@PLT
.L852:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L853
	.p2align 4,,10
	.p2align 3
.L857:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L854
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L857
.L855:
	movq	-1048(%rbp), %r13
.L853:
	testq	%r13, %r13
	je	.L858
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L858:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L932
	addq	$1272, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L854:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L857
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L847:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L850
	jmp	.L848
	.p2align 4,,10
	.p2align 3
.L840:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L843
	jmp	.L841
	.p2align 4,,10
	.p2align 3
.L826:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L829
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L833:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L836
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L927:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movq	%r14, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$6, 2(%rax)
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L807
	call	_ZdlPv@PLT
.L807:
	movq	(%rbx), %rax
	movl	$61, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r14
	movq	8(%rax), %rbx
	movq	16(%rax), %rax
	movq	%rax, -1280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	-1304(%rbp), %rdi
	call	_ZN2v88internal19GetCoverageInfo_306EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEEPNS1_18CodeAssemblerLabelE
	movq	%rbx, %xmm5
	movq	%r14, %xmm4
	movq	-1280(%rbp), %xmm3
	punpcklqdq	%xmm5, %xmm4
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	%rax, -64(%rbp)
	punpcklqdq	%xmm5, %xmm3
	movaps	%xmm4, -1280(%rbp)
	leaq	-1216(%rbp), %r14
	movaps	%xmm3, -1296(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm0, -1216(%rbp)
	movq	$0, -1200(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm7
	movdqa	-80(%rbp), %xmm5
	leaq	40(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -1216(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1200(%rbp)
	movq	%rdx, -1208(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L808
	call	_ZdlPv@PLT
.L808:
	movq	-1248(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1176(%rbp)
	jne	.L933
.L809:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -800(%rbp)
	je	.L811
.L928:
	movq	-1264(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117835527, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L812
	call	_ZdlPv@PLT
.L812:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm6
	movaps	%xmm0, -1184(%rbp)
	movq	$0, -1168(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm6, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -1184(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L813
	call	_ZdlPv@PLT
.L813:
	movq	-1240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -608(%rbp)
	je	.L814
.L929:
	movq	-1248(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117835527, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L815
	call	_ZdlPv@PLT
.L815:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	8(%rax), %rsi
	movq	16(%rax), %rcx
	movq	32(%rax), %rdx
	movq	(%rax), %rax
	movaps	%xmm0, -1184(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -1168(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-288(%rbp), %rdi
	movq	%rax, -1184(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L816
	call	_ZdlPv@PLT
.L816:
	movq	-1256(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -416(%rbp)
	je	.L817
.L930:
	movq	-1240(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$6, 2(%rax)
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L818
	call	_ZdlPv@PLT
.L818:
	movq	-1304(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -224(%rbp)
	je	.L819
.L931:
	movq	-1256(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	leaq	-288(%rbp), %r8
	movq	$0, -1280(%rbp)
	movq	%r8, -1296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movq	-1296(%rbp), %r8
	movq	%r13, %rsi
	movl	$117835527, (%rax)
	leaq	4(%rax), %rdx
	movq	%r8, %rdi
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L820
	movq	%rax, -1296(%rbp)
	call	_ZdlPv@PLT
	movq	-1296(%rbp), %rax
.L820:
	movq	(%rax), %rax
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	cmovne	%rdx, %r14
	movq	16(%rax), %rdx
	movq	24(%rax), %rax
	testq	%rdx, %rdx
	cmovne	%rdx, %rbx
	testq	%rax, %rax
	cmove	-1280(%rbp), %rax
	movl	$60, %edx
	movq	%rax, -1280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rdi
	movl	$62, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rcx
	movq	-1304(%rbp), %rbx
	movq	%r14, %rsi
	movq	-1280(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal23IncrementBlockCount_309EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_3SmiEEE
	movl	$63, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L933:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-1280(%rbp), %xmm7
	movaps	%xmm0, -1216(%rbp)
	movaps	%xmm7, -96(%rbp)
	movdqa	-1296(%rbp), %xmm7
	movq	$0, -1200(%rbp)
	movaps	%xmm7, -80(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm7
	leaq	-864(%rbp), %rdi
	movq	%r14, %rsi
	leaq	32(%rax), %rdx
	movq	%rax, -1216(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-80(%rbp), %xmm7
	movq	%rdx, -1200(%rbp)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1208(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L810
	call	_ZdlPv@PLT
.L810:
	movq	-1264(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L809
.L932:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22499:
	.size	_ZN2v88internal24IncBlockCounterAssembler27GenerateIncBlockCounterImplEv, .-_ZN2v88internal24IncBlockCounterAssembler27GenerateIncBlockCounterImplEv
	.section	.rodata._ZN2v88internal8Builtins24Generate_IncBlockCounterEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/internal-coverage-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins24Generate_IncBlockCounterEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"IncBlockCounter"
	.section	.text._ZN2v88internal8Builtins24Generate_IncBlockCounterEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins24Generate_IncBlockCounterEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins24Generate_IncBlockCounterEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins24Generate_IncBlockCounterEPNS0_8compiler18CodeAssemblerStateE:
.LFB22495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$775, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$818, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L938
.L935:
	movq	%r13, %rdi
	call	_ZN2v88internal24IncBlockCounterAssembler27GenerateIncBlockCounterImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L939
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L938:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L935
.L939:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22495:
	.size	_ZN2v88internal8Builtins24Generate_IncBlockCounterEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins24Generate_IncBlockCounterEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal19GetCoverageInfo_306EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal19GetCoverageInfo_306EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEEPNS1_18CodeAssemblerLabelE, @function
_GLOBAL__sub_I__ZN2v88internal19GetCoverageInfo_306EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEEPNS1_18CodeAssemblerLabelE:
.LFB29372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29372:
	.size	_GLOBAL__sub_I__ZN2v88internal19GetCoverageInfo_306EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEEPNS1_18CodeAssemblerLabelE, .-_GLOBAL__sub_I__ZN2v88internal19GetCoverageInfo_306EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEEPNS1_18CodeAssemblerLabelE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal19GetCoverageInfo_306EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEEPNS1_18CodeAssemblerLabelE
	.weak	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_18SharedFunctionInfoEvE5valueE:
	.byte	7
	.byte	7
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.byte	7
	.byte	7
	.byte	6
	.byte	6
	.byte	6
	.byte	7
	.byte	5
	.byte	5
	.byte	6
	.byte	6
	.byte	7
	.byte	5
	.byte	7
	.byte	5
	.byte	5
	.byte	6
	.align 16
.LC5:
	.byte	7
	.byte	7
	.byte	6
	.byte	6
	.byte	6
	.byte	7
	.byte	5
	.byte	5
	.byte	6
	.byte	6
	.byte	5
	.byte	7
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
