	.file	"typed-array-foreach-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0:
.LFB29455:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	%rsi, %r13
	pushq	%rbx
	movq	%r13, %rax
	sarq	$3, %rax
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%r13, %r13
	js	.L14
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%rdx, %r14
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.L9
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%rax, %rdi
.L9:
	leaq	(%rdi,%r13), %r15
	movq	%rdi, (%rbx)
	movq	%r15, 16(%rbx)
	cmpq	%r12, %r14
	je	.L10
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
.L10:
	movq	%r15, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29455:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0:
.LFB29453:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	movabsq	$384307168202282325, %rax
	cmpq	%rax, %rdx
	ja	.L32
	pxor	%xmm0, %xmm0
	leaq	(%rdx,%rdx,2), %r14
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	%rdx, %r12
	movups	%xmm0, 8(%rdi)
	salq	$3, %r14
	xorl	%esi, %esi
	movq	$0, 24(%rdi)
	testq	%rdx, %rdx
	je	.L21
	movq	%r14, %rdi
	call	_Znwm@PLT
	leaq	-1(%r12), %rdx
	leaq	(%rax,%r14), %rsi
	movq	%rax, 8(%rbx)
	movq	%rsi, 24(%rbx)
	cmpq	$2, %rdx
	jbe	.L22
	movq	%r12, %rdi
	movq	%r12, %rcx
	pxor	%xmm0, %xmm0
	movq	%rax, %rdx
	shrq	%rdi
	andq	$-2, %rcx
	addq	%rdi, %rcx
	salq	$4, %rcx
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L19:
	movups	%xmm0, (%rdx)
	addq	$48, %rdx
	movups	%xmm0, -32(%rdx)
	movups	%xmm0, -16(%rdx)
	cmpq	%rcx, %rdx
	jne	.L19
	movq	%r12, %rdx
	movq	%r12, %rcx
	andq	$-2, %rdx
	andl	$1, %ecx
	leaq	(%rdx,%rdx,2), %rdi
	leaq	(%rax,%rdi,8), %rax
	cmpq	%rdx, %r12
	je	.L21
.L18:
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	cmpq	$1, %rcx
	je	.L21
	movq	$0, 40(%rax)
	movups	%xmm0, 24(%rax)
	cmpq	$2, %rcx
	je	.L21
	movq	$0, 64(%rax)
	movups	%xmm0, 48(%rax)
.L21:
	movq	%rsi, 16(%rbx)
	pxor	%xmm0, %xmm0
	leaq	56(%rbx), %rdi
	movq	%r13, %rsi
	movups	%xmm0, 32(%rbx)
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	$0, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movq	%r12, %rcx
	jmp	.L18
.L32:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29453:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev,"axG",@progbits,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev:
.LFB22413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$56, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L34
	call	_ZdlPv@PLT
.L34:
	movq	16(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	%r12, %r13
	je	.L35
	.p2align 4,,10
	.p2align 3
.L39:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L36
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L39
.L37:
	movq	8(%rbx), %r12
.L35:
	testq	%r12, %r12
	je	.L33
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L39
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L33:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22413:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev
	.set	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.section	.rodata._ZN2v88internal16kBuiltinName_358EPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"%TypedArray%.prototype.forEach"
	.section	.text._ZN2v88internal16kBuiltinName_358EPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal16kBuiltinName_358EPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal16kBuiltinName_358EPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal16kBuiltinName_358EPNS0_8compiler18CodeAssemblerStateE:
.LFB22410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-272(%rbp), %r15
	leaq	-240(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-280(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-184(%rbp), %r12
	movq	%r13, %rsi
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -280(%rbp)
	movq	%rdi, -240(%rbp)
	movq	%r12, %rdi
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -192(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L46
	call	_ZdlPv@PLT
.L46:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L47
	call	_ZdlPv@PLT
.L47:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L48
	call	_ZdlPv@PLT
.L48:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L49
	.p2align 4,,10
	.p2align 3
.L53:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L50
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L53
.L51:
	movq	-232(%rbp), %r12
.L49:
	testq	%r12, %r12
	je	.L54
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L54:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L69
	addq	$248, %rsp
	leaq	.LC2(%rip), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L53
	jmp	.L51
.L69:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22410:
	.size	_ZN2v88internal16kBuiltinName_358EPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal16kBuiltinName_358EPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SJ_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SJ_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SJ_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SJ_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SJ_:
.LFB26740:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$9, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$434042140868675335, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$6, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L71
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
.L71:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L72
	movq	%rdx, (%r15)
.L72:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L73
	movq	%rdx, (%r14)
.L73:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L74
	movq	%rdx, 0(%r13)
.L74:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L75
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L75:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L76
	movq	%rdx, (%rbx)
.L76:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L77
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L77:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L78
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L78:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L79
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L79:
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L70
	movq	-120(%rbp), %rbx
	movq	%rax, (%rbx)
.L70:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L113
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L113:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26740:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SJ_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SJ_
	.section	.rodata._ZN2v88internal22ForEachAllElements_359EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"../../deps/v8/../../deps/v8/src/builtins/typed-array-foreach.tq"
	.align 8
.LC4:
	.string	"../../deps/v8/../../deps/v8/src/builtins/typed-array.tq"
	.section	.text._ZN2v88internal22ForEachAllElements_359EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22ForEachAllElements_359EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal22ForEachAllElements_359EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal22ForEachAllElements_359EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEE:
.LFB22417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$312, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -4272(%rbp)
	leaq	-4096(%rbp), %r14
	leaq	-3840(%rbp), %rbx
	movq	%rdi, %r13
	movq	%r8, -4288(%rbp)
	leaq	-3968(%rbp), %r15
	movq	%rsi, -4176(%rbp)
	movq	%r14, %rsi
	movq	%rdx, -4240(%rbp)
	movl	$4, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -4096(%rbp)
	movq	%rbx, %rdi
	movq	%rbx, -4256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3648(%rbp), %rax
	movl	$8, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3456(%rbp), %rax
	movl	$8, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4248(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3264(%rbp), %rax
	movl	$9, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3072(%rbp), %rax
	movl	$7, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-4096(%rbp), %rax
	movl	$192, %edi
	movq	$0, -2872(%rbp)
	movq	$0, -2864(%rbp)
	movq	%rax, -2880(%rbp)
	movq	$0, -2856(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movups	%xmm0, (%rax)
	leaq	192(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -2872(%rbp)
	leaq	-2824(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2856(%rbp)
	movq	%rdx, -2864(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2840(%rbp)
	movq	%rax, -4304(%rbp)
	movq	$0, -2848(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-2688(%rbp), %rax
	movl	$9, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2496(%rbp), %rax
	movl	$9, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4152(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-4096(%rbp), %rax
	movl	$216, %edi
	movq	$0, -2296(%rbp)
	movq	$0, -2288(%rbp)
	movq	%rax, -2304(%rbp)
	movq	$0, -2280(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movups	%xmm0, (%rax)
	leaq	216(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -2296(%rbp)
	leaq	-2248(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2280(%rbp)
	movq	%rdx, -2288(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2264(%rbp)
	movq	%rax, -4312(%rbp)
	movq	$0, -2272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-2112(%rbp), %rax
	movl	$9, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4184(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1920(%rbp), %rax
	movl	$9, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4200(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1728(%rbp), %rax
	movl	$9, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4216(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1536(%rbp), %rax
	movl	$9, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4160(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1344(%rbp), %rax
	movl	$13, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1152(%rbp), %rax
	movl	$16, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4208(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-960(%rbp), %rax
	movl	$9, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4224(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-768(%rbp), %rax
	movl	$9, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4120(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-576(%rbp), %rax
	movl	$5, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4232(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-384(%rbp), %rax
	movl	$5, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4104(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-4272(%rbp), %xmm1
	movq	-4176(%rbp), %xmm2
	movaps	%xmm0, -3968(%rbp)
	movhps	-4288(%rbp), %xmm1
	movq	$0, -3952(%rbp)
	movhps	-4240(%rbp), %xmm2
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm2, -192(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm4
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movdqa	-176(%rbp), %xmm5
	leaq	32(%rax), %rdx
	movq	%rax, -3968(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L115
	call	_ZdlPv@PLT
.L115:
	leaq	-3784(%rbp), %r12
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3776(%rbp)
	jne	.L267
	cmpq	$0, -3584(%rbp)
	jne	.L268
.L119:
	cmpq	$0, -3392(%rbp)
	jne	.L269
.L124:
	leaq	-2880(%rbp), %rax
	cmpq	$0, -3200(%rbp)
	movq	%rax, -4176(%rbp)
	jne	.L270
	cmpq	$0, -3008(%rbp)
	jne	.L271
.L130:
	cmpq	$0, -2816(%rbp)
	jne	.L272
.L132:
	cmpq	$0, -2624(%rbp)
	jne	.L273
.L135:
	leaq	-2304(%rbp), %rax
	cmpq	$0, -2432(%rbp)
	movq	%rax, -4240(%rbp)
	jne	.L274
	cmpq	$0, -2240(%rbp)
	jne	.L275
.L141:
	cmpq	$0, -2048(%rbp)
	jne	.L276
.L143:
	cmpq	$0, -1856(%rbp)
	jne	.L277
.L145:
	cmpq	$0, -1664(%rbp)
	jne	.L278
.L147:
	cmpq	$0, -1472(%rbp)
	jne	.L279
.L149:
	cmpq	$0, -1280(%rbp)
	jne	.L280
.L151:
	cmpq	$0, -1088(%rbp)
	jne	.L281
.L154:
	cmpq	$0, -896(%rbp)
	jne	.L282
.L160:
	cmpq	$0, -704(%rbp)
	jne	.L283
.L162:
	cmpq	$0, -512(%rbp)
	leaq	-328(%rbp), %r12
	jne	.L284
.L164:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -3952(%rbp)
	movaps	%xmm0, -3968(%rbp)
	call	_Znwm@PLT
	movq	-4104(%rbp), %rdi
	movq	%r15, %rsi
	movl	$134678279, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -3968(%rbp)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3968(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L167
	call	_ZdlPv@PLT
.L167:
	movq	(%rbx), %rax
	movq	-4104(%rbp), %rdi
	movq	32(%rax), %r12
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4232(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4120(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4224(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4208(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4192(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4160(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4216(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4200(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4184(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4240(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4152(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4112(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4176(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4144(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4136(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4248(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4128(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4256(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L285
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -3952(%rbp)
	movaps	%xmm0, -3968(%rbp)
	call	_Znwm@PLT
	movq	-4256(%rbp), %rdi
	movq	%r15, %rsi
	movl	$134678279, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -3968(%rbp)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3968(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L117
	call	_ZdlPv@PLT
.L117:
	movq	(%rbx), %rax
	movl	$13, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	movq	(%rax), %rbx
	movq	16(%rax), %rcx
	movq	%rbx, -4272(%rbp)
	movq	8(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rcx, -4288(%rbp)
	movq	%rax, -4336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal34NewAttachedJSTypedArrayWitness_370EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_12JSTypedArrayEEE@PLT
	movq	-3960(%rbp), %rdx
	movq	-3952(%rbp), %rax
	leaq	.LC4(%rip), %rsi
	movq	%r14, %rdi
	movq	-3968(%rbp), %r12
	movq	%rdx, -4176(%rbp)
	movl	$91, %edx
	movq	%rax, -4240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm6
	movl	$64, %edi
	movq	-4272(%rbp), %xmm0
	movq	$0, -3952(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-4288(%rbp), %xmm0
	movhps	-4336(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	%r12, %xmm0
	movhps	-4176(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-4240(%rbp), %xmm0
	movhps	-4176(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3968(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm7
	movdqa	-176(%rbp), %xmm6
	movq	%r15, %rsi
	movq	-4128(%rbp), %rdi
	leaq	64(%rax), %rdx
	movq	%rax, -3968(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-160(%rbp), %xmm7
	movups	%xmm6, 16(%rax)
	movdqa	-144(%rbp), %xmm6
	movups	%xmm7, 32(%rax)
	movups	%xmm6, 48(%rax)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L118
	call	_ZdlPv@PLT
.L118:
	leaq	-3592(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3584(%rbp)
	je	.L119
.L268:
	leaq	-3592(%rbp), %rsi
	movq	%r14, %rdi
	movabsq	$506099734906603271, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3952(%rbp)
	movaps	%xmm0, -3968(%rbp)
	call	_Znwm@PLT
	movq	-4128(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3968(%rbp)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3968(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L120
	call	_ZdlPv@PLT
.L120:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	24(%rax), %rdx
	movq	40(%rax), %rsi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	%rdx, -4288(%rbp)
	movq	32(%rax), %rdx
	movq	%rsi, -4320(%rbp)
	movq	48(%rax), %rsi
	movq	%rcx, -4240(%rbp)
	movq	16(%rax), %rcx
	movq	%rdx, -4336(%rbp)
	movl	$16, %edx
	movq	56(%rax), %r12
	movq	%rsi, -4352(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%rcx, -4272(%rbp)
	movq	%rbx, -4176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$48, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE(%rip), %esi
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%rax, %rcx
	leaq	-4000(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATuintptr_201EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8UintPtrTEEE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rbx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13Cast5ATSmi_83EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm7
	pxor	%xmm0, %xmm0
	movq	-4352(%rbp), %xmm6
	movq	-4272(%rbp), %xmm4
	movl	$72, %edi
	movq	%rax, -128(%rbp)
	punpcklqdq	%xmm7, %xmm6
	movq	-4176(%rbp), %xmm5
	movq	-4336(%rbp), %xmm7
	movaps	%xmm0, -4000(%rbp)
	movhps	-4288(%rbp), %xmm4
	movaps	%xmm6, -4352(%rbp)
	movhps	-4320(%rbp), %xmm7
	movhps	-4240(%rbp), %xmm5
	movaps	%xmm4, -4272(%rbp)
	movaps	%xmm7, -4336(%rbp)
	movaps	%xmm5, -4176(%rbp)
	movaps	%xmm5, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movq	$0, -3984(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r12, %rsi
	movdqa	-192(%rbp), %xmm6
	movdqa	-176(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm4
	leaq	72(%rax), %rdx
	movq	%rax, -4000(%rbp)
	movdqa	-144(%rbp), %xmm5
	movq	-4136(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rdx, -3984(%rbp)
	movq	%rdx, -3992(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4000(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L121
	call	_ZdlPv@PLT
.L121:
	leaq	-3208(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3960(%rbp)
	jne	.L286
.L122:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -3392(%rbp)
	je	.L124
.L269:
	leaq	-3400(%rbp), %rsi
	movq	%r14, %rdi
	movabsq	$578157328944531207, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3952(%rbp)
	movaps	%xmm0, -3968(%rbp)
	call	_Znwm@PLT
	movq	-4248(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3968(%rbp)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3968(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L125
	call	_ZdlPv@PLT
.L125:
	movq	(%rbx), %rax
	movl	$56, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movq	48(%rax), %rdx
	movdqu	(%rax), %xmm6
	movq	$0, -3952(%rbp)
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -144(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -3968(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-192(%rbp), %xmm7
	movdqa	-176(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm5
	leaq	56(%rax), %rdx
	movq	%rax, -3968(%rbp)
	movq	%rcx, 48(%rax)
	movq	-4144(%rbp), %rdi
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L126
	call	_ZdlPv@PLT
.L126:
	leaq	-3016(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L270:
	leaq	-3208(%rbp), %rsi
	movq	%r14, %rdi
	movabsq	$578157328944531207, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -3952(%rbp)
	movaps	%xmm0, -3968(%rbp)
	call	_Znwm@PLT
	movq	-4136(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, (%rax)
	leaq	9(%rax), %rdx
	movb	$6, 8(%rax)
	movq	%rax, -3968(%rbp)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3968(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L128
	call	_ZdlPv@PLT
.L128:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	32(%rax), %rdi
	movq	8(%rax), %r10
	movq	16(%rax), %r9
	movq	24(%rax), %r8
	movq	40(%rax), %rsi
	movq	48(%rax), %rcx
	movq	64(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdi, -160(%rbp)
	movl	$64, %edi
	movq	%r10, -184(%rbp)
	movq	%r9, -176(%rbp)
	movq	%r8, -168(%rbp)
	movq	%rsi, -152(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movaps	%xmm0, -3968(%rbp)
	movq	%rax, -192(%rbp)
	movq	$0, -3952(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-176(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm5
	leaq	64(%rax), %rdx
	movq	%rax, -3968(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	leaq	-2880(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	movq	%rax, -4176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L129
	call	_ZdlPv@PLT
.L129:
	movq	-4304(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3008(%rbp)
	je	.L130
.L271:
	leaq	-3016(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -3952(%rbp)
	movaps	%xmm0, -3968(%rbp)
	call	_Znwm@PLT
	movl	$1799, %edi
	movq	%r15, %rsi
	movw	%di, 4(%rax)
	movq	-4144(%rbp), %rdi
	leaq	7(%rax), %rdx
	movl	$134678279, (%rax)
	movb	$6, 6(%rax)
	movq	%rax, -3968(%rbp)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L131
	call	_ZdlPv@PLT
.L131:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -2816(%rbp)
	je	.L132
.L272:
	movq	-4304(%rbp), %rsi
	movq	%r14, %rdi
	movabsq	$434042140868675335, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3952(%rbp)
	movaps	%xmm0, -3968(%rbp)
	call	_Znwm@PLT
	movq	-4176(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3968(%rbp)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3968(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L133
	call	_ZdlPv@PLT
.L133:
	movq	(%rbx), %rax
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	movq	24(%rax), %rdx
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	32(%rax), %r12
	movq	%rdx, -4304(%rbp)
	movq	40(%rax), %rdx
	movq	%rbx, -4240(%rbp)
	movq	48(%rax), %rbx
	movq	%rcx, -4272(%rbp)
	movq	16(%rax), %rcx
	movq	56(%rax), %rax
	movq	%rdx, -4336(%rbp)
	movl	$17, %edx
	movq	%rcx, -4288(%rbp)
	movq	%rax, -4320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$72, %edi
	movq	-4240(%rbp), %xmm0
	movq	$0, -3952(%rbp)
	movq	%rax, -128(%rbp)
	movhps	-4272(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-4288(%rbp), %xmm0
	movhps	-4304(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	%r12, %xmm0
	movhps	-4336(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%rbx, %xmm0
	movhps	-4320(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3968(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-192(%rbp), %xmm3
	movdqa	-176(%rbp), %xmm6
	movdqa	-160(%rbp), %xmm7
	leaq	72(%rax), %rdx
	movq	%rax, -3968(%rbp)
	movdqa	-144(%rbp), %xmm4
	movq	-4112(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm4, 48(%rax)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L134
	call	_ZdlPv@PLT
.L134:
	leaq	-2632(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2624(%rbp)
	je	.L135
.L273:
	leaq	-2632(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4072(%rbp)
	movq	$0, -4064(%rbp)
	movq	$0, -4056(%rbp)
	movq	$0, -4048(%rbp)
	movq	$0, -4040(%rbp)
	movq	$0, -4032(%rbp)
	movq	$0, -4024(%rbp)
	movq	$0, -4016(%rbp)
	movq	$0, -4000(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4000(%rbp), %rax
	movq	-4112(%rbp), %rdi
	leaq	-4048(%rbp), %r8
	pushq	%rax
	leaq	-4016(%rbp), %rax
	leaq	-4056(%rbp), %rcx
	pushq	%rax
	leaq	-4024(%rbp), %rax
	leaq	-4040(%rbp), %r9
	pushq	%rax
	leaq	-4032(%rbp), %rax
	leaq	-4064(%rbp), %rdx
	pushq	%rax
	leaq	-4072(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SJ_
	addq	$32, %rsp
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4000(%rbp), %rbx
	movq	-4016(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-4040(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	-4064(%rbp), %rcx
	movq	-4056(%rbp), %rdx
	movq	-4048(%rbp), %rsi
	movaps	%xmm0, -3968(%rbp)
	movq	-4032(%rbp), %r11
	movq	-4024(%rbp), %r10
	movq	%rdi, -4352(%rbp)
	movq	-4016(%rbp), %r9
	movq	-4072(%rbp), %rax
	movq	%rdi, -160(%rbp)
	movl	$72, %edi
	movq	-4000(%rbp), %rbx
	movq	%rcx, -4272(%rbp)
	movq	%rdx, -4288(%rbp)
	movq	%rsi, -4304(%rbp)
	movq	%r11, -4360(%rbp)
	movq	%r10, -4336(%rbp)
	movq	%r9, -4320(%rbp)
	movq	%rcx, -184(%rbp)
	movq	%rdx, -176(%rbp)
	movq	%rsi, -168(%rbp)
	movq	%r11, -152(%rbp)
	movq	%r10, -144(%rbp)
	movq	%r9, -136(%rbp)
	movq	%rax, -4240(%rbp)
	movq	%rax, -192(%rbp)
	movq	%rbx, -128(%rbp)
	movq	$0, -3952(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-192(%rbp), %xmm6
	movdqa	-176(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm4
	leaq	72(%rax), %rdx
	movq	%rax, -3968(%rbp)
	movdqa	-144(%rbp), %xmm5
	movq	-4152(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L136
	call	_ZdlPv@PLT
.L136:
	movq	-4240(%rbp), %xmm0
	movl	$72, %edi
	movq	%rbx, -128(%rbp)
	movq	$0, -3952(%rbp)
	movhps	-4272(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-4288(%rbp), %xmm0
	movhps	-4304(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-4352(%rbp), %xmm0
	movhps	-4360(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-4336(%rbp), %xmm0
	movhps	-4320(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3968(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-192(%rbp), %xmm5
	movdqa	-176(%rbp), %xmm3
	movdqa	-160(%rbp), %xmm6
	leaq	72(%rax), %rdx
	movq	%rax, -3968(%rbp)
	movdqa	-144(%rbp), %xmm7
	movq	-4120(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L137
	call	_ZdlPv@PLT
.L137:
	leaq	-712(%rbp), %rcx
	leaq	-2440(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L274:
	leaq	-2440(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4072(%rbp)
	movq	$0, -4064(%rbp)
	movq	$0, -4056(%rbp)
	movq	$0, -4048(%rbp)
	movq	$0, -4040(%rbp)
	movq	$0, -4032(%rbp)
	movq	$0, -4024(%rbp)
	movq	$0, -4016(%rbp)
	movq	$0, -4000(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4000(%rbp), %rax
	movq	-4152(%rbp), %rdi
	leaq	-4048(%rbp), %r8
	pushq	%rax
	leaq	-4016(%rbp), %rax
	leaq	-4040(%rbp), %r9
	pushq	%rax
	leaq	-4024(%rbp), %rax
	leaq	-4056(%rbp), %rcx
	pushq	%rax
	leaq	-4032(%rbp), %rax
	leaq	-4064(%rbp), %rdx
	pushq	%rax
	leaq	-4072(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SJ_
	addq	$32, %rsp
	movl	$99, %edx
	movq	%r14, %rdi
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	movq	-4040(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE(%rip), %esi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler16IsDetachedBufferENS0_8compiler5TNodeINS0_13JSArrayBufferEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-4040(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	-4064(%rbp), %rcx
	movq	-4056(%rbp), %rdx
	movq	-4048(%rbp), %rsi
	movaps	%xmm0, -3968(%rbp)
	movq	-4032(%rbp), %r11
	movq	-4024(%rbp), %r10
	movq	%rdi, -4320(%rbp)
	movq	-4016(%rbp), %r9
	movq	-4072(%rbp), %rax
	movq	%rdi, -160(%rbp)
	movl	$72, %edi
	movq	-4000(%rbp), %rbx
	movq	%rcx, -4336(%rbp)
	movq	%rdx, -4272(%rbp)
	movq	%rsi, -4288(%rbp)
	movq	%r11, -4352(%rbp)
	movq	%r10, -4360(%rbp)
	movq	%r9, -4368(%rbp)
	movq	%rcx, -184(%rbp)
	movq	%rdx, -176(%rbp)
	movq	%rsi, -168(%rbp)
	movq	%r11, -152(%rbp)
	movq	%r10, -144(%rbp)
	movq	%r9, -136(%rbp)
	movq	%rax, -4304(%rbp)
	movq	%rax, -192(%rbp)
	movq	%rbx, -128(%rbp)
	movq	$0, -3952(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-192(%rbp), %xmm4
	movdqa	-176(%rbp), %xmm5
	movdqa	-160(%rbp), %xmm3
	leaq	72(%rax), %rdx
	movq	%rax, -3968(%rbp)
	movdqa	-144(%rbp), %xmm6
	movq	-4240(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm6, 48(%rax)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L139
	call	_ZdlPv@PLT
.L139:
	movq	-4304(%rbp), %xmm0
	movl	$72, %edi
	movq	%rbx, -128(%rbp)
	movq	$0, -3952(%rbp)
	movhps	-4336(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-4272(%rbp), %xmm0
	movhps	-4288(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-4320(%rbp), %xmm0
	movhps	-4352(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-4360(%rbp), %xmm0
	movhps	-4368(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3968(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-192(%rbp), %xmm7
	movdqa	-176(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm5
	leaq	72(%rax), %rdx
	movq	%rax, -3968(%rbp)
	movdqa	-144(%rbp), %xmm3
	movq	-4184(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L140
	call	_ZdlPv@PLT
.L140:
	movq	-4312(%rbp), %rdx
	leaq	-2056(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2240(%rbp)
	je	.L141
.L275:
	movq	-4312(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4072(%rbp)
	movq	$0, -4064(%rbp)
	movq	$0, -4056(%rbp)
	movq	$0, -4048(%rbp)
	movq	$0, -4040(%rbp)
	movq	$0, -4032(%rbp)
	movq	$0, -4024(%rbp)
	movq	$0, -4016(%rbp)
	movq	$0, -4000(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4000(%rbp), %rax
	movq	-4240(%rbp), %rdi
	leaq	-4056(%rbp), %rcx
	pushq	%rax
	leaq	-4016(%rbp), %rax
	leaq	-4064(%rbp), %rdx
	pushq	%rax
	leaq	-4024(%rbp), %rax
	leaq	-4072(%rbp), %rsi
	pushq	%rax
	leaq	-4032(%rbp), %rax
	leaq	-4040(%rbp), %r9
	pushq	%rax
	leaq	-4048(%rbp), %r8
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SJ_
	movq	-4072(%rbp), %rax
	pxor	%xmm0, %xmm0
	addq	$32, %rsp
	movl	$72, %edi
	movaps	%xmm0, -3968(%rbp)
	movq	%rax, -192(%rbp)
	movq	-4064(%rbp), %rax
	movq	$0, -3952(%rbp)
	movq	%rax, -184(%rbp)
	movq	-4056(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-4048(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-4040(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-4032(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-4024(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-4016(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-4000(%rbp), %rax
	movq	%rax, -128(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-192(%rbp), %xmm6
	movdqa	-176(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm4
	leaq	72(%rax), %rdx
	movq	%rax, -3968(%rbp)
	movdqa	-144(%rbp), %xmm5
	movq	-4216(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L142
	call	_ZdlPv@PLT
.L142:
	leaq	-1672(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2048(%rbp)
	je	.L143
.L276:
	leaq	-2056(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4072(%rbp)
	movq	$0, -4064(%rbp)
	movq	$0, -4056(%rbp)
	movq	$0, -4048(%rbp)
	movq	$0, -4040(%rbp)
	movq	$0, -4032(%rbp)
	movq	$0, -4024(%rbp)
	movq	$0, -4016(%rbp)
	movq	$0, -4000(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4000(%rbp), %rax
	movq	-4184(%rbp), %rdi
	leaq	-4056(%rbp), %rcx
	pushq	%rax
	leaq	-4016(%rbp), %rax
	leaq	-4040(%rbp), %r9
	pushq	%rax
	leaq	-4024(%rbp), %rax
	leaq	-4048(%rbp), %r8
	pushq	%rax
	leaq	-4032(%rbp), %rax
	leaq	-4064(%rbp), %rdx
	pushq	%rax
	leaq	-4072(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SJ_
	addq	$32, %rsp
	movl	$100, %edx
	movq	%r14, %rdi
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$19, %edx
	movq	%r14, %rdi
	leaq	.LC3(%rip), %rsi
	movq	-4040(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4040(%rbp), %rax
	movl	$72, %edi
	movq	-4056(%rbp), %xmm0
	movq	-4072(%rbp), %xmm1
	movq	%rbx, -152(%rbp)
	movq	%rax, -160(%rbp)
	movq	-4024(%rbp), %rax
	movhps	-4048(%rbp), %xmm0
	movhps	-4064(%rbp), %xmm1
	movaps	%xmm0, -176(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -144(%rbp)
	movq	-4016(%rbp), %rax
	movaps	%xmm1, -192(%rbp)
	movq	%rax, -136(%rbp)
	movq	-4000(%rbp), %rax
	movaps	%xmm0, -3968(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -3952(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-192(%rbp), %xmm3
	movdqa	-176(%rbp), %xmm6
	movdqa	-160(%rbp), %xmm7
	leaq	72(%rax), %rdx
	movq	%rax, -3968(%rbp)
	movdqa	-144(%rbp), %xmm4
	movq	-4200(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm4, 48(%rax)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L144
	call	_ZdlPv@PLT
.L144:
	leaq	-1864(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1856(%rbp)
	je	.L145
.L277:
	leaq	-1864(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4072(%rbp)
	movq	$0, -4064(%rbp)
	movq	$0, -4056(%rbp)
	movq	$0, -4048(%rbp)
	movq	$0, -4040(%rbp)
	movq	$0, -4032(%rbp)
	movq	$0, -4024(%rbp)
	movq	$0, -4016(%rbp)
	movq	$0, -4000(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4000(%rbp), %rax
	movq	-4200(%rbp), %rdi
	leaq	-4056(%rbp), %rcx
	pushq	%rax
	leaq	-4016(%rbp), %rax
	leaq	-4064(%rbp), %rdx
	pushq	%rax
	leaq	-4024(%rbp), %rax
	leaq	-4072(%rbp), %rsi
	pushq	%rax
	leaq	-4032(%rbp), %rax
	leaq	-4040(%rbp), %r9
	pushq	%rax
	leaq	-4048(%rbp), %r8
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SJ_
	movq	-4072(%rbp), %rax
	pxor	%xmm0, %xmm0
	addq	$32, %rsp
	movl	$72, %edi
	movaps	%xmm0, -3968(%rbp)
	movq	%rax, -192(%rbp)
	movq	-4064(%rbp), %rax
	movq	$0, -3952(%rbp)
	movq	%rax, -184(%rbp)
	movq	-4056(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-4048(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-4040(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-4032(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-4024(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-4016(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-4000(%rbp), %rax
	movq	%rax, -128(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-192(%rbp), %xmm5
	movdqa	-176(%rbp), %xmm3
	movdqa	-160(%rbp), %xmm6
	leaq	72(%rax), %rdx
	movq	%rax, -3968(%rbp)
	movdqa	-144(%rbp), %xmm7
	movq	-4160(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L146
	call	_ZdlPv@PLT
.L146:
	leaq	-1480(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1664(%rbp)
	je	.L147
.L278:
	leaq	-1672(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4072(%rbp)
	movq	$0, -4064(%rbp)
	movq	$0, -4056(%rbp)
	movq	$0, -4048(%rbp)
	movq	$0, -4040(%rbp)
	movq	$0, -4032(%rbp)
	movq	$0, -4024(%rbp)
	movq	$0, -4016(%rbp)
	movq	$0, -4000(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4000(%rbp), %rax
	movq	-4216(%rbp), %rdi
	leaq	-4056(%rbp), %rcx
	pushq	%rax
	leaq	-4016(%rbp), %rax
	leaq	-4064(%rbp), %rdx
	pushq	%rax
	leaq	-4024(%rbp), %rax
	leaq	-4072(%rbp), %rsi
	pushq	%rax
	leaq	-4032(%rbp), %rax
	leaq	-4040(%rbp), %r9
	pushq	%rax
	leaq	-4048(%rbp), %r8
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SJ_
	movq	-4072(%rbp), %rax
	pxor	%xmm0, %xmm0
	addq	$32, %rsp
	movl	$72, %edi
	movaps	%xmm0, -3968(%rbp)
	movq	%rax, -192(%rbp)
	movq	-4064(%rbp), %rax
	movq	$0, -3952(%rbp)
	movq	%rax, -184(%rbp)
	movq	-4056(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-4048(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-4040(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-4032(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-4024(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-4016(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-4000(%rbp), %rax
	movq	%rax, -128(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-192(%rbp), %xmm4
	movdqa	-176(%rbp), %xmm5
	movdqa	-160(%rbp), %xmm3
	leaq	72(%rax), %rdx
	movq	%rax, -3968(%rbp)
	movdqa	-144(%rbp), %xmm6
	movq	-4120(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm6, 48(%rax)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L148
	call	_ZdlPv@PLT
.L148:
	leaq	-712(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1472(%rbp)
	je	.L149
.L279:
	leaq	-1480(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4088(%rbp)
	leaq	-4016(%rbp), %r12
	movq	$0, -4080(%rbp)
	movq	$0, -4072(%rbp)
	movq	$0, -4064(%rbp)
	movq	$0, -4056(%rbp)
	movq	$0, -4048(%rbp)
	movq	$0, -4040(%rbp)
	movq	$0, -4032(%rbp)
	movq	$0, -4024(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4024(%rbp), %rax
	movq	-4160(%rbp), %rdi
	leaq	-4072(%rbp), %rcx
	pushq	%rax
	leaq	-4032(%rbp), %rax
	leaq	-4056(%rbp), %r9
	pushq	%rax
	leaq	-4040(%rbp), %rax
	leaq	-4064(%rbp), %r8
	pushq	%rax
	leaq	-4048(%rbp), %rax
	leaq	-4080(%rbp), %rdx
	pushq	%rax
	leaq	-4088(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SJ_
	addq	$32, %rsp
	movl	$20, %edx
	movq	%r14, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$104, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$105, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %edi
	movq	-4088(%rbp), %rbx
	call	_ZN2v88internal42ExampleBuiltinForTorqueFunctionPointerTypeEm@PLT
	movq	%r14, %rdi
	movl	%eax, -4272(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	-4272(%rbp), %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movl	$2, %edx
	movq	%rbx, %r9
	movq	%r12, %rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rdx
	movq	-4048(%rbp), %xmm0
	leaq	-4000(%rbp), %r10
	movq	%rax, -4000(%rbp)
	movq	-3952(%rbp), %rax
	movq	%r10, %rdx
	movl	$1, %ecx
	movq	-4040(%rbp), %r8
	movhps	-4024(%rbp), %xmm0
	movl	$2, %esi
	movq	%rax, -3992(%rbp)
	leaq	-192(%rbp), %rax
	pushq	%rax
	movaps	%xmm0, -192(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$104, %edi
	movq	%rbx, -96(%rbp)
	movq	-4040(%rbp), %xmm0
	movq	-4056(%rbp), %xmm1
	movq	-4072(%rbp), %xmm2
	movq	$0, -3952(%rbp)
	movq	-4088(%rbp), %xmm3
	movhps	-4032(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movhps	-4048(%rbp), %xmm1
	movq	-4024(%rbp), %xmm0
	movhps	-4064(%rbp), %xmm2
	movhps	-4080(%rbp), %xmm3
	movaps	%xmm2, -176(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	-4088(%rbp), %xmm0
	movaps	%xmm1, -160(%rbp)
	movhps	-4024(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3968(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm7
	movq	%r15, %rsi
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm6
	movq	-4192(%rbp), %rdi
	leaq	104(%rax), %rdx
	movq	%rax, -3968(%rbp)
	movq	%rcx, 96(%rax)
	movdqa	-176(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm5
	movdqa	-144(%rbp), %xmm3
	movups	%xmm7, (%rax)
	movdqa	-112(%rbp), %xmm7
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm6, 64(%rax)
	movups	%xmm7, 80(%rax)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	popq	%rcx
	popq	%rsi
	testq	%rdi, %rdi
	je	.L150
	call	_ZdlPv@PLT
.L150:
	leaq	-1288(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1280(%rbp)
	je	.L151
.L280:
	leaq	-1288(%rbp), %rsi
	movq	%r14, %rdi
	movabsq	$434042140868675335, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$13, %edi
	movq	$0, -3952(%rbp)
	movaps	%xmm0, -3968(%rbp)
	call	_Znwm@PLT
	movq	-4192(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, (%rax)
	leaq	13(%rax), %rdx
	movl	$101123590, 8(%rax)
	movb	$8, 12(%rax)
	movq	%rax, -3968(%rbp)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3968(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L152
	call	_ZdlPv@PLT
.L152:
	movq	(%rbx), %rax
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	movq	40(%rax), %rdx
	movq	(%rax), %rbx
	movq	24(%rax), %rcx
	movq	8(%rax), %r12
	movq	%rdx, -4320(%rbp)
	movq	56(%rax), %rdx
	movq	%rbx, -4312(%rbp)
	movq	16(%rax), %rbx
	movq	%rdx, -4352(%rbp)
	movq	64(%rax), %rdx
	movq	%rbx, -4272(%rbp)
	movq	48(%rax), %rbx
	movq	%rcx, -4336(%rbp)
	movq	32(%rax), %rcx
	movq	96(%rax), %rax
	movq	%rdx, -4360(%rbp)
	movl	$20, %edx
	movq	%rcx, -4288(%rbp)
	movq	%rax, -4304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$21, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$95, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %xmm4
	movl	$128, %edi
	movq	-4312(%rbp), %xmm5
	movq	-4360(%rbp), %rax
	movq	$0, -3952(%rbp)
	movdqa	%xmm5, %xmm0
	punpcklqdq	%xmm4, %xmm0
	movq	-4336(%rbp), %xmm4
	movaps	%xmm0, -192(%rbp)
	movq	-4272(%rbp), %xmm0
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-4288(%rbp), %xmm0
	movhps	-4320(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%rbx, %xmm0
	movhps	-4352(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%rax, %xmm0
	movhps	-4304(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movdqa	%xmm5, %xmm0
	movhps	-4272(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movdqa	%xmm4, %xmm0
	movhps	-4304(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rax, %xmm0
	movhps	-4288(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3968(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm5
	movq	%r15, %rsi
	movdqa	-176(%rbp), %xmm3
	movdqa	-160(%rbp), %xmm6
	movdqa	-144(%rbp), %xmm7
	leaq	128(%rax), %rdx
	movq	%rax, -3968(%rbp)
	movdqa	-128(%rbp), %xmm4
	movups	%xmm5, (%rax)
	movdqa	-112(%rbp), %xmm5
	movups	%xmm3, 16(%rax)
	movdqa	-96(%rbp), %xmm3
	movq	-4208(%rbp), %rdi
	movups	%xmm6, 32(%rax)
	movdqa	-80(%rbp), %xmm6
	movups	%xmm7, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm5, 80(%rax)
	movups	%xmm3, 96(%rax)
	movups	%xmm6, 112(%rax)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L153
	call	_ZdlPv@PLT
.L153:
	leaq	-1096(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1088(%rbp)
	je	.L154
.L281:
	leaq	-1096(%rbp), %rsi
	movq	%r14, %rdi
	movabsq	$434042140868675335, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	$0, -3952(%rbp)
	movaps	%xmm0, -3968(%rbp)
	call	_Znwm@PLT
	movq	-4208(%rbp), %rdi
	movabsq	$506100838696421382, %rsi
	movq	%rbx, (%rax)
	leaq	16(%rax), %rdx
	movq	%rsi, 8(%rax)
	movq	%r15, %rsi
	movq	%rax, -3968(%rbp)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3968(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L155
	call	_ZdlPv@PLT
.L155:
	movq	(%rbx), %rax
	movl	$21, %edx
	movq	%r14, %rdi
	leaq	.LC3(%rip), %rsi
	leaq	-4016(%rbp), %r12
	movq	(%rax), %rbx
	movq	64(%rax), %rcx
	movq	%rbx, -4272(%rbp)
	movq	8(%rax), %rbx
	movq	%rcx, -4368(%rbp)
	movq	88(%rax), %rcx
	movq	%rbx, -4288(%rbp)
	movq	16(%rax), %rbx
	movq	%rcx, -4392(%rbp)
	movq	104(%rax), %rcx
	movq	%rbx, -4304(%rbp)
	movq	24(%rax), %rbx
	movq	%rcx, -4400(%rbp)
	movq	112(%rax), %rcx
	movq	%rbx, -4312(%rbp)
	movq	32(%rax), %rbx
	movq	%rcx, -4416(%rbp)
	movq	%rbx, -4336(%rbp)
	movq	40(%rax), %rbx
	movq	%rbx, -4320(%rbp)
	movq	48(%rax), %rbx
	movq	%rbx, -4352(%rbp)
	movq	56(%rax), %rbx
	movq	%rbx, -4360(%rbp)
	movq	80(%rax), %rbx
	movq	%rbx, -4384(%rbp)
	movq	96(%rax), %rbx
	movq	120(%rax), %rax
	movq	%rax, -4424(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19IsUndefinedConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	je	.L156
.L158:
	movq	-4416(%rbp), %xmm0
	movq	%rbx, %xmm1
	movq	%r12, %rdi
	movhps	-4400(%rbp), %xmm1
	movhps	-4424(%rbp), %xmm0
	movaps	%xmm1, -4448(%rbp)
	movaps	%xmm0, -4416(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-3968(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -4400(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-4448(%rbp), %xmm1
	movq	-4392(%rbp), %xmm2
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movdqa	-4416(%rbp), %xmm0
	movq	%rax, -4000(%rbp)
	movq	-3952(%rbp), %rax
	movhps	-4400(%rbp), %xmm2
	movaps	%xmm2, -192(%rbp)
	movq	%rax, -3992(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
.L266:
	leaq	-192(%rbp), %rcx
	movl	$6, %ebx
	movq	-4384(%rbp), %r9
	xorl	%esi, %esi
	pushq	%rbx
	leaq	-4000(%rbp), %rdx
	movq	%r12, %rdi
	pushq	%rcx
	movl	$1, %ecx
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	popq	%rax
	movq	%r12, %rdi
	popq	%rdx
	movq	-4272(%rbp), %xmm3
	movq	-4304(%rbp), %xmm2
	movq	-4336(%rbp), %xmm1
	movq	-4352(%rbp), %xmm0
	movhps	-4288(%rbp), %xmm3
	movhps	-4312(%rbp), %xmm2
	movhps	-4320(%rbp), %xmm1
	movhps	-4360(%rbp), %xmm0
	movaps	%xmm3, -4384(%rbp)
	movaps	%xmm2, -4304(%rbp)
	movaps	%xmm1, -4288(%rbp)
	movaps	%xmm0, -4272(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$17, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movdqa	-4272(%rbp), %xmm0
	movdqa	-4384(%rbp), %xmm3
	movl	$72, %edi
	movdqa	-4304(%rbp), %xmm2
	movdqa	-4288(%rbp), %xmm1
	movq	$0, -3952(%rbp)
	movq	-4368(%rbp), %rax
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -3968(%rbp)
	movq	%rax, -128(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-192(%rbp), %xmm6
	movdqa	-176(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm4
	leaq	72(%rax), %rdx
	movq	%rax, -3968(%rbp)
	movdqa	-144(%rbp), %xmm5
	movq	-4224(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L159
	call	_ZdlPv@PLT
.L159:
	leaq	-904(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -896(%rbp)
	je	.L160
.L282:
	leaq	-904(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4072(%rbp)
	movq	$0, -4064(%rbp)
	movq	$0, -4056(%rbp)
	movq	$0, -4048(%rbp)
	movq	$0, -4040(%rbp)
	movq	$0, -4032(%rbp)
	movq	$0, -4024(%rbp)
	movq	$0, -4016(%rbp)
	movq	$0, -4000(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4000(%rbp), %rax
	movq	-4224(%rbp), %rdi
	leaq	-4056(%rbp), %rcx
	pushq	%rax
	leaq	-4016(%rbp), %rax
	leaq	-4040(%rbp), %r9
	pushq	%rax
	leaq	-4024(%rbp), %rax
	leaq	-4048(%rbp), %r8
	pushq	%rax
	leaq	-4032(%rbp), %rax
	leaq	-4064(%rbp), %rdx
	pushq	%rax
	leaq	-4072(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SJ_
	addq	$32, %rsp
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4000(%rbp), %rbx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$72, %edi
	movq	%rbx, -128(%rbp)
	movq	-4024(%rbp), %xmm0
	movq	-4040(%rbp), %xmm1
	movq	-4056(%rbp), %xmm2
	movq	$0, -3952(%rbp)
	movq	-4072(%rbp), %xmm3
	movhps	-4016(%rbp), %xmm0
	movhps	-4032(%rbp), %xmm1
	movhps	-4048(%rbp), %xmm2
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-4064(%rbp), %xmm3
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -3968(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-192(%rbp), %xmm7
	movdqa	-176(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm5
	leaq	72(%rax), %rdx
	movq	%rax, -3968(%rbp)
	movdqa	-144(%rbp), %xmm3
	movq	-4112(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L161
	call	_ZdlPv@PLT
.L161:
	leaq	-2632(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -704(%rbp)
	je	.L162
.L283:
	leaq	-712(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4072(%rbp)
	movq	$0, -4064(%rbp)
	movq	$0, -4056(%rbp)
	movq	$0, -4048(%rbp)
	movq	$0, -4040(%rbp)
	movq	$0, -4032(%rbp)
	movq	$0, -4024(%rbp)
	movq	$0, -4016(%rbp)
	movq	$0, -4000(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4000(%rbp), %rax
	movq	-4120(%rbp), %rdi
	leaq	-4056(%rbp), %rcx
	pushq	%rax
	leaq	-4016(%rbp), %rax
	leaq	-4040(%rbp), %r9
	pushq	%rax
	leaq	-4024(%rbp), %rax
	leaq	-4048(%rbp), %r8
	pushq	%rax
	leaq	-4032(%rbp), %rax
	leaq	-4064(%rbp), %rdx
	pushq	%rax
	leaq	-4072(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SJ_
	addq	$32, %rsp
	movl	$23, %edx
	movq	%r14, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4072(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movaps	%xmm0, -3968(%rbp)
	movq	%rdx, -192(%rbp)
	movq	-4064(%rbp), %rdx
	movq	%rax, -160(%rbp)
	movq	%rdx, -184(%rbp)
	movq	-4056(%rbp), %rdx
	movq	$0, -3952(%rbp)
	movq	%rdx, -176(%rbp)
	movq	-4048(%rbp), %rdx
	movq	%rdx, -168(%rbp)
	call	_Znwm@PLT
	movq	-160(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-192(%rbp), %xmm3
	movdqa	-176(%rbp), %xmm6
	movq	-4232(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -3968(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L163
	call	_ZdlPv@PLT
.L163:
	leaq	-520(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L284:
	leaq	-520(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -3952(%rbp)
	movaps	%xmm0, -3968(%rbp)
	call	_Znwm@PLT
	movq	-4232(%rbp), %rdi
	movq	%r15, %rsi
	movl	$134678279, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -3968(%rbp)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3968(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L165
	call	_ZdlPv@PLT
.L165:
	movq	(%rbx), %rax
	movl	$10, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	movq	8(%rax), %rbx
	movq	(%rax), %r12
	movq	24(%rax), %r13
	movq	%rbx, -4272(%rbp)
	movq	16(%rax), %rbx
	movq	%rbx, -4288(%rbp)
	movq	32(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %xmm0
	movq	%r13, %xmm7
	movl	$40, %edi
	movhps	-4272(%rbp), %xmm0
	movq	%rbx, -160(%rbp)
	movaps	%xmm0, -192(%rbp)
	movq	-4288(%rbp), %xmm0
	movq	$0, -3952(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -176(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3968(%rbp)
	call	_Znwm@PLT
	movq	-160(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-192(%rbp), %xmm4
	movdqa	-176(%rbp), %xmm5
	movq	-4104(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -3968(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -3952(%rbp)
	movq	%rdx, -3960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L166
	call	_ZdlPv@PLT
.L166:
	leaq	-328(%rbp), %r12
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L156:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IsNullConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L158
	movq	-4416(%rbp), %xmm1
	movq	%rbx, %xmm0
	movq	%r12, %rdi
	movhps	-4400(%rbp), %xmm0
	movhps	-4424(%rbp), %xmm1
	movaps	%xmm0, -4448(%rbp)
	movaps	%xmm1, -4416(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-3968(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -4400(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-4448(%rbp), %xmm0
	movq	-4392(%rbp), %xmm2
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movdqa	-4416(%rbp), %xmm1
	movq	%rax, -4000(%rbp)
	movq	-3952(%rbp), %rax
	movhps	-4400(%rbp), %xmm2
	movaps	%xmm2, -192(%rbp)
	movq	%rax, -3992(%rbp)
	movaps	%xmm0, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L286:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %rdx
	movq	%r12, %rdi
	movdqa	-4176(%rbp), %xmm3
	movdqa	-4336(%rbp), %xmm6
	leaq	-192(%rbp), %rsi
	movaps	%xmm0, -4000(%rbp)
	movdqa	-4352(%rbp), %xmm7
	movq	$0, -3984(%rbp)
	movaps	%xmm3, -192(%rbp)
	movdqa	-4272(%rbp), %xmm3
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm7, -144(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4248(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4000(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L123
	call	_ZdlPv@PLT
.L123:
	leaq	-3400(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L122
.L285:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22417:
	.size	_ZN2v88internal22ForEachAllElements_359EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal22ForEachAllElements_359EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal35TypedArrayPrototypeForEachAssembler38GenerateTypedArrayPrototypeForEachImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal35TypedArrayPrototypeForEachAssembler38GenerateTypedArrayPrototypeForEachImplEv
	.type	_ZN2v88internal35TypedArrayPrototypeForEachAssembler38GenerateTypedArrayPrototypeForEachImplEv, @function
_ZN2v88internal35TypedArrayPrototypeForEachAssembler38GenerateTypedArrayPrototypeForEachImplEv:
.LFB22472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-2048(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$2504, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -2360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r13, %rcx
	movq	%r12, %rsi
	leaq	-2352(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-2336(%rbp), %rcx
	movl	$2, %esi
	movq	%r12, %rdi
	movq	-2352(%rbp), %r13
	movq	-2344(%rbp), %rax
	movq	%r12, -2224(%rbp)
	leaq	-2360(%rbp), %r12
	movq	%rcx, -2480(%rbp)
	movq	%rcx, -2208(%rbp)
	movq	%r13, -2192(%rbp)
	movq	%rax, -2496(%rbp)
	movq	$1, -2216(%rbp)
	movq	%rax, -2200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rax, -2464(%rbp)
	leaq	-2224(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -2504(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movq	%rax, %rbx
	movq	-2360(%rbp), %rax
	movq	$0, -2024(%rbp)
	movq	%rax, -2048(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -2040(%rbp)
	leaq	-1992(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2024(%rbp)
	movq	%rdx, -2032(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2008(%rbp)
	movq	%rax, -2376(%rbp)
	movq	$0, -2016(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2360(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1848(%rbp)
	movq	$0, -1840(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1832(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1848(%rbp)
	leaq	-1800(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1832(%rbp)
	movq	%rdx, -1840(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1816(%rbp)
	movq	%rax, -2440(%rbp)
	movq	$0, -1824(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2360(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1656(%rbp)
	movq	$0, -1648(%rbp)
	movq	%rax, -1664(%rbp)
	movq	$0, -1640(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1656(%rbp)
	leaq	-1608(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1640(%rbp)
	movq	%rdx, -1648(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1624(%rbp)
	movq	%rax, -2392(%rbp)
	movq	$0, -1632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2360(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1464(%rbp)
	movq	$0, -1456(%rbp)
	movq	%rax, -1472(%rbp)
	movq	$0, -1448(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1464(%rbp)
	leaq	-1416(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1448(%rbp)
	movq	%rdx, -1456(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1432(%rbp)
	movq	%rax, -2432(%rbp)
	movq	$0, -1440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2360(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1272(%rbp)
	movq	$0, -1264(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -1256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1272(%rbp)
	leaq	-1224(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1256(%rbp)
	movq	%rdx, -1264(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1240(%rbp)
	movq	%rax, -2384(%rbp)
	movq	$0, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2360(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1080(%rbp)
	movq	$0, -1072(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$0, -1064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1080(%rbp)
	leaq	-1032(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1064(%rbp)
	movq	%rdx, -1072(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1048(%rbp)
	movq	%rax, -2448(%rbp)
	movq	$0, -1056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2360(%rbp), %rax
	movl	$216, %edi
	movq	$0, -888(%rbp)
	movq	$0, -880(%rbp)
	movq	%rax, -896(%rbp)
	movq	$0, -872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -888(%rbp)
	leaq	-840(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -872(%rbp)
	movq	%rdx, -880(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -856(%rbp)
	movq	%rax, -2408(%rbp)
	movq	$0, -864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2360(%rbp), %rax
	movl	$120, %edi
	movq	$0, -696(%rbp)
	movq	$0, -688(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	$0, 112(%rax)
	leaq	120(%rax), %rdx
	movq	%r12, %rsi
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -696(%rbp)
	leaq	-648(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -680(%rbp)
	movq	%rdx, -688(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -664(%rbp)
	movq	%rax, -2424(%rbp)
	movq	$0, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2360(%rbp), %rax
	movl	$120, %edi
	movq	$0, -504(%rbp)
	movq	$0, -496(%rbp)
	movq	%rax, -512(%rbp)
	movq	$0, -488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	$0, 112(%rax)
	leaq	120(%rax), %rdx
	movq	%r12, %rsi
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -504(%rbp)
	leaq	-456(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -488(%rbp)
	movq	%rdx, -496(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -472(%rbp)
	movq	%rax, -2400(%rbp)
	movq	$0, -480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2360(%rbp), %rax
	movl	$120, %edi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	$0, 112(%rax)
	leaq	120(%rax), %rdx
	movq	%r12, %rsi
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -312(%rbp)
	leaq	-264(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	xorl	%edx, %edx
	movq	%rax, -2416(%rbp)
	movups	%xmm0, -280(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %xmm1
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movhps	-2496(%rbp), %xmm1
	movq	%rbx, -96(%rbp)
	leaq	-2176(%rbp), %r13
	movaps	%xmm1, -128(%rbp)
	movq	-2480(%rbp), %xmm1
	movaps	%xmm0, -2176(%rbp)
	movhps	-2464(%rbp), %xmm1
	movq	$0, -2160(%rbp)
	movaps	%xmm1, -112(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm4
	leaq	40(%rax), %rdx
	movq	%rax, -2176(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2160(%rbp)
	movq	%rdx, -2168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L288
	call	_ZdlPv@PLT
.L288:
	movq	-2376(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1984(%rbp)
	jne	.L522
	cmpq	$0, -1792(%rbp)
	jne	.L523
.L294:
	cmpq	$0, -1600(%rbp)
	jne	.L524
.L297:
	cmpq	$0, -1408(%rbp)
	jne	.L525
.L302:
	cmpq	$0, -1216(%rbp)
	jne	.L526
.L305:
	cmpq	$0, -1024(%rbp)
	jne	.L527
.L310:
	cmpq	$0, -832(%rbp)
	jne	.L528
.L313:
	cmpq	$0, -640(%rbp)
	jne	.L529
.L318:
	cmpq	$0, -448(%rbp)
	jne	.L530
.L320:
	cmpq	$0, -256(%rbp)
	jne	.L531
.L322:
	movq	-2416(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L324
	call	_ZdlPv@PLT
.L324:
	movq	-304(%rbp), %rbx
	movq	-312(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L325
	.p2align 4,,10
	.p2align 3
.L329:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L326
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L329
.L327:
	movq	-312(%rbp), %r13
.L325:
	testq	%r13, %r13
	je	.L330
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L330:
	movq	-2400(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L331
	call	_ZdlPv@PLT
.L331:
	movq	-496(%rbp), %rbx
	movq	-504(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L332
	.p2align 4,,10
	.p2align 3
.L336:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L333
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L336
.L334:
	movq	-504(%rbp), %r13
.L332:
	testq	%r13, %r13
	je	.L337
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L337:
	movq	-2424(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L338
	call	_ZdlPv@PLT
.L338:
	movq	-688(%rbp), %rbx
	movq	-696(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L339
	.p2align 4,,10
	.p2align 3
.L343:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L340
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L343
.L341:
	movq	-696(%rbp), %r13
.L339:
	testq	%r13, %r13
	je	.L344
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L344:
	movq	-2408(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-864(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L345
	call	_ZdlPv@PLT
.L345:
	movq	-880(%rbp), %rbx
	movq	-888(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L346
	.p2align 4,,10
	.p2align 3
.L350:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L347
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L350
.L348:
	movq	-888(%rbp), %r13
.L346:
	testq	%r13, %r13
	je	.L351
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L351:
	movq	-2448(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L352
	call	_ZdlPv@PLT
.L352:
	movq	-1072(%rbp), %rbx
	movq	-1080(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L353
	.p2align 4,,10
	.p2align 3
.L357:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L354
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L357
.L355:
	movq	-1080(%rbp), %r13
.L353:
	testq	%r13, %r13
	je	.L358
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L358:
	movq	-2384(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L359
	call	_ZdlPv@PLT
.L359:
	movq	-1264(%rbp), %rbx
	movq	-1272(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L360
	.p2align 4,,10
	.p2align 3
.L364:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L361
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L364
.L362:
	movq	-1272(%rbp), %r13
.L360:
	testq	%r13, %r13
	je	.L365
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L365:
	movq	-2432(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1440(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L366
	call	_ZdlPv@PLT
.L366:
	movq	-1456(%rbp), %rbx
	movq	-1464(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L367
	.p2align 4,,10
	.p2align 3
.L371:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L368
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L371
.L369:
	movq	-1464(%rbp), %r13
.L367:
	testq	%r13, %r13
	je	.L372
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L372:
	movq	-2392(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1632(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L373
	call	_ZdlPv@PLT
.L373:
	movq	-1648(%rbp), %rbx
	movq	-1656(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L374
	.p2align 4,,10
	.p2align 3
.L378:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L375
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L378
.L376:
	movq	-1656(%rbp), %r13
.L374:
	testq	%r13, %r13
	je	.L379
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L379:
	movq	-2440(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L380
	call	_ZdlPv@PLT
.L380:
	movq	-1840(%rbp), %rbx
	movq	-1848(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L381
	.p2align 4,,10
	.p2align 3
.L385:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L382
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L385
.L383:
	movq	-1848(%rbp), %r13
.L381:
	testq	%r13, %r13
	je	.L386
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L386:
	movq	-2376(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2016(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L387
	call	_ZdlPv@PLT
.L387:
	movq	-2032(%rbp), %rbx
	movq	-2040(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L388
	.p2align 4,,10
	.p2align 3
.L392:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L389
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L392
.L390:
	movq	-2040(%rbp), %r13
.L388:
	testq	%r13, %r13
	je	.L393
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L393:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L532
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L389:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L392
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L382:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L385
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L375:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L378
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L368:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L371
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L361:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L364
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L354:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L357
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L347:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L350
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L340:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L343
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L326:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L329
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L333:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L336
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L522:
	movq	-2376(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2160(%rbp)
	movaps	%xmm0, -2176(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2176(%rbp)
	movq	%rdx, -2160(%rbp)
	movq	%rdx, -2168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2176(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L290
	call	_ZdlPv@PLT
.L290:
	movq	(%rbx), %rax
	movl	$34, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	24(%rax), %r14
	movq	32(%rax), %rbx
	movq	%rsi, -2480(%rbp)
	movq	16(%rax), %rsi
	movq	%rcx, -2464(%rbp)
	movq	%rsi, -2496(%rbp)
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal22Cast12JSTypedArray_110EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm2
	movq	%rbx, %xmm5
	movq	-2496(%rbp), %xmm6
	punpcklqdq	%xmm5, %xmm5
	pxor	%xmm0, %xmm0
	movq	%rax, -80(%rbp)
	movq	-2464(%rbp), %xmm7
	punpcklqdq	%xmm2, %xmm6
	movl	$56, %edi
	movaps	%xmm5, -96(%rbp)
	leaq	-2256(%rbp), %r14
	movhps	-2480(%rbp), %xmm7
	movaps	%xmm5, -2528(%rbp)
	movaps	%xmm6, -2496(%rbp)
	movaps	%xmm7, -2464(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm0, -2256(%rbp)
	movq	$0, -2240(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm2
	movq	-80(%rbp), %rcx
	leaq	-1664(%rbp), %rdi
	movdqa	-96(%rbp), %xmm4
	leaq	56(%rax), %rdx
	movq	%r14, %rsi
	movq	%rax, -2256(%rbp)
	movups	%xmm2, (%rax)
	movdqa	-112(%rbp), %xmm2
	movq	%rcx, 48(%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm4, 32(%rax)
	movq	%rdx, -2240(%rbp)
	movq	%rdx, -2248(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L291
	call	_ZdlPv@PLT
.L291:
	movq	-2392(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2168(%rbp)
	jne	.L533
.L292:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1792(%rbp)
	je	.L294
.L523:
	movq	-2440(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1856(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2160(%rbp)
	movaps	%xmm0, -2176(%rbp)
	call	_Znwm@PLT
	movl	$2056, %edi
	movq	%r13, %rsi
	movw	%di, 4(%rax)
	leaq	6(%rax), %rdx
	movq	%r14, %rdi
	movl	$117769477, (%rax)
	movq	%rax, -2176(%rbp)
	movq	%rdx, -2160(%rbp)
	movq	%rdx, -2168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2176(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L295
	call	_ZdlPv@PLT
.L295:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	32(%rax), %rdx
	movdqu	(%rax), %xmm2
	movq	$0, -2160(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm0, -2176(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm2
	leaq	40(%rax), %rdx
	leaq	-512(%rbp), %rdi
	movq	%rax, -2176(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -2160(%rbp)
	movq	%rdx, -2168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L296
	call	_ZdlPv@PLT
.L296:
	movq	-2400(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1600(%rbp)
	je	.L297
.L524:
	movq	-2392(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1664(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2160(%rbp)
	movaps	%xmm0, -2176(%rbp)
	call	_Znwm@PLT
	movl	$2056, %esi
	movq	%r14, %rdi
	movw	%si, 4(%rax)
	leaq	7(%rax), %rdx
	movq	%r13, %rsi
	movl	$117769477, (%rax)
	movb	$7, 6(%rax)
	movq	%rax, -2176(%rbp)
	movq	%rdx, -2160(%rbp)
	movq	%rdx, -2168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2176(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L298
	call	_ZdlPv@PLT
.L298:
	movq	(%rbx), %rax
	movl	$36, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	48(%rax), %rbx
	movq	16(%rax), %r14
	movq	%rsi, -2480(%rbp)
	movq	24(%rax), %rsi
	movq	%rcx, -2464(%rbp)
	movq	%rsi, -2496(%rbp)
	movq	32(%rax), %rsi
	movq	%rsi, -2528(%rbp)
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal18EnsureAttached_369EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_12JSTypedArrayEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm6
	movq	%r14, %xmm4
	movq	-2528(%rbp), %xmm3
	movhps	-2496(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	-2464(%rbp), %xmm5
	punpcklqdq	%xmm6, %xmm3
	movaps	%xmm4, -2496(%rbp)
	leaq	-2256(%rbp), %r14
	movhps	-2480(%rbp), %xmm5
	movaps	%xmm3, -2528(%rbp)
	movaps	%xmm5, -2464(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -2256(%rbp)
	movq	%rbx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -2240(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm2
	leaq	64(%rax), %rdx
	leaq	-1280(%rbp), %rdi
	movups	%xmm7, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm2, 48(%rax)
	movq	%rax, -2256(%rbp)
	movq	%rdx, -2240(%rbp)
	movq	%rdx, -2248(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L299
	call	_ZdlPv@PLT
.L299:
	movq	-2384(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2168(%rbp)
	jne	.L534
.L300:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1408(%rbp)
	je	.L302
.L525:
	movq	-2432(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1472(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2160(%rbp)
	movaps	%xmm0, -2176(%rbp)
	call	_Znwm@PLT
	movl	$1800, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117769477, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -2176(%rbp)
	movq	%rdx, -2160(%rbp)
	movq	%rdx, -2168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2176(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L303
	call	_ZdlPv@PLT
.L303:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	32(%rax), %rdx
	movdqu	(%rax), %xmm6
	movq	$0, -2160(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm0, -2176(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm5
	leaq	40(%rax), %rdx
	leaq	-320(%rbp), %rdi
	movq	%rax, -2176(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -2160(%rbp)
	movq	%rdx, -2168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L304
	call	_ZdlPv@PLT
.L304:
	movq	-2416(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1216(%rbp)
	je	.L305
.L526:
	movq	-2384(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1280(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2160(%rbp)
	movaps	%xmm0, -2176(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$506381214161372421, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2176(%rbp)
	movq	%rdx, -2160(%rbp)
	movq	%rdx, -2168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2176(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L306
	call	_ZdlPv@PLT
.L306:
	movq	(%rbx), %rax
	movl	$38, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	24(%rax), %r14
	movq	40(%rax), %rbx
	movq	%rsi, -2496(%rbp)
	movq	32(%rax), %rsi
	movq	%rcx, -2480(%rbp)
	movq	16(%rax), %rcx
	movq	56(%rax), %rax
	movq	%rsi, -2528(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%rcx, -2464(%rbp)
	movq	%rbx, -2512(%rbp)
	movq	%rax, -2536(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	-2480(%rbp), %xmm2
	movq	-2464(%rbp), %rcx
	movhps	-2496(%rbp), %xmm2
	movq	%rcx, -2304(%rbp)
	movaps	%xmm2, -2320(%rbp)
	pushq	-2304(%rbp)
	pushq	-2312(%rbp)
	pushq	-2320(%rbp)
	movaps	%xmm2, -2480(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal82Cast71UT19ATCallableApiObject17ATCallableJSProxy15JSBoundFunction10JSFunction_1405EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm5
	movq	%rbx, %xmm4
	movq	-2528(%rbp), %xmm7
	movdqa	-2480(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movq	-2464(%rbp), %xmm3
	movq	-2536(%rbp), %xmm6
	movhps	-2512(%rbp), %xmm7
	movq	%rax, -64(%rbp)
	leaq	-2256(%rbp), %r14
	punpcklqdq	%xmm5, %xmm3
	movaps	%xmm7, -2528(%rbp)
	punpcklqdq	%xmm4, %xmm6
	movaps	%xmm3, -2464(%rbp)
	movaps	%xmm6, -2496(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm6, -80(%rbp)
	movaps	%xmm0, -2256(%rbp)
	movq	$0, -2240(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-128(%rbp), %xmm2
	leaq	-896(%rbp), %rdi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	72(%rax), %rdx
	movq	%r14, %rsi
	movdqa	-80(%rbp), %xmm4
	movq	%rcx, 64(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm4, 48(%rax)
	movq	%rax, -2256(%rbp)
	movq	%rdx, -2240(%rbp)
	movq	%rdx, -2248(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L307
	call	_ZdlPv@PLT
.L307:
	movq	-2408(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2168(%rbp)
	jne	.L535
.L308:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1024(%rbp)
	je	.L310
.L527:
	movq	-2448(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1088(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2160(%rbp)
	movaps	%xmm0, -2176(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$578438808199300357, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2176(%rbp)
	movq	%rdx, -2160(%rbp)
	movq	%rdx, -2168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2176(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L311
	call	_ZdlPv@PLT
.L311:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	32(%rax), %rdx
	movdqu	(%rax), %xmm2
	movq	$0, -2160(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm0, -2176(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	leaq	40(%rax), %rdx
	leaq	-704(%rbp), %rdi
	movq	%rax, -2176(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2160(%rbp)
	movq	%rdx, -2168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L312
	call	_ZdlPv@PLT
.L312:
	movq	-2424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -832(%rbp)
	je	.L313
.L528:
	movq	-2408(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	leaq	-896(%rbp), %r8
	movq	$0, -2464(%rbp)
	movq	%r8, -2480(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -2160(%rbp)
	movaps	%xmm0, -2176(%rbp)
	call	_Znwm@PLT
	movq	-2480(%rbp), %r8
	movq	%r13, %rsi
	movabsq	$578438808199300357, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%r8, %rdi
	movq	%rax, -2176(%rbp)
	movq	%rdx, -2160(%rbp)
	movq	%rdx, -2168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L314
	movq	%rax, -2480(%rbp)
	call	_ZdlPv@PLT
	movq	-2480(%rbp), %rax
.L314:
	movq	(%rax), %rax
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	movq	24(%rax), %rdx
	movq	(%rax), %xmm0
	movq	16(%rax), %rcx
	testq	%rdx, %rdx
	movhps	8(%rax), %xmm0
	cmovne	%rdx, %r14
	movq	48(%rax), %rdx
	movq	%rcx, -2528(%rbp)
	movaps	%xmm0, -2496(%rbp)
	testq	%rdx, %rdx
	cmove	-2464(%rbp), %rdx
	movq	%rdx, -2464(%rbp)
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	cmovne	%rdx, %rbx
	movl	$39, %edx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -2480(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movdqa	-2496(%rbp), %xmm0
	subq	$8, %rsp
	movq	-2528(%rbp), %rcx
	movq	-2480(%rbp), %r8
	movq	%r13, %rdi
	movaps	%xmm0, -2288(%rbp)
	movq	%rcx, -2272(%rbp)
	movq	%r8, %rsi
	pushq	-2272(%rbp)
	pushq	-2280(%rbp)
	pushq	-2288(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r13, %rdi
	movq	%rax, -2480(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$40, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	-2480(%rbp), %r8
	movq	-2464(%rbp), %rdx
	call	_ZN2v88internal22ForEachAllElements_359EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEE
	movq	-2504(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	cmpq	$0, -640(%rbp)
	je	.L318
.L529:
	movq	-2424(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-704(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2160(%rbp)
	movaps	%xmm0, -2176(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2176(%rbp)
	movq	%rdx, -2160(%rbp)
	movq	%rdx, -2168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2176(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L319
	call	_ZdlPv@PLT
.L319:
	movq	(%rbx), %rax
	movl	$43, %edx
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	movq	24(%rax), %rbx
	movq	%rcx, -2464(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -2480(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%rcx, -2496(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	-2464(%rbp), %xmm0
	movq	-2496(%rbp), %rcx
	movhps	-2480(%rbp), %xmm0
	movq	%rcx, -2240(%rbp)
	movaps	%xmm0, -2256(%rbp)
	pushq	-2240(%rbp)
	pushq	-2248(%rbp)
	pushq	-2256(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$25, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -448(%rbp)
	je	.L320
.L530:
	movq	-2400(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-512(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2160(%rbp)
	movaps	%xmm0, -2176(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2176(%rbp)
	movq	%rdx, -2160(%rbp)
	movq	%rdx, -2168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2176(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L321
	call	_ZdlPv@PLT
.L321:
	movq	(%rbx), %rax
	movl	$46, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	movq	24(%rax), %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal16kBuiltinName_358EPNS0_8compiler18CodeAssemblerStateE
	xorl	%r8d, %r8d
	movl	$100, %edx
	movq	%r14, %rsi
	movq	%rax, %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -256(%rbp)
	je	.L322
.L531:
	movq	-2416(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-320(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2160(%rbp)
	movaps	%xmm0, -2176(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2176(%rbp)
	movq	%rdx, -2160(%rbp)
	movq	%rdx, -2168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2176(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L323
	call	_ZdlPv@PLT
.L323:
	movq	(%rbx), %rax
	movl	$49, %edx
	movq	%r12, %rdi
	leaq	.LC3(%rip), %rsi
	movq	24(%rax), %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal16kBuiltinName_358EPNS0_8compiler18CodeAssemblerStateE
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movl	$50, %edx
	movq	%rax, %rcx
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L533:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movdqa	-2464(%rbp), %xmm5
	movdqa	-2496(%rbp), %xmm3
	movdqa	-2528(%rbp), %xmm4
	movaps	%xmm0, -2256(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movq	$0, -2240(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm4
	leaq	48(%rax), %rdx
	leaq	-1856(%rbp), %rdi
	movq	%rax, -2256(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm4, 32(%rax)
	movq	%rdx, -2240(%rbp)
	movq	%rdx, -2248(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L293
	call	_ZdlPv@PLT
.L293:
	movq	-2440(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L534:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movdqa	-2464(%rbp), %xmm5
	movdqa	-2496(%rbp), %xmm3
	movdqa	-2528(%rbp), %xmm4
	movaps	%xmm0, -2256(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -2240(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm2
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	56(%rax), %rdx
	leaq	-1472(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rax, -2256(%rbp)
	movq	%rdx, -2240(%rbp)
	movq	%rdx, -2248(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L301
	call	_ZdlPv@PLT
.L301:
	movq	-2432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L535:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-2480(%rbp), %xmm5
	movdqa	-2464(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movdqa	-2528(%rbp), %xmm4
	movdqa	-2496(%rbp), %xmm2
	movl	$64, %edi
	movaps	%xmm0, -2256(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm2, -80(%rbp)
	movq	$0, -2240(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm3
	leaq	64(%rax), %rdx
	leaq	-1088(%rbp), %rdi
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rax, -2256(%rbp)
	movq	%rdx, -2240(%rbp)
	movq	%rdx, -2248(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L309
	call	_ZdlPv@PLT
.L309:
	movq	-2448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L308
.L532:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22472:
	.size	_ZN2v88internal35TypedArrayPrototypeForEachAssembler38GenerateTypedArrayPrototypeForEachImplEv, .-_ZN2v88internal35TypedArrayPrototypeForEachAssembler38GenerateTypedArrayPrototypeForEachImplEv
	.section	.rodata._ZN2v88internal8Builtins35Generate_TypedArrayPrototypeForEachEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/typed-array-foreach-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins35Generate_TypedArrayPrototypeForEachEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"TypedArrayPrototypeForEach"
	.section	.text._ZN2v88internal8Builtins35Generate_TypedArrayPrototypeForEachEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins35Generate_TypedArrayPrototypeForEachEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins35Generate_TypedArrayPrototypeForEachEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins35Generate_TypedArrayPrototypeForEachEPNS0_8compiler18CodeAssemblerStateE:
.LFB22468:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$503, %ecx
	leaq	.LC5(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$920, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L540
.L537:
	movq	%r13, %rdi
	call	_ZN2v88internal35TypedArrayPrototypeForEachAssembler38GenerateTypedArrayPrototypeForEachImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L541
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L540:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L537
.L541:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22468:
	.size	_ZN2v88internal8Builtins35Generate_TypedArrayPrototypeForEachEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins35Generate_TypedArrayPrototypeForEachEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal16kBuiltinName_358EPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal16kBuiltinName_358EPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal16kBuiltinName_358EPNS0_8compiler18CodeAssemblerStateE:
.LFB29260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29260:
	.size	_GLOBAL__sub_I__ZN2v88internal16kBuiltinName_358EPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal16kBuiltinName_358EPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal16kBuiltinName_358EPNS0_8compiler18CodeAssemblerStateE
	.weak	_ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE:
	.byte	5
	.byte	5
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
