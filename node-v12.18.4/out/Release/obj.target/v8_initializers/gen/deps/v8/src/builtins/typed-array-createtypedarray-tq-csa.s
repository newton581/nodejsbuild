	.file	"typed-array-createtypedarray-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0:
.LFB31338:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	%rsi, %r13
	pushq	%rbx
	movq	%r13, %rax
	sarq	$3, %rax
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%r13, %r13
	js	.L14
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%rdx, %r14
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.L9
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%rax, %rdi
.L9:
	leaq	(%rdi,%r13), %r15
	movq	%rdi, (%rbx)
	movq	%r15, 16(%rbx)
	cmpq	%r12, %r14
	je	.L10
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
.L10:
	movq	%r15, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE31338:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0:
.LFB31337:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	subq	%rsi, %rdx
	js	.L21
	movq	%rdi, %rbx
	je	.L17
	movq	%rdx, %rdi
	movq	%rdx, %r12
	movq	%rsi, %r13
	call	_Znwm@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	leaq	(%rax,%r12), %r14
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	%r14, 16(%rbx)
	call	memcpy@PLT
	movq	%r14, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	xorl	%r14d, %r14d
	movq	$0, (%rdi)
	movq	$0, 16(%rdi)
	movq	%r14, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE31337:
	.size	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0:
.LFB31336:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	movabsq	$384307168202282325, %rax
	cmpq	%rax, %rdx
	ja	.L39
	pxor	%xmm0, %xmm0
	leaq	(%rdx,%rdx,2), %r14
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	%rdx, %r12
	movups	%xmm0, 8(%rdi)
	salq	$3, %r14
	xorl	%esi, %esi
	movq	$0, 24(%rdi)
	testq	%rdx, %rdx
	je	.L28
	movq	%r14, %rdi
	call	_Znwm@PLT
	leaq	-1(%r12), %rdx
	leaq	(%rax,%r14), %rsi
	movq	%rax, 8(%rbx)
	movq	%rsi, 24(%rbx)
	cmpq	$2, %rdx
	jbe	.L29
	movq	%r12, %rdi
	movq	%r12, %rcx
	pxor	%xmm0, %xmm0
	movq	%rax, %rdx
	shrq	%rdi
	andq	$-2, %rcx
	addq	%rdi, %rcx
	salq	$4, %rcx
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L26:
	movups	%xmm0, (%rdx)
	addq	$48, %rdx
	movups	%xmm0, -32(%rdx)
	movups	%xmm0, -16(%rdx)
	cmpq	%rcx, %rdx
	jne	.L26
	movq	%r12, %rdx
	movq	%r12, %rcx
	andq	$-2, %rdx
	andl	$1, %ecx
	leaq	(%rdx,%rdx,2), %rdi
	leaq	(%rax,%rdi,8), %rax
	cmpq	%rdx, %r12
	je	.L28
.L25:
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	cmpq	$1, %rcx
	je	.L28
	movq	$0, 40(%rax)
	movups	%xmm0, 24(%rax)
	cmpq	$2, %rcx
	je	.L28
	movq	$0, 64(%rax)
	movups	%xmm0, 48(%rax)
.L28:
	movq	%rsi, 16(%rbx)
	pxor	%xmm0, %xmm0
	leaq	56(%rbx), %rdi
	movq	%r13, %rsi
	movups	%xmm0, 32(%rbx)
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	$0, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	%r12, %rcx
	jmp	.L25
.L39:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE31336:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev,"axG",@progbits,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev:
.LFB22413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$56, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L41
	call	_ZdlPv@PLT
.L41:
	movq	16(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	%r12, %r13
	je	.L42
	.p2align 4,,10
	.p2align 3
.L46:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L43
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L46
.L44:
	movq	8(%rbx), %r12
.L42:
	testq	%r12, %r12
	je	.L40
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L46
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L40:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22413:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev
	.set	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.section	.rodata._ZN2v88internal22AllocateTypedArray_341EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEbNS4_INS0_3MapEEENS4_INS0_13JSArrayBufferEEENS4_INS0_8UintPtrTEEESC_SC_.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../../deps/v8/../../deps/v8/src/builtins/typed-array-createtypedarray.tq"
	.section	.text._ZN2v88internal22AllocateTypedArray_341EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEbNS4_INS0_3MapEEENS4_INS0_13JSArrayBufferEEENS4_INS0_8UintPtrTEEESC_SC_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22AllocateTypedArray_341EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEbNS4_INS0_3MapEEENS4_INS0_13JSArrayBufferEEENS4_INS0_8UintPtrTEEESC_SC_
	.type	_ZN2v88internal22AllocateTypedArray_341EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEbNS4_INS0_3MapEEENS4_INS0_13JSArrayBufferEEENS4_INS0_8UintPtrTEEESC_SC_, @function
_ZN2v88internal22AllocateTypedArray_341EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEbNS4_INS0_3MapEEENS4_INS0_13JSArrayBufferEEENS4_INS0_8UintPtrTEEESC_SC_:
.LFB22410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-1280(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1312(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1320(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$1416, %rsp
	movq	%r9, -1408(%rbp)
	movq	%rsi, -1384(%rbp)
	movq	%rcx, -1392(%rbp)
	movq	%r8, -1400(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1320(%rbp)
	movq	%rdi, -1280(%rbp)
	movl	$144, %edi
	movq	$0, -1272(%rbp)
	movq	$0, -1264(%rbp)
	movq	$0, -1256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1272(%rbp)
	leaq	-1224(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1256(%rbp)
	movq	%rdx, -1264(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1240(%rbp)
	movq	%rax, -1336(%rbp)
	movq	$0, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1320(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1080(%rbp)
	movq	$0, -1072(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$0, -1064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1080(%rbp)
	leaq	-1032(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1064(%rbp)
	movq	%rdx, -1072(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1048(%rbp)
	movq	%rax, -1376(%rbp)
	movq	$0, -1056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1320(%rbp), %rax
	movl	$216, %edi
	movq	$0, -888(%rbp)
	movq	$0, -880(%rbp)
	movq	%rax, -896(%rbp)
	movq	$0, -872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -888(%rbp)
	leaq	-840(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -872(%rbp)
	movq	%rdx, -880(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -856(%rbp)
	movq	%rax, -1368(%rbp)
	movq	$0, -864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1320(%rbp), %rax
	movl	$216, %edi
	movq	$0, -696(%rbp)
	movq	$0, -688(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -696(%rbp)
	leaq	-648(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -680(%rbp)
	movq	%rdx, -688(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -664(%rbp)
	movq	%rax, -1352(%rbp)
	movq	$0, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1320(%rbp), %rax
	movl	$168, %edi
	movq	$0, -504(%rbp)
	movq	$0, -496(%rbp)
	movq	%rax, -512(%rbp)
	movq	$0, -488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -504(%rbp)
	leaq	-456(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -488(%rbp)
	movq	%rdx, -496(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -472(%rbp)
	movq	%rax, -1360(%rbp)
	movq	$0, -480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1320(%rbp), %rax
	movl	$168, %edi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -312(%rbp)
	leaq	-264(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	xorl	%edx, %edx
	movq	%rax, -1344(%rbp)
	movups	%xmm0, -280(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	-1400(%rbp), %xmm2
	movq	-1384(%rbp), %xmm3
	movl	$48, %edi
	movaps	%xmm0, -1312(%rbp)
	movhps	24(%rbp), %xmm1
	movhps	-1408(%rbp), %xmm2
	movq	$0, -1296(%rbp)
	movhps	-1392(%rbp), %xmm3
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm1, -96(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	leaq	48(%rax), %rdx
	movq	%rax, -1312(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -1296(%rbp)
	movq	%rdx, -1304(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L53
	call	_ZdlPv@PLT
.L53:
	movq	-1336(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1216(%rbp)
	jne	.L192
.L54:
	cmpq	$0, -1024(%rbp)
	jne	.L193
.L59:
	cmpq	$0, -832(%rbp)
	jne	.L194
.L62:
	cmpq	$0, -640(%rbp)
	jne	.L195
.L65:
	cmpq	$0, -448(%rbp)
	leaq	-320(%rbp), %r14
	jne	.L196
.L68:
	movq	-1344(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -1296(%rbp)
	movaps	%xmm0, -1312(%rbp)
	call	_Znwm@PLT
	movl	$1285, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$84346631, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -1312(%rbp)
	movq	%rdx, -1296(%rbp)
	movq	%rdx, -1304(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1312(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L71
	call	_ZdlPv@PLT
.L71:
	movq	(%rbx), %rax
	movq	-1344(%rbp), %rdi
	movq	48(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L72
	call	_ZdlPv@PLT
.L72:
	movq	-304(%rbp), %rbx
	movq	-312(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L73
	.p2align 4,,10
	.p2align 3
.L77:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L74
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L77
.L75:
	movq	-312(%rbp), %r14
.L73:
	testq	%r14, %r14
	je	.L78
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L78:
	movq	-1360(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L79
	call	_ZdlPv@PLT
.L79:
	movq	-496(%rbp), %rbx
	movq	-504(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L80
	.p2align 4,,10
	.p2align 3
.L84:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L81
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L84
.L82:
	movq	-504(%rbp), %r14
.L80:
	testq	%r14, %r14
	je	.L85
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L85:
	movq	-1352(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L86
	call	_ZdlPv@PLT
.L86:
	movq	-688(%rbp), %rbx
	movq	-696(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L87
	.p2align 4,,10
	.p2align 3
.L91:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L88
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L91
.L89:
	movq	-696(%rbp), %r14
.L87:
	testq	%r14, %r14
	je	.L92
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L92:
	movq	-1368(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-864(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L93
	call	_ZdlPv@PLT
.L93:
	movq	-880(%rbp), %rbx
	movq	-888(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L94
	.p2align 4,,10
	.p2align 3
.L98:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L95
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L98
.L96:
	movq	-888(%rbp), %r14
.L94:
	testq	%r14, %r14
	je	.L99
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L99:
	movq	-1376(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L100
	call	_ZdlPv@PLT
.L100:
	movq	-1072(%rbp), %rbx
	movq	-1080(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L101
	.p2align 4,,10
	.p2align 3
.L105:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L102
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L105
.L103:
	movq	-1080(%rbp), %r14
.L101:
	testq	%r14, %r14
	je	.L106
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L106:
	movq	-1336(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L107
	call	_ZdlPv@PLT
.L107:
	movq	-1264(%rbp), %rbx
	movq	-1272(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L108
	.p2align 4,,10
	.p2align 3
.L112:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L109
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L112
.L110:
	movq	-1272(%rbp), %r14
.L108:
	testq	%r14, %r14
	je	.L113
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L113:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L197
	addq	$1416, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L112
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L102:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L105
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L95:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L98
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L88:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L91
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L74:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L77
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L81:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L84
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L192:
	movq	-1336(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -1296(%rbp)
	movaps	%xmm0, -1312(%rbp)
	call	_Znwm@PLT
	movl	$1285, %edi
	movq	%r13, %rsi
	movw	%di, 4(%rax)
	leaq	6(%rax), %rdx
	movq	%r15, %rdi
	movl	$84346631, (%rax)
	movq	%rax, -1312(%rbp)
	movq	%rdx, -1296(%rbp)
	movq	%rdx, -1304(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1312(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L55
	call	_ZdlPv@PLT
.L55:
	movq	(%r15), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	24(%rax), %rdx
	movq	(%rax), %rcx
	movq	32(%rax), %r15
	movq	%rsi, -1392(%rbp)
	movq	16(%rax), %rsi
	movq	40(%rax), %rax
	movq	%rdx, -1408(%rbp)
	movl	$29, %edx
	movq	%rsi, -1400(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -1384(%rbp)
	movq	%rax, -1416(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$30, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$31, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$32, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-128(%rbp), %rsi
	leaq	-56(%rbp), %rdx
	movq	%r13, %rdi
	movq	-1384(%rbp), %xmm0
	movq	$0, -64(%rbp)
	movq	$0, -1296(%rbp)
	movhps	-1392(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-1400(%rbp), %xmm0
	movhps	-1408(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%r15, %xmm0
	movhps	-1416(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -1312(%rbp)
	testb	%bl, %bl
	je	.L56
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1088(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L57
	call	_ZdlPv@PLT
.L57:
	movq	-1376(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1024(%rbp)
	je	.L59
	.p2align 4,,10
	.p2align 3
.L193:
	movq	-1376(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1088(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -1296(%rbp)
	movaps	%xmm0, -1312(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movabsq	$362263814143936263, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -1312(%rbp)
	movq	%rdx, -1296(%rbp)
	movq	%rdx, -1304(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1312(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L60
	call	_ZdlPv@PLT
.L60:
	movq	(%rbx), %rax
	movl	$33, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	24(%rax), %rbx
	movq	(%rax), %r15
	movq	%rcx, -1384(%rbp)
	movq	16(%rax), %rcx
	movq	%rbx, -1400(%rbp)
	movq	32(%rax), %rbx
	movq	40(%rax), %rax
	movq	%rcx, -1392(%rbp)
	movq	%rax, -1408(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler17AllocateByteArrayENS0_8compiler5TNodeINS0_8UintPtrTEEENS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	%r13, %rdi
	movq	%rax, -1424(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$34, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$35, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$15, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rdi
	movq	%rax, -1416(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$32, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %xmm0
	movl	$72, %edi
	movq	-1424(%rbp), %xmm7
	movhps	-1384(%rbp), %xmm0
	movq	$0, -1296(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	-1392(%rbp), %xmm0
	movq	%xmm7, -64(%rbp)
	movhps	-1400(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%rbx, %xmm0
	movhps	-1408(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm7, %xmm0
	movhps	-1416(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1312(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm7
	movq	-64(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	leaq	72(%rax), %rdx
	leaq	-704(%rbp), %rdi
	movups	%xmm7, (%rax)
	movdqa	-112(%rbp), %xmm7
	movq	%rcx, 64(%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rax, -1312(%rbp)
	movq	%rdx, -1296(%rbp)
	movq	%rdx, -1304(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L61
	call	_ZdlPv@PLT
.L61:
	movq	-1352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -832(%rbp)
	je	.L62
.L194:
	movq	-1368(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-896(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -1296(%rbp)
	movaps	%xmm0, -1312(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movabsq	$362263814143936263, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -1312(%rbp)
	movq	%rdx, -1296(%rbp)
	movq	%rdx, -1304(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1312(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L63
	call	_ZdlPv@PLT
.L63:
	movq	(%rbx), %rax
	movl	$37, %edx
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	movq	16(%rax), %r15
	movq	32(%rax), %rbx
	movq	%rcx, -1392(%rbp)
	movq	24(%rax), %rcx
	movq	40(%rax), %rax
	movq	%rsi, -1400(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -1384(%rbp)
	movq	%rbx, -1408(%rbp)
	movq	%rax, -1416(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal37Convert5ATSmi17ATconstexpr_int31_1406EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$43, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -1424(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-1424(%rbp), %r9
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_7RawPtrTEvE5valueE(%rip), %esi
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -1432(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$44, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1384(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal30Convert8ATintptr9ATuintptr_188EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8UintPtrTEEE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -1424(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1432(%rbp), %r9
	movq	-1424(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, -1424(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$54, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal19kEmptyByteArray_211EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$36, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -1432(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$32, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$72, %edi
	movq	%rbx, -64(%rbp)
	movq	-1392(%rbp), %xmm0
	movq	$0, -1296(%rbp)
	movhps	-1400(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%r15, %xmm0
	movhps	-1384(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-1408(%rbp), %xmm0
	movhps	-1416(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-1432(%rbp), %xmm0
	movhps	-1424(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1312(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-128(%rbp), %xmm6
	leaq	-704(%rbp), %rdi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm4
	leaq	72(%rax), %rdx
	movq	%r13, %rsi
	movdqa	-80(%rbp), %xmm5
	movq	%rcx, 64(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rax, -1312(%rbp)
	movq	%rdx, -1296(%rbp)
	movq	%rdx, -1304(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L64
	call	_ZdlPv@PLT
.L64:
	movq	-1352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -640(%rbp)
	je	.L65
.L195:
	movq	-1352(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-704(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -1296(%rbp)
	movaps	%xmm0, -1312(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movabsq	$362263814143936263, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -1312(%rbp)
	movq	%rdx, -1296(%rbp)
	movq	%rdx, -1304(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1312(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L66
	call	_ZdlPv@PLT
.L66:
	movq	(%rbx), %rax
	movq	24(%rax), %rsi
	movq	32(%rax), %rdx
	movq	40(%rax), %rdi
	movq	56(%rax), %r10
	movq	64(%rax), %r11
	movq	16(%rax), %rcx
	movq	%rsi, -1392(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	48(%rax), %r8
	movq	(%rax), %r15
	movq	%rdx, -1400(%rbp)
	movl	$61, %edx
	movq	8(%rax), %rbx
	movq	%rdi, -1408(%rbp)
	movq	%r12, %rdi
	movq	%r10, -1440(%rbp)
	movq	%r11, -1432(%rbp)
	movq	%rcx, -1384(%rbp)
	movq	%r8, -1448(%rbp)
	movq	%rbx, -1416(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal36AllocateFastOrSlowJSObjectFromMap_57EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3MapEEE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal29UnsafeCast12JSTypedArray_1414EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$62, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -1424(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-1424(%rbp), %r9
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%rbx, %rdx
	movl	$7, %esi
	movq	%r13, %rdi
	movq	-1448(%rbp), %r8
	movq	%rax, %rcx
	movl	$2, %r9d
	call	_ZN2v88internal8compiler13CodeAssembler13StoreToObjectENS0_21MachineRepresentationENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEEPNS1_4NodeENS0_25StoreToObjectWriteBarrierE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$63, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -1424(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-1424(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%rbx, %rdx
	movl	$7, %esi
	movq	%r13, %rdi
	movq	-1384(%rbp), %r8
	movq	%rax, %rcx
	movl	$2, %r9d
	call	_ZN2v88internal8compiler13CodeAssembler13StoreToObjectENS0_21MachineRepresentationENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEEPNS1_4NodeENS0_25StoreToObjectWriteBarrierE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$64, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -1424(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-1424(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	xorl	%r9d, %r9d
	movq	%rbx, %rdx
	movl	$5, %esi
	movq	-1392(%rbp), %r8
	movq	%rax, %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13StoreToObjectENS0_21MachineRepresentationENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEEPNS1_4NodeENS0_25StoreToObjectWriteBarrierE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$65, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -1424(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-1424(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	xorl	%r9d, %r9d
	movq	%rbx, %rdx
	movl	$5, %esi
	movq	-1400(%rbp), %r8
	movq	%rax, %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13StoreToObjectENS0_21MachineRepresentationENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEEPNS1_4NodeENS0_25StoreToObjectWriteBarrierE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$66, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -1424(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-1424(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	xorl	%r9d, %r9d
	movq	%rbx, %rdx
	movl	$5, %esi
	movq	-1408(%rbp), %r8
	movq	%rax, %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13StoreToObjectENS0_21MachineRepresentationENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEEPNS1_4NodeENS0_25StoreToObjectWriteBarrierE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$67, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -1424(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-1424(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	xorl	%r9d, %r9d
	movq	%rbx, %rdx
	movl	$5, %esi
	movq	-1440(%rbp), %r10
	movq	%rax, %rcx
	movq	%r13, %rdi
	movq	%r10, %r8
	call	_ZN2v88internal8compiler13CodeAssembler13StoreToObjectENS0_21MachineRepresentationENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEEPNS1_4NodeENS0_25StoreToObjectWriteBarrierE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$68, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -1424(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-1424(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%rbx, %rdx
	movl	$8, %esi
	movq	%r13, %rdi
	movq	-1432(%rbp), %r11
	movq	%rax, %rcx
	movl	$2, %r9d
	movq	%r11, %r8
	call	_ZN2v88internal8compiler13CodeAssembler13StoreToObjectENS0_21MachineRepresentationENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEEPNS1_4NodeENS0_25StoreToObjectWriteBarrierE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$69, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal27TypedArrayBuiltinsAssembler29SetupTypedArrayEmbedderFieldsENS0_8compiler5TNodeINS0_12JSTypedArrayEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$70, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %xmm0
	movl	$56, %edi
	movq	%rbx, -80(%rbp)
	movhps	-1416(%rbp), %xmm0
	movq	$0, -1296(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	-1384(%rbp), %xmm0
	movhps	-1392(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-1400(%rbp), %xmm0
	movhps	-1408(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1312(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm4
	leaq	56(%rax), %rdx
	leaq	-512(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movq	%rax, -1312(%rbp)
	movq	%rdx, -1296(%rbp)
	movq	%rdx, -1304(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L67
	call	_ZdlPv@PLT
.L67:
	movq	-1360(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L196:
	movq	-1360(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-512(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -1296(%rbp)
	movaps	%xmm0, -1312(%rbp)
	call	_Znwm@PLT
	movl	$1285, %esi
	movq	%r14, %rdi
	movw	%si, 4(%rax)
	leaq	7(%rax), %rdx
	movq	%r13, %rsi
	movl	$84346631, (%rax)
	movb	$7, 6(%rax)
	movq	%rax, -1312(%rbp)
	movq	%rdx, -1296(%rbp)
	movq	%rdx, -1304(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1312(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L69
	call	_ZdlPv@PLT
.L69:
	movq	(%rbx), %rax
	movl	$26, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	40(%rax), %rsi
	movq	(%rax), %r15
	movq	16(%rax), %r14
	movq	%rcx, -1384(%rbp)
	movq	24(%rax), %rcx
	movq	%rsi, -1408(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	48(%rax), %rbx
	movq	%rcx, -1392(%rbp)
	movq	32(%rax), %rcx
	movq	%rcx, -1400(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %xmm0
	movl	$56, %edi
	movq	%rbx, -80(%rbp)
	movhps	-1384(%rbp), %xmm0
	movq	$0, -1296(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	%r14, %xmm0
	leaq	-320(%rbp), %r14
	movhps	-1392(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-1400(%rbp), %xmm0
	movhps	-1408(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1312(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm6
	leaq	56(%rax), %rdx
	movq	%rax, -1312(%rbp)
	movdqa	-96(%rbp), %xmm7
	movq	%rcx, 48(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -1296(%rbp)
	movq	%rdx, -1304(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L70
	call	_ZdlPv@PLT
.L70:
	movq	-1344(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L56:
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-896(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L58
	call	_ZdlPv@PLT
.L58:
	movq	-1368(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L54
.L197:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22410:
	.size	_ZN2v88internal22AllocateTypedArray_341EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEbNS4_INS0_3MapEEENS4_INS0_13JSArrayBufferEEENS4_INS0_8UintPtrTEEESC_SC_, .-_ZN2v88internal22AllocateTypedArray_341EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEbNS4_INS0_3MapEEENS4_INS0_13JSArrayBufferEEENS4_INS0_8UintPtrTEEESC_SC_
	.section	.text._ZN2v88internal23ConstructByIterable_345EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEES8_PNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableINS0_7JSArrayEEEPNSB_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEEPNSB_IS7_EE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23ConstructByIterable_345EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEES8_PNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableINS0_7JSArrayEEEPNSB_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEEPNSB_IS7_EE
	.type	_ZN2v88internal23ConstructByIterable_345EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEES8_PNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableINS0_7JSArrayEEEPNSB_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEEPNSB_IS7_EE, @function
_ZN2v88internal23ConstructByIterable_345EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEES8_PNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableINS0_7JSArrayEEEPNSB_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEEPNSB_IS7_EE:
.LFB22507:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-408(%rbp), %r14
	leaq	-216(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-536(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-464(%rbp), %rbx
	subq	$584, %rsp
	movq	16(%rbp), %rax
	movq	%r9, -584(%rbp)
	movq	%rdi, -568(%rbp)
	movq	%rax, -592(%rbp)
	movq	24(%rbp), %rax
	movq	%rsi, -560(%rbp)
	movq	%rdx, -552(%rbp)
	movq	%r8, -576(%rbp)
	movq	%rax, -600(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -536(%rbp)
	movq	%rdi, -464(%rbp)
	movl	$72, %edi
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	$0, -440(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -440(%rbp)
	movq	%rdx, -448(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -424(%rbp)
	movq	%rax, -456(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-536(%rbp), %rax
	movl	$72, %edi
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	%rax, -272(%rbp)
	movq	$0, -248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -248(%rbp)
	movq	%rdx, -256(%rbp)
	xorl	%edx, %edx
	movq	%rax, -264(%rbp)
	movups	%xmm0, -232(%rbp)
	movq	$0, -240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-560(%rbp), %r11
	pxor	%xmm0, %xmm0
	movq	-552(%rbp), %r10
	movl	$24, %edi
	movq	%r13, -64(%rbp)
	leaq	-496(%rbp), %r13
	movq	%r11, -80(%rbp)
	movq	%r10, -72(%rbp)
	movaps	%xmm0, -496(%rbp)
	movq	$0, -480(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	-80(%rbp), %xmm1
	leaq	24(%rax), %rdx
	movq	%rax, -496(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm1, (%rax)
	movq	%rdx, -480(%rbp)
	movq	%rdx, -488(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-496(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L199
	call	_ZdlPv@PLT
.L199:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -400(%rbp)
	jne	.L245
	cmpq	$0, -208(%rbp)
	jne	.L246
.L203:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L205
	call	_ZdlPv@PLT
.L205:
	movq	-256(%rbp), %rbx
	movq	-264(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L206
	.p2align 4,,10
	.p2align 3
.L210:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L207
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L210
.L208:
	movq	-264(%rbp), %r13
.L206:
	testq	%r13, %r13
	je	.L211
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L211:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L212
	call	_ZdlPv@PLT
.L212:
	movq	-448(%rbp), %rbx
	movq	-456(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L213
	.p2align 4,,10
	.p2align 3
.L217:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L214
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L217
.L215:
	movq	-456(%rbp), %r13
.L213:
	testq	%r13, %r13
	je	.L218
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L218:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L247
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L210
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L214:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L217
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L245:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -480(%rbp)
	movaps	%xmm0, -496(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r9d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -496(%rbp)
	movq	%rdx, -480(%rbp)
	movq	%rdx, -488(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-496(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L201
	call	_ZdlPv@PLT
.L201:
	movq	(%rbx), %rax
	movl	$183, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r9
	movq	8(%rax), %rbx
	movq	16(%rax), %rax
	movq	%r9, -616(%rbp)
	movq	%rax, -552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-528(%rbp), %r10
	movq	-568(%rbp), %rsi
	movq	%r10, %rdi
	movq	%r10, -560(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-560(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$394, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-560(%rbp), %r10
	movq	-496(%rbp), %rsi
	movq	%r10, %rdi
	movq	%r10, -608(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	leaq	-80(%rbp), %rcx
	xorl	%esi, %esi
	movl	$2, %ebx
	movq	%rax, %r8
	movq	-608(%rbp), %r10
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rbx
	movq	-616(%rbp), %r9
	movhps	-552(%rbp), %xmm0
	leaq	-512(%rbp), %rdx
	pushq	%rcx
	movq	%r10, %rdi
	movl	$1, %ecx
	movq	%rax, -512(%rbp)
	movq	-480(%rbp), %rax
	movaps	%xmm0, -80(%rbp)
	movq	%r9, -560(%rbp)
	movq	%r10, -552(%rbp)
	movq	%rax, -504(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-552(%rbp), %r10
	movq	%rax, %rbx
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$182, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$184, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-568(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -552(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-552(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE(%rip), %esi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-560(%rbp), %r9
	movq	-568(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal26GetArrayBufferFunction_220EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEE@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	$0, -480(%rbp)
	movhps	-552(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -496(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm2
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-272(%rbp), %rdi
	movq	%rax, -496(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -480(%rbp)
	movq	%rdx, -488(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-496(%rbp), %rdi
	popq	%rsi
	popq	%r8
	testq	%rdi, %rdi
	je	.L202
	call	_ZdlPv@PLT
.L202:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -208(%rbp)
	je	.L203
.L246:
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	-272(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -480(%rbp)
	movaps	%xmm0, -496(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -496(%rbp)
	movq	%rdx, -480(%rbp)
	movq	%rdx, -488(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-496(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L204
	call	_ZdlPv@PLT
.L204:
	movq	(%rbx), %rax
	movl	$179, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	movq	16(%rax), %r8
	movq	(%rax), %r13
	movq	8(%rax), %rbx
	movq	%r8, -552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-552(%rbp), %r8
	movq	-600(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-592(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-584(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-576(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L203
.L247:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22507:
	.size	_ZN2v88internal23ConstructByIterable_345EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEES8_PNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableINS0_7JSArrayEEEPNSB_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEEPNSB_IS7_EE, .-_ZN2v88internal23ConstructByIterable_345EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEES8_PNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableINS0_7JSArrayEEEPNSB_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEEPNSB_IS7_EE
	.section	.text._ZN2v88internal25ConstructByTypedArray_346EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableIS7_EEPNSB_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEEPNSB_INS0_10JSReceiverEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25ConstructByTypedArray_346EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableIS7_EEPNSB_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEEPNSB_INS0_10JSReceiverEEE
	.type	_ZN2v88internal25ConstructByTypedArray_346EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableIS7_EEPNSB_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEEPNSB_INS0_10JSReceiverEEE, @function
_ZN2v88internal25ConstructByTypedArray_346EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableIS7_EEPNSB_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEEPNSB_INS0_10JSReceiverEEE:
.LFB22514:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-2016(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2048(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-2056(%rbp), %r12
	pushq	%rbx
	subq	$2200, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%r9, -2200(%rbp)
	movq	%rsi, -2160(%rbp)
	movq	%rdx, -2176(%rbp)
	movq	%rcx, -2184(%rbp)
	movq	%r8, -2192(%rbp)
	movq	%rax, -2208(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -2056(%rbp)
	movq	%rdi, -2016(%rbp)
	movl	$48, %edi
	movq	$0, -2008(%rbp)
	movq	$0, -2000(%rbp)
	movq	$0, -1992(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -2008(%rbp)
	leaq	-1960(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1992(%rbp)
	movq	%rdx, -2000(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1976(%rbp)
	movq	%rax, -2072(%rbp)
	movq	$0, -1984(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2056(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1816(%rbp)
	movq	$0, -1808(%rbp)
	movq	%rax, -1824(%rbp)
	movq	$0, -1800(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1816(%rbp)
	leaq	-1768(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1800(%rbp)
	movq	%rdx, -1808(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1784(%rbp)
	movq	%rax, -2104(%rbp)
	movq	$0, -1792(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2056(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1624(%rbp)
	movq	$0, -1616(%rbp)
	movq	%rax, -1632(%rbp)
	movq	$0, -1608(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1624(%rbp)
	leaq	-1576(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1608(%rbp)
	movq	%rdx, -1616(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1592(%rbp)
	movq	%rax, -2112(%rbp)
	movq	$0, -1600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2056(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1432(%rbp)
	movq	$0, -1424(%rbp)
	movq	%rax, -1440(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1432(%rbp)
	leaq	-1384(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -2120(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2056(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -2080(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2056(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -2128(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2056(%rbp), %rax
	movl	$120, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -2136(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2056(%rbp), %rax
	movl	$120, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -2088(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2056(%rbp), %rax
	movl	$120, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -2096(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2056(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -280(%rbp)
	leaq	-232(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -2144(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-2160(%rbp), %xmm1
	movaps	%xmm0, -2048(%rbp)
	movhps	-2176(%rbp), %xmm1
	movq	$0, -2032(%rbp)
	movaps	%xmm1, -2160(%rbp)
	call	_Znwm@PLT
	movdqa	-2160(%rbp), %xmm1
	movq	%r14, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -2048(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -2032(%rbp)
	movq	%rdx, -2040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L249
	call	_ZdlPv@PLT
.L249:
	movq	-2072(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1952(%rbp)
	jne	.L483
	cmpq	$0, -1760(%rbp)
	jne	.L484
.L254:
	cmpq	$0, -1568(%rbp)
	jne	.L485
.L257:
	cmpq	$0, -1376(%rbp)
	jne	.L486
.L260:
	cmpq	$0, -1184(%rbp)
	jne	.L487
.L263:
	cmpq	$0, -992(%rbp)
	jne	.L488
.L267:
	cmpq	$0, -800(%rbp)
	jne	.L489
.L271:
	cmpq	$0, -608(%rbp)
	jne	.L490
.L274:
	cmpq	$0, -416(%rbp)
	jne	.L491
.L277:
	cmpq	$0, -224(%rbp)
	jne	.L492
.L280:
	movq	-2144(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L282
	call	_ZdlPv@PLT
.L282:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L283
	.p2align 4,,10
	.p2align 3
.L287:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L284
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L287
.L285:
	movq	-280(%rbp), %r13
.L283:
	testq	%r13, %r13
	je	.L288
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L288:
	movq	-2096(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L289
	call	_ZdlPv@PLT
.L289:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L290
	.p2align 4,,10
	.p2align 3
.L294:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L291
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L294
.L292:
	movq	-472(%rbp), %r13
.L290:
	testq	%r13, %r13
	je	.L295
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L295:
	movq	-2088(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L296
	call	_ZdlPv@PLT
.L296:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L297
	.p2align 4,,10
	.p2align 3
.L301:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L298
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L301
.L299:
	movq	-664(%rbp), %r13
.L297:
	testq	%r13, %r13
	je	.L302
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L302:
	movq	-2136(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L303
	call	_ZdlPv@PLT
.L303:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L304
	.p2align 4,,10
	.p2align 3
.L308:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L305
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L308
.L306:
	movq	-856(%rbp), %r13
.L304:
	testq	%r13, %r13
	je	.L309
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L309:
	movq	-2128(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L310
	call	_ZdlPv@PLT
.L310:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L311
	.p2align 4,,10
	.p2align 3
.L315:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L312
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L315
.L313:
	movq	-1048(%rbp), %r13
.L311:
	testq	%r13, %r13
	je	.L316
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L316:
	movq	-2080(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L317
	call	_ZdlPv@PLT
.L317:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L318
	.p2align 4,,10
	.p2align 3
.L322:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L319
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L322
.L320:
	movq	-1240(%rbp), %r13
.L318:
	testq	%r13, %r13
	je	.L323
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L323:
	movq	-2120(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L324
	call	_ZdlPv@PLT
.L324:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L325
	.p2align 4,,10
	.p2align 3
.L329:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L326
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L329
.L327:
	movq	-1432(%rbp), %r13
.L325:
	testq	%r13, %r13
	je	.L330
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L330:
	movq	-2112(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L331
	call	_ZdlPv@PLT
.L331:
	movq	-1616(%rbp), %rbx
	movq	-1624(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L332
	.p2align 4,,10
	.p2align 3
.L336:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L333
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L336
.L334:
	movq	-1624(%rbp), %r13
.L332:
	testq	%r13, %r13
	je	.L337
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L337:
	movq	-2104(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L338
	call	_ZdlPv@PLT
.L338:
	movq	-1808(%rbp), %rbx
	movq	-1816(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L339
	.p2align 4,,10
	.p2align 3
.L343:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L340
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L343
.L341:
	movq	-1816(%rbp), %r13
.L339:
	testq	%r13, %r13
	je	.L344
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L344:
	movq	-2072(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L345
	call	_ZdlPv@PLT
.L345:
	movq	-2000(%rbp), %rbx
	movq	-2008(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L346
	.p2align 4,,10
	.p2align 3
.L350:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L347
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L350
.L348:
	movq	-2008(%rbp), %r13
.L346:
	testq	%r13, %r13
	je	.L351
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L351:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L493
	addq	$2200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L350
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L340:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L343
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L333:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L336
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L326:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L329
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L319:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L322
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L312:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L315
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L305:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L308
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L298:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L301
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L284:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L287
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L291:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L294
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L483:
	movq	-2072(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -2032(%rbp)
	movaps	%xmm0, -2048(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movq	%r14, %rdi
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -2048(%rbp)
	movq	%rdx, -2032(%rbp)
	movq	%rdx, -2040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2048(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L251
	call	_ZdlPv@PLT
.L251:
	movq	(%rbx), %rax
	movl	$192, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -2160(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal26GetArrayBufferFunction_220EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEE@PLT
	movl	$193, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -2224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-2160(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	movq	%rax, -2176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$197, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler16IsDetachedBufferENS0_8compiler5TNodeINS0_13JSArrayBufferEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm3
	pxor	%xmm0, %xmm0
	movq	-2224(%rbp), %xmm2
	movhps	-2160(%rbp), %xmm3
	movl	$32, %edi
	movaps	%xmm0, -2048(%rbp)
	movhps	-2176(%rbp), %xmm2
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm2, -2176(%rbp)
	movaps	%xmm3, -2160(%rbp)
	movaps	%xmm2, -80(%rbp)
	movq	$0, -2032(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-1824(%rbp), %rdi
	movq	%rax, -2048(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -2032(%rbp)
	movq	%rdx, -2040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L252
	call	_ZdlPv@PLT
.L252:
	movdqa	-2160(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-2176(%rbp), %xmm7
	movaps	%xmm0, -2048(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	movq	$0, -2032(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-1632(%rbp), %rdi
	movq	%rax, -2048(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -2032(%rbp)
	movq	%rdx, -2040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L253
	call	_ZdlPv@PLT
.L253:
	movq	-2112(%rbp), %rcx
	movq	-2104(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1760(%rbp)
	je	.L254
.L484:
	movq	-2104(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1824(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2032(%rbp)
	movaps	%xmm0, -2048(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2048(%rbp)
	movq	%rdx, -2032(%rbp)
	movq	%rdx, -2040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2048(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L255
	call	_ZdlPv@PLT
.L255:
	movq	(%rbx), %rax
	movl	$32, %edi
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm4
	movq	$0, -2032(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm0, -2048(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-1440(%rbp), %rdi
	movq	%rax, -2048(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -2032(%rbp)
	movq	%rdx, -2040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L256
	call	_ZdlPv@PLT
.L256:
	movq	-2120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1568(%rbp)
	je	.L257
.L485:
	movq	-2112(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1632(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2032(%rbp)
	movaps	%xmm0, -2048(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2048(%rbp)
	movq	%rdx, -2032(%rbp)
	movq	%rdx, -2040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2048(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L258
	call	_ZdlPv@PLT
.L258:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	16(%rax), %rsi
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rsi, -2176(%rbp)
	movl	$48, %esi
	movq	%rcx, -2160(%rbp)
	movq	%rax, -2224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE(%rip), %esi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATuintptr_201EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8UintPtrTEEE@PLT
	movq	%rbx, %xmm7
	movl	$40, %edi
	movq	-2160(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movq	$0, -2032(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-2176(%rbp), %xmm0
	movhps	-2224(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2048(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	leaq	40(%rax), %rdx
	leaq	-1248(%rbp), %rdi
	movq	%rax, -2048(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -2032(%rbp)
	movq	%rdx, -2040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L259
	call	_ZdlPv@PLT
.L259:
	movq	-2080(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1376(%rbp)
	je	.L260
.L486:
	movq	-2120(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1440(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2032(%rbp)
	movaps	%xmm0, -2048(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2048(%rbp)
	movq	%rdx, -2032(%rbp)
	movq	%rdx, -2040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2048(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L261
	call	_ZdlPv@PLT
.L261:
	movq	(%rbx), %rax
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	16(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rcx, -2160(%rbp)
	movq	%rax, -2176(%rbp)
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %xmm0
	movl	$40, %edi
	movq	$0, -2032(%rbp)
	movhps	-2160(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	movhps	-2176(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2048(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	leaq	40(%rax), %rdx
	leaq	-1248(%rbp), %rdi
	movq	%rax, -2048(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -2032(%rbp)
	movq	%rdx, -2040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L262
	call	_ZdlPv@PLT
.L262:
	movq	-2080(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1184(%rbp)
	je	.L263
.L487:
	movq	-2080(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1248(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2032(%rbp)
	movaps	%xmm0, -2048(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2048(%rbp)
	movq	%rdx, -2032(%rbp)
	movq	%rdx, -2040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2048(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L264
	call	_ZdlPv@PLT
.L264:
	movq	(%rbx), %rax
	movl	$202, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	24(%rax), %r14
	movq	32(%rax), %rbx
	movq	%rsi, -2176(%rbp)
	movq	16(%rax), %rsi
	movq	%rcx, -2160(%rbp)
	movq	%rsi, -2224(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%r14, -2240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal27TypedArrayBuiltinsAssembler19IsSharedArrayBufferENS0_8compiler5TNodeINS0_13JSArrayBufferEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15Word32BinaryNotENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	-2224(%rbp), %xmm4
	movq	-2160(%rbp), %xmm5
	movq	%rbx, -64(%rbp)
	movhps	-2240(%rbp), %xmm4
	movaps	%xmm0, -2048(%rbp)
	movhps	-2176(%rbp), %xmm5
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm4, -2224(%rbp)
	movaps	%xmm5, -2160(%rbp)
	movaps	%xmm5, -96(%rbp)
	movq	$0, -2032(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	leaq	40(%rax), %rdx
	leaq	-1056(%rbp), %rdi
	movq	%rax, -2048(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2032(%rbp)
	movq	%rdx, -2040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L265
	call	_ZdlPv@PLT
.L265:
	movdqa	-2160(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movdqa	-2224(%rbp), %xmm3
	movaps	%xmm0, -2048(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movq	%rbx, -64(%rbp)
	movq	$0, -2032(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	leaq	40(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -2048(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2032(%rbp)
	movq	%rdx, -2040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L266
	call	_ZdlPv@PLT
.L266:
	movq	-2096(%rbp), %rcx
	movq	-2128(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -992(%rbp)
	je	.L267
.L488:
	movq	-2128(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2032(%rbp)
	movaps	%xmm0, -2048(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2048(%rbp)
	movq	%rdx, -2032(%rbp)
	movq	%rdx, -2040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2048(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L268
	call	_ZdlPv@PLT
.L268:
	movq	(%rbx), %rax
	movl	$203, %edx
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	movq	24(%rax), %r14
	movq	32(%rax), %rbx
	movq	%rcx, -2160(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -2224(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -2176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2176(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	-2160(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler18SpeciesConstructorENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_10JSReceiverEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -2240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$205, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler16IsDetachedBufferENS0_8compiler5TNodeINS0_13JSArrayBufferEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -2176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %xmm4
	pxor	%xmm0, %xmm0
	movq	-2240(%rbp), %xmm6
	movq	-2160(%rbp), %xmm7
	movl	$40, %edi
	movaps	%xmm0, -2048(%rbp)
	punpcklqdq	%xmm4, %xmm6
	movq	%rbx, -64(%rbp)
	movhps	-2224(%rbp), %xmm7
	movaps	%xmm6, -80(%rbp)
	movaps	%xmm6, -2240(%rbp)
	movaps	%xmm7, -2160(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	$0, -2032(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm2
	leaq	40(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -2048(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -2032(%rbp)
	movq	%rdx, -2040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L269
	call	_ZdlPv@PLT
.L269:
	movdqa	-2160(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movdqa	-2240(%rbp), %xmm6
	movaps	%xmm0, -2048(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm6, -80(%rbp)
	movq	%rbx, -64(%rbp)
	movq	$0, -2032(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm7
	movdqa	-80(%rbp), %xmm4
	leaq	40(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -2048(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2032(%rbp)
	movq	%rdx, -2040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L270
	call	_ZdlPv@PLT
.L270:
	movq	-2088(%rbp), %rcx
	movq	-2136(%rbp), %rdx
	movq	%r12, %rdi
	movq	-2176(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -800(%rbp)
	je	.L271
.L489:
	movq	-2136(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2032(%rbp)
	movaps	%xmm0, -2048(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2048(%rbp)
	movq	%rdx, -2032(%rbp)
	movq	%rdx, -2040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2048(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L272
	call	_ZdlPv@PLT
.L272:
	movq	(%rbx), %rax
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	16(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rcx, -2160(%rbp)
	movq	%rax, -2176(%rbp)
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %xmm0
	movl	$40, %edi
	movq	$0, -2032(%rbp)
	movhps	-2160(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	movhps	-2176(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2048(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm2
	leaq	40(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -2048(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -2032(%rbp)
	movq	%rdx, -2040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L273
	call	_ZdlPv@PLT
.L273:
	movq	-2088(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -608(%rbp)
	je	.L274
.L490:
	movq	-2088(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2032(%rbp)
	movaps	%xmm0, -2048(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2048(%rbp)
	movq	%rdx, -2032(%rbp)
	movq	%rdx, -2040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2048(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L275
	call	_ZdlPv@PLT
.L275:
	movq	(%rbx), %rax
	movl	$202, %edx
	movq	%r12, %rdi
	movq	24(%rax), %rsi
	movq	16(%rax), %rcx
	movq	(%rax), %r14
	movq	8(%rax), %r15
	movq	%rsi, -2176(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	32(%rax), %rbx
	movq	%rcx, -2160(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %xmm3
	movq	%r14, %xmm0
	movl	$40, %edi
	punpcklqdq	%xmm3, %xmm0
	movq	%rbx, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	-2160(%rbp), %xmm0
	movq	$0, -2032(%rbp)
	movhps	-2176(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2048(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	leaq	40(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -2048(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2032(%rbp)
	movq	%rdx, -2040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L276
	call	_ZdlPv@PLT
.L276:
	movq	-2096(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -416(%rbp)
	je	.L277
.L491:
	movq	-2096(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2032(%rbp)
	movaps	%xmm0, -2048(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2048(%rbp)
	movq	%rdx, -2032(%rbp)
	movq	%rdx, -2040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2048(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L278
	call	_ZdlPv@PLT
.L278:
	movq	(%rbx), %rax
	movl	$207, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rbx
	movq	32(%rax), %r15
	movq	16(%rax), %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %xmm4
	movq	%rbx, %xmm0
	movl	$24, %edi
	punpcklqdq	%xmm4, %xmm0
	movq	%r14, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2048(%rbp)
	movq	$0, -2032(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-288(%rbp), %rdi
	movq	%rax, -2048(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -2032(%rbp)
	movq	%rdx, -2040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L279
	call	_ZdlPv@PLT
.L279:
	movq	-2144(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -224(%rbp)
	je	.L280
.L492:
	movq	-2144(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-288(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2032(%rbp)
	movaps	%xmm0, -2048(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -2048(%rbp)
	movq	%rdx, -2032(%rbp)
	movq	%rdx, -2040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2048(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L281
	call	_ZdlPv@PLT
.L281:
	movq	(%rbx), %rax
	movl	$189, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	movq	16(%rax), %r15
	movq	(%rax), %r13
	movq	8(%rax), %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2208(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-2200(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-2192(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-2184(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L280
.L493:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22514:
	.size	_ZN2v88internal25ConstructByTypedArray_346EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableIS7_EEPNSB_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEEPNSB_INS0_10JSReceiverEEE, .-_ZN2v88internal25ConstructByTypedArray_346EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableIS7_EEPNSB_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEEPNSB_INS0_10JSReceiverEEE
	.section	.text._ZN2v88internal25ConstructByJSReceiver_348EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableIS7_EEPNSB_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEESD_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25ConstructByJSReceiver_348EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableIS7_EEPNSB_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEESD_
	.type	_ZN2v88internal25ConstructByJSReceiver_348EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableIS7_EEPNSB_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEESD_, @function
_ZN2v88internal25ConstructByJSReceiver_348EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableIS7_EEPNSB_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEESD_:
.LFB22588:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-3008(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-3136(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-3224(%rbp), %r12
	pushq	%rbx
	subq	$3448, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%r9, -3408(%rbp)
	movq	%rsi, -3376(%rbp)
	movq	%rdx, -3392(%rbp)
	movq	%rcx, -3360(%rbp)
	movq	%r8, -3400(%rbp)
	movq	%rax, -3416(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -3224(%rbp)
	movq	%rdi, -3008(%rbp)
	movl	$48, %edi
	movq	$0, -3000(%rbp)
	movq	$0, -2992(%rbp)
	movq	$0, -2984(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -3000(%rbp)
	leaq	-2952(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2984(%rbp)
	movq	%rdx, -2992(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2968(%rbp)
	movq	%rax, -3240(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3224(%rbp), %rax
	movl	$72, %edi
	movq	$0, -2808(%rbp)
	movq	$0, -2800(%rbp)
	movq	%rax, -2816(%rbp)
	movq	$0, -2792(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -2808(%rbp)
	leaq	-2760(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2792(%rbp)
	movq	%rdx, -2800(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2776(%rbp)
	movq	%rax, -3328(%rbp)
	movq	$0, -2784(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3224(%rbp), %rax
	movl	$96, %edi
	movq	$0, -2616(%rbp)
	movq	$0, -2608(%rbp)
	movq	%rax, -2624(%rbp)
	movq	$0, -2600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -2616(%rbp)
	leaq	-2568(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2600(%rbp)
	movq	%rdx, -2608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2584(%rbp)
	movq	%rax, -3264(%rbp)
	movq	$0, -2592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3224(%rbp), %rax
	movl	$96, %edi
	movq	$0, -2424(%rbp)
	movq	$0, -2416(%rbp)
	movq	%rax, -2432(%rbp)
	movq	$0, -2408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -2424(%rbp)
	leaq	-2376(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2408(%rbp)
	movq	%rdx, -2416(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2392(%rbp)
	movq	%rax, -3336(%rbp)
	movq	$0, -2400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3224(%rbp), %rax
	movl	$120, %edi
	movq	$0, -2232(%rbp)
	movq	$0, -2224(%rbp)
	movq	%rax, -2240(%rbp)
	movq	$0, -2216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -2232(%rbp)
	leaq	-2184(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2216(%rbp)
	movq	%rdx, -2224(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2200(%rbp)
	movq	%rax, -3256(%rbp)
	movq	$0, -2208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3224(%rbp), %rax
	movl	$72, %edi
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movq	%rax, -2048(%rbp)
	movq	$0, -2024(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -2040(%rbp)
	leaq	-1992(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2024(%rbp)
	movq	%rdx, -2032(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2008(%rbp)
	movq	%rax, -3280(%rbp)
	movq	$0, -2016(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3224(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1848(%rbp)
	movq	$0, -1840(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1832(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1848(%rbp)
	leaq	-1800(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1832(%rbp)
	movq	%rdx, -1840(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1816(%rbp)
	movq	%rax, -3288(%rbp)
	movq	$0, -1824(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3224(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1656(%rbp)
	movq	$0, -1648(%rbp)
	movq	%rax, -1664(%rbp)
	movq	$0, -1640(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1656(%rbp)
	leaq	-1608(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1640(%rbp)
	movq	%rdx, -1648(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1624(%rbp)
	movq	%rax, -3352(%rbp)
	movq	$0, -1632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3224(%rbp), %rax
	movl	$48, %edi
	movq	$0, -1464(%rbp)
	movq	$0, -1456(%rbp)
	movq	%rax, -1472(%rbp)
	movq	$0, -1448(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1464(%rbp)
	leaq	-1416(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1448(%rbp)
	movq	%rdx, -1456(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1432(%rbp)
	movq	%rax, -3272(%rbp)
	movq	$0, -1440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3224(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1272(%rbp)
	movq	$0, -1264(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -1256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1272(%rbp)
	leaq	-1224(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1256(%rbp)
	movq	%rdx, -1264(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1240(%rbp)
	movq	%rax, -3344(%rbp)
	movq	$0, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3224(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1080(%rbp)
	movq	$0, -1072(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$0, -1064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1080(%rbp)
	leaq	-1032(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1064(%rbp)
	movq	%rdx, -1072(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1048(%rbp)
	movq	%rax, -3312(%rbp)
	movq	$0, -1056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3224(%rbp), %rax
	movl	$72, %edi
	movq	$0, -888(%rbp)
	movq	$0, -880(%rbp)
	movq	%rax, -896(%rbp)
	movq	$0, -872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -888(%rbp)
	leaq	-840(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -872(%rbp)
	movq	%rdx, -880(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -856(%rbp)
	movq	%rax, -3296(%rbp)
	movq	$0, -864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3224(%rbp), %rax
	movl	$96, %edi
	movq	$0, -696(%rbp)
	movq	$0, -688(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -696(%rbp)
	leaq	-648(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -680(%rbp)
	movq	%rdx, -688(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -664(%rbp)
	movq	%rax, -3304(%rbp)
	movq	$0, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3224(%rbp), %rax
	movl	$72, %edi
	movq	$0, -504(%rbp)
	movq	$0, -496(%rbp)
	movq	%rax, -512(%rbp)
	movq	$0, -488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -504(%rbp)
	leaq	-456(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -488(%rbp)
	movq	%rdx, -496(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -472(%rbp)
	movq	%rax, -3320(%rbp)
	movq	$0, -480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3224(%rbp), %rax
	movl	$72, %edi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -312(%rbp)
	leaq	-264(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	xorl	%edx, %edx
	movq	%rax, -3248(%rbp)
	movups	%xmm0, -280(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-3376(%rbp), %xmm1
	movaps	%xmm0, -3136(%rbp)
	movhps	-3392(%rbp), %xmm1
	movq	$0, -3120(%rbp)
	movaps	%xmm1, -3376(%rbp)
	call	_Znwm@PLT
	movdqa	-3376(%rbp), %xmm1
	movq	%r14, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -3136(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L495
	call	_ZdlPv@PLT
.L495:
	movq	-3240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2944(%rbp)
	jne	.L838
	cmpq	$0, -2752(%rbp)
	jne	.L839
.L501:
	cmpq	$0, -2560(%rbp)
	jne	.L840
.L504:
	cmpq	$0, -2368(%rbp)
	jne	.L841
.L509:
	cmpq	$0, -2176(%rbp)
	jne	.L842
.L512:
	cmpq	$0, -1984(%rbp)
	jne	.L843
.L515:
	cmpq	$0, -1792(%rbp)
	jne	.L844
.L517:
	cmpq	$0, -1600(%rbp)
	jne	.L845
.L521:
	cmpq	$0, -1408(%rbp)
	jne	.L846
.L524:
	cmpq	$0, -1216(%rbp)
	jne	.L847
.L529:
	cmpq	$0, -1024(%rbp)
	jne	.L848
.L532:
	cmpq	$0, -832(%rbp)
	jne	.L849
.L535:
	cmpq	$0, -640(%rbp)
	jne	.L850
.L538:
	cmpq	$0, -448(%rbp)
	jne	.L851
.L542:
	cmpq	$0, -256(%rbp)
	jne	.L852
.L544:
	movq	-3248(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L546
	call	_ZdlPv@PLT
.L546:
	movq	-304(%rbp), %rbx
	movq	-312(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L547
	.p2align 4,,10
	.p2align 3
.L551:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L548
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L551
.L549:
	movq	-312(%rbp), %r13
.L547:
	testq	%r13, %r13
	je	.L552
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L552:
	movq	-3320(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L553
	call	_ZdlPv@PLT
.L553:
	movq	-496(%rbp), %rbx
	movq	-504(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L554
	.p2align 4,,10
	.p2align 3
.L558:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L555
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L558
.L556:
	movq	-504(%rbp), %r13
.L554:
	testq	%r13, %r13
	je	.L559
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L559:
	movq	-3304(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L560
	call	_ZdlPv@PLT
.L560:
	movq	-688(%rbp), %rbx
	movq	-696(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L561
	.p2align 4,,10
	.p2align 3
.L565:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L562
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L565
.L563:
	movq	-696(%rbp), %r13
.L561:
	testq	%r13, %r13
	je	.L566
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L566:
	movq	-3296(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-864(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L567
	call	_ZdlPv@PLT
.L567:
	movq	-880(%rbp), %rbx
	movq	-888(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L568
	.p2align 4,,10
	.p2align 3
.L572:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L569
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L572
.L570:
	movq	-888(%rbp), %r13
.L568:
	testq	%r13, %r13
	je	.L573
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L573:
	movq	-3312(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L574
	call	_ZdlPv@PLT
.L574:
	movq	-1072(%rbp), %rbx
	movq	-1080(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L575
	.p2align 4,,10
	.p2align 3
.L579:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L576
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L579
.L577:
	movq	-1080(%rbp), %r13
.L575:
	testq	%r13, %r13
	je	.L580
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L580:
	movq	-3344(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L581
	call	_ZdlPv@PLT
.L581:
	movq	-1264(%rbp), %rbx
	movq	-1272(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L582
	.p2align 4,,10
	.p2align 3
.L586:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L583
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L586
.L584:
	movq	-1272(%rbp), %r13
.L582:
	testq	%r13, %r13
	je	.L587
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L587:
	movq	-3272(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1440(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L588
	call	_ZdlPv@PLT
.L588:
	movq	-1456(%rbp), %rbx
	movq	-1464(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L589
	.p2align 4,,10
	.p2align 3
.L593:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L590
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L593
.L591:
	movq	-1464(%rbp), %r13
.L589:
	testq	%r13, %r13
	je	.L594
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L594:
	movq	-3352(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1632(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L595
	call	_ZdlPv@PLT
.L595:
	movq	-1648(%rbp), %rbx
	movq	-1656(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L596
	.p2align 4,,10
	.p2align 3
.L600:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L597
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L600
.L598:
	movq	-1656(%rbp), %r13
.L596:
	testq	%r13, %r13
	je	.L601
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L601:
	movq	-3288(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L602
	call	_ZdlPv@PLT
.L602:
	movq	-1840(%rbp), %rbx
	movq	-1848(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L603
	.p2align 4,,10
	.p2align 3
.L607:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L604
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L607
.L605:
	movq	-1848(%rbp), %r13
.L603:
	testq	%r13, %r13
	je	.L608
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L608:
	movq	-3280(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2016(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L609
	call	_ZdlPv@PLT
.L609:
	movq	-2032(%rbp), %rbx
	movq	-2040(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L610
	.p2align 4,,10
	.p2align 3
.L614:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L611
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L614
.L612:
	movq	-2040(%rbp), %r13
.L610:
	testq	%r13, %r13
	je	.L615
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L615:
	movq	-3256(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L616
	call	_ZdlPv@PLT
.L616:
	movq	-2224(%rbp), %rbx
	movq	-2232(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L617
	.p2align 4,,10
	.p2align 3
.L621:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L618
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L621
.L619:
	movq	-2232(%rbp), %r13
.L617:
	testq	%r13, %r13
	je	.L622
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L622:
	movq	-3336(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L623
	call	_ZdlPv@PLT
.L623:
	movq	-2416(%rbp), %rbx
	movq	-2424(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L624
	.p2align 4,,10
	.p2align 3
.L628:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L625
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L628
.L626:
	movq	-2424(%rbp), %r13
.L624:
	testq	%r13, %r13
	je	.L629
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L629:
	movq	-3264(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L630
	call	_ZdlPv@PLT
.L630:
	movq	-2608(%rbp), %rbx
	movq	-2616(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L631
	.p2align 4,,10
	.p2align 3
.L635:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L632
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L635
.L633:
	movq	-2616(%rbp), %r13
.L631:
	testq	%r13, %r13
	je	.L636
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L636:
	movq	-3328(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L637
	call	_ZdlPv@PLT
.L637:
	movq	-2800(%rbp), %rbx
	movq	-2808(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L638
	.p2align 4,,10
	.p2align 3
.L642:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L639
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L642
.L640:
	movq	-2808(%rbp), %r13
.L638:
	testq	%r13, %r13
	je	.L643
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L643:
	movq	-3240(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2976(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L644
	call	_ZdlPv@PLT
.L644:
	movq	-2992(%rbp), %rbx
	movq	-3000(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L645
	.p2align 4,,10
	.p2align 3
.L649:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L646
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L649
.L647:
	movq	-3000(%rbp), %r13
.L645:
	testq	%r13, %r13
	je	.L650
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L650:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L853
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L646:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L649
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L639:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L642
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L632:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L635
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L625:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L628
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L618:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L621
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L611:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L614
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L604:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L607
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L597:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L600
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L590:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L593
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L583:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L586
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L576:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L579
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L569:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L572
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L562:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L565
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L548:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L551
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L555:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L558
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L838:
	movq	-3240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movl	$1799, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	2(%rax), %rdx
	movq	%r14, %rdi
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3136(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L497
	call	_ZdlPv@PLT
.L497:
	movq	(%rbx), %rax
	movl	$300, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	leaq	-3168(%rbp), %r14
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -3376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rcx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-3376(%rbp), %rdx
	call	_ZN2v88internal17CodeStubAssembler17GetIteratorMethodENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10HeapObjectEEEPNS2_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	movq	%rax, -3392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm3
	movl	$32, %edi
	movq	-3376(%rbp), %rax
	movq	$0, -3152(%rbp)
	movq	%rax, %xmm0
	movq	%rax, %xmm4
	movhps	-3392(%rbp), %xmm0
	punpcklqdq	%xmm4, %xmm3
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -3440(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm0, -3168(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm2
	leaq	-2624(%rbp), %rdi
	movq	%r14, %rsi
	leaq	32(%rax), %rdx
	movq	%rax, -3168(%rbp)
	movups	%xmm2, (%rax)
	movdqa	-112(%rbp), %xmm2
	movq	%rdx, -3152(%rbp)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -3160(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L498
	call	_ZdlPv@PLT
.L498:
	movq	-3264(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3128(%rbp)
	jne	.L854
.L499:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2752(%rbp)
	je	.L501
.L839:
	movq	-3328(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2816(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movq	%r14, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$7, 2(%rax)
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3136(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L502
	call	_ZdlPv@PLT
.L502:
	movq	(%rbx), %rax
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movdqu	(%rax), %xmm0
	movaps	%xmm1, -3136(%rbp)
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -3376(%rbp)
	call	_Znwm@PLT
	movdqa	-3376(%rbp), %xmm0
	leaq	-1472(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -3136(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L503
	call	_ZdlPv@PLT
.L503:
	movq	-3272(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2560(%rbp)
	je	.L504
.L840:
	movq	-3264(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2624(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134678279, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3136(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L505
	call	_ZdlPv@PLT
.L505:
	movq	(%rbx), %rax
	movl	$299, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	24(%rax), %rbx
	movq	%rcx, -3376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$301, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal82Cast71UT19ATCallableApiObject17ATCallableJSProxy15JSBoundFunction10JSFunction_1405EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm5
	movq	%rbx, %xmm4
	pxor	%xmm0, %xmm0
	punpcklqdq	%xmm4, %xmm4
	movl	$40, %edi
	movq	%rax, -96(%rbp)
	movhps	-3376(%rbp), %xmm5
	movaps	%xmm4, -3392(%rbp)
	leaq	-3168(%rbp), %r14
	movaps	%xmm5, -3376(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm0, -3168(%rbp)
	movq	$0, -3152(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm2
	movq	-96(%rbp), %rcx
	movq	%r14, %rsi
	leaq	40(%rax), %rdx
	leaq	-2240(%rbp), %rdi
	movq	%rax, -3168(%rbp)
	movups	%xmm2, (%rax)
	movdqa	-112(%rbp), %xmm2
	movq	%rcx, 32(%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -3152(%rbp)
	movq	%rdx, -3160(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L506
	call	_ZdlPv@PLT
.L506:
	movq	-3256(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3128(%rbp)
	jne	.L855
.L507:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2368(%rbp)
	je	.L509
.L841:
	movq	-3336(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2432(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134743815, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3136(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L510
	call	_ZdlPv@PLT
.L510:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm2
	movaps	%xmm0, -3136(%rbp)
	movq	$0, -3120(%rbp)
	movq	%rdx, -112(%rbp)
	movaps	%xmm2, -128(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movdqa	-128(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-2048(%rbp), %rdi
	movq	%rax, -3136(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L511
	call	_ZdlPv@PLT
.L511:
	movq	-3280(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2176(%rbp)
	je	.L512
.L842:
	movq	-3256(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2240(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134743815, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3136(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L513
	call	_ZdlPv@PLT
.L513:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	8(%rax), %rsi
	movq	16(%rax), %rcx
	movq	32(%rax), %rdx
	movq	(%rax), %rax
	movaps	%xmm0, -3136(%rbp)
	movq	%rsi, -120(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -3120(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm3
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-1856(%rbp), %rdi
	movq	%rax, -3136(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L514
	call	_ZdlPv@PLT
.L514:
	movq	-3288(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1984(%rbp)
	je	.L515
.L843:
	movq	-3280(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2048(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3136(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L516
	call	_ZdlPv@PLT
.L516:
	movq	(%rbx), %rax
	movl	$302, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$69, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -1792(%rbp)
	je	.L517
.L844:
	movq	-3288(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1856(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117966599, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3136(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L518
	call	_ZdlPv@PLT
.L518:
	movq	(%rbx), %rax
	movl	$301, %edx
	leaq	.LC2(%rip), %rsi
	leaq	-3216(%rbp), %r14
	movq	16(%rax), %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rdi, -3472(%rbp)
	movq	%r12, %rdi
	movq	%rcx, -3440(%rbp)
	movq	%rax, -3392(%rbp)
	movq	%rbx, -3456(%rbp)
	leaq	-3184(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$303, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$7, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-3200(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3376(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$7, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pushq	%rbx
	movq	-3392(%rbp), %rcx
	movq	%r14, %r9
	pushq	-3376(%rbp)
	movq	-3456(%rbp), %rdx
	movq	%r13, %r8
	movq	%r15, %rdi
	movq	-3440(%rbp), %rsi
	call	_ZN2v88internal23ConstructByIterable_345EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEES8_PNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableINS0_7JSArrayEEEPNSB_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEEPNSB_IS7_EE
	cmpq	$0, -3128(%rbp)
	popq	%r11
	popq	%rax
	jne	.L856
.L519:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-3376(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	cmpq	$0, -1600(%rbp)
	je	.L521
.L845:
	movq	-3352(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1664(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$578438803904530183, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3136(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L522
	call	_ZdlPv@PLT
.L522:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	64(%rax), %rdx
	movdqu	48(%rax), %xmm4
	movaps	%xmm0, -3136(%rbp)
	movq	$0, -3120(%rbp)
	movq	%rdx, -112(%rbp)
	movaps	%xmm4, -128(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movdqa	-128(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-320(%rbp), %rdi
	movq	%rax, -3136(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L523
	call	_ZdlPv@PLT
.L523:
	movq	-3248(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1408(%rbp)
	je	.L524
.L846:
	movq	-3272(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1472(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r10d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%r10w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3136(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L525
	call	_ZdlPv@PLT
.L525:
	movq	(%rbx), %rax
	movl	$307, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	leaq	-3168(%rbp), %r14
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	%rcx, -3376(%rbp)
	movq	%rbx, -3392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal16kLengthString_68EPNS0_8compiler18CodeAssemblerStateE@PLT
	leaq	-3184(%rbp), %r10
	movq	%r15, %rsi
	movq	%r10, %rdi
	movq	%rax, -3440(%rbp)
	movq	%r10, -3456(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3456(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-3456(%rbp), %r10
	movq	-3136(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$2, %ebx
	movq	-3456(%rbp), %r10
	pushq	%rbx
	movq	-3376(%rbp), %r9
	movhps	-3440(%rbp), %xmm0
	movl	$1, %ecx
	movq	%rax, -3168(%rbp)
	movq	-3120(%rbp), %rax
	movq	%r10, %rdi
	movaps	%xmm0, -128(%rbp)
	movq	%rax, -3160(%rbp)
	leaq	-128(%rbp), %rax
	pushq	%rax
	movq	%r10, -3440(%rbp)
	movq	%rax, -3472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-3440(%rbp), %r10
	movq	%rax, %rbx
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$308, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	-3376(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler11ToSmiLengthENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEEPNS2_18CodeAssemblerLabelE@PLT
	movq	%r14, %rdi
	movq	%rax, -3440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3440(%rbp), %rax
	movq	%rbx, %xmm6
	movq	-3376(%rbp), %xmm7
	punpcklqdq	%xmm6, %xmm6
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	$0, -3152(%rbp)
	movhps	-3392(%rbp), %xmm7
	movaps	%xmm6, -3456(%rbp)
	movaps	%xmm7, -3376(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm0, -3168(%rbp)
	movq	%rax, -96(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-128(%rbp), %xmm2
	movdqa	-112(%rbp), %xmm6
	leaq	40(%rax), %rdx
	leaq	-1088(%rbp), %rdi
	movq	%rax, -3168(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -3152(%rbp)
	movq	%rdx, -3160(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3168(%rbp), %rdi
	popq	%r8
	popq	%r9
	testq	%rdi, %rdi
	je	.L526
	call	_ZdlPv@PLT
.L526:
	movq	-3312(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3128(%rbp)
	jne	.L857
.L527:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1216(%rbp)
	je	.L529
.L847:
	movq	-3344(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1280(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134743815, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3136(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L530
	call	_ZdlPv@PLT
.L530:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm7
	movaps	%xmm0, -3136(%rbp)
	movq	$0, -3120(%rbp)
	movq	%rdx, -112(%rbp)
	movaps	%xmm7, -128(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-896(%rbp), %rdi
	movq	%rax, -3136(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L531
	call	_ZdlPv@PLT
.L531:
	movq	-3296(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1024(%rbp)
	je	.L532
.L848:
	movq	-3312(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1088(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134743815, (%rax)
	leaq	5(%rax), %rdx
	movb	$6, 4(%rax)
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3136(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L533
	call	_ZdlPv@PLT
.L533:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	8(%rax), %rsi
	movq	16(%rax), %rcx
	movq	32(%rax), %rdx
	movq	(%rax), %rax
	movaps	%xmm0, -3136(%rbp)
	movq	%rsi, -120(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -3120(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm3
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-704(%rbp), %rdi
	movq	%rax, -3136(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L534
	call	_ZdlPv@PLT
.L534:
	movq	-3304(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -832(%rbp)
	je	.L535
.L849:
	movq	-3296(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-896(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movl	$1799, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r14, %rdi
	movb	$8, 2(%rax)
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3136(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L536
	call	_ZdlPv@PLT
.L536:
	movq	(%rbx), %rax
	movl	$309, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r14
	movq	%rcx, -3376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r14, -112(%rbp)
	movhps	-3376(%rbp), %xmm0
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movdqa	-128(%rbp), %xmm2
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-512(%rbp), %rdi
	movq	%rax, -3136(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L537
	call	_ZdlPv@PLT
.L537:
	movq	-3320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -640(%rbp)
	je	.L538
.L850:
	movq	-3304(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-704(%rbp), %rbx
	xorl	%r14d, %r14d
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$101189383, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3136(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L539
	call	_ZdlPv@PLT
.L539:
	movq	(%rbx), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rdx
	movq	8(%rax), %rbx
	movq	24(%rax), %rax
	testq	%rdx, %rdx
	cmovne	%rdx, %r14
	movl	$308, %edx
	movq	%rax, -3376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$310, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal26GetArrayBufferFunction_220EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEE@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	$0, -3120(%rbp)
	movhps	-3376(%rbp), %xmm0
	movq	%rax, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movdqa	-128(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-320(%rbp), %rdi
	movq	%rax, -3136(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L541
	call	_ZdlPv@PLT
.L541:
	movq	-3248(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -448(%rbp)
	je	.L542
.L851:
	movq	-3320(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-512(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movq	%r14, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$8, 2(%rax)
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3136(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L543
	call	_ZdlPv@PLT
.L543:
	movq	(%rbx), %rax
	movl	$313, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r14
	movq	16(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$206, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ThrowRangeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -256(%rbp)
	je	.L544
.L852:
	movq	-3248(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-320(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3136(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L545
	call	_ZdlPv@PLT
.L545:
	movq	(%rbx), %rax
	movl	$295, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	movq	16(%rax), %r15
	movq	(%rax), %r13
	movq	8(%rax), %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3416(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3408(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3400(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-3360(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L854:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %rsi
	movq	%r14, %rdi
	movdqa	-3440(%rbp), %xmm7
	movq	-3376(%rbp), %rax
	leaq	-104(%rbp), %rdx
	movaps	%xmm0, -3168(%rbp)
	movq	$0, -3152(%rbp)
	movq	%rax, -112(%rbp)
	movaps	%xmm7, -128(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2816(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L500
	call	_ZdlPv@PLT
.L500:
	movq	-3328(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L855:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %rsi
	movq	%r14, %rdi
	movdqa	-3376(%rbp), %xmm4
	movdqa	-3392(%rbp), %xmm5
	leaq	-96(%rbp), %rdx
	movaps	%xmm0, -3168(%rbp)
	movq	$0, -3152(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2432(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L508
	call	_ZdlPv@PLT
.L508:
	movq	-3336(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L856:
	movq	-3456(%rbp), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-3472(%rbp), %xmm2
	movq	-3440(%rbp), %xmm1
	movq	%rax, %xmm5
	movq	%rax, %xmm0
	movhps	-3392(%rbp), %xmm2
	punpcklqdq	%xmm5, %xmm1
	movhps	-3392(%rbp), %xmm0
	movaps	%xmm2, -3472(%rbp)
	movaps	%xmm1, -3488(%rbp)
	movaps	%xmm0, -3456(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-3376(%rbp), %rdi
	movq	%rax, -3440(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r14, %rdi
	movq	%rax, -3392(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-3440(%rbp), %rdx
	movdqa	-3456(%rbp), %xmm0
	leaq	-3168(%rbp), %r8
	movdqa	-3488(%rbp), %xmm1
	movdqa	-3472(%rbp), %xmm2
	movq	%r8, %rdi
	leaq	-128(%rbp), %rsi
	movaps	%xmm0, -96(%rbp)
	movq	%rax, %xmm0
	movhps	-3392(%rbp), %xmm0
	movq	%rdx, -64(%rbp)
	leaq	-56(%rbp), %rdx
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movq	%r8, -3392(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm0, -3168(%rbp)
	movq	$0, -3152(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3392(%rbp), %r8
	leaq	-1664(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L520
	call	_ZdlPv@PLT
.L520:
	movq	-3352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L857:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-96(%rbp), %rdx
	movq	%r14, %rdi
	movdqa	-3376(%rbp), %xmm3
	movdqa	-3456(%rbp), %xmm7
	movaps	%xmm0, -3168(%rbp)
	movq	-3472(%rbp), %rsi
	movq	$0, -3152(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1280(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L528
	call	_ZdlPv@PLT
.L528:
	movq	-3344(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L527
.L853:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22588:
	.size	_ZN2v88internal25ConstructByJSReceiver_348EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableIS7_EEPNSB_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEESD_, .-_ZN2v88internal25ConstructByJSReceiver_348EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableIS7_EEPNSB_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEESD_
	.section	.rodata._ZN2v88internal24Cast13JSArrayBuffer_1473EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"../../deps/v8/../../deps/v8/src/builtins/base.tq"
	.section	.text._ZN2v88internal24Cast13JSArrayBuffer_1473EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24Cast13JSArrayBuffer_1473EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal24Cast13JSArrayBuffer_1473EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal24Cast13JSArrayBuffer_1473EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE:
.LFB22707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	leaq	-1632(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1760(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1800(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1864, %rsp
	movq	%rsi, -1888(%rbp)
	movq	%rdx, -1904(%rbp)
	movq	%rcx, -1872(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1800(%rbp)
	movq	%rdi, -1632(%rbp)
	movl	$48, %edi
	movq	$0, -1624(%rbp)
	movq	$0, -1616(%rbp)
	movq	$0, -1608(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1624(%rbp)
	leaq	-1576(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1608(%rbp)
	movq	%rdx, -1616(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1592(%rbp)
	movq	%rax, -1816(%rbp)
	movq	$0, -1600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1432(%rbp)
	movq	$0, -1424(%rbp)
	movq	%rax, -1440(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1432(%rbp)
	leaq	-1384(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -1832(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1864(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$96, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1840(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1848(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	$0, -472(%rbp)
	movq	%rax, -480(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1824(%rbp)
	movq	$0, -464(%rbp)
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	$0, -440(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1888(%rbp), %xmm1
	movaps	%xmm0, -1760(%rbp)
	movhps	-1904(%rbp), %xmm1
	movq	$0, -1744(%rbp)
	movaps	%xmm1, -1888(%rbp)
	call	_Znwm@PLT
	movdqa	-1888(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L859
	call	_ZdlPv@PLT
.L859:
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1568(%rbp)
	jne	.L1043
	cmpq	$0, -1376(%rbp)
	jne	.L1044
.L865:
	cmpq	$0, -1184(%rbp)
	jne	.L1045
.L868:
	cmpq	$0, -992(%rbp)
	jne	.L1046
.L873:
	cmpq	$0, -800(%rbp)
	jne	.L1047
.L876:
	cmpq	$0, -608(%rbp)
	leaq	-288(%rbp), %r15
	jne	.L1048
	cmpq	$0, -416(%rbp)
	jne	.L1049
.L882:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L884
	call	_ZdlPv@PLT
.L884:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L885
	call	_ZdlPv@PLT
.L885:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L886
	.p2align 4,,10
	.p2align 3
.L890:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L887
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L890
.L888:
	movq	-280(%rbp), %r14
.L886:
	testq	%r14, %r14
	je	.L891
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L891:
	movq	-1824(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L892
	call	_ZdlPv@PLT
.L892:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L893
	.p2align 4,,10
	.p2align 3
.L897:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L894
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L897
.L895:
	movq	-472(%rbp), %r14
.L893:
	testq	%r14, %r14
	je	.L898
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L898:
	movq	-1848(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L899
	call	_ZdlPv@PLT
.L899:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L900
	.p2align 4,,10
	.p2align 3
.L904:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L901
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L904
.L902:
	movq	-664(%rbp), %r14
.L900:
	testq	%r14, %r14
	je	.L905
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L905:
	movq	-1840(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L906
	call	_ZdlPv@PLT
.L906:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L907
	.p2align 4,,10
	.p2align 3
.L911:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L908
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L911
.L909:
	movq	-856(%rbp), %r14
.L907:
	testq	%r14, %r14
	je	.L912
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L912:
	movq	-1864(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L913
	call	_ZdlPv@PLT
.L913:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L914
	.p2align 4,,10
	.p2align 3
.L918:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L915
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L918
.L916:
	movq	-1048(%rbp), %r14
.L914:
	testq	%r14, %r14
	je	.L919
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L919:
	movq	-1832(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L920
	call	_ZdlPv@PLT
.L920:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L921
	.p2align 4,,10
	.p2align 3
.L925:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L922
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L925
.L923:
	movq	-1240(%rbp), %r14
.L921:
	testq	%r14, %r14
	je	.L926
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L926:
	movq	-1856(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L927
	call	_ZdlPv@PLT
.L927:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L928
	.p2align 4,,10
	.p2align 3
.L932:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L929
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L932
.L930:
	movq	-1432(%rbp), %r14
.L928:
	testq	%r14, %r14
	je	.L933
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L933:
	movq	-1816(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L934
	call	_ZdlPv@PLT
.L934:
	movq	-1616(%rbp), %rbx
	movq	-1624(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L935
	.p2align 4,,10
	.p2align 3
.L939:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L936
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L939
.L937:
	movq	-1624(%rbp), %r14
.L935:
	testq	%r14, %r14
	je	.L940
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L940:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1050
	addq	$1864, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L936:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L939
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L929:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L932
	jmp	.L930
	.p2align 4,,10
	.p2align 3
.L922:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L925
	jmp	.L923
	.p2align 4,,10
	.p2align 3
.L915:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L918
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L908:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L911
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L901:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L904
	jmp	.L902
	.p2align 4,,10
	.p2align 3
.L887:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L890
	jmp	.L888
	.p2align 4,,10
	.p2align 3
.L894:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L897
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L1043:
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r9d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L861
	call	_ZdlPv@PLT
.L861:
	movq	(%r15), %rax
	movl	$2026, %edx
	movq	%r12, %rdi
	leaq	.LC3(%rip), %rsi
	leaq	-1792(%rbp), %r15
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rcx, -1904(%rbp)
	movq	%rax, -1888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1888(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1888(%rbp), %rax
	movl	$32, %edi
	movq	-1904(%rbp), %xmm2
	movq	$0, -1776(%rbp)
	movq	%rax, %xmm0
	movq	%rax, %xmm6
	punpcklqdq	%xmm0, %xmm0
	punpcklqdq	%xmm6, %xmm2
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -1904(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -1792(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movq	%r15, %rsi
	leaq	32(%rax), %rdx
	leaq	-1248(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L862
	call	_ZdlPv@PLT
.L862:
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1752(%rbp)
	jne	.L1051
.L863:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1376(%rbp)
	je	.L865
.L1044:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1440(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r8d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L866
	call	_ZdlPv@PLT
.L866:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-480(%rbp), %rdi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L867
	call	_ZdlPv@PLT
.L867:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1184(%rbp)
	je	.L868
.L1045:
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1248(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L869
	call	_ZdlPv@PLT
.L869:
	movq	(%r15), %rax
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	24(%rax), %rax
	movq	%rcx, -1904(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -1888(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1888(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal23Cast13JSArrayBuffer_114EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rcx
	movq	%r15, %xmm3
	pxor	%xmm0, %xmm0
	movhps	-1904(%rbp), %xmm3
	movl	$32, %edi
	movq	%rax, -72(%rbp)
	leaq	-1792(%rbp), %r15
	movq	%rcx, -80(%rbp)
	movaps	%xmm3, -1904(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -1792(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	32(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L870
	call	_ZdlPv@PLT
.L870:
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1752(%rbp)
	jne	.L1052
.L871:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -992(%rbp)
	je	.L873
.L1046:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rdi
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L874
	call	_ZdlPv@PLT
.L874:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-480(%rbp), %rdi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L875
	call	_ZdlPv@PLT
.L875:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L876
.L1047:
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L877
	call	_ZdlPv@PLT
.L877:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	24(%rax), %rdx
	movdqu	(%rax), %xmm4
	movaps	%xmm0, -1760(%rbp)
	movq	$0, -1744(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm4, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -1760(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L878
	call	_ZdlPv@PLT
.L878:
	movq	-1848(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L1048:
	movq	-1848(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%r15, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L880
	call	_ZdlPv@PLT
.L880:
	movq	(%rbx), %rax
	movl	$114, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r15
	movq	%rcx, -1888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r15, -80(%rbp)
	movhps	-1888(%rbp), %xmm0
	leaq	-288(%rbp), %r15
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm6
	leaq	24(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L881
	call	_ZdlPv@PLT
.L881:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -416(%rbp)
	je	.L882
.L1049:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L883
	call	_ZdlPv@PLT
.L883:
	movq	-1872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L882
	.p2align 4,,10
	.p2align 3
.L1051:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1904(%rbp), %xmm7
	movl	$24, %edi
	movaps	%xmm0, -1792(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-1440(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L864
	call	_ZdlPv@PLT
.L864:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L1052:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1904(%rbp), %xmm7
	movl	$24, %edi
	movaps	%xmm0, -1792(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-1056(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L872
	call	_ZdlPv@PLT
.L872:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L871
.L1050:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22707:
	.size	_ZN2v88internal24Cast13JSArrayBuffer_1473EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, .-_ZN2v88internal24Cast13JSArrayBuffer_1473EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_3SmiENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverES6_NS0_6UnionTIS5_NS0_10HeapNumberEEES8_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EESL_PNSD_ISB_EESP_SL_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_3SmiENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverES6_NS0_6UnionTIS5_NS0_10HeapNumberEEES8_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EESL_PNSD_ISB_EESP_SL_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_3SmiENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverES6_NS0_6UnionTIS5_NS0_10HeapNumberEEES8_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EESL_PNSD_ISB_EESP_SL_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_3SmiENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverES6_NS0_6UnionTIS5_NS0_10HeapNumberEEES8_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EESL_PNSD_ISB_EESP_SL_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_3SmiENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverES6_NS0_6UnionTIS5_NS0_10HeapNumberEEES8_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EESL_PNSD_ISB_EESP_SL_:
.LFB27506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$10, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$577875841032521479, %rcx
	movq	%rcx, (%rax)
	movl	$1287, %ecx
	leaq	10(%rax), %rdx
	movw	%cx, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1054
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %rax
.L1054:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1055
	movq	%rdx, (%r15)
.L1055:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1056
	movq	%rdx, (%r14)
.L1056:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1057
	movq	%rdx, 0(%r13)
.L1057:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1058
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1058:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1059
	movq	%rdx, (%rbx)
.L1059:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1060
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1060:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1061
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1061:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1062
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1062:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1063
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1063:
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L1053
	movq	-128(%rbp), %rsi
	movq	%rax, (%rsi)
.L1053:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1100
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1100:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27506:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_3SmiENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverES6_NS0_6UnionTIS5_NS0_10HeapNumberEEES8_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EESL_PNSD_ISB_EESP_SL_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_3SmiENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverES6_NS0_6UnionTIS5_NS0_10HeapNumberEEES8_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EESL_PNSD_ISB_EESP_SL_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_3SmiENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverES6_NS0_6UnionTIS5_NS0_10HeapNumberEEES8_S6_NS0_13JSArrayBufferENS0_12JSTypedArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSF_IS4_EEPNSF_IS5_EEPNSF_IS6_EEPNSF_IS7_EEPNSF_IS8_EESN_PNSF_ISB_EESR_SN_PNSF_ISC_EEPNSF_ISD_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_3SmiENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverES6_NS0_6UnionTIS5_NS0_10HeapNumberEEES8_S6_NS0_13JSArrayBufferENS0_12JSTypedArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSF_IS4_EEPNSF_IS5_EEPNSF_IS6_EEPNSF_IS7_EEPNSF_IS8_EESN_PNSF_ISB_EESR_SN_PNSF_ISC_EEPNSF_ISD_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_3SmiENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverES6_NS0_6UnionTIS5_NS0_10HeapNumberEEES8_S6_NS0_13JSArrayBufferENS0_12JSTypedArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSF_IS4_EEPNSF_IS5_EEPNSF_IS6_EEPNSF_IS7_EEPNSF_IS8_EESN_PNSF_ISB_EESR_SN_PNSF_ISC_EEPNSF_ISD_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_3SmiENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverES6_NS0_6UnionTIS5_NS0_10HeapNumberEEES8_S6_NS0_13JSArrayBufferENS0_12JSTypedArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSF_IS4_EEPNSF_IS5_EEPNSF_IS6_EEPNSF_IS7_EEPNSF_IS8_EESN_PNSF_ISB_EESR_SN_PNSF_ISC_EEPNSF_ISD_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_3SmiENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverES6_NS0_6UnionTIS5_NS0_10HeapNumberEEES8_S6_NS0_13JSArrayBufferENS0_12JSTypedArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSF_IS4_EEPNSF_IS5_EEPNSF_IS6_EEPNSF_IS7_EEPNSF_IS8_EESN_PNSF_ISB_EESR_SN_PNSF_ISC_EEPNSF_ISD_EE:
.LFB27512:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$12, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$120, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$577875841032521479, %rcx
	movq	%rcx, (%rax)
	leaq	12(%rax), %rdx
	movl	$117900551, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1102
	movq	%rax, -152(%rbp)
	call	_ZdlPv@PLT
	movq	-152(%rbp), %rax
.L1102:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1103
	movq	%rdx, (%r15)
.L1103:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1104
	movq	%rdx, (%r14)
.L1104:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1105
	movq	%rdx, 0(%r13)
.L1105:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1106
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1106:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1107
	movq	%rdx, (%rbx)
.L1107:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1108
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1108:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1109
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1109:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1110
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1110:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1111
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1111:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1112
	movq	-128(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1112:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1113
	movq	-136(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1113:
	movq	88(%rax), %rax
	testq	%rax, %rax
	je	.L1101
	movq	-144(%rbp), %rbx
	movq	%rax, (%rbx)
.L1101:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1156
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1156:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27512:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_3SmiENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverES6_NS0_6UnionTIS5_NS0_10HeapNumberEEES8_S6_NS0_13JSArrayBufferENS0_12JSTypedArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSF_IS4_EEPNSF_IS5_EEPNSF_IS6_EEPNSF_IS7_EEPNSF_IS8_EESN_PNSF_ISB_EESR_SN_PNSF_ISC_EEPNSF_ISD_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_3SmiENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverES6_NS0_6UnionTIS5_NS0_10HeapNumberEEES8_S6_NS0_13JSArrayBufferENS0_12JSTypedArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSF_IS4_EEPNSF_IS5_EEPNSF_IS6_EEPNSF_IS7_EEPNSF_IS8_EESN_PNSF_ISB_EESR_SN_PNSF_ISC_EEPNSF_ISD_EE
	.section	.rodata._ZN2v88internal24TypedArrayInitialize_342EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEbNS4_INS0_3MapEEENS4_INS0_3SmiEEENS0_34TorqueStructTypedArrayElementsInfoENS4_INS0_10JSReceiverEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"../../deps/v8/../../deps/v8/src/builtins/typed-array.tq"
	.section	.text._ZN2v88internal24TypedArrayInitialize_342EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEbNS4_INS0_3MapEEENS4_INS0_3SmiEEENS0_34TorqueStructTypedArrayElementsInfoENS4_INS0_10JSReceiverEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24TypedArrayInitialize_342EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEbNS4_INS0_3MapEEENS4_INS0_3SmiEEENS0_34TorqueStructTypedArrayElementsInfoENS4_INS0_10JSReceiverEEE
	.type	_ZN2v88internal24TypedArrayInitialize_342EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEbNS4_INS0_3MapEEENS4_INS0_3SmiEEENS0_34TorqueStructTypedArrayElementsInfoENS4_INS0_10JSReceiverEEE, @function
_ZN2v88internal24TypedArrayInitialize_342EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEbNS4_INS0_3MapEEENS4_INS0_3SmiEEENS0_34TorqueStructTypedArrayElementsInfoENS4_INS0_10JSReceiverEEE:
.LFB22426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$1128, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	24(%rbp), %r14
	movl	%edx, -5120(%rbp)
	movq	%rdi, %r13
	leaq	-4840(%rbp), %r15
	leaq	-4520(%rbp), %r12
	movq	%r9, -4912(%rbp)
	leaq	-4328(%rbp), %rbx
	movq	%rsi, -4864(%rbp)
	movq	%rcx, -4880(%rbp)
	movq	%r8, -4896(%rbp)
	movq	%rax, -4920(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -4840(%rbp)
	movq	%rdi, -4576(%rbp)
	movl	$144, %edi
	movq	$0, -4568(%rbp)
	movq	$0, -4560(%rbp)
	movq	$0, -4552(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r12, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rdx, -4552(%rbp)
	movq	%rdx, -4560(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4536(%rbp)
	movq	%rax, -4568(%rbp)
	movq	$0, -4544(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4840(%rbp), %rax
	movl	$288, %edi
	movq	$0, -4376(%rbp)
	movq	$0, -4368(%rbp)
	movq	%rax, -4384(%rbp)
	movq	$0, -4360(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rdx, -4360(%rbp)
	movq	%rdx, -4368(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4344(%rbp)
	movq	%rax, -4376(%rbp)
	movq	$0, -4352(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4840(%rbp), %rax
	movl	$288, %edi
	movq	$0, -4184(%rbp)
	movq	$0, -4176(%rbp)
	movq	%rax, -4192(%rbp)
	movq	$0, -4168(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -4184(%rbp)
	leaq	-4136(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4168(%rbp)
	movq	%rdx, -4176(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4152(%rbp)
	movq	%rax, -4952(%rbp)
	movq	$0, -4160(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4840(%rbp), %rax
	movl	$264, %edi
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	%rax, -4000(%rbp)
	movq	$0, -3976(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -3992(%rbp)
	leaq	-3944(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3976(%rbp)
	movq	%rdx, -3984(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3960(%rbp)
	movq	%rax, -4992(%rbp)
	movq	$0, -3968(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4840(%rbp), %rax
	movl	$144, %edi
	movq	$0, -3800(%rbp)
	movq	$0, -3792(%rbp)
	movq	%rax, -3808(%rbp)
	movq	$0, -3784(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -3800(%rbp)
	leaq	-3752(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3784(%rbp)
	movq	%rdx, -3792(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3768(%rbp)
	movq	%rax, -4960(%rbp)
	movq	$0, -3776(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4840(%rbp), %rax
	movl	$168, %edi
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	movq	%rax, -3616(%rbp)
	movq	$0, -3592(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -3608(%rbp)
	leaq	-3560(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3592(%rbp)
	movq	%rdx, -3600(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3576(%rbp)
	movq	%rax, -5056(%rbp)
	movq	$0, -3584(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4840(%rbp), %rax
	movl	$240, %edi
	movq	$0, -3416(%rbp)
	movq	$0, -3408(%rbp)
	movq	%rax, -3424(%rbp)
	movq	$0, -3400(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -3416(%rbp)
	leaq	-3368(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3400(%rbp)
	movq	%rdx, -3408(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3384(%rbp)
	movq	%rax, -5104(%rbp)
	movq	$0, -3392(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4840(%rbp), %rax
	movl	$240, %edi
	movq	$0, -3224(%rbp)
	movq	$0, -3216(%rbp)
	movq	%rax, -3232(%rbp)
	movq	$0, -3208(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -3224(%rbp)
	leaq	-3176(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3208(%rbp)
	movq	%rdx, -3216(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3192(%rbp)
	movq	%rax, -5088(%rbp)
	movq	$0, -3200(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4840(%rbp), %rax
	movl	$240, %edi
	movq	$0, -3032(%rbp)
	movq	$0, -3024(%rbp)
	movq	%rax, -3040(%rbp)
	movq	$0, -3016(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -3032(%rbp)
	leaq	-2984(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3016(%rbp)
	movq	%rdx, -3024(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3000(%rbp)
	movq	%rax, -5024(%rbp)
	movq	$0, -3008(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4840(%rbp), %rax
	movl	$240, %edi
	movq	$0, -2840(%rbp)
	movq	$0, -2832(%rbp)
	movq	%rax, -2848(%rbp)
	movq	$0, -2824(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -2840(%rbp)
	leaq	-2792(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2824(%rbp)
	movq	%rdx, -2832(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2808(%rbp)
	movq	%rax, -5080(%rbp)
	movq	$0, -2816(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4840(%rbp), %rax
	movl	$288, %edi
	movq	$0, -2648(%rbp)
	movq	$0, -2640(%rbp)
	movq	%rax, -2656(%rbp)
	movq	$0, -2632(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -2648(%rbp)
	leaq	-2600(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2632(%rbp)
	movq	%rdx, -2640(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2616(%rbp)
	movq	%rax, -5232(%rbp)
	movq	$0, -2624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4840(%rbp), %rax
	movl	$288, %edi
	movq	$0, -2456(%rbp)
	movq	$0, -2448(%rbp)
	movq	%rax, -2464(%rbp)
	movq	$0, -2440(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -2456(%rbp)
	leaq	-2408(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2440(%rbp)
	movq	%rdx, -2448(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2424(%rbp)
	movq	%rax, -5240(%rbp)
	movq	$0, -2432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4840(%rbp), %rax
	movl	$288, %edi
	movq	$0, -2264(%rbp)
	movq	$0, -2256(%rbp)
	movq	%rax, -2272(%rbp)
	movq	$0, -2248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -2264(%rbp)
	leaq	-2216(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2248(%rbp)
	movq	%rdx, -2256(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2232(%rbp)
	movq	%rax, -5016(%rbp)
	movq	$0, -2240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4840(%rbp), %rax
	movl	$240, %edi
	movq	$0, -2072(%rbp)
	movq	$0, -2064(%rbp)
	movq	%rax, -2080(%rbp)
	movq	$0, -2056(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -2072(%rbp)
	leaq	-2024(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2056(%rbp)
	movq	%rdx, -2064(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2040(%rbp)
	movq	%rax, -5200(%rbp)
	movq	$0, -2048(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4840(%rbp), %rax
	movl	$240, %edi
	movq	$0, -1880(%rbp)
	movq	$0, -1872(%rbp)
	movq	%rax, -1888(%rbp)
	movq	$0, -1864(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -1880(%rbp)
	leaq	-1832(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1864(%rbp)
	movq	%rdx, -1872(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1848(%rbp)
	movq	%rax, -5216(%rbp)
	movq	$0, -1856(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4840(%rbp), %rax
	movl	$240, %edi
	movq	$0, -1688(%rbp)
	movq	$0, -1680(%rbp)
	movq	%rax, -1696(%rbp)
	movq	$0, -1672(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -1688(%rbp)
	leaq	-1640(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1672(%rbp)
	movq	%rdx, -1680(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1656(%rbp)
	movq	%rax, -5168(%rbp)
	movq	$0, -1664(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4840(%rbp), %rax
	movl	$264, %edi
	movq	$0, -1496(%rbp)
	movq	$0, -1488(%rbp)
	movq	%rax, -1504(%rbp)
	movq	$0, -1480(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -1496(%rbp)
	leaq	-1448(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1480(%rbp)
	movq	%rdx, -1488(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1464(%rbp)
	movq	%rax, -5072(%rbp)
	movq	$0, -1472(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4840(%rbp), %rax
	movl	$288, %edi
	movq	$0, -1304(%rbp)
	movq	$0, -1296(%rbp)
	movq	%rax, -1312(%rbp)
	movq	$0, -1288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -1304(%rbp)
	leaq	-1256(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1288(%rbp)
	movq	%rdx, -1296(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1272(%rbp)
	movq	%rax, -5248(%rbp)
	movq	$0, -1280(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4840(%rbp), %rax
	movl	$312, %edi
	movq	$0, -1112(%rbp)
	movq	$0, -1104(%rbp)
	movq	%rax, -1120(%rbp)
	movq	$0, -1096(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rax, -1112(%rbp)
	leaq	-1064(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1096(%rbp)
	movq	%rdx, -1104(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1080(%rbp)
	movq	%rax, -5184(%rbp)
	movq	$0, -1088(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4840(%rbp), %rax
	movl	$264, %edi
	movq	$0, -920(%rbp)
	movq	$0, -912(%rbp)
	movq	%rax, -928(%rbp)
	movq	$0, -904(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -920(%rbp)
	leaq	-872(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -904(%rbp)
	movq	%rdx, -912(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -888(%rbp)
	movq	%rax, -5152(%rbp)
	movq	$0, -896(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4840(%rbp), %rax
	movl	$288, %edi
	movq	$0, -728(%rbp)
	movq	$0, -720(%rbp)
	movq	%rax, -736(%rbp)
	movq	$0, -712(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -728(%rbp)
	leaq	-680(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -712(%rbp)
	movq	%rdx, -720(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -696(%rbp)
	movq	%rax, -5160(%rbp)
	movq	$0, -704(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4840(%rbp), %rax
	movl	$168, %edi
	movq	$0, -536(%rbp)
	movq	$0, -528(%rbp)
	movq	%rax, -544(%rbp)
	movq	$0, -520(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -536(%rbp)
	leaq	-488(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -520(%rbp)
	movq	%rdx, -528(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -504(%rbp)
	movq	%rax, -5144(%rbp)
	movq	$0, -512(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4840(%rbp), %rax
	movl	$168, %edi
	movq	$0, -344(%rbp)
	movq	$0, -336(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -328(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -344(%rbp)
	leaq	-296(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -328(%rbp)
	movq	%rdx, -336(%rbp)
	xorl	%edx, %edx
	movq	%rax, -5128(%rbp)
	movups	%xmm0, -312(%rbp)
	movq	$0, -320(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %xmm1
	pxor	%xmm0, %xmm0
	movq	-4896(%rbp), %xmm2
	movq	-4864(%rbp), %xmm3
	movhps	-4912(%rbp), %xmm1
	movl	$48, %edi
	movaps	%xmm0, -4704(%rbp)
	movhps	-4920(%rbp), %xmm2
	movaps	%xmm1, -128(%rbp)
	leaq	-4704(%rbp), %r14
	movhps	-4880(%rbp), %xmm3
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm3, -160(%rbp)
	movq	$0, -4688(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-144(%rbp), %xmm7
	leaq	48(%rax), %rdx
	movq	%rax, -4704(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm5, 32(%rax)
	leaq	-4576(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4688(%rbp)
	movq	%rdx, -4696(%rbp)
	movq	%rax, -5136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1158
	call	_ZdlPv@PLT
.L1158:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4384(%rbp), %rax
	cmpq	$0, -4512(%rbp)
	movq	%rax, -4928(%rbp)
	leaq	-4192(%rbp), %rax
	movq	%rax, -4920(%rbp)
	jne	.L1377
.L1159:
	leaq	-3808(%rbp), %rax
	cmpq	$0, -4320(%rbp)
	movq	%rax, -4944(%rbp)
	jne	.L1378
.L1163:
	leaq	-4000(%rbp), %rax
	cmpq	$0, -4128(%rbp)
	movq	%rax, -4976(%rbp)
	jne	.L1379
.L1166:
	leaq	-3616(%rbp), %rax
	cmpq	$0, -3936(%rbp)
	movq	%rax, -5008(%rbp)
	jne	.L1380
.L1169:
	cmpq	$0, -3744(%rbp)
	jne	.L1381
.L1172:
	leaq	-3424(%rbp), %rax
	cmpq	$0, -3552(%rbp)
	movq	%rax, -4960(%rbp)
	leaq	-3232(%rbp), %rax
	movq	%rax, -5040(%rbp)
	jne	.L1382
.L1174:
	leaq	-1504(%rbp), %rax
	cmpq	$0, -3360(%rbp)
	movq	%rax, -4864(%rbp)
	jne	.L1383
.L1178:
	leaq	-3040(%rbp), %rax
	cmpq	$0, -3168(%rbp)
	movq	%rax, -4952(%rbp)
	leaq	-2848(%rbp), %rax
	movq	%rax, -4992(%rbp)
	jne	.L1384
.L1180:
	leaq	-2080(%rbp), %rax
	cmpq	$0, -2976(%rbp)
	movq	%rax, -5056(%rbp)
	jne	.L1385
.L1183:
	leaq	-2656(%rbp), %rax
	cmpq	$0, -2784(%rbp)
	movq	%rax, -5104(%rbp)
	leaq	-2464(%rbp), %rax
	movq	%rax, -5088(%rbp)
	jne	.L1386
.L1185:
	leaq	-2272(%rbp), %rax
	cmpq	$0, -2592(%rbp)
	movq	%rax, -4896(%rbp)
	jne	.L1387
.L1190:
	cmpq	$0, -2400(%rbp)
	jne	.L1388
.L1192:
	leaq	-544(%rbp), %rax
	cmpq	$0, -2208(%rbp)
	movq	%rax, -4880(%rbp)
	jne	.L1389
.L1194:
	leaq	-1888(%rbp), %rax
	cmpq	$0, -2016(%rbp)
	movq	%rax, -5024(%rbp)
	leaq	-1696(%rbp), %rax
	movq	%rax, -5080(%rbp)
	jne	.L1390
.L1196:
	cmpq	$0, -1824(%rbp)
	jne	.L1391
.L1201:
	cmpq	$0, -1632(%rbp)
	jne	.L1392
.L1203:
	leaq	-1120(%rbp), %rax
	cmpq	$0, -1440(%rbp)
	movq	%rax, -5016(%rbp)
	leaq	-1312(%rbp), %rax
	movq	%rax, -4912(%rbp)
	jne	.L1393
.L1208:
	leaq	-928(%rbp), %rax
	cmpq	$0, -1248(%rbp)
	movq	%rax, -5120(%rbp)
	jne	.L1394
.L1213:
	leaq	-736(%rbp), %rax
	cmpq	$0, -1056(%rbp)
	movq	%rax, -5072(%rbp)
	jne	.L1395
.L1216:
	cmpq	$0, -864(%rbp)
	jne	.L1396
	cmpq	$0, -672(%rbp)
	jne	.L1397
.L1221:
	cmpq	$0, -480(%rbp)
	leaq	-352(%rbp), %r12
	jne	.L1398
.L1227:
	movq	-5128(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -4688(%rbp)
	movaps	%xmm0, -4704(%rbp)
	call	_Znwm@PLT
	movl	$1796, %ecx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movl	$84281095, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -4704(%rbp)
	movq	%rdx, -4688(%rbp)
	movq	%rdx, -4696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4704(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1230
	call	_ZdlPv@PLT
.L1230:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	48(%rax), %r13
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4880(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5072(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5120(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5016(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4912(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4864(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5080(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5024(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5056(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4896(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5088(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5104(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4992(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4952(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5040(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4960(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5008(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4944(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4976(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4920(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4928(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5136(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1399
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1377:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -4688(%rbp)
	movaps	%xmm0, -4704(%rbp)
	call	_Znwm@PLT
	movl	$1796, %edi
	movq	%r14, %rsi
	movw	%di, 4(%rax)
	movq	-5136(%rbp), %rdi
	leaq	6(%rax), %rdx
	movl	$84281095, (%rax)
	movq	%rax, -4704(%rbp)
	movq	%rdx, -4688(%rbp)
	movq	%rdx, -4696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4704(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L1160
	call	_ZdlPv@PLT
.L1160:
	movq	(%r12), %rax
	movl	$77, %edx
	movq	%r15, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %r12
	movq	%rsi, -4920(%rbp)
	movq	32(%rax), %rsi
	movq	%rcx, -4912(%rbp)
	movq	24(%rax), %rcx
	movq	40(%rax), %rax
	movq	%rsi, -4928(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -4864(%rbp)
	movq	%rax, -4976(%rbp)
	movq	%r12, -4944(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$26, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal36Convert9ATuintptr13ATPositiveSmi_187EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$27, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4864(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEENS3_INS0_9IntegralTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -4880(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$30, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4864(%rbp), %rdx
	movq	-4880(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShrENS1_11SloppyTNodeINS0_5WordTEEENS3_INS0_9IntegralTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -4896(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4896(%rbp), %r8
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -4896(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-64(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movq	-4944(%rbp), %xmm2
	movq	-4928(%rbp), %rax
	movq	%r12, %xmm6
	leaq	-160(%rbp), %r12
	movq	-4864(%rbp), %xmm5
	movhps	-4880(%rbp), %xmm6
	movq	%r12, %rsi
	movaps	%xmm0, -4704(%rbp)
	movdqa	%xmm2, %xmm7
	movdqa	%xmm2, %xmm3
	movq	%rax, %xmm4
	movq	-4912(%rbp), %xmm2
	punpcklqdq	%xmm4, %xmm5
	punpcklqdq	%xmm7, %xmm7
	movhps	-4976(%rbp), %xmm4
	movq	%rdx, -4912(%rbp)
	movhps	-4864(%rbp), %xmm3
	movhps	-4920(%rbp), %xmm2
	movaps	%xmm6, -4880(%rbp)
	movaps	%xmm7, -5040(%rbp)
	movaps	%xmm5, -4944(%rbp)
	movaps	%xmm4, -5008(%rbp)
	movaps	%xmm3, -4976(%rbp)
	movaps	%xmm2, -4864(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm6, -80(%rbp)
	movq	$0, -4688(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-4384(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4704(%rbp), %rdi
	movq	-4912(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L1161
	call	_ZdlPv@PLT
	movq	-4912(%rbp), %rdx
.L1161:
	movdqa	-4864(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqa	-4976(%rbp), %xmm7
	movaps	%xmm0, -4704(%rbp)
	movaps	%xmm6, -160(%rbp)
	movdqa	-5008(%rbp), %xmm6
	movaps	%xmm7, -144(%rbp)
	movdqa	-4944(%rbp), %xmm7
	movaps	%xmm6, -128(%rbp)
	movdqa	-5040(%rbp), %xmm6
	movaps	%xmm7, -112(%rbp)
	movdqa	-4880(%rbp), %xmm7
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	movq	$0, -4688(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-4192(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4920(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1162
	call	_ZdlPv@PLT
.L1162:
	movq	-4952(%rbp), %rcx
	movq	-4896(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1159
	.p2align 4,,10
	.p2align 3
.L1378:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movabsq	$289645464880809735, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$12, %edi
	movq	$0, -4688(%rbp)
	movaps	%xmm0, -4704(%rbp)
	call	_Znwm@PLT
	movq	-4928(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	12(%rax), %rdx
	movl	$84215302, 8(%rax)
	movq	%rax, -4704(%rbp)
	movq	%rdx, -4688(%rbp)
	movq	%rdx, -4696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4704(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1164
	call	_ZdlPv@PLT
.L1164:
	movq	(%rbx), %rax
	leaq	-160(%rbp), %rsi
	leaq	-112(%rbp), %rdx
	movq	%r14, %rdi
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm1
	movdqu	32(%rax), %xmm6
	movq	$0, -4688(%rbp)
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -4704(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3808(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1165
	call	_ZdlPv@PLT
.L1165:
	movq	-4960(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1163
	.p2align 4,,10
	.p2align 3
.L1379:
	movq	-4952(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$289645464880809735, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$12, %edi
	movq	$0, -4688(%rbp)
	movaps	%xmm0, -4704(%rbp)
	call	_Znwm@PLT
	movq	-4920(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	12(%rax), %rdx
	movl	$84215302, 8(%rax)
	movq	%rax, -4704(%rbp)
	movq	%rdx, -4688(%rbp)
	movq	%rdx, -4696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4704(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1167
	call	_ZdlPv@PLT
.L1167:
	movq	(%rbx), %rax
	movq	40(%rax), %rdx
	movq	24(%rax), %rsi
	movq	8(%rax), %rcx
	movq	72(%rax), %rdi
	movq	(%rax), %rbx
	movq	48(%rax), %r12
	movq	%rdx, -4976(%rbp)
	movq	56(%rax), %rdx
	movq	%rsi, -4912(%rbp)
	movq	32(%rax), %rsi
	movq	%rcx, -4880(%rbp)
	movq	%rdx, -5008(%rbp)
	movq	64(%rax), %rdx
	movq	16(%rax), %rcx
	movq	%rsi, -4952(%rbp)
	leaq	.LC4(%rip), %rsi
	movq	%rdx, -5040(%rbp)
	movl	$31, %edx
	movq	%rdi, -5264(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -4896(%rbp)
	movq	%rbx, -4864(%rbp)
	movq	88(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-160(%rbp), %rsi
	leaq	-72(%rbp), %rdx
	movq	%r14, %rdi
	movq	-4864(%rbp), %xmm0
	movq	%rbx, -80(%rbp)
	movq	$0, -4688(%rbp)
	movhps	-4880(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-4896(%rbp), %xmm0
	movhps	-4912(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-4952(%rbp), %xmm0
	movhps	-4976(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%r12, %xmm0
	movhps	-5008(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-5040(%rbp), %xmm0
	movhps	-5264(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -4704(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-4000(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1168
	call	_ZdlPv@PLT
.L1168:
	movq	-4992(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1166
	.p2align 4,,10
	.p2align 3
.L1381:
	movq	-4960(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -4688(%rbp)
	movaps	%xmm0, -4704(%rbp)
	call	_Znwm@PLT
	movl	$1796, %ecx
	movq	-4944(%rbp), %rdi
	movq	%r14, %rsi
	movl	$84281095, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -4704(%rbp)
	movq	%rdx, -4688(%rbp)
	movq	%rdx, -4696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4704(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1173
	call	_ZdlPv@PLT
.L1173:
	movq	(%rbx), %rax
	movl	$78, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$186, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal17CodeStubAssembler15ThrowRangeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1380:
	movq	-4992(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-160(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1542, %esi
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movw	%si, -152(%rbp)
	leaq	-149(%rbp), %rdx
	movq	%r12, %rsi
	movabsq	$289645464880809735, %rax
	movq	%rax, -160(%rbp)
	movaps	%xmm0, -4704(%rbp)
	movb	$5, -150(%rbp)
	movq	$0, -4688(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4976(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4704(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1170
	call	_ZdlPv@PLT
.L1170:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rsi
	movq	8(%rax), %rcx
	movq	40(%rax), %rdx
	movq	(%rax), %rbx
	movq	%rsi, -4912(%rbp)
	movq	32(%rax), %rsi
	movq	%rcx, -4880(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -4952(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -4992(%rbp)
	movl	$77, %edx
	movq	%rcx, -4896(%rbp)
	movq	%rbx, -4864(%rbp)
	movq	80(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-104(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-4864(%rbp), %xmm0
	movq	%rbx, -112(%rbp)
	movq	$0, -4688(%rbp)
	movhps	-4880(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-4896(%rbp), %xmm0
	movhps	-4912(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-4952(%rbp), %xmm0
	movhps	-4992(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -4704(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3616(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -5008(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1171
	call	_ZdlPv@PLT
.L1171:
	movq	-5056(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1385:
	movq	-5024(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4816(%rbp)
	movq	$0, -4808(%rbp)
	movq	$0, -4800(%rbp)
	movq	$0, -4792(%rbp)
	movq	$0, -4784(%rbp)
	movq	$0, -4776(%rbp)
	movq	$0, -4768(%rbp)
	movq	$0, -4760(%rbp)
	movq	$0, -4752(%rbp)
	movq	$0, -4736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-4736(%rbp), %rax
	movq	-4952(%rbp), %rdi
	pushq	%rax
	leaq	-4752(%rbp), %rax
	leaq	-4800(%rbp), %rcx
	pushq	%rax
	leaq	-4760(%rbp), %rax
	leaq	-4808(%rbp), %rdx
	pushq	%rax
	leaq	-4768(%rbp), %rax
	leaq	-4816(%rbp), %rsi
	pushq	%rax
	leaq	-4776(%rbp), %rax
	leaq	-4784(%rbp), %r9
	pushq	%rax
	leaq	-4792(%rbp), %r8
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_3SmiENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverES6_NS0_6UnionTIS5_NS0_10HeapNumberEEES8_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EESL_PNSD_ISB_EESP_SL_
	addq	$48, %rsp
	movl	$80, %edi
	movq	-4752(%rbp), %xmm0
	movq	-4768(%rbp), %xmm1
	movq	-4784(%rbp), %xmm2
	movq	$0, -4688(%rbp)
	movq	-4800(%rbp), %xmm3
	movhps	-4736(%rbp), %xmm0
	movq	-4816(%rbp), %xmm4
	movhps	-4760(%rbp), %xmm1
	movhps	-4776(%rbp), %xmm2
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-4792(%rbp), %xmm3
	movhps	-4808(%rbp), %xmm4
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -4704(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-144(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm7
	movq	-5056(%rbp), %rdi
	leaq	80(%rax), %rdx
	movq	%rax, -4704(%rbp)
	movups	%xmm5, (%rax)
	movdqa	-112(%rbp), %xmm5
	movups	%xmm6, 16(%rax)
	movdqa	-96(%rbp), %xmm6
	movups	%xmm7, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	movq	%rdx, -4688(%rbp)
	movq	%rdx, -4696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1184
	call	_ZdlPv@PLT
.L1184:
	movq	-5200(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1183
	.p2align 4,,10
	.p2align 3
.L1384:
	movq	-5088(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4816(%rbp)
	leaq	-80(%rbp), %rbx
	movq	$0, -4808(%rbp)
	movq	$0, -4800(%rbp)
	movq	$0, -4792(%rbp)
	movq	$0, -4784(%rbp)
	movq	$0, -4776(%rbp)
	movq	$0, -4768(%rbp)
	movq	$0, -4760(%rbp)
	movq	$0, -4752(%rbp)
	movq	$0, -4736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-4736(%rbp), %rax
	movq	-5040(%rbp), %rdi
	pushq	%rax
	leaq	-4752(%rbp), %rax
	leaq	-4800(%rbp), %rcx
	pushq	%rax
	leaq	-4760(%rbp), %rax
	leaq	-4784(%rbp), %r9
	pushq	%rax
	leaq	-4768(%rbp), %rax
	leaq	-4792(%rbp), %r8
	pushq	%rax
	leaq	-4776(%rbp), %rax
	leaq	-4808(%rbp), %rdx
	pushq	%rax
	leaq	-4816(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_3SmiENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverES6_NS0_6UnionTIS5_NS0_10HeapNumberEEES8_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EESL_PNSD_ISB_EESP_SL_
	addq	$48, %rsp
	movl	$89, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal46FromConstexpr9ATuintptr17ATconstexpr_int31_162EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4768(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	leaq	-160(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler18UintPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -4880(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	-4752(%rbp), %xmm6
	movq	%r14, %rdi
	movq	-4768(%rbp), %xmm7
	movaps	%xmm0, -4704(%rbp)
	movq	-4784(%rbp), %xmm5
	movq	-4800(%rbp), %xmm4
	movq	$0, -4688(%rbp)
	movq	-4816(%rbp), %xmm3
	movhps	-4736(%rbp), %xmm6
	movhps	-4760(%rbp), %xmm7
	movhps	-4776(%rbp), %xmm5
	movhps	-4792(%rbp), %xmm4
	movaps	%xmm7, -112(%rbp)
	movhps	-4808(%rbp), %xmm3
	movaps	%xmm6, -5104(%rbp)
	movaps	%xmm7, -5056(%rbp)
	movaps	%xmm5, -4992(%rbp)
	movaps	%xmm4, -4912(%rbp)
	movaps	%xmm3, -4896(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm6, -96(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4952(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1181
	call	_ZdlPv@PLT
.L1181:
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqa	-4896(%rbp), %xmm6
	movdqa	-4912(%rbp), %xmm7
	movaps	%xmm0, -4704(%rbp)
	movdqa	-4992(%rbp), %xmm5
	movq	$0, -4688(%rbp)
	movaps	%xmm6, -160(%rbp)
	movdqa	-5056(%rbp), %xmm6
	movaps	%xmm7, -144(%rbp)
	movdqa	-5104(%rbp), %xmm7
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2848(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4992(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1182
	call	_ZdlPv@PLT
.L1182:
	movq	-5080(%rbp), %rcx
	movq	-5024(%rbp), %rdx
	movq	%r15, %rdi
	movq	-4880(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1180
	.p2align 4,,10
	.p2align 3
.L1383:
	movq	-5104(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4832(%rbp)
	leaq	-4752(%rbp), %r12
	movq	$0, -4824(%rbp)
	movq	$0, -4816(%rbp)
	movq	$0, -4808(%rbp)
	movq	$0, -4800(%rbp)
	movq	$0, -4792(%rbp)
	movq	$0, -4784(%rbp)
	movq	$0, -4776(%rbp)
	movq	$0, -4768(%rbp)
	movq	$0, -4760(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-4760(%rbp), %rax
	movq	-4960(%rbp), %rdi
	pushq	%rax
	leaq	-4768(%rbp), %rax
	leaq	-4808(%rbp), %r8
	pushq	%rax
	leaq	-4776(%rbp), %rax
	leaq	-4816(%rbp), %rcx
	pushq	%rax
	leaq	-4784(%rbp), %rax
	leaq	-4800(%rbp), %r9
	pushq	%rax
	leaq	-4792(%rbp), %rax
	leaq	-4824(%rbp), %rdx
	pushq	%rax
	leaq	-4832(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_3SmiENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverES6_NS0_6UnionTIS5_NS0_10HeapNumberEEES8_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EESL_PNSD_ISB_EESP_SL_
	addq	$48, %rsp
	movl	$86, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$85, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4832(%rbp), %r9
	movq	-4792(%rbp), %rcx
	movq	%r12, %rdi
	movq	-4768(%rbp), %rax
	movq	-4776(%rbp), %rbx
	movq	%r9, -4992(%rbp)
	movq	%rcx, -4880(%rbp)
	movq	%rax, -4896(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory9ConstructEPNS0_7IsolateE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -4912(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler8LoadRootENS0_9RootIndexE@PLT
	movq	-4704(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -4952(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-160(%rbp), %r10
	movq	%rbx, -128(%rbp)
	movq	-4896(%rbp), %xmm0
	movl	$5, %ebx
	movq	%rax, %r8
	movq	-4992(%rbp), %r9
	movq	-4688(%rbp), %rax
	movhps	-4880(%rbp), %xmm0
	pushq	%rbx
	xorl	%esi, %esi
	movq	%r12, %rdi
	pushq	%r10
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	leaq	-4736(%rbp), %rdx
	movaps	%xmm0, -160(%rbp)
	movq	-4912(%rbp), %xmm0
	movq	%rcx, -4736(%rbp)
	movl	$1, %ecx
	movhps	-4952(%rbp), %xmm0
	movq	%r10, -4880(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	%rax, -4728(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-72(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rbx, -80(%rbp)
	movq	-4768(%rbp), %xmm0
	movq	-4880(%rbp), %r10
	movq	$0, -4688(%rbp)
	movq	-4784(%rbp), %xmm1
	movq	-4800(%rbp), %xmm2
	movq	-4816(%rbp), %xmm3
	movhps	-4760(%rbp), %xmm0
	movq	%r10, %rsi
	movq	-4832(%rbp), %xmm4
	movhps	-4776(%rbp), %xmm1
	movhps	-4792(%rbp), %xmm2
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-4808(%rbp), %xmm3
	movhps	-4824(%rbp), %xmm4
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -4704(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4864(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4704(%rbp), %rdi
	popq	%r11
	popq	%rbx
	testq	%rdi, %rdi
	je	.L1179
	call	_ZdlPv@PLT
.L1179:
	movq	-5072(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1178
	.p2align 4,,10
	.p2align 3
.L1382:
	movq	-5056(%rbp), %rsi
	movq	%r15, %rdi
	movl	$1796, %r12d
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movw	%r12w, -156(%rbp)
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	leaq	-160(%rbp), %r12
	leaq	-153(%rbp), %rdx
	movaps	%xmm0, -4704(%rbp)
	movq	%r12, %rsi
	movl	$84281095, -160(%rbp)
	movb	$5, -154(%rbp)
	movq	$0, -4688(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5008(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4704(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1175
	call	_ZdlPv@PLT
.L1175:
	movq	(%rbx), %rax
	movl	$79, %edx
	movq	%r15, %rdi
	movq	8(%rax), %rcx
	movq	24(%rax), %rsi
	movq	(%rax), %rbx
	movq	%rcx, -4952(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -4960(%rbp)
	movq	32(%rax), %rsi
	movq	%rcx, -4992(%rbp)
	movq	40(%rax), %rcx
	movq	48(%rax), %rax
	movq	%rsi, -5040(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -4864(%rbp)
	movq	%rax, -4880(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4880(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATuintptr_201EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8UintPtrTEEE@PLT
	movl	$80, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -5264(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rbx, -4912(%rbp)
	call	_ZN2v88internal26GetArrayBufferFunction_220EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEE@PLT
	movl	$81, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal46FromConstexpr9ATuintptr17ATconstexpr_int31_162EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$84, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -5056(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4864(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -4896(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm6
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-4880(%rbp), %xmm7
	leaq	-80(%rbp), %rbx
	movq	-5040(%rbp), %xmm5
	movhps	-5056(%rbp), %xmm6
	movq	-4992(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdx
	movaps	%xmm6, -96(%rbp)
	movq	-4912(%rbp), %xmm1
	movhps	-4864(%rbp), %xmm5
	movhps	-5264(%rbp), %xmm7
	movaps	%xmm6, -5056(%rbp)
	movhps	-4960(%rbp), %xmm4
	movaps	%xmm7, -4880(%rbp)
	movhps	-4952(%rbp), %xmm1
	movaps	%xmm5, -5040(%rbp)
	movaps	%xmm4, -4992(%rbp)
	movaps	%xmm1, -4864(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm0, -4704(%rbp)
	movq	$0, -4688(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3424(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1176
	call	_ZdlPv@PLT
.L1176:
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqa	-4864(%rbp), %xmm7
	movdqa	-4992(%rbp), %xmm6
	movaps	%xmm0, -4704(%rbp)
	movq	$0, -4688(%rbp)
	movaps	%xmm7, -160(%rbp)
	movdqa	-5040(%rbp), %xmm7
	movaps	%xmm6, -144(%rbp)
	movdqa	-4880(%rbp), %xmm6
	movaps	%xmm7, -128(%rbp)
	movdqa	-5056(%rbp), %xmm7
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3232(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -5040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1177
	call	_ZdlPv@PLT
.L1177:
	movq	-5088(%rbp), %rcx
	movq	-5104(%rbp), %rdx
	movq	%r15, %rdi
	movq	-4896(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1174
	.p2align 4,,10
	.p2align 3
.L1390:
	movq	-5200(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4816(%rbp)
	movq	$0, -4808(%rbp)
	movq	$0, -4800(%rbp)
	movq	$0, -4792(%rbp)
	movq	$0, -4784(%rbp)
	movq	$0, -4776(%rbp)
	movq	$0, -4768(%rbp)
	movq	$0, -4760(%rbp)
	movq	$0, -4752(%rbp)
	movq	$0, -4736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-4736(%rbp), %rax
	movq	-5056(%rbp), %rdi
	pushq	%rax
	leaq	-4752(%rbp), %rax
	leaq	-4800(%rbp), %rcx
	pushq	%rax
	leaq	-4760(%rbp), %rax
	leaq	-4808(%rbp), %rdx
	pushq	%rax
	leaq	-4768(%rbp), %rax
	leaq	-4816(%rbp), %rsi
	pushq	%rax
	leaq	-4776(%rbp), %rax
	leaq	-4784(%rbp), %r9
	pushq	%rax
	leaq	-4792(%rbp), %r8
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_3SmiENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverES6_NS0_6UnionTIS5_NS0_10HeapNumberEEES8_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EESL_PNSD_ISB_EESP_SL_
	addq	$48, %rsp
	movq	%r15, %rdi
	movl	$106, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	cmpb	$0, -5120(%rbp)
	movl	$80, %edi
	movq	-4752(%rbp), %xmm0
	movq	-4768(%rbp), %xmm1
	movq	-4784(%rbp), %xmm2
	movq	$0, -4688(%rbp)
	movq	-4800(%rbp), %xmm3
	movhps	-4736(%rbp), %xmm0
	movq	-4816(%rbp), %xmm4
	movhps	-4760(%rbp), %xmm1
	movhps	-4776(%rbp), %xmm2
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-4792(%rbp), %xmm3
	movhps	-4808(%rbp), %xmm4
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -4704(%rbp)
	je	.L1197
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-144(%rbp), %xmm6
	leaq	80(%rax), %rdx
	movq	%rax, -4704(%rbp)
	movdqa	-128(%rbp), %xmm7
	movq	%rdx, -4688(%rbp)
	movq	-5024(%rbp), %rdi
	movups	%xmm5, (%rax)
	movdqa	-112(%rbp), %xmm5
	movups	%xmm6, 16(%rax)
	movdqa	-96(%rbp), %xmm6
	movups	%xmm7, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	movq	%rdx, -4696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1198
	call	_ZdlPv@PLT
.L1198:
	movq	-5216(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1696(%rbp), %rax
	cmpq	$0, -1824(%rbp)
	movq	%rax, -5080(%rbp)
	je	.L1201
	.p2align 4,,10
	.p2align 3
.L1391:
	movq	-5216(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4832(%rbp)
	leaq	-4752(%rbp), %r12
	movq	$0, -4824(%rbp)
	movq	$0, -4816(%rbp)
	movq	$0, -4808(%rbp)
	movq	$0, -4800(%rbp)
	movq	$0, -4792(%rbp)
	movq	$0, -4784(%rbp)
	movq	$0, -4776(%rbp)
	movq	$0, -4768(%rbp)
	movq	$0, -4760(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-4760(%rbp), %rax
	movq	-5024(%rbp), %rdi
	pushq	%rax
	leaq	-4768(%rbp), %rax
	leaq	-4816(%rbp), %rcx
	pushq	%rax
	leaq	-4776(%rbp), %rax
	leaq	-4808(%rbp), %r8
	pushq	%rax
	leaq	-4784(%rbp), %rax
	leaq	-4800(%rbp), %r9
	pushq	%rax
	leaq	-4792(%rbp), %rax
	leaq	-4824(%rbp), %rdx
	pushq	%rax
	leaq	-4832(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_3SmiENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverES6_NS0_6UnionTIS5_NS0_10HeapNumberEEES8_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EESL_PNSD_ISB_EESP_SL_
	addq	$48, %rsp
	movl	$107, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4832(%rbp), %r9
	movq	%r12, %rdi
	movq	-4768(%rbp), %rax
	movq	-4776(%rbp), %rbx
	movq	%r9, -5200(%rbp)
	movq	%rax, -4912(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory9ConstructEPNS0_7IsolateE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rax, -5016(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler8LoadRootENS0_9RootIndexE@PLT
	movq	-4704(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -5120(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-160(%rbp), %r10
	movq	%rbx, -128(%rbp)
	movq	-4912(%rbp), %xmm0
	movl	$5, %ebx
	movq	%rax, %r8
	movq	-5200(%rbp), %r9
	movq	-4688(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	pushq	%rbx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	xorl	%esi, %esi
	pushq	%r10
	leaq	-4736(%rbp), %rdx
	movq	%r12, %rdi
	movaps	%xmm0, -160(%rbp)
	movq	-5016(%rbp), %xmm0
	movq	%rcx, -4736(%rbp)
	movl	$1, %ecx
	movhps	-5120(%rbp), %xmm0
	movq	%r10, -4912(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	%rax, -4728(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-72(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rbx, -80(%rbp)
	movq	-4768(%rbp), %xmm0
	movq	-4912(%rbp), %r10
	movq	$0, -4688(%rbp)
	movq	-4784(%rbp), %xmm1
	movq	-4800(%rbp), %xmm2
	movq	-4816(%rbp), %xmm3
	movhps	-4760(%rbp), %xmm0
	movq	%r10, %rsi
	movq	-4832(%rbp), %xmm4
	movhps	-4776(%rbp), %xmm1
	movhps	-4792(%rbp), %xmm2
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-4808(%rbp), %xmm3
	movhps	-4824(%rbp), %xmm4
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -4704(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4864(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4704(%rbp), %rdi
	popq	%rsi
	popq	%r8
	testq	%rdi, %rdi
	je	.L1202
	call	_ZdlPv@PLT
.L1202:
	movq	-5072(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1632(%rbp)
	je	.L1203
.L1392:
	movq	-5168(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4832(%rbp)
	leaq	-4752(%rbp), %rbx
	movq	$0, -4824(%rbp)
	movq	$0, -4816(%rbp)
	movq	$0, -4808(%rbp)
	movq	$0, -4800(%rbp)
	movq	$0, -4792(%rbp)
	movq	$0, -4784(%rbp)
	movq	$0, -4776(%rbp)
	movq	$0, -4768(%rbp)
	movq	$0, -4760(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-4760(%rbp), %rax
	movq	-5080(%rbp), %rdi
	pushq	%rax
	leaq	-4768(%rbp), %rax
	leaq	-4816(%rbp), %rcx
	pushq	%rax
	leaq	-4776(%rbp), %rax
	leaq	-4800(%rbp), %r9
	pushq	%rax
	leaq	-4784(%rbp), %rax
	leaq	-4808(%rbp), %r8
	pushq	%rax
	leaq	-4792(%rbp), %rax
	leaq	-4824(%rbp), %rdx
	pushq	%rax
	leaq	-4832(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_3SmiENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverES6_NS0_6UnionTIS5_NS0_10HeapNumberEEES8_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EESL_PNSD_ISB_EESP_SL_
	addq	$48, %rsp
	movl	$110, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4832(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal32GetArrayBufferNoInitFunction_221EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -5120(%rbp)
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$109, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4776(%rbp), %rax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, -4912(%rbp)
	movq	-4832(%rbp), %rax
	movq	%rax, -5016(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19IsUndefinedConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	je	.L1204
.L1206:
	movq	%r12, %xmm0
	movq	%rbx, %rdi
	movhps	-4912(%rbp), %xmm0
	movaps	%xmm0, -5200(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	xorl	%edx, %edx
	movq	%rax, %rsi
.L1376:
	movq	%r14, %rdi
	leaq	-160(%rbp), %r12
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-4704(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, -4912(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$4, %edi
	movq	-5016(%rbp), %r9
	xorl	%esi, %esi
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	pushq	%r12
	movdqa	-5200(%rbp), %xmm0
	movq	%rbx, %rdi
	leaq	-4736(%rbp), %rdx
	movq	-5120(%rbp), %xmm1
	movq	%rax, -4736(%rbp)
	movq	-4688(%rbp), %rax
	movaps	%xmm0, -144(%rbp)
	movhps	-4912(%rbp), %xmm1
	movaps	%xmm1, -160(%rbp)
	movq	%rax, -4728(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	popq	%rdx
	movq	%rbx, %rdi
	popq	%rcx
	movq	%rax, -4912(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-72(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-4768(%rbp), %xmm0
	movq	-4912(%rbp), %rax
	movq	$0, -4688(%rbp)
	movq	-4784(%rbp), %xmm1
	movq	-4800(%rbp), %xmm2
	movq	-4816(%rbp), %xmm3
	movhps	-4760(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movq	-4832(%rbp), %xmm4
	movhps	-4776(%rbp), %xmm1
	movhps	-4792(%rbp), %xmm2
	movaps	%xmm0, -96(%rbp)
	movhps	-4808(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -128(%rbp)
	movhps	-4824(%rbp), %xmm4
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -4704(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4864(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1207
	call	_ZdlPv@PLT
.L1207:
	movq	-5072(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1203
	.p2align 4,,10
	.p2align 3
.L1389:
	movq	-5016(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4832(%rbp)
	movq	$0, -4824(%rbp)
	movq	$0, -4816(%rbp)
	movq	$0, -4808(%rbp)
	movq	$0, -4800(%rbp)
	movq	$0, -4792(%rbp)
	movq	$0, -4784(%rbp)
	movq	$0, -4776(%rbp)
	movq	$0, -4768(%rbp)
	movq	$0, -4760(%rbp)
	movq	$0, -4752(%rbp)
	movq	$0, -4736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-4736(%rbp), %rax
	movq	-4896(%rbp), %rdi
	pushq	%rax
	leaq	-4752(%rbp), %rax
	leaq	-4816(%rbp), %rcx
	pushq	%rax
	leaq	-4760(%rbp), %rax
	leaq	-4800(%rbp), %r9
	pushq	%rax
	leaq	-4768(%rbp), %rax
	leaq	-4808(%rbp), %r8
	pushq	%rax
	leaq	-4776(%rbp), %rax
	leaq	-4824(%rbp), %rdx
	pushq	%rax
	leaq	-4784(%rbp), %rax
	leaq	-4832(%rbp), %rsi
	pushq	%rax
	leaq	-4792(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_3SmiENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverES6_NS0_6UnionTIS5_NS0_10HeapNumberEEES8_S6_NS0_13JSArrayBufferENS0_12JSTypedArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSF_IS4_EEPNSF_IS5_EEPNSF_IS6_EEPNSF_IS7_EEPNSF_IS8_EESN_PNSF_ISB_EESR_SN_PNSF_ISC_EEPNSF_ISD_EE
	addq	$64, %rsp
	movl	$103, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm0, %xmm0
	leaq	-104(%rbp), %rdx
	movq	%r14, %rdi
	movq	-4832(%rbp), %rax
	leaq	-160(%rbp), %rsi
	movaps	%xmm0, -4704(%rbp)
	movq	$0, -4688(%rbp)
	movq	%rax, -160(%rbp)
	movq	-4824(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-4816(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-4808(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-4800(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-4792(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-4736(%rbp), %rax
	movq	%rax, -112(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4880(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1195
	call	_ZdlPv@PLT
.L1195:
	movq	-5144(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1194
	.p2align 4,,10
	.p2align 3
.L1388:
	movq	-5240(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4832(%rbp)
	movq	$0, -4824(%rbp)
	movq	$0, -4816(%rbp)
	movq	$0, -4808(%rbp)
	movq	$0, -4800(%rbp)
	movq	$0, -4792(%rbp)
	movq	$0, -4784(%rbp)
	movq	$0, -4776(%rbp)
	movq	$0, -4768(%rbp)
	movq	$0, -4760(%rbp)
	movq	$0, -4752(%rbp)
	movq	$0, -4736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-4736(%rbp), %rax
	movq	-5088(%rbp), %rdi
	pushq	%rax
	leaq	-4752(%rbp), %rax
	leaq	-4816(%rbp), %rcx
	pushq	%rax
	leaq	-4760(%rbp), %rax
	leaq	-4800(%rbp), %r9
	pushq	%rax
	leaq	-4768(%rbp), %rax
	leaq	-4808(%rbp), %r8
	pushq	%rax
	leaq	-4776(%rbp), %rax
	leaq	-4824(%rbp), %rdx
	pushq	%rax
	leaq	-4784(%rbp), %rax
	leaq	-4832(%rbp), %rsi
	pushq	%rax
	leaq	-4792(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_3SmiENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverES6_NS0_6UnionTIS5_NS0_10HeapNumberEEES8_S6_NS0_13JSArrayBufferENS0_12JSTypedArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSF_IS4_EEPNSF_IS5_EEPNSF_IS6_EEPNSF_IS7_EEPNSF_IS8_EESN_PNSF_ISB_EESR_SN_PNSF_ISC_EEPNSF_ISD_EE
	addq	$64, %rsp
	leaq	-64(%rbp), %rdx
	movq	%r14, %rdi
	movq	-4752(%rbp), %xmm0
	leaq	-160(%rbp), %rsi
	movq	-4768(%rbp), %xmm1
	movq	$0, -4688(%rbp)
	movq	-4784(%rbp), %xmm2
	movq	-4800(%rbp), %xmm3
	movq	-4816(%rbp), %xmm4
	movhps	-4736(%rbp), %xmm0
	movq	-4832(%rbp), %xmm5
	movhps	-4760(%rbp), %xmm1
	movhps	-4776(%rbp), %xmm2
	movhps	-4792(%rbp), %xmm3
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-4808(%rbp), %xmm4
	movhps	-4824(%rbp), %xmm5
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -4704(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4896(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1193
	call	_ZdlPv@PLT
.L1193:
	movq	-5016(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1192
	.p2align 4,,10
	.p2align 3
.L1387:
	movq	-5232(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4832(%rbp)
	movq	$0, -4824(%rbp)
	movq	$0, -4816(%rbp)
	movq	$0, -4808(%rbp)
	movq	$0, -4800(%rbp)
	movq	$0, -4792(%rbp)
	movq	$0, -4784(%rbp)
	movq	$0, -4776(%rbp)
	movq	$0, -4768(%rbp)
	movq	$0, -4760(%rbp)
	movq	$0, -4752(%rbp)
	movq	$0, -4736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-4736(%rbp), %rax
	movq	-5104(%rbp), %rdi
	pushq	%rax
	leaq	-4752(%rbp), %rax
	leaq	-4800(%rbp), %r9
	pushq	%rax
	leaq	-4760(%rbp), %rax
	leaq	-4808(%rbp), %r8
	pushq	%rax
	leaq	-4768(%rbp), %rax
	leaq	-4816(%rbp), %rcx
	pushq	%rax
	leaq	-4776(%rbp), %rax
	leaq	-4824(%rbp), %rdx
	pushq	%rax
	leaq	-4784(%rbp), %rax
	leaq	-4832(%rbp), %rsi
	pushq	%rax
	leaq	-4792(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_3SmiENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverES6_NS0_6UnionTIS5_NS0_10HeapNumberEEES8_S6_NS0_13JSArrayBufferENS0_12JSTypedArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSF_IS4_EEPNSF_IS5_EEPNSF_IS6_EEPNSF_IS7_EEPNSF_IS8_EESN_PNSF_ISB_EESR_SN_PNSF_ISC_EEPNSF_ISD_EE
	addq	$64, %rsp
	movl	$99, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4736(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler28LoadJSTypedArrayBackingStoreENS0_8compiler5TNodeINS0_12JSTypedArrayEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$100, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-4784(%rbp), %rcx
	call	_ZN2v88internal27TypedArrayBuiltinsAssembler11CallCMemsetENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEENS3_INS0_8UintPtrTEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$98, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-160(%rbp), %rsi
	leaq	-64(%rbp), %rdx
	movq	%r14, %rdi
	movq	-4752(%rbp), %xmm0
	movq	-4768(%rbp), %xmm1
	movq	$0, -4688(%rbp)
	movq	-4784(%rbp), %xmm2
	movq	-4800(%rbp), %xmm3
	movq	-4816(%rbp), %xmm4
	movhps	-4736(%rbp), %xmm0
	movq	-4832(%rbp), %xmm5
	movhps	-4760(%rbp), %xmm1
	movhps	-4776(%rbp), %xmm2
	movhps	-4792(%rbp), %xmm3
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-4808(%rbp), %xmm4
	movhps	-4824(%rbp), %xmm5
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -4704(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4896(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1191
	call	_ZdlPv@PLT
.L1191:
	movq	-5016(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1190
	.p2align 4,,10
	.p2align 3
.L1386:
	movq	-5080(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4816(%rbp)
	movq	$0, -4808(%rbp)
	movq	$0, -4800(%rbp)
	movq	$0, -4792(%rbp)
	movq	$0, -4784(%rbp)
	movq	$0, -4776(%rbp)
	movq	$0, -4768(%rbp)
	movq	$0, -4760(%rbp)
	movq	$0, -4752(%rbp)
	movq	$0, -4736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-4736(%rbp), %rax
	movq	-4992(%rbp), %rdi
	pushq	%rax
	leaq	-4752(%rbp), %rax
	leaq	-4800(%rbp), %rcx
	pushq	%rax
	leaq	-4760(%rbp), %rax
	leaq	-4784(%rbp), %r9
	pushq	%rax
	leaq	-4768(%rbp), %rax
	leaq	-4792(%rbp), %r8
	pushq	%rax
	leaq	-4776(%rbp), %rax
	leaq	-4808(%rbp), %rdx
	pushq	%rax
	leaq	-4816(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_3SmiENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverES6_NS0_6UnionTIS5_NS0_10HeapNumberEEES8_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EESL_PNSD_ISB_EESP_SL_
	addq	$48, %rsp
	movl	$91, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4768(%rbp), %rdx
	movq	-4816(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal27TypedArrayBuiltinsAssembler25AllocateEmptyOnHeapBufferENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_8UintPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$95, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$96, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4800(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal36Convert9ATuintptr13ATPositiveSmi_187EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$94, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pushq	%r12
	movq	%rbx, %r8
	movl	$1, %edx
	pushq	-4768(%rbp)
	movq	-4736(%rbp), %r9
	movq	%r13, %rdi
	movq	-4808(%rbp), %rcx
	movq	-4816(%rbp), %rsi
	call	_ZN2v88internal22AllocateTypedArray_341EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEbNS4_INS0_3MapEEENS4_INS0_13JSArrayBufferEEENS4_INS0_8UintPtrTEEESC_SC_
	movl	$98, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -4880(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	popq	%r9
	leaq	-160(%rbp), %rsi
	popq	%r10
	movq	-4752(%rbp), %xmm0
	leaq	-64(%rbp), %rdx
	movq	%r14, %rdi
	movq	-4768(%rbp), %xmm1
	movq	-4784(%rbp), %xmm2
	cmpb	$0, -5120(%rbp)
	movq	$0, -4688(%rbp)
	movhps	-4736(%rbp), %xmm0
	movhps	-4760(%rbp), %xmm1
	movq	-4800(%rbp), %xmm3
	movq	-4816(%rbp), %xmm4
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	movhps	-4776(%rbp), %xmm2
	movhps	-4880(%rbp), %xmm0
	movhps	-4792(%rbp), %xmm3
	movhps	-4808(%rbp), %xmm4
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -4704(%rbp)
	je	.L1186
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1187
	call	_ZdlPv@PLT
.L1187:
	movq	-5232(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2464(%rbp), %rax
	movq	%rax, -5088(%rbp)
	jmp	.L1185
	.p2align 4,,10
	.p2align 3
.L1396:
	movq	-5152(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$577875841032521479, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$11, %edi
	movq	$0, -4688(%rbp)
	movaps	%xmm0, -4704(%rbp)
	call	_Znwm@PLT
	movl	$1287, %r10d
	movq	-5120(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	11(%rax), %rdx
	movw	%r10w, 8(%rax)
	movb	$8, 10(%rax)
	movq	%rax, -4704(%rbp)
	movq	%rdx, -4688(%rbp)
	movq	%rdx, -4696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1220
	call	_ZdlPv@PLT
.L1220:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -672(%rbp)
	je	.L1221
.L1397:
	movq	-5160(%rbp), %rsi
	xorl	%r9d, %r9d
	movq	%r15, %rdi
	xorl	%ebx, %ebx
	leaq	-160(%rbp), %r12
	movq	%r9, -5184(%rbp)
	movq	$0, -5152(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-148(%rbp), %rdx
	movabsq	$577875841032521479, %rax
	movaps	%xmm0, -4704(%rbp)
	movq	%rax, -160(%rbp)
	movl	$117966087, -152(%rbp)
	movq	$0, -4688(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5072(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4704(%rbp), %rdi
	movq	-5184(%rbp), %r9
	testq	%rdi, %rdi
	je	.L1222
	movq	%rax, -5160(%rbp)
	call	_ZdlPv@PLT
	movq	-5184(%rbp), %r9
	movq	-5160(%rbp), %rax
.L1222:
	movq	(%rax), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	48(%rax), %rdx
	movq	(%rax), %r10
	movq	8(%rax), %rcx
	movq	16(%rax), %r8
	testq	%rdx, %rdx
	cmove	-5152(%rbp), %rdx
	movq	32(%rax), %xmm0
	movq	%r10, %xmm2
	movq	%rcx, %xmm7
	movq	%r8, %xmm1
	movq	%r10, -5232(%rbp)
	movq	%rdx, -5152(%rbp)
	movq	72(%rax), %rdx
	punpcklqdq	%xmm7, %xmm2
	movhps	24(%rax), %xmm1
	movhps	40(%rax), %xmm0
	movaps	%xmm2, -5216(%rbp)
	testq	%rdx, %rdx
	movaps	%xmm1, -5200(%rbp)
	cmovne	%rdx, %r9
	movq	88(%rax), %rdx
	movaps	%xmm0, -5184(%rbp)
	movq	%rcx, -5168(%rbp)
	testq	%rdx, %rdx
	movq	%r9, -5240(%rbp)
	cmovne	%rdx, %rbx
	movl	$117, %edx
	movq	%r8, -5160(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$118, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-5160(%rbp), %r8
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal36Convert9ATuintptr13ATPositiveSmi_187EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$116, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -5160(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-5160(%rbp), %rax
	movq	%rbx, %r8
	movq	-5232(%rbp), %r10
	movq	-5240(%rbp), %r9
	movq	-5168(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r13, %rdi
	pushq	%rax
	movq	%r10, %rsi
	pushq	-5152(%rbp)
	call	_ZN2v88internal22AllocateTypedArray_341EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEbNS4_INS0_3MapEEENS4_INS0_13JSArrayBufferEEENS4_INS0_8UintPtrTEEESC_SC_
	leaq	-104(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqa	-5184(%rbp), %xmm0
	movdqa	-5216(%rbp), %xmm2
	movq	%rax, -112(%rbp)
	movdqa	-5200(%rbp), %xmm1
	movq	$0, -4688(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -4704(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4880(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4704(%rbp), %rdi
	popq	%r8
	popq	%r9
	testq	%rdi, %rdi
	je	.L1226
	call	_ZdlPv@PLT
.L1226:
	movq	-5144(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1221
	.p2align 4,,10
	.p2align 3
.L1395:
	movq	-5184(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-160(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-147(%rbp), %rdx
	movabsq	$577875841032521479, %rax
	movaps	%xmm0, -4704(%rbp)
	movq	%rax, -160(%rbp)
	movl	$134743303, -152(%rbp)
	movb	$7, -148(%rbp)
	movq	$0, -4688(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5016(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4704(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1217
	call	_ZdlPv@PLT
.L1217:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	24(%rax), %r11
	movq	32(%rax), %r10
	movq	40(%rax), %r9
	movq	48(%rax), %r8
	movq	%rcx, -5072(%rbp)
	movq	16(%rax), %rcx
	movq	56(%rax), %rdi
	movq	64(%rax), %rsi
	movq	80(%rax), %rdx
	movq	%rcx, -5184(%rbp)
	movq	72(%rax), %rcx
	movq	96(%rax), %rax
	movq	%rbx, -160(%rbp)
	movq	-5072(%rbp), %rbx
	movq	%rdi, -104(%rbp)
	movq	%r14, %rdi
	movq	%rsi, -96(%rbp)
	movq	%r12, %rsi
	movq	%rbx, -152(%rbp)
	movq	-5184(%rbp), %rbx
	movq	%rdx, -80(%rbp)
	leaq	-64(%rbp), %rdx
	movq	%r11, -136(%rbp)
	movq	%r10, -128(%rbp)
	movq	%r9, -120(%rbp)
	movq	%r8, -112(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%rax, -72(%rbp)
	movaps	%xmm0, -4704(%rbp)
	movq	%rbx, -144(%rbp)
	movq	$0, -4688(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-736(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -5072(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1218
	call	_ZdlPv@PLT
.L1218:
	movq	-5160(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1216
	.p2align 4,,10
	.p2align 3
.L1394:
	movq	-5248(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-160(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-148(%rbp), %rdx
	movabsq	$577875841032521479, %rax
	movaps	%xmm0, -4704(%rbp)
	movq	%rax, -160(%rbp)
	movl	$134743303, -152(%rbp)
	movq	$0, -4688(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4912(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4704(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1214
	call	_ZdlPv@PLT
.L1214:
	movq	(%rbx), %rax
	leaq	-72(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqu	64(%rax), %xmm0
	movdqu	(%rax), %xmm4
	movdqu	16(%rax), %xmm3
	movdqu	32(%rax), %xmm2
	movdqu	48(%rax), %xmm1
	movq	80(%rax), %rax
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -160(%rbp)
	movq	%rax, -80(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -4704(%rbp)
	movq	$0, -4688(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-928(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -5120(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1215
	call	_ZdlPv@PLT
.L1215:
	movq	-5152(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1213
	.p2align 4,,10
	.p2align 3
.L1393:
	movq	-5072(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$577875841032521479, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$11, %edi
	movq	$0, -4688(%rbp)
	movaps	%xmm0, -4704(%rbp)
	call	_Znwm@PLT
	movl	$1287, %r11d
	movq	-4864(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	11(%rax), %rdx
	movw	%r11w, 8(%rax)
	movb	$8, 10(%rax)
	movq	%rax, -4704(%rbp)
	movq	%rdx, -4688(%rbp)
	movq	%rdx, -4696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4704(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1209
	call	_ZdlPv@PLT
.L1209:
	movq	(%rbx), %rax
	movq	8(%rax), %rbx
	movq	40(%rax), %rsi
	movq	56(%rax), %rdx
	movq	24(%rax), %rcx
	movq	72(%rax), %rdi
	movq	(%rax), %r12
	movq	%rbx, -4912(%rbp)
	movq	%rsi, -5200(%rbp)
	movq	16(%rax), %rbx
	movq	48(%rax), %rsi
	movq	%rdx, -5168(%rbp)
	movq	64(%rax), %rdx
	movq	%rcx, -5072(%rbp)
	movq	32(%rax), %rcx
	movq	%rbx, -5016(%rbp)
	movq	%rsi, -5216(%rbp)
	movq	80(%rax), %rbx
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -5232(%rbp)
	movl	$114, %edx
	movq	%rdi, -5240(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -5120(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal24Cast13JSArrayBuffer_1473EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	movq	%rbx, %xmm6
	movq	%r12, %xmm1
	movq	-5232(%rbp), %xmm7
	leaq	-4736(%rbp), %rbx
	punpcklqdq	%xmm6, %xmm6
	pxor	%xmm0, %xmm0
	movq	-5216(%rbp), %xmm5
	movq	-5120(%rbp), %xmm4
	leaq	-56(%rbp), %rdx
	movq	%rbx, %rdi
	movq	-5016(%rbp), %xmm2
	leaq	-160(%rbp), %r12
	movhps	-5240(%rbp), %xmm7
	movq	%rax, -64(%rbp)
	movhps	-5168(%rbp), %xmm5
	movhps	-5072(%rbp), %xmm2
	movhps	-5200(%rbp), %xmm4
	movq	%r12, %rsi
	movaps	%xmm6, -5264(%rbp)
	movhps	-4912(%rbp), %xmm1
	movaps	%xmm7, -5232(%rbp)
	movaps	%xmm5, -5216(%rbp)
	movaps	%xmm4, -5200(%rbp)
	movaps	%xmm2, -5120(%rbp)
	movaps	%xmm1, -5072(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm6, -80(%rbp)
	movaps	%xmm0, -4736(%rbp)
	movq	$0, -4720(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1120(%rbp), %rax
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -5016(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4736(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1210
	call	_ZdlPv@PLT
.L1210:
	movq	-5184(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1312(%rbp), %rax
	cmpq	$0, -4696(%rbp)
	movq	%rax, -4912(%rbp)
	jne	.L1400
.L1211:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L1208
	.p2align 4,,10
	.p2align 3
.L1398:
	movq	-5144(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -4688(%rbp)
	movaps	%xmm0, -4704(%rbp)
	call	_Znwm@PLT
	movl	$1796, %esi
	movq	-4880(%rbp), %rdi
	movw	%si, 4(%rax)
	leaq	7(%rax), %rdx
	movq	%r14, %rsi
	movl	$84281095, (%rax)
	movb	$7, 6(%rax)
	movq	%rax, -4704(%rbp)
	movq	%rdx, -4688(%rbp)
	movq	%rdx, -4696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4704(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1228
	call	_ZdlPv@PLT
.L1228:
	movq	(%rbx), %rax
	movl	$73, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	8(%rax), %rbx
	movq	(%rax), %r13
	movq	40(%rax), %rcx
	movq	16(%rax), %r12
	movq	%rbx, -5144(%rbp)
	movq	24(%rax), %rbx
	movq	%rcx, -5184(%rbp)
	movq	%rbx, -5152(%rbp)
	movq	32(%rax), %rbx
	movq	%rbx, -5160(%rbp)
	movq	48(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %xmm0
	leaq	-104(%rbp), %rdx
	movq	%r14, %rdi
	movhps	-5144(%rbp), %xmm0
	movq	%rbx, -112(%rbp)
	leaq	-160(%rbp), %rsi
	movaps	%xmm0, -160(%rbp)
	movq	%r12, %xmm0
	leaq	-352(%rbp), %r12
	movhps	-5152(%rbp), %xmm0
	movq	$0, -4688(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	-5160(%rbp), %xmm0
	movhps	-5184(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -4704(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1229
	call	_ZdlPv@PLT
.L1229:
	movq	-5128(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1227
	.p2align 4,,10
	.p2align 3
.L1186:
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5088(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1189
	call	_ZdlPv@PLT
.L1189:
	movq	-5240(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2656(%rbp), %rax
	movq	%rax, -5104(%rbp)
	jmp	.L1185
	.p2align 4,,10
	.p2align 3
.L1197:
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-144(%rbp), %xmm5
	leaq	80(%rax), %rdx
	movq	%rax, -4704(%rbp)
	movdqa	-128(%rbp), %xmm6
	movq	%rdx, -4688(%rbp)
	movq	-5080(%rbp), %rdi
	movups	%xmm7, (%rax)
	movdqa	-112(%rbp), %xmm7
	movups	%xmm5, 16(%rax)
	movdqa	-96(%rbp), %xmm5
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rdx, -4696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1200
	call	_ZdlPv@PLT
.L1200:
	movq	-5168(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1888(%rbp), %rax
	movq	%rax, -5024(%rbp)
	jmp	.L1196
	.p2align 4,,10
	.p2align 3
.L1204:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IsNullConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L1206
	movq	%r12, %xmm0
	movq	%rbx, %rdi
	movhps	-4912(%rbp), %xmm0
	movaps	%xmm0, -5200(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	movq	%rax, %rsi
	jmp	.L1376
	.p2align 4,,10
	.p2align 3
.L1400:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-64(%rbp), %rdx
	movq	%r12, %rsi
	movdqa	-5072(%rbp), %xmm6
	movdqa	-5120(%rbp), %xmm7
	movq	%rbx, %rdi
	movaps	%xmm0, -4736(%rbp)
	movdqa	-5232(%rbp), %xmm5
	movq	$0, -4720(%rbp)
	movaps	%xmm6, -160(%rbp)
	movdqa	-5200(%rbp), %xmm6
	movaps	%xmm7, -144(%rbp)
	movdqa	-5216(%rbp), %xmm7
	movaps	%xmm6, -128(%rbp)
	movdqa	-5264(%rbp), %xmm6
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm6, -80(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4912(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4736(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1212
	call	_ZdlPv@PLT
.L1212:
	movq	-5248(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1211
.L1399:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22426:
	.size	_ZN2v88internal24TypedArrayInitialize_342EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEbNS4_INS0_3MapEEENS4_INS0_3SmiEEENS0_34TorqueStructTypedArrayElementsInfoENS4_INS0_10JSReceiverEEE, .-_ZN2v88internal24TypedArrayInitialize_342EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEbNS4_INS0_3MapEEENS4_INS0_3SmiEEENS0_34TorqueStructTypedArrayElementsInfoENS4_INS0_10JSReceiverEEE
	.section	.text._ZN2v88internal21ConstructByLength_343EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3MapEEENS4_INS0_6ObjectEEENS0_34TorqueStructTypedArrayElementsInfoE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21ConstructByLength_343EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3MapEEENS4_INS0_6ObjectEEENS0_34TorqueStructTypedArrayElementsInfoE
	.type	_ZN2v88internal21ConstructByLength_343EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3MapEEENS4_INS0_6ObjectEEENS0_34TorqueStructTypedArrayElementsInfoE, @function
_ZN2v88internal21ConstructByLength_343EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3MapEEENS4_INS0_6ObjectEEENS0_34TorqueStructTypedArrayElementsInfoE:
.LFB22460:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1416(%rbp), %r15
	leaq	-1472(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1640(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$1752, %rsp
	movq	%r9, -1728(%rbp)
	movq	%rdi, -1696(%rbp)
	movq	%rcx, -1760(%rbp)
	movq	%r8, -1744(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1640(%rbp)
	movq	%rdi, -1472(%rbp)
	movl	$120, %edi
	movq	$0, -1464(%rbp)
	movq	$0, -1456(%rbp)
	movq	$0, -1448(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r15, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -1448(%rbp)
	movq	%rdx, -1456(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1432(%rbp)
	movq	%rax, -1464(%rbp)
	movq	$0, -1440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1640(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1272(%rbp)
	movq	$0, -1264(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -1256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1272(%rbp)
	leaq	-1224(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1256(%rbp)
	movq	%rdx, -1264(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1240(%rbp)
	movq	%rax, -1704(%rbp)
	movq	$0, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1640(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1080(%rbp)
	movq	$0, -1072(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$0, -1064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1080(%rbp)
	leaq	-1032(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1064(%rbp)
	movq	%rdx, -1072(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1048(%rbp)
	movq	%rax, -1664(%rbp)
	movq	$0, -1056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1640(%rbp), %rax
	movl	$144, %edi
	movq	$0, -888(%rbp)
	movq	$0, -880(%rbp)
	movq	%rax, -896(%rbp)
	movq	$0, -872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -888(%rbp)
	leaq	-840(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -872(%rbp)
	movq	%rdx, -880(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -856(%rbp)
	movq	%rax, -1680(%rbp)
	movq	$0, -864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1640(%rbp), %rax
	movl	$168, %edi
	movq	$0, -696(%rbp)
	movq	$0, -688(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -696(%rbp)
	leaq	-648(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -680(%rbp)
	movq	%rdx, -688(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -664(%rbp)
	movq	%rax, -1672(%rbp)
	movq	$0, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1640(%rbp), %rax
	movl	$144, %edi
	movq	$0, -504(%rbp)
	movq	$0, -496(%rbp)
	movq	%rax, -512(%rbp)
	movq	$0, -488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -504(%rbp)
	leaq	-456(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -488(%rbp)
	movq	%rdx, -496(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -472(%rbp)
	movq	%rax, -1688(%rbp)
	movq	$0, -480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1640(%rbp), %rax
	movl	$144, %edi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -312(%rbp)
	leaq	-264(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	xorl	%edx, %edx
	movq	%rax, -1656(%rbp)
	movups	%xmm0, -280(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1760(%rbp), %r10
	movq	-1744(%rbp), %r11
	pxor	%xmm0, %xmm0
	movq	-1728(%rbp), %r9
	movl	$40, %edi
	movq	%r13, -128(%rbp)
	leaq	-1600(%rbp), %r13
	movq	%r10, -112(%rbp)
	movq	%r11, -104(%rbp)
	movq	%r9, -96(%rbp)
	movaps	%xmm0, -1600(%rbp)
	movq	%rbx, -120(%rbp)
	movq	$0, -1584(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm4
	leaq	40(%rax), %rdx
	movq	%rax, -1600(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1402
	call	_ZdlPv@PLT
.L1402:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1408(%rbp)
	jne	.L1562
	cmpq	$0, -1216(%rbp)
	jne	.L1563
.L1408:
	cmpq	$0, -1024(%rbp)
	jne	.L1564
.L1411:
	cmpq	$0, -832(%rbp)
	jne	.L1565
.L1414:
	cmpq	$0, -640(%rbp)
	jne	.L1566
.L1416:
	cmpq	$0, -448(%rbp)
	leaq	-320(%rbp), %r14
	jne	.L1567
.L1420:
	movq	-1656(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -1584(%rbp)
	movaps	%xmm0, -1600(%rbp)
	call	_Znwm@PLT
	movl	$1796, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$84412167, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -1600(%rbp)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1600(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1423
	call	_ZdlPv@PLT
.L1423:
	movq	(%rbx), %rax
	movq	-1656(%rbp), %rdi
	movq	40(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1424
	call	_ZdlPv@PLT
.L1424:
	movq	-304(%rbp), %rbx
	movq	-312(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1425
	.p2align 4,,10
	.p2align 3
.L1429:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1426
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%rbx, %r14
	jne	.L1429
.L1427:
	movq	-312(%rbp), %r14
.L1425:
	testq	%r14, %r14
	je	.L1430
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1430:
	movq	-1688(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1431
	call	_ZdlPv@PLT
.L1431:
	movq	-496(%rbp), %rbx
	movq	-504(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1432
	.p2align 4,,10
	.p2align 3
.L1436:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1433
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1436
.L1434:
	movq	-504(%rbp), %r14
.L1432:
	testq	%r14, %r14
	je	.L1437
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1437:
	movq	-1672(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1438
	call	_ZdlPv@PLT
.L1438:
	movq	-688(%rbp), %rbx
	movq	-696(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1439
	.p2align 4,,10
	.p2align 3
.L1443:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1440
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1443
.L1441:
	movq	-696(%rbp), %r14
.L1439:
	testq	%r14, %r14
	je	.L1444
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1444:
	movq	-1680(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-864(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1445
	call	_ZdlPv@PLT
.L1445:
	movq	-880(%rbp), %rbx
	movq	-888(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1446
	.p2align 4,,10
	.p2align 3
.L1450:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1447
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1450
.L1448:
	movq	-888(%rbp), %r14
.L1446:
	testq	%r14, %r14
	je	.L1451
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1451:
	movq	-1664(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1452
	call	_ZdlPv@PLT
.L1452:
	movq	-1072(%rbp), %rbx
	movq	-1080(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1453
	.p2align 4,,10
	.p2align 3
.L1457:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1454
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1457
.L1455:
	movq	-1080(%rbp), %r14
.L1453:
	testq	%r14, %r14
	je	.L1458
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1458:
	movq	-1704(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1459
	call	_ZdlPv@PLT
.L1459:
	movq	-1264(%rbp), %rbx
	movq	-1272(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1460
	.p2align 4,,10
	.p2align 3
.L1464:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1461
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1464
.L1462:
	movq	-1272(%rbp), %r14
.L1460:
	testq	%r14, %r14
	je	.L1465
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1465:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1440(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1466
	call	_ZdlPv@PLT
.L1466:
	movq	-1456(%rbp), %rbx
	movq	-1464(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1467
	.p2align 4,,10
	.p2align 3
.L1471:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1468
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1471
.L1469:
	movq	-1464(%rbp), %r14
.L1467:
	testq	%r14, %r14
	je	.L1472
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1472:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1568
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1468:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1471
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1461:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1464
	jmp	.L1462
	.p2align 4,,10
	.p2align 3
.L1454:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1457
	jmp	.L1455
	.p2align 4,,10
	.p2align 3
.L1447:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1450
	jmp	.L1448
	.p2align 4,,10
	.p2align 3
.L1440:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1443
	jmp	.L1441
	.p2align 4,,10
	.p2align 3
.L1426:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1429
	jmp	.L1427
	.p2align 4,,10
	.p2align 3
.L1433:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1436
	jmp	.L1434
	.p2align 4,,10
	.p2align 3
.L1562:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1584(%rbp)
	movaps	%xmm0, -1600(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$84412167, (%rax)
	leaq	5(%rax), %rdx
	movb	$4, 4(%rax)
	movq	%rax, -1600(%rbp)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1600(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1404
	call	_ZdlPv@PLT
.L1404:
	movq	(%rbx), %rax
	movl	$128, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r14
	movq	%rcx, -1744(%rbp)
	movq	24(%rax), %rcx
	movq	32(%rax), %rax
	movq	%rcx, -1760(%rbp)
	movq	%rax, -1776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1696(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rbx, -1728(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16ToInteger_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_23ToIntegerTruncationModeE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$127, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$133, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1696(%rbp), %rdi
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal22Cast13ATPositiveSmi_84EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm4
	movq	%r14, %xmm6
	movq	-1776(%rbp), %xmm5
	movhps	-1760(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	-1728(%rbp), %xmm7
	punpcklqdq	%xmm4, %xmm5
	movaps	%xmm6, -1760(%rbp)
	leaq	-1632(%rbp), %r14
	movhps	-1744(%rbp), %xmm7
	movaps	%xmm5, -1776(%rbp)
	movaps	%xmm7, -1728(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm0, -1632(%rbp)
	movq	%rbx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -1616(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm1
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm2
	leaq	64(%rax), %rdx
	leaq	-1088(%rbp), %rdi
	movq	%rax, -1632(%rbp)
	movups	%xmm1, (%rax)
	movdqa	-96(%rbp), %xmm1
	movups	%xmm4, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm2, 48(%rax)
	movq	%rdx, -1616(%rbp)
	movq	%rdx, -1624(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1632(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1405
	call	_ZdlPv@PLT
.L1405:
	movq	-1664(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1592(%rbp)
	jne	.L1569
.L1406:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1216(%rbp)
	je	.L1408
.L1563:
	movq	-1704(%rbp), %rsi
	movq	%r12, %rdi
	movl	$2052, %ebx
	leaq	-1280(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -1584(%rbp)
	movaps	%xmm0, -1600(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%bx, 4(%rax)
	leaq	7(%rax), %rdx
	movl	$84412167, (%rax)
	movb	$8, 6(%rax)
	movq	%rax, -1600(%rbp)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1600(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1409
	call	_ZdlPv@PLT
.L1409:
	movq	(%rbx), %rax
	movl	$48, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movdqu	(%rax), %xmm5
	movq	$0, -1584(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -1600(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm4
	leaq	48(%rax), %rdx
	leaq	-896(%rbp), %rdi
	movq	%rax, -1600(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1410
	call	_ZdlPv@PLT
.L1410:
	movq	-1680(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1024(%rbp)
	je	.L1411
.L1564:
	movq	-1664(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1088(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -1584(%rbp)
	movaps	%xmm0, -1600(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$434606177398556423, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -1600(%rbp)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1600(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1412
	call	_ZdlPv@PLT
.L1412:
	movq	(%rbx), %rax
	movl	$56, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movq	56(%rax), %rdx
	movdqu	(%rax), %xmm2
	movq	$0, -1584(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -80(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -1600(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm1
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm5
	leaq	56(%rax), %rdx
	leaq	-704(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm1, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rax, -1600(%rbp)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1413
	call	_ZdlPv@PLT
.L1413:
	movq	-1672(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -832(%rbp)
	je	.L1414
.L1565:
	movq	-1680(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-896(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -1584(%rbp)
	movaps	%xmm0, -1600(%rbp)
	call	_Znwm@PLT
	movl	$2052, %r11d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$84412167, (%rax)
	leaq	6(%rax), %rdx
	movw	%r11w, 4(%rax)
	movq	%rax, -1600(%rbp)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1600(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1415
	call	_ZdlPv@PLT
.L1415:
	movq	(%rbx), %rax
	movl	$134, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r14
	movq	16(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1696(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$206, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ThrowRangeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -640(%rbp)
	je	.L1416
.L1566:
	movq	-1672(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-704(%rbp), %r14
	xorl	%ebx, %ebx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -1584(%rbp)
	movaps	%xmm0, -1600(%rbp)
	call	_Znwm@PLT
	movl	$2052, %r10d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$84412167, (%rax)
	leaq	7(%rax), %rdx
	movw	%r10w, 4(%rax)
	movb	$6, 6(%rax)
	movq	%rax, -1600(%rbp)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1600(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L1417
	call	_ZdlPv@PLT
.L1417:
	movq	(%r14), %rax
	movl	$133, %edx
	movq	%r12, %rdi
	movq	32(%rax), %rsi
	movq	24(%rax), %xmm1
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	%rsi, -1728(%rbp)
	movq	16(%rax), %xmm0
	movdqa	%xmm1, %xmm3
	leaq	.LC2(%rip), %rsi
	movq	48(%rax), %rax
	movq	%rcx, %xmm6
	movq	%r14, %xmm2
	movq	%rcx, -1712(%rbp)
	punpcklqdq	%xmm6, %xmm2
	punpcklqdq	%xmm1, %xmm0
	testq	%rax, %rax
	movhps	-1728(%rbp), %xmm3
	movaps	%xmm2, -1792(%rbp)
	cmovne	%rax, %rbx
	movaps	%xmm3, -1744(%rbp)
	movaps	%xmm0, -1776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$135, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1696(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal26GetArrayBufferFunction_220EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEE@PLT
	movl	$138, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -1760(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$137, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pushq	-1736(%rbp)
	movq	%rbx, %r8
	movq	-1760(%rbp), %r9
	pushq	-1744(%rbp)
	movl	$1, %edx
	movq	%r14, %rsi
	movq	-1712(%rbp), %rcx
	movq	-1696(%rbp), %rdi
	call	_ZN2v88internal24TypedArrayInitialize_342EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEbNS4_INS0_3MapEEENS4_INS0_3SmiEEENS0_34TorqueStructTypedArrayElementsInfoENS4_INS0_10JSReceiverEEE
	movdqa	-1776(%rbp), %xmm0
	movl	$48, %edi
	movdqa	-1792(%rbp), %xmm2
	movq	-1728(%rbp), %rsi
	movq	%rax, -88(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rsi, -96(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm0, -1600(%rbp)
	movq	$0, -1584(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm1
	leaq	48(%rax), %rdx
	leaq	-512(%rbp), %rdi
	movq	%rax, -1600(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm1, 32(%rax)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1600(%rbp), %rdi
	popq	%r8
	popq	%r9
	testq	%rdi, %rdi
	je	.L1419
	call	_ZdlPv@PLT
.L1419:
	movq	-1688(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1416
	.p2align 4,,10
	.p2align 3
.L1567:
	movq	-1688(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-512(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -1584(%rbp)
	movaps	%xmm0, -1600(%rbp)
	call	_Znwm@PLT
	movl	$1796, %esi
	movq	%r14, %rdi
	movw	%si, 4(%rax)
	leaq	6(%rax), %rdx
	movq	%r13, %rsi
	movl	$84412167, (%rax)
	movq	%rax, -1600(%rbp)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1600(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1421
	call	_ZdlPv@PLT
.L1421:
	movq	(%rbx), %rax
	movl	$124, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %r14
	movq	32(%rax), %rbx
	movq	%rsi, -1728(%rbp)
	movq	24(%rax), %rsi
	movq	40(%rax), %rax
	movq	%rcx, -1696(%rbp)
	movq	%rsi, -1744(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rax, -1760(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$48, %edi
	movq	-1696(%rbp), %xmm0
	movq	$0, -1584(%rbp)
	movhps	-1728(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%r14, %xmm0
	leaq	-320(%rbp), %r14
	movhps	-1744(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%rbx, %xmm0
	movhps	-1760(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1600(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm2
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm5
	leaq	48(%rax), %rdx
	movq	%rax, -1600(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1422
	call	_ZdlPv@PLT
.L1422:
	movq	-1656(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1420
	.p2align 4,,10
	.p2align 3
.L1569:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %rsi
	movq	%r14, %rdi
	movdqa	-1728(%rbp), %xmm6
	movdqa	-1760(%rbp), %xmm7
	leaq	-72(%rbp), %rdx
	movaps	%xmm0, -1632(%rbp)
	movdqa	-1776(%rbp), %xmm4
	movq	%rbx, -80(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movq	$0, -1616(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1280(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1632(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1407
	call	_ZdlPv@PLT
.L1407:
	movq	-1704(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1406
.L1568:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22460:
	.size	_ZN2v88internal21ConstructByLength_343EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3MapEEENS4_INS0_6ObjectEEENS0_34TorqueStructTypedArrayElementsInfoE, .-_ZN2v88internal21ConstructByLength_343EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3MapEEENS4_INS0_6ObjectEEENS0_34TorqueStructTypedArrayElementsInfoE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_10HeapObjectENS0_6ObjectENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverENS0_3SmiENS0_12JSTypedArrayESB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EESV_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_10HeapObjectENS0_6ObjectENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverENS0_3SmiENS0_12JSTypedArrayESB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EESV_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_10HeapObjectENS0_6ObjectENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverENS0_3SmiENS0_12JSTypedArrayESB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EESV_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_10HeapObjectENS0_6ObjectENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverENS0_3SmiENS0_12JSTypedArrayESB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EESV_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_10HeapObjectENS0_6ObjectENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverENS0_3SmiENS0_12JSTypedArrayESB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EESV_:
.LFB27558:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$10, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$434320308720568071, %rcx
	movq	%rcx, (%rax)
	movl	$1799, %ecx
	leaq	10(%rax), %rdx
	movw	%cx, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1571
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %rax
.L1571:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1572
	movq	%rdx, (%r15)
.L1572:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1573
	movq	%rdx, (%r14)
.L1573:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1574
	movq	%rdx, 0(%r13)
.L1574:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1575
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1575:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1576
	movq	%rdx, (%rbx)
.L1576:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1577
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1577:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1578
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1578:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1579
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1579:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1580
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1580:
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L1570
	movq	-128(%rbp), %rsi
	movq	%rax, (%rsi)
.L1570:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1617
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1617:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27558:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_10HeapObjectENS0_6ObjectENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverENS0_3SmiENS0_12JSTypedArrayESB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EESV_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_10HeapObjectENS0_6ObjectENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverENS0_3SmiENS0_12JSTypedArrayESB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EESV_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_10HeapObjectENS0_6ObjectENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverENS0_3SmiENS0_12JSTypedArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_10HeapObjectENS0_6ObjectENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverENS0_3SmiENS0_12JSTypedArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_10HeapObjectENS0_6ObjectENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverENS0_3SmiENS0_12JSTypedArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_10HeapObjectENS0_6ObjectENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverENS0_3SmiENS0_12JSTypedArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_10HeapObjectENS0_6ObjectENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverENS0_3SmiENS0_12JSTypedArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EE:
.LFB27560:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$9, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$434320308720568071, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1619
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
.L1619:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1620
	movq	%rdx, (%r15)
.L1620:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1621
	movq	%rdx, (%r14)
.L1621:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1622
	movq	%rdx, 0(%r13)
.L1622:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1623
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1623:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1624
	movq	%rdx, (%rbx)
.L1624:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1625
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1625:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1626
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1626:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1627
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1627:
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L1618
	movq	-120(%rbp), %rbx
	movq	%rax, (%rbx)
.L1618:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1661
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1661:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27560:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_10HeapObjectENS0_6ObjectENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverENS0_3SmiENS0_12JSTypedArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_10HeapObjectENS0_6ObjectENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverENS0_3SmiENS0_12JSTypedArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EE
	.section	.rodata._ZN2v88internal24ConstructByArrayLike_344EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3MapEEENS4_INS0_10HeapObjectEEENS4_INS0_6ObjectEEENS0_34TorqueStructTypedArrayElementsInfoENS4_INS0_10JSReceiverEEE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"Construct"
	.section	.text._ZN2v88internal24ConstructByArrayLike_344EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3MapEEENS4_INS0_10HeapObjectEEENS4_INS0_6ObjectEEENS0_34TorqueStructTypedArrayElementsInfoENS4_INS0_10JSReceiverEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24ConstructByArrayLike_344EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3MapEEENS4_INS0_10HeapObjectEEENS4_INS0_6ObjectEEENS0_34TorqueStructTypedArrayElementsInfoENS4_INS0_10JSReceiverEEE
	.type	_ZN2v88internal24ConstructByArrayLike_344EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3MapEEENS4_INS0_10HeapObjectEEENS4_INS0_6ObjectEEENS0_34TorqueStructTypedArrayElementsInfoENS4_INS0_10JSReceiverEEE, @function
_ZN2v88internal24ConstructByArrayLike_344EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3MapEEENS4_INS0_10HeapObjectEEENS4_INS0_6ObjectEEENS0_34TorqueStructTypedArrayElementsInfoENS4_INS0_10JSReceiverEEE:
.LFB22479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$632, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %r10
	movq	24(%rbp), %r11
	movq	%r10, -4480(%rbp)
	movq	%rsi, %r13
	movq	%r8, %r15
	movq	%rdx, %rbx
	movq	%r11, -4432(%rbp)
	leaq	-4416(%rbp), %r14
	leaq	-4120(%rbp), %r12
	movq	%r9, -4464(%rbp)
	movq	%rdi, -4424(%rbp)
	movq	%rcx, -4448(%rbp)
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	movq	%rdi, -4416(%rbp)
	movq	%rdi, -4176(%rbp)
	movl	$168, %edi
	movq	$0, -4168(%rbp)
	movq	$0, -4160(%rbp)
	movq	$0, -4152(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -4152(%rbp)
	movq	%rdx, -4160(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4136(%rbp)
	movq	%rax, -4168(%rbp)
	movq	$0, -4144(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4416(%rbp), %rax
	movl	$192, %edi
	movq	$0, -3976(%rbp)
	movq	$0, -3968(%rbp)
	movq	%rax, -3984(%rbp)
	movq	$0, -3960(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -3976(%rbp)
	leaq	-3928(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3960(%rbp)
	movq	%rdx, -3968(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3944(%rbp)
	movq	%rax, -4512(%rbp)
	movq	$0, -3952(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4416(%rbp), %rax
	movl	$216, %edi
	movq	$0, -3784(%rbp)
	movq	$0, -3776(%rbp)
	movq	%rax, -3792(%rbp)
	movq	$0, -3768(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -3784(%rbp)
	leaq	-3736(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3768(%rbp)
	movq	%rdx, -3776(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3752(%rbp)
	movq	%rax, -4544(%rbp)
	movq	$0, -3760(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4416(%rbp), %rax
	movl	$168, %edi
	movq	$0, -3592(%rbp)
	movq	$0, -3584(%rbp)
	movq	%rax, -3600(%rbp)
	movq	$0, -3576(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -3592(%rbp)
	leaq	-3544(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3576(%rbp)
	movq	%rdx, -3584(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3560(%rbp)
	movq	%rax, -4560(%rbp)
	movq	$0, -3568(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4416(%rbp), %rax
	movl	$192, %edi
	movq	$0, -3400(%rbp)
	movq	$0, -3392(%rbp)
	movq	%rax, -3408(%rbp)
	movq	$0, -3384(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -3400(%rbp)
	leaq	-3352(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3384(%rbp)
	movq	%rdx, -3392(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3368(%rbp)
	movq	%rax, -4576(%rbp)
	movq	$0, -3376(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4416(%rbp), %rax
	movl	$240, %edi
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	%rax, -3216(%rbp)
	movq	$0, -3192(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -3208(%rbp)
	leaq	-3160(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3192(%rbp)
	movq	%rdx, -3200(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3176(%rbp)
	movq	%rax, -4720(%rbp)
	movq	$0, -3184(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4416(%rbp), %rax
	movl	$264, %edi
	movq	$0, -3016(%rbp)
	movq	$0, -3008(%rbp)
	movq	%rax, -3024(%rbp)
	movq	$0, -3000(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -3016(%rbp)
	leaq	-2968(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3000(%rbp)
	movq	%rdx, -3008(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2984(%rbp)
	movq	%rax, -4496(%rbp)
	movq	$0, -2992(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4416(%rbp), %rax
	movl	$240, %edi
	movq	$0, -2824(%rbp)
	movq	$0, -2816(%rbp)
	movq	%rax, -2832(%rbp)
	movq	$0, -2808(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -2824(%rbp)
	leaq	-2776(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2808(%rbp)
	movq	%rdx, -2816(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2792(%rbp)
	movq	%rax, -4672(%rbp)
	movq	$0, -2800(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4416(%rbp), %rax
	movl	$240, %edi
	movq	$0, -2632(%rbp)
	movq	$0, -2624(%rbp)
	movq	%rax, -2640(%rbp)
	movq	$0, -2616(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -2632(%rbp)
	leaq	-2584(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2616(%rbp)
	movq	%rdx, -2624(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2600(%rbp)
	movq	%rax, -4592(%rbp)
	movq	$0, -2608(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4416(%rbp), %rax
	movl	$240, %edi
	movq	$0, -2440(%rbp)
	movq	$0, -2432(%rbp)
	movq	%rax, -2448(%rbp)
	movq	$0, -2424(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -2440(%rbp)
	leaq	-2392(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2424(%rbp)
	movq	%rdx, -2432(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2408(%rbp)
	movq	%rax, -4488(%rbp)
	movq	$0, -2416(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4416(%rbp), %rax
	movl	$240, %edi
	movq	$0, -2248(%rbp)
	movq	$0, -2240(%rbp)
	movq	%rax, -2256(%rbp)
	movq	$0, -2232(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -2248(%rbp)
	leaq	-2200(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2232(%rbp)
	movq	%rdx, -2240(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2216(%rbp)
	movq	%rax, -4624(%rbp)
	movq	$0, -2224(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4416(%rbp), %rax
	movl	$240, %edi
	movq	$0, -2056(%rbp)
	movq	$0, -2048(%rbp)
	movq	%rax, -2064(%rbp)
	movq	$0, -2040(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -2056(%rbp)
	leaq	-2008(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2040(%rbp)
	movq	%rdx, -2048(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2024(%rbp)
	movq	%rax, -4680(%rbp)
	movq	$0, -2032(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4416(%rbp), %rax
	movl	$240, %edi
	movq	$0, -1864(%rbp)
	movq	$0, -1856(%rbp)
	movq	%rax, -1872(%rbp)
	movq	$0, -1848(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -1864(%rbp)
	leaq	-1816(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1848(%rbp)
	movq	%rdx, -1856(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1832(%rbp)
	movq	%rax, -4600(%rbp)
	movq	$0, -1840(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4416(%rbp), %rax
	movl	$240, %edi
	movq	$0, -1672(%rbp)
	movq	$0, -1664(%rbp)
	movq	%rax, -1680(%rbp)
	movq	$0, -1656(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -1672(%rbp)
	leaq	-1624(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1656(%rbp)
	movq	%rdx, -1664(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1640(%rbp)
	movq	%rax, -4528(%rbp)
	movq	$0, -1648(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4416(%rbp), %rax
	movl	$240, %edi
	movq	$0, -1480(%rbp)
	movq	$0, -1472(%rbp)
	movq	%rax, -1488(%rbp)
	movq	$0, -1464(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -1480(%rbp)
	leaq	-1432(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1464(%rbp)
	movq	%rdx, -1472(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1448(%rbp)
	movq	%rax, -4696(%rbp)
	movq	$0, -1456(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4416(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1288(%rbp)
	movq	$0, -1280(%rbp)
	movq	%rax, -1296(%rbp)
	movq	$0, -1272(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%r14, %rsi
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1288(%rbp)
	leaq	-1240(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1272(%rbp)
	movq	%rdx, -1280(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1256(%rbp)
	movq	%rax, -4640(%rbp)
	movq	$0, -1264(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4416(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1096(%rbp)
	movq	$0, -1088(%rbp)
	movq	%rax, -1104(%rbp)
	movq	$0, -1080(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1096(%rbp)
	leaq	-1048(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1080(%rbp)
	movq	%rdx, -1088(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1064(%rbp)
	movq	%rax, -4704(%rbp)
	movq	$0, -1072(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4416(%rbp), %rax
	movl	$216, %edi
	movq	$0, -904(%rbp)
	movq	$0, -896(%rbp)
	movq	%rax, -912(%rbp)
	movq	$0, -888(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -904(%rbp)
	leaq	-856(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -888(%rbp)
	movq	%rdx, -896(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -872(%rbp)
	movq	%rax, -4648(%rbp)
	movq	$0, -880(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4416(%rbp), %rax
	movl	$216, %edi
	movq	$0, -712(%rbp)
	movq	$0, -704(%rbp)
	movq	%rax, -720(%rbp)
	movq	$0, -696(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -712(%rbp)
	leaq	-664(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -696(%rbp)
	movq	%rdx, -704(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -680(%rbp)
	movq	%rax, -4656(%rbp)
	movq	$0, -688(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4416(%rbp), %rax
	movl	$192, %edi
	movq	$0, -520(%rbp)
	movq	$0, -512(%rbp)
	movq	%rax, -528(%rbp)
	movq	$0, -504(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -520(%rbp)
	leaq	-472(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -504(%rbp)
	movq	%rdx, -512(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -488(%rbp)
	movq	%rax, -4688(%rbp)
	movq	$0, -496(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4416(%rbp), %rax
	movl	$192, %edi
	movq	$0, -328(%rbp)
	movq	$0, -320(%rbp)
	movq	%rax, -336(%rbp)
	movq	$0, -312(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -328(%rbp)
	leaq	-280(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -312(%rbp)
	movq	%rdx, -320(%rbp)
	xorl	%edx, %edx
	movq	%rax, -4608(%rbp)
	movups	%xmm0, -296(%rbp)
	movq	$0, -304(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4480(%rbp), %r10
	pxor	%xmm0, %xmm0
	movq	-4432(%rbp), %r11
	movq	-4464(%rbp), %r9
	movq	-4448(%rbp), %rax
	movl	$56, %edi
	movq	%r15, -120(%rbp)
	movq	%r10, -112(%rbp)
	leaq	-4304(%rbp), %r15
	movq	%r11, -104(%rbp)
	movq	%r9, -96(%rbp)
	movaps	%xmm0, -4304(%rbp)
	movq	%r13, -144(%rbp)
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -4288(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm7
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm6
	leaq	56(%rax), %rdx
	movq	%rax, -4304(%rbp)
	movq	%rcx, 48(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	leaq	-4176(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4288(%rbp)
	movq	%rdx, -4296(%rbp)
	movq	%rax, -4632(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1663
	call	_ZdlPv@PLT
.L1663:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3792(%rbp), %rax
	cmpq	$0, -4112(%rbp)
	movq	%rax, -4520(%rbp)
	leaq	-3984(%rbp), %rax
	movq	%rax, -4432(%rbp)
	jne	.L1877
.L1664:
	leaq	-3600(%rbp), %rax
	cmpq	$0, -3920(%rbp)
	movq	%rax, -4504(%rbp)
	jne	.L1878
.L1669:
	leaq	-3408(%rbp), %rax
	cmpq	$0, -3728(%rbp)
	movq	%rax, -4512(%rbp)
	jne	.L1879
.L1672:
	cmpq	$0, -3536(%rbp)
	jne	.L1880
.L1675:
	leaq	-3216(%rbp), %rax
	cmpq	$0, -3344(%rbp)
	movq	%rax, -4480(%rbp)
	jne	.L1881
.L1677:
	leaq	-1296(%rbp), %rax
	cmpq	$0, -3152(%rbp)
	movq	%rax, -4448(%rbp)
	jne	.L1882
.L1682:
	leaq	-2832(%rbp), %rax
	cmpq	$0, -2960(%rbp)
	movq	%rax, -4576(%rbp)
	leaq	-2640(%rbp), %rax
	movq	%rax, -4544(%rbp)
	jne	.L1883
	cmpq	$0, -2768(%rbp)
	jne	.L1884
.L1689:
	leaq	-2256(%rbp), %rax
	cmpq	$0, -2576(%rbp)
	movq	%rax, -4560(%rbp)
	jne	.L1885
	cmpq	$0, -2384(%rbp)
	jne	.L1886
.L1693:
	leaq	-2064(%rbp), %rax
	cmpq	$0, -2192(%rbp)
	movq	%rax, -4592(%rbp)
	leaq	-1872(%rbp), %rax
	movq	%rax, -4464(%rbp)
	jne	.L1887
	cmpq	$0, -2000(%rbp)
	jne	.L1888
.L1698:
	cmpq	$0, -1808(%rbp)
	jne	.L1889
.L1700:
	leaq	-1488(%rbp), %rax
	cmpq	$0, -1616(%rbp)
	movq	%rax, -4600(%rbp)
	jne	.L1890
.L1702:
	cmpq	$0, -1424(%rbp)
	leaq	-720(%rbp), %rbx
	jne	.L1891
.L1704:
	leaq	-1104(%rbp), %rax
	cmpq	$0, -1232(%rbp)
	leaq	-912(%rbp), %r13
	movq	%rax, -4624(%rbp)
	jne	.L1892
	cmpq	$0, -1040(%rbp)
	jne	.L1893
.L1709:
	cmpq	$0, -848(%rbp)
	jne	.L1894
.L1711:
	leaq	-528(%rbp), %rax
	cmpq	$0, -656(%rbp)
	movq	%rax, -4424(%rbp)
	jne	.L1895
.L1713:
	cmpq	$0, -464(%rbp)
	leaq	-336(%rbp), %r12
	jne	.L1896
.L1715:
	movq	-4608(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -4288(%rbp)
	movaps	%xmm0, -4304(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movabsq	$506377902758496007, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -4304(%rbp)
	movq	%rdx, -4288(%rbp)
	movq	%rdx, -4296(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4304(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L1718
	call	_ZdlPv@PLT
.L1718:
	movq	(%r15), %rax
	movq	%r12, %rdi
	movq	56(%rax), %r15
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4424(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4624(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4448(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4600(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4528(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1648(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1719
	call	_ZdlPv@PLT
.L1719:
	movq	-1664(%rbp), %rbx
	movq	-1672(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1720
	.p2align 4,,10
	.p2align 3
.L1724:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1721
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1724
.L1722:
	movq	-1672(%rbp), %r12
.L1720:
	testq	%r12, %r12
	je	.L1725
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1725:
	movq	-4464(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4592(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4560(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4488(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2416(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1726
	call	_ZdlPv@PLT
.L1726:
	movq	-2432(%rbp), %rbx
	movq	-2440(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1727
	.p2align 4,,10
	.p2align 3
.L1731:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1728
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L1731
.L1729:
	movq	-2440(%rbp), %r12
.L1727:
	testq	%r12, %r12
	je	.L1732
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1732:
	movq	-4544(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4576(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4496(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1733
	call	_ZdlPv@PLT
.L1733:
	movq	-3008(%rbp), %rbx
	movq	-3016(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1734
	.p2align 4,,10
	.p2align 3
.L1738:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1735
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1738
.L1736:
	movq	-3016(%rbp), %r12
.L1734:
	testq	%r12, %r12
	je	.L1739
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1739:
	movq	-4480(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4512(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4504(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4520(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4432(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4632(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1897
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1735:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1738
	jmp	.L1736
	.p2align 4,,10
	.p2align 3
.L1721:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1724
	jmp	.L1722
	.p2align 4,,10
	.p2align 3
.L1728:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1731
	jmp	.L1729
	.p2align 4,,10
	.p2align 3
.L1877:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -4288(%rbp)
	movaps	%xmm0, -4304(%rbp)
	call	_Znwm@PLT
	movl	$1029, %r8d
	movq	-4632(%rbp), %rdi
	movq	%r15, %rsi
	movl	$134678279, (%rax)
	leaq	7(%rax), %rdx
	movw	%r8w, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -4304(%rbp)
	movq	%rdx, -4288(%rbp)
	movq	%rdx, -4296(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4304(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1665
	call	_ZdlPv@PLT
.L1665:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	leaq	-4336(%rbp), %r13
	movq	8(%rax), %rcx
	movq	32(%rax), %rsi
	movq	40(%rax), %rdx
	movq	24(%rax), %r12
	movq	(%rax), %rbx
	movq	%rcx, -4464(%rbp)
	movq	16(%rax), %rcx
	movq	48(%rax), %rax
	movq	%rsi, -4480(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -4504(%rbp)
	movl	$148, %edx
	movq	%rax, -4520(%rbp)
	movq	%rcx, -4432(%rbp)
	movq	%r12, -4448(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4424(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler11ToSmiLengthENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEEPNS2_18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, -80(%rbp)
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	%rbx, %xmm5
	leaq	-144(%rbp), %r12
	leaq	-72(%rbp), %rdx
	movq	-4480(%rbp), %xmm7
	movq	-4520(%rbp), %xmm6
	movhps	-4464(%rbp), %xmm5
	movq	%r12, %rsi
	movq	-4432(%rbp), %xmm3
	movhps	-4504(%rbp), %xmm7
	movaps	%xmm5, -4464(%rbp)
	movhps	-4448(%rbp), %xmm6
	movhps	-4448(%rbp), %xmm3
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm6, -4736(%rbp)
	movaps	%xmm7, -4480(%rbp)
	movaps	%xmm3, -4448(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm0, -4336(%rbp)
	movq	$0, -4320(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3792(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -4520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1666
	call	_ZdlPv@PLT
.L1666:
	movq	-4544(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3984(%rbp), %rax
	cmpq	$0, -4296(%rbp)
	movq	%rax, -4432(%rbp)
	jne	.L1898
.L1667:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L1664
	.p2align 4,,10
	.p2align 3
.L1880:
	movq	-4560(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -4288(%rbp)
	movaps	%xmm0, -4304(%rbp)
	call	_Znwm@PLT
	movl	$1029, %edi
	movq	%r15, %rsi
	movw	%di, 4(%rax)
	movq	-4504(%rbp), %rdi
	leaq	7(%rax), %rdx
	movl	$134678279, (%rax)
	movb	$7, 6(%rax)
	movq	%rax, -4304(%rbp)
	movq	%rdx, -4288(%rbp)
	movq	%rdx, -4296(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4304(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1676
	call	_ZdlPv@PLT
.L1676:
	movq	(%rbx), %rax
	movl	$149, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	movq	(%rax), %r12
	movq	24(%rax), %r13
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4424(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$206, %edx
	movq	%r12, %rsi
	call	_ZN2v88internal17CodeStubAssembler15ThrowRangeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L1675
	.p2align 4,,10
	.p2align 3
.L1879:
	movq	-4544(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-144(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	-135(%rbp), %rdx
	movaps	%xmm0, -4304(%rbp)
	movabsq	$578435496796423943, %rax
	movq	%rax, -144(%rbp)
	movb	$6, -136(%rbp)
	movq	$0, -4288(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4520(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4304(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1673
	call	_ZdlPv@PLT
.L1673:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	24(%rax), %rdi
	movq	32(%rax), %rsi
	movq	48(%rax), %rdx
	movq	(%rax), %r10
	movq	8(%rax), %r9
	movq	16(%rax), %r8
	movq	40(%rax), %rcx
	movq	64(%rax), %rax
	movq	%rdi, -120(%rbp)
	movq	%r15, %rdi
	movq	%rsi, -112(%rbp)
	movq	%r12, %rsi
	movq	%rdx, -96(%rbp)
	leaq	-80(%rbp), %rdx
	movq	%r10, -144(%rbp)
	movq	%r9, -136(%rbp)
	movq	%r8, -128(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%rax, -88(%rbp)
	movaps	%xmm0, -4304(%rbp)
	movq	$0, -4288(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3408(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -4512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1674
	call	_ZdlPv@PLT
.L1674:
	movq	-4576(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1672
	.p2align 4,,10
	.p2align 3
.L1878:
	movq	-4512(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-144(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	-136(%rbp), %rdx
	movabsq	$578435496796423943, %rax
	movaps	%xmm0, -4304(%rbp)
	movq	%rax, -144(%rbp)
	movq	$0, -4288(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4432(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4304(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1670
	call	_ZdlPv@PLT
.L1670:
	movq	(%rbx), %rax
	leaq	-88(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movdqu	32(%rax), %xmm0
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm1
	movq	48(%rax), %rax
	movq	$0, -4288(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -4304(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3600(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -4504(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1671
	call	_ZdlPv@PLT
.L1671:
	movq	-4560(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1669
	.p2align 4,,10
	.p2align 3
.L1881:
	movq	-4576(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-144(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	-136(%rbp), %rdx
	movabsq	$434320308720568071, %rax
	movaps	%xmm0, -4304(%rbp)
	movq	%rax, -144(%rbp)
	movq	$0, -4288(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4512(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4304(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1678
	call	_ZdlPv@PLT
.L1678:
	movq	(%rbx), %rax
	movq	24(%rax), %rdx
	movq	8(%rax), %rbx
	movq	16(%rax), %rsi
	movq	40(%rax), %rdi
	movq	%rdx, -4560(%rbp)
	movq	32(%rax), %rdx
	movq	(%rax), %r13
	movq	%rbx, -4464(%rbp)
	movq	48(%rax), %rbx
	movq	56(%rax), %rax
	movq	%rsi, -4480(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -4448(%rbp)
	movl	$148, %edx
	movq	%rdi, -4576(%rbp)
	movq	%r14, %rdi
	movq	%rax, -4544(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$152, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$151, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %r9
	movq	%r13, %rsi
	xorl	%edx, %edx
	movq	-4448(%rbp), %xmm5
	movq	-4544(%rbp), %r8
	movq	-4464(%rbp), %rcx
	movq	-4424(%rbp), %rdi
	movhps	-4576(%rbp), %xmm5
	movaps	%xmm5, -4448(%rbp)
	pushq	-4440(%rbp)
	pushq	-4448(%rbp)
	call	_ZN2v88internal24TypedArrayInitialize_342EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEbNS4_INS0_3MapEEENS4_INS0_3SmiEEENS0_34TorqueStructTypedArrayElementsInfoENS4_INS0_10JSReceiverEEE
	movl	$155, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -4576(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4480(%rbp), %rsi
	movq	-4424(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZN2v88internal22Cast12JSTypedArray_109EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-56(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %xmm6
	movq	%rbx, %xmm2
	leaq	-4336(%rbp), %r13
	movq	-4480(%rbp), %xmm3
	movq	-4576(%rbp), %xmm1
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	movhps	-4544(%rbp), %xmm2
	movdqa	-4448(%rbp), %xmm5
	movdqa	%xmm3, %xmm4
	movaps	%xmm2, -96(%rbp)
	movhps	-4464(%rbp), %xmm6
	punpcklqdq	%xmm3, %xmm1
	movhps	-4560(%rbp), %xmm4
	movaps	%xmm2, -4736(%rbp)
	movaps	%xmm1, -4576(%rbp)
	movaps	%xmm4, -4544(%rbp)
	movaps	%xmm6, -4464(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm1, -80(%rbp)
	movaps	%xmm0, -4336(%rbp)
	movq	$0, -4320(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3024(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4336(%rbp), %rdi
	popq	%rcx
	popq	%rsi
	testq	%rdi, %rdi
	je	.L1679
	call	_ZdlPv@PLT
.L1679:
	movq	-4496(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3216(%rbp), %rax
	cmpq	$0, -4296(%rbp)
	movq	%rax, -4480(%rbp)
	jne	.L1899
.L1680:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L1677
	.p2align 4,,10
	.p2align 3
.L1883:
	movq	-4496(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-3024(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-144(%rbp), %rsi
	movabsq	$434320308720568071, %rax
	movq	%rax, -144(%rbp)
	movq	%r15, %rdi
	movl	$1799, %eax
	leaq	-133(%rbp), %rdx
	movw	%ax, -136(%rbp)
	movaps	%xmm0, -4304(%rbp)
	movb	$7, -134(%rbp)
	movq	$0, -4288(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4304(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1686
	call	_ZdlPv@PLT
.L1686:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	24(%rax), %rsi
	movq	56(%rax), %rdx
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	%rsi, -4736(%rbp)
	movq	40(%rax), %rsi
	movq	%rdx, -4760(%rbp)
	movq	64(%rax), %rdx
	movq	%rsi, -4544(%rbp)
	movq	48(%rax), %rsi
	movq	%rcx, -4720(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -4752(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	32(%rax), %r13
	movq	%rdx, -4560(%rbp)
	movl	$157, %edx
	movq	%rcx, -4464(%rbp)
	movq	%rbx, -4576(%rbp)
	movq	80(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-4424(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE(%rip), %esi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-4424(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler16IsDetachedBufferENS0_8compiler5TNodeINS0_13JSArrayBufferEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm5
	pxor	%xmm0, %xmm0
	movq	-4560(%rbp), %xmm7
	movq	-4752(%rbp), %xmm3
	movl	$80, %edi
	movq	-4464(%rbp), %xmm1
	movaps	%xmm0, -4304(%rbp)
	movq	-4576(%rbp), %xmm2
	punpcklqdq	%xmm5, %xmm7
	movq	%r13, %xmm5
	movq	$0, -4288(%rbp)
	movhps	-4760(%rbp), %xmm3
	movhps	-4544(%rbp), %xmm5
	movaps	%xmm7, -80(%rbp)
	movhps	-4736(%rbp), %xmm1
	movhps	-4720(%rbp), %xmm2
	movaps	%xmm7, -4560(%rbp)
	movaps	%xmm3, -4752(%rbp)
	movaps	%xmm5, -4544(%rbp)
	movaps	%xmm1, -4464(%rbp)
	movaps	%xmm2, -4720(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm5
	leaq	80(%rax), %rdx
	movq	%rax, -4304(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-112(%rbp), %xmm7
	movups	%xmm6, 16(%rax)
	movdqa	-80(%rbp), %xmm6
	movups	%xmm7, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	leaq	-2832(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4288(%rbp)
	movq	%rdx, -4296(%rbp)
	movq	%rax, -4576(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1687
	call	_ZdlPv@PLT
.L1687:
	movdqa	-4720(%rbp), %xmm7
	movdqa	-4464(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movl	$80, %edi
	movdqa	-4544(%rbp), %xmm6
	movaps	%xmm0, -4304(%rbp)
	movaps	%xmm7, -144(%rbp)
	movdqa	-4752(%rbp), %xmm7
	movaps	%xmm5, -128(%rbp)
	movdqa	-4560(%rbp), %xmm5
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movq	$0, -4288(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm7
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm5
	leaq	80(%rax), %rdx
	movq	%rax, -4304(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-96(%rbp), %xmm6
	movups	%xmm7, 16(%rax)
	movdqa	-80(%rbp), %xmm7
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	leaq	-2640(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4288(%rbp)
	movq	%rdx, -4296(%rbp)
	movq	%rax, -4544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1688
	call	_ZdlPv@PLT
.L1688:
	movq	-4592(%rbp), %rcx
	movq	-4672(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2768(%rbp)
	je	.L1689
.L1884:
	movq	-4672(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4408(%rbp)
	movq	$0, -4400(%rbp)
	movq	$0, -4392(%rbp)
	movq	$0, -4384(%rbp)
	movq	$0, -4376(%rbp)
	movq	$0, -4368(%rbp)
	movq	$0, -4360(%rbp)
	movq	$0, -4352(%rbp)
	movq	$0, -4344(%rbp)
	movq	$0, -4336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-4336(%rbp), %rax
	movq	-4576(%rbp), %rdi
	pushq	%rax
	leaq	-4344(%rbp), %rax
	leaq	-4392(%rbp), %rcx
	pushq	%rax
	leaq	-4352(%rbp), %rax
	leaq	-4376(%rbp), %r9
	pushq	%rax
	leaq	-4360(%rbp), %rax
	leaq	-4384(%rbp), %r8
	pushq	%rax
	leaq	-4368(%rbp), %rax
	leaq	-4400(%rbp), %rdx
	pushq	%rax
	leaq	-4408(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_10HeapObjectENS0_6ObjectENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverENS0_3SmiENS0_12JSTypedArrayESB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EESV_
	addq	$48, %rsp
	movq	%r14, %rdi
	movl	$158, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4424(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movl	$50, %edx
	movq	-4408(%rbp), %rsi
	leaq	.LC5(%rip), %rcx
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L1689
	.p2align 4,,10
	.p2align 3
.L1882:
	movq	-4720(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1799, %edx
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movw	%dx, -136(%rbp)
	leaq	-144(%rbp), %rsi
	movabsq	$434320308720568071, %rax
	leaq	-134(%rbp), %rdx
	movq	%rax, -144(%rbp)
	movaps	%xmm0, -4304(%rbp)
	movq	$0, -4288(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4480(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4304(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1683
	call	_ZdlPv@PLT
.L1683:
	movq	(%rbx), %rax
	movl	$72, %edi
	movdqu	48(%rax), %xmm0
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	64(%rax), %rdx
	movdqu	(%rax), %xmm5
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -80(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -4304(%rbp)
	movq	$0, -4288(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm5
	movq	-80(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm6
	leaq	72(%rax), %rdx
	movq	%rax, -4304(%rbp)
	movups	%xmm5, (%rax)
	movdqa	-128(%rbp), %xmm5
	movq	%rcx, 64(%rax)
	movups	%xmm5, 16(%rax)
	movdqa	-112(%rbp), %xmm5
	movups	%xmm6, 48(%rax)
	movups	%xmm5, 32(%rax)
	leaq	-1296(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4288(%rbp)
	movq	%rdx, -4296(%rbp)
	movq	%rax, -4448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1684
	call	_ZdlPv@PLT
.L1684:
	movq	-4640(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1682
	.p2align 4,,10
	.p2align 3
.L1885:
	movq	-4592(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4408(%rbp)
	movq	$0, -4400(%rbp)
	movq	$0, -4392(%rbp)
	movq	$0, -4384(%rbp)
	movq	$0, -4376(%rbp)
	movq	$0, -4368(%rbp)
	movq	$0, -4360(%rbp)
	movq	$0, -4352(%rbp)
	movq	$0, -4344(%rbp)
	movq	$0, -4336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-4336(%rbp), %rax
	movq	-4544(%rbp), %rdi
	pushq	%rax
	leaq	-4344(%rbp), %rax
	leaq	-4392(%rbp), %rcx
	pushq	%rax
	leaq	-4352(%rbp), %rax
	leaq	-4376(%rbp), %r9
	pushq	%rax
	leaq	-4360(%rbp), %rax
	leaq	-4384(%rbp), %r8
	pushq	%rax
	leaq	-4368(%rbp), %rax
	leaq	-4400(%rbp), %rdx
	pushq	%rax
	leaq	-4408(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_10HeapObjectENS0_6ObjectENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverENS0_3SmiENS0_12JSTypedArrayESB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EESV_
	addq	$48, %rsp
	movl	$160, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4424(%rbp), %rbx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4336(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler16LoadElementsKindENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-4368(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal23ElementsKindNotEqual_72EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6Int32TEEES6_@PLT
	pxor	%xmm0, %xmm0
	movl	$80, %edi
	movq	-4344(%rbp), %xmm4
	movq	-4360(%rbp), %xmm6
	movaps	%xmm0, -4304(%rbp)
	movq	%rax, %r12
	movq	-4376(%rbp), %xmm7
	movq	-4392(%rbp), %xmm3
	movq	-4408(%rbp), %xmm5
	movhps	-4336(%rbp), %xmm4
	movq	$0, -4288(%rbp)
	movhps	-4352(%rbp), %xmm6
	movhps	-4368(%rbp), %xmm7
	movaps	%xmm4, -4720(%rbp)
	movhps	-4384(%rbp), %xmm3
	movhps	-4400(%rbp), %xmm5
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm6, -4672(%rbp)
	movaps	%xmm7, -4592(%rbp)
	movaps	%xmm3, -4560(%rbp)
	movaps	%xmm5, -4464(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm4
	movdqa	-128(%rbp), %xmm3
	leaq	-2448(%rbp), %rdi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	leaq	80(%rax), %rdx
	movq	%r15, %rsi
	movdqa	-80(%rbp), %xmm7
	movups	%xmm4, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movq	%rax, -4304(%rbp)
	movq	%rdx, -4288(%rbp)
	movq	%rdx, -4296(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1691
	call	_ZdlPv@PLT
.L1691:
	movdqa	-4464(%rbp), %xmm4
	movdqa	-4560(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movl	$80, %edi
	movdqa	-4592(%rbp), %xmm5
	movdqa	-4672(%rbp), %xmm6
	movaps	%xmm0, -4304(%rbp)
	movdqa	-4720(%rbp), %xmm7
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	movq	$0, -4288(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm4
	movdqa	-128(%rbp), %xmm1
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	leaq	80(%rax), %rdx
	movq	%rax, -4304(%rbp)
	movdqa	-80(%rbp), %xmm5
	movups	%xmm4, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm5, 64(%rax)
	leaq	-2256(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4288(%rbp)
	movq	%rdx, -4296(%rbp)
	movq	%rax, -4560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1692
	call	_ZdlPv@PLT
.L1692:
	movq	-4624(%rbp), %rcx
	movq	-4488(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2384(%rbp)
	je	.L1693
.L1886:
	movq	-4488(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4408(%rbp)
	leaq	-2448(%rbp), %r12
	movq	$0, -4400(%rbp)
	movq	$0, -4392(%rbp)
	movq	$0, -4384(%rbp)
	movq	$0, -4376(%rbp)
	movq	$0, -4368(%rbp)
	movq	$0, -4360(%rbp)
	movq	$0, -4352(%rbp)
	movq	$0, -4344(%rbp)
	movq	$0, -4336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-4336(%rbp), %rax
	pushq	%rax
	leaq	-4344(%rbp), %rax
	leaq	-4392(%rbp), %rcx
	pushq	%rax
	leaq	-4352(%rbp), %rax
	leaq	-4376(%rbp), %r9
	pushq	%rax
	leaq	-4360(%rbp), %rax
	leaq	-4384(%rbp), %r8
	pushq	%rax
	leaq	-4368(%rbp), %rax
	leaq	-4400(%rbp), %rdx
	pushq	%rax
	leaq	-4408(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_10HeapObjectENS0_6ObjectENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverENS0_3SmiENS0_12JSTypedArrayESB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EESV_
	addq	$48, %rsp
	movl	$161, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4408(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movaps	%xmm0, -4304(%rbp)
	movq	%rax, -144(%rbp)
	movq	-4400(%rbp), %rax
	movq	$0, -4288(%rbp)
	movq	%rax, -136(%rbp)
	movq	-4392(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-4384(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-4376(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-4368(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-4360(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-4352(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-4344(%rbp), %rax
	movq	%rax, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm7
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm1
	leaq	72(%rax), %rdx
	movq	%rax, -4304(%rbp)
	movdqa	-144(%rbp), %xmm6
	movq	-4448(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm1, 48(%rax)
	movq	%rdx, -4288(%rbp)
	movq	%rdx, -4296(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1694
	call	_ZdlPv@PLT
.L1694:
	movq	-4640(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1693
	.p2align 4,,10
	.p2align 3
.L1887:
	movq	-4624(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4408(%rbp)
	movq	$0, -4400(%rbp)
	movq	$0, -4392(%rbp)
	movq	$0, -4384(%rbp)
	movq	$0, -4376(%rbp)
	movq	$0, -4368(%rbp)
	movq	$0, -4360(%rbp)
	movq	$0, -4352(%rbp)
	movq	$0, -4344(%rbp)
	movq	$0, -4336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-4336(%rbp), %rax
	movq	-4560(%rbp), %rdi
	pushq	%rax
	leaq	-4344(%rbp), %rax
	leaq	-4392(%rbp), %rcx
	pushq	%rax
	leaq	-4352(%rbp), %rax
	leaq	-4376(%rbp), %r9
	pushq	%rax
	leaq	-4360(%rbp), %rax
	leaq	-4384(%rbp), %r8
	pushq	%rax
	leaq	-4368(%rbp), %rax
	leaq	-4400(%rbp), %rdx
	pushq	%rax
	leaq	-4408(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_10HeapObjectENS0_6ObjectENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverENS0_3SmiENS0_12JSTypedArrayESB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EESV_
	addq	$48, %rsp
	movl	$163, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4424(%rbp), %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4352(%rbp), %r13
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17IntPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$80, %edi
	movq	-4344(%rbp), %xmm1
	movq	-4360(%rbp), %xmm2
	movaps	%xmm0, -4304(%rbp)
	movq	-4376(%rbp), %xmm4
	movq	-4392(%rbp), %xmm6
	movq	-4408(%rbp), %xmm7
	movhps	-4336(%rbp), %xmm1
	movq	$0, -4288(%rbp)
	movhps	-4352(%rbp), %xmm2
	movhps	-4368(%rbp), %xmm4
	movaps	%xmm1, -4736(%rbp)
	movhps	-4384(%rbp), %xmm6
	movhps	-4400(%rbp), %xmm7
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm2, -4720(%rbp)
	movaps	%xmm4, -4672(%rbp)
	movaps	%xmm6, -4624(%rbp)
	movaps	%xmm7, -4464(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm3
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	leaq	80(%rax), %rdx
	movq	%rax, -4304(%rbp)
	movdqa	-80(%rbp), %xmm7
	movq	-4592(%rbp), %rdi
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movq	%rdx, -4288(%rbp)
	movq	%rdx, -4296(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1696
	call	_ZdlPv@PLT
.L1696:
	movdqa	-4464(%rbp), %xmm4
	movdqa	-4624(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movl	$80, %edi
	movdqa	-4672(%rbp), %xmm2
	movdqa	-4720(%rbp), %xmm3
	movaps	%xmm0, -4304(%rbp)
	movdqa	-4736(%rbp), %xmm5
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movq	$0, -4288(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm7
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm1
	leaq	80(%rax), %rdx
	movq	%rax, -4304(%rbp)
	movdqa	-80(%rbp), %xmm2
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm1, 48(%rax)
	movups	%xmm2, 64(%rax)
	leaq	-1872(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4288(%rbp)
	movq	%rdx, -4296(%rbp)
	movq	%rax, -4464(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1697
	call	_ZdlPv@PLT
.L1697:
	movq	-4600(%rbp), %rcx
	movq	-4680(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2000(%rbp)
	je	.L1698
.L1888:
	movq	-4680(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4408(%rbp)
	movq	$0, -4400(%rbp)
	movq	$0, -4392(%rbp)
	movq	$0, -4384(%rbp)
	movq	$0, -4376(%rbp)
	movq	$0, -4368(%rbp)
	movq	$0, -4360(%rbp)
	movq	$0, -4352(%rbp)
	movq	$0, -4344(%rbp)
	movq	$0, -4336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-4336(%rbp), %rax
	movq	-4592(%rbp), %rdi
	pushq	%rax
	leaq	-4344(%rbp), %rax
	leaq	-4376(%rbp), %r9
	pushq	%rax
	leaq	-4352(%rbp), %rax
	leaq	-4384(%rbp), %r8
	pushq	%rax
	leaq	-4360(%rbp), %rax
	leaq	-4392(%rbp), %rcx
	pushq	%rax
	leaq	-4368(%rbp), %rax
	leaq	-4400(%rbp), %rdx
	pushq	%rax
	leaq	-4408(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_10HeapObjectENS0_6ObjectENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverENS0_3SmiENS0_12JSTypedArrayESB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EESV_
	addq	$48, %rsp
	movl	$164, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$40, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-4424(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	movq	-4344(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE(%rip), %esi
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$166, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4424(%rbp), %r13
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4344(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler28LoadJSTypedArrayBackingStoreENS0_8compiler5TNodeINS0_12JSTypedArrayEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r13, -4424(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4336(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler28LoadJSTypedArrayBackingStoreENS0_8compiler5TNodeINS0_12JSTypedArrayEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-4424(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal27TypedArrayBuiltinsAssembler11CallCMemcpyENS0_8compiler5TNodeINS0_7RawPtrTEEES5_NS3_INS0_8UintPtrTEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$163, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$80, %edi
	movq	-4344(%rbp), %xmm0
	movq	-4360(%rbp), %xmm1
	movq	-4376(%rbp), %xmm2
	movq	-4392(%rbp), %xmm3
	movq	$0, -4288(%rbp)
	movq	-4408(%rbp), %xmm4
	movhps	-4336(%rbp), %xmm0
	movhps	-4352(%rbp), %xmm1
	movhps	-4368(%rbp), %xmm2
	movhps	-4384(%rbp), %xmm3
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-4400(%rbp), %xmm4
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -4304(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm5
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	80(%rax), %rdx
	movq	%rax, -4304(%rbp)
	movdqa	-80(%rbp), %xmm4
	movq	-4464(%rbp), %rdi
	movups	%xmm3, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm4, 64(%rax)
	movq	%rdx, -4288(%rbp)
	movq	%rdx, -4296(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1699
	call	_ZdlPv@PLT
.L1699:
	movq	-4600(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1808(%rbp)
	je	.L1700
.L1889:
	movq	-4600(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4408(%rbp)
	movq	$0, -4400(%rbp)
	movq	$0, -4392(%rbp)
	movq	$0, -4384(%rbp)
	movq	$0, -4376(%rbp)
	movq	$0, -4368(%rbp)
	movq	$0, -4360(%rbp)
	movq	$0, -4352(%rbp)
	movq	$0, -4344(%rbp)
	movq	$0, -4336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-4336(%rbp), %rax
	movq	-4464(%rbp), %rdi
	pushq	%rax
	leaq	-4344(%rbp), %rax
	leaq	-4392(%rbp), %rcx
	pushq	%rax
	leaq	-4352(%rbp), %rax
	leaq	-4376(%rbp), %r9
	pushq	%rax
	leaq	-4360(%rbp), %rax
	leaq	-4384(%rbp), %r8
	pushq	%rax
	leaq	-4368(%rbp), %rax
	leaq	-4400(%rbp), %rdx
	pushq	%rax
	leaq	-4408(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_10HeapObjectENS0_6ObjectENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverENS0_3SmiENS0_12JSTypedArrayESB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EESV_
	addq	$48, %rsp
	movl	$160, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$80, %edi
	movq	-4344(%rbp), %xmm0
	movq	-4360(%rbp), %xmm1
	movq	-4376(%rbp), %xmm2
	movq	-4392(%rbp), %xmm3
	movq	$0, -4288(%rbp)
	movq	-4408(%rbp), %xmm4
	movhps	-4336(%rbp), %xmm0
	movhps	-4352(%rbp), %xmm1
	movhps	-4368(%rbp), %xmm2
	movhps	-4384(%rbp), %xmm3
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-4400(%rbp), %xmm4
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -4304(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm1
	movdqa	-128(%rbp), %xmm2
	leaq	-1680(%rbp), %rdi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm5
	leaq	80(%rax), %rdx
	movq	%r15, %rsi
	movdqa	-80(%rbp), %xmm6
	movups	%xmm1, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	movq	%rax, -4304(%rbp)
	movq	%rdx, -4288(%rbp)
	movq	%rdx, -4296(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1701
	call	_ZdlPv@PLT
.L1701:
	movq	-4528(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1700
	.p2align 4,,10
	.p2align 3
.L1890:
	movq	-4528(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4408(%rbp)
	leaq	-1680(%rbp), %r12
	movq	$0, -4400(%rbp)
	movq	$0, -4392(%rbp)
	movq	$0, -4384(%rbp)
	movq	$0, -4376(%rbp)
	movq	$0, -4368(%rbp)
	movq	$0, -4360(%rbp)
	movq	$0, -4352(%rbp)
	movq	$0, -4344(%rbp)
	movq	$0, -4336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-4336(%rbp), %rax
	pushq	%rax
	leaq	-4344(%rbp), %rax
	leaq	-4392(%rbp), %rcx
	pushq	%rax
	leaq	-4352(%rbp), %rax
	leaq	-4376(%rbp), %r9
	pushq	%rax
	leaq	-4360(%rbp), %rax
	leaq	-4384(%rbp), %r8
	pushq	%rax
	leaq	-4368(%rbp), %rax
	leaq	-4400(%rbp), %rdx
	pushq	%rax
	leaq	-4408(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_10HeapObjectENS0_6ObjectENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverENS0_3SmiENS0_12JSTypedArrayESB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EESV_
	addq	$48, %rsp
	movl	$157, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$80, %edi
	movq	-4344(%rbp), %xmm0
	movq	-4360(%rbp), %xmm1
	movq	-4376(%rbp), %xmm2
	movq	-4392(%rbp), %xmm3
	movq	$0, -4288(%rbp)
	movq	-4408(%rbp), %xmm4
	movhps	-4336(%rbp), %xmm0
	movhps	-4352(%rbp), %xmm1
	movhps	-4368(%rbp), %xmm2
	movhps	-4384(%rbp), %xmm3
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-4400(%rbp), %xmm4
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -4304(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm4
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm1
	movdqa	-96(%rbp), %xmm2
	leaq	80(%rax), %rdx
	movq	%rax, -4304(%rbp)
	movdqa	-80(%rbp), %xmm3
	movq	-4600(%rbp), %rdi
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm3, 64(%rax)
	movq	%rdx, -4288(%rbp)
	movq	%rdx, -4296(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1703
	call	_ZdlPv@PLT
.L1703:
	movq	-4696(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1702
	.p2align 4,,10
	.p2align 3
.L1891:
	movq	-4696(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4408(%rbp)
	movq	$0, -4400(%rbp)
	movq	$0, -4392(%rbp)
	movq	$0, -4384(%rbp)
	movq	$0, -4376(%rbp)
	movq	$0, -4368(%rbp)
	movq	$0, -4360(%rbp)
	movq	$0, -4352(%rbp)
	movq	$0, -4344(%rbp)
	movq	$0, -4336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-4336(%rbp), %rax
	movq	-4600(%rbp), %rdi
	pushq	%rax
	leaq	-4344(%rbp), %rax
	leaq	-4392(%rbp), %rcx
	pushq	%rax
	leaq	-4352(%rbp), %rax
	leaq	-4376(%rbp), %r9
	pushq	%rax
	leaq	-4360(%rbp), %rax
	leaq	-4384(%rbp), %r8
	pushq	%rax
	leaq	-4368(%rbp), %rax
	leaq	-4400(%rbp), %rdx
	pushq	%rax
	leaq	-4408(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_10HeapObjectENS0_6ObjectENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverENS0_3SmiENS0_12JSTypedArrayESB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EESV_
	addq	$48, %rsp
	movl	$154, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$169, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4408(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movaps	%xmm0, -4304(%rbp)
	movq	%rax, -144(%rbp)
	movq	-4400(%rbp), %rax
	movq	$0, -4288(%rbp)
	movq	%rax, -136(%rbp)
	movq	-4392(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-4384(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-4376(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-4368(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-4360(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-4352(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-4344(%rbp), %rax
	movq	%rax, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movdqa	-128(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm6
	leaq	72(%rax), %rdx
	movq	%rax, -4304(%rbp)
	movdqa	-144(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm4
	movq	%rcx, 64(%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm4, 48(%rax)
	movq	%rdx, -4288(%rbp)
	movq	%rdx, -4296(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1705
	call	_ZdlPv@PLT
.L1705:
	movq	-4656(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1704
	.p2align 4,,10
	.p2align 3
.L1892:
	movq	-4640(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4400(%rbp)
	movq	$0, -4392(%rbp)
	movq	$0, -4384(%rbp)
	movq	$0, -4376(%rbp)
	movq	$0, -4368(%rbp)
	movq	$0, -4360(%rbp)
	movq	$0, -4352(%rbp)
	movq	$0, -4344(%rbp)
	movq	$0, -4336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4336(%rbp), %rax
	movq	-4448(%rbp), %rdi
	leaq	-4376(%rbp), %r8
	pushq	%rax
	leaq	-4344(%rbp), %rax
	leaq	-4384(%rbp), %rcx
	pushq	%rax
	leaq	-4352(%rbp), %rax
	leaq	-4368(%rbp), %r9
	pushq	%rax
	leaq	-4360(%rbp), %rax
	leaq	-4392(%rbp), %rdx
	pushq	%rax
	leaq	-4400(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_10HeapObjectENS0_6ObjectENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverENS0_3SmiENS0_12JSTypedArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EE
	addq	$32, %rsp
	movl	$170, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4424(%rbp), %r13
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4344(%rbp), %r13
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17IntPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-4368(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	-4392(%rbp), %rcx
	movq	-4384(%rbp), %rsi
	movq	-4376(%rbp), %rdx
	movaps	%xmm0, -4304(%rbp)
	movq	-4360(%rbp), %r11
	movq	-4352(%rbp), %r10
	movq	%rdi, -4720(%rbp)
	movq	-4344(%rbp), %r9
	movq	-4400(%rbp), %rax
	movq	%rdi, -112(%rbp)
	movl	$72, %edi
	movq	-4336(%rbp), %r13
	movq	%rcx, -4672(%rbp)
	movq	%rsi, -4680(%rbp)
	movq	%rdx, -4696(%rbp)
	movq	%r11, -4736(%rbp)
	movq	%r10, -4752(%rbp)
	movq	%r9, -4760(%rbp)
	movq	%rcx, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%r11, -104(%rbp)
	movq	%r10, -96(%rbp)
	movq	%r9, -88(%rbp)
	movq	%rax, -4640(%rbp)
	movq	%rax, -144(%rbp)
	movq	%r13, -80(%rbp)
	movq	$0, -4288(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm2
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm5
	leaq	72(%rax), %rdx
	movq	%rax, -4304(%rbp)
	movdqa	-144(%rbp), %xmm1
	movq	-4624(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm1, (%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rdx, -4288(%rbp)
	movq	%rdx, -4296(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1707
	call	_ZdlPv@PLT
.L1707:
	movq	-4640(%rbp), %xmm0
	movl	$72, %edi
	movq	%r13, -80(%rbp)
	leaq	-912(%rbp), %r13
	movq	$0, -4288(%rbp)
	movhps	-4672(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-4680(%rbp), %xmm0
	movhps	-4696(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-4720(%rbp), %xmm0
	movhps	-4736(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-4752(%rbp), %xmm0
	movhps	-4760(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -4304(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r13, %rdi
	movq	%r15, %rsi
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm4
	leaq	72(%rax), %rdx
	movq	%rax, -4304(%rbp)
	movdqa	-144(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm1
	movq	%rcx, 64(%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm1, 48(%rax)
	movq	%rdx, -4288(%rbp)
	movq	%rdx, -4296(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1708
	call	_ZdlPv@PLT
.L1708:
	movq	-4648(%rbp), %rcx
	movq	-4704(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1040(%rbp)
	je	.L1709
.L1893:
	movq	-4704(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4400(%rbp)
	movq	$0, -4392(%rbp)
	movq	$0, -4384(%rbp)
	movq	$0, -4376(%rbp)
	movq	$0, -4368(%rbp)
	movq	$0, -4360(%rbp)
	movq	$0, -4352(%rbp)
	movq	$0, -4344(%rbp)
	movq	$0, -4336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4336(%rbp), %rax
	movq	-4624(%rbp), %rdi
	leaq	-4368(%rbp), %r9
	pushq	%rax
	leaq	-4344(%rbp), %rax
	leaq	-4384(%rbp), %rcx
	pushq	%rax
	leaq	-4352(%rbp), %rax
	leaq	-4376(%rbp), %r8
	pushq	%rax
	leaq	-4360(%rbp), %rax
	leaq	-4392(%rbp), %rdx
	pushq	%rax
	leaq	-4400(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_10HeapObjectENS0_6ObjectENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverENS0_3SmiENS0_12JSTypedArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EE
	addq	$32, %rsp
	movl	$171, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4424(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4336(%rbp), %rax
	movl	$440, %esi
	movq	-4400(%rbp), %rdx
	leaq	-144(%rbp), %rcx
	movl	$3, %r8d
	movq	%r15, %rdi
	movq	%rax, -144(%rbp)
	movq	-4384(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-4344(%rbp), %rax
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$170, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4400(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movaps	%xmm0, -4304(%rbp)
	movq	%rax, -144(%rbp)
	movq	-4392(%rbp), %rax
	movq	$0, -4288(%rbp)
	movq	%rax, -136(%rbp)
	movq	-4384(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-4376(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-4368(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-4360(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-4352(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-4344(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-4336(%rbp), %rax
	movq	%rax, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r13, %rdi
	movq	%r15, %rsi
	movdqa	-128(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm5
	leaq	72(%rax), %rdx
	movq	%rax, -4304(%rbp)
	movdqa	-144(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm7
	movq	%rcx, 64(%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rdx, -4288(%rbp)
	movq	%rdx, -4296(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1710
	call	_ZdlPv@PLT
.L1710:
	movq	-4648(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -848(%rbp)
	je	.L1711
.L1894:
	movq	-4648(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4400(%rbp)
	movq	$0, -4392(%rbp)
	movq	$0, -4384(%rbp)
	movq	$0, -4376(%rbp)
	movq	$0, -4368(%rbp)
	movq	$0, -4360(%rbp)
	movq	$0, -4352(%rbp)
	movq	$0, -4344(%rbp)
	movq	$0, -4336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4336(%rbp), %rax
	movq	%r13, %rdi
	leaq	-4384(%rbp), %rcx
	pushq	%rax
	leaq	-4344(%rbp), %rax
	leaq	-4368(%rbp), %r9
	pushq	%rax
	leaq	-4352(%rbp), %rax
	leaq	-4376(%rbp), %r8
	pushq	%rax
	leaq	-4360(%rbp), %rax
	leaq	-4392(%rbp), %rdx
	pushq	%rax
	leaq	-4400(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_10HeapObjectENS0_6ObjectENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverENS0_3SmiENS0_12JSTypedArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EE
	addq	$32, %rsp
	movl	$154, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4400(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movaps	%xmm0, -4304(%rbp)
	movq	%rax, -144(%rbp)
	movq	-4392(%rbp), %rax
	movq	$0, -4288(%rbp)
	movq	%rax, -136(%rbp)
	movq	-4384(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-4376(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-4368(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-4360(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-4352(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-4344(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-4336(%rbp), %rax
	movq	%rax, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm1
	leaq	72(%rax), %rdx
	movq	%rax, -4304(%rbp)
	movdqa	-144(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm2
	movq	%rcx, 64(%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm2, 48(%rax)
	movq	%rdx, -4288(%rbp)
	movq	%rdx, -4296(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1712
	call	_ZdlPv@PLT
.L1712:
	movq	-4656(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1711
	.p2align 4,,10
	.p2align 3
.L1895:
	movq	-4656(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -4400(%rbp)
	movq	$0, -4392(%rbp)
	movq	$0, -4384(%rbp)
	movq	$0, -4376(%rbp)
	movq	$0, -4368(%rbp)
	movq	$0, -4360(%rbp)
	movq	$0, -4352(%rbp)
	movq	$0, -4344(%rbp)
	movq	$0, -4336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4336(%rbp), %rax
	movq	%rbx, %rdi
	leaq	-4384(%rbp), %rcx
	pushq	%rax
	leaq	-4344(%rbp), %rax
	leaq	-4368(%rbp), %r9
	pushq	%rax
	leaq	-4352(%rbp), %rax
	leaq	-4376(%rbp), %r8
	pushq	%rax
	leaq	-4360(%rbp), %rax
	leaq	-4392(%rbp), %rdx
	pushq	%rax
	leaq	-4400(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_10HeapObjectENS0_6ObjectENS0_8UintPtrTENS0_6Int32TENS0_10JSReceiverENS0_3SmiENS0_12JSTypedArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EEPNSD_ISA_EEPNSD_ISB_EE
	addq	$32, %rsp
	movl	$174, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-144(%rbp), %rsi
	leaq	-80(%rbp), %rdx
	movq	%r15, %rdi
	movq	-4352(%rbp), %xmm0
	movq	-4368(%rbp), %xmm1
	movq	$0, -4288(%rbp)
	movq	-4384(%rbp), %xmm2
	movq	-4400(%rbp), %xmm3
	movhps	-4336(%rbp), %xmm0
	movhps	-4360(%rbp), %xmm1
	movhps	-4376(%rbp), %xmm2
	movhps	-4392(%rbp), %xmm3
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -4304(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4424(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1714
	call	_ZdlPv@PLT
.L1714:
	movq	-4688(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1713
	.p2align 4,,10
	.p2align 3
.L1896:
	movq	-4688(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -4288(%rbp)
	movaps	%xmm0, -4304(%rbp)
	call	_Znwm@PLT
	movq	-4424(%rbp), %rdi
	movq	%r15, %rsi
	movabsq	$506377902758496007, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -4304(%rbp)
	movq	%rdx, -4288(%rbp)
	movq	%rdx, -4296(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4304(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L1716
	call	_ZdlPv@PLT
.L1716:
	movq	(%r12), %rax
	movq	%r14, %rdi
	movq	24(%rax), %rdx
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	32(%rax), %r12
	movq	%rdx, -4672(%rbp)
	movq	40(%rax), %rdx
	movq	%rsi, -4648(%rbp)
	movq	16(%rax), %rsi
	movq	%rdx, -4680(%rbp)
	movq	48(%rax), %rdx
	movq	56(%rax), %rax
	movq	%rsi, -4656(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -4688(%rbp)
	movl	$143, %edx
	movq	%rcx, -4640(%rbp)
	movq	%rax, -4696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-144(%rbp), %rsi
	leaq	-80(%rbp), %rdx
	movq	%r15, %rdi
	movq	-4640(%rbp), %xmm0
	movq	$0, -4288(%rbp)
	movhps	-4648(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-4656(%rbp), %xmm0
	movhps	-4672(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%r12, %xmm0
	leaq	-336(%rbp), %r12
	movhps	-4680(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-4688(%rbp), %xmm0
	movhps	-4696(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -4304(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1717
	call	_ZdlPv@PLT
.L1717:
	movq	-4608(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1715
	.p2align 4,,10
	.p2align 3
.L1899:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-4464(%rbp), %xmm3
	movq	%r12, %rsi
	movdqa	-4544(%rbp), %xmm5
	movdqa	-4448(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	leaq	-64(%rbp), %rdx
	movq	%r13, %rdi
	movdqa	-4736(%rbp), %xmm6
	movdqa	-4576(%rbp), %xmm4
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm0, -4336(%rbp)
	movq	$0, -4320(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4480(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1681
	call	_ZdlPv@PLT
.L1681:
	movq	-4720(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1680
	.p2align 4,,10
	.p2align 3
.L1898:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-4464(%rbp), %xmm3
	movq	%r12, %rsi
	movdqa	-4448(%rbp), %xmm5
	movdqa	-4480(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rdx
	movq	%r13, %rdi
	movdqa	-4736(%rbp), %xmm6
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm0, -4336(%rbp)
	movq	$0, -4320(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4432(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1668
	call	_ZdlPv@PLT
.L1668:
	movq	-4512(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1667
.L1897:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22479:
	.size	_ZN2v88internal24ConstructByArrayLike_344EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3MapEEENS4_INS0_10HeapObjectEEENS4_INS0_6ObjectEEENS0_34TorqueStructTypedArrayElementsInfoENS4_INS0_10JSReceiverEEE, .-_ZN2v88internal24ConstructByArrayLike_344EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3MapEEENS4_INS0_10HeapObjectEEENS4_INS0_6ObjectEEENS0_34TorqueStructTypedArrayElementsInfoENS4_INS0_10JSReceiverEEE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EEPNSA_IS8_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EEPNSA_IS8_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EEPNSA_IS8_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EEPNSA_IS8_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EEPNSA_IS8_EE:
.LFB27587:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$7, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$1288, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$134678279, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$4, 6(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1901
	movq	%rax, -112(%rbp)
	call	_ZdlPv@PLT
	movq	-112(%rbp), %rax
.L1901:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1902
	movq	%rdx, (%r15)
.L1902:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1903
	movq	%rdx, (%r14)
.L1903:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1904
	movq	%rdx, 0(%r13)
.L1904:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1905
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1905:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1906
	movq	%rdx, (%rbx)
.L1906:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1907
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1907:
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L1900
	movq	-104(%rbp), %rbx
	movq	%rax, (%rbx)
.L1900:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1935
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1935:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27587:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EEPNSA_IS8_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EEPNSA_IS8_EE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EEPNSA_IS8_EESK_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EEPNSA_IS8_EESK_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EEPNSA_IS8_EESK_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EEPNSA_IS8_EESK_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EEPNSA_IS8_EESK_:
.LFB27589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$8, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$361419402149037831, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1937
	movq	%rax, -120(%rbp)
	call	_ZdlPv@PLT
	movq	-120(%rbp), %rax
.L1937:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1938
	movq	%rdx, (%r15)
.L1938:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1939
	movq	%rdx, (%r14)
.L1939:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1940
	movq	%rdx, 0(%r13)
.L1940:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1941
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1941:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1942
	movq	%rdx, (%rbx)
.L1942:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1943
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1943:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1944
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1944:
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L1936
	movq	-112(%rbp), %rbx
	movq	%rax, (%rbx)
.L1936:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1975
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1975:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27589:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EEPNSA_IS8_EESK_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EEPNSA_IS8_EESK_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_:
.LFB27603:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$10, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$361419402149037831, %rcx
	movq	%rcx, (%rax)
	movl	$1286, %ecx
	leaq	10(%rax), %rdx
	movw	%cx, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1977
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %rax
.L1977:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1978
	movq	%rdx, (%r15)
.L1978:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1979
	movq	%rdx, (%r14)
.L1979:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1980
	movq	%rdx, 0(%r13)
.L1980:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1981
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1981:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1982
	movq	%rdx, (%rbx)
.L1982:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1983
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1983:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1984
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1984:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1985
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1985:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1986
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1986:
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L1976
	movq	-128(%rbp), %rsi
	movq	%rax, (%rsi)
.L1976:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2023
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2023:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27603:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_SL_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_SL_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_SL_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_SL_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_SL_:
.LFB27609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$11, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$361419402149037831, %rcx
	movq	%rcx, (%rax)
	movl	$1286, %ecx
	leaq	11(%rax), %rdx
	movw	%cx, 8(%rax)
	movb	$5, 10(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2025
	movq	%rax, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %rax
.L2025:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2026
	movq	%rdx, (%r15)
.L2026:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2027
	movq	%rdx, (%r14)
.L2027:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2028
	movq	%rdx, 0(%r13)
.L2028:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2029
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L2029:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2030
	movq	%rdx, (%rbx)
.L2030:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2031
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2031:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2032
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2032:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2033
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2033:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2034
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2034:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2035
	movq	-128(%rbp), %rsi
	movq	%rdx, (%rsi)
.L2035:
	movq	80(%rax), %rax
	testq	%rax, %rax
	je	.L2024
	movq	-136(%rbp), %rcx
	movq	%rax, (%rcx)
.L2024:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2075
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2075:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27609:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_SL_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_SL_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_S7_S7_S8_S9_S9_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_SL_SL_SN_SP_SP_SL_SL_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_S7_S7_S8_S9_S9_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_SL_SL_SN_SP_SP_SL_SL_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_S7_S7_S8_S9_S9_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_SL_SL_SN_SP_SP_SL_SL_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_S7_S7_S8_S9_S9_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_SL_SL_SN_SP_SP_SL_SL_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_S7_S7_S8_S9_S9_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_SL_SL_SN_SP_SP_SL_SL_:
.LFB27619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$17, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$152, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -88(%rbp)
	movq	%r9, -96(%rbp)
	movq	104(%rbp), %r15
	movq	%rax, -104(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	88(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	96(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movdqa	.LC6(%rip), %xmm0
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movb	$5, 16(%rax)
	leaq	17(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2077
	movq	%rax, -192(%rbp)
	call	_ZdlPv@PLT
	movq	-192(%rbp), %rax
.L2077:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2078
	movq	%rdx, (%r14)
.L2078:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2079
	movq	%rdx, 0(%r13)
.L2079:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2080
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L2080:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2081
	movq	%rdx, (%rbx)
.L2081:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2082
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2082:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2083
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2083:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2084
	movq	-112(%rbp), %rcx
	movq	%rdx, (%rcx)
.L2084:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2085
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2085:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2086
	movq	-128(%rbp), %rcx
	movq	%rdx, (%rcx)
.L2086:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2087
	movq	-136(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2087:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2088
	movq	-144(%rbp), %rsi
	movq	%rdx, (%rsi)
.L2088:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2089
	movq	-152(%rbp), %rcx
	movq	%rdx, (%rcx)
.L2089:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2090
	movq	-160(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2090:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2091
	movq	-168(%rbp), %rsi
	movq	%rdx, (%rsi)
.L2091:
	movq	112(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2092
	movq	-176(%rbp), %rdi
	movq	%rdx, (%rdi)
.L2092:
	movq	120(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2093
	movq	-184(%rbp), %rcx
	movq	%rdx, (%rcx)
.L2093:
	movq	128(%rax), %rax
	testq	%rax, %rax
	je	.L2076
	movq	%rax, (%r15)
.L2076:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2151
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2151:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27619:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_S7_S7_S8_S9_S9_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_SL_SL_SN_SP_SP_SL_SL_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_S7_S7_S8_S9_S9_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_SL_SL_SN_SP_SP_SL_SL_
	.section	.rodata._ZN2v88internal26ConstructByArrayBuffer_347EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3MapEEENS4_INS0_13JSArrayBufferEEENS4_INS0_6ObjectEEESC_NS0_34TorqueStructTypedArrayElementsInfoE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"start offset"
.LC9:
	.string	"byte length"
	.section	.text._ZN2v88internal26ConstructByArrayBuffer_347EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3MapEEENS4_INS0_13JSArrayBufferEEENS4_INS0_6ObjectEEESC_NS0_34TorqueStructTypedArrayElementsInfoE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26ConstructByArrayBuffer_347EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3MapEEENS4_INS0_13JSArrayBufferEEENS4_INS0_6ObjectEEESC_NS0_34TorqueStructTypedArrayElementsInfoE
	.type	_ZN2v88internal26ConstructByArrayBuffer_347EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3MapEEENS4_INS0_13JSArrayBufferEEENS4_INS0_6ObjectEEESC_NS0_34TorqueStructTypedArrayElementsInfoE, @function
_ZN2v88internal26ConstructByArrayBuffer_347EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3MapEEENS4_INS0_13JSArrayBufferEEENS4_INS0_6ObjectEEESC_NS0_34TorqueStructTypedArrayElementsInfoE:
.LFB22527:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$728, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	24(%rbp), %r14
	movq	%r9, -8448(%rbp)
	movq	%rdi, %r13
	movq	%rdx, %rbx
	leaq	-8360(%rbp), %r15
	movq	%rsi, -8384(%rbp)
	leaq	-8008(%rbp), %r12
	movq	%r8, -8464(%rbp)
	movq	%rcx, -8480(%rbp)
	movq	%rax, -8432(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	%rdi, -8360(%rbp)
	movq	%rdi, -8064(%rbp)
	movl	$168, %edi
	movq	$0, -8056(%rbp)
	movq	$0, -8048(%rbp)
	movq	$0, -8040(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -8056(%rbp)
	leaq	-8064(%rbp), %rax
	movq	%rdx, -8040(%rbp)
	movq	%rdx, -8048(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -8024(%rbp)
	movq	%rax, -8376(%rbp)
	movq	$0, -8032(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8360(%rbp), %rax
	movl	$192, %edi
	movq	$0, -7864(%rbp)
	movq	$0, -7856(%rbp)
	movq	%rax, -7872(%rbp)
	movq	$0, -7848(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-7816(%rbp), %rcx
	movq	%r15, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -7848(%rbp)
	movq	%rdx, -7856(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -8600(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -7832(%rbp)
	movq	%rax, -7864(%rbp)
	movq	$0, -7840(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8360(%rbp), %rax
	movl	$216, %edi
	movq	$0, -7672(%rbp)
	movq	$0, -7664(%rbp)
	movq	%rax, -7680(%rbp)
	movq	$0, -7656(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-7624(%rbp), %rcx
	movq	%r15, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -7656(%rbp)
	movq	%rdx, -7664(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -8608(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -7640(%rbp)
	movq	%rax, -7672(%rbp)
	movq	$0, -7648(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8360(%rbp), %rax
	movl	$240, %edi
	movq	$0, -7480(%rbp)
	movq	$0, -7472(%rbp)
	movq	%rax, -7488(%rbp)
	movq	$0, -7464(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-7432(%rbp), %rcx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rdx, -7464(%rbp)
	movq	%rdx, -7472(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -8648(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -7448(%rbp)
	movq	%rax, -7480(%rbp)
	movq	$0, -7456(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8360(%rbp), %rax
	movl	$192, %edi
	movq	$0, -7288(%rbp)
	movq	$0, -7280(%rbp)
	movq	%rax, -7296(%rbp)
	movq	$0, -7272(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-7240(%rbp), %rcx
	movq	%r15, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -7272(%rbp)
	movq	%rdx, -7280(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -8688(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -7256(%rbp)
	movq	%rax, -7288(%rbp)
	movq	$0, -7264(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8360(%rbp), %rax
	movl	$216, %edi
	movq	$0, -7096(%rbp)
	movq	$0, -7088(%rbp)
	movq	%rax, -7104(%rbp)
	movq	$0, -7080(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-7048(%rbp), %rcx
	movq	%r15, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rcx, %rdi
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -7080(%rbp)
	movq	%rdx, -7088(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -8800(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -7064(%rbp)
	movq	%rax, -7096(%rbp)
	movq	$0, -7072(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-6912(%rbp), %rcx
	movl	$13, %edx
	movq	%r15, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -8560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-6720(%rbp), %rcx
	movl	$8, %edx
	movq	%r15, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -8488(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-6528(%rbp), %rcx
	movl	$8, %edx
	movq	%r15, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -8504(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-6336(%rbp), %rcx
	movl	$8, %edx
	movq	%r15, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -8392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-8360(%rbp), %rax
	movl	$240, %edi
	movq	$0, -6136(%rbp)
	movq	$0, -6128(%rbp)
	movq	%rax, -6144(%rbp)
	movq	$0, -6120(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-6088(%rbp), %rcx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rdx, -6120(%rbp)
	movq	%rdx, -6128(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -8720(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -6104(%rbp)
	movq	%rax, -6136(%rbp)
	movq	$0, -6112(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8360(%rbp), %rax
	movl	$264, %edi
	movq	$0, -5944(%rbp)
	movq	$0, -5936(%rbp)
	movq	%rax, -5952(%rbp)
	movq	$0, -5928(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-5896(%rbp), %rcx
	movq	%r15, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rdx, -5928(%rbp)
	movq	%rdx, -5936(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -8832(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -5912(%rbp)
	movq	%rax, -5944(%rbp)
	movq	$0, -5920(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8360(%rbp), %rax
	movl	$288, %edi
	movq	$0, -5752(%rbp)
	movq	$0, -5744(%rbp)
	movq	%rax, -5760(%rbp)
	movq	$0, -5736(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-5704(%rbp), %rcx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rcx, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rdx, -5736(%rbp)
	movq	%rdx, -5744(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -8736(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -5720(%rbp)
	movq	%rax, -5752(%rbp)
	movq	$0, -5728(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-5568(%rbp), %rcx
	movl	$10, %edx
	movq	%r15, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -8400(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-5376(%rbp), %rcx
	movl	$10, %edx
	movq	%r15, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -8552(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-5184(%rbp), %rcx
	movl	$10, %edx
	movq	%r15, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -8568(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-4992(%rbp), %rcx
	movl	$11, %edx
	movq	%r15, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -8496(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-4800(%rbp), %rcx
	movl	$16, %edx
	movq	%r15, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -8512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-4608(%rbp), %rcx
	movl	$11, %edx
	movq	%r15, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -8520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-4416(%rbp), %rcx
	movl	$11, %edx
	movq	%r15, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -8528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-4224(%rbp), %rcx
	movl	$11, %edx
	movq	%r15, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -8536(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-4032(%rbp), %rcx
	movl	$11, %edx
	movq	%r15, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -8544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3840(%rbp), %rcx
	movl	$16, %edx
	movq	%r15, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -8744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3648(%rbp), %rcx
	movl	$17, %edx
	movq	%r15, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -8616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3456(%rbp), %rcx
	movl	$16, %edx
	movq	%r15, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -8624(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3264(%rbp), %rcx
	movl	$11, %edx
	movq	%r15, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -8576(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3072(%rbp), %rcx
	movl	$17, %edx
	movq	%r15, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -8640(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2880(%rbp), %rcx
	movl	$17, %edx
	movq	%r15, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -8656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2688(%rbp), %rcx
	movl	$16, %edx
	movq	%r15, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -8664(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2496(%rbp), %rcx
	movl	$12, %edx
	movq	%r15, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -8672(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-8360(%rbp), %rax
	movl	$288, %edi
	movq	$0, -2296(%rbp)
	movq	$0, -2288(%rbp)
	movq	%rax, -2304(%rbp)
	movq	$0, -2280(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2248(%rbp), %rcx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rcx, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rdx, -2280(%rbp)
	movq	%rdx, -2288(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -8808(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2264(%rbp)
	movq	%rax, -2296(%rbp)
	movq	$0, -2272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-2112(%rbp), %rcx
	movl	$13, %edx
	movq	%r15, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -8416(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1920(%rbp), %rcx
	movl	$11, %edx
	movq	%r15, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -8704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1728(%rbp), %rcx
	movl	$11, %edx
	movq	%r15, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -8632(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1536(%rbp), %rcx
	movl	$11, %edx
	movq	%r15, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -8408(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-8360(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1336(%rbp)
	movq	$0, -1328(%rbp)
	movq	%rax, -1344(%rbp)
	movq	$0, -1320(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	leaq	192(%rax), %rdx
	leaq	-1288(%rbp), %rcx
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movq	%rcx, %rdi
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -1320(%rbp)
	movq	%rdx, -1328(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -8768(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1304(%rbp)
	movq	%rax, -1336(%rbp)
	movq	$0, -1312(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8360(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1144(%rbp)
	movq	$0, -1136(%rbp)
	movq	%rax, -1152(%rbp)
	movq	$0, -1128(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	leaq	168(%rax), %rdx
	leaq	-1096(%rbp), %rcx
	movups	%xmm0, (%rax)
	movq	$0, 160(%rax)
	movq	%rcx, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -1128(%rbp)
	movq	%rdx, -1136(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -8784(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1112(%rbp)
	movq	%rax, -1144(%rbp)
	movq	$0, -1120(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8360(%rbp), %rax
	movl	$168, %edi
	movq	$0, -952(%rbp)
	movq	$0, -944(%rbp)
	movq	%rax, -960(%rbp)
	movq	$0, -936(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	leaq	168(%rax), %rdx
	leaq	-904(%rbp), %rcx
	movups	%xmm0, (%rax)
	movq	$0, 160(%rax)
	movq	%rcx, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -936(%rbp)
	movq	%rdx, -944(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -8776(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -920(%rbp)
	movq	%rax, -952(%rbp)
	movq	$0, -928(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8360(%rbp), %rax
	movl	$168, %edi
	movq	$0, -760(%rbp)
	movq	$0, -752(%rbp)
	movq	%rax, -768(%rbp)
	movq	$0, -744(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	movq	%r15, %rsi
	leaq	168(%rax), %rdx
	leaq	-712(%rbp), %rcx
	movups	%xmm0, (%rax)
	movq	$0, 160(%rax)
	movq	%rcx, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -744(%rbp)
	movq	%rdx, -752(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -8760(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -728(%rbp)
	movq	%rax, -760(%rbp)
	movq	$0, -736(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-576(%rbp), %rcx
	movl	$8, %edx
	movq	%r15, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -8696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-8360(%rbp), %rax
	movl	$192, %edi
	movq	$0, -376(%rbp)
	movq	$0, -368(%rbp)
	movq	%rax, -384(%rbp)
	movq	$0, -360(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-328(%rbp), %rcx
	movq	%r15, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -360(%rbp)
	movq	%rdx, -368(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -8752(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -376(%rbp)
	movups	%xmm0, -344(%rbp)
	movq	$0, -352(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8384(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movq	-8480(%rbp), %r10
	movq	-8464(%rbp), %r11
	movq	-8448(%rbp), %r9
	movq	%rbx, -184(%rbp)
	movl	$56, %edi
	movq	-8432(%rbp), %rbx
	movq	%rsi, -192(%rbp)
	movq	%r10, -176(%rbp)
	movq	%r11, -168(%rbp)
	movq	%r9, -160(%rbp)
	movq	%r14, -144(%rbp)
	leaq	-8192(%rbp), %r14
	movaps	%xmm0, -8192(%rbp)
	movq	%rbx, -152(%rbp)
	movq	$0, -8176(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm5
	movdqa	-176(%rbp), %xmm6
	movdqa	-160(%rbp), %xmm7
	leaq	56(%rax), %rdx
	movq	%rax, -8192(%rbp)
	movq	%rcx, 48(%rax)
	movq	-8376(%rbp), %rdi
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -8176(%rbp)
	movq	%rdx, -8184(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2153
	call	_ZdlPv@PLT
.L2153:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7872(%rbp), %rax
	cmpq	$0, -8000(%rbp)
	movq	%rax, -8584(%rbp)
	jne	.L2476
.L2154:
	leaq	-7488(%rbp), %rax
	cmpq	$0, -7808(%rbp)
	movq	%rax, -8592(%rbp)
	leaq	-7680(%rbp), %rax
	movq	%rax, -8464(%rbp)
	jne	.L2477
.L2157:
	leaq	-7296(%rbp), %rax
	cmpq	$0, -7616(%rbp)
	movq	%rax, -8600(%rbp)
	jne	.L2478
.L2161:
	leaq	-7104(%rbp), %rax
	cmpq	$0, -7424(%rbp)
	movq	%rax, -8608(%rbp)
	jne	.L2479
.L2164:
	leaq	-768(%rbp), %rax
	cmpq	$0, -7232(%rbp)
	movq	%rax, -8384(%rbp)
	jne	.L2480
	cmpq	$0, -7040(%rbp)
	jne	.L2481
.L2169:
	cmpq	$0, -6848(%rbp)
	jne	.L2482
.L2172:
	leaq	-1344(%rbp), %rax
	cmpq	$0, -6656(%rbp)
	movq	%rax, -8432(%rbp)
	jne	.L2483
.L2176:
	cmpq	$0, -6464(%rbp)
	jne	.L2484
.L2178:
	leaq	-6144(%rbp), %rax
	cmpq	$0, -6272(%rbp)
	movq	%rax, -8648(%rbp)
	jne	.L2485
.L2180:
	leaq	-5760(%rbp), %rax
	cmpq	$0, -6080(%rbp)
	movq	%rax, -8688(%rbp)
	leaq	-5952(%rbp), %rax
	movq	%rax, -8480(%rbp)
	jne	.L2486
.L2183:
	leaq	-960(%rbp), %rax
	cmpq	$0, -5888(%rbp)
	movq	%rax, -8448(%rbp)
	jne	.L2487
	cmpq	$0, -5696(%rbp)
	jne	.L2488
.L2190:
	cmpq	$0, -5504(%rbp)
	jne	.L2489
.L2193:
	cmpq	$0, -5312(%rbp)
	jne	.L2490
.L2196:
	cmpq	$0, -5120(%rbp)
	jne	.L2491
.L2197:
	cmpq	$0, -4928(%rbp)
	jne	.L2492
.L2200:
	cmpq	$0, -4736(%rbp)
	jne	.L2493
.L2202:
	cmpq	$0, -4544(%rbp)
	jne	.L2494
.L2206:
	cmpq	$0, -4352(%rbp)
	jne	.L2495
.L2208:
	cmpq	$0, -4160(%rbp)
	jne	.L2496
.L2211:
	cmpq	$0, -3968(%rbp)
	jne	.L2497
.L2213:
	cmpq	$0, -3776(%rbp)
	jne	.L2498
.L2217:
	cmpq	$0, -3584(%rbp)
	jne	.L2499
.L2220:
	cmpq	$0, -3392(%rbp)
	jne	.L2500
.L2223:
	cmpq	$0, -3200(%rbp)
	jne	.L2501
.L2226:
	leaq	-1152(%rbp), %rax
	cmpq	$0, -3008(%rbp)
	movq	%rax, -8720(%rbp)
	jne	.L2502
	cmpq	$0, -2816(%rbp)
	jne	.L2503
.L2231:
	leaq	-2304(%rbp), %rax
	cmpq	$0, -2624(%rbp)
	movq	%rax, -8736(%rbp)
	jne	.L2504
	cmpq	$0, -2432(%rbp)
	jne	.L2505
.L2237:
	cmpq	$0, -2240(%rbp)
	jne	.L2506
.L2240:
	cmpq	$0, -2048(%rbp)
	jne	.L2507
.L2243:
	cmpq	$0, -1856(%rbp)
	jne	.L2508
.L2247:
	cmpq	$0, -1664(%rbp)
	jne	.L2509
.L2249:
	cmpq	$0, -1472(%rbp)
	jne	.L2510
.L2251:
	cmpq	$0, -1280(%rbp)
	jne	.L2511
.L2253:
	cmpq	$0, -1088(%rbp)
	jne	.L2512
.L2255:
	cmpq	$0, -896(%rbp)
	jne	.L2513
.L2256:
	cmpq	$0, -704(%rbp)
	jne	.L2514
.L2257:
	cmpq	$0, -512(%rbp)
	leaq	-384(%rbp), %r12
	jne	.L2515
.L2258:
	movq	-8752(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$505534590224893703, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -8176(%rbp)
	movaps	%xmm0, -8192(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -8192(%rbp)
	movq	%rdx, -8176(%rbp)
	movq	%rdx, -8184(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8192(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2261
	call	_ZdlPv@PLT
.L2261:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	56(%rax), %r13
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8696(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8384(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8448(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8720(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8432(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8408(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8632(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8704(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8416(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8736(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8672(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8664(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8656(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8640(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8576(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8624(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8616(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8744(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8544(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8536(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8528(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8520(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8512(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8496(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8568(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8552(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8400(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8688(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8480(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8648(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8392(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8504(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8488(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8560(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8608(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8600(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8592(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8464(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8584(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8376(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2516
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2476:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	$0, -8272(%rbp)
	leaq	-192(%rbp), %rbx
	movq	$0, -8264(%rbp)
	movq	$0, -8256(%rbp)
	movq	$0, -8248(%rbp)
	movq	$0, -8240(%rbp)
	movq	$0, -8232(%rbp)
	movq	$0, -8224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8224(%rbp), %rax
	movq	-8376(%rbp), %rdi
	leaq	-8256(%rbp), %rcx
	pushq	%rax
	leaq	-8232(%rbp), %rax
	leaq	-8240(%rbp), %r9
	pushq	%rax
	leaq	-8248(%rbp), %r8
	leaq	-8264(%rbp), %rdx
	leaq	-8272(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EEPNSA_IS8_EE
	movl	$216, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal46FromConstexpr9ATuintptr17ATconstexpr_int31_162EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$217, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -8384(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8248(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-128(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	-8224(%rbp), %rax
	movq	%r14, %rdi
	movq	-8240(%rbp), %xmm4
	movq	%rdx, -8592(%rbp)
	movq	-8256(%rbp), %xmm5
	movq	-8272(%rbp), %xmm6
	movaps	%xmm0, -8192(%rbp)
	movq	%rax, -8480(%rbp)
	movhps	-8232(%rbp), %xmm4
	movq	%rax, -144(%rbp)
	movq	-8384(%rbp), %rax
	movhps	-8248(%rbp), %xmm5
	movhps	-8264(%rbp), %xmm6
	movaps	%xmm4, -8464(%rbp)
	movq	%rax, -136(%rbp)
	movaps	%xmm5, -8448(%rbp)
	movaps	%xmm6, -8432(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movq	$0, -8176(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8584(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	popq	%rax
	popq	%rdx
	movq	-8592(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L2155
	call	_ZdlPv@PLT
	movq	-8592(%rbp), %rdx
.L2155:
	movdqa	-8432(%rbp), %xmm7
	movdqa	-8448(%rbp), %xmm6
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-8480(%rbp), %xmm0
	movq	$0, -8176(%rbp)
	movaps	%xmm7, -192(%rbp)
	movdqa	-8464(%rbp), %xmm7
	movhps	-8384(%rbp), %xmm0
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm0, -8192(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8392(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2156
	call	_ZdlPv@PLT
.L2156:
	movq	-8600(%rbp), %rdx
	leaq	-6280(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L2154
	.p2align 4,,10
	.p2align 3
.L2478:
	movq	-8608(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-192(%rbp), %rsi
	movabsq	$361419402149037831, %rax
	leaq	-183(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, -192(%rbp)
	movaps	%xmm0, -8192(%rbp)
	movb	$8, -184(%rbp)
	movq	$0, -8176(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8464(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8192(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2162
	call	_ZdlPv@PLT
.L2162:
	movq	(%rbx), %rax
	movl	$64, %edi
	movdqu	48(%rax), %xmm0
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movdqu	(%rax), %xmm6
	movq	$0, -8176(%rbp)
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -8192(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm7
	movdqa	-176(%rbp), %xmm6
	movq	%r14, %rsi
	leaq	64(%rax), %rdx
	movq	%rax, -8192(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-160(%rbp), %xmm7
	movups	%xmm6, 16(%rax)
	movdqa	-144(%rbp), %xmm6
	movups	%xmm7, 32(%rax)
	movups	%xmm6, 48(%rax)
	leaq	-7296(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -8176(%rbp)
	movq	%rdx, -8184(%rbp)
	movq	%rax, -8600(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2163
	call	_ZdlPv@PLT
.L2163:
	movq	-8688(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2161
	.p2align 4,,10
	.p2align 3
.L2477:
	movq	-8600(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -8288(%rbp)
	leaq	-8224(%rbp), %r12
	movq	$0, -8280(%rbp)
	movq	$0, -8272(%rbp)
	movq	$0, -8264(%rbp)
	movq	$0, -8256(%rbp)
	movq	$0, -8248(%rbp)
	movq	$0, -8240(%rbp)
	movq	$0, -8232(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-8232(%rbp), %rax
	movq	-8584(%rbp), %rdi
	pushq	%rax
	leaq	-8240(%rbp), %rax
	leaq	-8256(%rbp), %r9
	pushq	%rax
	leaq	-8248(%rbp), %rax
	leaq	-8264(%rbp), %r8
	pushq	%rax
	leaq	-8272(%rbp), %rcx
	leaq	-8280(%rbp), %rdx
	leaq	-8288(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EEPNSA_IS8_EESK_
	addq	$32, %rsp
	movl	$220, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8264(%rbp), %rdx
	movl	$1, %ecx
	movq	%r14, %rdi
	movq	-8288(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler16ToInteger_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_23ToIntegerTruncationModeE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	movq	%rax, -8432(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$219, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18TryNumberToUintPtrENS0_8compiler5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEEPNS2_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%rax, -8384(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-112(%rbp), %rdx
	movq	%r12, %rdi
	movq	-8240(%rbp), %xmm0
	movq	-8256(%rbp), %xmm1
	movq	-8272(%rbp), %xmm2
	movq	$0, -8208(%rbp)
	movq	-8288(%rbp), %xmm3
	movhps	-8232(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	leaq	-192(%rbp), %rbx
	movhps	-8248(%rbp), %xmm1
	movhps	-8384(%rbp), %xmm0
	movhps	-8264(%rbp), %xmm2
	movq	%rbx, %rsi
	movaps	%xmm1, -160(%rbp)
	movhps	-8280(%rbp), %xmm3
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm0, -8224(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8592(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2158
	call	_ZdlPv@PLT
.L2158:
	movq	-8648(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7680(%rbp), %rax
	cmpq	$0, -8184(%rbp)
	movq	%rax, -8464(%rbp)
	jne	.L2517
.L2159:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L2157
	.p2align 4,,10
	.p2align 3
.L2480:
	movq	-8688(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -8280(%rbp)
	movq	$0, -8272(%rbp)
	movq	$0, -8264(%rbp)
	movq	$0, -8256(%rbp)
	movq	$0, -8248(%rbp)
	movq	$0, -8240(%rbp)
	movq	$0, -8232(%rbp)
	movq	$0, -8224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-8224(%rbp), %rax
	movq	-8600(%rbp), %rdi
	pushq	%rax
	leaq	-8232(%rbp), %rax
	leaq	-8264(%rbp), %rcx
	pushq	%rax
	leaq	-8240(%rbp), %rax
	leaq	-8248(%rbp), %r9
	pushq	%rax
	leaq	-8256(%rbp), %r8
	leaq	-8272(%rbp), %rdx
	leaq	-8280(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EEPNSA_IS8_EESK_
	addq	$32, %rsp
	movl	$221, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-8280(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movaps	%xmm0, -8192(%rbp)
	movq	%rax, -192(%rbp)
	movq	-8272(%rbp), %rax
	movq	$0, -8176(%rbp)
	movq	%rax, -184(%rbp)
	movq	-8264(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-8256(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-8248(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-8240(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-8232(%rbp), %rax
	movq	%rax, -144(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm7
	movdqa	-176(%rbp), %xmm6
	movdqa	-160(%rbp), %xmm5
	leaq	56(%rax), %rdx
	movq	%rax, -8192(%rbp)
	movq	%rcx, 48(%rax)
	movq	-8384(%rbp), %rdi
	movups	%xmm7, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rdx, -8176(%rbp)
	movq	%rdx, -8184(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2168
	call	_ZdlPv@PLT
.L2168:
	movq	-8760(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -7040(%rbp)
	je	.L2169
.L2481:
	movq	-8800(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-192(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r14, %rdi
	leaq	-183(%rbp), %rdx
	movaps	%xmm0, -8192(%rbp)
	movabsq	$361419402149037831, %rax
	movq	%rax, -192(%rbp)
	movb	$5, -184(%rbp)
	movq	$0, -8176(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8608(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8192(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L2170
	call	_ZdlPv@PLT
.L2170:
	movq	(%r12), %rax
	movq	%r15, %rdi
	movq	(%rax), %rsi
	movq	24(%rax), %rdx
	movq	8(%rax), %rcx
	movq	40(%rax), %r12
	movq	%rsi, -8480(%rbp)
	movq	48(%rax), %rsi
	movq	%rdx, -8800(%rbp)
	movq	32(%rax), %rdx
	movq	%rcx, -8648(%rbp)
	movq	16(%rax), %rcx
	movq	64(%rax), %rax
	movq	%rdx, -8848(%rbp)
	movl	$219, %edx
	movq	%rsi, -8432(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -8688(%rbp)
	movq	%rax, -8448(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$224, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$48, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal46FromConstexpr9ATuintptr17ATconstexpr_int31_162EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -8864(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8864(%rbp), %r8
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEENS3_INS0_9IntegralTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -8880(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal46FromConstexpr9ATuintptr17ATconstexpr_int31_162EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -8864(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8880(%rbp), %r8
	movq	-8864(%rbp), %rdx
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -8864(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8864(%rbp), %rdx
	movq	-8448(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordAndENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -8880(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal46FromConstexpr9ATuintptr17ATconstexpr_int31_162EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -8864(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8880(%rbp), %r8
	movq	-8864(%rbp), %rdx
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -8864(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %xmm7
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-8480(%rbp), %xmm0
	leaq	-88(%rbp), %rdx
	movq	-8448(%rbp), %rax
	movq	$0, -8176(%rbp)
	movhps	-8648(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-8688(%rbp), %xmm0
	movhps	-8800(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-8848(%rbp), %xmm0
	punpcklqdq	%xmm7, %xmm0
	movq	%rax, %xmm7
	movaps	%xmm0, -160(%rbp)
	movq	-8432(%rbp), %xmm0
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%r12, %xmm0
	movhps	-8432(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%rax, %xmm0
	movq	-8864(%rbp), %rax
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -8192(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8560(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2171
	call	_ZdlPv@PLT
.L2171:
	leaq	-6856(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -6848(%rbp)
	je	.L2172
.L2482:
	leaq	-6856(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-192(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r14, %rdi
	leaq	-179(%rbp), %rdx
	movabsq	$361419402149037831, %rax
	movaps	%xmm0, -8192(%rbp)
	movq	%rax, -192(%rbp)
	movl	$84214789, -184(%rbp)
	movb	$4, -180(%rbp)
	movq	$0, -8176(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8560(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8192(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L2173
	call	_ZdlPv@PLT
.L2173:
	movq	(%r12), %rax
	movq	24(%rax), %rdx
	movq	40(%rax), %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rsi
	movq	56(%rax), %r10
	movq	96(%rax), %r12
	movq	%rdx, -8648(%rbp)
	movq	%rdi, -8800(%rbp)
	movq	32(%rax), %rdx
	movq	48(%rax), %rdi
	movq	%rcx, -8448(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -8432(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -8688(%rbp)
	movl	$224, %edx
	movq	%rdi, -8848(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -8480(%rbp)
	movq	%r10, -8864(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-128(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	-8848(%rbp), %xmm7
	movq	%r14, %rdi
	movq	-8688(%rbp), %xmm4
	movaps	%xmm0, -8192(%rbp)
	movq	-8480(%rbp), %xmm5
	movq	-8432(%rbp), %xmm6
	movq	$0, -8176(%rbp)
	movhps	-8864(%rbp), %xmm7
	movhps	-8800(%rbp), %xmm4
	movhps	-8448(%rbp), %xmm6
	movhps	-8648(%rbp), %xmm5
	movq	%rdx, -8448(%rbp)
	movaps	%xmm7, -8848(%rbp)
	movaps	%xmm4, -8688(%rbp)
	movaps	%xmm5, -8480(%rbp)
	movaps	%xmm6, -8432(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8488(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	movq	-8448(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L2174
	call	_ZdlPv@PLT
	movq	-8448(%rbp), %rdx
.L2174:
	movdqa	-8432(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movdqa	-8480(%rbp), %xmm5
	movdqa	-8688(%rbp), %xmm7
	movaps	%xmm0, -8192(%rbp)
	movaps	%xmm6, -192(%rbp)
	movdqa	-8848(%rbp), %xmm6
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movq	$0, -8176(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8504(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2175
	call	_ZdlPv@PLT
.L2175:
	leaq	-6472(%rbp), %rcx
	leaq	-6664(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L2172
	.p2align 4,,10
	.p2align 3
.L2479:
	movq	-8648(%rbp), %rsi
	movq	%r15, %rdi
	movl	$1288, %ebx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movw	%bx, -184(%rbp)
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	leaq	-192(%rbp), %rbx
	leaq	-182(%rbp), %rdx
	movabsq	$361419402149037831, %rax
	movaps	%xmm0, -8192(%rbp)
	movq	%rbx, %rsi
	movq	%rax, -192(%rbp)
	movq	$0, -8176(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8592(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8192(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L2165
	call	_ZdlPv@PLT
.L2165:
	movq	(%r12), %rax
	leaq	-120(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movdqu	48(%rax), %xmm0
	movdqu	(%rax), %xmm3
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	72(%rax), %rax
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -192(%rbp)
	movq	%rax, -128(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -8192(%rbp)
	movq	$0, -8176(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-7104(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -8608(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2166
	call	_ZdlPv@PLT
.L2166:
	movq	-8800(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2164
	.p2align 4,,10
	.p2align 3
.L2484:
	leaq	-6472(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -8280(%rbp)
	movq	$0, -8272(%rbp)
	movq	$0, -8264(%rbp)
	movq	$0, -8256(%rbp)
	movq	$0, -8248(%rbp)
	movq	$0, -8240(%rbp)
	movq	$0, -8232(%rbp)
	movq	$0, -8224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-8224(%rbp), %rax
	movq	-8504(%rbp), %rdi
	pushq	%rax
	leaq	-8232(%rbp), %rax
	leaq	-8264(%rbp), %rcx
	pushq	%rax
	leaq	-8240(%rbp), %rax
	leaq	-8248(%rbp), %r9
	pushq	%rax
	leaq	-8256(%rbp), %r8
	leaq	-8272(%rbp), %rdx
	leaq	-8280(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EEPNSA_IS8_EESK_
	addq	$32, %rsp
	movl	$217, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$64, %edi
	movq	-8232(%rbp), %xmm0
	movq	-8248(%rbp), %xmm1
	movq	-8264(%rbp), %xmm2
	movq	-8280(%rbp), %xmm3
	movq	$0, -8176(%rbp)
	movhps	-8224(%rbp), %xmm0
	movhps	-8240(%rbp), %xmm1
	movhps	-8256(%rbp), %xmm2
	movhps	-8272(%rbp), %xmm3
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -8192(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm6
	movdqa	-160(%rbp), %xmm5
	movq	-8392(%rbp), %rdi
	leaq	64(%rax), %rdx
	movq	%rax, -8192(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-144(%rbp), %xmm7
	movups	%xmm6, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rdx, -8176(%rbp)
	movq	%rdx, -8184(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2179
	call	_ZdlPv@PLT
.L2179:
	leaq	-6280(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2178
	.p2align 4,,10
	.p2align 3
.L2483:
	leaq	-6664(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -8280(%rbp)
	movq	$0, -8272(%rbp)
	movq	$0, -8264(%rbp)
	movq	$0, -8256(%rbp)
	movq	$0, -8248(%rbp)
	movq	$0, -8240(%rbp)
	movq	$0, -8232(%rbp)
	movq	$0, -8224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-8224(%rbp), %rax
	movq	-8488(%rbp), %rdi
	pushq	%rax
	leaq	-8232(%rbp), %rax
	leaq	-8264(%rbp), %rcx
	pushq	%rax
	leaq	-8240(%rbp), %rax
	leaq	-8248(%rbp), %r9
	pushq	%rax
	leaq	-8256(%rbp), %r8
	leaq	-8272(%rbp), %rdx
	leaq	-8280(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EEPNSA_IS8_EESK_
	addq	$32, %rsp
	movl	$225, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	.LC7(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	-192(%rbp), %rsi
	leaq	-128(%rbp), %rdx
	movq	%r14, %rdi
	movq	-8232(%rbp), %xmm0
	movq	%rax, %xmm5
	movq	-8248(%rbp), %xmm1
	movq	$0, -8176(%rbp)
	movq	-8264(%rbp), %xmm2
	movq	-8280(%rbp), %xmm3
	punpcklqdq	%xmm5, %xmm0
	movhps	-8240(%rbp), %xmm1
	movhps	-8256(%rbp), %xmm2
	movhps	-8272(%rbp), %xmm3
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -8192(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8432(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2177
	call	_ZdlPv@PLT
.L2177:
	movq	-8768(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2176
	.p2align 4,,10
	.p2align 3
.L2487:
	movq	-8832(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1286, %r11d
	pxor	%xmm0, %xmm0
	movabsq	$361419402149037831, %rax
	leaq	-192(%rbp), %rsi
	leaq	-181(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, -192(%rbp)
	movw	%r11w, -184(%rbp)
	movaps	%xmm0, -8192(%rbp)
	movb	$8, -182(%rbp)
	movq	$0, -8176(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8480(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8192(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2188
	call	_ZdlPv@PLT
.L2188:
	movq	(%rbx), %rax
	movl	$56, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movq	48(%rax), %rdx
	movdqu	(%rax), %xmm6
	movq	$0, -8176(%rbp)
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -144(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -8192(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm4
	movdqa	-176(%rbp), %xmm5
	movdqa	-160(%rbp), %xmm7
	leaq	56(%rax), %rdx
	movq	%rax, -8192(%rbp)
	movq	%rcx, 48(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm7, 32(%rax)
	leaq	-960(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -8176(%rbp)
	movq	%rdx, -8184(%rbp)
	movq	%rax, -8448(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2189
	call	_ZdlPv@PLT
.L2189:
	movq	-8776(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -5696(%rbp)
	je	.L2190
.L2488:
	movq	-8736(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-192(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r14, %rdi
	leaq	-180(%rbp), %rdx
	movabsq	$361419402149037831, %rax
	movaps	%xmm0, -8192(%rbp)
	movq	%rax, -192(%rbp)
	movl	$101188870, -184(%rbp)
	movq	$0, -8176(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8688(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8192(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L2191
	call	_ZdlPv@PLT
.L2191:
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	24(%rax), %rcx
	movq	%rsi, -8720(%rbp)
	movq	16(%rax), %rsi
	movq	%rdx, -8848(%rbp)
	movq	48(%rax), %rdx
	movq	%rdi, -8880(%rbp)
	movq	72(%rax), %rdi
	movq	(%rax), %r12
	movq	%rcx, -8800(%rbp)
	movq	32(%rax), %rcx
	movq	88(%rax), %rax
	movq	%rsi, -8736(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -8864(%rbp)
	movl	$232, %edx
	movq	%rdi, -8896(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -8832(%rbp)
	movq	%rax, -8912(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %xmm0
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movhps	-8720(%rbp), %xmm0
	leaq	-112(%rbp), %rdx
	movq	$0, -8176(%rbp)
	movaps	%xmm0, -192(%rbp)
	movq	-8736(%rbp), %xmm0
	movhps	-8800(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-8832(%rbp), %xmm0
	movhps	-8848(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-8864(%rbp), %xmm0
	movhps	-8880(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-8912(%rbp), %xmm0
	movhps	-8896(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8192(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8400(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2192
	call	_ZdlPv@PLT
.L2192:
	leaq	-5512(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -5504(%rbp)
	je	.L2193
.L2489:
	leaq	-5512(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -8296(%rbp)
	leaq	-192(%rbp), %rbx
	movq	$0, -8288(%rbp)
	movq	$0, -8280(%rbp)
	movq	$0, -8272(%rbp)
	movq	$0, -8264(%rbp)
	movq	$0, -8256(%rbp)
	movq	$0, -8248(%rbp)
	movq	$0, -8240(%rbp)
	movq	$0, -8232(%rbp)
	movq	$0, -8224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-8224(%rbp), %rax
	movq	-8400(%rbp), %rdi
	pushq	%rax
	leaq	-8232(%rbp), %rax
	leaq	-8280(%rbp), %rcx
	pushq	%rax
	leaq	-8240(%rbp), %rax
	leaq	-8264(%rbp), %r9
	pushq	%rax
	leaq	-8248(%rbp), %rax
	leaq	-8272(%rbp), %r8
	pushq	%rax
	leaq	-8256(%rbp), %rax
	leaq	-8288(%rbp), %rdx
	pushq	%rax
	leaq	-8296(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_
	addq	$48, %rsp
	movl	$238, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8280(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler16IsDetachedBufferENS0_8compiler5TNodeINS0_13JSArrayBufferEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-112(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	-8232(%rbp), %xmm5
	movq	%r14, %rdi
	movq	-8248(%rbp), %xmm6
	movq	%rdx, -8864(%rbp)
	movq	-8264(%rbp), %xmm7
	movaps	%xmm0, -8192(%rbp)
	movq	-8280(%rbp), %xmm1
	movq	-8296(%rbp), %xmm3
	movhps	-8224(%rbp), %xmm5
	movhps	-8240(%rbp), %xmm6
	movq	$0, -8176(%rbp)
	movhps	-8256(%rbp), %xmm7
	movhps	-8272(%rbp), %xmm1
	movaps	%xmm5, -8800(%rbp)
	movhps	-8288(%rbp), %xmm3
	movaps	%xmm6, -8832(%rbp)
	movaps	%xmm7, -8848(%rbp)
	movaps	%xmm1, -8720(%rbp)
	movaps	%xmm3, -8736(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8552(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	movq	-8864(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L2194
	call	_ZdlPv@PLT
	movq	-8864(%rbp), %rdx
.L2194:
	movdqa	-8736(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movdqa	-8720(%rbp), %xmm4
	movdqa	-8848(%rbp), %xmm5
	movdqa	-8832(%rbp), %xmm7
	movq	%r14, %rdi
	movaps	%xmm0, -8192(%rbp)
	movaps	%xmm6, -192(%rbp)
	movdqa	-8800(%rbp), %xmm6
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movq	$0, -8176(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8568(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2195
	call	_ZdlPv@PLT
.L2195:
	leaq	-5128(%rbp), %rcx
	leaq	-5320(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -5312(%rbp)
	je	.L2196
.L2490:
	leaq	-5320(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -8296(%rbp)
	movq	$0, -8288(%rbp)
	movq	$0, -8280(%rbp)
	movq	$0, -8272(%rbp)
	movq	$0, -8264(%rbp)
	movq	$0, -8256(%rbp)
	movq	$0, -8248(%rbp)
	movq	$0, -8240(%rbp)
	movq	$0, -8232(%rbp)
	movq	$0, -8224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-8224(%rbp), %rax
	movq	-8552(%rbp), %rdi
	pushq	%rax
	leaq	-8232(%rbp), %rax
	leaq	-8264(%rbp), %r9
	pushq	%rax
	leaq	-8240(%rbp), %rax
	leaq	-8280(%rbp), %rcx
	pushq	%rax
	leaq	-8248(%rbp), %rax
	leaq	-8272(%rbp), %r8
	pushq	%rax
	leaq	-8256(%rbp), %rax
	leaq	-8288(%rbp), %rdx
	pushq	%rax
	leaq	-8296(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_
	addq	$48, %rsp
	movl	$239, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	movl	$50, %edx
	movq	%r14, %rdi
	movq	-8296(%rbp), %rsi
	leaq	.LC5(%rip), %rcx
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -5120(%rbp)
	je	.L2197
.L2491:
	leaq	-5128(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -8296(%rbp)
	movq	$0, -8288(%rbp)
	movq	$0, -8280(%rbp)
	movq	$0, -8272(%rbp)
	movq	$0, -8264(%rbp)
	movq	$0, -8256(%rbp)
	movq	$0, -8248(%rbp)
	movq	$0, -8240(%rbp)
	movq	$0, -8232(%rbp)
	movq	$0, -8224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-8224(%rbp), %rax
	movq	-8568(%rbp), %rdi
	pushq	%rax
	leaq	-8232(%rbp), %rax
	leaq	-8264(%rbp), %r9
	pushq	%rax
	leaq	-8240(%rbp), %rax
	leaq	-8272(%rbp), %r8
	pushq	%rax
	leaq	-8248(%rbp), %rax
	leaq	-8280(%rbp), %rcx
	pushq	%rax
	leaq	-8256(%rbp), %rax
	leaq	-8288(%rbp), %rdx
	pushq	%rax
	leaq	-8296(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_
	addq	$48, %rsp
	movl	$243, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	movq	-8280(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE(%rip), %esi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$246, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8264(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$88, %edi
	movq	-8232(%rbp), %xmm2
	movq	-8248(%rbp), %xmm4
	movaps	%xmm0, -8192(%rbp)
	movq	-8264(%rbp), %xmm5
	movq	-8280(%rbp), %xmm6
	movhps	-8224(%rbp), %xmm2
	movq	%rbx, -112(%rbp)
	movq	-8296(%rbp), %xmm7
	movhps	-8240(%rbp), %xmm4
	movaps	%xmm2, -128(%rbp)
	movhps	-8256(%rbp), %xmm5
	movhps	-8272(%rbp), %xmm6
	movaps	%xmm2, -8832(%rbp)
	movhps	-8288(%rbp), %xmm7
	movaps	%xmm4, -8848(%rbp)
	movaps	%xmm5, -8720(%rbp)
	movaps	%xmm6, -8736(%rbp)
	movaps	%xmm7, -8800(%rbp)
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movq	$0, -8176(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm4
	movq	%r14, %rsi
	movq	-112(%rbp), %rcx
	movdqa	-176(%rbp), %xmm5
	movdqa	-160(%rbp), %xmm7
	leaq	88(%rax), %rdx
	movq	%rax, -8192(%rbp)
	movdqa	-144(%rbp), %xmm6
	movups	%xmm4, (%rax)
	movdqa	-128(%rbp), %xmm4
	movq	%rcx, 80(%rax)
	movq	-8496(%rbp), %rdi
	movups	%xmm5, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm4, 64(%rax)
	movq	%rdx, -8176(%rbp)
	movq	%rdx, -8184(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2198
	call	_ZdlPv@PLT
.L2198:
	movdqa	-8800(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movq	%rbx, -112(%rbp)
	movl	$88, %edi
	movdqa	-8736(%rbp), %xmm7
	movdqa	-8720(%rbp), %xmm6
	movaps	%xmm0, -8192(%rbp)
	movdqa	-8848(%rbp), %xmm4
	movaps	%xmm5, -192(%rbp)
	movdqa	-8832(%rbp), %xmm5
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movq	$0, -8176(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm7
	movq	%r14, %rsi
	movq	-112(%rbp), %rcx
	movdqa	-176(%rbp), %xmm6
	movdqa	-160(%rbp), %xmm4
	leaq	88(%rax), %rdx
	movq	%rax, -8192(%rbp)
	movdqa	-144(%rbp), %xmm5
	movups	%xmm7, (%rax)
	movdqa	-128(%rbp), %xmm7
	movq	%rcx, 80(%rax)
	movq	-8576(%rbp), %rdi
	movups	%xmm6, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm7, 64(%rax)
	movq	%rdx, -8176(%rbp)
	movq	%rdx, -8184(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2199
	call	_ZdlPv@PLT
.L2199:
	leaq	-3208(%rbp), %rcx
	leaq	-4936(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -4928(%rbp)
	je	.L2200
.L2492:
	leaq	-4936(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -8304(%rbp)
	movq	$0, -8296(%rbp)
	movq	$0, -8288(%rbp)
	movq	$0, -8280(%rbp)
	movq	$0, -8272(%rbp)
	movq	$0, -8264(%rbp)
	movq	$0, -8256(%rbp)
	movq	$0, -8248(%rbp)
	movq	$0, -8240(%rbp)
	movq	$0, -8232(%rbp)
	movq	$0, -8224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8224(%rbp), %rax
	movq	-8496(%rbp), %rdi
	leaq	-8288(%rbp), %rcx
	pushq	%rax
	leaq	-8232(%rbp), %rax
	leaq	-8272(%rbp), %r9
	pushq	%rax
	leaq	-8240(%rbp), %rax
	leaq	-8280(%rbp), %r8
	pushq	%rax
	leaq	-8248(%rbp), %rax
	leaq	-8296(%rbp), %rdx
	pushq	%rax
	leaq	-8256(%rbp), %rax
	leaq	-8304(%rbp), %rsi
	pushq	%rax
	leaq	-8264(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_SL_
	addq	$48, %rsp
	movl	$249, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$48, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal46FromConstexpr9ATuintptr17ATconstexpr_int31_162EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8264(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEENS3_INS0_9IntegralTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal46FromConstexpr9ATuintptr17ATconstexpr_int31_162EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8224(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordAndENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal46FromConstexpr9ATuintptr17ATconstexpr_int31_162EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-64(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rbx, -72(%rbp)
	movq	-8240(%rbp), %xmm0
	movq	-8224(%rbp), %rax
	leaq	-192(%rbp), %rsi
	movq	$0, -8176(%rbp)
	movq	-8256(%rbp), %xmm4
	movq	-8272(%rbp), %xmm1
	movhps	-8232(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movq	-8288(%rbp), %xmm2
	movaps	%xmm0, -128(%rbp)
	movhps	-8248(%rbp), %xmm4
	movq	-8224(%rbp), %xmm0
	movq	-8304(%rbp), %xmm3
	movhps	-8264(%rbp), %xmm1
	movhps	-8280(%rbp), %xmm2
	movaps	%xmm4, -144(%rbp)
	movhps	-8264(%rbp), %xmm0
	movhps	-8296(%rbp), %xmm3
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	-8256(%rbp), %xmm0
	movaps	%xmm3, -192(%rbp)
	movhps	-8224(%rbp), %xmm0
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8192(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8512(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2201
	call	_ZdlPv@PLT
.L2201:
	leaq	-4744(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -4736(%rbp)
	je	.L2202
.L2493:
	leaq	-4744(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC8(%rip), %xmm0
	leaq	-192(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-176(%rbp), %rdx
	movq	$0, -8176(%rbp)
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8192(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8512(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8192(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2203
	call	_ZdlPv@PLT
.L2203:
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	24(%rax), %rcx
	movq	(%rax), %rbx
	movq	72(%rax), %r11
	movq	%rsi, -8736(%rbp)
	movq	16(%rax), %rsi
	movq	%rdx, -8864(%rbp)
	movq	%rdi, -8896(%rbp)
	movq	48(%rax), %rdx
	movq	64(%rax), %rdi
	movq	%rcx, -8832(%rbp)
	movq	32(%rax), %rcx
	movq	%rsi, -8800(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -8880(%rbp)
	movl	$249, %edx
	movq	120(%rax), %r12
	movq	%rdi, -8912(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -8848(%rbp)
	movq	%r11, -8928(%rbp)
	movq	%rbx, -8720(%rbp)
	movq	80(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm0, %xmm0
	movl	$88, %edi
	movq	-8912(%rbp), %xmm1
	movq	-8880(%rbp), %xmm3
	movaps	%xmm0, -8192(%rbp)
	movq	-8848(%rbp), %xmm2
	movq	-8800(%rbp), %xmm4
	movhps	-8928(%rbp), %xmm1
	movq	%rbx, -112(%rbp)
	movq	-8720(%rbp), %xmm5
	movhps	-8896(%rbp), %xmm3
	movaps	%xmm1, -128(%rbp)
	movhps	-8864(%rbp), %xmm2
	movhps	-8832(%rbp), %xmm4
	movaps	%xmm1, -8912(%rbp)
	movhps	-8736(%rbp), %xmm5
	movaps	%xmm3, -8880(%rbp)
	movaps	%xmm2, -8848(%rbp)
	movaps	%xmm4, -8800(%rbp)
	movaps	%xmm5, -8720(%rbp)
	movaps	%xmm5, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movq	$0, -8176(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm6
	movq	%r14, %rsi
	movq	-112(%rbp), %rcx
	movdqa	-176(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm5
	leaq	88(%rax), %rdx
	movq	%rax, -8192(%rbp)
	movdqa	-144(%rbp), %xmm7
	movups	%xmm6, (%rax)
	movdqa	-128(%rbp), %xmm6
	movq	%rcx, 80(%rax)
	movq	-8520(%rbp), %rdi
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm6, 64(%rax)
	movq	%rdx, -8176(%rbp)
	movq	%rdx, -8184(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2204
	call	_ZdlPv@PLT
.L2204:
	movdqa	-8720(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movq	%rbx, -112(%rbp)
	movl	$88, %edi
	movdqa	-8800(%rbp), %xmm5
	movdqa	-8848(%rbp), %xmm7
	movaps	%xmm0, -8192(%rbp)
	movdqa	-8880(%rbp), %xmm6
	movaps	%xmm4, -192(%rbp)
	movdqa	-8912(%rbp), %xmm4
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movq	$0, -8176(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm5
	movq	%r14, %rsi
	movq	-112(%rbp), %rcx
	movdqa	-176(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm6
	leaq	88(%rax), %rdx
	movq	%rax, -8192(%rbp)
	movdqa	-144(%rbp), %xmm4
	movups	%xmm5, (%rax)
	movdqa	-128(%rbp), %xmm5
	movq	%rcx, 80(%rax)
	movq	-8528(%rbp), %rdi
	movups	%xmm7, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rdx, -8176(%rbp)
	movq	%rdx, -8184(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2205
	call	_ZdlPv@PLT
.L2205:
	leaq	-4360(%rbp), %rcx
	leaq	-4552(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -4544(%rbp)
	je	.L2206
.L2494:
	leaq	-4552(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -8304(%rbp)
	movq	$0, -8296(%rbp)
	movq	$0, -8288(%rbp)
	movq	$0, -8280(%rbp)
	movq	$0, -8272(%rbp)
	movq	$0, -8264(%rbp)
	movq	$0, -8256(%rbp)
	movq	$0, -8248(%rbp)
	movq	$0, -8240(%rbp)
	movq	$0, -8232(%rbp)
	movq	$0, -8224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8224(%rbp), %rax
	movq	-8520(%rbp), %rdi
	leaq	-8288(%rbp), %rcx
	pushq	%rax
	leaq	-8232(%rbp), %rax
	leaq	-8272(%rbp), %r9
	pushq	%rax
	leaq	-8240(%rbp), %rax
	leaq	-8280(%rbp), %r8
	pushq	%rax
	leaq	-8248(%rbp), %rax
	leaq	-8296(%rbp), %rdx
	pushq	%rax
	leaq	-8256(%rbp), %rax
	leaq	-8304(%rbp), %rsi
	pushq	%rax
	leaq	-8264(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_SL_
	addq	$48, %rsp
	movl	$250, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	.LC9(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	leaq	-192(%rbp), %rsi
	leaq	-128(%rbp), %rdx
	movq	%r14, %rdi
	movq	-8256(%rbp), %xmm0
	movq	%rax, %xmm7
	movq	-8272(%rbp), %xmm1
	movq	$0, -8176(%rbp)
	movq	-8288(%rbp), %xmm2
	movq	-8304(%rbp), %xmm3
	punpcklqdq	%xmm7, %xmm0
	movhps	-8264(%rbp), %xmm1
	movhps	-8280(%rbp), %xmm2
	movhps	-8296(%rbp), %xmm3
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -8192(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8432(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2207
	call	_ZdlPv@PLT
.L2207:
	movq	-8768(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -4352(%rbp)
	je	.L2208
.L2495:
	leaq	-4360(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -8304(%rbp)
	movq	$0, -8296(%rbp)
	movq	$0, -8288(%rbp)
	movq	$0, -8280(%rbp)
	movq	$0, -8272(%rbp)
	movq	$0, -8264(%rbp)
	movq	$0, -8256(%rbp)
	movq	$0, -8248(%rbp)
	movq	$0, -8240(%rbp)
	movq	$0, -8232(%rbp)
	movq	$0, -8224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8224(%rbp), %rax
	movq	-8528(%rbp), %rdi
	leaq	-8288(%rbp), %rcx
	pushq	%rax
	leaq	-8232(%rbp), %rax
	leaq	-8272(%rbp), %r9
	pushq	%rax
	leaq	-8240(%rbp), %rax
	leaq	-8280(%rbp), %r8
	pushq	%rax
	leaq	-8248(%rbp), %rax
	leaq	-8296(%rbp), %rdx
	pushq	%rax
	leaq	-8256(%rbp), %rax
	leaq	-8304(%rbp), %rsi
	pushq	%rax
	leaq	-8264(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_SL_
	addq	$48, %rsp
	movl	$255, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8248(%rbp), %rdx
	movq	-8224(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-8304(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	-8232(%rbp), %rbx
	movq	-8272(%rbp), %rdi
	movq	-8296(%rbp), %rsi
	movq	-8288(%rbp), %rcx
	movq	-8280(%rbp), %rdx
	movq	%rbx, -8720(%rbp)
	movq	-8264(%rbp), %r10
	movq	-8256(%rbp), %r11
	movq	%rax, -8736(%rbp)
	movq	-8248(%rbp), %r9
	movq	-8240(%rbp), %r8
	movq	%rax, -192(%rbp)
	movq	-8224(%rbp), %rbx
	movq	-8720(%rbp), %rax
	movq	%rdi, -8864(%rbp)
	movq	%rdi, -160(%rbp)
	movl	$88, %edi
	movq	%rsi, -8800(%rbp)
	movq	%rcx, -8832(%rbp)
	movq	%rdx, -8848(%rbp)
	movq	%r10, -8880(%rbp)
	movq	%r11, -8896(%rbp)
	movq	%r9, -8912(%rbp)
	movq	%r8, -8928(%rbp)
	movq	%rsi, -184(%rbp)
	movq	%rcx, -176(%rbp)
	movq	%rdx, -168(%rbp)
	movq	%r10, -152(%rbp)
	movq	%r11, -144(%rbp)
	movq	%r9, -136(%rbp)
	movq	%r8, -128(%rbp)
	movq	%rax, -120(%rbp)
	movq	%rbx, -112(%rbp)
	movaps	%xmm0, -8192(%rbp)
	movq	$0, -8176(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm6
	movq	%r14, %rsi
	movq	-112(%rbp), %rcx
	movdqa	-176(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm5
	leaq	88(%rax), %rdx
	movq	%rax, -8192(%rbp)
	movdqa	-144(%rbp), %xmm7
	movups	%xmm6, (%rax)
	movdqa	-128(%rbp), %xmm6
	movq	%rcx, 80(%rax)
	movq	-8536(%rbp), %rdi
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm6, 64(%rax)
	movq	%rdx, -8176(%rbp)
	movq	%rdx, -8184(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2209
	call	_ZdlPv@PLT
.L2209:
	movq	-8736(%rbp), %xmm0
	movl	$88, %edi
	movq	%rbx, -112(%rbp)
	movq	$0, -8176(%rbp)
	movhps	-8800(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-8832(%rbp), %xmm0
	movhps	-8848(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-8864(%rbp), %xmm0
	movhps	-8880(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-8896(%rbp), %xmm0
	movhps	-8912(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-8928(%rbp), %xmm0
	movhps	-8720(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8192(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm4
	movdqa	-128(%rbp), %xmm1
	movq	-8544(%rbp), %rdi
	leaq	88(%rax), %rdx
	movq	%rax, -8192(%rbp)
	movdqa	-176(%rbp), %xmm5
	movdqa	-160(%rbp), %xmm7
	movq	%rcx, 80(%rax)
	movdqa	-144(%rbp), %xmm6
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm1, 64(%rax)
	movq	%rdx, -8176(%rbp)
	movq	%rdx, -8184(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2210
	call	_ZdlPv@PLT
.L2210:
	leaq	-3976(%rbp), %rcx
	leaq	-4168(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -4160(%rbp)
	je	.L2211
.L2496:
	leaq	-4168(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -8304(%rbp)
	movq	$0, -8296(%rbp)
	movq	$0, -8288(%rbp)
	movq	$0, -8280(%rbp)
	movq	$0, -8272(%rbp)
	movq	$0, -8264(%rbp)
	movq	$0, -8256(%rbp)
	movq	$0, -8248(%rbp)
	movq	$0, -8240(%rbp)
	movq	$0, -8232(%rbp)
	movq	$0, -8224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8224(%rbp), %rax
	movq	-8536(%rbp), %rdi
	leaq	-8288(%rbp), %rcx
	pushq	%rax
	leaq	-8232(%rbp), %rax
	leaq	-8296(%rbp), %rdx
	pushq	%rax
	leaq	-8240(%rbp), %rax
	leaq	-8304(%rbp), %rsi
	pushq	%rax
	leaq	-8248(%rbp), %rax
	leaq	-8272(%rbp), %r9
	pushq	%rax
	leaq	-8256(%rbp), %rax
	leaq	-8280(%rbp), %r8
	pushq	%rax
	leaq	-8264(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_SL_
	movq	-8304(%rbp), %rax
	pxor	%xmm0, %xmm0
	addq	$48, %rsp
	movl	$56, %edi
	movaps	%xmm0, -8192(%rbp)
	movq	%rax, -192(%rbp)
	movq	-8296(%rbp), %rax
	movq	$0, -8176(%rbp)
	movq	%rax, -184(%rbp)
	movq	-8288(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-8280(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-8272(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-8264(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-8256(%rbp), %rax
	movq	%rax, -144(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm4
	movdqa	-176(%rbp), %xmm5
	movdqa	-160(%rbp), %xmm7
	leaq	56(%rax), %rdx
	movq	%rax, -8192(%rbp)
	movq	%rcx, 48(%rax)
	movq	-8384(%rbp), %rdi
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -8176(%rbp)
	movq	%rdx, -8184(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2212
	call	_ZdlPv@PLT
.L2212:
	movq	-8760(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3968(%rbp)
	je	.L2213
.L2497:
	leaq	-3976(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -8312(%rbp)
	movq	$0, -8304(%rbp)
	movq	$0, -8296(%rbp)
	movq	$0, -8288(%rbp)
	movq	$0, -8280(%rbp)
	movq	$0, -8272(%rbp)
	movq	$0, -8264(%rbp)
	movq	$0, -8256(%rbp)
	movq	$0, -8248(%rbp)
	movq	$0, -8240(%rbp)
	movq	$0, -8232(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8232(%rbp), %rax
	movq	-8544(%rbp), %rdi
	leaq	-8280(%rbp), %r9
	pushq	%rax
	leaq	-8240(%rbp), %rax
	leaq	-8296(%rbp), %rcx
	pushq	%rax
	leaq	-8248(%rbp), %rax
	leaq	-8288(%rbp), %r8
	pushq	%rax
	leaq	-8256(%rbp), %rax
	leaq	-8304(%rbp), %rdx
	pushq	%rax
	leaq	-8264(%rbp), %rax
	leaq	-8312(%rbp), %rsi
	pushq	%rax
	leaq	-8272(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_SL_
	addq	$48, %rsp
	movl	$259, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8256(%rbp), %rdx
	movq	-8232(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	movq	%rax, -8720(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$260, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$37, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8272(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShrENS1_11SloppyTNodeINS0_5WordTEEENS3_INS0_9IntegralTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal36Convert13ATPositiveSmi9ATuintptr_190EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8UintPtrTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-8248(%rbp), %rdx
	movq	-8264(%rbp), %rsi
	movq	%rbx, -120(%rbp)
	movq	-8280(%rbp), %xmm0
	movq	%rbx, -88(%rbp)
	movq	%rbx, %xmm6
	leaq	-192(%rbp), %rbx
	movq	-8296(%rbp), %xmm1
	movq	%rdx, -128(%rbp)
	leaq	-56(%rbp), %rdx
	movhps	-8272(%rbp), %xmm0
	movq	%rsi, -96(%rbp)
	movq	%rbx, %rsi
	movq	-8264(%rbp), %xmm3
	movaps	%xmm0, -160(%rbp)
	movhps	-8288(%rbp), %xmm1
	movq	-8232(%rbp), %xmm0
	movq	-8312(%rbp), %xmm2
	movaps	%xmm1, -176(%rbp)
	movq	%r12, %xmm1
	leaq	-8224(%rbp), %r12
	movhps	-8256(%rbp), %xmm3
	movhps	-8272(%rbp), %xmm0
	punpcklqdq	%xmm1, %xmm6
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movhps	-8304(%rbp), %xmm2
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm6, -8736(%rbp)
	movaps	%xmm6, -80(%rbp)
	movaps	%xmm0, -8224(%rbp)
	movq	$0, -8208(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8616(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2214
	call	_ZdlPv@PLT
.L2214:
	leaq	-3592(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -8184(%rbp)
	jne	.L2518
.L2215:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -3776(%rbp)
	je	.L2217
.L2498:
	leaq	-3784(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC10(%rip), %xmm0
	leaq	-192(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-176(%rbp), %rdx
	movq	$0, -8176(%rbp)
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8192(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8744(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8192(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2218
	call	_ZdlPv@PLT
.L2218:
	movq	(%rbx), %rax
	movl	$56, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movq	48(%rax), %rdx
	movdqu	(%rax), %xmm6
	movq	$0, -8176(%rbp)
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -144(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -8192(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm3
	movdqa	-176(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm5
	leaq	56(%rax), %rdx
	movq	%rax, -8192(%rbp)
	movq	%rcx, 48(%rax)
	movq	-8384(%rbp), %rdi
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rdx, -8176(%rbp)
	movq	%rdx, -8184(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2219
	call	_ZdlPv@PLT
.L2219:
	movq	-8760(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3584(%rbp)
	je	.L2220
.L2499:
	leaq	-3592(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-192(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC10(%rip), %xmm0
	movq	%rbx, %rsi
	movq	%r14, %rdi
	leaq	-175(%rbp), %rdx
	movb	$6, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8192(%rbp)
	movq	$0, -8176(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8616(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8192(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L2221
	call	_ZdlPv@PLT
.L2221:
	movq	(%r12), %rax
	pxor	%xmm0, %xmm0
	movq	(%rax), %rsi
	movq	8(%rax), %rcx
	movq	16(%rax), %rdx
	movq	32(%rax), %rdi
	movq	40(%rax), %r11
	movq	48(%rax), %r10
	movq	%rsi, -8720(%rbp)
	movq	80(%rax), %rsi
	movq	56(%rax), %r9
	movq	%rcx, -8736(%rbp)
	movq	64(%rax), %r8
	movq	88(%rax), %rcx
	movq	%rdx, -8800(%rbp)
	movq	24(%rax), %r12
	movq	96(%rax), %rdx
	movq	%rsi, -8848(%rbp)
	movq	104(%rax), %rsi
	movq	%rdi, -8832(%rbp)
	movq	72(%rax), %rdi
	movq	%rsi, -8864(%rbp)
	movq	112(%rax), %rsi
	movq	128(%rax), %rax
	movq	%r12, -168(%rbp)
	movq	%rsi, -8880(%rbp)
	movq	-8720(%rbp), %rsi
	movq	%rsi, -192(%rbp)
	movq	-8736(%rbp), %rsi
	movq	%rsi, -184(%rbp)
	movq	-8800(%rbp), %rsi
	movq	%rsi, -176(%rbp)
	movq	-8832(%rbp), %rsi
	movq	%rsi, -160(%rbp)
	movq	-8848(%rbp), %rsi
	movq	%rdi, -120(%rbp)
	movq	%r14, %rdi
	movq	%rsi, -112(%rbp)
	movq	-8864(%rbp), %rsi
	movq	%rdx, -96(%rbp)
	leaq	-64(%rbp), %rdx
	movq	%rsi, -88(%rbp)
	movq	-8880(%rbp), %rsi
	movq	%r11, -152(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%rbx, %rsi
	movq	%r10, -144(%rbp)
	movq	%r9, -136(%rbp)
	movq	%r8, -128(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%rax, -72(%rbp)
	movaps	%xmm0, -8192(%rbp)
	movq	$0, -8176(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8624(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2222
	call	_ZdlPv@PLT
.L2222:
	leaq	-3400(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3392(%rbp)
	je	.L2223
.L2500:
	leaq	-3400(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-192(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC11(%rip), %xmm0
	movq	%rbx, %rsi
	movq	%r14, %rdi
	leaq	-176(%rbp), %rdx
	movq	$0, -8176(%rbp)
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8192(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8624(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8192(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L2224
	call	_ZdlPv@PLT
.L2224:
	movq	(%r12), %rax
	movq	24(%rax), %rdx
	movq	40(%rax), %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rsi
	movq	56(%rax), %r10
	movq	72(%rax), %r11
	movq	%rdx, -8832(%rbp)
	movq	32(%rax), %rdx
	movq	%rdi, -8864(%rbp)
	movq	48(%rax), %rdi
	movq	80(%rax), %r12
	movq	%rcx, -8736(%rbp)
	movq	16(%rax), %rcx
	movq	120(%rax), %rax
	movq	%rsi, -8720(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -8848(%rbp)
	movl	$260, %edx
	movq	%rdi, -8880(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -8800(%rbp)
	movq	%r10, -8896(%rbp)
	movq	%r11, -8912(%rbp)
	movq	%rax, -8928(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$246, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-104(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-8720(%rbp), %xmm0
	movq	%r12, -112(%rbp)
	movq	$0, -8176(%rbp)
	movhps	-8736(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-8800(%rbp), %xmm0
	movhps	-8832(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-8848(%rbp), %xmm0
	movhps	-8864(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-8880(%rbp), %xmm0
	movhps	-8896(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-8928(%rbp), %xmm0
	movhps	-8912(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8192(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8408(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2225
	call	_ZdlPv@PLT
.L2225:
	leaq	-1480(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3200(%rbp)
	je	.L2226
.L2501:
	leaq	-3208(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -8304(%rbp)
	movq	$0, -8296(%rbp)
	movq	$0, -8288(%rbp)
	movq	$0, -8280(%rbp)
	movq	$0, -8272(%rbp)
	movq	$0, -8264(%rbp)
	movq	$0, -8256(%rbp)
	movq	$0, -8248(%rbp)
	movq	$0, -8240(%rbp)
	movq	$0, -8232(%rbp)
	movq	$0, -8224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8224(%rbp), %rax
	movq	-8576(%rbp), %rdi
	leaq	-8288(%rbp), %rcx
	pushq	%rax
	leaq	-8232(%rbp), %rax
	leaq	-8272(%rbp), %r9
	pushq	%rax
	leaq	-8240(%rbp), %rax
	leaq	-8280(%rbp), %r8
	pushq	%rax
	leaq	-8248(%rbp), %rax
	leaq	-8296(%rbp), %rdx
	pushq	%rax
	leaq	-8256(%rbp), %rax
	leaq	-8304(%rbp), %rsi
	pushq	%rax
	leaq	-8264(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_SL_
	addq	$48, %rsp
	movl	$266, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$26, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-8240(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal36Convert9ATuintptr13ATPositiveSmi_187EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$27, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8264(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rbx, -8720(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler7WordShlENS1_11SloppyTNodeINS0_5WordTEEENS3_INS0_9IntegralTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$30, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8264(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordShrENS1_11SloppyTNodeINS0_5WordTEEENS3_INS0_9IntegralTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8720(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	leaq	-192(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -8736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-56(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-8240(%rbp), %xmm0
	movq	-8720(%rbp), %rax
	movq	%r12, -64(%rbp)
	movq	-8224(%rbp), %xmm1
	movq	-8256(%rbp), %xmm7
	movq	-8256(%rbp), %xmm2
	movdqa	%xmm0, %xmm3
	movq	-8272(%rbp), %xmm4
	movq	%xmm0, -80(%rbp)
	movhps	-8264(%rbp), %xmm1
	punpcklqdq	%xmm0, %xmm7
	movhps	-8232(%rbp), %xmm3
	movq	%rax, -72(%rbp)
	movq	-8288(%rbp), %xmm5
	movhps	-8248(%rbp), %xmm2
	movaps	%xmm1, -112(%rbp)
	movq	-8304(%rbp), %xmm6
	movhps	-8264(%rbp), %xmm4
	movaps	%xmm1, -8832(%rbp)
	pxor	%xmm1, %xmm1
	movhps	-8280(%rbp), %xmm5
	movhps	-8296(%rbp), %xmm6
	movq	%xmm0, -8816(%rbp)
	movaps	%xmm7, -8800(%rbp)
	movaps	%xmm3, -8848(%rbp)
	movaps	%xmm2, -8864(%rbp)
	movaps	%xmm4, -8880(%rbp)
	movaps	%xmm5, -8896(%rbp)
	movaps	%xmm6, -8912(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm1, -8192(%rbp)
	movq	%rdx, -8928(%rbp)
	movq	$0, -8176(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8640(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	movq	-8928(%rbp), %rdx
	movq	-8816(%rbp), %xmm0
	testq	%rdi, %rdi
	je	.L2227
	movq	%rdx, -8816(%rbp)
	movq	%xmm0, -8928(%rbp)
	call	_ZdlPv@PLT
	movq	-8816(%rbp), %rdx
	movq	-8928(%rbp), %xmm0
.L2227:
	movdqa	-8912(%rbp), %xmm7
	movdqa	-8896(%rbp), %xmm6
	movhps	-8720(%rbp), %xmm0
	movq	%rbx, %rsi
	movdqa	-8880(%rbp), %xmm1
	movaps	%xmm0, -80(%rbp)
	movq	%r14, %rdi
	movdqa	-8864(%rbp), %xmm2
	movdqa	-8848(%rbp), %xmm3
	movdqa	-8832(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movaps	%xmm7, -192(%rbp)
	movdqa	-8800(%rbp), %xmm7
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm0, -8192(%rbp)
	movq	%r12, -64(%rbp)
	movq	$0, -8176(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8656(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2228
	call	_ZdlPv@PLT
.L2228:
	movq	-8736(%rbp), %rsi
	leaq	-2824(%rbp), %rcx
	leaq	-3016(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L2226
	.p2align 4,,10
	.p2align 3
.L2486:
	movq	-8720(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -8304(%rbp)
	leaq	-8224(%rbp), %r12
	movq	$0, -8296(%rbp)
	movq	$0, -8288(%rbp)
	movq	$0, -8280(%rbp)
	movq	$0, -8272(%rbp)
	movq	$0, -8264(%rbp)
	movq	$0, -8256(%rbp)
	movq	$0, -8248(%rbp)
	movq	$0, -8240(%rbp)
	movq	$0, -8232(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-8232(%rbp), %rax
	movq	-8648(%rbp), %rdi
	pushq	%rax
	leaq	-8240(%rbp), %rax
	leaq	-8272(%rbp), %r9
	pushq	%rax
	leaq	-8248(%rbp), %rax
	leaq	-8288(%rbp), %rcx
	pushq	%rax
	leaq	-8256(%rbp), %rax
	leaq	-8280(%rbp), %r8
	pushq	%rax
	leaq	-8264(%rbp), %rax
	leaq	-8296(%rbp), %rdx
	pushq	%rax
	leaq	-8304(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_
	addq	$48, %rsp
	movl	$234, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8272(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	-8304(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler10ToSmiIndexENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEEPNS2_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, -104(%rbp)
	leaq	-96(%rbp), %rdx
	movq	%r12, %rdi
	movq	-8240(%rbp), %xmm0
	movq	-8272(%rbp), %rax
	leaq	-192(%rbp), %rbx
	movq	$0, -8208(%rbp)
	movq	-8256(%rbp), %xmm1
	movq	%rbx, %rsi
	movq	-8272(%rbp), %xmm4
	movq	-8288(%rbp), %xmm2
	movhps	-8232(%rbp), %xmm0
	movq	%rax, -112(%rbp)
	movq	-8304(%rbp), %xmm3
	movhps	-8248(%rbp), %xmm1
	movhps	-8264(%rbp), %xmm4
	movaps	%xmm0, -128(%rbp)
	movhps	-8280(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -160(%rbp)
	movhps	-8296(%rbp), %xmm3
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -8224(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8688(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2184
	call	_ZdlPv@PLT
.L2184:
	movq	-8736(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-5952(%rbp), %rax
	cmpq	$0, -8184(%rbp)
	movq	%rax, -8480(%rbp)
	jne	.L2519
.L2185:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L2183
	.p2align 4,,10
	.p2align 3
.L2485:
	leaq	-6280(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -8280(%rbp)
	movq	$0, -8272(%rbp)
	movq	$0, -8264(%rbp)
	movq	$0, -8256(%rbp)
	movq	$0, -8248(%rbp)
	movq	$0, -8240(%rbp)
	movq	$0, -8232(%rbp)
	movq	$0, -8224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-8224(%rbp), %rax
	movq	-8392(%rbp), %rdi
	pushq	%rax
	leaq	-8232(%rbp), %rax
	leaq	-8264(%rbp), %rcx
	pushq	%rax
	leaq	-8240(%rbp), %rax
	leaq	-8248(%rbp), %r9
	pushq	%rax
	leaq	-8256(%rbp), %r8
	leaq	-8272(%rbp), %rdx
	leaq	-8280(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EEPNSA_IS8_EESK_
	addq	$32, %rsp
	movl	$229, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal51FromConstexpr13ATPositiveSmi17ATconstexpr_int31_153EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$230, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	movq	%rax, -8448(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$232, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8248(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, -128(%rbp)
	leaq	-112(%rbp), %rdx
	movq	%r14, %rdi
	movq	-8232(%rbp), %xmm7
	leaq	-192(%rbp), %rbx
	movq	-8248(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movq	-8264(%rbp), %xmm2
	movq	%rbx, %rsi
	movq	-8280(%rbp), %xmm4
	movq	%rdx, -8864(%rbp)
	movhps	-8224(%rbp), %xmm7
	movhps	-8240(%rbp), %xmm3
	movaps	%xmm0, -8192(%rbp)
	movhps	-8256(%rbp), %xmm2
	movhps	-8272(%rbp), %xmm4
	movaps	%xmm7, -8800(%rbp)
	movaps	%xmm3, -8688(%rbp)
	movaps	%xmm2, -8480(%rbp)
	movaps	%xmm4, -8848(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -8176(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8648(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	movq	-8864(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L2181
	call	_ZdlPv@PLT
	movq	-8864(%rbp), %rdx
.L2181:
	movdqa	-8848(%rbp), %xmm6
	movdqa	-8480(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movdqa	-8688(%rbp), %xmm5
	movdqa	-8800(%rbp), %xmm7
	movq	%r14, %rdi
	movaps	%xmm0, -8192(%rbp)
	movq	-8448(%rbp), %rax
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	movq	%rax, -128(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -8176(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8400(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2182
	call	_ZdlPv@PLT
.L2182:
	movq	-8720(%rbp), %rdx
	leaq	-5512(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L2180
	.p2align 4,,10
	.p2align 3
.L2502:
	leaq	-3016(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -8352(%rbp)
	movq	$0, -8344(%rbp)
	movq	$0, -8336(%rbp)
	movq	$0, -8328(%rbp)
	movq	$0, -8320(%rbp)
	movq	$0, -8312(%rbp)
	movq	$0, -8304(%rbp)
	movq	$0, -8296(%rbp)
	movq	$0, -8288(%rbp)
	movq	$0, -8280(%rbp)
	movq	$0, -8272(%rbp)
	movq	$0, -8264(%rbp)
	movq	$0, -8256(%rbp)
	movq	$0, -8248(%rbp)
	movq	$0, -8240(%rbp)
	movq	$0, -8232(%rbp)
	movq	$0, -8224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8224(%rbp), %rax
	movq	-8640(%rbp), %rdi
	leaq	-8336(%rbp), %rcx
	pushq	%rax
	leaq	-8232(%rbp), %rax
	leaq	-8344(%rbp), %rdx
	pushq	%rax
	leaq	-8240(%rbp), %rax
	leaq	-8352(%rbp), %rsi
	pushq	%rax
	leaq	-8248(%rbp), %rax
	leaq	-8320(%rbp), %r9
	pushq	%rax
	leaq	-8256(%rbp), %rax
	leaq	-8328(%rbp), %r8
	pushq	%rax
	leaq	-8264(%rbp), %rax
	pushq	%rax
	leaq	-8272(%rbp), %rax
	pushq	%rax
	leaq	-8280(%rbp), %rax
	pushq	%rax
	leaq	-8288(%rbp), %rax
	pushq	%rax
	leaq	-8296(%rbp), %rax
	pushq	%rax
	leaq	-8304(%rbp), %rax
	pushq	%rax
	leaq	-8312(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_S7_S7_S8_S9_S9_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_SL_SL_SN_SP_SP_SL_SL_
	movq	-8352(%rbp), %rax
	pxor	%xmm0, %xmm0
	addq	$96, %rsp
	movl	$56, %edi
	movaps	%xmm0, -8192(%rbp)
	movq	%rax, -192(%rbp)
	movq	-8344(%rbp), %rax
	movq	$0, -8176(%rbp)
	movq	%rax, -184(%rbp)
	movq	-8336(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-8328(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-8320(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-8312(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-8304(%rbp), %rax
	movq	%rax, -144(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm5
	movdqa	-176(%rbp), %xmm6
	movdqa	-160(%rbp), %xmm1
	leaq	56(%rax), %rdx
	movq	%rax, -8192(%rbp)
	movq	%rcx, 48(%rax)
	movq	-8720(%rbp), %rdi
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm1, 32(%rax)
	movq	%rdx, -8176(%rbp)
	movq	%rdx, -8184(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2230
	call	_ZdlPv@PLT
.L2230:
	movq	-8784(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2816(%rbp)
	je	.L2231
.L2503:
	leaq	-2824(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -8352(%rbp)
	movq	$0, -8344(%rbp)
	movq	$0, -8336(%rbp)
	movq	$0, -8328(%rbp)
	movq	$0, -8320(%rbp)
	movq	$0, -8312(%rbp)
	movq	$0, -8304(%rbp)
	movq	$0, -8296(%rbp)
	movq	$0, -8288(%rbp)
	movq	$0, -8280(%rbp)
	movq	$0, -8272(%rbp)
	movq	$0, -8264(%rbp)
	movq	$0, -8256(%rbp)
	movq	$0, -8248(%rbp)
	movq	$0, -8240(%rbp)
	movq	$0, -8232(%rbp)
	movq	$0, -8224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8224(%rbp), %rax
	movq	-8656(%rbp), %rdi
	leaq	-8336(%rbp), %rcx
	pushq	%rax
	leaq	-8232(%rbp), %rax
	leaq	-8320(%rbp), %r9
	pushq	%rax
	leaq	-8240(%rbp), %rax
	leaq	-8328(%rbp), %r8
	pushq	%rax
	leaq	-8248(%rbp), %rax
	leaq	-8344(%rbp), %rdx
	pushq	%rax
	leaq	-8256(%rbp), %rax
	leaq	-8352(%rbp), %rsi
	pushq	%rax
	leaq	-8264(%rbp), %rax
	pushq	%rax
	leaq	-8272(%rbp), %rax
	pushq	%rax
	leaq	-8280(%rbp), %rax
	pushq	%rax
	leaq	-8288(%rbp), %rax
	pushq	%rax
	leaq	-8296(%rbp), %rax
	pushq	%rax
	leaq	-8304(%rbp), %rax
	pushq	%rax
	leaq	-8312(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_S7_S7_S8_S9_S9_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_SL_SL_SN_SP_SP_SL_SL_
	addq	$96, %rsp
	movl	$31, %edx
	movq	%r15, %rdi
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-192(%rbp), %rsi
	leaq	-64(%rbp), %rdx
	movq	%r14, %rdi
	movq	-8240(%rbp), %xmm0
	movq	-8256(%rbp), %xmm1
	movq	$0, -8176(%rbp)
	movq	-8272(%rbp), %xmm2
	movq	-8288(%rbp), %xmm3
	movq	-8304(%rbp), %xmm4
	movhps	-8224(%rbp), %xmm0
	movq	-8320(%rbp), %xmm5
	movhps	-8248(%rbp), %xmm1
	movq	-8336(%rbp), %xmm6
	movhps	-8264(%rbp), %xmm2
	movaps	%xmm0, -80(%rbp)
	movq	-8352(%rbp), %xmm7
	movhps	-8280(%rbp), %xmm3
	movhps	-8296(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -112(%rbp)
	movhps	-8312(%rbp), %xmm5
	movhps	-8328(%rbp), %xmm6
	movaps	%xmm3, -128(%rbp)
	movhps	-8344(%rbp), %xmm7
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -8192(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8664(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2232
	call	_ZdlPv@PLT
.L2232:
	leaq	-2632(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2231
	.p2align 4,,10
	.p2align 3
.L2504:
	leaq	-2632(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-192(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC6(%rip), %xmm0
	movq	%rbx, %rsi
	movq	%r14, %rdi
	leaq	-176(%rbp), %rdx
	movq	$0, -8176(%rbp)
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8192(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8664(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8192(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L2234
	call	_ZdlPv@PLT
.L2234:
	movq	(%r12), %rax
	movq	24(%rax), %rdx
	movq	40(%rax), %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rsi
	movq	56(%rax), %r10
	movq	64(%rax), %r11
	movq	%rdx, -8880(%rbp)
	movq	32(%rax), %rdx
	movq	%rdi, -8912(%rbp)
	movq	48(%rax), %rdi
	movq	80(%rax), %r12
	movq	%rcx, -8848(%rbp)
	movq	16(%rax), %rcx
	movq	120(%rax), %rax
	movq	%rsi, -8832(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -8896(%rbp)
	movl	$266, %edx
	movq	%rdi, -8928(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -8864(%rbp)
	movq	%r10, -8816(%rbp)
	movq	%r11, -8936(%rbp)
	movq	%rax, -8736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$271, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8736(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -8800(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %xmm7
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-8928(%rbp), %xmm3
	leaq	-96(%rbp), %r12
	movq	-8896(%rbp), %xmm2
	movhps	-8800(%rbp), %xmm7
	movq	-8864(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movq	%r12, %rdx
	movaps	%xmm7, -112(%rbp)
	movq	-8832(%rbp), %xmm5
	movhps	-8816(%rbp), %xmm3
	movq	-8936(%rbp), %xmm1
	movhps	-8912(%rbp), %xmm2
	movhps	-8880(%rbp), %xmm4
	movaps	%xmm7, -8960(%rbp)
	movhps	-8736(%rbp), %xmm1
	movhps	-8848(%rbp), %xmm5
	movaps	%xmm3, -8928(%rbp)
	movaps	%xmm1, -8736(%rbp)
	movaps	%xmm2, -8896(%rbp)
	movaps	%xmm4, -8864(%rbp)
	movaps	%xmm5, -8832(%rbp)
	movaps	%xmm5, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -8192(%rbp)
	movq	$0, -8176(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8672(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2235
	call	_ZdlPv@PLT
.L2235:
	movdqa	-8832(%rbp), %xmm2
	movdqa	-8864(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movq	%r12, %rdx
	movdqa	-8896(%rbp), %xmm4
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movdqa	-8928(%rbp), %xmm7
	movdqa	-8736(%rbp), %xmm5
	movdqa	-8960(%rbp), %xmm6
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm0, -8192(%rbp)
	movq	$0, -8176(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2304(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -8736(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2236
	call	_ZdlPv@PLT
.L2236:
	movq	-8808(%rbp), %rcx
	movq	-8800(%rbp), %rsi
	leaq	-2440(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2432(%rbp)
	je	.L2237
.L2505:
	leaq	-2440(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$361419402149037831, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$12, %edi
	movq	$0, -8176(%rbp)
	movaps	%xmm0, -8192(%rbp)
	call	_Znwm@PLT
	movq	-8672(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	12(%rax), %rdx
	movl	$67437830, 8(%rax)
	movq	%rax, -8192(%rbp)
	movq	%rdx, -8176(%rbp)
	movq	%rdx, -8184(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8192(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2238
	call	_ZdlPv@PLT
.L2238:
	movq	(%rbx), %rax
	movq	%r13, %rdi
	movq	40(%rax), %rdx
	movq	8(%rax), %rsi
	movq	(%rax), %rbx
	movq	24(%rax), %rcx
	movq	%rdx, -8896(%rbp)
	movq	56(%rax), %rdx
	movq	%rsi, -8832(%rbp)
	movq	16(%rax), %rsi
	movq	%rdx, -8912(%rbp)
	movq	72(%rax), %rdx
	movq	48(%rax), %r12
	movq	%rbx, -8800(%rbp)
	movq	%rcx, -8864(%rbp)
	movq	64(%rax), %rbx
	movq	32(%rax), %rcx
	movq	%rdx, -8928(%rbp)
	movq	80(%rax), %rdx
	movq	88(%rax), %rax
	movq	%rsi, -8848(%rbp)
	movl	$1, %esi
	movq	%rcx, -8880(%rbp)
	movq	%rdx, -8816(%rbp)
	movq	%rax, -8936(%rbp)
	call	_ZN2v88internal42FromConstexpr6ATbool16ATconstexpr_bool_165EPNS0_8compiler18CodeAssemblerStateEb@PLT
	leaq	-192(%rbp), %rsi
	leaq	-88(%rbp), %rdx
	movq	%r14, %rdi
	movq	-8800(%rbp), %xmm0
	movq	%rax, -96(%rbp)
	movq	$0, -8176(%rbp)
	movhps	-8832(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-8848(%rbp), %xmm0
	movhps	-8864(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-8880(%rbp), %xmm0
	movhps	-8896(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%r12, %xmm0
	movhps	-8912(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	movhps	-8928(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-8816(%rbp), %xmm0
	movhps	-8936(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8192(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8416(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2239
	call	_ZdlPv@PLT
.L2239:
	leaq	-2056(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2240(%rbp)
	je	.L2240
.L2506:
	movq	-8808(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$361419402149037831, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$12, %edi
	movq	$0, -8176(%rbp)
	movaps	%xmm0, -8192(%rbp)
	call	_Znwm@PLT
	movq	-8736(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	12(%rax), %rdx
	movl	$67437830, 8(%rax)
	movq	%rax, -8192(%rbp)
	movq	%rdx, -8176(%rbp)
	movq	%rdx, -8184(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8192(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2241
	call	_ZdlPv@PLT
.L2241:
	movq	(%rbx), %rax
	movq	(%rax), %rbx
	movq	8(%rax), %rsi
	movq	40(%rax), %rdx
	movq	24(%rax), %rcx
	movq	64(%rax), %rdi
	movq	80(%rax), %r12
	movq	%rbx, -8808(%rbp)
	movq	56(%rax), %rbx
	movq	%rsi, -8832(%rbp)
	movq	%rdx, -8896(%rbp)
	movq	16(%rax), %rsi
	movq	48(%rax), %rdx
	movq	%rcx, -8864(%rbp)
	movq	%rbx, -8800(%rbp)
	movq	32(%rax), %rcx
	movq	72(%rax), %rbx
	movq	88(%rax), %rax
	movq	%rsi, -8848(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -8912(%rbp)
	movl	$272, %edx
	movq	%rdi, -8928(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -8880(%rbp)
	movq	%rax, -8936(%rbp)
	movq	%rbx, -8816(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8800(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler18UintPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$271, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-88(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rbx, -96(%rbp)
	movq	-8808(%rbp), %xmm0
	leaq	-192(%rbp), %rsi
	movq	$0, -8176(%rbp)
	movhps	-8832(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-8848(%rbp), %xmm0
	movhps	-8864(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-8880(%rbp), %xmm0
	movhps	-8896(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-8912(%rbp), %xmm0
	movhps	-8800(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-8928(%rbp), %xmm0
	movhps	-8816(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%r12, %xmm0
	movhps	-8936(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8192(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8416(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2242
	call	_ZdlPv@PLT
.L2242:
	leaq	-2056(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2048(%rbp)
	je	.L2243
.L2507:
	leaq	-2056(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-192(%rbp), %rsi
	movabsq	$361419402149037831, %rax
	leaq	-179(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, -192(%rbp)
	movaps	%xmm0, -8192(%rbp)
	movl	$67437830, -184(%rbp)
	movb	$4, -180(%rbp)
	movq	$0, -8176(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8416(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8192(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2244
	call	_ZdlPv@PLT
.L2244:
	movq	(%rbx), %rax
	movl	$88, %edi
	movq	(%rax), %rbx
	movdqu	16(%rax), %xmm3
	movdqu	32(%rax), %xmm2
	movdqu	48(%rax), %xmm1
	movdqu	64(%rax), %xmm0
	movdqu	(%rax), %xmm4
	movq	%rbx, -8800(%rbp)
	movq	8(%rax), %rbx
	movq	96(%rax), %r12
	movq	%rbx, -8808(%rbp)
	movq	16(%rax), %rbx
	movq	%rbx, -8832(%rbp)
	movq	24(%rax), %rbx
	movq	%rbx, -8848(%rbp)
	movq	32(%rax), %rbx
	movq	%rbx, -8864(%rbp)
	movq	40(%rax), %rbx
	movq	%rbx, -8880(%rbp)
	movq	48(%rax), %rbx
	movq	%rbx, -8896(%rbp)
	movq	56(%rax), %rbx
	movq	%rbx, -8912(%rbp)
	movq	64(%rax), %rbx
	movq	%rbx, -8928(%rbp)
	movq	72(%rax), %rbx
	movq	%rbx, -8816(%rbp)
	movq	80(%rax), %rbx
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8192(%rbp)
	movq	%rbx, -112(%rbp)
	movq	$0, -8176(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm1
	movdqa	-128(%rbp), %xmm5
	movq	-8704(%rbp), %rdi
	leaq	88(%rax), %rdx
	movq	%rax, -8192(%rbp)
	movdqa	-176(%rbp), %xmm2
	movdqa	-160(%rbp), %xmm3
	movq	%rcx, 80(%rax)
	movdqa	-144(%rbp), %xmm7
	movups	%xmm1, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rdx, -8176(%rbp)
	movq	%rdx, -8184(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2245
	call	_ZdlPv@PLT
.L2245:
	movq	-8800(%rbp), %xmm0
	movl	$88, %edi
	movq	%rbx, -112(%rbp)
	movq	$0, -8176(%rbp)
	movhps	-8808(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-8832(%rbp), %xmm0
	movhps	-8848(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-8864(%rbp), %xmm0
	movhps	-8880(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-8896(%rbp), %xmm0
	movhps	-8912(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-8928(%rbp), %xmm0
	movhps	-8816(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8192(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm4
	movq	-8632(%rbp), %rdi
	leaq	88(%rax), %rdx
	movq	%rax, -8192(%rbp)
	movdqa	-176(%rbp), %xmm1
	movdqa	-160(%rbp), %xmm2
	movq	%rcx, 80(%rax)
	movdqa	-144(%rbp), %xmm3
	movups	%xmm6, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm4, 64(%rax)
	movq	%rdx, -8176(%rbp)
	movq	%rdx, -8184(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2246
	call	_ZdlPv@PLT
.L2246:
	leaq	-1672(%rbp), %rcx
	leaq	-1864(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1856(%rbp)
	je	.L2247
.L2508:
	leaq	-1864(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -8304(%rbp)
	movq	$0, -8296(%rbp)
	movq	$0, -8288(%rbp)
	movq	$0, -8280(%rbp)
	movq	$0, -8272(%rbp)
	movq	$0, -8264(%rbp)
	movq	$0, -8256(%rbp)
	movq	$0, -8248(%rbp)
	movq	$0, -8240(%rbp)
	movq	$0, -8232(%rbp)
	movq	$0, -8224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8224(%rbp), %rax
	movq	-8704(%rbp), %rdi
	leaq	-8288(%rbp), %rcx
	pushq	%rax
	leaq	-8232(%rbp), %rax
	leaq	-8272(%rbp), %r9
	pushq	%rax
	leaq	-8240(%rbp), %rax
	leaq	-8280(%rbp), %r8
	pushq	%rax
	leaq	-8248(%rbp), %rax
	leaq	-8296(%rbp), %rdx
	pushq	%rax
	leaq	-8256(%rbp), %rax
	leaq	-8304(%rbp), %rsi
	pushq	%rax
	leaq	-8264(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_SL_
	addq	$48, %rsp
	movl	$273, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-8304(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movaps	%xmm0, -8192(%rbp)
	movq	%rax, -192(%rbp)
	movq	-8296(%rbp), %rax
	movq	$0, -8176(%rbp)
	movq	%rax, -184(%rbp)
	movq	-8288(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-8280(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-8272(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-8264(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-8256(%rbp), %rax
	movq	%rax, -144(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm7
	movdqa	-176(%rbp), %xmm5
	movdqa	-160(%rbp), %xmm6
	leaq	56(%rax), %rdx
	movq	%rax, -8192(%rbp)
	movq	%rcx, 48(%rax)
	movq	-8448(%rbp), %rdi
	movups	%xmm7, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -8176(%rbp)
	movq	%rdx, -8184(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2248
	call	_ZdlPv@PLT
.L2248:
	movq	-8776(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1664(%rbp)
	je	.L2249
.L2509:
	leaq	-1672(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -8304(%rbp)
	movq	$0, -8296(%rbp)
	movq	$0, -8288(%rbp)
	movq	$0, -8280(%rbp)
	movq	$0, -8272(%rbp)
	movq	$0, -8264(%rbp)
	movq	$0, -8256(%rbp)
	movq	$0, -8248(%rbp)
	movq	$0, -8240(%rbp)
	movq	$0, -8232(%rbp)
	movq	$0, -8224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8224(%rbp), %rax
	movq	-8632(%rbp), %rdi
	leaq	-8288(%rbp), %rcx
	pushq	%rax
	leaq	-8232(%rbp), %rax
	leaq	-8272(%rbp), %r9
	pushq	%rax
	leaq	-8240(%rbp), %rax
	leaq	-8280(%rbp), %r8
	pushq	%rax
	leaq	-8248(%rbp), %rax
	leaq	-8296(%rbp), %rdx
	pushq	%rax
	leaq	-8256(%rbp), %rax
	leaq	-8304(%rbp), %rsi
	pushq	%rax
	leaq	-8264(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_SL_
	addq	$48, %rsp
	movl	$246, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm0, %xmm0
	leaq	-104(%rbp), %rdx
	movq	%r14, %rdi
	movq	-8304(%rbp), %rax
	leaq	-192(%rbp), %rsi
	movaps	%xmm0, -8192(%rbp)
	movq	$0, -8176(%rbp)
	movq	%rax, -192(%rbp)
	movq	-8296(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-8288(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-8280(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-8272(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-8264(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-8256(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-8248(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-8240(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-8232(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-8224(%rbp), %rax
	movq	%rax, -112(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8408(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2250
	call	_ZdlPv@PLT
.L2250:
	leaq	-1480(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1472(%rbp)
	je	.L2251
.L2510:
	leaq	-1480(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -8304(%rbp)
	movq	$0, -8296(%rbp)
	movq	$0, -8288(%rbp)
	movq	$0, -8280(%rbp)
	movq	$0, -8272(%rbp)
	movq	$0, -8264(%rbp)
	movq	$0, -8256(%rbp)
	movq	$0, -8248(%rbp)
	movq	$0, -8240(%rbp)
	movq	$0, -8232(%rbp)
	movq	$0, -8224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8224(%rbp), %rax
	movq	-8408(%rbp), %rdi
	leaq	-8288(%rbp), %rcx
	pushq	%rax
	leaq	-8232(%rbp), %rax
	leaq	-8272(%rbp), %r9
	pushq	%rax
	leaq	-8240(%rbp), %rax
	leaq	-8280(%rbp), %r8
	pushq	%rax
	leaq	-8248(%rbp), %rax
	leaq	-8296(%rbp), %rdx
	pushq	%rax
	leaq	-8256(%rbp), %rax
	leaq	-8304(%rbp), %rsi
	pushq	%rax
	leaq	-8264(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TES7_NS0_3SmiES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_PNSB_IS7_EEPNSB_IS8_EESL_PNSB_IS9_EESL_SL_
	addq	$48, %rsp
	movl	$278, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$279, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-8240(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal36Convert9ATuintptr13ATPositiveSmi_187EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$277, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pushq	%rbx
	movq	-8248(%rbp), %r9
	xorl	%edx, %edx
	pushq	-8232(%rbp)
	movq	-8288(%rbp), %r8
	movq	%r13, %rdi
	movq	-8296(%rbp), %rcx
	movq	-8304(%rbp), %rsi
	call	_ZN2v88internal22AllocateTypedArray_341EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEbNS4_INS0_3MapEEENS4_INS0_13JSArrayBufferEEENS4_INS0_8UintPtrTEEESC_SC_
	leaq	-192(%rbp), %rsi
	leaq	-128(%rbp), %rdx
	movq	%r14, %rdi
	movq	-8256(%rbp), %xmm0
	movq	%rax, %xmm1
	movq	-8288(%rbp), %xmm2
	movq	$0, -8176(%rbp)
	movq	-8304(%rbp), %xmm3
	punpcklqdq	%xmm1, %xmm0
	movhps	-8280(%rbp), %xmm2
	movq	-8272(%rbp), %xmm1
	movhps	-8296(%rbp), %xmm3
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-8264(%rbp), %xmm1
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -8192(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8696(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	popq	%r9
	popq	%r10
	testq	%rdi, %rdi
	je	.L2252
	call	_ZdlPv@PLT
.L2252:
	leaq	-520(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1280(%rbp)
	je	.L2253
.L2511:
	movq	-8768(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-192(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r14, %rdi
	leaq	-184(%rbp), %rdx
	movabsq	$505534590224893703, %rax
	movaps	%xmm0, -8192(%rbp)
	movq	%rax, -192(%rbp)
	movq	$0, -8176(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8432(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8192(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L2254
	call	_ZdlPv@PLT
.L2254:
	movq	(%r12), %rax
	movl	$282, %edx
	movq	%r15, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %r12
	movq	56(%rax), %rax
	movq	%rsi, -8768(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rax, -8800(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movl	$166, %esi
	movq	-8768(%rbp), %xmm0
	movl	$2, %r8d
	movq	%r14, %rdi
	movhps	-8800(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -1088(%rbp)
	je	.L2255
.L2512:
	movq	-8784(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -8272(%rbp)
	movq	$0, -8264(%rbp)
	movq	$0, -8256(%rbp)
	movq	$0, -8248(%rbp)
	movq	$0, -8240(%rbp)
	movq	$0, -8232(%rbp)
	movq	$0, -8224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8224(%rbp), %rax
	movq	-8720(%rbp), %rdi
	leaq	-8256(%rbp), %rcx
	pushq	%rax
	leaq	-8232(%rbp), %rax
	leaq	-8240(%rbp), %r9
	pushq	%rax
	leaq	-8248(%rbp), %r8
	leaq	-8264(%rbp), %rdx
	leaq	-8272(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EEPNSA_IS8_EE
	movl	$285, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	-8272(%rbp), %rsi
	movl	$186, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ThrowRangeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -896(%rbp)
	popq	%rdi
	popq	%r8
	je	.L2256
.L2513:
	movq	-8776(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -8272(%rbp)
	movq	$0, -8264(%rbp)
	movq	$0, -8256(%rbp)
	movq	$0, -8248(%rbp)
	movq	$0, -8240(%rbp)
	movq	$0, -8232(%rbp)
	movq	$0, -8224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8224(%rbp), %rax
	movq	-8448(%rbp), %rdi
	leaq	-8256(%rbp), %rcx
	pushq	%rax
	leaq	-8232(%rbp), %rax
	leaq	-8240(%rbp), %r9
	pushq	%rax
	leaq	-8248(%rbp), %r8
	leaq	-8264(%rbp), %rdx
	leaq	-8272(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EEPNSA_IS8_EE
	movl	$288, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$206, %edx
	movq	-8240(%rbp), %rcx
	movq	-8272(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ThrowRangeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -704(%rbp)
	popq	%rcx
	popq	%rsi
	je	.L2257
.L2514:
	movq	-8760(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -8272(%rbp)
	movq	$0, -8264(%rbp)
	movq	$0, -8256(%rbp)
	movq	$0, -8248(%rbp)
	movq	$0, -8240(%rbp)
	movq	$0, -8232(%rbp)
	movq	$0, -8224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8224(%rbp), %rax
	movq	-8384(%rbp), %rdi
	leaq	-8256(%rbp), %rcx
	pushq	%rax
	leaq	-8232(%rbp), %rax
	leaq	-8240(%rbp), %r9
	pushq	%rax
	leaq	-8248(%rbp), %r8
	leaq	-8264(%rbp), %rdx
	leaq	-8272(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_3MapENS0_13JSArrayBufferENS0_6ObjectES6_NS0_8UintPtrTENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EEPNSA_IS8_EE
	movl	$291, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$195, %edx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	-8248(%rbp), %rcx
	movq	-8272(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ThrowRangeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	jmp	.L2257
	.p2align 4,,10
	.p2align 3
.L2515:
	leaq	-520(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$505534590224893703, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -8176(%rbp)
	movaps	%xmm0, -8192(%rbp)
	call	_Znwm@PLT
	movq	-8696(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -8192(%rbp)
	movq	%rdx, -8176(%rbp)
	movq	%rdx, -8184(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8192(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2259
	call	_ZdlPv@PLT
.L2259:
	movq	(%rbx), %rax
	movl	$212, %edx
	movq	%r15, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rbx
	movq	32(%rax), %r12
	movq	16(%rax), %r13
	movq	%rsi, -8768(%rbp)
	movq	24(%rax), %rsi
	movq	%rbx, -8760(%rbp)
	movq	48(%rax), %rbx
	movq	%rsi, -8776(%rbp)
	movq	40(%rax), %rsi
	movq	56(%rax), %rax
	movq	%rsi, -8800(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rax, -8784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-192(%rbp), %rsi
	leaq	-128(%rbp), %rdx
	movq	%r14, %rdi
	movq	-8760(%rbp), %xmm0
	movq	$0, -8176(%rbp)
	movhps	-8768(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	%r13, %xmm0
	movhps	-8776(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	%r12, %xmm0
	leaq	-384(%rbp), %r12
	movhps	-8800(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%rbx, %xmm0
	movhps	-8784(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8192(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2260
	call	_ZdlPv@PLT
.L2260:
	movq	-8752(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2258
	.p2align 4,,10
	.p2align 3
.L2517:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-120(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-8240(%rbp), %xmm0
	movq	-8432(%rbp), %rax
	movq	$0, -8208(%rbp)
	movq	-8256(%rbp), %xmm1
	movq	-8272(%rbp), %xmm2
	movq	-8288(%rbp), %xmm3
	movhps	-8232(%rbp), %xmm0
	movq	%rax, -128(%rbp)
	movhps	-8248(%rbp), %xmm1
	movhps	-8264(%rbp), %xmm2
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-8280(%rbp), %xmm3
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -8224(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8464(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2160
	call	_ZdlPv@PLT
.L2160:
	movq	-8608(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2159
	.p2align 4,,10
	.p2align 3
.L2519:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-8304(%rbp), %rdx
	movq	-8272(%rbp), %rax
	movaps	%xmm0, -8224(%rbp)
	movq	$0, -8208(%rbp)
	movq	%rdx, -192(%rbp)
	movq	-8296(%rbp), %rdx
	movq	%rax, -160(%rbp)
	movq	%rdx, -184(%rbp)
	movq	-8288(%rbp), %rdx
	movq	%rax, -112(%rbp)
	movq	%rdx, -176(%rbp)
	movq	-8280(%rbp), %rdx
	movq	%rdx, -168(%rbp)
	movq	-8264(%rbp), %rdx
	movq	%rdx, -152(%rbp)
	movq	-8256(%rbp), %rdx
	movq	%rdx, -144(%rbp)
	movq	-8248(%rbp), %rdx
	movq	%rdx, -136(%rbp)
	movq	-8240(%rbp), %rdx
	movq	%rdx, -128(%rbp)
	movq	-8232(%rbp), %rdx
	movq	%rdx, -120(%rbp)
	leaq	-104(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8480(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2186
	call	_ZdlPv@PLT
.L2186:
	movq	-8832(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2185
	.p2align 4,,10
	.p2align 3
.L2518:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-8248(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movq	-8280(%rbp), %xmm0
	movq	-8264(%rbp), %rsi
	movq	$0, -8208(%rbp)
	movq	-8264(%rbp), %xmm3
	movdqa	-8736(%rbp), %xmm4
	movq	%rax, -128(%rbp)
	movhps	-8272(%rbp), %xmm0
	movq	-8720(%rbp), %rax
	movq	%rsi, -96(%rbp)
	movq	%rbx, %rsi
	movaps	%xmm0, -160(%rbp)
	movhps	-8256(%rbp), %xmm3
	movq	-8232(%rbp), %xmm0
	movq	-8296(%rbp), %xmm1
	movq	-8312(%rbp), %xmm2
	movq	%rax, -120(%rbp)
	movhps	-8272(%rbp), %xmm0
	movhps	-8288(%rbp), %xmm1
	movq	%rax, -88(%rbp)
	movhps	-8304(%rbp), %xmm2
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm0, -8224(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8744(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2216
	call	_ZdlPv@PLT
.L2216:
	leaq	-3784(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2215
.L2516:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22527:
	.size	_ZN2v88internal26ConstructByArrayBuffer_347EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3MapEEENS4_INS0_13JSArrayBufferEEENS4_INS0_6ObjectEEESC_NS0_34TorqueStructTypedArrayElementsInfoE, .-_ZN2v88internal26ConstructByArrayBuffer_347EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3MapEEENS4_INS0_13JSArrayBufferEEENS4_INS0_6ObjectEEESC_NS0_34TorqueStructTypedArrayElementsInfoE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSFunctionENS0_10JSReceiverENS0_6ObjectES6_S6_NS0_3MapENS0_8UintPtrTENS0_6Int32TES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_SJ_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESJ_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSFunctionENS0_10JSReceiverENS0_6ObjectES6_S6_NS0_3MapENS0_8UintPtrTENS0_6Int32TES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_SJ_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESJ_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSFunctionENS0_10JSReceiverENS0_6ObjectES6_S6_NS0_3MapENS0_8UintPtrTENS0_6Int32TES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_SJ_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESJ_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSFunctionENS0_10JSReceiverENS0_6ObjectES6_S6_NS0_3MapENS0_8UintPtrTENS0_6Int32TES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_SJ_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESJ_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSFunctionENS0_10JSReceiverENS0_6ObjectES6_S6_NS0_3MapENS0_8UintPtrTENS0_6Int32TES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_SJ_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESJ_:
.LFB27674:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$10, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$362267125614053127, %rcx
	movq	%rcx, (%rax)
	movl	$2052, %ecx
	leaq	10(%rax), %rdx
	movw	%cx, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2521
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %rax
.L2521:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2522
	movq	%rdx, (%r15)
.L2522:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2523
	movq	%rdx, (%r14)
.L2523:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2524
	movq	%rdx, 0(%r13)
.L2524:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2525
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L2525:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2526
	movq	%rdx, (%rbx)
.L2526:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2527
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2527:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2528
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2528:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2529
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2529:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2530
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2530:
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L2520
	movq	-128(%rbp), %rsi
	movq	%rax, (%rsi)
.L2520:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2567
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2567:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27674:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSFunctionENS0_10JSReceiverENS0_6ObjectES6_S6_NS0_3MapENS0_8UintPtrTENS0_6Int32TES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_SJ_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESJ_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSFunctionENS0_10JSReceiverENS0_6ObjectES6_S6_NS0_3MapENS0_8UintPtrTENS0_6Int32TES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_SJ_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESJ_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSFunctionENS0_10JSReceiverENS0_6ObjectES6_S6_NS0_3MapENS0_8UintPtrTENS0_6Int32TES6_NS0_10HeapObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESK_SK_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESK_PNSC_ISA_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSFunctionENS0_10JSReceiverENS0_6ObjectES6_S6_NS0_3MapENS0_8UintPtrTENS0_6Int32TES6_NS0_10HeapObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESK_SK_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESK_PNSC_ISA_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSFunctionENS0_10JSReceiverENS0_6ObjectES6_S6_NS0_3MapENS0_8UintPtrTENS0_6Int32TES6_NS0_10HeapObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESK_SK_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESK_PNSC_ISA_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSFunctionENS0_10JSReceiverENS0_6ObjectES6_S6_NS0_3MapENS0_8UintPtrTENS0_6Int32TES6_NS0_10HeapObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESK_SK_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESK_PNSC_ISA_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSFunctionENS0_10JSReceiverENS0_6ObjectES6_S6_NS0_3MapENS0_8UintPtrTENS0_6Int32TES6_NS0_10HeapObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESK_SK_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESK_PNSC_ISA_EE:
.LFB27677:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$11, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$362267125614053127, %rcx
	movq	%rcx, (%rax)
	movl	$2052, %ecx
	leaq	11(%rax), %rdx
	movw	%cx, 8(%rax)
	movb	$7, 10(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2569
	movq	%rax, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %rax
.L2569:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2570
	movq	%rdx, (%r15)
.L2570:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2571
	movq	%rdx, (%r14)
.L2571:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2572
	movq	%rdx, 0(%r13)
.L2572:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2573
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L2573:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2574
	movq	%rdx, (%rbx)
.L2574:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2575
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2575:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2576
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2576:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2577
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2577:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2578
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2578:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2579
	movq	-128(%rbp), %rsi
	movq	%rdx, (%rsi)
.L2579:
	movq	80(%rax), %rax
	testq	%rax, %rax
	je	.L2568
	movq	-136(%rbp), %rcx
	movq	%rax, (%rcx)
.L2568:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2619
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2619:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27677:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSFunctionENS0_10JSReceiverENS0_6ObjectES6_S6_NS0_3MapENS0_8UintPtrTENS0_6Int32TES6_NS0_10HeapObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESK_SK_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESK_PNSC_ISA_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSFunctionENS0_10JSReceiverENS0_6ObjectES6_S6_NS0_3MapENS0_8UintPtrTENS0_6Int32TES6_NS0_10HeapObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESK_SK_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESK_PNSC_ISA_EE
	.section	.text._ZN2v88internal25CreateTypedArrayAssembler28GenerateCreateTypedArrayImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25CreateTypedArrayAssembler28GenerateCreateTypedArrayImplEv
	.type	_ZN2v88internal25CreateTypedArrayAssembler28GenerateCreateTypedArrayImplEv, @function
_ZN2v88internal25CreateTypedArrayAssembler28GenerateCreateTypedArrayImplEv:
.LFB22636:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$5, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-3712(%rbp), %r15
	leaq	-3568(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$4008, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, -3720(%rbp)
	movq	%rax, -3712(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -3872(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -3888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, -3904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, -3920(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	leaq	-3440(%rbp), %r12
	movq	%rax, -3936(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$144, %edi
	movq	$0, -3432(%rbp)
	movq	%rax, -3952(%rbp)
	movq	-3712(%rbp), %rax
	movq	$0, -3424(%rbp)
	movq	%rax, -3440(%rbp)
	movq	$0, -3416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -3432(%rbp)
	leaq	-3384(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3416(%rbp)
	movq	%rdx, -3424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3400(%rbp)
	movq	%rax, -3728(%rbp)
	movq	$0, -3408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3712(%rbp), %rax
	movl	$264, %edi
	movq	$0, -3240(%rbp)
	movq	$0, -3232(%rbp)
	movq	%rax, -3248(%rbp)
	movq	$0, -3224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -3240(%rbp)
	leaq	-3192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3224(%rbp)
	movq	%rdx, -3232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3208(%rbp)
	movq	%rax, -3816(%rbp)
	movq	$0, -3216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3712(%rbp), %rax
	movl	$288, %edi
	movq	$0, -3048(%rbp)
	movq	$0, -3040(%rbp)
	movq	%rax, -3056(%rbp)
	movq	$0, -3032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -3048(%rbp)
	leaq	-3000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3032(%rbp)
	movq	%rdx, -3040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3016(%rbp)
	movq	%rax, -3768(%rbp)
	movq	$0, -3024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3712(%rbp), %rax
	movl	$240, %edi
	movq	$0, -2856(%rbp)
	movq	$0, -2848(%rbp)
	movq	%rax, -2864(%rbp)
	movq	$0, -2840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -2856(%rbp)
	leaq	-2808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2840(%rbp)
	movq	%rdx, -2848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2824(%rbp)
	movq	%rax, -3792(%rbp)
	movq	$0, -2832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3712(%rbp), %rax
	movl	$264, %edi
	movq	$0, -2664(%rbp)
	movq	$0, -2656(%rbp)
	movq	%rax, -2672(%rbp)
	movq	$0, -2648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -2664(%rbp)
	leaq	-2616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2648(%rbp)
	movq	%rdx, -2656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2632(%rbp)
	movq	%rax, -3848(%rbp)
	movq	$0, -2640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3712(%rbp), %rax
	movl	$288, %edi
	movq	$0, -2472(%rbp)
	movq	$0, -2464(%rbp)
	movq	%rax, -2480(%rbp)
	movq	$0, -2456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -2472(%rbp)
	leaq	-2424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2456(%rbp)
	movq	%rdx, -2464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2440(%rbp)
	movq	%rax, -3808(%rbp)
	movq	$0, -2448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3712(%rbp), %rax
	movl	$240, %edi
	movq	$0, -2280(%rbp)
	movq	$0, -2272(%rbp)
	movq	%rax, -2288(%rbp)
	movq	$0, -2264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -2280(%rbp)
	leaq	-2232(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2264(%rbp)
	movq	%rdx, -2272(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2248(%rbp)
	movq	%rax, -3752(%rbp)
	movq	$0, -2256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3712(%rbp), %rax
	movl	$264, %edi
	movq	$0, -2088(%rbp)
	movq	$0, -2080(%rbp)
	movq	%rax, -2096(%rbp)
	movq	$0, -2072(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -2088(%rbp)
	leaq	-2040(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2072(%rbp)
	movq	%rdx, -2080(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2056(%rbp)
	movq	%rax, -3824(%rbp)
	movq	$0, -2064(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3712(%rbp), %rax
	movl	$288, %edi
	movq	$0, -1896(%rbp)
	movq	$0, -1888(%rbp)
	movq	%rax, -1904(%rbp)
	movq	$0, -1880(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -1896(%rbp)
	leaq	-1848(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1880(%rbp)
	movq	%rdx, -1888(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1864(%rbp)
	movq	%rax, -3776(%rbp)
	movq	$0, -1872(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3712(%rbp), %rax
	movl	$360, %edi
	movq	$0, -1704(%rbp)
	movq	$0, -1696(%rbp)
	movq	%rax, -1712(%rbp)
	movq	$0, -1688(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	360(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 352(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movq	%rax, -1704(%rbp)
	leaq	-1656(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1688(%rbp)
	movq	%rdx, -1696(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1672(%rbp)
	movq	%rax, -3840(%rbp)
	movq	$0, -1680(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3712(%rbp), %rax
	movl	$240, %edi
	movq	$0, -1512(%rbp)
	movq	$0, -1504(%rbp)
	movq	%rax, -1520(%rbp)
	movq	$0, -1496(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -1512(%rbp)
	leaq	-1464(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1496(%rbp)
	movq	%rdx, -1504(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1480(%rbp)
	movq	%rax, -3800(%rbp)
	movq	$0, -1488(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3712(%rbp), %rax
	movl	$264, %edi
	movq	$0, -1320(%rbp)
	movq	$0, -1312(%rbp)
	movq	%rax, -1328(%rbp)
	movq	$0, -1304(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -1320(%rbp)
	leaq	-1272(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1304(%rbp)
	movq	%rdx, -1312(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1288(%rbp)
	movq	%rax, -3856(%rbp)
	movq	$0, -1296(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3712(%rbp), %rax
	movl	$288, %edi
	movq	$0, -1128(%rbp)
	movq	$0, -1120(%rbp)
	movq	%rax, -1136(%rbp)
	movq	$0, -1112(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -1128(%rbp)
	leaq	-1080(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1112(%rbp)
	movq	%rdx, -1120(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1096(%rbp)
	movq	%rax, -3760(%rbp)
	movq	$0, -1104(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3712(%rbp), %rax
	movl	$360, %edi
	movq	$0, -936(%rbp)
	movq	$0, -928(%rbp)
	movq	%rax, -944(%rbp)
	movq	$0, -920(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	360(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 352(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movq	%rax, -936(%rbp)
	leaq	-888(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -920(%rbp)
	movq	%rdx, -928(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -904(%rbp)
	movq	%rax, -3832(%rbp)
	movq	$0, -912(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3712(%rbp), %rax
	movl	$240, %edi
	movq	$0, -744(%rbp)
	movq	$0, -736(%rbp)
	movq	%rax, -752(%rbp)
	movq	$0, -728(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -744(%rbp)
	leaq	-696(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -728(%rbp)
	movq	%rdx, -736(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -712(%rbp)
	movq	%rax, -3784(%rbp)
	movq	$0, -720(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3712(%rbp), %rax
	movl	$240, %edi
	movq	$0, -552(%rbp)
	movq	$0, -544(%rbp)
	movq	%rax, -560(%rbp)
	movq	$0, -536(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -552(%rbp)
	leaq	-504(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -536(%rbp)
	movq	%rdx, -544(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -520(%rbp)
	movq	%rax, -3736(%rbp)
	movq	$0, -528(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3712(%rbp), %rax
	movl	$288, %edi
	movq	$0, -360(%rbp)
	movq	$0, -352(%rbp)
	movq	%rax, -368(%rbp)
	movq	$0, -344(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -360(%rbp)
	leaq	-312(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -344(%rbp)
	movq	%rdx, -352(%rbp)
	xorl	%edx, %edx
	movq	%rax, -3744(%rbp)
	movups	%xmm0, -328(%rbp)
	movq	$0, -336(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movq	-3872(%rbp), %xmm1
	movaps	%xmm0, -3568(%rbp)
	movhps	-3888(%rbp), %xmm1
	movq	$0, -3552(%rbp)
	movaps	%xmm1, -176(%rbp)
	movq	-3904(%rbp), %xmm1
	movhps	-3920(%rbp), %xmm1
	movaps	%xmm1, -160(%rbp)
	movq	-3936(%rbp), %xmm1
	movhps	-3952(%rbp), %xmm1
	movaps	%xmm1, -144(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm6
	movq	%r12, %rdi
	movq	%r14, %rsi
	movdqa	-160(%rbp), %xmm7
	leaq	48(%rax), %rdx
	movq	%rax, -3568(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-144(%rbp), %xmm6
	movups	%xmm7, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -3552(%rbp)
	movq	%rdx, -3560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2621
	call	_ZdlPv@PLT
.L2621:
	movq	-3728(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3376(%rbp)
	jne	.L3003
	cmpq	$0, -3184(%rbp)
	jne	.L3004
.L2627:
	cmpq	$0, -2992(%rbp)
	jne	.L3005
.L2630:
	cmpq	$0, -2800(%rbp)
	jne	.L3006
.L2633:
	cmpq	$0, -2608(%rbp)
	jne	.L3007
.L2637:
	cmpq	$0, -2416(%rbp)
	jne	.L3008
.L2639:
	cmpq	$0, -2224(%rbp)
	jne	.L3009
.L2646:
	cmpq	$0, -2032(%rbp)
	jne	.L3010
.L2650:
	cmpq	$0, -1840(%rbp)
	jne	.L3011
.L2652:
	cmpq	$0, -1648(%rbp)
	jne	.L3012
.L2656:
	cmpq	$0, -1456(%rbp)
	jne	.L3013
.L2659:
	cmpq	$0, -1264(%rbp)
	jne	.L3014
.L2663:
	cmpq	$0, -1072(%rbp)
	jne	.L3015
.L2665:
	cmpq	$0, -880(%rbp)
	jne	.L3016
.L2669:
	cmpq	$0, -688(%rbp)
	jne	.L3017
.L2672:
	cmpq	$0, -496(%rbp)
	jne	.L3018
.L2674:
	cmpq	$0, -304(%rbp)
	jne	.L3019
.L2675:
	movq	-3744(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2680
	call	_ZdlPv@PLT
.L2680:
	movq	-352(%rbp), %rbx
	movq	-360(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2681
	.p2align 4,,10
	.p2align 3
.L2685:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2682
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L2685
.L2683:
	movq	-360(%rbp), %r12
.L2681:
	testq	%r12, %r12
	je	.L2686
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2686:
	movq	-3736(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2687
	call	_ZdlPv@PLT
.L2687:
	movq	-544(%rbp), %rbx
	movq	-552(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2688
	.p2align 4,,10
	.p2align 3
.L2692:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2689
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L2692
.L2690:
	movq	-552(%rbp), %r12
.L2688:
	testq	%r12, %r12
	je	.L2693
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2693:
	movq	-3784(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-720(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2694
	call	_ZdlPv@PLT
.L2694:
	movq	-736(%rbp), %rbx
	movq	-744(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2695
	.p2align 4,,10
	.p2align 3
.L2699:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2696
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L2699
.L2697:
	movq	-744(%rbp), %r12
.L2695:
	testq	%r12, %r12
	je	.L2700
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2700:
	movq	-3832(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-912(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2701
	call	_ZdlPv@PLT
.L2701:
	movq	-928(%rbp), %rbx
	movq	-936(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2702
	.p2align 4,,10
	.p2align 3
.L2706:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2703
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L2706
.L2704:
	movq	-936(%rbp), %r12
.L2702:
	testq	%r12, %r12
	je	.L2707
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2707:
	movq	-3760(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2708
	call	_ZdlPv@PLT
.L2708:
	movq	-1120(%rbp), %rbx
	movq	-1128(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2709
	.p2align 4,,10
	.p2align 3
.L2713:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2710
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L2713
.L2711:
	movq	-1128(%rbp), %r12
.L2709:
	testq	%r12, %r12
	je	.L2714
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2714:
	movq	-3856(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1296(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2715
	call	_ZdlPv@PLT
.L2715:
	movq	-1312(%rbp), %rbx
	movq	-1320(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2716
	.p2align 4,,10
	.p2align 3
.L2720:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2717
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L2720
.L2718:
	movq	-1320(%rbp), %r12
.L2716:
	testq	%r12, %r12
	je	.L2721
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2721:
	movq	-3800(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1488(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2722
	call	_ZdlPv@PLT
.L2722:
	movq	-1504(%rbp), %rbx
	movq	-1512(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2723
	.p2align 4,,10
	.p2align 3
.L2727:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2724
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L2727
.L2725:
	movq	-1512(%rbp), %r12
.L2723:
	testq	%r12, %r12
	je	.L2728
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2728:
	movq	-3840(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1680(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2729
	call	_ZdlPv@PLT
.L2729:
	movq	-1696(%rbp), %rbx
	movq	-1704(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2730
	.p2align 4,,10
	.p2align 3
.L2734:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2731
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L2734
.L2732:
	movq	-1704(%rbp), %r12
.L2730:
	testq	%r12, %r12
	je	.L2735
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2735:
	movq	-3776(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1872(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2736
	call	_ZdlPv@PLT
.L2736:
	movq	-1888(%rbp), %rbx
	movq	-1896(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2737
	.p2align 4,,10
	.p2align 3
.L2741:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2738
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L2741
.L2739:
	movq	-1896(%rbp), %r12
.L2737:
	testq	%r12, %r12
	je	.L2742
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2742:
	movq	-3824(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2743
	call	_ZdlPv@PLT
.L2743:
	movq	-2080(%rbp), %rbx
	movq	-2088(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2744
	.p2align 4,,10
	.p2align 3
.L2748:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2745
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L2748
.L2746:
	movq	-2088(%rbp), %r12
.L2744:
	testq	%r12, %r12
	je	.L2749
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2749:
	movq	-3752(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2750
	call	_ZdlPv@PLT
.L2750:
	movq	-2272(%rbp), %rbx
	movq	-2280(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2751
	.p2align 4,,10
	.p2align 3
.L2755:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2752
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L2755
.L2753:
	movq	-2280(%rbp), %r12
.L2751:
	testq	%r12, %r12
	je	.L2756
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2756:
	movq	-3808(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2757
	call	_ZdlPv@PLT
.L2757:
	movq	-2464(%rbp), %rbx
	movq	-2472(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2758
	.p2align 4,,10
	.p2align 3
.L2762:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2759
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L2762
.L2760:
	movq	-2472(%rbp), %r12
.L2758:
	testq	%r12, %r12
	je	.L2763
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2763:
	movq	-3848(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2764
	call	_ZdlPv@PLT
.L2764:
	movq	-2656(%rbp), %rbx
	movq	-2664(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2765
	.p2align 4,,10
	.p2align 3
.L2769:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2766
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L2769
.L2767:
	movq	-2664(%rbp), %r12
.L2765:
	testq	%r12, %r12
	je	.L2770
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2770:
	movq	-3792(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2771
	call	_ZdlPv@PLT
.L2771:
	movq	-2848(%rbp), %rbx
	movq	-2856(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2772
	.p2align 4,,10
	.p2align 3
.L2776:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2773
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L2776
.L2774:
	movq	-2856(%rbp), %r12
.L2772:
	testq	%r12, %r12
	je	.L2777
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2777:
	movq	-3768(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2778
	call	_ZdlPv@PLT
.L2778:
	movq	-3040(%rbp), %rbx
	movq	-3048(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2779
	.p2align 4,,10
	.p2align 3
.L2783:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2780
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L2783
.L2781:
	movq	-3048(%rbp), %r12
.L2779:
	testq	%r12, %r12
	je	.L2784
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2784:
	movq	-3816(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2785
	call	_ZdlPv@PLT
.L2785:
	movq	-3232(%rbp), %rbx
	movq	-3240(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2786
	.p2align 4,,10
	.p2align 3
.L2790:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2787
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L2790
.L2788:
	movq	-3240(%rbp), %r12
.L2786:
	testq	%r12, %r12
	je	.L2791
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2791:
	movq	-3728(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2792
	call	_ZdlPv@PLT
.L2792:
	movq	-3424(%rbp), %rbx
	movq	-3432(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2793
	.p2align 4,,10
	.p2align 3
.L2797:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2794
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L2797
.L2795:
	movq	-3432(%rbp), %r12
.L2793:
	testq	%r12, %r12
	je	.L2798
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2798:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3020
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2794:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L2797
	jmp	.L2795
	.p2align 4,,10
	.p2align 3
.L2787:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L2790
	jmp	.L2788
	.p2align 4,,10
	.p2align 3
.L2780:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L2783
	jmp	.L2781
	.p2align 4,,10
	.p2align 3
.L2773:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L2776
	jmp	.L2774
	.p2align 4,,10
	.p2align 3
.L2766:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L2769
	jmp	.L2767
	.p2align 4,,10
	.p2align 3
.L2759:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L2762
	jmp	.L2760
	.p2align 4,,10
	.p2align 3
.L2752:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L2755
	jmp	.L2753
	.p2align 4,,10
	.p2align 3
.L2745:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L2748
	jmp	.L2746
	.p2align 4,,10
	.p2align 3
.L2738:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L2741
	jmp	.L2739
	.p2align 4,,10
	.p2align 3
.L2731:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L2734
	jmp	.L2732
	.p2align 4,,10
	.p2align 3
.L2724:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L2727
	jmp	.L2725
	.p2align 4,,10
	.p2align 3
.L2717:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L2720
	jmp	.L2718
	.p2align 4,,10
	.p2align 3
.L2710:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L2713
	jmp	.L2711
	.p2align 4,,10
	.p2align 3
.L2703:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L2706
	jmp	.L2704
	.p2align 4,,10
	.p2align 3
.L2696:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L2699
	jmp	.L2697
	.p2align 4,,10
	.p2align 3
.L2682:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L2685
	jmp	.L2683
	.p2align 4,,10
	.p2align 3
.L2689:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L2692
	jmp	.L2690
	.p2align 4,,10
	.p2align 3
.L3003:
	movq	-3728(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-176(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$2056, %eax
	movq	%rbx, %rsi
	movq	%r14, %rdi
	pxor	%xmm0, %xmm0
	leaq	-170(%rbp), %rdx
	movw	%ax, -172(%rbp)
	movaps	%xmm0, -3568(%rbp)
	movl	$134678279, -176(%rbp)
	movq	$0, -3552(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3568(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L2623
	call	_ZdlPv@PLT
.L2623:
	movq	(%r12), %rax
	movl	$325, %edx
	movq	8(%rax), %rcx
	movq	16(%rax), %rsi
	movq	32(%rax), %rdi
	movq	(%rax), %r12
	movq	%rcx, -3872(%rbp)
	movq	24(%rax), %rcx
	movq	40(%rax), %rax
	movq	%rsi, -3888(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdi, -3920(%rbp)
	movq	%r15, %rdi
	movq	%rcx, %r13
	movq	%rax, -3936(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3888(%rbp), %rcx
	movq	-3872(%rbp), %rdx
	movq	%r12, %rsi
	movq	-3720(%rbp), %rdi
	call	_ZN2v88internal16GetDerivedMap_56EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSFunctionEEENS4_INS0_10JSReceiverEEE@PLT
	movl	$330, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -3904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3720(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3904(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal27TypedArrayBuiltinsAssembler25GetTypedArrayElementsInfoENS0_8compiler5TNodeINS0_3MapEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -3952(%rbp)
	movq	%rdx, -3968(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$329, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$333, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$334, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3720(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal13Cast5ATSmi_83EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %xmm5
	movq	%r13, %xmm3
	movq	-3968(%rbp), %xmm7
	movq	-3888(%rbp), %xmm4
	movq	-3920(%rbp), %xmm6
	movq	%r12, %xmm2
	movq	%rax, -88(%rbp)
	leaq	-3600(%rbp), %r12
	punpcklqdq	%xmm5, %xmm7
	punpcklqdq	%xmm3, %xmm4
	movq	%r12, %rdi
	movq	%r13, -96(%rbp)
	movq	-3904(%rbp), %xmm5
	movhps	-3936(%rbp), %xmm6
	movhps	-3872(%rbp), %xmm2
	movaps	%xmm7, -3968(%rbp)
	movaps	%xmm6, -3904(%rbp)
	movhps	-3952(%rbp), %xmm5
	movaps	%xmm4, -3888(%rbp)
	movaps	%xmm5, -3952(%rbp)
	movaps	%xmm2, -3872(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm0, -3600(%rbp)
	movq	$0, -3584(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3056(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2624
	call	_ZdlPv@PLT
.L2624:
	movq	-3768(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3560(%rbp)
	jne	.L3021
.L2625:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -3184(%rbp)
	je	.L2627
.L3004:
	movq	-3816(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-3248(%rbp), %r12
	movl	$2052, %r13d
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-176(%rbp), %rsi
	movabsq	$362267125614053127, %rax
	leaq	-165(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, -176(%rbp)
	movaps	%xmm0, -3568(%rbp)
	movw	%r13w, -168(%rbp)
	movb	$8, -166(%rbp)
	movq	$0, -3552(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2628
	call	_ZdlPv@PLT
.L2628:
	movq	(%rbx), %rax
	movl	$80, %edi
	movdqu	64(%rax), %xmm0
	movdqu	16(%rax), %xmm3
	movdqu	32(%rax), %xmm2
	movdqu	48(%rax), %xmm1
	movdqu	(%rax), %xmm6
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3568(%rbp)
	movq	$0, -3552(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm6
	movdqa	-160(%rbp), %xmm7
	movq	%r14, %rsi
	leaq	80(%rax), %rdx
	leaq	-2864(%rbp), %rdi
	movq	%rax, -3568(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-144(%rbp), %xmm6
	movups	%xmm7, 16(%rax)
	movdqa	-128(%rbp), %xmm7
	movups	%xmm6, 32(%rax)
	movdqa	-112(%rbp), %xmm6
	movups	%xmm7, 48(%rax)
	movups	%xmm6, 64(%rax)
	movq	%rdx, -3552(%rbp)
	movq	%rdx, -3560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2629
	call	_ZdlPv@PLT
.L2629:
	movq	-3792(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2992(%rbp)
	je	.L2630
.L3005:
	movq	-3768(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-3056(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-176(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-164(%rbp), %rdx
	movabsq	$362267125614053127, %rax
	movaps	%xmm0, -3568(%rbp)
	movq	%rax, -176(%rbp)
	movl	$101189636, -168(%rbp)
	movq	$0, -3552(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2631
	call	_ZdlPv@PLT
.L2631:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rbx
	movq	8(%rax), %rsi
	movq	40(%rax), %rdx
	movq	(%rax), %rcx
	movq	%rbx, -3904(%rbp)
	movq	32(%rax), %rbx
	movq	%rsi, %r13
	movq	16(%rax), %rsi
	movq	%rdx, -3936(%rbp)
	movq	56(%rax), %rdx
	movq	48(%rax), %r12
	movq	%rbx, -3920(%rbp)
	movq	64(%rax), %rbx
	movq	88(%rax), %rax
	movq	%rsi, -3888(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -3952(%rbp)
	movl	$335, %edx
	movq	%rcx, -3872(%rbp)
	movq	%rax, -3968(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %xmm6
	movl	$80, %edi
	movq	-3872(%rbp), %xmm0
	movq	$0, -3552(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-3888(%rbp), %xmm0
	movhps	-3904(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-3920(%rbp), %xmm0
	movhps	-3936(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%r12, %xmm0
	movhps	-3952(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%rbx, %xmm0
	movhps	-3968(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3568(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-160(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm5
	leaq	80(%rax), %rdx
	leaq	-560(%rbp), %rdi
	movq	%rax, -3568(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-144(%rbp), %xmm7
	movups	%xmm6, 16(%rax)
	movdqa	-128(%rbp), %xmm6
	movups	%xmm7, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rdx, -3552(%rbp)
	movq	%rdx, -3560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2632
	call	_ZdlPv@PLT
.L2632:
	movq	-3736(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2800(%rbp)
	je	.L2633
.L3006:
	movq	-3792(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -3704(%rbp)
	leaq	-2864(%rbp), %r12
	movq	$0, -3696(%rbp)
	leaq	-176(%rbp), %rbx
	movq	$0, -3688(%rbp)
	movq	$0, -3680(%rbp)
	movq	$0, -3672(%rbp)
	movq	$0, -3664(%rbp)
	movq	$0, -3656(%rbp)
	movq	$0, -3648(%rbp)
	movq	$0, -3632(%rbp)
	movq	$0, -3616(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-3616(%rbp), %rax
	pushq	%rax
	leaq	-3632(%rbp), %rax
	leaq	-3672(%rbp), %r9
	pushq	%rax
	leaq	-3648(%rbp), %rax
	leaq	-3688(%rbp), %rcx
	pushq	%rax
	leaq	-3656(%rbp), %rax
	leaq	-3680(%rbp), %r8
	pushq	%rax
	leaq	-3664(%rbp), %rax
	leaq	-3696(%rbp), %rdx
	pushq	%rax
	leaq	-3704(%rbp), %rsi
	leaq	-3600(%rbp), %r12
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSFunctionENS0_10JSReceiverENS0_6ObjectES6_S6_NS0_3MapENS0_8UintPtrTENS0_6Int32TES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_SJ_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESJ_
	addq	$48, %rsp
	movl	$337, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3616(%rbp), %rsi
	movq	-3720(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal23Cast13JSArrayBuffer_114EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-80(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-3632(%rbp), %xmm0
	movq	%rax, %xmm7
	movq	-3616(%rbp), %xmm5
	movq	$0, -3584(%rbp)
	movq	-3656(%rbp), %xmm1
	movq	-3672(%rbp), %xmm2
	movq	-3688(%rbp), %xmm3
	movhps	-3616(%rbp), %xmm0
	punpcklqdq	%xmm7, %xmm5
	movq	-3704(%rbp), %xmm4
	movhps	-3648(%rbp), %xmm1
	movhps	-3664(%rbp), %xmm2
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3680(%rbp), %xmm3
	movhps	-3696(%rbp), %xmm4
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm0, -3600(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2480(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2634
	call	_ZdlPv@PLT
.L2634:
	movq	-3808(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3560(%rbp)
	jne	.L3022
.L2635:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2608(%rbp)
	je	.L2637
.L3007:
	movq	-3848(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -3704(%rbp)
	leaq	-2672(%rbp), %r12
	movq	$0, -3696(%rbp)
	movq	$0, -3688(%rbp)
	movq	$0, -3680(%rbp)
	movq	$0, -3672(%rbp)
	movq	$0, -3664(%rbp)
	movq	$0, -3656(%rbp)
	movq	$0, -3648(%rbp)
	movq	$0, -3632(%rbp)
	movq	$0, -3616(%rbp)
	movq	$0, -3600(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3600(%rbp), %rax
	movq	%r12, %rdi
	leaq	-3688(%rbp), %rcx
	pushq	%rax
	leaq	-3616(%rbp), %rax
	leaq	-3696(%rbp), %rdx
	pushq	%rax
	leaq	-3632(%rbp), %rax
	leaq	-3704(%rbp), %rsi
	pushq	%rax
	leaq	-3648(%rbp), %rax
	leaq	-3672(%rbp), %r9
	pushq	%rax
	leaq	-3656(%rbp), %rax
	leaq	-3680(%rbp), %r8
	pushq	%rax
	leaq	-3664(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSFunctionENS0_10JSReceiverENS0_6ObjectES6_S6_NS0_3MapENS0_8UintPtrTENS0_6Int32TES6_NS0_10HeapObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESK_SK_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESK_PNSC_ISA_EE
	addq	$48, %rsp
	movl	$80, %edi
	movq	-3632(%rbp), %xmm0
	movq	-3656(%rbp), %xmm1
	movq	-3672(%rbp), %xmm2
	movq	$0, -3552(%rbp)
	movq	-3688(%rbp), %xmm3
	movhps	-3616(%rbp), %xmm0
	movq	-3704(%rbp), %xmm4
	movhps	-3648(%rbp), %xmm1
	movhps	-3664(%rbp), %xmm2
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3680(%rbp), %xmm3
	movhps	-3696(%rbp), %xmm4
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm0, -3568(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm6
	movq	%r14, %rsi
	movdqa	-160(%rbp), %xmm5
	movdqa	-144(%rbp), %xmm7
	leaq	80(%rax), %rdx
	leaq	-2288(%rbp), %rdi
	movq	%rax, -3568(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-128(%rbp), %xmm6
	movups	%xmm5, 16(%rax)
	movdqa	-112(%rbp), %xmm5
	movups	%xmm7, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rdx, -3552(%rbp)
	movq	%rdx, -3560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2638
	call	_ZdlPv@PLT
.L2638:
	movq	-3752(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2416(%rbp)
	je	.L2639
.L3008:
	movq	-3808(%rbp), %rsi
	xorl	%r9d, %r9d
	movq	%r15, %rdi
	xorl	%ebx, %ebx
	leaq	-2480(%rbp), %r8
	movq	%r9, -3904(%rbp)
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	movq	%r8, -3888(%rbp)
	movq	$0, -3872(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-176(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-164(%rbp), %rdx
	movabsq	$362267125614053127, %rax
	movaps	%xmm0, -3568(%rbp)
	movq	%rax, -176(%rbp)
	movl	$117901316, -168(%rbp)
	movq	$0, -3552(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3888(%rbp), %r8
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3568(%rbp), %rdi
	movq	-3904(%rbp), %r9
	testq	%rdi, %rdi
	je	.L2640
	movq	%rax, -3888(%rbp)
	call	_ZdlPv@PLT
	movq	-3904(%rbp), %r9
	movq	-3888(%rbp), %rax
.L2640:
	movq	(%rax), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	(%rax), %rdx
	movq	56(%rax), %xmm7
	testq	%rdx, %rdx
	cmove	-3872(%rbp), %rdx
	movhps	64(%rax), %xmm7
	movaps	%xmm7, -3888(%rbp)
	movq	%rdx, -3872(%rbp)
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	cmovne	%rdx, %rbx
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	cmovne	%rdx, %r9
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	movq	%r9, -3904(%rbp)
	cmovne	%rdx, %r12
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	movl	$338, %edx
	cmove	%r13, %rcx
	movq	%rcx, %r13
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %r8
	pushq	-3880(%rbp)
	movq	-3720(%rbp), %rbx
	pushq	-3888(%rbp)
	movq	%r12, %rdx
	movq	%r13, %rcx
	movq	-3904(%rbp), %r9
	movq	%rbx, %rdi
	movq	-3872(%rbp), %rsi
	call	_ZN2v88internal26ConstructByArrayBuffer_347EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3MapEEENS4_INS0_13JSArrayBufferEEENS4_INS0_6ObjectEEESC_NS0_34TorqueStructTypedArrayElementsInfoE
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -2224(%rbp)
	popq	%r11
	popq	%rbx
	je	.L2646
.L3009:
	movq	-3752(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -3704(%rbp)
	leaq	-2288(%rbp), %r12
	movq	$0, -3696(%rbp)
	leaq	-176(%rbp), %rbx
	movq	$0, -3688(%rbp)
	movq	$0, -3680(%rbp)
	movq	$0, -3672(%rbp)
	movq	$0, -3664(%rbp)
	movq	$0, -3656(%rbp)
	movq	$0, -3648(%rbp)
	movq	$0, -3632(%rbp)
	movq	$0, -3616(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-3616(%rbp), %rax
	pushq	%rax
	leaq	-3632(%rbp), %rax
	leaq	-3672(%rbp), %r9
	pushq	%rax
	leaq	-3648(%rbp), %rax
	leaq	-3688(%rbp), %rcx
	pushq	%rax
	leaq	-3656(%rbp), %rax
	leaq	-3680(%rbp), %r8
	pushq	%rax
	leaq	-3664(%rbp), %rax
	leaq	-3696(%rbp), %rdx
	pushq	%rax
	leaq	-3704(%rbp), %rsi
	leaq	-3600(%rbp), %r12
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSFunctionENS0_10JSReceiverENS0_6ObjectES6_S6_NS0_3MapENS0_8UintPtrTENS0_6Int32TES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_SJ_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESJ_
	addq	$48, %rsp
	movl	$340, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3616(%rbp), %rsi
	movq	-3720(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal22Cast12JSTypedArray_109EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-80(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-3632(%rbp), %xmm0
	movq	%rax, %xmm7
	movq	-3616(%rbp), %xmm5
	movq	$0, -3584(%rbp)
	movq	-3656(%rbp), %xmm1
	movq	-3672(%rbp), %xmm2
	movq	-3688(%rbp), %xmm3
	movhps	-3616(%rbp), %xmm0
	punpcklqdq	%xmm7, %xmm5
	movq	-3704(%rbp), %xmm4
	movhps	-3648(%rbp), %xmm1
	movhps	-3664(%rbp), %xmm2
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3680(%rbp), %xmm3
	movhps	-3696(%rbp), %xmm4
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm0, -3600(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1904(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2647
	call	_ZdlPv@PLT
.L2647:
	movq	-3776(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3560(%rbp)
	jne	.L3023
.L2648:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2032(%rbp)
	je	.L2650
.L3010:
	movq	-3824(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -3704(%rbp)
	leaq	-2096(%rbp), %r12
	movq	$0, -3696(%rbp)
	movq	$0, -3688(%rbp)
	movq	$0, -3680(%rbp)
	movq	$0, -3672(%rbp)
	movq	$0, -3664(%rbp)
	movq	$0, -3656(%rbp)
	movq	$0, -3648(%rbp)
	movq	$0, -3632(%rbp)
	movq	$0, -3616(%rbp)
	movq	$0, -3600(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3600(%rbp), %rax
	movq	%r12, %rdi
	leaq	-3688(%rbp), %rcx
	pushq	%rax
	leaq	-3616(%rbp), %rax
	leaq	-3696(%rbp), %rdx
	pushq	%rax
	leaq	-3632(%rbp), %rax
	leaq	-3704(%rbp), %rsi
	pushq	%rax
	leaq	-3648(%rbp), %rax
	leaq	-3672(%rbp), %r9
	pushq	%rax
	leaq	-3656(%rbp), %rax
	leaq	-3680(%rbp), %r8
	pushq	%rax
	leaq	-3664(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSFunctionENS0_10JSReceiverENS0_6ObjectES6_S6_NS0_3MapENS0_8UintPtrTENS0_6Int32TES6_NS0_10HeapObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESK_SK_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESK_PNSC_ISA_EE
	addq	$48, %rsp
	movl	$80, %edi
	movq	-3632(%rbp), %xmm0
	movq	-3656(%rbp), %xmm1
	movq	-3672(%rbp), %xmm2
	movq	$0, -3552(%rbp)
	movq	-3688(%rbp), %xmm3
	movhps	-3616(%rbp), %xmm0
	movq	-3704(%rbp), %xmm4
	movhps	-3648(%rbp), %xmm1
	movhps	-3664(%rbp), %xmm2
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3680(%rbp), %xmm3
	movhps	-3696(%rbp), %xmm4
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm0, -3568(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm6
	movq	%r14, %rsi
	movdqa	-160(%rbp), %xmm5
	movdqa	-144(%rbp), %xmm7
	leaq	80(%rax), %rdx
	leaq	-1520(%rbp), %rdi
	movq	%rax, -3568(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-128(%rbp), %xmm6
	movups	%xmm5, 16(%rax)
	movdqa	-112(%rbp), %xmm5
	movups	%xmm7, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rdx, -3552(%rbp)
	movq	%rdx, -3560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2651
	call	_ZdlPv@PLT
.L2651:
	movq	-3800(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1840(%rbp)
	je	.L2652
.L3011:
	movq	-3776(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-176(%rbp), %rbx
	leaq	-1904(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r14, %rdi
	leaq	-164(%rbp), %rdx
	movabsq	$362267125614053127, %rax
	movaps	%xmm0, -3568(%rbp)
	movq	%rax, -176(%rbp)
	movl	$117901316, -168(%rbp)
	movq	$0, -3552(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3568(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L2653
	call	_ZdlPv@PLT
.L2653:
	movq	(%r12), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	leaq	-3648(%rbp), %r12
	movq	8(%rax), %rdx
	movq	(%rax), %rcx
	movq	%rdx, -3920(%rbp)
	movq	16(%rax), %rdx
	movq	%rcx, -3888(%rbp)
	movq	%rdx, -3936(%rbp)
	movq	24(%rax), %rdx
	movq	%rdx, -3952(%rbp)
	movq	32(%rax), %rdx
	movq	%rdx, -3968(%rbp)
	movq	40(%rax), %rdx
	movq	%rdx, -3976(%rbp)
	movq	48(%rax), %rdx
	movq	%rdx, -3984(%rbp)
	movq	56(%rax), %rdx
	movq	%rdx, -3992(%rbp)
	movq	64(%rax), %rdx
	movq	%rdx, -4000(%rbp)
	movq	72(%rax), %rdx
	movq	88(%rax), %rax
	movq	%rdx, -4008(%rbp)
	movl	$341, %edx
	movq	%rax, -3904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$7, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-3632(%rbp), %rax
	movl	$8, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -3872(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-3616(%rbp), %rax
	movl	$7, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	subq	$8, %rsp
	movq	%r12, %r8
	movq	-3872(%rbp), %r9
	pushq	%r13
	movq	-3904(%rbp), %rdx
	movq	%r14, %rcx
	movq	-3888(%rbp), %rsi
	movq	-3720(%rbp), %rdi
	call	_ZN2v88internal25ConstructByTypedArray_346EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableIS7_EEPNSB_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEEPNSB_INS0_10JSReceiverEEE
	cmpq	$0, -3560(%rbp)
	popq	%r9
	popq	%r10
	jne	.L3024
.L2654:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-3872(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	cmpq	$0, -1648(%rbp)
	je	.L2656
.L3012:
	movq	-3840(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-176(%rbp), %rbx
	leaq	-1712(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movabsq	$362267125614053127, %rax
	movl	$2055, %r8d
	leaq	-161(%rbp), %rdx
	movaps	%xmm0, -3568(%rbp)
	movq	%rax, -176(%rbp)
	movw	%r8w, -164(%rbp)
	movl	$117901316, -168(%rbp)
	movb	$7, -162(%rbp)
	movq	$0, -3552(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3568(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L2657
	call	_ZdlPv@PLT
.L2657:
	movq	(%r12), %rax
	pxor	%xmm0, %xmm0
	movq	(%rax), %rcx
	movq	16(%rax), %rsi
	movq	56(%rax), %rdi
	movq	104(%rax), %rdx
	movq	%rsi, -3872(%rbp)
	movq	%rcx, %r13
	movq	64(%rax), %rsi
	movq	24(%rax), %r11
	movq	32(%rax), %r10
	movq	40(%rax), %r9
	movq	48(%rax), %r8
	movq	96(%rax), %rcx
	movq	8(%rax), %r12
	movq	112(%rax), %rax
	movq	%r13, -176(%rbp)
	movq	-3872(%rbp), %r13
	movq	%rdi, -120(%rbp)
	movq	%r14, %rdi
	movq	%rsi, -112(%rbp)
	movq	%rbx, %rsi
	movq	%rdx, -96(%rbp)
	leaq	-80(%rbp), %rdx
	movq	%r11, -152(%rbp)
	movq	%r10, -144(%rbp)
	movq	%r9, -136(%rbp)
	movq	%r8, -128(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%rax, -88(%rbp)
	movaps	%xmm0, -3568(%rbp)
	movq	%r12, -168(%rbp)
	movq	%r13, -160(%rbp)
	movq	$0, -3552(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-368(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2658
	call	_ZdlPv@PLT
.L2658:
	movq	-3744(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1456(%rbp)
	je	.L2659
.L3013:
	movq	-3800(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -3704(%rbp)
	leaq	-1520(%rbp), %r12
	movq	$0, -3696(%rbp)
	leaq	-176(%rbp), %rbx
	movq	$0, -3688(%rbp)
	movq	$0, -3680(%rbp)
	movq	$0, -3672(%rbp)
	movq	$0, -3664(%rbp)
	movq	$0, -3656(%rbp)
	movq	$0, -3648(%rbp)
	movq	$0, -3632(%rbp)
	movq	$0, -3616(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-3616(%rbp), %rax
	pushq	%rax
	leaq	-3632(%rbp), %rax
	leaq	-3672(%rbp), %r9
	pushq	%rax
	leaq	-3648(%rbp), %rax
	leaq	-3688(%rbp), %rcx
	pushq	%rax
	leaq	-3656(%rbp), %rax
	leaq	-3680(%rbp), %r8
	pushq	%rax
	leaq	-3664(%rbp), %rax
	leaq	-3696(%rbp), %rdx
	pushq	%rax
	leaq	-3704(%rbp), %rsi
	leaq	-3600(%rbp), %r12
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSFunctionENS0_10JSReceiverENS0_6ObjectES6_S6_NS0_3MapENS0_8UintPtrTENS0_6Int32TES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_SJ_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESJ_
	addq	$48, %rsp
	movl	$343, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3616(%rbp), %rsi
	movq	-3720(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal20Cast10JSReceiver_140EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-80(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-3632(%rbp), %xmm0
	movq	%rax, %xmm7
	movq	-3616(%rbp), %xmm5
	movq	$0, -3584(%rbp)
	movq	-3656(%rbp), %xmm1
	movq	-3672(%rbp), %xmm2
	movq	-3688(%rbp), %xmm3
	movhps	-3616(%rbp), %xmm0
	punpcklqdq	%xmm7, %xmm5
	movq	-3704(%rbp), %xmm4
	movhps	-3648(%rbp), %xmm1
	movhps	-3664(%rbp), %xmm2
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3680(%rbp), %xmm3
	movhps	-3696(%rbp), %xmm4
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm0, -3600(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1136(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2660
	call	_ZdlPv@PLT
.L2660:
	movq	-3760(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3560(%rbp)
	jne	.L3025
.L2661:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1264(%rbp)
	je	.L2663
.L3014:
	movq	-3856(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -3704(%rbp)
	leaq	-1328(%rbp), %r12
	movq	$0, -3696(%rbp)
	movq	$0, -3688(%rbp)
	movq	$0, -3680(%rbp)
	movq	$0, -3672(%rbp)
	movq	$0, -3664(%rbp)
	movq	$0, -3656(%rbp)
	movq	$0, -3648(%rbp)
	movq	$0, -3632(%rbp)
	movq	$0, -3616(%rbp)
	movq	$0, -3600(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3600(%rbp), %rax
	movq	%r12, %rdi
	leaq	-3688(%rbp), %rcx
	pushq	%rax
	leaq	-3616(%rbp), %rax
	leaq	-3696(%rbp), %rdx
	pushq	%rax
	leaq	-3632(%rbp), %rax
	leaq	-3704(%rbp), %rsi
	pushq	%rax
	leaq	-3648(%rbp), %rax
	leaq	-3672(%rbp), %r9
	pushq	%rax
	leaq	-3656(%rbp), %rax
	leaq	-3680(%rbp), %r8
	pushq	%rax
	leaq	-3664(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSFunctionENS0_10JSReceiverENS0_6ObjectES6_S6_NS0_3MapENS0_8UintPtrTENS0_6Int32TES6_NS0_10HeapObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESK_SK_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESK_PNSC_ISA_EE
	addq	$48, %rsp
	movl	$80, %edi
	movq	-3632(%rbp), %xmm0
	movq	-3656(%rbp), %xmm1
	movq	-3672(%rbp), %xmm2
	movq	$0, -3552(%rbp)
	movq	-3688(%rbp), %xmm3
	movhps	-3616(%rbp), %xmm0
	movq	-3704(%rbp), %xmm4
	movhps	-3648(%rbp), %xmm1
	movhps	-3664(%rbp), %xmm2
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3680(%rbp), %xmm3
	movhps	-3696(%rbp), %xmm4
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm0, -3568(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm6
	movq	%r14, %rsi
	movdqa	-160(%rbp), %xmm5
	movdqa	-144(%rbp), %xmm7
	leaq	80(%rax), %rdx
	leaq	-752(%rbp), %rdi
	movq	%rax, -3568(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-128(%rbp), %xmm6
	movups	%xmm5, 16(%rax)
	movdqa	-112(%rbp), %xmm5
	movups	%xmm7, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rdx, -3552(%rbp)
	movq	%rdx, -3560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2664
	call	_ZdlPv@PLT
.L2664:
	movq	-3784(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1072(%rbp)
	je	.L2665
.L3015:
	movq	-3760(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-176(%rbp), %rbx
	leaq	-1136(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r14, %rdi
	leaq	-164(%rbp), %rdx
	movabsq	$362267125614053127, %rax
	movaps	%xmm0, -3568(%rbp)
	movq	%rax, -176(%rbp)
	movl	$117901316, -168(%rbp)
	movq	$0, -3552(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3568(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L2666
	call	_ZdlPv@PLT
.L2666:
	movq	(%r12), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	leaq	-3648(%rbp), %r12
	movq	8(%rax), %rdx
	movq	(%rax), %rcx
	movq	%rdx, -3920(%rbp)
	movq	16(%rax), %rdx
	movq	%rcx, -3888(%rbp)
	movq	%rdx, -3936(%rbp)
	movq	24(%rax), %rdx
	movq	%rdx, -3952(%rbp)
	movq	32(%rax), %rdx
	movq	%rdx, -3968(%rbp)
	movq	40(%rax), %rdx
	movq	%rdx, -3976(%rbp)
	movq	48(%rax), %rdx
	movq	%rdx, -3984(%rbp)
	movq	56(%rax), %rdx
	movq	%rdx, -3992(%rbp)
	movq	64(%rax), %rdx
	movq	%rdx, -4000(%rbp)
	movq	72(%rax), %rdx
	movq	88(%rax), %rax
	movq	%rdx, -4008(%rbp)
	movl	$344, %edx
	movq	%rax, -3904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$7, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-3632(%rbp), %rax
	movl	$8, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -3872(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-3616(%rbp), %rax
	movl	$7, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	subq	$8, %rsp
	movq	%r12, %r8
	movq	-3888(%rbp), %rsi
	pushq	%r13
	movq	-3720(%rbp), %rdi
	movq	%r14, %rcx
	movq	-3872(%rbp), %r9
	movq	-3904(%rbp), %rdx
	call	_ZN2v88internal25ConstructByJSReceiver_348EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableIS7_EEPNSB_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEESD_
	cmpq	$0, -3560(%rbp)
	popq	%rsi
	popq	%rdi
	jne	.L3026
.L2667:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-3872(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	cmpq	$0, -880(%rbp)
	je	.L2669
.L3016:
	movq	-3832(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-176(%rbp), %rbx
	leaq	-944(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$2055, %ecx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movabsq	$362267125614053127, %rax
	pxor	%xmm0, %xmm0
	leaq	-161(%rbp), %rdx
	movw	%cx, -164(%rbp)
	movq	%rax, -176(%rbp)
	movaps	%xmm0, -3568(%rbp)
	movl	$117901316, -168(%rbp)
	movb	$7, -162(%rbp)
	movq	$0, -3552(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3568(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L2670
	call	_ZdlPv@PLT
.L2670:
	movq	(%r12), %rax
	pxor	%xmm0, %xmm0
	movq	16(%rax), %rcx
	movq	(%rax), %r13
	movq	56(%rax), %rdi
	movq	64(%rax), %rsi
	movq	104(%rax), %rdx
	movq	24(%rax), %r11
	movq	%rcx, -3872(%rbp)
	movq	32(%rax), %r10
	movq	40(%rax), %r9
	movq	48(%rax), %r8
	movq	96(%rax), %rcx
	movq	8(%rax), %r12
	movq	112(%rax), %rax
	movq	%r13, -176(%rbp)
	movq	-3872(%rbp), %r13
	movq	%rdi, -120(%rbp)
	movq	%r14, %rdi
	movq	%rsi, -112(%rbp)
	movq	%rbx, %rsi
	movq	%rdx, -96(%rbp)
	leaq	-80(%rbp), %rdx
	movq	%r11, -152(%rbp)
	movq	%r10, -144(%rbp)
	movq	%r9, -136(%rbp)
	movq	%r8, -128(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%rax, -88(%rbp)
	movaps	%xmm0, -3568(%rbp)
	movq	%r12, -168(%rbp)
	movq	%r13, -160(%rbp)
	movq	$0, -3552(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-368(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2671
	call	_ZdlPv@PLT
.L2671:
	movq	-3744(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -688(%rbp)
	je	.L2672
.L3017:
	movq	-3784(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -3696(%rbp)
	leaq	-752(%rbp), %r12
	movq	$0, -3688(%rbp)
	movq	$0, -3680(%rbp)
	movq	$0, -3672(%rbp)
	movq	$0, -3664(%rbp)
	movq	$0, -3656(%rbp)
	movq	$0, -3648(%rbp)
	movq	$0, -3632(%rbp)
	movq	$0, -3616(%rbp)
	movq	$0, -3600(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-3600(%rbp), %rax
	pushq	%rax
	leaq	-3616(%rbp), %rax
	leaq	-3680(%rbp), %rcx
	pushq	%rax
	leaq	-3632(%rbp), %rax
	leaq	-3664(%rbp), %r9
	pushq	%rax
	leaq	-3648(%rbp), %rax
	leaq	-3672(%rbp), %r8
	pushq	%rax
	leaq	-3656(%rbp), %rax
	leaq	-3688(%rbp), %rdx
	pushq	%rax
	leaq	-3696(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSFunctionENS0_10JSReceiverENS0_6ObjectES6_S6_NS0_3MapENS0_8UintPtrTENS0_6Int32TES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_SJ_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESJ_
	addq	$48, %rsp
	movl	$348, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$349, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$80, %edi
	movq	-3616(%rbp), %xmm0
	movq	-3648(%rbp), %xmm1
	movq	-3664(%rbp), %xmm2
	movq	-3680(%rbp), %xmm3
	movq	$0, -3552(%rbp)
	movq	-3696(%rbp), %xmm4
	movhps	-3600(%rbp), %xmm0
	movhps	-3632(%rbp), %xmm1
	movhps	-3656(%rbp), %xmm2
	movhps	-3672(%rbp), %xmm3
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3688(%rbp), %xmm4
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3568(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-160(%rbp), %xmm6
	movdqa	-144(%rbp), %xmm5
	leaq	80(%rax), %rdx
	leaq	-560(%rbp), %rdi
	movq	%rax, -3568(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-128(%rbp), %xmm7
	movups	%xmm6, 16(%rax)
	movdqa	-112(%rbp), %xmm6
	movups	%xmm5, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm6, 64(%rax)
	movq	%rdx, -3552(%rbp)
	movq	%rdx, -3560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2673
	call	_ZdlPv@PLT
.L2673:
	movq	-3736(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -496(%rbp)
	je	.L2674
.L3018:
	movq	-3736(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -3696(%rbp)
	leaq	-560(%rbp), %r12
	movq	$0, -3688(%rbp)
	movq	$0, -3680(%rbp)
	movq	$0, -3672(%rbp)
	movq	$0, -3664(%rbp)
	movq	$0, -3656(%rbp)
	movq	$0, -3648(%rbp)
	movq	$0, -3632(%rbp)
	movq	$0, -3616(%rbp)
	movq	$0, -3600(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-3600(%rbp), %rax
	pushq	%rax
	leaq	-3616(%rbp), %rax
	leaq	-3680(%rbp), %rcx
	pushq	%rax
	leaq	-3632(%rbp), %rax
	leaq	-3664(%rbp), %r9
	pushq	%rax
	leaq	-3648(%rbp), %rax
	leaq	-3672(%rbp), %r8
	pushq	%rax
	leaq	-3656(%rbp), %rax
	leaq	-3688(%rbp), %rdx
	pushq	%rax
	leaq	-3696(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSFunctionENS0_10JSReceiverENS0_6ObjectES6_S6_NS0_3MapENS0_8UintPtrTENS0_6Int32TES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESJ_SJ_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESJ_
	addq	$48, %rsp
	movl	$354, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3720(%rbp), %rbx
	movq	-3632(%rbp), %r8
	movq	-3616(%rbp), %r9
	movq	-3600(%rbp), %rcx
	movq	-3648(%rbp), %rdx
	movq	-3696(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal21ConstructByLength_343EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3MapEEENS4_INS0_6ObjectEEENS0_34TorqueStructTypedArrayElementsInfoE
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -304(%rbp)
	je	.L2675
.L3019:
	movq	-3744(%rbp), %rsi
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	leaq	-368(%rbp), %r8
	xorl	%ebx, %ebx
	movq	%r8, -3872(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-176(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-164(%rbp), %rdx
	movabsq	$362267125614053127, %rax
	movaps	%xmm0, -3568(%rbp)
	movq	%rax, -176(%rbp)
	movl	$117966596, -168(%rbp)
	movq	$0, -3552(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3872(%rbp), %r8
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2676
	movq	%rax, -3872(%rbp)
	call	_ZdlPv@PLT
	movq	-3872(%rbp), %rax
.L2676:
	movq	(%rax), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	(%rax), %rdx
	movq	56(%rax), %xmm7
	movq	72(%rax), %rcx
	movq	80(%rax), %r8
	testq	%rdx, %rdx
	movhps	64(%rax), %xmm7
	cmovne	%rdx, %r12
	movq	48(%rax), %rdx
	movaps	%xmm7, -3872(%rbp)
	movq	%rcx, -3904(%rbp)
	testq	%rdx, %rdx
	movq	%r8, -3888(%rbp)
	cmove	%r13, %rdx
	movq	%rdx, %r13
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	cmovne	%rdx, %rbx
	movl	$359, %edx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$358, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %r9
	pushq	-3864(%rbp)
	movq	-3720(%rbp), %rbx
	pushq	-3872(%rbp)
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	-3888(%rbp), %r8
	movq	%rbx, %rdi
	movq	-3904(%rbp), %rcx
	call	_ZN2v88internal24ConstructByArrayLike_344EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_3MapEEENS4_INS0_10HeapObjectEEENS4_INS0_6ObjectEEENS0_34TorqueStructTypedArrayElementsInfoENS4_INS0_10JSReceiverEEE
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	jmp	.L2675
	.p2align 4,,10
	.p2align 3
.L3021:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-88(%rbp), %rdx
	movq	%rbx, %rsi
	movdqa	-3872(%rbp), %xmm7
	movq	%r12, %rdi
	movq	%r13, -96(%rbp)
	movdqa	-3888(%rbp), %xmm6
	movaps	%xmm0, -3600(%rbp)
	movaps	%xmm7, -176(%rbp)
	movdqa	-3904(%rbp), %xmm7
	movaps	%xmm6, -160(%rbp)
	movdqa	-3952(%rbp), %xmm6
	movaps	%xmm7, -144(%rbp)
	movdqa	-3968(%rbp), %xmm7
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movq	$0, -3584(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3248(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2626
	call	_ZdlPv@PLT
.L2626:
	movq	-3816(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2625
	.p2align 4,,10
	.p2align 3
.L3022:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-3704(%rbp), %rdx
	movq	-3616(%rbp), %rax
	movaps	%xmm0, -3600(%rbp)
	movq	$0, -3584(%rbp)
	movq	%rdx, -176(%rbp)
	movq	-3696(%rbp), %rdx
	movq	%rax, -104(%rbp)
	movq	%rdx, -168(%rbp)
	movq	-3688(%rbp), %rdx
	movq	%rax, -96(%rbp)
	movq	%rdx, -160(%rbp)
	movq	-3680(%rbp), %rdx
	movq	%rdx, -152(%rbp)
	movq	-3672(%rbp), %rdx
	movq	%rdx, -144(%rbp)
	movq	-3664(%rbp), %rdx
	movq	%rdx, -136(%rbp)
	movq	-3656(%rbp), %rdx
	movq	%rdx, -128(%rbp)
	movq	-3648(%rbp), %rdx
	movq	%rdx, -120(%rbp)
	movq	-3632(%rbp), %rdx
	movq	%rdx, -112(%rbp)
	leaq	-88(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2672(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2636
	call	_ZdlPv@PLT
.L2636:
	movq	-3848(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2635
	.p2align 4,,10
	.p2align 3
.L3023:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-3704(%rbp), %rdx
	movq	-3616(%rbp), %rax
	movaps	%xmm0, -3600(%rbp)
	movq	$0, -3584(%rbp)
	movq	%rdx, -176(%rbp)
	movq	-3696(%rbp), %rdx
	movq	%rax, -104(%rbp)
	movq	%rdx, -168(%rbp)
	movq	-3688(%rbp), %rdx
	movq	%rax, -96(%rbp)
	movq	%rdx, -160(%rbp)
	movq	-3680(%rbp), %rdx
	movq	%rdx, -152(%rbp)
	movq	-3672(%rbp), %rdx
	movq	%rdx, -144(%rbp)
	movq	-3664(%rbp), %rdx
	movq	%rdx, -136(%rbp)
	movq	-3656(%rbp), %rdx
	movq	%rdx, -128(%rbp)
	movq	-3648(%rbp), %rdx
	movq	%rdx, -120(%rbp)
	movq	-3632(%rbp), %rdx
	movq	%rdx, -112(%rbp)
	leaq	-88(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2096(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2649
	call	_ZdlPv@PLT
.L2649:
	movq	-3824(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2648
	.p2align 4,,10
	.p2align 3
.L3024:
	movq	-3936(%rbp), %xmm5
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	-3968(%rbp), %xmm4
	movq	-3888(%rbp), %xmm1
	movq	-3984(%rbp), %xmm3
	movq	-4000(%rbp), %xmm2
	movhps	-3952(%rbp), %xmm5
	movq	-3904(%rbp), %xmm0
	movhps	-3976(%rbp), %xmm4
	movhps	-3920(%rbp), %xmm1
	movhps	-3992(%rbp), %xmm3
	movaps	%xmm5, -4032(%rbp)
	movhps	-4008(%rbp), %xmm2
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm1, -4048(%rbp)
	movaps	%xmm4, -3968(%rbp)
	movaps	%xmm3, -3952(%rbp)
	movaps	%xmm2, -3936(%rbp)
	movaps	%xmm0, -3920(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-3872(%rbp), %rdi
	movq	%rax, -3904(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, -3888(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movdqa	-3920(%rbp), %xmm0
	movq	%rbx, %rsi
	movq	-3904(%rbp), %rdx
	leaq	-3600(%rbp), %r8
	movdqa	-4048(%rbp), %xmm1
	movdqa	-4032(%rbp), %xmm5
	movq	$0, -3584(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rax, %xmm0
	movdqa	-3968(%rbp), %xmm4
	movdqa	-3952(%rbp), %xmm3
	movhps	-3888(%rbp), %xmm0
	movdqa	-3936(%rbp), %xmm2
	movq	%r8, %rdi
	movq	%rdx, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	leaq	-56(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r8, -3888(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm0, -3600(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3888(%rbp), %r8
	leaq	-1712(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2655
	call	_ZdlPv@PLT
.L2655:
	movq	-3840(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2654
	.p2align 4,,10
	.p2align 3
.L3025:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-3704(%rbp), %rdx
	movq	-3616(%rbp), %rax
	movaps	%xmm0, -3600(%rbp)
	movq	$0, -3584(%rbp)
	movq	%rdx, -176(%rbp)
	movq	-3696(%rbp), %rdx
	movq	%rax, -104(%rbp)
	movq	%rdx, -168(%rbp)
	movq	-3688(%rbp), %rdx
	movq	%rax, -96(%rbp)
	movq	%rdx, -160(%rbp)
	movq	-3680(%rbp), %rdx
	movq	%rdx, -152(%rbp)
	movq	-3672(%rbp), %rdx
	movq	%rdx, -144(%rbp)
	movq	-3664(%rbp), %rdx
	movq	%rdx, -136(%rbp)
	movq	-3656(%rbp), %rdx
	movq	%rdx, -128(%rbp)
	movq	-3648(%rbp), %rdx
	movq	%rdx, -120(%rbp)
	movq	-3632(%rbp), %rdx
	movq	%rdx, -112(%rbp)
	leaq	-88(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1328(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2662
	call	_ZdlPv@PLT
.L2662:
	movq	-3856(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2661
	.p2align 4,,10
	.p2align 3
.L3026:
	movq	-3936(%rbp), %xmm5
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	-3968(%rbp), %xmm4
	movq	-3888(%rbp), %xmm1
	movq	-3984(%rbp), %xmm3
	movq	-4000(%rbp), %xmm2
	movhps	-3952(%rbp), %xmm5
	movq	-3904(%rbp), %xmm0
	movhps	-3976(%rbp), %xmm4
	movhps	-3920(%rbp), %xmm1
	movhps	-3992(%rbp), %xmm3
	movaps	%xmm5, -4032(%rbp)
	movhps	-4008(%rbp), %xmm2
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm1, -4048(%rbp)
	movaps	%xmm4, -3968(%rbp)
	movaps	%xmm3, -3952(%rbp)
	movaps	%xmm2, -3936(%rbp)
	movaps	%xmm0, -3920(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-3872(%rbp), %rdi
	movq	%rax, -3904(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	%r12, %rdi
	movq	%rax, -3888(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movdqa	-3920(%rbp), %xmm0
	movq	%rbx, %rsi
	movq	-3904(%rbp), %rdx
	leaq	-3600(%rbp), %r8
	movdqa	-4048(%rbp), %xmm1
	movdqa	-4032(%rbp), %xmm5
	movq	$0, -3584(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rax, %xmm0
	movdqa	-3968(%rbp), %xmm4
	movdqa	-3952(%rbp), %xmm3
	movhps	-3888(%rbp), %xmm0
	movdqa	-3936(%rbp), %xmm2
	movq	%r8, %rdi
	movq	%rdx, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	leaq	-56(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r8, -3888(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm0, -3600(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3888(%rbp), %r8
	leaq	-944(%rbp), %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2668
	call	_ZdlPv@PLT
.L2668:
	movq	-3832(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2667
.L3020:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22636:
	.size	_ZN2v88internal25CreateTypedArrayAssembler28GenerateCreateTypedArrayImplEv, .-_ZN2v88internal25CreateTypedArrayAssembler28GenerateCreateTypedArrayImplEv
	.section	.rodata._ZN2v88internal8Builtins25Generate_CreateTypedArrayEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/typed-array-createtypedarray-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins25Generate_CreateTypedArrayEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"CreateTypedArray"
	.section	.text._ZN2v88internal8Builtins25Generate_CreateTypedArrayEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins25Generate_CreateTypedArrayEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins25Generate_CreateTypedArrayEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins25Generate_CreateTypedArrayEPNS0_8compiler18CodeAssemblerStateE:
.LFB22632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$2605, %ecx
	leaq	.LC12(%rip), %rdx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$915, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L3031
.L3028:
	movq	%r13, %rdi
	call	_ZN2v88internal25CreateTypedArrayAssembler28GenerateCreateTypedArrayImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3032
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3031:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L3028
.L3032:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22632:
	.size	_ZN2v88internal8Builtins25Generate_CreateTypedArrayEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins25Generate_CreateTypedArrayEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_6ObjectES5_S5_NS0_10JSFunctionEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EESE_SE_PNS8_IS6_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_6ObjectES5_S5_NS0_10JSFunctionEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EESE_SE_PNS8_IS6_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_6ObjectES5_S5_NS0_10JSFunctionEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EESE_SE_PNS8_IS6_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_6ObjectES5_S5_NS0_10JSFunctionEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EESE_SE_PNS8_IS6_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_6ObjectES5_S5_NS0_10JSFunctionEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EESE_SE_PNS8_IS6_EE:
.LFB27697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$6, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$1800, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$134743815, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3034
	movq	%rax, -104(%rbp)
	call	_ZdlPv@PLT
	movq	-104(%rbp), %rax
.L3034:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L3035
	movq	%rdx, (%r15)
.L3035:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L3036
	movq	%rdx, (%r14)
.L3036:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L3037
	movq	%rdx, 0(%r13)
.L3037:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L3038
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L3038:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L3039
	movq	%rdx, (%rbx)
.L3039:
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L3033
	movq	-96(%rbp), %rbx
	movq	%rax, (%rbx)
.L3033:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3064
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3064:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27697:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_6ObjectES5_S5_NS0_10JSFunctionEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EESE_SE_PNS8_IS6_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_6ObjectES5_S5_NS0_10JSFunctionEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EESE_SE_PNS8_IS6_EE
	.section	.text._ZN2v88internal27TypedArraySpeciesCreate_349EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEPKcNS0_7int31_tENS4_INS0_12JSTypedArrayEEENS4_INS0_6ObjectEEESD_SD_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27TypedArraySpeciesCreate_349EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEPKcNS0_7int31_tENS4_INS0_12JSTypedArrayEEENS4_INS0_6ObjectEEESD_SD_
	.type	_ZN2v88internal27TypedArraySpeciesCreate_349EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEPKcNS0_7int31_tENS4_INS0_12JSTypedArrayEEENS4_INS0_6ObjectEEESD_SD_, @function
_ZN2v88internal27TypedArraySpeciesCreate_349EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEPKcNS0_7int31_tENS4_INS0_12JSTypedArrayEEENS4_INS0_6ObjectEEESD_SD_:
.LFB22681:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-3008(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-3240(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$3448, %rsp
	movq	%r9, -3392(%rbp)
	movq	%rdx, -3376(%rbp)
	movl	%ecx, -3408(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -3240(%rbp)
	movq	%rdi, -3008(%rbp)
	movl	$120, %edi
	movq	$0, -3000(%rbp)
	movq	$0, -2992(%rbp)
	movq	$0, -2984(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -3000(%rbp)
	leaq	-2952(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2984(%rbp)
	movq	%rdx, -2992(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2968(%rbp)
	movq	%rax, -3256(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3240(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2808(%rbp)
	movq	$0, -2800(%rbp)
	movq	%rax, -2816(%rbp)
	movq	$0, -2792(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2808(%rbp)
	leaq	-2760(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2792(%rbp)
	movq	%rdx, -2800(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2776(%rbp)
	movq	%rax, -3304(%rbp)
	movq	$0, -2784(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3240(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2616(%rbp)
	movq	$0, -2608(%rbp)
	movq	%rax, -2624(%rbp)
	movq	$0, -2600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2616(%rbp)
	leaq	-2568(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2600(%rbp)
	movq	%rdx, -2608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2584(%rbp)
	movq	%rax, -3312(%rbp)
	movq	$0, -2592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3240(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2424(%rbp)
	movq	$0, -2416(%rbp)
	movq	%rax, -2432(%rbp)
	movq	$0, -2408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2424(%rbp)
	leaq	-2376(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2408(%rbp)
	movq	%rdx, -2416(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2392(%rbp)
	movq	%rax, -3328(%rbp)
	movq	$0, -2400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3240(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2232(%rbp)
	movq	$0, -2224(%rbp)
	movq	%rax, -2240(%rbp)
	movq	$0, -2216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2232(%rbp)
	leaq	-2184(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2216(%rbp)
	movq	%rdx, -2224(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2200(%rbp)
	movq	%rax, -3296(%rbp)
	movq	$0, -2208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3240(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movq	%rax, -2048(%rbp)
	movq	$0, -2024(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%r12, %rsi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2040(%rbp)
	leaq	-1992(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2024(%rbp)
	movq	%rdx, -2032(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2008(%rbp)
	movq	%rax, -3272(%rbp)
	movq	$0, -2016(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3240(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1848(%rbp)
	movq	$0, -1840(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1832(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1848(%rbp)
	leaq	-1800(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1832(%rbp)
	movq	%rdx, -1840(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1816(%rbp)
	movq	%rax, -3368(%rbp)
	movq	$0, -1824(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3240(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1656(%rbp)
	movq	$0, -1648(%rbp)
	movq	%rax, -1664(%rbp)
	movq	$0, -1640(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1656(%rbp)
	leaq	-1608(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1640(%rbp)
	movq	%rdx, -1648(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1624(%rbp)
	movq	%rax, -3320(%rbp)
	movq	$0, -1632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3240(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1464(%rbp)
	movq	$0, -1456(%rbp)
	movq	%rax, -1472(%rbp)
	movq	$0, -1448(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1464(%rbp)
	leaq	-1416(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1448(%rbp)
	movq	%rdx, -1456(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1432(%rbp)
	movq	%rax, -3344(%rbp)
	movq	$0, -1440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3240(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1272(%rbp)
	movq	$0, -1264(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -1256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1272(%rbp)
	leaq	-1224(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1256(%rbp)
	movq	%rdx, -1264(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1240(%rbp)
	movq	%rax, -3336(%rbp)
	movq	$0, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3240(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1080(%rbp)
	movq	$0, -1072(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$0, -1064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1080(%rbp)
	leaq	-1032(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1064(%rbp)
	movq	%rdx, -1072(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1048(%rbp)
	movq	%rax, -3360(%rbp)
	movq	$0, -1056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3240(%rbp), %rax
	movl	$192, %edi
	movq	$0, -888(%rbp)
	movq	$0, -880(%rbp)
	movq	%rax, -896(%rbp)
	movq	$0, -872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -888(%rbp)
	leaq	-840(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -872(%rbp)
	movq	%rdx, -880(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -856(%rbp)
	movq	%rax, -3352(%rbp)
	movq	$0, -864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3240(%rbp), %rax
	movl	$192, %edi
	movq	$0, -696(%rbp)
	movq	$0, -688(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -696(%rbp)
	leaq	-648(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -680(%rbp)
	movq	%rdx, -688(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -664(%rbp)
	movq	%rax, -3288(%rbp)
	movq	$0, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3240(%rbp), %rax
	movl	$144, %edi
	movq	$0, -504(%rbp)
	movq	$0, -496(%rbp)
	movq	%rax, -512(%rbp)
	movq	$0, -488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -504(%rbp)
	leaq	-456(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -488(%rbp)
	movq	%rdx, -496(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -472(%rbp)
	movq	%rax, -3280(%rbp)
	movq	$0, -480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3240(%rbp), %rax
	movl	$144, %edi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -312(%rbp)
	leaq	-264(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	xorl	%edx, %edx
	movq	%rax, -3264(%rbp)
	movups	%xmm0, -280(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	-3392(%rbp), %r9
	movl	$40, %edi
	movq	%r13, -128(%rbp)
	leaq	-3136(%rbp), %r13
	movq	%rax, -104(%rbp)
	movq	24(%rbp), %rax
	movq	%r9, -112(%rbp)
	movaps	%xmm0, -3136(%rbp)
	movq	%rbx, -120(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -3120(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm4
	leaq	40(%rax), %rdx
	movq	%rax, -3136(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3066
	call	_ZdlPv@PLT
.L3066:
	movq	-3256(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2944(%rbp)
	jne	.L3391
	cmpq	$0, -2752(%rbp)
	jne	.L3392
.L3071:
	cmpq	$0, -2560(%rbp)
	jne	.L3393
.L3073:
	cmpq	$0, -2368(%rbp)
	jne	.L3394
.L3076:
	cmpq	$0, -2176(%rbp)
	jne	.L3395
.L3078:
	cmpq	$0, -1984(%rbp)
	jne	.L3396
.L3080:
	cmpq	$0, -1792(%rbp)
	jne	.L3397
.L3084:
	cmpq	$0, -1600(%rbp)
	jne	.L3398
.L3087:
	cmpq	$0, -1408(%rbp)
	jne	.L3399
.L3090:
	cmpq	$0, -1216(%rbp)
	jne	.L3400
.L3091:
	cmpq	$0, -1024(%rbp)
	jne	.L3401
.L3096:
	cmpq	$0, -832(%rbp)
	jne	.L3402
.L3099:
	cmpq	$0, -640(%rbp)
	jne	.L3403
.L3102:
	cmpq	$0, -448(%rbp)
	leaq	-320(%rbp), %r14
	jne	.L3404
.L3106:
	movq	-3264(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movl	$1800, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134743815, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3136(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L3109
	call	_ZdlPv@PLT
.L3109:
	movq	(%rbx), %rax
	movq	-3264(%rbp), %rdi
	movq	40(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3110
	call	_ZdlPv@PLT
.L3110:
	movq	-304(%rbp), %rbx
	movq	-312(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L3111
	.p2align 4,,10
	.p2align 3
.L3115:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3112
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3115
.L3113:
	movq	-312(%rbp), %r14
.L3111:
	testq	%r14, %r14
	je	.L3116
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3116:
	movq	-3280(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3117
	call	_ZdlPv@PLT
.L3117:
	movq	-496(%rbp), %rbx
	movq	-504(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L3118
	.p2align 4,,10
	.p2align 3
.L3122:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3119
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3122
.L3120:
	movq	-504(%rbp), %r14
.L3118:
	testq	%r14, %r14
	je	.L3123
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3123:
	movq	-3288(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3124
	call	_ZdlPv@PLT
.L3124:
	movq	-688(%rbp), %rbx
	movq	-696(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L3125
	.p2align 4,,10
	.p2align 3
.L3129:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3126
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3129
.L3127:
	movq	-696(%rbp), %r14
.L3125:
	testq	%r14, %r14
	je	.L3130
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3130:
	movq	-3352(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-864(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3131
	call	_ZdlPv@PLT
.L3131:
	movq	-880(%rbp), %rbx
	movq	-888(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L3132
	.p2align 4,,10
	.p2align 3
.L3136:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3133
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3136
.L3134:
	movq	-888(%rbp), %r14
.L3132:
	testq	%r14, %r14
	je	.L3137
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3137:
	movq	-3360(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3138
	call	_ZdlPv@PLT
.L3138:
	movq	-1072(%rbp), %rbx
	movq	-1080(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L3139
	.p2align 4,,10
	.p2align 3
.L3143:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3140
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3143
.L3141:
	movq	-1080(%rbp), %r14
.L3139:
	testq	%r14, %r14
	je	.L3144
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3144:
	movq	-3336(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3145
	call	_ZdlPv@PLT
.L3145:
	movq	-1264(%rbp), %rbx
	movq	-1272(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L3146
	.p2align 4,,10
	.p2align 3
.L3150:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3147
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3150
.L3148:
	movq	-1272(%rbp), %r14
.L3146:
	testq	%r14, %r14
	je	.L3151
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3151:
	movq	-3344(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1440(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3152
	call	_ZdlPv@PLT
.L3152:
	movq	-1456(%rbp), %rbx
	movq	-1464(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L3153
	.p2align 4,,10
	.p2align 3
.L3157:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3154
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3157
.L3155:
	movq	-1464(%rbp), %r14
.L3153:
	testq	%r14, %r14
	je	.L3158
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3158:
	movq	-3320(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1632(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3159
	call	_ZdlPv@PLT
.L3159:
	movq	-1648(%rbp), %rbx
	movq	-1656(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L3160
	.p2align 4,,10
	.p2align 3
.L3164:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3161
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3164
.L3162:
	movq	-1656(%rbp), %r14
.L3160:
	testq	%r14, %r14
	je	.L3165
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3165:
	movq	-3368(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3166
	call	_ZdlPv@PLT
.L3166:
	movq	-1840(%rbp), %rbx
	movq	-1848(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L3167
	.p2align 4,,10
	.p2align 3
.L3171:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3168
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3171
.L3169:
	movq	-1848(%rbp), %r14
.L3167:
	testq	%r14, %r14
	je	.L3172
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3172:
	movq	-3272(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2016(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3173
	call	_ZdlPv@PLT
.L3173:
	movq	-2032(%rbp), %rbx
	movq	-2040(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L3174
	.p2align 4,,10
	.p2align 3
.L3178:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3175
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3178
.L3176:
	movq	-2040(%rbp), %r14
.L3174:
	testq	%r14, %r14
	je	.L3179
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3179:
	movq	-3296(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3180
	call	_ZdlPv@PLT
.L3180:
	movq	-2224(%rbp), %rbx
	movq	-2232(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L3181
	.p2align 4,,10
	.p2align 3
.L3185:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3182
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3185
.L3183:
	movq	-2232(%rbp), %r14
.L3181:
	testq	%r14, %r14
	je	.L3186
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3186:
	movq	-3328(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3187
	call	_ZdlPv@PLT
.L3187:
	movq	-2416(%rbp), %rbx
	movq	-2424(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L3188
	.p2align 4,,10
	.p2align 3
.L3192:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3189
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3192
.L3190:
	movq	-2424(%rbp), %r14
.L3188:
	testq	%r14, %r14
	je	.L3193
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3193:
	movq	-3312(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3194
	call	_ZdlPv@PLT
.L3194:
	movq	-2608(%rbp), %rbx
	movq	-2616(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L3195
	.p2align 4,,10
	.p2align 3
.L3199:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3196
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3199
.L3197:
	movq	-2616(%rbp), %r14
.L3195:
	testq	%r14, %r14
	je	.L3200
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3200:
	movq	-3304(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3201
	call	_ZdlPv@PLT
.L3201:
	movq	-2800(%rbp), %rbx
	movq	-2808(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L3202
	.p2align 4,,10
	.p2align 3
.L3206:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3203
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3206
.L3204:
	movq	-2808(%rbp), %r14
.L3202:
	testq	%r14, %r14
	je	.L3207
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3207:
	movq	-3256(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2976(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3208
	call	_ZdlPv@PLT
.L3208:
	movq	-2992(%rbp), %rbx
	movq	-3000(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L3209
	.p2align 4,,10
	.p2align 3
.L3213:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3210
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%rbx, %r14
	jne	.L3213
.L3211:
	movq	-3000(%rbp), %r14
.L3209:
	testq	%r14, %r14
	je	.L3214
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3214:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3405
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3210:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3213
	jmp	.L3211
	.p2align 4,,10
	.p2align 3
.L3203:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3206
	jmp	.L3204
	.p2align 4,,10
	.p2align 3
.L3196:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3199
	jmp	.L3197
	.p2align 4,,10
	.p2align 3
.L3189:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3192
	jmp	.L3190
	.p2align 4,,10
	.p2align 3
.L3182:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3185
	jmp	.L3183
	.p2align 4,,10
	.p2align 3
.L3175:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3178
	jmp	.L3176
	.p2align 4,,10
	.p2align 3
.L3168:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3171
	jmp	.L3169
	.p2align 4,,10
	.p2align 3
.L3161:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3164
	jmp	.L3162
	.p2align 4,,10
	.p2align 3
.L3154:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3157
	jmp	.L3155
	.p2align 4,,10
	.p2align 3
.L3147:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3150
	jmp	.L3148
	.p2align 4,,10
	.p2align 3
.L3140:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3143
	jmp	.L3141
	.p2align 4,,10
	.p2align 3
.L3133:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3136
	jmp	.L3134
	.p2align 4,,10
	.p2align 3
.L3126:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3129
	jmp	.L3127
	.p2align 4,,10
	.p2align 3
.L3112:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3115
	jmp	.L3113
	.p2align 4,,10
	.p2align 3
.L3119:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3122
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L3391:
	movq	-3256(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134743815, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3136(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L3068
	call	_ZdlPv@PLT
.L3068:
	movq	(%rbx), %rax
	movl	$367, %edx
	movq	%r12, %rdi
	movq	24(%rax), %rsi
	movq	16(%rax), %rcx
	movq	(%rax), %rbx
	movq	8(%rax), %r14
	movq	32(%rax), %rax
	movq	%rsi, -3456(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -3440(%rbp)
	movq	%rax, -3424(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal27TypedArrayBuiltinsAssembler21GetDefaultConstructorENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_12JSTypedArrayEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -3464(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$370, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -3392(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-3392(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE(%rip), %esi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -3392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3392(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler30IsPrototypeTypedArrayPrototypeENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_3MapEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -3392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3392(%rbp), %r8
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler15Word32BinaryNotENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -3392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %xmm3
	movq	%rbx, %xmm7
	movq	-3424(%rbp), %xmm5
	punpcklqdq	%xmm3, %xmm7
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movq	-3440(%rbp), %xmm6
	movhps	-3464(%rbp), %xmm5
	movaps	%xmm7, -128(%rbp)
	movhps	-3456(%rbp), %xmm6
	movaps	%xmm5, -3424(%rbp)
	movaps	%xmm6, -3440(%rbp)
	movaps	%xmm7, -3456(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm0, -3136(%rbp)
	movq	$0, -3120(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	48(%rax), %rdx
	leaq	-2816(%rbp), %rdi
	movq	%rax, -3136(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3069
	call	_ZdlPv@PLT
.L3069:
	movdqa	-3456(%rbp), %xmm5
	movdqa	-3440(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movdqa	-3424(%rbp), %xmm7
	movaps	%xmm0, -3136(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	$0, -3120(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	48(%rax), %rdx
	leaq	-2624(%rbp), %rdi
	movq	%rax, -3136(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3070
	call	_ZdlPv@PLT
.L3070:
	movq	-3312(%rbp), %rcx
	movq	-3304(%rbp), %rdx
	movq	%r12, %rdi
	movq	-3392(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2752(%rbp)
	je	.L3071
.L3392:
	movq	-3304(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3216(%rbp)
	leaq	-2816(%rbp), %r14
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	$0, -3192(%rbp)
	movq	$0, -3184(%rbp)
	movq	$0, -3168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r14, %rdi
	leaq	-3168(%rbp), %rax
	pushq	%rax
	leaq	-3200(%rbp), %rcx
	leaq	-3208(%rbp), %rdx
	leaq	-3216(%rbp), %rsi
	leaq	-3184(%rbp), %r9
	leaq	-3192(%rbp), %r8
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_6ObjectES5_S5_NS0_10JSFunctionEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EESE_SE_PNS8_IS6_EE
	movl	$48, %edi
	movq	-3184(%rbp), %xmm0
	movq	-3200(%rbp), %xmm1
	movq	-3216(%rbp), %xmm2
	movq	$0, -3120(%rbp)
	movhps	-3168(%rbp), %xmm0
	movhps	-3192(%rbp), %xmm1
	movhps	-3208(%rbp), %xmm2
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	48(%rax), %rdx
	leaq	-2048(%rbp), %rdi
	movq	%rax, -3136(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	popq	%r14
	popq	%rax
	testq	%rdi, %rdi
	je	.L3072
	call	_ZdlPv@PLT
.L3072:
	movq	-3272(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2560(%rbp)
	je	.L3073
.L3393:
	movq	-3312(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3216(%rbp)
	leaq	-2624(%rbp), %r14
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	$0, -3192(%rbp)
	movq	$0, -3184(%rbp)
	movq	$0, -3168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r14, %rdi
	leaq	-3168(%rbp), %rax
	pushq	%rax
	leaq	-3200(%rbp), %rcx
	leaq	-3184(%rbp), %r9
	leaq	-3192(%rbp), %r8
	leaq	-3208(%rbp), %rdx
	leaq	-3216(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_6ObjectES5_S5_NS0_10JSFunctionEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EESE_SE_PNS8_IS6_EE
	movl	$371, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler39IsTypedArraySpeciesProtectorCellInvalidEv@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movq	-3184(%rbp), %xmm3
	movq	-3200(%rbp), %xmm4
	movq	-3216(%rbp), %xmm2
	movaps	%xmm0, -3136(%rbp)
	movhps	-3168(%rbp), %xmm3
	movq	$0, -3120(%rbp)
	movhps	-3192(%rbp), %xmm4
	movhps	-3208(%rbp), %xmm2
	movaps	%xmm3, -3392(%rbp)
	movaps	%xmm4, -3424(%rbp)
	movaps	%xmm2, -3440(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	leaq	48(%rax), %rdx
	leaq	-2432(%rbp), %rdi
	movq	%rax, -3136(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	popq	%r11
	popq	%rbx
	testq	%rdi, %rdi
	je	.L3074
	call	_ZdlPv@PLT
.L3074:
	movdqa	-3440(%rbp), %xmm7
	movdqa	-3424(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movdqa	-3392(%rbp), %xmm4
	movaps	%xmm0, -3136(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movq	$0, -3120(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	48(%rax), %rdx
	leaq	-2240(%rbp), %rdi
	movq	%rax, -3136(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3075
	call	_ZdlPv@PLT
.L3075:
	movq	-3296(%rbp), %rcx
	movq	-3328(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2368(%rbp)
	je	.L3076
.L3394:
	movq	-3328(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3216(%rbp)
	leaq	-2432(%rbp), %r14
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	$0, -3192(%rbp)
	movq	$0, -3184(%rbp)
	movq	$0, -3168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r14, %rdi
	leaq	-3168(%rbp), %rax
	pushq	%rax
	leaq	-3184(%rbp), %r9
	leaq	-3200(%rbp), %rcx
	leaq	-3208(%rbp), %rdx
	leaq	-3216(%rbp), %rsi
	leaq	-3192(%rbp), %r8
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_6ObjectES5_S5_NS0_10JSFunctionEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EESE_SE_PNS8_IS6_EE
	movl	$48, %edi
	movq	-3184(%rbp), %xmm0
	movq	-3200(%rbp), %xmm1
	movq	-3216(%rbp), %xmm2
	movq	$0, -3120(%rbp)
	movhps	-3168(%rbp), %xmm0
	movhps	-3192(%rbp), %xmm1
	movhps	-3208(%rbp), %xmm2
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	leaq	48(%rax), %rdx
	leaq	-2048(%rbp), %rdi
	movq	%rax, -3136(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	popq	%r9
	popq	%r10
	testq	%rdi, %rdi
	je	.L3077
	call	_ZdlPv@PLT
.L3077:
	movq	-3272(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2176(%rbp)
	je	.L3078
.L3395:
	movq	-3296(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3232(%rbp)
	leaq	-2240(%rbp), %r14
	movq	$0, -3224(%rbp)
	movq	$0, -3216(%rbp)
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	$0, -3192(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r14, %rdi
	leaq	-3192(%rbp), %rax
	pushq	%rax
	leaq	-3208(%rbp), %r8
	leaq	-3216(%rbp), %rcx
	leaq	-3200(%rbp), %r9
	leaq	-3224(%rbp), %rdx
	leaq	-3232(%rbp), %rsi
	leaq	-3184(%rbp), %r14
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_6ObjectES5_S5_NS0_10JSFunctionEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EESE_SE_PNS8_IS6_EE
	movl	$374, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$373, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3232(%rbp), %r9
	movq	%r14, %rdi
	movq	-3216(%rbp), %rcx
	movq	-3208(%rbp), %rsi
	movq	-3192(%rbp), %rax
	movq	%r9, -3456(%rbp)
	movq	-3200(%rbp), %rbx
	movq	%rcx, -3440(%rbp)
	movq	%rsi, -3424(%rbp)
	movq	%rax, -3392(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$915, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-3136(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-128(%rbp), %r10
	movq	%rbx, -96(%rbp)
	movq	%r14, %rdi
	movl	$5, %ebx
	movq	%rax, %r8
	movq	-3392(%rbp), %xmm0
	movq	-3456(%rbp), %r9
	pushq	%rbx
	movq	-3120(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rsi
	movl	$1, %ecx
	punpcklqdq	%xmm0, %xmm0
	pushq	%r10
	leaq	-3168(%rbp), %rdx
	movaps	%xmm0, -128(%rbp)
	movq	-3440(%rbp), %xmm0
	movq	%rsi, -3168(%rbp)
	xorl	%esi, %esi
	movhps	-3424(%rbp), %xmm0
	movq	%r10, -3392(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -3160(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$380, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-80(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rbx, -88(%rbp)
	movq	-3216(%rbp), %xmm0
	movq	-3392(%rbp), %r10
	movq	$0, -3120(%rbp)
	movq	-3232(%rbp), %xmm1
	movq	-3200(%rbp), %rax
	movhps	-3208(%rbp), %xmm0
	movq	%r10, %rsi
	movhps	-3224(%rbp), %xmm1
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-512(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3079
	call	_ZdlPv@PLT
.L3079:
	movq	-3280(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1984(%rbp)
	je	.L3080
.L3396:
	movq	-3272(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3224(%rbp)
	leaq	-2048(%rbp), %r14
	movq	$0, -3216(%rbp)
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	$0, -3192(%rbp)
	movq	$0, -3184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r14, %rdi
	leaq	-3184(%rbp), %rax
	pushq	%rax
	leaq	-3192(%rbp), %r9
	leaq	-3200(%rbp), %r8
	leaq	-3208(%rbp), %rcx
	leaq	-3216(%rbp), %rdx
	leaq	-3224(%rbp), %rsi
	leaq	-3168(%rbp), %r14
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_6ObjectES5_S5_NS0_10JSFunctionEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EESE_SE_PNS8_IS6_EE
	movl	$384, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3184(%rbp), %rcx
	movq	-3216(%rbp), %rdx
	movq	%r13, %rdi
	movq	-3224(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler18SpeciesConstructorENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_10JSReceiverEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal23Cast13ATConstructor_127EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movl	$64, %edi
	movq	%rbx, -80(%rbp)
	movq	-3192(%rbp), %xmm0
	movq	-3208(%rbp), %xmm1
	movq	%rax, -72(%rbp)
	movq	-3224(%rbp), %xmm2
	movhps	-3184(%rbp), %xmm0
	movq	$0, -3152(%rbp)
	movhps	-3200(%rbp), %xmm1
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3216(%rbp), %xmm2
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm0, -3168(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	leaq	64(%rax), %rdx
	leaq	-1664(%rbp), %rdi
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm4, 48(%rax)
	movq	%rax, -3168(%rbp)
	movq	%rdx, -3152(%rbp)
	movq	%rdx, -3160(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3168(%rbp), %rdi
	popq	%rsi
	popq	%r8
	testq	%rdi, %rdi
	je	.L3081
	call	_ZdlPv@PLT
.L3081:
	movq	-3320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3128(%rbp)
	jne	.L3406
.L3082:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1792(%rbp)
	je	.L3084
.L3397:
	movq	-3368(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1856(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movl	$1800, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134743815, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3136(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L3085
	call	_ZdlPv@PLT
.L3085:
	movq	(%rbx), %rax
	movl	$48, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movdqu	(%rax), %xmm5
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm3
	leaq	48(%rax), %rdx
	leaq	-1472(%rbp), %rdi
	movq	%rax, -3136(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm3, 32(%rax)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3086
	call	_ZdlPv@PLT
.L3086:
	movq	-3344(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1600(%rbp)
	je	.L3087
.L3398:
	movq	-3320(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1664(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$506381214178346759, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3136(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L3088
	call	_ZdlPv@PLT
.L3088:
	movq	(%rbx), %rax
	movl	$56, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movq	56(%rax), %rdx
	movdqu	(%rax), %xmm4
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -80(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	56(%rax), %rdx
	leaq	-1280(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3089
	call	_ZdlPv@PLT
.L3089:
	movq	-3336(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1408(%rbp)
	je	.L3090
.L3399:
	movq	-3344(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3216(%rbp)
	leaq	-1472(%rbp), %r14
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	$0, -3192(%rbp)
	movq	$0, -3184(%rbp)
	movq	$0, -3168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r14, %rdi
	leaq	-3168(%rbp), %rax
	pushq	%rax
	leaq	-3200(%rbp), %rcx
	leaq	-3184(%rbp), %r9
	leaq	-3192(%rbp), %r8
	leaq	-3208(%rbp), %rdx
	leaq	-3216(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_6ObjectES5_S5_NS0_10JSFunctionEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EESE_SE_PNS8_IS6_EE
	movl	$385, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -1216(%rbp)
	popq	%r14
	popq	%rax
	je	.L3091
.L3400:
	movq	-3336(%rbp), %rsi
	movq	%r12, %rdi
	movl	$1800, %ebx
	leaq	-1280(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%bx, 4(%rax)
	leaq	7(%rax), %rdx
	movl	$134743815, (%rax)
	movb	$7, 6(%rax)
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3136(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L3092
	call	_ZdlPv@PLT
.L3092:
	movq	(%rbx), %rax
	movl	$389, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	16(%rax), %r14
	movq	48(%rax), %rbx
	movq	%rcx, -3392(%rbp)
	movq	8(%rax), %rcx
	movq	%rcx, -3424(%rbp)
	movq	24(%rax), %rcx
	movq	%rcx, -3440(%rbp)
	movq	32(%rax), %rcx
	movq	%rcx, -3456(%rbp)
	movq	40(%rax), %rcx
	movq	%rcx, -3464(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$390, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -3472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpl	$1, -3408(%rbp)
	movq	-3392(%rbp), %xmm0
	movq	$0, -3120(%rbp)
	movhps	-3424(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%r14, %xmm0
	movhps	-3440(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-3456(%rbp), %xmm0
	movhps	-3464(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	movhps	-3472(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3136(%rbp)
	je	.L3407
	leaq	-128(%rbp), %rsi
	leaq	-64(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-896(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3095
	call	_ZdlPv@PLT
.L3095:
	movq	-3352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1024(%rbp)
	je	.L3096
.L3401:
	movq	-3360(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1088(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$578438808216274695, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3136(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L3097
	call	_ZdlPv@PLT
.L3097:
	movq	(%rbx), %rax
	leaq	-3184(%rbp), %r14
	movq	%r12, %rdi
	movq	24(%rax), %rsi
	movq	8(%rax), %rcx
	movq	40(%rax), %rdx
	movq	(%rax), %rbx
	movq	%rsi, -3440(%rbp)
	movq	32(%rax), %rsi
	movq	%rcx, -3424(%rbp)
	movq	16(%rax), %rcx
	movq	48(%rax), %rax
	movq	%rsi, -3456(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -3464(%rbp)
	movl	$391, %edx
	movq	%rcx, -3392(%rbp)
	movq	%rax, -3408(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory9ConstructEPNS0_7IsolateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$4, %esi
	movq	%r14, %rdi
	movq	%rax, -3472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler8LoadRootENS0_9RootIndexE@PLT
	movq	-3136(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -3480(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-128(%rbp), %rsi
	movl	$5, %edi
	movq	%rbx, %r9
	movq	-3408(%rbp), %xmm0
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rsi
	movq	-3392(%rbp), %rcx
	xorl	%esi, %esi
	movq	%r14, %rdi
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -3168(%rbp)
	movq	-3120(%rbp), %rax
	leaq	-3168(%rbp), %rdx
	movaps	%xmm0, -128(%rbp)
	movq	-3472(%rbp), %xmm0
	movq	%rcx, -96(%rbp)
	movl	$1, %ecx
	movhps	-3480(%rbp), %xmm0
	movq	%rax, -3160(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r14, %rdi
	movq	%rax, -3472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$390, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$64, %edi
	movq	$0, -3120(%rbp)
	movhps	-3424(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-3392(%rbp), %xmm0
	movhps	-3440(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-3456(%rbp), %xmm0
	movhps	-3464(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-3408(%rbp), %xmm0
	movhps	-3472(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm6
	leaq	64(%rax), %rdx
	leaq	-704(%rbp), %rdi
	movups	%xmm4, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	popq	%r10
	popq	%r11
	testq	%rdi, %rdi
	je	.L3098
	call	_ZdlPv@PLT
.L3098:
	movq	-3288(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -832(%rbp)
	je	.L3099
.L3402:
	movq	-3352(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-896(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$578438808216274695, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3136(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L3100
	call	_ZdlPv@PLT
.L3100:
	movq	(%rbx), %rax
	movl	$394, %edx
	leaq	-3184(%rbp), %r14
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	24(%rax), %rsi
	movq	(%rax), %rbx
	movq	%rcx, -3424(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -3456(%rbp)
	movq	40(%rax), %rsi
	movq	%rcx, -3440(%rbp)
	movq	32(%rax), %rcx
	movq	48(%rax), %rax
	movq	%rsi, -3464(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -3392(%rbp)
	movq	%rax, -3408(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory9ConstructEPNS0_7IsolateE@PLT
	movl	$3, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movl	$4, %esi
	movq	%r14, %rdi
	movq	%rax, -3472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler8LoadRootENS0_9RootIndexE@PLT
	movq	-3136(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -3480(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-128(%rbp), %rsi
	movl	$7, %edi
	movq	%rbx, %r9
	movq	-3408(%rbp), %xmm1
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rsi
	movq	-3392(%rbp), %rcx
	xorl	%esi, %esi
	movq	%r14, %rdi
	punpcklqdq	%xmm1, %xmm1
	movq	%rax, -3168(%rbp)
	movq	-3440(%rbp), %xmm0
	leaq	-3168(%rbp), %rdx
	movaps	%xmm1, -128(%rbp)
	movq	-3120(%rbp), %rax
	movq	-3472(%rbp), %xmm1
	movhps	-3456(%rbp), %xmm0
	movq	%rcx, -80(%rbp)
	movl	$1, %ecx
	movhps	-3480(%rbp), %xmm1
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3456(%rbp)
	movq	%rax, -3160(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r14, %rdi
	movq	%rax, -3440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$390, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm1
	movl	$64, %edi
	movdqa	-3456(%rbp), %xmm0
	movhps	-3424(%rbp), %xmm1
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	-3392(%rbp), %xmm0
	movaps	%xmm1, -128(%rbp)
	movhps	-3464(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-3408(%rbp), %xmm0
	movhps	-3440(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm5
	leaq	64(%rax), %rdx
	leaq	-704(%rbp), %rdi
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	popq	%r8
	popq	%r9
	testq	%rdi, %rdi
	je	.L3101
	call	_ZdlPv@PLT
.L3101:
	movq	-3288(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -640(%rbp)
	je	.L3102
.L3403:
	movq	-3288(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-704(%rbp), %rbx
	xorl	%r14d, %r14d
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movabsq	$578438808216274695, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3136(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L3103
	call	_ZdlPv@PLT
.L3103:
	movq	(%rbx), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r8
	movq	56(%rax), %rdx
	movq	16(%rax), %xmm0
	movq	32(%rax), %rbx
	testq	%rdx, %rdx
	movq	%r8, %xmm1
	movq	%r8, -3392(%rbp)
	cmovne	%rdx, %r14
	movhps	8(%rax), %xmm1
	movhps	24(%rax), %xmm0
	movl	$397, %edx
	movaps	%xmm1, -3424(%rbp)
	movaps	%xmm0, -3408(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3392(%rbp), %r8
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	-3376(%rbp), %rcx
	movq	%r8, %rsi
	call	_ZN2v88internal27TypedArrayBuiltinsAssembler18ValidateTypedArrayENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEEPKc@PLT
	movq	%r13, %rdi
	movq	%rax, -3392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movdqa	-3408(%rbp), %xmm0
	movl	$48, %edi
	movdqa	-3424(%rbp), %xmm1
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	%rbx, %xmm0
	movhps	-3392(%rbp), %xmm0
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm4
	leaq	48(%rax), %rdx
	leaq	-512(%rbp), %rdi
	movq	%rax, -3136(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3105
	call	_ZdlPv@PLT
.L3105:
	movq	-3280(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L3102
	.p2align 4,,10
	.p2align 3
.L3404:
	movq	-3280(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-512(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -3120(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movl	$1800, %esi
	movq	%r14, %rdi
	movw	%si, 4(%rax)
	leaq	6(%rax), %rdx
	movq	%r13, %rsi
	movl	$134743815, (%rax)
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3136(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L3107
	call	_ZdlPv@PLT
.L3107:
	movq	(%rbx), %rax
	movl	$363, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	16(%rax), %r14
	movq	(%rax), %r15
	movq	32(%rax), %rbx
	movq	%rcx, -3392(%rbp)
	movq	24(%rax), %rcx
	movq	40(%rax), %rax
	movq	%rcx, -3376(%rbp)
	movq	%rax, -3408(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %xmm0
	movl	$48, %edi
	movq	$0, -3120(%rbp)
	movhps	-3392(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%r14, %xmm0
	leaq	-320(%rbp), %r14
	movhps	-3376(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%rbx, %xmm0
	movhps	-3408(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3136(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm3
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm5
	leaq	48(%rax), %rdx
	movq	%rax, -3136(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3108
	call	_ZdlPv@PLT
.L3108:
	movq	-3264(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L3106
	.p2align 4,,10
	.p2align 3
.L3407:
	movl	$64, %edi
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm3
	leaq	64(%rax), %rdx
	leaq	-1088(%rbp), %rdi
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rax, -3136(%rbp)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3094
	call	_ZdlPv@PLT
.L3094:
	movq	-3360(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L3091
	.p2align 4,,10
	.p2align 3
.L3406:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-128(%rbp), %rsi
	leaq	-72(%rbp), %rdx
	movq	%r14, %rdi
	movq	-3192(%rbp), %xmm0
	movq	%rbx, -80(%rbp)
	movq	-3208(%rbp), %xmm1
	movq	-3224(%rbp), %xmm2
	movq	$0, -3152(%rbp)
	movhps	-3184(%rbp), %xmm0
	movhps	-3200(%rbp), %xmm1
	movhps	-3216(%rbp), %xmm2
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3168(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1856(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3083
	call	_ZdlPv@PLT
.L3083:
	movq	-3368(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L3082
.L3405:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22681:
	.size	_ZN2v88internal27TypedArraySpeciesCreate_349EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEPKcNS0_7int31_tENS4_INS0_12JSTypedArrayEEENS4_INS0_6ObjectEEESD_SD_, .-_ZN2v88internal27TypedArraySpeciesCreate_349EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEPKcNS0_7int31_tENS4_INS0_12JSTypedArrayEEENS4_INS0_6ObjectEEESD_SD_
	.section	.text._ZN2v88internal35TypedArraySpeciesCreateByLength_350EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEPKcNS4_INS0_12JSTypedArrayEEENS4_INS0_3SmiEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal35TypedArraySpeciesCreateByLength_350EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEPKcNS4_INS0_12JSTypedArrayEEENS4_INS0_3SmiEEE
	.type	_ZN2v88internal35TypedArraySpeciesCreateByLength_350EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEPKcNS4_INS0_12JSTypedArrayEEENS4_INS0_3SmiEEE, @function
_ZN2v88internal35TypedArraySpeciesCreateByLength_350EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEPKcNS4_INS0_12JSTypedArrayEEENS4_INS0_3SmiEEE:
.LFB22700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-1000(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1096(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1056(%rbp), %rbx
	subq	$1160, %rsp
	movq	%rsi, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	movq	%rcx, -1152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1096(%rbp)
	movq	%rdi, -1056(%rbp)
	movl	$72, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1048(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1096(%rbp), %rax
	movl	$96, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%r12, %rsi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1128(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1096(%rbp), %rax
	movl	$96, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1120(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1096(%rbp), %rax
	movl	$96, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -1136(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1096(%rbp), %rax
	movl	$96, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -280(%rbp)
	leaq	-232(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -1112(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1168(%rbp), %r10
	pxor	%xmm0, %xmm0
	movq	-1152(%rbp), %r9
	movl	$24, %edi
	movq	%r13, -80(%rbp)
	leaq	-1088(%rbp), %r13
	movq	%r10, -96(%rbp)
	movq	%r9, -88(%rbp)
	movaps	%xmm0, -1088(%rbp)
	movq	$0, -1072(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm1
	leaq	24(%rax), %rdx
	movq	%rax, -1088(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm1, (%rax)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3409
	call	_ZdlPv@PLT
.L3409:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -992(%rbp)
	jne	.L3521
	cmpq	$0, -800(%rbp)
	jne	.L3522
.L3414:
	cmpq	$0, -608(%rbp)
	jne	.L3523
.L3416:
	cmpq	$0, -416(%rbp)
	leaq	-288(%rbp), %r14
	jne	.L3524
.L3419:
	movq	-1112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1072(%rbp)
	movaps	%xmm0, -1088(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117835527, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1088(%rbp)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1088(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L3422
	call	_ZdlPv@PLT
.L3422:
	movq	(%rbx), %rax
	movq	-1112(%rbp), %rdi
	movq	24(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3423
	call	_ZdlPv@PLT
.L3423:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L3424
	.p2align 4,,10
	.p2align 3
.L3428:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3425
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3428
.L3426:
	movq	-280(%rbp), %r14
.L3424:
	testq	%r14, %r14
	je	.L3429
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3429:
	movq	-1136(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3430
	call	_ZdlPv@PLT
.L3430:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L3431
	.p2align 4,,10
	.p2align 3
.L3435:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3432
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3435
.L3433:
	movq	-472(%rbp), %r14
.L3431:
	testq	%r14, %r14
	je	.L3436
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3436:
	movq	-1120(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3437
	call	_ZdlPv@PLT
.L3437:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L3438
	.p2align 4,,10
	.p2align 3
.L3442:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3439
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3442
.L3440:
	movq	-664(%rbp), %r14
.L3438:
	testq	%r14, %r14
	je	.L3443
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3443:
	movq	-1128(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3444
	call	_ZdlPv@PLT
.L3444:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L3445
	.p2align 4,,10
	.p2align 3
.L3449:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3446
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3449
.L3447:
	movq	-856(%rbp), %r14
.L3445:
	testq	%r14, %r14
	je	.L3450
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3450:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3451
	call	_ZdlPv@PLT
.L3451:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L3452
	.p2align 4,,10
	.p2align 3
.L3456:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3453
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3456
.L3454:
	movq	-1048(%rbp), %r14
.L3452:
	testq	%r14, %r14
	je	.L3457
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3457:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3525
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3453:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3456
	jmp	.L3454
	.p2align 4,,10
	.p2align 3
.L3446:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3449
	jmp	.L3447
	.p2align 4,,10
	.p2align 3
.L3439:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3442
	jmp	.L3440
	.p2align 4,,10
	.p2align 3
.L3425:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3428
	jmp	.L3426
	.p2align 4,,10
	.p2align 3
.L3432:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L3435
	jmp	.L3433
	.p2align 4,,10
	.p2align 3
.L3521:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1072(%rbp)
	movaps	%xmm0, -1088(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$6, 2(%rax)
	movq	%rax, -1088(%rbp)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1088(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L3411
	call	_ZdlPv@PLT
.L3411:
	movq	(%rbx), %rax
	movl	$408, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rax
	movq	%rsi, -1184(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -1168(%rbp)
	movq	%rax, -1152(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$407, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -1192(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1192(%rbp), %rax
	movq	%r14, %rdi
	movq	-1152(%rbp), %r9
	movq	-1184(%rbp), %r8
	movq	-1176(%rbp), %rdx
	movl	$1, %ecx
	pushq	%rax
	movq	-1168(%rbp), %rsi
	pushq	%rbx
	call	_ZN2v88internal27TypedArraySpeciesCreate_349EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEPKcNS0_7int31_tENS4_INS0_12JSTypedArrayEEENS4_INS0_6ObjectEEESD_SD_
	movl	$409, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -1176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-1176(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1152(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal36Convert9ATuintptr13ATPositiveSmi_187EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -1192(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1192(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-1152(%rbp), %xmm2
	movq	-1168(%rbp), %xmm3
	movaps	%xmm0, -1088(%rbp)
	movhps	-1176(%rbp), %xmm2
	movq	$0, -1072(%rbp)
	movhps	-1184(%rbp), %xmm3
	movaps	%xmm2, -1152(%rbp)
	movaps	%xmm3, -1168(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm2, -80(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -1088(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1088(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L3412
	call	_ZdlPv@PLT
.L3412:
	movdqa	-1168(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-1152(%rbp), %xmm7
	movaps	%xmm0, -1088(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	movq	$0, -1072(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -1088(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3413
	call	_ZdlPv@PLT
.L3413:
	movq	-1120(%rbp), %rcx
	movq	-1128(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -800(%rbp)
	je	.L3414
.L3522:
	movq	-1128(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1072(%rbp)
	movaps	%xmm0, -1088(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$117835527, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1088(%rbp)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1088(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L3415
	call	_ZdlPv@PLT
.L3415:
	movq	(%rbx), %rax
	movl	$410, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$311, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -608(%rbp)
	je	.L3416
.L3523:
	movq	-1120(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1072(%rbp)
	movaps	%xmm0, -1088(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117835527, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1088(%rbp)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1088(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L3417
	call	_ZdlPv@PLT
.L3417:
	movq	(%rbx), %rax
	movl	$413, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	16(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rcx, -1152(%rbp)
	movq	%rax, -1168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %xmm0
	movl	$32, %edi
	movq	$0, -1072(%rbp)
	movhps	-1152(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	movhps	-1168(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1088(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -1088(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3418
	call	_ZdlPv@PLT
.L3418:
	movq	-1136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3524:
	movq	-1136(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1072(%rbp)
	movaps	%xmm0, -1088(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117835527, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1088(%rbp)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1088(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L3420
	call	_ZdlPv@PLT
.L3420:
	movq	(%rbx), %rax
	movl	$401, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	16(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rcx, -1152(%rbp)
	movq	%rax, -1168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %xmm0
	movl	$32, %edi
	movq	$0, -1072(%rbp)
	movhps	-1152(%rbp), %xmm0
	leaq	-288(%rbp), %r14
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	movhps	-1168(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1088(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-80(%rbp), %xmm5
	leaq	32(%rax), %rdx
	movq	%rax, -1088(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3421
	call	_ZdlPv@PLT
.L3421:
	movq	-1112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L3419
.L3525:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22700:
	.size	_ZN2v88internal35TypedArraySpeciesCreateByLength_350EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEPKcNS4_INS0_12JSTypedArrayEEENS4_INS0_3SmiEEE, .-_ZN2v88internal35TypedArraySpeciesCreateByLength_350EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEPKcNS4_INS0_12JSTypedArrayEEENS4_INS0_3SmiEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal22AllocateTypedArray_341EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEbNS4_INS0_3MapEEENS4_INS0_13JSArrayBufferEEENS4_INS0_8UintPtrTEEESC_SC_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal22AllocateTypedArray_341EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEbNS4_INS0_3MapEEENS4_INS0_13JSArrayBufferEEENS4_INS0_8UintPtrTEEESC_SC_, @function
_GLOBAL__sub_I__ZN2v88internal22AllocateTypedArray_341EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEbNS4_INS0_3MapEEENS4_INS0_13JSArrayBufferEEENS4_INS0_8UintPtrTEEESC_SC_:
.LFB30661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE30661:
	.size	_GLOBAL__sub_I__ZN2v88internal22AllocateTypedArray_341EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEbNS4_INS0_3MapEEENS4_INS0_13JSArrayBufferEEENS4_INS0_8UintPtrTEEESC_SC_, .-_GLOBAL__sub_I__ZN2v88internal22AllocateTypedArray_341EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEbNS4_INS0_3MapEEENS4_INS0_13JSArrayBufferEEENS4_INS0_8UintPtrTEEESC_SC_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal22AllocateTypedArray_341EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEEbNS4_INS0_3MapEEENS4_INS0_13JSArrayBufferEEENS4_INS0_8UintPtrTEEESC_SC_
	.weak	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE:
	.byte	8
	.byte	7
	.weak	_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE:
	.byte	5
	.byte	5
	.weak	_ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZN2v88internal13MachineTypeOfINS0_7RawPtrTEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_7RawPtrTEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_7RawPtrTEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_7RawPtrTEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_7RawPtrTEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_7RawPtrTEvE5valueE:
	.byte	5
	.byte	0
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC6:
	.byte	7
	.byte	7
	.byte	7
	.byte	8
	.byte	8
	.byte	5
	.byte	4
	.byte	5
	.byte	6
	.byte	5
	.byte	5
	.byte	5
	.byte	4
	.byte	6
	.byte	6
	.byte	5
	.align 16
.LC8:
	.byte	7
	.byte	7
	.byte	7
	.byte	8
	.byte	8
	.byte	5
	.byte	4
	.byte	5
	.byte	6
	.byte	5
	.byte	5
	.byte	5
	.byte	4
	.byte	5
	.byte	5
	.byte	4
	.align 16
.LC10:
	.byte	7
	.byte	7
	.byte	7
	.byte	8
	.byte	8
	.byte	5
	.byte	4
	.byte	5
	.byte	6
	.byte	5
	.byte	5
	.byte	5
	.byte	4
	.byte	5
	.byte	5
	.byte	5
	.align 16
.LC11:
	.byte	7
	.byte	7
	.byte	7
	.byte	8
	.byte	8
	.byte	5
	.byte	4
	.byte	5
	.byte	6
	.byte	5
	.byte	5
	.byte	5
	.byte	4
	.byte	5
	.byte	5
	.byte	6
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
