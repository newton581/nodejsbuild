	.file	"array-reverse-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0:
.LFB29849:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	%rsi, %r13
	pushq	%rbx
	movq	%r13, %rax
	sarq	$3, %rax
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%r13, %r13
	js	.L14
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%rdx, %r14
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.L9
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%rax, %rdi
.L9:
	leaq	(%rdi,%r13), %r15
	movq	%rdi, (%rbx)
	movq	%r15, 16(%rbx)
	cmpq	%r12, %r14
	je	.L10
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
.L10:
	movq	%r15, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29849:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0:
.LFB29848:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	subq	%rsi, %rdx
	js	.L21
	movq	%rdi, %rbx
	je	.L17
	movq	%rdx, %rdi
	movq	%rdx, %r12
	movq	%rsi, %r13
	call	_Znwm@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	leaq	(%rax,%r12), %r14
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	%r14, 16(%rbx)
	call	memcpy@PLT
	movq	%r14, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	xorl	%r14d, %r14d
	movq	$0, (%rdi)
	movq	$0, 16(%rdi)
	movq	%r14, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29848:
	.size	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0:
.LFB29847:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	movabsq	$384307168202282325, %rax
	cmpq	%rax, %rdx
	ja	.L39
	pxor	%xmm0, %xmm0
	leaq	(%rdx,%rdx,2), %r14
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	%rdx, %r12
	movups	%xmm0, 8(%rdi)
	salq	$3, %r14
	xorl	%esi, %esi
	movq	$0, 24(%rdi)
	testq	%rdx, %rdx
	je	.L28
	movq	%r14, %rdi
	call	_Znwm@PLT
	leaq	-1(%r12), %rdx
	leaq	(%rax,%r14), %rsi
	movq	%rax, 8(%rbx)
	movq	%rsi, 24(%rbx)
	cmpq	$2, %rdx
	jbe	.L29
	movq	%r12, %rdi
	movq	%r12, %rcx
	pxor	%xmm0, %xmm0
	movq	%rax, %rdx
	shrq	%rdi
	andq	$-2, %rcx
	addq	%rdi, %rcx
	salq	$4, %rcx
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L26:
	movups	%xmm0, (%rdx)
	addq	$48, %rdx
	movups	%xmm0, -32(%rdx)
	movups	%xmm0, -16(%rdx)
	cmpq	%rcx, %rdx
	jne	.L26
	movq	%r12, %rdx
	movq	%r12, %rcx
	andq	$-2, %rdx
	andl	$1, %ecx
	leaq	(%rdx,%rdx,2), %rdi
	leaq	(%rax,%rdi,8), %rax
	cmpq	%rdx, %r12
	je	.L28
.L25:
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	cmpq	$1, %rcx
	je	.L28
	movq	$0, 40(%rax)
	movups	%xmm0, 24(%rax)
	cmpq	$2, %rcx
	je	.L28
	movq	$0, 64(%rax)
	movups	%xmm0, 48(%rax)
.L28:
	movq	%rsi, 16(%rbx)
	pxor	%xmm0, %xmm0
	leaq	56(%rbx), %rdi
	movq	%r13, %rsi
	movups	%xmm0, 32(%rbx)
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	$0, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	%r12, %rcx
	jmp	.L25
.L39:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29847:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev,"axG",@progbits,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev:
.LFB22413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$56, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L41
	call	_ZdlPv@PLT
.L41:
	movq	16(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	%r12, %r13
	je	.L42
	.p2align 4,,10
	.p2align 3
.L46:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L43
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L46
.L44:
	movq	8(%rbx), %r12
.L42:
	testq	%r12, %r12
	je	.L40
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L46
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L40:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22413:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev
	.set	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.section	.rodata._ZN2v88internal46StoreElement23ATFastPackedSmiElements5ATSmi_31EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEESA_.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../../deps/v8/../../deps/v8/src/builtins/array-reverse.tq"
	.section	.text._ZN2v88internal46StoreElement23ATFastPackedSmiElements5ATSmi_31EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEESA_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal46StoreElement23ATFastPackedSmiElements5ATSmi_31EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEESA_
	.type	_ZN2v88internal46StoreElement23ATFastPackedSmiElements5ATSmi_31EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEESA_, @function
_ZN2v88internal46StoreElement23ATFastPackedSmiElements5ATSmi_31EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEESA_:
.LFB22463:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-616(%rbp), %r15
	leaq	-704(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-712(%rbp), %r12
	pushq	%rbx
	subq	$728, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -744(%rbp)
	movq	%rdx, -752(%rbp)
	movq	%rcx, -760(%rbp)
	movq	%r8, -768(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -712(%rbp)
	movq	%rdi, -672(%rbp)
	movl	$96, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r15, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -664(%rbp)
	leaq	-672(%rbp), %rax
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -736(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-712(%rbp), %rax
	movl	$96, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	leaq	-424(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movq	%rcx, %rbx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movq	%rbx, %rdi
	leaq	-232(%rbp), %rbx
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -728(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -472(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-712(%rbp), %rax
	movl	$96, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-760(%rbp), %xmm1
	movq	-744(%rbp), %xmm2
	movaps	%xmm0, -704(%rbp)
	movhps	-768(%rbp), %xmm1
	movq	$0, -688(%rbp)
	movhps	-752(%rbp), %xmm2
	movaps	%xmm1, -80(%rbp)
	movaps	%xmm2, -96(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movq	%r14, %rsi
	movq	-736(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -704(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L53
	call	_ZdlPv@PLT
.L53:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -608(%rbp)
	jne	.L121
.L54:
	cmpq	$0, -416(%rbp)
	leaq	-288(%rbp), %r13
	jne	.L122
.L57:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -688(%rbp)
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	$101058311, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -704(%rbp)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L60
	call	_ZdlPv@PLT
.L60:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L61
	call	_ZdlPv@PLT
.L61:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L62
	.p2align 4,,10
	.p2align 3
.L66:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L63
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L66
.L64:
	movq	-280(%rbp), %r13
.L62:
	testq	%r13, %r13
	je	.L67
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L67:
	movq	-728(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L68
	call	_ZdlPv@PLT
.L68:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L69
	.p2align 4,,10
	.p2align 3
.L73:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L70
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L73
.L71:
	movq	-472(%rbp), %r13
.L69:
	testq	%r13, %r13
	je	.L74
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L74:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L75
	call	_ZdlPv@PLT
.L75:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L76
	.p2align 4,,10
	.p2align 3
.L80:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L77
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L80
.L78:
	movq	-664(%rbp), %r13
.L76:
	testq	%r13, %r13
	je	.L81
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L81:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L123
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L80
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L63:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L66
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L70:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L73
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L121:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -688(%rbp)
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movq	-736(%rbp), %rdi
	movq	%r14, %rsi
	movl	$101058311, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -704(%rbp)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L55
	movq	%rax, -736(%rbp)
	call	_ZdlPv@PLT
	movq	-736(%rbp), %rax
.L55:
	movq	(%rax), %rax
	movl	$37, %edx
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	movq	%rcx, -736(%rbp)
	movq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rsi, -744(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -760(%rbp)
	movq	%rax, -752(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-744(%rbp), %rdx
	movq	-736(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal27UnsafeCast10FixedArray_1409EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$38, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -768(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movq	-760(%rbp), %r13
	movq	-768(%rbp), %r10
	movq	%r13, %rdx
	movq	%r10, %rsi
	movq	%r10, -760(%rbp)
	call	_ZN2v88internal17CodeStubAssembler21FixedArrayBoundsCheckENS0_8compiler5TNodeINS0_14FixedArrayBaseEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movq	%r13, %rdx
	pushq	$0
	movq	-760(%rbp), %r10
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	-752(%rbp), %rcx
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler37StoreFixedArrayOrPropertyArrayElementEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$36, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$35, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$32, %edi
	movq	-736(%rbp), %xmm0
	movq	$0, -688(%rbp)
	movhps	-744(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%r13, %xmm0
	movhps	-752(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm6
	movq	%r14, %rsi
	leaq	32(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -704(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-704(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L56
	call	_ZdlPv@PLT
.L56:
	movq	-728(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L122:
	movq	-728(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -688(%rbp)
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	$101058311, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -704(%rbp)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-704(%rbp), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L58
	call	_ZdlPv@PLT
.L58:
	movq	0(%r13), %rax
	movl	$32, %edi
	leaq	-288(%rbp), %r13
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm7
	movq	$0, -688(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rdi
	movq	%r14, %rsi
	movdqa	-80(%rbp), %xmm6
	leaq	32(%rax), %rdx
	movq	%rax, -704(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L59
	call	_ZdlPv@PLT
.L59:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L57
.L123:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22463:
	.size	_ZN2v88internal46StoreElement23ATFastPackedSmiElements5ATSmi_31EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEESA_, .-_ZN2v88internal46StoreElement23ATFastPackedSmiElements5ATSmi_31EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEESA_
	.section	.text._ZN2v88internal53StoreElement26ATFastPackedDoubleElements9ATfloat64_33EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEENS4_INS0_8Float64TEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal53StoreElement26ATFastPackedDoubleElements9ATfloat64_33EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEENS4_INS0_8Float64TEEE
	.type	_ZN2v88internal53StoreElement26ATFastPackedDoubleElements9ATfloat64_33EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEENS4_INS0_8Float64TEEE, @function
_ZN2v88internal53StoreElement26ATFastPackedDoubleElements9ATfloat64_33EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEENS4_INS0_8Float64TEEE:
.LFB22480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-616(%rbp), %r15
	leaq	-704(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-712(%rbp), %r12
	pushq	%rbx
	subq	$728, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -744(%rbp)
	movq	%rdx, -752(%rbp)
	movq	%rcx, -760(%rbp)
	movq	%r8, -768(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -712(%rbp)
	movq	%rdi, -672(%rbp)
	movl	$96, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r15, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -664(%rbp)
	leaq	-672(%rbp), %rax
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -736(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-712(%rbp), %rax
	movl	$96, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	leaq	-424(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movq	%rcx, %rbx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movq	%rbx, %rdi
	leaq	-232(%rbp), %rbx
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -728(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -472(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-712(%rbp), %rax
	movl	$96, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-760(%rbp), %xmm1
	movq	-744(%rbp), %xmm2
	movaps	%xmm0, -704(%rbp)
	movhps	-768(%rbp), %xmm1
	movq	$0, -688(%rbp)
	movhps	-752(%rbp), %xmm2
	movaps	%xmm1, -80(%rbp)
	movaps	%xmm2, -96(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movq	%r14, %rsi
	movq	-736(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -704(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L125
	call	_ZdlPv@PLT
.L125:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -608(%rbp)
	jne	.L193
.L126:
	cmpq	$0, -416(%rbp)
	leaq	-288(%rbp), %r13
	jne	.L194
.L129:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -688(%rbp)
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	$218498823, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -704(%rbp)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L132
	call	_ZdlPv@PLT
.L132:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L133
	call	_ZdlPv@PLT
.L133:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L134
	.p2align 4,,10
	.p2align 3
.L138:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L135
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L138
.L136:
	movq	-280(%rbp), %r13
.L134:
	testq	%r13, %r13
	je	.L139
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L139:
	movq	-728(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L140
	call	_ZdlPv@PLT
.L140:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L141
	.p2align 4,,10
	.p2align 3
.L145:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L142
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L145
.L143:
	movq	-472(%rbp), %r13
.L141:
	testq	%r13, %r13
	je	.L146
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L146:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L147
	call	_ZdlPv@PLT
.L147:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L148
	.p2align 4,,10
	.p2align 3
.L152:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L149
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L152
.L150:
	movq	-664(%rbp), %r13
.L148:
	testq	%r13, %r13
	je	.L153
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L153:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L195
	addq	$728, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L152
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L135:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L138
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L142:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L145
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L193:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -688(%rbp)
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movq	-736(%rbp), %rdi
	movq	%r14, %rsi
	movl	$218498823, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -704(%rbp)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L127
	movq	%rax, -736(%rbp)
	call	_ZdlPv@PLT
	movq	-736(%rbp), %rax
.L127:
	movq	(%rax), %rax
	movl	$51, %edx
	movq	8(%rax), %rsi
	movq	16(%rax), %rdi
	movq	(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rsi, -744(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdi, -760(%rbp)
	movq	%r12, %rdi
	movq	%rcx, -736(%rbp)
	movq	%rax, -752(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-744(%rbp), %rdx
	movq	-736(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal33UnsafeCast16FixedDoubleArray_1413EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$52, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -768(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	-768(%rbp), %r10
	movq	-760(%rbp), %r13
	movq	-752(%rbp), %rcx
	movq	%r10, %rsi
	movq	%r13, %rdx
	call	_ZN2v88internal17CodeStubAssembler28StoreFixedDoubleArrayElementENS0_8compiler5TNodeINS0_16FixedDoubleArrayEEEPNS2_4NodeENS3_INS0_8Float64TEEENS1_13ParameterModeENS0_11CheckBoundsE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$50, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$48, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$32, %edi
	movq	-736(%rbp), %xmm0
	movq	$0, -688(%rbp)
	movhps	-744(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%r13, %xmm0
	movhps	-752(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm6
	movq	%r14, %rsi
	leaq	32(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -704(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L128
	call	_ZdlPv@PLT
.L128:
	movq	-728(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L194:
	movq	-728(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -688(%rbp)
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	$218498823, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -704(%rbp)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-704(%rbp), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L130
	call	_ZdlPv@PLT
.L130:
	movq	0(%r13), %rax
	movl	$32, %edi
	leaq	-288(%rbp), %r13
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm7
	movq	$0, -688(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rdi
	movq	%r14, %rsi
	movdqa	-80(%rbp), %xmm6
	leaq	32(%rax), %rdx
	movq	%rax, -704(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L131
	call	_ZdlPv@PLT
.L131:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L129
.L195:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22480:
	.size	_ZN2v88internal53StoreElement26ATFastPackedDoubleElements9ATfloat64_33EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEENS4_INS0_8Float64TEEE, .-_ZN2v88internal53StoreElement26ATFastPackedDoubleElements9ATfloat64_33EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEENS4_INS0_8Float64TEEE
	.section	.rodata._ZN2v88internal24Cast13ATFastJSArray_1429EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"../../deps/v8/../../deps/v8/src/builtins/base.tq"
	.section	.text._ZN2v88internal24Cast13ATFastJSArray_1429EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24Cast13ATFastJSArray_1429EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal24Cast13ATFastJSArray_1429EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal24Cast13ATFastJSArray_1429EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE:
.LFB22528:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	leaq	-1632(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1760(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1800(%rbp), %r12
	pushq	%rbx
	subq	$1880, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -1872(%rbp)
	movq	%rsi, -1888(%rbp)
	movq	%rdx, -1904(%rbp)
	movq	%rcx, -1912(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1800(%rbp)
	movq	%rdi, -1632(%rbp)
	movl	$48, %edi
	movq	$0, -1624(%rbp)
	movq	$0, -1616(%rbp)
	movq	$0, -1608(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1624(%rbp)
	leaq	-1576(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1608(%rbp)
	movq	%rdx, -1616(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1592(%rbp)
	movq	%rax, -1816(%rbp)
	movq	$0, -1600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1432(%rbp)
	movq	$0, -1424(%rbp)
	movq	%rax, -1440(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1432(%rbp)
	leaq	-1384(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -1840(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1864(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$96, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1848(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1832(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	$0, -472(%rbp)
	movq	%rax, -480(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1824(%rbp)
	movq	$0, -464(%rbp)
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	$0, -440(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1888(%rbp), %xmm1
	movaps	%xmm0, -1760(%rbp)
	movhps	-1904(%rbp), %xmm1
	movq	$0, -1744(%rbp)
	movaps	%xmm1, -1888(%rbp)
	call	_Znwm@PLT
	movdqa	-1888(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L197
	call	_ZdlPv@PLT
.L197:
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1568(%rbp)
	jne	.L381
	cmpq	$0, -1376(%rbp)
	jne	.L382
.L203:
	cmpq	$0, -1184(%rbp)
	jne	.L383
.L206:
	cmpq	$0, -992(%rbp)
	jne	.L384
.L211:
	cmpq	$0, -800(%rbp)
	jne	.L385
.L214:
	cmpq	$0, -608(%rbp)
	leaq	-288(%rbp), %r15
	jne	.L386
.L217:
	cmpq	$0, -416(%rbp)
	jne	.L387
.L220:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L222
	call	_ZdlPv@PLT
.L222:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L223
	call	_ZdlPv@PLT
.L223:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L224
	.p2align 4,,10
	.p2align 3
.L228:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L225
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L228
.L226:
	movq	-280(%rbp), %r14
.L224:
	testq	%r14, %r14
	je	.L229
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L229:
	movq	-1824(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L230
	call	_ZdlPv@PLT
.L230:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L231
	.p2align 4,,10
	.p2align 3
.L235:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L232
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L235
.L233:
	movq	-472(%rbp), %r14
.L231:
	testq	%r14, %r14
	je	.L236
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L236:
	movq	-1832(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L237
	call	_ZdlPv@PLT
.L237:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L238
	.p2align 4,,10
	.p2align 3
.L242:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L239
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L242
.L240:
	movq	-664(%rbp), %r14
.L238:
	testq	%r14, %r14
	je	.L243
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L243:
	movq	-1848(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L244
	call	_ZdlPv@PLT
.L244:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L245
	.p2align 4,,10
	.p2align 3
.L249:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L246
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L249
.L247:
	movq	-856(%rbp), %r14
.L245:
	testq	%r14, %r14
	je	.L250
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L250:
	movq	-1864(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L251
	call	_ZdlPv@PLT
.L251:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L252
	.p2align 4,,10
	.p2align 3
.L256:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L253
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L256
.L254:
	movq	-1048(%rbp), %r14
.L252:
	testq	%r14, %r14
	je	.L257
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L257:
	movq	-1840(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L258
	call	_ZdlPv@PLT
.L258:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L259
	.p2align 4,,10
	.p2align 3
.L263:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L260
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L263
.L261:
	movq	-1240(%rbp), %r14
.L259:
	testq	%r14, %r14
	je	.L264
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L264:
	movq	-1856(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L265
	call	_ZdlPv@PLT
.L265:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L266
	.p2align 4,,10
	.p2align 3
.L270:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L267
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L270
.L268:
	movq	-1432(%rbp), %r14
.L266:
	testq	%r14, %r14
	je	.L271
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L271:
	movq	-1816(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L272
	call	_ZdlPv@PLT
.L272:
	movq	-1616(%rbp), %rbx
	movq	-1624(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L273
	.p2align 4,,10
	.p2align 3
.L277:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L274
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L277
.L275:
	movq	-1624(%rbp), %r14
.L273:
	testq	%r14, %r14
	je	.L278
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L278:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L388
	addq	$1880, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L277
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L267:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L270
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L260:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L263
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L253:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L256
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L246:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L249
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L239:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L242
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L225:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L228
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L232:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L235
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L381:
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r9d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L199
	call	_ZdlPv@PLT
.L199:
	movq	(%rbx), %rax
	movl	$2026, %edx
	movq	%r12, %rdi
	leaq	.LC3(%rip), %rsi
	leaq	-1792(%rbp), %r15
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	%rcx, -1888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1872(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-96(%rbp), %rsi
	leaq	-64(%rbp), %rdx
	movq	%r15, %rdi
	movq	-1888(%rbp), %xmm2
	movq	%rbx, %xmm0
	movq	%rbx, %xmm4
	movq	$0, -1776(%rbp)
	punpcklqdq	%xmm0, %xmm0
	punpcklqdq	%xmm4, %xmm2
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -1888(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -1792(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1248(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L200
	call	_ZdlPv@PLT
.L200:
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1752(%rbp)
	jne	.L389
.L201:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1376(%rbp)
	je	.L203
.L382:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1440(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r8d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L204
	call	_ZdlPv@PLT
.L204:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-480(%rbp), %rdi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L205
	call	_ZdlPv@PLT
.L205:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1184(%rbp)
	je	.L206
.L383:
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-96(%rbp), %rbx
	leaq	-1248(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-92(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movaps	%xmm0, -1760(%rbp)
	movl	$117966855, -96(%rbp)
	movq	$0, -1744(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L207
	call	_ZdlPv@PLT
.L207:
	movq	(%r15), %rax
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	24(%rax), %rax
	movq	%rcx, -1904(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -1888(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1888(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r13, %rcx
	movq	-1872(%rbp), %rdi
	call	_ZN2v88internal23Cast13ATFastJSArray_135EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-64(%rbp), %rdx
	movq	%rbx, %rsi
	movq	-1888(%rbp), %rcx
	movq	%r15, %xmm3
	leaq	-1792(%rbp), %r15
	movq	%rax, -72(%rbp)
	movhps	-1904(%rbp), %xmm3
	movq	%r15, %rdi
	movaps	%xmm0, -1792(%rbp)
	movq	%rcx, -80(%rbp)
	movaps	%xmm3, -1904(%rbp)
	movaps	%xmm3, -96(%rbp)
	movq	$0, -1776(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-864(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L208
	call	_ZdlPv@PLT
.L208:
	movq	-1848(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1752(%rbp)
	jne	.L390
.L209:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -992(%rbp)
	je	.L211
.L384:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$2055, %edi
	pxor	%xmm0, %xmm0
	leaq	-96(%rbp), %rsi
	movw	%di, -96(%rbp)
	leaq	-93(%rbp), %rdx
	movq	%r13, %rdi
	movaps	%xmm0, -1760(%rbp)
	movb	$7, -94(%rbp)
	movq	$0, -1744(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L212
	call	_ZdlPv@PLT
.L212:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-480(%rbp), %rdi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L213
	call	_ZdlPv@PLT
.L213:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L214
.L385:
	movq	-1848(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-96(%rbp), %rbx
	leaq	-864(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-92(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movaps	%xmm0, -1760(%rbp)
	movl	$117901319, -96(%rbp)
	movq	$0, -1744(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L215
	call	_ZdlPv@PLT
.L215:
	movq	(%r15), %rax
	leaq	-72(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movdqu	(%rax), %xmm0
	movq	24(%rax), %rax
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-672(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L216
	call	_ZdlPv@PLT
.L216:
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L387:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L221
	call	_ZdlPv@PLT
.L221:
	movq	-1912(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L386:
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-96(%rbp), %rbx
	leaq	-672(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-93(%rbp), %rdx
	movq	%r13, %rdi
	movl	$2055, %esi
	movb	$7, -94(%rbp)
	movw	%si, -96(%rbp)
	movq	%rbx, %rsi
	movaps	%xmm0, -1760(%rbp)
	movq	$0, -1744(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L218
	call	_ZdlPv@PLT
.L218:
	movq	(%r15), %rax
	movl	$147, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %r15
	movq	%rsi, -1904(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -1888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, -80(%rbp)
	leaq	-72(%rbp), %rdx
	movq	%rbx, %rsi
	movq	-1888(%rbp), %xmm0
	movq	%r13, %rdi
	leaq	-288(%rbp), %r15
	movq	$0, -1744(%rbp)
	movhps	-1904(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1760(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L219
	call	_ZdlPv@PLT
.L219:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L390:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-72(%rbp), %rdx
	movq	%rbx, %rsi
	movdqa	-1904(%rbp), %xmm7
	movq	-1888(%rbp), %rax
	movq	%r15, %rdi
	movaps	%xmm0, -1792(%rbp)
	movq	$0, -1776(%rbp)
	movq	%rax, -80(%rbp)
	movaps	%xmm7, -96(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1056(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L210
	call	_ZdlPv@PLT
.L210:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L389:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movdqa	-1888(%rbp), %xmm5
	movaps	%xmm0, -1792(%rbp)
	movaps	%xmm5, -96(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-1440(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L202
	call	_ZdlPv@PLT
.L202:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L201
.L388:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22528:
	.size	_ZN2v88internal24Cast13ATFastJSArray_1429EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, .-_ZN2v88internal24Cast13ATFastJSArray_1429EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE9AddInputsENS1_5TNodeIS3_EENS7_IS4_EENS7_IS5_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE9AddInputsENS1_5TNodeIS3_EENS7_IS4_EENS7_IS5_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE9AddInputsENS1_5TNodeIS3_EENS7_IS4_EENS7_IS5_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE9AddInputsENS1_5TNodeIS3_EENS7_IS4_EENS7_IS5_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE9AddInputsENS1_5TNodeIS3_EENS7_IS4_EENS7_IS5_EE:
.LFB26874:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$24, %edi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -48(%rbp)
	movq	%rdx, -40(%rbp)
	movq	%rcx, -32(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	-32(%rbp), %rcx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movdqa	-48(%rbp), %xmm1
	leaq	24(%rax), %rdx
	movq	%rax, -80(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm1, (%rax)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L391
	call	_ZdlPv@PLT
.L391:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L398
	addq	$72, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L398:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26874:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE9AddInputsENS1_5TNodeIS3_EENS7_IS4_EENS7_IS5_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE9AddInputsENS1_5TNodeIS3_EENS7_IS4_EENS7_IS5_EE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EE:
.LFB26884:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	movl	$3, %edi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -64(%rbp)
	movq	$0, -48(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%r14, %rdi
	leaq	-64(%rbp), %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$6, 2(%rax)
	movq	%rax, -64(%rbp)
	movq	%rdx, -48(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-64(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L400
	call	_ZdlPv@PLT
.L400:
	movq	(%r14), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L401
	movq	%rdx, 0(%r13)
.L401:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L402
	movq	%rdx, (%r12)
.L402:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L399
	movq	%rax, (%rbx)
.L399:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L418
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L418:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26884:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EE
	.section	.text._ZN2v88internal52LoadElement26ATFastPackedDoubleElements9ATfloat64_30EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal52LoadElement26ATFastPackedDoubleElements9ATfloat64_30EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE
	.type	_ZN2v88internal52LoadElement26ATFastPackedDoubleElements9ATfloat64_30EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE, @function
_ZN2v88internal52LoadElement26ATFastPackedDoubleElements9ATfloat64_30EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE:
.LFB22447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1648(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1400(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$1688, %rsp
	movq	%rsi, -1664(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1648(%rbp)
	movq	%rdi, -1456(%rbp)
	movl	$72, %edi
	movq	$0, -1448(%rbp)
	movq	$0, -1440(%rbp)
	movq	$0, -1432(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r12, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1448(%rbp)
	leaq	-1456(%rbp), %rax
	movq	%rdx, -1432(%rbp)
	movq	%rdx, -1440(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1416(%rbp)
	movq	%rax, -1656(%rbp)
	movq	$0, -1424(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1648(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1256(%rbp)
	movq	$0, -1248(%rbp)
	movq	%rax, -1264(%rbp)
	movq	$0, -1240(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1208(%rbp), %rcx
	movq	%r13, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rdx, -1240(%rbp)
	movq	%rdx, -1248(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1672(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1224(%rbp)
	movq	%rax, -1256(%rbp)
	movq	$0, -1232(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1648(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1064(%rbp)
	movq	$0, -1056(%rbp)
	movq	%rax, -1072(%rbp)
	movq	$0, -1048(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1016(%rbp), %rcx
	movq	%r13, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -1048(%rbp)
	movq	%rdx, -1056(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1720(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1032(%rbp)
	movq	%rax, -1064(%rbp)
	movq	$0, -1040(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1648(%rbp), %rax
	movl	$96, %edi
	movq	$0, -872(%rbp)
	movq	$0, -864(%rbp)
	movq	%rax, -880(%rbp)
	movq	$0, -856(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-824(%rbp), %rcx
	movq	%r13, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -856(%rbp)
	movq	%rdx, -864(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1712(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -840(%rbp)
	movq	%rax, -872(%rbp)
	movq	$0, -848(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1648(%rbp), %rax
	movl	$120, %edi
	movq	$0, -680(%rbp)
	movq	$0, -672(%rbp)
	movq	%rax, -688(%rbp)
	movq	$0, -664(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-632(%rbp), %rcx
	movq	%r13, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -664(%rbp)
	movq	%rdx, -672(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1704(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -648(%rbp)
	movq	%rax, -680(%rbp)
	movq	$0, -656(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1648(%rbp), %rax
	movl	$96, %edi
	movq	$0, -488(%rbp)
	movq	$0, -480(%rbp)
	movq	%rax, -496(%rbp)
	movq	$0, -472(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-440(%rbp), %rcx
	movq	%r13, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -472(%rbp)
	movq	%rdx, -480(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1696(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -456(%rbp)
	movq	%rax, -488(%rbp)
	movq	$0, -464(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1648(%rbp), %rax
	movl	$96, %edi
	movq	$0, -296(%rbp)
	movq	$0, -288(%rbp)
	movq	%rax, -304(%rbp)
	movq	$0, -280(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-248(%rbp), %rcx
	movq	%r13, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -280(%rbp)
	movq	%rdx, -288(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1688(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -296(%rbp)
	movups	%xmm0, -264(%rbp)
	movq	$0, -272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1664(%rbp), %r10
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	%rbx, -104(%rbp)
	leaq	-1584(%rbp), %rbx
	movq	%r10, -112(%rbp)
	movaps	%xmm0, -1584(%rbp)
	movq	%r15, -96(%rbp)
	movq	$0, -1568(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-112(%rbp), %xmm2
	movq	%rbx, %rsi
	movq	-1656(%rbp), %rdi
	leaq	24(%rax), %rdx
	movq	%rax, -1584(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -1568(%rbp)
	movq	%rdx, -1576(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L420
	call	_ZdlPv@PLT
.L420:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1072(%rbp), %rax
	cmpq	$0, -1392(%rbp)
	movq	%rax, -1680(%rbp)
	leaq	-1264(%rbp), %rax
	movq	%rax, -1664(%rbp)
	jne	.L487
.L421:
	cmpq	$0, -1200(%rbp)
	leaq	-880(%rbp), %r12
	jne	.L488
.L425:
	leaq	-688(%rbp), %rax
	cmpq	$0, -1008(%rbp)
	movq	%rax, -1672(%rbp)
	jne	.L489
.L428:
	cmpq	$0, -816(%rbp)
	jne	.L490
.L431:
	cmpq	$0, -624(%rbp)
	leaq	-496(%rbp), %r14
	jne	.L491
.L433:
	cmpq	$0, -432(%rbp)
	leaq	-304(%rbp), %r15
	jne	.L492
.L436:
	movq	-1688(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1568(%rbp)
	movaps	%xmm0, -1584(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	$218498823, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1584(%rbp)
	movq	%rdx, -1568(%rbp)
	movq	%rdx, -1576(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1584(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L439
	call	_ZdlPv@PLT
.L439:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rbx
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-1672(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-1680(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-1664(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-1656(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L493
	addq	$1688, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L487:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	$0, -1640(%rbp)
	leaq	-1616(%rbp), %r12
	movq	$0, -1632(%rbp)
	movq	$0, -1624(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1656(%rbp), %rdi
	leaq	-1624(%rbp), %rcx
	leaq	-1632(%rbp), %rdx
	leaq	-1640(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EE
	movl	$24, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1632(%rbp), %rdx
	movq	-1640(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal33UnsafeCast16FixedDoubleArray_1413EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$27, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movq	-1624(%rbp), %rdx
	movq	%r15, -1728(%rbp)
	call	_ZN2v88internal17CodeStubAssembler23LoadDoubleWithHoleCheckENS0_8compiler5TNodeINS0_16FixedDoubleArrayEEENS3_INS0_3SmiEEEPNS2_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, -64(%rbp)
	leaq	-112(%rbp), %r15
	movq	%r12, %rdi
	movq	-1640(%rbp), %xmm0
	movq	-1624(%rbp), %rax
	leaq	-56(%rbp), %rdx
	movq	%r15, %rsi
	movq	-1728(%rbp), %rcx
	movq	$0, -1600(%rbp)
	movhps	-1632(%rbp), %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rcx, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movaps	%xmm0, -1616(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-1680(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1616(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L422
	call	_ZdlPv@PLT
.L422:
	movq	-1720(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1264(%rbp), %rax
	cmpq	$0, -1576(%rbp)
	movq	%rax, -1664(%rbp)
	jne	.L494
.L423:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L492:
	movq	-1696(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1568(%rbp)
	movaps	%xmm0, -1584(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movl	$218498823, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1584(%rbp)
	movq	%rdx, -1568(%rbp)
	movq	%rdx, -1576(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1584(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L437
	call	_ZdlPv@PLT
.L437:
	movq	(%r15), %rax
	movl	$21, %edx
	movq	%r13, %rdi
	movq	16(%rax), %rsi
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	24(%rax), %rax
	movq	%rsi, -1704(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -1696(%rbp)
	movq	%rax, -1712(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-112(%rbp), %rsi
	leaq	-80(%rbp), %rdx
	movq	%rbx, %rdi
	movq	$0, -1568(%rbp)
	movq	%r15, %xmm0
	leaq	-304(%rbp), %r15
	movhps	-1696(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-1704(%rbp), %xmm0
	movhps	-1712(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1584(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r15, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L438
	call	_ZdlPv@PLT
.L438:
	movq	-1688(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L491:
	movq	-1704(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-112(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-107(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movaps	%xmm0, -1584(%rbp)
	movl	$117835527, -112(%rbp)
	movb	$13, -108(%rbp)
	movq	$0, -1568(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-1672(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1584(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L434
	call	_ZdlPv@PLT
.L434:
	movq	(%r14), %rax
	movl	$27, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	%rcx, -1704(%rbp)
	movq	16(%rax), %rcx
	movq	32(%rax), %rax
	movq	%rcx, -1712(%rbp)
	movq	%rax, -1720(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %xmm0
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movhps	-1704(%rbp), %xmm0
	leaq	-80(%rbp), %rdx
	leaq	-496(%rbp), %r14
	movq	$0, -1568(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	-1712(%rbp), %xmm0
	movhps	-1720(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1584(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L435
	call	_ZdlPv@PLT
.L435:
	movq	-1696(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L490:
	movq	-1712(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-112(%rbp), %rsi
	movq	%rbx, %rdi
	leaq	-108(%rbp), %rdx
	movaps	%xmm0, -1584(%rbp)
	movl	$117835527, -112(%rbp)
	movq	$0, -1568(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L432
	call	_ZdlPv@PLT
.L432:
	movl	$28, %edx
	movq	%r13, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L489:
	movq	-1720(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-112(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1543, %eax
	movq	%r15, %rsi
	movq	%rbx, %rdi
	pxor	%xmm0, %xmm0
	leaq	-105(%rbp), %rdx
	movw	%ax, -108(%rbp)
	movaps	%xmm0, -1584(%rbp)
	movl	$117835527, -112(%rbp)
	movb	$13, -106(%rbp)
	movq	$0, -1568(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-1680(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L429
	movq	%rax, -1672(%rbp)
	call	_ZdlPv@PLT
	movq	-1672(%rbp), %rax
.L429:
	movq	(%rax), %rax
	leaq	-72(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm1
	movq	48(%rax), %rax
	movq	$0, -1568(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -1584(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-688(%rbp), %rax
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -1672(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L430
	call	_ZdlPv@PLT
.L430:
	movq	-1704(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L488:
	movq	-1672(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-112(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1543, %edx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	pxor	%xmm0, %xmm0
	movw	%dx, -108(%rbp)
	leaq	-106(%rbp), %rdx
	movaps	%xmm0, -1584(%rbp)
	movl	$117835527, -112(%rbp)
	movq	$0, -1568(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-1664(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1584(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L426
	call	_ZdlPv@PLT
.L426:
	movq	(%r12), %rax
	leaq	-80(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	leaq	-880(%rbp), %r12
	movdqu	(%rax), %xmm0
	movdqu	16(%rax), %xmm3
	movq	$0, -1568(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -1584(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L427
	call	_ZdlPv@PLT
.L427:
	movq	-1712(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L494:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-64(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	-1640(%rbp), %xmm0
	movq	-1624(%rbp), %rax
	movq	$0, -1600(%rbp)
	movq	-1728(%rbp), %rcx
	movhps	-1632(%rbp), %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rcx, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movaps	%xmm0, -1616(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-1664(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1616(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L424
	call	_ZdlPv@PLT
.L424:
	movq	-1672(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L423
.L493:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22447:
	.size	_ZN2v88internal52LoadElement26ATFastPackedDoubleElements9ATfloat64_30EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE, .-_ZN2v88internal52LoadElement26ATFastPackedDoubleElements9ATfloat64_30EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_10FixedArrayES6_NS0_7IntPtrTES7_S5_S5_S7_NS0_10HeapObjectES7_S7_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EESK_SG_SG_SK_PNSA_IS8_EESK_SK_SK_SK_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_10FixedArrayES6_NS0_7IntPtrTES7_S5_S5_S7_NS0_10HeapObjectES7_S7_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EESK_SG_SG_SK_PNSA_IS8_EESK_SK_SK_SK_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_10FixedArrayES6_NS0_7IntPtrTES7_S5_S5_S7_NS0_10HeapObjectES7_S7_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EESK_SG_SG_SK_PNSA_IS8_EESK_SK_SK_SK_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_10FixedArrayES6_NS0_7IntPtrTES7_S5_S5_S7_NS0_10HeapObjectES7_S7_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EESK_SG_SG_SK_PNSA_IS8_EESK_SK_SK_SK_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_10FixedArrayES6_NS0_7IntPtrTES7_S5_S5_S7_NS0_10HeapObjectES7_S7_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EESK_SG_SG_SK_PNSA_IS8_EESK_SK_SK_SK_:
.LFB26896:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$15, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$136, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -88(%rbp)
	movq	%r9, -96(%rbp)
	movq	88(%rbp), %r15
	movq	%rax, -104(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$433758466851866375, %rcx
	movq	%rcx, (%rax)
	movl	$1285, %ecx
	leaq	15(%rax), %rdx
	movl	$84346118, 8(%rax)
	movw	%cx, 12(%rax)
	movb	$5, 14(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L496
	movq	%rax, -176(%rbp)
	call	_ZdlPv@PLT
	movq	-176(%rbp), %rax
.L496:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L497
	movq	%rdx, (%r14)
.L497:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L498
	movq	%rdx, 0(%r13)
.L498:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L499
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L499:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L500
	movq	%rdx, (%rbx)
.L500:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L501
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L501:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L502
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L502:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L503
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L503:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L504
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L504:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L505
	movq	-128(%rbp), %rsi
	movq	%rdx, (%rsi)
.L505:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L506
	movq	-136(%rbp), %rcx
	movq	%rdx, (%rcx)
.L506:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L507
	movq	-144(%rbp), %rbx
	movq	%rdx, (%rbx)
.L507:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L508
	movq	-152(%rbp), %rsi
	movq	%rdx, (%rsi)
.L508:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L509
	movq	-160(%rbp), %rcx
	movq	%rdx, (%rcx)
.L509:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L510
	movq	-168(%rbp), %rdi
	movq	%rdx, (%rdi)
.L510:
	movq	112(%rax), %rax
	testq	%rax, %rax
	je	.L495
	movq	%rax, (%r15)
.L495:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L562
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L562:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26896:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_10FixedArrayES6_NS0_7IntPtrTES7_S5_S5_S7_NS0_10HeapObjectES7_S7_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EESK_SG_SG_SK_PNSA_IS8_EESK_SK_SK_SK_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_10FixedArrayES6_NS0_7IntPtrTES7_S5_S5_S7_NS0_10HeapObjectES7_S7_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EESK_SG_SG_SK_PNSA_IS8_EESK_SK_SK_SK_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_10FixedArrayES6_NS0_7IntPtrTES7_S5_S5_S7_NS0_10HeapObjectES7_S7_S7_S7_S8_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EESK_SG_SG_SK_PNSA_IS8_EESK_SK_SK_SK_SM_SK_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_10FixedArrayES6_NS0_7IntPtrTES7_S5_S5_S7_NS0_10HeapObjectES7_S7_S7_S7_S8_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EESK_SG_SG_SK_PNSA_IS8_EESK_SK_SK_SK_SM_SK_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_10FixedArrayES6_NS0_7IntPtrTES7_S5_S5_S7_NS0_10HeapObjectES7_S7_S7_S7_S8_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EESK_SG_SG_SK_PNSA_IS8_EESK_SK_SK_SK_SM_SK_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_10FixedArrayES6_NS0_7IntPtrTES7_S5_S5_S7_NS0_10HeapObjectES7_S7_S7_S7_S8_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EESK_SG_SG_SK_PNSA_IS8_EESK_SK_SK_SK_SM_SK_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_10FixedArrayES6_NS0_7IntPtrTES7_S5_S5_S7_NS0_10HeapObjectES7_S7_S7_S7_S8_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EESK_SG_SG_SK_PNSA_IS8_EESK_SK_SK_SK_SM_SK_:
.LFB26909:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$17, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$152, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -88(%rbp)
	movq	%r9, -96(%rbp)
	movq	104(%rbp), %r15
	movq	%rax, -104(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	88(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	96(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movdqa	.LC4(%rip), %xmm0
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movb	$5, 16(%rax)
	leaq	17(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L564
	movq	%rax, -192(%rbp)
	call	_ZdlPv@PLT
	movq	-192(%rbp), %rax
.L564:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L565
	movq	%rdx, (%r14)
.L565:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L566
	movq	%rdx, 0(%r13)
.L566:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L567
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L567:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L568
	movq	%rdx, (%rbx)
.L568:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L569
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L569:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L570
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L570:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L571
	movq	-112(%rbp), %rcx
	movq	%rdx, (%rcx)
.L571:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L572
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L572:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L573
	movq	-128(%rbp), %rcx
	movq	%rdx, (%rcx)
.L573:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L574
	movq	-136(%rbp), %rbx
	movq	%rdx, (%rbx)
.L574:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L575
	movq	-144(%rbp), %rsi
	movq	%rdx, (%rsi)
.L575:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L576
	movq	-152(%rbp), %rcx
	movq	%rdx, (%rcx)
.L576:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L577
	movq	-160(%rbp), %rbx
	movq	%rdx, (%rbx)
.L577:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L578
	movq	-168(%rbp), %rsi
	movq	%rdx, (%rsi)
.L578:
	movq	112(%rax), %rdx
	testq	%rdx, %rdx
	je	.L579
	movq	-176(%rbp), %rdi
	movq	%rdx, (%rdi)
.L579:
	movq	120(%rax), %rdx
	testq	%rdx, %rdx
	je	.L580
	movq	-184(%rbp), %rcx
	movq	%rdx, (%rcx)
.L580:
	movq	128(%rax), %rax
	testq	%rax, %rax
	je	.L563
	movq	%rax, (%r15)
.L563:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L638
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L638:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26909:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_10FixedArrayES6_NS0_7IntPtrTES7_S5_S5_S7_NS0_10HeapObjectES7_S7_S7_S7_S8_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EESK_SG_SG_SK_PNSA_IS8_EESK_SK_SK_SK_SM_SK_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_10FixedArrayES6_NS0_7IntPtrTES7_S5_S5_S7_NS0_10HeapObjectES7_S7_S7_S7_S8_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EESK_SG_SG_SK_PNSA_IS8_EESK_SK_SK_SK_SM_SK_
	.section	.rodata._ZN2v88internal45LoadElement23ATFastPackedSmiElements5ATSmi_28EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"../../deps/v8/../../deps/v8/src/builtins/torque-internal.tq"
	.section	.text._ZN2v88internal45LoadElement23ATFastPackedSmiElements5ATSmi_28EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal45LoadElement23ATFastPackedSmiElements5ATSmi_28EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE
	.type	_ZN2v88internal45LoadElement23ATFastPackedSmiElements5ATSmi_28EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE, @function
_ZN2v88internal45LoadElement23ATFastPackedSmiElements5ATSmi_28EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE:
.LFB22410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1920(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$3, %edx
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-2096(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$2200, %rsp
	movq	%rsi, -2168(%rbp)
	movq	%r12, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -2096(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1728(%rbp), %rax
	movl	$15, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -2144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1536(%rbp), %rax
	movl	$15, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -2120(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1344(%rbp), %rax
	movl	$17, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -2160(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1152(%rbp), %rax
	movl	$10, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-960(%rbp), %rax
	movl	$12, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -2112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-768(%rbp), %rax
	movl	$11, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -2152(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-576(%rbp), %rax
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -2128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-384(%rbp), %rax
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -2104(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-2168(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	%r13, -184(%rbp)
	leaq	-1952(%rbp), %r13
	movaps	%xmm0, -1952(%rbp)
	movq	%rax, -192(%rbp)
	movq	%rbx, -176(%rbp)
	movq	$0, -1936(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-192(%rbp), %xmm6
	leaq	24(%rax), %rdx
	movq	%rax, -1952(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L640
	call	_ZdlPv@PLT
.L640:
	leaq	-1864(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1856(%rbp)
	jne	.L707
	cmpq	$0, -1664(%rbp)
	jne	.L708
.L644:
	cmpq	$0, -1472(%rbp)
	jne	.L709
.L646:
	cmpq	$0, -1280(%rbp)
	jne	.L710
.L648:
	cmpq	$0, -1088(%rbp)
	jne	.L711
.L650:
	cmpq	$0, -896(%rbp)
	jne	.L712
.L652:
	cmpq	$0, -704(%rbp)
	jne	.L713
.L655:
	cmpq	$0, -512(%rbp)
	leaq	-328(%rbp), %r14
	jne	.L714
.L658:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movq	-2104(%rbp), %rdi
	movq	%r13, %rsi
	movl	$101058311, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L661
	call	_ZdlPv@PLT
.L661:
	movq	(%rbx), %rax
	movq	-2104(%rbp), %rdi
	movq	24(%rax), %r13
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2128(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2152(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2112(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2136(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2160(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2120(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2144(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L715
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L707:
	.cfi_restore_state
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	$0, -1976(%rbp)
	movq	$0, -1968(%rbp)
	movq	$0, -1960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1960(%rbp), %rcx
	leaq	-1968(%rbp), %rdx
	movq	%r15, %rdi
	leaq	-1976(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EE
	movl	$11, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1968(%rbp), %rdx
	movq	-1976(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal27UnsafeCast10FixedArray_1409EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$12, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -2168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%rax, -2184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-2168(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$55, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -2176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1960(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$56, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$37, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	-2176(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -2208(%rbp)
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -2192(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2208(%rbp), %r8
	movq	-2192(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, -2192(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-72(%rbp), %rdx
	pxor	%xmm1, %xmm1
	movq	%r13, %rdi
	movq	-2168(%rbp), %rax
	movq	-2176(%rbp), %rcx
	leaq	-192(%rbp), %r8
	movq	%rdx, -2224(%rbp)
	movq	-1976(%rbp), %xmm7
	movq	%r8, %rsi
	movq	%rbx, -120(%rbp)
	movq	-1960(%rbp), %xmm0
	movq	%rax, -168(%rbp)
	movq	%rax, -160(%rbp)
	movq	-2184(%rbp), %rax
	movhps	-1968(%rbp), %xmm7
	movq	%rcx, -144(%rbp)
	movq	-2168(%rbp), %rcx
	movq	%rax, -152(%rbp)
	movq	%rax, -104(%rbp)
	movq	-2176(%rbp), %rax
	movq	%r8, -2216(%rbp)
	movq	%xmm0, -176(%rbp)
	movq	%xmm0, -136(%rbp)
	movq	%xmm0, -128(%rbp)
	movq	%xmm0, -2232(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%rax, -96(%rbp)
	movaps	%xmm7, -2208(%rbp)
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm1, -1952(%rbp)
	movq	%rbx, -88(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -1936(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2144(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	movq	-2216(%rbp), %r8
	movq	-2232(%rbp), %xmm0
	movq	-2224(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L642
	movq	%rdx, -2232(%rbp)
	movq	%r8, -2224(%rbp)
	movq	%xmm0, -2216(%rbp)
	call	_ZdlPv@PLT
	movq	-2232(%rbp), %rdx
	movq	-2224(%rbp), %r8
	movq	-2216(%rbp), %xmm0
.L642:
	movdqa	%xmm0, %xmm2
	movq	%r8, %rsi
	movq	%r13, %rdi
	movq	%rbx, -80(%rbp)
	movdqa	-2208(%rbp), %xmm6
	movq	-2168(%rbp), %xmm5
	movq	$0, -1936(%rbp)
	movaps	%xmm6, -192(%rbp)
	punpcklqdq	%xmm5, %xmm2
	movdqa	%xmm5, %xmm1
	movq	-2176(%rbp), %xmm6
	movq	%rbx, %xmm5
	movaps	%xmm2, -176(%rbp)
	movhps	-2184(%rbp), %xmm1
	movdqa	%xmm6, %xmm2
	movaps	%xmm1, -160(%rbp)
	punpcklqdq	%xmm0, %xmm2
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	movdqa	%xmm6, %xmm0
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1952(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2120(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L643
	call	_ZdlPv@PLT
.L643:
	movq	-2192(%rbp), %rsi
	leaq	-1480(%rbp), %rcx
	leaq	-1672(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1664(%rbp)
	je	.L644
.L708:
	leaq	-1672(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -2072(%rbp)
	movq	$0, -2064(%rbp)
	movq	$0, -2056(%rbp)
	movq	$0, -2048(%rbp)
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movq	$0, -2024(%rbp)
	movq	$0, -2016(%rbp)
	movq	$0, -2008(%rbp)
	movq	$0, -2000(%rbp)
	movq	$0, -1992(%rbp)
	movq	$0, -1984(%rbp)
	movq	$0, -1976(%rbp)
	movq	$0, -1968(%rbp)
	movq	$0, -1960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1960(%rbp), %rax
	movq	-2144(%rbp), %rdi
	leaq	-2040(%rbp), %r9
	pushq	%rax
	leaq	-1968(%rbp), %rax
	leaq	-2048(%rbp), %r8
	pushq	%rax
	leaq	-1976(%rbp), %rax
	leaq	-2056(%rbp), %rcx
	pushq	%rax
	leaq	-1984(%rbp), %rax
	leaq	-2064(%rbp), %rdx
	pushq	%rax
	leaq	-1992(%rbp), %rax
	leaq	-2072(%rbp), %rsi
	pushq	%rax
	leaq	-2000(%rbp), %rax
	pushq	%rax
	leaq	-2008(%rbp), %rax
	pushq	%rax
	leaq	-2016(%rbp), %rax
	pushq	%rax
	leaq	-2024(%rbp), %rax
	pushq	%rax
	leaq	-2032(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_10FixedArrayES6_NS0_7IntPtrTES7_S5_S5_S7_NS0_10HeapObjectES7_S7_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EESK_SG_SG_SK_PNSA_IS8_EESK_SK_SK_SK_
	addq	$80, %rsp
	movl	$39, %edx
	movq	%r12, %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1960(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrMulENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1984(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$38, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	-1992(%rbp), %rdx
	call	_ZN2v88internal45UnsafeNewReference20UT5ATSmi10HeapObject_1411EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	pxor	%xmm0, %xmm0
	leaq	-56(%rbp), %rdx
	movq	%r13, %rdi
	movq	-2072(%rbp), %rax
	leaq	-192(%rbp), %rsi
	movq	%rax, -192(%rbp)
	movq	-2064(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-2056(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-2048(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-2040(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-2032(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-2024(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-2016(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-2008(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-2000(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-1992(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-1984(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-1976(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-1968(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-1960(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-1952(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-1944(%rbp), %rax
	movaps	%xmm0, -1952(%rbp)
	movq	%rax, -64(%rbp)
	movq	$0, -1936(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2160(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L645
	call	_ZdlPv@PLT
.L645:
	leaq	-1288(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1472(%rbp)
	je	.L646
.L709:
	leaq	-1480(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -2072(%rbp)
	movq	$0, -2064(%rbp)
	movq	$0, -2056(%rbp)
	movq	$0, -2048(%rbp)
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movq	$0, -2024(%rbp)
	movq	$0, -2016(%rbp)
	movq	$0, -2008(%rbp)
	movq	$0, -2000(%rbp)
	movq	$0, -1992(%rbp)
	movq	$0, -1984(%rbp)
	movq	$0, -1976(%rbp)
	movq	$0, -1968(%rbp)
	movq	$0, -1960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1960(%rbp), %rax
	movq	-2120(%rbp), %rdi
	leaq	-2056(%rbp), %rcx
	pushq	%rax
	leaq	-1968(%rbp), %rax
	leaq	-2040(%rbp), %r9
	pushq	%rax
	leaq	-1976(%rbp), %rax
	leaq	-2048(%rbp), %r8
	pushq	%rax
	leaq	-1984(%rbp), %rax
	leaq	-2064(%rbp), %rdx
	pushq	%rax
	leaq	-1992(%rbp), %rax
	leaq	-2072(%rbp), %rsi
	pushq	%rax
	leaq	-2000(%rbp), %rax
	pushq	%rax
	leaq	-2008(%rbp), %rax
	pushq	%rax
	leaq	-2016(%rbp), %rax
	pushq	%rax
	leaq	-2024(%rbp), %rax
	pushq	%rax
	leaq	-2032(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_10FixedArrayES6_NS0_7IntPtrTES7_S5_S5_S7_NS0_10HeapObjectES7_S7_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EESK_SG_SG_SK_PNSA_IS8_EESK_SK_SK_SK_
	addq	$80, %rsp
	movl	$41, %edx
	movq	%r12, %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-192(%rbp), %rsi
	leaq	-112(%rbp), %rdx
	movq	%r13, %rdi
	movq	-2008(%rbp), %xmm0
	movq	-2024(%rbp), %xmm1
	movq	$0, -1936(%rbp)
	movq	-2040(%rbp), %xmm2
	movq	-2056(%rbp), %xmm3
	movq	-2072(%rbp), %xmm4
	movhps	-2000(%rbp), %xmm0
	movhps	-2016(%rbp), %xmm1
	movhps	-2032(%rbp), %xmm2
	movhps	-2048(%rbp), %xmm3
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-2064(%rbp), %xmm4
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2136(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L647
	call	_ZdlPv@PLT
.L647:
	leaq	-1096(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1280(%rbp)
	je	.L648
.L710:
	leaq	-1288(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -2088(%rbp)
	movq	$0, -2080(%rbp)
	movq	$0, -2072(%rbp)
	movq	$0, -2064(%rbp)
	movq	$0, -2056(%rbp)
	movq	$0, -2048(%rbp)
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movq	$0, -2024(%rbp)
	movq	$0, -2016(%rbp)
	movq	$0, -2008(%rbp)
	movq	$0, -2000(%rbp)
	movq	$0, -1992(%rbp)
	movq	$0, -1984(%rbp)
	movq	$0, -1976(%rbp)
	movq	$0, -1968(%rbp)
	movq	$0, -1960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1960(%rbp), %rax
	movq	-2160(%rbp), %rdi
	leaq	-2072(%rbp), %rcx
	pushq	%rax
	leaq	-1968(%rbp), %rax
	leaq	-2056(%rbp), %r9
	pushq	%rax
	leaq	-1976(%rbp), %rax
	leaq	-2064(%rbp), %r8
	pushq	%rax
	leaq	-1984(%rbp), %rax
	leaq	-2080(%rbp), %rdx
	pushq	%rax
	leaq	-1992(%rbp), %rax
	leaq	-2088(%rbp), %rsi
	pushq	%rax
	leaq	-2000(%rbp), %rax
	pushq	%rax
	leaq	-2008(%rbp), %rax
	pushq	%rax
	leaq	-2016(%rbp), %rax
	pushq	%rax
	leaq	-2024(%rbp), %rax
	pushq	%rax
	leaq	-2032(%rbp), %rax
	pushq	%rax
	leaq	-2040(%rbp), %rax
	pushq	%rax
	leaq	-2048(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_10FixedArrayES6_NS0_7IntPtrTES7_S5_S5_S7_NS0_10HeapObjectES7_S7_S7_S7_S8_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EESK_SG_SG_SK_PNSA_IS8_EESK_SK_SK_SK_SM_SK_
	addq	$96, %rsp
	movl	$56, %edx
	movq	%r12, %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-192(%rbp), %rsi
	leaq	-96(%rbp), %rdx
	movq	%r13, %rdi
	movq	-1968(%rbp), %xmm0
	movq	-2024(%rbp), %xmm1
	movq	$0, -1936(%rbp)
	movq	-2040(%rbp), %xmm2
	movq	-2056(%rbp), %xmm3
	movq	-2072(%rbp), %xmm4
	movhps	-1960(%rbp), %xmm0
	movq	-2088(%rbp), %xmm5
	movhps	-2016(%rbp), %xmm1
	movhps	-2032(%rbp), %xmm2
	movhps	-2048(%rbp), %xmm3
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-2064(%rbp), %xmm4
	movhps	-2080(%rbp), %xmm5
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm5, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2112(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L649
	call	_ZdlPv@PLT
.L649:
	leaq	-904(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1088(%rbp)
	je	.L650
.L711:
	leaq	-1096(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movl	$1286, %esi
	movabsq	$433758466851866375, %rcx
	movq	-2136(%rbp), %rdi
	movw	%si, 8(%rax)
	leaq	10(%rax), %rdx
	movq	%r13, %rsi
	movq	%rcx, (%rax)
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L651
	call	_ZdlPv@PLT
.L651:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -896(%rbp)
	je	.L652
.L712:
	leaq	-904(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$12, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movq	-2112(%rbp), %rdi
	movq	%r13, %rsi
	movabsq	$433758466851866375, %rcx
	movq	%rcx, (%rax)
	leaq	12(%rax), %rdx
	movl	$84346118, 8(%rax)
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L653
	call	_ZdlPv@PLT
.L653:
	movq	(%rbx), %rax
	leaq	-192(%rbp), %rsi
	leaq	-104(%rbp), %rdx
	movq	%r13, %rdi
	movdqu	80(%rax), %xmm6
	movdqu	64(%rax), %xmm0
	movdqu	(%rax), %xmm4
	movdqu	16(%rax), %xmm3
	movdqu	32(%rax), %xmm2
	movdqu	48(%rax), %xmm1
	punpcklqdq	%xmm6, %xmm0
	movq	88(%rax), %rax
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -192(%rbp)
	movq	%rax, -112(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -1952(%rbp)
	movq	$0, -1936(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2152(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L654
	call	_ZdlPv@PLT
.L654:
	leaq	-712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -704(%rbp)
	je	.L655
.L713:
	leaq	-712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$11, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movq	-2152(%rbp), %rdi
	movq	%r13, %rsi
	movabsq	$433758466851866375, %rcx
	movq	%rcx, (%rax)
	movl	$1798, %ecx
	leaq	11(%rax), %rdx
	movw	%cx, 8(%rax)
	movb	$5, 10(%rax)
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L656
	call	_ZdlPv@PLT
.L656:
	movq	(%rbx), %rax
	movl	$12, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	16(%rax), %r8
	movq	8(%rax), %rcx
	movq	72(%rax), %r9
	movq	80(%rax), %r10
	movq	%r8, -2192(%rbp)
	movq	(%rax), %rbx
	movq	%rcx, -2168(%rbp)
	movq	%r9, -2184(%rbp)
	movq	%r10, -2176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-2176(%rbp), %r10
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-2184(%rbp), %r9
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6ObjectEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -2176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-2176(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal21UnsafeCast5ATSmi_1410EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movq	%rbx, %xmm0
	movl	$32, %edi
	movq	-2192(%rbp), %r8
	movhps	-2168(%rbp), %xmm0
	movq	%rax, -168(%rbp)
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movq	%r8, -176(%rbp)
	movaps	%xmm0, -1952(%rbp)
	movq	$0, -1936(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm5
	movdqa	-176(%rbp), %xmm7
	movq	%r13, %rsi
	movq	-2128(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -1952(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L657
	call	_ZdlPv@PLT
.L657:
	leaq	-520(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L714:
	leaq	-520(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movq	-2128(%rbp), %rdi
	movq	%r13, %rsi
	movl	$101058311, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L659
	call	_ZdlPv@PLT
.L659:
	movq	(%rbx), %rax
	movl	$9, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rbx
	movq	(%rax), %r14
	movq	%rbx, -2168(%rbp)
	movq	16(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rax, -2176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %xmm0
	movl	$32, %edi
	movq	$0, -1936(%rbp)
	movhps	-2168(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	%rbx, %xmm0
	movhps	-2176(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm6
	movdqa	-176(%rbp), %xmm5
	movq	%r13, %rsi
	movq	-2104(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -1952(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L660
	call	_ZdlPv@PLT
.L660:
	leaq	-328(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L658
.L715:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22410:
	.size	_ZN2v88internal45LoadElement23ATFastPackedSmiElements5ATSmi_28EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE, .-_ZN2v88internal45LoadElement23ATFastPackedSmiElements5ATSmi_28EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE:
.LFB26918:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	$4, %edi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	leaq	-80(%rbp), %rsi
	movl	$134612743, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L717
	call	_ZdlPv@PLT
.L717:
	movq	(%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L718
	movq	%rdx, (%r14)
.L718:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L719
	movq	%rdx, 0(%r13)
.L719:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L720
	movq	%rdx, (%r12)
.L720:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L716
	movq	%rax, (%rbx)
.L716:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L739
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L739:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26918:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE
	.section	.text._ZN2v88internal134LoadElement26ATFastPackedObjectElements90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_29EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal134LoadElement26ATFastPackedObjectElements90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_29EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE
	.type	_ZN2v88internal134LoadElement26ATFastPackedObjectElements90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_29EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE, @function
_ZN2v88internal134LoadElement26ATFastPackedObjectElements90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_29EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE:
.LFB22443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1920(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$3, %edx
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-2096(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$2200, %rsp
	movq	%rsi, -2168(%rbp)
	movq	%r12, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -2096(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1728(%rbp), %rax
	movl	$15, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -2160(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1536(%rbp), %rax
	movl	$15, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1344(%rbp), %rax
	movl	$17, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -2120(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1152(%rbp), %rax
	movl	$10, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -2152(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-960(%rbp), %rax
	movl	$12, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -2128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-768(%rbp), %rax
	movl	$11, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -2112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-576(%rbp), %rax
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -2144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-384(%rbp), %rax
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -2104(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-2168(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	%r13, -184(%rbp)
	leaq	-1952(%rbp), %r13
	movaps	%xmm0, -1952(%rbp)
	movq	%rax, -192(%rbp)
	movq	%rbx, -176(%rbp)
	movq	$0, -1936(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-192(%rbp), %xmm6
	leaq	24(%rax), %rdx
	movq	%rax, -1952(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L741
	call	_ZdlPv@PLT
.L741:
	leaq	-1864(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1856(%rbp)
	jne	.L804
	cmpq	$0, -1664(%rbp)
	jne	.L805
.L745:
	cmpq	$0, -1472(%rbp)
	jne	.L806
.L747:
	cmpq	$0, -1280(%rbp)
	jne	.L807
.L749:
	cmpq	$0, -1088(%rbp)
	jne	.L808
.L751:
	cmpq	$0, -896(%rbp)
	jne	.L809
.L753:
	cmpq	$0, -704(%rbp)
	jne	.L810
.L756:
	cmpq	$0, -512(%rbp)
	leaq	-328(%rbp), %r14
	jne	.L811
.L759:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movq	-2104(%rbp), %rdi
	movq	%r13, %rsi
	movl	$134612743, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L761
	call	_ZdlPv@PLT
.L761:
	movq	(%rbx), %rax
	movq	-2104(%rbp), %rdi
	movq	24(%rax), %r13
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2144(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2112(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2128(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2152(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2120(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2136(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2160(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L812
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L804:
	.cfi_restore_state
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	$0, -1976(%rbp)
	movq	$0, -1968(%rbp)
	movq	$0, -1960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1960(%rbp), %rcx
	leaq	-1968(%rbp), %rdx
	movq	%r15, %rdi
	leaq	-1976(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EE
	movl	$17, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1968(%rbp), %rdx
	movq	-1976(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal27UnsafeCast10FixedArray_1409EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$18, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -2168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%rax, -2184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-2168(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$55, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -2176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1960(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$56, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$37, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	-2176(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -2208(%rbp)
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -2192(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2208(%rbp), %r8
	movq	-2192(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, -2192(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-72(%rbp), %rdx
	pxor	%xmm1, %xmm1
	movq	%r13, %rdi
	movq	-2168(%rbp), %rax
	movq	-2176(%rbp), %rcx
	leaq	-192(%rbp), %r8
	movq	%rdx, -2224(%rbp)
	movq	-1976(%rbp), %xmm7
	movq	%r8, %rsi
	movq	%rbx, -120(%rbp)
	movq	-1960(%rbp), %xmm0
	movq	%rax, -168(%rbp)
	movq	%rax, -160(%rbp)
	movq	-2184(%rbp), %rax
	movhps	-1968(%rbp), %xmm7
	movq	%rcx, -144(%rbp)
	movq	-2168(%rbp), %rcx
	movq	%rax, -152(%rbp)
	movq	%rax, -104(%rbp)
	movq	-2176(%rbp), %rax
	movq	%r8, -2216(%rbp)
	movq	%xmm0, -176(%rbp)
	movq	%xmm0, -136(%rbp)
	movq	%xmm0, -128(%rbp)
	movq	%xmm0, -2232(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%rax, -96(%rbp)
	movaps	%xmm7, -2208(%rbp)
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm1, -1952(%rbp)
	movq	%rbx, -88(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -1936(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2160(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	movq	-2216(%rbp), %r8
	movq	-2232(%rbp), %xmm0
	movq	-2224(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L743
	movq	%r8, -2232(%rbp)
	movq	%xmm0, -2216(%rbp)
	call	_ZdlPv@PLT
	movq	-2232(%rbp), %r8
	movq	-2224(%rbp), %rdx
	movq	-2216(%rbp), %xmm0
.L743:
	movdqa	%xmm0, %xmm2
	movq	%r8, %rsi
	movq	%r13, %rdi
	movq	%rbx, -80(%rbp)
	movdqa	-2208(%rbp), %xmm6
	movq	-2168(%rbp), %xmm5
	movq	$0, -1936(%rbp)
	movaps	%xmm6, -192(%rbp)
	punpcklqdq	%xmm5, %xmm2
	movdqa	%xmm5, %xmm1
	movq	-2176(%rbp), %xmm6
	movq	%rbx, %xmm5
	movaps	%xmm2, -176(%rbp)
	movhps	-2184(%rbp), %xmm1
	movdqa	%xmm6, %xmm2
	movaps	%xmm1, -160(%rbp)
	punpcklqdq	%xmm0, %xmm2
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	movdqa	%xmm6, %xmm0
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1952(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2136(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L744
	call	_ZdlPv@PLT
.L744:
	movq	-2192(%rbp), %rsi
	leaq	-1480(%rbp), %rcx
	leaq	-1672(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1664(%rbp)
	je	.L745
.L805:
	leaq	-1672(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -2072(%rbp)
	movq	$0, -2064(%rbp)
	movq	$0, -2056(%rbp)
	movq	$0, -2048(%rbp)
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movq	$0, -2024(%rbp)
	movq	$0, -2016(%rbp)
	movq	$0, -2008(%rbp)
	movq	$0, -2000(%rbp)
	movq	$0, -1992(%rbp)
	movq	$0, -1984(%rbp)
	movq	$0, -1976(%rbp)
	movq	$0, -1968(%rbp)
	movq	$0, -1960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1960(%rbp), %rax
	movq	-2160(%rbp), %rdi
	leaq	-2040(%rbp), %r9
	pushq	%rax
	leaq	-1968(%rbp), %rax
	leaq	-2048(%rbp), %r8
	pushq	%rax
	leaq	-1976(%rbp), %rax
	leaq	-2056(%rbp), %rcx
	pushq	%rax
	leaq	-1984(%rbp), %rax
	leaq	-2064(%rbp), %rdx
	pushq	%rax
	leaq	-1992(%rbp), %rax
	leaq	-2072(%rbp), %rsi
	pushq	%rax
	leaq	-2000(%rbp), %rax
	pushq	%rax
	leaq	-2008(%rbp), %rax
	pushq	%rax
	leaq	-2016(%rbp), %rax
	pushq	%rax
	leaq	-2024(%rbp), %rax
	pushq	%rax
	leaq	-2032(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_10FixedArrayES6_NS0_7IntPtrTES7_S5_S5_S7_NS0_10HeapObjectES7_S7_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EESK_SG_SG_SK_PNSA_IS8_EESK_SK_SK_SK_
	addq	$80, %rsp
	movl	$39, %edx
	movq	%r12, %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1960(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrMulENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1984(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$38, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	-1992(%rbp), %rdx
	call	_ZN2v88internal45UnsafeNewReference20UT5ATSmi10HeapObject_1411EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	pxor	%xmm0, %xmm0
	leaq	-56(%rbp), %rdx
	movq	%r13, %rdi
	movq	-2072(%rbp), %rax
	leaq	-192(%rbp), %rsi
	movq	%rax, -192(%rbp)
	movq	-2064(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-2056(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-2048(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-2040(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-2032(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-2024(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-2016(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-2008(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-2000(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-1992(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-1984(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-1976(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-1968(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-1960(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-1952(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-1944(%rbp), %rax
	movaps	%xmm0, -1952(%rbp)
	movq	%rax, -64(%rbp)
	movq	$0, -1936(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2120(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L746
	call	_ZdlPv@PLT
.L746:
	leaq	-1288(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1472(%rbp)
	je	.L747
.L806:
	leaq	-1480(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -2072(%rbp)
	movq	$0, -2064(%rbp)
	movq	$0, -2056(%rbp)
	movq	$0, -2048(%rbp)
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movq	$0, -2024(%rbp)
	movq	$0, -2016(%rbp)
	movq	$0, -2008(%rbp)
	movq	$0, -2000(%rbp)
	movq	$0, -1992(%rbp)
	movq	$0, -1984(%rbp)
	movq	$0, -1976(%rbp)
	movq	$0, -1968(%rbp)
	movq	$0, -1960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1960(%rbp), %rax
	movq	-2136(%rbp), %rdi
	leaq	-2056(%rbp), %rcx
	pushq	%rax
	leaq	-1968(%rbp), %rax
	leaq	-2040(%rbp), %r9
	pushq	%rax
	leaq	-1976(%rbp), %rax
	leaq	-2048(%rbp), %r8
	pushq	%rax
	leaq	-1984(%rbp), %rax
	leaq	-2064(%rbp), %rdx
	pushq	%rax
	leaq	-1992(%rbp), %rax
	leaq	-2072(%rbp), %rsi
	pushq	%rax
	leaq	-2000(%rbp), %rax
	pushq	%rax
	leaq	-2008(%rbp), %rax
	pushq	%rax
	leaq	-2016(%rbp), %rax
	pushq	%rax
	leaq	-2024(%rbp), %rax
	pushq	%rax
	leaq	-2032(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_10FixedArrayES6_NS0_7IntPtrTES7_S5_S5_S7_NS0_10HeapObjectES7_S7_S7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EESK_SG_SG_SK_PNSA_IS8_EESK_SK_SK_SK_
	addq	$80, %rsp
	movl	$41, %edx
	movq	%r12, %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-192(%rbp), %rsi
	leaq	-112(%rbp), %rdx
	movq	%r13, %rdi
	movq	-2008(%rbp), %xmm0
	movq	-2024(%rbp), %xmm1
	movq	$0, -1936(%rbp)
	movq	-2040(%rbp), %xmm2
	movq	-2056(%rbp), %xmm3
	movq	-2072(%rbp), %xmm4
	movhps	-2000(%rbp), %xmm0
	movhps	-2016(%rbp), %xmm1
	movhps	-2032(%rbp), %xmm2
	movhps	-2048(%rbp), %xmm3
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-2064(%rbp), %xmm4
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2152(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L748
	call	_ZdlPv@PLT
.L748:
	leaq	-1096(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1280(%rbp)
	je	.L749
.L807:
	leaq	-1288(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -2088(%rbp)
	movq	$0, -2080(%rbp)
	movq	$0, -2072(%rbp)
	movq	$0, -2064(%rbp)
	movq	$0, -2056(%rbp)
	movq	$0, -2048(%rbp)
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movq	$0, -2024(%rbp)
	movq	$0, -2016(%rbp)
	movq	$0, -2008(%rbp)
	movq	$0, -2000(%rbp)
	movq	$0, -1992(%rbp)
	movq	$0, -1984(%rbp)
	movq	$0, -1976(%rbp)
	movq	$0, -1968(%rbp)
	movq	$0, -1960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1960(%rbp), %rax
	movq	-2120(%rbp), %rdi
	leaq	-2072(%rbp), %rcx
	pushq	%rax
	leaq	-1968(%rbp), %rax
	leaq	-2056(%rbp), %r9
	pushq	%rax
	leaq	-1976(%rbp), %rax
	leaq	-2064(%rbp), %r8
	pushq	%rax
	leaq	-1984(%rbp), %rax
	leaq	-2080(%rbp), %rdx
	pushq	%rax
	leaq	-1992(%rbp), %rax
	leaq	-2088(%rbp), %rsi
	pushq	%rax
	leaq	-2000(%rbp), %rax
	pushq	%rax
	leaq	-2008(%rbp), %rax
	pushq	%rax
	leaq	-2016(%rbp), %rax
	pushq	%rax
	leaq	-2024(%rbp), %rax
	pushq	%rax
	leaq	-2032(%rbp), %rax
	pushq	%rax
	leaq	-2040(%rbp), %rax
	pushq	%rax
	leaq	-2048(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_10FixedArrayES6_NS0_7IntPtrTES7_S5_S5_S7_NS0_10HeapObjectES7_S7_S7_S7_S8_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EESI_PNSA_IS7_EESK_SG_SG_SK_PNSA_IS8_EESK_SK_SK_SK_SM_SK_
	addq	$96, %rsp
	movl	$56, %edx
	movq	%r12, %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-192(%rbp), %rsi
	leaq	-96(%rbp), %rdx
	movq	%r13, %rdi
	movq	-1968(%rbp), %xmm0
	movq	-2024(%rbp), %xmm1
	movq	$0, -1936(%rbp)
	movq	-2040(%rbp), %xmm2
	movq	-2056(%rbp), %xmm3
	movq	-2072(%rbp), %xmm4
	movhps	-1960(%rbp), %xmm0
	movq	-2088(%rbp), %xmm5
	movhps	-2016(%rbp), %xmm1
	movhps	-2032(%rbp), %xmm2
	movhps	-2048(%rbp), %xmm3
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-2064(%rbp), %xmm4
	movhps	-2080(%rbp), %xmm5
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm5, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L750
	call	_ZdlPv@PLT
.L750:
	leaq	-904(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1088(%rbp)
	je	.L751
.L808:
	leaq	-1096(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movl	$1286, %esi
	movabsq	$433758466851866375, %rcx
	movq	-2152(%rbp), %rdi
	movw	%si, 8(%rax)
	leaq	10(%rax), %rdx
	movq	%r13, %rsi
	movq	%rcx, (%rax)
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L752
	call	_ZdlPv@PLT
.L752:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -896(%rbp)
	je	.L753
.L809:
	leaq	-904(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$433758466851866375, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$12, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movq	-2128(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	leaq	12(%rax), %rdx
	movl	$84346118, 8(%rax)
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L754
	call	_ZdlPv@PLT
.L754:
	movq	(%rbx), %rax
	leaq	-192(%rbp), %rsi
	leaq	-104(%rbp), %rdx
	movq	%r13, %rdi
	movdqu	80(%rax), %xmm6
	movdqu	64(%rax), %xmm0
	movdqu	(%rax), %xmm4
	movdqu	16(%rax), %xmm3
	movdqu	32(%rax), %xmm2
	movdqu	48(%rax), %xmm1
	punpcklqdq	%xmm6, %xmm0
	movq	88(%rax), %rax
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -192(%rbp)
	movq	%rax, -112(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -1952(%rbp)
	movq	$0, -1936(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2112(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L755
	call	_ZdlPv@PLT
.L755:
	leaq	-712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -704(%rbp)
	je	.L756
.L810:
	leaq	-712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$11, %edi
	movq	$0, -1936(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movq	-2112(%rbp), %rdi
	movq	%r13, %rsi
	movabsq	$433758466851866375, %rcx
	movq	%rcx, (%rax)
	movl	$1798, %ecx
	leaq	11(%rax), %rdx
	movw	%cx, 8(%rax)
	movb	$5, 10(%rax)
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1952(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L757
	call	_ZdlPv@PLT
.L757:
	movq	(%rbx), %rax
	movl	$18, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	16(%rax), %r8
	movq	8(%rax), %rcx
	movq	72(%rax), %r9
	movq	80(%rax), %r10
	movq	%r8, -2192(%rbp)
	movq	(%rax), %rbx
	movq	%rcx, -2168(%rbp)
	movq	%r9, -2184(%rbp)
	movq	%r10, -2176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-2176(%rbp), %r10
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-2184(%rbp), %r9
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6ObjectEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -2176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-2176(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal107UnsafeCast90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_1412EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movq	%rbx, %xmm0
	movl	$32, %edi
	movq	-2192(%rbp), %r8
	movhps	-2168(%rbp), %xmm0
	movq	%rax, -168(%rbp)
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movq	%r8, -176(%rbp)
	movaps	%xmm0, -1952(%rbp)
	movq	$0, -1936(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm5
	movdqa	-176(%rbp), %xmm7
	movq	%r13, %rsi
	movq	-2144(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -1952(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L758
	call	_ZdlPv@PLT
.L758:
	leaq	-520(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L811:
	leaq	-520(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -1984(%rbp)
	movq	$0, -1976(%rbp)
	movq	$0, -1968(%rbp)
	movq	$0, -1960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2144(%rbp), %rdi
	leaq	-1968(%rbp), %rcx
	leaq	-1960(%rbp), %r8
	leaq	-1976(%rbp), %rdx
	leaq	-1984(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE
	movl	$15, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$32, %edi
	movq	-1968(%rbp), %xmm0
	movq	-1984(%rbp), %xmm1
	movq	$0, -1936(%rbp)
	movhps	-1960(%rbp), %xmm0
	movhps	-1976(%rbp), %xmm1
	movaps	%xmm0, -176(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -192(%rbp)
	movaps	%xmm0, -1952(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm6
	movdqa	-176(%rbp), %xmm5
	movq	%r13, %rsi
	movq	-2104(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -1952(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L760
	call	_ZdlPv@PLT
.L760:
	leaq	-328(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L759
.L812:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22443:
	.size	_ZN2v88internal134LoadElement26ATFastPackedObjectElements90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_29EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE, .-_ZN2v88internal134LoadElement26ATFastPackedObjectElements90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_29EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_6ObjectENS0_10FixedArrayES7_NS0_7IntPtrTES8_S5_S5_S8_NS0_10HeapObjectES8_S8_S8_S8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EESL_PNSB_IS8_EESN_SH_SH_SN_PNSB_IS9_EESN_SN_SN_SN_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_6ObjectENS0_10FixedArrayES7_NS0_7IntPtrTES8_S5_S5_S8_NS0_10HeapObjectES8_S8_S8_S8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EESL_PNSB_IS8_EESN_SH_SH_SN_PNSB_IS9_EESN_SN_SN_SN_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_6ObjectENS0_10FixedArrayES7_NS0_7IntPtrTES8_S5_S5_S8_NS0_10HeapObjectES8_S8_S8_S8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EESL_PNSB_IS8_EESN_SH_SH_SN_PNSB_IS9_EESN_SN_SN_SN_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_6ObjectENS0_10FixedArrayES7_NS0_7IntPtrTES8_S5_S5_S8_NS0_10HeapObjectES8_S8_S8_S8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EESL_PNSB_IS8_EESN_SH_SH_SN_PNSB_IS9_EESN_SN_SN_SN_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_6ObjectENS0_10FixedArrayES7_NS0_7IntPtrTES8_S5_S5_S8_NS0_10HeapObjectES8_S8_S8_S8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EESL_PNSB_IS8_EESN_SH_SH_SN_PNSB_IS9_EESN_SN_SN_SN_:
.LFB26936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$16, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$152, %rsp
	movq	16(%rbp), %rax
	movq	%rdx, -88(%rbp)
	movq	88(%rbp), %r15
	movq	96(%rbp), %r14
	movq	%r8, -96(%rbp)
	movq	%rax, -112(%rbp)
	movq	24(%rbp), %rax
	movq	%r9, -104(%rbp)
	movq	%rax, -120(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movabsq	$361703071853971207, %rsi
	movabsq	$361700864223938054, %rdi
	movq	%rsi, (%rax)
	leaq	16(%rax), %rdx
	leaq	-80(%rbp), %rsi
	movq	%rdi, 8(%rax)
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L814
	movq	%rax, -184(%rbp)
	call	_ZdlPv@PLT
	movq	-184(%rbp), %rax
.L814:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L815
	movq	%rdx, 0(%r13)
.L815:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L816
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L816:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L817
	movq	%rdx, (%rbx)
.L817:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L818
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L818:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L819
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L819:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L820
	movq	-112(%rbp), %rcx
	movq	%rdx, (%rcx)
.L820:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L821
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L821:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L822
	movq	-128(%rbp), %rcx
	movq	%rdx, (%rcx)
.L822:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L823
	movq	-136(%rbp), %rbx
	movq	%rdx, (%rbx)
.L823:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L824
	movq	-144(%rbp), %rcx
	movq	%rdx, (%rcx)
.L824:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L825
	movq	-152(%rbp), %rbx
	movq	%rdx, (%rbx)
.L825:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L826
	movq	-160(%rbp), %rsi
	movq	%rdx, (%rsi)
.L826:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L827
	movq	-168(%rbp), %rcx
	movq	%rdx, (%rcx)
.L827:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L828
	movq	-176(%rbp), %rbx
	movq	%rdx, (%rbx)
.L828:
	movq	112(%rax), %rdx
	testq	%rdx, %rdx
	je	.L829
	movq	%rdx, (%r15)
.L829:
	movq	120(%rax), %rax
	testq	%rax, %rax
	je	.L813
	movq	%rax, (%r14)
.L813:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L884
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L884:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26936:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_6ObjectENS0_10FixedArrayES7_NS0_7IntPtrTES8_S5_S5_S8_NS0_10HeapObjectES8_S8_S8_S8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EESL_PNSB_IS8_EESN_SH_SH_SN_PNSB_IS9_EESN_SN_SN_SN_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_6ObjectENS0_10FixedArrayES7_NS0_7IntPtrTES8_S5_S5_S8_NS0_10HeapObjectES8_S8_S8_S8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EESL_PNSB_IS8_EESN_SH_SH_SN_PNSB_IS9_EESN_SN_SN_SN_
	.section	.text._ZN2v88internal135StoreElement26ATFastPackedObjectElements90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_32EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal135StoreElement26ATFastPackedObjectElements90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_32EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal135StoreElement26ATFastPackedObjectElements90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_32EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal135StoreElement26ATFastPackedObjectElements90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_32EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEENS4_INS0_6ObjectEEE:
.LFB22464:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1936(%rbp), %r15
	leaq	-2104(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1968(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$2264, %rsp
	movq	%rcx, -2200(%rbp)
	movq	%r8, -2208(%rbp)
	movq	%rsi, -2184(%rbp)
	movq	%r14, %rsi
	movq	%rdx, -2192(%rbp)
	movl	$4, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -2104(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1744(%rbp), %rax
	movl	$16, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -2128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1552(%rbp), %rax
	movl	$16, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -2160(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1360(%rbp), %rax
	movl	$18, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -2144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1168(%rbp), %rax
	movl	$11, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -2176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-976(%rbp), %rax
	movl	$13, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -2152(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-784(%rbp), %rax
	movl	$12, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-592(%rbp), %rax
	movl	$4, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -2168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-400(%rbp), %rax
	movl	$4, %edx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -2120(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movl	$32, %edi
	movq	-2200(%rbp), %xmm0
	movq	-2184(%rbp), %xmm1
	movq	$0, -1952(%rbp)
	movhps	-2208(%rbp), %xmm0
	movhps	-2192(%rbp), %xmm1
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -1968(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm2
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-192(%rbp), %xmm3
	leaq	32(%rax), %rdx
	movq	%rax, -1968(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -1952(%rbp)
	movq	%rdx, -1960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L886
	call	_ZdlPv@PLT
.L886:
	leaq	-1880(%rbp), %r12
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1872(%rbp)
	jne	.L953
	cmpq	$0, -1680(%rbp)
	jne	.L954
.L890:
	cmpq	$0, -1488(%rbp)
	jne	.L955
.L892:
	cmpq	$0, -1296(%rbp)
	jne	.L956
.L894:
	cmpq	$0, -1104(%rbp)
	jne	.L957
.L897:
	cmpq	$0, -912(%rbp)
	jne	.L958
.L899:
	cmpq	$0, -720(%rbp)
	jne	.L959
.L902:
	cmpq	$0, -528(%rbp)
	leaq	-344(%rbp), %r12
	jne	.L960
.L905:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1952(%rbp)
	movaps	%xmm0, -1968(%rbp)
	call	_Znwm@PLT
	movq	-2120(%rbp), %rdi
	movq	%r13, %rsi
	movl	$134612743, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1968(%rbp)
	movq	%rdx, -1952(%rbp)
	movq	%rdx, -1960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L907
	call	_ZdlPv@PLT
.L907:
	movq	-2120(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2168(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2136(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2152(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2176(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2144(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2160(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-2128(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L961
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L953:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	$0, -2000(%rbp)
	movq	$0, -1992(%rbp)
	movq	$0, -1984(%rbp)
	movq	$0, -1976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1976(%rbp), %r8
	movq	%r15, %rdi
	leaq	-1984(%rbp), %rcx
	leaq	-1992(%rbp), %rdx
	leaq	-2000(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE
	movl	$44, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1992(%rbp), %rdx
	movq	-2000(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal27UnsafeCast10FixedArray_1409EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$45, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -2200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$8, %esi
	movq	%r14, %rdi
	movq	%rax, -2208(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-2200(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$55, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -2192(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1984(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$56, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -2184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$37, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2184(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	-2192(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, -2216(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2216(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	-208(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, -2216(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-80(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	-2208(%rbp), %rcx
	movq	-2192(%rbp), %rax
	movq	%r13, %rdi
	movq	%rdx, -2224(%rbp)
	movq	-1984(%rbp), %xmm1
	movaps	%xmm0, -1968(%rbp)
	movq	-2200(%rbp), %xmm7
	movq	-2000(%rbp), %xmm5
	movq	%rcx, -160(%rbp)
	movq	-2184(%rbp), %rcx
	movdqa	%xmm1, %xmm4
	movdqa	%xmm7, %xmm6
	movq	%rax, -152(%rbp)
	movq	%rax, -104(%rbp)
	movq	-2184(%rbp), %rax
	movhps	-1976(%rbp), %xmm4
	punpcklqdq	%xmm6, %xmm6
	movq	%rcx, -128(%rbp)
	movq	-2208(%rbp), %rcx
	movhps	-1992(%rbp), %xmm5
	movq	%xmm1, -144(%rbp)
	movq	%xmm1, -136(%rbp)
	movq	%xmm1, -2280(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rax, -88(%rbp)
	movaps	%xmm4, -2272(%rbp)
	movaps	%xmm5, -2256(%rbp)
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm6, -2240(%rbp)
	movaps	%xmm6, -176(%rbp)
	movq	%xmm7, -120(%rbp)
	movq	$0, -1952(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2128(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1968(%rbp), %rdi
	movq	-2224(%rbp), %rdx
	movq	-2280(%rbp), %xmm1
	testq	%rdi, %rdi
	je	.L888
	movq	%rdx, -2280(%rbp)
	movq	%xmm1, -2224(%rbp)
	call	_ZdlPv@PLT
	movq	-2280(%rbp), %rdx
	movq	-2224(%rbp), %xmm1
.L888:
	movdqa	-2256(%rbp), %xmm7
	punpcklqdq	%xmm1, %xmm1
	movq	%r12, %rsi
	movq	-2208(%rbp), %xmm0
	movdqa	-2272(%rbp), %xmm2
	movdqa	-2240(%rbp), %xmm3
	movaps	%xmm1, -144(%rbp)
	movq	%r13, %rdi
	movaps	%xmm7, -208(%rbp)
	movq	-2184(%rbp), %xmm7
	movhps	-2192(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -112(%rbp)
	movdqa	%xmm7, %xmm0
	movdqa	%xmm7, %xmm1
	punpcklqdq	%xmm0, %xmm0
	movhps	-2200(%rbp), %xmm1
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -1968(%rbp)
	movq	$0, -1952(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2160(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L889
	call	_ZdlPv@PLT
.L889:
	movq	-2216(%rbp), %rsi
	leaq	-1496(%rbp), %rcx
	leaq	-1688(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1680(%rbp)
	je	.L890
.L954:
	leaq	-1688(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -2096(%rbp)
	movq	$0, -2088(%rbp)
	movq	$0, -2080(%rbp)
	movq	$0, -2072(%rbp)
	movq	$0, -2064(%rbp)
	movq	$0, -2056(%rbp)
	movq	$0, -2048(%rbp)
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movq	$0, -2024(%rbp)
	movq	$0, -2016(%rbp)
	movq	$0, -2008(%rbp)
	movq	$0, -2000(%rbp)
	movq	$0, -1992(%rbp)
	movq	$0, -1984(%rbp)
	movq	$0, -1976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-1976(%rbp), %rax
	movq	-2128(%rbp), %rdi
	pushq	%rax
	leaq	-1984(%rbp), %rax
	leaq	-2064(%rbp), %r9
	pushq	%rax
	leaq	-1992(%rbp), %rax
	leaq	-2072(%rbp), %r8
	pushq	%rax
	leaq	-2000(%rbp), %rax
	leaq	-2080(%rbp), %rcx
	pushq	%rax
	leaq	-2008(%rbp), %rax
	leaq	-2088(%rbp), %rdx
	pushq	%rax
	leaq	-2016(%rbp), %rax
	leaq	-2096(%rbp), %rsi
	pushq	%rax
	leaq	-2024(%rbp), %rax
	pushq	%rax
	leaq	-2032(%rbp), %rax
	pushq	%rax
	leaq	-2040(%rbp), %rax
	pushq	%rax
	leaq	-2048(%rbp), %rax
	pushq	%rax
	leaq	-2056(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_6ObjectENS0_10FixedArrayES7_NS0_7IntPtrTES8_S5_S5_S8_NS0_10HeapObjectES8_S8_S8_S8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EESL_PNSB_IS8_EESN_SH_SH_SN_PNSB_IS9_EESN_SN_SN_SN_
	addq	$96, %rsp
	movl	$39, %edx
	movq	%r14, %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1976(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrMulENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2000(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$38, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rcx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	-2008(%rbp), %rdx
	call	_ZN2v88internal45UnsafeNewReference20UT5ATSmi10HeapObject_1411EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	leaq	-208(%rbp), %rsi
	leaq	-64(%rbp), %rdx
	movq	%r13, %rdi
	movq	-1984(%rbp), %xmm0
	movq	-2000(%rbp), %xmm1
	movq	$0, -1952(%rbp)
	movq	-2016(%rbp), %xmm2
	movq	-2032(%rbp), %xmm3
	movhps	-1976(%rbp), %xmm0
	movhps	-1992(%rbp), %xmm1
	movq	-2048(%rbp), %xmm4
	movq	-2064(%rbp), %xmm5
	movaps	%xmm0, -96(%rbp)
	movhps	-2008(%rbp), %xmm2
	movq	-1968(%rbp), %xmm0
	movq	-2080(%rbp), %xmm6
	movq	-2096(%rbp), %xmm7
	movhps	-2024(%rbp), %xmm3
	movaps	%xmm2, -128(%rbp)
	movhps	-2040(%rbp), %xmm4
	movhps	-1960(%rbp), %xmm0
	movhps	-2056(%rbp), %xmm5
	movhps	-2072(%rbp), %xmm6
	movaps	%xmm4, -160(%rbp)
	movhps	-2088(%rbp), %xmm7
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -1968(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2144(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L891
	call	_ZdlPv@PLT
.L891:
	leaq	-1304(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1488(%rbp)
	je	.L892
.L955:
	leaq	-1496(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -2096(%rbp)
	movq	$0, -2088(%rbp)
	movq	$0, -2080(%rbp)
	movq	$0, -2072(%rbp)
	movq	$0, -2064(%rbp)
	movq	$0, -2056(%rbp)
	movq	$0, -2048(%rbp)
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movq	$0, -2024(%rbp)
	movq	$0, -2016(%rbp)
	movq	$0, -2008(%rbp)
	movq	$0, -2000(%rbp)
	movq	$0, -1992(%rbp)
	movq	$0, -1984(%rbp)
	movq	$0, -1976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-1976(%rbp), %rax
	movq	-2160(%rbp), %rdi
	pushq	%rax
	leaq	-1984(%rbp), %rax
	leaq	-2080(%rbp), %rcx
	pushq	%rax
	leaq	-1992(%rbp), %rax
	leaq	-2064(%rbp), %r9
	pushq	%rax
	leaq	-2000(%rbp), %rax
	leaq	-2072(%rbp), %r8
	pushq	%rax
	leaq	-2008(%rbp), %rax
	leaq	-2088(%rbp), %rdx
	pushq	%rax
	leaq	-2016(%rbp), %rax
	leaq	-2096(%rbp), %rsi
	pushq	%rax
	leaq	-2024(%rbp), %rax
	pushq	%rax
	leaq	-2032(%rbp), %rax
	pushq	%rax
	leaq	-2040(%rbp), %rax
	pushq	%rax
	leaq	-2048(%rbp), %rax
	pushq	%rax
	leaq	-2056(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_6ObjectENS0_10FixedArrayES7_NS0_7IntPtrTES8_S5_S5_S8_NS0_10HeapObjectES8_S8_S8_S8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EESL_PNSB_IS8_EESN_SH_SH_SN_PNSB_IS9_EESN_SN_SN_SN_
	addq	$96, %rsp
	movl	$41, %edx
	movq	%r14, %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm0, %xmm0
	leaq	-120(%rbp), %rdx
	movq	%r13, %rdi
	movq	-2096(%rbp), %rax
	leaq	-208(%rbp), %rsi
	movaps	%xmm0, -1968(%rbp)
	movq	$0, -1952(%rbp)
	movq	%rax, -208(%rbp)
	movq	-2088(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-2080(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-2072(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-2064(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-2056(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-2048(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-2040(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-2032(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-2024(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-2016(%rbp), %rax
	movq	%rax, -128(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2176(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L893
	call	_ZdlPv@PLT
.L893:
	leaq	-1112(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1296(%rbp)
	je	.L894
.L956:
	leaq	-1304(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-208(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1287, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movdqa	.LC6(%rip), %xmm0
	movw	%dx, -192(%rbp)
	leaq	-190(%rbp), %rdx
	movq	$0, -1952(%rbp)
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1968(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2144(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L895
	movq	%rax, -2184(%rbp)
	call	_ZdlPv@PLT
	movq	-2184(%rbp), %rax
.L895:
	movq	(%rax), %rax
	movq	56(%rax), %r10
	movq	8(%rax), %rdx
	movq	24(%rax), %rsi
	movq	40(%rax), %rdi
	movq	(%rax), %rcx
	movq	64(%rax), %r11
	movq	%r10, -2272(%rbp)
	movq	72(%rax), %r10
	movq	%rdx, -2192(%rbp)
	movq	16(%rax), %rdx
	movq	80(%rax), %r9
	movq	%rsi, -2208(%rbp)
	movq	%rdi, -2240(%rbp)
	movq	32(%rax), %rsi
	movq	48(%rax), %rdi
	movq	%r10, -2280(%rbp)
	movq	128(%rax), %r10
	movq	136(%rax), %rax
	movq	%rdx, -2200(%rbp)
	movl	$56, %edx
	movq	%rsi, -2216(%rbp)
	leaq	.LC5(%rip), %rsi
	movq	%rdi, -2256(%rbp)
	movq	%r14, %rdi
	movq	%rcx, -2184(%rbp)
	movq	%r11, -2224(%rbp)
	movq	%r9, -2288(%rbp)
	movq	%r10, -2296(%rbp)
	movq	%rax, -2304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-104(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	-2184(%rbp), %xmm0
	movq	-2304(%rbp), %rax
	movq	$0, -1952(%rbp)
	movhps	-2192(%rbp), %xmm0
	movq	%rax, -112(%rbp)
	movaps	%xmm0, -208(%rbp)
	movq	-2200(%rbp), %xmm0
	movhps	-2208(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-2216(%rbp), %xmm0
	movhps	-2240(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-2256(%rbp), %xmm0
	movhps	-2272(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-2224(%rbp), %xmm0
	movhps	-2280(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-2288(%rbp), %xmm0
	movhps	-2296(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1968(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2152(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L896
	call	_ZdlPv@PLT
.L896:
	leaq	-920(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1104(%rbp)
	je	.L897
.L957:
	leaq	-1112(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-208(%rbp), %rsi
	movabsq	$361703071853971207, %rax
	movq	%rax, -208(%rbp)
	movq	%r13, %rdi
	movl	$1542, %eax
	leaq	-197(%rbp), %rdx
	movw	%ax, -200(%rbp)
	movaps	%xmm0, -1968(%rbp)
	movb	$5, -198(%rbp)
	movq	$0, -1952(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2176(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L898
	call	_ZdlPv@PLT
.L898:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -912(%rbp)
	je	.L899
.L958:
	leaq	-920(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-208(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	-195(%rbp), %rdx
	movabsq	$361703071853971207, %rax
	movaps	%xmm0, -1968(%rbp)
	movq	%rax, -208(%rbp)
	movl	$117769734, -200(%rbp)
	movb	$5, -196(%rbp)
	movq	$0, -1952(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2152(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L900
	movq	%rax, -2184(%rbp)
	call	_ZdlPv@PLT
	movq	-2184(%rbp), %rax
.L900:
	movq	(%rax), %rax
	pxor	%xmm0, %xmm0
	movq	8(%rax), %rdx
	movq	16(%rax), %rsi
	movq	(%rax), %rcx
	movq	24(%rax), %r11
	movq	32(%rax), %r10
	movq	40(%rax), %r9
	movq	%rdx, -2184(%rbp)
	movq	72(%rax), %rdx
	movq	48(%rax), %r8
	movq	%rsi, -2192(%rbp)
	movq	56(%rax), %rdi
	movq	64(%rax), %rsi
	movq	%rdx, -2200(%rbp)
	movq	88(%rax), %rdx
	movq	96(%rax), %rax
	movq	%rcx, -208(%rbp)
	movq	-2184(%rbp), %rcx
	movq	%rdi, -152(%rbp)
	movq	%r13, %rdi
	movq	%rsi, -144(%rbp)
	movq	%r12, %rsi
	movq	%rcx, -200(%rbp)
	movq	-2192(%rbp), %rcx
	movq	%rdx, -128(%rbp)
	leaq	-112(%rbp), %rdx
	movq	%rcx, -192(%rbp)
	movq	-2200(%rbp), %rcx
	movq	%r11, -184(%rbp)
	movq	%r10, -176(%rbp)
	movq	%r9, -168(%rbp)
	movq	%r8, -160(%rbp)
	movq	%rcx, -136(%rbp)
	movq	%rax, -120(%rbp)
	movaps	%xmm0, -1968(%rbp)
	movq	$0, -1952(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2136(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L901
	call	_ZdlPv@PLT
.L901:
	leaq	-728(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -720(%rbp)
	je	.L902
.L959:
	leaq	-728(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-208(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-196(%rbp), %rdx
	movabsq	$361703071853971207, %rax
	movaps	%xmm0, -1968(%rbp)
	movq	%rax, -208(%rbp)
	movl	$84346374, -200(%rbp)
	movq	$0, -1952(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-2136(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1968(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L903
	call	_ZdlPv@PLT
.L903:
	movq	(%r12), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	movq	8(%rax), %rdx
	movq	(%rax), %rcx
	movq	88(%rax), %r8
	movq	80(%rax), %r12
	movq	%rdx, -2200(%rbp)
	movq	16(%rax), %rdx
	movq	%rcx, -2192(%rbp)
	movq	24(%rax), %rcx
	movq	%rdx, -2208(%rbp)
	movl	$45, %edx
	movq	%rcx, -2184(%rbp)
	movq	%r8, -2216(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-2216(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdx
	movl	$8, %esi
	movq	%r13, %rdi
	movq	-2184(%rbp), %r8
	movq	%rax, %rcx
	movl	$2, %r9d
	call	_ZN2v88internal8compiler13CodeAssembler13StoreToObjectENS0_21MachineRepresentationENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEEPNS1_4NodeENS0_25StoreToObjectWriteBarrierE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$43, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$41, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$32, %edi
	movq	-2192(%rbp), %xmm0
	movq	$0, -1952(%rbp)
	movhps	-2200(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-2208(%rbp), %xmm0
	movhps	-2184(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1968(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm7
	movdqa	-192(%rbp), %xmm2
	movq	%r13, %rsi
	movq	-2168(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -1968(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -1952(%rbp)
	movq	%rdx, -1960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L904
	call	_ZdlPv@PLT
.L904:
	leaq	-536(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L902
	.p2align 4,,10
	.p2align 3
.L960:
	leaq	-536(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -2000(%rbp)
	movq	$0, -1992(%rbp)
	movq	$0, -1984(%rbp)
	movq	$0, -1976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2168(%rbp), %rdi
	leaq	-1984(%rbp), %rcx
	leaq	-1992(%rbp), %rdx
	leaq	-2000(%rbp), %rsi
	leaq	-1976(%rbp), %r8
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE
	movl	$32, %edi
	movq	-1984(%rbp), %xmm0
	movq	-2000(%rbp), %xmm1
	movq	$0, -1952(%rbp)
	movhps	-1976(%rbp), %xmm0
	movhps	-1992(%rbp), %xmm1
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -1968(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm3
	movdqa	-192(%rbp), %xmm4
	movq	%r13, %rsi
	movq	-2120(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -1968(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -1952(%rbp)
	movq	%rdx, -1960(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L906
	call	_ZdlPv@PLT
.L906:
	leaq	-344(%rbp), %r12
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L905
.L961:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22464:
	.size	_ZN2v88internal135StoreElement26ATFastPackedObjectElements90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_32EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal135StoreElement26ATFastPackedObjectElements90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_32EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EE:
.LFB26946:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$2, %edi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -64(%rbp)
	movq	$0, -48(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r12, %rdi
	leaq	-64(%rbp), %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -64(%rbp)
	movq	%rdx, -48(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-64(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L963
	call	_ZdlPv@PLT
.L963:
	movq	(%r12), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L964
	movq	%rdx, 0(%r13)
.L964:
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L962
	movq	%rax, (%rbx)
.L962:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L977
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L977:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26946:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS9_EESJ_SJ_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS9_EESJ_SJ_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS9_EESJ_SJ_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS9_EESJ_SJ_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS9_EESJ_SJ_:
.LFB26955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$6, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$2056, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$134678535, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L979
	movq	%rax, -104(%rbp)
	call	_ZdlPv@PLT
	movq	-104(%rbp), %rax
.L979:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L980
	movq	%rdx, (%r15)
.L980:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L981
	movq	%rdx, (%r14)
.L981:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L982
	movq	%rdx, 0(%r13)
.L982:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L983
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L983:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L984
	movq	%rdx, (%rbx)
.L984:
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L978
	movq	-96(%rbp), %rbx
	movq	%rax, (%rbx)
.L978:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1009
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1009:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26955:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS9_EESJ_SJ_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS9_EESJ_SJ_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS9_EESK_SK_SG_SG_PNSC_ISA_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS9_EESK_SK_SG_SG_PNSC_ISA_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS9_EESK_SK_SG_SG_PNSC_ISA_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS9_EESK_SK_SG_SG_PNSC_ISA_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS9_EESK_SK_SG_SG_PNSC_ISA_EE:
.LFB26962:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$9, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$578721382704547847, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1011
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
.L1011:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1012
	movq	%rdx, (%r15)
.L1012:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1013
	movq	%rdx, (%r14)
.L1013:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1014
	movq	%rdx, 0(%r13)
.L1014:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1015
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1015:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1016
	movq	%rdx, (%rbx)
.L1016:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1017
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1017:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1018
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1018:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1019
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1019:
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L1010
	movq	-120(%rbp), %rbx
	movq	%rax, (%rbx)
.L1010:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1053
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1053:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26962:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS9_EESK_SK_SG_SG_PNSC_ISA_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS9_EESK_SK_SG_SG_PNSC_ISA_EE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS9_EESK_SK_SG_SG_PNSC_ISA_EESM_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS9_EESK_SK_SG_SG_PNSC_ISA_EESM_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS9_EESK_SK_SG_SG_PNSC_ISA_EESM_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS9_EESK_SK_SG_SG_PNSC_ISA_EESM_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS9_EESK_SK_SG_SG_PNSC_ISA_EESM_:
.LFB26964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$10, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$578721382704547847, %rcx
	movq	%rcx, (%rax)
	movl	$1799, %ecx
	leaq	10(%rax), %rdx
	movw	%cx, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1055
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %rax
.L1055:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1056
	movq	%rdx, (%r15)
.L1056:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1057
	movq	%rdx, (%r14)
.L1057:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1058
	movq	%rdx, 0(%r13)
.L1058:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1059
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1059:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1060
	movq	%rdx, (%rbx)
.L1060:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1061
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1061:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1062
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1062:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1063
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1063:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1064
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1064:
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L1054
	movq	-128(%rbp), %rsi
	movq	%rax, (%rsi)
.L1054:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1101
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1101:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26964:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS9_EESK_SK_SG_SG_PNSC_ISA_EESM_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS9_EESK_SK_SG_SG_PNSC_ISA_EESM_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_NS0_5BoolTEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS9_EESL_SL_SH_SH_PNSD_ISA_EESN_PNSD_ISB_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_NS0_5BoolTEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS9_EESL_SL_SH_SH_PNSD_ISA_EESN_PNSD_ISB_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_NS0_5BoolTEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS9_EESL_SL_SH_SH_PNSD_ISA_EESN_PNSD_ISB_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_NS0_5BoolTEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS9_EESL_SL_SH_SH_PNSD_ISA_EESN_PNSD_ISB_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_NS0_5BoolTEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS9_EESL_SL_SH_SH_PNSD_ISA_EESN_PNSD_ISB_EE:
.LFB26966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$11, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$578721382704547847, %rcx
	movq	%rcx, (%rax)
	movl	$1799, %ecx
	leaq	11(%rax), %rdx
	movw	%cx, 8(%rax)
	movb	$4, 10(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1103
	movq	%rax, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %rax
.L1103:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1104
	movq	%rdx, (%r15)
.L1104:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1105
	movq	%rdx, (%r14)
.L1105:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1106
	movq	%rdx, 0(%r13)
.L1106:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1107
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1107:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1108
	movq	%rdx, (%rbx)
.L1108:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1109
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1109:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1110
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1110:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1111
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1111:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1112
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1112:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1113
	movq	-128(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1113:
	movq	80(%rax), %rax
	testq	%rax, %rax
	je	.L1102
	movq	-136(%rbp), %rcx
	movq	%rax, (%rcx)
.L1102:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1153
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1153:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26966:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_NS0_5BoolTEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS9_EESL_SL_SH_SH_PNSD_ISA_EESN_PNSD_ISB_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_NS0_5BoolTEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS9_EESL_SL_SH_SH_PNSD_ISA_EESN_PNSD_ISB_EE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_NS0_5BoolTESB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS9_EESL_SL_SH_SH_PNSD_ISA_EESN_PNSD_ISB_EESP_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_NS0_5BoolTESB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS9_EESL_SL_SH_SH_PNSD_ISA_EESN_PNSD_ISB_EESP_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_NS0_5BoolTESB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS9_EESL_SL_SH_SH_PNSD_ISA_EESN_PNSD_ISB_EESP_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_NS0_5BoolTESB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS9_EESL_SL_SH_SH_PNSD_ISA_EESN_PNSD_ISB_EESP_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_NS0_5BoolTESB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS9_EESL_SL_SH_SH_PNSD_ISA_EESN_PNSD_ISB_EESP_:
.LFB26968:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$12, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$120, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$578721382704547847, %rcx
	movq	%rcx, (%rax)
	leaq	12(%rax), %rdx
	movl	$67372807, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1155
	movq	%rax, -152(%rbp)
	call	_ZdlPv@PLT
	movq	-152(%rbp), %rax
.L1155:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1156
	movq	%rdx, (%r15)
.L1156:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1157
	movq	%rdx, (%r14)
.L1157:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1158
	movq	%rdx, 0(%r13)
.L1158:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1159
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1159:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1160
	movq	%rdx, (%rbx)
.L1160:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1161
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1161:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1162
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1162:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1163
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1163:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1164
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1164:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1165
	movq	-128(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1165:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1166
	movq	-136(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1166:
	movq	88(%rax), %rax
	testq	%rax, %rax
	je	.L1154
	movq	-144(%rbp), %rbx
	movq	%rax, (%rbx)
.L1154:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1209
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1209:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26968:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_NS0_5BoolTESB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS9_EESL_SL_SH_SH_PNSD_ISA_EESN_PNSD_ISB_EESP_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_NS0_5BoolTESB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS9_EESL_SL_SH_SH_PNSD_ISA_EESN_PNSD_ISB_EESP_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_:
.LFB26972:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	movl	$3, %edi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -64(%rbp)
	movq	$0, -48(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r14, %rdi
	leaq	-64(%rbp), %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -64(%rbp)
	movq	%rdx, -48(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-64(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L1211
	call	_ZdlPv@PLT
.L1211:
	movq	(%r14), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1212
	movq	%rdx, 0(%r13)
.L1212:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1213
	movq	%rdx, (%r12)
.L1213:
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L1210
	movq	%rax, (%rbx)
.L1210:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1229
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1229:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26972:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_
	.section	.text._ZN2v88internal22GenericArrayReverse_34EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22GenericArrayReverse_34EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal22GenericArrayReverse_34EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal22GenericArrayReverse_34EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE:
.LFB22481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$1720, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -5520(%rbp)
	leaq	-5496(%rbp), %r12
	leaq	-5344(%rbp), %rbx
	movq	%rdi, %r15
	movq	%rdx, -5760(%rbp)
	movq	%r12, %rsi
	movl	$2, %edx
	leaq	-5376(%rbp), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -5496(%rbp)
	movq	%rbx, %rdi
	movq	%rbx, -5728(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-5152(%rbp), %rax
	movl	$6, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5552(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-4960(%rbp), %rax
	movl	$6, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5608(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-4768(%rbp), %rax
	movl	$9, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5624(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-4576(%rbp), %rax
	movl	$9, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-4384(%rbp), %rax
	movl	$10, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5632(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-4192(%rbp), %rax
	movl	$10, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-4000(%rbp), %rax
	movl	$11, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5600(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3808(%rbp), %rax
	movl	$11, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3616(%rbp), %rax
	movl	$12, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5536(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3424(%rbp), %rax
	movl	$10, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5640(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3232(%rbp), %rax
	movl	$10, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3040(%rbp), %rax
	movl	$11, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5664(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2848(%rbp), %rax
	movl	$11, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5688(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2656(%rbp), %rax
	movl	$12, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5568(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2464(%rbp), %rax
	movl	$10, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2272(%rbp), %rax
	movl	$10, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2080(%rbp), %rax
	movl	$11, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1888(%rbp), %rax
	movl	$11, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5672(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1696(%rbp), %rax
	movl	$12, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1504(%rbp), %rax
	movl	$10, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5712(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1312(%rbp), %rax
	movl	$10, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1120(%rbp), %rax
	movl	$10, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-928(%rbp), %rax
	movl	$10, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5576(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-736(%rbp), %rax
	movl	$6, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-544(%rbp), %rax
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5720(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-352(%rbp), %rax
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5736(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movq	-5520(%rbp), %xmm0
	movaps	%xmm1, -5376(%rbp)
	movhps	-5760(%rbp), %xmm0
	movq	$0, -5360(%rbp)
	movaps	%xmm0, -5520(%rbp)
	call	_Znwm@PLT
	movdqa	-5520(%rbp), %xmm0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1231
	call	_ZdlPv@PLT
.L1231:
	leaq	-5288(%rbp), %rbx
	movq	%r12, %rdi
	leaq	-5408(%rbp), %r14
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-5392(%rbp), %rax
	cmpq	$0, -5280(%rbp)
	movq	%rax, -5520(%rbp)
	jne	.L1405
	cmpq	$0, -5088(%rbp)
	jne	.L1406
.L1234:
	cmpq	$0, -4896(%rbp)
	jne	.L1407
.L1237:
	cmpq	$0, -4704(%rbp)
	jne	.L1408
.L1240:
	cmpq	$0, -4512(%rbp)
	jne	.L1409
.L1242:
	cmpq	$0, -4320(%rbp)
	jne	.L1410
.L1245:
	cmpq	$0, -4128(%rbp)
	jne	.L1411
.L1247:
	cmpq	$0, -3936(%rbp)
	jne	.L1412
.L1250:
	cmpq	$0, -3744(%rbp)
	jne	.L1413
.L1252:
	cmpq	$0, -3552(%rbp)
	jne	.L1414
.L1254:
	cmpq	$0, -3360(%rbp)
	jne	.L1415
.L1257:
	cmpq	$0, -3168(%rbp)
	jne	.L1416
.L1259:
	cmpq	$0, -2976(%rbp)
	jne	.L1417
.L1262:
	cmpq	$0, -2784(%rbp)
	jne	.L1418
.L1264:
	cmpq	$0, -2592(%rbp)
	jne	.L1419
.L1266:
	cmpq	$0, -2400(%rbp)
	jne	.L1420
.L1269:
	cmpq	$0, -2208(%rbp)
	jne	.L1421
.L1271:
	cmpq	$0, -2016(%rbp)
	jne	.L1422
.L1274:
	cmpq	$0, -1824(%rbp)
	jne	.L1423
.L1276:
	cmpq	$0, -1632(%rbp)
	jne	.L1424
.L1278:
	cmpq	$0, -1440(%rbp)
	jne	.L1425
.L1281:
	cmpq	$0, -1248(%rbp)
	jne	.L1426
.L1283:
	cmpq	$0, -1056(%rbp)
	jne	.L1427
.L1285:
	cmpq	$0, -864(%rbp)
	jne	.L1428
.L1287:
	cmpq	$0, -672(%rbp)
	jne	.L1429
.L1289:
	cmpq	$0, -480(%rbp)
	leaq	-296(%rbp), %r15
	jne	.L1430
.L1291:
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	$0, -5408(%rbp)
	movq	$0, -5392(%rbp)
	movq	$0, -5376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-5736(%rbp), %rbx
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	-5520(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_
	movq	%rbx, %rdi
	movq	-5376(%rbp), %r13
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5720(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5680(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5576(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5584(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5592(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5712(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5560(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5672(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5656(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5704(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5696(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5568(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5688(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5664(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5648(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5640(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5536(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5616(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5600(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5528(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5632(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5544(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5624(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5608(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5552(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5728(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1431
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1405:
	.cfi_restore_state
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	$0, -5408(%rbp)
	movq	$0, -5392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-5520(%rbp), %rdx
	movq	-5728(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EE
	movl	$76, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5392(%rbp), %rdx
	movq	-5408(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ToObject_InlineENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$79, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-5408(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal21GetLengthProperty_244EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$88, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -5760(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$89, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -5792(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -5776(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5776(%rbp), %rdx
	movq	-5760(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberSubENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r13, %rdi
	movq	%rax, -5776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$91, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$48, %edi
	movq	-5408(%rbp), %xmm0
	movq	$0, -5360(%rbp)
	movhps	-5392(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%rbx, %xmm0
	movhps	-5760(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-5792(%rbp), %xmm0
	movhps	-5776(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -5376(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm7
	movq	-5552(%rbp), %rdi
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-144(%rbp), %xmm7
	movq	%rdx, -5360(%rbp)
	movups	%xmm7, 16(%rax)
	movdqa	-128(%rbp), %xmm7
	movq	%rdx, -5368(%rbp)
	movups	%xmm7, 32(%rax)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1233
	call	_ZdlPv@PLT
.L1233:
	leaq	-5096(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -5088(%rbp)
	je	.L1234
.L1406:
	leaq	-5096(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	pushq	-5520(%rbp)
	movq	%r14, %r9
	movq	-5552(%rbp), %rdi
	leaq	-5424(%rbp), %rcx
	leaq	-5416(%rbp), %r8
	leaq	-5432(%rbp), %rdx
	leaq	-5440(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS9_EESJ_SJ_
	movq	-5392(%rbp), %rdx
	movq	-5408(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal19NumberIsLessThan_75EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movq	-5408(%rbp), %xmm6
	movq	-5424(%rbp), %xmm7
	movq	%rax, %rbx
	movq	-5440(%rbp), %xmm5
	movaps	%xmm0, -5376(%rbp)
	movhps	-5392(%rbp), %xmm6
	movq	$0, -5360(%rbp)
	movhps	-5416(%rbp), %xmm7
	movhps	-5432(%rbp), %xmm5
	movaps	%xmm6, -5776(%rbp)
	movaps	%xmm7, -5760(%rbp)
	movaps	%xmm5, -5792(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm6
	movq	%r13, %rsi
	movq	-5608(%rbp), %rdi
	leaq	48(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-144(%rbp), %xmm7
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L1235
	call	_ZdlPv@PLT
.L1235:
	movdqa	-5792(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movdqa	-5760(%rbp), %xmm6
	movaps	%xmm0, -5376(%rbp)
	movaps	%xmm7, -160(%rbp)
	movdqa	-5776(%rbp), %xmm7
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm7, -128(%rbp)
	movq	$0, -5360(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm6
	movdqa	-144(%rbp), %xmm7
	movq	%r13, %rsi
	movq	-5680(%rbp), %rdi
	leaq	48(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-128(%rbp), %xmm6
	movups	%xmm7, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1236
	call	_ZdlPv@PLT
.L1236:
	leaq	-680(%rbp), %rcx
	leaq	-4904(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -4896(%rbp)
	je	.L1237
.L1407:
	leaq	-4904(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5416(%rbp), %rax
	movq	-5608(%rbp), %rdi
	pushq	%rax
	leaq	-5440(%rbp), %rcx
	leaq	-5432(%rbp), %r8
	leaq	-5424(%rbp), %r9
	leaq	-5448(%rbp), %rdx
	leaq	-5456(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS9_EESJ_SJ_
	movl	$92, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$93, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -5840(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$98, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -5824(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5456(%rbp), %r9
	movq	%r14, %rdi
	movq	-5424(%rbp), %rax
	movq	-5440(%rbp), %rbx
	movq	%r9, -5776(%rbp)
	movq	%rax, -5760(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$159, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-5376(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	xorl	%esi, %esi
	movq	%r14, %rdi
	leaq	-160(%rbp), %r10
	movl	$2, %ebx
	movq	%rax, %r8
	movq	-5776(%rbp), %r9
	pushq	%rbx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-5520(%rbp), %rdx
	movhps	-5760(%rbp), %xmm0
	pushq	%r10
	movl	$1, %ecx
	movq	%rax, -5392(%rbp)
	movq	-5360(%rbp), %rax
	movq	%r10, -5848(%rbp)
	movaps	%xmm0, -160(%rbp)
	movq	%rax, -5384(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$101, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -5760(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5760(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, -5760(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-88(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	-5424(%rbp), %xmm6
	movq	-5848(%rbp), %r10
	movq	%rdx, -5744(%rbp)
	movq	-5440(%rbp), %xmm7
	movaps	%xmm0, -5376(%rbp)
	movq	-5456(%rbp), %xmm5
	movhps	-5416(%rbp), %xmm6
	movq	%r10, %rsi
	movq	%rbx, -96(%rbp)
	movaps	%xmm6, -5792(%rbp)
	movhps	-5432(%rbp), %xmm7
	movhps	-5448(%rbp), %xmm5
	movaps	%xmm6, -128(%rbp)
	movq	-5840(%rbp), %xmm6
	movaps	%xmm7, -5808(%rbp)
	movhps	-5824(%rbp), %xmm6
	movq	%r10, -5840(%rbp)
	movaps	%xmm5, -5776(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm6, -5824(%rbp)
	movaps	%xmm6, -112(%rbp)
	movq	$0, -5360(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5624(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	movq	-5840(%rbp), %r10
	movq	-5744(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L1238
	call	_ZdlPv@PLT
	movq	-5744(%rbp), %rdx
	movq	-5840(%rbp), %r10
.L1238:
	pxor	%xmm0, %xmm0
	movq	%r10, %rsi
	movq	%r13, %rdi
	movq	%rbx, -96(%rbp)
	movdqa	-5776(%rbp), %xmm7
	movdqa	-5808(%rbp), %xmm6
	movaps	%xmm0, -5376(%rbp)
	movq	$0, -5360(%rbp)
	movaps	%xmm7, -160(%rbp)
	movdqa	-5792(%rbp), %xmm7
	movaps	%xmm6, -144(%rbp)
	movdqa	-5824(%rbp), %xmm6
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5544(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1239
	call	_ZdlPv@PLT
.L1239:
	movq	-5760(%rbp), %rsi
	leaq	-4520(%rbp), %rcx
	leaq	-4712(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -4704(%rbp)
	je	.L1240
.L1408:
	leaq	-4712(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -5480(%rbp)
	movq	$0, -5472(%rbp)
	movq	$0, -5464(%rbp)
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-5416(%rbp), %rax
	movq	-5624(%rbp), %rdi
	leaq	-5464(%rbp), %rcx
	pushq	%rax
	leaq	-5424(%rbp), %rax
	leaq	-5456(%rbp), %r8
	pushq	%rax
	leaq	-5432(%rbp), %rax
	leaq	-5448(%rbp), %r9
	pushq	%rax
	leaq	-5440(%rbp), %rax
	leaq	-5472(%rbp), %rdx
	pushq	%rax
	leaq	-5480(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS9_EESK_SK_SG_SG_PNSC_ISA_EE
	addq	$32, %rsp
	movl	$103, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5480(%rbp), %r9
	movq	%r14, %rdi
	movq	-5448(%rbp), %rax
	movq	-5464(%rbp), %rbx
	movq	%r9, -5776(%rbp)
	movq	%rax, -5760(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-5376(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	xorl	%esi, %esi
	movq	%r14, %rdi
	leaq	-160(%rbp), %r10
	movl	$2, %ebx
	movq	%rax, %r8
	movq	-5776(%rbp), %r9
	pushq	%rbx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-5520(%rbp), %rdx
	movhps	-5760(%rbp), %xmm0
	pushq	%r10
	movl	$1, %ecx
	movq	%rax, -5392(%rbp)
	movq	-5360(%rbp), %rax
	movq	%r10, -5760(%rbp)
	movaps	%xmm0, -160(%rbp)
	movq	%rax, -5384(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$101, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-88(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rbx, -112(%rbp)
	movq	-5424(%rbp), %rax
	movq	-5760(%rbp), %r10
	movq	$0, -5360(%rbp)
	movq	-5448(%rbp), %xmm0
	movq	-5464(%rbp), %xmm1
	movq	-5480(%rbp), %xmm2
	movq	%rax, -104(%rbp)
	movq	%r10, %rsi
	movq	-5416(%rbp), %rax
	movhps	-5440(%rbp), %xmm0
	movhps	-5456(%rbp), %xmm1
	movhps	-5472(%rbp), %xmm2
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -5376(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5544(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	popq	%r10
	popq	%r11
	testq	%rdi, %rdi
	je	.L1241
	call	_ZdlPv@PLT
.L1241:
	leaq	-4520(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -4512(%rbp)
	je	.L1242
.L1409:
	leaq	-4520(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -5480(%rbp)
	movq	$0, -5472(%rbp)
	movq	$0, -5464(%rbp)
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-5416(%rbp), %rax
	movq	-5544(%rbp), %rdi
	leaq	-5464(%rbp), %rcx
	pushq	%rax
	leaq	-5424(%rbp), %rax
	leaq	-5456(%rbp), %r8
	pushq	%rax
	leaq	-5432(%rbp), %rax
	leaq	-5448(%rbp), %r9
	pushq	%rax
	leaq	-5440(%rbp), %rax
	leaq	-5472(%rbp), %rdx
	pushq	%rax
	leaq	-5480(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS9_EESK_SK_SG_SG_PNSC_ISA_EE
	addq	$32, %rsp
	movl	$107, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5480(%rbp), %r9
	movq	%r14, %rdi
	movq	-5440(%rbp), %rax
	movq	-5464(%rbp), %rbx
	movq	%r9, -5776(%rbp)
	movq	%rax, -5760(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$159, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-5376(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	xorl	%esi, %esi
	movq	%r14, %rdi
	leaq	-160(%rbp), %rcx
	movl	$2, %ebx
	movq	%rax, %r8
	movq	-5776(%rbp), %r9
	pushq	%rbx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-5520(%rbp), %rdx
	movhps	-5760(%rbp), %xmm0
	pushq	%rcx
	movl	$1, %ecx
	movq	%rax, -5392(%rbp)
	movq	-5360(%rbp), %rax
	movaps	%xmm0, -160(%rbp)
	movq	%rax, -5384(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$110, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -5760(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5760(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, -5760(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-5416(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	-5432(%rbp), %xmm7
	movq	-5448(%rbp), %xmm3
	movl	$80, %edi
	movaps	%xmm0, -5376(%rbp)
	movq	-5464(%rbp), %xmm4
	movq	-5480(%rbp), %xmm2
	movhps	-5424(%rbp), %xmm7
	movq	%rax, -5776(%rbp)
	movhps	-5440(%rbp), %xmm3
	movhps	-5456(%rbp), %xmm4
	movaps	%xmm7, -5840(%rbp)
	movhps	-5472(%rbp), %xmm2
	movaps	%xmm3, -5824(%rbp)
	movaps	%xmm4, -5808(%rbp)
	movaps	%xmm2, -5792(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rbx, -88(%rbp)
	movq	$0, -5360(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-144(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm6
	movq	-5632(%rbp), %rdi
	leaq	80(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-112(%rbp), %xmm7
	movups	%xmm5, 16(%rax)
	movdqa	-96(%rbp), %xmm5
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	popq	%r8
	popq	%r9
	testq	%rdi, %rdi
	je	.L1243
	call	_ZdlPv@PLT
.L1243:
	movdqa	-5808(%rbp), %xmm7
	movdqa	-5792(%rbp), %xmm6
	movl	$80, %edi
	movq	$0, -5360(%rbp)
	movq	-5776(%rbp), %xmm0
	movdqa	-5824(%rbp), %xmm5
	movaps	%xmm7, -144(%rbp)
	movq	%rbx, %xmm7
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm6, -160(%rbp)
	movdqa	-5840(%rbp), %xmm6
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm0, -5376(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-144(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm7
	movq	-5528(%rbp), %rdi
	leaq	80(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movups	%xmm5, (%rax)
	movdqa	-112(%rbp), %xmm5
	movups	%xmm6, 16(%rax)
	movdqa	-96(%rbp), %xmm6
	movups	%xmm7, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1244
	call	_ZdlPv@PLT
.L1244:
	movq	-5760(%rbp), %rsi
	leaq	-4136(%rbp), %rcx
	leaq	-4328(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -4320(%rbp)
	je	.L1245
.L1410:
	leaq	-4328(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -5488(%rbp)
	movq	$0, -5480(%rbp)
	movq	$0, -5472(%rbp)
	movq	$0, -5464(%rbp)
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5416(%rbp), %rax
	movq	-5632(%rbp), %rdi
	pushq	%rax
	leaq	-5424(%rbp), %rax
	leaq	-5472(%rbp), %rcx
	pushq	%rax
	leaq	-5432(%rbp), %rax
	leaq	-5464(%rbp), %r8
	pushq	%rax
	leaq	-5440(%rbp), %rax
	leaq	-5456(%rbp), %r9
	pushq	%rax
	leaq	-5448(%rbp), %rax
	leaq	-5480(%rbp), %rdx
	pushq	%rax
	leaq	-5488(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS9_EESK_SK_SG_SG_PNSC_ISA_EESM_
	addq	$48, %rsp
	movl	$112, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5488(%rbp), %r9
	movq	%r14, %rdi
	movq	-5448(%rbp), %rax
	movq	-5472(%rbp), %rbx
	movq	%r9, -5776(%rbp)
	movq	%rax, -5760(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-5376(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	xorl	%esi, %esi
	movq	%r14, %rdi
	leaq	-160(%rbp), %rcx
	movl	$2, %ebx
	movq	%rax, %r8
	movq	-5776(%rbp), %r9
	pushq	%rbx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-5520(%rbp), %rdx
	movhps	-5760(%rbp), %xmm0
	pushq	%rcx
	movl	$1, %ecx
	movq	%rax, -5392(%rbp)
	movq	-5360(%rbp), %rax
	movaps	%xmm0, -160(%rbp)
	movq	%rax, -5384(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$110, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-5440(%rbp), %rax
	movl	$80, %edi
	movq	-5456(%rbp), %xmm0
	movq	-5472(%rbp), %xmm1
	movq	%rbx, -104(%rbp)
	movhps	-5448(%rbp), %xmm0
	movq	%rax, -112(%rbp)
	movq	-5488(%rbp), %xmm2
	movaps	%xmm0, -128(%rbp)
	movq	-5424(%rbp), %xmm0
	movhps	-5464(%rbp), %xmm1
	movhps	-5480(%rbp), %xmm2
	movaps	%xmm1, -144(%rbp)
	movhps	-5416(%rbp), %xmm0
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -5376(%rbp)
	movq	$0, -5360(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-144(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm6
	movq	-5528(%rbp), %rdi
	leaq	80(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-112(%rbp), %xmm7
	movups	%xmm5, 16(%rax)
	movdqa	-96(%rbp), %xmm5
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	popq	%rcx
	popq	%rsi
	testq	%rdi, %rdi
	je	.L1246
	call	_ZdlPv@PLT
.L1246:
	leaq	-4136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -4128(%rbp)
	je	.L1247
.L1411:
	leaq	-4136(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -5472(%rbp)
	movq	$0, -5464(%rbp)
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5416(%rbp), %rax
	pushq	-5520(%rbp)
	pushq	%r14
	leaq	-5456(%rbp), %rcx
	leaq	-5440(%rbp), %r9
	pushq	%rax
	leaq	-5424(%rbp), %rax
	movq	-5528(%rbp), %rdi
	leaq	-5448(%rbp), %r8
	pushq	%rax
	leaq	-5432(%rbp), %rax
	leaq	-5464(%rbp), %rdx
	pushq	%rax
	leaq	-5472(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS9_EESK_SK_SG_SG_PNSC_ISA_EESM_
	addq	$48, %rsp
	movl	$116, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5408(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$88, %edi
	movq	-5408(%rbp), %xmm5
	movq	-5424(%rbp), %xmm6
	movaps	%xmm0, -5376(%rbp)
	movq	-5440(%rbp), %xmm7
	movq	-5456(%rbp), %xmm3
	movhps	-5392(%rbp), %xmm5
	movq	%rbx, -80(%rbp)
	movq	-5472(%rbp), %xmm4
	movhps	-5416(%rbp), %xmm6
	movaps	%xmm5, -96(%rbp)
	movhps	-5432(%rbp), %xmm7
	movhps	-5448(%rbp), %xmm3
	movaps	%xmm5, -5760(%rbp)
	movhps	-5464(%rbp), %xmm4
	movaps	%xmm6, -5776(%rbp)
	movaps	%xmm7, -5792(%rbp)
	movaps	%xmm3, -5808(%rbp)
	movaps	%xmm4, -5824(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movq	$0, -5360(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm6
	movq	-80(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm5
	movdqa	-144(%rbp), %xmm7
	leaq	88(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-112(%rbp), %xmm6
	movq	-5600(%rbp), %rdi
	movups	%xmm7, 16(%rax)
	movdqa	-96(%rbp), %xmm7
	movq	%rcx, 80(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1248
	call	_ZdlPv@PLT
.L1248:
	movdqa	-5824(%rbp), %xmm5
	movdqa	-5808(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$88, %edi
	movdqa	-5792(%rbp), %xmm7
	movq	%rbx, -80(%rbp)
	movaps	%xmm5, -160(%rbp)
	movdqa	-5776(%rbp), %xmm5
	movaps	%xmm6, -144(%rbp)
	movdqa	-5760(%rbp), %xmm6
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm0, -5376(%rbp)
	movq	$0, -5360(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm7
	movq	-80(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm6
	movdqa	-144(%rbp), %xmm5
	leaq	88(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-112(%rbp), %xmm7
	movq	-5616(%rbp), %rdi
	movups	%xmm5, 16(%rax)
	movdqa	-96(%rbp), %xmm5
	movq	%rcx, 80(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1249
	call	_ZdlPv@PLT
.L1249:
	leaq	-3752(%rbp), %rcx
	leaq	-3944(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -3936(%rbp)
	je	.L1250
.L1412:
	leaq	-3944(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -5480(%rbp)
	movq	$0, -5472(%rbp)
	movq	$0, -5464(%rbp)
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-5416(%rbp), %rax
	pushq	-5520(%rbp)
	movq	-5600(%rbp), %rdi
	pushq	%r14
	leaq	-5464(%rbp), %rcx
	leaq	-5448(%rbp), %r9
	pushq	%rax
	leaq	-5424(%rbp), %rax
	leaq	-5456(%rbp), %r8
	pushq	%rax
	leaq	-5432(%rbp), %rax
	leaq	-5472(%rbp), %rdx
	pushq	%rax
	leaq	-5440(%rbp), %rax
	leaq	-5480(%rbp), %rsi
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_NS0_5BoolTEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS9_EESL_SL_SH_SH_PNSD_ISA_EESN_PNSD_ISB_EE
	addq	$48, %rsp
	movq	%r15, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5408(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-5392(%rbp), %rax
	movl	$96, %edi
	movq	-5416(%rbp), %xmm0
	movq	-5432(%rbp), %xmm1
	movq	%rbx, -72(%rbp)
	movq	-5448(%rbp), %xmm2
	movhps	-5408(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movq	-5464(%rbp), %xmm3
	movhps	-5424(%rbp), %xmm1
	movaps	%xmm0, -96(%rbp)
	movq	-5480(%rbp), %xmm4
	movhps	-5440(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -112(%rbp)
	movhps	-5456(%rbp), %xmm3
	movhps	-5472(%rbp), %xmm4
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm0, -5376(%rbp)
	movq	$0, -5360(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-144(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm5
	movq	-5536(%rbp), %rdi
	leaq	96(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-112(%rbp), %xmm6
	movups	%xmm7, 16(%rax)
	movdqa	-96(%rbp), %xmm7
	movups	%xmm5, 32(%rax)
	movdqa	-80(%rbp), %xmm5
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm5, 80(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1251
	call	_ZdlPv@PLT
.L1251:
	leaq	-3560(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3744(%rbp)
	je	.L1252
.L1413:
	leaq	-3752(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -5480(%rbp)
	movq	$0, -5472(%rbp)
	movq	$0, -5464(%rbp)
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-5416(%rbp), %rax
	pushq	-5520(%rbp)
	movq	-5616(%rbp), %rdi
	pushq	%r14
	leaq	-5464(%rbp), %rcx
	leaq	-5472(%rbp), %rdx
	pushq	%rax
	leaq	-5424(%rbp), %rax
	leaq	-5448(%rbp), %r9
	pushq	%rax
	leaq	-5432(%rbp), %rax
	leaq	-5456(%rbp), %r8
	pushq	%rax
	leaq	-5440(%rbp), %rax
	leaq	-5480(%rbp), %rsi
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_NS0_5BoolTEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS9_EESL_SL_SH_SH_PNSD_ISA_EESN_PNSD_ISB_EE
	addq	$48, %rsp
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal42FromConstexpr6ATbool16ATconstexpr_bool_165EPNS0_8compiler18CodeAssemblerStateEb@PLT
	movl	$96, %edi
	movq	-5392(%rbp), %xmm0
	movq	-5416(%rbp), %xmm1
	movq	-5432(%rbp), %xmm2
	movq	%rax, %xmm6
	movq	-5448(%rbp), %xmm3
	movq	$0, -5360(%rbp)
	movq	-5464(%rbp), %xmm4
	punpcklqdq	%xmm6, %xmm0
	movq	-5480(%rbp), %xmm5
	movhps	-5408(%rbp), %xmm1
	movhps	-5424(%rbp), %xmm2
	movhps	-5440(%rbp), %xmm3
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-5456(%rbp), %xmm4
	movhps	-5472(%rbp), %xmm5
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -5376(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-144(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm6
	movq	-5536(%rbp), %rdi
	leaq	96(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-112(%rbp), %xmm7
	movups	%xmm5, 16(%rax)
	movdqa	-96(%rbp), %xmm5
	movups	%xmm6, 32(%rax)
	movdqa	-80(%rbp), %xmm6
	movups	%xmm7, 48(%rax)
	movups	%xmm5, 64(%rax)
	movups	%xmm6, 80(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1253
	call	_ZdlPv@PLT
.L1253:
	leaq	-3560(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3552(%rbp)
	je	.L1254
.L1414:
	leaq	-3560(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -5488(%rbp)
	movq	$0, -5480(%rbp)
	movq	$0, -5472(%rbp)
	movq	$0, -5464(%rbp)
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5416(%rbp), %rax
	pushq	-5520(%rbp)
	pushq	%r14
	leaq	-5472(%rbp), %rcx
	leaq	-5480(%rbp), %rdx
	pushq	%rax
	leaq	-5424(%rbp), %rax
	movq	-5536(%rbp), %rdi
	leaq	-5488(%rbp), %rsi
	pushq	%rax
	leaq	-5432(%rbp), %rax
	leaq	-5456(%rbp), %r9
	pushq	%rax
	leaq	-5440(%rbp), %rax
	leaq	-5464(%rbp), %r8
	pushq	%rax
	leaq	-5448(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_NS0_5BoolTESB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS9_EESL_SL_SH_SH_PNSD_ISA_EESN_PNSD_ISB_EESP_
	pxor	%xmm0, %xmm0
	addq	$64, %rsp
	movq	-5424(%rbp), %xmm1
	movq	-5440(%rbp), %xmm2
	movl	$80, %edi
	movq	-5456(%rbp), %xmm5
	movaps	%xmm0, -5376(%rbp)
	movq	-5472(%rbp), %xmm6
	movq	-5488(%rbp), %xmm7
	movhps	-5416(%rbp), %xmm1
	movq	$0, -5360(%rbp)
	movhps	-5432(%rbp), %xmm2
	movhps	-5448(%rbp), %xmm5
	movaps	%xmm1, -96(%rbp)
	movq	-5392(%rbp), %rbx
	movhps	-5464(%rbp), %xmm6
	movhps	-5480(%rbp), %xmm7
	movaps	%xmm1, -5808(%rbp)
	movaps	%xmm2, -5824(%rbp)
	movaps	%xmm5, -5760(%rbp)
	movaps	%xmm6, -5776(%rbp)
	movaps	%xmm7, -5792(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-144(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm6
	movq	-5640(%rbp), %rdi
	leaq	80(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-112(%rbp), %xmm7
	movups	%xmm5, 16(%rax)
	movdqa	-96(%rbp), %xmm5
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1255
	call	_ZdlPv@PLT
.L1255:
	movdqa	-5792(%rbp), %xmm6
	movdqa	-5776(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movl	$80, %edi
	movdqa	-5760(%rbp), %xmm3
	movdqa	-5824(%rbp), %xmm4
	movaps	%xmm0, -5376(%rbp)
	movdqa	-5808(%rbp), %xmm5
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movq	$0, -5360(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	leaq	80(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movdqa	-144(%rbp), %xmm7
	movq	-5648(%rbp), %rdi
	movups	%xmm6, (%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1256
	call	_ZdlPv@PLT
.L1256:
	leaq	-3176(%rbp), %rcx
	leaq	-3368(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -3360(%rbp)
	je	.L1257
.L1415:
	leaq	-3368(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -5488(%rbp)
	movq	$0, -5480(%rbp)
	movq	$0, -5472(%rbp)
	movq	$0, -5464(%rbp)
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5416(%rbp), %rax
	movq	-5640(%rbp), %rdi
	pushq	%rax
	leaq	-5424(%rbp), %rax
	leaq	-5472(%rbp), %rcx
	pushq	%rax
	leaq	-5432(%rbp), %rax
	leaq	-5464(%rbp), %r8
	pushq	%rax
	leaq	-5440(%rbp), %rax
	leaq	-5456(%rbp), %r9
	pushq	%rax
	leaq	-5448(%rbp), %rax
	leaq	-5480(%rbp), %rdx
	pushq	%rax
	leaq	-5488(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS9_EESK_SK_SG_SG_PNSC_ISA_EESM_
	addq	$48, %rsp
	movl	$118, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5488(%rbp), %r9
	movq	-5456(%rbp), %rbx
	movq	%r14, %rdi
	movq	-5472(%rbp), %rax
	movq	%r9, -5792(%rbp)
	movq	%rbx, -5776(%rbp)
	movq	-5432(%rbp), %rbx
	movq	%rax, -5760(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$712, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-5376(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-160(%rbp), %r10
	xorl	%esi, %esi
	movq	%rbx, -144(%rbp)
	movl	$3, %ebx
	movq	%rax, %r8
	movq	-5792(%rbp), %r9
	movq	-5760(%rbp), %xmm0
	pushq	%rbx
	movq	-5520(%rbp), %rdx
	movl	$1, %ecx
	movq	%r14, %rdi
	pushq	%r10
	movq	-5360(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r11
	movhps	-5776(%rbp), %xmm0
	movq	%r11, -5392(%rbp)
	movq	%r10, -5808(%rbp)
	movaps	%xmm0, -160(%rbp)
	movq	%rax, -5384(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$121, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5488(%rbp), %r9
	movq	-5448(%rbp), %rbx
	movq	%r14, %rdi
	movq	-5472(%rbp), %rax
	movq	%r9, -5792(%rbp)
	movq	%rbx, -5776(%rbp)
	movq	-5440(%rbp), %rbx
	movq	%rax, -5760(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$712, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-5376(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-5808(%rbp), %r10
	xorl	%esi, %esi
	movq	%rbx, -144(%rbp)
	movl	$3, %ebx
	movq	%rax, %r8
	movq	-5792(%rbp), %r9
	movq	-5760(%rbp), %xmm0
	pushq	%rbx
	movq	-5520(%rbp), %rdx
	movl	$1, %ecx
	movq	%r14, %rdi
	pushq	%r10
	movq	-5360(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r11
	movhps	-5776(%rbp), %xmm0
	movq	%r11, -5392(%rbp)
	movaps	%xmm0, -160(%rbp)
	movq	%rax, -5384(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$116, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$80, %edi
	movq	-5424(%rbp), %xmm0
	movq	-5440(%rbp), %xmm1
	movq	-5456(%rbp), %xmm2
	movq	-5472(%rbp), %xmm3
	movq	$0, -5360(%rbp)
	movq	-5488(%rbp), %xmm4
	movhps	-5416(%rbp), %xmm0
	movhps	-5432(%rbp), %xmm1
	movhps	-5448(%rbp), %xmm2
	movhps	-5464(%rbp), %xmm3
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-5480(%rbp), %xmm4
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -5376(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	leaq	80(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movdqa	-144(%rbp), %xmm7
	movq	-5576(%rbp), %rdi
	movups	%xmm6, (%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1258
	call	_ZdlPv@PLT
.L1258:
	leaq	-872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3168(%rbp)
	je	.L1259
.L1416:
	leaq	-3176(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -5472(%rbp)
	movq	$0, -5464(%rbp)
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5416(%rbp), %rax
	pushq	-5520(%rbp)
	pushq	%r14
	leaq	-5456(%rbp), %rcx
	leaq	-5440(%rbp), %r9
	pushq	%rax
	leaq	-5424(%rbp), %rax
	movq	-5648(%rbp), %rdi
	leaq	-5448(%rbp), %r8
	pushq	%rax
	leaq	-5432(%rbp), %rax
	leaq	-5464(%rbp), %rdx
	pushq	%rax
	leaq	-5472(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS9_EESK_SK_SG_SG_PNSC_ISA_EESM_
	addq	$48, %rsp
	movl	$122, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8False_66EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5408(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$88, %edi
	movq	-5408(%rbp), %xmm3
	movq	-5424(%rbp), %xmm4
	movaps	%xmm0, -5376(%rbp)
	movq	-5440(%rbp), %xmm1
	movq	-5456(%rbp), %xmm2
	movhps	-5392(%rbp), %xmm3
	movq	%rbx, -80(%rbp)
	movq	-5472(%rbp), %xmm5
	movhps	-5416(%rbp), %xmm4
	movaps	%xmm3, -96(%rbp)
	movhps	-5432(%rbp), %xmm1
	movhps	-5448(%rbp), %xmm2
	movaps	%xmm3, -5760(%rbp)
	movhps	-5464(%rbp), %xmm5
	movaps	%xmm4, -5776(%rbp)
	movaps	%xmm1, -5792(%rbp)
	movaps	%xmm2, -5808(%rbp)
	movaps	%xmm5, -5824(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movq	$0, -5360(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	leaq	88(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movdqa	-160(%rbp), %xmm6
	movdqa	-144(%rbp), %xmm7
	movq	%rcx, 80(%rax)
	movq	-5664(%rbp), %rdi
	movups	%xmm3, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1260
	call	_ZdlPv@PLT
.L1260:
	movdqa	-5824(%rbp), %xmm6
	movdqa	-5808(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movl	$88, %edi
	movdqa	-5792(%rbp), %xmm3
	movdqa	-5776(%rbp), %xmm4
	movq	%rbx, -80(%rbp)
	movdqa	-5760(%rbp), %xmm5
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm0, -5376(%rbp)
	movq	$0, -5360(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	leaq	88(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movdqa	-160(%rbp), %xmm6
	movdqa	-144(%rbp), %xmm7
	movq	%rcx, 80(%rax)
	movq	-5688(%rbp), %rdi
	movups	%xmm3, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1261
	call	_ZdlPv@PLT
.L1261:
	leaq	-2792(%rbp), %rcx
	leaq	-2984(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2976(%rbp)
	je	.L1262
.L1417:
	leaq	-2984(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -5480(%rbp)
	movq	$0, -5472(%rbp)
	movq	$0, -5464(%rbp)
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-5416(%rbp), %rax
	pushq	-5520(%rbp)
	movq	-5664(%rbp), %rdi
	pushq	%r14
	leaq	-5464(%rbp), %rcx
	leaq	-5448(%rbp), %r9
	pushq	%rax
	leaq	-5424(%rbp), %rax
	leaq	-5456(%rbp), %r8
	pushq	%rax
	leaq	-5432(%rbp), %rax
	leaq	-5472(%rbp), %rdx
	pushq	%rax
	leaq	-5440(%rbp), %rax
	leaq	-5480(%rbp), %rsi
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_NS0_5BoolTEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS9_EESL_SL_SH_SH_PNSD_ISA_EESN_PNSD_ISB_EE
	addq	$48, %rsp
	movq	%r15, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5408(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-5392(%rbp), %rax
	movl	$96, %edi
	movq	-5416(%rbp), %xmm0
	movq	-5432(%rbp), %xmm1
	movq	%rbx, -72(%rbp)
	movq	-5448(%rbp), %xmm2
	movhps	-5408(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movq	-5464(%rbp), %xmm3
	movhps	-5424(%rbp), %xmm1
	movaps	%xmm0, -96(%rbp)
	movq	-5480(%rbp), %xmm4
	movhps	-5440(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -112(%rbp)
	movhps	-5456(%rbp), %xmm3
	movhps	-5472(%rbp), %xmm4
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm0, -5376(%rbp)
	movq	$0, -5360(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	leaq	96(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movdqa	-144(%rbp), %xmm7
	movups	%xmm6, (%rax)
	movdqa	-80(%rbp), %xmm6
	movq	-5568(%rbp), %rdi
	movups	%xmm3, 32(%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm5, 64(%rax)
	movups	%xmm6, 80(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1263
	call	_ZdlPv@PLT
.L1263:
	leaq	-2600(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2784(%rbp)
	je	.L1264
.L1418:
	leaq	-2792(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -5480(%rbp)
	movq	$0, -5472(%rbp)
	movq	$0, -5464(%rbp)
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-5416(%rbp), %rax
	pushq	-5520(%rbp)
	movq	-5688(%rbp), %rdi
	pushq	%r14
	leaq	-5464(%rbp), %rcx
	leaq	-5472(%rbp), %rdx
	pushq	%rax
	leaq	-5424(%rbp), %rax
	leaq	-5448(%rbp), %r9
	pushq	%rax
	leaq	-5432(%rbp), %rax
	leaq	-5456(%rbp), %r8
	pushq	%rax
	leaq	-5440(%rbp), %rax
	leaq	-5480(%rbp), %rsi
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_NS0_5BoolTEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS9_EESL_SL_SH_SH_PNSD_ISA_EESN_PNSD_ISB_EE
	addq	$48, %rsp
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal42FromConstexpr6ATbool16ATconstexpr_bool_165EPNS0_8compiler18CodeAssemblerStateEb@PLT
	movl	$96, %edi
	movq	-5392(%rbp), %xmm0
	movq	-5416(%rbp), %xmm1
	movq	-5432(%rbp), %xmm2
	movq	%rax, %xmm7
	movq	-5448(%rbp), %xmm3
	movq	$0, -5360(%rbp)
	movq	-5464(%rbp), %xmm4
	punpcklqdq	%xmm7, %xmm0
	movq	-5480(%rbp), %xmm5
	movhps	-5408(%rbp), %xmm1
	movhps	-5424(%rbp), %xmm2
	movhps	-5440(%rbp), %xmm3
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-5456(%rbp), %xmm4
	movhps	-5472(%rbp), %xmm5
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -5376(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm2
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	leaq	96(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movdqa	-144(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm7
	movups	%xmm3, (%rax)
	movq	-5568(%rbp), %rdi
	movups	%xmm2, 32(%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	movups	%xmm7, 80(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1265
	call	_ZdlPv@PLT
.L1265:
	leaq	-2600(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2592(%rbp)
	je	.L1266
.L1419:
	leaq	-2600(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -5488(%rbp)
	movq	$0, -5480(%rbp)
	movq	$0, -5472(%rbp)
	movq	$0, -5464(%rbp)
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5416(%rbp), %rax
	pushq	-5520(%rbp)
	pushq	%r14
	leaq	-5472(%rbp), %rcx
	leaq	-5480(%rbp), %rdx
	pushq	%rax
	leaq	-5424(%rbp), %rax
	movq	-5568(%rbp), %rdi
	leaq	-5488(%rbp), %rsi
	pushq	%rax
	leaq	-5432(%rbp), %rax
	leaq	-5456(%rbp), %r9
	pushq	%rax
	leaq	-5440(%rbp), %rax
	leaq	-5464(%rbp), %r8
	pushq	%rax
	leaq	-5448(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_NS0_5BoolTESB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS9_EESL_SL_SH_SH_PNSD_ISA_EESN_PNSD_ISB_EESP_
	pxor	%xmm0, %xmm0
	addq	$64, %rsp
	movq	-5424(%rbp), %xmm6
	movq	-5440(%rbp), %xmm7
	movl	$80, %edi
	movq	-5456(%rbp), %xmm3
	movaps	%xmm0, -5376(%rbp)
	movq	-5472(%rbp), %xmm4
	movq	-5488(%rbp), %xmm1
	movhps	-5416(%rbp), %xmm6
	movq	$0, -5360(%rbp)
	movhps	-5432(%rbp), %xmm7
	movhps	-5448(%rbp), %xmm3
	movaps	%xmm6, -96(%rbp)
	movq	-5392(%rbp), %rbx
	movhps	-5464(%rbp), %xmm4
	movhps	-5480(%rbp), %xmm1
	movaps	%xmm6, -5792(%rbp)
	movaps	%xmm7, -5808(%rbp)
	movaps	%xmm3, -5760(%rbp)
	movaps	%xmm4, -5776(%rbp)
	movaps	%xmm1, -5824(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm2
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm1
	leaq	80(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movdqa	-144(%rbp), %xmm4
	movq	-5696(%rbp), %rdi
	movups	%xmm3, (%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm1, 64(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1267
	call	_ZdlPv@PLT
.L1267:
	movdqa	-5824(%rbp), %xmm6
	movdqa	-5776(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movl	$80, %edi
	movdqa	-5760(%rbp), %xmm3
	movdqa	-5808(%rbp), %xmm4
	movaps	%xmm0, -5376(%rbp)
	movdqa	-5792(%rbp), %xmm2
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	$0, -5360(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm3
	leaq	80(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movdqa	-144(%rbp), %xmm1
	movq	-5704(%rbp), %rdi
	movups	%xmm5, (%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm3, 64(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1268
	call	_ZdlPv@PLT
.L1268:
	leaq	-2216(%rbp), %rcx
	leaq	-2408(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2400(%rbp)
	je	.L1269
.L1420:
	leaq	-2408(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -5488(%rbp)
	movq	$0, -5480(%rbp)
	movq	$0, -5472(%rbp)
	movq	$0, -5464(%rbp)
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5416(%rbp), %rax
	movq	-5696(%rbp), %rdi
	pushq	%rax
	leaq	-5424(%rbp), %rax
	leaq	-5472(%rbp), %rcx
	pushq	%rax
	leaq	-5432(%rbp), %rax
	leaq	-5464(%rbp), %r8
	pushq	%rax
	leaq	-5440(%rbp), %rax
	leaq	-5456(%rbp), %r9
	pushq	%rax
	leaq	-5448(%rbp), %rax
	leaq	-5480(%rbp), %rdx
	pushq	%rax
	leaq	-5488(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS9_EESK_SK_SG_SG_PNSC_ISA_EESM_
	addq	$48, %rsp
	movl	$124, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5488(%rbp), %r9
	movq	-5456(%rbp), %rbx
	movq	%r14, %rdi
	movq	-5472(%rbp), %rax
	movq	%r9, -5792(%rbp)
	movq	%rbx, -5776(%rbp)
	movq	-5432(%rbp), %rbx
	movq	%rax, -5760(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$712, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-5376(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-160(%rbp), %r10
	xorl	%esi, %esi
	movq	%rbx, -144(%rbp)
	movl	$3, %ebx
	movq	%rax, %r8
	movq	-5792(%rbp), %r9
	movq	-5760(%rbp), %xmm0
	pushq	%rbx
	movq	-5520(%rbp), %rdx
	movl	$1, %ecx
	movq	%r14, %rdi
	pushq	%r10
	movq	-5360(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r11
	movhps	-5776(%rbp), %xmm0
	movq	%r11, -5392(%rbp)
	movq	%r10, -5808(%rbp)
	movaps	%xmm0, -160(%rbp)
	movq	%rax, -5384(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$127, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal59FromConstexpr14ATLanguageMode24ATconstexpr_LanguageMode_166EPNS0_8compiler18CodeAssemblerStateENS0_12LanguageModeE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5488(%rbp), %r9
	movq	-5448(%rbp), %rcx
	movq	%r14, %rdi
	movq	-5472(%rbp), %rax
	movq	%r9, -5792(%rbp)
	movq	%rcx, -5776(%rbp)
	movq	%rax, -5760(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$160, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-5376(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-5808(%rbp), %r10
	xorl	%esi, %esi
	movq	%rbx, -144(%rbp)
	movl	$3, %ebx
	movq	%rax, %r8
	movq	-5792(%rbp), %r9
	movq	-5760(%rbp), %xmm0
	pushq	%rbx
	movq	-5520(%rbp), %rdx
	movl	$1, %ecx
	movq	%r14, %rdi
	pushq	%r10
	movq	-5360(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r11
	movhps	-5776(%rbp), %xmm0
	movq	%r11, -5392(%rbp)
	movaps	%xmm0, -160(%rbp)
	movq	%rax, -5384(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$122, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$80, %edi
	movq	-5424(%rbp), %xmm0
	movq	-5440(%rbp), %xmm1
	movq	-5456(%rbp), %xmm2
	movq	-5472(%rbp), %xmm3
	movq	$0, -5360(%rbp)
	movq	-5488(%rbp), %xmm4
	movhps	-5416(%rbp), %xmm0
	movhps	-5432(%rbp), %xmm1
	movhps	-5448(%rbp), %xmm2
	movhps	-5464(%rbp), %xmm3
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-5480(%rbp), %xmm4
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -5376(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm4
	movdqa	-128(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm1
	movdqa	-96(%rbp), %xmm6
	leaq	80(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movdqa	-144(%rbp), %xmm2
	movq	-5584(%rbp), %rdi
	movups	%xmm4, (%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm1, 48(%rax)
	movups	%xmm6, 64(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1270
	call	_ZdlPv@PLT
.L1270:
	leaq	-1064(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2208(%rbp)
	je	.L1271
.L1421:
	leaq	-2216(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -5472(%rbp)
	movq	$0, -5464(%rbp)
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5416(%rbp), %rax
	pushq	-5520(%rbp)
	pushq	%r14
	leaq	-5456(%rbp), %rcx
	leaq	-5440(%rbp), %r9
	pushq	%rax
	leaq	-5424(%rbp), %rax
	movq	-5704(%rbp), %rdi
	leaq	-5448(%rbp), %r8
	pushq	%rax
	leaq	-5432(%rbp), %rax
	leaq	-5464(%rbp), %rdx
	pushq	%rax
	leaq	-5472(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS9_EESK_SK_SG_SG_PNSC_ISA_EESM_
	addq	$48, %rsp
	movl	$128, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5408(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$88, %edi
	movq	-5408(%rbp), %xmm2
	movq	-5424(%rbp), %xmm5
	movaps	%xmm0, -5376(%rbp)
	movq	-5440(%rbp), %xmm6
	movq	-5456(%rbp), %xmm7
	movhps	-5392(%rbp), %xmm2
	movq	%rbx, -80(%rbp)
	movq	-5472(%rbp), %xmm3
	movhps	-5416(%rbp), %xmm5
	movaps	%xmm2, -96(%rbp)
	movhps	-5432(%rbp), %xmm6
	movhps	-5448(%rbp), %xmm7
	movaps	%xmm2, -5808(%rbp)
	movhps	-5464(%rbp), %xmm3
	movaps	%xmm5, -5824(%rbp)
	movaps	%xmm6, -5760(%rbp)
	movaps	%xmm7, -5776(%rbp)
	movaps	%xmm3, -5792(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movq	$0, -5360(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm5
	leaq	88(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movdqa	-160(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm3
	movq	%rcx, 80(%rax)
	movq	-5656(%rbp), %rdi
	movups	%xmm4, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1272
	call	_ZdlPv@PLT
.L1272:
	movdqa	-5792(%rbp), %xmm1
	movdqa	-5776(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$88, %edi
	movdqa	-5760(%rbp), %xmm7
	movdqa	-5824(%rbp), %xmm3
	movq	%rbx, -80(%rbp)
	movdqa	-5808(%rbp), %xmm4
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm0, -5376(%rbp)
	movq	$0, -5360(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm1
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	88(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movdqa	-160(%rbp), %xmm2
	movdqa	-144(%rbp), %xmm5
	movq	%rcx, 80(%rax)
	movq	-5672(%rbp), %rdi
	movups	%xmm1, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1273
	call	_ZdlPv@PLT
.L1273:
	leaq	-1832(%rbp), %rcx
	leaq	-2024(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2016(%rbp)
	je	.L1274
.L1422:
	leaq	-2024(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -5480(%rbp)
	movq	$0, -5472(%rbp)
	movq	$0, -5464(%rbp)
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-5416(%rbp), %rax
	pushq	-5520(%rbp)
	movq	-5656(%rbp), %rdi
	pushq	%r14
	leaq	-5464(%rbp), %rcx
	leaq	-5448(%rbp), %r9
	pushq	%rax
	leaq	-5424(%rbp), %rax
	leaq	-5456(%rbp), %r8
	pushq	%rax
	leaq	-5432(%rbp), %rax
	leaq	-5472(%rbp), %rdx
	pushq	%rax
	leaq	-5440(%rbp), %rax
	leaq	-5480(%rbp), %rsi
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_NS0_5BoolTEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS9_EESL_SL_SH_SH_PNSD_ISA_EESN_PNSD_ISB_EE
	addq	$48, %rsp
	movq	%r15, %rdi
	call	_ZN2v88internal8False_66EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5408(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-5392(%rbp), %rax
	movl	$96, %edi
	movq	-5416(%rbp), %xmm0
	movq	-5432(%rbp), %xmm1
	movq	%rbx, -72(%rbp)
	movq	-5448(%rbp), %xmm2
	movhps	-5408(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movq	-5464(%rbp), %xmm3
	movhps	-5424(%rbp), %xmm1
	movaps	%xmm0, -96(%rbp)
	movq	-5480(%rbp), %xmm4
	movhps	-5440(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -112(%rbp)
	movhps	-5456(%rbp), %xmm3
	movhps	-5472(%rbp), %xmm4
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm0, -5376(%rbp)
	movq	$0, -5360(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm2
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm1
	leaq	96(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movdqa	-144(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm6
	movups	%xmm3, (%rax)
	movq	-5560(%rbp), %rdi
	movups	%xmm2, 32(%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm1, 64(%rax)
	movups	%xmm6, 80(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1275
	call	_ZdlPv@PLT
.L1275:
	leaq	-1640(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1824(%rbp)
	je	.L1276
.L1423:
	leaq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -5480(%rbp)
	movq	$0, -5472(%rbp)
	movq	$0, -5464(%rbp)
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-5416(%rbp), %rax
	pushq	-5520(%rbp)
	movq	-5672(%rbp), %rdi
	pushq	%r14
	leaq	-5464(%rbp), %rcx
	leaq	-5472(%rbp), %rdx
	pushq	%rax
	leaq	-5424(%rbp), %rax
	leaq	-5448(%rbp), %r9
	pushq	%rax
	leaq	-5432(%rbp), %rax
	leaq	-5456(%rbp), %r8
	pushq	%rax
	leaq	-5440(%rbp), %rax
	leaq	-5480(%rbp), %rsi
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_NS0_5BoolTEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS9_EESL_SL_SH_SH_PNSD_ISA_EESN_PNSD_ISB_EE
	addq	$48, %rsp
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal42FromConstexpr6ATbool16ATconstexpr_bool_165EPNS0_8compiler18CodeAssemblerStateEb@PLT
	movl	$96, %edi
	movq	-5392(%rbp), %xmm0
	movq	-5416(%rbp), %xmm1
	movq	-5432(%rbp), %xmm2
	movq	%rax, %xmm7
	movq	-5448(%rbp), %xmm3
	movq	$0, -5360(%rbp)
	movq	-5464(%rbp), %xmm4
	punpcklqdq	%xmm7, %xmm0
	movq	-5480(%rbp), %xmm5
	movhps	-5408(%rbp), %xmm1
	movhps	-5424(%rbp), %xmm2
	movhps	-5440(%rbp), %xmm3
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-5456(%rbp), %xmm4
	movhps	-5472(%rbp), %xmm5
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -5376(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm2
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm1
	leaq	96(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movdqa	-144(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm6
	movups	%xmm3, (%rax)
	movq	-5560(%rbp), %rdi
	movups	%xmm2, 32(%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm1, 64(%rax)
	movups	%xmm6, 80(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1277
	call	_ZdlPv@PLT
.L1277:
	leaq	-1640(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1632(%rbp)
	je	.L1278
.L1424:
	leaq	-1640(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -5488(%rbp)
	movq	$0, -5480(%rbp)
	movq	$0, -5472(%rbp)
	movq	$0, -5464(%rbp)
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5416(%rbp), %rax
	pushq	-5520(%rbp)
	pushq	%r14
	leaq	-5472(%rbp), %rcx
	leaq	-5480(%rbp), %rdx
	pushq	%rax
	leaq	-5424(%rbp), %rax
	movq	-5560(%rbp), %rdi
	leaq	-5488(%rbp), %rsi
	pushq	%rax
	leaq	-5432(%rbp), %rax
	leaq	-5456(%rbp), %r9
	pushq	%rax
	leaq	-5440(%rbp), %rax
	leaq	-5464(%rbp), %r8
	pushq	%rax
	leaq	-5448(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_NS0_5BoolTESB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EEPNSD_IS9_EESL_SL_SH_SH_PNSD_ISA_EESN_PNSD_ISB_EESP_
	pxor	%xmm0, %xmm0
	addq	$64, %rsp
	movq	-5424(%rbp), %xmm4
	movq	-5440(%rbp), %xmm1
	movl	$80, %edi
	movq	-5456(%rbp), %xmm2
	movaps	%xmm0, -5376(%rbp)
	movq	-5472(%rbp), %xmm5
	movq	-5488(%rbp), %xmm6
	movhps	-5416(%rbp), %xmm4
	movq	$0, -5360(%rbp)
	movhps	-5432(%rbp), %xmm1
	movhps	-5448(%rbp), %xmm2
	movaps	%xmm4, -96(%rbp)
	movq	-5392(%rbp), %rbx
	movhps	-5464(%rbp), %xmm5
	movhps	-5480(%rbp), %xmm6
	movaps	%xmm4, -5824(%rbp)
	movaps	%xmm1, -5808(%rbp)
	movaps	%xmm2, -5792(%rbp)
	movaps	%xmm5, -5776(%rbp)
	movaps	%xmm6, -5760(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm5
	leaq	80(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movdqa	-144(%rbp), %xmm3
	movq	-5712(%rbp), %rdi
	movups	%xmm7, (%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1279
	call	_ZdlPv@PLT
.L1279:
	movdqa	-5760(%rbp), %xmm1
	movdqa	-5776(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$80, %edi
	movdqa	-5792(%rbp), %xmm7
	movdqa	-5808(%rbp), %xmm3
	movaps	%xmm0, -5376(%rbp)
	movdqa	-5824(%rbp), %xmm4
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movq	$0, -5360(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm1
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	80(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movdqa	-144(%rbp), %xmm5
	movq	-5592(%rbp), %rdi
	movups	%xmm2, (%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1280
	call	_ZdlPv@PLT
.L1280:
	leaq	-1256(%rbp), %rcx
	leaq	-1448(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1440(%rbp)
	je	.L1281
.L1425:
	leaq	-1448(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -5488(%rbp)
	movq	$0, -5480(%rbp)
	movq	$0, -5472(%rbp)
	movq	$0, -5464(%rbp)
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5416(%rbp), %rax
	movq	-5712(%rbp), %rdi
	pushq	%rax
	leaq	-5424(%rbp), %rax
	leaq	-5464(%rbp), %r8
	pushq	%rax
	leaq	-5432(%rbp), %rax
	leaq	-5472(%rbp), %rcx
	pushq	%rax
	leaq	-5440(%rbp), %rax
	leaq	-5456(%rbp), %r9
	pushq	%rax
	leaq	-5448(%rbp), %rax
	leaq	-5480(%rbp), %rdx
	pushq	%rax
	leaq	-5488(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS9_EESK_SK_SG_SG_PNSC_ISA_EESM_
	addq	$48, %rsp
	movl	$130, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal59FromConstexpr14ATLanguageMode24ATconstexpr_LanguageMode_166EPNS0_8compiler18CodeAssemblerStateENS0_12LanguageModeE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5488(%rbp), %r9
	movq	-5456(%rbp), %rcx
	movq	%r14, %rdi
	movq	-5472(%rbp), %rax
	movq	%r9, -5792(%rbp)
	movq	%rcx, -5776(%rbp)
	movq	%rax, -5760(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$160, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-5376(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-160(%rbp), %r10
	xorl	%esi, %esi
	movq	%rbx, -144(%rbp)
	movl	$3, %ebx
	movq	%rax, %r8
	movq	-5792(%rbp), %r9
	movq	-5760(%rbp), %xmm0
	pushq	%rbx
	movq	-5520(%rbp), %rdx
	movl	$1, %ecx
	movq	%r14, %rdi
	pushq	%r10
	movq	-5360(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r11
	movhps	-5776(%rbp), %xmm0
	movq	%r11, -5392(%rbp)
	movq	%r10, -5808(%rbp)
	movaps	%xmm0, -160(%rbp)
	movq	%rax, -5384(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$133, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5488(%rbp), %r9
	movq	-5448(%rbp), %rbx
	movq	%r14, %rdi
	movq	-5472(%rbp), %rax
	movq	%r9, -5792(%rbp)
	movq	%rbx, -5776(%rbp)
	movq	-5440(%rbp), %rbx
	movq	%rax, -5760(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$712, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-5376(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-5808(%rbp), %r10
	xorl	%esi, %esi
	movq	%rbx, -144(%rbp)
	movl	$3, %ebx
	movq	%rax, %r8
	movq	-5792(%rbp), %r9
	movq	-5760(%rbp), %xmm0
	pushq	%rbx
	movq	-5520(%rbp), %rdx
	movl	$1, %ecx
	movq	%r14, %rdi
	pushq	%r10
	movq	-5360(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r11
	movhps	-5776(%rbp), %xmm0
	movq	%r11, -5392(%rbp)
	movaps	%xmm0, -160(%rbp)
	movq	%rax, -5384(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$128, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$80, %edi
	movq	-5424(%rbp), %xmm0
	movq	-5440(%rbp), %xmm1
	movq	-5456(%rbp), %xmm2
	movq	-5472(%rbp), %xmm3
	movq	$0, -5360(%rbp)
	movq	-5488(%rbp), %xmm4
	movhps	-5416(%rbp), %xmm0
	movhps	-5432(%rbp), %xmm1
	movhps	-5448(%rbp), %xmm2
	movhps	-5464(%rbp), %xmm3
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-5480(%rbp), %xmm4
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -5376(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm2
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm1
	leaq	80(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movdqa	-144(%rbp), %xmm4
	movq	-5592(%rbp), %rdi
	movups	%xmm3, (%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm1, 64(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1282
	call	_ZdlPv@PLT
.L1282:
	leaq	-1256(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1248(%rbp)
	je	.L1283
.L1426:
	leaq	-1256(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -5472(%rbp)
	movq	$0, -5464(%rbp)
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5416(%rbp), %rax
	pushq	-5520(%rbp)
	pushq	%r14
	leaq	-5456(%rbp), %rcx
	leaq	-5440(%rbp), %r9
	pushq	%rax
	leaq	-5424(%rbp), %rax
	movq	-5592(%rbp), %rdi
	leaq	-5448(%rbp), %r8
	pushq	%rax
	leaq	-5432(%rbp), %rax
	leaq	-5464(%rbp), %rdx
	pushq	%rax
	leaq	-5472(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS9_EESK_SK_SG_SG_PNSC_ISA_EESM_
	addq	$48, %rsp
	movl	$122, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$80, %edi
	movq	-5408(%rbp), %xmm0
	movq	-5424(%rbp), %xmm1
	movq	-5440(%rbp), %xmm2
	movq	-5456(%rbp), %xmm3
	movq	$0, -5360(%rbp)
	movq	-5472(%rbp), %xmm4
	movhps	-5392(%rbp), %xmm0
	movhps	-5416(%rbp), %xmm1
	movhps	-5432(%rbp), %xmm2
	movhps	-5448(%rbp), %xmm3
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-5464(%rbp), %xmm4
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -5376(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm2
	leaq	80(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movdqa	-144(%rbp), %xmm7
	movq	-5584(%rbp), %rdi
	movups	%xmm6, (%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm2, 64(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1284
	call	_ZdlPv@PLT
.L1284:
	leaq	-1064(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1056(%rbp)
	je	.L1285
.L1427:
	leaq	-1064(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -5472(%rbp)
	movq	$0, -5464(%rbp)
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5416(%rbp), %rax
	pushq	-5520(%rbp)
	pushq	%r14
	leaq	-5456(%rbp), %rcx
	leaq	-5440(%rbp), %r9
	pushq	%rax
	leaq	-5424(%rbp), %rax
	movq	-5584(%rbp), %rdi
	leaq	-5448(%rbp), %r8
	pushq	%rax
	leaq	-5432(%rbp), %rax
	leaq	-5464(%rbp), %rdx
	pushq	%rax
	leaq	-5472(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS9_EESK_SK_SG_SG_PNSC_ISA_EESM_
	addq	$48, %rsp
	movl	$116, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$80, %edi
	movq	-5408(%rbp), %xmm0
	movq	-5424(%rbp), %xmm1
	movq	-5440(%rbp), %xmm2
	movq	-5456(%rbp), %xmm3
	movq	$0, -5360(%rbp)
	movq	-5472(%rbp), %xmm4
	movhps	-5392(%rbp), %xmm0
	movhps	-5416(%rbp), %xmm1
	movhps	-5432(%rbp), %xmm2
	movhps	-5448(%rbp), %xmm3
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-5464(%rbp), %xmm4
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -5376(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm3
	leaq	80(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movdqa	-144(%rbp), %xmm1
	movq	-5576(%rbp), %rdi
	movups	%xmm5, (%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm3, 64(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1286
	call	_ZdlPv@PLT
.L1286:
	leaq	-872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -864(%rbp)
	je	.L1287
.L1428:
	leaq	-872(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -5472(%rbp)
	movq	$0, -5464(%rbp)
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5416(%rbp), %rax
	pushq	-5520(%rbp)
	pushq	%r14
	leaq	-5456(%rbp), %rcx
	leaq	-5440(%rbp), %r9
	pushq	%rax
	leaq	-5424(%rbp), %rax
	movq	-5576(%rbp), %rdi
	leaq	-5448(%rbp), %r8
	pushq	%rax
	leaq	-5432(%rbp), %rax
	leaq	-5464(%rbp), %rdx
	pushq	%rax
	leaq	-5472(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_S4_S4_NS0_7OddballESA_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS9_EESK_SK_SG_SG_PNSC_ISA_EESM_
	addq	$48, %rsp
	movl	$137, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5440(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberAddENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r13, %rdi
	movq	%rax, -5776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$138, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5432(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberSubENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r13, %rdi
	movq	%rax, -5760(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$91, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$48, %edi
	movq	-5456(%rbp), %xmm0
	movq	-5472(%rbp), %xmm1
	movq	$0, -5360(%rbp)
	movhps	-5448(%rbp), %xmm0
	movhps	-5464(%rbp), %xmm1
	movaps	%xmm0, -144(%rbp)
	movq	-5776(%rbp), %xmm0
	movaps	%xmm1, -160(%rbp)
	movhps	-5760(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -5376(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-144(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm5
	movq	-5552(%rbp), %rdi
	leaq	48(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1288
	call	_ZdlPv@PLT
.L1288:
	leaq	-5096(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -672(%rbp)
	je	.L1289
.L1429:
	leaq	-680(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	pushq	-5520(%rbp)
	movq	%r14, %r9
	movq	-5680(%rbp), %rdi
	leaq	-5424(%rbp), %rcx
	leaq	-5416(%rbp), %r8
	leaq	-5432(%rbp), %rdx
	leaq	-5440(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS9_EESJ_SJ_
	movl	$142, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-5440(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movaps	%xmm0, -5376(%rbp)
	movq	%rax, -160(%rbp)
	movq	-5432(%rbp), %rax
	movq	$0, -5360(%rbp)
	movq	%rax, -152(%rbp)
	movq	-5424(%rbp), %rax
	movq	%rax, -144(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movdqa	-160(%rbp), %xmm1
	movq	%r13, %rsi
	movq	-5720(%rbp), %rdi
	leaq	24(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm1, (%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L1290
	call	_ZdlPv@PLT
.L1290:
	leaq	-488(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1289
	.p2align 4,,10
	.p2align 3
.L1430:
	leaq	-488(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-5520(%rbp), %rcx
	movq	-5720(%rbp), %rdi
	movq	%r14, %rdx
	leaq	-5416(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_
	movl	$73, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-5416(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movaps	%xmm0, -5376(%rbp)
	movq	%rax, -160(%rbp)
	movq	-5408(%rbp), %rax
	movq	$0, -5360(%rbp)
	movq	%rax, -152(%rbp)
	movq	-5392(%rbp), %rax
	movq	%rax, -144(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movdqa	-160(%rbp), %xmm6
	movq	%r13, %rsi
	movq	-5736(%rbp), %rdi
	leaq	24(%rax), %rdx
	movq	%rax, -5376(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -5360(%rbp)
	movq	%rdx, -5368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1292
	call	_ZdlPv@PLT
.L1292:
	leaq	-296(%rbp), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1291
.L1431:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22481:
	.size	_ZN2v88internal22GenericArrayReverse_34EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal22GenericArrayReverse_34EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7JSArrayENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7JSArrayENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7JSArrayENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7JSArrayENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7JSArrayENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE:
.LFB26985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	$4, %edi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	leaq	-80(%rbp), %rsi
	movl	$67569671, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L1433
	call	_ZdlPv@PLT
.L1433:
	movq	(%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1434
	movq	%rdx, (%r14)
.L1434:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1435
	movq	%rdx, 0(%r13)
.L1435:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1436
	movq	%rdx, (%r12)
.L1436:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1432
	movq	%rax, (%rbx)
.L1432:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1455
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1455:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26985:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7JSArrayENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7JSArrayENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiES5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESD_SD_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiES5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESD_SD_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiES5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESD_SD_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiES5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESD_SD_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiES5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESD_SD_:
.LFB27014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$5, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$56, %rsp
	movq	%r8, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$101058311, (%rax)
	leaq	5(%rax), %rdx
	movb	$6, 4(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1457
	movq	%rax, -96(%rbp)
	call	_ZdlPv@PLT
	movq	-96(%rbp), %rax
.L1457:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1458
	movq	%rdx, (%r15)
.L1458:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1459
	movq	%rdx, (%r14)
.L1459:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1460
	movq	%rdx, 0(%r13)
.L1460:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1461
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1461:
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L1456
	movq	%rax, (%rbx)
.L1456:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1483
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1483:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27014:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiES5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESD_SD_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiES5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESD_SD_
	.section	.text._ZN2v88internal58FastPackedArrayReverse23ATFastPackedSmiElements5ATSmi_1430EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal58FastPackedArrayReverse23ATFastPackedSmiElements5ATSmi_1430EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE
	.type	_ZN2v88internal58FastPackedArrayReverse23ATFastPackedSmiElements5ATSmi_1430EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE, @function
_ZN2v88internal58FastPackedArrayReverse23ATFastPackedSmiElements5ATSmi_1430EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE:
.LFB22541:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-1192(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1328(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$1400, %rsp
	movq	%rsi, -1344(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1328(%rbp)
	movq	%rdi, -1248(%rbp)
	movl	$72, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1248(%rbp), %rax
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -1336(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1328(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1000(%rbp), %rcx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1384(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1048(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1328(%rbp), %rax
	movl	$120, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-808(%rbp), %rcx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1392(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -856(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1328(%rbp), %rax
	movl	$120, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-616(%rbp), %rcx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1408(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -664(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1328(%rbp), %rax
	movl	$72, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	leaq	-424(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1400(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -472(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1328(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	leaq	-232(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1376(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1344(%rbp), %r9
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	%r13, -88(%rbp)
	leaq	-1280(%rbp), %r13
	movq	%r9, -96(%rbp)
	movaps	%xmm0, -1280(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -1264(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm1
	movq	%r13, %rsi
	movq	-1336(%rbp), %rdi
	leaq	24(%rax), %rdx
	movq	%rax, -1280(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm1, (%rax)
	movq	%rdx, -1264(%rbp)
	movq	%rdx, -1272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1485
	call	_ZdlPv@PLT
.L1485:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1184(%rbp)
	jne	.L1515
	leaq	-1288(%rbp), %rax
	movq	%rax, -1352(%rbp)
	leaq	-1296(%rbp), %rax
	movq	%rax, -1344(%rbp)
	leaq	-1056(%rbp), %rax
	movq	%rax, -1368(%rbp)
.L1486:
	leaq	-864(%rbp), %rax
	cmpq	$0, -992(%rbp)
	leaq	-672(%rbp), %r15
	movq	%rax, -1360(%rbp)
	jne	.L1517
	cmpq	$0, -800(%rbp)
	jne	.L1518
.L1491:
	cmpq	$0, -608(%rbp)
	leaq	-480(%rbp), %rbx
	jne	.L1519
.L1493:
	cmpq	$0, -416(%rbp)
	leaq	-288(%rbp), %r14
	jne	.L1520
.L1494:
	movq	-1376(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -1296(%rbp)
	movq	$0, -1288(%rbp)
	movq	$0, -1280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1352(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	-1344(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EE
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-1360(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-1368(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-1336(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1521
	addq	$1400, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1515:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	$0, -1304(%rbp)
	movq	$0, -1296(%rbp)
	movq	$0, -1288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1296(%rbp), %rax
	movq	-1336(%rbp), %rdi
	leaq	-1288(%rbp), %rcx
	movq	%rax, %rdx
	leaq	-1304(%rbp), %rsi
	movq	%rcx, -1352(%rbp)
	movq	%rax, -1344(%rbp)
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EE
	movl	$60, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$61, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1288(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r8, -1360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-1360(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %rbx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$63, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1288(%rbp), %rax
	movl	$40, %edi
	movq	-1304(%rbp), %xmm0
	movq	%r15, -72(%rbp)
	movhps	-1296(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1280(%rbp)
	movq	%rbx, -64(%rbp)
	movq	$0, -1264(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-96(%rbp), %xmm2
	movq	%r13, %rsi
	movdqa	-80(%rbp), %xmm3
	leaq	40(%rax), %rdx
	movq	%rax, -1280(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	leaq	-1056(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1264(%rbp)
	movq	%rdx, -1272(%rbp)
	movq	%rax, -1368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1487
	call	_ZdlPv@PLT
.L1487:
	movq	-1384(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L1517:
	movq	-1384(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -1320(%rbp)
	movq	$0, -1312(%rbp)
	movq	$0, -1304(%rbp)
	movq	$0, -1296(%rbp)
	movq	$0, -1288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1352(%rbp), %r9
	movq	-1344(%rbp), %r8
	leaq	-1304(%rbp), %rcx
	movq	-1368(%rbp), %rdi
	leaq	-1312(%rbp), %rdx
	leaq	-1320(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiES5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESD_SD_
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1296(%rbp), %rbx
	movq	-1288(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1312(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	-1304(%rbp), %rsi
	movq	-1296(%rbp), %rdx
	movq	-1320(%rbp), %rax
	movl	$40, %edi
	movaps	%xmm0, -1280(%rbp)
	movq	-1288(%rbp), %r15
	movq	%rcx, -1440(%rbp)
	movq	%rsi, -1424(%rbp)
	movq	%rdx, -1416(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rax, -1432(%rbp)
	movq	%rax, -96(%rbp)
	movq	%r15, -64(%rbp)
	movq	$0, -1264(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movq	-1360(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -1280(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1264(%rbp)
	movq	%rdx, -1272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1489
	call	_ZdlPv@PLT
.L1489:
	movq	-1432(%rbp), %xmm0
	movl	$40, %edi
	movq	%r15, -64(%rbp)
	leaq	-672(%rbp), %r15
	movq	$0, -1264(%rbp)
	movhps	-1440(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-1424(%rbp), %xmm0
	movhps	-1416(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1280(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	leaq	40(%rax), %rdx
	movq	%rax, -1280(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1264(%rbp)
	movq	%rdx, -1272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1490
	call	_ZdlPv@PLT
.L1490:
	movq	-1408(%rbp), %rcx
	movq	-1392(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -800(%rbp)
	je	.L1491
.L1518:
	movq	-1392(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -1320(%rbp)
	movq	$0, -1312(%rbp)
	movq	$0, -1304(%rbp)
	movq	$0, -1296(%rbp)
	movq	$0, -1288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1352(%rbp), %r9
	movq	-1344(%rbp), %r8
	leaq	-1304(%rbp), %rcx
	movq	-1360(%rbp), %rdi
	leaq	-1312(%rbp), %rdx
	leaq	-1320(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiES5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESD_SD_
	movl	$64, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1296(%rbp), %rcx
	movq	-1312(%rbp), %rdx
	movq	%r14, %rdi
	movq	-1320(%rbp), %rsi
	call	_ZN2v88internal45LoadElement23ATFastPackedSmiElements5ATSmi_28EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE
	movl	$65, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1288(%rbp), %rcx
	movq	-1312(%rbp), %rdx
	movq	%r14, %rdi
	movq	-1320(%rbp), %rsi
	call	_ZN2v88internal45LoadElement23ATFastPackedSmiElements5ATSmi_28EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE
	movl	$66, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -1392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1392(%rbp), %r8
	movq	%r14, %rdi
	movq	-1296(%rbp), %rcx
	movq	-1312(%rbp), %rdx
	movq	-1320(%rbp), %rsi
	call	_ZN2v88internal46StoreElement23ATFastPackedSmiElements5ATSmi_31EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEESA_
	movl	$67, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1288(%rbp), %rcx
	movq	%rbx, %r8
	movq	%r14, %rdi
	movq	-1312(%rbp), %rdx
	movq	-1320(%rbp), %rsi
	call	_ZN2v88internal46StoreElement23ATFastPackedSmiElements5ATSmi_31EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEESA_
	movl	$68, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	movl	$1, %esi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1296(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r8, -1392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-1392(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %rbx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -1416(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$69, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	movl	$1, %esi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1288(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r8, -1392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-1392(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %r14
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$63, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1416(%rbp), %rcx
	movq	-1304(%rbp), %rax
	movl	$40, %edi
	movq	-1320(%rbp), %xmm0
	movq	%rbx, -64(%rbp)
	movq	%rcx, -72(%rbp)
	movhps	-1312(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1280(%rbp)
	movq	$0, -1264(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	movq	-1368(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -1280(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -1264(%rbp)
	movq	%rdx, -1272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1492
	call	_ZdlPv@PLT
.L1492:
	movq	-1384(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1491
	.p2align 4,,10
	.p2align 3
.L1520:
	movq	-1400(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -1296(%rbp)
	movq	$0, -1288(%rbp)
	movq	$0, -1280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1352(%rbp), %rdx
	movq	%r13, %rcx
	movq	%rbx, %rdi
	movq	-1344(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EE
	movq	-1296(%rbp), %rsi
	movq	-1280(%rbp), %rcx
	movq	%r14, %rdi
	movq	-1288(%rbp), %rdx
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE9AddInputsENS1_5TNodeIS3_EENS7_IS4_EENS7_IS5_EE
	movq	-1376(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1494
	.p2align 4,,10
	.p2align 3
.L1519:
	movq	-1408(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -1312(%rbp)
	movq	$0, -1304(%rbp)
	movq	$0, -1296(%rbp)
	movq	$0, -1288(%rbp)
	movq	$0, -1280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1352(%rbp), %r8
	movq	%r13, %r9
	movq	-1344(%rbp), %rcx
	leaq	-1304(%rbp), %rdx
	leaq	-1312(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiES5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESD_SD_
	movq	%r12, %rdi
	movl	$59, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rdi
	movl	$152, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1312(%rbp), %rsi
	movq	-1296(%rbp), %rcx
	movq	%rbx, %rdi
	movq	-1304(%rbp), %rdx
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE9AddInputsENS1_5TNodeIS3_EENS7_IS4_EENS7_IS5_EE
	movq	-1400(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1493
.L1521:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22541:
	.size	_ZN2v88internal58FastPackedArrayReverse23ATFastPackedSmiElements5ATSmi_1430EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE, .-_ZN2v88internal58FastPackedArrayReverse23ATFastPackedSmiElements5ATSmi_1430EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE
	.section	.text._ZN2v88internal147FastPackedArrayReverse26ATFastPackedObjectElements90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_1431EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal147FastPackedArrayReverse26ATFastPackedObjectElements90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_1431EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE
	.type	_ZN2v88internal147FastPackedArrayReverse26ATFastPackedObjectElements90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_1431EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE, @function
_ZN2v88internal147FastPackedArrayReverse26ATFastPackedObjectElements90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_1431EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE:
.LFB22545:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-1192(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1328(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$1400, %rsp
	movq	%rsi, -1344(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1328(%rbp)
	movq	%rdi, -1248(%rbp)
	movl	$72, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1248(%rbp), %rax
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -1336(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1328(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1000(%rbp), %rcx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1384(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1048(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1328(%rbp), %rax
	movl	$120, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-808(%rbp), %rcx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1392(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -856(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1328(%rbp), %rax
	movl	$120, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-616(%rbp), %rcx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1408(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -664(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1328(%rbp), %rax
	movl	$72, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	leaq	-424(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1400(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -472(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1328(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	leaq	-232(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1376(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1344(%rbp), %r9
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	%r13, -88(%rbp)
	leaq	-1280(%rbp), %r13
	movq	%r9, -96(%rbp)
	movaps	%xmm0, -1280(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -1264(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm1
	movq	%r13, %rsi
	movq	-1336(%rbp), %rdi
	leaq	24(%rax), %rdx
	movq	%rax, -1280(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm1, (%rax)
	movq	%rdx, -1264(%rbp)
	movq	%rdx, -1272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1523
	call	_ZdlPv@PLT
.L1523:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1184(%rbp)
	jne	.L1553
	leaq	-1288(%rbp), %rax
	movq	%rax, -1352(%rbp)
	leaq	-1296(%rbp), %rax
	movq	%rax, -1344(%rbp)
	leaq	-1056(%rbp), %rax
	movq	%rax, -1368(%rbp)
.L1524:
	leaq	-864(%rbp), %rax
	cmpq	$0, -992(%rbp)
	leaq	-672(%rbp), %r15
	movq	%rax, -1360(%rbp)
	jne	.L1555
	cmpq	$0, -800(%rbp)
	jne	.L1556
.L1529:
	cmpq	$0, -608(%rbp)
	leaq	-480(%rbp), %rbx
	jne	.L1557
.L1531:
	cmpq	$0, -416(%rbp)
	leaq	-288(%rbp), %r14
	jne	.L1558
.L1532:
	movq	-1376(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -1296(%rbp)
	movq	$0, -1288(%rbp)
	movq	$0, -1280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1352(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	-1344(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EE
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-1360(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-1368(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-1336(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1559
	addq	$1400, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1553:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	$0, -1304(%rbp)
	movq	$0, -1296(%rbp)
	movq	$0, -1288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1296(%rbp), %rax
	movq	-1336(%rbp), %rdi
	leaq	-1288(%rbp), %rcx
	movq	%rax, %rdx
	leaq	-1304(%rbp), %rsi
	movq	%rcx, -1352(%rbp)
	movq	%rax, -1344(%rbp)
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EE
	movl	$60, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$61, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1288(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r8, -1360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-1360(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %rbx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$63, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1288(%rbp), %rax
	movl	$40, %edi
	movq	-1304(%rbp), %xmm0
	movq	%r15, -72(%rbp)
	movhps	-1296(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1280(%rbp)
	movq	%rbx, -64(%rbp)
	movq	$0, -1264(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-96(%rbp), %xmm2
	movq	%r13, %rsi
	movdqa	-80(%rbp), %xmm3
	leaq	40(%rax), %rdx
	movq	%rax, -1280(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	leaq	-1056(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1264(%rbp)
	movq	%rdx, -1272(%rbp)
	movq	%rax, -1368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1525
	call	_ZdlPv@PLT
.L1525:
	movq	-1384(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1524
	.p2align 4,,10
	.p2align 3
.L1555:
	movq	-1384(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -1320(%rbp)
	movq	$0, -1312(%rbp)
	movq	$0, -1304(%rbp)
	movq	$0, -1296(%rbp)
	movq	$0, -1288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1352(%rbp), %r9
	movq	-1344(%rbp), %r8
	leaq	-1304(%rbp), %rcx
	movq	-1368(%rbp), %rdi
	leaq	-1312(%rbp), %rdx
	leaq	-1320(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiES5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESD_SD_
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1296(%rbp), %rbx
	movq	-1288(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1312(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	-1304(%rbp), %rsi
	movq	-1296(%rbp), %rdx
	movq	-1320(%rbp), %rax
	movl	$40, %edi
	movaps	%xmm0, -1280(%rbp)
	movq	-1288(%rbp), %r15
	movq	%rcx, -1440(%rbp)
	movq	%rsi, -1424(%rbp)
	movq	%rdx, -1416(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rax, -1432(%rbp)
	movq	%rax, -96(%rbp)
	movq	%r15, -64(%rbp)
	movq	$0, -1264(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movq	-1360(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -1280(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1264(%rbp)
	movq	%rdx, -1272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1527
	call	_ZdlPv@PLT
.L1527:
	movq	-1432(%rbp), %xmm0
	movl	$40, %edi
	movq	%r15, -64(%rbp)
	leaq	-672(%rbp), %r15
	movq	$0, -1264(%rbp)
	movhps	-1440(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-1424(%rbp), %xmm0
	movhps	-1416(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1280(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	leaq	40(%rax), %rdx
	movq	%rax, -1280(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1264(%rbp)
	movq	%rdx, -1272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1528
	call	_ZdlPv@PLT
.L1528:
	movq	-1408(%rbp), %rcx
	movq	-1392(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -800(%rbp)
	je	.L1529
.L1556:
	movq	-1392(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -1320(%rbp)
	movq	$0, -1312(%rbp)
	movq	$0, -1304(%rbp)
	movq	$0, -1296(%rbp)
	movq	$0, -1288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1352(%rbp), %r9
	movq	-1344(%rbp), %r8
	leaq	-1304(%rbp), %rcx
	movq	-1360(%rbp), %rdi
	leaq	-1312(%rbp), %rdx
	leaq	-1320(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiES5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESD_SD_
	movl	$64, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1296(%rbp), %rcx
	movq	-1312(%rbp), %rdx
	movq	%r14, %rdi
	movq	-1320(%rbp), %rsi
	call	_ZN2v88internal134LoadElement26ATFastPackedObjectElements90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_29EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE
	movl	$65, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1288(%rbp), %rcx
	movq	-1312(%rbp), %rdx
	movq	%r14, %rdi
	movq	-1320(%rbp), %rsi
	call	_ZN2v88internal134LoadElement26ATFastPackedObjectElements90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_29EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE
	movl	$66, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -1392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1392(%rbp), %r8
	movq	%r14, %rdi
	movq	-1296(%rbp), %rcx
	movq	-1312(%rbp), %rdx
	movq	-1320(%rbp), %rsi
	call	_ZN2v88internal135StoreElement26ATFastPackedObjectElements90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_32EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEENS4_INS0_6ObjectEEE
	movl	$67, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1288(%rbp), %rcx
	movq	%rbx, %r8
	movq	%r14, %rdi
	movq	-1312(%rbp), %rdx
	movq	-1320(%rbp), %rsi
	call	_ZN2v88internal135StoreElement26ATFastPackedObjectElements90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_32EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEENS4_INS0_6ObjectEEE
	movl	$68, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	movl	$1, %esi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1296(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r8, -1392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-1392(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %rbx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -1416(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$69, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	movl	$1, %esi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1288(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r8, -1392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-1392(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %r14
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$63, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1416(%rbp), %rcx
	movq	-1304(%rbp), %rax
	movl	$40, %edi
	movq	-1320(%rbp), %xmm0
	movq	%rbx, -64(%rbp)
	movq	%rcx, -72(%rbp)
	movhps	-1312(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1280(%rbp)
	movq	$0, -1264(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	movq	-1368(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -1280(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -1264(%rbp)
	movq	%rdx, -1272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1530
	call	_ZdlPv@PLT
.L1530:
	movq	-1384(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1529
	.p2align 4,,10
	.p2align 3
.L1558:
	movq	-1400(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -1296(%rbp)
	movq	$0, -1288(%rbp)
	movq	$0, -1280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1352(%rbp), %rdx
	movq	%r13, %rcx
	movq	%rbx, %rdi
	movq	-1344(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EE
	movq	-1296(%rbp), %rsi
	movq	-1280(%rbp), %rcx
	movq	%r14, %rdi
	movq	-1288(%rbp), %rdx
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE9AddInputsENS1_5TNodeIS3_EENS7_IS4_EENS7_IS5_EE
	movq	-1376(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1532
	.p2align 4,,10
	.p2align 3
.L1557:
	movq	-1408(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -1312(%rbp)
	movq	$0, -1304(%rbp)
	movq	$0, -1296(%rbp)
	movq	$0, -1288(%rbp)
	movq	$0, -1280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1352(%rbp), %r8
	movq	%r13, %r9
	movq	-1344(%rbp), %rcx
	leaq	-1304(%rbp), %rdx
	leaq	-1312(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiES5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESD_SD_
	movq	%r12, %rdi
	movl	$59, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rdi
	movl	$156, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1312(%rbp), %rsi
	movq	-1296(%rbp), %rcx
	movq	%rbx, %rdi
	movq	-1304(%rbp), %rdx
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE9AddInputsENS1_5TNodeIS3_EENS7_IS4_EENS7_IS5_EE
	movq	-1400(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1531
.L1559:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22545:
	.size	_ZN2v88internal147FastPackedArrayReverse26ATFastPackedObjectElements90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_1431EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE, .-_ZN2v88internal147FastPackedArrayReverse26ATFastPackedObjectElements90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_1431EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE
	.section	.text._ZN2v88internal65FastPackedArrayReverse26ATFastPackedDoubleElements9ATfloat64_1432EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal65FastPackedArrayReverse26ATFastPackedDoubleElements9ATfloat64_1432EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE
	.type	_ZN2v88internal65FastPackedArrayReverse26ATFastPackedDoubleElements9ATfloat64_1432EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE, @function
_ZN2v88internal65FastPackedArrayReverse26ATFastPackedDoubleElements9ATfloat64_1432EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE:
.LFB22546:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-1192(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1328(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$1400, %rsp
	movq	%rsi, -1344(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1328(%rbp)
	movq	%rdi, -1248(%rbp)
	movl	$72, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1248(%rbp), %rax
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -1336(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1328(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1000(%rbp), %rcx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1384(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1048(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1328(%rbp), %rax
	movl	$120, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-808(%rbp), %rcx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1392(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -856(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1328(%rbp), %rax
	movl	$120, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-616(%rbp), %rcx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1408(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -664(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1328(%rbp), %rax
	movl	$72, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	leaq	-424(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1400(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -472(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1328(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	leaq	-232(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1376(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1344(%rbp), %r9
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	%r13, -88(%rbp)
	leaq	-1280(%rbp), %r13
	movq	%r9, -96(%rbp)
	movaps	%xmm0, -1280(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -1264(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm1
	movq	%r13, %rsi
	movq	-1336(%rbp), %rdi
	leaq	24(%rax), %rdx
	movq	%rax, -1280(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm1, (%rax)
	movq	%rdx, -1264(%rbp)
	movq	%rdx, -1272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1561
	call	_ZdlPv@PLT
.L1561:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1184(%rbp)
	jne	.L1591
	leaq	-1288(%rbp), %rax
	movq	%rax, -1352(%rbp)
	leaq	-1296(%rbp), %rax
	movq	%rax, -1344(%rbp)
	leaq	-1056(%rbp), %rax
	movq	%rax, -1368(%rbp)
.L1562:
	leaq	-864(%rbp), %rax
	cmpq	$0, -992(%rbp)
	leaq	-672(%rbp), %r15
	movq	%rax, -1360(%rbp)
	jne	.L1593
	cmpq	$0, -800(%rbp)
	jne	.L1594
.L1567:
	cmpq	$0, -608(%rbp)
	leaq	-480(%rbp), %rbx
	jne	.L1595
.L1569:
	cmpq	$0, -416(%rbp)
	leaq	-288(%rbp), %r14
	jne	.L1596
.L1570:
	movq	-1376(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -1296(%rbp)
	movq	$0, -1288(%rbp)
	movq	$0, -1280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1352(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	-1344(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EE
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-1360(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-1368(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-1336(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1597
	addq	$1400, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1591:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	$0, -1304(%rbp)
	movq	$0, -1296(%rbp)
	movq	$0, -1288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1296(%rbp), %rax
	movq	-1336(%rbp), %rdi
	leaq	-1288(%rbp), %rcx
	movq	%rax, %rdx
	leaq	-1304(%rbp), %rsi
	movq	%rcx, -1352(%rbp)
	movq	%rax, -1344(%rbp)
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EE
	movl	$60, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$61, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1288(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r8, -1360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-1360(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %rbx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$63, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1288(%rbp), %rax
	movl	$40, %edi
	movq	-1304(%rbp), %xmm0
	movq	%r15, -72(%rbp)
	movhps	-1296(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1280(%rbp)
	movq	%rbx, -64(%rbp)
	movq	$0, -1264(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-96(%rbp), %xmm2
	movq	%r13, %rsi
	movdqa	-80(%rbp), %xmm3
	leaq	40(%rax), %rdx
	movq	%rax, -1280(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	leaq	-1056(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1264(%rbp)
	movq	%rdx, -1272(%rbp)
	movq	%rax, -1368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1563
	call	_ZdlPv@PLT
.L1563:
	movq	-1384(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1562
	.p2align 4,,10
	.p2align 3
.L1593:
	movq	-1384(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -1320(%rbp)
	movq	$0, -1312(%rbp)
	movq	$0, -1304(%rbp)
	movq	$0, -1296(%rbp)
	movq	$0, -1288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1352(%rbp), %r9
	movq	-1344(%rbp), %r8
	leaq	-1304(%rbp), %rcx
	movq	-1368(%rbp), %rdi
	leaq	-1312(%rbp), %rdx
	leaq	-1320(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiES5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESD_SD_
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1296(%rbp), %rbx
	movq	-1288(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1312(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	-1304(%rbp), %rsi
	movq	-1296(%rbp), %rdx
	movq	-1320(%rbp), %rax
	movl	$40, %edi
	movaps	%xmm0, -1280(%rbp)
	movq	-1288(%rbp), %r15
	movq	%rcx, -1440(%rbp)
	movq	%rsi, -1424(%rbp)
	movq	%rdx, -1416(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rax, -1432(%rbp)
	movq	%rax, -96(%rbp)
	movq	%r15, -64(%rbp)
	movq	$0, -1264(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movq	-1360(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -1280(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1264(%rbp)
	movq	%rdx, -1272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1565
	call	_ZdlPv@PLT
.L1565:
	movq	-1432(%rbp), %xmm0
	movl	$40, %edi
	movq	%r15, -64(%rbp)
	leaq	-672(%rbp), %r15
	movq	$0, -1264(%rbp)
	movhps	-1440(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-1424(%rbp), %xmm0
	movhps	-1416(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1280(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	leaq	40(%rax), %rdx
	movq	%rax, -1280(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1264(%rbp)
	movq	%rdx, -1272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1566
	call	_ZdlPv@PLT
.L1566:
	movq	-1408(%rbp), %rcx
	movq	-1392(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -800(%rbp)
	je	.L1567
.L1594:
	movq	-1392(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -1320(%rbp)
	movq	$0, -1312(%rbp)
	movq	$0, -1304(%rbp)
	movq	$0, -1296(%rbp)
	movq	$0, -1288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1352(%rbp), %r9
	movq	-1344(%rbp), %r8
	leaq	-1304(%rbp), %rcx
	movq	-1360(%rbp), %rdi
	leaq	-1312(%rbp), %rdx
	leaq	-1320(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiES5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESD_SD_
	movl	$64, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1296(%rbp), %rcx
	movq	-1312(%rbp), %rdx
	movq	%r14, %rdi
	movq	-1320(%rbp), %rsi
	call	_ZN2v88internal52LoadElement26ATFastPackedDoubleElements9ATfloat64_30EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE
	movl	$65, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1288(%rbp), %rcx
	movq	-1312(%rbp), %rdx
	movq	%r14, %rdi
	movq	-1320(%rbp), %rsi
	call	_ZN2v88internal52LoadElement26ATFastPackedDoubleElements9ATfloat64_30EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE
	movl	$66, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -1392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1392(%rbp), %r8
	movq	%r14, %rdi
	movq	-1296(%rbp), %rcx
	movq	-1312(%rbp), %rdx
	movq	-1320(%rbp), %rsi
	call	_ZN2v88internal53StoreElement26ATFastPackedDoubleElements9ATfloat64_33EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEENS4_INS0_8Float64TEEE
	movl	$67, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1288(%rbp), %rcx
	movq	%rbx, %r8
	movq	%r14, %rdi
	movq	-1312(%rbp), %rdx
	movq	-1320(%rbp), %rsi
	call	_ZN2v88internal53StoreElement26ATFastPackedDoubleElements9ATfloat64_33EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEENS4_INS0_8Float64TEEE
	movl	$68, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	movl	$1, %esi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1296(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r8, -1392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-1392(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %rbx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -1416(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$69, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	movl	$1, %esi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1288(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r8, -1392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-1392(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %r14
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$63, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1416(%rbp), %rcx
	movq	-1304(%rbp), %rax
	movl	$40, %edi
	movq	-1320(%rbp), %xmm0
	movq	%rbx, -64(%rbp)
	movq	%rcx, -72(%rbp)
	movhps	-1312(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1280(%rbp)
	movq	$0, -1264(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	movq	-1368(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -1280(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -1264(%rbp)
	movq	%rdx, -1272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1568
	call	_ZdlPv@PLT
.L1568:
	movq	-1384(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1567
	.p2align 4,,10
	.p2align 3
.L1596:
	movq	-1400(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -1296(%rbp)
	movq	$0, -1288(%rbp)
	movq	$0, -1280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1352(%rbp), %rdx
	movq	%r13, %rcx
	movq	%rbx, %rdi
	movq	-1344(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EE
	movq	-1296(%rbp), %rsi
	movq	-1280(%rbp), %rcx
	movq	%r14, %rdi
	movq	-1288(%rbp), %rdx
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE9AddInputsENS1_5TNodeIS3_EENS7_IS4_EENS7_IS5_EE
	movq	-1376(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1570
	.p2align 4,,10
	.p2align 3
.L1595:
	movq	-1408(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -1312(%rbp)
	movq	$0, -1304(%rbp)
	movq	$0, -1296(%rbp)
	movq	$0, -1288(%rbp)
	movq	$0, -1280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1352(%rbp), %r8
	movq	%r13, %r9
	movq	-1344(%rbp), %rcx
	leaq	-1304(%rbp), %rdx
	leaq	-1312(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiES5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESD_SD_
	movq	%r12, %rdi
	movl	$59, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rdi
	movl	$159, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1312(%rbp), %rsi
	movq	-1296(%rbp), %rcx
	movq	%rbx, %rdi
	movq	-1304(%rbp), %rdx
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_14FixedArrayBaseENS0_3SmiEEE9AddInputsENS1_5TNodeIS3_EENS7_IS4_EENS7_IS5_EE
	movq	-1400(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1569
.L1597:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22546:
	.size	_ZN2v88internal65FastPackedArrayReverse26ATFastPackedDoubleElements9ATfloat64_1432EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE, .-_ZN2v88internal65FastPackedArrayReverse26ATFastPackedDoubleElements9ATfloat64_1432EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE
	.section	.text._ZN2v88internal28TryFastPackedArrayReverse_35EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28TryFastPackedArrayReverse_35EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal28TryFastPackedArrayReverse_35EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal28TryFastPackedArrayReverse_35EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE:
.LFB22503:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-3104(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-3168(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-2976(%rbp), %rbx
	subq	$3304, %rsp
	movq	%rcx, -3296(%rbp)
	movq	%rsi, -3312(%rbp)
	movq	%r12, %rsi
	movq	%rdx, -3328(%rbp)
	movl	$2, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -3168(%rbp)
	movq	%rbx, %rdi
	movq	%rbx, -3288(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2784(%rbp), %rax
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3280(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2592(%rbp), %rax
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3208(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2400(%rbp), %rax
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2208(%rbp), %rax
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2016(%rbp), %rax
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3232(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1824(%rbp), %rax
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1632(%rbp), %rax
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3224(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1440(%rbp), %rax
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3248(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1248(%rbp), %rax
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3216(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1056(%rbp), %rax
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3184(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-864(%rbp), %rax
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3200(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-672(%rbp), %rax
	movl	$2, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-480(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-288(%rbp), %rax
	movl	$2, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movq	-3312(%rbp), %xmm0
	movaps	%xmm1, -3104(%rbp)
	movhps	-3328(%rbp), %xmm0
	movq	$0, -3088(%rbp)
	movaps	%xmm0, -3312(%rbp)
	call	_Znwm@PLT
	movdqa	-3312(%rbp), %xmm0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -3104(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -3088(%rbp)
	movq	%rdx, -3096(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1599
	call	_ZdlPv@PLT
.L1599:
	leaq	-2920(%rbp), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2912(%rbp)
	jne	.L1701
	cmpq	$0, -2720(%rbp)
	jne	.L1702
.L1604:
	cmpq	$0, -2528(%rbp)
	jne	.L1703
.L1606:
	cmpq	$0, -2336(%rbp)
	jne	.L1704
.L1610:
	cmpq	$0, -2144(%rbp)
	jne	.L1705
.L1612:
	cmpq	$0, -1952(%rbp)
	jne	.L1706
.L1615:
	cmpq	$0, -1760(%rbp)
	jne	.L1707
.L1617:
	cmpq	$0, -1568(%rbp)
	jne	.L1708
.L1620:
	cmpq	$0, -1376(%rbp)
	jne	.L1709
.L1622:
	cmpq	$0, -1184(%rbp)
	jne	.L1710
.L1624:
	cmpq	$0, -992(%rbp)
	jne	.L1711
.L1626:
	cmpq	$0, -800(%rbp)
	jne	.L1712
.L1628:
	cmpq	$0, -608(%rbp)
	leaq	-232(%rbp), %r14
	jne	.L1713
	cmpq	$0, -416(%rbp)
	jne	.L1714
.L1632:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -3088(%rbp)
	movaps	%xmm0, -3104(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	-3176(%rbp), %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -3104(%rbp)
	movq	%rdx, -3088(%rbp)
	movq	%rdx, -3096(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1634
	call	_ZdlPv@PLT
.L1634:
	movq	-3176(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3192(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3272(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3200(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3184(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3216(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3248(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3224(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3256(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3232(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3264(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3240(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3208(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3280(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3288(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1715
	addq	$3304, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1701:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	$0, -3152(%rbp)
	leaq	-3136(%rbp), %r15
	movq	$0, -3144(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3288(%rbp), %rdi
	leaq	-3144(%rbp), %rdx
	leaq	-3152(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EE
	movl	$147, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3144(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	-3152(%rbp), %rsi
	call	_ZN2v88internal24Cast13ATFastJSArray_1429EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	leaq	-96(%rbp), %rsi
	leaq	-64(%rbp), %rdx
	movq	%r15, %rdi
	movq	-3152(%rbp), %xmm0
	movq	%rax, %xmm2
	movq	-3144(%rbp), %xmm1
	movq	$0, -3120(%rbp)
	movhps	-3144(%rbp), %xmm0
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -80(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3208(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1601
	call	_ZdlPv@PLT
.L1601:
	leaq	-2536(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3096(%rbp)
	jne	.L1716
.L1602:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2720(%rbp)
	je	.L1604
.L1702:
	leaq	-2728(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3152(%rbp)
	movq	$0, -3144(%rbp)
	movq	$0, -3136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3280(%rbp), %rdi
	leaq	-3136(%rbp), %rcx
	leaq	-3144(%rbp), %rdx
	leaq	-3152(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_
	movq	-3192(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	$0, -3088(%rbp)
	movaps	%xmm0, -3104(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1605
	call	_ZdlPv@PLT
.L1605:
	leaq	-424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2528(%rbp)
	je	.L1606
.L1703:
	leaq	-2536(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-96(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-92(%rbp), %rdx
	movaps	%xmm0, -3104(%rbp)
	movl	$117966855, -96(%rbp)
	movq	$0, -3088(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3208(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3104(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1607
	call	_ZdlPv@PLT
.L1607:
	movq	(%rbx), %rax
	movl	$149, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	%rcx, -3328(%rbp)
	movq	%rbx, -3312(%rbp)
	movq	24(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE(%rip), %esi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	%rbx, -3344(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler19LoadMapElementsKindENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$150, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal59FromConstexpr14ATElementsKind24ATconstexpr_ElementsKind_167EPNS0_8compiler18CodeAssemblerStateENS0_12ElementsKindE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %xmm3
	pxor	%xmm0, %xmm0
	movq	-3344(%rbp), %xmm2
	movl	$32, %edi
	movaps	%xmm0, -3104(%rbp)
	movq	$0, -3088(%rbp)
	punpcklqdq	%xmm3, %xmm2
	movq	-3312(%rbp), %xmm3
	movaps	%xmm2, -3344(%rbp)
	movhps	-3328(%rbp), %xmm3
	movaps	%xmm2, -80(%rbp)
	movaps	%xmm3, -3312(%rbp)
	movaps	%xmm3, -96(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movq	%r13, %rsi
	movq	-3240(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -3104(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -3088(%rbp)
	movq	%rdx, -3096(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1608
	call	_ZdlPv@PLT
.L1608:
	movdqa	-3312(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-3344(%rbp), %xmm7
	movaps	%xmm0, -3104(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	movq	$0, -3088(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	movq	%r13, %rsi
	movq	-3264(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -3104(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -3088(%rbp)
	movq	%rdx, -3096(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1609
	call	_ZdlPv@PLT
.L1609:
	leaq	-2152(%rbp), %rcx
	leaq	-2344(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2336(%rbp)
	je	.L1610
.L1704:
	leaq	-2344(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3160(%rbp)
	movq	$0, -3152(%rbp)
	movq	$0, -3144(%rbp)
	movq	$0, -3136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3240(%rbp), %rdi
	leaq	-3136(%rbp), %r8
	leaq	-3144(%rbp), %rcx
	leaq	-3152(%rbp), %rdx
	leaq	-3160(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7JSArrayENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE
	movl	$151, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3144(%rbp), %rdx
	movq	-3160(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal30EnsureWriteableFastElements_50EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEE@PLT
	movl	$153, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	movq	-3144(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE(%rip), %esi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3144(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler21LoadFastJSArrayLengthENS0_8compiler11SloppyTNodeINS0_7JSArrayEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$152, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r14, %rdi
	movq	-3160(%rbp), %rsi
	call	_ZN2v88internal58FastPackedArrayReverse23ATFastPackedSmiElements5ATSmi_1430EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE
	movl	$150, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$32, %edi
	movq	-3144(%rbp), %xmm0
	movq	-3160(%rbp), %xmm1
	movq	$0, -3088(%rbp)
	movhps	-3136(%rbp), %xmm0
	movhps	-3152(%rbp), %xmm1
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -3104(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movq	%r13, %rsi
	movq	-3200(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -3104(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -3088(%rbp)
	movq	%rdx, -3096(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1611
	call	_ZdlPv@PLT
.L1611:
	leaq	-808(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2144(%rbp)
	je	.L1612
.L1705:
	leaq	-2152(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3160(%rbp)
	movq	$0, -3152(%rbp)
	movq	$0, -3144(%rbp)
	movq	$0, -3136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3264(%rbp), %rdi
	leaq	-3144(%rbp), %rcx
	leaq	-3136(%rbp), %r8
	leaq	-3152(%rbp), %rdx
	leaq	-3160(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7JSArrayENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE
	movl	$154, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$2, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal59FromConstexpr14ATElementsKind24ATconstexpr_ElementsKind_167EPNS0_8compiler18CodeAssemblerStateENS0_12ElementsKindE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3136(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-3144(%rbp), %xmm4
	movq	-3160(%rbp), %xmm5
	movaps	%xmm0, -3104(%rbp)
	movhps	-3136(%rbp), %xmm4
	movq	$0, -3088(%rbp)
	movhps	-3152(%rbp), %xmm5
	movaps	%xmm4, -3328(%rbp)
	movaps	%xmm5, -3312(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%r13, %rsi
	movq	-3232(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -3104(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -3088(%rbp)
	movq	%rdx, -3096(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1613
	call	_ZdlPv@PLT
.L1613:
	movdqa	-3312(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-3328(%rbp), %xmm3
	movaps	%xmm0, -3104(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movq	$0, -3088(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movq	%r13, %rsi
	movq	-3256(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -3104(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -3088(%rbp)
	movq	%rdx, -3096(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1614
	call	_ZdlPv@PLT
.L1614:
	leaq	-1768(%rbp), %rcx
	leaq	-1960(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1952(%rbp)
	je	.L1615
.L1706:
	leaq	-1960(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3160(%rbp)
	movq	$0, -3152(%rbp)
	movq	$0, -3144(%rbp)
	movq	$0, -3136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3232(%rbp), %rdi
	leaq	-3136(%rbp), %r8
	leaq	-3144(%rbp), %rcx
	leaq	-3152(%rbp), %rdx
	leaq	-3160(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7JSArrayENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE
	movl	$155, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3144(%rbp), %rdx
	movq	-3160(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal30EnsureWriteableFastElements_50EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEE@PLT
	movl	$157, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	movq	-3144(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE(%rip), %esi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3144(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler21LoadFastJSArrayLengthENS0_8compiler11SloppyTNodeINS0_7JSArrayEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$156, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r14, %rdi
	movq	-3160(%rbp), %rsi
	call	_ZN2v88internal147FastPackedArrayReverse26ATFastPackedObjectElements90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_1431EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE
	movl	$154, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$32, %edi
	movq	-3144(%rbp), %xmm0
	movq	-3160(%rbp), %xmm1
	movq	$0, -3088(%rbp)
	movhps	-3136(%rbp), %xmm0
	movhps	-3152(%rbp), %xmm1
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -3104(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%r13, %rsi
	movq	-3184(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -3104(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -3088(%rbp)
	movq	%rdx, -3096(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1616
	call	_ZdlPv@PLT
.L1616:
	leaq	-1000(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1760(%rbp)
	je	.L1617
.L1707:
	leaq	-1768(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3160(%rbp)
	movq	$0, -3152(%rbp)
	movq	$0, -3144(%rbp)
	movq	$0, -3136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3256(%rbp), %rdi
	leaq	-3144(%rbp), %rcx
	leaq	-3136(%rbp), %r8
	leaq	-3152(%rbp), %rdx
	leaq	-3160(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7JSArrayENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE
	movl	$158, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$4, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal59FromConstexpr14ATElementsKind24ATconstexpr_ElementsKind_167EPNS0_8compiler18CodeAssemblerStateENS0_12ElementsKindE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3136(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11Word32EqualENS1_5TNodeINS0_7Word32TEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-3144(%rbp), %xmm6
	movq	-3160(%rbp), %xmm7
	movaps	%xmm0, -3104(%rbp)
	movhps	-3136(%rbp), %xmm6
	movq	$0, -3088(%rbp)
	movhps	-3152(%rbp), %xmm7
	movaps	%xmm6, -3312(%rbp)
	movaps	%xmm7, -3328(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm6, -80(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	movq	%r13, %rsi
	movq	-3224(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -3104(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -3088(%rbp)
	movq	%rdx, -3096(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1618
	call	_ZdlPv@PLT
.L1618:
	movdqa	-3328(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-3312(%rbp), %xmm5
	movaps	%xmm0, -3104(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movq	$0, -3088(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%r13, %rsi
	movq	-3248(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -3104(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -3088(%rbp)
	movq	%rdx, -3096(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1619
	call	_ZdlPv@PLT
.L1619:
	leaq	-1384(%rbp), %rcx
	leaq	-1576(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1568(%rbp)
	je	.L1620
.L1708:
	leaq	-1576(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3160(%rbp)
	movq	$0, -3152(%rbp)
	movq	$0, -3144(%rbp)
	movq	$0, -3136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3224(%rbp), %rdi
	leaq	-3136(%rbp), %r8
	leaq	-3144(%rbp), %rcx
	leaq	-3152(%rbp), %rdx
	leaq	-3160(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7JSArrayENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE
	movl	$160, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	movq	-3144(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE(%rip), %esi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3144(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler21LoadFastJSArrayLengthENS0_8compiler11SloppyTNodeINS0_7JSArrayEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$159, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r14, %rdi
	movq	-3160(%rbp), %rsi
	call	_ZN2v88internal65FastPackedArrayReverse26ATFastPackedDoubleElements9ATfloat64_1432EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE
	movl	$158, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$32, %edi
	movq	-3144(%rbp), %xmm0
	movq	-3160(%rbp), %xmm1
	movq	$0, -3088(%rbp)
	movhps	-3136(%rbp), %xmm0
	movhps	-3152(%rbp), %xmm1
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -3104(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	movq	%r13, %rsi
	movq	-3216(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -3104(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -3088(%rbp)
	movq	%rdx, -3096(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1621
	call	_ZdlPv@PLT
.L1621:
	leaq	-1192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1376(%rbp)
	je	.L1622
.L1709:
	leaq	-1384(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3160(%rbp)
	movq	$0, -3152(%rbp)
	movq	$0, -3144(%rbp)
	movq	$0, -3136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3248(%rbp), %rdi
	leaq	-3144(%rbp), %rcx
	leaq	-3136(%rbp), %r8
	leaq	-3152(%rbp), %rdx
	leaq	-3160(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7JSArrayENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE
	movl	$162, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3192(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	$0, -3088(%rbp)
	movaps	%xmm0, -3104(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1623
	call	_ZdlPv@PLT
.L1623:
	leaq	-424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1184(%rbp)
	je	.L1624
.L1710:
	leaq	-1192(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3160(%rbp)
	movq	$0, -3152(%rbp)
	movq	$0, -3144(%rbp)
	movq	$0, -3136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3216(%rbp), %rdi
	leaq	-3144(%rbp), %rcx
	leaq	-3136(%rbp), %r8
	leaq	-3152(%rbp), %rdx
	leaq	-3160(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7JSArrayENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE
	movl	$154, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$32, %edi
	movq	-3144(%rbp), %xmm0
	movq	-3160(%rbp), %xmm1
	movq	$0, -3088(%rbp)
	movhps	-3136(%rbp), %xmm0
	movhps	-3152(%rbp), %xmm1
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -3104(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movq	%r13, %rsi
	movq	-3184(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -3104(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -3088(%rbp)
	movq	%rdx, -3096(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1625
	call	_ZdlPv@PLT
.L1625:
	leaq	-1000(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -992(%rbp)
	je	.L1626
.L1711:
	leaq	-1000(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3160(%rbp)
	movq	$0, -3152(%rbp)
	movq	$0, -3144(%rbp)
	movq	$0, -3136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3184(%rbp), %rdi
	leaq	-3144(%rbp), %rcx
	leaq	-3136(%rbp), %r8
	leaq	-3152(%rbp), %rdx
	leaq	-3160(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7JSArrayENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE
	movl	$150, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$32, %edi
	movq	-3144(%rbp), %xmm0
	movq	-3160(%rbp), %xmm1
	movq	$0, -3088(%rbp)
	movhps	-3136(%rbp), %xmm0
	movhps	-3152(%rbp), %xmm1
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -3104(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%r13, %rsi
	movq	-3200(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -3104(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -3088(%rbp)
	movq	%rdx, -3096(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1627
	call	_ZdlPv@PLT
.L1627:
	leaq	-808(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L1628
.L1712:
	leaq	-808(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3160(%rbp)
	movq	$0, -3152(%rbp)
	movq	$0, -3144(%rbp)
	movq	$0, -3136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3200(%rbp), %rdi
	leaq	-3144(%rbp), %rcx
	leaq	-3136(%rbp), %r8
	leaq	-3152(%rbp), %rdx
	leaq	-3160(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7JSArrayENS0_6Int32TEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE
	movl	$146, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$145, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movq	-3160(%rbp), %xmm0
	movaps	%xmm1, -3104(%rbp)
	movhps	-3152(%rbp), %xmm0
	movq	$0, -3088(%rbp)
	movaps	%xmm0, -3312(%rbp)
	call	_Znwm@PLT
	movdqa	-3312(%rbp), %xmm0
	movq	-3272(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -3104(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -3088(%rbp)
	movq	%rdx, -3096(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1629
	call	_ZdlPv@PLT
.L1629:
	leaq	-616(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1628
	.p2align 4,,10
	.p2align 3
.L1713:
	leaq	-616(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3144(%rbp)
	movq	$0, -3136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3272(%rbp), %rdi
	leaq	-3136(%rbp), %rdx
	leaq	-3144(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EE
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movq	-3144(%rbp), %xmm0
	movaps	%xmm1, -3104(%rbp)
	movhps	-3136(%rbp), %xmm0
	movq	$0, -3088(%rbp)
	movaps	%xmm0, -3312(%rbp)
	call	_Znwm@PLT
	movdqa	-3312(%rbp), %xmm0
	movq	-3176(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -3104(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -3088(%rbp)
	movq	%rdx, -3096(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1631
	call	_ZdlPv@PLT
.L1631:
	leaq	-232(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -416(%rbp)
	je	.L1632
.L1714:
	leaq	-424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3192(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	$0, -3088(%rbp)
	movaps	%xmm0, -3104(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1633
	call	_ZdlPv@PLT
.L1633:
	movq	-3296(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1632
	.p2align 4,,10
	.p2align 3
.L1716:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3144(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	-3152(%rbp), %rdx
	movl	$24, %edi
	movaps	%xmm0, -3136(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -3120(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm2
	movq	%r15, %rsi
	movq	-3280(%rbp), %rdi
	leaq	24(%rax), %rdx
	movq	%rax, -3136(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -3120(%rbp)
	movq	%rdx, -3128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1603
	call	_ZdlPv@PLT
.L1603:
	leaq	-2728(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1602
.L1715:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22503:
	.size	_ZN2v88internal28TryFastPackedArrayReverse_35EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, .-_ZN2v88internal28TryFastPackedArrayReverse_35EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal30ArrayPrototypeReverseAssembler33GenerateArrayPrototypeReverseImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30ArrayPrototypeReverseAssembler33GenerateArrayPrototypeReverseImplEv
	.type	_ZN2v88internal30ArrayPrototypeReverseAssembler33GenerateArrayPrototypeReverseImplEv, @function
_ZN2v88internal30ArrayPrototypeReverseAssembler33GenerateArrayPrototypeReverseImplEv:
.LFB22521:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-824(%rbp), %r15
	leaq	-880(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1208, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, -1200(%rbp)
	movq	%rax, -1128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r13, %rcx
	movq	%r12, %rsi
	leaq	-1120(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-1104(%rbp), %rcx
	movl	$2, %esi
	movq	%r12, %rdi
	movq	-1120(%rbp), %r13
	movq	-1112(%rbp), %rax
	movq	%r12, -1056(%rbp)
	leaq	-1128(%rbp), %r12
	movq	%rcx, -1192(%rbp)
	movq	%rcx, -1040(%rbp)
	movq	%r13, -1024(%rbp)
	movq	%rax, -1168(%rbp)
	movq	$1, -1048(%rbp)
	movq	%rax, -1032(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rax, -1184(%rbp)
	leaq	-1056(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1208(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -872(%rbp)
	movq	$0, -864(%rbp)
	movq	%rax, %rbx
	movq	-1128(%rbp), %rax
	movq	$0, -856(%rbp)
	movq	%rax, -880(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r15, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -856(%rbp)
	movq	%rdx, -864(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -840(%rbp)
	movq	%rax, -872(%rbp)
	movq	$0, -848(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1128(%rbp), %rax
	movl	$144, %edi
	movq	$0, -680(%rbp)
	movq	$0, -672(%rbp)
	movq	%rax, -688(%rbp)
	movq	$0, -664(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -680(%rbp)
	leaq	-632(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -664(%rbp)
	movq	%rdx, -672(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -648(%rbp)
	movq	%rax, -1160(%rbp)
	movq	$0, -656(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1128(%rbp), %rax
	movl	$144, %edi
	movq	$0, -488(%rbp)
	movq	$0, -480(%rbp)
	movq	%rax, -496(%rbp)
	movq	$0, -472(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -488(%rbp)
	leaq	-440(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -472(%rbp)
	movq	%rdx, -480(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -456(%rbp)
	movq	%rax, -1152(%rbp)
	movq	$0, -464(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1128(%rbp), %rax
	movl	$120, %edi
	movq	$0, -296(%rbp)
	movq	$0, -288(%rbp)
	movq	%rax, -304(%rbp)
	movq	$0, -280(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -296(%rbp)
	leaq	-248(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -280(%rbp)
	movq	%rdx, -288(%rbp)
	xorl	%edx, %edx
	movq	%rax, -1144(%rbp)
	movups	%xmm0, -264(%rbp)
	movq	$0, -272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %xmm1
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movhps	-1168(%rbp), %xmm1
	movq	%rbx, -80(%rbp)
	leaq	-1008(%rbp), %r13
	movaps	%xmm1, -112(%rbp)
	movq	-1192(%rbp), %xmm1
	movaps	%xmm0, -1008(%rbp)
	movhps	-1184(%rbp), %xmm1
	movq	$0, -992(%rbp)
	movaps	%xmm1, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	leaq	40(%rax), %rdx
	movq	%rax, -1008(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -992(%rbp)
	movq	%rdx, -1000(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1008(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1718
	call	_ZdlPv@PLT
.L1718:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -816(%rbp)
	jne	.L1815
	cmpq	$0, -624(%rbp)
	jne	.L1816
.L1724:
	cmpq	$0, -432(%rbp)
	jne	.L1817
.L1727:
	cmpq	$0, -240(%rbp)
	jne	.L1818
.L1729:
	movq	-1144(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1733
	call	_ZdlPv@PLT
.L1733:
	movq	-288(%rbp), %rbx
	movq	-296(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1734
	.p2align 4,,10
	.p2align 3
.L1738:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1735
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1738
.L1736:
	movq	-296(%rbp), %r13
.L1734:
	testq	%r13, %r13
	je	.L1739
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1739:
	movq	-1152(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-464(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1740
	call	_ZdlPv@PLT
.L1740:
	movq	-480(%rbp), %rbx
	movq	-488(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1741
	.p2align 4,,10
	.p2align 3
.L1745:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1742
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1745
.L1743:
	movq	-488(%rbp), %r13
.L1741:
	testq	%r13, %r13
	je	.L1746
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1746:
	movq	-1160(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1747
	call	_ZdlPv@PLT
.L1747:
	movq	-672(%rbp), %rbx
	movq	-680(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1748
	.p2align 4,,10
	.p2align 3
.L1752:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1749
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1752
.L1750:
	movq	-680(%rbp), %r13
.L1748:
	testq	%r13, %r13
	je	.L1753
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1753:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-848(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1754
	call	_ZdlPv@PLT
.L1754:
	movq	-864(%rbp), %rbx
	movq	-872(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1755
	.p2align 4,,10
	.p2align 3
.L1759:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1756
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1759
.L1757:
	movq	-872(%rbp), %r13
.L1755:
	testq	%r13, %r13
	je	.L1760
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1760:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1819
	addq	$1208, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1756:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1759
	jmp	.L1757
	.p2align 4,,10
	.p2align 3
.L1749:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1752
	jmp	.L1750
	.p2align 4,,10
	.p2align 3
.L1735:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1738
	jmp	.L1736
	.p2align 4,,10
	.p2align 3
.L1742:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1745
	jmp	.L1743
	.p2align 4,,10
	.p2align 3
.L1815:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-112(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	%rax, %rsi
	leaq	-107(%rbp), %rdx
	movq	%rax, -1168(%rbp)
	movaps	%xmm0, -1008(%rbp)
	movl	$117769477, -112(%rbp)
	movb	$8, -108(%rbp)
	movq	$0, -992(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1008(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1720
	call	_ZdlPv@PLT
.L1720:
	movq	(%rbx), %rax
	movl	$170, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	24(%rax), %r14
	movq	32(%rax), %rbx
	movq	%rsi, -1192(%rbp)
	movq	16(%rax), %rsi
	movq	%rcx, -1184(%rbp)
	movq	%rsi, -1232(%rbp)
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	-1200(%rbp), %rdi
	call	_ZN2v88internal28TryFastPackedArrayReverse_35EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	movq	%r14, %xmm7
	movq	-1168(%rbp), %rsi
	movq	-1232(%rbp), %xmm5
	movq	%rbx, %xmm4
	leaq	-1088(%rbp), %r14
	leaq	-64(%rbp), %rbx
	movq	-1184(%rbp), %xmm6
	punpcklqdq	%xmm4, %xmm4
	punpcklqdq	%xmm7, %xmm5
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdx
	movhps	-1192(%rbp), %xmm6
	movq	%r14, %rdi
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm4, -1248(%rbp)
	movaps	%xmm5, -1232(%rbp)
	movaps	%xmm6, -1184(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm0, -1088(%rbp)
	movq	$0, -1072(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-496(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1721
	call	_ZdlPv@PLT
.L1721:
	movq	-1152(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1000(%rbp)
	jne	.L1820
.L1722:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -624(%rbp)
	je	.L1724
.L1816:
	movq	-1160(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-688(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -992(%rbp)
	movaps	%xmm0, -1008(%rbp)
	call	_Znwm@PLT
	movl	$2056, %esi
	movq	%r14, %rdi
	movw	%si, 4(%rax)
	leaq	6(%rax), %rdx
	movq	%r13, %rsi
	movl	$117769477, (%rax)
	movq	%rax, -1008(%rbp)
	movq	%rdx, -992(%rbp)
	movq	%rdx, -1000(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1008(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1725
	call	_ZdlPv@PLT
.L1725:
	movq	(%rbx), %rax
	leaq	-112(%rbp), %rsi
	leaq	-72(%rbp), %rdx
	movq	%r13, %rdi
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm1
	movq	32(%rax), %rax
	movq	$0, -992(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -1008(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-304(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1008(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1726
	call	_ZdlPv@PLT
.L1726:
	movq	-1144(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -432(%rbp)
	je	.L1727
.L1817:
	movq	-1152(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-496(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -992(%rbp)
	movaps	%xmm0, -1008(%rbp)
	call	_Znwm@PLT
	movl	$2056, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117769477, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -1008(%rbp)
	movq	%rdx, -992(%rbp)
	movq	%rdx, -1000(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1008(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1728
	call	_ZdlPv@PLT
.L1728:
	movq	(%rbx), %rax
	movl	$171, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	32(%rax), %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1208(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	cmpq	$0, -240(%rbp)
	je	.L1729
.L1818:
	movq	-1144(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	movq	$0, -1168(%rbp)
	leaq	-304(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -992(%rbp)
	movaps	%xmm0, -1008(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -1008(%rbp)
	movq	%rdx, -992(%rbp)
	movq	%rdx, -1000(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1008(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1730
	call	_ZdlPv@PLT
.L1730:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	movq	24(%rax), %rdx
	movq	32(%rax), %rax
	testq	%rdx, %rdx
	cmovne	%rdx, %r14
	testq	%rax, %rax
	cmove	-1168(%rbp), %rax
	movl	$174, %edx
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1200(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	call	_ZN2v88internal22GenericArrayReverse_34EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	movq	-1208(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	jmp	.L1729
	.p2align 4,,10
	.p2align 3
.L1820:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movdqa	-1184(%rbp), %xmm7
	movq	-1168(%rbp), %rsi
	movaps	%xmm0, -1088(%rbp)
	movq	$0, -1072(%rbp)
	movaps	%xmm7, -112(%rbp)
	movdqa	-1232(%rbp), %xmm7
	movaps	%xmm7, -96(%rbp)
	movdqa	-1248(%rbp), %xmm7
	movaps	%xmm7, -80(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-688(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1723
	call	_ZdlPv@PLT
.L1723:
	movq	-1160(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1722
.L1819:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22521:
	.size	_ZN2v88internal30ArrayPrototypeReverseAssembler33GenerateArrayPrototypeReverseImplEv, .-_ZN2v88internal30ArrayPrototypeReverseAssembler33GenerateArrayPrototypeReverseImplEv
	.section	.rodata._ZN2v88internal8Builtins30Generate_ArrayPrototypeReverseEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/array-reverse-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins30Generate_ArrayPrototypeReverseEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"ArrayPrototypeReverse"
	.section	.text._ZN2v88internal8Builtins30Generate_ArrayPrototypeReverseEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins30Generate_ArrayPrototypeReverseEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins30Generate_ArrayPrototypeReverseEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins30Generate_ArrayPrototypeReverseEPNS0_8compiler18CodeAssemblerStateE:
.LFB22517:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1707, %ecx
	leaq	.LC7(%rip), %rdx
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$776, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L1825
.L1822:
	movq	%r13, %rdi
	call	_ZN2v88internal30ArrayPrototypeReverseAssembler33GenerateArrayPrototypeReverseImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1826
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1825:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L1822
.L1826:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22517:
	.size	_ZN2v88internal8Builtins30Generate_ArrayPrototypeReverseEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins30Generate_ArrayPrototypeReverseEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal45LoadElement23ATFastPackedSmiElements5ATSmi_28EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal45LoadElement23ATFastPackedSmiElements5ATSmi_28EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE, @function
_GLOBAL__sub_I__ZN2v88internal45LoadElement23ATFastPackedSmiElements5ATSmi_28EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE:
.LFB29561:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29561:
	.size	_GLOBAL__sub_I__ZN2v88internal45LoadElement23ATFastPackedSmiElements5ATSmi_28EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE, .-_GLOBAL__sub_I__ZN2v88internal45LoadElement23ATFastPackedSmiElements5ATSmi_28EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal45LoadElement23ATFastPackedSmiElements5ATSmi_28EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_14FixedArrayBaseEEENS4_INS0_3SmiEEE
	.weak	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.byte	7
	.byte	7
	.byte	6
	.byte	7
	.byte	7
	.byte	5
	.byte	5
	.byte	6
	.byte	6
	.byte	5
	.byte	7
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	7
	.align 16
.LC6:
	.byte	7
	.byte	7
	.byte	6
	.byte	8
	.byte	7
	.byte	7
	.byte	5
	.byte	5
	.byte	6
	.byte	6
	.byte	5
	.byte	7
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
