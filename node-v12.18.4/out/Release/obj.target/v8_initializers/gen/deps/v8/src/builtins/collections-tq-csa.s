	.file	"collections-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0:
.LFB29365:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	%rsi, %r13
	pushq	%rbx
	movq	%r13, %rax
	sarq	$3, %rax
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%r13, %r13
	js	.L14
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%rdx, %r14
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.L9
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%rax, %rdi
.L9:
	leaq	(%rdi,%r13), %r15
	movq	%rdi, (%rbx)
	movq	%r15, 16(%rbx)
	cmpq	%r12, %r14
	je	.L10
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
.L10:
	movq	%r15, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29365:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0:
.LFB29363:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	movabsq	$384307168202282325, %rax
	cmpq	%rax, %rdx
	ja	.L32
	pxor	%xmm0, %xmm0
	leaq	(%rdx,%rdx,2), %r14
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	%rdx, %r12
	movups	%xmm0, 8(%rdi)
	salq	$3, %r14
	xorl	%esi, %esi
	movq	$0, 24(%rdi)
	testq	%rdx, %rdx
	je	.L21
	movq	%r14, %rdi
	call	_Znwm@PLT
	leaq	-1(%r12), %rdx
	leaq	(%rax,%r14), %rsi
	movq	%rax, 8(%rbx)
	movq	%rsi, 24(%rbx)
	cmpq	$2, %rdx
	jbe	.L22
	movq	%r12, %rdi
	movq	%r12, %rcx
	pxor	%xmm0, %xmm0
	movq	%rax, %rdx
	shrq	%rdi
	andq	$-2, %rcx
	addq	%rdi, %rcx
	salq	$4, %rcx
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L19:
	movups	%xmm0, (%rdx)
	addq	$48, %rdx
	movups	%xmm0, -32(%rdx)
	movups	%xmm0, -16(%rdx)
	cmpq	%rcx, %rdx
	jne	.L19
	movq	%r12, %rdx
	movq	%r12, %rcx
	andq	$-2, %rdx
	andl	$1, %ecx
	leaq	(%rdx,%rdx,2), %rdi
	leaq	(%rax,%rdi,8), %rax
	cmpq	%rdx, %r12
	je	.L21
.L18:
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	cmpq	$1, %rcx
	je	.L21
	movq	$0, 40(%rax)
	movups	%xmm0, 24(%rax)
	cmpq	$2, %rcx
	je	.L21
	movq	$0, 64(%rax)
	movups	%xmm0, 48(%rax)
.L21:
	movq	%rsi, 16(%rbx)
	pxor	%xmm0, %xmm0
	leaq	56(%rbx), %rdi
	movq	%r13, %rsi
	movups	%xmm0, 32(%rbx)
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	$0, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movq	%r12, %rcx
	jmp	.L18
.L32:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29363:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev,"axG",@progbits,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev:
.LFB22413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$56, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L34
	call	_ZdlPv@PLT
.L34:
	movq	16(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	%r12, %r13
	je	.L35
	.p2align 4,,10
	.p2align 3
.L39:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L36
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L39
.L37:
	movq	8(%rbx), %r12
.L35:
	testq	%r12, %r12
	je	.L33
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L39
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L33:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22413:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev
	.set	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_EE9AddInputsENS1_5TNodeIS3_EENS6_IS4_EES8_S8_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_EE9AddInputsENS1_5TNodeIS3_EENS6_IS4_EES8_S8_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_EE9AddInputsENS1_5TNodeIS3_EENS6_IS4_EES8_S8_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_EE9AddInputsENS1_5TNodeIS3_EENS6_IS4_EES8_S8_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_EE9AddInputsENS1_5TNodeIS3_EENS6_IS4_EES8_S8_:
.LFB26653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %xmm0
	movq	%rdx, %xmm2
	movq	%rsi, %xmm1
	punpcklqdq	%xmm2, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$32, %edi
	subq	$104, %rsp
	movq	%r8, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movhps	-104(%rbp), %xmm0
	movaps	%xmm1, -64(%rbp)
	movaps	%xmm0, -48(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	$0, -80(%rbp)
	call	_Znwm@PLT
	movdqa	-64(%rbp), %xmm3
	movq	%r12, %rdi
	leaq	-96(%rbp), %rsi
	movdqa	-48(%rbp), %xmm4
	leaq	32(%rax), %rdx
	movq	%rax, -96(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -80(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L45
	call	_ZdlPv@PLT
.L45:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L52
	addq	$104, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L52:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26653:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_EE9AddInputsENS1_5TNodeIS3_EENS6_IS4_EES8_S8_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_EE9AddInputsENS1_5TNodeIS3_EENS6_IS4_EES8_S8_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_:
.LFB26654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	$4, %edi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	leaq	-80(%rbp), %rsi
	movl	$134744071, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L54
	call	_ZdlPv@PLT
.L54:
	movq	(%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L55
	movq	%rdx, (%r14)
.L55:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L56
	movq	%rdx, 0(%r13)
.L56:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L57
	movq	%rdx, (%r12)
.L57:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L53
	movq	%rax, (%rbx)
.L53:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L76
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L76:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26654:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_3SmiENS0_14FixedArrayBaseENS0_10FixedArrayES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_3SmiENS0_14FixedArrayBaseENS0_10FixedArrayES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_3SmiENS0_14FixedArrayBaseENS0_10FixedArrayES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_3SmiENS0_14FixedArrayBaseENS0_10FixedArrayES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_3SmiENS0_14FixedArrayBaseENS0_10FixedArrayES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_:
.LFB26680:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$8, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$578438799609563143, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L78
	movq	%rax, -120(%rbp)
	call	_ZdlPv@PLT
	movq	-120(%rbp), %rax
.L78:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L79
	movq	%rdx, (%r15)
.L79:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L80
	movq	%rdx, (%r14)
.L80:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L81
	movq	%rdx, 0(%r13)
.L81:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L82
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L82:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L83
	movq	%rdx, (%rbx)
.L83:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L84
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L84:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L85
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L85:
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L77
	movq	-112(%rbp), %rbx
	movq	%rax, (%rbx)
.L77:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L116
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L116:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26680:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_3SmiENS0_14FixedArrayBaseENS0_10FixedArrayES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_3SmiENS0_14FixedArrayBaseENS0_10FixedArrayES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_3SmiENS0_14FixedArrayBaseENS0_16FixedDoubleArrayES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_3SmiENS0_14FixedArrayBaseENS0_16FixedDoubleArrayES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_3SmiENS0_14FixedArrayBaseENS0_16FixedDoubleArrayES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_3SmiENS0_14FixedArrayBaseENS0_16FixedDoubleArrayES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_3SmiENS0_14FixedArrayBaseENS0_16FixedDoubleArrayES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_:
.LFB26693:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$8, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$578438799609563143, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L118
	movq	%rax, -120(%rbp)
	call	_ZdlPv@PLT
	movq	-120(%rbp), %rax
.L118:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L119
	movq	%rdx, (%r15)
.L119:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L120
	movq	%rdx, (%r14)
.L120:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L121
	movq	%rdx, 0(%r13)
.L121:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L122
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L122:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L123
	movq	%rdx, (%rbx)
.L123:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L124
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L124:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L125
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L125:
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L117
	movq	-112(%rbp), %rbx
	movq	%rax, (%rbx)
.L117:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L156
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L156:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26693:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_3SmiENS0_14FixedArrayBaseENS0_16FixedDoubleArrayES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_3SmiENS0_14FixedArrayBaseENS0_16FixedDoubleArrayES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_
	.section	.rodata._ZN2v88internal33LoadKeyValuePairNoSideEffects_267EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../../deps/v8/../../deps/v8/src/builtins/collections.tq"
	.section	.text._ZN2v88internal33LoadKeyValuePairNoSideEffects_267EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33LoadKeyValuePairNoSideEffects_267EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal33LoadKeyValuePairNoSideEffects_267EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal33LoadKeyValuePairNoSideEffects_267EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE:
.LFB22410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$2744, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -6792(%rbp)
	leaq	-6496(%rbp), %r12
	leaq	-6272(%rbp), %rbx
	movq	%rdi, %r14
	movq	%rsi, -6512(%rbp)
	movq	%r12, %rsi
	leaq	-6400(%rbp), %r15
	movq	%rdx, -6520(%rbp)
	movl	$2, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -6496(%rbp)
	movq	%rbx, %rdi
	movq	%rbx, -6776(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-6080(%rbp), %rax
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-5888(%rbp), %rax
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6576(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-5696(%rbp), %rax
	movl	$7, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-5504(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-5312(%rbp), %rax
	movl	$7, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-5120(%rbp), %rax
	movl	$7, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-4928(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6624(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-4736(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-4544(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6632(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-4352(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6640(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-4160(%rbp), %rax
	movl	$9, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6608(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3968(%rbp), %rax
	movl	$9, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6536(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3776(%rbp), %rax
	movl	$6, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6600(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3584(%rbp), %rax
	movl	$7, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3392(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3200(%rbp), %rax
	movl	$7, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6664(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3008(%rbp), %rax
	movl	$7, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2816(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6688(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2624(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6552(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2432(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2240(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2048(%rbp), %rax
	movl	$9, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6672(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1856(%rbp), %rax
	movl	$9, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6568(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1664(%rbp), %rax
	movl	$6, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6712(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1472(%rbp), %rax
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1280(%rbp), %rax
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6768(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1088(%rbp), %rax
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6720(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-896(%rbp), %rax
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6728(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-704(%rbp), %rax
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-512(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6736(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-320(%rbp), %rax
	movl	$4, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movq	-6512(%rbp), %xmm0
	movaps	%xmm1, -6400(%rbp)
	movhps	-6520(%rbp), %xmm0
	movq	$0, -6384(%rbp)
	movaps	%xmm0, -6512(%rbp)
	call	_Znwm@PLT
	movdqa	-6512(%rbp), %xmm0
	movq	%rbx, %rdi
	movq	%r15, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L158
	call	_ZdlPv@PLT
.L158:
	leaq	-6216(%rbp), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-6432(%rbp), %rax
	cmpq	$0, -6208(%rbp)
	movq	%rax, -6512(%rbp)
	jne	.L406
.L159:
	leaq	-6440(%rbp), %rax
	cmpq	$0, -6016(%rbp)
	movq	%rax, -6528(%rbp)
	leaq	-6448(%rbp), %rax
	movq	%rax, -6520(%rbp)
	jne	.L407
.L163:
	cmpq	$0, -5824(%rbp)
	jne	.L408
	cmpq	$0, -5632(%rbp)
	jne	.L409
.L170:
	cmpq	$0, -5440(%rbp)
	jne	.L410
.L173:
	cmpq	$0, -5248(%rbp)
	jne	.L411
.L177:
	cmpq	$0, -5056(%rbp)
	jne	.L412
.L180:
	cmpq	$0, -4864(%rbp)
	jne	.L413
.L183:
	cmpq	$0, -4672(%rbp)
	jne	.L414
.L185:
	cmpq	$0, -4480(%rbp)
	jne	.L415
.L188:
	cmpq	$0, -4288(%rbp)
	jne	.L416
.L190:
	cmpq	$0, -4096(%rbp)
	jne	.L417
.L192:
	cmpq	$0, -3904(%rbp)
	jne	.L418
.L195:
	cmpq	$0, -3712(%rbp)
	jne	.L419
.L197:
	cmpq	$0, -3520(%rbp)
	jne	.L420
.L202:
	cmpq	$0, -3328(%rbp)
	jne	.L421
.L205:
	cmpq	$0, -3136(%rbp)
	jne	.L422
.L209:
	cmpq	$0, -2944(%rbp)
	jne	.L423
.L212:
	cmpq	$0, -2752(%rbp)
	jne	.L424
.L215:
	cmpq	$0, -2560(%rbp)
	jne	.L425
.L217:
	cmpq	$0, -2368(%rbp)
	jne	.L426
.L220:
	cmpq	$0, -2176(%rbp)
	jne	.L427
.L222:
	cmpq	$0, -1984(%rbp)
	jne	.L428
.L224:
	cmpq	$0, -1792(%rbp)
	jne	.L429
.L227:
	cmpq	$0, -1600(%rbp)
	jne	.L430
.L229:
	cmpq	$0, -1408(%rbp)
	jne	.L431
.L231:
	cmpq	$0, -1216(%rbp)
	jne	.L432
.L235:
	cmpq	$0, -1024(%rbp)
	jne	.L433
.L237:
	cmpq	$0, -832(%rbp)
	jne	.L434
.L240:
	cmpq	$0, -640(%rbp)
	leaq	-264(%rbp), %r13
	jne	.L435
.L242:
	cmpq	$0, -448(%rbp)
	jne	.L436
.L243:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$0, -6448(%rbp)
	movq	$0, -6440(%rbp)
	movq	$0, -6432(%rbp)
	movq	$0, -6400(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-6784(%rbp), %rbx
	movq	%r15, %r8
	movq	-6512(%rbp), %rcx
	movq	-6528(%rbp), %rdx
	movq	-6520(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_
	movq	%rbx, %rdi
	movq	-6432(%rbp), %r14
	movq	-6400(%rbp), %r13
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6736(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6560(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6728(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6720(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6768(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6648(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6712(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6568(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6672(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6656(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6696(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6552(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6688(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6680(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6664(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6704(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6760(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6600(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6536(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6608(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6640(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6632(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6544(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6624(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6616(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6592(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6584(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6752(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6576(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6744(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6776(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L437
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	movq	%r13, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L406:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -6384(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	-6776(%rbp), %rdi
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r15, %rsi
	movq	%rax, -6400(%rbp)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L160
	call	_ZdlPv@PLT
.L160:
	movq	(%rbx), %rax
	movl	$11, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %r13
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$12, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal24Cast13ATFastJSArray_1429EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %xmm7
	movq	%rbx, %xmm0
	movl	$40, %edi
	punpcklqdq	%xmm7, %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	%r13, %xmm0
	movq	$0, -6416(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -6432(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm7
	leaq	40(%rax), %rdx
	movq	%rax, -6432(%rbp)
	movq	%rcx, 32(%rax)
	movq	-6576(%rbp), %rdi
	movups	%xmm4, (%rax)
	movups	%xmm7, 16(%rax)
	leaq	-6432(%rbp), %rax
	movq	%rax, %rsi
	movq	%rdx, -6416(%rbp)
	movq	%rdx, -6424(%rbp)
	movq	%rax, -6512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L161
	call	_ZdlPv@PLT
.L161:
	leaq	-5832(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -6392(%rbp)
	jne	.L438
.L162:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L408:
	leaq	-5832(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -6384(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movq	-6576(%rbp), %rdi
	movq	%r15, %rsi
	movl	$134744071, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -6400(%rbp)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L166
	call	_ZdlPv@PLT
.L166:
	movq	(%rbx), %rax
	movl	$13, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rbx
	movq	(%rax), %rcx
	movq	32(%rax), %r13
	movq	%rbx, -6832(%rbp)
	movq	16(%rax), %rbx
	movq	%rcx, -6816(%rbp)
	movq	%rbx, -6848(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler21LoadFastJSArrayLengthENS0_8compiler11SloppyTNodeINS0_7JSArrayEEE@PLT
	movq	%r15, %rdi
	movq	%rax, -6864(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$14, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE(%rip), %esi
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$15, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal20Cast10FixedArray_103EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm7
	pxor	%xmm0, %xmm0
	movq	-6864(%rbp), %xmm4
	movq	-6848(%rbp), %xmm5
	movl	$64, %edi
	movq	%rbx, -80(%rbp)
	movq	-6816(%rbp), %xmm6
	punpcklqdq	%xmm7, %xmm4
	movq	%r13, %xmm7
	movq	%rax, -72(%rbp)
	punpcklqdq	%xmm7, %xmm5
	movaps	%xmm4, -6864(%rbp)
	movhps	-6832(%rbp), %xmm6
	movaps	%xmm5, -6848(%rbp)
	movaps	%xmm6, -6816(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm0, -6432(%rbp)
	movq	$0, -6416(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm7
	movq	-6584(%rbp), %rdi
	movq	-6512(%rbp), %rsi
	leaq	64(%rax), %rdx
	movq	%rax, -6432(%rbp)
	movups	%xmm4, (%rax)
	movdqa	-112(%rbp), %xmm4
	movups	%xmm7, 32(%rax)
	movups	%xmm4, 16(%rax)
	movdqa	-80(%rbp), %xmm4
	movq	%rdx, -6416(%rbp)
	movups	%xmm4, 48(%rax)
	movq	%rdx, -6424(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L167
	call	_ZdlPv@PLT
.L167:
	leaq	-5448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -6392(%rbp)
	jne	.L439
.L168:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -5632(%rbp)
	je	.L170
.L409:
	leaq	-5640(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -6384(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movl	$1798, %ecx
	movq	-6752(%rbp), %rdi
	movq	%r15, %rsi
	movl	$117966855, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -6400(%rbp)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L171
	call	_ZdlPv@PLT
.L171:
	movq	(%rbx), %rax
	movl	$48, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movdqu	(%rax), %xmm5
	movq	$0, -6384(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm4
	movq	-6600(%rbp), %rdi
	leaq	48(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L172
	call	_ZdlPv@PLT
.L172:
	leaq	-3720(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -5440(%rbp)
	je	.L173
.L410:
	leaq	-5448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -6384(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movq	-6584(%rbp), %rdi
	movq	%r15, %rsi
	movabsq	$506381205571635207, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L174
	call	_ZdlPv@PLT
.L174:
	movq	(%rbx), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	24(%rax), %rdx
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	%rdx, -6800(%rbp)
	movq	40(%rax), %rdx
	movq	%rcx, -6832(%rbp)
	movq	32(%rax), %rcx
	movq	%rbx, -6848(%rbp)
	movq	16(%rax), %rbx
	movq	%rdx, -6880(%rbp)
	movl	$17, %edx
	movq	%rcx, -6816(%rbp)
	movq	%rbx, -6864(%rbp)
	movq	56(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-6816(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17IntPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movq	-6816(%rbp), %xmm7
	movq	-6864(%rbp), %xmm3
	movaps	%xmm0, -6400(%rbp)
	movq	-6832(%rbp), %xmm4
	movhps	-6880(%rbp), %xmm7
	movq	%rbx, -80(%rbp)
	movhps	-6800(%rbp), %xmm3
	movhps	-6848(%rbp), %xmm4
	movaps	%xmm7, -6880(%rbp)
	movaps	%xmm3, -6864(%rbp)
	movaps	%xmm4, -6816(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	$0, -6384(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm5
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	56(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movq	%rcx, 48(%rax)
	movq	-6592(%rbp), %rdi
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L175
	call	_ZdlPv@PLT
.L175:
	movdqa	-6816(%rbp), %xmm4
	movdqa	-6864(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movdqa	-6880(%rbp), %xmm6
	movq	%rbx, -80(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm0, -6400(%rbp)
	movq	$0, -6384(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm7
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	leaq	56(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movq	%rcx, 48(%rax)
	movq	-6616(%rbp), %rdi
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L176
	call	_ZdlPv@PLT
.L176:
	leaq	-5064(%rbp), %rcx
	leaq	-5256(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -5248(%rbp)
	je	.L177
.L411:
	leaq	-5256(%rbp), %rsi
	movq	%r12, %rdi
	movl	$1798, %r13d
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -6384(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movq	-6592(%rbp), %rdi
	movq	%r15, %rsi
	movl	$117966855, (%rax)
	leaq	7(%rax), %rdx
	movw	%r13w, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -6400(%rbp)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L178
	call	_ZdlPv@PLT
.L178:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	8(%rax), %rcx
	movq	24(%rax), %rdx
	movq	40(%rax), %rsi
	movq	(%rax), %rbx
	movq	%rcx, -6816(%rbp)
	movq	16(%rax), %rcx
	movq	%rdx, -6848(%rbp)
	movq	32(%rax), %rdx
	movq	48(%rax), %r13
	movq	%rsi, -6800(%rbp)
	xorl	%esi, %esi
	movq	%rcx, -6832(%rbp)
	movq	%rdx, -6864(%rbp)
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal25LoadElementOrUndefined_51EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_3SmiEEE@PLT
	movq	%rbx, %xmm0
	movl	$64, %edi
	movq	%r13, -80(%rbp)
	movhps	-6816(%rbp), %xmm0
	movq	%rax, -72(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	-6832(%rbp), %xmm0
	movq	$0, -6384(%rbp)
	movhps	-6848(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-6864(%rbp), %xmm0
	movhps	-6800(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	leaq	64(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movq	-6624(%rbp), %rdi
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L179
	call	_ZdlPv@PLT
.L179:
	leaq	-4872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -5056(%rbp)
	je	.L180
.L412:
	leaq	-5064(%rbp), %rsi
	movq	%r12, %rdi
	movl	$1798, %ebx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -6384(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movq	-6616(%rbp), %rdi
	movq	%r15, %rsi
	movw	%bx, 4(%rax)
	leaq	7(%rax), %rdx
	movl	$117966855, (%rax)
	movb	$7, 6(%rax)
	movq	%rax, -6400(%rbp)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L181
	call	_ZdlPv@PLT
.L181:
	movq	(%rbx), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rbx
	movq	40(%rax), %rdx
	movq	(%rax), %rcx
	movq	16(%rax), %r13
	movq	%rbx, -6832(%rbp)
	movq	24(%rax), %rbx
	movq	%rdx, -6800(%rbp)
	movl	$18, %edx
	movq	%rbx, -6848(%rbp)
	movq	32(%rax), %rbx
	movq	%rcx, -6816(%rbp)
	movq	%rbx, -6864(%rbp)
	movq	48(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$17, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -6880(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$64, %edi
	movq	-6816(%rbp), %xmm0
	movq	$0, -6384(%rbp)
	movhps	-6832(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%r13, %xmm0
	movhps	-6848(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-6864(%rbp), %xmm0
	movhps	-6800(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	movhps	-6880(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	leaq	64(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movq	-6544(%rbp), %rdi
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L182
	call	_ZdlPv@PLT
.L182:
	leaq	-4680(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -4864(%rbp)
	je	.L183
.L413:
	leaq	-4872(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -6488(%rbp)
	movq	$0, -6480(%rbp)
	movq	$0, -6472(%rbp)
	movq	$0, -6464(%rbp)
	movq	$0, -6456(%rbp)
	movq	$0, -6448(%rbp)
	movq	$0, -6440(%rbp)
	movq	$0, -6432(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	pushq	-6512(%rbp)
	movq	-6624(%rbp), %rdi
	pushq	-6528(%rbp)
	leaq	-6472(%rbp), %rcx
	leaq	-6480(%rbp), %rdx
	pushq	-6520(%rbp)
	leaq	-6488(%rbp), %rsi
	leaq	-6456(%rbp), %r9
	leaq	-6464(%rbp), %r8
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_3SmiENS0_14FixedArrayBaseENS0_10FixedArrayES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_
	addq	$32, %rsp
	movl	$64, %edi
	movq	-6440(%rbp), %xmm0
	movq	-6456(%rbp), %xmm1
	movq	-6472(%rbp), %xmm2
	movq	$0, -6384(%rbp)
	movq	-6488(%rbp), %xmm3
	movhps	-6432(%rbp), %xmm0
	movhps	-6448(%rbp), %xmm1
	movhps	-6464(%rbp), %xmm2
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-6480(%rbp), %xmm3
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm2
	leaq	64(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movq	-6544(%rbp), %rdi
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm2, 48(%rax)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L184
	call	_ZdlPv@PLT
.L184:
	leaq	-4680(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -4672(%rbp)
	je	.L185
.L414:
	leaq	-4680(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -6488(%rbp)
	movq	$0, -6480(%rbp)
	movq	$0, -6472(%rbp)
	movq	$0, -6464(%rbp)
	movq	$0, -6456(%rbp)
	movq	$0, -6448(%rbp)
	movq	$0, -6440(%rbp)
	movq	$0, -6432(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	pushq	-6512(%rbp)
	movq	-6544(%rbp), %rdi
	pushq	-6528(%rbp)
	leaq	-6472(%rbp), %rcx
	leaq	-6456(%rbp), %r9
	pushq	-6520(%rbp)
	leaq	-6464(%rbp), %r8
	leaq	-6480(%rbp), %rdx
	leaq	-6488(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_3SmiENS0_14FixedArrayBaseENS0_10FixedArrayES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_
	addq	$32, %rsp
	movl	$19, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-6456(%rbp), %rbx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17IntPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	-6440(%rbp), %xmm5
	movq	-6456(%rbp), %xmm6
	movaps	%xmm0, -6400(%rbp)
	movq	-6472(%rbp), %xmm7
	movq	-6488(%rbp), %xmm2
	movhps	-6432(%rbp), %xmm5
	movq	$0, -6384(%rbp)
	movhps	-6448(%rbp), %xmm6
	movhps	-6464(%rbp), %xmm7
	movaps	%xmm5, -6832(%rbp)
	movhps	-6480(%rbp), %xmm2
	movaps	%xmm6, -6816(%rbp)
	movaps	%xmm7, -6848(%rbp)
	movaps	%xmm2, -6864(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm1
	movdqa	-80(%rbp), %xmm7
	leaq	64(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movq	-6632(%rbp), %rdi
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L186
	call	_ZdlPv@PLT
.L186:
	movdqa	-6864(%rbp), %xmm4
	movdqa	-6848(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movdqa	-6816(%rbp), %xmm5
	movdqa	-6832(%rbp), %xmm6
	movaps	%xmm0, -6400(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm6, -80(%rbp)
	movq	$0, -6384(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm1
	movdqa	-112(%rbp), %xmm7
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm2
	leaq	64(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movq	-6640(%rbp), %rdi
	movups	%xmm1, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm2, 48(%rax)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L187
	call	_ZdlPv@PLT
.L187:
	leaq	-4296(%rbp), %rcx
	leaq	-4488(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -4480(%rbp)
	je	.L188
.L415:
	leaq	-4488(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -6488(%rbp)
	movq	$0, -6480(%rbp)
	movq	$0, -6472(%rbp)
	movq	$0, -6464(%rbp)
	movq	$0, -6456(%rbp)
	movq	$0, -6448(%rbp)
	movq	$0, -6440(%rbp)
	movq	$0, -6432(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	pushq	-6512(%rbp)
	movq	-6632(%rbp), %rdi
	pushq	-6528(%rbp)
	leaq	-6456(%rbp), %r9
	leaq	-6464(%rbp), %r8
	pushq	-6520(%rbp)
	leaq	-6472(%rbp), %rcx
	leaq	-6480(%rbp), %rdx
	leaq	-6488(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_3SmiENS0_14FixedArrayBaseENS0_10FixedArrayES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_
	addq	$32, %rsp
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	-6488(%rbp), %rsi
	movq	-6440(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal25LoadElementOrUndefined_51EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_3SmiEEE@PLT
	movq	-6488(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movaps	%xmm0, -6400(%rbp)
	movq	%rdx, -128(%rbp)
	movq	-6480(%rbp), %rdx
	movq	%rax, -64(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-6472(%rbp), %rdx
	movq	$0, -6384(%rbp)
	movq	%rdx, -112(%rbp)
	movq	-6464(%rbp), %rdx
	movq	%rdx, -104(%rbp)
	movq	-6456(%rbp), %rdx
	movq	%rdx, -96(%rbp)
	movq	-6448(%rbp), %rdx
	movq	%rdx, -88(%rbp)
	movq	-6440(%rbp), %rdx
	movq	%rdx, -80(%rbp)
	movq	-6432(%rbp), %rdx
	movq	%rdx, -72(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-128(%rbp), %xmm5
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm1
	leaq	72(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movdqa	-80(%rbp), %xmm7
	movq	-6608(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L189
	call	_ZdlPv@PLT
.L189:
	leaq	-4104(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -4288(%rbp)
	je	.L190
.L416:
	leaq	-4296(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -6488(%rbp)
	movq	$0, -6480(%rbp)
	movq	$0, -6472(%rbp)
	movq	$0, -6464(%rbp)
	movq	$0, -6456(%rbp)
	movq	$0, -6448(%rbp)
	movq	$0, -6440(%rbp)
	movq	$0, -6432(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	pushq	-6512(%rbp)
	movq	-6640(%rbp), %rdi
	pushq	-6528(%rbp)
	leaq	-6472(%rbp), %rcx
	leaq	-6456(%rbp), %r9
	pushq	-6520(%rbp)
	leaq	-6464(%rbp), %r8
	leaq	-6480(%rbp), %rdx
	leaq	-6488(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_3SmiENS0_14FixedArrayBaseENS0_10FixedArrayES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_
	addq	$32, %rsp
	movl	$20, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$19, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$72, %edi
	movq	%rbx, -64(%rbp)
	movq	-6440(%rbp), %xmm0
	movq	-6456(%rbp), %xmm1
	movq	-6472(%rbp), %xmm2
	movq	$0, -6384(%rbp)
	movq	-6488(%rbp), %xmm3
	movhps	-6432(%rbp), %xmm0
	movhps	-6448(%rbp), %xmm1
	movhps	-6464(%rbp), %xmm2
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-6480(%rbp), %xmm3
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	leaq	72(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movdqa	-80(%rbp), %xmm5
	movq	-6536(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L191
	call	_ZdlPv@PLT
.L191:
	leaq	-3912(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -4096(%rbp)
	je	.L192
.L417:
	leaq	-4104(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -6384(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movq	-6608(%rbp), %rdi
	movq	%r15, %rsi
	movabsq	$578438799609563143, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -6400(%rbp)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L193
	call	_ZdlPv@PLT
.L193:
	movq	(%rbx), %rax
	movl	$72, %edi
	movdqu	48(%rax), %xmm0
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	64(%rax), %rdx
	movdqu	(%rax), %xmm6
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -64(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -6400(%rbp)
	movq	$0, -6384(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-128(%rbp), %xmm1
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm4
	leaq	72(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movdqa	-80(%rbp), %xmm2
	movq	-6536(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm1, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm2, 48(%rax)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L194
	call	_ZdlPv@PLT
.L194:
	leaq	-3912(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3904(%rbp)
	je	.L195
.L418:
	leaq	-3912(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -6384(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movq	-6536(%rbp), %rdi
	movq	%r15, %rsi
	movabsq	$578438799609563143, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -6400(%rbp)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L196
	call	_ZdlPv@PLT
.L196:
	movq	(%rbx), %rax
	movl	$16, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %r9
	movq	56(%rax), %rcx
	movq	(%rax), %r13
	movq	64(%rax), %rbx
	movq	%r9, -6832(%rbp)
	movq	%rcx, -6816(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-6832(%rbp), %r9
	movq	%rbx, %r8
	movq	%r13, %rsi
	movq	-6816(%rbp), %rcx
	movq	-6560(%rbp), %rdi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_EE9AddInputsENS1_5TNodeIS3_EENS6_IS4_EES8_S8_
	leaq	-648(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3712(%rbp)
	je	.L197
.L419:
	leaq	-3720(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -6384(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movl	$1798, %r11d
	movq	-6600(%rbp), %rdi
	movq	%r15, %rsi
	movl	$117966855, (%rax)
	leaq	6(%rax), %rdx
	movw	%r11w, 4(%rax)
	movq	%rax, -6400(%rbp)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L198
	call	_ZdlPv@PLT
.L198:
	movq	(%rbx), %rax
	movl	$23, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rbx
	movq	(%rax), %rcx
	movq	16(%rax), %r13
	movq	%rbx, -6832(%rbp)
	movq	24(%rax), %rbx
	movq	%rcx, -6816(%rbp)
	movq	%rbx, -6848(%rbp)
	movq	32(%rax), %rbx
	movq	%rbx, -6864(%rbp)
	movq	40(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal26Cast16FixedDoubleArray_104EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm5
	pxor	%xmm0, %xmm0
	movq	-6864(%rbp), %xmm3
	movq	-6816(%rbp), %xmm6
	movl	$64, %edi
	movaps	%xmm0, -6432(%rbp)
	punpcklqdq	%xmm5, %xmm3
	movq	%r13, %xmm5
	movq	%rbx, -80(%rbp)
	movhps	-6848(%rbp), %xmm5
	movhps	-6832(%rbp), %xmm6
	movaps	%xmm3, -6864(%rbp)
	movaps	%xmm5, -6848(%rbp)
	movaps	%xmm6, -6816(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -6416(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm1
	movdqa	-80(%rbp), %xmm7
	leaq	64(%rax), %rdx
	movq	%rax, -6432(%rbp)
	movq	-6704(%rbp), %rdi
	movq	-6512(%rbp), %rsi
	movups	%xmm3, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rdx, -6416(%rbp)
	movq	%rdx, -6424(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L199
	call	_ZdlPv@PLT
.L199:
	leaq	-3336(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -6392(%rbp)
	jne	.L440
.L200:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -3520(%rbp)
	je	.L202
.L420:
	leaq	-3528(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -6384(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movl	$1798, %r10d
	movq	-6760(%rbp), %rdi
	movq	%r15, %rsi
	movl	$117966855, (%rax)
	leaq	7(%rax), %rdx
	movw	%r10w, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -6400(%rbp)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L203
	call	_ZdlPv@PLT
.L203:
	movq	(%rbx), %rax
	movl	$48, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movdqu	(%rax), %xmm4
	movq	$0, -6384(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm2
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm5
	movq	-6712(%rbp), %rdi
	leaq	48(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L204
	call	_ZdlPv@PLT
.L204:
	leaq	-1608(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3328(%rbp)
	je	.L205
.L421:
	leaq	-3336(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -6384(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movq	-6704(%rbp), %rdi
	movq	%r15, %rsi
	movabsq	$506381205571635207, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L206
	call	_ZdlPv@PLT
.L206:
	movq	(%rbx), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	24(%rax), %rdx
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	%rdx, -6800(%rbp)
	movq	40(%rax), %rdx
	movq	%rcx, -6832(%rbp)
	movq	32(%rax), %rcx
	movq	%rbx, -6848(%rbp)
	movq	16(%rax), %rbx
	movq	%rdx, -6880(%rbp)
	movl	$25, %edx
	movq	%rcx, -6816(%rbp)
	movq	%rbx, -6864(%rbp)
	movq	56(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-6816(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17IntPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movq	-6816(%rbp), %xmm7
	movq	-6864(%rbp), %xmm4
	movaps	%xmm0, -6400(%rbp)
	movq	-6832(%rbp), %xmm2
	movhps	-6880(%rbp), %xmm7
	movq	%rbx, -80(%rbp)
	movhps	-6800(%rbp), %xmm4
	movhps	-6848(%rbp), %xmm2
	movaps	%xmm7, -6816(%rbp)
	movaps	%xmm4, -6864(%rbp)
	movaps	%xmm2, -6832(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	$0, -6384(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm1
	movdqa	-96(%rbp), %xmm7
	leaq	56(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movq	%rcx, 48(%rax)
	movq	-6664(%rbp), %rdi
	movups	%xmm6, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L207
	call	_ZdlPv@PLT
.L207:
	movdqa	-6832(%rbp), %xmm4
	movdqa	-6864(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movdqa	-6816(%rbp), %xmm3
	movq	%rbx, -80(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -6400(%rbp)
	movq	$0, -6384(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm5
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm1
	leaq	56(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movq	%rcx, 48(%rax)
	movq	-6680(%rbp), %rdi
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm1, 32(%rax)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L208
	call	_ZdlPv@PLT
.L208:
	leaq	-2952(%rbp), %rcx
	leaq	-3144(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -3136(%rbp)
	je	.L209
.L422:
	leaq	-3144(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -6384(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movl	$1798, %r9d
	movq	-6664(%rbp), %rdi
	movq	%r15, %rsi
	movl	$117966855, (%rax)
	leaq	7(%rax), %rdx
	movw	%r9w, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -6400(%rbp)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L210
	call	_ZdlPv@PLT
.L210:
	movq	(%rbx), %rax
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	8(%rax), %rbx
	movq	(%rax), %rcx
	movq	40(%rax), %rdx
	movq	16(%rax), %r13
	movq	%rbx, -6832(%rbp)
	movq	24(%rax), %rbx
	movq	%rcx, -6816(%rbp)
	movq	%rbx, -6848(%rbp)
	movq	32(%rax), %rbx
	movq	%rdx, -6800(%rbp)
	movq	%rbx, -6864(%rbp)
	movq	48(%rax), %rbx
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal25LoadElementOrUndefined_52EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_16FixedDoubleArrayEEENS4_INS0_3SmiEEE@PLT
	movl	$64, %edi
	movq	%rbx, -80(%rbp)
	movq	-6816(%rbp), %xmm0
	movq	%rax, -72(%rbp)
	movhps	-6832(%rbp), %xmm0
	movq	$0, -6384(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	%r13, %xmm0
	movhps	-6848(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-6864(%rbp), %xmm0
	movhps	-6800(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm4
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	leaq	64(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movq	-6688(%rbp), %rdi
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L211
	call	_ZdlPv@PLT
.L211:
	leaq	-2760(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2944(%rbp)
	je	.L212
.L423:
	leaq	-2952(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -6384(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movl	$1798, %r8d
	movq	-6680(%rbp), %rdi
	movq	%r15, %rsi
	movl	$117966855, (%rax)
	leaq	7(%rax), %rdx
	movw	%r8w, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -6400(%rbp)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L213
	call	_ZdlPv@PLT
.L213:
	movq	(%rbx), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rbx
	movq	40(%rax), %rdx
	movq	(%rax), %rcx
	movq	16(%rax), %r13
	movq	%rbx, -6832(%rbp)
	movq	24(%rax), %rbx
	movq	%rdx, -6800(%rbp)
	movl	$26, %edx
	movq	%rbx, -6848(%rbp)
	movq	32(%rax), %rbx
	movq	%rcx, -6816(%rbp)
	movq	%rbx, -6864(%rbp)
	movq	48(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$25, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -6880(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$64, %edi
	movq	-6816(%rbp), %xmm0
	movq	$0, -6384(%rbp)
	movhps	-6832(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%r13, %xmm0
	movhps	-6848(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-6864(%rbp), %xmm0
	movhps	-6800(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	movhps	-6880(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm1
	movdqa	-80(%rbp), %xmm7
	leaq	64(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movq	-6552(%rbp), %rdi
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L214
	call	_ZdlPv@PLT
.L214:
	leaq	-2568(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2752(%rbp)
	je	.L215
.L424:
	leaq	-2760(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -6488(%rbp)
	movq	$0, -6480(%rbp)
	movq	$0, -6472(%rbp)
	movq	$0, -6464(%rbp)
	movq	$0, -6456(%rbp)
	movq	$0, -6448(%rbp)
	movq	$0, -6440(%rbp)
	movq	$0, -6432(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	pushq	-6512(%rbp)
	movq	-6688(%rbp), %rdi
	pushq	-6528(%rbp)
	leaq	-6472(%rbp), %rcx
	leaq	-6480(%rbp), %rdx
	pushq	-6520(%rbp)
	leaq	-6488(%rbp), %rsi
	leaq	-6456(%rbp), %r9
	leaq	-6464(%rbp), %r8
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_3SmiENS0_14FixedArrayBaseENS0_16FixedDoubleArrayES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_
	addq	$32, %rsp
	movl	$64, %edi
	movq	-6440(%rbp), %xmm0
	movq	-6456(%rbp), %xmm1
	movq	-6472(%rbp), %xmm2
	movq	$0, -6384(%rbp)
	movq	-6488(%rbp), %xmm3
	movhps	-6432(%rbp), %xmm0
	movhps	-6448(%rbp), %xmm1
	movhps	-6464(%rbp), %xmm2
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-6480(%rbp), %xmm3
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm2
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm5
	leaq	64(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movq	-6552(%rbp), %rdi
	movups	%xmm4, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L216
	call	_ZdlPv@PLT
.L216:
	leaq	-2568(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2560(%rbp)
	je	.L217
.L425:
	leaq	-2568(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -6488(%rbp)
	movq	$0, -6480(%rbp)
	movq	$0, -6472(%rbp)
	movq	$0, -6464(%rbp)
	movq	$0, -6456(%rbp)
	movq	$0, -6448(%rbp)
	movq	$0, -6440(%rbp)
	movq	$0, -6432(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	pushq	-6512(%rbp)
	movq	-6552(%rbp), %rdi
	pushq	-6528(%rbp)
	leaq	-6472(%rbp), %rcx
	leaq	-6456(%rbp), %r9
	pushq	-6520(%rbp)
	leaq	-6464(%rbp), %r8
	leaq	-6480(%rbp), %rdx
	leaq	-6488(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_3SmiENS0_14FixedArrayBaseENS0_16FixedDoubleArrayES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_
	addq	$32, %rsp
	movl	$27, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-6456(%rbp), %rbx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17IntPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	-6440(%rbp), %xmm1
	movq	-6456(%rbp), %xmm3
	movaps	%xmm0, -6400(%rbp)
	movq	-6472(%rbp), %xmm5
	movq	-6488(%rbp), %xmm6
	movhps	-6432(%rbp), %xmm1
	movq	$0, -6384(%rbp)
	movhps	-6448(%rbp), %xmm3
	movhps	-6464(%rbp), %xmm5
	movaps	%xmm1, -6816(%rbp)
	movhps	-6480(%rbp), %xmm6
	movaps	%xmm3, -6832(%rbp)
	movaps	%xmm5, -6848(%rbp)
	movaps	%xmm6, -6864(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm1
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm7
	movdqa	-80(%rbp), %xmm4
	leaq	64(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movq	-6696(%rbp), %rdi
	movups	%xmm6, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm4, 48(%rax)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L218
	call	_ZdlPv@PLT
.L218:
	movdqa	-6864(%rbp), %xmm2
	movdqa	-6848(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movdqa	-6832(%rbp), %xmm5
	movdqa	-6816(%rbp), %xmm6
	movaps	%xmm0, -6400(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm6, -80(%rbp)
	movq	$0, -6384(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm1
	movdqa	-112(%rbp), %xmm7
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm2
	leaq	64(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movq	-6656(%rbp), %rdi
	movups	%xmm1, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm2, 48(%rax)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L219
	call	_ZdlPv@PLT
.L219:
	leaq	-2184(%rbp), %rcx
	leaq	-2376(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2368(%rbp)
	je	.L220
.L426:
	leaq	-2376(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -6488(%rbp)
	movq	$0, -6480(%rbp)
	movq	$0, -6472(%rbp)
	movq	$0, -6464(%rbp)
	movq	$0, -6456(%rbp)
	movq	$0, -6448(%rbp)
	movq	$0, -6440(%rbp)
	movq	$0, -6432(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	pushq	-6512(%rbp)
	movq	-6696(%rbp), %rdi
	pushq	-6528(%rbp)
	leaq	-6472(%rbp), %rcx
	leaq	-6456(%rbp), %r9
	pushq	-6520(%rbp)
	leaq	-6464(%rbp), %r8
	leaq	-6480(%rbp), %rdx
	leaq	-6488(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_3SmiENS0_14FixedArrayBaseENS0_16FixedDoubleArrayES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_
	addq	$32, %rsp
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	-6440(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal25LoadElementOrUndefined_52EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_16FixedDoubleArrayEEENS4_INS0_3SmiEEE@PLT
	movq	-6488(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movaps	%xmm0, -6400(%rbp)
	movq	%rdx, -128(%rbp)
	movq	-6480(%rbp), %rdx
	movq	%rax, -64(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-6472(%rbp), %rdx
	movq	$0, -6384(%rbp)
	movq	%rdx, -112(%rbp)
	movq	-6464(%rbp), %rdx
	movq	%rdx, -104(%rbp)
	movq	-6456(%rbp), %rdx
	movq	%rdx, -96(%rbp)
	movq	-6448(%rbp), %rdx
	movq	%rdx, -88(%rbp)
	movq	-6440(%rbp), %rdx
	movq	%rdx, -80(%rbp)
	movq	-6432(%rbp), %rdx
	movq	%rdx, -72(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-128(%rbp), %xmm3
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	leaq	72(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movdqa	-80(%rbp), %xmm1
	movq	-6672(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm1, 48(%rax)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L221
	call	_ZdlPv@PLT
.L221:
	leaq	-1992(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2176(%rbp)
	je	.L222
.L427:
	leaq	-2184(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -6488(%rbp)
	movq	$0, -6480(%rbp)
	movq	$0, -6472(%rbp)
	movq	$0, -6464(%rbp)
	movq	$0, -6456(%rbp)
	movq	$0, -6448(%rbp)
	movq	$0, -6440(%rbp)
	movq	$0, -6432(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	pushq	-6512(%rbp)
	movq	-6656(%rbp), %rdi
	pushq	-6528(%rbp)
	leaq	-6472(%rbp), %rcx
	leaq	-6456(%rbp), %r9
	pushq	-6520(%rbp)
	leaq	-6464(%rbp), %r8
	leaq	-6480(%rbp), %rdx
	leaq	-6488(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_NS0_7JSArrayENS0_3SmiENS0_14FixedArrayBaseENS0_16FixedDoubleArrayES4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EESE_PNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESE_
	addq	$32, %rsp
	movl	$28, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$27, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$72, %edi
	movq	%rbx, -64(%rbp)
	movq	-6440(%rbp), %xmm0
	movq	-6456(%rbp), %xmm1
	movq	-6472(%rbp), %xmm2
	movq	$0, -6384(%rbp)
	movq	-6488(%rbp), %xmm3
	movhps	-6432(%rbp), %xmm0
	movhps	-6448(%rbp), %xmm1
	movhps	-6464(%rbp), %xmm2
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-6480(%rbp), %xmm3
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-128(%rbp), %xmm7
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm2
	leaq	72(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movdqa	-80(%rbp), %xmm3
	movq	-6568(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L223
	call	_ZdlPv@PLT
.L223:
	leaq	-1800(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1984(%rbp)
	je	.L224
.L428:
	leaq	-1992(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -6384(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movq	-6672(%rbp), %rdi
	movq	%r15, %rsi
	movabsq	$578438799609563143, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -6400(%rbp)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L225
	call	_ZdlPv@PLT
.L225:
	movq	(%rbx), %rax
	movl	$72, %edi
	movdqu	48(%rax), %xmm0
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	64(%rax), %rdx
	movdqu	(%rax), %xmm5
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -64(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -6400(%rbp)
	movq	$0, -6384(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-128(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm1
	movdqa	-96(%rbp), %xmm7
	leaq	72(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movdqa	-80(%rbp), %xmm4
	movq	-6568(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm4, 48(%rax)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L226
	call	_ZdlPv@PLT
.L226:
	leaq	-1800(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1792(%rbp)
	je	.L227
.L429:
	leaq	-1800(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -6384(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movq	-6568(%rbp), %rdi
	movq	%r15, %rsi
	movabsq	$578438799609563143, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -6400(%rbp)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L228
	call	_ZdlPv@PLT
.L228:
	movq	(%rbx), %rax
	movl	$24, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %r9
	movq	56(%rax), %rcx
	movq	(%rax), %r13
	movq	64(%rax), %rbx
	movq	%r9, -6832(%rbp)
	movq	%rcx, -6816(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-6832(%rbp), %r9
	movq	%rbx, %r8
	movq	%r13, %rsi
	movq	-6816(%rbp), %rcx
	movq	-6560(%rbp), %rdi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_EE9AddInputsENS1_5TNodeIS3_EENS6_IS4_EES8_S8_
	leaq	-648(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1600(%rbp)
	je	.L229
.L430:
	leaq	-1608(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -6384(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movl	$1798, %edi
	movq	%r15, %rsi
	movw	%di, 4(%rax)
	movq	-6712(%rbp), %rdi
	leaq	6(%rax), %rdx
	movl	$117966855, (%rax)
	movq	%rax, -6400(%rbp)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L230
	call	_ZdlPv@PLT
.L230:
	movl	$31, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$32, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -1408(%rbp)
	je	.L231
.L431:
	leaq	-1416(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -6384(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	-6648(%rbp), %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rsi
	movb	$8, 2(%rax)
	movq	%rax, -6400(%rbp)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L232
	call	_ZdlPv@PLT
.L232:
	movq	(%rbx), %rax
	movl	$36, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r13
	movq	%rcx, -6816(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal21Cast10JSReceiver_1404EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm0
	movl	$40, %edi
	movq	$0, -6416(%rbp)
	movhps	-6816(%rbp), %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	%r13, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -6432(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm2
	movdqa	-112(%rbp), %xmm3
	movq	-6720(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -6432(%rbp)
	movq	%rcx, 32(%rax)
	movq	-6512(%rbp), %rsi
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -6416(%rbp)
	movq	%rdx, -6424(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L233
	call	_ZdlPv@PLT
.L233:
	leaq	-1032(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -6392(%rbp)
	jne	.L441
.L234:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1216(%rbp)
	je	.L235
.L432:
	leaq	-1224(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -6456(%rbp)
	movq	$0, -6448(%rbp)
	movq	$0, -6440(%rbp)
	movq	$0, -6432(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-6512(%rbp), %r8
	movq	-6528(%rbp), %rcx
	leaq	-6456(%rbp), %rsi
	movq	-6520(%rbp), %rdx
	movq	-6768(%rbp), %rdi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_
	movq	-6456(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movaps	%xmm0, -6400(%rbp)
	movq	%rax, -128(%rbp)
	movq	-6448(%rbp), %rax
	movq	$0, -6384(%rbp)
	movq	%rax, -120(%rbp)
	movq	-6440(%rbp), %rax
	movq	%rax, -112(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movdqa	-128(%rbp), %xmm5
	movq	%r15, %rsi
	movq	-6728(%rbp), %rdi
	leaq	24(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L236
	call	_ZdlPv@PLT
.L236:
	leaq	-840(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1024(%rbp)
	je	.L237
.L433:
	leaq	-1032(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -6384(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movq	-6720(%rbp), %rdi
	movq	%r15, %rsi
	movl	$134744071, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -6400(%rbp)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L238
	call	_ZdlPv@PLT
.L238:
	movl	$37, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-6736(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r15, %rsi
	movq	$0, -6384(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L239
	call	_ZdlPv@PLT
.L239:
	leaq	-456(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -832(%rbp)
	je	.L240
.L434:
	leaq	-840(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -6384(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	-6728(%rbp), %rdi
	movq	%r15, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -6400(%rbp)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6400(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L241
	call	_ZdlPv@PLT
.L241:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movl	$39, %edx
	leaq	.LC2(%rip), %rsi
	movq	(%rax), %r13
	movq	16(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$40, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
	movl	$70, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L407:
	leaq	-6024(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -6456(%rbp)
	movq	$0, -6448(%rbp)
	movq	$0, -6440(%rbp)
	movq	$0, -6432(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-6512(%rbp), %r8
	movq	-6528(%rbp), %rcx
	leaq	-6456(%rbp), %rsi
	movq	-6520(%rbp), %rdx
	movq	-6744(%rbp), %rdi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_
	movq	-6456(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movaps	%xmm0, -6400(%rbp)
	movq	%rax, -128(%rbp)
	movq	-6448(%rbp), %rax
	movq	$0, -6384(%rbp)
	movq	%rax, -120(%rbp)
	movq	-6440(%rbp), %rax
	movq	%rax, -112(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	movq	%r15, %rsi
	movq	-6648(%rbp), %rdi
	leaq	24(%rax), %rdx
	movq	%rax, -6400(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -6384(%rbp)
	movq	%rdx, -6392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L164
	call	_ZdlPv@PLT
.L164:
	leaq	-1416(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L436:
	leaq	-456(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-6736(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r15, %rsi
	movq	$0, -6384(%rbp)
	movaps	%xmm0, -6400(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L244
	call	_ZdlPv@PLT
.L244:
	movq	-6792(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L435:
	leaq	-648(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -6448(%rbp)
	movq	$0, -6440(%rbp)
	movq	$0, -6432(%rbp)
	movq	$0, -6400(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-6512(%rbp), %rcx
	movq	%r15, %r8
	movq	-6528(%rbp), %rdx
	movq	-6520(%rbp), %rsi
	movq	-6560(%rbp), %rdi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_
	movq	%r12, %rdi
	movl	$8, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-6448(%rbp), %rsi
	movq	-6784(%rbp), %rdi
	movq	-6400(%rbp), %r8
	movq	-6432(%rbp), %rcx
	movq	-6440(%rbp), %rdx
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_EE9AddInputsENS1_5TNodeIS3_EENS6_IS4_EES8_S8_
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L438:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r13, %r8
	movq	%r13, %rcx
	movq	-6744(%rbp), %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_EE9AddInputsENS1_5TNodeIS3_EENS6_IS4_EES8_S8_
	leaq	-6024(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L439:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, -80(%rbp)
	movq	-6512(%rbp), %rbx
	pxor	%xmm0, %xmm0
	movdqa	-6816(%rbp), %xmm6
	movdqa	-6848(%rbp), %xmm1
	leaq	-128(%rbp), %rsi
	leaq	-72(%rbp), %rdx
	movdqa	-6864(%rbp), %xmm7
	movq	%rbx, %rdi
	movaps	%xmm0, -6432(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	$0, -6416(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6752(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L169
	call	_ZdlPv@PLT
.L169:
	leaq	-5640(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L440:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, -80(%rbp)
	movq	-6512(%rbp), %rbx
	pxor	%xmm0, %xmm0
	movdqa	-6816(%rbp), %xmm4
	movdqa	-6848(%rbp), %xmm6
	leaq	-128(%rbp), %rsi
	leaq	-72(%rbp), %rdx
	movdqa	-6864(%rbp), %xmm1
	movq	%rbx, %rdi
	movaps	%xmm0, -6432(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movq	$0, -6416(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6760(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L201
	call	_ZdlPv@PLT
.L201:
	leaq	-3528(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L441:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %rsi
	movq	%r13, %r8
	movq	%r13, %rcx
	movq	-6768(%rbp), %rdi
	movq	-6816(%rbp), %rdx
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_EE9AddInputsENS1_5TNodeIS3_EENS6_IS4_EES8_S8_
	leaq	-1224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L234
.L437:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22410:
	.size	_ZN2v88internal33LoadKeyValuePairNoSideEffects_267EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, .-_ZN2v88internal33LoadKeyValuePairNoSideEffects_267EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal20LoadKeyValuePair_268EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20LoadKeyValuePair_268EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal20LoadKeyValuePair_268EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal20LoadKeyValuePair_268EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE:
.LFB22462:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-1248(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1376(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1432(%rbp), %r12
	pushq	%rbx
	subq	$1496, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1432(%rbp)
	movq	%rdi, -1248(%rbp)
	movl	$48, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -1448(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1432(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1488(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1432(%rbp), %rax
	movl	$120, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1480(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1432(%rbp), %rax
	movl	$48, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1472(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1432(%rbp), %rax
	movl	$96, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -1464(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1432(%rbp), %rax
	movl	$96, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -280(%rbp)
	leaq	-232(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -1456(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1504(%rbp), %xmm1
	movaps	%xmm0, -1376(%rbp)
	movhps	-1512(%rbp), %xmm1
	movq	$0, -1360(%rbp)
	movaps	%xmm1, -1504(%rbp)
	call	_Znwm@PLT
	movdqa	-1504(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1376(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1360(%rbp)
	movq	%rdx, -1368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L443
	call	_ZdlPv@PLT
.L443:
	movq	-1448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1184(%rbp)
	jne	.L582
	cmpq	$0, -992(%rbp)
	jne	.L583
.L449:
	cmpq	$0, -800(%rbp)
	jne	.L584
.L452:
	cmpq	$0, -608(%rbp)
	jne	.L585
.L455:
	cmpq	$0, -416(%rbp)
	leaq	-288(%rbp), %r14
	jne	.L586
.L458:
	movq	-1456(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1360(%rbp)
	movaps	%xmm0, -1376(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1376(%rbp)
	movq	%rdx, -1360(%rbp)
	movq	%rdx, -1368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1376(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L461
	call	_ZdlPv@PLT
.L461:
	movq	(%rbx), %rax
	movq	-1456(%rbp), %rdi
	movq	24(%rax), %r13
	movq	16(%rax), %r14
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L462
	call	_ZdlPv@PLT
.L462:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L463
	.p2align 4,,10
	.p2align 3
.L467:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L464
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L467
.L465:
	movq	-280(%rbp), %r15
.L463:
	testq	%r15, %r15
	je	.L468
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L468:
	movq	-1464(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L469
	call	_ZdlPv@PLT
.L469:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L470
	.p2align 4,,10
	.p2align 3
.L474:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L471
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L474
.L472:
	movq	-472(%rbp), %r15
.L470:
	testq	%r15, %r15
	je	.L475
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L475:
	movq	-1472(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L476
	call	_ZdlPv@PLT
.L476:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L477
	.p2align 4,,10
	.p2align 3
.L481:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L478
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L481
.L479:
	movq	-664(%rbp), %r15
.L477:
	testq	%r15, %r15
	je	.L482
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L482:
	movq	-1480(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L483
	call	_ZdlPv@PLT
.L483:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L484
	.p2align 4,,10
	.p2align 3
.L488:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L485
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L488
.L486:
	movq	-856(%rbp), %r15
.L484:
	testq	%r15, %r15
	je	.L489
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L489:
	movq	-1488(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L490
	call	_ZdlPv@PLT
.L490:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L491
	.p2align 4,,10
	.p2align 3
.L495:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L492
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L495
.L493:
	movq	-1048(%rbp), %r15
.L491:
	testq	%r15, %r15
	je	.L496
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L496:
	movq	-1448(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L497
	call	_ZdlPv@PLT
.L497:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L498
	.p2align 4,,10
	.p2align 3
.L502:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L499
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L502
.L500:
	movq	-1240(%rbp), %r15
.L498:
	testq	%r15, %r15
	je	.L503
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L503:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L587
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	movq	%r13, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L499:
	.cfi_restore_state
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L502
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L492:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L495
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L485:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L488
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L478:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L481
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L464:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L467
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L471:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L474
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L582:
	movq	-1448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1360(%rbp)
	movaps	%xmm0, -1376(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	2(%rax), %rdx
	movq	%r15, %rdi
	movq	%rax, -1376(%rbp)
	movq	%rdx, -1360(%rbp)
	movq	%rdx, -1368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1376(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L445
	call	_ZdlPv@PLT
.L445:
	movq	(%rbx), %rax
	movl	$49, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	%r15, %rdx
	call	_ZN2v88internal33LoadKeyValuePairNoSideEffects_267EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	movq	%r15, %xmm3
	movq	%rbx, %xmm2
	pxor	%xmm0, %xmm0
	punpcklqdq	%xmm3, %xmm2
	movl	$40, %edi
	movq	%rdx, -64(%rbp)
	leaq	-1408(%rbp), %rbx
	movaps	%xmm2, -1504(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -1408(%rbp)
	movq	%r15, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -1392(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rsi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	leaq	40(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -1408(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1392(%rbp)
	movq	%rdx, -1400(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L446
	call	_ZdlPv@PLT
.L446:
	movq	-1480(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1368(%rbp)
	jne	.L588
.L447:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -992(%rbp)
	je	.L449
.L583:
	movq	-1488(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1360(%rbp)
	movaps	%xmm0, -1376(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%r15, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$8, 2(%rax)
	movq	%rax, -1376(%rbp)
	movq	%rdx, -1360(%rbp)
	movq	%rdx, -1368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1376(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L450
	call	_ZdlPv@PLT
.L450:
	movq	(%rbx), %rax
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movdqu	(%rax), %xmm0
	movaps	%xmm1, -1376(%rbp)
	movq	$0, -1360(%rbp)
	movaps	%xmm0, -1504(%rbp)
	call	_Znwm@PLT
	movdqa	-1504(%rbp), %xmm0
	leaq	-672(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1376(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -1360(%rbp)
	movq	%rdx, -1368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L451
	call	_ZdlPv@PLT
.L451:
	movq	-1472(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L452
.L584:
	movq	-1480(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1360(%rbp)
	movaps	%xmm0, -1376(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -1376(%rbp)
	movq	%rdx, -1360(%rbp)
	movq	%rdx, -1368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1376(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L453
	call	_ZdlPv@PLT
.L453:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	8(%rax), %rsi
	movq	24(%rax), %rcx
	movq	32(%rax), %rdx
	movq	(%rax), %rax
	movaps	%xmm0, -1376(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -1360(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -1376(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1360(%rbp)
	movq	%rdx, -1368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L454
	call	_ZdlPv@PLT
.L454:
	movq	-1464(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -608(%rbp)
	je	.L455
.L585:
	movq	-1472(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1360(%rbp)
	movaps	%xmm0, -1376(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1376(%rbp)
	movq	%rdx, -1360(%rbp)
	movq	%rdx, -1368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1376(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L456
	call	_ZdlPv@PLT
.L456:
	movq	(%rbx), %rax
	movl	$53, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	leaq	-1424(%rbp), %r15
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -1504(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal37Convert5ATSmi17ATconstexpr_int31_1406EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, -1512(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-1376(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	movq	%rbx, %r9
	xorl	%esi, %esi
	movq	%rax, %r8
	movq	-1360(%rbp), %rax
	pushq	%rdi
	movl	$1, %ecx
	movq	-1504(%rbp), %xmm0
	leaq	-1408(%rbp), %r10
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r11
	movq	%r15, %rdi
	movq	%rax, -1400(%rbp)
	leaq	-96(%rbp), %rax
	movq	%r10, %rdx
	pushq	%rax
	movhps	-1512(%rbp), %xmm0
	movq	%r11, -1408(%rbp)
	movq	%r10, -1536(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -1528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r15, %rdi
	movq	%rax, -1512(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$54, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal37Convert5ATSmi17ATconstexpr_int31_1406EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, -1520(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-1376(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-1528(%rbp), %rsi
	movq	%rbx, %r9
	movl	$2, %edi
	movq	-1536(%rbp), %r10
	pushq	%rdi
	movq	%rax, %r8
	movl	$1, %ecx
	pushq	%rsi
	movq	-1360(%rbp), %rax
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	-1504(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r11
	movq	%r10, %rdx
	movq	%r11, -1408(%rbp)
	movhps	-1520(%rbp), %xmm0
	movq	%rax, -1400(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r15, %rdi
	movq	%rax, -1520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$52, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$32, %edi
	movq	$0, -1360(%rbp)
	movhps	-1504(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-1512(%rbp), %xmm0
	movhps	-1520(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1376(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -1376(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -1360(%rbp)
	movq	%rdx, -1368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L457
	call	_ZdlPv@PLT
.L457:
	movq	-1464(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L586:
	movq	-1464(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1360(%rbp)
	movaps	%xmm0, -1376(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1376(%rbp)
	movq	%rdx, -1360(%rbp)
	movq	%rdx, -1368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1376(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L459
	call	_ZdlPv@PLT
.L459:
	movq	(%rbx), %rax
	movl	$45, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r14
	movq	8(%rax), %r15
	movq	16(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rax, -1504(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %xmm5
	movq	%r14, %xmm0
	movl	$32, %edi
	punpcklqdq	%xmm5, %xmm0
	leaq	-288(%rbp), %r14
	movq	$0, -1360(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	movhps	-1504(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1376(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-80(%rbp), %xmm7
	leaq	32(%rax), %rdx
	movq	%rax, -1376(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1360(%rbp)
	movq	%rdx, -1368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L460
	call	_ZdlPv@PLT
.L460:
	movq	-1456(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L588:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movdqa	-1504(%rbp), %xmm5
	movaps	%xmm0, -1408(%rbp)
	movaps	%xmm5, -96(%rbp)
	movq	%r15, -80(%rbp)
	movq	$0, -1392(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%rbx, %rsi
	leaq	24(%rax), %rdx
	leaq	-1056(%rbp), %rdi
	movq	%rax, -1408(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1392(%rbp)
	movq	%rdx, -1400(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L448
	call	_ZdlPv@PLT
.L448:
	movq	-1488(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L447
.L587:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22462:
	.size	_ZN2v88internal20LoadKeyValuePair_268EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal20LoadKeyValuePair_268EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal33LoadKeyValuePairNoSideEffects_267EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal33LoadKeyValuePairNoSideEffects_267EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, @function
_GLOBAL__sub_I__ZN2v88internal33LoadKeyValuePairNoSideEffects_267EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE:
.LFB29189:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29189:
	.size	_GLOBAL__sub_I__ZN2v88internal33LoadKeyValuePairNoSideEffects_267EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, .-_GLOBAL__sub_I__ZN2v88internal33LoadKeyValuePairNoSideEffects_267EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal33LoadKeyValuePairNoSideEffects_267EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.weak	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
