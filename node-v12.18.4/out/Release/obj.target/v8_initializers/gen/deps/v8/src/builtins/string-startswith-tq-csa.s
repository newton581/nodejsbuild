	.file	"string-startswith-tq-csa.cc"
	.text
	.section	.rodata._ZN2v88internal34StringPrototypeStartsWithAssembler37GenerateStringPrototypeStartsWithImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../../deps/v8/../../deps/v8/src/builtins/string-startswith.tq"
	.section	.rodata._ZN2v88internal34StringPrototypeStartsWithAssembler37GenerateStringPrototypeStartsWithImplEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"String.prototype.startsWith"
	.section	.text._ZN2v88internal34StringPrototypeStartsWithAssembler37GenerateStringPrototypeStartsWithImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal34StringPrototypeStartsWithAssembler37GenerateStringPrototypeStartsWithImplEv
	.type	_ZN2v88internal34StringPrototypeStartsWithAssembler37GenerateStringPrototypeStartsWithImplEv, @function
_ZN2v88internal34StringPrototypeStartsWithAssembler37GenerateStringPrototypeStartsWithImplEv:
.LFB22418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$2280, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -2056(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r13, %rcx
	movq	%r12, %rsi
	leaq	-2048(%rbp), %rdi
	movq	%rax, %rdx
	leaq	-2056(%rbp), %r13
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-2040(%rbp), %rcx
	movl	$2, %esi
	movq	%r12, %rdi
	movq	-2048(%rbp), %rax
	movq	-2032(%rbp), %rbx
	movq	%r12, -1920(%rbp)
	leaq	-1744(%rbp), %r12
	movq	%rcx, -2160(%rbp)
	movq	%rcx, -1896(%rbp)
	movq	$1, -1912(%rbp)
	movq	%rbx, -1904(%rbp)
	movq	%rax, -2176(%rbp)
	movq	%rax, -1888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-1920(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -2184(%rbp)
	movq	%rax, -2144(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -1736(%rbp)
	movq	$0, -1728(%rbp)
	movq	%rax, %r14
	movq	-2056(%rbp), %rax
	movq	$0, -1720(%rbp)
	movq	%rax, -1744(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1688(%rbp), %rcx
	movq	%r13, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -1720(%rbp)
	movq	%rdx, -1728(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2072(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1704(%rbp)
	movq	%rax, -1736(%rbp)
	movq	$0, -1712(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2056(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1544(%rbp)
	movq	$0, -1536(%rbp)
	movq	%rax, -1552(%rbp)
	movq	$0, -1528(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1496(%rbp), %rcx
	movq	%r13, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -1528(%rbp)
	movq	%rdx, -1536(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2080(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1512(%rbp)
	movq	%rax, -1544(%rbp)
	movq	$0, -1520(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2056(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1352(%rbp)
	movq	$0, -1344(%rbp)
	movq	%rax, -1360(%rbp)
	movq	$0, -1336(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1304(%rbp), %rcx
	movq	%r13, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -1336(%rbp)
	movq	%rdx, -1344(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2088(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1320(%rbp)
	movq	%rax, -1352(%rbp)
	movq	$0, -1328(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2056(%rbp), %rax
	movl	$336, %edi
	movq	$0, -1160(%rbp)
	movq	$0, -1152(%rbp)
	movq	%rax, -1168(%rbp)
	movq	$0, -1144(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1112(%rbp), %rcx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rdx, -1144(%rbp)
	movq	%rdx, -1152(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2104(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1128(%rbp)
	movq	%rax, -1160(%rbp)
	movq	$0, -1136(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2056(%rbp), %rax
	movl	$336, %edi
	movq	$0, -968(%rbp)
	movq	$0, -960(%rbp)
	movq	%rax, -976(%rbp)
	movq	$0, -952(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-920(%rbp), %rcx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rdx, -952(%rbp)
	movq	%rdx, -960(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2096(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -936(%rbp)
	movq	%rax, -968(%rbp)
	movq	$0, -944(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2056(%rbp), %rax
	movl	$432, %edi
	movq	$0, -776(%rbp)
	movq	$0, -768(%rbp)
	movq	%rax, -784(%rbp)
	movq	$0, -760(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-728(%rbp), %rcx
	movq	%r13, %rsi
	leaq	432(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, -744(%rbp)
	movq	%rax, -776(%rbp)
	movq	%rdx, -760(%rbp)
	movq	%rdx, -768(%rbp)
	xorl	%edx, %edx
	movq	$0, -752(%rbp)
	movq	%rcx, -2128(%rbp)
	xorl	%ecx, %ecx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2056(%rbp), %rax
	movl	$456, %edi
	movq	$0, -584(%rbp)
	movq	$0, -576(%rbp)
	movq	%rax, -592(%rbp)
	movq	$0, -568(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-536(%rbp), %rcx
	movq	%r13, %rsi
	leaq	456(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movq	%rax, -584(%rbp)
	movq	$0, 448(%rax)
	movq	%rdx, -568(%rbp)
	movq	%rdx, -576(%rbp)
	xorl	%edx, %edx
	movq	$0, -560(%rbp)
	movq	%rcx, -2120(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -552(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2056(%rbp), %rax
	movl	$336, %edi
	movq	$0, -392(%rbp)
	movq	$0, -384(%rbp)
	movq	%rax, -400(%rbp)
	movq	$0, -376(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-344(%rbp), %rcx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rdx, -376(%rbp)
	movq	%rdx, -384(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2112(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -392(%rbp)
	movups	%xmm0, -360(%rbp)
	movq	$0, -368(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movq	-2176(%rbp), %xmm1
	movl	$40, %edi
	movq	%r14, -176(%rbp)
	leaq	-1872(%rbp), %r14
	movhps	-2160(%rbp), %xmm1
	movaps	%xmm0, -1872(%rbp)
	movaps	%xmm1, -208(%rbp)
	movq	%rbx, %xmm1
	movhps	-2144(%rbp), %xmm1
	movq	$0, -1856(%rbp)
	movaps	%xmm1, -192(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movdqa	-208(%rbp), %xmm3
	movdqa	-192(%rbp), %xmm4
	leaq	40(%rax), %rdx
	movq	%rax, -1872(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -1856(%rbp)
	movq	%rdx, -1864(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1872(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2
	call	_ZdlPv@PLT
.L2:
	movq	-2072(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1680(%rbp)
	jne	.L182
	cmpq	$0, -1488(%rbp)
	jne	.L183
.L7:
	cmpq	$0, -1296(%rbp)
	jne	.L184
.L9:
	cmpq	$0, -1104(%rbp)
	jne	.L185
.L13:
	cmpq	$0, -912(%rbp)
	jne	.L186
.L15:
	cmpq	$0, -720(%rbp)
	jne	.L187
.L20:
	cmpq	$0, -528(%rbp)
	jne	.L188
.L23:
	cmpq	$0, -336(%rbp)
	jne	.L189
.L25:
	movq	-2112(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L27
	call	_ZdlPv@PLT
.L27:
	movq	-384(%rbp), %rbx
	movq	-392(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L28
	.p2align 4,,10
	.p2align 3
.L32:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L29
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L32
.L30:
	movq	-392(%rbp), %r12
.L28:
	testq	%r12, %r12
	je	.L33
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L33:
	movq	-2120(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-560(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L34
	call	_ZdlPv@PLT
.L34:
	movq	-576(%rbp), %rbx
	movq	-584(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L35
	.p2align 4,,10
	.p2align 3
.L39:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L36
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L39
.L37:
	movq	-584(%rbp), %r12
.L35:
	testq	%r12, %r12
	je	.L40
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L40:
	movq	-2128(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L41
	call	_ZdlPv@PLT
.L41:
	movq	-768(%rbp), %rbx
	movq	-776(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L42
	.p2align 4,,10
	.p2align 3
.L46:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L43
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L46
.L44:
	movq	-776(%rbp), %r12
.L42:
	testq	%r12, %r12
	je	.L47
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L47:
	movq	-2096(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L48
	call	_ZdlPv@PLT
.L48:
	movq	-960(%rbp), %rbx
	movq	-968(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L49
	.p2align 4,,10
	.p2align 3
.L53:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L50
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L53
.L51:
	movq	-968(%rbp), %r12
.L49:
	testq	%r12, %r12
	je	.L54
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L54:
	movq	-2104(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L55
	call	_ZdlPv@PLT
.L55:
	movq	-1152(%rbp), %rbx
	movq	-1160(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L56
	.p2align 4,,10
	.p2align 3
.L60:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L57
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L60
.L58:
	movq	-1160(%rbp), %r12
.L56:
	testq	%r12, %r12
	je	.L61
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L61:
	movq	-2088(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L62
	call	_ZdlPv@PLT
.L62:
	movq	-1344(%rbp), %rbx
	movq	-1352(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L63
	.p2align 4,,10
	.p2align 3
.L67:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L64
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L67
.L65:
	movq	-1352(%rbp), %r12
.L63:
	testq	%r12, %r12
	je	.L68
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L68:
	movq	-2080(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L69
	call	_ZdlPv@PLT
.L69:
	movq	-1536(%rbp), %rbx
	movq	-1544(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L70
	.p2align 4,,10
	.p2align 3
.L74:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L71
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L74
.L72:
	movq	-1544(%rbp), %r12
.L70:
	testq	%r12, %r12
	je	.L75
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L75:
	movq	-2072(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L76
	call	_ZdlPv@PLT
.L76:
	movq	-1728(%rbp), %rbx
	movq	-1736(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L77
	.p2align 4,,10
	.p2align 3
.L81:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L78
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L81
.L79:
	movq	-1736(%rbp), %r12
.L77:
	testq	%r12, %r12
	je	.L82
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L82:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L190
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L81
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L71:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L74
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L64:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L67
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L57:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L60
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L50:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L53
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L43:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L46
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L29:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L32
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L36:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L39
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L182:
	movq	-2072(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1856(%rbp)
	movaps	%xmm0, -1872(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -1872(%rbp)
	movq	%rdx, -1856(%rbp)
	movq	%rdx, -1864(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1872(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L4
	call	_ZdlPv@PLT
.L4:
	movq	(%rbx), %rax
	movl	$14, %edx
	movq	%r13, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rsi
	movq	24(%rax), %rbx
	movq	%rcx, -2208(%rbp)
	movq	16(%rax), %rcx
	movq	32(%rax), %rax
	movq	%rsi, -2144(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -2160(%rbp)
	movq	%rax, -2176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-2144(%rbp), %xmm6
	movq	-2160(%rbp), %rcx
	movhps	-2208(%rbp), %xmm6
	movq	%rcx, -2000(%rbp)
	movaps	%xmm6, -2016(%rbp)
	pushq	-2000(%rbp)
	pushq	-2008(%rbp)
	pushq	-2016(%rbp)
	movaps	%xmm6, -2144(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r14, %rdi
	movq	%rax, -2208(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$15, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqa	-2144(%rbp), %xmm6
	movq	-2160(%rbp), %rcx
	movaps	%xmm6, -1984(%rbp)
	movq	%rcx, -1968(%rbp)
	pushq	-1968(%rbp)
	pushq	-1976(%rbp)
	pushq	-1984(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r14, %rdi
	movq	%rax, -2256(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$19, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2176(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rcx
	call	_ZN2v88internal26RequireObjectCoercible_241EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPKc@PLT
	movl	$22, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	movq	%rax, -2240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ToString_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$26, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2208(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23RegExpBuiltinsAssembler8IsRegExpENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -2224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm7
	pxor	%xmm0, %xmm0
	movq	-2176(%rbp), %xmm3
	movq	-2160(%rbp), %xmm4
	movdqa	-2144(%rbp), %xmm6
	movl	$72, %edi
	movaps	%xmm0, -1872(%rbp)
	movq	-2256(%rbp), %xmm5
	movhps	-2208(%rbp), %xmm3
	movq	%r12, -144(%rbp)
	punpcklqdq	%xmm7, %xmm4
	movaps	%xmm3, -2176(%rbp)
	movhps	-2240(%rbp), %xmm5
	movaps	%xmm4, -2160(%rbp)
	movaps	%xmm5, -2240(%rbp)
	movaps	%xmm6, -208(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movq	$0, -1856(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm2
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm7
	movq	-144(%rbp), %rcx
	leaq	72(%rax), %rdx
	leaq	-1552(%rbp), %rdi
	movq	%rax, -1872(%rbp)
	movups	%xmm2, (%rax)
	movdqa	-176(%rbp), %xmm2
	movups	%xmm7, 16(%rax)
	movdqa	-160(%rbp), %xmm7
	movq	%rcx, 64(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rdx, -1856(%rbp)
	movq	%rdx, -1864(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1872(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5
	call	_ZdlPv@PLT
.L5:
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movq	%r12, -144(%rbp)
	movdqa	-2144(%rbp), %xmm2
	movdqa	-2160(%rbp), %xmm7
	movdqa	-2176(%rbp), %xmm5
	movaps	%xmm0, -1872(%rbp)
	movaps	%xmm2, -208(%rbp)
	movdqa	-2240(%rbp), %xmm2
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movq	$0, -1856(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-208(%rbp), %xmm7
	movdqa	-192(%rbp), %xmm5
	movdqa	-176(%rbp), %xmm6
	leaq	72(%rax), %rdx
	leaq	-1360(%rbp), %rdi
	movdqa	-160(%rbp), %xmm2
	movq	%rcx, 64(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm2, 48(%rax)
	movq	%rax, -1872(%rbp)
	movq	%rdx, -1856(%rbp)
	movq	%rdx, -1864(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1872(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6
	call	_ZdlPv@PLT
.L6:
	movq	-2088(%rbp), %rcx
	movq	-2080(%rbp), %rdx
	movq	%r13, %rdi
	movq	-2224(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1488(%rbp)
	je	.L7
.L183:
	movq	-2080(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-1552(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -1856(%rbp)
	movaps	%xmm0, -1872(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movabsq	$578721382687638789, %rsi
	movq	%rsi, (%rax)
	leaq	9(%rax), %rdx
	movq	%r14, %rsi
	movb	$7, 8(%rax)
	movq	%rax, -1872(%rbp)
	movq	%rdx, -1856(%rbp)
	movq	%rdx, -1864(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1872(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L8
	call	_ZdlPv@PLT
.L8:
	movq	(%rbx), %rax
	movl	$27, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	movl	$53, %edx
	movq	%r12, %rsi
	leaq	.LC1(%rip), %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -1296(%rbp)
	je	.L9
.L184:
	movq	-2088(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-1360(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -1856(%rbp)
	movaps	%xmm0, -1872(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movabsq	$578721382687638789, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%rax, -1872(%rbp)
	movq	%rdx, -1856(%rbp)
	movq	%rdx, -1864(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1872(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	movq	(%rbx), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rcx
	movq	32(%rax), %rdx
	movq	56(%rax), %rdi
	movq	24(%rax), %rbx
	movq	%rsi, -2256(%rbp)
	movq	48(%rax), %rsi
	movq	40(%rax), %r12
	movq	%rcx, -2192(%rbp)
	movq	16(%rax), %rcx
	movq	64(%rax), %rax
	movq	%rdx, -2280(%rbp)
	movl	$31, %edx
	movq	%rsi, -2144(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rdi, -2288(%rbp)
	movq	%r13, %rdi
	movq	%rcx, -2272(%rbp)
	movq	%rax, -2160(%rbp)
	movq	%r12, -2304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ToString_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -2240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$34, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-2144(%rbp), %rdx
	call	_ZN2v88internal17CodeStubAssembler16ToInteger_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_23ToIntegerTruncationModeE@PLT
	movq	%r14, %rdi
	movq	%rax, -2224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$38, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2160(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler21LoadStringLengthAsSmiENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$41, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, -2176(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2176(%rbp), %rdx
	movq	-2224(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberMaxENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r14, %rdi
	movq	%rax, -2176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2176(%rbp), %r8
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler9NumberMinENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r14, %rdi
	movq	%rax, -2208(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$44, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2240(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler21LoadStringLengthAsSmiENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -2176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$47, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2208(%rbp), %rdx
	movq	-2176(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberAddENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r14, %rdi
	movq	%rax, -2312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-2312(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal22NumberIsGreaterThan_77EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	movq	%r12, %xmm7
	movq	%rbx, %xmm1
	movq	-2224(%rbp), %xmm6
	movq	-2272(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movq	-2208(%rbp), %xmm5
	movl	$112, %edi
	punpcklqdq	%xmm7, %xmm6
	movaps	%xmm0, -1872(%rbp)
	movq	-2160(%rbp), %xmm7
	movq	-2144(%rbp), %xmm2
	punpcklqdq	%xmm1, %xmm4
	movhps	-2176(%rbp), %xmm5
	movaps	%xmm6, -128(%rbp)
	movq	-2280(%rbp), %xmm3
	movq	-2256(%rbp), %xmm1
	movhps	-2240(%rbp), %xmm7
	movhps	-2288(%rbp), %xmm2
	movaps	%xmm5, -2208(%rbp)
	movhps	-2304(%rbp), %xmm3
	movaps	%xmm6, -2176(%rbp)
	movhps	-2192(%rbp), %xmm1
	movaps	%xmm7, -2240(%rbp)
	movaps	%xmm2, -2224(%rbp)
	movaps	%xmm3, -2160(%rbp)
	movaps	%xmm4, -2272(%rbp)
	movaps	%xmm1, -2144(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm5, -112(%rbp)
	movq	%rax, -2312(%rbp)
	movq	$0, -1856(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm3
	movdqa	-176(%rbp), %xmm2
	leaq	112(%rax), %rdx
	leaq	-1168(%rbp), %rdi
	movdqa	-160(%rbp), %xmm7
	movups	%xmm5, (%rax)
	movdqa	-144(%rbp), %xmm5
	movups	%xmm6, 16(%rax)
	movdqa	-128(%rbp), %xmm6
	movups	%xmm2, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm5, 64(%rax)
	movups	%xmm6, 80(%rax)
	movups	%xmm3, 96(%rax)
	movq	%rax, -1872(%rbp)
	movq	%rdx, -1856(%rbp)
	movq	%rdx, -1864(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1872(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L11
	call	_ZdlPv@PLT
.L11:
	movdqa	-2144(%rbp), %xmm4
	movdqa	-2272(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movl	$112, %edi
	movdqa	-2160(%rbp), %xmm7
	movdqa	-2224(%rbp), %xmm5
	movaps	%xmm0, -1872(%rbp)
	movdqa	-2240(%rbp), %xmm6
	movdqa	-2176(%rbp), %xmm3
	movaps	%xmm4, -208(%rbp)
	movdqa	-2208(%rbp), %xmm4
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movq	$0, -1856(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm2
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm4
	leaq	112(%rax), %rdx
	leaq	-976(%rbp), %rdi
	movdqa	-176(%rbp), %xmm5
	movdqa	-160(%rbp), %xmm6
	movdqa	-144(%rbp), %xmm3
	movups	%xmm2, (%rax)
	movdqa	-112(%rbp), %xmm2
	movups	%xmm7, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm3, 64(%rax)
	movups	%xmm4, 80(%rax)
	movups	%xmm2, 96(%rax)
	movq	%rax, -1872(%rbp)
	movq	%rdx, -1856(%rbp)
	movq	%rdx, -1864(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1872(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L12
	call	_ZdlPv@PLT
.L12:
	movq	-2096(%rbp), %rcx
	movq	-2104(%rbp), %rdx
	movq	%r13, %rdi
	movq	-2312(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1104(%rbp)
	je	.L13
.L185:
	movq	-2104(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-1168(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$14, %edi
	movq	$0, -1856(%rbp)
	movaps	%xmm0, -1872(%rbp)
	call	_Znwm@PLT
	movl	$1544, %r9d
	movq	%r12, %rdi
	movabsq	$578721382687638789, %rsi
	movq	%rsi, (%rax)
	leaq	14(%rax), %rdx
	movq	%r14, %rsi
	movl	$134743815, 8(%rax)
	movw	%r9w, 12(%rax)
	movq	%rax, -1872(%rbp)
	movq	%rdx, -1856(%rbp)
	movq	%rdx, -1864(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1872(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L14
	call	_ZdlPv@PLT
.L14:
	movq	%r15, %rdi
	call	_ZN2v88internal8False_66EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2184(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	cmpq	$0, -912(%rbp)
	je	.L15
.L186:
	movq	-2096(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-976(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$14, %edi
	movq	$0, -1856(%rbp)
	movaps	%xmm0, -1872(%rbp)
	call	_Znwm@PLT
	movl	$1544, %r8d
	movq	%r12, %rdi
	movabsq	$578721382687638789, %rsi
	movq	%rsi, (%rax)
	leaq	14(%rax), %rdx
	movq	%r14, %rsi
	movl	$134743815, 8(%rax)
	movw	%r8w, 12(%rax)
	movq	%rax, -1872(%rbp)
	movq	%rdx, -1856(%rbp)
	movq	%rdx, -1864(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1872(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L16
	call	_ZdlPv@PLT
.L16:
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	movq	24(%rax), %rbx
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	88(%rax), %r10
	movq	(%rax), %rcx
	movq	%rsi, -2208(%rbp)
	movq	16(%rax), %rsi
	movq	%rbx, -2240(%rbp)
	movq	32(%rax), %rbx
	movq	64(%rax), %r12
	movq	%rdx, -2192(%rbp)
	movq	%rsi, -2224(%rbp)
	movq	48(%rax), %rdx
	movq	72(%rax), %rsi
	movq	%rdi, -2280(%rbp)
	movq	80(%rax), %rdi
	movq	%rbx, -2256(%rbp)
	movq	96(%rax), %rbx
	movq	104(%rax), %rax
	movq	%rdx, -2272(%rbp)
	movl	$56, %edx
	movq	%rsi, -2144(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rdi, -2304(%rbp)
	movq	%r13, %rdi
	movq	%r10, -2288(%rbp)
	movq	%rax, -2160(%rbp)
	movq	%rcx, -2176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rsi
	movq	%r14, %r9
	movq	%rbx, %rcx
	movq	-2160(%rbp), %r8
	movq	-2144(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal32TryFastStringCompareSequence_332EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6StringEEES6_NS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEENS4_IS8_EEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %xmm2
	movq	%rbx, %xmm7
	movq	-2304(%rbp), %xmm5
	movq	-2272(%rbp), %xmm6
	movhps	-2160(%rbp), %xmm7
	movq	-2256(%rbp), %xmm3
	movhps	-2144(%rbp), %xmm2
	movhps	-2288(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movl	$152, %edi
	movq	-2224(%rbp), %xmm4
	movq	-2176(%rbp), %xmm1
	movhps	-2280(%rbp), %xmm6
	movhps	-2192(%rbp), %xmm3
	movaps	%xmm7, -2160(%rbp)
	movhps	-2240(%rbp), %xmm4
	movaps	%xmm2, -2144(%rbp)
	leaq	-1952(%rbp), %r12
	movhps	-2208(%rbp), %xmm1
	movaps	%xmm5, -2304(%rbp)
	movaps	%xmm6, -2272(%rbp)
	movaps	%xmm3, -2256(%rbp)
	movaps	%xmm4, -2224(%rbp)
	movaps	%xmm1, -2176(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	movaps	%xmm0, -1952(%rbp)
	movq	%rax, -64(%rbp)
	movq	$0, -1936(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm7
	movq	-64(%rbp), %rcx
	movq	%r12, %rsi
	movdqa	-192(%rbp), %xmm5
	movdqa	-176(%rbp), %xmm6
	leaq	152(%rax), %rdx
	leaq	-592(%rbp), %rdi
	movdqa	-160(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm4
	movups	%xmm7, (%rax)
	movdqa	-128(%rbp), %xmm2
	movdqa	-112(%rbp), %xmm7
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm6
	movq	%rcx, 144(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm2, 80(%rax)
	movups	%xmm7, 96(%rax)
	movups	%xmm5, 112(%rax)
	movups	%xmm6, 128(%rax)
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L17
	call	_ZdlPv@PLT
.L17:
	movq	-2120(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1864(%rbp)
	jne	.L191
.L18:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -720(%rbp)
	je	.L20
.L187:
	movq	-2128(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-784(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$18, %edi
	movq	$0, -1856(%rbp)
	movaps	%xmm0, -1872(%rbp)
	call	_Znwm@PLT
	movl	$1544, %edi
	movdqa	.LC2(%rip), %xmm0
	movq	%r14, %rsi
	movw	%di, 16(%rax)
	leaq	18(%rax), %rdx
	movq	%r12, %rdi
	movups	%xmm0, (%rax)
	movq	%rax, -1872(%rbp)
	movq	%rdx, -1856(%rbp)
	movq	%rdx, -1864(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1872(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L21
	call	_ZdlPv@PLT
.L21:
	movq	(%rbx), %rax
	movl	$112, %edi
	movdqu	96(%rax), %xmm0
	movdqu	16(%rax), %xmm5
	movdqu	32(%rax), %xmm4
	movdqu	48(%rax), %xmm3
	movdqu	64(%rax), %xmm2
	movdqu	80(%rax), %xmm1
	movdqu	(%rax), %xmm7
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -192(%rbp)
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -1872(%rbp)
	movq	$0, -1856(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm3
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm4
	movdqa	-176(%rbp), %xmm2
	movdqa	-160(%rbp), %xmm5
	leaq	112(%rax), %rdx
	leaq	-400(%rbp), %rdi
	movdqa	-144(%rbp), %xmm6
	movups	%xmm3, (%rax)
	movdqa	-128(%rbp), %xmm3
	movups	%xmm4, 16(%rax)
	movdqa	-112(%rbp), %xmm4
	movups	%xmm2, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	movups	%xmm3, 80(%rax)
	movups	%xmm4, 96(%rax)
	movq	%rax, -1872(%rbp)
	movq	%rdx, -1856(%rbp)
	movq	%rdx, -1864(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1872(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L22
	call	_ZdlPv@PLT
.L22:
	movq	-2112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -528(%rbp)
	je	.L23
.L188:
	movq	-2120(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-592(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$19, %edi
	movq	$0, -1856(%rbp)
	movaps	%xmm0, -1872(%rbp)
	call	_Znwm@PLT
	movl	$1544, %esi
	movdqa	.LC2(%rip), %xmm0
	movq	%r12, %rdi
	movw	%si, 16(%rax)
	leaq	19(%rax), %rdx
	movq	%r14, %rsi
	movb	$7, 18(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, -1872(%rbp)
	movq	%rdx, -1856(%rbp)
	movq	%rdx, -1864(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1872(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L24
	call	_ZdlPv@PLT
.L24:
	movq	(%rbx), %rax
	movq	-2184(%rbp), %rdi
	movq	144(%rax), %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	cmpq	$0, -336(%rbp)
	je	.L25
.L189:
	movq	-2112(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-400(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$14, %edi
	movq	$0, -1856(%rbp)
	movaps	%xmm0, -1872(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movabsq	$578721382687638789, %rcx
	movq	%rcx, (%rax)
	movl	$1544, %ecx
	leaq	14(%rax), %rdx
	movl	$134743815, 8(%rax)
	movw	%cx, 12(%rax)
	movq	%rax, -1872(%rbp)
	movq	%rdx, -1856(%rbp)
	movq	%rdx, -1864(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1872(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L26
	call	_ZdlPv@PLT
.L26:
	movq	(%rbx), %rax
	movl	$60, %edx
	movq	%r13, %rdi
	movq	72(%rax), %rsi
	movq	64(%rax), %rcx
	movq	24(%rax), %r12
	movq	96(%rax), %rbx
	movq	%rsi, -2160(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -2144(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdx
	movl	$340, %esi
	movq	%r14, %rdi
	movq	-2144(%rbp), %xmm0
	leaq	-208(%rbp), %rcx
	movl	$3, %r8d
	movq	%rbx, -192(%rbp)
	movhps	-2160(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-2184(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L191:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-2176(%rbp), %xmm5
	movdqa	-2144(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movdqa	-2160(%rbp), %xmm7
	movdqa	-2224(%rbp), %xmm6
	movl	$144, %edi
	movaps	%xmm0, -1952(%rbp)
	movdqa	-2256(%rbp), %xmm3
	movdqa	-2272(%rbp), %xmm4
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm5, -208(%rbp)
	movdqa	-2304(%rbp), %xmm5
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm7, -80(%rbp)
	movq	$0, -1936(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm6
	movq	%r12, %rsi
	movdqa	-192(%rbp), %xmm3
	movdqa	-176(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm2
	leaq	144(%rax), %rdx
	leaq	-784(%rbp), %rdi
	movdqa	-144(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm5
	movups	%xmm6, (%rax)
	movups	%xmm3, 16(%rax)
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm3
	movups	%xmm4, 32(%rax)
	movdqa	-80(%rbp), %xmm4
	movups	%xmm2, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm5, 80(%rax)
	movups	%xmm6, 96(%rax)
	movups	%xmm3, 112(%rax)
	movups	%xmm4, 128(%rax)
	movq	%rax, -1952(%rbp)
	movq	%rdx, -1936(%rbp)
	movq	%rdx, -1944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L19
	call	_ZdlPv@PLT
.L19:
	movq	-2128(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L18
.L190:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22418:
	.size	_ZN2v88internal34StringPrototypeStartsWithAssembler37GenerateStringPrototypeStartsWithImplEv, .-_ZN2v88internal34StringPrototypeStartsWithAssembler37GenerateStringPrototypeStartsWithImplEv
	.section	.rodata._ZN2v88internal8Builtins34Generate_StringPrototypeStartsWithEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/string-startswith-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins34Generate_StringPrototypeStartsWithEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"StringPrototypeStartsWith"
	.section	.text._ZN2v88internal8Builtins34Generate_StringPrototypeStartsWithEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins34Generate_StringPrototypeStartsWithEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins34Generate_StringPrototypeStartsWithEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins34Generate_StringPrototypeStartsWithEPNS0_8compiler18CodeAssemblerStateE:
.LFB22414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$146, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$913, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L196
.L193:
	movq	%r13, %rdi
	call	_ZN2v88internal34StringPrototypeStartsWithAssembler37GenerateStringPrototypeStartsWithImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L197
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L193
.L197:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22414:
	.size	_ZN2v88internal8Builtins34Generate_StringPrototypeStartsWithEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins34Generate_StringPrototypeStartsWithEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins34Generate_StringPrototypeStartsWithEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins34Generate_StringPrototypeStartsWithEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins34Generate_StringPrototypeStartsWithEPNS0_8compiler18CodeAssemblerStateE:
.LFB28965:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28965:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins34Generate_StringPrototypeStartsWithEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins34Generate_StringPrototypeStartsWithEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins34Generate_StringPrototypeStartsWithEPNS0_8compiler18CodeAssemblerStateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.byte	5
	.byte	5
	.byte	5
	.byte	7
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	7
	.byte	7
	.byte	8
	.byte	8
	.byte	8
	.byte	6
	.byte	7
	.byte	7
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
