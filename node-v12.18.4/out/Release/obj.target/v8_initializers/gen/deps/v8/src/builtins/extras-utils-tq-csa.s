	.file	"extras-utils-tq-csa.cc"
	.text
	.section	.rodata._ZN2v88internal39ExtrasUtilsCreatePrivateSymbolAssembler42GenerateExtrasUtilsCreatePrivateSymbolImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../../deps/v8/../../deps/v8/src/builtins/extras-utils.tq"
	.section	.text._ZN2v88internal39ExtrasUtilsCreatePrivateSymbolAssembler42GenerateExtrasUtilsCreatePrivateSymbolImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal39ExtrasUtilsCreatePrivateSymbolAssembler42GenerateExtrasUtilsCreatePrivateSymbolImplEv
	.type	_ZN2v88internal39ExtrasUtilsCreatePrivateSymbolAssembler42GenerateExtrasUtilsCreatePrivateSymbolImplEv, @function
_ZN2v88internal39ExtrasUtilsCreatePrivateSymbolAssembler42GenerateExtrasUtilsCreatePrivateSymbolImplEv:
.LFB22418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$440, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -424(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r12, %rcx
	movq	%r13, %rsi
	leaq	-400(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-392(%rbp), %rcx
	movl	$2, %esi
	movq	%r13, %rdi
	movq	-384(%rbp), %rbx
	movq	-400(%rbp), %rax
	movq	%r13, -336(%rbp)
	leaq	-424(%rbp), %r13
	movq	%rcx, -456(%rbp)
	movq	%rcx, -312(%rbp)
	movq	%rbx, -464(%rbp)
	movq	%rbx, -320(%rbp)
	leaq	-288(%rbp), %rbx
	movq	$1, -328(%rbp)
	movq	%rax, -440(%rbp)
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-336(%rbp), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -472(%rbp)
	movq	%rax, -448(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, %r12
	movq	-424(%rbp), %rax
	movq	$0, -264(%rbp)
	movq	%rax, -288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	-440(%rbp), %xmm1
	movq	%r12, -64(%rbp)
	leaq	-368(%rbp), %r12
	movhps	-456(%rbp), %xmm1
	movaps	%xmm0, -368(%rbp)
	movaps	%xmm1, -96(%rbp)
	movq	-464(%rbp), %xmm1
	movq	$0, -352(%rbp)
	movhps	-448(%rbp), %xmm1
	movaps	%xmm1, -80(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	leaq	40(%rax), %rdx
	movq	%rax, -368(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2
	call	_ZdlPv@PLT
.L2:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -224(%rbp)
	jne	.L27
.L3:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5
	call	_ZdlPv@PLT
.L5:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L6
	.p2align 4,,10
	.p2align 3
.L10:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L7
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L10
.L8:
	movq	-280(%rbp), %r12
.L6:
	testq	%r12, %r12
	je	.L11
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L11:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L28
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L10
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -352(%rbp)
	movaps	%xmm0, -368(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -368(%rbp)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-368(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L4
	call	_ZdlPv@PLT
.L4:
	movq	(%rbx), %rax
	movl	$12, %edx
	movq	%r13, %rdi
	leaq	-416(%rbp), %r12
	movq	8(%rax), %rcx
	movq	(%rax), %rsi
	movq	24(%rax), %r9
	movq	%rcx, -448(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -440(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%r9, -456(%rbp)
	movq	%rcx, -464(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-440(%rbp), %xmm0
	movq	-464(%rbp), %rcx
	movhps	-448(%rbp), %xmm0
	movq	%rcx, -352(%rbp)
	movaps	%xmm0, -368(%rbp)
	pushq	-352(%rbp)
	pushq	-360(%rbp)
	pushq	-368(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	leaq	-96(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	movq	-456(%rbp), %r9
	movl	$1, %r8d
	movl	$345, %esi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-472(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	jmp	.L3
.L28:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22418:
	.size	_ZN2v88internal39ExtrasUtilsCreatePrivateSymbolAssembler42GenerateExtrasUtilsCreatePrivateSymbolImplEv, .-_ZN2v88internal39ExtrasUtilsCreatePrivateSymbolAssembler42GenerateExtrasUtilsCreatePrivateSymbolImplEv
	.section	.rodata._ZN2v88internal8Builtins39Generate_ExtrasUtilsCreatePrivateSymbolEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/extras-utils-tq-csa.cc"
	.align 8
.LC2:
	.string	"ExtrasUtilsCreatePrivateSymbol"
	.section	.text._ZN2v88internal8Builtins39Generate_ExtrasUtilsCreatePrivateSymbolEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins39Generate_ExtrasUtilsCreatePrivateSymbolEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins39Generate_ExtrasUtilsCreatePrivateSymbolEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins39Generate_ExtrasUtilsCreatePrivateSymbolEPNS0_8compiler18CodeAssemblerStateE:
.LFB22414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$146, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$815, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L33
.L30:
	movq	%r13, %rdi
	call	_ZN2v88internal39ExtrasUtilsCreatePrivateSymbolAssembler42GenerateExtrasUtilsCreatePrivateSymbolImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L34
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L30
.L34:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22414:
	.size	_ZN2v88internal8Builtins39Generate_ExtrasUtilsCreatePrivateSymbolEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins39Generate_ExtrasUtilsCreatePrivateSymbolEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal40ExtrasUtilsMarkPromiseAsHandledAssembler43GenerateExtrasUtilsMarkPromiseAsHandledImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal40ExtrasUtilsMarkPromiseAsHandledAssembler43GenerateExtrasUtilsMarkPromiseAsHandledImplEv
	.type	_ZN2v88internal40ExtrasUtilsMarkPromiseAsHandledAssembler43GenerateExtrasUtilsMarkPromiseAsHandledImplEv, @function
_ZN2v88internal40ExtrasUtilsMarkPromiseAsHandledAssembler43GenerateExtrasUtilsMarkPromiseAsHandledImplEv:
.LFB22433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$440, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -424(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r12, %rcx
	movq	%r13, %rsi
	leaq	-400(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-392(%rbp), %rcx
	movl	$2, %esi
	movq	%r13, %rdi
	movq	-384(%rbp), %rbx
	movq	-400(%rbp), %rax
	movq	%r13, -336(%rbp)
	leaq	-424(%rbp), %r13
	movq	%rcx, -456(%rbp)
	movq	%rcx, -312(%rbp)
	movq	%rbx, -464(%rbp)
	movq	%rbx, -320(%rbp)
	leaq	-288(%rbp), %rbx
	movq	$1, -328(%rbp)
	movq	%rax, -440(%rbp)
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-336(%rbp), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -472(%rbp)
	movq	%rax, -448(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, %r12
	movq	-424(%rbp), %rax
	movq	$0, -264(%rbp)
	movq	%rax, -288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	-440(%rbp), %xmm1
	movq	%r12, -64(%rbp)
	leaq	-368(%rbp), %r12
	movhps	-456(%rbp), %xmm1
	movaps	%xmm0, -368(%rbp)
	movaps	%xmm1, -96(%rbp)
	movq	-464(%rbp), %xmm1
	movq	$0, -352(%rbp)
	movhps	-448(%rbp), %xmm1
	movaps	%xmm1, -80(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	leaq	40(%rax), %rdx
	movq	%rax, -368(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L36
	call	_ZdlPv@PLT
.L36:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -224(%rbp)
	jne	.L60
.L37:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L39
	call	_ZdlPv@PLT
.L39:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L40
	.p2align 4,,10
	.p2align 3
.L44:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L41
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L44
.L42:
	movq	-280(%rbp), %r12
.L40:
	testq	%r12, %r12
	je	.L45
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L45:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L61
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L44
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -352(%rbp)
	movaps	%xmm0, -368(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -368(%rbp)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-368(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L38
	call	_ZdlPv@PLT
.L38:
	movq	(%rbx), %rax
	movl	$17, %edx
	movq	%r13, %rdi
	leaq	-416(%rbp), %r12
	movq	8(%rax), %rcx
	movq	(%rax), %rsi
	movq	24(%rax), %r9
	movq	%rcx, -448(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -440(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%r9, -456(%rbp)
	movq	%rcx, -464(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-440(%rbp), %xmm0
	movq	-464(%rbp), %rcx
	movhps	-448(%rbp), %xmm0
	movq	%rcx, -352(%rbp)
	movaps	%xmm0, -368(%rbp)
	pushq	-352(%rbp)
	pushq	-360(%rbp)
	pushq	-368(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	leaq	-96(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	movq	-456(%rbp), %r9
	movl	$1, %r8d
	movl	$278, %esi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-472(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	jmp	.L37
.L61:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22433:
	.size	_ZN2v88internal40ExtrasUtilsMarkPromiseAsHandledAssembler43GenerateExtrasUtilsMarkPromiseAsHandledImplEv, .-_ZN2v88internal40ExtrasUtilsMarkPromiseAsHandledAssembler43GenerateExtrasUtilsMarkPromiseAsHandledImplEv
	.section	.rodata._ZN2v88internal8Builtins40Generate_ExtrasUtilsMarkPromiseAsHandledEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"ExtrasUtilsMarkPromiseAsHandled"
	.section	.text._ZN2v88internal8Builtins40Generate_ExtrasUtilsMarkPromiseAsHandledEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins40Generate_ExtrasUtilsMarkPromiseAsHandledEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins40Generate_ExtrasUtilsMarkPromiseAsHandledEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins40Generate_ExtrasUtilsMarkPromiseAsHandledEPNS0_8compiler18CodeAssemblerStateE:
.LFB22429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$181, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$816, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L66
.L63:
	movq	%r13, %rdi
	call	_ZN2v88internal40ExtrasUtilsMarkPromiseAsHandledAssembler43GenerateExtrasUtilsMarkPromiseAsHandledImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L67
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L63
.L67:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22429:
	.size	_ZN2v88internal8Builtins40Generate_ExtrasUtilsMarkPromiseAsHandledEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins40Generate_ExtrasUtilsMarkPromiseAsHandledEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal32ExtrasUtilsPromiseStateAssembler35GenerateExtrasUtilsPromiseStateImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32ExtrasUtilsPromiseStateAssembler35GenerateExtrasUtilsPromiseStateImplEv
	.type	_ZN2v88internal32ExtrasUtilsPromiseStateAssembler35GenerateExtrasUtilsPromiseStateImplEv, @function
_ZN2v88internal32ExtrasUtilsPromiseStateAssembler35GenerateExtrasUtilsPromiseStateImplEv:
.LFB22442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$440, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -424(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r12, %rcx
	movq	%r13, %rsi
	leaq	-400(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-392(%rbp), %rcx
	movl	$2, %esi
	movq	%r13, %rdi
	movq	-384(%rbp), %rbx
	movq	-400(%rbp), %rax
	movq	%r13, -336(%rbp)
	leaq	-424(%rbp), %r13
	movq	%rcx, -456(%rbp)
	movq	%rcx, -312(%rbp)
	movq	%rbx, -464(%rbp)
	movq	%rbx, -320(%rbp)
	leaq	-288(%rbp), %rbx
	movq	$1, -328(%rbp)
	movq	%rax, -440(%rbp)
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-336(%rbp), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -472(%rbp)
	movq	%rax, -448(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, %r12
	movq	-424(%rbp), %rax
	movq	$0, -264(%rbp)
	movq	%rax, -288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	-440(%rbp), %xmm1
	movq	%r12, -64(%rbp)
	leaq	-368(%rbp), %r12
	movhps	-456(%rbp), %xmm1
	movaps	%xmm0, -368(%rbp)
	movaps	%xmm1, -96(%rbp)
	movq	-464(%rbp), %xmm1
	movq	$0, -352(%rbp)
	movhps	-448(%rbp), %xmm1
	movaps	%xmm1, -80(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	leaq	40(%rax), %rdx
	movq	%rax, -368(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L69
	call	_ZdlPv@PLT
.L69:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -224(%rbp)
	jne	.L93
.L70:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L72
	call	_ZdlPv@PLT
.L72:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L73
	.p2align 4,,10
	.p2align 3
.L77:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L74
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L77
.L75:
	movq	-280(%rbp), %r12
.L73:
	testq	%r12, %r12
	je	.L78
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L78:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L94
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L77
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L93:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -352(%rbp)
	movaps	%xmm0, -368(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -368(%rbp)
	movq	%rdx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-368(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L71
	call	_ZdlPv@PLT
.L71:
	movq	(%rbx), %rax
	movl	$22, %edx
	movq	%r13, %rdi
	leaq	-416(%rbp), %r12
	movq	8(%rax), %rcx
	movq	(%rax), %rsi
	movq	24(%rax), %r9
	movq	%rcx, -448(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -440(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%r9, -456(%rbp)
	movq	%rcx, -464(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-440(%rbp), %xmm0
	movq	-464(%rbp), %rcx
	movhps	-448(%rbp), %xmm0
	movq	%rcx, -352(%rbp)
	movaps	%xmm0, -368(%rbp)
	pushq	-352(%rbp)
	pushq	-360(%rbp)
	pushq	-368(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	leaq	-96(%rbp), %rcx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	movq	-456(%rbp), %r9
	movl	$1, %r8d
	movl	$281, %esi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-472(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	jmp	.L70
.L94:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22442:
	.size	_ZN2v88internal32ExtrasUtilsPromiseStateAssembler35GenerateExtrasUtilsPromiseStateImplEv, .-_ZN2v88internal32ExtrasUtilsPromiseStateAssembler35GenerateExtrasUtilsPromiseStateImplEv
	.section	.rodata._ZN2v88internal8Builtins32Generate_ExtrasUtilsPromiseStateEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"ExtrasUtilsPromiseState"
	.section	.text._ZN2v88internal8Builtins32Generate_ExtrasUtilsPromiseStateEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins32Generate_ExtrasUtilsPromiseStateEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins32Generate_ExtrasUtilsPromiseStateEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins32Generate_ExtrasUtilsPromiseStateEPNS0_8compiler18CodeAssemblerStateE:
.LFB22438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$216, %ecx
	leaq	.LC1(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$817, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L99
.L96:
	movq	%r13, %rdi
	call	_ZN2v88internal32ExtrasUtilsPromiseStateAssembler35GenerateExtrasUtilsPromiseStateImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L100
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L96
.L100:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22438:
	.size	_ZN2v88internal8Builtins32Generate_ExtrasUtilsPromiseStateEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins32Generate_ExtrasUtilsPromiseStateEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins39Generate_ExtrasUtilsCreatePrivateSymbolEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins39Generate_ExtrasUtilsCreatePrivateSymbolEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins39Generate_ExtrasUtilsCreatePrivateSymbolEPNS0_8compiler18CodeAssemblerStateE:
.LFB28896:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28896:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins39Generate_ExtrasUtilsCreatePrivateSymbolEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins39Generate_ExtrasUtilsCreatePrivateSymbolEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins39Generate_ExtrasUtilsCreatePrivateSymbolEPNS0_8compiler18CodeAssemblerStateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
