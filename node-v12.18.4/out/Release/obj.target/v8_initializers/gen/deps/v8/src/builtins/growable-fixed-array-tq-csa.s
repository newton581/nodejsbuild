	.file	"growable-fixed-array-tq-csa.cc"
	.text
	.section	.rodata._ZN2v88internal25NewGrowableFixedArray_305EPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../../deps/v8/../../deps/v8/src/builtins/growable-fixed-array.tq"
	.section	.text._ZN2v88internal25NewGrowableFixedArray_305EPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25NewGrowableFixedArray_305EPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal25NewGrowableFixedArray_305EPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal25NewGrowableFixedArray_305EPNS0_8compiler18CodeAssemblerStateE:
.LFB22410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-216(%rbp), %r14
	leaq	-688(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-600(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-696(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$712, %rsp
	movq	%rdi, -712(%rbp)
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-656(%rbp), %rax
	movq	%rsi, -696(%rbp)
	movq	%rsi, -656(%rbp)
	movq	%r12, %rsi
	movq	%rax, -736(%rbp)
	movq	$0, -648(%rbp)
	movq	$0, -640(%rbp)
	movq	$0, -632(%rbp)
	movq	$0, -624(%rbp)
	movq	$0, -616(%rbp)
	movq	$0, -608(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	%rax, -464(%rbp)
	movq	$0, -440(%rbp)
	call	_Znwm@PLT
	leaq	-408(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -440(%rbp)
	movq	%rdx, -448(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -720(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -424(%rbp)
	movq	%rax, -456(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	%rax, -272(%rbp)
	movq	$0, -248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -248(%rbp)
	movq	%rdx, -256(%rbp)
	xorl	%edx, %edx
	movq	%rax, -264(%rbp)
	movups	%xmm0, -232(%rbp)
	movq	$0, -240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-736(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r15, %rsi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2
	call	_ZdlPv@PLT
.L2:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -592(%rbp)
	jne	.L71
.L3:
	cmpq	$0, -400(%rbp)
	leaq	-272(%rbp), %rbx
	jne	.L72
.L6:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$1287, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$5, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L9
	call	_ZdlPv@PLT
.L9:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movdqu	(%rax), %xmm1
	movq	16(%rax), %rdx
	movq	-712(%rbp), %rax
	movaps	%xmm1, -736(%rbp)
	movq	%rdx, 16(%rax)
	movups	%xmm1, (%rax)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	movq	-256(%rbp), %rbx
	movq	-264(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L11
	.p2align 4,,10
	.p2align 3
.L15:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L12
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L15
.L13:
	movq	-264(%rbp), %r14
.L11:
	testq	%r14, %r14
	je	.L16
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L16:
	movq	-720(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L17
	call	_ZdlPv@PLT
.L17:
	movq	-448(%rbp), %rbx
	movq	-456(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L18
	.p2align 4,,10
	.p2align 3
.L22:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L19
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L22
.L20:
	movq	-456(%rbp), %r14
.L18:
	testq	%r14, %r14
	je	.L23
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L23:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L24
	call	_ZdlPv@PLT
.L24:
	movq	-640(%rbp), %rbx
	movq	-648(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L25
	.p2align 4,,10
	.p2align 3
.L29:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L26
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L29
.L27:
	movq	-648(%rbp), %r13
.L25:
	testq	%r13, %r13
	je	.L30
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L30:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L73
	movq	-712(%rbp), %rax
	addq	$712, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L29
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L12:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L15
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L19:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L22
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L71:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-736(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r15, %rsi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4
	call	_ZdlPv@PLT
.L4:
	movl	$43, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal20kEmptyFixedArray_212EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, -744(%rbp)
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, -736(%rbp)
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$24, %edi
	movq	-744(%rbp), %xmm0
	movq	$0, -672(%rbp)
	movq	%rax, -64(%rbp)
	movhps	-736(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm3
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-464(%rbp), %rdi
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5
	call	_ZdlPv@PLT
.L5:
	movq	-720(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L72:
	movq	-720(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-464(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$1287, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rsi
	movb	$5, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L7
	call	_ZdlPv@PLT
.L7:
	movq	(%rbx), %rax
	movl	$42, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -744(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %edi
	movq	%rbx, -64(%rbp)
	movq	-736(%rbp), %xmm0
	movq	$0, -672(%rbp)
	leaq	-272(%rbp), %rbx
	movhps	-744(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movdqa	-80(%rbp), %xmm4
	leaq	24(%rax), %rdx
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L8
	call	_ZdlPv@PLT
.L8:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L6
.L73:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22410:
	.size	_ZN2v88internal25NewGrowableFixedArray_305EPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal25NewGrowableFixedArray_305EPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal25NewGrowableFixedArray_305EPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal25NewGrowableFixedArray_305EPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal25NewGrowableFixedArray_305EPNS0_8compiler18CodeAssemblerStateE:
.LFB28838:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28838:
	.size	_GLOBAL__sub_I__ZN2v88internal25NewGrowableFixedArray_305EPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal25NewGrowableFixedArray_305EPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal25NewGrowableFixedArray_305EPNS0_8compiler18CodeAssemblerStateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
