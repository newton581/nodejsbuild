	.file	"array-unshift-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev,"axG",@progbits,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev:
.LFB22413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$56, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L8
	call	_ZdlPv@PLT
.L8:
	movq	16(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	%r12, %r13
	je	.L9
	.p2align 4,,10
	.p2align 3
.L13:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L10
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L13
.L11:
	movq	8(%rbx), %r12
.L9:
	testq	%r12, %r12
	je	.L7
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L13
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L7:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22413:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev
	.set	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EE:
.LFB26624:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$8, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$434605082181699591, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L20
	movq	%rax, -120(%rbp)
	call	_ZdlPv@PLT
	movq	-120(%rbp), %rax
.L20:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L21
	movq	%rdx, (%r15)
.L21:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L22
	movq	%rdx, (%r14)
.L22:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L23
	movq	%rdx, 0(%r13)
.L23:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L24
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L24:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L25
	movq	%rdx, (%rbx)
.L25:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L26
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L26:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L27
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L27:
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L19
	movq	-112(%rbp), %rbx
	movq	%rax, (%rbx)
.L19:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L58
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L58:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26624:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EESP_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EESP_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EESP_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EESP_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EESP_:
.LFB26626:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$9, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$434605082181699591, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L60
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
.L60:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L61
	movq	%rdx, (%r15)
.L61:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L62
	movq	%rdx, (%r14)
.L62:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L63
	movq	%rdx, 0(%r13)
.L63:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L64
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L64:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L65
	movq	%rdx, (%rbx)
.L65:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L66
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L66:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L67
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L67:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L68
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L68:
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L59
	movq	-120(%rbp), %rbx
	movq	%rax, (%rbx)
.L59:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L102
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L102:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26626:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EESP_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EESP_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS5_EESK_PNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EEPNSE_IS9_EESQ_SQ_SQ_PNSE_ISC_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS5_EESK_PNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EEPNSE_IS9_EESQ_SQ_SQ_PNSE_ISC_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS5_EESK_PNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EEPNSE_IS9_EESQ_SQ_SQ_PNSE_ISC_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS5_EESK_PNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EEPNSE_IS9_EESQ_SQ_SQ_PNSE_ISC_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS5_EESK_PNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EEPNSE_IS9_EESQ_SQ_SQ_PNSE_ISC_EE:
.LFB26634:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$12, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$120, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$434605082181699591, %rcx
	movq	%rcx, (%rax)
	leaq	12(%rax), %rdx
	movl	$117966856, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L104
	movq	%rax, -152(%rbp)
	call	_ZdlPv@PLT
	movq	-152(%rbp), %rax
.L104:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L105
	movq	%rdx, (%r15)
.L105:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L106
	movq	%rdx, (%r14)
.L106:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L107
	movq	%rdx, 0(%r13)
.L107:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L108
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L108:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L109
	movq	%rdx, (%rbx)
.L109:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L110
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L110:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L111
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L111:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L112
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L112:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L113
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L113:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L114
	movq	-128(%rbp), %rsi
	movq	%rdx, (%rsi)
.L114:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L115
	movq	-136(%rbp), %rcx
	movq	%rdx, (%rcx)
.L115:
	movq	88(%rax), %rax
	testq	%rax, %rax
	je	.L103
	movq	-144(%rbp), %rbx
	movq	%rax, (%rbx)
.L103:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L158
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L158:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26634:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS5_EESK_PNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EEPNSE_IS9_EESQ_SQ_SQ_PNSE_ISC_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS5_EESK_PNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EEPNSE_IS9_EESQ_SQ_SQ_PNSE_ISC_EE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EESP_SR_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EESP_SR_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EESP_SR_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EESP_SR_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EESP_SR_:
.LFB26638:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$10, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$434605082181699591, %rcx
	movq	%rcx, (%rax)
	movl	$1544, %ecx
	leaq	10(%rax), %rdx
	movw	%cx, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L160
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %rax
.L160:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L161
	movq	%rdx, (%r15)
.L161:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L162
	movq	%rdx, (%r14)
.L162:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L163
	movq	%rdx, 0(%r13)
.L163:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L164
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L164:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L165
	movq	%rdx, (%rbx)
.L165:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L166
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L166:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L167
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L167:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L168
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L168:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L169
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L169:
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L159
	movq	-128(%rbp), %rsi
	movq	%rax, (%rsi)
.L159:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L206
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L206:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26638:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EESP_SR_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EESP_SR_
	.section	.rodata._ZN2v88internal22GenericArrayUnshift_49EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS0_21TorqueStructArgumentsE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../../deps/v8/../../deps/v8/src/builtins/array-unshift.tq"
	.section	.text._ZN2v88internal22GenericArrayUnshift_49EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS0_21TorqueStructArgumentsE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22GenericArrayUnshift_49EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS0_21TorqueStructArgumentsE
	.type	_ZN2v88internal22GenericArrayUnshift_49EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS0_21TorqueStructArgumentsE, @function
_ZN2v88internal22GenericArrayUnshift_49EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS0_21TorqueStructArgumentsE:
.LFB22410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-3232(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-3424(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$3656, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -3424(%rbp)
	movq	%rdi, -3232(%rbp)
	movl	$120, %edi
	movq	$0, -3224(%rbp)
	movq	$0, -3216(%rbp)
	movq	$0, -3208(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -3224(%rbp)
	leaq	-3176(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3208(%rbp)
	movq	%rdx, -3216(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3192(%rbp)
	movq	%rax, -3432(%rbp)
	movq	$0, -3200(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3424(%rbp), %rax
	movl	$192, %edi
	movq	$0, -3032(%rbp)
	movq	$0, -3024(%rbp)
	movq	%rax, -3040(%rbp)
	movq	$0, -3016(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -3032(%rbp)
	leaq	-2984(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3016(%rbp)
	movq	%rdx, -3024(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3000(%rbp)
	movq	%rax, -3472(%rbp)
	movq	$0, -3008(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3424(%rbp), %rax
	movl	$192, %edi
	movq	$0, -2840(%rbp)
	movq	$0, -2832(%rbp)
	movq	%rax, -2848(%rbp)
	movq	$0, -2824(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -2840(%rbp)
	leaq	-2792(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2824(%rbp)
	movq	%rdx, -2832(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2808(%rbp)
	movq	%rax, -3464(%rbp)
	movq	$0, -2816(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3424(%rbp), %rax
	movl	$192, %edi
	movq	$0, -2648(%rbp)
	movq	$0, -2640(%rbp)
	movq	%rax, -2656(%rbp)
	movq	$0, -2632(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -2648(%rbp)
	leaq	-2600(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2632(%rbp)
	movq	%rdx, -2640(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2616(%rbp)
	movq	%rax, -3536(%rbp)
	movq	$0, -2624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3424(%rbp), %rax
	movl	$216, %edi
	movq	$0, -2456(%rbp)
	movq	$0, -2448(%rbp)
	movq	%rax, -2464(%rbp)
	movq	$0, -2440(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -2456(%rbp)
	leaq	-2408(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2440(%rbp)
	movq	%rdx, -2448(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2424(%rbp)
	movq	%rax, -3440(%rbp)
	movq	$0, -2432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3424(%rbp), %rax
	movl	$216, %edi
	movq	$0, -2264(%rbp)
	movq	$0, -2256(%rbp)
	movq	%rax, -2272(%rbp)
	movq	$0, -2248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -2264(%rbp)
	leaq	-2216(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2248(%rbp)
	movq	%rdx, -2256(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2232(%rbp)
	movq	%rax, -3616(%rbp)
	movq	$0, -2240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3424(%rbp), %rax
	movl	$288, %edi
	movq	$0, -2072(%rbp)
	movq	$0, -2064(%rbp)
	movq	%rax, -2080(%rbp)
	movq	$0, -2056(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -2072(%rbp)
	leaq	-2024(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2056(%rbp)
	movq	%rdx, -2064(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2040(%rbp)
	movq	%rax, -3480(%rbp)
	movq	$0, -2048(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3424(%rbp), %rax
	movl	$288, %edi
	movq	$0, -1880(%rbp)
	movq	$0, -1872(%rbp)
	movq	%rax, -1888(%rbp)
	movq	$0, -1864(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -1880(%rbp)
	leaq	-1832(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1864(%rbp)
	movq	%rdx, -1872(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1848(%rbp)
	movq	%rax, -3576(%rbp)
	movq	$0, -1856(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3424(%rbp), %rax
	movl	$288, %edi
	movq	$0, -1688(%rbp)
	movq	$0, -1680(%rbp)
	movq	%rax, -1696(%rbp)
	movq	$0, -1672(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -1688(%rbp)
	leaq	-1640(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1672(%rbp)
	movq	%rdx, -1680(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1656(%rbp)
	movq	%rax, -3456(%rbp)
	movq	$0, -1664(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3424(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1496(%rbp)
	movq	$0, -1488(%rbp)
	movq	%rax, -1504(%rbp)
	movq	$0, -1480(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1496(%rbp)
	leaq	-1448(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1480(%rbp)
	movq	%rdx, -1488(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1464(%rbp)
	movq	%rax, -3632(%rbp)
	movq	$0, -1472(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3424(%rbp), %rax
	movl	$240, %edi
	movq	$0, -1304(%rbp)
	movq	$0, -1296(%rbp)
	movq	%rax, -1312(%rbp)
	movq	$0, -1288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -1304(%rbp)
	leaq	-1256(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1288(%rbp)
	movq	%rdx, -1296(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1272(%rbp)
	movq	%rax, -3592(%rbp)
	movq	$0, -1280(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3424(%rbp), %rax
	movl	$240, %edi
	movq	$0, -1112(%rbp)
	movq	$0, -1104(%rbp)
	movq	%rax, -1120(%rbp)
	movq	$0, -1096(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -1112(%rbp)
	leaq	-1064(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1096(%rbp)
	movq	%rdx, -1104(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1080(%rbp)
	movq	%rax, -3600(%rbp)
	movq	$0, -1088(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3424(%rbp), %rax
	movl	$240, %edi
	movq	$0, -920(%rbp)
	movq	$0, -912(%rbp)
	movq	%rax, -928(%rbp)
	movq	$0, -904(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -920(%rbp)
	leaq	-872(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -904(%rbp)
	movq	%rdx, -912(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -888(%rbp)
	movq	%rax, -3488(%rbp)
	movq	$0, -896(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3424(%rbp), %rax
	movl	$192, %edi
	movq	$0, -728(%rbp)
	movq	$0, -720(%rbp)
	movq	%rax, -736(%rbp)
	movq	$0, -712(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -728(%rbp)
	leaq	-680(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -712(%rbp)
	movq	%rdx, -720(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -696(%rbp)
	movq	%rax, -3448(%rbp)
	movq	$0, -704(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3424(%rbp), %rax
	movl	$144, %edi
	movq	$0, -536(%rbp)
	movq	$0, -528(%rbp)
	movq	%rax, -544(%rbp)
	movq	$0, -520(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -536(%rbp)
	leaq	-488(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -520(%rbp)
	movq	%rdx, -528(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -504(%rbp)
	movq	%rax, -3528(%rbp)
	movq	$0, -512(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3424(%rbp), %rax
	movl	$144, %edi
	movq	$0, -344(%rbp)
	movq	$0, -336(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -328(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -344(%rbp)
	leaq	-296(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -328(%rbp)
	movq	%rdx, -336(%rbp)
	xorl	%edx, %edx
	movq	%rax, -3584(%rbp)
	movups	%xmm0, -312(%rbp)
	movq	$0, -320(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	16(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	%r15, -160(%rbp)
	leaq	-3264(%rbp), %r15
	movq	%rax, -144(%rbp)
	movq	24(%rbp), %rax
	movaps	%xmm0, -3264(%rbp)
	movq	%rax, -136(%rbp)
	movq	32(%rbp), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -3248(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r13, %rdi
	movq	%r15, %rsi
	movdqa	-160(%rbp), %xmm6
	movdqa	-144(%rbp), %xmm7
	leaq	40(%rax), %rdx
	movq	%rax, -3264(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -3248(%rbp)
	movq	%rdx, -3256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L208
	call	_ZdlPv@PLT
.L208:
	movq	-3432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3168(%rbp)
	jne	.L427
.L209:
	leaq	-2848(%rbp), %rax
	cmpq	$0, -2976(%rbp)
	movq	%rax, -3504(%rbp)
	leaq	-2656(%rbp), %rax
	movq	%rax, -3520(%rbp)
	jne	.L428
	cmpq	$0, -2784(%rbp)
	jne	.L429
.L216:
	cmpq	$0, -2592(%rbp)
	jne	.L430
.L217:
	leaq	-2272(%rbp), %rax
	cmpq	$0, -2400(%rbp)
	movq	%rax, -3536(%rbp)
	leaq	-1504(%rbp), %rax
	movq	%rax, -3568(%rbp)
	jne	.L431
.L219:
	leaq	-1888(%rbp), %rax
	cmpq	$0, -2208(%rbp)
	movq	%rax, -3552(%rbp)
	jne	.L432
	cmpq	$0, -2016(%rbp)
	jne	.L433
.L225:
	cmpq	$0, -1824(%rbp)
	jne	.L434
.L227:
	cmpq	$0, -1632(%rbp)
	jne	.L435
.L229:
	leaq	-1312(%rbp), %rax
	cmpq	$0, -1440(%rbp)
	movq	%rax, -3464(%rbp)
	jne	.L436
.L231:
	leaq	-1120(%rbp), %rax
	cmpq	$0, -1248(%rbp)
	movq	%rax, -3576(%rbp)
	jne	.L437
	cmpq	$0, -1056(%rbp)
	jne	.L438
.L236:
	cmpq	$0, -864(%rbp)
	jne	.L439
.L238:
	cmpq	$0, -672(%rbp)
	jne	.L440
.L240:
	cmpq	$0, -480(%rbp)
	leaq	-352(%rbp), %r13
	jne	.L441
.L242:
	movq	-3584(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -3248(%rbp)
	movaps	%xmm0, -3264(%rbp)
	call	_Znwm@PLT
	movl	$2053, %ecx
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$84215815, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -3264(%rbp)
	movq	%rdx, -3248(%rbp)
	movq	%rdx, -3256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3264(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L245
	call	_ZdlPv@PLT
.L245:
	movq	(%rbx), %rax
	movq	%r13, %rdi
	movq	40(%rax), %r14
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3528(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-512(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L246
	call	_ZdlPv@PLT
.L246:
	movq	-528(%rbp), %rbx
	movq	-536(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L247
	.p2align 4,,10
	.p2align 3
.L251:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L248
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L251
.L249:
	movq	-536(%rbp), %r13
.L247:
	testq	%r13, %r13
	je	.L252
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L252:
	movq	-3448(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L253
	call	_ZdlPv@PLT
.L253:
	movq	-720(%rbp), %rbx
	movq	-728(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L254
	.p2align 4,,10
	.p2align 3
.L258:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L255
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L258
.L256:
	movq	-728(%rbp), %r13
.L254:
	testq	%r13, %r13
	je	.L259
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L259:
	movq	-3488(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-896(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L260
	call	_ZdlPv@PLT
.L260:
	movq	-912(%rbp), %rbx
	movq	-920(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L261
	.p2align 4,,10
	.p2align 3
.L265:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L262
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L265
.L263:
	movq	-920(%rbp), %r13
.L261:
	testq	%r13, %r13
	je	.L266
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L266:
	movq	-3576(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3464(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3568(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3456(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L267
	call	_ZdlPv@PLT
.L267:
	movq	-1680(%rbp), %rbx
	movq	-1688(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L268
	.p2align 4,,10
	.p2align 3
.L272:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L269
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L272
.L270:
	movq	-1688(%rbp), %r13
.L268:
	testq	%r13, %r13
	je	.L273
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L273:
	movq	-3552(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3480(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L274
	call	_ZdlPv@PLT
.L274:
	movq	-2064(%rbp), %rbx
	movq	-2072(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L275
	.p2align 4,,10
	.p2align 3
.L279:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L276
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L279
.L277:
	movq	-2072(%rbp), %r13
.L275:
	testq	%r13, %r13
	je	.L280
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L280:
	movq	-3536(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3440(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L281
	call	_ZdlPv@PLT
.L281:
	movq	-2448(%rbp), %rbx
	movq	-2456(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L282
	.p2align 4,,10
	.p2align 3
.L286:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L283
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L286
.L284:
	movq	-2456(%rbp), %r13
.L282:
	testq	%r13, %r13
	je	.L287
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L287:
	movq	-3520(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3504(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3472(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3008(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L288
	call	_ZdlPv@PLT
.L288:
	movq	-3024(%rbp), %rbx
	movq	-3032(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L289
	.p2align 4,,10
	.p2align 3
.L293:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L290
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L293
.L291:
	movq	-3032(%rbp), %r13
.L289:
	testq	%r13, %r13
	je	.L294
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L294:
	movq	-3432(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L295
	call	_ZdlPv@PLT
.L295:
	movq	-3216(%rbp), %rbx
	movq	-3224(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L296
	.p2align 4,,10
	.p2align 3
.L300:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L297
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L300
.L298:
	movq	-3224(%rbp), %r13
.L296:
	testq	%r13, %r13
	je	.L301
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L301:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L442
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L300
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L290:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L293
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L283:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L286
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L276:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L279
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L269:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L272
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L262:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L265
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L248:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L251
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L255:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L258
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L427:
	movq	-3432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -3248(%rbp)
	movaps	%xmm0, -3264(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$84215815, (%rax)
	leaq	5(%rax), %rdx
	movb	$5, 4(%rax)
	movq	%rax, -3264(%rbp)
	movq	%rdx, -3248(%rbp)
	movq	%rdx, -3256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3264(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L210
	call	_ZdlPv@PLT
.L210:
	movq	(%rbx), %rax
	movl	$11, %edx
	movq	%r12, %rdi
	movq	24(%rax), %rsi
	movq	16(%rax), %rcx
	movq	(%rax), %rbx
	movq	8(%rax), %r13
	movq	32(%rax), %rax
	movq	%rsi, -3648(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rcx, -3568(%rbp)
	movq	%rax, -3504(%rbp)
	movq	%r13, -3552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ToObject_InlineENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	movq	%rax, -3688(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$14, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal21GetLengthProperty_244EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$17, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -3664(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3504(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal26Convert5ATSmi8ATintptr_184EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movl	$20, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -3520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-3520(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17IntPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm7
	pxor	%xmm0, %xmm0
	movq	-3664(%rbp), %xmm4
	movq	-3504(%rbp), %xmm5
	movhps	-3552(%rbp), %xmm7
	movq	-3568(%rbp), %xmm6
	movl	$64, %edi
	movhps	-3520(%rbp), %xmm4
	movaps	%xmm7, -3504(%rbp)
	movhps	-3688(%rbp), %xmm5
	movhps	-3648(%rbp), %xmm6
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm4, -3680(%rbp)
	movaps	%xmm5, -3664(%rbp)
	movaps	%xmm6, -3520(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm0, -3264(%rbp)
	movq	$0, -3248(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm6
	movdqa	-144(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	64(%rax), %rdx
	leaq	-3040(%rbp), %rdi
	movq	%rax, -3264(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-128(%rbp), %xmm6
	movups	%xmm7, 16(%rax)
	movdqa	-112(%rbp), %xmm7
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rdx, -3248(%rbp)
	movq	%rdx, -3256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L211
	call	_ZdlPv@PLT
.L211:
	movdqa	-3504(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movdqa	-3520(%rbp), %xmm7
	movaps	%xmm0, -3264(%rbp)
	movaps	%xmm6, -160(%rbp)
	movdqa	-3664(%rbp), %xmm6
	movaps	%xmm7, -144(%rbp)
	movdqa	-3680(%rbp), %xmm7
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movq	$0, -3248(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm4
	movdqa	-128(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm5
	leaq	64(%rax), %rdx
	leaq	-736(%rbp), %rdi
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rax, -3264(%rbp)
	movq	%rdx, -3248(%rbp)
	movq	%rdx, -3256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L212
	call	_ZdlPv@PLT
.L212:
	movq	-3448(%rbp), %rcx
	movq	-3472(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L428:
	movq	-3472(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3368(%rbp)
	leaq	-3040(%rbp), %r13
	movq	$0, -3360(%rbp)
	movq	$0, -3352(%rbp)
	movq	$0, -3344(%rbp)
	movq	$0, -3336(%rbp)
	movq	$0, -3328(%rbp)
	movq	$0, -3312(%rbp)
	movq	$0, -3296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r13, %rdi
	leaq	-3296(%rbp), %rax
	pushq	%rax
	leaq	-3312(%rbp), %rax
	leaq	-3352(%rbp), %rcx
	pushq	%rax
	leaq	-3328(%rbp), %rax
	leaq	-3336(%rbp), %r9
	pushq	%rax
	leaq	-3344(%rbp), %r8
	leaq	-3360(%rbp), %rdx
	leaq	-3368(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EE
	addq	$32, %rsp
	movl	$22, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3296(%rbp), %rdx
	movq	-3312(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberAddENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movsd	.LC2(%rip), %xmm0
	movq	%r14, %rdi
	call	_ZN2v88internal60FromConstexpr20UT5ATSmi10HeapNumber19ATconstexpr_float64_157EPNS0_8compiler18CodeAssemblerStateEd@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal22NumberIsGreaterThan_77EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	-3312(%rbp), %xmm2
	movq	-3336(%rbp), %xmm3
	movaps	%xmm0, -3264(%rbp)
	movq	%rax, %r13
	movq	-3352(%rbp), %xmm1
	movq	-3368(%rbp), %xmm4
	movhps	-3296(%rbp), %xmm2
	movq	$0, -3248(%rbp)
	movhps	-3328(%rbp), %xmm3
	movhps	-3344(%rbp), %xmm1
	movaps	%xmm2, -3520(%rbp)
	movhps	-3360(%rbp), %xmm4
	movaps	%xmm3, -3648(%rbp)
	movaps	%xmm1, -3568(%rbp)
	movaps	%xmm4, -3552(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm4
	movdqa	-128(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm5
	leaq	64(%rax), %rdx
	movq	%rax, -3264(%rbp)
	movq	-3504(%rbp), %rdi
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rdx, -3248(%rbp)
	movq	%rdx, -3256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L214
	call	_ZdlPv@PLT
.L214:
	movdqa	-3552(%rbp), %xmm4
	movdqa	-3568(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movdqa	-3648(%rbp), %xmm6
	movdqa	-3520(%rbp), %xmm7
	movaps	%xmm0, -3264(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movq	$0, -3248(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm4
	movdqa	-128(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm5
	leaq	64(%rax), %rdx
	movq	%rax, -3264(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	leaq	-2656(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3248(%rbp)
	movq	%rdx, -3256(%rbp)
	movq	%rax, -3520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L215
	call	_ZdlPv@PLT
.L215:
	movq	-3536(%rbp), %rcx
	movq	-3464(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2784(%rbp)
	je	.L216
.L429:
	movq	-3464(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3368(%rbp)
	movq	$0, -3360(%rbp)
	movq	$0, -3352(%rbp)
	movq	$0, -3344(%rbp)
	movq	$0, -3336(%rbp)
	movq	$0, -3328(%rbp)
	movq	$0, -3312(%rbp)
	movq	$0, -3296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3296(%rbp), %rax
	movq	-3504(%rbp), %rdi
	pushq	%rax
	leaq	-3312(%rbp), %rax
	leaq	-3336(%rbp), %r9
	pushq	%rax
	leaq	-3328(%rbp), %rax
	leaq	-3352(%rbp), %rcx
	pushq	%rax
	leaq	-3344(%rbp), %r8
	leaq	-3360(%rbp), %rdx
	leaq	-3368(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EE
	addq	$32, %rsp
	movl	$23, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$189, %edx
	movq	-3368(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -2592(%rbp)
	je	.L217
.L430:
	movq	-3536(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3368(%rbp)
	movq	$0, -3360(%rbp)
	movq	$0, -3352(%rbp)
	movq	$0, -3344(%rbp)
	movq	$0, -3336(%rbp)
	movq	$0, -3328(%rbp)
	movq	$0, -3312(%rbp)
	movq	$0, -3296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3296(%rbp), %rax
	movq	-3520(%rbp), %rdi
	pushq	%rax
	leaq	-3312(%rbp), %rax
	leaq	-3352(%rbp), %rcx
	pushq	%rax
	leaq	-3328(%rbp), %rax
	leaq	-3336(%rbp), %r9
	pushq	%rax
	leaq	-3344(%rbp), %r8
	leaq	-3360(%rbp), %rdx
	leaq	-3368(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EE
	addq	$32, %rsp
	movl	$27, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	.LC1(%rip), %rsi
	movl	$30, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3368(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	-3312(%rbp), %rax
	movl	$72, %edi
	movaps	%xmm0, -3264(%rbp)
	movq	%rdx, -160(%rbp)
	movq	-3360(%rbp), %rdx
	movq	%rax, -112(%rbp)
	movq	%rdx, -152(%rbp)
	movq	-3352(%rbp), %rdx
	movq	%rax, -96(%rbp)
	movq	%rdx, -144(%rbp)
	movq	-3344(%rbp), %rdx
	movq	$0, -3248(%rbp)
	movq	%rdx, -136(%rbp)
	movq	-3336(%rbp), %rdx
	movq	%rdx, -128(%rbp)
	movq	-3328(%rbp), %rdx
	movq	%rdx, -120(%rbp)
	movq	-3296(%rbp), %rdx
	movq	%rdx, -104(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm4
	leaq	72(%rax), %rdx
	leaq	-2464(%rbp), %rdi
	movdqa	-144(%rbp), %xmm5
	movq	%rcx, 64(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rax, -3264(%rbp)
	movq	%rdx, -3248(%rbp)
	movq	%rdx, -3256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L218
	call	_ZdlPv@PLT
.L218:
	movq	-3440(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L431:
	movq	-3440(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3376(%rbp)
	leaq	-2464(%rbp), %r13
	movq	$0, -3368(%rbp)
	movq	$0, -3360(%rbp)
	movq	$0, -3352(%rbp)
	movq	$0, -3344(%rbp)
	movq	$0, -3336(%rbp)
	movq	$0, -3328(%rbp)
	movq	$0, -3312(%rbp)
	movq	$0, -3296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3296(%rbp), %rax
	movq	%r13, %rdi
	leaq	-3352(%rbp), %r8
	pushq	%rax
	leaq	-3312(%rbp), %rax
	leaq	-3360(%rbp), %rcx
	pushq	%rax
	leaq	-3328(%rbp), %rax
	leaq	-3344(%rbp), %r9
	pushq	%rax
	leaq	-3336(%rbp), %rax
	leaq	-3368(%rbp), %rdx
	pushq	%rax
	leaq	-3376(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EESP_
	addq	$32, %rsp
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	-3296(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal22NumberIsGreaterThan_77EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	movq	-3344(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	-3368(%rbp), %rcx
	movq	-3360(%rbp), %rsi
	movq	-3352(%rbp), %rdx
	movq	%rax, %r13
	movaps	%xmm0, -3264(%rbp)
	movq	-3336(%rbp), %r11
	movq	-3328(%rbp), %r10
	movq	%rdi, -3664(%rbp)
	movq	-3312(%rbp), %r9
	movq	-3376(%rbp), %rax
	movq	%rdi, -128(%rbp)
	movl	$72, %edi
	movq	-3296(%rbp), %rbx
	movq	%rcx, -3696(%rbp)
	movq	%rsi, -3552(%rbp)
	movq	%rdx, -3688(%rbp)
	movq	%r11, -3648(%rbp)
	movq	%r10, -3464(%rbp)
	movq	%r9, -3568(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%rsi, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%r11, -120(%rbp)
	movq	%r10, -112(%rbp)
	movq	%r9, -104(%rbp)
	movq	%rax, -3680(%rbp)
	movq	%rax, -160(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -3248(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm4
	leaq	72(%rax), %rdx
	movq	%rax, -3264(%rbp)
	movdqa	-144(%rbp), %xmm5
	movq	-3536(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rdx, -3248(%rbp)
	movq	%rdx, -3256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L220
	call	_ZdlPv@PLT
.L220:
	movq	-3680(%rbp), %xmm0
	movl	$72, %edi
	movq	%rbx, -96(%rbp)
	movq	$0, -3248(%rbp)
	movhps	-3696(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-3552(%rbp), %xmm0
	movhps	-3688(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-3664(%rbp), %xmm0
	movhps	-3648(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-3464(%rbp), %xmm0
	movhps	-3568(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3264(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm4
	leaq	72(%rax), %rdx
	movq	%rax, -3264(%rbp)
	movdqa	-144(%rbp), %xmm5
	movq	%rcx, 64(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	leaq	-1504(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3248(%rbp)
	movq	%rdx, -3256(%rbp)
	movq	%rax, -3568(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L221
	call	_ZdlPv@PLT
.L221:
	movq	-3632(%rbp), %rcx
	movq	-3616(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L432:
	movq	-3616(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3392(%rbp)
	movq	$0, -3384(%rbp)
	movq	$0, -3376(%rbp)
	movq	$0, -3368(%rbp)
	movq	$0, -3360(%rbp)
	movq	$0, -3352(%rbp)
	movq	$0, -3344(%rbp)
	movq	$0, -3336(%rbp)
	movq	$0, -3328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3328(%rbp), %rax
	movq	-3536(%rbp), %rdi
	leaq	-3376(%rbp), %rcx
	pushq	%rax
	leaq	-3336(%rbp), %rax
	leaq	-3368(%rbp), %r8
	pushq	%rax
	leaq	-3344(%rbp), %rax
	leaq	-3360(%rbp), %r9
	pushq	%rax
	leaq	-3352(%rbp), %rax
	leaq	-3384(%rbp), %rdx
	pushq	%rax
	leaq	-3392(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EESP_
	addq	$32, %rsp
	movl	$32, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3328(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberSubENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r15, %rdi
	movq	%rax, -3464(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$35, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3336(%rbp), %rdx
	movq	-3328(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberAddENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberSubENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r15, %rdi
	leaq	-3312(%rbp), %r13
	movq	%rax, -3552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$38, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3392(%rbp), %r9
	movq	%r13, %rdi
	movq	-3352(%rbp), %rbx
	movq	%r9, -3616(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$159, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-3264(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	-160(%rbp), %rcx
	movl	$2, %ebx
	movq	%rax, %r8
	movq	-3616(%rbp), %r9
	pushq	%rbx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movhps	-3464(%rbp), %xmm0
	leaq	-3296(%rbp), %rdx
	pushq	%rcx
	movl	$1, %ecx
	movq	%rax, -3296(%rbp)
	movq	-3248(%rbp), %rax
	movaps	%xmm0, -160(%rbp)
	movq	%rax, -3288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$41, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3328(%rbp), %rax
	movq	%rbx, %xmm4
	movq	-3552(%rbp), %xmm3
	movq	-3344(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movq	-3360(%rbp), %xmm6
	movl	$96, %edi
	movq	-3376(%rbp), %xmm7
	movq	%rax, -3688(%rbp)
	punpcklqdq	%xmm4, %xmm3
	movq	-3392(%rbp), %xmm2
	movq	%rax, -96(%rbp)
	movhps	-3336(%rbp), %xmm5
	movhps	-3352(%rbp), %xmm6
	movq	-3464(%rbp), %rax
	movhps	-3368(%rbp), %xmm7
	movaps	%xmm6, -128(%rbp)
	movhps	-3384(%rbp), %xmm2
	movaps	%xmm5, -3680(%rbp)
	movaps	%xmm6, -3664(%rbp)
	movaps	%xmm7, -3648(%rbp)
	movaps	%xmm2, -3616(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm3, -3552(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm0, -3264(%rbp)
	movq	%rax, -88(%rbp)
	movq	$0, -3248(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm7
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm6
	leaq	96(%rax), %rdx
	leaq	-2080(%rbp), %rdi
	movups	%xmm5, (%rax)
	movdqa	-96(%rbp), %xmm5
	movups	%xmm6, 16(%rax)
	movdqa	-80(%rbp), %xmm6
	movups	%xmm7, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm5, 64(%rax)
	movups	%xmm6, 80(%rax)
	movq	%rax, -3264(%rbp)
	movq	%rdx, -3248(%rbp)
	movq	%rdx, -3256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3264(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L223
	call	_ZdlPv@PLT
.L223:
	movq	-3688(%rbp), %xmm0
	movdqa	-3616(%rbp), %xmm7
	movl	$96, %edi
	movq	$0, -3248(%rbp)
	movdqa	-3648(%rbp), %xmm2
	movdqa	-3664(%rbp), %xmm3
	movdqa	-3680(%rbp), %xmm4
	movdqa	-3552(%rbp), %xmm5
	movhps	-3464(%rbp), %xmm0
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm0, -3264(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm2
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm4
	leaq	96(%rax), %rdx
	movq	%rax, -3264(%rbp)
	movdqa	-144(%rbp), %xmm7
	movdqa	-80(%rbp), %xmm5
	movups	%xmm6, (%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm5, 80(%rax)
	leaq	-1888(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3248(%rbp)
	movq	%rdx, -3256(%rbp)
	movq	%rax, -3552(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L224
	call	_ZdlPv@PLT
.L224:
	movq	-3576(%rbp), %rcx
	movq	-3480(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2016(%rbp)
	je	.L225
.L433:
	movq	-3480(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3416(%rbp)
	leaq	-2080(%rbp), %r13
	movq	$0, -3408(%rbp)
	movq	$0, -3400(%rbp)
	movq	$0, -3392(%rbp)
	movq	$0, -3384(%rbp)
	movq	$0, -3376(%rbp)
	movq	$0, -3368(%rbp)
	movq	$0, -3360(%rbp)
	movq	$0, -3352(%rbp)
	movq	$0, -3344(%rbp)
	movq	$0, -3336(%rbp)
	movq	$0, -3328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r13, %rdi
	leaq	-3328(%rbp), %rax
	pushq	%rax
	leaq	-3336(%rbp), %rax
	leaq	-3400(%rbp), %rcx
	pushq	%rax
	leaq	-3344(%rbp), %rax
	leaq	-3392(%rbp), %r8
	pushq	%rax
	leaq	-3352(%rbp), %rax
	leaq	-3384(%rbp), %r9
	pushq	%rax
	leaq	-3360(%rbp), %rax
	leaq	-3408(%rbp), %rdx
	pushq	%rax
	leaq	-3368(%rbp), %rax
	leaq	-3416(%rbp), %rsi
	pushq	%rax
	leaq	-3376(%rbp), %rax
	leaq	-3312(%rbp), %r13
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS5_EESK_PNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EEPNSE_IS9_EESQ_SQ_SQ_PNSE_ISC_EE
	addq	$64, %rsp
	movl	$43, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3416(%rbp), %r9
	movq	%r13, %rdi
	movq	-3344(%rbp), %rax
	movq	-3376(%rbp), %rbx
	movq	%r9, -3616(%rbp)
	movq	%rax, -3464(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-3264(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r8
	movq	-3248(%rbp), %rax
	movl	$2, %ebx
	leaq	-3296(%rbp), %r11
	pushq	%rbx
	movq	-3616(%rbp), %r9
	movq	%r11, %rdx
	movl	$1, %ecx
	movq	%rax, -3288(%rbp)
	leaq	-160(%rbp), %rax
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	movhps	-3464(%rbp), %xmm0
	pushq	%rax
	movq	%r10, -3296(%rbp)
	movq	%r11, -3680(%rbp)
	movaps	%xmm0, -160(%rbp)
	movq	%rax, -3648(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$46, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3416(%rbp), %r9
	movq	-3376(%rbp), %rcx
	movq	%r13, %rdi
	movq	-3336(%rbp), %rsi
	movq	%r9, -3664(%rbp)
	movq	%rcx, -3616(%rbp)
	movq	%rsi, -3464(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$712, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-3264(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-3648(%rbp), %rcx
	xorl	%esi, %esi
	movq	%rbx, -144(%rbp)
	movl	$3, %ebx
	movq	-3680(%rbp), %r11
	movq	%rax, %r8
	movq	%r13, %rdi
	pushq	%rbx
	movq	-3664(%rbp), %r9
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	pushq	%rcx
	movq	-3248(%rbp), %rax
	movq	%r11, %rdx
	movl	$1, %ecx
	movq	-3616(%rbp), %xmm0
	movq	%r10, -3296(%rbp)
	movq	%rax, -3288(%rbp)
	movhps	-3464(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$41, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$96, %edi
	movq	-3336(%rbp), %xmm0
	movq	-3352(%rbp), %xmm1
	movq	-3368(%rbp), %xmm2
	movq	-3384(%rbp), %xmm3
	movq	$0, -3248(%rbp)
	movq	-3400(%rbp), %xmm4
	movhps	-3328(%rbp), %xmm0
	movq	-3416(%rbp), %xmm5
	movhps	-3344(%rbp), %xmm1
	movhps	-3360(%rbp), %xmm2
	movhps	-3376(%rbp), %xmm3
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3392(%rbp), %xmm4
	movhps	-3408(%rbp), %xmm5
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -3264(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm2
	leaq	-1696(%rbp), %rdi
	movdqa	-144(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm3
	leaq	96(%rax), %rdx
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm5, 80(%rax)
	movq	%rax, -3264(%rbp)
	movq	%rdx, -3248(%rbp)
	movq	%rdx, -3256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L226
	call	_ZdlPv@PLT
.L226:
	movq	-3456(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1824(%rbp)
	je	.L227
.L434:
	movq	-3576(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3416(%rbp)
	leaq	-3312(%rbp), %r13
	movq	$0, -3408(%rbp)
	movq	$0, -3400(%rbp)
	movq	$0, -3392(%rbp)
	movq	$0, -3384(%rbp)
	movq	$0, -3376(%rbp)
	movq	$0, -3368(%rbp)
	movq	$0, -3360(%rbp)
	movq	$0, -3352(%rbp)
	movq	$0, -3344(%rbp)
	movq	$0, -3336(%rbp)
	movq	$0, -3328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3328(%rbp), %rax
	movq	-3552(%rbp), %rdi
	pushq	%rax
	leaq	-3336(%rbp), %rax
	leaq	-3392(%rbp), %r8
	pushq	%rax
	leaq	-3344(%rbp), %rax
	leaq	-3400(%rbp), %rcx
	pushq	%rax
	leaq	-3352(%rbp), %rax
	leaq	-3384(%rbp), %r9
	pushq	%rax
	leaq	-3360(%rbp), %rax
	leaq	-3408(%rbp), %rdx
	pushq	%rax
	leaq	-3368(%rbp), %rax
	leaq	-3416(%rbp), %rsi
	pushq	%rax
	leaq	-3376(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS5_EESK_PNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EEPNSE_IS9_EESQ_SQ_SQ_PNSE_ISC_EE
	addq	$64, %rsp
	movl	$49, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal59FromConstexpr14ATLanguageMode24ATconstexpr_LanguageMode_166EPNS0_8compiler18CodeAssemblerStateENS0_12LanguageModeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3416(%rbp), %r9
	movq	-3336(%rbp), %rcx
	movq	%r13, %rdi
	movq	-3376(%rbp), %rax
	movq	%r9, -3616(%rbp)
	movq	%rcx, -3576(%rbp)
	movq	%rax, -3464(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$160, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-3264(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	xorl	%esi, %esi
	movq	%rbx, -144(%rbp)
	movq	%rcx, -3296(%rbp)
	movl	$3, %ebx
	movq	%rax, %r8
	movq	%r13, %rdi
	leaq	-160(%rbp), %rcx
	pushq	%rbx
	movq	-3464(%rbp), %xmm0
	leaq	-3296(%rbp), %rdx
	movq	-3616(%rbp), %r9
	movq	-3248(%rbp), %rax
	pushq	%rcx
	movl	$1, %ecx
	movhps	-3576(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%rax, -3288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$41, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$96, %edi
	movq	-3336(%rbp), %xmm0
	movq	-3352(%rbp), %xmm1
	movq	-3368(%rbp), %xmm2
	movq	-3384(%rbp), %xmm3
	movq	$0, -3248(%rbp)
	movq	-3400(%rbp), %xmm4
	movhps	-3328(%rbp), %xmm0
	movq	-3416(%rbp), %xmm5
	movhps	-3344(%rbp), %xmm1
	movhps	-3360(%rbp), %xmm2
	movhps	-3376(%rbp), %xmm3
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3392(%rbp), %xmm4
	movhps	-3408(%rbp), %xmm5
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -3264(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm2
	leaq	-1696(%rbp), %rdi
	movdqa	-144(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm3
	leaq	96(%rax), %rdx
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm5, 80(%rax)
	movq	%rax, -3264(%rbp)
	movq	%rdx, -3248(%rbp)
	movq	%rdx, -3256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3264(%rbp), %rdi
	popq	%rbx
	popq	%r13
	testq	%rdi, %rdi
	je	.L228
	call	_ZdlPv@PLT
.L228:
	movq	-3456(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1632(%rbp)
	je	.L229
.L435:
	movq	-3456(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3400(%rbp)
	leaq	-1696(%rbp), %r13
	movq	$0, -3392(%rbp)
	movq	$0, -3384(%rbp)
	movq	$0, -3376(%rbp)
	movq	$0, -3368(%rbp)
	movq	$0, -3360(%rbp)
	movq	$0, -3352(%rbp)
	movq	$0, -3344(%rbp)
	movq	$0, -3336(%rbp)
	movq	$0, -3328(%rbp)
	movq	$0, -3312(%rbp)
	movq	$0, -3296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r13, %rdi
	leaq	-3296(%rbp), %rax
	pushq	%rax
	leaq	-3312(%rbp), %rax
	leaq	-3384(%rbp), %rcx
	pushq	%rax
	leaq	-3328(%rbp), %rax
	leaq	-3368(%rbp), %r9
	pushq	%rax
	leaq	-3336(%rbp), %rax
	leaq	-3376(%rbp), %r8
	pushq	%rax
	leaq	-3344(%rbp), %rax
	leaq	-3392(%rbp), %rdx
	pushq	%rax
	leaq	-3352(%rbp), %rax
	leaq	-3400(%rbp), %rsi
	pushq	%rax
	leaq	-3360(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS5_EESK_PNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EEPNSE_IS9_EESQ_SQ_SQ_PNSE_ISC_EE
	addq	$64, %rsp
	movl	$53, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3336(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberSubENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$30, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$72, %edi
	movq	%rbx, -96(%rbp)
	movq	-3352(%rbp), %xmm0
	movq	-3368(%rbp), %xmm1
	movq	-3384(%rbp), %xmm2
	movq	$0, -3248(%rbp)
	movq	-3400(%rbp), %xmm3
	movhps	-3344(%rbp), %xmm0
	movhps	-3360(%rbp), %xmm1
	movhps	-3376(%rbp), %xmm2
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3392(%rbp), %xmm3
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3264(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm1
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-160(%rbp), %xmm6
	leaq	72(%rax), %rdx
	leaq	-2464(%rbp), %rdi
	movdqa	-144(%rbp), %xmm7
	movq	%rcx, 64(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm2, 48(%rax)
	movq	%rax, -3264(%rbp)
	movq	%rdx, -3248(%rbp)
	movq	%rdx, -3256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L230
	call	_ZdlPv@PLT
.L230:
	movq	-3440(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L436:
	movq	-3632(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3376(%rbp)
	movq	$0, -3368(%rbp)
	movq	$0, -3360(%rbp)
	movq	$0, -3352(%rbp)
	movq	$0, -3344(%rbp)
	movq	$0, -3336(%rbp)
	movq	$0, -3328(%rbp)
	movq	$0, -3312(%rbp)
	movq	$0, -3296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3296(%rbp), %rax
	movq	-3568(%rbp), %rdi
	leaq	-3360(%rbp), %rcx
	pushq	%rax
	leaq	-3312(%rbp), %rax
	leaq	-3344(%rbp), %r9
	pushq	%rax
	leaq	-3328(%rbp), %rax
	leaq	-3352(%rbp), %r8
	pushq	%rax
	leaq	-3336(%rbp), %rax
	leaq	-3368(%rbp), %rdx
	pushq	%rax
	leaq	-3376(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EESP_
	addq	$32, %rsp
	movl	$57, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$62, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3296(%rbp), %rax
	movl	$80, %edi
	movq	-3328(%rbp), %xmm0
	movq	-3344(%rbp), %xmm1
	movq	%rbx, -88(%rbp)
	movq	-3360(%rbp), %xmm2
	movhps	-3312(%rbp), %xmm0
	movq	%rax, -96(%rbp)
	movq	-3376(%rbp), %xmm3
	movaps	%xmm0, -112(%rbp)
	movhps	-3336(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movhps	-3352(%rbp), %xmm2
	movaps	%xmm1, -128(%rbp)
	movhps	-3368(%rbp), %xmm3
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm0, -3264(%rbp)
	movq	$0, -3248(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm5
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	80(%rax), %rdx
	movq	%rax, -3264(%rbp)
	movdqa	-144(%rbp), %xmm4
	movq	-3464(%rbp), %rdi
	movups	%xmm3, (%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movq	%rdx, -3248(%rbp)
	movq	%rdx, -3256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L232
	call	_ZdlPv@PLT
.L232:
	movq	-3592(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L437:
	movq	-3592(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3384(%rbp)
	movq	$0, -3376(%rbp)
	movq	$0, -3368(%rbp)
	movq	$0, -3360(%rbp)
	movq	$0, -3352(%rbp)
	movq	$0, -3344(%rbp)
	movq	$0, -3336(%rbp)
	movq	$0, -3328(%rbp)
	movq	$0, -3312(%rbp)
	movq	$0, -3296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3296(%rbp), %rax
	movq	-3464(%rbp), %rdi
	pushq	%rax
	leaq	-3312(%rbp), %rax
	leaq	-3368(%rbp), %rcx
	pushq	%rax
	leaq	-3328(%rbp), %rax
	leaq	-3352(%rbp), %r9
	pushq	%rax
	leaq	-3336(%rbp), %rax
	leaq	-3360(%rbp), %r8
	pushq	%rax
	leaq	-3344(%rbp), %rax
	leaq	-3376(%rbp), %rdx
	pushq	%rax
	leaq	-3384(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EESP_SR_
	addq	$48, %rsp
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3296(%rbp), %rbx
	movq	-3328(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$80, %edi
	movq	-3312(%rbp), %xmm1
	movq	-3336(%rbp), %xmm4
	movaps	%xmm0, -3264(%rbp)
	movq	-3352(%rbp), %xmm5
	movq	-3368(%rbp), %xmm6
	movq	-3384(%rbp), %xmm7
	movhps	-3296(%rbp), %xmm1
	movq	$0, -3248(%rbp)
	movhps	-3328(%rbp), %xmm4
	movhps	-3344(%rbp), %xmm5
	movaps	%xmm1, -3680(%rbp)
	movhps	-3360(%rbp), %xmm6
	movhps	-3376(%rbp), %xmm7
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm4, -3664(%rbp)
	movaps	%xmm5, -3648(%rbp)
	movaps	%xmm6, -3632(%rbp)
	movaps	%xmm7, -3616(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm1
	movdqa	-128(%rbp), %xmm3
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	leaq	80(%rax), %rdx
	movq	%rax, -3264(%rbp)
	movdqa	-144(%rbp), %xmm2
	movq	-3576(%rbp), %rdi
	movups	%xmm1, (%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rdx, -3248(%rbp)
	movq	%rdx, -3256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L234
	call	_ZdlPv@PLT
.L234:
	movdqa	-3616(%rbp), %xmm6
	movdqa	-3632(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movl	$80, %edi
	movdqa	-3648(%rbp), %xmm1
	movdqa	-3664(%rbp), %xmm2
	movaps	%xmm0, -3264(%rbp)
	movdqa	-3680(%rbp), %xmm3
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movq	$0, -3248(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm4
	movdqa	-128(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm5
	leaq	80(%rax), %rdx
	leaq	-928(%rbp), %rdi
	movdqa	-96(%rbp), %xmm1
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm1, 64(%rax)
	movq	%rax, -3264(%rbp)
	movq	%rdx, -3248(%rbp)
	movq	%rdx, -3256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L235
	call	_ZdlPv@PLT
.L235:
	movq	-3488(%rbp), %rcx
	movq	-3600(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1056(%rbp)
	je	.L236
.L438:
	movq	-3600(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3408(%rbp)
	movq	$0, -3400(%rbp)
	movq	$0, -3392(%rbp)
	movq	$0, -3384(%rbp)
	movq	$0, -3376(%rbp)
	movq	$0, -3368(%rbp)
	movq	$0, -3360(%rbp)
	movq	$0, -3352(%rbp)
	movq	$0, -3344(%rbp)
	movq	$0, -3336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3336(%rbp), %rax
	movq	-3576(%rbp), %rdi
	pushq	%rax
	leaq	-3344(%rbp), %rax
	leaq	-3384(%rbp), %r8
	pushq	%rax
	leaq	-3352(%rbp), %rax
	leaq	-3392(%rbp), %rcx
	pushq	%rax
	leaq	-3360(%rbp), %rax
	leaq	-3376(%rbp), %r9
	pushq	%rax
	leaq	-3368(%rbp), %rax
	leaq	-3400(%rbp), %rdx
	pushq	%rax
	leaq	-3408(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EESP_SR_
	addq	$48, %rsp
	movl	$64, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3336(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	-3384(%rbp), %rdx
	movq	-3392(%rbp), %rax
	leaq	-3328(%rbp), %r13
	movq	%rdx, -3288(%rbp)
	movq	-3376(%rbp), %rdx
	movq	%rax, -3296(%rbp)
	movq	%rdx, -3280(%rbp)
	pushq	-3280(%rbp)
	pushq	-3288(%rbp)
	pushq	%rax
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3408(%rbp), %r9
	movq	-3336(%rbp), %rcx
	movq	%r13, %rdi
	movq	-3368(%rbp), %rax
	movq	%r9, -3600(%rbp)
	movq	%rcx, -3632(%rbp)
	movq	%rax, -3616(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$712, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-3264(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	xorl	%esi, %esi
	movq	%rbx, -144(%rbp)
	movq	%rcx, -3312(%rbp)
	movl	$3, %ebx
	movq	%rax, %r8
	movq	%r13, %rdi
	leaq	-160(%rbp), %rcx
	pushq	%rbx
	movq	-3616(%rbp), %xmm0
	leaq	-3312(%rbp), %rdx
	movq	-3600(%rbp), %r9
	movq	-3248(%rbp), %rax
	pushq	%rcx
	movl	$1, %ecx
	movhps	-3632(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%rax, -3304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$67, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3336(%rbp), %rbx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$62, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3344(%rbp), %rax
	movl	$80, %edi
	movq	-3360(%rbp), %xmm0
	movq	-3376(%rbp), %xmm1
	movq	%rbx, -88(%rbp)
	movq	-3392(%rbp), %xmm2
	movhps	-3352(%rbp), %xmm0
	movq	%rax, -96(%rbp)
	movq	-3408(%rbp), %xmm3
	movaps	%xmm0, -112(%rbp)
	movhps	-3368(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movhps	-3384(%rbp), %xmm2
	movaps	%xmm1, -128(%rbp)
	movhps	-3400(%rbp), %xmm3
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm0, -3264(%rbp)
	movq	$0, -3248(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm4
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	leaq	80(%rax), %rdx
	movq	%rax, -3264(%rbp)
	movdqa	-144(%rbp), %xmm3
	movq	-3464(%rbp), %rdi
	movups	%xmm2, (%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	movq	%rdx, -3248(%rbp)
	movq	%rdx, -3256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3264(%rbp), %rdi
	popq	%r10
	popq	%r11
	testq	%rdi, %rdi
	je	.L237
	call	_ZdlPv@PLT
.L237:
	movq	-3592(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -864(%rbp)
	je	.L238
.L439:
	movq	-3488(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3392(%rbp)
	leaq	-928(%rbp), %r13
	movq	$0, -3384(%rbp)
	movq	$0, -3376(%rbp)
	movq	$0, -3368(%rbp)
	movq	$0, -3360(%rbp)
	movq	$0, -3352(%rbp)
	movq	$0, -3344(%rbp)
	movq	$0, -3336(%rbp)
	movq	$0, -3328(%rbp)
	movq	$0, -3312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r13, %rdi
	leaq	-3312(%rbp), %rax
	pushq	%rax
	leaq	-3328(%rbp), %rax
	leaq	-3376(%rbp), %rcx
	pushq	%rax
	leaq	-3336(%rbp), %rax
	leaq	-3360(%rbp), %r9
	pushq	%rax
	leaq	-3344(%rbp), %rax
	leaq	-3368(%rbp), %r8
	pushq	%rax
	leaq	-3352(%rbp), %rax
	leaq	-3384(%rbp), %rdx
	pushq	%rax
	leaq	-3392(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_SB_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EESP_SR_
	addq	$48, %rsp
	movl	$20, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$64, %edi
	movq	-3344(%rbp), %xmm0
	movq	-3360(%rbp), %xmm1
	movq	-3376(%rbp), %xmm2
	movq	-3392(%rbp), %xmm3
	movq	$0, -3248(%rbp)
	movhps	-3336(%rbp), %xmm0
	movhps	-3352(%rbp), %xmm1
	movhps	-3368(%rbp), %xmm2
	movhps	-3384(%rbp), %xmm3
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3264(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm2
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm1
	leaq	64(%rax), %rdx
	leaq	-736(%rbp), %rdi
	movups	%xmm7, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rax, -3264(%rbp)
	movq	%rdx, -3248(%rbp)
	movq	%rdx, -3256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L239
	call	_ZdlPv@PLT
.L239:
	movq	-3448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -672(%rbp)
	je	.L240
.L440:
	movq	-3448(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3392(%rbp)
	leaq	-736(%rbp), %r13
	movq	$0, -3384(%rbp)
	movq	$0, -3376(%rbp)
	movq	$0, -3368(%rbp)
	movq	$0, -3360(%rbp)
	movq	$0, -3352(%rbp)
	movq	$0, -3344(%rbp)
	movq	$0, -3336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r13, %rdi
	leaq	-3336(%rbp), %rax
	pushq	%rax
	leaq	-3344(%rbp), %rax
	leaq	-3376(%rbp), %rcx
	pushq	%rax
	leaq	-3352(%rbp), %rax
	leaq	-3360(%rbp), %r9
	pushq	%rax
	leaq	-3368(%rbp), %r8
	leaq	-3384(%rbp), %rdx
	leaq	-3392(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_7RawPtrTES5_NS0_7IntPtrTENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EEPNSD_IS9_EE
	addq	$32, %rsp
	movl	$72, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3336(%rbp), %rdx
	movq	-3344(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberAddENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$73, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal16kLengthString_68EPNS0_8compiler18CodeAssemblerStateE@PLT
	leaq	-3328(%rbp), %r10
	movq	%r14, %rsi
	movq	%r10, %rdi
	movq	%rax, -3592(%rbp)
	movq	%r10, -3616(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3616(%rbp), %r10
	movq	-3392(%rbp), %r13
	movq	-3352(%rbp), %r14
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$712, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-3616(%rbp), %r10
	movq	-3264(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-160(%rbp), %rsi
	movl	$3, %edi
	movq	-3616(%rbp), %r10
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%r14, %xmm0
	pushq	%rsi
	movhps	-3592(%rbp), %xmm0
	movq	%r13, %r9
	movl	$1, %ecx
	movq	%rax, -3312(%rbp)
	movq	-3248(%rbp), %rax
	xorl	%esi, %esi
	movq	%r10, %rdi
	leaq	-3312(%rbp), %rdx
	movaps	%xmm0, -160(%rbp)
	movq	%r10, -3592(%rbp)
	movq	%rax, -3304(%rbp)
	movq	%rbx, -144(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-3592(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$76, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3360(%rbp), %rax
	movl	$48, %edi
	movq	-3376(%rbp), %xmm0
	movq	-3392(%rbp), %xmm1
	movq	%rbx, -120(%rbp)
	movhps	-3368(%rbp), %xmm0
	movq	%rax, -128(%rbp)
	movhps	-3384(%rbp), %xmm1
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -3264(%rbp)
	movq	$0, -3248(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm4
	movq	%r15, %rsi
	movdqa	-144(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm6
	leaq	48(%rax), %rdx
	leaq	-544(%rbp), %rdi
	movq	%rax, -3264(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -3248(%rbp)
	movq	%rdx, -3256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3264(%rbp), %rdi
	popq	%r8
	popq	%r9
	testq	%rdi, %rdi
	je	.L241
	call	_ZdlPv@PLT
.L241:
	movq	-3528(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L441:
	movq	-3528(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-544(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -3248(%rbp)
	movaps	%xmm0, -3264(%rbp)
	call	_Znwm@PLT
	movl	$2053, %esi
	movq	%r13, %rdi
	movw	%si, 4(%rax)
	leaq	6(%rax), %rdx
	movq	%r15, %rsi
	movl	$84215815, (%rax)
	movq	%rax, -3264(%rbp)
	movq	%rdx, -3248(%rbp)
	movq	%rdx, -3256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3264(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L243
	call	_ZdlPv@PLT
.L243:
	movq	(%rbx), %rax
	movl	$8, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	16(%rax), %r13
	movq	(%rax), %r14
	movq	32(%rax), %rbx
	movq	%rcx, -3592(%rbp)
	movq	24(%rax), %rcx
	movq	40(%rax), %rax
	movq	%rcx, -3616(%rbp)
	movq	%rax, -3632(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %xmm0
	movl	$48, %edi
	movq	$0, -3248(%rbp)
	movhps	-3592(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%r13, %xmm0
	leaq	-352(%rbp), %r13
	movhps	-3616(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	movhps	-3632(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3264(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm2
	movq	%r13, %rdi
	movq	%r15, %rsi
	movdqa	-160(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm1
	leaq	48(%rax), %rdx
	movq	%rax, -3264(%rbp)
	movups	%xmm2, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm1, 16(%rax)
	movq	%rdx, -3248(%rbp)
	movq	%rdx, -3256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L244
	call	_ZdlPv@PLT
.L244:
	movq	-3584(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L242
.L442:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22410:
	.size	_ZN2v88internal22GenericArrayUnshift_49EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS0_21TorqueStructArgumentsE, .-_ZN2v88internal22GenericArrayUnshift_49EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS0_21TorqueStructArgumentsE
	.section	.text._ZN2v88internal30ArrayPrototypeUnshiftAssembler33GenerateArrayPrototypeUnshiftImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal30ArrayPrototypeUnshiftAssembler33GenerateArrayPrototypeUnshiftImplEv
	.type	_ZN2v88internal30ArrayPrototypeUnshiftAssembler33GenerateArrayPrototypeUnshiftImplEv, @function
_ZN2v88internal30ArrayPrototypeUnshiftAssembler33GenerateArrayPrototypeUnshiftImplEv:
.LFB22440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1928(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$2072, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, -1928(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r13, %rcx
	movq	%r12, %rsi
	leaq	-1904(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-1896(%rbp), %rcx
	movl	$2, %esi
	movq	%r12, %rdi
	movq	-1904(%rbp), %rax
	movq	-1888(%rbp), %rbx
	movq	%r12, -1840(%rbp)
	leaq	-1664(%rbp), %r12
	movq	%rcx, -2024(%rbp)
	movq	%rcx, -1816(%rbp)
	movq	$1, -1832(%rbp)
	movq	%rbx, -1824(%rbp)
	movq	%rax, -2048(%rbp)
	movq	%rax, -1808(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-1840(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -2032(%rbp)
	movq	%rax, -2016(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -1656(%rbp)
	movq	$0, -1648(%rbp)
	movq	%rax, %r13
	movq	-1928(%rbp), %rax
	movq	$0, -1640(%rbp)
	movq	%rax, -1664(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1608(%rbp), %rcx
	movq	%r15, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -1640(%rbp)
	movq	%rdx, -1648(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1944(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1624(%rbp)
	movq	%rax, -1656(%rbp)
	movq	$0, -1632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1928(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1464(%rbp)
	movq	$0, -1456(%rbp)
	movq	%rax, -1472(%rbp)
	movq	$0, -1448(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1416(%rbp), %rcx
	movq	%r15, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rdx, -1448(%rbp)
	movq	%rdx, -1456(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1992(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1432(%rbp)
	movq	%rax, -1464(%rbp)
	movq	$0, -1440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1928(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1272(%rbp)
	movq	$0, -1264(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -1256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1224(%rbp), %rcx
	movq	%r15, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -1256(%rbp)
	movq	%rdx, -1264(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1960(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1240(%rbp)
	movq	%rax, -1272(%rbp)
	movq	$0, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1928(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1080(%rbp)
	movq	$0, -1072(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$0, -1064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1032(%rbp), %rcx
	movq	%r15, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -1064(%rbp)
	movq	%rdx, -1072(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1968(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1048(%rbp)
	movq	%rax, -1080(%rbp)
	movq	$0, -1056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1928(%rbp), %rax
	movl	$168, %edi
	movq	$0, -888(%rbp)
	movq	$0, -880(%rbp)
	movq	%rax, -896(%rbp)
	movq	$0, -872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-840(%rbp), %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -872(%rbp)
	movq	%rdx, -880(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -1976(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -856(%rbp)
	movq	%rax, -888(%rbp)
	movq	$0, -864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1928(%rbp), %rax
	movl	$192, %edi
	movq	$0, -696(%rbp)
	movq	$0, -688(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-648(%rbp), %rcx
	movq	%r15, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -680(%rbp)
	movq	%rdx, -688(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -2000(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -664(%rbp)
	movq	%rax, -696(%rbp)
	movq	$0, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1928(%rbp), %rax
	movl	$192, %edi
	movq	$0, -504(%rbp)
	movq	$0, -496(%rbp)
	movq	%rax, -512(%rbp)
	movq	$0, -488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-456(%rbp), %rsi
	xorl	%ecx, %ecx
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -488(%rbp)
	movq	%rdx, -496(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -1984(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -472(%rbp)
	movq	%rax, -504(%rbp)
	movq	$0, -480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1928(%rbp), %rax
	movl	$120, %edi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-264(%rbp), %rcx
	movq	%r15, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1952(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -312(%rbp)
	movups	%xmm0, -280(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	-2048(%rbp), %xmm1
	movq	%r13, -96(%rbp)
	leaq	-1792(%rbp), %r13
	movhps	-2024(%rbp), %xmm1
	movaps	%xmm0, -1792(%rbp)
	movaps	%xmm1, -128(%rbp)
	movq	%rbx, %xmm1
	movhps	-2016(%rbp), %xmm1
	movq	$0, -1776(%rbp)
	movaps	%xmm1, -112(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm2
	movdqa	-112(%rbp), %xmm3
	leaq	40(%rax), %rdx
	movq	%rax, -1792(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L444
	call	_ZdlPv@PLT
.L444:
	movq	-1944(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1600(%rbp)
	jne	.L641
	cmpq	$0, -1408(%rbp)
	jne	.L642
.L450:
	cmpq	$0, -1216(%rbp)
	jne	.L643
.L453:
	cmpq	$0, -1024(%rbp)
	jne	.L644
.L457:
	cmpq	$0, -832(%rbp)
	jne	.L645
.L460:
	cmpq	$0, -640(%rbp)
	jne	.L646
.L465:
	cmpq	$0, -448(%rbp)
	jne	.L647
.L468:
	cmpq	$0, -256(%rbp)
	jne	.L648
.L471:
	movq	-1952(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L475
	call	_ZdlPv@PLT
.L475:
	movq	-304(%rbp), %rbx
	movq	-312(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L476
	.p2align 4,,10
	.p2align 3
.L480:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L477
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L480
.L478:
	movq	-312(%rbp), %r12
.L476:
	testq	%r12, %r12
	je	.L481
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L481:
	movq	-1984(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L482
	call	_ZdlPv@PLT
.L482:
	movq	-496(%rbp), %rbx
	movq	-504(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L483
	.p2align 4,,10
	.p2align 3
.L487:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L484
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L487
.L485:
	movq	-504(%rbp), %r12
.L483:
	testq	%r12, %r12
	je	.L488
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L488:
	movq	-2000(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L489
	call	_ZdlPv@PLT
.L489:
	movq	-688(%rbp), %rbx
	movq	-696(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L490
	.p2align 4,,10
	.p2align 3
.L494:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L491
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L494
.L492:
	movq	-696(%rbp), %r12
.L490:
	testq	%r12, %r12
	je	.L495
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L495:
	movq	-1976(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-864(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L496
	call	_ZdlPv@PLT
.L496:
	movq	-880(%rbp), %rbx
	movq	-888(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L497
	.p2align 4,,10
	.p2align 3
.L501:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L498
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L501
.L499:
	movq	-888(%rbp), %r12
.L497:
	testq	%r12, %r12
	je	.L502
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L502:
	movq	-1968(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L503
	call	_ZdlPv@PLT
.L503:
	movq	-1072(%rbp), %rbx
	movq	-1080(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L504
	.p2align 4,,10
	.p2align 3
.L508:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L505
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L508
.L506:
	movq	-1080(%rbp), %r12
.L504:
	testq	%r12, %r12
	je	.L509
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L509:
	movq	-1960(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L510
	call	_ZdlPv@PLT
.L510:
	movq	-1264(%rbp), %rbx
	movq	-1272(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L511
	.p2align 4,,10
	.p2align 3
.L515:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L512
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L515
.L513:
	movq	-1272(%rbp), %r12
.L511:
	testq	%r12, %r12
	je	.L516
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L516:
	movq	-1992(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1440(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L517
	call	_ZdlPv@PLT
.L517:
	movq	-1456(%rbp), %rbx
	movq	-1464(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L518
	.p2align 4,,10
	.p2align 3
.L522:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L519
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L522
.L520:
	movq	-1464(%rbp), %r12
.L518:
	testq	%r12, %r12
	je	.L523
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L523:
	movq	-1944(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1632(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L524
	call	_ZdlPv@PLT
.L524:
	movq	-1648(%rbp), %rbx
	movq	-1656(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L525
	.p2align 4,,10
	.p2align 3
.L529:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L526
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L529
.L527:
	movq	-1656(%rbp), %r12
.L525:
	testq	%r12, %r12
	je	.L530
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L530:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L649
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L526:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L529
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L519:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L522
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L512:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L515
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L505:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L508
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L498:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L501
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L491:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L494
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L477:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L480
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L484:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L487
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L641:
	movq	-1944(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1776(%rbp)
	movaps	%xmm0, -1792(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -1792(%rbp)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1792(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L446
	call	_ZdlPv@PLT
.L446:
	movq	(%rbx), %rax
	movl	$83, %edx
	movq	%r15, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rsi
	movq	24(%rax), %r12
	movq	32(%rax), %rbx
	movq	%rcx, -2024(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -2016(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rcx, -2048(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rsi
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal24Cast13ATFastJSArray_1429EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %xmm2
	movq	%rbx, %xmm4
	movq	-2048(%rbp), %xmm5
	punpcklqdq	%xmm4, %xmm4
	pxor	%xmm0, %xmm0
	movq	%rax, -80(%rbp)
	movq	-2016(%rbp), %xmm6
	punpcklqdq	%xmm2, %xmm5
	movl	$56, %edi
	movaps	%xmm4, -96(%rbp)
	leaq	-1872(%rbp), %r12
	movhps	-2024(%rbp), %xmm6
	movaps	%xmm4, -2064(%rbp)
	movaps	%xmm5, -2048(%rbp)
	movaps	%xmm6, -2016(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm0, -1872(%rbp)
	movq	$0, -1856(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm3
	movq	-80(%rbp), %rcx
	leaq	-1280(%rbp), %rdi
	movdqa	-112(%rbp), %xmm2
	leaq	56(%rax), %rdx
	movq	%r12, %rsi
	movq	%rax, -1872(%rbp)
	movups	%xmm3, (%rax)
	movdqa	-96(%rbp), %xmm3
	movq	%rcx, 48(%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm3, 32(%rax)
	movq	%rdx, -1856(%rbp)
	movq	%rdx, -1864(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1872(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L447
	call	_ZdlPv@PLT
.L447:
	movq	-1960(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1784(%rbp)
	jne	.L650
.L448:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1408(%rbp)
	je	.L450
.L642:
	movq	-1992(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-1472(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -1776(%rbp)
	movaps	%xmm0, -1792(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r8d
	movq	%r12, %rdi
	movq	%r13, %rsi
	movl	$117769477, (%rax)
	leaq	6(%rax), %rdx
	movw	%r8w, 4(%rax)
	movq	%rax, -1792(%rbp)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1792(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L451
	call	_ZdlPv@PLT
.L451:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	32(%rax), %rdx
	movdqu	(%rax), %xmm4
	movq	$0, -1776(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm0, -1792(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm6
	leaq	40(%rax), %rdx
	leaq	-320(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L452
	call	_ZdlPv@PLT
.L452:
	movq	-1952(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1216(%rbp)
	je	.L453
.L643:
	movq	-1960(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-1280(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -1776(%rbp)
	movaps	%xmm0, -1792(%rbp)
	call	_Znwm@PLT
	movl	$2056, %edi
	movq	%r13, %rsi
	movw	%di, 4(%rax)
	leaq	7(%rax), %rdx
	movq	%r12, %rdi
	movl	$117769477, (%rax)
	movb	$7, 6(%rax)
	movq	%rax, -1792(%rbp)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1792(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L454
	call	_ZdlPv@PLT
.L454:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rsi
	movq	32(%rax), %rdx
	movq	24(%rax), %rbx
	movq	48(%rax), %r12
	movq	%rcx, -2024(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -2016(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rdx, -2080(%rbp)
	movl	$84, %edx
	movq	%rcx, -2048(%rbp)
	movq	%rbx, -2064(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal30EnsureWriteableFastElements_50EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEE@PLT
	movl	$86, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE(%rip), %esi
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$87, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler15IsExtensibleMapENS0_8compiler11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -2088(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2088(%rbp), %r8
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler15Word32BinaryNotENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -2088(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %xmm2
	pxor	%xmm0, %xmm0
	movq	-2080(%rbp), %xmm7
	movq	-2016(%rbp), %xmm3
	movl	$56, %edi
	movaps	%xmm0, -1792(%rbp)
	punpcklqdq	%xmm2, %xmm7
	movq	%rbx, -80(%rbp)
	movq	-2048(%rbp), %xmm2
	movhps	-2024(%rbp), %xmm3
	movaps	%xmm7, -96(%rbp)
	movhps	-2064(%rbp), %xmm2
	movaps	%xmm7, -2080(%rbp)
	movaps	%xmm2, -2048(%rbp)
	movaps	%xmm3, -2016(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm4
	leaq	56(%rax), %rdx
	leaq	-1088(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm4, 32(%rax)
	movq	%rax, -1792(%rbp)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L455
	call	_ZdlPv@PLT
.L455:
	movdqa	-2016(%rbp), %xmm5
	movdqa	-2048(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movdqa	-2080(%rbp), %xmm7
	movq	%rbx, -80(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm0, -1792(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm2
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm4
	leaq	56(%rax), %rdx
	leaq	-896(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm4, 32(%rax)
	movq	%rax, -1792(%rbp)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L456
	call	_ZdlPv@PLT
.L456:
	movq	-1976(%rbp), %rcx
	movq	-1968(%rbp), %rdx
	movq	%r15, %rdi
	movq	-2088(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1024(%rbp)
	je	.L457
.L644:
	movq	-1968(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-1088(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -1776(%rbp)
	movaps	%xmm0, -1792(%rbp)
	call	_Znwm@PLT
	movl	$1800, %esi
	movq	%r12, %rdi
	movw	%si, 4(%rax)
	leaq	7(%rax), %rdx
	movq	%r13, %rsi
	movl	$117769477, (%rax)
	movb	$7, 6(%rax)
	movq	%rax, -1792(%rbp)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1792(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L458
	call	_ZdlPv@PLT
.L458:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	32(%rax), %rdx
	movdqu	(%rax), %xmm5
	movq	$0, -1776(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm0, -1792(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	leaq	40(%rax), %rdx
	leaq	-320(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L459
	call	_ZdlPv@PLT
.L459:
	movq	-1952(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -832(%rbp)
	je	.L460
.L645:
	movq	-1976(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-896(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -1776(%rbp)
	movaps	%xmm0, -1792(%rbp)
	call	_Znwm@PLT
	movl	$1800, %ecx
	movq	%r12, %rdi
	movq	%r13, %rsi
	movl	$117769477, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -1792(%rbp)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1792(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L461
	call	_ZdlPv@PLT
.L461:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	leaq	-1872(%rbp), %r12
	movq	8(%rax), %rcx
	movq	24(%rax), %rbx
	movq	(%rax), %rsi
	movq	40(%rax), %rdx
	movq	%rcx, -2024(%rbp)
	movq	16(%rax), %rcx
	movq	%rbx, -2064(%rbp)
	movq	32(%rax), %rbx
	movq	%rsi, -2016(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rdx, -2088(%rbp)
	movl	$88, %edx
	movq	%rcx, -2048(%rbp)
	movq	%rbx, -2080(%rbp)
	movq	48(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler25EnsureArrayLengthWritableENS0_8compiler5TNodeINS0_3MapEEEPNS2_18CodeAssemblerLabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm4
	pxor	%xmm0, %xmm0
	movq	-2080(%rbp), %xmm5
	punpcklqdq	%xmm4, %xmm4
	movl	$64, %edi
	movq	-2048(%rbp), %xmm6
	movq	-2016(%rbp), %xmm7
	movhps	-2088(%rbp), %xmm5
	movaps	%xmm4, -2112(%rbp)
	movhps	-2064(%rbp), %xmm6
	movhps	-2024(%rbp), %xmm7
	movaps	%xmm5, -2080(%rbp)
	movaps	%xmm6, -2048(%rbp)
	movaps	%xmm7, -2016(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm0, -1872(%rbp)
	movq	$0, -1856(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm2
	movdqa	-112(%rbp), %xmm3
	movq	%r12, %rsi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	leaq	64(%rax), %rdx
	leaq	-512(%rbp), %rdi
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rax, -1872(%rbp)
	movq	%rdx, -1856(%rbp)
	movq	%rdx, -1864(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1872(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L462
	call	_ZdlPv@PLT
.L462:
	movq	-1984(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1784(%rbp)
	jne	.L651
.L463:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -640(%rbp)
	je	.L465
.L646:
	movq	-2000(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-704(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -1776(%rbp)
	movaps	%xmm0, -1792(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movabsq	$506381214161372421, %rsi
	movq	%rsi, (%rax)
	leaq	8(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -1792(%rbp)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1792(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L466
	call	_ZdlPv@PLT
.L466:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	32(%rax), %rdx
	movdqu	(%rax), %xmm6
	movq	$0, -1776(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm0, -1792(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm2
	leaq	40(%rax), %rdx
	leaq	-320(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L467
	call	_ZdlPv@PLT
.L467:
	movq	-1952(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -448(%rbp)
	je	.L468
.L647:
	movq	-1984(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-512(%rbp), %rbx
	xorl	%r12d, %r12d
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -1776(%rbp)
	movaps	%xmm0, -1792(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movabsq	$506381214161372421, %rsi
	movq	%rsi, (%rax)
	leaq	8(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -1792(%rbp)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1792(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L469
	call	_ZdlPv@PLT
.L469:
	movq	(%rbx), %rax
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	movq	16(%rax), %rdx
	movq	24(%rax), %rcx
	testq	%rdx, %rdx
	movq	%rcx, -2048(%rbp)
	cmovne	%rdx, %r12
	movl	$91, %edx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal23LoadTargetFromFrame_304EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	movq	%rax, -2024(%rbp)
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$92, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -2016(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-1920(%rbp), %r12
	call	_ZN2v88internal28Convert7ATint328ATintptr_183EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movl	$90, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$209, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-1792(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-128(%rbp), %r8
	movq	%r12, %rdi
	movq	%rbx, -112(%rbp)
	movq	%rax, %rdx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-2024(%rbp), %xmm0
	movq	-2048(%rbp), %rcx
	movq	%rax, -1872(%rbp)
	movq	-1776(%rbp), %rax
	leaq	-1872(%rbp), %rsi
	movl	$3, %r9d
	movhps	-2016(%rbp), %xmm0
	movq	%rax, -1864(%rbp)
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler16TailCallStubImplERKNS0_23CallInterfaceDescriptorENS1_5TNodeINS0_4CodeEEENS6_INS0_6ObjectEEESt16initializer_listIPNS1_4NodeEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -256(%rbp)
	je	.L471
.L648:
	movq	-1952(%rbp), %rsi
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	leaq	-320(%rbp), %r8
	movq	%r8, -2016(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1776(%rbp)
	movaps	%xmm0, -1792(%rbp)
	call	_Znwm@PLT
	movq	-2016(%rbp), %r8
	movq	%r13, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%r8, %rdi
	movq	%rax, -1792(%rbp)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1792(%rbp), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L472
	call	_ZdlPv@PLT
.L472:
	movq	0(%r13), %rax
	movq	%r15, %rdi
	leaq	.LC1(%rip), %rsi
	movq	24(%rax), %rdx
	movq	(%rax), %xmm0
	movq	16(%rax), %r13
	testq	%rdx, %rdx
	movhps	8(%rax), %xmm0
	cmovne	%rdx, %r12
	movq	32(%rax), %rdx
	movaps	%xmm0, -2016(%rbp)
	testq	%rdx, %rdx
	cmovne	%rdx, %rbx
	movl	$95, %edx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqa	-2016(%rbp), %xmm0
	movq	%r13, -1776(%rbp)
	movq	%rbx, %rdx
	pushq	-1776(%rbp)
	movaps	%xmm0, -1792(%rbp)
	pushq	-1784(%rbp)
	pushq	-1792(%rbp)
	call	_ZN2v88internal22GenericArrayUnshift_49EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS0_21TorqueStructArgumentsE
	movq	-2032(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L650:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movdqa	-2016(%rbp), %xmm3
	movaps	%xmm0, -1872(%rbp)
	movaps	%xmm3, -128(%rbp)
	movdqa	-2048(%rbp), %xmm3
	movq	$0, -1856(%rbp)
	movaps	%xmm3, -112(%rbp)
	movdqa	-2064(%rbp), %xmm3
	movaps	%xmm3, -96(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movq	%r12, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	leaq	48(%rax), %rdx
	leaq	-1472(%rbp), %rdi
	movq	%rax, -1872(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -1856(%rbp)
	movq	%rdx, -1864(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1872(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L449
	call	_ZdlPv@PLT
.L449:
	movq	-1992(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L651:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-2016(%rbp), %xmm7
	movdqa	-2048(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movdqa	-2080(%rbp), %xmm2
	movdqa	-2112(%rbp), %xmm4
	movl	$64, %edi
	movaps	%xmm0, -1872(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movq	$0, -1856(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm6
	movq	%r12, %rsi
	movdqa	-96(%rbp), %xmm7
	movdqa	-80(%rbp), %xmm3
	leaq	64(%rax), %rdx
	leaq	-704(%rbp), %rdi
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rax, -1872(%rbp)
	movq	%rdx, -1856(%rbp)
	movq	%rdx, -1864(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1872(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L464
	call	_ZdlPv@PLT
.L464:
	movq	-2000(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L463
.L649:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22440:
	.size	_ZN2v88internal30ArrayPrototypeUnshiftAssembler33GenerateArrayPrototypeUnshiftImplEv, .-_ZN2v88internal30ArrayPrototypeUnshiftAssembler33GenerateArrayPrototypeUnshiftImplEv
	.section	.rodata._ZN2v88internal8Builtins30Generate_ArrayPrototypeUnshiftEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/array-unshift-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins30Generate_ArrayPrototypeUnshiftEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"ArrayPrototypeUnshift"
	.section	.text._ZN2v88internal8Builtins30Generate_ArrayPrototypeUnshiftEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins30Generate_ArrayPrototypeUnshiftEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins30Generate_ArrayPrototypeUnshiftEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins30Generate_ArrayPrototypeUnshiftEPNS0_8compiler18CodeAssemblerStateE:
.LFB22436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$512, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$784, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L656
.L653:
	movq	%r13, %rdi
	call	_ZN2v88internal30ArrayPrototypeUnshiftAssembler33GenerateArrayPrototypeUnshiftImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L657
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L656:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L653
.L657:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22436:
	.size	_ZN2v88internal8Builtins30Generate_ArrayPrototypeUnshiftEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins30Generate_ArrayPrototypeUnshiftEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal22GenericArrayUnshift_49EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS0_21TorqueStructArgumentsE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal22GenericArrayUnshift_49EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS0_21TorqueStructArgumentsE, @function
_GLOBAL__sub_I__ZN2v88internal22GenericArrayUnshift_49EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS0_21TorqueStructArgumentsE:
.LFB29133:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29133:
	.size	_GLOBAL__sub_I__ZN2v88internal22GenericArrayUnshift_49EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS0_21TorqueStructArgumentsE, .-_GLOBAL__sub_I__ZN2v88internal22GenericArrayUnshift_49EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS0_21TorqueStructArgumentsE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal22GenericArrayUnshift_49EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS0_21TorqueStructArgumentsE
	.weak	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	4294967295
	.long	1128267775
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
