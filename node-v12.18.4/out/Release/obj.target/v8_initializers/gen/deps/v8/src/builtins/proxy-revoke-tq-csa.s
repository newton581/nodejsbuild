	.file	"proxy-revoke-tq-csa.cc"
	.text
	.section	.rodata._ZN2v88internal23UnsafeCast7JSProxy_1466EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../../deps/v8/../../deps/v8/src/builtins/base.tq"
	.align 8
.LC1:
	.string	"../../deps/v8/../../deps/v8/src/builtins/proxy-revoke.tq"
	.section	.text._ZN2v88internal23UnsafeCast7JSProxy_1466EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23UnsafeCast7JSProxy_1466EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal23UnsafeCast7JSProxy_1466EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal23UnsafeCast7JSProxy_1466EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE:
.LFB22428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-216(%rbp), %r14
	leaq	-688(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-600(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-696(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-656(%rbp), %rbx
	subq	$696, %rsp
	movq	%rsi, -736(%rbp)
	movq	%rdx, -720(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -696(%rbp)
	movq	%rdi, -656(%rbp)
	movl	$48, %edi
	movq	$0, -648(%rbp)
	movq	$0, -640(%rbp)
	movq	$0, -632(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -632(%rbp)
	movq	%rdx, -640(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -616(%rbp)
	movq	%rax, -648(%rbp)
	movq	$0, -624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	%rax, -464(%rbp)
	movq	$0, -440(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -456(%rbp)
	leaq	-408(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -440(%rbp)
	movq	%rdx, -448(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -424(%rbp)
	movq	%rax, -712(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	%rax, -272(%rbp)
	movq	$0, -248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -248(%rbp)
	movq	%rdx, -256(%rbp)
	xorl	%edx, %edx
	movq	%rax, -264(%rbp)
	movups	%xmm0, -232(%rbp)
	movq	$0, -240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-736(%rbp), %xmm1
	movaps	%xmm0, -688(%rbp)
	movhps	-720(%rbp), %xmm1
	movq	$0, -672(%rbp)
	movaps	%xmm1, -736(%rbp)
	call	_Znwm@PLT
	movdqa	-736(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -688(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2
	call	_ZdlPv@PLT
.L2:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -592(%rbp)
	jne	.L71
.L3:
	cmpq	$0, -400(%rbp)
	leaq	-272(%rbp), %rbx
	jne	.L72
.L6:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L9
	call	_ZdlPv@PLT
.L9:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	16(%rax), %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	movq	-256(%rbp), %rbx
	movq	-264(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L11
	.p2align 4,,10
	.p2align 3
.L15:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L12
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L15
.L13:
	movq	-264(%rbp), %r14
.L11:
	testq	%r14, %r14
	je	.L16
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L16:
	movq	-712(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L17
	call	_ZdlPv@PLT
.L17:
	movq	-448(%rbp), %rbx
	movq	-456(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L18
	.p2align 4,,10
	.p2align 3
.L22:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L19
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L22
.L20:
	movq	-456(%rbp), %r14
.L18:
	testq	%r14, %r14
	je	.L23
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L23:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L24
	call	_ZdlPv@PLT
.L24:
	movq	-640(%rbp), %rbx
	movq	-648(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L25
	.p2align 4,,10
	.p2align 3
.L29:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L26
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L29
.L27:
	movq	-648(%rbp), %r13
.L25:
	testq	%r13, %r13
	je	.L30
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L30:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L73
	addq	$696, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L29
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L12:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L15
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L19:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L22
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L71:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r15, %rsi
	movw	%di, (%rax)
	leaq	2(%rax), %rdx
	movq	%rbx, %rdi
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L4
	call	_ZdlPv@PLT
.L4:
	movq	(%rbx), %rax
	movl	$2790, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	%rcx, -736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm2
	movl	$24, %edi
	movq	-736(%rbp), %xmm0
	movq	%rbx, -64(%rbp)
	movq	$0, -672(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm3
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-464(%rbp), %rdi
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5
	call	_ZdlPv@PLT
.L5:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L72:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-464(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rsi
	movb	$7, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L7
	call	_ZdlPv@PLT
.L7:
	movq	(%rbx), %rax
	movl	$25, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -720(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rcx, -736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %edi
	movq	%rbx, -64(%rbp)
	movq	-736(%rbp), %xmm0
	movq	$0, -672(%rbp)
	leaq	-272(%rbp), %rbx
	movhps	-720(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movdqa	-80(%rbp), %xmm4
	leaq	24(%rax), %rdx
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L8
	call	_ZdlPv@PLT
.L8:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L6
.L73:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22428:
	.size	_ZN2v88internal23UnsafeCast7JSProxy_1466EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal23UnsafeCast7JSProxy_1466EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal20ProxyRevokeAssembler23GenerateProxyRevokeImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20ProxyRevokeAssembler23GenerateProxyRevokeImplEv
	.type	_ZN2v88internal20ProxyRevokeAssembler23GenerateProxyRevokeImplEv, @function
_ZN2v88internal20ProxyRevokeAssembler23GenerateProxyRevokeImplEv:
.LFB22418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-568(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-664(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$680, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, -664(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$24, %edi
	movq	$0, -616(%rbp)
	movq	$0, -608(%rbp)
	movq	%rax, %r12
	movq	-664(%rbp), %rax
	movq	$0, -600(%rbp)
	movq	%rax, -624(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r15, %rdi
	movq	$0, 16(%rax)
	movq	%rax, -616(%rbp)
	leaq	-624(%rbp), %rax
	movq	%rdx, -600(%rbp)
	movq	%rdx, -608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -584(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$48, %edi
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	%rax, -432(%rbp)
	movq	$0, -408(%rbp)
	call	_Znwm@PLT
	leaq	-376(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movq	%rcx, %rbx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movq	%rbx, %rdi
	leaq	-184(%rbp), %rbx
	movups	%xmm0, 32(%rax)
	movq	%rdx, -408(%rbp)
	movq	%rdx, -416(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -680(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -392(%rbp)
	movq	%rax, -424(%rbp)
	movq	$0, -400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-704(%rbp), %rdi
	movq	%r12, (%rax)
	leaq	-656(%rbp), %r12
	leaq	8(%rax), %rdx
	movq	%r12, %rsi
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L75
	call	_ZdlPv@PLT
.L75:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -560(%rbp)
	jne	.L146
	cmpq	$0, -368(%rbp)
	jne	.L147
.L80:
	cmpq	$0, -176(%rbp)
	jne	.L148
.L82:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L85
	call	_ZdlPv@PLT
.L85:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L86
	.p2align 4,,10
	.p2align 3
.L90:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L87
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L90
.L88:
	movq	-232(%rbp), %r12
.L86:
	testq	%r12, %r12
	je	.L91
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L91:
	movq	-680(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L92
	call	_ZdlPv@PLT
.L92:
	movq	-416(%rbp), %rbx
	movq	-424(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L93
	.p2align 4,,10
	.p2align 3
.L97:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L94
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L97
.L95:
	movq	-424(%rbp), %r12
.L93:
	testq	%r12, %r12
	je	.L98
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L98:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L99
	call	_ZdlPv@PLT
.L99:
	movq	-608(%rbp), %rbx
	movq	-616(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L100
	.p2align 4,,10
	.p2align 3
.L104:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L101
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L104
.L102:
	movq	-616(%rbp), %r12
.L100:
	testq	%r12, %r12
	je	.L105
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L105:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L149
	addq	$680, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L104
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L87:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L90
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L94:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L97
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L146:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-704(%rbp), %rdi
	movq	%r12, %rsi
	movb	$7, (%rax)
	leaq	1(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L77
	movq	%rax, -704(%rbp)
	call	_ZdlPv@PLT
	movq	-704(%rbp), %rax
.L77:
	movq	(%rax), %rax
	movl	$14, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	movq	(%rax), %rax
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$4, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal57FromConstexpr13ATContextSlot23ATconstexpr_ContextSlot_171EPNS0_8compiler18CodeAssemblerStateEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -688(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-688(%rbp), %rdx
	movq	-704(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_7IntPtrTEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -688(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$17, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal7Null_63EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -712(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-712(%rbp), %rdx
	movq	-688(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, -712(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-704(%rbp), %xmm1
	movaps	%xmm0, -656(%rbp)
	movhps	-688(%rbp), %xmm1
	movq	$0, -640(%rbp)
	movaps	%xmm1, -704(%rbp)
	call	_Znwm@PLT
	movdqa	-704(%rbp), %xmm2
	leaq	-432(%rbp), %rdi
	movq	%r12, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -656(%rbp)
	movups	%xmm2, (%rax)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L78
	call	_ZdlPv@PLT
.L78:
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movdqa	-704(%rbp), %xmm3
	leaq	-240(%rbp), %rdi
	movq	%r12, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -656(%rbp)
	movups	%xmm3, (%rax)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L79
	call	_ZdlPv@PLT
.L79:
	movq	-680(%rbp), %rdx
	movq	-712(%rbp), %rsi
	movq	%rbx, %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -368(%rbp)
	je	.L80
.L147:
	movq	-680(%rbp), %rsi
	leaq	-432(%rbp), %r8
	movq	%r13, %rdi
	movq	%r8, -704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	-704(%rbp), %r8
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r12, %rsi
	movq	%r8, %rdi
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L81
	call	_ZdlPv@PLT
.L81:
	movl	$18, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-704(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -176(%rbp)
	je	.L82
.L148:
	leaq	-240(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	$0, -704(%rbp)
	movq	%r8, -688(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	-688(%rbp), %r8
	movq	%r12, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%r8, %rdi
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L83
	movq	%rax, -688(%rbp)
	call	_ZdlPv@PLT
	movq	-688(%rbp), %rax
.L83:
	movq	(%rax), %rax
	movl	$22, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	movq	(%rax), %r8
	movq	8(%rax), %rax
	testq	%rax, %rax
	cmove	-704(%rbp), %rax
	movq	%r8, -720(%rbp)
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal7Null_63EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	movl	$4, %esi
	movq	%rax, -712(%rbp)
	call	_ZN2v88internal57FromConstexpr13ATContextSlot23ATconstexpr_ContextSlot_171EPNS0_8compiler18CodeAssemblerStateEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -688(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-720(%rbp), %r8
	movq	-712(%rbp), %rcx
	movq	%r12, %rdi
	movq	-688(%rbp), %rdx
	movq	%r8, %rsi
	movq	%r8, -688(%rbp)
	call	_ZN2v88internal17CodeStubAssembler19StoreContextElementENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_7IntPtrTEEENS3_INS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rdi
	movl	$25, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-688(%rbp), %r8
	movq	-704(%rbp), %rdx
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal23UnsafeCast7JSProxy_1466EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	movl	$28, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, -712(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdi
	movq	%rax, -688(%rbp)
	call	_ZN2v88internal7Null_63EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-688(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-712(%rbp), %r10
	movl	$2, %r9d
	movq	%r12, %rdi
	movq	-704(%rbp), %r8
	movq	%rax, %rcx
	movl	$7, %esi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler13StoreToObjectENS0_21MachineRepresentationENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEEPNS1_4NodeENS0_25StoreToObjectWriteBarrierE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$31, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rdi
	movq	%rax, -688(%rbp)
	call	_ZN2v88internal7Null_63EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-688(%rbp), %r9
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-712(%rbp), %r10
	movl	$2, %r9d
	movq	%r12, %rdi
	movq	-704(%rbp), %r8
	movq	%rax, %rcx
	movl	$7, %esi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler13StoreToObjectENS0_21MachineRepresentationENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEEPNS1_4NodeENS0_25StoreToObjectWriteBarrierE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$34, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-704(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L82
.L149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22418:
	.size	_ZN2v88internal20ProxyRevokeAssembler23GenerateProxyRevokeImplEv, .-_ZN2v88internal20ProxyRevokeAssembler23GenerateProxyRevokeImplEv
	.section	.rodata._ZN2v88internal8Builtins20Generate_ProxyRevokeEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/proxy-revoke-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins20Generate_ProxyRevokeEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"ProxyRevoke"
	.section	.text._ZN2v88internal8Builtins20Generate_ProxyRevokeEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins20Generate_ProxyRevokeEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins20Generate_ProxyRevokeEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins20Generate_ProxyRevokeEPNS0_8compiler18CodeAssemblerStateE:
.LFB22414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$146, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$858, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L154
.L151:
	movq	%r13, %rdi
	call	_ZN2v88internal20ProxyRevokeAssembler23GenerateProxyRevokeImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L155
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L151
.L155:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22414:
	.size	_ZN2v88internal8Builtins20Generate_ProxyRevokeEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins20Generate_ProxyRevokeEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins20Generate_ProxyRevokeEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins20Generate_ProxyRevokeEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins20Generate_ProxyRevokeEPNS0_8compiler18CodeAssemblerStateE:
.LFB28902:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28902:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins20Generate_ProxyRevokeEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins20Generate_ProxyRevokeEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins20Generate_ProxyRevokeEPNS0_8compiler18CodeAssemblerStateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
