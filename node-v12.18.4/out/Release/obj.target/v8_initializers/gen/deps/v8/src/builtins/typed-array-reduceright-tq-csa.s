	.file	"typed-array-reduceright-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0:
.LFB29626:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	%rsi, %r13
	pushq	%rbx
	movq	%r13, %rax
	sarq	$3, %rax
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%r13, %r13
	js	.L14
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%rdx, %r14
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.L9
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%rax, %rdi
.L9:
	leaq	(%rdi,%r13), %r15
	movq	%rdi, (%rbx)
	movq	%r15, 16(%rbx)
	cmpq	%r12, %r14
	je	.L10
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
.L10:
	movq	%r15, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29626:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0:
.LFB29625:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	subq	%rsi, %rdx
	js	.L21
	movq	%rdi, %rbx
	je	.L17
	movq	%rdx, %rdi
	movq	%rdx, %r12
	movq	%rsi, %r13
	call	_Znwm@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	leaq	(%rax,%r12), %r14
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	%r14, 16(%rbx)
	call	memcpy@PLT
	movq	%r14, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	xorl	%r14d, %r14d
	movq	$0, (%rdi)
	movq	$0, 16(%rdi)
	movq	%r14, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29625:
	.size	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0:
.LFB29624:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	movabsq	$384307168202282325, %rax
	cmpq	%rax, %rdx
	ja	.L39
	pxor	%xmm0, %xmm0
	leaq	(%rdx,%rdx,2), %r14
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	%rdx, %r12
	movups	%xmm0, 8(%rdi)
	salq	$3, %r14
	xorl	%esi, %esi
	movq	$0, 24(%rdi)
	testq	%rdx, %rdx
	je	.L28
	movq	%r14, %rdi
	call	_Znwm@PLT
	leaq	-1(%r12), %rdx
	leaq	(%rax,%r14), %rsi
	movq	%rax, 8(%rbx)
	movq	%rsi, 24(%rbx)
	cmpq	$2, %rdx
	jbe	.L29
	movq	%r12, %rdi
	movq	%r12, %rcx
	pxor	%xmm0, %xmm0
	movq	%rax, %rdx
	shrq	%rdi
	andq	$-2, %rcx
	addq	%rdi, %rcx
	salq	$4, %rcx
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L26:
	movups	%xmm0, (%rdx)
	addq	$48, %rdx
	movups	%xmm0, -32(%rdx)
	movups	%xmm0, -16(%rdx)
	cmpq	%rcx, %rdx
	jne	.L26
	movq	%r12, %rdx
	movq	%r12, %rcx
	andq	$-2, %rdx
	andl	$1, %ecx
	leaq	(%rdx,%rdx,2), %rdi
	leaq	(%rax,%rdi,8), %rax
	cmpq	%rdx, %r12
	je	.L28
.L25:
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	cmpq	$1, %rcx
	je	.L28
	movq	$0, 40(%rax)
	movups	%xmm0, 24(%rax)
	cmpq	$2, %rcx
	je	.L28
	movq	$0, 64(%rax)
	movups	%xmm0, 48(%rax)
.L28:
	movq	%rsi, 16(%rbx)
	pxor	%xmm0, %xmm0
	leaq	56(%rbx), %rdi
	movq	%r13, %rsi
	movups	%xmm0, 32(%rbx)
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	$0, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	%r12, %rcx
	jmp	.L25
.L39:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29624:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev,"axG",@progbits,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev:
.LFB22413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$56, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L41
	call	_ZdlPv@PLT
.L41:
	movq	16(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	%r12, %r13
	je	.L42
	.p2align 4,,10
	.p2align 3
.L46:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L43
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L46
.L44:
	movq	8(%rbx), %r12
.L42:
	testq	%r12, %r12
	je	.L40
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L46
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L40:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22413:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev
	.set	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.section	.rodata._ZN2v88internal16kBuiltinName_362EPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"%TypedArray%.prototype.reduceRight"
	.section	.text._ZN2v88internal16kBuiltinName_362EPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal16kBuiltinName_362EPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal16kBuiltinName_362EPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal16kBuiltinName_362EPNS0_8compiler18CodeAssemblerStateE:
.LFB22410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-272(%rbp), %r15
	leaq	-240(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-280(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-184(%rbp), %r12
	movq	%r13, %rsi
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -280(%rbp)
	movq	%rdi, -240(%rbp)
	movq	%r12, %rdi
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -192(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L53
	call	_ZdlPv@PLT
.L53:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L54
	call	_ZdlPv@PLT
.L54:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L55
	call	_ZdlPv@PLT
.L55:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L56
	.p2align 4,,10
	.p2align 3
.L60:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L57
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L60
.L58:
	movq	-232(%rbp), %r12
.L56:
	testq	%r12, %r12
	je	.L61
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L61:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L76
	addq	$248, %rsp
	leaq	.LC2(%rip), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L60
	jmp	.L58
.L76:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22410:
	.size	_ZN2v88internal16kBuiltinName_362EPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal16kBuiltinName_362EPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SH_SJ_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SH_SJ_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SH_SJ_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SH_SJ_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SH_SJ_:
.LFB26806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$10, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$434042140868675335, %rcx
	movq	%rcx, (%rax)
	movl	$1544, %ecx
	leaq	10(%rax), %rdx
	movw	%cx, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L78
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %rax
.L78:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L79
	movq	%rdx, (%r15)
.L79:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L80
	movq	%rdx, (%r14)
.L80:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L81
	movq	%rdx, 0(%r13)
.L81:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L82
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L82:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L83
	movq	%rdx, (%rbx)
.L83:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L84
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L84:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L85
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L85:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L86
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L86:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L87
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L87:
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L77
	movq	-128(%rbp), %rsi
	movq	%rax, (%rsi)
.L77:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L124
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L124:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26806:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SH_SJ_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SH_SJ_
	.section	.rodata._ZN2v88internal26ReduceRightAllElements_363EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"../../deps/v8/../../deps/v8/src/builtins/typed-array-reduceright.tq"
	.align 8
.LC4:
	.string	"../../deps/v8/../../deps/v8/src/builtins/typed-array.tq"
	.section	.text._ZN2v88internal26ReduceRightAllElements_363EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26ReduceRightAllElements_363EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal26ReduceRightAllElements_363EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal26ReduceRightAllElements_363EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEE:
.LFB22417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$1880, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -5536(%rbp)
	leaq	-5480(%rbp), %r15
	leaq	-5216(%rbp), %rbx
	movq	%r8, -5544(%rbp)
	leaq	-5344(%rbp), %r14
	movq	%rdi, -5496(%rbp)
	movq	%rsi, -5520(%rbp)
	movq	%r15, %rsi
	movq	%rdx, -5504(%rbp)
	movl	$4, %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -5480(%rbp)
	movq	%rbx, %rdi
	movq	%rbx, -5760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-5024(%rbp), %rax
	movl	$8, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -5576(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-5480(%rbp), %rax
	movl	$192, %edi
	movq	$0, -4824(%rbp)
	movq	$0, -4816(%rbp)
	movq	%rax, -4832(%rbp)
	movq	$0, -4808(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movups	%xmm0, (%rax)
	leaq	192(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -4824(%rbp)
	leaq	-4776(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4808(%rbp)
	movq	%rdx, -4816(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4792(%rbp)
	movq	%rax, -5608(%rbp)
	movq	$0, -4800(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-4640(%rbp), %rax
	movl	$9, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -5584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-4448(%rbp), %rax
	movl	$7, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -5592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-5480(%rbp), %rax
	movl	$192, %edi
	movq	$0, -4248(%rbp)
	movq	$0, -4240(%rbp)
	movq	%rax, -4256(%rbp)
	movq	$0, -4232(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -4248(%rbp)
	leaq	-4200(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4232(%rbp)
	movq	%rdx, -4240(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4216(%rbp)
	movq	%rax, -5600(%rbp)
	movq	$0, -4224(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5480(%rbp), %rax
	movl	$240, %edi
	movq	$0, -4056(%rbp)
	movq	$0, -4048(%rbp)
	movq	%rax, -4064(%rbp)
	movq	$0, -4040(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -4056(%rbp)
	leaq	-4008(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4040(%rbp)
	movq	%rdx, -4048(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4024(%rbp)
	movq	%rax, -5728(%rbp)
	movq	$0, -4032(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5480(%rbp), %rax
	movl	$240, %edi
	movq	$0, -3864(%rbp)
	movq	$0, -3856(%rbp)
	movq	%rax, -3872(%rbp)
	movq	$0, -3848(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -3864(%rbp)
	leaq	-3816(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3848(%rbp)
	movq	%rdx, -3856(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3832(%rbp)
	movq	%rax, -5568(%rbp)
	movq	$0, -3840(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5480(%rbp), %rax
	movl	$240, %edi
	movq	$0, -3672(%rbp)
	movq	$0, -3664(%rbp)
	movq	%rax, -3680(%rbp)
	movq	$0, -3656(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -3672(%rbp)
	leaq	-3624(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3656(%rbp)
	movq	%rdx, -3664(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3640(%rbp)
	movq	%rax, -5616(%rbp)
	movq	$0, -3648(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5480(%rbp), %rax
	movl	$240, %edi
	movq	$0, -3480(%rbp)
	movq	$0, -3472(%rbp)
	movq	%rax, -3488(%rbp)
	movq	$0, -3464(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -3480(%rbp)
	leaq	-3432(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3464(%rbp)
	movq	%rdx, -3472(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3448(%rbp)
	movq	%rax, -5672(%rbp)
	movq	$0, -3456(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5480(%rbp), %rax
	movl	$240, %edi
	movq	$0, -3288(%rbp)
	movq	$0, -3280(%rbp)
	movq	%rax, -3296(%rbp)
	movq	$0, -3272(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -3288(%rbp)
	leaq	-3240(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3272(%rbp)
	movq	%rdx, -3280(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3256(%rbp)
	movq	%rax, -5680(%rbp)
	movq	$0, -3264(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5480(%rbp), %rax
	movl	$240, %edi
	movq	$0, -3096(%rbp)
	movq	$0, -3088(%rbp)
	movq	%rax, -3104(%rbp)
	movq	$0, -3080(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -3096(%rbp)
	leaq	-3048(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3080(%rbp)
	movq	%rdx, -3088(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3064(%rbp)
	movq	%rax, -5792(%rbp)
	movq	$0, -3072(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5480(%rbp), %rax
	movl	$240, %edi
	movq	$0, -2904(%rbp)
	movq	$0, -2896(%rbp)
	movq	%rax, -2912(%rbp)
	movq	$0, -2888(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -2904(%rbp)
	leaq	-2856(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2888(%rbp)
	movq	%rdx, -2896(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2872(%rbp)
	movq	%rax, -5808(%rbp)
	movq	$0, -2880(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5480(%rbp), %rax
	movl	$336, %edi
	movq	$0, -2712(%rbp)
	movq	$0, -2704(%rbp)
	movq	%rax, -2720(%rbp)
	movq	$0, -2696(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -2712(%rbp)
	leaq	-2664(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2696(%rbp)
	movq	%rdx, -2704(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2680(%rbp)
	movq	%rax, -5696(%rbp)
	movq	$0, -2688(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5480(%rbp), %rax
	movl	$312, %edi
	movq	$0, -2520(%rbp)
	movq	$0, -2512(%rbp)
	movq	%rax, -2528(%rbp)
	movq	$0, -2504(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rax, -2520(%rbp)
	leaq	-2472(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2504(%rbp)
	movq	%rdx, -2512(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2488(%rbp)
	movq	%rax, -5856(%rbp)
	movq	$0, -2496(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5480(%rbp), %rax
	movl	$336, %edi
	movq	$0, -2328(%rbp)
	movq	$0, -2320(%rbp)
	movq	%rax, -2336(%rbp)
	movq	$0, -2312(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -2328(%rbp)
	leaq	-2280(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2312(%rbp)
	movq	%rdx, -2320(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2296(%rbp)
	movq	%rax, -5720(%rbp)
	movq	$0, -2304(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5480(%rbp), %rax
	movl	$288, %edi
	movq	$0, -2136(%rbp)
	movq	$0, -2128(%rbp)
	movq	%rax, -2144(%rbp)
	movq	$0, -2120(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -2136(%rbp)
	leaq	-2088(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2120(%rbp)
	movq	%rdx, -2128(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2104(%rbp)
	movq	%rax, -5744(%rbp)
	movq	$0, -2112(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5480(%rbp), %rax
	movl	$480, %edi
	movq	$0, -1944(%rbp)
	movq	$0, -1936(%rbp)
	movq	%rax, -1952(%rbp)
	movq	$0, -1928(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movups	%xmm0, (%rax)
	leaq	480(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -1944(%rbp)
	leaq	-1896(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1928(%rbp)
	movq	%rdx, -1936(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1912(%rbp)
	movq	%rax, -5824(%rbp)
	movq	$0, -1920(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5480(%rbp), %rax
	movl	$288, %edi
	movq	$0, -1752(%rbp)
	movq	$0, -1744(%rbp)
	movq	%rax, -1760(%rbp)
	movq	$0, -1736(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -1752(%rbp)
	leaq	-1704(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1736(%rbp)
	movq	%rdx, -1744(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1720(%rbp)
	movq	%rax, -5552(%rbp)
	movq	$0, -1728(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5480(%rbp), %rax
	movl	$240, %edi
	movq	$0, -1560(%rbp)
	movq	$0, -1552(%rbp)
	movq	%rax, -1568(%rbp)
	movq	$0, -1544(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -1560(%rbp)
	leaq	-1512(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1544(%rbp)
	movq	%rdx, -1552(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1528(%rbp)
	movq	%rax, -5776(%rbp)
	movq	$0, -1536(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5480(%rbp), %rax
	movl	$240, %edi
	movq	$0, -1368(%rbp)
	movq	$0, -1360(%rbp)
	movq	%rax, -1376(%rbp)
	movq	$0, -1352(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -1368(%rbp)
	leaq	-1320(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1352(%rbp)
	movq	%rdx, -1360(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1336(%rbp)
	movq	%rax, -5752(%rbp)
	movq	$0, -1344(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5480(%rbp), %rax
	movl	$264, %edi
	movq	$0, -1176(%rbp)
	movq	$0, -1168(%rbp)
	movq	%rax, -1184(%rbp)
	movq	$0, -1160(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -1176(%rbp)
	leaq	-1128(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1160(%rbp)
	movq	%rdx, -1168(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1144(%rbp)
	movq	%rax, -5864(%rbp)
	movq	$0, -1152(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5480(%rbp), %rax
	movl	$288, %edi
	movq	$0, -984(%rbp)
	movq	$0, -976(%rbp)
	movq	%rax, -992(%rbp)
	movq	$0, -968(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -984(%rbp)
	leaq	-936(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -968(%rbp)
	movq	%rdx, -976(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -952(%rbp)
	movq	%rax, -5832(%rbp)
	movq	$0, -960(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5480(%rbp), %rax
	movl	$240, %edi
	movq	$0, -792(%rbp)
	movq	$0, -784(%rbp)
	movq	%rax, -800(%rbp)
	movq	$0, -776(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -792(%rbp)
	leaq	-744(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -776(%rbp)
	movq	%rdx, -784(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -760(%rbp)
	movq	%rax, -5840(%rbp)
	movq	$0, -768(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5480(%rbp), %rax
	movl	$120, %edi
	movq	$0, -600(%rbp)
	movq	$0, -592(%rbp)
	movq	%rax, -608(%rbp)
	movq	$0, -584(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -600(%rbp)
	leaq	-552(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -584(%rbp)
	movq	%rdx, -592(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -568(%rbp)
	movq	%rax, -5848(%rbp)
	movq	$0, -576(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5480(%rbp), %rax
	movl	$120, %edi
	movq	$0, -408(%rbp)
	movq	$0, -400(%rbp)
	movq	%rax, -416(%rbp)
	movq	$0, -392(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -408(%rbp)
	leaq	-360(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -392(%rbp)
	movq	%rdx, -400(%rbp)
	xorl	%edx, %edx
	movq	%rax, -5768(%rbp)
	movups	%xmm0, -376(%rbp)
	movq	$0, -384(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-5536(%rbp), %xmm1
	movq	-5520(%rbp), %xmm2
	movaps	%xmm0, -5344(%rbp)
	movhps	-5544(%rbp), %xmm1
	movq	$0, -5328(%rbp)
	movhps	-5504(%rbp), %xmm2
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm2, -224(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm5
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movdqa	-208(%rbp), %xmm6
	leaq	32(%rax), %rdx
	movq	%rax, -5344(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -5328(%rbp)
	movq	%rdx, -5336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L126
	call	_ZdlPv@PLT
.L126:
	leaq	-5160(%rbp), %r12
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -5152(%rbp)
	jne	.L365
.L127:
	leaq	-4832(%rbp), %rax
	cmpq	$0, -4960(%rbp)
	movq	%rax, -5544(%rbp)
	jne	.L366
.L130:
	cmpq	$0, -4768(%rbp)
	jne	.L367
.L135:
	leaq	-4256(%rbp), %rax
	cmpq	$0, -4576(%rbp)
	movq	%rax, -5608(%rbp)
	jne	.L368
.L138:
	cmpq	$0, -4384(%rbp)
	jne	.L369
.L141:
	leaq	-4064(%rbp), %rax
	cmpq	$0, -4192(%rbp)
	movq	%rax, -5504(%rbp)
	jne	.L370
.L143:
	leaq	-3872(%rbp), %rax
	cmpq	$0, -4000(%rbp)
	movq	%rax, -5600(%rbp)
	leaq	-1376(%rbp), %rax
	movq	%rax, -5520(%rbp)
	jne	.L371
.L146:
	leaq	-3680(%rbp), %rax
	cmpq	$0, -3808(%rbp)
	movq	%rax, -5632(%rbp)
	leaq	-3488(%rbp), %rax
	movq	%rax, -5648(%rbp)
	jne	.L372
.L149:
	leaq	-3104(%rbp), %rax
	cmpq	$0, -3616(%rbp)
	movq	%rax, -5664(%rbp)
	jne	.L373
.L152:
	leaq	-3296(%rbp), %rax
	cmpq	$0, -3424(%rbp)
	movq	%rax, -5616(%rbp)
	jne	.L374
.L154:
	leaq	-2912(%rbp), %rax
	cmpq	$0, -3232(%rbp)
	movq	%rax, -5712(%rbp)
	jne	.L375
	cmpq	$0, -3040(%rbp)
	jne	.L376
.L158:
	leaq	-2720(%rbp), %rax
	cmpq	$0, -2848(%rbp)
	movq	%rax, -5672(%rbp)
	jne	.L377
.L160:
	leaq	-2336(%rbp), %rax
	cmpq	$0, -2656(%rbp)
	movq	%rax, -5680(%rbp)
	leaq	-2528(%rbp), %rax
	movq	%rax, -5568(%rbp)
	jne	.L378
.L162:
	leaq	-2144(%rbp), %rax
	cmpq	$0, -2464(%rbp)
	movq	%rax, -5696(%rbp)
	jne	.L379
.L167:
	leaq	-1760(%rbp), %rax
	cmpq	$0, -2272(%rbp)
	movq	%rax, -5536(%rbp)
	jne	.L380
.L170:
	leaq	-1952(%rbp), %rax
	cmpq	$0, -2080(%rbp)
	movq	%rax, -5720(%rbp)
	jne	.L381
	cmpq	$0, -1888(%rbp)
	jne	.L382
.L176:
	leaq	-1568(%rbp), %rax
	cmpq	$0, -1696(%rbp)
	movq	%rax, -5744(%rbp)
	jne	.L383
	cmpq	$0, -1504(%rbp)
	jne	.L384
.L185:
	leaq	-992(%rbp), %rax
	cmpq	$0, -1312(%rbp)
	movq	%rax, -5728(%rbp)
	leaq	-1184(%rbp), %rax
	movq	%rax, -5552(%rbp)
	jne	.L385
.L187:
	leaq	-800(%rbp), %rax
	cmpq	$0, -1120(%rbp)
	movq	%rax, -5752(%rbp)
	jne	.L386
.L191:
	cmpq	$0, -928(%rbp)
	jne	.L387
.L194:
	cmpq	$0, -736(%rbp)
	leaq	-608(%rbp), %r12
	jne	.L388
.L196:
	cmpq	$0, -544(%rbp)
	leaq	-416(%rbp), %r13
	jne	.L389
.L199:
	movq	-5768(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -5328(%rbp)
	movaps	%xmm0, -5344(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	$134678279, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -5344(%rbp)
	movq	%rdx, -5328(%rbp)
	movq	%rdx, -5336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5344(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L202
	call	_ZdlPv@PLT
.L202:
	movq	(%rbx), %rax
	movq	%r13, %rdi
	movq	32(%rax), %rbx
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5752(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5728(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5552(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5520(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5744(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5536(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5720(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5696(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5680(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5568(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5672(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5712(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5664(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5616(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5648(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5632(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5600(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5504(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5608(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5592(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5584(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5544(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5576(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5760(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L390
	leaq	-40(%rbp), %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L365:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	-224(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-220(%rbp), %rdx
	movaps	%xmm0, -5344(%rbp)
	movl	$134678279, -224(%rbp)
	movq	$0, -5328(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5760(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5344(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L128
	call	_ZdlPv@PLT
.L128:
	movq	(%rbx), %rax
	movl	$13, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	movq	(%rax), %rbx
	movq	16(%rax), %rcx
	movq	%rbx, -5536(%rbp)
	movq	8(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rcx, -5544(%rbp)
	movq	%rax, -5632(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-5496(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal34NewAttachedJSTypedArrayWitness_370EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_12JSTypedArrayEEE@PLT
	movq	-5336(%rbp), %rsi
	movl	$91, %edx
	movq	%r15, %rdi
	movq	-5328(%rbp), %rax
	movq	-5344(%rbp), %r12
	movq	%rsi, -5520(%rbp)
	leaq	.LC4(%rip), %rsi
	movq	%rax, -5504(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm7
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-5536(%rbp), %xmm0
	leaq	-160(%rbp), %rdx
	movq	$0, -5328(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-5544(%rbp), %xmm0
	movhps	-5632(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	%r12, %xmm0
	movhps	-5520(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-5504(%rbp), %xmm0
	movhps	-5520(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -5344(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5576(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L129
	call	_ZdlPv@PLT
.L129:
	leaq	-4968(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L370:
	movq	-5600(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-224(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-216(%rbp), %rdx
	movabsq	$434042140868675335, %rax
	movaps	%xmm0, -5344(%rbp)
	movq	%rax, -224(%rbp)
	movq	$0, -5328(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5608(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5344(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L144
	call	_ZdlPv@PLT
.L144:
	movq	(%rbx), %rax
	movl	$17, %edx
	movq	%r15, %rdi
	movq	24(%rax), %rsi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	32(%rax), %r13
	movq	%rsi, -5600(%rbp)
	movq	40(%rax), %rsi
	movq	%rcx, -5504(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -5632(%rbp)
	movq	48(%rax), %rsi
	movq	%rcx, -5536(%rbp)
	movq	%rsi, -5648(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%rbx, -5520(%rbp)
	movq	56(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$18, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-5496(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	-5496(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -5664(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm7
	movl	$80, %edi
	movq	-5520(%rbp), %xmm0
	movq	-5600(%rbp), %rax
	movq	$0, -5328(%rbp)
	movhps	-5504(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	%rax, %xmm4
	movq	-5536(%rbp), %xmm0
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	%r13, %xmm0
	movhps	-5632(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-5648(%rbp), %xmm0
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	%rax, %xmm0
	movhps	-5664(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -5344(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm7
	movdqa	-192(%rbp), %xmm6
	movq	%r14, %rsi
	movdqa	-160(%rbp), %xmm5
	leaq	80(%rax), %rdx
	movq	%rax, -5344(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-208(%rbp), %xmm7
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 16(%rax)
	movdqa	-176(%rbp), %xmm7
	movups	%xmm5, 64(%rax)
	movups	%xmm7, 48(%rax)
	leaq	-4064(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5328(%rbp)
	movq	%rdx, -5336(%rbp)
	movq	%rax, -5504(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L145
	call	_ZdlPv@PLT
.L145:
	movq	-5728(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L369:
	leaq	-4392(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1799, %r11d
	pxor	%xmm0, %xmm0
	leaq	-224(%rbp), %rsi
	leaq	-217(%rbp), %rdx
	movq	%r14, %rdi
	movw	%r11w, -220(%rbp)
	movaps	%xmm0, -5344(%rbp)
	movl	$134678279, -224(%rbp)
	movb	$6, -218(%rbp)
	movq	$0, -5328(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5592(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L142
	call	_ZdlPv@PLT
.L142:
	movq	-5496(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L368:
	leaq	-4584(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-224(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-215(%rbp), %rdx
	movaps	%xmm0, -5344(%rbp)
	movabsq	$578157328944531207, %rax
	movq	%rax, -224(%rbp)
	movb	$6, -216(%rbp)
	movq	$0, -5328(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5584(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5344(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L139
	call	_ZdlPv@PLT
.L139:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	24(%rax), %rdi
	movq	32(%rax), %rsi
	movq	48(%rax), %rdx
	movq	(%rax), %r10
	movq	8(%rax), %r9
	movq	16(%rax), %r8
	movq	40(%rax), %rcx
	movq	64(%rax), %rax
	movq	%rdi, -200(%rbp)
	movq	%r14, %rdi
	movq	%rsi, -192(%rbp)
	movq	%r13, %rsi
	movq	%rdx, -176(%rbp)
	leaq	-160(%rbp), %rdx
	movq	%r10, -224(%rbp)
	movq	%r9, -216(%rbp)
	movq	%r8, -208(%rbp)
	movq	%rcx, -184(%rbp)
	movq	%rax, -168(%rbp)
	movaps	%xmm0, -5344(%rbp)
	movq	$0, -5328(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-4256(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -5608(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L140
	call	_ZdlPv@PLT
.L140:
	movq	-5600(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L367:
	movq	-5608(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-224(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-216(%rbp), %rdx
	movabsq	$578157328944531207, %rax
	movaps	%xmm0, -5344(%rbp)
	movq	%rax, -224(%rbp)
	movq	$0, -5328(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5544(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5344(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L136
	call	_ZdlPv@PLT
.L136:
	movq	(%rbx), %rax
	leaq	-168(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movdqu	32(%rax), %xmm0
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm1
	movq	48(%rax), %rax
	movq	$0, -5328(%rbp)
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -176(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -5344(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5592(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L137
	call	_ZdlPv@PLT
.L137:
	leaq	-4392(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L366:
	leaq	-4968(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-224(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-216(%rbp), %rdx
	movabsq	$506099734906603271, %rax
	movaps	%xmm0, -5344(%rbp)
	movq	%rax, -224(%rbp)
	movq	$0, -5328(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5576(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5344(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L131
	call	_ZdlPv@PLT
.L131:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	%rsi, -5544(%rbp)
	movq	32(%rax), %rsi
	movq	%rdx, -5648(%rbp)
	movq	48(%rax), %rdx
	movq	%rcx, -5504(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -5632(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	56(%rax), %r12
	movq	%rdx, -5664(%rbp)
	movl	$16, %edx
	movq	%rcx, -5536(%rbp)
	movq	%rbx, -5520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$48, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-5496(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE(%rip), %esi
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-5496(%rbp), %r12
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATuintptr_201EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8UintPtrTEEE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rbx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal13Cast5ATSmi_83EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm5
	pxor	%xmm0, %xmm0
	movq	-5664(%rbp), %xmm7
	movq	-5536(%rbp), %xmm6
	leaq	-5376(%rbp), %r12
	movq	-5520(%rbp), %xmm4
	leaq	-152(%rbp), %rdx
	punpcklqdq	%xmm5, %xmm7
	movq	%r13, %rsi
	movq	-5632(%rbp), %xmm5
	movq	%r12, %rdi
	movhps	-5544(%rbp), %xmm6
	movhps	-5504(%rbp), %xmm4
	movq	%rax, -160(%rbp)
	movhps	-5648(%rbp), %xmm5
	movaps	%xmm7, -5664(%rbp)
	movaps	%xmm5, -5632(%rbp)
	movaps	%xmm6, -5536(%rbp)
	movaps	%xmm4, -5520(%rbp)
	movaps	%xmm4, -224(%rbp)
	movaps	%xmm6, -208(%rbp)
	movaps	%xmm5, -192(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm0, -5376(%rbp)
	movq	$0, -5360(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5584(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L132
	call	_ZdlPv@PLT
.L132:
	leaq	-4584(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4832(%rbp), %rax
	cmpq	$0, -5336(%rbp)
	movq	%rax, -5544(%rbp)
	jne	.L391
.L133:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L371:
	movq	-5728(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5400(%rbp)
	movq	$0, -5392(%rbp)
	movq	$0, -5376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5376(%rbp), %rax
	movq	-5504(%rbp), %rdi
	pushq	%rax
	leaq	-5392(%rbp), %rax
	leaq	-5440(%rbp), %rcx
	pushq	%rax
	leaq	-5400(%rbp), %rax
	leaq	-5424(%rbp), %r9
	pushq	%rax
	leaq	-5408(%rbp), %rax
	leaq	-5432(%rbp), %r8
	pushq	%rax
	leaq	-5416(%rbp), %rax
	leaq	-5448(%rbp), %rdx
	pushq	%rax
	leaq	-5456(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SH_SJ_
	movq	-5496(%rbp), %rbx
	addq	$48, %rsp
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5376(%rbp), %r13
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler24IntPtrGreaterThanOrEqualENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$80, %edi
	movq	-5392(%rbp), %xmm7
	movq	-5408(%rbp), %xmm5
	movaps	%xmm0, -5344(%rbp)
	movq	-5424(%rbp), %xmm6
	movq	-5440(%rbp), %xmm3
	movq	-5456(%rbp), %xmm4
	movhps	-5376(%rbp), %xmm7
	movq	$0, -5328(%rbp)
	movhps	-5400(%rbp), %xmm5
	movhps	-5416(%rbp), %xmm6
	movaps	%xmm7, -5536(%rbp)
	movhps	-5432(%rbp), %xmm3
	movhps	-5448(%rbp), %xmm4
	movaps	%xmm5, -5520(%rbp)
	movaps	%xmm6, -5632(%rbp)
	movaps	%xmm3, -5664(%rbp)
	movaps	%xmm4, -5648(%rbp)
	movaps	%xmm4, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm7, -160(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm6
	movq	%r14, %rsi
	movdqa	-208(%rbp), %xmm7
	movdqa	-192(%rbp), %xmm5
	movq	-5600(%rbp), %rdi
	leaq	80(%rax), %rdx
	movq	%rax, -5344(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-176(%rbp), %xmm6
	movups	%xmm7, 16(%rax)
	movdqa	-160(%rbp), %xmm7
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movq	%rdx, -5328(%rbp)
	movq	%rdx, -5336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L147
	call	_ZdlPv@PLT
.L147:
	movdqa	-5648(%rbp), %xmm5
	movdqa	-5664(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$80, %edi
	movdqa	-5632(%rbp), %xmm7
	movaps	%xmm0, -5344(%rbp)
	movaps	%xmm5, -224(%rbp)
	movdqa	-5520(%rbp), %xmm5
	movaps	%xmm6, -208(%rbp)
	movdqa	-5536(%rbp), %xmm6
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	movq	$0, -5328(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm7
	movdqa	-208(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm6
	leaq	80(%rax), %rdx
	movq	%rax, -5344(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-176(%rbp), %xmm7
	movups	%xmm5, 16(%rax)
	movdqa	-160(%rbp), %xmm5
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm5, 64(%rax)
	leaq	-1376(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5328(%rbp)
	movq	%rdx, -5336(%rbp)
	movq	%rax, -5520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L148
	call	_ZdlPv@PLT
.L148:
	movq	-5752(%rbp), %rcx
	movq	-5568(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L372:
	movq	-5568(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5400(%rbp)
	movq	$0, -5392(%rbp)
	movq	$0, -5376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5376(%rbp), %rax
	movq	-5600(%rbp), %rdi
	pushq	%rax
	leaq	-5392(%rbp), %rax
	leaq	-5424(%rbp), %r9
	pushq	%rax
	leaq	-5400(%rbp), %rax
	leaq	-5432(%rbp), %r8
	pushq	%rax
	leaq	-5408(%rbp), %rax
	leaq	-5440(%rbp), %rcx
	pushq	%rax
	leaq	-5416(%rbp), %rax
	leaq	-5448(%rbp), %rdx
	pushq	%rax
	leaq	-5456(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SH_SJ_
	addq	$48, %rsp
	movl	$99, %edx
	movq	%r15, %rdi
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-5496(%rbp), %rbx
	movq	%r14, %rdi
	movq	%rax, %r12
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	movq	-5424(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE(%rip), %esi
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler16IsDetachedBufferENS0_8compiler5TNodeINS0_13JSArrayBufferEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$80, %edi
	movq	-5392(%rbp), %xmm7
	movq	-5408(%rbp), %xmm5
	movaps	%xmm0, -5344(%rbp)
	movq	-5424(%rbp), %xmm6
	movq	-5440(%rbp), %xmm3
	movq	-5456(%rbp), %xmm4
	movhps	-5376(%rbp), %xmm7
	movq	$0, -5328(%rbp)
	movhps	-5400(%rbp), %xmm5
	movhps	-5416(%rbp), %xmm6
	movaps	%xmm7, -5536(%rbp)
	movhps	-5432(%rbp), %xmm3
	movhps	-5448(%rbp), %xmm4
	movaps	%xmm5, -5664(%rbp)
	movaps	%xmm6, -5712(%rbp)
	movaps	%xmm3, -5568(%rbp)
	movaps	%xmm4, -5648(%rbp)
	movaps	%xmm4, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm7, -160(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm6
	movq	%r14, %rsi
	movdqa	-208(%rbp), %xmm7
	movdqa	-192(%rbp), %xmm5
	movq	-5632(%rbp), %rdi
	leaq	80(%rax), %rdx
	movq	%rax, -5344(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-176(%rbp), %xmm6
	movups	%xmm7, 16(%rax)
	movdqa	-160(%rbp), %xmm7
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movq	%rdx, -5328(%rbp)
	movq	%rdx, -5336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L150
	call	_ZdlPv@PLT
.L150:
	movdqa	-5648(%rbp), %xmm5
	movdqa	-5568(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$80, %edi
	movdqa	-5712(%rbp), %xmm7
	movaps	%xmm0, -5344(%rbp)
	movaps	%xmm5, -224(%rbp)
	movdqa	-5664(%rbp), %xmm5
	movaps	%xmm6, -208(%rbp)
	movdqa	-5536(%rbp), %xmm6
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	movq	$0, -5328(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm7
	movdqa	-208(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm6
	leaq	80(%rax), %rdx
	movq	%rax, -5344(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-176(%rbp), %xmm7
	movups	%xmm5, 16(%rax)
	movdqa	-160(%rbp), %xmm5
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm5, 64(%rax)
	leaq	-3488(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5328(%rbp)
	movq	%rdx, -5336(%rbp)
	movq	%rax, -5648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L151
	call	_ZdlPv@PLT
.L151:
	movq	-5672(%rbp), %rcx
	movq	-5616(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L373:
	movq	-5616(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5400(%rbp)
	movq	$0, -5392(%rbp)
	movq	$0, -5376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5376(%rbp), %rax
	movq	-5632(%rbp), %rdi
	pushq	%rax
	leaq	-5392(%rbp), %rax
	leaq	-5440(%rbp), %rcx
	pushq	%rax
	leaq	-5400(%rbp), %rax
	leaq	-5448(%rbp), %rdx
	pushq	%rax
	leaq	-5408(%rbp), %rax
	leaq	-5456(%rbp), %rsi
	pushq	%rax
	leaq	-5416(%rbp), %rax
	leaq	-5424(%rbp), %r9
	pushq	%rax
	leaq	-5432(%rbp), %r8
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SH_SJ_
	addq	$48, %rsp
	movl	$80, %edi
	movq	-5392(%rbp), %xmm0
	movq	-5408(%rbp), %xmm1
	movq	-5424(%rbp), %xmm2
	movq	$0, -5328(%rbp)
	movq	-5440(%rbp), %xmm3
	movhps	-5376(%rbp), %xmm0
	movq	-5456(%rbp), %xmm4
	movhps	-5400(%rbp), %xmm1
	movhps	-5416(%rbp), %xmm2
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-5432(%rbp), %xmm3
	movhps	-5448(%rbp), %xmm4
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm4, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -5344(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm6
	movq	%r14, %rsi
	movdqa	-208(%rbp), %xmm7
	movdqa	-192(%rbp), %xmm5
	movq	-5664(%rbp), %rdi
	leaq	80(%rax), %rdx
	movq	%rax, -5344(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-176(%rbp), %xmm6
	movups	%xmm7, 16(%rax)
	movdqa	-160(%rbp), %xmm7
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movq	%rdx, -5328(%rbp)
	movq	%rdx, -5336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L153
	call	_ZdlPv@PLT
.L153:
	movq	-5792(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L375:
	movq	-5680(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5400(%rbp)
	movq	$0, -5392(%rbp)
	movq	$0, -5376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5376(%rbp), %rax
	movq	-5616(%rbp), %rdi
	pushq	%rax
	leaq	-5392(%rbp), %rax
	leaq	-5440(%rbp), %rcx
	pushq	%rax
	leaq	-5400(%rbp), %rax
	leaq	-5448(%rbp), %rdx
	pushq	%rax
	leaq	-5408(%rbp), %rax
	leaq	-5456(%rbp), %rsi
	pushq	%rax
	leaq	-5416(%rbp), %rax
	leaq	-5424(%rbp), %r9
	pushq	%rax
	leaq	-5432(%rbp), %r8
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SH_SJ_
	addq	$48, %rsp
	movl	$80, %edi
	movq	-5392(%rbp), %xmm0
	movq	-5408(%rbp), %xmm1
	movq	-5424(%rbp), %xmm2
	movq	$0, -5328(%rbp)
	movq	-5440(%rbp), %xmm3
	movhps	-5376(%rbp), %xmm0
	movq	-5456(%rbp), %xmm4
	movhps	-5400(%rbp), %xmm1
	movhps	-5416(%rbp), %xmm2
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-5432(%rbp), %xmm3
	movhps	-5448(%rbp), %xmm4
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm4, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -5344(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-208(%rbp), %xmm5
	movdqa	-192(%rbp), %xmm6
	movq	-5712(%rbp), %rdi
	leaq	80(%rax), %rdx
	movq	%rax, -5344(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-176(%rbp), %xmm7
	movups	%xmm5, 16(%rax)
	movdqa	-160(%rbp), %xmm5
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rdx, -5328(%rbp)
	movq	%rdx, -5336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L157
	call	_ZdlPv@PLT
.L157:
	movq	-5808(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3040(%rbp)
	je	.L158
.L376:
	movq	-5792(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5400(%rbp)
	movq	$0, -5392(%rbp)
	movq	$0, -5376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5376(%rbp), %rax
	movq	-5664(%rbp), %rdi
	pushq	%rax
	leaq	-5392(%rbp), %rax
	leaq	-5440(%rbp), %rcx
	pushq	%rax
	leaq	-5400(%rbp), %rax
	leaq	-5448(%rbp), %rdx
	pushq	%rax
	leaq	-5408(%rbp), %rax
	leaq	-5456(%rbp), %rsi
	pushq	%rax
	leaq	-5416(%rbp), %rax
	leaq	-5424(%rbp), %r9
	pushq	%rax
	leaq	-5432(%rbp), %r8
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SH_SJ_
	addq	$48, %rsp
	movl	$80, %edi
	movq	-5392(%rbp), %xmm0
	movq	-5408(%rbp), %xmm1
	movq	-5424(%rbp), %xmm2
	movq	$0, -5328(%rbp)
	movq	-5440(%rbp), %xmm3
	movhps	-5376(%rbp), %xmm0
	movq	-5456(%rbp), %xmm4
	movhps	-5400(%rbp), %xmm1
	movhps	-5416(%rbp), %xmm2
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-5432(%rbp), %xmm3
	movhps	-5448(%rbp), %xmm4
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm4, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -5344(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm6
	movq	%r14, %rsi
	movdqa	-208(%rbp), %xmm7
	movdqa	-192(%rbp), %xmm5
	movq	-5520(%rbp), %rdi
	leaq	80(%rax), %rdx
	movq	%rax, -5344(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-176(%rbp), %xmm6
	movups	%xmm7, 16(%rax)
	movdqa	-160(%rbp), %xmm7
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movq	%rdx, -5328(%rbp)
	movq	%rdx, -5336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L159
	call	_ZdlPv@PLT
.L159:
	movq	-5752(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L374:
	movq	-5672(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5400(%rbp)
	movq	$0, -5392(%rbp)
	movq	$0, -5376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5376(%rbp), %rax
	movq	-5648(%rbp), %rdi
	pushq	%rax
	leaq	-5392(%rbp), %rax
	leaq	-5440(%rbp), %rcx
	pushq	%rax
	leaq	-5400(%rbp), %rax
	leaq	-5424(%rbp), %r9
	pushq	%rax
	leaq	-5408(%rbp), %rax
	leaq	-5432(%rbp), %r8
	pushq	%rax
	leaq	-5416(%rbp), %rax
	leaq	-5448(%rbp), %rdx
	pushq	%rax
	leaq	-5456(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SH_SJ_
	addq	$48, %rsp
	movl	$100, %edx
	movq	%r15, %rdi
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$20, %edx
	movq	%r15, %rdi
	leaq	.LC3(%rip), %rsi
	movq	-5424(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-5424(%rbp), %rax
	movl	$80, %edi
	movq	-5440(%rbp), %xmm0
	movq	-5456(%rbp), %xmm1
	movq	%rbx, -184(%rbp)
	movhps	-5432(%rbp), %xmm0
	movq	%rax, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	movq	-5408(%rbp), %xmm0
	movhps	-5448(%rbp), %xmm1
	movaps	%xmm1, -224(%rbp)
	movhps	-5400(%rbp), %xmm0
	movq	$0, -5328(%rbp)
	movaps	%xmm0, -176(%rbp)
	movq	-5392(%rbp), %xmm0
	movhps	-5376(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -5344(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-208(%rbp), %xmm6
	movdqa	-192(%rbp), %xmm7
	movq	-5616(%rbp), %rdi
	leaq	80(%rax), %rdx
	movq	%rax, -5344(%rbp)
	movups	%xmm5, (%rax)
	movdqa	-176(%rbp), %xmm5
	movups	%xmm6, 16(%rax)
	movdqa	-160(%rbp), %xmm6
	movups	%xmm7, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	movq	%rdx, -5328(%rbp)
	movq	%rdx, -5336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L155
	call	_ZdlPv@PLT
.L155:
	movq	-5680(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L377:
	movq	-5808(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -5472(%rbp)
	leaq	-5392(%rbp), %r12
	movq	$0, -5464(%rbp)
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5400(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5400(%rbp), %rax
	movq	-5712(%rbp), %rdi
	pushq	%rax
	leaq	-5408(%rbp), %rax
	leaq	-5456(%rbp), %rcx
	pushq	%rax
	leaq	-5416(%rbp), %rax
	leaq	-5440(%rbp), %r9
	pushq	%rax
	leaq	-5424(%rbp), %rax
	leaq	-5448(%rbp), %r8
	pushq	%rax
	leaq	-5432(%rbp), %rax
	leaq	-5464(%rbp), %rdx
	pushq	%rax
	leaq	-5472(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SH_SJ_
	addq	$48, %rsp
	movl	$21, %edx
	movq	%r15, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$104, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$105, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-5496(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %edi
	movq	-5472(%rbp), %rbx
	call	_ZN2v88internal42ExampleBuiltinForTorqueFunctionPointerTypeEm@PLT
	movq	%r15, %rdi
	movl	%eax, %r13d
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	%r13d, %edx
	movq	%r14, %rdi
	leaq	-224(%rbp), %r13
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movl	$2, %edx
	movq	%rbx, %r9
	movq	%r12, %rdi
	pushq	%rdx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-5432(%rbp), %xmm0
	leaq	-5376(%rbp), %r10
	pushq	%r13
	movq	%r10, %rdx
	movl	$1, %ecx
	movl	$2, %esi
	movq	-5424(%rbp), %r8
	movq	%rax, -5376(%rbp)
	movhps	-5400(%rbp), %xmm0
	movq	-5328(%rbp), %rax
	movaps	%xmm0, -224(%rbp)
	movq	%rax, -5368(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-112(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-5408(%rbp), %xmm0
	movq	-5400(%rbp), %rax
	movq	%rbx, -120(%rbp)
	movq	-5424(%rbp), %xmm1
	movq	-5440(%rbp), %xmm2
	movq	$0, -5328(%rbp)
	movhps	-5400(%rbp), %xmm0
	movq	%rax, -128(%rbp)
	movq	-5456(%rbp), %xmm3
	movaps	%xmm0, -160(%rbp)
	movhps	-5416(%rbp), %xmm1
	movq	-5400(%rbp), %xmm0
	movq	-5472(%rbp), %xmm4
	movhps	-5432(%rbp), %xmm2
	movhps	-5448(%rbp), %xmm3
	movaps	%xmm1, -176(%rbp)
	movhps	-5472(%rbp), %xmm0
	movhps	-5464(%rbp), %xmm4
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -224(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm0, -5344(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5672(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5344(%rbp), %rdi
	popq	%r9
	popq	%r10
	testq	%rdi, %rdi
	je	.L161
	call	_ZdlPv@PLT
.L161:
	movq	-5696(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L381:
	movq	-5744(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$434042140868675335, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$12, %edi
	movq	$0, -5328(%rbp)
	movaps	%xmm0, -5344(%rbp)
	call	_Znwm@PLT
	movq	-5696(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	12(%rax), %rdx
	movl	$134743560, 8(%rax)
	movq	%rax, -5344(%rbp)
	movq	%rdx, -5328(%rbp)
	movq	%rdx, -5336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5344(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L174
	call	_ZdlPv@PLT
.L174:
	movq	(%rbx), %rax
	movl	$26, %edx
	movq	%r15, %rdi
	movq	40(%rax), %rsi
	movq	8(%rax), %rcx
	movq	88(%rax), %xmm0
	movq	(%rax), %rbx
	movq	48(%rax), %r13
	movq	64(%rax), %r12
	movq	%rsi, -5888(%rbp)
	movq	56(%rax), %rsi
	movq	%rcx, -5808(%rbp)
	movq	24(%rax), %rcx
	movq	%rbx, -5720(%rbp)
	movq	%rsi, -5872(%rbp)
	movq	72(%rax), %rsi
	movq	%rcx, -5856(%rbp)
	movq	32(%rax), %rcx
	movq	%rsi, -5904(%rbp)
	movq	80(%rax), %rsi
	movq	%rcx, -5744(%rbp)
	movq	16(%rax), %rbx
	movq	%rsi, -5792(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%xmm0, -5928(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$28, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-5496(%rbp), %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$95, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -5920(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-5904(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movq	%r14, %rdi
	movq	-5720(%rbp), %xmm1
	movq	-5928(%rbp), %xmm0
	leaq	-224(%rbp), %rsi
	movq	$0, -5328(%rbp)
	movq	%rax, %xmm7
	movhps	-5808(%rbp), %xmm1
	movaps	%xmm1, -224(%rbp)
	movq	%rbx, %xmm1
	movhps	-5856(%rbp), %xmm1
	movaps	%xmm1, -208(%rbp)
	movq	-5744(%rbp), %xmm1
	movhps	-5888(%rbp), %xmm1
	movaps	%xmm1, -192(%rbp)
	movq	%r13, %xmm1
	movhps	-5872(%rbp), %xmm1
	movaps	%xmm1, -176(%rbp)
	movq	%r12, %xmm1
	punpcklqdq	%xmm7, %xmm1
	movaps	%xmm1, -160(%rbp)
	movq	-5792(%rbp), %xmm1
	punpcklqdq	%xmm0, %xmm1
	movaps	%xmm1, -144(%rbp)
	movdqa	%xmm0, %xmm1
	movhps	-5792(%rbp), %xmm0
	movhps	-5720(%rbp), %xmm1
	movaps	%xmm0, -96(%rbp)
	movq	%rax, %xmm0
	movhps	-5744(%rbp), %xmm0
	movaps	%xmm1, -128(%rbp)
	movq	%rbx, %xmm1
	movhps	-5920(%rbp), %xmm1
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -5344(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1952(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -5720(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L175
	call	_ZdlPv@PLT
.L175:
	movq	-5824(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1888(%rbp)
	je	.L176
.L382:
	movq	-5824(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-224(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC5(%rip), %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-204(%rbp), %rdx
	movl	$117835784, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -5344(%rbp)
	movq	$0, -5328(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5720(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5344(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L177
	call	_ZdlPv@PLT
.L177:
	movq	(%rbx), %rax
	movl	$27, %edx
	movq	%r15, %rdi
	leaq	.LC3(%rip), %rsi
	leaq	-5392(%rbp), %r12
	movq	(%rax), %rbx
	movq	32(%rax), %rcx
	movq	%rbx, -5744(%rbp)
	movq	8(%rax), %rbx
	movq	%rcx, -5856(%rbp)
	movq	48(%rax), %rcx
	movq	%rbx, -5792(%rbp)
	movq	16(%rax), %rbx
	movq	%rcx, -5872(%rbp)
	movq	72(%rax), %rcx
	movq	%rbx, -5808(%rbp)
	movq	24(%rax), %rbx
	movq	%rcx, -5920(%rbp)
	movq	88(%rax), %rcx
	movq	%rbx, -5824(%rbp)
	movq	40(%rax), %rbx
	movq	%rcx, -5936(%rbp)
	movq	112(%rax), %rcx
	movq	%rbx, -5888(%rbp)
	movq	56(%rax), %rbx
	movq	%rcx, -5960(%rbp)
	movq	128(%rax), %rcx
	movq	%rbx, -5904(%rbp)
	movq	80(%rax), %rbx
	movq	%rcx, -5968(%rbp)
	movq	136(%rax), %rcx
	movq	%rbx, -5928(%rbp)
	movq	104(%rax), %rbx
	movq	%rcx, -5984(%rbp)
	movq	%rbx, -5952(%rbp)
	movq	120(%rax), %rbx
	movq	144(%rax), %rcx
	movq	152(%rax), %rax
	movq	%rcx, -5992(%rbp)
	movq	%rax, -6000(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-5496(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19IsUndefinedConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	je	.L178
.L180:
	movq	-5984(%rbp), %xmm0
	movq	%rbx, %xmm1
	movq	%r12, %rdi
	movhps	-5968(%rbp), %xmm1
	movhps	-5992(%rbp), %xmm0
	movaps	%xmm1, -6016(%rbp)
	movaps	%xmm0, -5984(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-5344(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -5968(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-6016(%rbp), %xmm1
	movq	-5960(%rbp), %xmm2
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movdqa	-5984(%rbp), %xmm0
	movq	%rax, -5376(%rbp)
	movq	-5328(%rbp), %rax
	movhps	-5968(%rbp), %xmm2
	movaps	%xmm2, -224(%rbp)
	movq	%rax, -5368(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
.L364:
	movl	$7, %edi
	movq	-5952(%rbp), %r9
	xorl	%esi, %esi
	movq	-6000(%rbp), %rax
	pushq	%rdi
	leaq	-5376(%rbp), %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	pushq	%r13
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	popq	%rcx
	movq	%r12, %rdi
	popq	%rsi
	movq	-5744(%rbp), %xmm4
	movq	%rax, -5960(%rbp)
	movq	-5808(%rbp), %xmm3
	movq	-5856(%rbp), %xmm2
	movq	-5872(%rbp), %xmm1
	movhps	-5792(%rbp), %xmm4
	movq	-5928(%rbp), %xmm0
	movhps	-5824(%rbp), %xmm3
	movhps	-5888(%rbp), %xmm2
	movaps	%xmm4, -5952(%rbp)
	movhps	-5904(%rbp), %xmm1
	movhps	-5936(%rbp), %xmm0
	movaps	%xmm3, -5824(%rbp)
	movaps	%xmm2, -5808(%rbp)
	movaps	%xmm1, -5792(%rbp)
	movaps	%xmm0, -5744(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$23, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movdqa	-5792(%rbp), %xmm1
	movq	%r13, %rsi
	movdqa	-5744(%rbp), %xmm0
	movdqa	-5952(%rbp), %xmm4
	movdqa	-5824(%rbp), %xmm3
	leaq	-128(%rbp), %rdx
	movq	%r14, %rdi
	movaps	%xmm1, -176(%rbp)
	movq	-5960(%rbp), %xmm1
	movdqa	-5808(%rbp), %xmm2
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-5920(%rbp), %xmm1
	movaps	%xmm4, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -5344(%rbp)
	movq	$0, -5328(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5536(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L181
	call	_ZdlPv@PLT
.L181:
	movq	-5552(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L380:
	movq	-5720(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-224(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1800, %edi
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movw	%di, -212(%rbp)
	leaq	-210(%rbp), %rdx
	movq	%r14, %rdi
	movabsq	$434042140868675335, %rax
	movq	%rax, -224(%rbp)
	movaps	%xmm0, -5344(%rbp)
	movl	$134743560, -216(%rbp)
	movq	$0, -5328(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5680(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5344(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L171
	call	_ZdlPv@PLT
.L171:
	movq	(%rbx), %rax
	movl	$24, %edx
	movq	%r15, %rdi
	movq	24(%rax), %rsi
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	48(%rax), %r12
	movq	%rsi, -5808(%rbp)
	movq	40(%rax), %rsi
	movq	%rbx, -5536(%rbp)
	movq	32(%rax), %rbx
	movq	%rsi, -5856(%rbp)
	movq	56(%rax), %rsi
	movq	%rcx, -5720(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -5888(%rbp)
	movq	72(%rax), %rsi
	movq	%rcx, -5792(%rbp)
	movq	%rsi, -5872(%rbp)
	movq	80(%rax), %rsi
	movq	88(%rax), %rax
	movq	%rsi, -5904(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%rax, -5920(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$23, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-128(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-5536(%rbp), %xmm0
	movq	-5904(%rbp), %rax
	movq	$0, -5328(%rbp)
	movhps	-5720(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-5792(%rbp), %xmm0
	movhps	-5808(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	%rbx, %xmm0
	movhps	-5856(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	%r12, %xmm0
	movhps	-5888(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	%rax, %xmm0
	movhps	-5872(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%rax, %xmm0
	movhps	-5920(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -5344(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1760(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -5536(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L172
	call	_ZdlPv@PLT
.L172:
	movq	-5552(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L379:
	movq	-5856(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-224(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-211(%rbp), %rdx
	movabsq	$434042140868675335, %rax
	movaps	%xmm0, -5344(%rbp)
	movq	%rax, -224(%rbp)
	movl	$134743560, -216(%rbp)
	movb	$8, -212(%rbp)
	movq	$0, -5328(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5568(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5344(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L168
	call	_ZdlPv@PLT
.L168:
	movq	(%rbx), %rax
	leaq	-128(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movdqu	64(%rax), %xmm0
	movdqu	(%rax), %xmm4
	movdqu	16(%rax), %xmm3
	movdqu	32(%rax), %xmm2
	movdqu	48(%rax), %xmm1
	movdqu	80(%rax), %xmm6
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -224(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -5344(%rbp)
	movq	$0, -5328(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2144(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -5696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L169
	call	_ZdlPv@PLT
.L169:
	movq	-5744(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L378:
	movq	-5696(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-224(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	movabsq	$434042140868675335, %rax
	movl	$2054, %r8d
	leaq	-210(%rbp), %rdx
	movaps	%xmm0, -5344(%rbp)
	movq	%rax, -224(%rbp)
	movw	%r8w, -212(%rbp)
	movl	$117835272, -216(%rbp)
	movq	$0, -5328(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5672(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5344(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L163
	call	_ZdlPv@PLT
.L163:
	movq	(%rbx), %rax
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	104(%rax), %r12
	movq	%rsi, -5696(%rbp)
	movq	%rdx, -5808(%rbp)
	movq	32(%rax), %rsi
	movq	48(%rax), %rdx
	movq	%rdi, -5872(%rbp)
	movq	72(%rax), %rdi
	movq	%rcx, -5568(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -5792(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%rdx, -5888(%rbp)
	movl	$21, %edx
	movq	%rdi, -5904(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -5680(%rbp)
	movq	%rbx, -5536(%rbp)
	movq	64(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$22, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$23, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5496(%rbp), %rdi
	movq	%r14, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal17Cast9ATTheHole_88EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-112(%rbp), %rdx
	movq	%r13, %rsi
	movq	-5888(%rbp), %xmm6
	movq	%r12, %xmm7
	movq	-5792(%rbp), %xmm3
	movq	%rbx, %xmm5
	leaq	-5376(%rbp), %r12
	punpcklqdq	%xmm5, %xmm7
	movhps	-5904(%rbp), %xmm5
	movq	%rax, -120(%rbp)
	movq	-5536(%rbp), %xmm4
	movq	%r12, %rdi
	movq	-5680(%rbp), %xmm2
	movhps	-5872(%rbp), %xmm6
	movhps	-5808(%rbp), %xmm3
	movaps	%xmm7, -5920(%rbp)
	movhps	-5696(%rbp), %xmm2
	movhps	-5568(%rbp), %xmm4
	movaps	%xmm5, -5904(%rbp)
	movaps	%xmm6, -5888(%rbp)
	movaps	%xmm3, -5792(%rbp)
	movaps	%xmm2, -5696(%rbp)
	movaps	%xmm4, -5536(%rbp)
	movaps	%xmm4, -224(%rbp)
	movaps	%xmm2, -208(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm0, -5376(%rbp)
	movq	%rbx, -128(%rbp)
	movq	$0, -5360(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2336(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -5680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L164
	call	_ZdlPv@PLT
.L164:
	movq	-5720(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2528(%rbp), %rax
	cmpq	$0, -5336(%rbp)
	movq	%rax, -5568(%rbp)
	jne	.L392
.L165:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L383:
	movq	-5552(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$434042140868675335, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$12, %edi
	movq	$0, -5328(%rbp)
	movaps	%xmm0, -5344(%rbp)
	call	_Znwm@PLT
	movq	-5536(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	12(%rax), %rdx
	movl	$134743560, 8(%rax)
	movq	%rax, -5344(%rbp)
	movq	%rdx, -5328(%rbp)
	movq	%rdx, -5336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5344(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L183
	call	_ZdlPv@PLT
.L183:
	movq	(%rbx), %rax
	movl	$22, %edx
	movq	%r15, %rdi
	movq	24(%rax), %rsi
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	32(%rax), %r13
	movq	%rsi, -5808(%rbp)
	movq	40(%rax), %rsi
	movq	48(%rax), %r12
	movq	%rbx, -5552(%rbp)
	movq	%rsi, -5824(%rbp)
	movq	56(%rax), %rsi
	movq	64(%rax), %rbx
	movq	%rcx, -5744(%rbp)
	movq	16(%rax), %rcx
	movq	72(%rax), %rax
	movq	%rsi, -5856(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%rcx, -5792(%rbp)
	movq	%rax, -5888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$18, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$80, %edi
	movq	-5552(%rbp), %xmm0
	movq	$0, -5328(%rbp)
	movhps	-5744(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-5792(%rbp), %xmm0
	movhps	-5808(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	%r13, %xmm0
	movhps	-5824(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	%r12, %xmm0
	movhps	-5856(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	%rbx, %xmm0
	movhps	-5888(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -5344(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-208(%rbp), %xmm5
	movdqa	-192(%rbp), %xmm6
	movdqa	-176(%rbp), %xmm3
	leaq	80(%rax), %rdx
	movq	%rax, -5344(%rbp)
	movdqa	-160(%rbp), %xmm4
	movups	%xmm7, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm4, 64(%rax)
	leaq	-1568(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5328(%rbp)
	movq	%rdx, -5336(%rbp)
	movq	%rax, -5744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L184
	call	_ZdlPv@PLT
.L184:
	movq	-5776(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1504(%rbp)
	je	.L185
.L384:
	movq	-5776(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -5456(%rbp)
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5400(%rbp)
	movq	$0, -5392(%rbp)
	movq	$0, -5376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5376(%rbp), %rax
	movq	-5744(%rbp), %rdi
	pushq	%rax
	leaq	-5392(%rbp), %rax
	leaq	-5440(%rbp), %rcx
	pushq	%rax
	leaq	-5400(%rbp), %rax
	leaq	-5424(%rbp), %r9
	pushq	%rax
	leaq	-5408(%rbp), %rax
	leaq	-5432(%rbp), %r8
	pushq	%rax
	leaq	-5416(%rbp), %rax
	leaq	-5448(%rbp), %rdx
	pushq	%rax
	leaq	-5456(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SH_SJ_
	movq	-5496(%rbp), %rbx
	addq	$48, %rsp
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5376(%rbp), %r13
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-5392(%rbp), %rax
	movq	-5408(%rbp), %xmm0
	movl	$80, %edi
	movq	-5424(%rbp), %xmm1
	movq	%rbx, -152(%rbp)
	movq	-5440(%rbp), %xmm2
	movhps	-5400(%rbp), %xmm0
	movq	-5456(%rbp), %xmm3
	movq	%rax, -160(%rbp)
	movhps	-5416(%rbp), %xmm1
	movaps	%xmm0, -176(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-5432(%rbp), %xmm2
	movhps	-5448(%rbp), %xmm3
	movaps	%xmm1, -192(%rbp)
	movaps	%xmm3, -224(%rbp)
	movaps	%xmm2, -208(%rbp)
	movaps	%xmm0, -5344(%rbp)
	movq	$0, -5328(%rbp)
	call	_Znwm@PLT
	movdqa	-224(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-208(%rbp), %xmm5
	movdqa	-192(%rbp), %xmm6
	movdqa	-176(%rbp), %xmm3
	leaq	80(%rax), %rdx
	movq	%rax, -5344(%rbp)
	movdqa	-160(%rbp), %xmm4
	movq	-5504(%rbp), %rdi
	movups	%xmm7, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm4, 64(%rax)
	movq	%rdx, -5328(%rbp)
	movq	%rdx, -5336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L186
	call	_ZdlPv@PLT
.L186:
	movq	-5728(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L385:
	movq	-5752(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -5464(%rbp)
	leaq	-5376(%rbp), %r12
	movq	$0, -5456(%rbp)
	leaq	-224(%rbp), %r13
	movq	$0, -5448(%rbp)
	movq	$0, -5440(%rbp)
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	$0, -5400(%rbp)
	movq	$0, -5392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5392(%rbp), %rax
	movq	-5520(%rbp), %rdi
	pushq	%rax
	leaq	-5400(%rbp), %rax
	leaq	-5432(%rbp), %r9
	pushq	%rax
	leaq	-5408(%rbp), %rax
	leaq	-5448(%rbp), %rcx
	pushq	%rax
	leaq	-5416(%rbp), %rax
	leaq	-5440(%rbp), %r8
	pushq	%rax
	leaq	-5424(%rbp), %rax
	leaq	-5456(%rbp), %rdx
	pushq	%rax
	leaq	-5464(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_12JSTypedArrayENS0_10JSReceiverENS0_6ObjectES4_S4_NS0_3SmiES7_S6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESD_SD_PNS9_IS7_EESJ_SH_SJ_
	addq	$48, %rsp
	movl	$33, %edx
	movq	%r15, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$34, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-5400(%rbp), %rsi
	movq	-5496(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal17Cast9ATTheHole_88EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-128(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-5416(%rbp), %xmm0
	movq	%rax, %xmm7
	movq	-5400(%rbp), %xmm4
	movq	$0, -5360(%rbp)
	movq	-5432(%rbp), %xmm1
	movq	-5448(%rbp), %xmm2
	movhps	-5408(%rbp), %xmm0
	punpcklqdq	%xmm7, %xmm4
	movq	-5464(%rbp), %xmm3
	movaps	%xmm0, -176(%rbp)
	movhps	-5424(%rbp), %xmm1
	movq	-5400(%rbp), %xmm0
	movhps	-5440(%rbp), %xmm2
	movhps	-5456(%rbp), %xmm3
	movaps	%xmm2, -208(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm3, -224(%rbp)
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -192(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm0, -5376(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5728(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L188
	call	_ZdlPv@PLT
.L188:
	movq	-5832(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1184(%rbp), %rax
	cmpq	$0, -5336(%rbp)
	movq	%rax, -5552(%rbp)
	jne	.L393
.L189:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L389:
	movq	-5848(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -5328(%rbp)
	movaps	%xmm0, -5344(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movl	$134678279, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -5344(%rbp)
	movq	%rdx, -5328(%rbp)
	movq	%rdx, -5336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5344(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L200
	call	_ZdlPv@PLT
.L200:
	movq	(%rbx), %rax
	movl	$10, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	movq	8(%rax), %rbx
	movq	(%rax), %r13
	movq	24(%rax), %rcx
	movq	%rbx, -5496(%rbp)
	movq	16(%rax), %rbx
	movq	%rcx, -5808(%rbp)
	movq	%rbx, -5792(%rbp)
	movq	32(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %xmm0
	leaq	-224(%rbp), %rsi
	leaq	-184(%rbp), %rdx
	movhps	-5496(%rbp), %xmm0
	movq	%r14, %rdi
	leaq	-416(%rbp), %r13
	movq	%rbx, -192(%rbp)
	movaps	%xmm0, -224(%rbp)
	movq	-5792(%rbp), %xmm0
	movq	$0, -5328(%rbp)
	movhps	-5808(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -5344(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L201
	call	_ZdlPv@PLT
.L201:
	movq	-5768(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L388:
	movq	-5840(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-224(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-214(%rbp), %rdx
	movabsq	$434042140868675335, %rax
	movaps	%xmm0, -5344(%rbp)
	movq	%rax, -224(%rbp)
	movl	$2056, %eax
	movw	%ax, -216(%rbp)
	movq	$0, -5328(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5752(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5344(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L197
	call	_ZdlPv@PLT
.L197:
	movq	(%rbx), %rax
	movl	$37, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	movq	8(%rax), %rbx
	movq	24(%rax), %rcx
	movq	(%rax), %r12
	movq	%rbx, -5496(%rbp)
	movq	16(%rax), %rbx
	movq	%rcx, -5808(%rbp)
	movq	%rbx, -5792(%rbp)
	movq	72(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$38, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	movhps	-5496(%rbp), %xmm0
	leaq	-184(%rbp), %rdx
	leaq	-608(%rbp), %r12
	movq	%rbx, -192(%rbp)
	movaps	%xmm0, -224(%rbp)
	movq	-5792(%rbp), %xmm0
	movq	$0, -5328(%rbp)
	movhps	-5808(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -5344(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L198
	call	_ZdlPv@PLT
.L198:
	movq	-5848(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L387:
	movq	-5832(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-224(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-212(%rbp), %rdx
	movabsq	$434042140868675335, %rax
	movaps	%xmm0, -5344(%rbp)
	movq	%rax, -224(%rbp)
	movl	$117966856, -216(%rbp)
	movq	$0, -5328(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5728(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5344(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L195
	call	_ZdlPv@PLT
.L195:
	movq	(%rbx), %rax
	movl	$35, %edx
	movq	%r15, %rdi
	leaq	.LC3(%rip), %rsi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-5496(%rbp), %rbx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal16kBuiltinName_362EPNS0_8compiler18CodeAssemblerStateE
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movl	$153, %edx
	movq	%rax, %rcx
	movq	%r12, %rsi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L386:
	movq	-5864(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-224(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$2056, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movabsq	$434042140868675335, %rax
	pxor	%xmm0, %xmm0
	movw	%dx, -216(%rbp)
	leaq	-213(%rbp), %rdx
	movq	%rax, -224(%rbp)
	movaps	%xmm0, -5344(%rbp)
	movb	$8, -214(%rbp)
	movq	$0, -5328(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5552(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5344(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L192
	call	_ZdlPv@PLT
.L192:
	movq	(%rbx), %rax
	leaq	-144(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movdqu	48(%rax), %xmm0
	movdqu	(%rax), %xmm3
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movdqu	64(%rax), %xmm5
	movaps	%xmm0, -176(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -224(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm2, -208(%rbp)
	movaps	%xmm1, -192(%rbp)
	movaps	%xmm0, -5344(%rbp)
	movq	$0, -5328(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-800(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -5752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5344(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L193
	call	_ZdlPv@PLT
.L193:
	movq	-5840(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L178:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IsNullConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L180
	movq	-5984(%rbp), %xmm1
	movq	%rbx, %xmm0
	movq	%r12, %rdi
	movhps	-5968(%rbp), %xmm0
	movhps	-5992(%rbp), %xmm1
	movaps	%xmm0, -6016(%rbp)
	movaps	%xmm1, -5984(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-5344(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -5968(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-6016(%rbp), %xmm0
	movq	-5960(%rbp), %xmm2
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movdqa	-5984(%rbp), %xmm1
	movq	%rax, -5376(%rbp)
	movq	-5328(%rbp), %rax
	movhps	-5968(%rbp), %xmm2
	movaps	%xmm2, -224(%rbp)
	movq	%rax, -5368(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm1, -192(%rbp)
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L391:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movdqa	-5520(%rbp), %xmm6
	movdqa	-5632(%rbp), %xmm3
	leaq	-160(%rbp), %rdx
	movaps	%xmm0, -5376(%rbp)
	movq	$0, -5360(%rbp)
	movaps	%xmm6, -224(%rbp)
	movdqa	-5536(%rbp), %xmm6
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm6, -208(%rbp)
	movdqa	-5664(%rbp), %xmm6
	movaps	%xmm6, -176(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5544(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L134
	call	_ZdlPv@PLT
.L134:
	movq	-5608(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L392:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-5536(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-5696(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	leaq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -128(%rbp)
	movdqa	-5792(%rbp), %xmm7
	movdqa	-5888(%rbp), %xmm6
	movaps	%xmm3, -224(%rbp)
	movdqa	-5904(%rbp), %xmm5
	movdqa	-5920(%rbp), %xmm3
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm0, -5376(%rbp)
	movq	$0, -5360(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5568(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L166
	call	_ZdlPv@PLT
.L166:
	movq	-5856(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L393:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-5464(%rbp), %rdx
	movq	-5400(%rbp), %rax
	movaps	%xmm0, -5376(%rbp)
	movq	$0, -5360(%rbp)
	movq	%rdx, -224(%rbp)
	movq	-5456(%rbp), %rdx
	movq	%rax, -160(%rbp)
	movq	%rdx, -216(%rbp)
	movq	-5448(%rbp), %rdx
	movq	%rax, -152(%rbp)
	movq	%rdx, -208(%rbp)
	movq	-5440(%rbp), %rdx
	movq	%rax, -144(%rbp)
	movq	%rdx, -200(%rbp)
	movq	-5432(%rbp), %rdx
	movq	%rdx, -192(%rbp)
	movq	-5424(%rbp), %rdx
	movq	%rdx, -184(%rbp)
	movq	-5416(%rbp), %rdx
	movq	%rdx, -176(%rbp)
	movq	-5408(%rbp), %rdx
	movq	%rdx, -168(%rbp)
	leaq	-136(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5552(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L190
	call	_ZdlPv@PLT
.L190:
	movq	-5864(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L189
.L390:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22417:
	.size	_ZN2v88internal26ReduceRightAllElements_363EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal26ReduceRightAllElements_363EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE:
.LFB26840:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$5, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$56, %rsp
	movq	%r8, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L395
	movq	%rax, -96(%rbp)
	call	_ZdlPv@PLT
	movq	-96(%rbp), %rax
.L395:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L396
	movq	%rdx, (%r15)
.L396:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L397
	movq	%rdx, (%r14)
.L397:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L398
	movq	%rdx, 0(%r13)
.L398:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L399
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L399:
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L394
	movq	%rax, (%rbx)
.L394:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L421
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L421:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26840:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE
	.section	.text._ZN2v88internal39TypedArrayPrototypeReduceRightAssembler42GenerateTypedArrayPrototypeReduceRightImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal39TypedArrayPrototypeReduceRightAssembler42GenerateTypedArrayPrototypeReduceRightImplEv
	.type	_ZN2v88internal39TypedArrayPrototypeReduceRightAssembler42GenerateTypedArrayPrototypeReduceRightImplEv, @function
_ZN2v88internal39TypedArrayPrototypeReduceRightAssembler42GenerateTypedArrayPrototypeReduceRightImplEv:
.LFB22490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$3352, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, -3168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r13, %rcx
	movq	%r12, %rsi
	leaq	-3120(%rbp), %rdi
	movq	%rax, %rdx
	leaq	-2816(%rbp), %r13
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-3112(%rbp), %rcx
	movl	$2, %esi
	movq	%r12, %rdi
	movq	-3120(%rbp), %rax
	movq	-3104(%rbp), %rbx
	movq	%r12, -2992(%rbp)
	leaq	-3168(%rbp), %r12
	movq	%rcx, -3312(%rbp)
	movq	%rcx, -2968(%rbp)
	movq	$1, -2984(%rbp)
	movq	%rbx, -2976(%rbp)
	movq	%rax, -3328(%rbp)
	movq	%rax, -2960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-2992(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -3336(%rbp)
	movq	%rax, -3296(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -2808(%rbp)
	movq	$0, -2800(%rbp)
	movq	%rax, %r15
	movq	-3168(%rbp), %rax
	movq	$0, -2792(%rbp)
	movq	%rax, -2816(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2760(%rbp), %rcx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -2792(%rbp)
	movq	%rdx, -2800(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3176(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2776(%rbp)
	movq	%rax, -2808(%rbp)
	movq	$0, -2784(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3168(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2616(%rbp)
	movq	$0, -2608(%rbp)
	movq	%rax, -2624(%rbp)
	movq	$0, -2600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2568(%rbp), %rcx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rdx, -2600(%rbp)
	movq	%rdx, -2608(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3272(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2584(%rbp)
	movq	%rax, -2616(%rbp)
	movq	$0, -2592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3168(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2424(%rbp)
	movq	$0, -2416(%rbp)
	movq	%rax, -2432(%rbp)
	movq	$0, -2408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2376(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -2408(%rbp)
	movq	%rdx, -2416(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3200(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2392(%rbp)
	movq	%rax, -2424(%rbp)
	movq	$0, -2400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3168(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2232(%rbp)
	movq	$0, -2224(%rbp)
	movq	%rax, -2240(%rbp)
	movq	$0, -2216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2184(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -2216(%rbp)
	movq	%rdx, -2224(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3280(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2200(%rbp)
	movq	%rax, -2232(%rbp)
	movq	$0, -2208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3168(%rbp), %rax
	movl	$192, %edi
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movq	%rax, -2048(%rbp)
	movq	$0, -2024(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1992(%rbp), %rcx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -2024(%rbp)
	movq	%rdx, -2032(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3192(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2008(%rbp)
	movq	%rax, -2040(%rbp)
	movq	$0, -2016(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3168(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1848(%rbp)
	movq	$0, -1840(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1832(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1800(%rbp), %rcx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -1832(%rbp)
	movq	%rdx, -1840(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3264(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1816(%rbp)
	movq	%rax, -1848(%rbp)
	movq	$0, -1824(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3168(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1656(%rbp)
	movq	$0, -1648(%rbp)
	movq	%rax, -1664(%rbp)
	movq	$0, -1640(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-1608(%rbp), %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -1640(%rbp)
	movq	%rdx, -1648(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -3240(%rbp)
	movq	%r12, %rsi
	movups	%xmm0, -1624(%rbp)
	movq	%rax, -1656(%rbp)
	movq	$0, -1632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3168(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1464(%rbp)
	movq	$0, -1456(%rbp)
	movq	%rax, -1472(%rbp)
	movq	$0, -1448(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1416(%rbp), %rcx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -1448(%rbp)
	movq	%rdx, -1456(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3256(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1432(%rbp)
	movq	%rax, -1464(%rbp)
	movq	$0, -1440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3168(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1272(%rbp)
	movq	$0, -1264(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -1256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-1224(%rbp), %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -1256(%rbp)
	movq	%rdx, -1264(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -3216(%rbp)
	movq	%r12, %rsi
	movups	%xmm0, -1240(%rbp)
	movq	%rax, -1272(%rbp)
	movq	$0, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3168(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1080(%rbp)
	movq	$0, -1072(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$0, -1064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1032(%rbp), %rcx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -1064(%rbp)
	movq	%rdx, -1072(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3232(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1048(%rbp)
	movq	%rax, -1080(%rbp)
	movq	$0, -1056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3168(%rbp), %rax
	movl	$216, %edi
	movq	$0, -888(%rbp)
	movq	$0, -880(%rbp)
	movq	%rax, -896(%rbp)
	movq	$0, -872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-840(%rbp), %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -872(%rbp)
	movq	%rdx, -880(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -3184(%rbp)
	movq	%r12, %rsi
	movups	%xmm0, -856(%rbp)
	movq	%rax, -888(%rbp)
	movq	$0, -864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3168(%rbp), %rax
	movl	$120, %edi
	movq	$0, -696(%rbp)
	movq	$0, -688(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	leaq	-648(%rbp), %rcx
	movq	$0, 112(%rax)
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -680(%rbp)
	movq	%rdx, -688(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3248(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -664(%rbp)
	movq	%rax, -696(%rbp)
	movq	$0, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3168(%rbp), %rax
	movl	$120, %edi
	movq	$0, -504(%rbp)
	movq	$0, -496(%rbp)
	movq	%rax, -512(%rbp)
	movq	$0, -488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	120(%rax), %rdx
	leaq	-456(%rbp), %rsi
	movq	$0, 112(%rax)
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -488(%rbp)
	movq	%rdx, -496(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -3208(%rbp)
	movq	%r12, %rsi
	movups	%xmm0, -472(%rbp)
	movq	%rax, -504(%rbp)
	movq	$0, -480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3168(%rbp), %rax
	movl	$120, %edi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	leaq	-264(%rbp), %rcx
	movq	$0, 112(%rax)
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	movq	%rcx, -3224(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, -312(%rbp)
	movups	%xmm0, -280(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	-3328(%rbp), %xmm1
	movq	%r15, -96(%rbp)
	leaq	-2944(%rbp), %r15
	movhps	-3312(%rbp), %xmm1
	movaps	%xmm0, -2944(%rbp)
	movaps	%xmm1, -128(%rbp)
	movq	%rbx, %xmm1
	movhps	-3296(%rbp), %xmm1
	movq	$0, -2928(%rbp)
	movaps	%xmm1, -112(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r13, %rdi
	movq	%r15, %rsi
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm5
	leaq	40(%rax), %rdx
	movq	%rax, -2944(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L423
	call	_ZdlPv@PLT
.L423:
	movq	-3176(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2752(%rbp)
	jne	.L748
	cmpq	$0, -2560(%rbp)
	jne	.L749
.L429:
	cmpq	$0, -2368(%rbp)
	jne	.L750
.L432:
	cmpq	$0, -2176(%rbp)
	jne	.L751
.L437:
	cmpq	$0, -1984(%rbp)
	jne	.L752
.L440:
	cmpq	$0, -1792(%rbp)
	jne	.L753
.L445:
	cmpq	$0, -1600(%rbp)
	jne	.L754
.L448:
	cmpq	$0, -1408(%rbp)
	jne	.L755
.L452:
	cmpq	$0, -1216(%rbp)
	jne	.L756
.L455:
	cmpq	$0, -1024(%rbp)
	jne	.L757
.L458:
	cmpq	$0, -832(%rbp)
	jne	.L758
.L461:
	cmpq	$0, -640(%rbp)
	jne	.L759
.L467:
	cmpq	$0, -448(%rbp)
	jne	.L760
.L469:
	cmpq	$0, -256(%rbp)
	jne	.L761
.L471:
	movq	-3224(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L472
	call	_ZdlPv@PLT
.L472:
	movq	-304(%rbp), %rbx
	movq	-312(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L473
	.p2align 4,,10
	.p2align 3
.L477:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L474
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L477
.L475:
	movq	-312(%rbp), %r13
.L473:
	testq	%r13, %r13
	je	.L478
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L478:
	movq	-3208(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L479
	call	_ZdlPv@PLT
.L479:
	movq	-496(%rbp), %rbx
	movq	-504(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L480
	.p2align 4,,10
	.p2align 3
.L484:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L481
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L484
.L482:
	movq	-504(%rbp), %r13
.L480:
	testq	%r13, %r13
	je	.L485
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L485:
	movq	-3248(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L486
	call	_ZdlPv@PLT
.L486:
	movq	-688(%rbp), %rbx
	movq	-696(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L487
	.p2align 4,,10
	.p2align 3
.L491:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L488
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L491
.L489:
	movq	-696(%rbp), %r13
.L487:
	testq	%r13, %r13
	je	.L492
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L492:
	movq	-3184(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-864(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L493
	call	_ZdlPv@PLT
.L493:
	movq	-880(%rbp), %rbx
	movq	-888(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L494
	.p2align 4,,10
	.p2align 3
.L498:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L495
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L498
.L496:
	movq	-888(%rbp), %r13
.L494:
	testq	%r13, %r13
	je	.L499
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L499:
	movq	-3232(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L500
	call	_ZdlPv@PLT
.L500:
	movq	-1072(%rbp), %rbx
	movq	-1080(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L501
	.p2align 4,,10
	.p2align 3
.L505:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L502
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L505
.L503:
	movq	-1080(%rbp), %r13
.L501:
	testq	%r13, %r13
	je	.L506
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L506:
	movq	-3216(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L507
	call	_ZdlPv@PLT
.L507:
	movq	-1264(%rbp), %rbx
	movq	-1272(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L508
	.p2align 4,,10
	.p2align 3
.L512:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L509
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L512
.L510:
	movq	-1272(%rbp), %r13
.L508:
	testq	%r13, %r13
	je	.L513
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L513:
	movq	-3256(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1440(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L514
	call	_ZdlPv@PLT
.L514:
	movq	-1456(%rbp), %rbx
	movq	-1464(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L515
	.p2align 4,,10
	.p2align 3
.L519:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L516
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L519
.L517:
	movq	-1464(%rbp), %r13
.L515:
	testq	%r13, %r13
	je	.L520
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L520:
	movq	-3240(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1632(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L521
	call	_ZdlPv@PLT
.L521:
	movq	-1648(%rbp), %rbx
	movq	-1656(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L522
	.p2align 4,,10
	.p2align 3
.L526:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L523
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L526
.L524:
	movq	-1656(%rbp), %r13
.L522:
	testq	%r13, %r13
	je	.L527
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L527:
	movq	-3264(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L528
	call	_ZdlPv@PLT
.L528:
	movq	-1840(%rbp), %rbx
	movq	-1848(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L529
	.p2align 4,,10
	.p2align 3
.L533:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L530
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L533
.L531:
	movq	-1848(%rbp), %r13
.L529:
	testq	%r13, %r13
	je	.L534
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L534:
	movq	-3192(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2016(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L535
	call	_ZdlPv@PLT
.L535:
	movq	-2032(%rbp), %rbx
	movq	-2040(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L536
	.p2align 4,,10
	.p2align 3
.L540:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L537
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L540
.L538:
	movq	-2040(%rbp), %r13
.L536:
	testq	%r13, %r13
	je	.L541
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L541:
	movq	-3280(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L542
	call	_ZdlPv@PLT
.L542:
	movq	-2224(%rbp), %rbx
	movq	-2232(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L543
	.p2align 4,,10
	.p2align 3
.L547:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L544
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L547
.L545:
	movq	-2232(%rbp), %r13
.L543:
	testq	%r13, %r13
	je	.L548
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L548:
	movq	-3200(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L549
	call	_ZdlPv@PLT
.L549:
	movq	-2416(%rbp), %rbx
	movq	-2424(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L550
	.p2align 4,,10
	.p2align 3
.L554:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L551
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L554
.L552:
	movq	-2424(%rbp), %r13
.L550:
	testq	%r13, %r13
	je	.L555
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L555:
	movq	-3272(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L556
	call	_ZdlPv@PLT
.L556:
	movq	-2608(%rbp), %rbx
	movq	-2616(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L557
	.p2align 4,,10
	.p2align 3
.L561:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L558
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L561
.L559:
	movq	-2616(%rbp), %r13
.L557:
	testq	%r13, %r13
	je	.L562
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L562:
	movq	-3176(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L563
	call	_ZdlPv@PLT
.L563:
	movq	-2800(%rbp), %rbx
	movq	-2808(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L564
	.p2align 4,,10
	.p2align 3
.L568:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L565
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L568
.L566:
	movq	-2808(%rbp), %r13
.L564:
	testq	%r13, %r13
	je	.L569
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L569:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L762
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L565:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L568
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L558:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L561
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L551:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L554
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L544:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L547
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L537:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L540
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L530:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L533
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L523:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L526
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L516:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L519
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L509:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L512
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L502:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L505
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L495:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L498
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L488:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L491
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L474:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L477
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L481:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L484
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L748:
	movq	-3176(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2928(%rbp)
	movaps	%xmm0, -2944(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2944(%rbp)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2944(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L425
	call	_ZdlPv@PLT
.L425:
	movq	(%rbx), %rax
	movl	$50, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rsi
	movq	24(%rax), %r13
	movq	32(%rax), %rbx
	movq	%rcx, -3312(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -3296(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%rcx, -3328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal22Cast12JSTypedArray_110EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %xmm3
	movq	%rbx, %xmm6
	movq	-3328(%rbp), %xmm7
	punpcklqdq	%xmm6, %xmm6
	pxor	%xmm0, %xmm0
	movq	%rax, -80(%rbp)
	movq	-3296(%rbp), %xmm2
	punpcklqdq	%xmm3, %xmm7
	movl	$56, %edi
	movaps	%xmm6, -96(%rbp)
	leaq	-3024(%rbp), %r13
	movhps	-3312(%rbp), %xmm2
	movaps	%xmm6, -3360(%rbp)
	movaps	%xmm7, -3328(%rbp)
	movaps	%xmm2, -3296(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm0, -3024(%rbp)
	movq	$0, -3008(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm2
	movq	-80(%rbp), %rcx
	leaq	-2432(%rbp), %rdi
	movdqa	-112(%rbp), %xmm3
	leaq	56(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -3024(%rbp)
	movups	%xmm2, (%rax)
	movdqa	-96(%rbp), %xmm2
	movq	%rcx, 48(%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm2, 32(%rax)
	movq	%rdx, -3008(%rbp)
	movq	%rdx, -3016(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L426
	call	_ZdlPv@PLT
.L426:
	movq	-3200(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2936(%rbp)
	jne	.L763
.L427:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2560(%rbp)
	je	.L429
.L749:
	movq	-3272(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2624(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2928(%rbp)
	movaps	%xmm0, -2944(%rbp)
	call	_Znwm@PLT
	movl	$2056, %edi
	movq	%r15, %rsi
	movw	%di, 4(%rax)
	leaq	6(%rax), %rdx
	movq	%r13, %rdi
	movl	$117769477, (%rax)
	movq	%rax, -2944(%rbp)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2944(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L430
	call	_ZdlPv@PLT
.L430:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	32(%rax), %rdx
	movdqu	(%rax), %xmm3
	movq	$0, -2928(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm0, -2944(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-128(%rbp), %xmm2
	movdqa	-112(%rbp), %xmm4
	leaq	40(%rax), %rdx
	leaq	-512(%rbp), %rdi
	movq	%rax, -2944(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L431
	call	_ZdlPv@PLT
.L431:
	movq	-3208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2368(%rbp)
	je	.L432
.L750:
	movq	-3200(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2432(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2928(%rbp)
	movaps	%xmm0, -2944(%rbp)
	call	_Znwm@PLT
	movl	$2056, %esi
	movq	%r13, %rdi
	movw	%si, 4(%rax)
	leaq	7(%rax), %rdx
	movq	%r15, %rsi
	movl	$117769477, (%rax)
	movb	$7, 6(%rax)
	movq	%rax, -2944(%rbp)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2944(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L433
	call	_ZdlPv@PLT
.L433:
	movq	(%rbx), %rax
	movl	$52, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rsi
	movq	48(%rax), %rbx
	movq	16(%rax), %r13
	movq	%rcx, -3312(%rbp)
	movq	24(%rax), %rcx
	movq	%rsi, -3296(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%rcx, -3328(%rbp)
	movq	32(%rax), %rcx
	movq	%rcx, -3360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18EnsureAttached_369EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_12JSTypedArrayEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm5
	pxor	%xmm0, %xmm0
	movq	-3360(%rbp), %xmm4
	movq	-3296(%rbp), %xmm6
	movl	$64, %edi
	movaps	%xmm0, -3024(%rbp)
	punpcklqdq	%xmm5, %xmm4
	movq	%r13, %xmm5
	movq	%rbx, -80(%rbp)
	leaq	-3024(%rbp), %r13
	movhps	-3328(%rbp), %xmm5
	movhps	-3312(%rbp), %xmm6
	movaps	%xmm4, -3360(%rbp)
	movaps	%xmm5, -3328(%rbp)
	movaps	%xmm6, -3296(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -3008(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm2
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	leaq	64(%rax), %rdx
	leaq	-2048(%rbp), %rdi
	movups	%xmm3, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rax, -3024(%rbp)
	movq	%rdx, -3008(%rbp)
	movq	%rdx, -3016(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L434
	call	_ZdlPv@PLT
.L434:
	movq	-3192(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2936(%rbp)
	jne	.L764
.L435:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2176(%rbp)
	je	.L437
.L751:
	movq	-3280(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2240(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2928(%rbp)
	movaps	%xmm0, -2944(%rbp)
	call	_Znwm@PLT
	movl	$1800, %ecx
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$117769477, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -2944(%rbp)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2944(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L438
	call	_ZdlPv@PLT
.L438:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	32(%rax), %rdx
	movdqu	(%rax), %xmm3
	movq	$0, -2928(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm0, -2944(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-128(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm2
	leaq	40(%rax), %rdx
	leaq	-320(%rbp), %rdi
	movq	%rax, -2944(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L439
	call	_ZdlPv@PLT
.L439:
	movq	-3224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1984(%rbp)
	je	.L440
.L752:
	movq	-3192(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2048(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2928(%rbp)
	movaps	%xmm0, -2944(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movabsq	$506381214161372421, %rsi
	movq	%rsi, (%rax)
	leaq	8(%rax), %rdx
	movq	%r15, %rsi
	movq	%rax, -2944(%rbp)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2944(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L441
	call	_ZdlPv@PLT
.L441:
	movq	(%rbx), %rax
	movl	$54, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	movq	8(%rax), %rcx
	movq	24(%rax), %r13
	movq	40(%rax), %rbx
	movq	%rsi, -3312(%rbp)
	movq	16(%rax), %rsi
	movq	%rcx, -3328(%rbp)
	movq	32(%rax), %rcx
	movq	56(%rax), %rax
	movq	%rsi, -3296(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%rcx, -3360(%rbp)
	movq	%rbx, -3344(%rbp)
	movq	%rax, -3376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3296(%rbp), %rsi
	subq	$8, %rsp
	movq	%r15, %rdi
	movq	-3312(%rbp), %xmm3
	movq	%rsi, -3072(%rbp)
	movq	%rbx, %rsi
	movhps	-3328(%rbp), %xmm3
	pushq	-3072(%rbp)
	movaps	%xmm3, -3088(%rbp)
	pushq	-3080(%rbp)
	pushq	-3088(%rbp)
	movaps	%xmm3, -3312(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal82Cast71UT19ATCallableApiObject17ATCallableJSProxy15JSBoundFunction10JSFunction_1405EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %xmm5
	movq	%rbx, %xmm6
	movq	-3360(%rbp), %xmm2
	movdqa	-3312(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movq	-3296(%rbp), %xmm4
	movq	-3376(%rbp), %xmm7
	movhps	-3344(%rbp), %xmm2
	movq	%rax, -64(%rbp)
	leaq	-3024(%rbp), %r13
	punpcklqdq	%xmm5, %xmm4
	movaps	%xmm2, -3360(%rbp)
	punpcklqdq	%xmm6, %xmm7
	movaps	%xmm4, -3296(%rbp)
	movaps	%xmm7, -3328(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	movaps	%xmm0, -3024(%rbp)
	movq	$0, -3008(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	leaq	-1664(%rbp), %rdi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm7
	leaq	72(%rax), %rdx
	movq	%r13, %rsi
	movdqa	-80(%rbp), %xmm2
	movq	%rcx, 64(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm2, 48(%rax)
	movq	%rax, -3024(%rbp)
	movq	%rdx, -3008(%rbp)
	movq	%rdx, -3016(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L442
	call	_ZdlPv@PLT
.L442:
	movq	-3240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2936(%rbp)
	jne	.L765
.L443:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1792(%rbp)
	je	.L445
.L753:
	movq	-3264(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1856(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2928(%rbp)
	movaps	%xmm0, -2944(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movabsq	$578438808199300357, %rsi
	movq	%rsi, (%rax)
	leaq	8(%rax), %rdx
	movq	%r15, %rsi
	movq	%rax, -2944(%rbp)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2944(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L446
	call	_ZdlPv@PLT
.L446:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	32(%rax), %rdx
	movdqu	(%rax), %xmm6
	movq	$0, -2928(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm0, -2944(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm5
	leaq	40(%rax), %rdx
	leaq	-704(%rbp), %rdi
	movq	%rax, -2944(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L447
	call	_ZdlPv@PLT
.L447:
	movq	-3248(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1600(%rbp)
	je	.L448
.L754:
	movq	-3240(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1664(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -2928(%rbp)
	movaps	%xmm0, -2944(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movabsq	$578438808199300357, %rsi
	movq	%rsi, (%rax)
	leaq	9(%rax), %rdx
	movq	%r15, %rsi
	movb	$7, 8(%rax)
	movq	%rax, -2944(%rbp)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2944(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L449
	call	_ZdlPv@PLT
.L449:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	40(%rax), %rdx
	movq	(%rax), %rsi
	movq	16(%rax), %rbx
	movq	%rcx, -3312(%rbp)
	movq	24(%rax), %rcx
	movq	%rdx, -3344(%rbp)
	movq	48(%rax), %rdx
	movq	%rcx, -3328(%rbp)
	movq	32(%rax), %rcx
	movq	64(%rax), %rax
	movq	%rsi, -3296(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%rdx, -3376(%rbp)
	movl	$55, %edx
	movq	%rcx, -3360(%rbp)
	movq	%rax, -3384(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$2, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler24IntPtrGreaterThanOrEqualENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm7
	pxor	%xmm0, %xmm0
	movq	-3376(%rbp), %xmm3
	movq	-3360(%rbp), %xmm5
	movhps	-3328(%rbp), %xmm7
	movq	-3296(%rbp), %xmm6
	movl	$64, %edi
	movhps	-3384(%rbp), %xmm3
	movaps	%xmm7, -3328(%rbp)
	movhps	-3344(%rbp), %xmm5
	movhps	-3312(%rbp), %xmm6
	movaps	%xmm3, -3376(%rbp)
	movaps	%xmm5, -3360(%rbp)
	movaps	%xmm6, -3296(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm0, -2944(%rbp)
	movq	$0, -2928(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm7
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm6
	leaq	64(%rax), %rdx
	leaq	-1472(%rbp), %rdi
	movups	%xmm3, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm6, 48(%rax)
	movq	%rax, -2944(%rbp)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L450
	call	_ZdlPv@PLT
.L450:
	movdqa	-3296(%rbp), %xmm4
	movdqa	-3328(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movdqa	-3360(%rbp), %xmm3
	movdqa	-3376(%rbp), %xmm7
	movaps	%xmm0, -2944(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	movq	$0, -2928(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm2
	movdqa	-112(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	leaq	64(%rax), %rdx
	leaq	-1280(%rbp), %rdi
	movups	%xmm2, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rax, -2944(%rbp)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L451
	call	_ZdlPv@PLT
.L451:
	movq	-3216(%rbp), %rcx
	movq	-3256(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1408(%rbp)
	je	.L452
.L755:
	movq	-3256(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1472(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2928(%rbp)
	movaps	%xmm0, -2944(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movabsq	$506381214161372421, %rsi
	movq	%rsi, (%rax)
	leaq	8(%rax), %rdx
	movq	%r15, %rsi
	movq	%rax, -2944(%rbp)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2944(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L453
	call	_ZdlPv@PLT
.L453:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	8(%rax), %rcx
	movq	40(%rax), %rdx
	movq	(%rax), %rsi
	movq	16(%rax), %rbx
	movq	%rcx, -3312(%rbp)
	movq	24(%rax), %rcx
	movq	%rdx, -3344(%rbp)
	movq	48(%rax), %rdx
	movq	%rcx, -3328(%rbp)
	movq	32(%rax), %rcx
	movq	56(%rax), %rax
	movq	%rsi, -3296(%rbp)
	movl	$1, %esi
	movq	%rcx, -3360(%rbp)
	movq	%rdx, -3376(%rbp)
	movq	%rax, -3384(%rbp)
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	-3296(%rbp), %xmm0
	movq	%rbx, -3040(%rbp)
	pushq	-3040(%rbp)
	movhps	-3312(%rbp), %xmm0
	movaps	%xmm0, -3056(%rbp)
	pushq	-3048(%rbp)
	pushq	-3056(%rbp)
	movaps	%xmm0, -3296(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$72, %edi
	movq	%r13, -64(%rbp)
	movdqa	-3296(%rbp), %xmm0
	movq	$0, -2928(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	%rbx, %xmm0
	movhps	-3328(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-3360(%rbp), %xmm0
	movhps	-3344(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-3376(%rbp), %xmm0
	movhps	-3384(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2944(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-128(%rbp), %xmm3
	leaq	-1088(%rbp), %rdi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm2
	leaq	72(%rax), %rdx
	movq	%r15, %rsi
	movdqa	-80(%rbp), %xmm6
	movq	%rcx, 64(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm6, 48(%rax)
	movq	%rax, -2944(%rbp)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L454
	call	_ZdlPv@PLT
.L454:
	movq	-3232(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1216(%rbp)
	je	.L455
.L756:
	movq	-3216(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1280(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2928(%rbp)
	movaps	%xmm0, -2944(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movabsq	$506381214161372421, %rsi
	movq	%rsi, (%rax)
	leaq	8(%rax), %rdx
	movq	%r15, %rsi
	movq	%rax, -2944(%rbp)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2944(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L456
	call	_ZdlPv@PLT
.L456:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	24(%rax), %rbx
	movq	8(%rax), %rcx
	movq	(%rax), %rsi
	movq	32(%rax), %r13
	movq	%rbx, -3360(%rbp)
	movq	40(%rax), %rbx
	movq	%rcx, -3312(%rbp)
	movq	16(%rax), %rcx
	movq	%rbx, -3344(%rbp)
	movq	48(%rax), %rbx
	movq	56(%rax), %rax
	movq	%rsi, -3296(%rbp)
	movq	%rcx, -3328(%rbp)
	movq	%rax, -3376(%rbp)
	call	_ZN2v88internal10TheHole_62EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$72, %edi
	movq	-3296(%rbp), %xmm0
	movq	$0, -2928(%rbp)
	movq	%rax, -64(%rbp)
	movhps	-3312(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-3328(%rbp), %xmm0
	movhps	-3360(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%r13, %xmm0
	movhps	-3344(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	movhps	-3376(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2944(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	leaq	-896(%rbp), %rdi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm3
	leaq	72(%rax), %rdx
	movq	%r15, %rsi
	movdqa	-80(%rbp), %xmm7
	movq	%rcx, 64(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rax, -2944(%rbp)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L457
	call	_ZdlPv@PLT
.L457:
	movq	-3184(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1024(%rbp)
	je	.L458
.L757:
	movq	-3232(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1088(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -2928(%rbp)
	movaps	%xmm0, -2944(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movabsq	$506381214161372421, %rsi
	movq	%rsi, (%rax)
	leaq	9(%rax), %rdx
	movq	%r15, %rsi
	movb	$8, 8(%rax)
	movq	%rax, -2944(%rbp)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2944(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L459
	call	_ZdlPv@PLT
.L459:
	movq	(%rbx), %rax
	movl	$72, %edi
	movdqu	48(%rax), %xmm0
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	64(%rax), %rdx
	movdqu	(%rax), %xmm6
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -64(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -2944(%rbp)
	movq	$0, -2928(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-128(%rbp), %xmm2
	leaq	-896(%rbp), %rdi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	leaq	72(%rax), %rdx
	movq	%r15, %rsi
	movdqa	-80(%rbp), %xmm3
	movq	%rcx, 64(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rax, -2944(%rbp)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L460
	call	_ZdlPv@PLT
.L460:
	movq	-3184(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -832(%rbp)
	je	.L461
.L758:
	movq	-3184(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	leaq	-896(%rbp), %r8
	movq	$0, -3296(%rbp)
	movq	%r8, -3328(%rbp)
	movq	$0, -3312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -2928(%rbp)
	movaps	%xmm0, -2944(%rbp)
	call	_Znwm@PLT
	movq	-3328(%rbp), %r8
	movabsq	$506381214161372421, %rsi
	movq	%rsi, (%rax)
	leaq	9(%rax), %rdx
	movq	%r15, %rsi
	movb	$8, 8(%rax)
	movq	%r8, %rdi
	movq	%rax, -2944(%rbp)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2944(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L462
	movq	%rax, -3328(%rbp)
	call	_ZdlPv@PLT
	movq	-3328(%rbp), %rax
.L462:
	movq	(%rax), %rax
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	cmovne	%rdx, %r13
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	cmove	-3296(%rbp), %rdx
	movq	%rdx, -3296(%rbp)
	movq	56(%rax), %rdx
	movq	64(%rax), %rax
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	cmove	-3312(%rbp), %rcx
	movl	$57, %edx
	testq	%rax, %rax
	cmovne	%rax, %rbx
	movq	%rcx, -3312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3312(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-3296(%rbp), %rdx
	movq	%rbx, %r8
	call	_ZN2v88internal26ReduceRightAllElements_363EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_12JSTypedArrayEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEE
	movq	-3336(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	cmpq	$0, -640(%rbp)
	je	.L467
.L759:
	movq	-3248(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-704(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2928(%rbp)
	movaps	%xmm0, -2944(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2944(%rbp)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2944(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L468
	call	_ZdlPv@PLT
.L468:
	movq	(%rbx), %rax
	movl	$60, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rsi
	movq	24(%rax), %rbx
	movq	%rcx, -3312(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -3296(%rbp)
	leaq	.LC3(%rip), %rsi
	movq	%rcx, -3328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	-3296(%rbp), %xmm0
	movq	-3328(%rbp), %rcx
	movhps	-3312(%rbp), %xmm0
	movq	%rcx, -3008(%rbp)
	movaps	%xmm0, -3024(%rbp)
	pushq	-3008(%rbp)
	pushq	-3016(%rbp)
	pushq	-3024(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$25, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -448(%rbp)
	je	.L469
.L760:
	movq	-3208(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-512(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2928(%rbp)
	movaps	%xmm0, -2944(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2944(%rbp)
	movq	%rdx, -2928(%rbp)
	movq	%rdx, -2936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2944(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L470
	call	_ZdlPv@PLT
.L470:
	movq	(%rbx), %rax
	movl	$63, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	movq	24(%rax), %r13
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal16kBuiltinName_362EPNS0_8compiler18CodeAssemblerStateE
	xorl	%r8d, %r8d
	movl	$100, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -256(%rbp)
	je	.L471
.L761:
	movq	-3224(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-320(%rbp), %r13
	movq	$0, -3160(%rbp)
	movq	$0, -3152(%rbp)
	movq	$0, -3144(%rbp)
	movq	$0, -3136(%rbp)
	movq	$0, -3128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3144(%rbp), %rcx
	movq	%r13, %rdi
	leaq	-3128(%rbp), %r9
	leaq	-3136(%rbp), %r8
	leaq	-3152(%rbp), %rdx
	leaq	-3160(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EESA_PNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE
	movl	$66, %edx
	movq	%r12, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal16kBuiltinName_362EPNS0_8compiler18CodeAssemblerStateE
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movl	$50, %edx
	movq	-3136(%rbp), %rsi
	movq	%rax, %rcx
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L763:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movdqa	-3296(%rbp), %xmm7
	movdqa	-3328(%rbp), %xmm2
	movdqa	-3360(%rbp), %xmm6
	movaps	%xmm0, -3024(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movq	$0, -3008(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm7
	leaq	48(%rax), %rdx
	leaq	-2624(%rbp), %rdi
	movq	%rax, -3024(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -3008(%rbp)
	movq	%rdx, -3016(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L428
	call	_ZdlPv@PLT
.L428:
	movq	-3272(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L764:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %rsi
	movq	%r13, %rdi
	movdqa	-3296(%rbp), %xmm2
	movdqa	-3328(%rbp), %xmm6
	leaq	-72(%rbp), %rdx
	movaps	%xmm0, -3024(%rbp)
	movdqa	-3360(%rbp), %xmm4
	movq	%rbx, -80(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movq	$0, -3008(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2240(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L436
	call	_ZdlPv@PLT
.L436:
	movq	-3280(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L765:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-3312(%rbp), %xmm5
	movq	%r13, %rdi
	movdqa	-3296(%rbp), %xmm7
	movdqa	-3360(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %rsi
	leaq	-64(%rbp), %rdx
	movdqa	-3328(%rbp), %xmm6
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm6, -80(%rbp)
	movaps	%xmm0, -3024(%rbp)
	movq	$0, -3008(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1856(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L444
	call	_ZdlPv@PLT
.L444:
	movq	-3264(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L443
.L762:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22490:
	.size	_ZN2v88internal39TypedArrayPrototypeReduceRightAssembler42GenerateTypedArrayPrototypeReduceRightImplEv, .-_ZN2v88internal39TypedArrayPrototypeReduceRightAssembler42GenerateTypedArrayPrototypeReduceRightImplEv
	.section	.rodata._ZN2v88internal8Builtins39Generate_TypedArrayPrototypeReduceRightEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/typed-array-reduceright-tq-csa.cc"
	.align 8
.LC7:
	.string	"TypedArrayPrototypeReduceRight"
	.section	.text._ZN2v88internal8Builtins39Generate_TypedArrayPrototypeReduceRightEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins39Generate_TypedArrayPrototypeReduceRightEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins39Generate_TypedArrayPrototypeReduceRightEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins39Generate_TypedArrayPrototypeReduceRightEPNS0_8compiler18CodeAssemblerStateE:
.LFB22486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$677, %ecx
	leaq	.LC6(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$922, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L770
.L767:
	movq	%r13, %rdi
	call	_ZN2v88internal39TypedArrayPrototypeReduceRightAssembler42GenerateTypedArrayPrototypeReduceRightImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L771
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L770:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L767
.L771:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22486:
	.size	_ZN2v88internal8Builtins39Generate_TypedArrayPrototypeReduceRightEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins39Generate_TypedArrayPrototypeReduceRightEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal16kBuiltinName_362EPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal16kBuiltinName_362EPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal16kBuiltinName_362EPNS0_8compiler18CodeAssemblerStateE:
.LFB29386:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29386:
	.size	_GLOBAL__sub_I__ZN2v88internal16kBuiltinName_362EPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal16kBuiltinName_362EPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal16kBuiltinName_362EPNS0_8compiler18CodeAssemblerStateE
	.weak	_ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_13JSArrayBufferEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_8UintPtrTEvE5valueE:
	.byte	5
	.byte	5
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC5:
	.byte	7
	.byte	7
	.byte	7
	.byte	8
	.byte	7
	.byte	7
	.byte	6
	.byte	6
	.byte	8
	.byte	6
	.byte	8
	.byte	8
	.byte	8
	.byte	7
	.byte	7
	.byte	7
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
