	.file	"regexp-source-tq-csa.cc"
	.text
	.section	.rodata._ZN2v88internal18Cast8JSRegExp_1467EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../../deps/v8/../../deps/v8/src/builtins/base.tq"
	.align 8
.LC1:
	.string	"../../deps/v8/../../deps/v8/src/builtins/regexp-source.tq"
	.section	.text._ZN2v88internal18Cast8JSRegExp_1467EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18Cast8JSRegExp_1467EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal18Cast8JSRegExp_1467EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal18Cast8JSRegExp_1467EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE:
.LFB22434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	leaq	-1632(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1760(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1800(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1864, %rsp
	movq	%rsi, -1888(%rbp)
	movq	%rdx, -1904(%rbp)
	movq	%rcx, -1872(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1800(%rbp)
	movq	%rdi, -1632(%rbp)
	movl	$48, %edi
	movq	$0, -1624(%rbp)
	movq	$0, -1616(%rbp)
	movq	$0, -1608(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1624(%rbp)
	leaq	-1576(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1608(%rbp)
	movq	%rdx, -1616(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1592(%rbp)
	movq	%rax, -1816(%rbp)
	movq	$0, -1600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1432(%rbp)
	movq	$0, -1424(%rbp)
	movq	%rax, -1440(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1432(%rbp)
	leaq	-1384(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -1832(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1864(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$96, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1840(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1848(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	$0, -472(%rbp)
	movq	%rax, -480(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1824(%rbp)
	movq	$0, -464(%rbp)
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	$0, -440(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1888(%rbp), %xmm1
	movaps	%xmm0, -1760(%rbp)
	movhps	-1904(%rbp), %xmm1
	movq	$0, -1744(%rbp)
	movaps	%xmm1, -1888(%rbp)
	call	_Znwm@PLT
	movdqa	-1888(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2
	call	_ZdlPv@PLT
.L2:
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1568(%rbp)
	jne	.L187
	cmpq	$0, -1376(%rbp)
	jne	.L188
.L8:
	cmpq	$0, -1184(%rbp)
	jne	.L189
.L11:
	cmpq	$0, -992(%rbp)
	jne	.L190
.L16:
	cmpq	$0, -800(%rbp)
	jne	.L191
.L19:
	cmpq	$0, -608(%rbp)
	leaq	-288(%rbp), %r15
	jne	.L192
	cmpq	$0, -416(%rbp)
	jne	.L193
.L25:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L27
	call	_ZdlPv@PLT
.L27:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L28
	call	_ZdlPv@PLT
.L28:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L29
	.p2align 4,,10
	.p2align 3
.L33:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L30
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L33
.L31:
	movq	-280(%rbp), %r14
.L29:
	testq	%r14, %r14
	je	.L34
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L34:
	movq	-1824(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L35
	call	_ZdlPv@PLT
.L35:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L36
	.p2align 4,,10
	.p2align 3
.L40:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L37
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L40
.L38:
	movq	-472(%rbp), %r14
.L36:
	testq	%r14, %r14
	je	.L41
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L41:
	movq	-1848(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L42
	call	_ZdlPv@PLT
.L42:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L43
	.p2align 4,,10
	.p2align 3
.L47:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L44
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L47
.L45:
	movq	-664(%rbp), %r14
.L43:
	testq	%r14, %r14
	je	.L48
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L48:
	movq	-1840(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L49
	call	_ZdlPv@PLT
.L49:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L50
	.p2align 4,,10
	.p2align 3
.L54:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L51
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L54
.L52:
	movq	-856(%rbp), %r14
.L50:
	testq	%r14, %r14
	je	.L55
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L55:
	movq	-1864(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L56
	call	_ZdlPv@PLT
.L56:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L57
	.p2align 4,,10
	.p2align 3
.L61:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L58
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L61
.L59:
	movq	-1048(%rbp), %r14
.L57:
	testq	%r14, %r14
	je	.L62
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L62:
	movq	-1832(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L63
	call	_ZdlPv@PLT
.L63:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L64
	.p2align 4,,10
	.p2align 3
.L68:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L65
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L68
.L66:
	movq	-1240(%rbp), %r14
.L64:
	testq	%r14, %r14
	je	.L69
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L69:
	movq	-1856(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L70
	call	_ZdlPv@PLT
.L70:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L71
	.p2align 4,,10
	.p2align 3
.L75:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L72
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L75
.L73:
	movq	-1432(%rbp), %r14
.L71:
	testq	%r14, %r14
	je	.L76
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L76:
	movq	-1816(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L77
	call	_ZdlPv@PLT
.L77:
	movq	-1616(%rbp), %rbx
	movq	-1624(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L78
	.p2align 4,,10
	.p2align 3
.L82:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L79
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L82
.L80:
	movq	-1624(%rbp), %r14
.L78:
	testq	%r14, %r14
	je	.L83
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L83:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L194
	addq	$1864, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L82
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L72:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L75
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L65:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L68
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L58:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L61
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L51:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L54
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L44:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L47
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L30:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L33
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L37:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L40
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L187:
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r9d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L4
	call	_ZdlPv@PLT
.L4:
	movq	(%r15), %rax
	movl	$2026, %edx
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rsi
	leaq	-1792(%rbp), %r15
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rcx, -1904(%rbp)
	movq	%rax, -1888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1888(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1888(%rbp), %rax
	movl	$32, %edi
	movq	-1904(%rbp), %xmm2
	movq	$0, -1776(%rbp)
	movq	%rax, %xmm0
	movq	%rax, %xmm6
	punpcklqdq	%xmm0, %xmm0
	punpcklqdq	%xmm6, %xmm2
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -1904(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -1792(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movq	%r15, %rsi
	leaq	32(%rax), %rdx
	leaq	-1248(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5
	call	_ZdlPv@PLT
.L5:
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1752(%rbp)
	jne	.L195
.L6:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1376(%rbp)
	je	.L8
.L188:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1440(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r8d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L9
	call	_ZdlPv@PLT
.L9:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-480(%rbp), %rdi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1184(%rbp)
	je	.L11
.L189:
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1248(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L12
	call	_ZdlPv@PLT
.L12:
	movq	(%r15), %rax
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	24(%rax), %rax
	movq	%rcx, -1904(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -1888(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1888(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal17Cast8JSRegExp_130EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rcx
	movq	%r15, %xmm3
	pxor	%xmm0, %xmm0
	movhps	-1904(%rbp), %xmm3
	movl	$32, %edi
	movq	%rax, -72(%rbp)
	leaq	-1792(%rbp), %r15
	movq	%rcx, -80(%rbp)
	movaps	%xmm3, -1904(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -1792(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	32(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L13
	call	_ZdlPv@PLT
.L13:
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1752(%rbp)
	jne	.L196
.L14:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -992(%rbp)
	je	.L16
.L190:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rdi
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L17
	call	_ZdlPv@PLT
.L17:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-480(%rbp), %rdi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L18
	call	_ZdlPv@PLT
.L18:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L19
.L191:
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L20
	call	_ZdlPv@PLT
.L20:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	24(%rax), %rdx
	movdqu	(%rax), %xmm4
	movaps	%xmm0, -1760(%rbp)
	movq	$0, -1744(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm4, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -1760(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L21
	call	_ZdlPv@PLT
.L21:
	movq	-1848(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L192:
	movq	-1848(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%r15, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L23
	call	_ZdlPv@PLT
.L23:
	movq	(%rbx), %rax
	movl	$17, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r15
	movq	%rcx, -1888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r15, -80(%rbp)
	movhps	-1888(%rbp), %xmm0
	leaq	-288(%rbp), %r15
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm6
	leaq	24(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L24
	call	_ZdlPv@PLT
.L24:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -416(%rbp)
	je	.L25
.L193:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L26
	call	_ZdlPv@PLT
.L26:
	movq	-1872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L195:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1904(%rbp), %xmm7
	movl	$24, %edi
	movaps	%xmm0, -1792(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-1440(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L7
	call	_ZdlPv@PLT
.L7:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L196:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1904(%rbp), %xmm7
	movl	$24, %edi
	movaps	%xmm0, -1792(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-1056(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L15
	call	_ZdlPv@PLT
.L15:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L14
.L194:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22434:
	.size	_ZN2v88internal18Cast8JSRegExp_1467EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, .-_ZN2v88internal18Cast8JSRegExp_1467EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.section	.rodata._ZN2v88internal36RegExpPrototypeSourceGetterAssembler39GenerateRegExpPrototypeSourceGetterImplEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	"RegExp.prototype.source"
.LC3:
	.string	"(?:)"
	.section	.text._ZN2v88internal36RegExpPrototypeSourceGetterAssembler39GenerateRegExpPrototypeSourceGetterImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal36RegExpPrototypeSourceGetterAssembler39GenerateRegExpPrototypeSourceGetterImplEv
	.type	_ZN2v88internal36RegExpPrototypeSourceGetterAssembler39GenerateRegExpPrototypeSourceGetterImplEv, @function
_ZN2v88internal36RegExpPrototypeSourceGetterAssembler39GenerateRegExpPrototypeSourceGetterImplEv:
.LFB22418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1384(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1568(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1440(%rbp), %rbx
	subq	$1656, %rsp
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, -1608(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	-1608(%rbp), %r12
	movq	%rax, -1680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -1432(%rbp)
	movq	%rax, -1696(%rbp)
	movq	-1608(%rbp), %rax
	movq	$0, -1424(%rbp)
	movq	%rax, -1440(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r15, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -1432(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1608(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -1664(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1608(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1632(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1608(%rbp), %rax
	movl	$72, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1624(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1608(%rbp), %rax
	movl	$72, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1648(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1608(%rbp), %rax
	movl	$48, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -1640(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1608(%rbp), %rax
	movl	$48, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -280(%rbp)
	leaq	-232(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -1656(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1680(%rbp), %xmm1
	movaps	%xmm0, -1568(%rbp)
	movhps	-1696(%rbp), %xmm1
	movq	$0, -1552(%rbp)
	movaps	%xmm1, -1680(%rbp)
	call	_Znwm@PLT
	movdqa	-1680(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1568(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L198
	call	_ZdlPv@PLT
.L198:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1376(%rbp)
	jne	.L355
	cmpq	$0, -1184(%rbp)
	jne	.L356
.L204:
	cmpq	$0, -992(%rbp)
	jne	.L357
.L207:
	cmpq	$0, -800(%rbp)
	jne	.L358
.L209:
	cmpq	$0, -608(%rbp)
	jne	.L359
.L212:
	cmpq	$0, -416(%rbp)
	jne	.L360
.L216:
	cmpq	$0, -224(%rbp)
	jne	.L361
.L218:
	movq	-1656(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L220
	call	_ZdlPv@PLT
.L220:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L221
	.p2align 4,,10
	.p2align 3
.L225:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L222
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L225
.L223:
	movq	-280(%rbp), %r13
.L221:
	testq	%r13, %r13
	je	.L226
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L226:
	movq	-1640(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L227
	call	_ZdlPv@PLT
.L227:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L228
	.p2align 4,,10
	.p2align 3
.L232:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L229
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L232
.L230:
	movq	-472(%rbp), %r13
.L228:
	testq	%r13, %r13
	je	.L233
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L233:
	movq	-1648(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L234
	call	_ZdlPv@PLT
.L234:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L235
	.p2align 4,,10
	.p2align 3
.L239:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L236
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L239
.L237:
	movq	-664(%rbp), %r13
.L235:
	testq	%r13, %r13
	je	.L240
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L240:
	movq	-1624(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L241
	call	_ZdlPv@PLT
.L241:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L242
	.p2align 4,,10
	.p2align 3
.L246:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L243
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L246
.L244:
	movq	-856(%rbp), %r13
.L242:
	testq	%r13, %r13
	je	.L247
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L247:
	movq	-1632(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L248
	call	_ZdlPv@PLT
.L248:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L249
	.p2align 4,,10
	.p2align 3
.L253:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L250
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L253
.L251:
	movq	-1048(%rbp), %r13
.L249:
	testq	%r13, %r13
	je	.L254
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L254:
	movq	-1664(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L255
	call	_ZdlPv@PLT
.L255:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L256
	.p2align 4,,10
	.p2align 3
.L260:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L257
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L260
.L258:
	movq	-1240(%rbp), %r13
.L256:
	testq	%r13, %r13
	je	.L261
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L261:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L262
	call	_ZdlPv@PLT
.L262:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L263
	.p2align 4,,10
	.p2align 3
.L267:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L264
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L267
.L265:
	movq	-1432(%rbp), %r13
.L263:
	testq	%r13, %r13
	je	.L268
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L268:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L362
	addq	$1656, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L264:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L267
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L257:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L260
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L250:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L253
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L243:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L246
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L236:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L239
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L222:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L225
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L229:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L232
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L355:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r9d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L200
	call	_ZdlPv@PLT
.L200:
	movq	(%rbx), %rax
	movl	$16, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -1680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$17, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	-1680(%rbp), %rdx
	call	_ZN2v88internal18Cast8JSRegExp_1467EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	movq	%rbx, %xmm3
	pxor	%xmm0, %xmm0
	movq	-1680(%rbp), %xmm2
	movhps	-1680(%rbp), %xmm3
	movl	$40, %edi
	movq	%rax, -64(%rbp)
	leaq	-1600(%rbp), %rbx
	punpcklqdq	%xmm2, %xmm2
	movaps	%xmm3, -1680(%rbp)
	movaps	%xmm2, -1696(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm2, -80(%rbp)
	movaps	%xmm0, -1600(%rbp)
	movq	$0, -1584(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rsi
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm6
	leaq	40(%rax), %rdx
	leaq	-1056(%rbp), %rdi
	movq	%rax, -1600(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L201
	call	_ZdlPv@PLT
.L201:
	movq	-1632(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1560(%rbp)
	jne	.L363
.L202:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1184(%rbp)
	je	.L204
.L356:
	movq	-1664(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1248(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L205
	call	_ZdlPv@PLT
.L205:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm7
	movaps	%xmm0, -1568(%rbp)
	movq	$0, -1552(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm7, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -1568(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L206
	call	_ZdlPv@PLT
.L206:
	movq	-1624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -992(%rbp)
	je	.L207
.L357:
	movq	-1632(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L208
	call	_ZdlPv@PLT
.L208:
	movq	(%rbx), %rax
	movl	$18, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	32(%rax), %r8
	movq	%r8, -1680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-1680(%rbp), %r8
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -800(%rbp)
	je	.L209
.L358:
	movq	-1624(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r8d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L210
	call	_ZdlPv@PLT
.L210:
	movq	(%rbx), %rax
	movl	$20, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -1696(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rcx, -1680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$17, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %edi
	movq	%rbx, -80(%rbp)
	movq	-1680(%rbp), %xmm0
	movq	$0, -1552(%rbp)
	movhps	-1696(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -1568(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L211
	call	_ZdlPv@PLT
.L211:
	movq	-1648(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -608(%rbp)
	je	.L212
.L359:
	movq	-1648(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%rbx, %rdi
	movb	$8, 2(%rax)
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L213
	call	_ZdlPv@PLT
.L213:
	movq	(%rbx), %rax
	movl	$16, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -1680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$23, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1680(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rbx, -1696(%rbp)
	call	_ZN2v88internal23RegExpBuiltinsAssembler32IsReceiverInitialRegExpPrototypeENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15Word32BinaryNotENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1696(%rbp), %xmm4
	movaps	%xmm0, -1568(%rbp)
	movhps	-1680(%rbp), %xmm4
	movq	$0, -1552(%rbp)
	movaps	%xmm4, -1680(%rbp)
	call	_Znwm@PLT
	movdqa	-1680(%rbp), %xmm7
	leaq	-480(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1568(%rbp)
	movups	%xmm7, (%rax)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L214
	call	_ZdlPv@PLT
.L214:
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movdqa	-1680(%rbp), %xmm5
	leaq	-288(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1568(%rbp)
	movups	%xmm5, (%rax)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L215
	call	_ZdlPv@PLT
.L215:
	movq	-1656(%rbp), %rcx
	movq	-1640(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -416(%rbp)
	je	.L216
.L360:
	movq	-1640(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L217
	call	_ZdlPv@PLT
.L217:
	movq	(%rbx), %rax
	movl	$25, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	movl	$156, %edx
	movq	%rbx, %rsi
	leaq	.LC2(%rip), %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -224(%rbp)
	je	.L218
.L361:
	movq	-1656(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-288(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L219
	call	_ZdlPv@PLT
.L219:
	movq	(%rbx), %rax
	movl	$27, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	movq	(%rax), %r9
	movq	%r9, -1680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$30, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	leaq	-96(%rbp), %rcx
	movq	%r13, %rdi
	movq	%rbx, -96(%rbp)
	movq	-1680(%rbp), %r9
	movl	$1, %r8d
	movl	$148, %esi
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$28, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal129FromConstexpr90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol18ATconstexpr_string_169EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L363:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-1680(%rbp), %xmm6
	movdqa	-1696(%rbp), %xmm7
	movaps	%xmm0, -1600(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	movq	$0, -1584(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%rbx, %rsi
	leaq	32(%rax), %rdx
	leaq	-1248(%rbp), %rdi
	movq	%rax, -1600(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1584(%rbp)
	movq	%rdx, -1592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L203
	call	_ZdlPv@PLT
.L203:
	movq	-1664(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L202
.L362:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22418:
	.size	_ZN2v88internal36RegExpPrototypeSourceGetterAssembler39GenerateRegExpPrototypeSourceGetterImplEv, .-_ZN2v88internal36RegExpPrototypeSourceGetterAssembler39GenerateRegExpPrototypeSourceGetterImplEv
	.section	.rodata._ZN2v88internal8Builtins36Generate_RegExpPrototypeSourceGetterEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/regexp-source-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins36Generate_RegExpPrototypeSourceGetterEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"RegExpPrototypeSourceGetter"
	.section	.text._ZN2v88internal8Builtins36Generate_RegExpPrototypeSourceGetterEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins36Generate_RegExpPrototypeSourceGetterEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins36Generate_RegExpPrototypeSourceGetterEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins36Generate_RegExpPrototypeSourceGetterEPNS0_8compiler18CodeAssemblerStateE:
.LFB22414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$146, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$871, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L368
.L365:
	movq	%r13, %rdi
	call	_ZN2v88internal36RegExpPrototypeSourceGetterAssembler39GenerateRegExpPrototypeSourceGetterImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L369
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L365
.L369:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22414:
	.size	_ZN2v88internal8Builtins36Generate_RegExpPrototypeSourceGetterEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins36Generate_RegExpPrototypeSourceGetterEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins36Generate_RegExpPrototypeSourceGetterEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins36Generate_RegExpPrototypeSourceGetterEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins36Generate_RegExpPrototypeSourceGetterEPNS0_8compiler18CodeAssemblerStateE:
.LFB28992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28992:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins36Generate_RegExpPrototypeSourceGetterEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins36Generate_RegExpPrototypeSourceGetterEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins36Generate_RegExpPrototypeSourceGetterEPNS0_8compiler18CodeAssemblerStateE
	.weak	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE:
	.byte	7
	.byte	7
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
