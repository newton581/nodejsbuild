	.file	"math-tq-csa.cc"
	.text
	.section	.rodata._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0:
.LFB29776:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	movabsq	$384307168202282325, %rax
	cmpq	%rax, %rdx
	ja	.L19
	pxor	%xmm0, %xmm0
	leaq	(%rdx,%rdx,2), %r14
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	%rdx, %r12
	movups	%xmm0, 8(%rdi)
	salq	$3, %r14
	xorl	%esi, %esi
	movq	$0, 24(%rdi)
	testq	%rdx, %rdx
	je	.L7
	movq	%r14, %rdi
	call	_Znwm@PLT
	leaq	-1(%r12), %rdx
	leaq	(%rax,%r14), %rsi
	movq	%rax, 8(%rbx)
	movq	%rsi, 24(%rbx)
	cmpq	$2, %rdx
	jbe	.L8
	movq	%r12, %rdi
	movq	%r12, %rcx
	pxor	%xmm0, %xmm0
	movq	%rax, %rdx
	shrq	%rdi
	andq	$-2, %rcx
	addq	%rdi, %rcx
	salq	$4, %rcx
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L5:
	movups	%xmm0, (%rdx)
	addq	$48, %rdx
	movups	%xmm0, -32(%rdx)
	movups	%xmm0, -16(%rdx)
	cmpq	%rcx, %rdx
	jne	.L5
	movq	%r12, %rdx
	movq	%r12, %rcx
	andq	$-2, %rdx
	andl	$1, %ecx
	leaq	(%rdx,%rdx,2), %rdi
	leaq	(%rax,%rdi,8), %rax
	cmpq	%rdx, %r12
	je	.L7
.L4:
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	cmpq	$1, %rcx
	je	.L7
	movq	$0, 40(%rax)
	movups	%xmm0, 24(%rax)
	cmpq	$2, %rcx
	je	.L7
	movq	$0, 64(%rax)
	movups	%xmm0, 48(%rax)
.L7:
	movq	%rsi, 16(%rbx)
	pxor	%xmm0, %xmm0
	leaq	56(%rbx), %rdi
	movq	%r13, %rsi
	movups	%xmm0, 32(%rbx)
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	$0, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movq	%r12, %rcx
	jmp	.L4
.L19:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29776:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev,"axG",@progbits,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev:
.LFB22421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$56, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L21
	call	_ZdlPv@PLT
.L21:
	movq	16(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	%r12, %r13
	je	.L22
	.p2align 4,,10
	.p2align 3
.L26:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L23
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L26
.L24:
	movq	8(%rbx), %r12
.L22:
	testq	%r12, %r12
	je	.L20
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L26
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L20:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22421:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev
	.set	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.section	.rodata._ZN2v88internal17MathAcosAssembler20GenerateMathAcosImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../../deps/v8/../../deps/v8/src/builtins/math.tq"
	.section	.text._ZN2v88internal17MathAcosAssembler20GenerateMathAcosImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17MathAcosAssembler20GenerateMathAcosImplEv
	.type	_ZN2v88internal17MathAcosAssembler20GenerateMathAcosImplEv, @function
_ZN2v88internal17MathAcosAssembler20GenerateMathAcosImplEv:
.LFB22418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-240(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-272(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	-280(%rbp), %r13
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -312(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-304(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-312(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L33
	call	_ZdlPv@PLT
.L33:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L57
.L34:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L36
	call	_ZdlPv@PLT
.L36:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L37
	.p2align 4,,10
	.p2align 3
.L41:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L38
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L41
.L39:
	movq	-232(%rbp), %r12
.L37:
	testq	%r12, %r12
	je	.L42
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L42:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L58
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L41
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L57:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L35
	call	_ZdlPv@PLT
.L35:
	movq	(%r15), %rax
	movl	$11, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	8(%rax), %r8
	movq	(%rax), %r15
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-304(%rbp), %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler15ToNumber_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert9ATfloat6420UT5ATSmi10HeapNumber_194EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movl	$12, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11Float64AcosENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATfloat64_199EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L34
.L58:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22418:
	.size	_ZN2v88internal17MathAcosAssembler20GenerateMathAcosImplEv, .-_ZN2v88internal17MathAcosAssembler20GenerateMathAcosImplEv
	.section	.rodata._ZN2v88internal8Builtins17Generate_MathAcosEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/math-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins17Generate_MathAcosEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"MathAcos"
	.section	.text._ZN2v88internal8Builtins17Generate_MathAcosEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins17Generate_MathAcosEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins17Generate_MathAcosEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins17Generate_MathAcosEPNS0_8compiler18CodeAssemblerStateE:
.LFB22414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$146, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$820, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L63
.L60:
	movq	%r13, %rdi
	call	_ZN2v88internal17MathAcosAssembler20GenerateMathAcosImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L64
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L60
.L64:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22414:
	.size	_ZN2v88internal8Builtins17Generate_MathAcosEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins17Generate_MathAcosEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal18MathAcoshAssembler21GenerateMathAcoshImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18MathAcoshAssembler21GenerateMathAcoshImplEv
	.type	_ZN2v88internal18MathAcoshAssembler21GenerateMathAcoshImplEv, @function
_ZN2v88internal18MathAcoshAssembler21GenerateMathAcoshImplEv:
.LFB22433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-240(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-272(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	-280(%rbp), %r13
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -312(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-304(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-312(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L66
	call	_ZdlPv@PLT
.L66:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L90
.L67:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L69
	call	_ZdlPv@PLT
.L69:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L70
	.p2align 4,,10
	.p2align 3
.L74:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L71
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L74
.L72:
	movq	-232(%rbp), %r12
.L70:
	testq	%r12, %r12
	je	.L75
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L75:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L91
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L74
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L90:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L68
	call	_ZdlPv@PLT
.L68:
	movq	(%r15), %rax
	movl	$20, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	8(%rax), %r8
	movq	(%rax), %r15
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-304(%rbp), %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler15ToNumber_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert9ATfloat6420UT5ATSmi10HeapNumber_194EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movl	$21, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12Float64AcoshENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATfloat64_199EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L67
.L91:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22433:
	.size	_ZN2v88internal18MathAcoshAssembler21GenerateMathAcoshImplEv, .-_ZN2v88internal18MathAcoshAssembler21GenerateMathAcoshImplEv
	.section	.rodata._ZN2v88internal8Builtins18Generate_MathAcoshEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"MathAcosh"
	.section	.text._ZN2v88internal8Builtins18Generate_MathAcoshEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins18Generate_MathAcoshEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins18Generate_MathAcoshEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins18Generate_MathAcoshEPNS0_8compiler18CodeAssemblerStateE:
.LFB22429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$177, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$821, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L96
.L93:
	movq	%r13, %rdi
	call	_ZN2v88internal18MathAcoshAssembler21GenerateMathAcoshImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L97
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L93
.L97:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22429:
	.size	_ZN2v88internal8Builtins18Generate_MathAcoshEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins18Generate_MathAcoshEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal17MathAsinAssembler20GenerateMathAsinImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17MathAsinAssembler20GenerateMathAsinImplEv
	.type	_ZN2v88internal17MathAsinAssembler20GenerateMathAsinImplEv, @function
_ZN2v88internal17MathAsinAssembler20GenerateMathAsinImplEv:
.LFB22442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-240(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-272(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	-280(%rbp), %r13
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -312(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-304(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-312(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L99
	call	_ZdlPv@PLT
.L99:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L123
.L100:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L102
	call	_ZdlPv@PLT
.L102:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L103
	.p2align 4,,10
	.p2align 3
.L107:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L104
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L107
.L105:
	movq	-232(%rbp), %r12
.L103:
	testq	%r12, %r12
	je	.L108
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L108:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L124
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L107
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L123:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L101
	call	_ZdlPv@PLT
.L101:
	movq	(%r15), %rax
	movl	$29, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	8(%rax), %r8
	movq	(%rax), %r15
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-304(%rbp), %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler15ToNumber_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert9ATfloat6420UT5ATSmi10HeapNumber_194EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movl	$30, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11Float64AsinENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATfloat64_199EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L100
.L124:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22442:
	.size	_ZN2v88internal17MathAsinAssembler20GenerateMathAsinImplEv, .-_ZN2v88internal17MathAsinAssembler20GenerateMathAsinImplEv
	.section	.rodata._ZN2v88internal8Builtins17Generate_MathAsinEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"MathAsin"
	.section	.text._ZN2v88internal8Builtins17Generate_MathAsinEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins17Generate_MathAsinEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins17Generate_MathAsinEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins17Generate_MathAsinEPNS0_8compiler18CodeAssemblerStateE:
.LFB22438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$208, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$822, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L129
.L126:
	movq	%r13, %rdi
	call	_ZN2v88internal17MathAsinAssembler20GenerateMathAsinImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L130
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L126
.L130:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22438:
	.size	_ZN2v88internal8Builtins17Generate_MathAsinEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins17Generate_MathAsinEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal18MathAsinhAssembler21GenerateMathAsinhImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18MathAsinhAssembler21GenerateMathAsinhImplEv
	.type	_ZN2v88internal18MathAsinhAssembler21GenerateMathAsinhImplEv, @function
_ZN2v88internal18MathAsinhAssembler21GenerateMathAsinhImplEv:
.LFB22451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-240(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-272(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	-280(%rbp), %r13
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -312(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-304(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-312(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L132
	call	_ZdlPv@PLT
.L132:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L156
.L133:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L135
	call	_ZdlPv@PLT
.L135:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L136
	.p2align 4,,10
	.p2align 3
.L140:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L137
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L140
.L138:
	movq	-232(%rbp), %r12
.L136:
	testq	%r12, %r12
	je	.L141
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L141:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L157
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L140
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L156:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L134
	call	_ZdlPv@PLT
.L134:
	movq	(%r15), %rax
	movl	$38, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	8(%rax), %r8
	movq	(%rax), %r15
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-304(%rbp), %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler15ToNumber_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert9ATfloat6420UT5ATSmi10HeapNumber_194EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movl	$39, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12Float64AsinhENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATfloat64_199EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L133
.L157:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22451:
	.size	_ZN2v88internal18MathAsinhAssembler21GenerateMathAsinhImplEv, .-_ZN2v88internal18MathAsinhAssembler21GenerateMathAsinhImplEv
	.section	.rodata._ZN2v88internal8Builtins18Generate_MathAsinhEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"MathAsinh"
	.section	.text._ZN2v88internal8Builtins18Generate_MathAsinhEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins18Generate_MathAsinhEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins18Generate_MathAsinhEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins18Generate_MathAsinhEPNS0_8compiler18CodeAssemblerStateE:
.LFB22447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$239, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$823, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L162
.L159:
	movq	%r13, %rdi
	call	_ZN2v88internal18MathAsinhAssembler21GenerateMathAsinhImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L163
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L159
.L163:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22447:
	.size	_ZN2v88internal8Builtins18Generate_MathAsinhEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins18Generate_MathAsinhEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal17MathAtanAssembler20GenerateMathAtanImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17MathAtanAssembler20GenerateMathAtanImplEv
	.type	_ZN2v88internal17MathAtanAssembler20GenerateMathAtanImplEv, @function
_ZN2v88internal17MathAtanAssembler20GenerateMathAtanImplEv:
.LFB22460:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-240(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-272(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	-280(%rbp), %r13
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -312(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-304(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-312(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L165
	call	_ZdlPv@PLT
.L165:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L189
.L166:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L168
	call	_ZdlPv@PLT
.L168:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L169
	.p2align 4,,10
	.p2align 3
.L173:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L170
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L173
.L171:
	movq	-232(%rbp), %r12
.L169:
	testq	%r12, %r12
	je	.L174
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L174:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L190
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L173
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L189:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L167
	call	_ZdlPv@PLT
.L167:
	movq	(%r15), %rax
	movl	$47, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	8(%rax), %r8
	movq	(%rax), %r15
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-304(%rbp), %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler15ToNumber_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert9ATfloat6420UT5ATSmi10HeapNumber_194EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movl	$48, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11Float64AtanENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATfloat64_199EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L166
.L190:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22460:
	.size	_ZN2v88internal17MathAtanAssembler20GenerateMathAtanImplEv, .-_ZN2v88internal17MathAtanAssembler20GenerateMathAtanImplEv
	.section	.rodata._ZN2v88internal8Builtins17Generate_MathAtanEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"MathAtan"
	.section	.text._ZN2v88internal8Builtins17Generate_MathAtanEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins17Generate_MathAtanEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins17Generate_MathAtanEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins17Generate_MathAtanEPNS0_8compiler18CodeAssemblerStateE:
.LFB22456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$270, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$824, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L195
.L192:
	movq	%r13, %rdi
	call	_ZN2v88internal17MathAtanAssembler20GenerateMathAtanImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L196
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L192
.L196:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22456:
	.size	_ZN2v88internal8Builtins17Generate_MathAtanEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins17Generate_MathAtanEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal18MathAtan2Assembler21GenerateMathAtan2ImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18MathAtan2Assembler21GenerateMathAtan2ImplEv
	.type	_ZN2v88internal18MathAtan2Assembler21GenerateMathAtan2ImplEv, @function
_ZN2v88internal18MathAtan2Assembler21GenerateMathAtan2ImplEv:
.LFB22469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$5, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-216(%rbp), %r14
	leaq	-272(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdi
	movl	$1, %esi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r13, %rdi
	leaq	-312(%rbp), %r13
	movq	%rax, -336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$72, %edi
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	%rax, %rbx
	movq	-312(%rbp), %rax
	movq	$0, -248(%rbp)
	movq	%rax, -272(%rbp)
	call	_Znwm@PLT
	pxor	%xmm1, %xmm1
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -248(%rbp)
	movq	%rdx, -256(%rbp)
	xorl	%edx, %edx
	movq	%rax, -264(%rbp)
	movups	%xmm1, -232(%rbp)
	movq	$0, -240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm1, %xmm1
	movl	$24, %edi
	movq	-328(%rbp), %xmm0
	movq	%rbx, -64(%rbp)
	leaq	-304(%rbp), %rbx
	movhps	-336(%rbp), %xmm0
	movaps	%xmm1, -304(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -288(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movdqa	-80(%rbp), %xmm2
	leaq	24(%rax), %rdx
	movq	%rax, -304(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -288(%rbp)
	movq	%rdx, -296(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L198
	call	_ZdlPv@PLT
.L198:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -208(%rbp)
	jne	.L222
.L199:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L201
	call	_ZdlPv@PLT
.L201:
	movq	-256(%rbp), %rbx
	movq	-264(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L202
	.p2align 4,,10
	.p2align 3
.L206:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L203
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L206
.L204:
	movq	-264(%rbp), %r12
.L202:
	testq	%r12, %r12
	je	.L207
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L207:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L223
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L206
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L222:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -288(%rbp)
	movaps	%xmm0, -304(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -304(%rbp)
	movq	%rdx, -288(%rbp)
	movq	%rdx, -296(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-304(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L200
	call	_ZdlPv@PLT
.L200:
	movq	(%r15), %rax
	movl	$56, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	16(%rax), %r9
	movq	(%rax), %r8
	movq	8(%rax), %r15
	movq	%r9, -336(%rbp)
	movq	%r8, -328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-328(%rbp), %r8
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler15ToNumber_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert9ATfloat6420UT5ATSmi10HeapNumber_194EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movl	$57, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-336(%rbp), %r9
	movq	-328(%rbp), %r8
	movq	%rbx, %rdi
	movq	%r9, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler15ToNumber_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-328(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert9ATfloat6420UT5ATSmi10HeapNumber_194EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movl	$58, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-328(%rbp), %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler12Float64Atan2ENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATfloat64_199EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L199
.L223:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22469:
	.size	_ZN2v88internal18MathAtan2Assembler21GenerateMathAtan2ImplEv, .-_ZN2v88internal18MathAtan2Assembler21GenerateMathAtan2ImplEv
	.section	.rodata._ZN2v88internal8Builtins18Generate_MathAtan2EPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"MathAtan2"
	.section	.text._ZN2v88internal8Builtins18Generate_MathAtan2EPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins18Generate_MathAtan2EPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins18Generate_MathAtan2EPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins18Generate_MathAtan2EPNS0_8compiler18CodeAssemblerStateE:
.LFB22465:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$301, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$825, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L228
.L225:
	movq	%r13, %rdi
	call	_ZN2v88internal18MathAtan2Assembler21GenerateMathAtan2ImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L229
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L225
.L229:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22465:
	.size	_ZN2v88internal8Builtins18Generate_MathAtan2EPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins18Generate_MathAtan2EPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal18MathAtanhAssembler21GenerateMathAtanhImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18MathAtanhAssembler21GenerateMathAtanhImplEv
	.type	_ZN2v88internal18MathAtanhAssembler21GenerateMathAtanhImplEv, @function
_ZN2v88internal18MathAtanhAssembler21GenerateMathAtanhImplEv:
.LFB22481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-240(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-272(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	-280(%rbp), %r13
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -312(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-304(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-312(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L231
	call	_ZdlPv@PLT
.L231:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L255
.L232:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L234
	call	_ZdlPv@PLT
.L234:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L235
	.p2align 4,,10
	.p2align 3
.L239:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L236
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L239
.L237:
	movq	-232(%rbp), %r12
.L235:
	testq	%r12, %r12
	je	.L240
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L240:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L256
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L239
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L255:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L233
	call	_ZdlPv@PLT
.L233:
	movq	(%r15), %rax
	movl	$66, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	8(%rax), %r8
	movq	(%rax), %r15
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-304(%rbp), %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler15ToNumber_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert9ATfloat6420UT5ATSmi10HeapNumber_194EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movl	$67, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12Float64AtanhENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATfloat64_199EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L232
.L256:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22481:
	.size	_ZN2v88internal18MathAtanhAssembler21GenerateMathAtanhImplEv, .-_ZN2v88internal18MathAtanhAssembler21GenerateMathAtanhImplEv
	.section	.rodata._ZN2v88internal8Builtins18Generate_MathAtanhEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"MathAtanh"
	.section	.text._ZN2v88internal8Builtins18Generate_MathAtanhEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins18Generate_MathAtanhEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins18Generate_MathAtanhEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins18Generate_MathAtanhEPNS0_8compiler18CodeAssemblerStateE:
.LFB22477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$342, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$826, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L261
.L258:
	movq	%r13, %rdi
	call	_ZN2v88internal18MathAtanhAssembler21GenerateMathAtanhImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L262
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L258
.L262:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22477:
	.size	_ZN2v88internal8Builtins18Generate_MathAtanhEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins18Generate_MathAtanhEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal17MathCbrtAssembler20GenerateMathCbrtImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17MathCbrtAssembler20GenerateMathCbrtImplEv
	.type	_ZN2v88internal17MathCbrtAssembler20GenerateMathCbrtImplEv, @function
_ZN2v88internal17MathCbrtAssembler20GenerateMathCbrtImplEv:
.LFB22490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-240(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-272(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	-280(%rbp), %r13
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -312(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-304(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-312(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L264
	call	_ZdlPv@PLT
.L264:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L288
.L265:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L267
	call	_ZdlPv@PLT
.L267:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L268
	.p2align 4,,10
	.p2align 3
.L272:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L269
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L272
.L270:
	movq	-232(%rbp), %r12
.L268:
	testq	%r12, %r12
	je	.L273
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L273:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L289
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L269:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L272
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L288:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L266
	call	_ZdlPv@PLT
.L266:
	movq	(%r15), %rax
	movl	$75, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	8(%rax), %r8
	movq	(%rax), %r15
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-304(%rbp), %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler15ToNumber_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert9ATfloat6420UT5ATSmi10HeapNumber_194EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movl	$76, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11Float64CbrtENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATfloat64_199EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L265
.L289:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22490:
	.size	_ZN2v88internal17MathCbrtAssembler20GenerateMathCbrtImplEv, .-_ZN2v88internal17MathCbrtAssembler20GenerateMathCbrtImplEv
	.section	.rodata._ZN2v88internal8Builtins17Generate_MathCbrtEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"MathCbrt"
	.section	.text._ZN2v88internal8Builtins17Generate_MathCbrtEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins17Generate_MathCbrtEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins17Generate_MathCbrtEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins17Generate_MathCbrtEPNS0_8compiler18CodeAssemblerStateE:
.LFB22486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$373, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC10(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$827, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L294
.L291:
	movq	%r13, %rdi
	call	_ZN2v88internal17MathCbrtAssembler20GenerateMathCbrtImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L295
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L294:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L291
.L295:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22486:
	.size	_ZN2v88internal8Builtins17Generate_MathCbrtEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins17Generate_MathCbrtEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal16MathCosAssembler19GenerateMathCosImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16MathCosAssembler19GenerateMathCosImplEv
	.type	_ZN2v88internal16MathCosAssembler19GenerateMathCosImplEv, @function
_ZN2v88internal16MathCosAssembler19GenerateMathCosImplEv:
.LFB22517:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-240(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-272(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	-280(%rbp), %r13
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -312(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-304(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-312(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L297
	call	_ZdlPv@PLT
.L297:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L321
.L298:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L300
	call	_ZdlPv@PLT
.L300:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L301
	.p2align 4,,10
	.p2align 3
.L305:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L302
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L305
.L303:
	movq	-232(%rbp), %r12
.L301:
	testq	%r12, %r12
	je	.L306
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L306:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L322
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L305
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L321:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L299
	call	_ZdlPv@PLT
.L299:
	movq	(%r15), %rax
	movl	$104, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	8(%rax), %r8
	movq	(%rax), %r15
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-304(%rbp), %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler15ToNumber_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert9ATfloat6420UT5ATSmi10HeapNumber_194EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movl	$105, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler10Float64CosENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATfloat64_199EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L298
.L322:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22517:
	.size	_ZN2v88internal16MathCosAssembler19GenerateMathCosImplEv, .-_ZN2v88internal16MathCosAssembler19GenerateMathCosImplEv
	.section	.rodata._ZN2v88internal8Builtins16Generate_MathCosEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"MathCos"
	.section	.text._ZN2v88internal8Builtins16Generate_MathCosEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins16Generate_MathCosEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins16Generate_MathCosEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins16Generate_MathCosEPNS0_8compiler18CodeAssemblerStateE:
.LFB22513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$502, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$829, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L327
.L324:
	movq	%r13, %rdi
	call	_ZN2v88internal16MathCosAssembler19GenerateMathCosImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L328
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L324
.L328:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22513:
	.size	_ZN2v88internal8Builtins16Generate_MathCosEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins16Generate_MathCosEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal17MathCoshAssembler20GenerateMathCoshImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17MathCoshAssembler20GenerateMathCoshImplEv
	.type	_ZN2v88internal17MathCoshAssembler20GenerateMathCoshImplEv, @function
_ZN2v88internal17MathCoshAssembler20GenerateMathCoshImplEv:
.LFB22526:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-240(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-272(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	-280(%rbp), %r13
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -312(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-304(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-312(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L330
	call	_ZdlPv@PLT
.L330:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L354
.L331:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L333
	call	_ZdlPv@PLT
.L333:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L334
	.p2align 4,,10
	.p2align 3
.L338:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L335
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L338
.L336:
	movq	-232(%rbp), %r12
.L334:
	testq	%r12, %r12
	je	.L339
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L339:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L355
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L338
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L354:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L332
	call	_ZdlPv@PLT
.L332:
	movq	(%r15), %rax
	movl	$113, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	8(%rax), %r8
	movq	(%rax), %r15
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-304(%rbp), %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler15ToNumber_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert9ATfloat6420UT5ATSmi10HeapNumber_194EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movl	$114, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11Float64CoshENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATfloat64_199EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L331
.L355:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22526:
	.size	_ZN2v88internal17MathCoshAssembler20GenerateMathCoshImplEv, .-_ZN2v88internal17MathCoshAssembler20GenerateMathCoshImplEv
	.section	.rodata._ZN2v88internal8Builtins17Generate_MathCoshEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"MathCosh"
	.section	.text._ZN2v88internal8Builtins17Generate_MathCoshEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins17Generate_MathCoshEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins17Generate_MathCoshEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins17Generate_MathCoshEPNS0_8compiler18CodeAssemblerStateE:
.LFB22522:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$533, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$830, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L360
.L357:
	movq	%r13, %rdi
	call	_ZN2v88internal17MathCoshAssembler20GenerateMathCoshImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L361
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L360:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L357
.L361:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22522:
	.size	_ZN2v88internal8Builtins17Generate_MathCoshEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins17Generate_MathCoshEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal16MathExpAssembler19GenerateMathExpImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16MathExpAssembler19GenerateMathExpImplEv
	.type	_ZN2v88internal16MathExpAssembler19GenerateMathExpImplEv, @function
_ZN2v88internal16MathExpAssembler19GenerateMathExpImplEv:
.LFB22535:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-240(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-272(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	-280(%rbp), %r13
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -312(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-304(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-312(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L363
	call	_ZdlPv@PLT
.L363:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L387
.L364:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L366
	call	_ZdlPv@PLT
.L366:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L367
	.p2align 4,,10
	.p2align 3
.L371:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L368
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L371
.L369:
	movq	-232(%rbp), %r12
.L367:
	testq	%r12, %r12
	je	.L372
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L372:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L388
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L371
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L387:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L365
	call	_ZdlPv@PLT
.L365:
	movq	(%r15), %rax
	movl	$122, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	8(%rax), %r8
	movq	(%rax), %r15
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-304(%rbp), %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler15ToNumber_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert9ATfloat6420UT5ATSmi10HeapNumber_194EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movl	$123, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler10Float64ExpENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATfloat64_199EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L364
.L388:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22535:
	.size	_ZN2v88internal16MathExpAssembler19GenerateMathExpImplEv, .-_ZN2v88internal16MathExpAssembler19GenerateMathExpImplEv
	.section	.rodata._ZN2v88internal8Builtins16Generate_MathExpEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"MathExp"
	.section	.text._ZN2v88internal8Builtins16Generate_MathExpEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins16Generate_MathExpEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins16Generate_MathExpEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins16Generate_MathExpEPNS0_8compiler18CodeAssemblerStateE:
.LFB22531:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$564, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$831, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L393
.L390:
	movq	%r13, %rdi
	call	_ZN2v88internal16MathExpAssembler19GenerateMathExpImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L394
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L393:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L390
.L394:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22531:
	.size	_ZN2v88internal8Builtins16Generate_MathExpEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins16Generate_MathExpEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal18MathExpm1Assembler21GenerateMathExpm1ImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18MathExpm1Assembler21GenerateMathExpm1ImplEv
	.type	_ZN2v88internal18MathExpm1Assembler21GenerateMathExpm1ImplEv, @function
_ZN2v88internal18MathExpm1Assembler21GenerateMathExpm1ImplEv:
.LFB22544:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-240(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-272(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	-280(%rbp), %r13
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -312(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-304(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-312(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L396
	call	_ZdlPv@PLT
.L396:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L420
.L397:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L399
	call	_ZdlPv@PLT
.L399:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L400
	.p2align 4,,10
	.p2align 3
.L404:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L401
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L404
.L402:
	movq	-232(%rbp), %r12
.L400:
	testq	%r12, %r12
	je	.L405
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L405:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L421
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L401:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L404
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L420:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L398
	call	_ZdlPv@PLT
.L398:
	movq	(%r15), %rax
	movl	$131, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	8(%rax), %r8
	movq	(%rax), %r15
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-304(%rbp), %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler15ToNumber_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert9ATfloat6420UT5ATSmi10HeapNumber_194EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movl	$132, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12Float64Expm1ENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATfloat64_199EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L397
.L421:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22544:
	.size	_ZN2v88internal18MathExpm1Assembler21GenerateMathExpm1ImplEv, .-_ZN2v88internal18MathExpm1Assembler21GenerateMathExpm1ImplEv
	.section	.rodata._ZN2v88internal8Builtins18Generate_MathExpm1EPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC14:
	.string	"MathExpm1"
	.section	.text._ZN2v88internal8Builtins18Generate_MathExpm1EPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins18Generate_MathExpm1EPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins18Generate_MathExpm1EPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins18Generate_MathExpm1EPNS0_8compiler18CodeAssemblerStateE:
.LFB22540:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$595, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC14(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$832, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L426
.L423:
	movq	%r13, %rdi
	call	_ZN2v88internal18MathExpm1Assembler21GenerateMathExpm1ImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L427
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L426:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L423
.L427:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22540:
	.size	_ZN2v88internal8Builtins18Generate_MathExpm1EPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins18Generate_MathExpm1EPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal19MathFroundAssembler22GenerateMathFroundImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19MathFroundAssembler22GenerateMathFroundImplEv
	.type	_ZN2v88internal19MathFroundAssembler22GenerateMathFroundImplEv, @function
_ZN2v88internal19MathFroundAssembler22GenerateMathFroundImplEv:
.LFB22553:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-240(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-272(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	-280(%rbp), %r13
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -312(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-304(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-312(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L429
	call	_ZdlPv@PLT
.L429:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L453
.L430:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L432
	call	_ZdlPv@PLT
.L432:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L433
	.p2align 4,,10
	.p2align 3
.L437:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L434
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L437
.L435:
	movq	-232(%rbp), %r12
.L433:
	testq	%r12, %r12
	je	.L438
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L438:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L454
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L434:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L437
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L453:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L431
	call	_ZdlPv@PLT
.L431:
	movq	(%r15), %rax
	movl	$138, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	8(%rax), %r8
	movq	(%rax), %r15
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-304(%rbp), %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler15ToNumber_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert9ATfloat3220UT5ATSmi10HeapNumber_198EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movl	$139, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal31Convert9ATfloat649ATfloat32_196EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8Float32TEEE@PLT
	movl	$140, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATfloat64_199EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L430
.L454:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22553:
	.size	_ZN2v88internal19MathFroundAssembler22GenerateMathFroundImplEv, .-_ZN2v88internal19MathFroundAssembler22GenerateMathFroundImplEv
	.section	.rodata._ZN2v88internal8Builtins19Generate_MathFroundEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"MathFround"
	.section	.text._ZN2v88internal8Builtins19Generate_MathFroundEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins19Generate_MathFroundEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins19Generate_MathFroundEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins19Generate_MathFroundEPNS0_8compiler18CodeAssemblerStateE:
.LFB22549:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$626, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC15(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$833, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L459
.L456:
	movq	%r13, %rdi
	call	_ZN2v88internal19MathFroundAssembler22GenerateMathFroundImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L460
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L459:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L456
.L460:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22549:
	.size	_ZN2v88internal8Builtins19Generate_MathFroundEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins19Generate_MathFroundEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal16MathLogAssembler19GenerateMathLogImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16MathLogAssembler19GenerateMathLogImplEv
	.type	_ZN2v88internal16MathLogAssembler19GenerateMathLogImplEv, @function
_ZN2v88internal16MathLogAssembler19GenerateMathLogImplEv:
.LFB22562:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-240(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-272(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	-280(%rbp), %r13
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -312(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-304(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-312(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L462
	call	_ZdlPv@PLT
.L462:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L486
.L463:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L465
	call	_ZdlPv@PLT
.L465:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L466
	.p2align 4,,10
	.p2align 3
.L470:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L467
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L470
.L468:
	movq	-232(%rbp), %r12
.L466:
	testq	%r12, %r12
	je	.L471
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L471:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L487
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L467:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L470
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L486:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L464
	call	_ZdlPv@PLT
.L464:
	movq	(%r15), %rax
	movl	$148, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	8(%rax), %r8
	movq	(%rax), %r15
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-304(%rbp), %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler15ToNumber_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert9ATfloat6420UT5ATSmi10HeapNumber_194EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movl	$149, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler10Float64LogENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATfloat64_199EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L463
.L487:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22562:
	.size	_ZN2v88internal16MathLogAssembler19GenerateMathLogImplEv, .-_ZN2v88internal16MathLogAssembler19GenerateMathLogImplEv
	.section	.rodata._ZN2v88internal8Builtins16Generate_MathLogEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"MathLog"
	.section	.text._ZN2v88internal8Builtins16Generate_MathLogEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins16Generate_MathLogEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins16Generate_MathLogEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins16Generate_MathLogEPNS0_8compiler18CodeAssemblerStateE:
.LFB22558:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$658, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC16(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$834, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L492
.L489:
	movq	%r13, %rdi
	call	_ZN2v88internal16MathLogAssembler19GenerateMathLogImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L493
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L492:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L489
.L493:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22558:
	.size	_ZN2v88internal8Builtins16Generate_MathLogEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins16Generate_MathLogEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal18MathLog1pAssembler21GenerateMathLog1pImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18MathLog1pAssembler21GenerateMathLog1pImplEv
	.type	_ZN2v88internal18MathLog1pAssembler21GenerateMathLog1pImplEv, @function
_ZN2v88internal18MathLog1pAssembler21GenerateMathLog1pImplEv:
.LFB22571:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-240(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-272(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	-280(%rbp), %r13
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -312(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-304(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-312(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L495
	call	_ZdlPv@PLT
.L495:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L519
.L496:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L498
	call	_ZdlPv@PLT
.L498:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L499
	.p2align 4,,10
	.p2align 3
.L503:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L500
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L503
.L501:
	movq	-232(%rbp), %r12
.L499:
	testq	%r12, %r12
	je	.L504
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L504:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L520
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L500:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L503
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L519:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L497
	call	_ZdlPv@PLT
.L497:
	movq	(%r15), %rax
	movl	$157, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	8(%rax), %r8
	movq	(%rax), %r15
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-304(%rbp), %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler15ToNumber_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert9ATfloat6420UT5ATSmi10HeapNumber_194EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movl	$158, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12Float64Log1pENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATfloat64_199EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L496
.L520:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22571:
	.size	_ZN2v88internal18MathLog1pAssembler21GenerateMathLog1pImplEv, .-_ZN2v88internal18MathLog1pAssembler21GenerateMathLog1pImplEv
	.section	.rodata._ZN2v88internal8Builtins18Generate_MathLog1pEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC17:
	.string	"MathLog1p"
	.section	.text._ZN2v88internal8Builtins18Generate_MathLog1pEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins18Generate_MathLog1pEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins18Generate_MathLog1pEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins18Generate_MathLog1pEPNS0_8compiler18CodeAssemblerStateE:
.LFB22567:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$689, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC17(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$835, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L525
.L522:
	movq	%r13, %rdi
	call	_ZN2v88internal18MathLog1pAssembler21GenerateMathLog1pImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L526
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L525:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L522
.L526:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22567:
	.size	_ZN2v88internal8Builtins18Generate_MathLog1pEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins18Generate_MathLog1pEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal18MathLog10Assembler21GenerateMathLog10ImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18MathLog10Assembler21GenerateMathLog10ImplEv
	.type	_ZN2v88internal18MathLog10Assembler21GenerateMathLog10ImplEv, @function
_ZN2v88internal18MathLog10Assembler21GenerateMathLog10ImplEv:
.LFB22580:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-240(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-272(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	-280(%rbp), %r13
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -312(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-304(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-312(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L528
	call	_ZdlPv@PLT
.L528:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L552
.L529:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L531
	call	_ZdlPv@PLT
.L531:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L532
	.p2align 4,,10
	.p2align 3
.L536:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L533
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L536
.L534:
	movq	-232(%rbp), %r12
.L532:
	testq	%r12, %r12
	je	.L537
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L537:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L553
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L533:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L536
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L552:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L530
	call	_ZdlPv@PLT
.L530:
	movq	(%r15), %rax
	movl	$166, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	8(%rax), %r8
	movq	(%rax), %r15
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-304(%rbp), %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler15ToNumber_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert9ATfloat6420UT5ATSmi10HeapNumber_194EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movl	$167, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12Float64Log10ENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATfloat64_199EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L529
.L553:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22580:
	.size	_ZN2v88internal18MathLog10Assembler21GenerateMathLog10ImplEv, .-_ZN2v88internal18MathLog10Assembler21GenerateMathLog10ImplEv
	.section	.rodata._ZN2v88internal8Builtins18Generate_MathLog10EPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC18:
	.string	"MathLog10"
	.section	.text._ZN2v88internal8Builtins18Generate_MathLog10EPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins18Generate_MathLog10EPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins18Generate_MathLog10EPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins18Generate_MathLog10EPNS0_8compiler18CodeAssemblerStateE:
.LFB22576:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$720, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC18(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$836, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L558
.L555:
	movq	%r13, %rdi
	call	_ZN2v88internal18MathLog10Assembler21GenerateMathLog10ImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L559
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L558:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L555
.L559:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22576:
	.size	_ZN2v88internal8Builtins18Generate_MathLog10EPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins18Generate_MathLog10EPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal17MathLog2Assembler20GenerateMathLog2ImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17MathLog2Assembler20GenerateMathLog2ImplEv
	.type	_ZN2v88internal17MathLog2Assembler20GenerateMathLog2ImplEv, @function
_ZN2v88internal17MathLog2Assembler20GenerateMathLog2ImplEv:
.LFB22589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-240(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-272(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	-280(%rbp), %r13
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -312(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-304(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-312(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L561
	call	_ZdlPv@PLT
.L561:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L585
.L562:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L564
	call	_ZdlPv@PLT
.L564:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L565
	.p2align 4,,10
	.p2align 3
.L569:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L566
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L569
.L567:
	movq	-232(%rbp), %r12
.L565:
	testq	%r12, %r12
	je	.L570
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L570:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L586
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L566:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L569
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L585:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L563
	call	_ZdlPv@PLT
.L563:
	movq	(%r15), %rax
	movl	$175, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	8(%rax), %r8
	movq	(%rax), %r15
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-304(%rbp), %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler15ToNumber_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert9ATfloat6420UT5ATSmi10HeapNumber_194EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movl	$176, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11Float64Log2ENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATfloat64_199EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L562
.L586:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22589:
	.size	_ZN2v88internal17MathLog2Assembler20GenerateMathLog2ImplEv, .-_ZN2v88internal17MathLog2Assembler20GenerateMathLog2ImplEv
	.section	.rodata._ZN2v88internal8Builtins17Generate_MathLog2EPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC19:
	.string	"MathLog2"
	.section	.text._ZN2v88internal8Builtins17Generate_MathLog2EPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins17Generate_MathLog2EPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins17Generate_MathLog2EPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins17Generate_MathLog2EPNS0_8compiler18CodeAssemblerStateE:
.LFB22585:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$751, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC19(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$837, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L591
.L588:
	movq	%r13, %rdi
	call	_ZN2v88internal17MathLog2Assembler20GenerateMathLog2ImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L592
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L591:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L588
.L592:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22585:
	.size	_ZN2v88internal8Builtins17Generate_MathLog2EPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins17Generate_MathLog2EPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal16MathSinAssembler19GenerateMathSinImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16MathSinAssembler19GenerateMathSinImplEv
	.type	_ZN2v88internal16MathSinAssembler19GenerateMathSinImplEv, @function
_ZN2v88internal16MathSinAssembler19GenerateMathSinImplEv:
.LFB22598:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-240(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-272(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	-280(%rbp), %r13
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -312(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-304(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-312(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L594
	call	_ZdlPv@PLT
.L594:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L618
.L595:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L597
	call	_ZdlPv@PLT
.L597:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L598
	.p2align 4,,10
	.p2align 3
.L602:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L599
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L602
.L600:
	movq	-232(%rbp), %r12
.L598:
	testq	%r12, %r12
	je	.L603
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L603:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L619
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L599:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L602
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L618:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L596
	call	_ZdlPv@PLT
.L596:
	movq	(%r15), %rax
	movl	$184, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	8(%rax), %r8
	movq	(%rax), %r15
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-304(%rbp), %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler15ToNumber_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert9ATfloat6420UT5ATSmi10HeapNumber_194EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movl	$185, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler10Float64SinENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATfloat64_199EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L595
.L619:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22598:
	.size	_ZN2v88internal16MathSinAssembler19GenerateMathSinImplEv, .-_ZN2v88internal16MathSinAssembler19GenerateMathSinImplEv
	.section	.rodata._ZN2v88internal8Builtins16Generate_MathSinEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC20:
	.string	"MathSin"
	.section	.text._ZN2v88internal8Builtins16Generate_MathSinEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins16Generate_MathSinEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins16Generate_MathSinEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins16Generate_MathSinEPNS0_8compiler18CodeAssemblerStateE:
.LFB22594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$782, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC20(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$838, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L624
.L621:
	movq	%r13, %rdi
	call	_ZN2v88internal16MathSinAssembler19GenerateMathSinImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L625
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L624:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L621
.L625:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22594:
	.size	_ZN2v88internal8Builtins16Generate_MathSinEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins16Generate_MathSinEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal17MathSignAssembler20GenerateMathSignImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17MathSignAssembler20GenerateMathSignImplEv
	.type	_ZN2v88internal17MathSignAssembler20GenerateMathSignImplEv, @function
_ZN2v88internal17MathSignAssembler20GenerateMathSignImplEv:
.LFB22607:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1000(%rbp), %r15
	leaq	-1056(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1088(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1160, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -1096(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	leaq	-1096(%rbp), %r12
	movq	%rax, -1152(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -1048(%rbp)
	movq	%rax, -1168(%rbp)
	movq	-1096(%rbp), %rax
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r15, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1048(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1096(%rbp), %rax
	movl	$96, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1112(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1096(%rbp), %rax
	movl	$96, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1120(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1096(%rbp), %rax
	movl	$96, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -1136(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1096(%rbp), %rax
	movl	$96, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -280(%rbp)
	leaq	-232(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -1128(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1152(%rbp), %xmm1
	movaps	%xmm0, -1088(%rbp)
	movhps	-1168(%rbp), %xmm1
	movq	$0, -1072(%rbp)
	movaps	%xmm1, -1152(%rbp)
	call	_Znwm@PLT
	movdqa	-1152(%rbp), %xmm1
	movq	%r14, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1088(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L627
	call	_ZdlPv@PLT
.L627:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -992(%rbp)
	jne	.L739
	cmpq	$0, -800(%rbp)
	jne	.L740
.L632:
	cmpq	$0, -608(%rbp)
	jne	.L741
.L634:
	cmpq	$0, -416(%rbp)
	jne	.L742
.L638:
	cmpq	$0, -224(%rbp)
	jne	.L743
.L640:
	movq	-1128(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L642
	call	_ZdlPv@PLT
.L642:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L643
	.p2align 4,,10
	.p2align 3
.L647:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L644
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L647
.L645:
	movq	-280(%rbp), %r13
.L643:
	testq	%r13, %r13
	je	.L648
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L648:
	movq	-1136(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L649
	call	_ZdlPv@PLT
.L649:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L650
	.p2align 4,,10
	.p2align 3
.L654:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L651
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L654
.L652:
	movq	-472(%rbp), %r13
.L650:
	testq	%r13, %r13
	je	.L655
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L655:
	movq	-1120(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L656
	call	_ZdlPv@PLT
.L656:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L657
	.p2align 4,,10
	.p2align 3
.L661:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L658
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L661
.L659:
	movq	-664(%rbp), %r13
.L657:
	testq	%r13, %r13
	je	.L662
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L662:
	movq	-1112(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L663
	call	_ZdlPv@PLT
.L663:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L664
	.p2align 4,,10
	.p2align 3
.L668:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L665
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L668
.L666:
	movq	-856(%rbp), %r13
.L664:
	testq	%r13, %r13
	je	.L669
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L669:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L670
	call	_ZdlPv@PLT
.L670:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L671
	.p2align 4,,10
	.p2align 3
.L675:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L672
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L675
.L673:
	movq	-1048(%rbp), %r13
.L671:
	testq	%r13, %r13
	je	.L676
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L676:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L744
	addq	$1160, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L672:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L675
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L665:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L668
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L658:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L661
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L644:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L647
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L651:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L654
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L739:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1072(%rbp)
	movaps	%xmm0, -1088(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1088(%rbp)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1088(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L629
	call	_ZdlPv@PLT
.L629:
	movq	(%r14), %rax
	movl	$191, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r14
	movq	8(%rax), %rax
	movq	%rax, -1152(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1152(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r14, -1176(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15ToNumber_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$192, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%r14, -1200(%rbp)
	call	_ZN2v88internal43Convert9ATfloat6420UT5ATSmi10HeapNumber_194EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movl	$194, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -1168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal46FromConstexpr9ATfloat6417ATconstexpr_int31_163EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1168(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15Float64LessThanENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-1200(%rbp), %xmm2
	movq	-1176(%rbp), %xmm3
	movaps	%xmm0, -1088(%rbp)
	movhps	-1168(%rbp), %xmm2
	movq	$0, -1072(%rbp)
	movhps	-1152(%rbp), %xmm3
	movaps	%xmm2, -1168(%rbp)
	movaps	%xmm3, -1152(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm2, -80(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -1088(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L630
	call	_ZdlPv@PLT
.L630:
	movdqa	-1152(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-1168(%rbp), %xmm7
	movaps	%xmm0, -1088(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	movq	$0, -1072(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -1088(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L631
	call	_ZdlPv@PLT
.L631:
	movq	-1120(%rbp), %rcx
	movq	-1112(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -800(%rbp)
	je	.L632
.L740:
	movq	-1112(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1072(%rbp)
	movaps	%xmm0, -1088(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$218630151, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1088(%rbp)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L633
	call	_ZdlPv@PLT
.L633:
	movl	$195, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$-1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -608(%rbp)
	je	.L634
.L741:
	movq	-1120(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1072(%rbp)
	movaps	%xmm0, -1088(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$218630151, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1088(%rbp)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1088(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L635
	call	_ZdlPv@PLT
.L635:
	movq	(%r14), %rax
	movl	$196, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	%rsi, -1176(%rbp)
	movq	16(%rax), %rsi
	movq	24(%rax), %rax
	movq	%rcx, -1168(%rbp)
	movq	%rsi, -1200(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rax, -1152(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal46FromConstexpr9ATfloat6417ATconstexpr_int31_163EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1152(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler18Float64GreaterThanENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-1200(%rbp), %xmm4
	movq	-1168(%rbp), %xmm5
	movaps	%xmm0, -1088(%rbp)
	movhps	-1152(%rbp), %xmm4
	movq	$0, -1072(%rbp)
	movhps	-1176(%rbp), %xmm5
	movaps	%xmm4, -1200(%rbp)
	movaps	%xmm5, -1152(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -1088(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L636
	call	_ZdlPv@PLT
.L636:
	movdqa	-1152(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-1200(%rbp), %xmm7
	movaps	%xmm0, -1088(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	movq	$0, -1072(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-288(%rbp), %rdi
	movq	%rax, -1088(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L637
	call	_ZdlPv@PLT
.L637:
	movq	-1128(%rbp), %rcx
	movq	-1136(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -416(%rbp)
	je	.L638
.L742:
	movq	-1136(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1072(%rbp)
	movaps	%xmm0, -1088(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$218630151, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1088(%rbp)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L639
	call	_ZdlPv@PLT
.L639:
	movl	$197, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -224(%rbp)
	je	.L640
.L743:
	movq	-1128(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-288(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1072(%rbp)
	movaps	%xmm0, -1088(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$218630151, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1088(%rbp)
	movq	%rdx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1088(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L641
	call	_ZdlPv@PLT
.L641:
	movq	(%r14), %rax
	movl	$199, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	movq	16(%rax), %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L640
.L744:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22607:
	.size	_ZN2v88internal17MathSignAssembler20GenerateMathSignImplEv, .-_ZN2v88internal17MathSignAssembler20GenerateMathSignImplEv
	.section	.rodata._ZN2v88internal8Builtins17Generate_MathSignEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC21:
	.string	"MathSign"
	.section	.text._ZN2v88internal8Builtins17Generate_MathSignEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins17Generate_MathSignEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins17Generate_MathSignEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins17Generate_MathSignEPNS0_8compiler18CodeAssemblerStateE:
.LFB22603:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$813, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC21(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$839, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L749
.L746:
	movq	%r13, %rdi
	call	_ZN2v88internal17MathSignAssembler20GenerateMathSignImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L750
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L749:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L746
.L750:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22603:
	.size	_ZN2v88internal8Builtins17Generate_MathSignEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins17Generate_MathSignEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal17MathSinhAssembler20GenerateMathSinhImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17MathSinhAssembler20GenerateMathSinhImplEv
	.type	_ZN2v88internal17MathSinhAssembler20GenerateMathSinhImplEv, @function
_ZN2v88internal17MathSinhAssembler20GenerateMathSinhImplEv:
.LFB22619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-240(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-272(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	-280(%rbp), %r13
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -312(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-304(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-312(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L752
	call	_ZdlPv@PLT
.L752:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L776
.L753:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L755
	call	_ZdlPv@PLT
.L755:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L756
	.p2align 4,,10
	.p2align 3
.L760:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L757
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L760
.L758:
	movq	-232(%rbp), %r12
.L756:
	testq	%r12, %r12
	je	.L761
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L761:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L777
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L757:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L760
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L776:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L754
	call	_ZdlPv@PLT
.L754:
	movq	(%r15), %rax
	movl	$208, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	8(%rax), %r8
	movq	(%rax), %r15
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-304(%rbp), %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler15ToNumber_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert9ATfloat6420UT5ATSmi10HeapNumber_194EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movl	$209, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11Float64SinhENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATfloat64_199EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L753
.L777:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22619:
	.size	_ZN2v88internal17MathSinhAssembler20GenerateMathSinhImplEv, .-_ZN2v88internal17MathSinhAssembler20GenerateMathSinhImplEv
	.section	.rodata._ZN2v88internal8Builtins17Generate_MathSinhEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC22:
	.string	"MathSinh"
	.section	.text._ZN2v88internal8Builtins17Generate_MathSinhEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins17Generate_MathSinhEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins17Generate_MathSinhEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins17Generate_MathSinhEPNS0_8compiler18CodeAssemblerStateE:
.LFB22615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$901, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$840, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L782
.L779:
	movq	%r13, %rdi
	call	_ZN2v88internal17MathSinhAssembler20GenerateMathSinhImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L783
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L782:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L779
.L783:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22615:
	.size	_ZN2v88internal8Builtins17Generate_MathSinhEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins17Generate_MathSinhEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal17MathSqrtAssembler20GenerateMathSqrtImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17MathSqrtAssembler20GenerateMathSqrtImplEv
	.type	_ZN2v88internal17MathSqrtAssembler20GenerateMathSqrtImplEv, @function
_ZN2v88internal17MathSqrtAssembler20GenerateMathSqrtImplEv:
.LFB22628:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-240(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-272(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	-280(%rbp), %r13
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -312(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-304(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-312(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L785
	call	_ZdlPv@PLT
.L785:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L809
.L786:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L788
	call	_ZdlPv@PLT
.L788:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L789
	.p2align 4,,10
	.p2align 3
.L793:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L790
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L793
.L791:
	movq	-232(%rbp), %r12
.L789:
	testq	%r12, %r12
	je	.L794
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L794:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L810
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L790:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L793
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L809:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L787
	call	_ZdlPv@PLT
.L787:
	movq	(%r15), %rax
	movl	$217, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	8(%rax), %r8
	movq	(%rax), %r15
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-304(%rbp), %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler15ToNumber_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert9ATfloat6420UT5ATSmi10HeapNumber_194EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movl	$218, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11Float64SqrtENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATfloat64_199EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L786
.L810:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22628:
	.size	_ZN2v88internal17MathSqrtAssembler20GenerateMathSqrtImplEv, .-_ZN2v88internal17MathSqrtAssembler20GenerateMathSqrtImplEv
	.section	.rodata._ZN2v88internal8Builtins17Generate_MathSqrtEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC23:
	.string	"MathSqrt"
	.section	.text._ZN2v88internal8Builtins17Generate_MathSqrtEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins17Generate_MathSqrtEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins17Generate_MathSqrtEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins17Generate_MathSqrtEPNS0_8compiler18CodeAssemblerStateE:
.LFB22624:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$932, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC23(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$841, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L815
.L812:
	movq	%r13, %rdi
	call	_ZN2v88internal17MathSqrtAssembler20GenerateMathSqrtImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L816
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L815:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L812
.L816:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22624:
	.size	_ZN2v88internal8Builtins17Generate_MathSqrtEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins17Generate_MathSqrtEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal16MathTanAssembler19GenerateMathTanImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16MathTanAssembler19GenerateMathTanImplEv
	.type	_ZN2v88internal16MathTanAssembler19GenerateMathTanImplEv, @function
_ZN2v88internal16MathTanAssembler19GenerateMathTanImplEv:
.LFB22637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-240(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-272(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	-280(%rbp), %r13
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -312(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-304(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-312(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L818
	call	_ZdlPv@PLT
.L818:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L842
.L819:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L821
	call	_ZdlPv@PLT
.L821:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L822
	.p2align 4,,10
	.p2align 3
.L826:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L823
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L826
.L824:
	movq	-232(%rbp), %r12
.L822:
	testq	%r12, %r12
	je	.L827
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L827:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L843
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L823:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L826
	jmp	.L824
	.p2align 4,,10
	.p2align 3
.L842:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L820
	call	_ZdlPv@PLT
.L820:
	movq	(%r15), %rax
	movl	$226, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	8(%rax), %r8
	movq	(%rax), %r15
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-304(%rbp), %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler15ToNumber_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert9ATfloat6420UT5ATSmi10HeapNumber_194EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movl	$227, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler10Float64TanENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATfloat64_199EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L819
.L843:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22637:
	.size	_ZN2v88internal16MathTanAssembler19GenerateMathTanImplEv, .-_ZN2v88internal16MathTanAssembler19GenerateMathTanImplEv
	.section	.rodata._ZN2v88internal8Builtins16Generate_MathTanEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC24:
	.string	"MathTan"
	.section	.text._ZN2v88internal8Builtins16Generate_MathTanEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins16Generate_MathTanEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins16Generate_MathTanEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins16Generate_MathTanEPNS0_8compiler18CodeAssemblerStateE:
.LFB22633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$963, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC24(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$842, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L848
.L845:
	movq	%r13, %rdi
	call	_ZN2v88internal16MathTanAssembler19GenerateMathTanImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L849
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L848:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L845
.L849:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22633:
	.size	_ZN2v88internal8Builtins16Generate_MathTanEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins16Generate_MathTanEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal17MathTanhAssembler20GenerateMathTanhImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17MathTanhAssembler20GenerateMathTanhImplEv
	.type	_ZN2v88internal17MathTanhAssembler20GenerateMathTanhImplEv, @function
_ZN2v88internal17MathTanhAssembler20GenerateMathTanhImplEv:
.LFB22646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-240(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-272(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	leaq	-280(%rbp), %r13
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -312(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-304(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-312(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%rbx, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L851
	call	_ZdlPv@PLT
.L851:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L875
.L852:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L854
	call	_ZdlPv@PLT
.L854:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L855
	.p2align 4,,10
	.p2align 3
.L859:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L856
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L859
.L857:
	movq	-232(%rbp), %r12
.L855:
	testq	%r12, %r12
	je	.L860
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L860:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L876
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L856:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L859
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L875:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L853
	call	_ZdlPv@PLT
.L853:
	movq	(%r15), %rax
	movl	$235, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	8(%rax), %r8
	movq	(%rax), %r15
	movq	%r8, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-304(%rbp), %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal17CodeStubAssembler15ToNumber_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert9ATfloat6420UT5ATSmi10HeapNumber_194EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movl	$236, %edx
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11Float64TanhENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATfloat64_199EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8Float64TEEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L852
.L876:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22646:
	.size	_ZN2v88internal17MathTanhAssembler20GenerateMathTanhImplEv, .-_ZN2v88internal17MathTanhAssembler20GenerateMathTanhImplEv
	.section	.rodata._ZN2v88internal8Builtins17Generate_MathTanhEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC25:
	.string	"MathTanh"
	.section	.text._ZN2v88internal8Builtins17Generate_MathTanhEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins17Generate_MathTanhEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins17Generate_MathTanhEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins17Generate_MathTanhEPNS0_8compiler18CodeAssemblerStateE:
.LFB22642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$994, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC25(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$843, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L881
.L878:
	movq	%r13, %rdi
	call	_ZN2v88internal17MathTanhAssembler20GenerateMathTanhImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L882
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L881:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L878
.L882:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22642:
	.size	_ZN2v88internal8Builtins17Generate_MathTanhEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins17Generate_MathTanhEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EE:
.LFB26937:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$2, %edi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -64(%rbp)
	movq	$0, -48(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r12, %rdi
	leaq	-64(%rbp), %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -64(%rbp)
	movq	%rdx, -48(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-64(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L884
	call	_ZdlPv@PLT
.L884:
	movq	(%r12), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L885
	movq	%rdx, 0(%r13)
.L885:
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L883
	movq	%rax, (%rbx)
.L883:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L898
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L898:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26937:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EE
	.section	.text._ZN2v88internal18MathClz32Assembler21GenerateMathClz32ImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18MathClz32Assembler21GenerateMathClz32ImplEv
	.type	_ZN2v88internal18MathClz32Assembler21GenerateMathClz32ImplEv, @function
_ZN2v88internal18MathClz32Assembler21GenerateMathClz32ImplEv:
.LFB22499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1016(%rbp), %r14
	leaq	-1072(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1200(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1304, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -1256(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	leaq	-1256(%rbp), %r12
	movq	%rax, -1312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -1064(%rbp)
	movq	%rax, -1320(%rbp)
	movq	-1256(%rbp), %rax
	movq	$0, -1056(%rbp)
	movq	%rax, -1072(%rbp)
	movq	$0, -1048(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -1048(%rbp)
	movq	%rdx, -1056(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1032(%rbp)
	movq	%rax, -1064(%rbp)
	movq	$0, -1040(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1256(%rbp), %rax
	movl	$144, %edi
	movq	$0, -872(%rbp)
	movq	$0, -864(%rbp)
	movq	%rax, -880(%rbp)
	movq	$0, -856(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -872(%rbp)
	leaq	-824(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -856(%rbp)
	movq	%rdx, -864(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -840(%rbp)
	movq	%rax, -1296(%rbp)
	movq	$0, -848(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1256(%rbp), %rax
	movl	$168, %edi
	movq	$0, -680(%rbp)
	movq	$0, -672(%rbp)
	movq	%rax, -688(%rbp)
	movq	$0, -664(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -680(%rbp)
	leaq	-632(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -664(%rbp)
	movq	%rdx, -672(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -648(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -656(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1256(%rbp), %rax
	movl	$120, %edi
	movq	$0, -488(%rbp)
	movq	$0, -480(%rbp)
	movq	%rax, -496(%rbp)
	movq	$0, -472(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -488(%rbp)
	leaq	-440(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -472(%rbp)
	movq	%rdx, -480(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -456(%rbp)
	movq	%rax, -1288(%rbp)
	movq	$0, -464(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1256(%rbp), %rax
	movl	$120, %edi
	movq	$0, -296(%rbp)
	movq	$0, -288(%rbp)
	movq	%rax, -304(%rbp)
	movq	$0, -280(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -296(%rbp)
	leaq	-248(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -280(%rbp)
	movq	%rdx, -288(%rbp)
	xorl	%edx, %edx
	movq	%rax, -1272(%rbp)
	movups	%xmm0, -264(%rbp)
	movq	$0, -272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1312(%rbp), %xmm1
	movaps	%xmm0, -1200(%rbp)
	movhps	-1320(%rbp), %xmm1
	movq	$0, -1184(%rbp)
	movaps	%xmm1, -1312(%rbp)
	call	_Znwm@PLT
	movdqa	-1312(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1200(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1184(%rbp)
	movq	%rdx, -1192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L900
	call	_ZdlPv@PLT
.L900:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1008(%rbp)
	jne	.L1016
	cmpq	$0, -816(%rbp)
	jne	.L1017
.L905:
	cmpq	$0, -624(%rbp)
	jne	.L1018
.L908:
	cmpq	$0, -432(%rbp)
	jne	.L1019
.L912:
	cmpq	$0, -240(%rbp)
	jne	.L1020
.L915:
	movq	-1272(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L917
	call	_ZdlPv@PLT
.L917:
	movq	-288(%rbp), %rbx
	movq	-296(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L918
	.p2align 4,,10
	.p2align 3
.L922:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L919
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L922
.L920:
	movq	-296(%rbp), %r13
.L918:
	testq	%r13, %r13
	je	.L923
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L923:
	movq	-1288(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-464(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L924
	call	_ZdlPv@PLT
.L924:
	movq	-480(%rbp), %rbx
	movq	-488(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L925
	.p2align 4,,10
	.p2align 3
.L929:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L926
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L929
.L927:
	movq	-488(%rbp), %r13
.L925:
	testq	%r13, %r13
	je	.L930
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L930:
	movq	-1280(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L931
	call	_ZdlPv@PLT
.L931:
	movq	-672(%rbp), %rbx
	movq	-680(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L932
	.p2align 4,,10
	.p2align 3
.L936:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L933
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L936
.L934:
	movq	-680(%rbp), %r13
.L932:
	testq	%r13, %r13
	je	.L937
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L937:
	movq	-1296(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-848(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L938
	call	_ZdlPv@PLT
.L938:
	movq	-864(%rbp), %rbx
	movq	-872(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L939
	.p2align 4,,10
	.p2align 3
.L943:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L940
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L943
.L941:
	movq	-872(%rbp), %r13
.L939:
	testq	%r13, %r13
	je	.L944
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L944:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1040(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L945
	call	_ZdlPv@PLT
.L945:
	movq	-1056(%rbp), %rbx
	movq	-1064(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L946
	.p2align 4,,10
	.p2align 3
.L950:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L947
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L950
.L948:
	movq	-1064(%rbp), %r13
.L946:
	testq	%r13, %r13
	je	.L951
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L951:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1021
	addq	$1304, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L947:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L950
	jmp	.L948
	.p2align 4,,10
	.p2align 3
.L940:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L943
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L933:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L936
	jmp	.L934
	.p2align 4,,10
	.p2align 3
.L919:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L922
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L926:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L929
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L1016:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	$0, -1248(%rbp)
	movq	$0, -1240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	leaq	-1240(%rbp), %rdx
	leaq	-1248(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EE
	movl	$84, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1240(%rbp), %rdx
	movq	-1248(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ToNumber_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$86, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$87, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$88, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal13Cast5ATSmi_83EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %xmm2
	movq	-1248(%rbp), %xmm0
	movl	$56, %edi
	punpcklqdq	%xmm2, %xmm2
	movq	%r15, -96(%rbp)
	movhps	-1240(%rbp), %xmm0
	movaps	%xmm2, -1312(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -80(%rbp)
	movaps	%xmm0, -1232(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -64(%rbp)
	movq	$0, -1216(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-112(%rbp), %xmm3
	leaq	-688(%rbp), %rdi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	leaq	56(%rax), %rdx
	movq	%rax, -1232(%rbp)
	movq	%rcx, 48(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	leaq	-1232(%rbp), %rax
	movq	%rax, %rsi
	movq	%rdx, -1216(%rbp)
	movq	%rdx, -1224(%rbp)
	movq	%rax, -1320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L902
	call	_ZdlPv@PLT
.L902:
	movq	-1280(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1192(%rbp)
	jne	.L1022
.L903:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -816(%rbp)
	je	.L905
.L1017:
	movq	-1296(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-880(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -1184(%rbp)
	movaps	%xmm0, -1200(%rbp)
	call	_Znwm@PLT
	movl	$2056, %esi
	movq	%r15, %rdi
	movw	%si, 4(%rax)
	leaq	6(%rax), %rdx
	movq	%r13, %rsi
	movl	$67635207, (%rax)
	movq	%rax, -1200(%rbp)
	movq	%rdx, -1184(%rbp)
	movq	%rdx, -1192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1200(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L906
	call	_ZdlPv@PLT
.L906:
	movq	(%r15), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	32(%rax), %rdx
	movdqu	(%rax), %xmm6
	movq	$0, -1184(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -80(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm0, -1200(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm3
	leaq	40(%rax), %rdx
	leaq	-496(%rbp), %rdi
	movq	%rax, -1200(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -1184(%rbp)
	movq	%rdx, -1192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L907
	call	_ZdlPv@PLT
.L907:
	movq	-1288(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -624(%rbp)
	je	.L908
.L1018:
	movq	-1280(%rbp), %rsi
	leaq	-688(%rbp), %r8
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	movq	%r8, -1312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -1184(%rbp)
	movaps	%xmm0, -1200(%rbp)
	call	_Znwm@PLT
	movl	$2056, %ecx
	movq	-1312(%rbp), %r8
	movq	%r13, %rsi
	movl	$67635207, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%r8, %rdi
	movb	$6, 6(%rax)
	movq	%rax, -1200(%rbp)
	movq	%rdx, -1184(%rbp)
	movq	%rdx, -1192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L909
	movq	%rax, -1312(%rbp)
	call	_ZdlPv@PLT
	movq	-1312(%rbp), %rax
.L909:
	movq	(%rax), %rax
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	48(%rax), %rdx
	movq	16(%rax), %rcx
	movq	(%rax), %xmm0
	testq	%rdx, %rdx
	movq	%rcx, -1312(%rbp)
	movq	32(%rax), %rcx
	cmovne	%rdx, %r15
	movhps	8(%rax), %xmm0
	movl	$89, %edx
	movq	%rcx, -1328(%rbp)
	movaps	%xmm0, -1344(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal25Convert7ATint325ATSmi_192EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$88, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -1320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movdqa	-1344(%rbp), %xmm0
	movl	$40, %edi
	movq	-1328(%rbp), %rcx
	movq	$0, -1184(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	-1312(%rbp), %xmm0
	movq	%rcx, -80(%rbp)
	movhps	-1320(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1200(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	leaq	40(%rax), %rdx
	leaq	-304(%rbp), %rdi
	movq	%rax, -1200(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1184(%rbp)
	movq	%rdx, -1192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L911
	call	_ZdlPv@PLT
.L911:
	movq	-1272(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -432(%rbp)
	je	.L912
.L1019:
	movq	-1288(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-496(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1184(%rbp)
	movaps	%xmm0, -1200(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$67635207, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -1200(%rbp)
	movq	%rdx, -1184(%rbp)
	movq	%rdx, -1192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1200(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L913
	call	_ZdlPv@PLT
.L913:
	movq	(%r15), %rax
	movl	$91, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	32(%rax), %r15
	movq	%rsi, -1320(%rbp)
	movq	16(%rax), %rsi
	movq	%rcx, -1312(%rbp)
	movq	%rsi, -1344(%rbp)
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$92, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler31TruncateHeapNumberValueToWord32ENS0_8compiler5TNodeINS0_10HeapNumberEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -1328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$88, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$40, %edi
	movq	%r15, -80(%rbp)
	movq	-1312(%rbp), %xmm0
	movq	$0, -1184(%rbp)
	movhps	-1320(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-1344(%rbp), %xmm0
	movhps	-1328(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1200(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	40(%rax), %rdx
	leaq	-304(%rbp), %rdi
	movq	%rax, -1200(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1184(%rbp)
	movq	%rdx, -1192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L914
	call	_ZdlPv@PLT
.L914:
	movq	-1272(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -240(%rbp)
	je	.L915
.L1020:
	movq	-1272(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-304(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1184(%rbp)
	movaps	%xmm0, -1200(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$67635207, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -1200(%rbp)
	movq	%rdx, -1184(%rbp)
	movq	%rdx, -1192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1200(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L916
	call	_ZdlPv@PLT
.L916:
	movq	(%r15), %rax
	movq	%r12, %rdi
	movl	$87, %edx
	leaq	.LC1(%rip), %rsi
	movq	24(%rax), %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$96, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9Word32ClzENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal41Convert20UT5ATSmi10HeapNumber7ATint32_173EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6Int32TEEE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L915
	.p2align 4,,10
	.p2align 3
.L1022:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$48, %edi
	movq	-1248(%rbp), %xmm0
	movdqa	-1312(%rbp), %xmm3
	movq	%r15, -96(%rbp)
	movhps	-1240(%rbp), %xmm0
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1232(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -1216(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	leaq	-880(%rbp), %rdi
	movdqa	-80(%rbp), %xmm6
	movq	-1320(%rbp), %rsi
	leaq	48(%rax), %rdx
	movq	%rax, -1232(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -1216(%rbp)
	movq	%rdx, -1224(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L904
	call	_ZdlPv@PLT
.L904:
	movq	-1296(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L903
.L1021:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22499:
	.size	_ZN2v88internal18MathClz32Assembler21GenerateMathClz32ImplEv, .-_ZN2v88internal18MathClz32Assembler21GenerateMathClz32ImplEv
	.section	.rodata._ZN2v88internal8Builtins18Generate_MathClz32EPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC26:
	.string	"MathClz32"
	.section	.text._ZN2v88internal8Builtins18Generate_MathClz32EPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins18Generate_MathClz32EPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins18Generate_MathClz32EPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins18Generate_MathClz32EPNS0_8compiler18CodeAssemblerStateE:
.LFB22495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$404, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC26(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$828, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L1027
.L1024:
	movq	%r13, %rdi
	call	_ZN2v88internal18MathClz32Assembler21GenerateMathClz32ImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1028
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1027:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L1024
.L1028:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22495:
	.size	_ZN2v88internal8Builtins18Generate_MathClz32EPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins18Generate_MathClz32EPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESF_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESF_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESF_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESF_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESF_:
.LFB26975:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$10, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$290206233024333061, %rcx
	movq	%rcx, (%rax)
	movl	$1293, %ecx
	leaq	10(%rax), %rdx
	movw	%cx, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1030
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %rax
.L1030:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1031
	movq	%rdx, (%r15)
.L1031:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1032
	movq	%rdx, (%r14)
.L1032:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1033
	movq	%rdx, 0(%r13)
.L1033:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1034
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1034:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1035
	movq	%rdx, (%rbx)
.L1035:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1036
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1036:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1037
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1037:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1038
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1038:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1039
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1039:
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L1029
	movq	-128(%rbp), %rsi
	movq	%rax, (%rsi)
.L1029:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1076
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1076:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26975:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESF_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESF_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESF_SP_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESF_SP_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESF_SP_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESF_SP_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESF_SP_:
.LFB26977:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$11, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$290206233024333061, %rcx
	movq	%rcx, (%rax)
	movl	$1293, %ecx
	leaq	11(%rax), %rdx
	movw	%cx, 8(%rax)
	movb	$13, 10(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1078
	movq	%rax, -144(%rbp)
	call	_ZdlPv@PLT
	movq	-144(%rbp), %rax
.L1078:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1079
	movq	%rdx, (%r15)
.L1079:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1080
	movq	%rdx, (%r14)
.L1080:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1081
	movq	%rdx, 0(%r13)
.L1081:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1082
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1082:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1083
	movq	%rdx, (%rbx)
.L1083:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1084
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1084:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1085
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1085:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1086
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1086:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1087
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1087:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1088
	movq	-128(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1088:
	movq	80(%rax), %rax
	testq	%rax, %rax
	je	.L1077
	movq	-136(%rbp), %rcx
	movq	%rax, (%rcx)
.L1077:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1128
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1128:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26977:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESF_SP_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESF_SP_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_S9_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESG_SQ_SQ_SM_SG_SG_SG_SG_PNSC_ISA_EESG_SG_SG_SG_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_S9_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESG_SQ_SQ_SM_SG_SG_SG_SG_PNSC_ISA_EESG_SG_SG_SG_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_S9_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESG_SQ_SQ_SM_SG_SG_SG_SG_PNSC_ISA_EESG_SG_SG_SG_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_S9_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESG_SQ_SQ_SM_SG_SG_SG_SG_PNSC_ISA_EESG_SG_SG_SG_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_S9_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESG_SQ_SQ_SM_SG_SG_SG_SG_PNSC_ISA_EESG_SG_SG_SG_:
.LFB26983:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$22, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	88(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	96(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	104(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	112(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	120(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	128(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	136(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	144(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$1285, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movdqa	.LC27(%rip), %xmm0
	movl	$84215557, 16(%rax)
	leaq	22(%rax), %rdx
	movw	%cx, 20(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1130
	movq	%rax, -232(%rbp)
	call	_ZdlPv@PLT
	movq	-232(%rbp), %rax
.L1130:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1131
	movq	%rdx, (%r15)
.L1131:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1132
	movq	%rdx, (%r14)
.L1132:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1133
	movq	%rdx, 0(%r13)
.L1133:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1134
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1134:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1135
	movq	%rdx, (%rbx)
.L1135:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1136
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1136:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1137
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1137:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1138
	movq	-112(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1138:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1139
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1139:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1140
	movq	-128(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1140:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1141
	movq	-136(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1141:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1142
	movq	-144(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1142:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1143
	movq	-152(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1143:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1144
	movq	-160(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1144:
	movq	112(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1145
	movq	-168(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1145:
	movq	120(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1146
	movq	-176(%rbp), %rdi
	movq	%rdx, (%rdi)
.L1146:
	movq	128(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1147
	movq	-184(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1147:
	movq	136(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1148
	movq	-192(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1148:
	movq	144(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1149
	movq	-200(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1149:
	movq	152(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1150
	movq	-208(%rbp), %rdi
	movq	%rdx, (%rdi)
.L1150:
	movq	160(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1151
	movq	-216(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1151:
	movq	168(%rax), %rax
	testq	%rax, %rax
	je	.L1129
	movq	-224(%rbp), %rbx
	movq	%rax, (%rbx)
.L1129:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1224
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1224:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26983:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_S9_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESG_SQ_SQ_SM_SG_SG_SG_SG_PNSC_ISA_EESG_SG_SG_SG_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_S9_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESG_SQ_SQ_SM_SG_SG_SG_SG_PNSC_ISA_EESG_SG_SG_SG_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_S9_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESG_SQ_SQ_SM_SG_SG_SG_SG_PNSC_ISA_EESG_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_S9_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESG_SQ_SQ_SM_SG_SG_SG_SG_PNSC_ISA_EESG_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_S9_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESG_SQ_SQ_SM_SG_SG_SG_SG_PNSC_ISA_EESG_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_S9_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESG_SQ_SQ_SM_SG_SG_SG_SG_PNSC_ISA_EESG_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_S9_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESG_SQ_SQ_SM_SG_SG_SG_SG_PNSC_ISA_EESG_:
.LFB26999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$19, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$168, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	88(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	96(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	104(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	112(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	120(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$1797, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movdqa	.LC27(%rip), %xmm0
	movw	%cx, 16(%rax)
	leaq	19(%rax), %rdx
	movb	$5, 18(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1226
	movq	%rax, -208(%rbp)
	call	_ZdlPv@PLT
	movq	-208(%rbp), %rax
.L1226:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1227
	movq	%rdx, (%r15)
.L1227:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1228
	movq	%rdx, (%r14)
.L1228:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1229
	movq	%rdx, 0(%r13)
.L1229:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1230
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1230:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1231
	movq	%rdx, (%rbx)
.L1231:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1232
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1232:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1233
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1233:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1234
	movq	-112(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1234:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1235
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1235:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1236
	movq	-128(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1236:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1237
	movq	-136(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1237:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1238
	movq	-144(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1238:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1239
	movq	-152(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1239:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1240
	movq	-160(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1240:
	movq	112(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1241
	movq	-168(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1241:
	movq	120(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1242
	movq	-176(%rbp), %rdi
	movq	%rdx, (%rdi)
.L1242:
	movq	128(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1243
	movq	-184(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1243:
	movq	136(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1244
	movq	-192(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1244:
	movq	144(%rax), %rax
	testq	%rax, %rax
	je	.L1225
	movq	-200(%rbp), %rsi
	movq	%rax, (%rsi)
.L1225:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1308
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1308:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26999:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_S9_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESG_SQ_SQ_SM_SG_SG_SG_SG_PNSC_ISA_EESG_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_S9_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESG_SQ_SQ_SM_SG_SG_SG_SG_PNSC_ISA_EESG_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TEEE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TEEE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TEEE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TEEE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TEEE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EE:
.LFB27003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$9, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$290206233024333061, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$13, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1310
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
.L1310:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1311
	movq	%rdx, (%r15)
.L1311:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1312
	movq	%rdx, (%r14)
.L1312:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1313
	movq	%rdx, 0(%r13)
.L1313:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1314
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1314:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1315
	movq	%rdx, (%rbx)
.L1315:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1316
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1316:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1317
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1317:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1318
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1318:
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L1309
	movq	-120(%rbp), %rbx
	movq	%rax, (%rbx)
.L1309:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1352
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1352:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27003:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TEEE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TEEE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESP_SP_SF_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESP_SP_SF_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESP_SP_SF_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESP_SP_SF_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESP_SP_SF_:
.LFB27008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$12, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$120, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$290206233024333061, %rcx
	movq	%rcx, (%rax)
	leaq	12(%rax), %rdx
	movl	$84741389, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1354
	movq	%rax, -152(%rbp)
	call	_ZdlPv@PLT
	movq	-152(%rbp), %rax
.L1354:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1355
	movq	%rdx, (%r15)
.L1355:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1356
	movq	%rdx, (%r14)
.L1356:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1357
	movq	%rdx, 0(%r13)
.L1357:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1358
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1358:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1359
	movq	%rdx, (%rbx)
.L1359:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1360
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1360:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1361
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1361:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1362
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1362:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1363
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1363:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1364
	movq	-128(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1364:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1365
	movq	-136(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1365:
	movq	88(%rax), %rax
	testq	%rax, %rax
	je	.L1353
	movq	-144(%rbp), %rbx
	movq	%rax, (%rbx)
.L1353:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1408
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1408:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27008:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESP_SP_SF_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESP_SP_SF_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESQ_SQ_SG_SM_SG_SG_SG_SG_PNSC_ISA_EESG_SG_SG_SG_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESQ_SQ_SG_SM_SG_SG_SG_SG_PNSC_ISA_EESG_SG_SG_SG_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESQ_SQ_SG_SM_SG_SG_SG_SG_PNSC_ISA_EESG_SG_SG_SG_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESQ_SQ_SG_SM_SG_SG_SG_SG_PNSC_ISA_EESG_SG_SG_SG_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESQ_SQ_SG_SM_SG_SG_SG_SG_PNSC_ISA_EESG_SG_SG_SG_:
.LFB27010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$22, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	88(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	96(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	104(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	112(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	120(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	128(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	136(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	144(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$1285, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movdqa	.LC28(%rip), %xmm0
	movl	$84215557, 16(%rax)
	leaq	22(%rax), %rdx
	movw	%cx, 20(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1410
	movq	%rax, -232(%rbp)
	call	_ZdlPv@PLT
	movq	-232(%rbp), %rax
.L1410:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1411
	movq	%rdx, (%r15)
.L1411:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1412
	movq	%rdx, (%r14)
.L1412:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1413
	movq	%rdx, 0(%r13)
.L1413:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1414
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1414:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1415
	movq	%rdx, (%rbx)
.L1415:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1416
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1416:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1417
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1417:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1418
	movq	-112(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1418:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1419
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1419:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1420
	movq	-128(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1420:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1421
	movq	-136(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1421:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1422
	movq	-144(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1422:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1423
	movq	-152(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1423:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1424
	movq	-160(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1424:
	movq	112(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1425
	movq	-168(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1425:
	movq	120(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1426
	movq	-176(%rbp), %rdi
	movq	%rdx, (%rdi)
.L1426:
	movq	128(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1427
	movq	-184(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1427:
	movq	136(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1428
	movq	-192(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1428:
	movq	144(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1429
	movq	-200(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1429:
	movq	152(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1430
	movq	-208(%rbp), %rdi
	movq	%rdx, (%rdi)
.L1430:
	movq	160(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1431
	movq	-216(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1431:
	movq	168(%rax), %rax
	testq	%rax, %rax
	je	.L1409
	movq	-224(%rbp), %rbx
	movq	%rax, (%rbx)
.L1409:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1504
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1504:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27010:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESQ_SQ_SG_SM_SG_SG_SG_SG_PNSC_ISA_EESG_SG_SG_SG_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESQ_SQ_SG_SM_SG_SG_SG_SG_PNSC_ISA_EESG_SG_SG_SG_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESQ_SQ_SG_SM_SG_SG_SG_SG_PNSC_ISA_EESG_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESQ_SQ_SG_SM_SG_SG_SG_SG_PNSC_ISA_EESG_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESQ_SQ_SG_SM_SG_SG_SG_SG_PNSC_ISA_EESG_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESQ_SQ_SG_SM_SG_SG_SG_SG_PNSC_ISA_EESG_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESQ_SQ_SG_SM_SG_SG_SG_SG_PNSC_ISA_EESG_:
.LFB27016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$19, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$168, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	88(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	96(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	104(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	112(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	120(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$1797, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movdqa	.LC28(%rip), %xmm0
	movw	%cx, 16(%rax)
	leaq	19(%rax), %rdx
	movb	$5, 18(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1506
	movq	%rax, -208(%rbp)
	call	_ZdlPv@PLT
	movq	-208(%rbp), %rax
.L1506:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1507
	movq	%rdx, (%r15)
.L1507:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1508
	movq	%rdx, (%r14)
.L1508:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1509
	movq	%rdx, 0(%r13)
.L1509:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1510
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1510:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1511
	movq	%rdx, (%rbx)
.L1511:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1512
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1512:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1513
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1513:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1514
	movq	-112(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1514:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1515
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1515:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1516
	movq	-128(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1516:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1517
	movq	-136(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1517:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1518
	movq	-144(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1518:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1519
	movq	-152(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1519:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1520
	movq	-160(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1520:
	movq	112(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1521
	movq	-168(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1521:
	movq	120(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1522
	movq	-176(%rbp), %rdi
	movq	%rdx, (%rdi)
.L1522:
	movq	128(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1523
	movq	-184(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1523:
	movq	136(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1524
	movq	-192(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1524:
	movq	144(%rax), %rax
	testq	%rax, %rax
	je	.L1505
	movq	-200(%rbp), %rsi
	movq	%rax, (%rsi)
.L1505:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1588
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1588:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27016:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESQ_SQ_SG_SM_SG_SG_SG_SG_PNSC_ISA_EESG_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESQ_SQ_SG_SM_SG_SG_SG_SG_PNSC_ISA_EESG_
	.section	.rodata._ZN2v88internal18MathHypotAssembler21GenerateMathHypotImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC29:
	.string	"../../deps/v8/../../deps/v8/src/builtins/torque-internal.tq"
	.section	.text._ZN2v88internal18MathHypotAssembler21GenerateMathHypotImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18MathHypotAssembler21GenerateMathHypotImplEv
	.type	_ZN2v88internal18MathHypotAssembler21GenerateMathHypotImplEv, @function
_ZN2v88internal18MathHypotAssembler21GenerateMathHypotImplEv:
.LFB22655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$4024, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$1, %esi
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %r12
	movq	%r13, -7496(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r14, %rcx
	movq	%r12, %rsi
	leaq	-7312(%rbp), %rdi
	movq	%rax, %rdx
	leaq	-7496(%rbp), %r14
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-7304(%rbp), %rbx
	movl	$2, %esi
	movq	%r12, %rdi
	movq	-7296(%rbp), %rcx
	movq	-7312(%rbp), %rax
	movq	%r12, -7216(%rbp)
	leaq	-7112(%rbp), %r12
	movq	%rbx, -7552(%rbp)
	movq	%rbx, -7192(%rbp)
	leaq	-7216(%rbp), %rbx
	movq	%rcx, -7536(%rbp)
	movq	%rcx, -7200(%rbp)
	movq	$1, -7208(%rbp)
	movq	%rax, -7568(%rbp)
	movq	%rax, -7184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rbx, %rdi
	movq	%rbx, -7752(%rbp)
	leaq	-6920(%rbp), %rbx
	movq	%rax, -7520(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -7160(%rbp)
	movq	$0, -7152(%rbp)
	movq	%rax, %r15
	movq	-7496(%rbp), %rax
	movq	$0, -7144(%rbp)
	movq	%rax, -7168(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -7144(%rbp)
	movq	%rdx, -7152(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -7128(%rbp)
	movq	%rax, -7160(%rbp)
	movq	$0, -7136(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7496(%rbp), %rax
	movl	$144, %edi
	movq	$0, -6968(%rbp)
	movq	$0, -6960(%rbp)
	movq	%rax, -6976(%rbp)
	movq	$0, -6952(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rdx, -6952(%rbp)
	movq	%rdx, -6960(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6936(%rbp)
	movq	%rax, -6968(%rbp)
	movq	%rbx, -7616(%rbp)
	leaq	-6728(%rbp), %rbx
	movq	$0, -6944(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7496(%rbp), %rax
	movl	$144, %edi
	movq	$0, -6776(%rbp)
	movq	$0, -6768(%rbp)
	movq	%rax, -6784(%rbp)
	movq	$0, -6760(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rdx, -6760(%rbp)
	movq	%rdx, -6768(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6744(%rbp)
	movq	%rax, -6776(%rbp)
	movq	$0, -6752(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7496(%rbp), %rax
	movl	$240, %edi
	movq	$0, -6584(%rbp)
	movq	$0, -6576(%rbp)
	movq	%rax, -6592(%rbp)
	movq	$0, -6568(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-6536(%rbp), %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rsi, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rdx, -6568(%rbp)
	movq	%rdx, -6576(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -7768(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -6552(%rbp)
	movq	%rax, -6584(%rbp)
	movq	$0, -6560(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-6400(%rbp), %rsi
	movl	$10, %edx
	movq	%rsi, %rdi
	movq	%rsi, -7576(%rbp)
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-7496(%rbp), %rax
	movl	$264, %edi
	movq	$0, -6200(%rbp)
	movq	$0, -6192(%rbp)
	movq	%rax, -6208(%rbp)
	movq	$0, -6184(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-6152(%rbp), %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rdx, -6184(%rbp)
	movq	%rdx, -6192(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -7648(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -6168(%rbp)
	movq	%rax, -6200(%rbp)
	movq	$0, -6176(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7496(%rbp), %rax
	movl	$264, %edi
	movq	$0, -6008(%rbp)
	movq	$0, -6000(%rbp)
	movq	%rax, -6016(%rbp)
	movq	$0, -5992(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-5960(%rbp), %rsi
	xorl	%ecx, %ecx
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rdx, -5992(%rbp)
	movq	%rdx, -6000(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -7688(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -5976(%rbp)
	movq	%rax, -6008(%rbp)
	movq	$0, -5984(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7496(%rbp), %rax
	movl	$528, %edi
	movq	$0, -5816(%rbp)
	movq	$0, -5808(%rbp)
	movq	%rax, -5824(%rbp)
	movq	$0, -5800(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-5768(%rbp), %rsi
	leaq	528(%rax), %rdx
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	%rax, -5816(%rbp)
	movq	%rdx, -5800(%rbp)
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movups	%xmm0, 480(%rax)
	movq	%rdx, -5808(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -7704(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movq	$0, -5792(%rbp)
	movups	%xmm0, -5784(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7496(%rbp), %rax
	movl	$528, %edi
	movq	$0, -5624(%rbp)
	movq	$0, -5616(%rbp)
	movq	%rax, -5632(%rbp)
	movq	$0, -5608(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-5576(%rbp), %rsi
	leaq	528(%rax), %rdx
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	%rax, -5624(%rbp)
	movq	%rdx, -5608(%rbp)
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movups	%xmm0, 480(%rax)
	movq	%rdx, -5616(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -7728(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movq	$0, -5600(%rbp)
	movups	%xmm0, -5592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7496(%rbp), %rax
	movl	$576, %edi
	movq	$0, -5432(%rbp)
	movq	$0, -5424(%rbp)
	movq	%rax, -5440(%rbp)
	movq	$0, -5416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-5384(%rbp), %rsi
	leaq	576(%rax), %rdx
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	%rax, -5432(%rbp)
	movq	%rdx, -5416(%rbp)
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movups	%xmm0, 480(%rax)
	movq	%rdx, -5424(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -7736(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	movups	%xmm0, 544(%rax)
	movups	%xmm0, 560(%rax)
	movq	$0, -5408(%rbp)
	movups	%xmm0, -5400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7496(%rbp), %rax
	movl	$408, %edi
	movq	$0, -5240(%rbp)
	movq	$0, -5232(%rbp)
	movq	%rax, -5248(%rbp)
	movq	$0, -5224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-5192(%rbp), %rsi
	leaq	408(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 400(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movq	%rdx, -5224(%rbp)
	movq	%rdx, -5232(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -7744(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -5208(%rbp)
	movq	%rax, -5240(%rbp)
	movq	$0, -5216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7496(%rbp), %rax
	movl	$456, %edi
	movq	$0, -5048(%rbp)
	movq	$0, -5040(%rbp)
	movq	%rax, -5056(%rbp)
	movq	$0, -5032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-5000(%rbp), %rsi
	leaq	456(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movq	%rax, -5048(%rbp)
	movq	$0, 448(%rax)
	movq	%rdx, -5032(%rbp)
	movq	%rdx, -5040(%rbp)
	xorl	%edx, %edx
	movq	$0, -5024(%rbp)
	movq	%rsi, -7792(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -5016(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7496(%rbp), %rax
	movl	$456, %edi
	movq	$0, -4856(%rbp)
	movq	$0, -4848(%rbp)
	movq	%rax, -4864(%rbp)
	movq	$0, -4840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-4808(%rbp), %rsi
	leaq	456(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movq	%rax, -4856(%rbp)
	movq	$0, 448(%rax)
	movq	%rdx, -4840(%rbp)
	movq	%rdx, -4848(%rbp)
	xorl	%edx, %edx
	movq	$0, -4832(%rbp)
	movq	%rsi, -7808(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -4824(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7496(%rbp), %rax
	movl	$288, %edi
	movq	$0, -4664(%rbp)
	movq	$0, -4656(%rbp)
	movq	%rax, -4672(%rbp)
	movq	$0, -4648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-4616(%rbp), %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rdx, -4648(%rbp)
	movq	%rdx, -4656(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -7824(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -4632(%rbp)
	movq	%rax, -4664(%rbp)
	movq	$0, -4640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7496(%rbp), %rax
	movl	$288, %edi
	movq	$0, -4472(%rbp)
	movq	$0, -4464(%rbp)
	movq	%rax, -4480(%rbp)
	movq	$0, -4456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-4424(%rbp), %rsi
	xorl	%ecx, %ecx
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rdx, -4456(%rbp)
	movq	%rdx, -4464(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -7760(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -4440(%rbp)
	movq	%rax, -4472(%rbp)
	movq	$0, -4448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7496(%rbp), %rax
	movl	$264, %edi
	movq	$0, -4280(%rbp)
	movq	$0, -4272(%rbp)
	movq	%rax, -4288(%rbp)
	movq	$0, -4264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-4232(%rbp), %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rdx, -4264(%rbp)
	movq	%rdx, -4272(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -7776(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -4248(%rbp)
	movq	%rax, -4280(%rbp)
	movq	$0, -4256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7496(%rbp), %rax
	movl	$240, %edi
	movq	$0, -4088(%rbp)
	movq	$0, -4080(%rbp)
	movq	%rax, -4096(%rbp)
	movq	$0, -4072(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-4040(%rbp), %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rdx, -4072(%rbp)
	movq	%rdx, -4080(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -7832(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -4056(%rbp)
	movq	%rax, -4088(%rbp)
	movq	$0, -4064(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7496(%rbp), %rax
	movl	$240, %edi
	movq	$0, -3896(%rbp)
	movq	$0, -3888(%rbp)
	movq	%rax, -3904(%rbp)
	movq	$0, -3880(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-3848(%rbp), %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rdx, -3880(%rbp)
	movq	%rdx, -3888(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -7816(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -3864(%rbp)
	movq	%rax, -3896(%rbp)
	movq	$0, -3872(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7496(%rbp), %rax
	movl	$216, %edi
	movq	$0, -3704(%rbp)
	movq	$0, -3696(%rbp)
	movq	%rax, -3712(%rbp)
	movq	$0, -3688(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-3656(%rbp), %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -3688(%rbp)
	movq	%rdx, -3696(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -7840(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -3672(%rbp)
	movq	%rax, -3704(%rbp)
	movq	$0, -3680(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7496(%rbp), %rax
	movl	$216, %edi
	movq	$0, -3512(%rbp)
	movq	$0, -3504(%rbp)
	movq	%rax, -3520(%rbp)
	movq	$0, -3496(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-3464(%rbp), %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rsi, %rdi
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -3496(%rbp)
	movq	%rdx, -3504(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -7856(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -3480(%rbp)
	movq	%rax, -3512(%rbp)
	movq	$0, -3488(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-3328(%rbp), %rsi
	movl	$9, %edx
	movq	%rsi, %rdi
	movq	%rsi, -7640(%rbp)
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-7496(%rbp), %rax
	movl	$216, %edi
	movq	$0, -3128(%rbp)
	movq	$0, -3120(%rbp)
	movq	%rax, -3136(%rbp)
	movq	$0, -3112(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-3080(%rbp), %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -3112(%rbp)
	movq	%rdx, -3120(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -7888(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -3096(%rbp)
	movq	%rax, -3128(%rbp)
	movq	$0, -3104(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7496(%rbp), %rax
	movl	$216, %edi
	movq	$0, -2936(%rbp)
	movq	$0, -2928(%rbp)
	movq	%rax, -2944(%rbp)
	movq	$0, -2920(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-2888(%rbp), %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -2920(%rbp)
	movq	%rdx, -2928(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -7904(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -2904(%rbp)
	movq	%rax, -2936(%rbp)
	movq	$0, -2912(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7496(%rbp), %rax
	movl	$216, %edi
	movq	$0, -2744(%rbp)
	movq	$0, -2736(%rbp)
	movq	%rax, -2752(%rbp)
	movq	$0, -2728(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2696(%rbp), %rsi
	xorl	%ecx, %ecx
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -2728(%rbp)
	movq	%rdx, -2736(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -7920(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -2712(%rbp)
	movq	%rax, -2744(%rbp)
	movq	$0, -2720(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7496(%rbp), %rax
	movl	$216, %edi
	movq	$0, -2552(%rbp)
	movq	$0, -2544(%rbp)
	movq	%rax, -2560(%rbp)
	movq	$0, -2536(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-2504(%rbp), %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -2536(%rbp)
	movq	%rdx, -2544(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -7936(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -2520(%rbp)
	movq	%rax, -2552(%rbp)
	movq	$0, -2528(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7496(%rbp), %rax
	movl	$216, %edi
	movq	$0, -2360(%rbp)
	movq	$0, -2352(%rbp)
	movq	%rax, -2368(%rbp)
	movq	$0, -2344(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-2312(%rbp), %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -2344(%rbp)
	movq	%rdx, -2352(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -7952(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -2328(%rbp)
	movq	%rax, -2360(%rbp)
	movq	$0, -2336(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7496(%rbp), %rax
	movl	$288, %edi
	movq	$0, -2168(%rbp)
	movq	$0, -2160(%rbp)
	movq	%rax, -2176(%rbp)
	movq	$0, -2152(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-2120(%rbp), %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rdx, -2152(%rbp)
	movq	%rdx, -2160(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -7872(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -2136(%rbp)
	movq	%rax, -2168(%rbp)
	movq	$0, -2144(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7496(%rbp), %rax
	movl	$288, %edi
	movq	$0, -1976(%rbp)
	movq	$0, -1968(%rbp)
	movq	%rax, -1984(%rbp)
	movq	$0, -1960(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-1928(%rbp), %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rsi, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rdx, -1960(%rbp)
	movq	%rdx, -1968(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -7968(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -1944(%rbp)
	movq	%rax, -1976(%rbp)
	movq	$0, -1952(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-1792(%rbp), %rsi
	movl	$22, %edx
	movq	%rsi, %rdi
	movq	%rsi, -7656(%rbp)
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1600(%rbp), %rsi
	movl	$22, %edx
	movq	%rsi, %rdi
	movq	%rsi, -7664(%rbp)
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1408(%rbp), %rsi
	movl	$24, %edx
	movq	%rsi, %rdi
	movq	%rsi, -7696(%rbp)
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1216(%rbp), %rsi
	movl	$17, %edx
	movq	%rsi, %rdi
	movq	%rsi, -7712(%rbp)
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-7496(%rbp), %rax
	movl	$456, %edi
	movq	$0, -1016(%rbp)
	movq	$0, -1008(%rbp)
	movq	%rax, -1024(%rbp)
	movq	$0, -1000(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-968(%rbp), %rsi
	leaq	456(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movq	%rax, -1016(%rbp)
	movq	$0, 448(%rax)
	movq	%rdx, -1000(%rbp)
	movq	%rdx, -1008(%rbp)
	xorl	%edx, %edx
	movq	$0, -992(%rbp)
	movq	%rsi, -7976(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -984(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7496(%rbp), %rax
	movl	$456, %edi
	movq	$0, -824(%rbp)
	movq	$0, -816(%rbp)
	movq	%rax, -832(%rbp)
	movq	$0, -808(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-776(%rbp), %rsi
	leaq	456(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movq	%rax, -824(%rbp)
	movq	$0, 448(%rax)
	movq	%rdx, -808(%rbp)
	movq	%rdx, -816(%rbp)
	xorl	%edx, %edx
	movq	$0, -800(%rbp)
	movq	%rsi, -7984(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -792(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7496(%rbp), %rax
	movl	$288, %edi
	movq	$0, -632(%rbp)
	movq	$0, -624(%rbp)
	movq	%rax, -640(%rbp)
	movq	$0, -616(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-584(%rbp), %rsi
	xorl	%ecx, %ecx
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rsi, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rdx, -616(%rbp)
	movq	%rdx, -624(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -7992(%rbp)
	movq	%r14, %rsi
	movq	%rax, -632(%rbp)
	movups	%xmm0, -600(%rbp)
	movq	$0, -608(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-448(%rbp), %rsi
	movl	$12, %edx
	movq	%rsi, %rdi
	movq	%rsi, -7720(%rbp)
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	pxor	%xmm0, %xmm0
	movq	-7568(%rbp), %xmm1
	movl	$40, %edi
	movq	%r15, -224(%rbp)
	leaq	-7248(%rbp), %r15
	movhps	-7552(%rbp), %xmm1
	movaps	%xmm0, -7248(%rbp)
	movaps	%xmm1, -256(%rbp)
	movq	-7536(%rbp), %xmm1
	movq	$0, -7232(%rbp)
	movhps	-7520(%rbp), %xmm1
	movaps	%xmm1, -240(%rbp)
	call	_Znwm@PLT
	movq	-224(%rbp), %rcx
	movdqa	-256(%rbp), %xmm5
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm6
	leaq	40(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	leaq	-7168(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	movq	%rax, -7864(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1590
	call	_ZdlPv@PLT
.L1590:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-6976(%rbp), %rax
	cmpq	$0, -7104(%rbp)
	movq	%rax, -7584(%rbp)
	leaq	-6784(%rbp), %rax
	movq	%rax, -7592(%rbp)
	jne	.L1846
	cmpq	$0, -6912(%rbp)
	jne	.L1847
.L1595:
	leaq	-6592(%rbp), %rax
	cmpq	$0, -6720(%rbp)
	movq	%rax, -7520(%rbp)
	jne	.L1848
.L1597:
	leaq	-3904(%rbp), %rax
	cmpq	$0, -6528(%rbp)
	movq	%rax, -7600(%rbp)
	jne	.L1849
.L1600:
	leaq	-6208(%rbp), %rax
	cmpq	$0, -6336(%rbp)
	movq	%rax, -7632(%rbp)
	leaq	-6016(%rbp), %rax
	movq	%rax, -7616(%rbp)
	jne	.L1850
.L1603:
	leaq	-4288(%rbp), %rax
	cmpq	$0, -6144(%rbp)
	movq	%rax, -7536(%rbp)
	jne	.L1851
.L1606:
	leaq	-5824(%rbp), %rax
	cmpq	$0, -5952(%rbp)
	movq	%rax, -7648(%rbp)
	leaq	-5632(%rbp), %rax
	movq	%rax, -7680(%rbp)
	jne	.L1852
.L1608:
	leaq	-5440(%rbp), %rax
	cmpq	$0, -5760(%rbp)
	movq	%rax, -7688(%rbp)
	jne	.L1853
.L1611:
	leaq	-5248(%rbp), %rax
	cmpq	$0, -5568(%rbp)
	movq	%rax, -7704(%rbp)
	jne	.L1854
.L1613:
	leaq	-5056(%rbp), %rax
	cmpq	$0, -5376(%rbp)
	movq	%rax, -7728(%rbp)
	jne	.L1855
	cmpq	$0, -5184(%rbp)
	jne	.L1856
.L1618:
	leaq	-4864(%rbp), %rax
	cmpq	$0, -4992(%rbp)
	movq	%rax, -7736(%rbp)
	jne	.L1857
.L1620:
	leaq	-4672(%rbp), %rax
	cmpq	$0, -4800(%rbp)
	movq	%rax, -7744(%rbp)
	leaq	-4480(%rbp), %rax
	movq	%rax, -7552(%rbp)
	jne	.L1858
	cmpq	$0, -4608(%rbp)
	jne	.L1859
.L1625:
	cmpq	$0, -4416(%rbp)
	jne	.L1860
.L1628:
	leaq	-4096(%rbp), %rax
	cmpq	$0, -4224(%rbp)
	movq	%rax, -7760(%rbp)
	jne	.L1861
	cmpq	$0, -4032(%rbp)
	jne	.L1862
.L1633:
	leaq	-3712(%rbp), %rax
	cmpq	$0, -3840(%rbp)
	movq	%rax, -7768(%rbp)
	leaq	-3520(%rbp), %rax
	movq	%rax, -7776(%rbp)
	jne	.L1863
	cmpq	$0, -3648(%rbp)
	jne	.L1864
.L1638:
	leaq	-3136(%rbp), %rax
	cmpq	$0, -3456(%rbp)
	movq	%rax, -7792(%rbp)
	jne	.L1865
	cmpq	$0, -3264(%rbp)
	jne	.L1866
.L1642:
	leaq	-2944(%rbp), %rax
	cmpq	$0, -3072(%rbp)
	movq	%rax, -7808(%rbp)
	leaq	-2752(%rbp), %rax
	movq	%rax, -7816(%rbp)
	jne	.L1867
	cmpq	$0, -2880(%rbp)
	jne	.L1868
.L1646:
	leaq	-2560(%rbp), %rax
	cmpq	$0, -2688(%rbp)
	movq	%rax, -7832(%rbp)
	jne	.L1869
.L1647:
	leaq	-2368(%rbp), %rax
	cmpq	$0, -2496(%rbp)
	movq	%rax, -7840(%rbp)
	jne	.L1870
.L1649:
	leaq	-2176(%rbp), %rax
	cmpq	$0, -2304(%rbp)
	movq	%rax, -7568(%rbp)
	jne	.L1871
.L1651:
	leaq	-1984(%rbp), %rax
	cmpq	$0, -2112(%rbp)
	movq	%rax, -7824(%rbp)
	jne	.L1872
	cmpq	$0, -1920(%rbp)
	jne	.L1873
.L1656:
	cmpq	$0, -1728(%rbp)
	jne	.L1874
.L1659:
	cmpq	$0, -1536(%rbp)
	jne	.L1875
.L1661:
	leaq	-1024(%rbp), %rax
	cmpq	$0, -1344(%rbp)
	movq	%rax, -7856(%rbp)
	jne	.L1876
	cmpq	$0, -1152(%rbp)
	jne	.L1877
.L1666:
	cmpq	$0, -960(%rbp)
	leaq	-832(%rbp), %rbx
	jne	.L1878
.L1668:
	cmpq	$0, -768(%rbp)
	leaq	-640(%rbp), %r12
	jne	.L1879
	cmpq	$0, -576(%rbp)
	jne	.L1880
.L1672:
	cmpq	$0, -384(%rbp)
	jne	.L1881
.L1674:
	movq	-7720(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7856(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7712(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7696(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7664(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7656(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7824(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7568(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7840(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7832(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7816(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7808(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7792(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7640(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7776(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7768(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7600(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7760(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7536(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7552(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7744(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7736(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7728(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7704(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7688(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7680(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7648(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7616(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7632(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7576(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7520(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7592(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7584(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7864(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1882
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1846:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -7232(%rbp)
	movaps	%xmm0, -7248(%rbp)
	call	_Znwm@PLT
	movq	-7864(%rbp), %rdi
	movq	%r15, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -7248(%rbp)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7248(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L1592
	call	_ZdlPv@PLT
.L1592:
	movq	(%r12), %rax
	movq	%r14, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	movq	24(%rax), %rdx
	movq	%rcx, -7536(%rbp)
	movq	16(%rax), %rcx
	movq	32(%rax), %rax
	movq	%rsi, -7552(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rdx, -7568(%rbp)
	movl	$245, %edx
	movq	%rcx, -7520(%rbp)
	movq	%rax, -7584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$246, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7520(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-7520(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	-7536(%rbp), %xmm5
	movq	-7584(%rbp), %xmm7
	movl	$48, %edi
	movaps	%xmm0, -7248(%rbp)
	movq	%rax, %xmm3
	movq	%rax, %xmm4
	movhps	-7552(%rbp), %xmm5
	movq	$0, -7232(%rbp)
	punpcklqdq	%xmm3, %xmm7
	movhps	-7568(%rbp), %xmm4
	movaps	%xmm5, -7536(%rbp)
	movaps	%xmm7, -7520(%rbp)
	movaps	%xmm4, -7568(%rbp)
	movaps	%xmm5, -256(%rbp)
	movaps	%xmm4, -240(%rbp)
	movaps	%xmm7, -224(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm7
	movdqa	-240(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-224(%rbp), %xmm5
	leaq	48(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm5, 32(%rax)
	leaq	-6976(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	movq	%rax, -7584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1593
	call	_ZdlPv@PLT
.L1593:
	movdqa	-7536(%rbp), %xmm7
	movdqa	-7568(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movdqa	-7520(%rbp), %xmm5
	movaps	%xmm0, -7248(%rbp)
	movaps	%xmm7, -256(%rbp)
	movaps	%xmm6, -240(%rbp)
	movaps	%xmm5, -224(%rbp)
	movq	$0, -7232(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm7
	movdqa	-240(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-224(%rbp), %xmm5
	leaq	48(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm5, 32(%rax)
	leaq	-6784(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	movq	%rax, -7592(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1594
	call	_ZdlPv@PLT
.L1594:
	movq	-7616(%rbp), %rdx
	movq	%rbx, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -6912(%rbp)
	je	.L1595
.L1847:
	movq	-7616(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -7232(%rbp)
	movaps	%xmm0, -7248(%rbp)
	call	_Znwm@PLT
	movl	$1288, %esi
	movq	-7584(%rbp), %rdi
	movw	%si, 4(%rax)
	leaq	6(%rax), %rdx
	movq	%r15, %rsi
	movl	$117769477, (%rax)
	movq	%rax, -7248(%rbp)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1596
	call	_ZdlPv@PLT
.L1596:
	movl	$247, %edx
	movq	%r14, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	-7752(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	jmp	.L1595
	.p2align 4,,10
	.p2align 3
.L1848:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -7232(%rbp)
	movaps	%xmm0, -7248(%rbp)
	call	_Znwm@PLT
	movl	$1288, %ecx
	movq	-7592(%rbp), %rdi
	movq	%r15, %rsi
	movl	$117769477, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -7248(%rbp)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7248(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1598
	call	_ZdlPv@PLT
.L1598:
	movq	(%rbx), %rax
	movl	$249, %edx
	movq	%r14, %rdi
	movq	24(%rax), %rsi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	40(%rax), %r12
	movq	%rsi, -7568(%rbp)
	movq	32(%rax), %rsi
	movq	%rcx, -7536(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -7616(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rcx, -7552(%rbp)
	movq	%rbx, -7520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r9d, %r9d
	movl	$1, %ecx
	movq	%r12, %rdx
	movl	$4, %r8d
	movl	$4, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler18AllocateFixedArrayENS0_12ElementsKindEPNS0_8compiler4NodeENS1_13ParameterModeENS_4base5FlagsINS1_14AllocationFlagEiEENS3_11SloppyTNodeINS0_3MapEEE@PLT
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, -7600(%rbp)
	call	_ZN2v88internal17CodeStubAssembler28FillFixedDoubleArrayWithZeroENS0_8compiler5TNodeINS0_16FixedDoubleArrayEEENS3_INS0_7IntPtrTEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$250, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal42FromConstexpr6ATbool16ATconstexpr_bool_165EPNS0_8compiler18CodeAssemblerStateEb@PLT
	movl	$251, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -7632(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal46FromConstexpr9ATfloat6417ATconstexpr_int31_163EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$252, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r12, %xmm7
	movq	-7520(%rbp), %xmm0
	movq	-7600(%rbp), %rcx
	movl	$80, %edi
	movq	%rbx, -192(%rbp)
	movhps	-7536(%rbp), %xmm0
	movq	%rax, -184(%rbp)
	movaps	%xmm0, -256(%rbp)
	movq	-7552(%rbp), %xmm0
	movq	$0, -7232(%rbp)
	movhps	-7568(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-7616(%rbp), %xmm0
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	%rcx, %xmm0
	movhps	-7632(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7248(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm6
	movdqa	-240(%rbp), %xmm5
	movq	%r15, %rsi
	movdqa	-224(%rbp), %xmm7
	leaq	80(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-208(%rbp), %xmm6
	movups	%xmm5, 16(%rax)
	movdqa	-192(%rbp), %xmm5
	movups	%xmm7, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm5, 64(%rax)
	leaq	-6592(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	movq	%rax, -7520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1599
	call	_ZdlPv@PLT
.L1599:
	movq	-7768(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1597
	.p2align 4,,10
	.p2align 3
.L1849:
	movq	-7768(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	movq	$0, -7280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7280(%rbp), %rax
	movq	-7520(%rbp), %rdi
	pushq	%rax
	leaq	-7320(%rbp), %rax
	leaq	-7368(%rbp), %rcx
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7352(%rbp), %r9
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7360(%rbp), %r8
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7376(%rbp), %rdx
	pushq	%rax
	leaq	-7384(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESF_
	addq	$48, %rsp
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7344(%rbp), %rdx
	movq	-7280(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$80, %edi
	movq	-7320(%rbp), %xmm6
	movq	-7336(%rbp), %xmm3
	movaps	%xmm0, -7248(%rbp)
	movq	-7352(%rbp), %xmm7
	movq	-7368(%rbp), %xmm4
	movq	-7384(%rbp), %xmm5
	movhps	-7280(%rbp), %xmm6
	movq	$0, -7232(%rbp)
	movhps	-7328(%rbp), %xmm3
	movhps	-7344(%rbp), %xmm7
	movaps	%xmm6, -7536(%rbp)
	movhps	-7360(%rbp), %xmm4
	movhps	-7376(%rbp), %xmm5
	movaps	%xmm3, -7568(%rbp)
	movaps	%xmm7, -7552(%rbp)
	movaps	%xmm4, -7616(%rbp)
	movaps	%xmm5, -7632(%rbp)
	movaps	%xmm5, -256(%rbp)
	movaps	%xmm4, -240(%rbp)
	movaps	%xmm7, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm6, -192(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm7
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm4
	movdqa	-224(%rbp), %xmm6
	movdqa	-208(%rbp), %xmm5
	leaq	80(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-192(%rbp), %xmm7
	movq	-7576(%rbp), %rdi
	movups	%xmm4, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm7, 64(%rax)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1601
	call	_ZdlPv@PLT
.L1601:
	movdqa	-7632(%rbp), %xmm4
	movdqa	-7616(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$80, %edi
	movdqa	-7552(%rbp), %xmm5
	movdqa	-7568(%rbp), %xmm7
	movaps	%xmm0, -7248(%rbp)
	movaps	%xmm4, -256(%rbp)
	movdqa	-7536(%rbp), %xmm4
	movaps	%xmm6, -240(%rbp)
	movaps	%xmm5, -224(%rbp)
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm4, -192(%rbp)
	movq	$0, -7232(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm5
	movdqa	-224(%rbp), %xmm7
	movdqa	-208(%rbp), %xmm4
	leaq	80(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-192(%rbp), %xmm6
	movups	%xmm5, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm6, 64(%rax)
	leaq	-3904(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	movq	%rax, -7600(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1602
	call	_ZdlPv@PLT
.L1602:
	movq	-7816(%rbp), %rcx
	leaq	-6344(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1600
	.p2align 4,,10
	.p2align 3
.L1850:
	leaq	-6344(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7320(%rbp), %rax
	movq	-7576(%rbp), %rdi
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7376(%rbp), %rcx
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7360(%rbp), %r9
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7368(%rbp), %r8
	pushq	%rax
	leaq	-7352(%rbp), %rax
	leaq	-7384(%rbp), %rdx
	pushq	%rax
	leaq	-7392(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESF_
	addq	$48, %rsp
	movl	$253, %edx
	movq	%r14, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7384(%rbp), %rdx
	subq	$8, %rsp
	movq	-7392(%rbp), %rax
	movq	-7320(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rdx, -7272(%rbp)
	movq	-7376(%rbp), %rdx
	movq	%rax, -7280(%rbp)
	movq	%rdx, -7264(%rbp)
	pushq	-7264(%rbp)
	pushq	-7272(%rbp)
	pushq	%rax
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7368(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ToNumber_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal43Convert9ATfloat6420UT5ATSmi10HeapNumber_194EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movl	$254, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal15Float64IsNaN_79EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8Float64TEEE@PLT
	pxor	%xmm0, %xmm0
	movq	-7328(%rbp), %xmm6
	movq	-7344(%rbp), %xmm3
	movl	$88, %edi
	movaps	%xmm0, -7248(%rbp)
	movq	%rax, %r12
	movq	-7360(%rbp), %xmm7
	movq	-7376(%rbp), %xmm2
	movhps	-7320(%rbp), %xmm6
	movq	-7392(%rbp), %xmm4
	movhps	-7336(%rbp), %xmm3
	movhps	-7352(%rbp), %xmm7
	movaps	%xmm6, -7680(%rbp)
	movhps	-7368(%rbp), %xmm2
	movhps	-7384(%rbp), %xmm4
	movaps	%xmm3, -7552(%rbp)
	movaps	%xmm7, -7616(%rbp)
	movaps	%xmm2, -7568(%rbp)
	movaps	%xmm4, -7536(%rbp)
	movaps	%xmm4, -256(%rbp)
	movaps	%xmm2, -240(%rbp)
	movaps	%xmm7, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm6, -192(%rbp)
	movq	%rbx, -176(%rbp)
	movq	$0, -7232(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm5
	movq	%r15, %rsi
	movq	-176(%rbp), %rcx
	movdqa	-240(%rbp), %xmm7
	movdqa	-224(%rbp), %xmm4
	leaq	88(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-208(%rbp), %xmm6
	movups	%xmm5, (%rax)
	movdqa	-192(%rbp), %xmm5
	movq	%rcx, 80(%rax)
	movq	-7632(%rbp), %rdi
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1604
	call	_ZdlPv@PLT
.L1604:
	movdqa	-7536(%rbp), %xmm7
	movdqa	-7568(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movl	$88, %edi
	movdqa	-7616(%rbp), %xmm6
	movdqa	-7552(%rbp), %xmm5
	movaps	%xmm0, -7248(%rbp)
	movdqa	-7680(%rbp), %xmm3
	movaps	%xmm7, -256(%rbp)
	movaps	%xmm4, -240(%rbp)
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm3, -192(%rbp)
	movq	%rbx, -176(%rbp)
	movq	$0, -7232(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-256(%rbp), %xmm7
	movdqa	-240(%rbp), %xmm4
	movdqa	-224(%rbp), %xmm6
	leaq	88(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-208(%rbp), %xmm5
	movdqa	-192(%rbp), %xmm3
	movq	%rcx, 80(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm3, 64(%rax)
	leaq	-6016(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	movq	%rax, -7616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1605
	call	_ZdlPv@PLT
.L1605:
	movq	-7688(%rbp), %rcx
	movq	-7648(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L1852:
	movq	-7688(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7320(%rbp), %rax
	movq	-7616(%rbp), %rdi
	leaq	-7368(%rbp), %r9
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7376(%rbp), %r8
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7384(%rbp), %rcx
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7392(%rbp), %rdx
	pushq	%rax
	leaq	-7352(%rbp), %rax
	leaq	-7400(%rbp), %rsi
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESF_SP_
	addq	$48, %rsp
	movl	$257, %edx
	movq	%r14, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7320(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler10Float64AbsENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%r15, %rdi
	movq	%rax, -7568(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$258, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$8, %esi
	movq	%r14, %rdi
	movq	%rax, -7680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	movq	-7352(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$46, %edx
	leaq	.LC29(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -7552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$37, %edx
	leaq	.LC29(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7328(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	-7552(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-7568(%rbp), %rax
	movl	$176, %edi
	movq	-7384(%rbp), %xmm7
	movq	-7352(%rbp), %xmm1
	movq	-7320(%rbp), %rbx
	movhps	-7376(%rbp), %xmm7
	movq	%rax, -168(%rbp)
	movq	-7328(%rbp), %xmm0
	movaps	%xmm7, -8112(%rbp)
	movdqa	%xmm1, %xmm6
	movq	-7336(%rbp), %xmm5
	movq	-7368(%rbp), %xmm3
	movq	-7400(%rbp), %xmm2
	movaps	%xmm7, -240(%rbp)
	movq	-7680(%rbp), %xmm7
	movdqa	%xmm1, %xmm4
	movq	-7552(%rbp), %rax
	punpcklqdq	%xmm0, %xmm5
	movhps	-7344(%rbp), %xmm6
	movhps	-7360(%rbp), %xmm3
	punpcklqdq	%xmm7, %xmm4
	movhps	-7392(%rbp), %xmm2
	movq	%xmm0, -136(%rbp)
	movq	%xmm0, -128(%rbp)
	movq	%xmm1, -120(%rbp)
	movaps	%xmm5, -8048(%rbp)
	movaps	%xmm6, -8064(%rbp)
	movaps	%xmm3, -8032(%rbp)
	movaps	%xmm3, -224(%rbp)
	movaps	%xmm6, -208(%rbp)
	movaps	%xmm5, -192(%rbp)
	movaps	%xmm4, -8080(%rbp)
	movaps	%xmm4, -160(%rbp)
	movq	%rax, -144(%rbp)
	movaps	%xmm2, -8096(%rbp)
	movaps	%xmm2, -256(%rbp)
	pxor	%xmm2, %xmm2
	movq	%rbx, -176(%rbp)
	movq	%xmm1, -8016(%rbp)
	movq	-7552(%rbp), %rax
	movq	%xmm0, -96(%rbp)
	movq	%xmm0, -88(%rbp)
	movq	%xmm0, -7688(%rbp)
	movq	%xmm7, -112(%rbp)
	movaps	%xmm2, -7248(%rbp)
	movq	%rax, -104(%rbp)
	movq	$0, -7232(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm7
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm4
	movdqa	-224(%rbp), %xmm6
	movdqa	-208(%rbp), %xmm5
	leaq	176(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-192(%rbp), %xmm3
	movups	%xmm7, (%rax)
	movdqa	-176(%rbp), %xmm7
	movups	%xmm4, 16(%rax)
	movdqa	-160(%rbp), %xmm4
	movq	-7648(%rbp), %rdi
	movups	%xmm6, 32(%rax)
	movdqa	-144(%rbp), %xmm6
	movups	%xmm5, 48(%rax)
	movdqa	-128(%rbp), %xmm5
	movups	%xmm3, 64(%rax)
	movdqa	-112(%rbp), %xmm3
	movups	%xmm7, 80(%rax)
	movdqa	-96(%rbp), %xmm7
	movups	%xmm4, 96(%rax)
	movups	%xmm6, 112(%rax)
	movups	%xmm5, 128(%rax)
	movups	%xmm3, 144(%rax)
	movups	%xmm7, 160(%rax)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	movq	-7688(%rbp), %xmm0
	movq	-8016(%rbp), %xmm1
	testq	%rdi, %rdi
	je	.L1609
	call	_ZdlPv@PLT
	movq	-8016(%rbp), %xmm1
	movq	-7688(%rbp), %xmm0
.L1609:
	movdqa	-8112(%rbp), %xmm6
	movdqa	-8032(%rbp), %xmm5
	movq	%rbx, %xmm2
	movl	$176, %edi
	movdqa	-8096(%rbp), %xmm4
	movhps	-7568(%rbp), %xmm2
	movdqa	-8064(%rbp), %xmm3
	movq	$0, -7232(%rbp)
	movaps	%xmm6, -240(%rbp)
	movdqa	-8048(%rbp), %xmm7
	movq	-7552(%rbp), %xmm6
	movaps	%xmm5, -224(%rbp)
	movdqa	%xmm0, %xmm5
	punpcklqdq	%xmm1, %xmm5
	movaps	%xmm2, -176(%rbp)
	movq	-7680(%rbp), %xmm1
	movdqa	%xmm6, %xmm2
	punpcklqdq	%xmm0, %xmm2
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm4, -256(%rbp)
	movdqa	-8080(%rbp), %xmm4
	punpcklqdq	%xmm6, %xmm1
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -7248(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm5
	movdqa	-224(%rbp), %xmm3
	movdqa	-208(%rbp), %xmm7
	leaq	176(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-192(%rbp), %xmm4
	movups	%xmm6, (%rax)
	movdqa	-176(%rbp), %xmm6
	movups	%xmm5, 16(%rax)
	movdqa	-160(%rbp), %xmm5
	movups	%xmm3, 32(%rax)
	movdqa	-144(%rbp), %xmm3
	movups	%xmm7, 48(%rax)
	movdqa	-128(%rbp), %xmm7
	movups	%xmm4, 64(%rax)
	movdqa	-112(%rbp), %xmm4
	movups	%xmm6, 80(%rax)
	movdqa	-96(%rbp), %xmm6
	movups	%xmm5, 96(%rax)
	movups	%xmm3, 112(%rax)
	movups	%xmm7, 128(%rax)
	movups	%xmm4, 144(%rax)
	movups	%xmm6, 160(%rax)
	leaq	-5632(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	movq	%rax, -7680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1610
	call	_ZdlPv@PLT
.L1610:
	movq	-7728(%rbp), %rcx
	movq	-7704(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1608
	.p2align 4,,10
	.p2align 3
.L1851:
	movq	-7648(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7320(%rbp), %rax
	movq	-7632(%rbp), %rdi
	leaq	-7384(%rbp), %rcx
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7368(%rbp), %r9
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7376(%rbp), %r8
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7392(%rbp), %rdx
	pushq	%rax
	leaq	-7352(%rbp), %rax
	leaq	-7400(%rbp), %rsi
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESF_SP_
	addq	$48, %rsp
	movl	$255, %edx
	movq	%r14, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal42FromConstexpr6ATbool16ATconstexpr_bool_165EPNS0_8compiler18CodeAssemblerStateEb@PLT
	movl	$254, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7352(%rbp), %rax
	movq	-7368(%rbp), %xmm0
	movl	$88, %edi
	movq	-7384(%rbp), %xmm1
	movq	-7400(%rbp), %xmm2
	movq	%rbx, -200(%rbp)
	movq	%rax, -208(%rbp)
	movq	-7336(%rbp), %rax
	movhps	-7360(%rbp), %xmm0
	movhps	-7376(%rbp), %xmm1
	movhps	-7392(%rbp), %xmm2
	movaps	%xmm0, -224(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -192(%rbp)
	movq	-7328(%rbp), %rax
	movaps	%xmm2, -256(%rbp)
	movq	%rax, -184(%rbp)
	movq	-7320(%rbp), %rax
	movaps	%xmm1, -240(%rbp)
	movaps	%xmm0, -7248(%rbp)
	movq	%rax, -176(%rbp)
	movq	$0, -7232(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-256(%rbp), %xmm7
	movdqa	-240(%rbp), %xmm4
	movdqa	-224(%rbp), %xmm6
	leaq	88(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-208(%rbp), %xmm5
	movdqa	-192(%rbp), %xmm3
	movq	%rcx, 80(%rax)
	movq	-7536(%rbp), %rdi
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm3, 64(%rax)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1607
	call	_ZdlPv@PLT
.L1607:
	movq	-7776(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1606
	.p2align 4,,10
	.p2align 3
.L1853:
	movq	-7704(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7488(%rbp)
	movq	$0, -7480(%rbp)
	movq	$0, -7472(%rbp)
	movq	$0, -7464(%rbp)
	movq	$0, -7456(%rbp)
	movq	$0, -7448(%rbp)
	movq	$0, -7440(%rbp)
	movq	$0, -7432(%rbp)
	movq	$0, -7424(%rbp)
	movq	$0, -7416(%rbp)
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7320(%rbp), %rax
	movq	-7648(%rbp), %rdi
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7456(%rbp), %r9
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7464(%rbp), %r8
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7472(%rbp), %rcx
	pushq	%rax
	leaq	-7352(%rbp), %rax
	leaq	-7480(%rbp), %rdx
	pushq	%rax
	leaq	-7360(%rbp), %rax
	leaq	-7488(%rbp), %rsi
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	leaq	-7376(%rbp), %rax
	pushq	%rax
	leaq	-7384(%rbp), %rax
	pushq	%rax
	leaq	-7392(%rbp), %rax
	pushq	%rax
	leaq	-7400(%rbp), %rax
	pushq	%rax
	leaq	-7408(%rbp), %rax
	pushq	%rax
	leaq	-7416(%rbp), %rax
	pushq	%rax
	leaq	-7424(%rbp), %rax
	pushq	%rax
	leaq	-7432(%rbp), %rax
	pushq	%rax
	leaq	-7440(%rbp), %rax
	pushq	%rax
	leaq	-7448(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_S9_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESG_SQ_SQ_SM_SG_SG_SG_SG_PNSC_ISA_EESG_SG_SG_SG_
	addq	$144, %rsp
	movl	$39, %edx
	movq	%r14, %rdi
	leaq	.LC29(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal20SizeOf9ATfloat64_340EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7320(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrMulENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7344(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$38, %edx
	leaq	.LC29(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	-7352(%rbp), %rdx
	call	_ZN2v88internal33UnsafeNewReference9ATfloat64_1426EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movl	$192, %edi
	movq	-7328(%rbp), %xmm0
	movq	-7344(%rbp), %xmm1
	movq	-7360(%rbp), %xmm2
	movq	-7376(%rbp), %xmm3
	movq	-7392(%rbp), %xmm4
	movhps	-7336(%rbp), %xmm1
	movq	-7408(%rbp), %xmm5
	movhps	-7320(%rbp), %xmm0
	movq	-7424(%rbp), %xmm6
	movhps	-7352(%rbp), %xmm2
	movq	-7440(%rbp), %xmm7
	movhps	-7368(%rbp), %xmm3
	movq	-7456(%rbp), %xmm8
	movhps	-7384(%rbp), %xmm4
	movaps	%xmm2, -128(%rbp)
	movq	-7472(%rbp), %xmm9
	movq	-7488(%rbp), %xmm10
	movhps	-7400(%rbp), %xmm5
	movhps	-7416(%rbp), %xmm6
	movhps	-7432(%rbp), %xmm7
	movhps	-7448(%rbp), %xmm8
	movhps	-7464(%rbp), %xmm9
	movaps	%xmm7, -208(%rbp)
	movhps	-7480(%rbp), %xmm10
	movaps	%xmm9, -240(%rbp)
	movaps	%xmm10, -256(%rbp)
	movaps	%xmm8, -224(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	-7248(%rbp), %xmm0
	movq	$0, -7232(%rbp)
	movhps	-7240(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7248(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm5
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm3
	movdqa	-224(%rbp), %xmm7
	movdqa	-208(%rbp), %xmm4
	leaq	192(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-192(%rbp), %xmm6
	movups	%xmm5, (%rax)
	movdqa	-176(%rbp), %xmm5
	movups	%xmm3, 16(%rax)
	movdqa	-160(%rbp), %xmm3
	movq	-7688(%rbp), %rdi
	movups	%xmm7, 32(%rax)
	movdqa	-144(%rbp), %xmm7
	movups	%xmm4, 48(%rax)
	movdqa	-128(%rbp), %xmm4
	movups	%xmm6, 64(%rax)
	movdqa	-112(%rbp), %xmm6
	movups	%xmm5, 80(%rax)
	movdqa	-96(%rbp), %xmm5
	movups	%xmm3, 96(%rax)
	movdqa	-80(%rbp), %xmm3
	movups	%xmm7, 112(%rax)
	movups	%xmm4, 128(%rax)
	movups	%xmm6, 144(%rax)
	movups	%xmm5, 160(%rax)
	movups	%xmm3, 176(%rax)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1612
	call	_ZdlPv@PLT
.L1612:
	movq	-7736(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1611
	.p2align 4,,10
	.p2align 3
.L1854:
	movq	-7728(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7488(%rbp)
	movq	$0, -7480(%rbp)
	movq	$0, -7472(%rbp)
	movq	$0, -7464(%rbp)
	movq	$0, -7456(%rbp)
	movq	$0, -7448(%rbp)
	movq	$0, -7440(%rbp)
	movq	$0, -7432(%rbp)
	movq	$0, -7424(%rbp)
	movq	$0, -7416(%rbp)
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7320(%rbp), %rax
	movq	-7680(%rbp), %rdi
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7472(%rbp), %rcx
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7456(%rbp), %r9
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7464(%rbp), %r8
	pushq	%rax
	leaq	-7352(%rbp), %rax
	leaq	-7480(%rbp), %rdx
	pushq	%rax
	leaq	-7360(%rbp), %rax
	leaq	-7488(%rbp), %rsi
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	leaq	-7376(%rbp), %rax
	pushq	%rax
	leaq	-7384(%rbp), %rax
	pushq	%rax
	leaq	-7392(%rbp), %rax
	pushq	%rax
	leaq	-7400(%rbp), %rax
	pushq	%rax
	leaq	-7408(%rbp), %rax
	pushq	%rax
	leaq	-7416(%rbp), %rax
	pushq	%rax
	leaq	-7424(%rbp), %rax
	pushq	%rax
	leaq	-7432(%rbp), %rax
	pushq	%rax
	leaq	-7440(%rbp), %rax
	pushq	%rax
	leaq	-7448(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_S9_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESG_SQ_SQ_SM_SG_SG_SG_SG_PNSC_ISA_EESG_SG_SG_SG_
	addq	$144, %rsp
	movl	$41, %edx
	movq	%r14, %rdi
	leaq	.LC29(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7488(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$136, %edi
	movq	%rax, -256(%rbp)
	movq	-7480(%rbp), %rax
	movq	%rax, -248(%rbp)
	movq	-7472(%rbp), %rax
	movq	%rax, -240(%rbp)
	movq	-7464(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	-7456(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-7448(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-7440(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-7432(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-7424(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-7416(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-7408(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-7400(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-7392(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-7384(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-7376(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-7368(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-7360(%rbp), %rax
	movaps	%xmm0, -7248(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -7232(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm7
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm4
	movq	-128(%rbp), %rcx
	movdqa	-224(%rbp), %xmm6
	leaq	136(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-208(%rbp), %xmm5
	movdqa	-192(%rbp), %xmm2
	movups	%xmm7, (%rax)
	movdqa	-176(%rbp), %xmm3
	movdqa	-160(%rbp), %xmm7
	movups	%xmm4, 16(%rax)
	movq	%rcx, 128(%rax)
	movdqa	-144(%rbp), %xmm4
	movq	-7704(%rbp), %rdi
	movups	%xmm6, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm2, 64(%rax)
	movups	%xmm3, 80(%rax)
	movups	%xmm7, 96(%rax)
	movups	%xmm4, 112(%rax)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1614
	call	_ZdlPv@PLT
.L1614:
	movq	-7744(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1613
	.p2align 4,,10
	.p2align 3
.L1855:
	movq	-7736(%rbp), %rsi
	movq	%r14, %rdi
	movabsq	$362263814143805189, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	$0, -7232(%rbp)
	movaps	%xmm0, -7248(%rbp)
	call	_Znwm@PLT
	movdqa	.LC27(%rip), %xmm0
	movq	-7688(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, 16(%rax)
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -7248(%rbp)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7248(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1616
	call	_ZdlPv@PLT
.L1616:
	movq	(%rbx), %rax
	movq	40(%rax), %rdx
	movq	88(%rax), %r10
	movq	8(%rax), %rcx
	movq	24(%rax), %rsi
	movq	72(%rax), %rdi
	movq	(%rax), %rbx
	movq	%rdx, -8032(%rbp)
	movq	56(%rax), %rdx
	movq	96(%rax), %r11
	movq	%r10, -8112(%rbp)
	movq	104(%rax), %r10
	movq	120(%rax), %r9
	movq	%rcx, -7568(%rbp)
	movq	%rsi, -7736(%rbp)
	movq	16(%rax), %rcx
	movq	32(%rax), %rsi
	movq	%rdx, -8048(%rbp)
	movq	%rdi, -8080(%rbp)
	movq	64(%rax), %rdx
	movq	80(%rax), %rdi
	movq	%r10, -8120(%rbp)
	movq	112(%rax), %r10
	movq	48(%rax), %r12
	movq	%rcx, -7728(%rbp)
	movq	%r11, -8000(%rbp)
	movq	%r10, -8128(%rbp)
	movq	%rbx, -7552(%rbp)
	movq	%rsi, -8016(%rbp)
	leaq	.LC29(%rip), %rsi
	movq	%rdx, -8064(%rbp)
	movl	$46, %edx
	movq	%rdi, -8096(%rbp)
	movq	%r14, %rdi
	movq	%r9, -8136(%rbp)
	movq	128(%rax), %r9
	movq	176(%rax), %r8
	movq	184(%rax), %rbx
	movq	%r9, -8144(%rbp)
	movq	%r8, -8152(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$152, %edi
	movq	%rbx, -112(%rbp)
	movq	-7552(%rbp), %xmm0
	movq	$0, -7232(%rbp)
	movhps	-7568(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	-7728(%rbp), %xmm0
	movhps	-7736(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-8016(%rbp), %xmm0
	movhps	-8032(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	%r12, %xmm0
	movhps	-8048(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-8064(%rbp), %xmm0
	movhps	-8080(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-8096(%rbp), %xmm0
	movhps	-8112(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-8000(%rbp), %xmm0
	movhps	-8120(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-8128(%rbp), %xmm0
	movhps	-8136(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-8144(%rbp), %xmm0
	movhps	-8152(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7248(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm5
	movq	-112(%rbp), %rcx
	movdqa	-224(%rbp), %xmm1
	leaq	152(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-208(%rbp), %xmm2
	movdqa	-192(%rbp), %xmm3
	movups	%xmm5, 16(%rax)
	movdqa	-176(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm5
	movq	%rcx, 144(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm3, 64(%rax)
	movups	%xmm7, 80(%rax)
	movups	%xmm4, 96(%rax)
	movups	%xmm6, 112(%rax)
	movups	%xmm5, 128(%rax)
	leaq	-5056(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	movq	%rax, -7728(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1617
	call	_ZdlPv@PLT
.L1617:
	movq	-7792(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -5184(%rbp)
	je	.L1618
.L1856:
	movq	-7744(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$17, %edi
	movq	$0, -7232(%rbp)
	movaps	%xmm0, -7248(%rbp)
	call	_Znwm@PLT
	movdqa	.LC27(%rip), %xmm0
	movq	-7704(%rbp), %rdi
	movq	%r15, %rsi
	movb	$5, 16(%rax)
	leaq	17(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -7248(%rbp)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1619
	call	_ZdlPv@PLT
.L1619:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L1618
	.p2align 4,,10
	.p2align 3
.L1858:
	movq	-7808(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7464(%rbp)
	movq	$0, -7456(%rbp)
	movq	$0, -7448(%rbp)
	movq	$0, -7440(%rbp)
	movq	$0, -7432(%rbp)
	movq	$0, -7424(%rbp)
	movq	$0, -7416(%rbp)
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7320(%rbp), %rax
	movq	-7736(%rbp), %rdi
	leaq	-7448(%rbp), %rcx
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7432(%rbp), %r9
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7440(%rbp), %r8
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7456(%rbp), %rdx
	pushq	%rax
	leaq	-7352(%rbp), %rax
	leaq	-7464(%rbp), %rsi
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	leaq	-7376(%rbp), %rax
	pushq	%rax
	leaq	-7384(%rbp), %rax
	pushq	%rax
	leaq	-7392(%rbp), %rax
	pushq	%rax
	leaq	-7400(%rbp), %rax
	pushq	%rax
	leaq	-7408(%rbp), %rax
	pushq	%rax
	leaq	-7416(%rbp), %rax
	pushq	%rax
	leaq	-7424(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_S9_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESG_SQ_SQ_SM_SG_SG_SG_SG_PNSC_ISA_EESG_
	addq	$112, %rsp
	movl	$258, %edx
	movq	%r14, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7376(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17Float64SilenceNaNENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7320(%rbp), %r8
	movq	%r15, %rdi
	movl	$1, %esi
	movq	-7328(%rbp), %r12
	movq	%r8, -7552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-7552(%rbp), %r8
	movq	%r15, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	%r12, %rdx
	movq	%rax, %rcx
	movl	$13, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13StoreToObjectENS0_21MachineRepresentationENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEEPNS1_4NodeENS0_25StoreToObjectWriteBarrierE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$259, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7400(%rbp), %rdx
	movq	-7376(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler18Float64GreaterThanENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$96, %edi
	movq	-7384(%rbp), %xmm5
	movq	-7400(%rbp), %xmm6
	movaps	%xmm0, -7248(%rbp)
	movq	-7416(%rbp), %xmm3
	movq	-7432(%rbp), %xmm7
	movhps	-7376(%rbp), %xmm5
	movq	-7448(%rbp), %xmm2
	movq	$0, -7232(%rbp)
	movq	-7464(%rbp), %xmm4
	movhps	-7392(%rbp), %xmm6
	movhps	-7408(%rbp), %xmm3
	movaps	%xmm5, -8032(%rbp)
	movhps	-7424(%rbp), %xmm7
	movhps	-7440(%rbp), %xmm2
	movaps	%xmm6, -8016(%rbp)
	movhps	-7456(%rbp), %xmm4
	movaps	%xmm3, -7808(%rbp)
	movaps	%xmm7, -7792(%rbp)
	movaps	%xmm2, -7568(%rbp)
	movaps	%xmm4, -7552(%rbp)
	movaps	%xmm4, -256(%rbp)
	movaps	%xmm2, -240(%rbp)
	movaps	%xmm7, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm3
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm7
	movdqa	-224(%rbp), %xmm4
	movdqa	-208(%rbp), %xmm6
	leaq	96(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-192(%rbp), %xmm5
	movdqa	-176(%rbp), %xmm1
	movups	%xmm3, (%rax)
	movq	-7744(%rbp), %rdi
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm5, 64(%rax)
	movups	%xmm1, 80(%rax)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1623
	call	_ZdlPv@PLT
.L1623:
	movdqa	-7552(%rbp), %xmm2
	movdqa	-7568(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movl	$96, %edi
	movdqa	-7792(%rbp), %xmm7
	movdqa	-7808(%rbp), %xmm4
	movaps	%xmm0, -7248(%rbp)
	movdqa	-8016(%rbp), %xmm6
	movdqa	-8032(%rbp), %xmm5
	movaps	%xmm2, -256(%rbp)
	movaps	%xmm3, -240(%rbp)
	movaps	%xmm7, -224(%rbp)
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movq	$0, -7232(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm1
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm2
	movdqa	-224(%rbp), %xmm3
	movdqa	-208(%rbp), %xmm7
	leaq	96(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-192(%rbp), %xmm4
	movdqa	-176(%rbp), %xmm6
	movups	%xmm1, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm6, 80(%rax)
	leaq	-4480(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	movq	%rax, -7552(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1624
	call	_ZdlPv@PLT
.L1624:
	movq	-7760(%rbp), %rcx
	movq	-7824(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -4608(%rbp)
	je	.L1625
.L1859:
	movq	-7824(%rbp), %rsi
	movq	%r14, %rdi
	movabsq	$290206233024333061, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$12, %edi
	movq	$0, -7232(%rbp)
	movaps	%xmm0, -7248(%rbp)
	call	_Znwm@PLT
	movq	-7744(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, (%rax)
	leaq	12(%rax), %rdx
	movl	$218957069, 8(%rax)
	movq	%rax, -7248(%rbp)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7248(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1626
	call	_ZdlPv@PLT
.L1626:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	56(%rax), %rdx
	movq	40(%rax), %rsi
	movq	8(%rax), %rbx
	movq	24(%rax), %rcx
	movq	%rdx, -8048(%rbp)
	movq	72(%rax), %rdx
	movq	%rsi, -8016(%rbp)
	movq	48(%rax), %rsi
	movq	%rdx, -8064(%rbp)
	movq	80(%rax), %rdx
	movq	(%rax), %r12
	movq	%rbx, -7792(%rbp)
	movq	%rcx, -7808(%rbp)
	movq	16(%rax), %rbx
	movq	32(%rax), %rcx
	movq	88(%rax), %rax
	movq	%rsi, -8032(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rdx, -8080(%rbp)
	movl	$260, %edx
	movq	%rcx, -7824(%rbp)
	movq	%rax, -7568(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$259, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %xmm0
	movl	$96, %edi
	movq	$0, -7232(%rbp)
	movhps	-7792(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	%rbx, %xmm0
	movhps	-7808(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-7824(%rbp), %xmm0
	movhps	-8016(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-8032(%rbp), %xmm0
	movhps	-8048(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-7568(%rbp), %xmm0
	movhps	-8064(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-8080(%rbp), %xmm0
	movhps	-7568(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7248(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm5
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm1
	movdqa	-224(%rbp), %xmm2
	movdqa	-208(%rbp), %xmm3
	leaq	96(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-192(%rbp), %xmm7
	movdqa	-176(%rbp), %xmm4
	movups	%xmm5, (%rax)
	movq	-7552(%rbp), %rdi
	movups	%xmm1, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm4, 80(%rax)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1627
	call	_ZdlPv@PLT
.L1627:
	movq	-7760(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -4416(%rbp)
	je	.L1628
.L1860:
	movq	-7760(%rbp), %rsi
	movq	%r14, %rdi
	movabsq	$290206233024333061, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$12, %edi
	movq	$0, -7232(%rbp)
	movaps	%xmm0, -7248(%rbp)
	call	_Znwm@PLT
	movq	-7552(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, (%rax)
	leaq	12(%rax), %rdx
	movl	$218957069, 8(%rax)
	movq	%rax, -7248(%rbp)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7248(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1629
	call	_ZdlPv@PLT
.L1629:
	movq	(%rbx), %rax
	movq	40(%rax), %rdx
	movq	24(%rax), %rsi
	movq	8(%rax), %rcx
	movq	72(%rax), %rdi
	movq	(%rax), %rbx
	movq	48(%rax), %r12
	movq	%rdx, -8016(%rbp)
	movq	56(%rax), %rdx
	movq	%rsi, -7808(%rbp)
	movq	32(%rax), %rsi
	movq	%rcx, -7760(%rbp)
	movq	%rdx, -8032(%rbp)
	movq	64(%rax), %rdx
	movq	16(%rax), %rcx
	movq	%rsi, -7824(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rdx, -8048(%rbp)
	movl	$256, %edx
	movq	%rdi, -8064(%rbp)
	movq	%r14, %rdi
	movq	%rcx, -7792(%rbp)
	movq	%rbx, -7568(%rbp)
	movq	80(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$254, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$88, %edi
	movq	-7568(%rbp), %xmm0
	movq	%rbx, -176(%rbp)
	movq	$0, -7232(%rbp)
	movhps	-7760(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	-7792(%rbp), %xmm0
	movhps	-7808(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-7824(%rbp), %xmm0
	movhps	-8016(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	%r12, %xmm0
	movhps	-8032(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-8048(%rbp), %xmm0
	movhps	-8064(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7248(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-256(%rbp), %xmm6
	movdqa	-240(%rbp), %xmm5
	movdqa	-224(%rbp), %xmm1
	leaq	88(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-208(%rbp), %xmm2
	movdqa	-192(%rbp), %xmm3
	movq	%rcx, 80(%rax)
	movq	-7536(%rbp), %rdi
	movups	%xmm6, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm3, 64(%rax)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1630
	call	_ZdlPv@PLT
.L1630:
	movq	-7776(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1628
	.p2align 4,,10
	.p2align 3
.L1857:
	movq	-7792(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7464(%rbp)
	movq	$0, -7456(%rbp)
	movq	$0, -7448(%rbp)
	movq	$0, -7440(%rbp)
	movq	$0, -7432(%rbp)
	movq	$0, -7424(%rbp)
	movq	$0, -7416(%rbp)
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7320(%rbp), %rax
	movq	-7728(%rbp), %rdi
	leaq	-7448(%rbp), %rcx
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7456(%rbp), %rdx
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7464(%rbp), %rsi
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7432(%rbp), %r9
	pushq	%rax
	leaq	-7352(%rbp), %rax
	leaq	-7440(%rbp), %r8
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	leaq	-7376(%rbp), %rax
	pushq	%rax
	leaq	-7384(%rbp), %rax
	pushq	%rax
	leaq	-7392(%rbp), %rax
	pushq	%rax
	leaq	-7400(%rbp), %rax
	pushq	%rax
	leaq	-7408(%rbp), %rax
	pushq	%rax
	leaq	-7416(%rbp), %rax
	pushq	%rax
	leaq	-7424(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_S9_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESG_SQ_SQ_SM_SG_SG_SG_SG_PNSC_ISA_EESG_
	movq	-7464(%rbp), %rax
	pxor	%xmm0, %xmm0
	addq	$112, %rsp
	movl	$152, %edi
	movq	%rax, -256(%rbp)
	movq	-7456(%rbp), %rax
	movq	%rax, -248(%rbp)
	movq	-7448(%rbp), %rax
	movq	%rax, -240(%rbp)
	movq	-7440(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	-7432(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-7424(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-7416(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-7408(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-7400(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-7392(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-7384(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-7376(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-7368(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-7360(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-7352(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-7344(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-7336(%rbp), %rax
	movaps	%xmm0, -7248(%rbp)
	movq	%rax, -128(%rbp)
	movq	-7328(%rbp), %rax
	movq	$0, -7232(%rbp)
	movq	%rax, -120(%rbp)
	movq	-7320(%rbp), %rax
	movq	%rax, -112(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm1
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm2
	movq	-112(%rbp), %rcx
	movdqa	-224(%rbp), %xmm3
	leaq	152(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-208(%rbp), %xmm7
	movdqa	-192(%rbp), %xmm4
	movups	%xmm1, (%rax)
	movdqa	-176(%rbp), %xmm6
	movdqa	-160(%rbp), %xmm5
	movups	%xmm2, 16(%rax)
	movdqa	-128(%rbp), %xmm2
	movdqa	-144(%rbp), %xmm1
	movups	%xmm3, 32(%rax)
	movq	%rcx, 144(%rax)
	movq	-7736(%rbp), %rdi
	movups	%xmm7, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm6, 80(%rax)
	movups	%xmm5, 96(%rax)
	movups	%xmm1, 112(%rax)
	movups	%xmm2, 128(%rax)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1621
	call	_ZdlPv@PLT
.L1621:
	movq	-7808(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1620
	.p2align 4,,10
	.p2align 3
.L1861:
	movq	-7776(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7320(%rbp), %rax
	movq	-7536(%rbp), %rdi
	leaq	-7384(%rbp), %rcx
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7368(%rbp), %r9
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7376(%rbp), %r8
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7392(%rbp), %rdx
	pushq	%rax
	leaq	-7352(%rbp), %rax
	leaq	-7400(%rbp), %rsi
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_S9_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESF_SP_
	addq	$48, %rsp
	movl	$252, %edx
	movq	%r14, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$80, %edi
	movq	-7336(%rbp), %xmm0
	movq	-7352(%rbp), %xmm1
	movq	-7368(%rbp), %xmm2
	movq	-7384(%rbp), %xmm3
	movq	$0, -7232(%rbp)
	movq	-7400(%rbp), %xmm4
	movhps	-7328(%rbp), %xmm0
	movhps	-7344(%rbp), %xmm1
	movhps	-7360(%rbp), %xmm2
	movhps	-7376(%rbp), %xmm3
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-7392(%rbp), %xmm4
	movaps	%xmm3, -240(%rbp)
	movaps	%xmm4, -256(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -7248(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm7
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm4
	movdqa	-224(%rbp), %xmm6
	movdqa	-208(%rbp), %xmm5
	leaq	80(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-192(%rbp), %xmm1
	movq	-7760(%rbp), %rdi
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm1, 64(%rax)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1632
	call	_ZdlPv@PLT
.L1632:
	movq	-7832(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -4032(%rbp)
	je	.L1633
.L1862:
	movq	-7832(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7320(%rbp), %rax
	movq	-7760(%rbp), %rdi
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7376(%rbp), %rcx
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7360(%rbp), %r9
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7368(%rbp), %r8
	pushq	%rax
	leaq	-7352(%rbp), %rax
	leaq	-7384(%rbp), %rdx
	pushq	%rax
	leaq	-7392(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESF_
	addq	$48, %rsp
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7320(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-7328(%rbp), %rax
	movq	-7344(%rbp), %xmm0
	movl	$80, %edi
	movq	-7360(%rbp), %xmm1
	movq	%rbx, -184(%rbp)
	movq	-7376(%rbp), %xmm2
	movhps	-7336(%rbp), %xmm0
	movq	-7392(%rbp), %xmm3
	movq	%rax, -192(%rbp)
	movhps	-7352(%rbp), %xmm1
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-7368(%rbp), %xmm2
	movhps	-7384(%rbp), %xmm3
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm3, -256(%rbp)
	movaps	%xmm2, -240(%rbp)
	movaps	%xmm0, -7248(%rbp)
	movq	$0, -7232(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm2
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm3
	movdqa	-224(%rbp), %xmm7
	movdqa	-208(%rbp), %xmm4
	leaq	80(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-192(%rbp), %xmm6
	movq	-7520(%rbp), %rdi
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm6, 64(%rax)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1634
	call	_ZdlPv@PLT
.L1634:
	movq	-7768(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1633
	.p2align 4,,10
	.p2align 3
.L1863:
	movq	-7816(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7320(%rbp), %rax
	movq	-7600(%rbp), %rdi
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7368(%rbp), %r8
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7376(%rbp), %rcx
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7360(%rbp), %r9
	pushq	%rax
	leaq	-7352(%rbp), %rax
	leaq	-7384(%rbp), %rdx
	pushq	%rax
	leaq	-7392(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES4_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESF_
	addq	$48, %rsp
	movl	$264, %edx
	movq	%r14, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movsd	.LC30(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v88internal48FromConstexpr9ATfloat6419ATconstexpr_float64_164EPNS0_8compiler18CodeAssemblerStateEd@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7328(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12Float64EqualENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-7360(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	-7384(%rbp), %rcx
	movq	-7376(%rbp), %rsi
	movq	-7368(%rbp), %rdx
	movaps	%xmm0, -7248(%rbp)
	movq	-7352(%rbp), %r10
	movq	-7344(%rbp), %r11
	movq	%rdi, -7816(%rbp)
	movq	-7336(%rbp), %r9
	movq	-7392(%rbp), %rax
	movq	%rdi, -224(%rbp)
	movl	$72, %edi
	movq	-7328(%rbp), %rbx
	movq	%rcx, -7776(%rbp)
	movq	%rsi, -7792(%rbp)
	movq	%rdx, -7808(%rbp)
	movq	%r10, -7824(%rbp)
	movq	%r11, -7832(%rbp)
	movq	%r9, -8016(%rbp)
	movq	%rcx, -248(%rbp)
	movq	%rsi, -240(%rbp)
	movq	%rdx, -232(%rbp)
	movq	%r10, -216(%rbp)
	movq	%r11, -208(%rbp)
	movq	%r9, -200(%rbp)
	movq	%rax, -7568(%rbp)
	movq	%rax, -256(%rbp)
	movq	%rbx, -192(%rbp)
	movq	$0, -7232(%rbp)
	call	_Znwm@PLT
	movq	-192(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-256(%rbp), %xmm5
	movdqa	-240(%rbp), %xmm1
	movdqa	-224(%rbp), %xmm2
	leaq	72(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-208(%rbp), %xmm3
	movq	-7768(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1636
	call	_ZdlPv@PLT
.L1636:
	movq	-7568(%rbp), %xmm0
	movl	$72, %edi
	movq	%rbx, -192(%rbp)
	movq	$0, -7232(%rbp)
	movhps	-7776(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	-7792(%rbp), %xmm0
	movhps	-7808(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-7816(%rbp), %xmm0
	movhps	-7824(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-7832(%rbp), %xmm0
	movhps	-8016(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7248(%rbp)
	call	_Znwm@PLT
	movq	-192(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-256(%rbp), %xmm7
	movdqa	-240(%rbp), %xmm4
	movdqa	-224(%rbp), %xmm6
	leaq	72(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-208(%rbp), %xmm5
	movq	%rcx, 64(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm5, 48(%rax)
	leaq	-3520(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	movq	%rax, -7776(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1637
	call	_ZdlPv@PLT
.L1637:
	movq	-7856(%rbp), %rcx
	movq	-7840(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -3648(%rbp)
	je	.L1638
.L1864:
	movq	-7840(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	movq	$0, -7248(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7320(%rbp), %rax
	pushq	%r15
	movq	-7768(%rbp), %rdi
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7360(%rbp), %rcx
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7344(%rbp), %r9
	pushq	%rax
	leaq	-7352(%rbp), %r8
	leaq	-7368(%rbp), %rdx
	leaq	-7376(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TEEE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EE
	addq	$32, %rsp
	movl	$265, %edx
	movq	%r14, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movsd	.LC30(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v88internal60FromConstexpr20UT5ATSmi10HeapNumber19ATconstexpr_float64_157EPNS0_8compiler18CodeAssemblerStateEd@PLT
	movq	-7752(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	jmp	.L1638
	.p2align 4,,10
	.p2align 3
.L1865:
	movq	-7856(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7320(%rbp), %rax
	movq	-7776(%rbp), %rdi
	leaq	-7352(%rbp), %r9
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7360(%rbp), %r8
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7368(%rbp), %rcx
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7376(%rbp), %rdx
	pushq	%rax
	leaq	-7384(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TEEE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EE
	addq	$32, %rsp
	movl	$266, %edx
	movq	%r14, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7352(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	-7376(%rbp), %rcx
	movq	-7368(%rbp), %rsi
	movq	-7360(%rbp), %rdx
	movaps	%xmm0, -7248(%rbp)
	movq	-7344(%rbp), %r10
	movq	-7336(%rbp), %r11
	movq	%rdi, -7824(%rbp)
	movq	-7384(%rbp), %rax
	movq	-7328(%rbp), %r12
	movq	%rdi, -224(%rbp)
	movl	$72, %edi
	movq	-7320(%rbp), %rbx
	movq	%rcx, -7792(%rbp)
	movq	%rsi, -7808(%rbp)
	movq	%rdx, -7816(%rbp)
	movq	%r10, -7832(%rbp)
	movq	%r11, -7840(%rbp)
	movq	%rcx, -248(%rbp)
	movq	%rsi, -240(%rbp)
	movq	%rdx, -232(%rbp)
	movq	%r10, -216(%rbp)
	movq	%r11, -208(%rbp)
	movq	%rax, -7568(%rbp)
	movq	%rax, -256(%rbp)
	movq	%r12, -200(%rbp)
	movq	%rbx, -192(%rbp)
	movq	$0, -7232(%rbp)
	call	_Znwm@PLT
	movq	-192(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-256(%rbp), %xmm1
	movdqa	-240(%rbp), %xmm2
	movdqa	-224(%rbp), %xmm3
	leaq	72(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-208(%rbp), %xmm7
	movq	-7640(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm1, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1640
	call	_ZdlPv@PLT
.L1640:
	movq	-7568(%rbp), %xmm0
	movq	%r12, %xmm4
	movl	$72, %edi
	movq	%rbx, -192(%rbp)
	movq	$0, -7232(%rbp)
	movhps	-7792(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	-7808(%rbp), %xmm0
	movhps	-7816(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-7824(%rbp), %xmm0
	movhps	-7832(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-7840(%rbp), %xmm0
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7248(%rbp)
	call	_Znwm@PLT
	movq	-192(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-256(%rbp), %xmm6
	movdqa	-240(%rbp), %xmm5
	movdqa	-224(%rbp), %xmm1
	leaq	72(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-208(%rbp), %xmm2
	movq	%rcx, 64(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm2, 48(%rax)
	leaq	-3136(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	movq	%rax, -7792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1641
	call	_ZdlPv@PLT
.L1641:
	movq	-7888(%rbp), %rcx
	leaq	-3272(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -3264(%rbp)
	je	.L1642
.L1866:
	leaq	-3272(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	movq	$0, -7248(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7320(%rbp), %rax
	pushq	%r15
	movq	-7640(%rbp), %rdi
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7360(%rbp), %rcx
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7344(%rbp), %r9
	pushq	%rax
	leaq	-7352(%rbp), %r8
	leaq	-7368(%rbp), %rdx
	leaq	-7376(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TEEE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EE
	addq	$32, %rsp
	movl	$267, %edx
	movq	%r14, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal7kNaN_69EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7752(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	jmp	.L1642
	.p2align 4,,10
	.p2align 3
.L1867:
	movq	-7888(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7320(%rbp), %rax
	movq	-7792(%rbp), %rdi
	leaq	-7360(%rbp), %r8
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7368(%rbp), %rcx
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7352(%rbp), %r9
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7376(%rbp), %rdx
	pushq	%rax
	leaq	-7384(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TEEE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EE
	addq	$32, %rsp
	movl	$268, %edx
	movq	%r14, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal46FromConstexpr9ATfloat6417ATconstexpr_int31_163EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7320(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12Float64EqualENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-7352(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	-7376(%rbp), %rcx
	movq	-7368(%rbp), %rsi
	movq	-7360(%rbp), %rdx
	movaps	%xmm0, -7248(%rbp)
	movq	-7344(%rbp), %r10
	movq	-7336(%rbp), %r11
	movq	%rdi, -7840(%rbp)
	movq	-7328(%rbp), %r9
	movq	-7384(%rbp), %rax
	movq	%rdi, -224(%rbp)
	movl	$72, %edi
	movq	-7320(%rbp), %rbx
	movq	%rcx, -7816(%rbp)
	movq	%rsi, -7824(%rbp)
	movq	%rdx, -7832(%rbp)
	movq	%r10, -7856(%rbp)
	movq	%r11, -7888(%rbp)
	movq	%r9, -8016(%rbp)
	movq	%rcx, -248(%rbp)
	movq	%rsi, -240(%rbp)
	movq	%rdx, -232(%rbp)
	movq	%r10, -216(%rbp)
	movq	%r11, -208(%rbp)
	movq	%r9, -200(%rbp)
	movq	%rax, -7568(%rbp)
	movq	%rax, -256(%rbp)
	movq	%rbx, -192(%rbp)
	movq	$0, -7232(%rbp)
	call	_Znwm@PLT
	movq	-192(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-256(%rbp), %xmm3
	movdqa	-240(%rbp), %xmm7
	movdqa	-224(%rbp), %xmm4
	leaq	72(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-208(%rbp), %xmm6
	movq	-7808(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm6, 48(%rax)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1644
	call	_ZdlPv@PLT
.L1644:
	movq	-7568(%rbp), %xmm0
	movl	$72, %edi
	movq	%rbx, -192(%rbp)
	movq	$0, -7232(%rbp)
	movhps	-7816(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	-7824(%rbp), %xmm0
	movhps	-7832(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-7840(%rbp), %xmm0
	movhps	-7856(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-7888(%rbp), %xmm0
	movhps	-8016(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7248(%rbp)
	call	_Znwm@PLT
	movq	-192(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-256(%rbp), %xmm5
	movdqa	-240(%rbp), %xmm1
	movdqa	-224(%rbp), %xmm2
	leaq	72(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-208(%rbp), %xmm3
	movq	%rcx, 64(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm3, 48(%rax)
	leaq	-2752(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	movq	%rax, -7816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1645
	call	_ZdlPv@PLT
.L1645:
	movq	-7920(%rbp), %rcx
	movq	-7904(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2880(%rbp)
	je	.L1646
.L1868:
	movq	-7904(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	movq	$0, -7248(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7320(%rbp), %rax
	pushq	%r15
	movq	-7808(%rbp), %rdi
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7360(%rbp), %rcx
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7344(%rbp), %r9
	pushq	%rax
	leaq	-7352(%rbp), %r8
	leaq	-7368(%rbp), %rdx
	leaq	-7376(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TEEE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EE
	addq	$32, %rsp
	movq	%r14, %rdi
	movl	$269, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	-7752(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	jmp	.L1646
	.p2align 4,,10
	.p2align 3
.L1870:
	movq	-7936(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7320(%rbp), %rax
	movq	-7832(%rbp), %rdi
	leaq	-7368(%rbp), %rcx
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7352(%rbp), %r9
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7360(%rbp), %r8
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7376(%rbp), %rdx
	pushq	%rax
	leaq	-7384(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TEEE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EE
	addq	$32, %rsp
	movl	$264, %edx
	movq	%r14, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7384(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movaps	%xmm0, -7248(%rbp)
	movq	%rax, -256(%rbp)
	movq	-7376(%rbp), %rax
	movq	$0, -7232(%rbp)
	movq	%rax, -248(%rbp)
	movq	-7368(%rbp), %rax
	movq	%rax, -240(%rbp)
	movq	-7360(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	-7352(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-7344(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-7336(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-7328(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-7320(%rbp), %rax
	movq	%rax, -192(%rbp)
	call	_Znwm@PLT
	movq	-192(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-256(%rbp), %xmm1
	movdqa	-240(%rbp), %xmm2
	movdqa	-224(%rbp), %xmm3
	leaq	72(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-208(%rbp), %xmm7
	movq	-7840(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm1, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1650
	call	_ZdlPv@PLT
.L1650:
	movq	-7952(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1649
	.p2align 4,,10
	.p2align 3
.L1869:
	movq	-7920(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7320(%rbp), %rax
	movq	-7816(%rbp), %rdi
	leaq	-7368(%rbp), %rcx
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7352(%rbp), %r9
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7360(%rbp), %r8
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7376(%rbp), %rdx
	pushq	%rax
	leaq	-7384(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TEEE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EE
	addq	$32, %rsp
	movl	$266, %edx
	movq	%r14, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7384(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movaps	%xmm0, -7248(%rbp)
	movq	%rax, -256(%rbp)
	movq	-7376(%rbp), %rax
	movq	$0, -7232(%rbp)
	movq	%rax, -248(%rbp)
	movq	-7368(%rbp), %rax
	movq	%rax, -240(%rbp)
	movq	-7360(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	-7352(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-7344(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-7336(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-7328(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-7320(%rbp), %rax
	movq	%rax, -192(%rbp)
	call	_Znwm@PLT
	movq	-192(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-256(%rbp), %xmm7
	movdqa	-240(%rbp), %xmm4
	movdqa	-224(%rbp), %xmm6
	leaq	72(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-208(%rbp), %xmm5
	movq	-7832(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1648
	call	_ZdlPv@PLT
.L1648:
	movq	-7936(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1647
	.p2align 4,,10
	.p2align 3
.L1871:
	movq	-7952(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7320(%rbp), %rax
	movq	-7840(%rbp), %rdi
	leaq	-7368(%rbp), %rcx
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7352(%rbp), %r9
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7360(%rbp), %r8
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7376(%rbp), %rdx
	pushq	%rax
	leaq	-7384(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TEEE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EE
	addq	$32, %rsp
	movl	$275, %edx
	movq	%r14, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal46FromConstexpr9ATfloat6417ATconstexpr_int31_163EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$276, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal46FromConstexpr9ATfloat6417ATconstexpr_int31_163EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$277, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	-7320(%rbp), %rdx
	movq	-7336(%rbp), %xmm0
	movl	$96, %edi
	movq	-7352(%rbp), %xmm1
	movq	%r12, -184(%rbp)
	movq	-7368(%rbp), %xmm2
	movhps	-7328(%rbp), %xmm0
	movq	-7384(%rbp), %xmm3
	movq	%rdx, -192(%rbp)
	movhps	-7344(%rbp), %xmm1
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-7360(%rbp), %xmm2
	movhps	-7376(%rbp), %xmm3
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm3, -256(%rbp)
	movaps	%xmm2, -240(%rbp)
	movaps	%xmm0, -7248(%rbp)
	movq	%rbx, -176(%rbp)
	movq	%rax, -168(%rbp)
	movq	$0, -7232(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm4
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm6
	movdqa	-224(%rbp), %xmm5
	movdqa	-208(%rbp), %xmm1
	leaq	96(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-192(%rbp), %xmm2
	movdqa	-176(%rbp), %xmm3
	movups	%xmm4, (%rax)
	movq	-7568(%rbp), %rdi
	movups	%xmm6, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm1, 48(%rax)
	movups	%xmm2, 64(%rax)
	movups	%xmm3, 80(%rax)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1652
	call	_ZdlPv@PLT
.L1652:
	movq	-7872(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1651
	.p2align 4,,10
	.p2align 3
.L1872:
	movq	-7872(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7320(%rbp), %rax
	movq	-7568(%rbp), %rdi
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7392(%rbp), %rcx
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7376(%rbp), %r9
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7384(%rbp), %r8
	pushq	%rax
	leaq	-7352(%rbp), %rax
	leaq	-7400(%rbp), %rdx
	pushq	%rax
	leaq	-7360(%rbp), %rax
	leaq	-7408(%rbp), %rsi
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESP_SP_SF_
	addq	$64, %rsp
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7368(%rbp), %rdx
	movq	-7320(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$96, %edi
	movq	-7328(%rbp), %xmm5
	movq	-7344(%rbp), %xmm6
	movaps	%xmm0, -7248(%rbp)
	movq	-7360(%rbp), %xmm3
	movq	-7376(%rbp), %xmm7
	movhps	-7320(%rbp), %xmm5
	movq	-7392(%rbp), %xmm2
	movq	$0, -7232(%rbp)
	movq	-7408(%rbp), %xmm4
	movhps	-7336(%rbp), %xmm6
	movhps	-7352(%rbp), %xmm3
	movaps	%xmm5, -7952(%rbp)
	movhps	-7368(%rbp), %xmm7
	movhps	-7384(%rbp), %xmm2
	movaps	%xmm6, -7936(%rbp)
	movhps	-7400(%rbp), %xmm4
	movaps	%xmm3, -7920(%rbp)
	movaps	%xmm7, -7904(%rbp)
	movaps	%xmm2, -7888(%rbp)
	movaps	%xmm4, -7856(%rbp)
	movaps	%xmm4, -256(%rbp)
	movaps	%xmm2, -240(%rbp)
	movaps	%xmm7, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm7
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm4
	movdqa	-224(%rbp), %xmm6
	movdqa	-208(%rbp), %xmm5
	leaq	96(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-192(%rbp), %xmm1
	movdqa	-176(%rbp), %xmm2
	movups	%xmm7, (%rax)
	movq	-7824(%rbp), %rdi
	movups	%xmm4, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm1, 64(%rax)
	movups	%xmm2, 80(%rax)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1654
	call	_ZdlPv@PLT
.L1654:
	movdqa	-7856(%rbp), %xmm5
	movdqa	-7888(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movl	$96, %edi
	movdqa	-7904(%rbp), %xmm2
	movdqa	-7920(%rbp), %xmm3
	movaps	%xmm0, -7248(%rbp)
	movdqa	-7936(%rbp), %xmm7
	movdqa	-7952(%rbp), %xmm4
	movaps	%xmm5, -256(%rbp)
	movaps	%xmm1, -240(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	movq	$0, -7232(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm5
	movdqa	-224(%rbp), %xmm1
	movdqa	-208(%rbp), %xmm2
	leaq	96(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-192(%rbp), %xmm3
	movdqa	-176(%rbp), %xmm7
	movups	%xmm6, (%rax)
	movq	-7720(%rbp), %rdi
	movups	%xmm5, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm3, 64(%rax)
	movups	%xmm7, 80(%rax)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1655
	call	_ZdlPv@PLT
.L1655:
	movq	-7968(%rbp), %rdx
	leaq	-392(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1920(%rbp)
	je	.L1656
.L1873:
	movq	-7968(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7320(%rbp), %rax
	movq	-7824(%rbp), %rdi
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7376(%rbp), %r9
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7384(%rbp), %r8
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7392(%rbp), %rcx
	pushq	%rax
	leaq	-7352(%rbp), %rax
	leaq	-7400(%rbp), %rdx
	pushq	%rax
	leaq	-7360(%rbp), %rax
	leaq	-7408(%rbp), %rsi
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESP_SP_SF_
	addq	$64, %rsp
	movl	$278, %edx
	movq	%r14, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$8, %esi
	movq	%r14, %rdi
	movq	%rax, -7888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	movq	-7360(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$46, %edx
	leaq	.LC29(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -7856(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$37, %edx
	leaq	.LC29(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7320(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	-7856(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-7856(%rbp), %rax
	movl	$176, %edi
	movq	-7360(%rbp), %xmm1
	movq	-7320(%rbp), %xmm0
	movq	-7888(%rbp), %xmm12
	movq	-7328(%rbp), %xmm6
	movdqa	%xmm1, %xmm5
	movq	-7344(%rbp), %xmm3
	movdqa	%xmm1, %xmm7
	movq	-7376(%rbp), %xmm2
	punpcklqdq	%xmm12, %xmm5
	movq	-7392(%rbp), %xmm4
	movhps	-7352(%rbp), %xmm7
	movq	-7408(%rbp), %xmm11
	punpcklqdq	%xmm0, %xmm6
	movhps	-7336(%rbp), %xmm3
	movq	%xmm0, -136(%rbp)
	movhps	-7384(%rbp), %xmm4
	movhps	-7368(%rbp), %xmm2
	movq	%xmm0, -128(%rbp)
	movhps	-7400(%rbp), %xmm11
	movaps	%xmm5, -8064(%rbp)
	movaps	%xmm6, -8048(%rbp)
	movaps	%xmm3, -8032(%rbp)
	movaps	%xmm7, -8016(%rbp)
	movaps	%xmm4, -7952(%rbp)
	movaps	%xmm11, -7936(%rbp)
	movaps	%xmm11, -256(%rbp)
	movaps	%xmm4, -240(%rbp)
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movq	%xmm1, -120(%rbp)
	movq	%rax, -144(%rbp)
	movaps	%xmm2, -7968(%rbp)
	movaps	%xmm2, -224(%rbp)
	pxor	%xmm2, %xmm2
	movq	%xmm1, -7920(%rbp)
	movq	-7856(%rbp), %rax
	movq	%xmm0, -96(%rbp)
	movq	%xmm0, -88(%rbp)
	movq	%xmm0, -7904(%rbp)
	movq	%xmm12, -112(%rbp)
	movaps	%xmm2, -7248(%rbp)
	movq	%rax, -104(%rbp)
	movq	$0, -7232(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm3
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm7
	movdqa	-224(%rbp), %xmm4
	movdqa	-208(%rbp), %xmm6
	leaq	176(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-176(%rbp), %xmm1
	movdqa	-192(%rbp), %xmm5
	movups	%xmm3, (%rax)
	movdqa	-160(%rbp), %xmm2
	movdqa	-144(%rbp), %xmm3
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movdqa	-128(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm4
	movups	%xmm6, 48(%rax)
	movdqa	-96(%rbp), %xmm6
	movq	-7656(%rbp), %rdi
	movups	%xmm1, 80(%rax)
	movups	%xmm5, 64(%rax)
	movups	%xmm2, 96(%rax)
	movups	%xmm3, 112(%rax)
	movups	%xmm7, 128(%rax)
	movups	%xmm4, 144(%rax)
	movups	%xmm6, 160(%rax)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	movq	-7904(%rbp), %xmm0
	movq	-7920(%rbp), %xmm1
	testq	%rdi, %rdi
	je	.L1657
	movq	%xmm0, -7920(%rbp)
	movq	%xmm1, -7904(%rbp)
	call	_ZdlPv@PLT
	movq	-7920(%rbp), %xmm0
	movq	-7904(%rbp), %xmm1
.L1657:
	movdqa	-7968(%rbp), %xmm5
	movdqa	-7952(%rbp), %xmm6
	movl	$176, %edi
	movq	$0, -7232(%rbp)
	movdqa	-8016(%rbp), %xmm2
	movdqa	-7936(%rbp), %xmm4
	movaps	%xmm5, -224(%rbp)
	movdqa	-8032(%rbp), %xmm3
	movq	-7856(%rbp), %xmm5
	movaps	%xmm6, -240(%rbp)
	movdqa	%xmm0, %xmm6
	movdqa	-8048(%rbp), %xmm7
	punpcklqdq	%xmm1, %xmm6
	movaps	%xmm2, -208(%rbp)
	movq	-7888(%rbp), %xmm1
	movdqa	%xmm5, %xmm2
	punpcklqdq	%xmm0, %xmm2
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm4, -256(%rbp)
	movdqa	-8064(%rbp), %xmm4
	punpcklqdq	%xmm5, %xmm1
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -7248(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm1
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm6
	movdqa	-224(%rbp), %xmm5
	movdqa	-192(%rbp), %xmm2
	leaq	176(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movups	%xmm1, (%rax)
	movdqa	-208(%rbp), %xmm1
	movdqa	-176(%rbp), %xmm3
	movdqa	-160(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm4
	movups	%xmm6, 16(%rax)
	movups	%xmm5, 32(%rax)
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm5
	movups	%xmm1, 48(%rax)
	movdqa	-96(%rbp), %xmm1
	movq	-7664(%rbp), %rdi
	movups	%xmm2, 64(%rax)
	movups	%xmm3, 80(%rax)
	movups	%xmm7, 96(%rax)
	movups	%xmm4, 112(%rax)
	movups	%xmm6, 128(%rax)
	movups	%xmm5, 144(%rax)
	movups	%xmm1, 160(%rax)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1658
	call	_ZdlPv@PLT
.L1658:
	leaq	-1544(%rbp), %rcx
	leaq	-1736(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1728(%rbp)
	je	.L1659
.L1874:
	leaq	-1736(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7488(%rbp)
	movq	$0, -7480(%rbp)
	movq	$0, -7472(%rbp)
	movq	$0, -7464(%rbp)
	movq	$0, -7456(%rbp)
	movq	$0, -7448(%rbp)
	movq	$0, -7440(%rbp)
	movq	$0, -7432(%rbp)
	movq	$0, -7424(%rbp)
	movq	$0, -7416(%rbp)
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7320(%rbp), %rax
	movq	-7656(%rbp), %rdi
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7456(%rbp), %r9
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7464(%rbp), %r8
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7472(%rbp), %rcx
	pushq	%rax
	leaq	-7352(%rbp), %rax
	leaq	-7480(%rbp), %rdx
	pushq	%rax
	leaq	-7360(%rbp), %rax
	leaq	-7488(%rbp), %rsi
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	leaq	-7376(%rbp), %rax
	pushq	%rax
	leaq	-7384(%rbp), %rax
	pushq	%rax
	leaq	-7392(%rbp), %rax
	pushq	%rax
	leaq	-7400(%rbp), %rax
	pushq	%rax
	leaq	-7408(%rbp), %rax
	pushq	%rax
	leaq	-7416(%rbp), %rax
	pushq	%rax
	leaq	-7424(%rbp), %rax
	pushq	%rax
	leaq	-7432(%rbp), %rax
	pushq	%rax
	leaq	-7440(%rbp), %rax
	pushq	%rax
	leaq	-7448(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESQ_SQ_SG_SM_SG_SG_SG_SG_PNSC_ISA_EESG_SG_SG_SG_
	addq	$144, %rsp
	movl	$39, %edx
	movq	%r14, %rdi
	leaq	.LC29(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal20SizeOf9ATfloat64_340EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7320(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrMulENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7344(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$38, %edx
	leaq	.LC29(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	-7352(%rbp), %rdx
	call	_ZN2v88internal33UnsafeNewReference9ATfloat64_1426EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movl	$192, %edi
	movq	-7328(%rbp), %xmm0
	movq	-7344(%rbp), %xmm1
	movq	-7360(%rbp), %xmm2
	movq	-7376(%rbp), %xmm3
	movq	-7392(%rbp), %xmm4
	movhps	-7336(%rbp), %xmm1
	movq	-7408(%rbp), %xmm5
	movhps	-7320(%rbp), %xmm0
	movq	-7424(%rbp), %xmm6
	movhps	-7352(%rbp), %xmm2
	movq	-7440(%rbp), %xmm7
	movhps	-7368(%rbp), %xmm3
	movq	-7456(%rbp), %xmm8
	movhps	-7384(%rbp), %xmm4
	movaps	%xmm2, -128(%rbp)
	movq	-7472(%rbp), %xmm9
	movq	-7488(%rbp), %xmm10
	movhps	-7400(%rbp), %xmm5
	movhps	-7416(%rbp), %xmm6
	movhps	-7432(%rbp), %xmm7
	movhps	-7448(%rbp), %xmm8
	movhps	-7464(%rbp), %xmm9
	movaps	%xmm7, -208(%rbp)
	movhps	-7480(%rbp), %xmm10
	movaps	%xmm9, -240(%rbp)
	movaps	%xmm10, -256(%rbp)
	movaps	%xmm8, -224(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	-7248(%rbp), %xmm0
	movq	$0, -7232(%rbp)
	movhps	-7240(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7248(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm2
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm3
	movdqa	-224(%rbp), %xmm7
	movdqa	-208(%rbp), %xmm4
	leaq	192(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-192(%rbp), %xmm6
	movdqa	-176(%rbp), %xmm5
	movups	%xmm2, (%rax)
	movdqa	-160(%rbp), %xmm1
	movdqa	-144(%rbp), %xmm2
	movups	%xmm3, 16(%rax)
	movups	%xmm7, 32(%rax)
	movdqa	-128(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm7
	movups	%xmm4, 48(%rax)
	movdqa	-96(%rbp), %xmm4
	movq	-7696(%rbp), %rdi
	movups	%xmm6, 64(%rax)
	movdqa	-80(%rbp), %xmm6
	movups	%xmm5, 80(%rax)
	movups	%xmm1, 96(%rax)
	movups	%xmm2, 112(%rax)
	movups	%xmm3, 128(%rax)
	movups	%xmm7, 144(%rax)
	movups	%xmm4, 160(%rax)
	movups	%xmm6, 176(%rax)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1660
	call	_ZdlPv@PLT
.L1660:
	leaq	-1352(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1536(%rbp)
	je	.L1661
.L1875:
	leaq	-1544(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7488(%rbp)
	movq	$0, -7480(%rbp)
	movq	$0, -7472(%rbp)
	movq	$0, -7464(%rbp)
	movq	$0, -7456(%rbp)
	movq	$0, -7448(%rbp)
	movq	$0, -7440(%rbp)
	movq	$0, -7432(%rbp)
	movq	$0, -7424(%rbp)
	movq	$0, -7416(%rbp)
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7320(%rbp), %rax
	movq	-7664(%rbp), %rdi
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7472(%rbp), %rcx
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7456(%rbp), %r9
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7464(%rbp), %r8
	pushq	%rax
	leaq	-7352(%rbp), %rax
	leaq	-7480(%rbp), %rdx
	pushq	%rax
	leaq	-7360(%rbp), %rax
	leaq	-7488(%rbp), %rsi
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	leaq	-7376(%rbp), %rax
	pushq	%rax
	leaq	-7384(%rbp), %rax
	pushq	%rax
	leaq	-7392(%rbp), %rax
	pushq	%rax
	leaq	-7400(%rbp), %rax
	pushq	%rax
	leaq	-7408(%rbp), %rax
	pushq	%rax
	leaq	-7416(%rbp), %rax
	pushq	%rax
	leaq	-7424(%rbp), %rax
	pushq	%rax
	leaq	-7432(%rbp), %rax
	pushq	%rax
	leaq	-7440(%rbp), %rax
	pushq	%rax
	leaq	-7448(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESQ_SQ_SG_SM_SG_SG_SG_SG_PNSC_ISA_EESG_SG_SG_SG_
	addq	$144, %rsp
	movl	$41, %edx
	movq	%r14, %rdi
	leaq	.LC29(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7488(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$136, %edi
	movq	%rax, -256(%rbp)
	movq	-7480(%rbp), %rax
	movq	%rax, -248(%rbp)
	movq	-7472(%rbp), %rax
	movq	%rax, -240(%rbp)
	movq	-7464(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	-7456(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-7448(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-7440(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-7432(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-7424(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-7416(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-7408(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-7400(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-7392(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-7384(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-7376(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-7368(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-7360(%rbp), %rax
	movaps	%xmm0, -7248(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -7232(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm5
	movq	%r15, %rsi
	movq	-128(%rbp), %rcx
	movdqa	-240(%rbp), %xmm1
	movdqa	-224(%rbp), %xmm2
	leaq	136(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-208(%rbp), %xmm3
	movdqa	-192(%rbp), %xmm7
	movups	%xmm5, (%rax)
	movdqa	-176(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm6
	movq	%rcx, 128(%rax)
	movdqa	-144(%rbp), %xmm5
	movq	-7712(%rbp), %rdi
	movups	%xmm1, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm4, 80(%rax)
	movups	%xmm6, 96(%rax)
	movups	%xmm5, 112(%rax)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1662
	call	_ZdlPv@PLT
.L1662:
	leaq	-1160(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1661
	.p2align 4,,10
	.p2align 3
.L1876:
	leaq	-1352(%rbp), %rsi
	movq	%r14, %rdi
	movabsq	$362263814143805189, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	$0, -7232(%rbp)
	movaps	%xmm0, -7248(%rbp)
	call	_Znwm@PLT
	movdqa	.LC28(%rip), %xmm0
	movq	-7696(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, 16(%rax)
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -7248(%rbp)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7248(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1664
	call	_ZdlPv@PLT
.L1664:
	movq	(%rbx), %rax
	movq	40(%rax), %rdx
	movq	8(%rax), %rcx
	movq	24(%rax), %rsi
	movq	72(%rax), %rdi
	movq	88(%rax), %r11
	movq	104(%rax), %r10
	movq	%rdx, -7952(%rbp)
	movq	56(%rax), %rdx
	movq	(%rax), %rbx
	movq	%rcx, -7888(%rbp)
	movq	120(%rax), %r9
	movq	16(%rax), %rcx
	movq	%rsi, -7920(%rbp)
	movq	%rdx, -7968(%rbp)
	movq	32(%rax), %rsi
	movq	64(%rax), %rdx
	movq	%rdi, -8032(%rbp)
	movq	80(%rax), %rdi
	movq	%r11, -8064(%rbp)
	movq	96(%rax), %r11
	movq	48(%rax), %r12
	movq	%r10, -8096(%rbp)
	movq	112(%rax), %r10
	movq	%rcx, -7904(%rbp)
	movq	%r11, -8080(%rbp)
	movq	%r10, -8112(%rbp)
	movq	%rbx, -7856(%rbp)
	movq	%rsi, -7936(%rbp)
	leaq	.LC29(%rip), %rsi
	movq	%rdx, -8016(%rbp)
	movl	$46, %edx
	movq	%rdi, -8048(%rbp)
	movq	%r14, %rdi
	movq	%r9, -8000(%rbp)
	movq	128(%rax), %r9
	movq	176(%rax), %r8
	movq	184(%rax), %rbx
	movq	%r9, -8120(%rbp)
	movq	%r8, -8128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$152, %edi
	movq	%rbx, -112(%rbp)
	movq	-7856(%rbp), %xmm0
	movq	$0, -7232(%rbp)
	movhps	-7888(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	-7904(%rbp), %xmm0
	movhps	-7920(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-7936(%rbp), %xmm0
	movhps	-7952(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	%r12, %xmm0
	movhps	-7968(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-8016(%rbp), %xmm0
	movhps	-8032(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-8048(%rbp), %xmm0
	movhps	-8064(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-8080(%rbp), %xmm0
	movhps	-8096(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-8112(%rbp), %xmm0
	movhps	-8000(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-8120(%rbp), %xmm0
	movhps	-8128(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7248(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm1
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm2
	movq	-112(%rbp), %rcx
	movdqa	-224(%rbp), %xmm3
	leaq	152(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movups	%xmm1, (%rax)
	movdqa	-208(%rbp), %xmm7
	movdqa	-192(%rbp), %xmm4
	movups	%xmm2, 16(%rax)
	movdqa	-176(%rbp), %xmm6
	movdqa	-160(%rbp), %xmm5
	movdqa	-144(%rbp), %xmm1
	movdqa	-128(%rbp), %xmm2
	movq	%rcx, 144(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm6, 80(%rax)
	movups	%xmm5, 96(%rax)
	movups	%xmm1, 112(%rax)
	movups	%xmm2, 128(%rax)
	leaq	-1024(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	movq	%rax, -7856(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1665
	call	_ZdlPv@PLT
.L1665:
	movq	-7976(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1152(%rbp)
	je	.L1666
.L1877:
	leaq	-1160(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$17, %edi
	movq	$0, -7232(%rbp)
	movaps	%xmm0, -7248(%rbp)
	call	_Znwm@PLT
	movdqa	.LC28(%rip), %xmm0
	movq	-7712(%rbp), %rdi
	movq	%r15, %rsi
	movb	$5, 16(%rax)
	leaq	17(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -7248(%rbp)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1667
	call	_ZdlPv@PLT
.L1667:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L1666
	.p2align 4,,10
	.p2align 3
.L1878:
	movq	-7976(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7464(%rbp)
	movq	$0, -7456(%rbp)
	movq	$0, -7448(%rbp)
	movq	$0, -7440(%rbp)
	movq	$0, -7432(%rbp)
	movq	$0, -7424(%rbp)
	movq	$0, -7416(%rbp)
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7320(%rbp), %rax
	movq	-7856(%rbp), %rdi
	leaq	-7448(%rbp), %rcx
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7456(%rbp), %rdx
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7464(%rbp), %rsi
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7432(%rbp), %r9
	pushq	%rax
	leaq	-7352(%rbp), %rax
	leaq	-7440(%rbp), %r8
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	leaq	-7376(%rbp), %rax
	pushq	%rax
	leaq	-7384(%rbp), %rax
	pushq	%rax
	leaq	-7392(%rbp), %rax
	pushq	%rax
	leaq	-7400(%rbp), %rax
	pushq	%rax
	leaq	-7408(%rbp), %rax
	pushq	%rax
	leaq	-7416(%rbp), %rax
	pushq	%rax
	leaq	-7424(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESQ_SQ_SG_SM_SG_SG_SG_SG_PNSC_ISA_EESG_
	movq	-7464(%rbp), %rax
	pxor	%xmm0, %xmm0
	addq	$112, %rsp
	movl	$152, %edi
	movq	%rax, -256(%rbp)
	movq	-7456(%rbp), %rax
	movq	%rax, -248(%rbp)
	movq	-7448(%rbp), %rax
	movq	%rax, -240(%rbp)
	movq	-7440(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	-7432(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-7424(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-7416(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-7408(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-7400(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-7392(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-7384(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-7376(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-7368(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-7360(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-7352(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-7344(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-7336(%rbp), %rax
	movaps	%xmm0, -7248(%rbp)
	movq	%rax, -128(%rbp)
	movq	-7328(%rbp), %rax
	movq	$0, -7232(%rbp)
	movq	%rax, -120(%rbp)
	movq	-7320(%rbp), %rax
	movq	%rax, -112(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movdqa	-256(%rbp), %xmm3
	movdqa	-240(%rbp), %xmm7
	leaq	152(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-224(%rbp), %xmm4
	movdqa	-208(%rbp), %xmm6
	movq	%rcx, 144(%rax)
	movdqa	-192(%rbp), %xmm5
	movdqa	-176(%rbp), %xmm1
	movups	%xmm3, (%rax)
	movdqa	-160(%rbp), %xmm2
	movdqa	-144(%rbp), %xmm3
	movups	%xmm7, 16(%rax)
	movdqa	-128(%rbp), %xmm7
	movups	%xmm4, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm5, 64(%rax)
	movups	%xmm1, 80(%rax)
	movups	%xmm2, 96(%rax)
	movups	%xmm3, 112(%rax)
	movups	%xmm7, 128(%rax)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1669
	call	_ZdlPv@PLT
.L1669:
	movq	-7984(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1668
	.p2align 4,,10
	.p2align 3
.L1879:
	movq	-7984(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7464(%rbp)
	movq	$0, -7456(%rbp)
	movq	$0, -7448(%rbp)
	movq	$0, -7440(%rbp)
	movq	$0, -7432(%rbp)
	movq	$0, -7424(%rbp)
	movq	$0, -7416(%rbp)
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7320(%rbp), %rax
	movq	%rbx, %rdi
	leaq	-7448(%rbp), %rcx
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7432(%rbp), %r9
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7440(%rbp), %r8
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7456(%rbp), %rdx
	pushq	%rax
	leaq	-7352(%rbp), %rax
	leaq	-7464(%rbp), %rsi
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	leaq	-7376(%rbp), %rax
	pushq	%rax
	leaq	-7384(%rbp), %rax
	pushq	%rax
	leaq	-7392(%rbp), %rax
	pushq	%rax
	leaq	-7400(%rbp), %rax
	pushq	%rax
	leaq	-7408(%rbp), %rax
	pushq	%rax
	leaq	-7416(%rbp), %rax
	pushq	%rax
	leaq	-7424(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_S7_S4_S4_S4_S4_NS0_10HeapObjectES4_EE10CreatePhisEPNS1_5TNodeIS3_EESE_PNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EESG_PNSC_IS7_EEPNSC_IS8_EEPNSC_IS9_EESQ_SQ_SG_SM_SG_SG_SG_SG_PNSC_ISA_EESG_
	addq	$112, %rsp
	movl	$278, %edx
	movq	%r14, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7328(%rbp), %r8
	movl	$1, %esi
	movq	%r15, %rdi
	movq	-7320(%rbp), %r9
	movq	%r8, -7888(%rbp)
	movq	%r9, -7904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-7904(%rbp), %r9
	movq	%r15, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-7888(%rbp), %r8
	movzwl	_ZN2v88internal13MachineTypeOfINS0_8Float64TEvE5valueE(%rip), %esi
	movq	%r15, %rdi
	movq	%rax, %rcx
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, -7888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7888(%rbp), %r8
	movq	-7400(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler10Float64DivENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, -7888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$279, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7888(%rbp), %r8
	movq	%r15, %rdi
	movq	%r8, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler10Float64MulENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, -7888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7888(%rbp), %r8
	movq	-7384(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler10Float64SubENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, -7888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$280, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7888(%rbp), %r9
	movq	-7392(%rbp), %rsi
	movq	%r15, %rdi
	movq	%r9, %rdx
	movq	%r9, -7920(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler10Float64AddENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, -7888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$281, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7392(%rbp), %rdx
	movq	-7888(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler10Float64SubENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, -7904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7920(%rbp), %r9
	movq	-7904(%rbp), %r8
	movq	%r15, %rdi
	movq	%r9, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler10Float64SubENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, -7904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$282, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	.LC1(%rip), %rsi
	movl	$277, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7888(%rbp), %rax
	movl	$96, %edi
	movq	-7416(%rbp), %xmm0
	movq	-7432(%rbp), %xmm1
	movq	-7400(%rbp), %rdx
	movq	$0, -7232(%rbp)
	movq	%rax, -184(%rbp)
	movq	-7904(%rbp), %rax
	movhps	-7408(%rbp), %xmm0
	movq	-7448(%rbp), %xmm2
	movhps	-7424(%rbp), %xmm1
	movq	-7464(%rbp), %xmm3
	movaps	%xmm0, -208(%rbp)
	movq	%rax, -176(%rbp)
	movq	-7376(%rbp), %rax
	pxor	%xmm0, %xmm0
	movhps	-7440(%rbp), %xmm2
	movhps	-7456(%rbp), %xmm3
	movq	%rdx, -192(%rbp)
	movaps	%xmm3, -256(%rbp)
	movaps	%xmm2, -240(%rbp)
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm0, -7248(%rbp)
	movq	%rax, -168(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm4
	movq	%r12, %rdi
	movdqa	-240(%rbp), %xmm6
	movdqa	-224(%rbp), %xmm5
	leaq	96(%rax), %rdx
	movq	%r15, %rsi
	movdqa	-208(%rbp), %xmm1
	movdqa	-192(%rbp), %xmm2
	movdqa	-176(%rbp), %xmm3
	movups	%xmm4, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm1, 48(%rax)
	movups	%xmm2, 64(%rax)
	movups	%xmm3, 80(%rax)
	movq	%rax, -7248(%rbp)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1671
	call	_ZdlPv@PLT
.L1671:
	movq	-7992(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -576(%rbp)
	je	.L1672
.L1880:
	movq	-7992(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-7320(%rbp), %rax
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7392(%rbp), %rcx
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7376(%rbp), %r9
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7384(%rbp), %r8
	pushq	%rax
	leaq	-7352(%rbp), %rax
	leaq	-7400(%rbp), %rdx
	pushq	%rax
	leaq	-7360(%rbp), %rax
	leaq	-7408(%rbp), %rsi
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESP_SP_SF_
	addq	$64, %rsp
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, -7888(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7320(%rbp), %rsi
	movq	-7888(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, -7888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-7328(%rbp), %rdx
	movl	$96, %edi
	movq	-7344(%rbp), %xmm0
	movq	-7360(%rbp), %xmm1
	movq	-7888(%rbp), %rax
	movq	$0, -7232(%rbp)
	movq	-7376(%rbp), %xmm2
	movhps	-7336(%rbp), %xmm0
	movq	-7392(%rbp), %xmm3
	movq	%rdx, -176(%rbp)
	movq	-7408(%rbp), %xmm4
	movhps	-7352(%rbp), %xmm1
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-7368(%rbp), %xmm2
	movhps	-7384(%rbp), %xmm3
	movaps	%xmm1, -208(%rbp)
	movhps	-7400(%rbp), %xmm4
	movaps	%xmm3, -240(%rbp)
	movaps	%xmm4, -256(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm0, -7248(%rbp)
	movq	%rax, -168(%rbp)
	call	_Znwm@PLT
	movdqa	-256(%rbp), %xmm7
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm4
	movdqa	-224(%rbp), %xmm6
	movdqa	-208(%rbp), %xmm5
	leaq	96(%rax), %rdx
	movq	%rax, -7248(%rbp)
	movdqa	-192(%rbp), %xmm1
	movdqa	-176(%rbp), %xmm2
	movups	%xmm7, (%rax)
	movq	-7568(%rbp), %rdi
	movups	%xmm4, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm1, 64(%rax)
	movups	%xmm2, 80(%rax)
	movq	%rdx, -7232(%rbp)
	movq	%rdx, -7240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1673
	call	_ZdlPv@PLT
.L1673:
	movq	-7872(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -384(%rbp)
	je	.L1674
.L1881:
	leaq	-392(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7320(%rbp), %rax
	movq	-7720(%rbp), %rdi
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7392(%rbp), %rcx
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7376(%rbp), %r9
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7384(%rbp), %r8
	pushq	%rax
	leaq	-7352(%rbp), %rax
	leaq	-7400(%rbp), %rdx
	pushq	%rax
	leaq	-7360(%rbp), %rax
	leaq	-7408(%rbp), %rsi
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectES4_NS0_16FixedDoubleArrayENS0_5BoolTENS0_8Float64TES9_S9_S4_EE10CreatePhisEPNS1_5TNodeIS3_EESD_PNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESF_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESP_SP_SF_
	addq	$64, %rsp
	movq	%r14, %rdi
	movl	$284, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7336(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11Float64SqrtENS1_11SloppyTNodeINS0_8Float64TEEE@PLT
	movq	%r15, %rdi
	movq	%rax, -7872(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7872(%rbp), %r8
	movq	-7344(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler10Float64MulENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, -7872(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-7872(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal43Convert20UT5ATSmi10HeapNumber9ATfloat64_199EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_8Float64TEEE@PLT
	movq	-7752(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	jmp	.L1674
.L1882:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22655:
	.size	_ZN2v88internal18MathHypotAssembler21GenerateMathHypotImplEv, .-_ZN2v88internal18MathHypotAssembler21GenerateMathHypotImplEv
	.section	.rodata._ZN2v88internal8Builtins18Generate_MathHypotEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC31:
	.string	"MathHypot"
	.section	.text._ZN2v88internal8Builtins18Generate_MathHypotEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins18Generate_MathHypotEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins18Generate_MathHypotEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins18Generate_MathHypotEPNS0_8compiler18CodeAssemblerStateE:
.LFB22651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1025, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC31(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$844, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L1887
.L1884:
	movq	%r13, %rdi
	call	_ZN2v88internal18MathHypotAssembler21GenerateMathHypotImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1888
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1887:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L1884
.L1888:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22651:
	.size	_ZN2v88internal8Builtins18Generate_MathHypotEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins18Generate_MathHypotEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins17Generate_MathAcosEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins17Generate_MathAcosEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins17Generate_MathAcosEPNS0_8compiler18CodeAssemblerStateE:
.LFB29537:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29537:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins17Generate_MathAcosEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins17Generate_MathAcosEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins17Generate_MathAcosEPNS0_8compiler18CodeAssemblerStateE
	.weak	_ZN2v88internal13MachineTypeOfINS0_8Float64TEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_8Float64TEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_8Float64TEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_8Float64TEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_8Float64TEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_8Float64TEvE5valueE:
	.byte	13
	.byte	6
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC27:
	.byte	5
	.byte	5
	.byte	5
	.byte	7
	.byte	8
	.byte	5
	.byte	7
	.byte	4
	.byte	13
	.byte	5
	.byte	13
	.byte	13
	.byte	7
	.byte	5
	.byte	5
	.byte	5
	.align 16
.LC28:
	.byte	5
	.byte	5
	.byte	5
	.byte	7
	.byte	8
	.byte	5
	.byte	7
	.byte	4
	.byte	13
	.byte	13
	.byte	13
	.byte	5
	.byte	7
	.byte	5
	.byte	5
	.byte	5
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC30:
	.long	0
	.long	2146435072
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
