	.file	"regexp-tq-csa.cc"
	.text
	.section	.rodata._ZN2v88internal22IsFastRegExpStrict_326EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../../deps/v8/../../deps/v8/src/builtins/regexp.tq"
	.section	.text._ZN2v88internal22IsFastRegExpStrict_326EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22IsFastRegExpStrict_326EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEE
	.type	_ZN2v88internal22IsFastRegExpStrict_326EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEE, @function
_ZN2v88internal22IsFastRegExpStrict_326EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEE:
.LFB22410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1368(%rbp), %r14
	leaq	-216(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1552(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1720(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1424(%rbp), %rbx
	subq	$1784, %rsp
	movq	%rdi, -1800(%rbp)
	movq	%rsi, -1792(%rbp)
	movq	%rdx, -1776(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1720(%rbp)
	movq	%rdi, -1424(%rbp)
	movl	$48, %edi
	movq	$0, -1416(%rbp)
	movq	$0, -1408(%rbp)
	movq	$0, -1400(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -1400(%rbp)
	movq	%rdx, -1408(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1384(%rbp)
	movq	%rax, -1416(%rbp)
	movq	$0, -1392(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1720(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1224(%rbp)
	movq	$0, -1216(%rbp)
	movq	%rax, -1232(%rbp)
	movq	$0, -1208(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1224(%rbp)
	leaq	-1176(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1208(%rbp)
	movq	%rdx, -1216(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1192(%rbp)
	movq	%rax, -1768(%rbp)
	movq	$0, -1200(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1720(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1032(%rbp)
	movq	$0, -1024(%rbp)
	movq	%rax, -1040(%rbp)
	movq	$0, -1016(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1032(%rbp)
	leaq	-984(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1016(%rbp)
	movq	%rdx, -1024(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1000(%rbp)
	movq	%rax, -1760(%rbp)
	movq	$0, -1008(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1720(%rbp), %rax
	movl	$48, %edi
	movq	$0, -840(%rbp)
	movq	$0, -832(%rbp)
	movq	%rax, -848(%rbp)
	movq	$0, -824(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -840(%rbp)
	leaq	-792(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -824(%rbp)
	movq	%rdx, -832(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -808(%rbp)
	movq	%rax, -1752(%rbp)
	movq	$0, -816(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1720(%rbp), %rax
	movl	$48, %edi
	movq	$0, -648(%rbp)
	movq	$0, -640(%rbp)
	movq	%rax, -656(%rbp)
	movq	$0, -632(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -648(%rbp)
	leaq	-600(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -632(%rbp)
	movq	%rdx, -640(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -616(%rbp)
	movq	%rax, -1744(%rbp)
	movq	$0, -624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1720(%rbp), %rax
	movl	$72, %edi
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	%rax, -464(%rbp)
	movq	$0, -440(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -456(%rbp)
	leaq	-408(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -440(%rbp)
	movq	%rdx, -448(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -424(%rbp)
	movq	%rax, -1736(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1720(%rbp), %rax
	movl	$72, %edi
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	%rax, -272(%rbp)
	movq	$0, -248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -248(%rbp)
	movq	%rdx, -256(%rbp)
	xorl	%edx, %edx
	movq	%rax, -264(%rbp)
	movups	%xmm0, -232(%rbp)
	movq	$0, -240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1792(%rbp), %xmm1
	movaps	%xmm0, -1552(%rbp)
	movhps	-1776(%rbp), %xmm1
	movq	$0, -1536(%rbp)
	movaps	%xmm1, -1792(%rbp)
	call	_Znwm@PLT
	movdqa	-1792(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1552(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2
	call	_ZdlPv@PLT
.L2:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1360(%rbp)
	jne	.L165
	cmpq	$0, -1168(%rbp)
	jne	.L166
.L9:
	cmpq	$0, -976(%rbp)
	jne	.L167
.L12:
	cmpq	$0, -784(%rbp)
	jne	.L168
.L15:
	cmpq	$0, -592(%rbp)
	jne	.L169
.L18:
	cmpq	$0, -400(%rbp)
	leaq	-272(%rbp), %rbx
	jne	.L170
.L21:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$4, 2(%rax)
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L24
	call	_ZdlPv@PLT
.L24:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L25
	call	_ZdlPv@PLT
.L25:
	movq	-256(%rbp), %rbx
	movq	-264(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L26
	.p2align 4,,10
	.p2align 3
.L30:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L27
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L30
.L28:
	movq	-264(%rbp), %r15
.L26:
	testq	%r15, %r15
	je	.L31
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L31:
	movq	-1736(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L32
	call	_ZdlPv@PLT
.L32:
	movq	-448(%rbp), %rbx
	movq	-456(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L33
	.p2align 4,,10
	.p2align 3
.L37:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L34
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L37
.L35:
	movq	-456(%rbp), %r15
.L33:
	testq	%r15, %r15
	je	.L38
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L38:
	movq	-1744(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L39
	call	_ZdlPv@PLT
.L39:
	movq	-640(%rbp), %rbx
	movq	-648(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L40
	.p2align 4,,10
	.p2align 3
.L44:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L41
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L44
.L42:
	movq	-648(%rbp), %r15
.L40:
	testq	%r15, %r15
	je	.L45
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L45:
	movq	-1752(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-816(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L46
	call	_ZdlPv@PLT
.L46:
	movq	-832(%rbp), %rbx
	movq	-840(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L47
	.p2align 4,,10
	.p2align 3
.L51:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L48
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L51
.L49:
	movq	-840(%rbp), %r15
.L47:
	testq	%r15, %r15
	je	.L52
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L52:
	movq	-1760(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1008(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L53
	call	_ZdlPv@PLT
.L53:
	movq	-1024(%rbp), %rbx
	movq	-1032(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L54
	.p2align 4,,10
	.p2align 3
.L58:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L55
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L58
.L56:
	movq	-1032(%rbp), %r15
.L54:
	testq	%r15, %r15
	je	.L59
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L59:
	movq	-1768(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L60
	call	_ZdlPv@PLT
.L60:
	movq	-1216(%rbp), %rbx
	movq	-1224(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L61
	.p2align 4,,10
	.p2align 3
.L65:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L62
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L65
.L63:
	movq	-1224(%rbp), %r15
.L61:
	testq	%r15, %r15
	je	.L66
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L66:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L67
	call	_ZdlPv@PLT
.L67:
	movq	-1408(%rbp), %rbx
	movq	-1416(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L68
	.p2align 4,,10
	.p2align 3
.L72:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L69
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L72
.L70:
	movq	-1416(%rbp), %r14
.L68:
	testq	%r14, %r14
	je	.L73
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L73:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L171
	addq	$1784, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L72
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L62:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L65
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L55:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L58
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L48:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L51
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L41:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L44
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L27:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L30
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L34:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L37
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L165:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r11d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r11w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L4
	call	_ZdlPv@PLT
.L4:
	movq	(%rbx), %rax
	movl	$13, %edx
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rsi
	leaq	-1712(%rbp), %rbx
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rcx, -1824(%rbp)
	movq	%rax, -1776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-1680(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1792(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1792(%rbp), %rcx
	movq	%r13, %r8
	movq	%rbx, %rdi
	movq	-1776(%rbp), %rdx
	movq	-1824(%rbp), %rsi
	call	_ZN2v88internal23RegExpBuiltinsAssembler25BranchIfFastRegExp_StrictENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10HeapObjectEEEPNS2_18CodeAssemblerLabelES9_@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -1672(%rbp)
	jne	.L172
	cmpq	$0, -1544(%rbp)
	jne	.L173
.L7:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1792(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1168(%rbp)
	je	.L9
.L166:
	movq	-1768(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1232(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r10d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r10w, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	movq	(%rbx), %rax
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movdqu	(%rax), %xmm0
	movaps	%xmm1, -1552(%rbp)
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1792(%rbp)
	call	_Znwm@PLT
	movdqa	-1792(%rbp), %xmm0
	leaq	-848(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1552(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L11
	call	_ZdlPv@PLT
.L11:
	movq	-1752(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -976(%rbp)
	je	.L12
.L167:
	movq	-1760(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1040(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r9d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L13
	call	_ZdlPv@PLT
.L13:
	movq	(%rbx), %rax
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movdqu	(%rax), %xmm0
	movaps	%xmm1, -1552(%rbp)
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1792(%rbp)
	call	_Znwm@PLT
	movdqa	-1792(%rbp), %xmm0
	leaq	-656(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1552(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L14
	call	_ZdlPv@PLT
.L14:
	movq	-1744(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -784(%rbp)
	je	.L15
.L168:
	movq	-1752(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-848(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r8d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L16
	call	_ZdlPv@PLT
.L16:
	movq	(%rbx), %rax
	movq	-1800(%rbp), %rdi
	movl	$1, %esi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -1792(%rbp)
	call	_ZN2v88internal42FromConstexpr6ATbool16ATconstexpr_bool_165EPNS0_8compiler18CodeAssemblerStateEb@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	$0, -1536(%rbp)
	movhps	-1792(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm2
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-464(%rbp), %rdi
	movq	%rax, -1552(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L17
	call	_ZdlPv@PLT
.L17:
	movq	-1736(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -592(%rbp)
	je	.L18
.L169:
	movq	-1744(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-656(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movl	$1799, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	2(%rax), %rdx
	movq	%rbx, %rdi
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L19
	call	_ZdlPv@PLT
.L19:
	movq	(%rbx), %rax
	movq	-1800(%rbp), %rdi
	xorl	%esi, %esi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -1792(%rbp)
	call	_ZN2v88internal42FromConstexpr6ATbool16ATconstexpr_bool_165EPNS0_8compiler18CodeAssemblerStateEb@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	$0, -1536(%rbp)
	movhps	-1792(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm3
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-464(%rbp), %rdi
	movq	%rax, -1552(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L20
	call	_ZdlPv@PLT
.L20:
	movq	-1736(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L170:
	movq	-1736(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-464(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$4, 2(%rax)
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L22
	call	_ZdlPv@PLT
.L22:
	movq	(%rbx), %rax
	movl	$12, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -1776(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -1792(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %edi
	movq	%rbx, -64(%rbp)
	movq	-1792(%rbp), %xmm0
	movq	$0, -1536(%rbp)
	leaq	-272(%rbp), %rbx
	movhps	-1776(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	-80(%rbp), %xmm4
	leaq	24(%rax), %rdx
	movq	%rax, -1552(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L23
	call	_ZdlPv@PLT
.L23:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L172:
	movq	-1792(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1776(%rbp), %rax
	movl	$24, %edi
	movq	-1824(%rbp), %xmm0
	movq	$0, -1696(%rbp)
	movq	%rax, %xmm7
	movq	%rax, -64(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1712(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm5
	movq	%rbx, %rsi
	leaq	24(%rax), %rdx
	leaq	-1232(%rbp), %rdi
	movq	%rax, -1712(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -1696(%rbp)
	movq	%rdx, -1704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6
	call	_ZdlPv@PLT
.L6:
	movq	-1768(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1544(%rbp)
	je	.L7
.L173:
	movq	-1824(%rbp), %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movhps	-1776(%rbp), %xmm0
	movaps	%xmm0, -1824(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-1824(%rbp), %xmm0
	movl	$24, %edi
	movq	-1776(%rbp), %rax
	movq	$0, -1696(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1712(%rbp)
	movq	%rax, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm6
	movq	%rbx, %rsi
	leaq	24(%rax), %rdx
	leaq	-1040(%rbp), %rdi
	movq	%rax, -1712(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1696(%rbp)
	movq	%rdx, -1704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L8
	call	_ZdlPv@PLT
.L8:
	movq	-1760(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L7
.L171:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22410:
	.size	_ZN2v88internal22IsFastRegExpStrict_326EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEE, .-_ZN2v88internal22IsFastRegExpStrict_326EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEE
	.section	.text._ZN2v88internal26IsFastRegExpPermissive_327EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26IsFastRegExpPermissive_327EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEE
	.type	_ZN2v88internal26IsFastRegExpPermissive_327EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEE, @function
_ZN2v88internal26IsFastRegExpPermissive_327EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEE:
.LFB22423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1368(%rbp), %r14
	leaq	-216(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1552(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1720(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1424(%rbp), %rbx
	subq	$1784, %rsp
	movq	%rdi, -1800(%rbp)
	movq	%rsi, -1792(%rbp)
	movq	%rdx, -1776(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1720(%rbp)
	movq	%rdi, -1424(%rbp)
	movl	$48, %edi
	movq	$0, -1416(%rbp)
	movq	$0, -1408(%rbp)
	movq	$0, -1400(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -1400(%rbp)
	movq	%rdx, -1408(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1384(%rbp)
	movq	%rax, -1416(%rbp)
	movq	$0, -1392(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1720(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1224(%rbp)
	movq	$0, -1216(%rbp)
	movq	%rax, -1232(%rbp)
	movq	$0, -1208(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1224(%rbp)
	leaq	-1176(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1208(%rbp)
	movq	%rdx, -1216(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1192(%rbp)
	movq	%rax, -1768(%rbp)
	movq	$0, -1200(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1720(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1032(%rbp)
	movq	$0, -1024(%rbp)
	movq	%rax, -1040(%rbp)
	movq	$0, -1016(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1032(%rbp)
	leaq	-984(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1016(%rbp)
	movq	%rdx, -1024(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1000(%rbp)
	movq	%rax, -1760(%rbp)
	movq	$0, -1008(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1720(%rbp), %rax
	movl	$48, %edi
	movq	$0, -840(%rbp)
	movq	$0, -832(%rbp)
	movq	%rax, -848(%rbp)
	movq	$0, -824(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -840(%rbp)
	leaq	-792(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -824(%rbp)
	movq	%rdx, -832(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -808(%rbp)
	movq	%rax, -1752(%rbp)
	movq	$0, -816(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1720(%rbp), %rax
	movl	$48, %edi
	movq	$0, -648(%rbp)
	movq	$0, -640(%rbp)
	movq	%rax, -656(%rbp)
	movq	$0, -632(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -648(%rbp)
	leaq	-600(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -632(%rbp)
	movq	%rdx, -640(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -616(%rbp)
	movq	%rax, -1744(%rbp)
	movq	$0, -624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1720(%rbp), %rax
	movl	$72, %edi
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	%rax, -464(%rbp)
	movq	$0, -440(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -456(%rbp)
	leaq	-408(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -440(%rbp)
	movq	%rdx, -448(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -424(%rbp)
	movq	%rax, -1736(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1720(%rbp), %rax
	movl	$72, %edi
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	%rax, -272(%rbp)
	movq	$0, -248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -248(%rbp)
	movq	%rdx, -256(%rbp)
	xorl	%edx, %edx
	movq	%rax, -264(%rbp)
	movups	%xmm0, -232(%rbp)
	movq	$0, -240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1792(%rbp), %xmm1
	movaps	%xmm0, -1552(%rbp)
	movhps	-1776(%rbp), %xmm1
	movq	$0, -1536(%rbp)
	movaps	%xmm1, -1792(%rbp)
	call	_Znwm@PLT
	movdqa	-1792(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1552(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L175
	call	_ZdlPv@PLT
.L175:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1360(%rbp)
	jne	.L337
	cmpq	$0, -1168(%rbp)
	jne	.L338
.L182:
	cmpq	$0, -976(%rbp)
	jne	.L339
.L185:
	cmpq	$0, -784(%rbp)
	jne	.L340
.L188:
	cmpq	$0, -592(%rbp)
	jne	.L341
.L191:
	cmpq	$0, -400(%rbp)
	leaq	-272(%rbp), %rbx
	jne	.L342
.L194:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$4, 2(%rax)
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L197
	call	_ZdlPv@PLT
.L197:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L198
	call	_ZdlPv@PLT
.L198:
	movq	-256(%rbp), %rbx
	movq	-264(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L199
	.p2align 4,,10
	.p2align 3
.L203:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L200
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L203
.L201:
	movq	-264(%rbp), %r15
.L199:
	testq	%r15, %r15
	je	.L204
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L204:
	movq	-1736(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L205
	call	_ZdlPv@PLT
.L205:
	movq	-448(%rbp), %rbx
	movq	-456(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L206
	.p2align 4,,10
	.p2align 3
.L210:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L207
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L210
.L208:
	movq	-456(%rbp), %r15
.L206:
	testq	%r15, %r15
	je	.L211
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L211:
	movq	-1744(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L212
	call	_ZdlPv@PLT
.L212:
	movq	-640(%rbp), %rbx
	movq	-648(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L213
	.p2align 4,,10
	.p2align 3
.L217:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L214
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L217
.L215:
	movq	-648(%rbp), %r15
.L213:
	testq	%r15, %r15
	je	.L218
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L218:
	movq	-1752(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-816(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L219
	call	_ZdlPv@PLT
.L219:
	movq	-832(%rbp), %rbx
	movq	-840(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L220
	.p2align 4,,10
	.p2align 3
.L224:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L221
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L224
.L222:
	movq	-840(%rbp), %r15
.L220:
	testq	%r15, %r15
	je	.L225
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L225:
	movq	-1760(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1008(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L226
	call	_ZdlPv@PLT
.L226:
	movq	-1024(%rbp), %rbx
	movq	-1032(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L227
	.p2align 4,,10
	.p2align 3
.L231:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L228
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L231
.L229:
	movq	-1032(%rbp), %r15
.L227:
	testq	%r15, %r15
	je	.L232
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L232:
	movq	-1768(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L233
	call	_ZdlPv@PLT
.L233:
	movq	-1216(%rbp), %rbx
	movq	-1224(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L234
	.p2align 4,,10
	.p2align 3
.L238:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L235
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L238
.L236:
	movq	-1224(%rbp), %r15
.L234:
	testq	%r15, %r15
	je	.L239
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L239:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L240
	call	_ZdlPv@PLT
.L240:
	movq	-1408(%rbp), %rbx
	movq	-1416(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L241
	.p2align 4,,10
	.p2align 3
.L245:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L242
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L245
.L243:
	movq	-1416(%rbp), %r14
.L241:
	testq	%r14, %r14
	je	.L246
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L246:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L343
	addq	$1784, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L245
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L235:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L238
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L228:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L231
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L221:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L224
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L214:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L217
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L200:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L203
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L207:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L210
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L337:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r11d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r11w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L177
	call	_ZdlPv@PLT
.L177:
	movq	(%rbx), %rax
	movl	$22, %edx
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rsi
	leaq	-1712(%rbp), %rbx
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rcx, -1824(%rbp)
	movq	%rax, -1776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-1680(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rdi
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%rax, -1792(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1792(%rbp), %rcx
	movq	%r13, %r8
	movq	%rbx, %rdi
	movq	-1776(%rbp), %rdx
	movq	-1824(%rbp), %rsi
	call	_ZN2v88internal23RegExpBuiltinsAssembler29BranchIfFastRegExp_PermissiveENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10HeapObjectEEEPNS2_18CodeAssemblerLabelES9_@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -1672(%rbp)
	jne	.L344
	cmpq	$0, -1544(%rbp)
	jne	.L345
.L180:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1792(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1168(%rbp)
	je	.L182
.L338:
	movq	-1768(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1232(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r10d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r10w, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L183
	call	_ZdlPv@PLT
.L183:
	movq	(%rbx), %rax
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movdqu	(%rax), %xmm0
	movaps	%xmm1, -1552(%rbp)
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1792(%rbp)
	call	_Znwm@PLT
	movdqa	-1792(%rbp), %xmm0
	leaq	-848(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1552(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L184
	call	_ZdlPv@PLT
.L184:
	movq	-1752(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -976(%rbp)
	je	.L185
.L339:
	movq	-1760(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1040(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r9d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L186
	call	_ZdlPv@PLT
.L186:
	movq	(%rbx), %rax
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movdqu	(%rax), %xmm0
	movaps	%xmm1, -1552(%rbp)
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1792(%rbp)
	call	_Znwm@PLT
	movdqa	-1792(%rbp), %xmm0
	leaq	-656(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1552(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L187
	call	_ZdlPv@PLT
.L187:
	movq	-1744(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -784(%rbp)
	je	.L188
.L340:
	movq	-1752(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-848(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r8d
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L189
	call	_ZdlPv@PLT
.L189:
	movq	(%rbx), %rax
	movq	-1800(%rbp), %rdi
	movl	$1, %esi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -1792(%rbp)
	call	_ZN2v88internal42FromConstexpr6ATbool16ATconstexpr_bool_165EPNS0_8compiler18CodeAssemblerStateEb@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	$0, -1536(%rbp)
	movhps	-1792(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm2
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-464(%rbp), %rdi
	movq	%rax, -1552(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L190
	call	_ZdlPv@PLT
.L190:
	movq	-1736(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -592(%rbp)
	je	.L191
.L341:
	movq	-1744(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-656(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movl	$1799, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	2(%rax), %rdx
	movq	%rbx, %rdi
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L192
	call	_ZdlPv@PLT
.L192:
	movq	(%rbx), %rax
	movq	-1800(%rbp), %rdi
	xorl	%esi, %esi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -1792(%rbp)
	call	_ZN2v88internal42FromConstexpr6ATbool16ATconstexpr_bool_165EPNS0_8compiler18CodeAssemblerStateEb@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	$0, -1536(%rbp)
	movhps	-1792(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm3
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-464(%rbp), %rdi
	movq	%rax, -1552(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L193
	call	_ZdlPv@PLT
.L193:
	movq	-1736(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L342:
	movq	-1736(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-464(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$4, 2(%rax)
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L195
	call	_ZdlPv@PLT
.L195:
	movq	(%rbx), %rax
	movl	$20, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -1776(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -1792(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %edi
	movq	%rbx, -64(%rbp)
	movq	-1792(%rbp), %xmm0
	movq	$0, -1536(%rbp)
	leaq	-272(%rbp), %rbx
	movhps	-1776(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	-80(%rbp), %xmm4
	leaq	24(%rax), %rdx
	movq	%rax, -1552(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L196
	call	_ZdlPv@PLT
.L196:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L344:
	movq	-1792(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1776(%rbp), %rax
	movl	$24, %edi
	movq	-1824(%rbp), %xmm0
	movq	$0, -1696(%rbp)
	movq	%rax, %xmm7
	movq	%rax, -64(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1712(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm5
	movq	%rbx, %rsi
	leaq	24(%rax), %rdx
	leaq	-1232(%rbp), %rdi
	movq	%rax, -1712(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -1696(%rbp)
	movq	%rdx, -1704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L179
	call	_ZdlPv@PLT
.L179:
	movq	-1768(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1544(%rbp)
	je	.L180
.L345:
	movq	-1824(%rbp), %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movhps	-1776(%rbp), %xmm0
	movaps	%xmm0, -1824(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-1824(%rbp), %xmm0
	movl	$24, %edi
	movq	-1776(%rbp), %rax
	movq	$0, -1696(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1712(%rbp)
	movq	%rax, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm6
	movq	%rbx, %rsi
	leaq	24(%rax), %rdx
	leaq	-1040(%rbp), %rdi
	movq	%rax, -1712(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1696(%rbp)
	movq	%rdx, -1704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L181
	call	_ZdlPv@PLT
.L181:
	movq	-1760(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L180
.L343:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22423:
	.size	_ZN2v88internal26IsFastRegExpPermissive_327EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEE, .-_ZN2v88internal26IsFastRegExpPermissive_327EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEE
	.section	.text._ZN2v88internal14FlagGetter_328EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS0_8JSRegExp4FlagENS0_7int31_tEPKc,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal14FlagGetter_328EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS0_8JSRegExp4FlagENS0_7int31_tEPKc
	.type	_ZN2v88internal14FlagGetter_328EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS0_8JSRegExp4FlagENS0_7int31_tEPKc, @function
_ZN2v88internal14FlagGetter_328EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS0_8JSRegExp4FlagENS0_7int31_tEPKc:
.LFB22424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-2400(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2528(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-2568(%rbp), %r12
	pushq	%rbx
	subq	$2680, %rsp
	.cfi_offset 3, -56
	movq	%r9, -2720(%rbp)
	movq	%rsi, -2688(%rbp)
	movq	%rdx, -2704(%rbp)
	movl	%ecx, -2712(%rbp)
	movl	%r8d, -2708(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -2568(%rbp)
	movq	%rdi, -2400(%rbp)
	movl	$48, %edi
	movq	$0, -2392(%rbp)
	movq	$0, -2384(%rbp)
	movq	$0, -2376(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -2392(%rbp)
	leaq	-2344(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2376(%rbp)
	movq	%rdx, -2384(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2360(%rbp)
	movq	%rax, -2584(%rbp)
	movq	$0, -2368(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2568(%rbp), %rax
	movl	$96, %edi
	movq	$0, -2200(%rbp)
	movq	$0, -2192(%rbp)
	movq	%rax, -2208(%rbp)
	movq	$0, -2184(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -2200(%rbp)
	leaq	-2152(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2184(%rbp)
	movq	%rdx, -2192(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2168(%rbp)
	movq	%rax, -2672(%rbp)
	movq	$0, -2176(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2568(%rbp), %rax
	movl	$120, %edi
	movq	$0, -2008(%rbp)
	movq	$0, -2000(%rbp)
	movq	%rax, -2016(%rbp)
	movq	$0, -1992(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -2008(%rbp)
	leaq	-1960(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1992(%rbp)
	movq	%rdx, -2000(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1976(%rbp)
	movq	%rax, -2616(%rbp)
	movq	$0, -1984(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2568(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1816(%rbp)
	movq	$0, -1808(%rbp)
	movq	%rax, -1824(%rbp)
	movq	$0, -1800(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1816(%rbp)
	leaq	-1768(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1800(%rbp)
	movq	%rdx, -1808(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1784(%rbp)
	movq	%rax, -2624(%rbp)
	movq	$0, -1792(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2568(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1624(%rbp)
	movq	$0, -1616(%rbp)
	movq	%rax, -1632(%rbp)
	movq	$0, -1608(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1624(%rbp)
	leaq	-1576(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1608(%rbp)
	movq	%rdx, -1616(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1592(%rbp)
	movq	%rax, -2632(%rbp)
	movq	$0, -1600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2568(%rbp), %rax
	movl	$48, %edi
	movq	$0, -1432(%rbp)
	movq	$0, -1424(%rbp)
	movq	%rax, -1440(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1432(%rbp)
	leaq	-1384(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -2640(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2568(%rbp), %rax
	movl	$48, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -2648(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2568(%rbp), %rax
	movl	$48, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -2656(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2568(%rbp), %rax
	movl	$48, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -2664(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2568(%rbp), %rax
	movl	$48, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -2608(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2568(%rbp), %rax
	movl	$72, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -2600(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2568(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -280(%rbp)
	leaq	-232(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -2592(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-2688(%rbp), %xmm1
	movaps	%xmm0, -2528(%rbp)
	movhps	-2704(%rbp), %xmm1
	movq	$0, -2512(%rbp)
	movaps	%xmm1, -2688(%rbp)
	call	_Znwm@PLT
	movdqa	-2688(%rbp), %xmm1
	movq	%r14, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -2528(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L347
	call	_ZdlPv@PLT
.L347:
	movq	-2584(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2336(%rbp)
	jne	.L626
	cmpq	$0, -2144(%rbp)
	jne	.L627
.L353:
	cmpq	$0, -1952(%rbp)
	jne	.L628
.L356:
	cmpq	$0, -1760(%rbp)
	jne	.L629
.L360:
	cmpq	$0, -1568(%rbp)
	jne	.L630
.L363:
	cmpq	$0, -1376(%rbp)
	jne	.L631
.L367:
	cmpq	$0, -1184(%rbp)
	jne	.L632
.L369:
	cmpq	$0, -992(%rbp)
	jne	.L633
.L374:
	cmpq	$0, -800(%rbp)
	jne	.L634
.L377:
	cmpq	$0, -608(%rbp)
	jne	.L635
.L380:
	cmpq	$0, -416(%rbp)
	leaq	-288(%rbp), %r14
	jne	.L636
.L383:
	movq	-2592(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2512(%rbp)
	movaps	%xmm0, -2528(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -2528(%rbp)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2528(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L386
	call	_ZdlPv@PLT
.L386:
	movq	(%rbx), %rax
	movq	-2592(%rbp), %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L387
	call	_ZdlPv@PLT
.L387:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L388
	.p2align 4,,10
	.p2align 3
.L392:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L389
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L392
.L390:
	movq	-280(%rbp), %r14
.L388:
	testq	%r14, %r14
	je	.L393
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L393:
	movq	-2600(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L394
	call	_ZdlPv@PLT
.L394:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L395
	.p2align 4,,10
	.p2align 3
.L399:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L396
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L399
.L397:
	movq	-472(%rbp), %r14
.L395:
	testq	%r14, %r14
	je	.L400
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L400:
	movq	-2608(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L401
	call	_ZdlPv@PLT
.L401:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L402
	.p2align 4,,10
	.p2align 3
.L406:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L403
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L406
.L404:
	movq	-664(%rbp), %r14
.L402:
	testq	%r14, %r14
	je	.L407
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L407:
	movq	-2664(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L408
	call	_ZdlPv@PLT
.L408:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L409
	.p2align 4,,10
	.p2align 3
.L413:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L410
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L413
.L411:
	movq	-856(%rbp), %r14
.L409:
	testq	%r14, %r14
	je	.L414
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L414:
	movq	-2656(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L415
	call	_ZdlPv@PLT
.L415:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L416
	.p2align 4,,10
	.p2align 3
.L420:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L417
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L420
.L418:
	movq	-1048(%rbp), %r14
.L416:
	testq	%r14, %r14
	je	.L421
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L421:
	movq	-2648(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L422
	call	_ZdlPv@PLT
.L422:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L423
	.p2align 4,,10
	.p2align 3
.L427:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L424
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L427
.L425:
	movq	-1240(%rbp), %r14
.L423:
	testq	%r14, %r14
	je	.L428
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L428:
	movq	-2640(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L429
	call	_ZdlPv@PLT
.L429:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L430
	.p2align 4,,10
	.p2align 3
.L434:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L431
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L434
.L432:
	movq	-1432(%rbp), %r14
.L430:
	testq	%r14, %r14
	je	.L435
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L435:
	movq	-2632(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L436
	call	_ZdlPv@PLT
.L436:
	movq	-1616(%rbp), %rbx
	movq	-1624(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L437
	.p2align 4,,10
	.p2align 3
.L441:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L438
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L441
.L439:
	movq	-1624(%rbp), %r14
.L437:
	testq	%r14, %r14
	je	.L442
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L442:
	movq	-2624(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L443
	call	_ZdlPv@PLT
.L443:
	movq	-1808(%rbp), %rbx
	movq	-1816(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L444
	.p2align 4,,10
	.p2align 3
.L448:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L445
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L448
.L446:
	movq	-1816(%rbp), %r14
.L444:
	testq	%r14, %r14
	je	.L449
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L449:
	movq	-2616(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L450
	call	_ZdlPv@PLT
.L450:
	movq	-2000(%rbp), %rbx
	movq	-2008(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L451
	.p2align 4,,10
	.p2align 3
.L455:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L452
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L455
.L453:
	movq	-2008(%rbp), %r14
.L451:
	testq	%r14, %r14
	je	.L456
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L456:
	movq	-2672(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L457
	call	_ZdlPv@PLT
.L457:
	movq	-2192(%rbp), %rbx
	movq	-2200(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L458
	.p2align 4,,10
	.p2align 3
.L462:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L459
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L462
.L460:
	movq	-2200(%rbp), %r14
.L458:
	testq	%r14, %r14
	je	.L463
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L463:
	movq	-2584(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L464
	call	_ZdlPv@PLT
.L464:
	movq	-2384(%rbp), %rbx
	movq	-2392(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L465
	.p2align 4,,10
	.p2align 3
.L469:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L466
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L469
.L467:
	movq	-2392(%rbp), %r14
.L465:
	testq	%r14, %r14
	je	.L470
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L470:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L637
	addq	$2680, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L466:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L469
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L459:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L462
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L452:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L455
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L445:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L448
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L438:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L441
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L431:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L434
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L424:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L427
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L417:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L420
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L410:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L413
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L403:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L406
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L389:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L392
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L396:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L399
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L626:
	movq	-2584(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -2512(%rbp)
	movaps	%xmm0, -2528(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%r14, %rdi
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -2528(%rbp)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2528(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L349
	call	_ZdlPv@PLT
.L349:
	movq	(%rbx), %rax
	movl	$66, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %r14
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$67, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rdx
	movq	%r13, %rcx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal18Cast8JSRegExp_1467EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm5
	movq	%r14, %xmm2
	movq	%rbx, %xmm3
	punpcklqdq	%xmm2, %xmm2
	punpcklqdq	%xmm5, %xmm3
	pxor	%xmm0, %xmm0
	movq	%rax, -64(%rbp)
	movl	$40, %edi
	movaps	%xmm3, -96(%rbp)
	leaq	-2560(%rbp), %r14
	movaps	%xmm2, -2704(%rbp)
	movaps	%xmm3, -2688(%rbp)
	movaps	%xmm2, -80(%rbp)
	movaps	%xmm0, -2560(%rbp)
	movq	$0, -2544(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	leaq	40(%rax), %rdx
	leaq	-2016(%rbp), %rdi
	movq	%rax, -2560(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2544(%rbp)
	movq	%rdx, -2552(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2560(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L350
	call	_ZdlPv@PLT
.L350:
	movq	-2616(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2520(%rbp)
	jne	.L638
.L351:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2144(%rbp)
	je	.L353
.L627:
	movq	-2672(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2208(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2512(%rbp)
	movaps	%xmm0, -2528(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2528(%rbp)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2528(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L354
	call	_ZdlPv@PLT
.L354:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm5
	movaps	%xmm0, -2528(%rbp)
	movq	$0, -2512(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm5, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-1824(%rbp), %rdi
	movq	%rax, -2528(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L355
	call	_ZdlPv@PLT
.L355:
	movq	-2624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1952(%rbp)
	je	.L356
.L628:
	movq	-2616(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2016(%rbp), %rbx
	xorl	%r14d, %r14d
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2512(%rbp)
	movaps	%xmm0, -2528(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2528(%rbp)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2528(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L357
	call	_ZdlPv@PLT
.L357:
	movq	(%rbx), %rax
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	32(%rax), %rdx
	movq	(%rax), %xmm0
	testq	%rdx, %rdx
	movhps	8(%rax), %xmm0
	cmovne	%rdx, %r14
	movl	$68, %edx
	movaps	%xmm0, -2688(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movzbl	-2712(%rbp), %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23RegExpBuiltinsAssembler14FastFlagGetterENS0_8compiler5TNodeINS0_8JSRegExpEEENS4_4FlagE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler21SelectBooleanConstantENS0_8compiler11SloppyTNodeINS0_5BoolTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$24, %edi
	movq	%rbx, -80(%rbp)
	movdqa	-2688(%rbp), %xmm0
	movq	$0, -2512(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2528(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -2528(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L359
	call	_ZdlPv@PLT
.L359:
	movq	-2600(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1760(%rbp)
	je	.L360
.L629:
	movq	-2624(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1824(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2512(%rbp)
	movaps	%xmm0, -2528(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -2528(%rbp)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2528(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L361
	call	_ZdlPv@PLT
.L361:
	movq	(%rbx), %rax
	movl	$70, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r14
	movq	%rcx, -2688(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$67, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r14, -80(%rbp)
	movhps	-2688(%rbp), %xmm0
	movq	$0, -2512(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2528(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-1632(%rbp), %rdi
	movq	%rax, -2528(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L362
	call	_ZdlPv@PLT
.L362:
	movq	-2632(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1568(%rbp)
	je	.L363
.L630:
	movq	-2632(%rbp), %rsi
	movq	%r12, %rdi
	movl	$2055, %ebx
	leaq	-1632(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2512(%rbp)
	movaps	%xmm0, -2528(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%bx, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -2528(%rbp)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2528(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L364
	call	_ZdlPv@PLT
.L364:
	movq	(%rbx), %rax
	movl	$66, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, %r14
	movq	%rax, -2688(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$73, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23RegExpBuiltinsAssembler32IsReceiverInitialRegExpPrototypeENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15Word32BinaryNotENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm4
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movhps	-2688(%rbp), %xmm4
	movaps	%xmm0, -2528(%rbp)
	movaps	%xmm4, -2688(%rbp)
	movq	$0, -2512(%rbp)
	call	_Znwm@PLT
	movdqa	-2688(%rbp), %xmm6
	leaq	-1440(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -2528(%rbp)
	movups	%xmm6, (%rax)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L365
	call	_ZdlPv@PLT
.L365:
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	$0, -2512(%rbp)
	movaps	%xmm0, -2528(%rbp)
	call	_Znwm@PLT
	movdqa	-2688(%rbp), %xmm7
	leaq	-1248(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -2528(%rbp)
	movups	%xmm7, (%rax)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L366
	call	_ZdlPv@PLT
.L366:
	movq	-2648(%rbp), %rcx
	movq	-2640(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1376(%rbp)
	je	.L367
.L631:
	movq	-2640(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1440(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -2512(%rbp)
	movaps	%xmm0, -2528(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r11d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%r11w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -2528(%rbp)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2528(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L368
	call	_ZdlPv@PLT
.L368:
	movq	(%rbx), %rax
	movl	$74, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	movl	$156, %edx
	movq	%r14, %rsi
	movq	-2720(%rbp), %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -1184(%rbp)
	je	.L369
.L632:
	movq	-2648(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1248(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -2512(%rbp)
	movaps	%xmm0, -2528(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r10d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%r10w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -2528(%rbp)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2528(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L370
	call	_ZdlPv@PLT
.L370:
	movq	(%rbx), %rax
	movl	$76, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpl	$-1, -2708(%rbp)
	movq	%rbx, %xmm0
	je	.L371
	movq	%r14, %xmm4
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movq	$0, -2512(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm1, -2528(%rbp)
	movaps	%xmm0, -2688(%rbp)
	call	_Znwm@PLT
	movdqa	-2688(%rbp), %xmm0
	leaq	-1056(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -2528(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L372
	call	_ZdlPv@PLT
.L372:
	movq	-2656(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -992(%rbp)
	je	.L374
.L633:
	movq	-2656(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -2512(%rbp)
	movaps	%xmm0, -2528(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r9d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -2528(%rbp)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2528(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L375
	call	_ZdlPv@PLT
.L375:
	movq	(%rbx), %rax
	movl	$77, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -2688(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	-2708(%rbp), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	leaq	-96(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movl	$1, %r8d
	movl	$148, %esi
	movq	%r14, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$76, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movhps	-2688(%rbp), %xmm0
	movaps	%xmm1, -2528(%rbp)
	movaps	%xmm0, -2688(%rbp)
	movq	$0, -2512(%rbp)
	call	_Znwm@PLT
	movdqa	-2688(%rbp), %xmm0
	leaq	-672(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -2528(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L376
	call	_ZdlPv@PLT
.L376:
	movq	-2608(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L377
.L634:
	movq	-2664(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -2512(%rbp)
	movaps	%xmm0, -2528(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r8d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -2528(%rbp)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2528(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L378
	call	_ZdlPv@PLT
.L378:
	movq	(%rbx), %rax
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movdqu	(%rax), %xmm0
	movaps	%xmm1, -2528(%rbp)
	movq	$0, -2512(%rbp)
	movaps	%xmm0, -2688(%rbp)
	call	_Znwm@PLT
	movdqa	-2688(%rbp), %xmm0
	leaq	-672(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -2528(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L379
	call	_ZdlPv@PLT
.L379:
	movq	-2608(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -608(%rbp)
	je	.L380
.L635:
	movq	-2608(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -2512(%rbp)
	movaps	%xmm0, -2528(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	2(%rax), %rdx
	movq	%r14, %rdi
	movq	%rax, -2528(%rbp)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2528(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L381
	call	_ZdlPv@PLT
.L381:
	movq	(%rbx), %rax
	movl	$79, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %xmm4
	movq	%rbx, %xmm0
	movl	$24, %edi
	punpcklqdq	%xmm4, %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2528(%rbp)
	movq	$0, -2512(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -2528(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L382
	call	_ZdlPv@PLT
.L382:
	movq	-2600(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L636:
	movq	-2600(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2512(%rbp)
	movaps	%xmm0, -2528(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%r14, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$8, 2(%rax)
	movq	%rax, -2528(%rbp)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2528(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L384
	call	_ZdlPv@PLT
.L384:
	movq	(%rbx), %rax
	movl	$63, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %r15
	movq	16(%rax), %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %xmm7
	movq	%rbx, %xmm0
	movl	$24, %edi
	punpcklqdq	%xmm7, %xmm0
	movq	%r14, -80(%rbp)
	leaq	-288(%rbp), %r14
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2528(%rbp)
	movq	$0, -2512(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm5
	leaq	24(%rax), %rdx
	movq	%rax, -2528(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L385
	call	_ZdlPv@PLT
.L385:
	movq	-2592(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L371:
	movq	%r14, %xmm6
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movq	$0, -2512(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm1, -2528(%rbp)
	movaps	%xmm0, -2688(%rbp)
	call	_Znwm@PLT
	movdqa	-2688(%rbp), %xmm0
	leaq	-864(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -2528(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -2512(%rbp)
	movq	%rdx, -2520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L373
	call	_ZdlPv@PLT
.L373:
	movq	-2664(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L638:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-2688(%rbp), %xmm7
	movdqa	-2704(%rbp), %xmm5
	movaps	%xmm0, -2560(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movq	$0, -2544(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm7
	movq	%r14, %rsi
	leaq	32(%rax), %rdx
	leaq	-2208(%rbp), %rdi
	movq	%rax, -2560(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2544(%rbp)
	movq	%rdx, -2552(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2560(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L352
	call	_ZdlPv@PLT
.L352:
	movq	-2672(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L351
.L637:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22424:
	.size	_ZN2v88internal14FlagGetter_328EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS0_8JSRegExp4FlagENS0_7int31_tEPKc, .-_ZN2v88internal14FlagGetter_328EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS0_8JSRegExp4FlagENS0_7int31_tEPKc
	.section	.rodata._ZN2v88internal36RegExpPrototypeGlobalGetterAssembler39GenerateRegExpPrototypeGlobalGetterImplEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"RegExp.prototype.global"
	.section	.text._ZN2v88internal36RegExpPrototypeGlobalGetterAssembler39GenerateRegExpPrototypeGlobalGetterImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal36RegExpPrototypeGlobalGetterAssembler39GenerateRegExpPrototypeGlobalGetterImplEv
	.type	_ZN2v88internal36RegExpPrototypeGlobalGetterAssembler39GenerateRegExpPrototypeGlobalGetterImplEv, @function
_ZN2v88internal36RegExpPrototypeGlobalGetterAssembler39GenerateRegExpPrototypeGlobalGetterImplEv:
.LFB22445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-272(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-184(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-240(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	-280(%rbp), %r12
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -304(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-312(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-304(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L640
	call	_ZdlPv@PLT
.L640:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L670
.L641:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L645
	call	_ZdlPv@PLT
.L645:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L646
	.p2align 4,,10
	.p2align 3
.L650:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L647
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L650
.L648:
	movq	-232(%rbp), %r13
.L646:
	testq	%r13, %r13
	je	.L651
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L651:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L671
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L647:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L650
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L670:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L642
	call	_ZdlPv@PLT
.L642:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rsi
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	testq	%rdx, %rdx
	movq	%rdx, %rbx
	cmove	-312(%rbp), %rbx
	testq	%rax, %rax
	cmove	-304(%rbp), %rax
	movl	$87, %edx
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rdi
	movl	$86, %edx
	leaq	.LC0(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %ecx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	-304(%rbp), %rdx
	leaq	.LC1(%rip), %r9
	movl	$31, %r8d
	call	_ZN2v88internal14FlagGetter_328EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS0_8JSRegExp4FlagENS0_7int31_tEPKc
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L641
.L671:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22445:
	.size	_ZN2v88internal36RegExpPrototypeGlobalGetterAssembler39GenerateRegExpPrototypeGlobalGetterImplEv, .-_ZN2v88internal36RegExpPrototypeGlobalGetterAssembler39GenerateRegExpPrototypeGlobalGetterImplEv
	.section	.rodata._ZN2v88internal8Builtins36Generate_RegExpPrototypeGlobalGetterEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/regexp-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins36Generate_RegExpPrototypeGlobalGetterEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"RegExpPrototypeGlobalGetter"
	.section	.text._ZN2v88internal8Builtins36Generate_RegExpPrototypeGlobalGetterEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins36Generate_RegExpPrototypeGlobalGetterEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins36Generate_RegExpPrototypeGlobalGetterEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins36Generate_RegExpPrototypeGlobalGetterEPNS0_8compiler18CodeAssemblerStateE:
.LFB22441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$460, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$874, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L676
.L673:
	movq	%r13, %rdi
	call	_ZN2v88internal36RegExpPrototypeGlobalGetterAssembler39GenerateRegExpPrototypeGlobalGetterImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L677
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L676:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L673
.L677:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22441:
	.size	_ZN2v88internal8Builtins36Generate_RegExpPrototypeGlobalGetterEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins36Generate_RegExpPrototypeGlobalGetterEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal40RegExpPrototypeIgnoreCaseGetterAssembler43GenerateRegExpPrototypeIgnoreCaseGetterImplEv.str1.1,"aMS",@progbits,1
.LC4:
	.string	"RegExp.prototype.ignoreCase"
	.section	.text._ZN2v88internal40RegExpPrototypeIgnoreCaseGetterAssembler43GenerateRegExpPrototypeIgnoreCaseGetterImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal40RegExpPrototypeIgnoreCaseGetterAssembler43GenerateRegExpPrototypeIgnoreCaseGetterImplEv
	.type	_ZN2v88internal40RegExpPrototypeIgnoreCaseGetterAssembler43GenerateRegExpPrototypeIgnoreCaseGetterImplEv, @function
_ZN2v88internal40RegExpPrototypeIgnoreCaseGetterAssembler43GenerateRegExpPrototypeIgnoreCaseGetterImplEv:
.LFB22454:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-272(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-184(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-240(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	-280(%rbp), %r12
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -304(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-312(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-304(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L679
	call	_ZdlPv@PLT
.L679:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L709
.L680:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L684
	call	_ZdlPv@PLT
.L684:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L685
	.p2align 4,,10
	.p2align 3
.L689:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L686
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L689
.L687:
	movq	-232(%rbp), %r13
.L685:
	testq	%r13, %r13
	je	.L690
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L690:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L710
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L686:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L689
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L709:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L681
	call	_ZdlPv@PLT
.L681:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rsi
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	testq	%rdx, %rdx
	movq	%rdx, %rbx
	cmove	-312(%rbp), %rbx
	testq	%rax, %rax
	cmove	-304(%rbp), %rax
	movl	$96, %edx
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rdi
	movl	$95, %edx
	leaq	.LC0(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$2, %ecx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	-304(%rbp), %rdx
	leaq	.LC4(%rip), %r9
	movl	$31, %r8d
	call	_ZN2v88internal14FlagGetter_328EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS0_8JSRegExp4FlagENS0_7int31_tEPKc
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L680
.L710:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22454:
	.size	_ZN2v88internal40RegExpPrototypeIgnoreCaseGetterAssembler43GenerateRegExpPrototypeIgnoreCaseGetterImplEv, .-_ZN2v88internal40RegExpPrototypeIgnoreCaseGetterAssembler43GenerateRegExpPrototypeIgnoreCaseGetterImplEv
	.section	.rodata._ZN2v88internal8Builtins40Generate_RegExpPrototypeIgnoreCaseGetterEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"RegExpPrototypeIgnoreCaseGetter"
	.section	.text._ZN2v88internal8Builtins40Generate_RegExpPrototypeIgnoreCaseGetterEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins40Generate_RegExpPrototypeIgnoreCaseGetterEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins40Generate_RegExpPrototypeIgnoreCaseGetterEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins40Generate_RegExpPrototypeIgnoreCaseGetterEPNS0_8compiler18CodeAssemblerStateE:
.LFB22450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$482, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$875, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L715
.L712:
	movq	%r13, %rdi
	call	_ZN2v88internal40RegExpPrototypeIgnoreCaseGetterAssembler43GenerateRegExpPrototypeIgnoreCaseGetterImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L716
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L715:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L712
.L716:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22450:
	.size	_ZN2v88internal8Builtins40Generate_RegExpPrototypeIgnoreCaseGetterEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins40Generate_RegExpPrototypeIgnoreCaseGetterEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal39RegExpPrototypeMultilineGetterAssembler42GenerateRegExpPrototypeMultilineGetterImplEv.str1.1,"aMS",@progbits,1
.LC6:
	.string	"RegExp.prototype.multiline"
	.section	.text._ZN2v88internal39RegExpPrototypeMultilineGetterAssembler42GenerateRegExpPrototypeMultilineGetterImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal39RegExpPrototypeMultilineGetterAssembler42GenerateRegExpPrototypeMultilineGetterImplEv
	.type	_ZN2v88internal39RegExpPrototypeMultilineGetterAssembler42GenerateRegExpPrototypeMultilineGetterImplEv, @function
_ZN2v88internal39RegExpPrototypeMultilineGetterAssembler42GenerateRegExpPrototypeMultilineGetterImplEv:
.LFB22463:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-272(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-184(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-240(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	-280(%rbp), %r12
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -304(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-312(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-304(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L718
	call	_ZdlPv@PLT
.L718:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L748
.L719:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L723
	call	_ZdlPv@PLT
.L723:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L724
	.p2align 4,,10
	.p2align 3
.L728:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L725
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L728
.L726:
	movq	-232(%rbp), %r13
.L724:
	testq	%r13, %r13
	je	.L729
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L729:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L749
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L725:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L728
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L748:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L720
	call	_ZdlPv@PLT
.L720:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rsi
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	testq	%rdx, %rdx
	movq	%rdx, %rbx
	cmove	-312(%rbp), %rbx
	testq	%rax, %rax
	cmove	-304(%rbp), %rax
	movl	$105, %edx
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rdi
	movl	$104, %edx
	leaq	.LC0(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$4, %ecx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	-304(%rbp), %rdx
	leaq	.LC6(%rip), %r9
	movl	$31, %r8d
	call	_ZN2v88internal14FlagGetter_328EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS0_8JSRegExp4FlagENS0_7int31_tEPKc
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L719
.L749:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22463:
	.size	_ZN2v88internal39RegExpPrototypeMultilineGetterAssembler42GenerateRegExpPrototypeMultilineGetterImplEv, .-_ZN2v88internal39RegExpPrototypeMultilineGetterAssembler42GenerateRegExpPrototypeMultilineGetterImplEv
	.section	.rodata._ZN2v88internal8Builtins39Generate_RegExpPrototypeMultilineGetterEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"RegExpPrototypeMultilineGetter"
	.section	.text._ZN2v88internal8Builtins39Generate_RegExpPrototypeMultilineGetterEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins39Generate_RegExpPrototypeMultilineGetterEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins39Generate_RegExpPrototypeMultilineGetterEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins39Generate_RegExpPrototypeMultilineGetterEPNS0_8compiler18CodeAssemblerStateE:
.LFB22459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$504, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$876, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L754
.L751:
	movq	%r13, %rdi
	call	_ZN2v88internal39RegExpPrototypeMultilineGetterAssembler42GenerateRegExpPrototypeMultilineGetterImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L755
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L754:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L751
.L755:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22459:
	.size	_ZN2v88internal8Builtins39Generate_RegExpPrototypeMultilineGetterEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins39Generate_RegExpPrototypeMultilineGetterEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal36RegExpPrototypeDotAllGetterAssembler39GenerateRegExpPrototypeDotAllGetterImplEv.str1.1,"aMS",@progbits,1
.LC8:
	.string	"RegExp.prototype.dotAll"
	.section	.text._ZN2v88internal36RegExpPrototypeDotAllGetterAssembler39GenerateRegExpPrototypeDotAllGetterImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal36RegExpPrototypeDotAllGetterAssembler39GenerateRegExpPrototypeDotAllGetterImplEv
	.type	_ZN2v88internal36RegExpPrototypeDotAllGetterAssembler39GenerateRegExpPrototypeDotAllGetterImplEv, @function
_ZN2v88internal36RegExpPrototypeDotAllGetterAssembler39GenerateRegExpPrototypeDotAllGetterImplEv:
.LFB22472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-272(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-184(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-240(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	-280(%rbp), %r12
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -304(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-312(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-304(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L757
	call	_ZdlPv@PLT
.L757:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L787
.L758:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L762
	call	_ZdlPv@PLT
.L762:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L763
	.p2align 4,,10
	.p2align 3
.L767:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L764
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L767
.L765:
	movq	-232(%rbp), %r13
.L763:
	testq	%r13, %r13
	je	.L768
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L768:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L788
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L764:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L767
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L787:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L759
	call	_ZdlPv@PLT
.L759:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rsi
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	testq	%rdx, %rdx
	movq	%rdx, %rbx
	cmove	-312(%rbp), %rbx
	testq	%rax, %rax
	cmove	-304(%rbp), %rax
	movl	$113, %edx
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$32, %ecx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	-304(%rbp), %rdx
	leaq	.LC8(%rip), %r9
	movl	$-1, %r8d
	call	_ZN2v88internal14FlagGetter_328EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS0_8JSRegExp4FlagENS0_7int31_tEPKc
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L758
.L788:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22472:
	.size	_ZN2v88internal36RegExpPrototypeDotAllGetterAssembler39GenerateRegExpPrototypeDotAllGetterImplEv, .-_ZN2v88internal36RegExpPrototypeDotAllGetterAssembler39GenerateRegExpPrototypeDotAllGetterImplEv
	.section	.rodata._ZN2v88internal8Builtins36Generate_RegExpPrototypeDotAllGetterEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"RegExpPrototypeDotAllGetter"
	.section	.text._ZN2v88internal8Builtins36Generate_RegExpPrototypeDotAllGetterEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins36Generate_RegExpPrototypeDotAllGetterEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins36Generate_RegExpPrototypeDotAllGetterEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins36Generate_RegExpPrototypeDotAllGetterEPNS0_8compiler18CodeAssemblerStateE:
.LFB22468:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$526, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$877, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L793
.L790:
	movq	%r13, %rdi
	call	_ZN2v88internal36RegExpPrototypeDotAllGetterAssembler39GenerateRegExpPrototypeDotAllGetterImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L794
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L793:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L790
.L794:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22468:
	.size	_ZN2v88internal8Builtins36Generate_RegExpPrototypeDotAllGetterEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins36Generate_RegExpPrototypeDotAllGetterEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal36RegExpPrototypeStickyGetterAssembler39GenerateRegExpPrototypeStickyGetterImplEv.str1.1,"aMS",@progbits,1
.LC10:
	.string	"RegExp.prototype.sticky"
	.section	.text._ZN2v88internal36RegExpPrototypeStickyGetterAssembler39GenerateRegExpPrototypeStickyGetterImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal36RegExpPrototypeStickyGetterAssembler39GenerateRegExpPrototypeStickyGetterImplEv
	.type	_ZN2v88internal36RegExpPrototypeStickyGetterAssembler39GenerateRegExpPrototypeStickyGetterImplEv, @function
_ZN2v88internal36RegExpPrototypeStickyGetterAssembler39GenerateRegExpPrototypeStickyGetterImplEv:
.LFB22481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-272(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-184(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-240(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	-280(%rbp), %r12
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -304(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-312(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-304(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L796
	call	_ZdlPv@PLT
.L796:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L826
.L797:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L801
	call	_ZdlPv@PLT
.L801:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L802
	.p2align 4,,10
	.p2align 3
.L806:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L803
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L806
.L804:
	movq	-232(%rbp), %r13
.L802:
	testq	%r13, %r13
	je	.L807
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L807:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L827
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L803:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L806
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L826:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L798
	call	_ZdlPv@PLT
.L798:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rsi
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	testq	%rdx, %rdx
	movq	%rdx, %rbx
	cmove	-312(%rbp), %rbx
	testq	%rax, %rax
	cmove	-304(%rbp), %rax
	movl	$121, %edx
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rdi
	movl	$120, %edx
	leaq	.LC0(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$8, %ecx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	-304(%rbp), %rdx
	leaq	.LC10(%rip), %r9
	movl	$11, %r8d
	call	_ZN2v88internal14FlagGetter_328EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS0_8JSRegExp4FlagENS0_7int31_tEPKc
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L797
.L827:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22481:
	.size	_ZN2v88internal36RegExpPrototypeStickyGetterAssembler39GenerateRegExpPrototypeStickyGetterImplEv, .-_ZN2v88internal36RegExpPrototypeStickyGetterAssembler39GenerateRegExpPrototypeStickyGetterImplEv
	.section	.rodata._ZN2v88internal8Builtins36Generate_RegExpPrototypeStickyGetterEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"RegExpPrototypeStickyGetter"
	.section	.text._ZN2v88internal8Builtins36Generate_RegExpPrototypeStickyGetterEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins36Generate_RegExpPrototypeStickyGetterEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins36Generate_RegExpPrototypeStickyGetterEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins36Generate_RegExpPrototypeStickyGetterEPNS0_8compiler18CodeAssemblerStateE:
.LFB22477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$547, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$878, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L832
.L829:
	movq	%r13, %rdi
	call	_ZN2v88internal36RegExpPrototypeStickyGetterAssembler39GenerateRegExpPrototypeStickyGetterImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L833
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L832:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L829
.L833:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22477:
	.size	_ZN2v88internal8Builtins36Generate_RegExpPrototypeStickyGetterEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins36Generate_RegExpPrototypeStickyGetterEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal37RegExpPrototypeUnicodeGetterAssembler40GenerateRegExpPrototypeUnicodeGetterImplEv.str1.1,"aMS",@progbits,1
.LC12:
	.string	"RegExp.prototype.unicode"
	.section	.text._ZN2v88internal37RegExpPrototypeUnicodeGetterAssembler40GenerateRegExpPrototypeUnicodeGetterImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal37RegExpPrototypeUnicodeGetterAssembler40GenerateRegExpPrototypeUnicodeGetterImplEv
	.type	_ZN2v88internal37RegExpPrototypeUnicodeGetterAssembler40GenerateRegExpPrototypeUnicodeGetterImplEv, @function
_ZN2v88internal37RegExpPrototypeUnicodeGetterAssembler40GenerateRegExpPrototypeUnicodeGetterImplEv:
.LFB22490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-272(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-184(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-240(%rbp), %rbx
	subq	$280, %rsp
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	-280(%rbp), %r12
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	%rax, -304(%rbp)
	movq	-280(%rbp), %rax
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-312(%rbp), %xmm1
	movaps	%xmm0, -272(%rbp)
	movhps	-304(%rbp), %xmm1
	movq	$0, -256(%rbp)
	movaps	%xmm1, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -272(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L835
	call	_ZdlPv@PLT
.L835:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -176(%rbp)
	jne	.L865
.L836:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L840
	call	_ZdlPv@PLT
.L840:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L841
	.p2align 4,,10
	.p2align 3
.L845:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L842
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L845
.L843:
	movq	-232(%rbp), %r13
.L841:
	testq	%r13, %r13
	je	.L846
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L846:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L866
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L842:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L845
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L865:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -272(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L837
	call	_ZdlPv@PLT
.L837:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rsi
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	testq	%rdx, %rdx
	movq	%rdx, %rbx
	cmove	-312(%rbp), %rbx
	testq	%rax, %rax
	cmove	-304(%rbp), %rax
	movl	$130, %edx
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rdi
	movl	$129, %edx
	leaq	.LC0(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %ecx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	-304(%rbp), %rdx
	leaq	.LC12(%rip), %r9
	movl	$13, %r8d
	call	_ZN2v88internal14FlagGetter_328EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS0_8JSRegExp4FlagENS0_7int31_tEPKc
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L836
.L866:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22490:
	.size	_ZN2v88internal37RegExpPrototypeUnicodeGetterAssembler40GenerateRegExpPrototypeUnicodeGetterImplEv, .-_ZN2v88internal37RegExpPrototypeUnicodeGetterAssembler40GenerateRegExpPrototypeUnicodeGetterImplEv
	.section	.rodata._ZN2v88internal8Builtins37Generate_RegExpPrototypeUnicodeGetterEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"RegExpPrototypeUnicodeGetter"
	.section	.text._ZN2v88internal8Builtins37Generate_RegExpPrototypeUnicodeGetterEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins37Generate_RegExpPrototypeUnicodeGetterEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins37Generate_RegExpPrototypeUnicodeGetterEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins37Generate_RegExpPrototypeUnicodeGetterEPNS0_8compiler18CodeAssemblerStateE:
.LFB22486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$569, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$879, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L871
.L868:
	movq	%r13, %rdi
	call	_ZN2v88internal37RegExpPrototypeUnicodeGetterAssembler40GenerateRegExpPrototypeUnicodeGetterImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L872
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L871:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L868
.L872:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22486:
	.size	_ZN2v88internal8Builtins37Generate_RegExpPrototypeUnicodeGetterEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins37Generate_RegExpPrototypeUnicodeGetterEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal19FastFlagsGetter_329EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19FastFlagsGetter_329EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEE
	.type	_ZN2v88internal19FastFlagsGetter_329EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEE, @function
_ZN2v88internal19FastFlagsGetter_329EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEE:
.LFB22491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-600(%rbp), %r14
	leaq	-216(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-688(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-696(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$712, %rsp
	movq	%rsi, -736(%rbp)
	movq	%rdx, -744(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -696(%rbp)
	movq	%rdi, -656(%rbp)
	movl	$48, %edi
	movq	$0, -648(%rbp)
	movq	$0, -640(%rbp)
	movq	$0, -632(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -648(%rbp)
	leaq	-656(%rbp), %rax
	movq	%rdx, -632(%rbp)
	movq	%rdx, -640(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -616(%rbp)
	movq	%rax, -720(%rbp)
	movq	$0, -624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	%rax, -464(%rbp)
	movq	$0, -440(%rbp)
	call	_Znwm@PLT
	leaq	-408(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -440(%rbp)
	movq	%rdx, -448(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -712(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -424(%rbp)
	movq	%rax, -456(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	%rax, -272(%rbp)
	movq	$0, -248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -248(%rbp)
	movq	%rdx, -256(%rbp)
	xorl	%edx, %edx
	movq	%rax, -264(%rbp)
	movups	%xmm0, -232(%rbp)
	movq	$0, -240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-736(%rbp), %xmm1
	movaps	%xmm0, -688(%rbp)
	movhps	-744(%rbp), %xmm1
	movq	$0, -672(%rbp)
	movaps	%xmm1, -736(%rbp)
	call	_Znwm@PLT
	movdqa	-736(%rbp), %xmm1
	movq	-720(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -688(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L874
	call	_ZdlPv@PLT
.L874:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -592(%rbp)
	jne	.L942
.L875:
	cmpq	$0, -400(%rbp)
	leaq	-272(%rbp), %rbx
	jne	.L943
.L878:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L881
	call	_ZdlPv@PLT
.L881:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L882
	call	_ZdlPv@PLT
.L882:
	movq	-256(%rbp), %rbx
	movq	-264(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L883
	.p2align 4,,10
	.p2align 3
.L887:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L884
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L887
.L885:
	movq	-264(%rbp), %r15
.L883:
	testq	%r15, %r15
	je	.L888
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L888:
	movq	-712(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L889
	call	_ZdlPv@PLT
.L889:
	movq	-448(%rbp), %rbx
	movq	-456(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L890
	.p2align 4,,10
	.p2align 3
.L894:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L891
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L894
.L892:
	movq	-456(%rbp), %r15
.L890:
	testq	%r15, %r15
	je	.L895
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L895:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L896
	call	_ZdlPv@PLT
.L896:
	movq	-640(%rbp), %rbx
	movq	-648(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L897
	.p2align 4,,10
	.p2align 3
.L901:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L898
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L901
.L899:
	movq	-648(%rbp), %r14
.L897:
	testq	%r14, %r14
	je	.L902
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L902:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L944
	addq	$712, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L898:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L901
	jmp	.L899
	.p2align 4,,10
	.p2align 3
.L884:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L887
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L891:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L894
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L942:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$1799, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	movq	-720(%rbp), %rdi
	leaq	2(%rax), %rdx
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L876
	movq	%rax, -720(%rbp)
	call	_ZdlPv@PLT
	movq	-720(%rbp), %rax
.L876:
	movq	(%rax), %rax
	movl	$140, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rcx, -720(%rbp)
	movq	%rax, -736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-736(%rbp), %rdx
	movl	$1, %ecx
	movq	%r13, %rdi
	movq	-720(%rbp), %rsi
	call	_ZN2v88internal23RegExpBuiltinsAssembler11FlagsGetterENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEEb@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$24, %edi
	movq	%rbx, -64(%rbp)
	movq	-720(%rbp), %xmm0
	movq	$0, -672(%rbp)
	movhps	-736(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm2
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-464(%rbp), %rdi
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L877
	call	_ZdlPv@PLT
.L877:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L875
	.p2align 4,,10
	.p2align 3
.L943:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-464(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$7, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L879
	call	_ZdlPv@PLT
.L879:
	movq	(%rbx), %rax
	movl	$138, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -736(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -720(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %edi
	movq	%rbx, -64(%rbp)
	movq	-720(%rbp), %xmm0
	movq	$0, -672(%rbp)
	leaq	-272(%rbp), %rbx
	movhps	-736(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	-80(%rbp), %xmm3
	leaq	24(%rax), %rdx
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L880
	call	_ZdlPv@PLT
.L880:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L878
.L944:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22491:
	.size	_ZN2v88internal19FastFlagsGetter_329EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEE, .-_ZN2v88internal19FastFlagsGetter_329EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEE
	.section	.text._ZN2v88internal19SlowFlagsGetter_330EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19SlowFlagsGetter_330EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal19SlowFlagsGetter_330EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal19SlowFlagsGetter_330EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE:
.LFB22498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-600(%rbp), %r14
	leaq	-216(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-688(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-696(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$712, %rsp
	movq	%rsi, -736(%rbp)
	movq	%rdx, -744(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -696(%rbp)
	movq	%rdi, -656(%rbp)
	movl	$48, %edi
	movq	$0, -648(%rbp)
	movq	$0, -640(%rbp)
	movq	$0, -632(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -648(%rbp)
	leaq	-656(%rbp), %rax
	movq	%rdx, -632(%rbp)
	movq	%rdx, -640(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -616(%rbp)
	movq	%rax, -720(%rbp)
	movq	$0, -624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	%rax, -464(%rbp)
	movq	$0, -440(%rbp)
	call	_Znwm@PLT
	leaq	-408(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -440(%rbp)
	movq	%rdx, -448(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -712(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -424(%rbp)
	movq	%rax, -456(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	%rax, -272(%rbp)
	movq	$0, -248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -248(%rbp)
	movq	%rdx, -256(%rbp)
	xorl	%edx, %edx
	movq	%rax, -264(%rbp)
	movups	%xmm0, -232(%rbp)
	movq	$0, -240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-736(%rbp), %xmm1
	movaps	%xmm0, -688(%rbp)
	movhps	-744(%rbp), %xmm1
	movq	$0, -672(%rbp)
	movaps	%xmm1, -736(%rbp)
	call	_Znwm@PLT
	movdqa	-736(%rbp), %xmm1
	movq	-720(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -688(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L946
	call	_ZdlPv@PLT
.L946:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -592(%rbp)
	jne	.L1014
.L947:
	cmpq	$0, -400(%rbp)
	leaq	-272(%rbp), %rbx
	jne	.L1015
.L950:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L953
	call	_ZdlPv@PLT
.L953:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L954
	call	_ZdlPv@PLT
.L954:
	movq	-256(%rbp), %rbx
	movq	-264(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L955
	.p2align 4,,10
	.p2align 3
.L959:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L956
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L959
.L957:
	movq	-264(%rbp), %r15
.L955:
	testq	%r15, %r15
	je	.L960
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L960:
	movq	-712(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L961
	call	_ZdlPv@PLT
.L961:
	movq	-448(%rbp), %rbx
	movq	-456(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L962
	.p2align 4,,10
	.p2align 3
.L966:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L963
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L966
.L964:
	movq	-456(%rbp), %r15
.L962:
	testq	%r15, %r15
	je	.L967
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L967:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L968
	call	_ZdlPv@PLT
.L968:
	movq	-640(%rbp), %rbx
	movq	-648(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L969
	.p2align 4,,10
	.p2align 3
.L973:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L970
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L973
.L971:
	movq	-648(%rbp), %r14
.L969:
	testq	%r14, %r14
	je	.L974
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L974:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1016
	addq	$712, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L970:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L973
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L956:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L959
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L963:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L966
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L1014:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	movq	-720(%rbp), %rdi
	leaq	2(%rax), %rdx
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L948
	movq	%rax, -720(%rbp)
	call	_ZdlPv@PLT
	movq	-720(%rbp), %rax
.L948:
	movq	(%rax), %rax
	movl	$145, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rcx, -720(%rbp)
	movq	%rax, -736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-736(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	-720(%rbp), %rsi
	call	_ZN2v88internal23RegExpBuiltinsAssembler11FlagsGetterENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEEb@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$24, %edi
	movq	%rbx, -64(%rbp)
	movq	-720(%rbp), %xmm0
	movq	$0, -672(%rbp)
	movhps	-736(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm2
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-464(%rbp), %rdi
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L949
	call	_ZdlPv@PLT
.L949:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L1015:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-464(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$7, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L951
	call	_ZdlPv@PLT
.L951:
	movq	(%rbx), %rax
	movl	$143, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -736(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -720(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %edi
	movq	%rbx, -64(%rbp)
	movq	-720(%rbp), %xmm0
	movq	$0, -672(%rbp)
	leaq	-272(%rbp), %rbx
	movhps	-736(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	-80(%rbp), %xmm3
	leaq	24(%rax), %rdx
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L952
	call	_ZdlPv@PLT
.L952:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L950
.L1016:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22498:
	.size	_ZN2v88internal19SlowFlagsGetter_330EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal19SlowFlagsGetter_330EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.section	.rodata._ZN2v88internal25Cast14ATFastJSRegExp_1469EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"../../deps/v8/../../deps/v8/src/builtins/base.tq"
	.section	.text._ZN2v88internal25Cast14ATFastJSRegExp_1469EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Cast14ATFastJSRegExp_1469EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal25Cast14ATFastJSRegExp_1469EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal25Cast14ATFastJSRegExp_1469EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE:
.LFB22517:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	leaq	-1632(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1760(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1800(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1864, %rsp
	movq	%rsi, -1888(%rbp)
	movq	%rdx, -1904(%rbp)
	movq	%rcx, -1872(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1800(%rbp)
	movq	%rdi, -1632(%rbp)
	movl	$48, %edi
	movq	$0, -1624(%rbp)
	movq	$0, -1616(%rbp)
	movq	$0, -1608(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1624(%rbp)
	leaq	-1576(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1608(%rbp)
	movq	%rdx, -1616(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1592(%rbp)
	movq	%rax, -1816(%rbp)
	movq	$0, -1600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1432(%rbp)
	movq	$0, -1424(%rbp)
	movq	%rax, -1440(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1432(%rbp)
	leaq	-1384(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -1832(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1864(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$96, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1840(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1848(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	$0, -472(%rbp)
	movq	%rax, -480(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1824(%rbp)
	movq	$0, -464(%rbp)
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	$0, -440(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1888(%rbp), %xmm1
	movaps	%xmm0, -1760(%rbp)
	movhps	-1904(%rbp), %xmm1
	movq	$0, -1744(%rbp)
	movaps	%xmm1, -1888(%rbp)
	call	_Znwm@PLT
	movdqa	-1888(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1018
	call	_ZdlPv@PLT
.L1018:
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1568(%rbp)
	jne	.L1202
	cmpq	$0, -1376(%rbp)
	jne	.L1203
.L1024:
	cmpq	$0, -1184(%rbp)
	jne	.L1204
.L1027:
	cmpq	$0, -992(%rbp)
	jne	.L1205
.L1032:
	cmpq	$0, -800(%rbp)
	jne	.L1206
.L1035:
	cmpq	$0, -608(%rbp)
	leaq	-288(%rbp), %r15
	jne	.L1207
	cmpq	$0, -416(%rbp)
	jne	.L1208
.L1041:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1043
	call	_ZdlPv@PLT
.L1043:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1044
	call	_ZdlPv@PLT
.L1044:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1045
	.p2align 4,,10
	.p2align 3
.L1049:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1046
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1049
.L1047:
	movq	-280(%rbp), %r14
.L1045:
	testq	%r14, %r14
	je	.L1050
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1050:
	movq	-1824(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1051
	call	_ZdlPv@PLT
.L1051:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1052
	.p2align 4,,10
	.p2align 3
.L1056:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1053
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1056
.L1054:
	movq	-472(%rbp), %r14
.L1052:
	testq	%r14, %r14
	je	.L1057
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1057:
	movq	-1848(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1058
	call	_ZdlPv@PLT
.L1058:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1059
	.p2align 4,,10
	.p2align 3
.L1063:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1060
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1063
.L1061:
	movq	-664(%rbp), %r14
.L1059:
	testq	%r14, %r14
	je	.L1064
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1064:
	movq	-1840(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1065
	call	_ZdlPv@PLT
.L1065:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1066
	.p2align 4,,10
	.p2align 3
.L1070:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1067
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1070
.L1068:
	movq	-856(%rbp), %r14
.L1066:
	testq	%r14, %r14
	je	.L1071
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1071:
	movq	-1864(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1072
	call	_ZdlPv@PLT
.L1072:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1073
	.p2align 4,,10
	.p2align 3
.L1077:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1074
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1077
.L1075:
	movq	-1048(%rbp), %r14
.L1073:
	testq	%r14, %r14
	je	.L1078
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1078:
	movq	-1832(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1079
	call	_ZdlPv@PLT
.L1079:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1080
	.p2align 4,,10
	.p2align 3
.L1084:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1081
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1084
.L1082:
	movq	-1240(%rbp), %r14
.L1080:
	testq	%r14, %r14
	je	.L1085
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1085:
	movq	-1856(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1086
	call	_ZdlPv@PLT
.L1086:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1087
	.p2align 4,,10
	.p2align 3
.L1091:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1088
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1091
.L1089:
	movq	-1432(%rbp), %r14
.L1087:
	testq	%r14, %r14
	je	.L1092
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1092:
	movq	-1816(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1093
	call	_ZdlPv@PLT
.L1093:
	movq	-1616(%rbp), %rbx
	movq	-1624(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1094
	.p2align 4,,10
	.p2align 3
.L1098:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1095
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1098
.L1096:
	movq	-1624(%rbp), %r14
.L1094:
	testq	%r14, %r14
	je	.L1099
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1099:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1209
	addq	$1864, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1095:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1098
	jmp	.L1096
	.p2align 4,,10
	.p2align 3
.L1088:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1091
	jmp	.L1089
	.p2align 4,,10
	.p2align 3
.L1081:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1084
	jmp	.L1082
	.p2align 4,,10
	.p2align 3
.L1074:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1077
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1067:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1070
	jmp	.L1068
	.p2align 4,,10
	.p2align 3
.L1060:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1063
	jmp	.L1061
	.p2align 4,,10
	.p2align 3
.L1046:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1049
	jmp	.L1047
	.p2align 4,,10
	.p2align 3
.L1053:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1056
	jmp	.L1054
	.p2align 4,,10
	.p2align 3
.L1202:
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r9d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L1020
	call	_ZdlPv@PLT
.L1020:
	movq	(%r15), %rax
	movl	$2026, %edx
	movq	%r12, %rdi
	leaq	.LC14(%rip), %rsi
	leaq	-1792(%rbp), %r15
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rcx, -1904(%rbp)
	movq	%rax, -1888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1888(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1888(%rbp), %rax
	movl	$32, %edi
	movq	-1904(%rbp), %xmm2
	movq	$0, -1776(%rbp)
	movq	%rax, %xmm0
	movq	%rax, %xmm6
	punpcklqdq	%xmm0, %xmm0
	punpcklqdq	%xmm6, %xmm2
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -1904(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -1792(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movq	%r15, %rsi
	leaq	32(%rax), %rdx
	leaq	-1248(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1021
	call	_ZdlPv@PLT
.L1021:
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1752(%rbp)
	jne	.L1210
.L1022:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1376(%rbp)
	je	.L1024
.L1203:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1440(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r8d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1025
	call	_ZdlPv@PLT
.L1025:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-480(%rbp), %rdi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1026
	call	_ZdlPv@PLT
.L1026:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1184(%rbp)
	je	.L1027
.L1204:
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1248(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L1028
	call	_ZdlPv@PLT
.L1028:
	movq	(%r15), %rax
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	24(%rax), %rax
	movq	%rcx, -1904(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -1888(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r13, %rcx
	movq	%rbx, %rdi
	movq	-1888(%rbp), %rdx
	call	_ZN2v88internal24Cast14ATFastJSRegExp_134EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rcx
	movq	%r15, %xmm3
	pxor	%xmm0, %xmm0
	movhps	-1904(%rbp), %xmm3
	movl	$32, %edi
	movq	%rax, -72(%rbp)
	leaq	-1792(%rbp), %r15
	movq	%rcx, -80(%rbp)
	movaps	%xmm3, -1904(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -1792(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	32(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1029
	call	_ZdlPv@PLT
.L1029:
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1752(%rbp)
	jne	.L1211
.L1030:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -992(%rbp)
	je	.L1032
.L1205:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rdi
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1033
	call	_ZdlPv@PLT
.L1033:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-480(%rbp), %rdi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1034
	call	_ZdlPv@PLT
.L1034:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L1035
.L1206:
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1036
	call	_ZdlPv@PLT
.L1036:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	24(%rax), %rdx
	movdqu	(%rax), %xmm4
	movaps	%xmm0, -1760(%rbp)
	movq	$0, -1744(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm4, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -1760(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1037
	call	_ZdlPv@PLT
.L1037:
	movq	-1848(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1035
	.p2align 4,,10
	.p2align 3
.L1207:
	movq	-1848(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%r15, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1039
	call	_ZdlPv@PLT
.L1039:
	movq	(%rbx), %rax
	movl	$160, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r15
	movq	%rcx, -1888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r15, -80(%rbp)
	movhps	-1888(%rbp), %xmm0
	leaq	-288(%rbp), %r15
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm6
	leaq	24(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1040
	call	_ZdlPv@PLT
.L1040:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -416(%rbp)
	je	.L1041
.L1208:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1042
	call	_ZdlPv@PLT
.L1042:
	movq	-1872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1041
	.p2align 4,,10
	.p2align 3
.L1210:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1904(%rbp), %xmm7
	movl	$24, %edi
	movaps	%xmm0, -1792(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-1440(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1023
	call	_ZdlPv@PLT
.L1023:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1211:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1904(%rbp), %xmm7
	movl	$24, %edi
	movaps	%xmm0, -1792(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-1056(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1031
	call	_ZdlPv@PLT
.L1031:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1030
.L1209:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22517:
	.size	_ZN2v88internal25Cast14ATFastJSRegExp_1469EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, .-_ZN2v88internal25Cast14ATFastJSRegExp_1469EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.section	.rodata._ZN2v88internal35RegExpPrototypeFlagsGetterAssembler38GenerateRegExpPrototypeFlagsGetterImplEv.str1.1,"aMS",@progbits,1
.LC15:
	.string	"RegExp.prototype.flags"
	.section	.text._ZN2v88internal35RegExpPrototypeFlagsGetterAssembler38GenerateRegExpPrototypeFlagsGetterImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal35RegExpPrototypeFlagsGetterAssembler38GenerateRegExpPrototypeFlagsGetterImplEv
	.type	_ZN2v88internal35RegExpPrototypeFlagsGetterAssembler38GenerateRegExpPrototypeFlagsGetterImplEv, @function
_ZN2v88internal35RegExpPrototypeFlagsGetterAssembler38GenerateRegExpPrototypeFlagsGetterImplEv:
.LFB22510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1000(%rbp), %r14
	leaq	-1056(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1184(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1256, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, -1272(%rbp)
	movq	%rax, -1224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	-1224(%rbp), %r12
	movq	%rax, -1296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$48, %edi
	movq	$0, -1048(%rbp)
	movq	%rax, -1280(%rbp)
	movq	-1224(%rbp), %rax
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1048(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1224(%rbp), %rax
	movl	$72, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1264(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1224(%rbp), %rax
	movl	$96, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1256(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1224(%rbp), %rax
	movl	$48, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1224(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -280(%rbp)
	leaq	-232(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -1240(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1296(%rbp), %xmm1
	movaps	%xmm0, -1184(%rbp)
	movhps	-1280(%rbp), %xmm1
	movq	$0, -1168(%rbp)
	movaps	%xmm1, -1296(%rbp)
	call	_Znwm@PLT
	movdqa	-1296(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1184(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1213
	call	_ZdlPv@PLT
.L1213:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -992(%rbp)
	jne	.L1338
	cmpq	$0, -800(%rbp)
	jne	.L1339
.L1219:
	cmpq	$0, -608(%rbp)
	jne	.L1340
.L1222:
	cmpq	$0, -416(%rbp)
	jne	.L1341
.L1225:
	cmpq	$0, -224(%rbp)
	jne	.L1342
.L1229:
	movq	-1240(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1233
	call	_ZdlPv@PLT
.L1233:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1234
	.p2align 4,,10
	.p2align 3
.L1238:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1235
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1238
.L1236:
	movq	-280(%rbp), %r13
.L1234:
	testq	%r13, %r13
	je	.L1239
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1239:
	movq	-1248(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1240
	call	_ZdlPv@PLT
.L1240:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1241
	.p2align 4,,10
	.p2align 3
.L1245:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1242
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1245
.L1243:
	movq	-472(%rbp), %r13
.L1241:
	testq	%r13, %r13
	je	.L1246
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1246:
	movq	-1256(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1247
	call	_ZdlPv@PLT
.L1247:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1248
	.p2align 4,,10
	.p2align 3
.L1252:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1249
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1252
.L1250:
	movq	-664(%rbp), %r13
.L1248:
	testq	%r13, %r13
	je	.L1253
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1253:
	movq	-1264(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1254
	call	_ZdlPv@PLT
.L1254:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1255
	.p2align 4,,10
	.p2align 3
.L1259:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1256
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1259
.L1257:
	movq	-856(%rbp), %r13
.L1255:
	testq	%r13, %r13
	je	.L1260
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1260:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1261
	call	_ZdlPv@PLT
.L1261:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1262
	.p2align 4,,10
	.p2align 3
.L1266:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1263
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1266
.L1264:
	movq	-1048(%rbp), %r13
.L1262:
	testq	%r13, %r13
	je	.L1267
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1267:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1343
	addq	$1256, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1263:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1266
	jmp	.L1264
	.p2align 4,,10
	.p2align 3
.L1256:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1259
	jmp	.L1257
	.p2align 4,,10
	.p2align 3
.L1249:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1252
	jmp	.L1250
	.p2align 4,,10
	.p2align 3
.L1235:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1238
	jmp	.L1236
	.p2align 4,,10
	.p2align 3
.L1242:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1245
	jmp	.L1243
	.p2align 4,,10
	.p2align 3
.L1338:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r8d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1215
	call	_ZdlPv@PLT
.L1215:
	movq	(%rbx), %rax
	movl	$155, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r15
	movq	8(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1272(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$155, %ecx
	movq	%r15, %rsi
	movq	%rbx, %rdx
	leaq	.LC15(%rip), %r8
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler20ThrowIfNotJSReceiverENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS0_15MessageTemplateEPKc@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$160, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	-1272(%rbp), %rdi
	call	_ZN2v88internal25Cast14ATFastJSRegExp_1469EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	movq	%rbx, %xmm3
	movq	%r15, %xmm2
	pxor	%xmm0, %xmm0
	punpcklqdq	%xmm3, %xmm2
	movl	$32, %edi
	movaps	%xmm0, -1216(%rbp)
	leaq	-1216(%rbp), %r15
	movaps	%xmm2, -1296(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	%rbx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -1200(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movq	%r15, %rsi
	leaq	32(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -1216(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1200(%rbp)
	movq	%rdx, -1208(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1216
	call	_ZdlPv@PLT
.L1216:
	movq	-1256(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1176(%rbp)
	jne	.L1344
.L1217:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -800(%rbp)
	je	.L1219
.L1339:
	movq	-1264(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rdi
	movb	$8, 2(%rax)
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1220
	call	_ZdlPv@PLT
.L1220:
	movq	(%rbx), %rax
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movdqu	(%rax), %xmm0
	movaps	%xmm1, -1184(%rbp)
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1296(%rbp)
	call	_Znwm@PLT
	movdqa	-1296(%rbp), %xmm0
	leaq	-480(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1184(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1221
	call	_ZdlPv@PLT
.L1221:
	movq	-1248(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -608(%rbp)
	je	.L1222
.L1340:
	movq	-1256(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1223
	call	_ZdlPv@PLT
.L1223:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	24(%rax), %rdx
	movdqu	(%rax), %xmm6
	movaps	%xmm0, -1184(%rbp)
	movq	$0, -1168(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm6, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-288(%rbp), %rdi
	movq	%rax, -1184(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1224
	call	_ZdlPv@PLT
.L1224:
	movq	-1240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -416(%rbp)
	je	.L1225
.L1341:
	movq	-1248(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	leaq	-480(%rbp), %r8
	movq	%r8, -1296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	-1296(%rbp), %r8
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r13, %rsi
	movq	%r8, %rdi
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1226
	movq	%rax, -1296(%rbp)
	call	_ZdlPv@PLT
	movq	-1296(%rbp), %rax
.L1226:
	movq	(%rax), %rax
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	testq	%rdx, %rdx
	cmovne	%rdx, %r15
	testq	%rax, %rax
	movl	$161, %edx
	cmovne	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdx
	movq	-1272(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal19SlowFlagsGetter_330EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -224(%rbp)
	je	.L1229
.L1342:
	movq	-1240(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	leaq	-288(%rbp), %r8
	movq	%r8, -1296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1168(%rbp)
	movaps	%xmm0, -1184(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	-1296(%rbp), %r8
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%r8, %rdi
	movq	%rax, -1184(%rbp)
	movq	%rdx, -1168(%rbp)
	movq	%rdx, -1176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1230
	movq	%rax, -1296(%rbp)
	call	_ZdlPv@PLT
	movq	-1296(%rbp), %rax
.L1230:
	movq	(%rax), %rax
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rsi
	movq	(%rax), %rdx
	movq	16(%rax), %rax
	testq	%rdx, %rdx
	cmovne	%rdx, %r15
	testq	%rax, %rax
	movl	$162, %edx
	cmovne	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdx
	movq	-1272(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal19FastFlagsGetter_329EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEE
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L1229
	.p2align 4,,10
	.p2align 3
.L1344:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movdqa	-1296(%rbp), %xmm3
	movaps	%xmm0, -1216(%rbp)
	movaps	%xmm3, -96(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -1200(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm4
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -1216(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -1200(%rbp)
	movq	%rdx, -1208(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1218
	call	_ZdlPv@PLT
.L1218:
	movq	-1264(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1217
.L1343:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22510:
	.size	_ZN2v88internal35RegExpPrototypeFlagsGetterAssembler38GenerateRegExpPrototypeFlagsGetterImplEv, .-_ZN2v88internal35RegExpPrototypeFlagsGetterAssembler38GenerateRegExpPrototypeFlagsGetterImplEv
	.section	.rodata._ZN2v88internal8Builtins35Generate_RegExpPrototypeFlagsGetterEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"RegExpPrototypeFlagsGetter"
	.section	.text._ZN2v88internal8Builtins35Generate_RegExpPrototypeFlagsGetterEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins35Generate_RegExpPrototypeFlagsGetterEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins35Generate_RegExpPrototypeFlagsGetterEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins35Generate_RegExpPrototypeFlagsGetterEPNS0_8compiler18CodeAssemblerStateE:
.LFB22506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$659, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC16(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$880, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L1349
.L1346:
	movq	%r13, %rdi
	call	_ZN2v88internal35RegExpPrototypeFlagsGetterAssembler38GenerateRegExpPrototypeFlagsGetterImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1350
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1349:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L1346
.L1350:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22506:
	.size	_ZN2v88internal8Builtins35Generate_RegExpPrototypeFlagsGetterEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins35Generate_RegExpPrototypeFlagsGetterEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal22IsFastRegExpStrict_326EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal22IsFastRegExpStrict_326EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEE, @function
_GLOBAL__sub_I__ZN2v88internal22IsFastRegExpStrict_326EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEE:
.LFB29155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29155:
	.size	_GLOBAL__sub_I__ZN2v88internal22IsFastRegExpStrict_326EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEE, .-_GLOBAL__sub_I__ZN2v88internal22IsFastRegExpStrict_326EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal22IsFastRegExpStrict_326EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
