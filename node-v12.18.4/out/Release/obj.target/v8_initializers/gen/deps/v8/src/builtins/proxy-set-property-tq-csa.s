	.file	"proxy-set-property-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0:
.LFB29277:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	%rsi, %r13
	pushq	%rbx
	movq	%r13, %rax
	sarq	$3, %rax
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%r13, %r13
	js	.L14
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%rdx, %r14
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.L9
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%rax, %rdi
.L9:
	leaq	(%rdi,%r13), %r15
	movq	%rdi, (%rbx)
	movq	%r15, 16(%rbx)
	cmpq	%r12, %r14
	je	.L10
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
.L10:
	movq	%r15, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29277:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev,"axG",@progbits,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev:
.LFB22413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$56, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L16
	call	_ZdlPv@PLT
.L16:
	movq	16(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	%r12, %r13
	je	.L17
	.p2align 4,,10
	.p2align 3
.L21:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L18
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L21
.L19:
	movq	8(%rbx), %r12
.L17:
	testq	%r12, %r12
	je	.L15
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L21
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L15:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22413:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev
	.set	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.section	.rodata._ZN2v88internal30CallThrowTypeErrorIfStrict_318EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS0_15MessageTemplateE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../../deps/v8/../../deps/v8/src/builtins/proxy-set-property.tq"
	.section	.text._ZN2v88internal30CallThrowTypeErrorIfStrict_318EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS0_15MessageTemplateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30CallThrowTypeErrorIfStrict_318EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS0_15MessageTemplateE
	.type	_ZN2v88internal30CallThrowTypeErrorIfStrict_318EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS0_15MessageTemplateE, @function
_ZN2v88internal30CallThrowTypeErrorIfStrict_318EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS0_15MessageTemplateE:
.LFB22410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-600(%rbp), %r14
	leaq	-216(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-696(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$712, %rsp
	movl	%edx, -728(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -696(%rbp)
	movq	%rdi, -656(%rbp)
	movl	$24, %edi
	movq	$0, -648(%rbp)
	movq	$0, -640(%rbp)
	movq	$0, -632(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	$0, 16(%rax)
	movq	%rax, -648(%rbp)
	leaq	-656(%rbp), %rax
	movq	%rdx, -632(%rbp)
	movq	%rdx, -640(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -616(%rbp)
	movq	%rax, -720(%rbp)
	movq	$0, -624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$24, %edi
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	%rax, -464(%rbp)
	movq	$0, -440(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-408(%rbp), %rcx
	movq	%r12, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 16(%rax)
	movq	%rdx, -440(%rbp)
	movq	%rdx, -448(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -712(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -424(%rbp)
	movq	%rax, -456(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$24, %edi
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	%rax, -272(%rbp)
	movq	$0, -248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r15, %rdi
	movq	$0, 16(%rax)
	movq	%rdx, -248(%rbp)
	movq	%rdx, -256(%rbp)
	xorl	%edx, %edx
	movq	%rax, -264(%rbp)
	movups	%xmm0, -232(%rbp)
	movq	$0, -240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-720(%rbp), %rdi
	movq	%r13, (%rax)
	leaq	-688(%rbp), %r13
	leaq	8(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L28
	call	_ZdlPv@PLT
.L28:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -592(%rbp)
	jne	.L96
.L29:
	leaq	-272(%rbp), %rax
	cmpq	$0, -400(%rbp)
	movq	%rax, %rbx
	jne	.L97
.L32:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movb	$7, (%rax)
	leaq	1(%rax), %rdx
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L35
	call	_ZdlPv@PLT
.L35:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L36
	call	_ZdlPv@PLT
.L36:
	movq	-256(%rbp), %r15
	movq	-264(%rbp), %r13
	cmpq	%r13, %r15
	je	.L37
	.p2align 4,,10
	.p2align 3
.L41:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L38
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r15, %r13
	jne	.L41
.L39:
	movq	-264(%rbp), %r13
.L37:
	testq	%r13, %r13
	je	.L42
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L42:
	movq	-712(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L43
	call	_ZdlPv@PLT
.L43:
	movq	-448(%rbp), %rbx
	movq	-456(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L44
	.p2align 4,,10
	.p2align 3
.L48:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L45
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L48
.L46:
	movq	-456(%rbp), %r13
.L44:
	testq	%r13, %r13
	je	.L49
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L49:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L50
	call	_ZdlPv@PLT
.L50:
	movq	-640(%rbp), %rbx
	movq	-648(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L51
	.p2align 4,,10
	.p2align 3
.L55:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L52
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L55
.L53:
	movq	-648(%rbp), %r13
.L51:
	testq	%r13, %r13
	je	.L56
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L56:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L98
	addq	$712, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L55
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L38:
	addq	$24, %r13
	cmpq	%r13, %r15
	jne	.L41
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L45:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L48
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L96:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-720(%rbp), %rdi
	movq	%r13, %rsi
	movb	$7, (%rax)
	leaq	1(%rax), %rdx
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L30
	movq	%rax, -720(%rbp)
	call	_ZdlPv@PLT
	movq	-720(%rbp), %rax
.L30:
	movq	(%rax), %rax
	movl	$15, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -720(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	-728(%rbp), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r13, %rdi
	movq	%rax, -728(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal7Null_63EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movq	%rax, -736(%rbp)
	call	_ZN2v88internal7Null_63EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, -744(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-720(%rbp), %rbx
	leaq	-80(%rbp), %rcx
	movq	-728(%rbp), %xmm0
	movq	-744(%rbp), %rax
	movl	$3, %r8d
	movl	$179, %esi
	movq	%r13, %rdi
	movhps	-736(%rbp), %xmm0
	movq	%rbx, %rdx
	movaps	%xmm0, -80(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$13, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	leaq	-464(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L31
	call	_ZdlPv@PLT
.L31:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L97:
	movq	-712(%rbp), %rsi
	leaq	-464(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, -720(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-720(%rbp), %r8
	movq	%r13, %rsi
	movb	$7, (%rax)
	leaq	1(%rax), %rdx
	movq	%r8, %rdi
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L33
	movq	%rax, -720(%rbp)
	call	_ZdlPv@PLT
	movq	-720(%rbp), %rax
.L33:
	movq	(%rax), %rax
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	(%rax), %rcx
	movaps	%xmm0, -688(%rbp)
	movq	$0, -672(%rbp)
	movq	%rcx, -720(%rbp)
	call	_Znwm@PLT
	movq	-720(%rbp), %rcx
	movq	%r13, %rsi
	leaq	8(%rax), %rdx
	movq	%rax, -688(%rbp)
	movq	%rcx, (%rax)
	leaq	-272(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -672(%rbp)
	movq	%rax, %rbx
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L34
	call	_ZdlPv@PLT
.L34:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L32
.L98:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22410:
	.size	_ZN2v88internal30CallThrowTypeErrorIfStrict_318EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS0_15MessageTemplateE, .-_ZN2v88internal30CallThrowTypeErrorIfStrict_318EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS0_15MessageTemplateE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectES6_S5_NS0_10JSReceiverES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESH_SF_PNS9_IS7_EESJ_SJ_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectES6_S5_NS0_10JSReceiverES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESH_SF_PNS9_IS7_EESJ_SJ_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectES6_S5_NS0_10JSReceiverES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESH_SF_PNS9_IS7_EESJ_SJ_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectES6_S5_NS0_10JSReceiverES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESH_SF_PNS9_IS7_EESJ_SJ_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectES6_S5_NS0_10JSReceiverES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESH_SF_PNS9_IS7_EESJ_SJ_:
.LFB26631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$9, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$506381214178281223, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L100
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
.L100:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L101
	movq	%rdx, (%r15)
.L101:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L102
	movq	%rdx, (%r14)
.L102:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L103
	movq	%rdx, 0(%r13)
.L103:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L104
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L104:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L105
	movq	%rdx, (%rbx)
.L105:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L106
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L106:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L107
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L107:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L108
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L108:
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L99
	movq	-120(%rbp), %rbx
	movq	%rax, (%rbx)
.L99:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L142
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L142:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26631:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectES6_S5_NS0_10JSReceiverES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESH_SF_PNS9_IS7_EESJ_SJ_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectES6_S5_NS0_10JSReceiverES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESH_SF_PNS9_IS7_EESJ_SJ_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectES6_S5_NS0_10JSReceiverES7_S7_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESH_SF_PNS9_IS7_EESJ_SJ_SH_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectES6_S5_NS0_10JSReceiverES7_S7_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESH_SF_PNS9_IS7_EESJ_SJ_SH_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectES6_S5_NS0_10JSReceiverES7_S7_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESH_SF_PNS9_IS7_EESJ_SJ_SH_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectES6_S5_NS0_10JSReceiverES7_S7_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESH_SF_PNS9_IS7_EESJ_SJ_SH_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectES6_S5_NS0_10JSReceiverES7_S7_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESH_SF_PNS9_IS7_EESJ_SJ_SH_:
.LFB26641:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$10, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$506381214178281223, %rcx
	movq	%rcx, (%rax)
	movl	$2055, %ecx
	leaq	10(%rax), %rdx
	movw	%cx, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L144
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %rax
.L144:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L145
	movq	%rdx, (%r15)
.L145:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L146
	movq	%rdx, (%r14)
.L146:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L147
	movq	%rdx, 0(%r13)
.L147:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L148
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L148:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L149
	movq	%rdx, (%rbx)
.L149:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L150
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L150:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L151
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L151:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L152
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L152:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L153
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L153:
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L143
	movq	-128(%rbp), %rsi
	movq	%rax, (%rsi)
.L143:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L190
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L190:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26641:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectES6_S5_NS0_10JSReceiverES7_S7_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESH_SF_PNS9_IS7_EESJ_SJ_SH_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectES6_S5_NS0_10JSReceiverES7_S7_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESH_SF_PNS9_IS7_EESJ_SJ_SH_
	.section	.rodata._ZN2v88internal25ProxySetPropertyAssembler28GenerateProxySetPropertyImplEv.str1.1,"aMS",@progbits,1
.LC3:
	.string	"set"
	.section	.text._ZN2v88internal25ProxySetPropertyAssembler28GenerateProxySetPropertyImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25ProxySetPropertyAssembler28GenerateProxySetPropertyImplEv
	.type	_ZN2v88internal25ProxySetPropertyAssembler28GenerateProxySetPropertyImplEv, @function
_ZN2v88internal25ProxySetPropertyAssembler28GenerateProxySetPropertyImplEv:
.LFB22425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-2968(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-2776(%rbp), %rbx
	subq	$3496, %rsp
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -3280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -3296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -3304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, -3328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	leaq	-3280(%rbp), %r12
	movq	%rax, -3312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$120, %edi
	movq	$0, -3016(%rbp)
	movq	$0, -3008(%rbp)
	movq	%rax, %r13
	movq	-3280(%rbp), %rax
	movq	$0, -3000(%rbp)
	movq	%rax, -3024(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -3016(%rbp)
	leaq	-3024(%rbp), %rax
	movq	%rdx, -3000(%rbp)
	movq	%rdx, -3008(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2984(%rbp)
	movq	%rax, -3288(%rbp)
	movq	$0, -2992(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3280(%rbp), %rax
	movl	$192, %edi
	movq	$0, -2824(%rbp)
	movq	$0, -2816(%rbp)
	movq	%rax, -2832(%rbp)
	movq	$0, -2808(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -2808(%rbp)
	movq	%rdx, -2816(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2792(%rbp)
	movq	%rax, -2824(%rbp)
	movq	$0, -2800(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3280(%rbp), %rax
	movl	$216, %edi
	movq	$0, -2632(%rbp)
	movq	$0, -2624(%rbp)
	movq	%rax, -2640(%rbp)
	movq	$0, -2616(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2584(%rbp), %rcx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -2616(%rbp)
	movq	%rdx, -2624(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3344(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2600(%rbp)
	movq	%rax, -2632(%rbp)
	movq	$0, -2608(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3280(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2440(%rbp)
	movq	$0, -2432(%rbp)
	movq	%rax, -2448(%rbp)
	movq	$0, -2424(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2392(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -2424(%rbp)
	movq	%rdx, -2432(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3376(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2408(%rbp)
	movq	%rax, -2440(%rbp)
	movq	$0, -2416(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3280(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2248(%rbp)
	movq	$0, -2240(%rbp)
	movq	%rax, -2256(%rbp)
	movq	$0, -2232(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2200(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -2232(%rbp)
	movq	%rdx, -2240(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3392(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2216(%rbp)
	movq	%rax, -2248(%rbp)
	movq	$0, -2224(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3280(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2056(%rbp)
	movq	$0, -2048(%rbp)
	movq	%rax, -2064(%rbp)
	movq	$0, -2040(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2008(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -2040(%rbp)
	movq	%rdx, -2048(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3424(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2024(%rbp)
	movq	%rax, -2056(%rbp)
	movq	$0, -2032(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3280(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1864(%rbp)
	movq	$0, -1856(%rbp)
	movq	%rax, -1872(%rbp)
	movq	$0, -1848(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1816(%rbp), %rcx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -1848(%rbp)
	movq	%rdx, -1856(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3400(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1832(%rbp)
	movq	%rax, -1864(%rbp)
	movq	$0, -1840(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3280(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1672(%rbp)
	movq	$0, -1664(%rbp)
	movq	%rax, -1680(%rbp)
	movq	$0, -1656(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1624(%rbp), %rcx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -1656(%rbp)
	movq	%rdx, -1664(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3488(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1640(%rbp)
	movq	%rax, -1672(%rbp)
	movq	$0, -1648(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3280(%rbp), %rax
	movl	$240, %edi
	movq	$0, -1480(%rbp)
	movq	$0, -1472(%rbp)
	movq	%rax, -1488(%rbp)
	movq	$0, -1464(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1432(%rbp), %rcx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rdx, -1464(%rbp)
	movq	%rdx, -1472(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3408(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1448(%rbp)
	movq	%rax, -1480(%rbp)
	movq	$0, -1456(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3280(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1288(%rbp)
	movq	$0, -1280(%rbp)
	movq	%rax, -1296(%rbp)
	movq	$0, -1272(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1240(%rbp), %rcx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -1272(%rbp)
	movq	%rdx, -1280(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3472(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1256(%rbp)
	movq	%rax, -1288(%rbp)
	movq	$0, -1264(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3280(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1096(%rbp)
	movq	$0, -1088(%rbp)
	movq	%rax, -1104(%rbp)
	movq	$0, -1080(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1048(%rbp), %rcx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -1080(%rbp)
	movq	%rdx, -1088(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3432(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1064(%rbp)
	movq	%rax, -1096(%rbp)
	movq	$0, -1072(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3280(%rbp), %rax
	movl	$240, %edi
	movq	$0, -904(%rbp)
	movq	$0, -896(%rbp)
	movq	%rax, -912(%rbp)
	movq	$0, -888(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-856(%rbp), %rcx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rdx, -888(%rbp)
	movq	%rdx, -896(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3440(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -872(%rbp)
	movq	%rax, -904(%rbp)
	movq	$0, -880(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3280(%rbp), %rax
	movl	$240, %edi
	movq	$0, -712(%rbp)
	movq	$0, -704(%rbp)
	movq	%rax, -720(%rbp)
	movq	$0, -696(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-664(%rbp), %rcx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rdx, -696(%rbp)
	movq	%rdx, -704(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3448(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -680(%rbp)
	movq	%rax, -712(%rbp)
	movq	$0, -688(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3280(%rbp), %rax
	movl	$168, %edi
	movq	$0, -520(%rbp)
	movq	$0, -512(%rbp)
	movq	%rax, -528(%rbp)
	movq	$0, -504(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-472(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -504(%rbp)
	movq	%rdx, -512(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3368(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -488(%rbp)
	movq	%rax, -520(%rbp)
	movq	$0, -496(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3280(%rbp), %rax
	movl	$144, %edi
	movq	$0, -328(%rbp)
	movq	$0, -320(%rbp)
	movq	%rax, -336(%rbp)
	movq	$0, -312(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	leaq	-280(%rbp), %rcx
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movq	%rcx, %rdi
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rdx, -312(%rbp)
	movq	%rdx, -320(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3336(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -328(%rbp)
	movups	%xmm0, -296(%rbp)
	movq	$0, -304(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	-3296(%rbp), %xmm1
	movq	%r13, -112(%rbp)
	leaq	-3152(%rbp), %r13
	movhps	-3304(%rbp), %xmm1
	movaps	%xmm0, -3152(%rbp)
	movaps	%xmm1, -144(%rbp)
	movq	-3328(%rbp), %xmm1
	movq	$0, -3136(%rbp)
	movhps	-3312(%rbp), %xmm1
	movaps	%xmm1, -128(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-144(%rbp), %xmm4
	movdqa	-128(%rbp), %xmm5
	movq	-3288(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -3152(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -3136(%rbp)
	movq	%rdx, -3144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L192
	call	_ZdlPv@PLT
.L192:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2640(%rbp), %rax
	cmpq	$0, -2960(%rbp)
	movq	%rax, -3312(%rbp)
	leaq	-2832(%rbp), %rax
	movq	%rax, -3296(%rbp)
	jne	.L360
.L193:
	leaq	-2448(%rbp), %rax
	cmpq	$0, -2768(%rbp)
	movq	%rax, -3360(%rbp)
	jne	.L361
	cmpq	$0, -2576(%rbp)
	jne	.L362
.L201:
	leaq	-2256(%rbp), %rax
	cmpq	$0, -2384(%rbp)
	movq	%rax, -3344(%rbp)
	jne	.L363
.L204:
	leaq	-1872(%rbp), %rax
	cmpq	$0, -2192(%rbp)
	movq	%rax, -3376(%rbp)
	leaq	-2064(%rbp), %rax
	movq	%rax, -3304(%rbp)
	jne	.L364
	cmpq	$0, -2000(%rbp)
	jne	.L365
.L212:
	leaq	-1488(%rbp), %rax
	cmpq	$0, -1808(%rbp)
	movq	%rax, -3392(%rbp)
	leaq	-1680(%rbp), %rax
	movq	%rax, -3328(%rbp)
	jne	.L366
.L215:
	leaq	-1296(%rbp), %rax
	cmpq	$0, -1616(%rbp)
	movq	%rax, -3400(%rbp)
	jne	.L367
.L220:
	leaq	-1104(%rbp), %rax
	cmpq	$0, -1424(%rbp)
	movq	%rax, -3424(%rbp)
	jne	.L368
	cmpq	$0, -1232(%rbp)
	jne	.L369
.L225:
	leaq	-912(%rbp), %rax
	cmpq	$0, -1040(%rbp)
	leaq	-720(%rbp), %r14
	movq	%rax, -3408(%rbp)
	jne	.L370
	cmpq	$0, -848(%rbp)
	jne	.L371
.L234:
	cmpq	$0, -656(%rbp)
	jne	.L372
.L235:
	cmpq	$0, -464(%rbp)
	jne	.L373
.L236:
	cmpq	$0, -272(%rbp)
	jne	.L374
.L238:
	movq	-3336(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L240
	call	_ZdlPv@PLT
.L240:
	movq	-320(%rbp), %rbx
	movq	-328(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L241
	.p2align 4,,10
	.p2align 3
.L245:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L242
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L245
.L243:
	movq	-328(%rbp), %r13
.L241:
	testq	%r13, %r13
	je	.L246
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L246:
	movq	-3368(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-496(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L247
	call	_ZdlPv@PLT
.L247:
	movq	-512(%rbp), %rbx
	movq	-520(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L248
	.p2align 4,,10
	.p2align 3
.L252:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L249
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L252
.L250:
	movq	-520(%rbp), %r13
.L248:
	testq	%r13, %r13
	je	.L253
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L253:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3408(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3424(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3400(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3392(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3328(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3376(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3304(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3344(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3360(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3312(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3296(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3288(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L375
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L245
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L249:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L252
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L360:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -3136(%rbp)
	movaps	%xmm0, -3152(%rbp)
	call	_Znwm@PLT
	movq	-3288(%rbp), %rdi
	movq	%r13, %rsi
	movl	$134678279, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -3152(%rbp)
	movq	%rdx, -3136(%rbp)
	movq	%rdx, -3144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3152(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L194
	call	_ZdlPv@PLT
.L194:
	movq	(%r14), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	24(%rax), %rdx
	movq	(%rax), %r14
	movq	%rcx, -3328(%rbp)
	movq	16(%rax), %rcx
	movq	32(%rax), %rax
	movq	%rdx, -3312(%rbp)
	movl	$28, %edx
	movq	%rcx, -3296(%rbp)
	movq	%rax, -3304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$29, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$30, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3296(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal25Cast15ATPrivateSymbol_125EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3304(%rbp), %rcx
	movq	%r14, %xmm7
	movq	-3296(%rbp), %xmm4
	movhps	-3328(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movq	%rax, -80(%rbp)
	movl	$72, %edi
	movdqa	%xmm4, %xmm6
	movdqa	%xmm4, %xmm3
	movq	%rcx, -112(%rbp)
	leaq	-3184(%rbp), %r14
	movhps	-3312(%rbp), %xmm6
	punpcklqdq	%xmm3, %xmm3
	movaps	%xmm7, -3504(%rbp)
	movaps	%xmm6, -3360(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm3, -3328(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -3184(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -3168(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm4
	movq	-80(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm3
	leaq	72(%rax), %rdx
	movq	%rax, -3184(%rbp)
	movups	%xmm4, (%rax)
	movdqa	-96(%rbp), %xmm4
	movq	%rcx, 64(%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm4, 48(%rax)
	leaq	-2640(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3168(%rbp)
	movq	%rdx, -3176(%rbp)
	movq	%rax, -3312(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L195
	call	_ZdlPv@PLT
.L195:
	movq	-3344(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2832(%rbp), %rax
	cmpq	$0, -3144(%rbp)
	movq	%rax, -3296(%rbp)
	jne	.L376
.L196:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L361:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movabsq	$506381214178281223, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3136(%rbp)
	movaps	%xmm0, -3152(%rbp)
	call	_Znwm@PLT
	movq	-3296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3152(%rbp)
	movq	%rdx, -3136(%rbp)
	movq	%rdx, -3144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3152(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L199
	call	_ZdlPv@PLT
.L199:
	movq	(%rbx), %rax
	movl	$56, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movq	48(%rax), %rdx
	movdqu	(%rax), %xmm5
	movq	$0, -3136(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3152(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm3
	leaq	56(%rax), %rdx
	movq	%rax, -3152(%rbp)
	movq	%rcx, 48(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	leaq	-2448(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3136(%rbp)
	movq	%rdx, -3144(%rbp)
	movq	%rax, -3360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L200
	call	_ZdlPv@PLT
.L200:
	movq	-3376(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2576(%rbp)
	je	.L201
.L362:
	movq	-3344(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	movabsq	$506381214178281223, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -3136(%rbp)
	movaps	%xmm0, -3152(%rbp)
	call	_Znwm@PLT
	movq	-3312(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%rax, -3152(%rbp)
	movq	%rdx, -3136(%rbp)
	movq	%rdx, -3144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3152(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L202
	call	_ZdlPv@PLT
.L202:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movl	$31, %edx
	leaq	.LC2(%rip), %rsi
	movq	(%rax), %rax
	testq	%rax, %rax
	cmovne	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	movl	$144, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal30CallThrowTypeErrorIfStrict_318EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS0_15MessageTemplateE
	movl	$32, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L363:
	movq	-3376(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -3136(%rbp)
	movaps	%xmm0, -3152(%rbp)
	call	_Znwm@PLT
	movl	$1800, %ecx
	movq	-3360(%rbp), %rdi
	movq	%r13, %rsi
	movl	$134678279, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -3152(%rbp)
	movq	%rdx, -3136(%rbp)
	movq	%rdx, -3144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3152(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L205
	call	_ZdlPv@PLT
.L205:
	movq	(%rbx), %rax
	movl	$34, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r14
	movq	%rcx, -3328(%rbp)
	movq	24(%rax), %rcx
	movq	%rbx, -3304(%rbp)
	movq	48(%rax), %rbx
	movq	%rcx, -3344(%rbp)
	movq	32(%rax), %rcx
	movq	%rcx, -3376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$35, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$30, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm4
	movl	$56, %edi
	movq	-3304(%rbp), %xmm0
	movq	%rbx, -96(%rbp)
	movhps	-3328(%rbp), %xmm0
	movq	$0, -3136(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	%r14, %xmm0
	movhps	-3344(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-3376(%rbp), %xmm0
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3152(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-144(%rbp), %xmm5
	leaq	56(%rax), %rdx
	movq	%rax, -3152(%rbp)
	movq	%rcx, 48(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm6, 32(%rax)
	leaq	-2256(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3136(%rbp)
	movq	%rdx, -3144(%rbp)
	movq	%rax, -3344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L206
	call	_ZdlPv@PLT
.L206:
	movq	-3392(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L364:
	movq	-3392(%rbp), %rsi
	movq	%r12, %rdi
	movl	$1800, %r14d
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -3136(%rbp)
	movaps	%xmm0, -3152(%rbp)
	call	_Znwm@PLT
	movq	-3344(%rbp), %rdi
	movq	%r13, %rsi
	movl	$134678279, (%rax)
	leaq	7(%rax), %rdx
	movw	%r14w, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -3152(%rbp)
	movq	%rdx, -3136(%rbp)
	movq	%rdx, -3144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3152(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L208
	call	_ZdlPv@PLT
.L208:
	movq	(%rbx), %rax
	movl	$29, %edx
	movq	%r12, %rdi
	movq	24(%rax), %rsi
	movq	16(%rax), %rcx
	movq	8(%rax), %r14
	movq	(%rax), %rbx
	movq	%rsi, -3376(%rbp)
	movq	32(%rax), %rsi
	movq	40(%rax), %rax
	movq	%rcx, -3328(%rbp)
	movq	%rsi, -3392(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rax, -3504(%rbp)
	movq	%rbx, -3304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$45, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE(%rip), %esi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal20Cast10JSReceiver_140EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm7
	pxor	%xmm0, %xmm0
	movq	-3392(%rbp), %xmm2
	movl	$64, %edi
	movaps	%xmm0, -3184(%rbp)
	movq	-3328(%rbp), %xmm4
	movq	-3304(%rbp), %xmm5
	movhps	-3504(%rbp), %xmm2
	movq	%rbx, -96(%rbp)
	leaq	-3184(%rbp), %r14
	punpcklqdq	%xmm7, %xmm5
	movhps	-3376(%rbp), %xmm4
	movaps	%xmm2, -3392(%rbp)
	movaps	%xmm4, -3328(%rbp)
	movaps	%xmm5, -3504(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movq	%rax, -88(%rbp)
	movq	$0, -3168(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm4
	movdqa	-128(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm6
	leaq	64(%rax), %rdx
	movq	%rax, -3184(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm6, 48(%rax)
	leaq	-1872(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3168(%rbp)
	movq	%rdx, -3176(%rbp)
	movq	%rax, -3376(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L209
	call	_ZdlPv@PLT
.L209:
	movq	-3400(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2064(%rbp), %rax
	cmpq	$0, -3144(%rbp)
	movq	%rax, -3304(%rbp)
	jne	.L377
.L210:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2000(%rbp)
	je	.L212
.L365:
	movq	-3424(%rbp), %rsi
	movq	%r12, %rdi
	movl	$1800, %ebx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -3136(%rbp)
	movaps	%xmm0, -3152(%rbp)
	call	_Znwm@PLT
	movq	-3304(%rbp), %rdi
	movq	%r13, %rsi
	movw	%bx, 4(%rax)
	leaq	7(%rax), %rdx
	movl	$134678279, (%rax)
	movb	$7, 6(%rax)
	movq	%rax, -3152(%rbp)
	movq	%rdx, -3136(%rbp)
	movq	%rdx, -3144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3152(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L213
	call	_ZdlPv@PLT
.L213:
	movq	(%rbx), %rax
	movl	$48, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movdqu	(%rax), %xmm7
	movq	$0, -3136(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3152(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm3
	leaq	48(%rax), %rdx
	leaq	-336(%rbp), %rdi
	movq	%rax, -3152(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm3, 32(%rax)
	movq	%rdx, -3136(%rbp)
	movq	%rdx, -3144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L214
	call	_ZdlPv@PLT
.L214:
	movq	-3336(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L366:
	movq	-3400(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$506381214178281223, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3136(%rbp)
	movaps	%xmm0, -3152(%rbp)
	call	_Znwm@PLT
	movq	-3376(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3152(%rbp)
	movq	%rdx, -3136(%rbp)
	movq	%rdx, -3144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3152(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L216
	call	_ZdlPv@PLT
.L216:
	movq	(%rbx), %rax
	movl	$48, %edx
	movq	24(%rax), %rdi
	movq	8(%rax), %rcx
	movq	16(%rax), %rsi
	movq	40(%rax), %r10
	movq	%rdi, -3424(%rbp)
	movq	32(%rax), %rdi
	movq	%rcx, -3328(%rbp)
	movq	56(%rax), %rcx
	movq	%rsi, -3392(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	(%rax), %rbx
	movq	%rdi, -3504(%rbp)
	movq	%r12, %rdi
	movq	%r10, -3520(%rbp)
	movq	%rcx, -3400(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-3328(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -3536(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3536(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal27UnsafeCast10JSReceiver_1457EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$52, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -3536(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %r8
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	-3400(%rbp), %r14
	leaq	.LC3(%rip), %rcx
	movq	%r14, %rdx
	call	_ZN2v88internal13GetMethod_245EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPKcPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm6
	movq	%rbx, %xmm2
	movq	-3504(%rbp), %xmm7
	movhps	-3536(%rbp), %xmm6
	movhps	-3328(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movq	%r14, -80(%rbp)
	movhps	-3520(%rbp), %xmm7
	movl	$80, %edi
	movaps	%xmm6, -96(%rbp)
	movq	-3392(%rbp), %xmm3
	movaps	%xmm6, -3536(%rbp)
	leaq	-3184(%rbp), %r14
	movhps	-3424(%rbp), %xmm3
	movaps	%xmm7, -3520(%rbp)
	movaps	%xmm3, -3504(%rbp)
	movaps	%xmm2, -3424(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm0, -3184(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -3168(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm4
	leaq	80(%rax), %rdx
	movq	%rax, -3184(%rbp)
	movdqa	-80(%rbp), %xmm5
	movups	%xmm2, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm5, 64(%rax)
	leaq	-1488(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3168(%rbp)
	movq	%rdx, -3176(%rbp)
	movq	%rax, -3392(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L217
	call	_ZdlPv@PLT
.L217:
	movq	-3408(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1680(%rbp), %rax
	cmpq	$0, -3144(%rbp)
	movq	%rax, -3328(%rbp)
	jne	.L378
.L218:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L367:
	movq	-3488(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3256(%rbp)
	movq	$0, -3248(%rbp)
	movq	$0, -3240(%rbp)
	movq	$0, -3232(%rbp)
	movq	$0, -3224(%rbp)
	movq	$0, -3216(%rbp)
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	$0, -3184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3184(%rbp), %rax
	movq	-3328(%rbp), %rdi
	leaq	-3240(%rbp), %rcx
	pushq	%rax
	leaq	-3200(%rbp), %rax
	leaq	-3248(%rbp), %rdx
	pushq	%rax
	leaq	-3208(%rbp), %rax
	leaq	-3256(%rbp), %rsi
	pushq	%rax
	leaq	-3216(%rbp), %rax
	leaq	-3224(%rbp), %r9
	pushq	%rax
	leaq	-3232(%rbp), %r8
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectES6_S5_NS0_10JSReceiverES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESH_SF_PNS9_IS7_EESJ_SJ_
	addq	$32, %rsp
	movl	$64, %edi
	movq	-3208(%rbp), %xmm0
	movq	-3224(%rbp), %xmm1
	movq	-3240(%rbp), %xmm2
	movq	$0, -3136(%rbp)
	movq	-3256(%rbp), %xmm3
	movhps	-3200(%rbp), %xmm0
	movhps	-3216(%rbp), %xmm1
	movhps	-3232(%rbp), %xmm2
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3248(%rbp), %xmm3
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3152(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm2
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm6
	leaq	64(%rax), %rdx
	movq	%rax, -3152(%rbp)
	movq	-3400(%rbp), %rdi
	movups	%xmm3, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm6, 48(%rax)
	movq	%rdx, -3136(%rbp)
	movq	%rdx, -3144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L221
	call	_ZdlPv@PLT
.L221:
	movq	-3472(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L368:
	movq	-3408(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$506381214178281223, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -3136(%rbp)
	movaps	%xmm0, -3152(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r11d
	movq	-3392(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	leaq	10(%rax), %rdx
	movw	%r11w, 8(%rax)
	movq	%rax, -3152(%rbp)
	movq	%rdx, -3136(%rbp)
	movq	%rdx, -3144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3152(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L223
	call	_ZdlPv@PLT
.L223:
	movq	(%rbx), %rax
	movl	$72, %edi
	movdqu	48(%rax), %xmm0
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	72(%rax), %rdx
	movdqu	(%rax), %xmm4
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -80(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3152(%rbp)
	movq	$0, -3136(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-144(%rbp), %xmm5
	leaq	72(%rax), %rdx
	movq	%rax, -3152(%rbp)
	movdqa	-96(%rbp), %xmm7
	movq	%rcx, 64(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm7, 48(%rax)
	leaq	-1104(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3136(%rbp)
	movq	%rdx, -3144(%rbp)
	movq	%rax, -3424(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L224
	call	_ZdlPv@PLT
.L224:
	movq	-3432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1232(%rbp)
	je	.L225
.L369:
	movq	-3472(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$506381214178281223, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3136(%rbp)
	movaps	%xmm0, -3152(%rbp)
	call	_Znwm@PLT
	movq	-3400(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3152(%rbp)
	movq	%rdx, -3136(%rbp)
	movq	%rdx, -3144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3152(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L226
	call	_ZdlPv@PLT
.L226:
	movq	(%rbx), %rax
	movl	$53, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	40(%rax), %rsi
	movq	(%rax), %rbx
	movq	16(%rax), %r14
	movq	%rcx, -3472(%rbp)
	movq	24(%rax), %rcx
	movq	%rsi, -3520(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -3488(%rbp)
	movq	32(%rax), %rcx
	movq	%rbx, -3408(%rbp)
	movq	56(%rax), %rbx
	movq	%rcx, -3504(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$56, %edi
	movq	%rbx, -96(%rbp)
	movq	-3408(%rbp), %xmm0
	movq	$0, -3136(%rbp)
	movhps	-3472(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%r14, %xmm0
	movhps	-3488(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-3504(%rbp), %xmm0
	movhps	-3520(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3152(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-144(%rbp), %xmm6
	leaq	56(%rax), %rdx
	leaq	-528(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rax, -3152(%rbp)
	movq	%rdx, -3136(%rbp)
	movq	%rdx, -3144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L227
	call	_ZdlPv@PLT
.L227:
	movq	-3368(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L370:
	movq	-3432(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3272(%rbp)
	leaq	-3200(%rbp), %r14
	movq	$0, -3264(%rbp)
	movq	$0, -3256(%rbp)
	movq	$0, -3248(%rbp)
	movq	$0, -3240(%rbp)
	movq	$0, -3232(%rbp)
	movq	$0, -3224(%rbp)
	movq	$0, -3216(%rbp)
	movq	$0, -3208(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3208(%rbp), %rax
	movq	-3424(%rbp), %rdi
	leaq	-3256(%rbp), %rcx
	pushq	%rax
	leaq	-3216(%rbp), %rax
	leaq	-3240(%rbp), %r9
	pushq	%rax
	leaq	-3224(%rbp), %rax
	leaq	-3248(%rbp), %r8
	pushq	%rax
	leaq	-3232(%rbp), %rax
	leaq	-3264(%rbp), %rdx
	pushq	%rax
	leaq	-3272(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectES6_S5_NS0_10JSReceiverES7_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESH_SF_PNS9_IS7_EESJ_SJ_
	addq	$32, %rsp
	movl	$52, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$70, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3232(%rbp), %rax
	movq	-3224(%rbp), %rbx
	movq	%r14, %rdi
	movq	%rax, -3472(%rbp)
	movq	-3216(%rbp), %rax
	movq	%rbx, %rsi
	movq	%rax, -3432(%rbp)
	movq	-3208(%rbp), %rax
	movq	%rax, -3408(%rbp)
	movq	-3272(%rbp), %rax
	movq	%rax, -3488(%rbp)
	movq	-3248(%rbp), %rax
	movq	%rax, -3504(%rbp)
	movq	-3240(%rbp), %rax
	movq	%rax, -3520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19IsUndefinedConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	je	.L229
.L231:
	movq	-3472(%rbp), %xmm1
	movq	%rbx, %xmm0
	movq	%r14, %rdi
	movhps	-3432(%rbp), %xmm0
	movhps	-3504(%rbp), %xmm1
	movaps	%xmm0, -3536(%rbp)
	movaps	%xmm1, -3472(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	xorl	%edx, %edx
	movq	%rax, %rsi
.L359:
	movq	%r13, %rdi
	movl	$7, %ebx
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	%r14, %rdi
	movl	$4, %esi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-3152(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -3432(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-144(%rbp), %rcx
	pushq	%rbx
	xorl	%esi, %esi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rcx
	movq	%r14, %rdi
	movq	%rax, -3184(%rbp)
	movq	-3136(%rbp), %rax
	movl	$1, %ecx
	leaq	-3184(%rbp), %rdx
	movq	-3408(%rbp), %xmm2
	movdqa	-3472(%rbp), %xmm1
	movdqa	-3536(%rbp), %xmm0
	movq	%rax, -3176(%rbp)
	movq	-3488(%rbp), %r9
	movq	-3520(%rbp), %rax
	movhps	-3432(%rbp), %xmm2
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	popq	%r9
	movq	%r14, %rdi
	popq	%r10
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$71, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal13ToBoolean_240EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEE@PLT
	movq	-3208(%rbp), %r14
	pxor	%xmm0, %xmm0
	movq	-3224(%rbp), %xmm4
	movq	-3240(%rbp), %xmm5
	movl	$80, %edi
	movaps	%xmm0, -3152(%rbp)
	movq	-3256(%rbp), %xmm6
	movq	-3272(%rbp), %xmm7
	movhps	-3216(%rbp), %xmm4
	movq	%rax, -3432(%rbp)
	movhps	-3232(%rbp), %xmm5
	movhps	-3248(%rbp), %xmm6
	movaps	%xmm4, -3520(%rbp)
	movhps	-3264(%rbp), %xmm7
	movaps	%xmm5, -3504(%rbp)
	movaps	%xmm6, -3488(%rbp)
	movaps	%xmm7, -3472(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movq	%r14, -80(%rbp)
	movq	%rbx, -72(%rbp)
	movq	$0, -3136(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm2
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm6
	leaq	80(%rax), %rdx
	movq	%rax, -3152(%rbp)
	movdqa	-80(%rbp), %xmm4
	movups	%xmm3, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm4, 64(%rax)
	leaq	-912(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3136(%rbp)
	movq	%rdx, -3144(%rbp)
	movq	%rax, -3408(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L232
	call	_ZdlPv@PLT
.L232:
	movq	%rbx, %xmm6
	movq	%r14, %xmm0
	movdqa	-3472(%rbp), %xmm5
	movdqa	-3488(%rbp), %xmm3
	punpcklqdq	%xmm6, %xmm0
	movdqa	-3504(%rbp), %xmm2
	movdqa	-3520(%rbp), %xmm7
	movl	$80, %edi
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	leaq	-720(%rbp), %r14
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm0, -3152(%rbp)
	movq	$0, -3136(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm5
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm2
	leaq	80(%rax), %rdx
	movq	%rax, -3152(%rbp)
	movdqa	-144(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm7
	movups	%xmm5, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm7, 64(%rax)
	movq	%rdx, -3136(%rbp)
	movq	%rdx, -3144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L233
	call	_ZdlPv@PLT
.L233:
	movq	-3448(%rbp), %rcx
	movq	-3440(%rbp), %rdx
	movq	%r12, %rdi
	movq	-3432(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -848(%rbp)
	je	.L234
.L371:
	movq	-3440(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3264(%rbp)
	movq	$0, -3256(%rbp)
	movq	$0, -3248(%rbp)
	movq	$0, -3240(%rbp)
	movq	$0, -3232(%rbp)
	movq	$0, -3224(%rbp)
	movq	$0, -3216(%rbp)
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	$0, -3184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3184(%rbp), %rax
	movq	-3408(%rbp), %rdi
	pushq	%rax
	leaq	-3200(%rbp), %rax
	leaq	-3248(%rbp), %rcx
	pushq	%rax
	leaq	-3208(%rbp), %rax
	leaq	-3232(%rbp), %r9
	pushq	%rax
	leaq	-3216(%rbp), %rax
	leaq	-3240(%rbp), %r8
	pushq	%rax
	leaq	-3224(%rbp), %rax
	leaq	-3256(%rbp), %rdx
	pushq	%rax
	leaq	-3264(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectES6_S5_NS0_10JSReceiverES7_S7_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESH_SF_PNS9_IS7_EESJ_SJ_SH_
	addq	$48, %rsp
	movl	$72, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r13, %rdi
	movq	-3248(%rbp), %r8
	movq	-3240(%rbp), %r9
	movq	-3256(%rbp), %rcx
	pushq	$1
	movq	-3208(%rbp), %rdx
	movq	-3264(%rbp), %rsi
	call	_ZN2v88internal24ProxiesCodeStubAssembler21CheckGetSetTrapResultENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_7JSProxyEEENS3_INS0_4NameEEENS3_INS0_6ObjectEEENS8_10AccessKindE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$73, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3240(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -656(%rbp)
	popq	%rdi
	popq	%r8
	je	.L235
.L372:
	movq	-3448(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3264(%rbp)
	movq	$0, -3256(%rbp)
	movq	$0, -3248(%rbp)
	movq	$0, -3240(%rbp)
	movq	$0, -3232(%rbp)
	movq	$0, -3224(%rbp)
	movq	$0, -3216(%rbp)
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	$0, -3184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r14, %rdi
	leaq	-3184(%rbp), %rax
	pushq	%rax
	leaq	-3200(%rbp), %rax
	leaq	-3232(%rbp), %r9
	pushq	%rax
	leaq	-3208(%rbp), %rax
	leaq	-3248(%rbp), %rcx
	pushq	%rax
	leaq	-3216(%rbp), %rax
	leaq	-3240(%rbp), %r8
	pushq	%rax
	leaq	-3224(%rbp), %rax
	leaq	-3256(%rbp), %rdx
	pushq	%rax
	leaq	-3264(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectES6_S5_NS0_10JSReceiverES7_S7_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESH_SF_PNS9_IS7_EESJ_SJ_SH_
	addq	$48, %rsp
	movl	$76, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$150, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r13, %rdi
	movq	%rax, -3432(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$75, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal59FromConstexpr20UT5ATSmi10HeapObject18ATconstexpr_string_168EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -3440(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3264(%rbp), %rdx
	movl	$179, %esi
	movq	-3432(%rbp), %xmm0
	movq	-3248(%rbp), %rax
	leaq	-144(%rbp), %rcx
	movl	$3, %r8d
	movq	%r13, %rdi
	movhps	-3440(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$77, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3240(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -464(%rbp)
	je	.L236
.L373:
	movq	-3368(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-528(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -3136(%rbp)
	movaps	%xmm0, -3152(%rbp)
	call	_Znwm@PLT
	movl	$1800, %esi
	movq	%rbx, %rdi
	movw	%si, 4(%rax)
	leaq	7(%rax), %rdx
	movq	%r13, %rsi
	movl	$134678279, (%rax)
	movb	$8, 6(%rax)
	movq	%rax, -3152(%rbp)
	movq	%rdx, -3136(%rbp)
	movq	%rdx, -3144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3152(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L237
	call	_ZdlPv@PLT
.L237:
	movq	(%rbx), %rax
	movl	$81, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	16(%rax), %rbx
	movq	(%rax), %r9
	movq	32(%rax), %rcx
	movq	%rbx, -3432(%rbp)
	movq	24(%rax), %rbx
	movq	48(%rax), %rax
	movq	%r9, -3472(%rbp)
	movq	%rcx, -3440(%rbp)
	movq	%rax, -3448(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3472(%rbp), %r9
	movl	$4, %r8d
	movq	%r13, %rdi
	movq	-3448(%rbp), %xmm0
	leaq	-144(%rbp), %rcx
	movl	$290, %esi
	movq	%r9, %rdx
	movhps	-3432(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	movhps	-3440(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$82, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -272(%rbp)
	je	.L238
.L374:
	movq	-3336(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-336(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -3136(%rbp)
	movaps	%xmm0, -3152(%rbp)
	call	_Znwm@PLT
	movl	$1800, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	$134678279, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -3152(%rbp)
	movq	%rdx, -3136(%rbp)
	movq	%rdx, -3144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3152(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L239
	call	_ZdlPv@PLT
.L239:
	movq	(%rbx), %rax
	movl	$85, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movl	$145, %edx
	leaq	.LC3(%rip), %rcx
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L229:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IsNullConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L231
	movq	-3472(%rbp), %xmm1
	movq	%rbx, %xmm0
	movq	%r14, %rdi
	movhps	-3432(%rbp), %xmm0
	movhps	-3504(%rbp), %xmm1
	movaps	%xmm0, -3536(%rbp)
	movaps	%xmm1, -3472(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	movq	%rax, %rsi
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L376:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-3504(%rbp), %xmm6
	movq	%r14, %rdi
	movdqa	-3360(%rbp), %xmm4
	movq	-3304(%rbp), %rax
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rdx
	movdqa	-3328(%rbp), %xmm5
	leaq	-144(%rbp), %rsi
	movaps	%xmm6, -144(%rbp)
	movq	%rax, -112(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm0, -3184(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -3168(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3296(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L197
	call	_ZdlPv@PLT
.L197:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L377:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movdqa	-3504(%rbp), %xmm6
	movdqa	-3328(%rbp), %xmm4
	movdqa	-3392(%rbp), %xmm5
	movaps	%xmm0, -3184(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -3168(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm6
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm3
	leaq	56(%rax), %rdx
	movq	%rax, -3184(%rbp)
	movq	%rcx, 48(%rax)
	movq	-3304(%rbp), %rdi
	movups	%xmm3, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm4, 32(%rax)
	movq	%rdx, -3168(%rbp)
	movq	%rdx, -3176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L211
	call	_ZdlPv@PLT
.L211:
	movq	-3424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L378:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-3424(%rbp), %xmm5
	movq	%r14, %rdi
	movdqa	-3504(%rbp), %xmm3
	movdqa	-3520(%rbp), %xmm6
	movdqa	-3536(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	leaq	-144(%rbp), %rsi
	movq	-3400(%rbp), %rax
	leaq	-72(%rbp), %rdx
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm5, -144(%rbp)
	movq	%rax, -80(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -3184(%rbp)
	movq	$0, -3168(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3328(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L219
	call	_ZdlPv@PLT
.L219:
	movq	-3488(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L218
.L375:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22425:
	.size	_ZN2v88internal25ProxySetPropertyAssembler28GenerateProxySetPropertyImplEv, .-_ZN2v88internal25ProxySetPropertyAssembler28GenerateProxySetPropertyImplEv
	.section	.rodata._ZN2v88internal8Builtins25Generate_ProxySetPropertyEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/proxy-set-property-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins25Generate_ProxySetPropertyEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"ProxySetProperty"
	.section	.text._ZN2v88internal8Builtins25Generate_ProxySetPropertyEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins25Generate_ProxySetPropertyEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins25Generate_ProxySetPropertyEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins25Generate_ProxySetPropertyEPNS0_8compiler18CodeAssemblerStateE:
.LFB22421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$181, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$859, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L383
.L380:
	movq	%r13, %rdi
	call	_ZN2v88internal25ProxySetPropertyAssembler28GenerateProxySetPropertyImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L384
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L383:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L380
.L384:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22421:
	.size	_ZN2v88internal8Builtins25Generate_ProxySetPropertyEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins25Generate_ProxySetPropertyEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal30CallThrowTypeErrorIfStrict_318EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS0_15MessageTemplateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal30CallThrowTypeErrorIfStrict_318EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS0_15MessageTemplateE, @function
_GLOBAL__sub_I__ZN2v88internal30CallThrowTypeErrorIfStrict_318EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS0_15MessageTemplateE:
.LFB29099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29099:
	.size	_GLOBAL__sub_I__ZN2v88internal30CallThrowTypeErrorIfStrict_318EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS0_15MessageTemplateE, .-_GLOBAL__sub_I__ZN2v88internal30CallThrowTypeErrorIfStrict_318EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS0_15MessageTemplateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal30CallThrowTypeErrorIfStrict_318EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS0_15MessageTemplateE
	.weak	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
