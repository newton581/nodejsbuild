	.file	"proxy-prevent-extensions-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev,"axG",@progbits,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev:
.LFB22421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$56, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L8
	call	_ZdlPv@PLT
.L8:
	movq	16(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	%r12, %r13
	je	.L9
	.p2align 4,,10
	.p2align 3
.L13:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L10
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L13
.L11:
	movq	8(%rbx), %r12
.L9:
	testq	%r12, %r12
	je	.L7
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L13
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L7:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22421:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev
	.set	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_7OddballENS0_10JSReceiverENS0_10HeapObjectES6_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESI_PNSA_IS8_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_7OddballENS0_10JSReceiverENS0_10HeapObjectES6_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESI_PNSA_IS8_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_7OddballENS0_10JSReceiverENS0_10HeapObjectES6_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESI_PNSA_IS8_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_7OddballENS0_10JSReceiverENS0_10HeapObjectES6_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESI_PNSA_IS8_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_7OddballENS0_10JSReceiverENS0_10HeapObjectES6_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESI_PNSA_IS8_EE:
.LFB26568:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$7, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$117901063, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$8, 6(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L20
	movq	%rax, -112(%rbp)
	call	_ZdlPv@PLT
	movq	-112(%rbp), %rax
.L20:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L21
	movq	%rdx, (%r15)
.L21:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L22
	movq	%rdx, (%r14)
.L22:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L23
	movq	%rdx, 0(%r13)
.L23:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L24
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L24:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L25
	movq	%rdx, (%rbx)
.L25:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L26
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L26:
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L19
	movq	-104(%rbp), %rbx
	movq	%rax, (%rbx)
.L19:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L54
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L54:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26568:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_7OddballENS0_10JSReceiverENS0_10HeapObjectES6_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESI_PNSA_IS8_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_7OddballENS0_10JSReceiverENS0_10HeapObjectES6_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESI_PNSA_IS8_EE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_7OddballENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_7OddballENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_7OddballENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_7OddballENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_7OddballENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE:
.LFB26571:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	$4, %edi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	leaq	-80(%rbp), %rsi
	movl	$134678279, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L56
	call	_ZdlPv@PLT
.L56:
	movq	(%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L57
	movq	%rdx, (%r14)
.L57:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L58
	movq	%rdx, 0(%r13)
.L58:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L59
	movq	%rdx, (%r12)
.L59:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L55
	movq	%rax, (%rbx)
.L55:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L78
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L78:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26571:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_7OddballENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_7OddballENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE
	.section	.rodata._ZN2v88internal31ProxyPreventExtensionsAssembler34GenerateProxyPreventExtensionsImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../../deps/v8/../../deps/v8/src/builtins/proxy-prevent-extensions.tq"
	.section	.rodata._ZN2v88internal31ProxyPreventExtensionsAssembler34GenerateProxyPreventExtensionsImplEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	"preventExtensions"
	.section	.text._ZN2v88internal31ProxyPreventExtensionsAssembler34GenerateProxyPreventExtensionsImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31ProxyPreventExtensionsAssembler34GenerateProxyPreventExtensionsImplEv
	.type	_ZN2v88internal31ProxyPreventExtensionsAssembler34GenerateProxyPreventExtensionsImplEv, @function
_ZN2v88internal31ProxyPreventExtensionsAssembler34GenerateProxyPreventExtensionsImplEv:
.LFB22418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-3528(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-3712(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$4056, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -3808(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -3824(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	leaq	-3808(%rbp), %r12
	movq	%rax, -3832(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$72, %edi
	movq	$0, -3576(%rbp)
	movq	$0, -3568(%rbp)
	movq	%rax, %rbx
	movq	-3808(%rbp), %rax
	movq	$0, -3560(%rbp)
	movq	%rax, -3584(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -3576(%rbp)
	leaq	-3584(%rbp), %rax
	movq	%rdx, -3560(%rbp)
	movq	%rdx, -3568(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3544(%rbp)
	movq	%rax, -3816(%rbp)
	movq	$0, -3552(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3808(%rbp), %rax
	movl	$96, %edi
	movq	$0, -3384(%rbp)
	movq	$0, -3376(%rbp)
	movq	%rax, -3392(%rbp)
	movq	$0, -3368(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-3336(%rbp), %rcx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -3368(%rbp)
	movq	%rdx, -3376(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3864(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -3352(%rbp)
	movq	%rax, -3384(%rbp)
	movq	$0, -3360(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3808(%rbp), %rax
	movl	$120, %edi
	movq	$0, -3192(%rbp)
	movq	$0, -3184(%rbp)
	movq	%rax, -3200(%rbp)
	movq	$0, -3176(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-3144(%rbp), %rcx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -3176(%rbp)
	movq	%rdx, -3184(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3904(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -3160(%rbp)
	movq	%rax, -3192(%rbp)
	movq	$0, -3168(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3808(%rbp), %rax
	movl	$144, %edi
	movq	$0, -3000(%rbp)
	movq	$0, -2992(%rbp)
	movq	%rax, -3008(%rbp)
	movq	$0, -2984(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2952(%rbp), %rcx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rdx, -2984(%rbp)
	movq	%rdx, -2992(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3952(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2968(%rbp)
	movq	%rax, -3000(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3808(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2808(%rbp)
	movq	$0, -2800(%rbp)
	movq	%rax, -2816(%rbp)
	movq	$0, -2792(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2760(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -2792(%rbp)
	movq	%rdx, -2800(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3944(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2776(%rbp)
	movq	%rax, -2808(%rbp)
	movq	$0, -2784(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3808(%rbp), %rax
	movl	$120, %edi
	movq	$0, -2616(%rbp)
	movq	$0, -2608(%rbp)
	movq	%rax, -2624(%rbp)
	movq	$0, -2600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2568(%rbp), %rcx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -2600(%rbp)
	movq	%rdx, -2608(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3968(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2584(%rbp)
	movq	%rax, -2616(%rbp)
	movq	$0, -2592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3808(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2424(%rbp)
	movq	$0, -2416(%rbp)
	movq	%rax, -2432(%rbp)
	movq	$0, -2408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2376(%rbp), %rcx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rdx, -2408(%rbp)
	movq	%rdx, -2416(%rbp)
	movq	%rcx, -3984(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movups	%xmm0, -2392(%rbp)
	movq	%rax, -2424(%rbp)
	movq	$0, -2400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3808(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2232(%rbp)
	movq	$0, -2224(%rbp)
	movq	%rax, -2240(%rbp)
	movq	$0, -2216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2184(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -2216(%rbp)
	movq	%rdx, -2224(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4000(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2200(%rbp)
	movq	%rax, -2232(%rbp)
	movq	$0, -2208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3808(%rbp), %rax
	movl	$192, %edi
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movq	%rax, -2048(%rbp)
	movq	$0, -2024(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1992(%rbp), %rcx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -2024(%rbp)
	movq	%rdx, -2032(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3856(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2008(%rbp)
	movq	%rax, -2040(%rbp)
	movq	$0, -2016(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3808(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1848(%rbp)
	movq	$0, -1840(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1832(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1800(%rbp), %rcx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -1832(%rbp)
	movq	%rdx, -1840(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3872(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1816(%rbp)
	movq	%rax, -1848(%rbp)
	movq	$0, -1824(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3808(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1656(%rbp)
	movq	$0, -1648(%rbp)
	movq	%rax, -1664(%rbp)
	movq	$0, -1640(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1608(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -1640(%rbp)
	movq	%rdx, -1648(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3848(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1624(%rbp)
	movq	%rax, -1656(%rbp)
	movq	$0, -1632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3808(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1464(%rbp)
	movq	$0, -1456(%rbp)
	movq	%rax, -1472(%rbp)
	movq	$0, -1448(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1416(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -1448(%rbp)
	movq	%rdx, -1456(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4032(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1432(%rbp)
	movq	%rax, -1464(%rbp)
	movq	$0, -1440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3808(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1272(%rbp)
	movq	$0, -1264(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -1256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1224(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -1256(%rbp)
	movq	%rdx, -1264(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3880(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1240(%rbp)
	movq	%rax, -1272(%rbp)
	movq	$0, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3808(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1080(%rbp)
	movq	$0, -1072(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$0, -1064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1032(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -1064(%rbp)
	movq	%rdx, -1072(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3888(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1048(%rbp)
	movq	%rax, -1080(%rbp)
	movq	$0, -1056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3808(%rbp), %rax
	movl	$96, %edi
	movq	$0, -888(%rbp)
	movq	$0, -880(%rbp)
	movq	%rax, -896(%rbp)
	movq	$0, -872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-840(%rbp), %rcx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -872(%rbp)
	movq	%rdx, -880(%rbp)
	movq	%rcx, -3840(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movups	%xmm0, -856(%rbp)
	movq	%rax, -888(%rbp)
	movq	$0, -864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3808(%rbp), %rax
	movl	$96, %edi
	movq	$0, -696(%rbp)
	movq	$0, -688(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-648(%rbp), %rcx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -680(%rbp)
	movq	%rdx, -688(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4016(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -664(%rbp)
	movq	%rax, -696(%rbp)
	movq	$0, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3808(%rbp), %rax
	movl	$96, %edi
	movq	$0, -504(%rbp)
	movq	$0, -496(%rbp)
	movq	%rax, -512(%rbp)
	movq	$0, -488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-456(%rbp), %rcx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -488(%rbp)
	movq	%rdx, -496(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4040(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -472(%rbp)
	movq	%rax, -504(%rbp)
	movq	$0, -480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3808(%rbp), %rax
	movl	$72, %edi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	leaq	-264(%rbp), %rsi
	movq	$0, 8(%rax)
	movq	%rsi, %rdi
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -4008(%rbp)
	movq	%r12, %rsi
	movq	%rax, -312(%rbp)
	movups	%xmm0, -280(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	-3824(%rbp), %xmm1
	movaps	%xmm0, -3712(%rbp)
	movhps	-3832(%rbp), %xmm1
	movq	%rbx, -112(%rbp)
	movaps	%xmm1, -128(%rbp)
	movq	$0, -3696(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movdqa	-128(%rbp), %xmm2
	movq	%r13, %rsi
	movq	-3816(%rbp), %rdi
	leaq	24(%rax), %rdx
	movq	%rax, -3712(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -3696(%rbp)
	movq	%rdx, -3704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L80
	call	_ZdlPv@PLT
.L80:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3200(%rbp), %rax
	cmpq	$0, -3520(%rbp)
	movq	%rax, -3832(%rbp)
	leaq	-3392(%rbp), %rax
	movq	%rax, -3824(%rbp)
	jne	.L318
.L81:
	leaq	-320(%rbp), %rax
	cmpq	$0, -3328(%rbp)
	movq	%rax, -3920(%rbp)
	jne	.L319
.L86:
	leaq	-2816(%rbp), %rax
	cmpq	$0, -3136(%rbp)
	movq	%rax, -3864(%rbp)
	jne	.L320
.L89:
	leaq	-2624(%rbp), %rax
	cmpq	$0, -2944(%rbp)
	movq	%rax, -3904(%rbp)
	jne	.L321
.L94:
	leaq	-2432(%rbp), %rax
	cmpq	$0, -2752(%rbp)
	movq	%rax, -3936(%rbp)
	jne	.L322
	cmpq	$0, -2560(%rbp)
	jne	.L323
.L100:
	leaq	-2240(%rbp), %rax
	cmpq	$0, -2368(%rbp)
	movq	%rax, -3944(%rbp)
	jne	.L324
	cmpq	$0, -2176(%rbp)
	jne	.L325
.L110:
	cmpq	$0, -1984(%rbp)
	jne	.L326
.L113:
	cmpq	$0, -1792(%rbp)
	jne	.L327
.L115:
	leaq	-1472(%rbp), %rax
	cmpq	$0, -1600(%rbp)
	movq	%rax, -3968(%rbp)
	jne	.L328
	cmpq	$0, -1408(%rbp)
	jne	.L329
.L121:
	cmpq	$0, -1216(%rbp)
	jne	.L330
.L122:
	cmpq	$0, -1024(%rbp)
	jne	.L331
.L123:
	leaq	-704(%rbp), %rax
	cmpq	$0, -832(%rbp)
	leaq	-512(%rbp), %r14
	movq	%rax, -3984(%rbp)
	jne	.L332
	cmpq	$0, -640(%rbp)
	jne	.L333
.L127:
	cmpq	$0, -448(%rbp)
	jne	.L334
.L128:
	cmpq	$0, -256(%rbp)
	jne	.L335
.L129:
	movq	-3920(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3984(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3840(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-864(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L131
	call	_ZdlPv@PLT
.L131:
	movq	-880(%rbp), %rbx
	movq	-888(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L132
	.p2align 4,,10
	.p2align 3
.L136:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L133
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L136
.L134:
	movq	-888(%rbp), %r13
.L132:
	testq	%r13, %r13
	je	.L137
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L137:
	movq	-3888(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L138
	call	_ZdlPv@PLT
.L138:
	movq	-1072(%rbp), %rbx
	movq	-1080(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L139
	.p2align 4,,10
	.p2align 3
.L143:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L140
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L143
.L141:
	movq	-1080(%rbp), %r13
.L139:
	testq	%r13, %r13
	je	.L144
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L144:
	movq	-3880(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L145
	call	_ZdlPv@PLT
.L145:
	movq	-1264(%rbp), %rbx
	movq	-1272(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L146
	.p2align 4,,10
	.p2align 3
.L150:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L147
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L150
.L148:
	movq	-1272(%rbp), %r13
.L146:
	testq	%r13, %r13
	je	.L151
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L151:
	movq	-3968(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3848(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1632(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L152
	call	_ZdlPv@PLT
.L152:
	movq	-1648(%rbp), %rbx
	movq	-1656(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L153
	.p2align 4,,10
	.p2align 3
.L157:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L154
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L157
.L155:
	movq	-1656(%rbp), %r13
.L153:
	testq	%r13, %r13
	je	.L158
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L158:
	movq	-3872(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L159
	call	_ZdlPv@PLT
.L159:
	movq	-1840(%rbp), %rbx
	movq	-1848(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L160
	.p2align 4,,10
	.p2align 3
.L164:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L161
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L164
.L162:
	movq	-1848(%rbp), %r13
.L160:
	testq	%r13, %r13
	je	.L165
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L165:
	movq	-3856(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2016(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L166
	call	_ZdlPv@PLT
.L166:
	movq	-2032(%rbp), %rbx
	movq	-2040(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L167
	.p2align 4,,10
	.p2align 3
.L171:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L168
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L171
.L169:
	movq	-2040(%rbp), %r13
.L167:
	testq	%r13, %r13
	je	.L172
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L172:
	movq	-3944(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3936(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3904(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3864(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3952(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2976(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L173
	call	_ZdlPv@PLT
.L173:
	movq	-2992(%rbp), %rbx
	movq	-3000(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L174
	.p2align 4,,10
	.p2align 3
.L178:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L175
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L178
.L176:
	movq	-3000(%rbp), %r13
.L174:
	testq	%r13, %r13
	je	.L179
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L179:
	movq	-3832(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3824(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3816(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L336
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L178
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L168:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L171
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L161:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L164
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L154:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L157
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L147:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L150
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L133:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L136
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L140:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L143
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L318:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$1799, %ebx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -3696(%rbp)
	movaps	%xmm0, -3712(%rbp)
	call	_Znwm@PLT
	movq	-3816(%rbp), %rdi
	movq	%r13, %rsi
	movw	%bx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -3712(%rbp)
	movq	%rdx, -3696(%rbp)
	movq	%rdx, -3704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L82
	call	_ZdlPv@PLT
.L82:
	movq	(%rbx), %rax
	movl	$14, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %rax
	movq	%rcx, -3824(%rbp)
	movq	%rax, -3832(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$22, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-3824(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal20Cast10JSReceiver_140EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm4
	pxor	%xmm0, %xmm0
	movq	-3832(%rbp), %xmm3
	movl	$40, %edi
	movq	%rax, -96(%rbp)
	leaq	-3744(%rbp), %r14
	punpcklqdq	%xmm4, %xmm3
	movq	%rbx, %xmm4
	movaps	%xmm0, -3744(%rbp)
	movhps	-3824(%rbp), %xmm4
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm3, -3920(%rbp)
	movaps	%xmm4, -3936(%rbp)
	movaps	%xmm4, -128(%rbp)
	movq	$0, -3728(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm5
	leaq	40(%rax), %rdx
	movq	%rax, -3744(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	leaq	-3200(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3728(%rbp)
	movq	%rdx, -3736(%rbp)
	movq	%rax, -3832(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L83
	call	_ZdlPv@PLT
.L83:
	movq	-3904(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3392(%rbp), %rax
	cmpq	$0, -3704(%rbp)
	movq	%rax, -3824(%rbp)
	jne	.L337
.L84:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L319:
	movq	-3864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -3696(%rbp)
	movaps	%xmm0, -3712(%rbp)
	call	_Znwm@PLT
	movq	-3824(%rbp), %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -3712(%rbp)
	movq	%rdx, -3696(%rbp)
	movq	%rdx, -3704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L87
	call	_ZdlPv@PLT
.L87:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm6
	movaps	%xmm0, -3712(%rbp)
	movq	$0, -3696(%rbp)
	movq	%rdx, -112(%rbp)
	movaps	%xmm6, -128(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movdqa	-128(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	movq	%rax, -3712(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	leaq	-320(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3696(%rbp)
	movq	%rdx, -3704(%rbp)
	movq	%rax, -3920(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L88
	call	_ZdlPv@PLT
.L88:
	movq	-4008(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L320:
	movq	-3904(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -3696(%rbp)
	movaps	%xmm0, -3712(%rbp)
	call	_Znwm@PLT
	movq	-3832(%rbp), %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -3712(%rbp)
	movq	%rdx, -3696(%rbp)
	movq	%rdx, -3704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L90
	call	_ZdlPv@PLT
.L90:
	movq	(%rbx), %rax
	movl	$25, %edx
	movq	%r12, %rdi
	movq	16(%rax), %rsi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	32(%rax), %rax
	movq	%rsi, -4064(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rcx, -3904(%rbp)
	movq	%rax, -3864(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	leaq	-3744(%rbp), %r14
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-3904(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -3936(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$29, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %r8
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	-3864(%rbp), %rdx
	leaq	.LC2(%rip), %rcx
	call	_ZN2v88internal13GetMethod_245EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPKcPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm7
	pxor	%xmm0, %xmm0
	movq	-3936(%rbp), %xmm5
	movhps	-3904(%rbp), %xmm7
	movl	$56, %edi
	movq	%rax, -80(%rbp)
	movq	-4064(%rbp), %xmm6
	movhps	-3864(%rbp), %xmm5
	movaps	%xmm7, -3904(%rbp)
	movhps	-3864(%rbp), %xmm6
	movaps	%xmm5, -4080(%rbp)
	movaps	%xmm6, -3936(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm0, -3744(%rbp)
	movq	$0, -3728(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm1
	movdqa	-96(%rbp), %xmm5
	leaq	56(%rax), %rdx
	movq	%rax, -3744(%rbp)
	movq	%rcx, 48(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm5, 32(%rax)
	leaq	-2816(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3728(%rbp)
	movq	%rdx, -3736(%rbp)
	movq	%rax, -3864(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L91
	call	_ZdlPv@PLT
.L91:
	movq	-3944(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3704(%rbp)
	jne	.L338
.L92:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L321:
	movq	-3952(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-3008(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -3696(%rbp)
	movaps	%xmm0, -3712(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r11d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	6(%rax), %rdx
	movw	%r11w, 4(%rax)
	movq	%rax, -3712(%rbp)
	movq	%rdx, -3696(%rbp)
	movq	%rdx, -3704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L95
	call	_ZdlPv@PLT
.L95:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	32(%rax), %rdx
	movdqu	(%rax), %xmm6
	movq	$0, -3696(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm0, -3712(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm4
	leaq	40(%rax), %rdx
	movq	%rax, -3712(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	leaq	-2624(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3696(%rbp)
	movq	%rdx, -3704(%rbp)
	movq	%rax, -3904(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L96
	call	_ZdlPv@PLT
.L96:
	movq	-3968(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L322:
	movq	-3944(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -3696(%rbp)
	movaps	%xmm0, -3712(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r10d
	movq	-3864(%rbp), %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	7(%rax), %rdx
	movw	%r10w, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -3712(%rbp)
	movq	%rdx, -3696(%rbp)
	movq	%rdx, -3704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L98
	call	_ZdlPv@PLT
.L98:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	16(%rax), %rdi
	movq	8(%rax), %r8
	movq	24(%rax), %rsi
	movq	32(%rax), %rcx
	movq	48(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdi, -112(%rbp)
	movl	$48, %edi
	movq	%r8, -120(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movaps	%xmm0, -3712(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -3696(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm2
	movdqa	-112(%rbp), %xmm1
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm5
	leaq	48(%rax), %rdx
	movq	%rax, -3712(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm5, 32(%rax)
	leaq	-2432(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3696(%rbp)
	movq	%rdx, -3704(%rbp)
	movq	%rax, -3936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L99
	call	_ZdlPv@PLT
.L99:
	movq	-3984(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2560(%rbp)
	je	.L100
.L323:
	movq	-3968(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -3696(%rbp)
	movaps	%xmm0, -3712(%rbp)
	call	_Znwm@PLT
	movq	-3904(%rbp), %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -3712(%rbp)
	movq	%rdx, -3696(%rbp)
	movq	%rdx, -3704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L101
	call	_ZdlPv@PLT
.L101:
	movq	(%rbx), %rax
	movl	$30, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rbx
	movq	(%rax), %r14
	movq	%rbx, -3944(%rbp)
	movq	16(%rax), %rbx
	movq	32(%rax), %rax
	movq	%rax, -3968(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %xmm0
	movl	$32, %edi
	movq	$0, -3696(%rbp)
	movhps	-3944(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%rbx, %xmm0
	movhps	-3968(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3712(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-896(%rbp), %rdi
	movq	%rax, -3712(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -3696(%rbp)
	movq	%rdx, -3704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L102
	call	_ZdlPv@PLT
.L102:
	movq	-3840(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L324:
	movq	-3984(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -3696(%rbp)
	movaps	%xmm0, -3712(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r9d
	movq	-3936(%rbp), %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	6(%rax), %rdx
	movw	%r9w, 4(%rax)
	movq	%rax, -3712(%rbp)
	movq	%rdx, -3696(%rbp)
	movq	%rdx, -3704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L104
	call	_ZdlPv@PLT
.L104:
	movq	(%rbx), %rax
	movl	$29, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	leaq	-3760(%rbp), %r14
	movq	(%rax), %rbx
	movq	32(%rax), %rcx
	movq	%rbx, -3968(%rbp)
	movq	8(%rax), %rbx
	movq	%rcx, -3984(%rbp)
	movq	%rbx, -4080(%rbp)
	movq	16(%rax), %rbx
	movq	%rbx, -4048(%rbp)
	movq	24(%rax), %rbx
	movq	40(%rax), %rax
	movq	%rbx, -3944(%rbp)
	movq	%rax, -4064(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$34, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19IsUndefinedConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	je	.L105
.L107:
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	xorl	%edx, %edx
	movq	%rax, %rsi
.L317:
	movq	%r13, %rdi
	movl	$4, %ebx
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	%r14, %rdi
	movl	$1, %esi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-3712(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -4088(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-128(%rbp), %rcx
	pushq	%rbx
	xorl	%esi, %esi
	movq	-4064(%rbp), %xmm0
	pushq	%rcx
	movq	%rax, %r8
	movq	%r14, %rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-3968(%rbp), %r9
	movl	$1, %ecx
	leaq	-3744(%rbp), %rdx
	movhps	-4088(%rbp), %xmm0
	movq	%rax, -3744(%rbp)
	movq	-3696(%rbp), %rax
	movaps	%xmm0, -128(%rbp)
	movq	-3944(%rbp), %xmm0
	movq	%rax, -3736(%rbp)
	movhps	-3984(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	popq	%rdi
	movq	%r14, %rdi
	popq	%r8
	movq	-3968(%rbp), %xmm2
	movq	%rax, %rbx
	movq	-3984(%rbp), %xmm4
	movq	-4048(%rbp), %xmm3
	movhps	-4080(%rbp), %xmm2
	movhps	-4064(%rbp), %xmm4
	movhps	-3944(%rbp), %xmm3
	movaps	%xmm2, -3968(%rbp)
	movaps	%xmm3, -4080(%rbp)
	movaps	%xmm4, -3984(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$39, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal13ToBoolean_240EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEE@PLT
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movdqa	-3968(%rbp), %xmm2
	movdqa	-4080(%rbp), %xmm3
	movdqa	-3984(%rbp), %xmm4
	movq	%rbx, -80(%rbp)
	movq	%rax, %r14
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm0, -3712(%rbp)
	movq	$0, -3696(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm1
	leaq	56(%rax), %rdx
	movq	%rax, -3712(%rbp)
	movq	%rcx, 48(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm1, 32(%rax)
	leaq	-2240(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3696(%rbp)
	movq	%rdx, -3704(%rbp)
	movq	%rax, -3944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L108
	call	_ZdlPv@PLT
.L108:
	movdqa	-3968(%rbp), %xmm5
	movdqa	-4080(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movdqa	-3984(%rbp), %xmm7
	movq	%rbx, -80(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm0, -3712(%rbp)
	movq	$0, -3696(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm2
	leaq	56(%rax), %rdx
	leaq	-1664(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm2, 32(%rax)
	movq	%rax, -3712(%rbp)
	movq	%rdx, -3696(%rbp)
	movq	%rdx, -3704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L109
	call	_ZdlPv@PLT
.L109:
	movq	-3848(%rbp), %rcx
	movq	-4000(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2176(%rbp)
	je	.L110
.L325:
	movq	-4000(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3800(%rbp)
	movq	$0, -3792(%rbp)
	movq	$0, -3784(%rbp)
	movq	$0, -3776(%rbp)
	movq	$0, -3768(%rbp)
	movq	$0, -3760(%rbp)
	movq	$0, -3744(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3744(%rbp), %rax
	movq	-3944(%rbp), %rdi
	leaq	-3784(%rbp), %rcx
	pushq	%rax
	leaq	-3760(%rbp), %rax
	leaq	-3768(%rbp), %r9
	pushq	%rax
	leaq	-3776(%rbp), %r8
	leaq	-3792(%rbp), %rdx
	leaq	-3800(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_7OddballENS0_10JSReceiverENS0_10HeapObjectES6_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESI_PNSA_IS8_EE
	movl	$40, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3768(%rbp), %rdx
	movq	-3800(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal22ObjectIsExtensible_311EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$42, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3744(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	-3768(%rbp), %xmm5
	movq	-3784(%rbp), %xmm6
	movl	$64, %edi
	movq	%rbx, -72(%rbp)
	movq	-3800(%rbp), %xmm7
	movhps	-3760(%rbp), %xmm5
	movq	%rax, -80(%rbp)
	movhps	-3776(%rbp), %xmm6
	movaps	%xmm5, -4064(%rbp)
	movhps	-3792(%rbp), %xmm7
	movaps	%xmm6, -4000(%rbp)
	movaps	%xmm7, -3984(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm0, -3712(%rbp)
	movq	%rax, -3968(%rbp)
	movq	$0, -3696(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm1
	movdqa	-112(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	leaq	64(%rax), %rdx
	leaq	-2048(%rbp), %rdi
	movups	%xmm1, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rax, -3712(%rbp)
	movq	%rdx, -3696(%rbp)
	movq	%rdx, -3704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3712(%rbp), %rdi
	popq	%rcx
	popq	%rsi
	testq	%rdi, %rdi
	je	.L111
	call	_ZdlPv@PLT
.L111:
	movq	-3968(%rbp), %xmm0
	movq	%rbx, %xmm1
	movdqa	-3984(%rbp), %xmm3
	movl	$64, %edi
	movdqa	-4000(%rbp), %xmm4
	movdqa	-4064(%rbp), %xmm2
	movq	$0, -3696(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -3712(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm7
	movdqa	-80(%rbp), %xmm3
	leaq	64(%rax), %rdx
	leaq	-1856(%rbp), %rdi
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rax, -3712(%rbp)
	movq	%rdx, -3696(%rbp)
	movq	%rdx, -3704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L112
	call	_ZdlPv@PLT
.L112:
	movq	-3872(%rbp), %rcx
	movq	-3856(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1984(%rbp)
	je	.L113
.L326:
	movq	-3856(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$578720278881175303, %rbx
	leaq	-2048(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3696(%rbp)
	movaps	%xmm0, -3712(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3712(%rbp)
	movq	%rdx, -3696(%rbp)
	movq	%rdx, -3704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L114
	call	_ZdlPv@PLT
.L114:
	movq	(%rbx), %rax
	movl	$43, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$143, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -1792(%rbp)
	je	.L115
.L327:
	movq	-3872(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$578720278881175303, %rbx
	leaq	-1856(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3696(%rbp)
	movaps	%xmm0, -3712(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3712(%rbp)
	movq	%rdx, -3696(%rbp)
	movq	%rdx, -3704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L116
	call	_ZdlPv@PLT
.L116:
	movq	(%rbx), %rax
	movl	$39, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	24(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r14
	movq	%rsi, -3984(%rbp)
	movq	32(%rax), %rsi
	movq	%rcx, -4000(%rbp)
	movq	40(%rax), %rcx
	movq	%rsi, -4064(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rcx, -4080(%rbp)
	movq	%rbx, -3968(%rbp)
	movq	48(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$56, %edi
	movq	-3968(%rbp), %xmm0
	movq	$0, -3696(%rbp)
	movq	%rbx, -80(%rbp)
	movhps	-3984(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%r14, %xmm0
	movhps	-4000(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-4064(%rbp), %xmm0
	movhps	-4080(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3712(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm1
	leaq	56(%rax), %rdx
	leaq	-1088(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm1, 32(%rax)
	movq	%rax, -3712(%rbp)
	movq	%rdx, -3696(%rbp)
	movq	%rdx, -3704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L117
	call	_ZdlPv@PLT
.L117:
	movq	-3888(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L328:
	movq	-3848(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3800(%rbp)
	leaq	-1664(%rbp), %r14
	movq	$0, -3792(%rbp)
	movq	$0, -3784(%rbp)
	movq	$0, -3776(%rbp)
	movq	$0, -3768(%rbp)
	movq	$0, -3760(%rbp)
	movq	$0, -3744(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3744(%rbp), %rax
	movq	%r14, %rdi
	leaq	-3768(%rbp), %r9
	pushq	%rax
	leaq	-3760(%rbp), %rax
	leaq	-3776(%rbp), %r8
	pushq	%rax
	leaq	-3784(%rbp), %rcx
	leaq	-3792(%rbp), %rdx
	leaq	-3800(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_7OddballENS0_10JSReceiverENS0_10HeapObjectES6_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESI_PNSA_IS8_EE
	movl	$46, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3784(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3768(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	-3792(%rbp), %rsi
	movq	-3784(%rbp), %rcx
	movq	-3776(%rbp), %rdx
	movaps	%xmm0, -3712(%rbp)
	movq	-3760(%rbp), %r10
	movq	-3800(%rbp), %rax
	movq	%rdi, -96(%rbp)
	movq	-3744(%rbp), %rbx
	movq	%rdi, -4048(%rbp)
	movl	$56, %edi
	movq	%rsi, -4000(%rbp)
	movq	%rcx, -4064(%rbp)
	movq	%rdx, -4080(%rbp)
	movq	%r10, -4088(%rbp)
	movq	%rsi, -120(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%r10, -88(%rbp)
	movq	%rax, -3984(%rbp)
	movq	%rax, -128(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -3696(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	56(%rax), %rdx
	movq	%rax, -3712(%rbp)
	movq	%rcx, 48(%rax)
	movq	-3968(%rbp), %rdi
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -3696(%rbp)
	movq	%rdx, -3704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3712(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L119
	call	_ZdlPv@PLT
.L119:
	movq	-3984(%rbp), %xmm0
	movl	$56, %edi
	movq	%rbx, -80(%rbp)
	movq	$0, -3696(%rbp)
	movhps	-4000(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-4064(%rbp), %xmm0
	movhps	-4080(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-4048(%rbp), %xmm0
	movhps	-4088(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3712(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm2
	leaq	56(%rax), %rdx
	leaq	-1280(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm2, 32(%rax)
	movq	%rax, -3712(%rbp)
	movq	%rdx, -3696(%rbp)
	movq	%rdx, -3704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L120
	call	_ZdlPv@PLT
.L120:
	movq	-3880(%rbp), %rcx
	movq	-4032(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1408(%rbp)
	je	.L121
.L329:
	movq	-4032(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3800(%rbp)
	movq	$0, -3792(%rbp)
	movq	$0, -3784(%rbp)
	movq	$0, -3776(%rbp)
	movq	$0, -3768(%rbp)
	movq	$0, -3760(%rbp)
	movq	$0, -3744(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3744(%rbp), %rax
	movq	-3968(%rbp), %rdi
	leaq	-3768(%rbp), %r9
	pushq	%rax
	leaq	-3760(%rbp), %rax
	leaq	-3784(%rbp), %rcx
	pushq	%rax
	leaq	-3776(%rbp), %r8
	leaq	-3792(%rbp), %rdx
	leaq	-3800(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_7OddballENS0_10JSReceiverENS0_10HeapObjectES6_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESI_PNSA_IS8_EE
	movl	$47, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	movl	$149, %edx
	movq	%r13, %rdi
	movq	-3800(%rbp), %rsi
	leaq	.LC2(%rip), %rcx
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -1216(%rbp)
	popq	%r10
	popq	%r11
	je	.L122
.L330:
	movq	-3880(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3800(%rbp)
	leaq	-1280(%rbp), %r14
	movq	$0, -3792(%rbp)
	movq	$0, -3784(%rbp)
	movq	$0, -3776(%rbp)
	movq	$0, -3768(%rbp)
	movq	$0, -3760(%rbp)
	movq	$0, -3744(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3744(%rbp), %rax
	movq	%r14, %rdi
	leaq	-3768(%rbp), %r9
	pushq	%rax
	leaq	-3760(%rbp), %rax
	leaq	-3776(%rbp), %r8
	pushq	%rax
	leaq	-3784(%rbp), %rcx
	leaq	-3792(%rbp), %rdx
	leaq	-3800(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_7OddballENS0_10JSReceiverENS0_10HeapObjectES6_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESI_PNSA_IS8_EE
	movl	$49, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8False_66EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -1024(%rbp)
	popq	%r8
	popq	%r9
	je	.L123
.L331:
	movq	-3888(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3800(%rbp)
	leaq	-1088(%rbp), %r14
	movq	$0, -3792(%rbp)
	movq	$0, -3784(%rbp)
	movq	$0, -3776(%rbp)
	movq	$0, -3768(%rbp)
	movq	$0, -3760(%rbp)
	movq	$0, -3744(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3744(%rbp), %rax
	movq	%r14, %rdi
	leaq	-3784(%rbp), %rcx
	pushq	%rax
	leaq	-3760(%rbp), %rax
	leaq	-3768(%rbp), %r9
	pushq	%rax
	leaq	-3776(%rbp), %r8
	leaq	-3792(%rbp), %rdx
	leaq	-3800(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_7OddballENS0_10JSReceiverENS0_10HeapObjectES6_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESI_PNSA_IS8_EE
	movl	$53, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rsi
	popq	%rdi
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L332:
	movq	-3840(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-896(%rbp), %r14
	movq	$0, -3776(%rbp)
	movq	$0, -3768(%rbp)
	movq	$0, -3760(%rbp)
	movq	$0, -3744(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3760(%rbp), %rcx
	movq	%r14, %rdi
	leaq	-3744(%rbp), %r8
	leaq	-3768(%rbp), %rdx
	leaq	-3776(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_7OddballENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE
	movl	$57, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3760(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-3760(%rbp), %xmm2
	movq	-3776(%rbp), %xmm3
	movaps	%xmm0, -3712(%rbp)
	movhps	-3744(%rbp), %xmm2
	movq	$0, -3696(%rbp)
	movhps	-3768(%rbp), %xmm3
	movaps	%xmm2, -4000(%rbp)
	movaps	%xmm3, -4032(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm1
	movdqa	-112(%rbp), %xmm5
	movq	%r13, %rsi
	movq	-3984(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -3712(%rbp)
	movups	%xmm1, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -3696(%rbp)
	movq	%rdx, -3704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L125
	call	_ZdlPv@PLT
.L125:
	pxor	%xmm0, %xmm0
	movdqa	-4032(%rbp), %xmm6
	movdqa	-4000(%rbp), %xmm7
	movl	$32, %edi
	movaps	%xmm0, -3712(%rbp)
	leaq	-512(%rbp), %r14
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movq	$0, -3696(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm3
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm4
	leaq	32(%rax), %rdx
	movq	%rax, -3712(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -3696(%rbp)
	movq	%rdx, -3704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L126
	call	_ZdlPv@PLT
.L126:
	movq	-4040(%rbp), %rcx
	movq	-4016(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -640(%rbp)
	je	.L127
.L333:
	movq	-4016(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3776(%rbp)
	movq	$0, -3768(%rbp)
	movq	$0, -3760(%rbp)
	movq	$0, -3744(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3984(%rbp), %rdi
	leaq	-3760(%rbp), %rcx
	leaq	-3744(%rbp), %r8
	leaq	-3768(%rbp), %rdx
	leaq	-3776(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_7OddballENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE
	movl	$58, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3744(%rbp), %rdx
	movq	-3776(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal32ObjectPreventExtensionsThrow_312EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -448(%rbp)
	je	.L128
.L334:
	movq	-4040(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3776(%rbp)
	movq	$0, -3768(%rbp)
	movq	$0, -3760(%rbp)
	movq	$0, -3744(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3760(%rbp), %rcx
	movq	%r14, %rdi
	leaq	-3744(%rbp), %r8
	leaq	-3768(%rbp), %rdx
	leaq	-3776(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_7OddballENS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EE
	movl	$60, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3744(%rbp), %rdx
	movq	-3776(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal36ObjectPreventExtensionsDontThrow_313EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -256(%rbp)
	je	.L129
.L335:
	movq	-4008(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -3696(%rbp)
	movaps	%xmm0, -3712(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	-3920(%rbp), %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -3712(%rbp)
	movq	%rdx, -3696(%rbp)
	movq	%rdx, -3704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L130
	call	_ZdlPv@PLT
.L130:
	movq	(%rbx), %rax
	movl	$63, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movl	$145, %edx
	leaq	.LC2(%rip), %rcx
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L105:
	movq	-3944(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IsNullConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L107
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	movq	%rax, %rsi
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L337:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-3936(%rbp), %xmm2
	movdqa	-3920(%rbp), %xmm1
	movaps	%xmm0, -3744(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movq	$0, -3728(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm2
	movdqa	-112(%rbp), %xmm1
	movq	%r14, %rsi
	movq	-3824(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -3744(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm1, 16(%rax)
	movq	%rdx, -3728(%rbp)
	movq	%rdx, -3736(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L85
	call	_ZdlPv@PLT
.L85:
	movq	-3864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L338:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movdqa	-3904(%rbp), %xmm6
	movdqa	-3936(%rbp), %xmm7
	movdqa	-4080(%rbp), %xmm5
	movaps	%xmm0, -3744(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movq	$0, -3728(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm2
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm1
	movdqa	-96(%rbp), %xmm6
	leaq	48(%rax), %rdx
	leaq	-3008(%rbp), %rdi
	movq	%rax, -3744(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -3728(%rbp)
	movq	%rdx, -3736(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L93
	call	_ZdlPv@PLT
.L93:
	movq	-3952(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L92
.L336:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22418:
	.size	_ZN2v88internal31ProxyPreventExtensionsAssembler34GenerateProxyPreventExtensionsImplEv, .-_ZN2v88internal31ProxyPreventExtensionsAssembler34GenerateProxyPreventExtensionsImplEv
	.section	.rodata._ZN2v88internal8Builtins31Generate_ProxyPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/proxy-prevent-extensions-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins31Generate_ProxyPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"ProxyPreventExtensions"
	.section	.text._ZN2v88internal8Builtins31Generate_ProxyPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins31Generate_ProxyPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins31Generate_ProxyPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins31Generate_ProxyPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE:
.LFB22414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$146, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$856, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L343
.L340:
	movq	%r13, %rdi
	call	_ZN2v88internal31ProxyPreventExtensionsAssembler34GenerateProxyPreventExtensionsImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L344
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L343:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L340
.L344:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22414:
	.size	_ZN2v88internal8Builtins31Generate_ProxyPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins31Generate_ProxyPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins31Generate_ProxyPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins31Generate_ProxyPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins31Generate_ProxyPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE:
.LFB29014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29014:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins31Generate_ProxyPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins31Generate_ProxyPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins31Generate_ProxyPreventExtensionsEPNS0_8compiler18CodeAssemblerStateE
	.weak	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
