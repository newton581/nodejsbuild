	.file	"regexp-match-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.rodata._ZN2v88internal32FastRegExpPrototypeMatchBody_320EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../../deps/v8/../../deps/v8/src/builtins/regexp-match.tq"
	.section	.text._ZN2v88internal32FastRegExpPrototypeMatchBody_320EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32FastRegExpPrototypeMatchBody_320EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEE
	.type	_ZN2v88internal32FastRegExpPrototypeMatchBody_320EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEE, @function
_ZN2v88internal32FastRegExpPrototypeMatchBody_320EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEE:
.LFB22410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-616(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-712(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-672(%rbp), %rbx
	subq	$712, %rsp
	movq	%rsi, -752(%rbp)
	movq	%rdx, -744(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -712(%rbp)
	movq	%rdi, -672(%rbp)
	movl	$72, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -664(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-712(%rbp), %rax
	movl	$96, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -736(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-712(%rbp), %rax
	movl	$96, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -280(%rbp)
	leaq	-232(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -728(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-752(%rbp), %r10
	pxor	%xmm0, %xmm0
	movq	-744(%rbp), %r9
	movl	$24, %edi
	movq	%r13, -80(%rbp)
	leaq	-704(%rbp), %r13
	movq	%r10, -96(%rbp)
	movq	%r9, -88(%rbp)
	movaps	%xmm0, -704(%rbp)
	movq	$0, -688(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm1
	leaq	24(%rax), %rdx
	movq	%rax, -704(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm1, (%rax)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L8
	call	_ZdlPv@PLT
.L8:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -608(%rbp)
	jne	.L76
.L9:
	cmpq	$0, -416(%rbp)
	leaq	-288(%rbp), %r15
	jne	.L77
.L12:
	movq	-728(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -688(%rbp)
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$134678279, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -704(%rbp)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-704(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L15
	call	_ZdlPv@PLT
.L15:
	movq	(%rbx), %rax
	movq	-728(%rbp), %rdi
	movq	24(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L16
	call	_ZdlPv@PLT
.L16:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L17
	.p2align 4,,10
	.p2align 3
.L21:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L18
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L21
.L19:
	movq	-280(%rbp), %r15
.L17:
	testq	%r15, %r15
	je	.L22
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L22:
	movq	-736(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L23
	call	_ZdlPv@PLT
.L23:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L24
	.p2align 4,,10
	.p2align 3
.L28:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L25
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L28
.L26:
	movq	-472(%rbp), %r15
.L24:
	testq	%r15, %r15
	je	.L29
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L29:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L30
	call	_ZdlPv@PLT
.L30:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L31
	.p2align 4,,10
	.p2align 3
.L35:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L32
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L35
.L33:
	movq	-664(%rbp), %r14
.L31:
	testq	%r14, %r14
	je	.L36
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L36:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L78
	addq	$712, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L35
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L18:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L21
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L25:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L28
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L76:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -688(%rbp)
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -704(%rbp)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-704(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	movq	(%rbx), %rax
	movl	$14, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %rax
	movq	%rcx, -744(%rbp)
	movq	%rax, -752(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-752(%rbp), %r15
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	-744(%rbp), %rdx
	movl	$1, %r8d
	movq	%r15, %rcx
	call	_ZN2v88internal23RegExpBuiltinsAssembler24RegExpPrototypeMatchBodyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_6StringEEEb@PLT
	movq	%r13, %rdi
	movq	%rax, -752(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm0
	movl	$32, %edi
	movq	$0, -688(%rbp)
	movhps	-744(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%r15, %xmm0
	movhps	-752(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -704(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L11
	call	_ZdlPv@PLT
.L11:
	movq	-736(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L77:
	movq	-736(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -688(%rbp)
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$134678279, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -704(%rbp)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-704(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L13
	call	_ZdlPv@PLT
.L13:
	movq	(%rbx), %rax
	movl	$12, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	16(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rcx, -744(%rbp)
	movq	%rax, -752(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %xmm0
	movl	$32, %edi
	movq	$0, -688(%rbp)
	movhps	-744(%rbp), %xmm0
	leaq	-288(%rbp), %r15
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	movhps	-752(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-80(%rbp), %xmm5
	leaq	32(%rax), %rdx
	movq	%rax, -704(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L14
	call	_ZdlPv@PLT
.L14:
	movq	-728(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L12
.L78:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22410:
	.size	_ZN2v88internal32FastRegExpPrototypeMatchBody_320EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEE, .-_ZN2v88internal32FastRegExpPrototypeMatchBody_320EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEE
	.section	.text._ZN2v88internal32SlowRegExpPrototypeMatchBody_321EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_6StringEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32SlowRegExpPrototypeMatchBody_321EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_6StringEEE
	.type	_ZN2v88internal32SlowRegExpPrototypeMatchBody_321EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_6StringEEE, @function
_ZN2v88internal32SlowRegExpPrototypeMatchBody_321EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_6StringEEE:
.LFB22420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-616(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-712(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-672(%rbp), %rbx
	subq	$712, %rsp
	movq	%rsi, -752(%rbp)
	movq	%rdx, -744(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -712(%rbp)
	movq	%rdi, -672(%rbp)
	movl	$72, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -664(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-712(%rbp), %rax
	movl	$96, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -736(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-712(%rbp), %rax
	movl	$96, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -280(%rbp)
	leaq	-232(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -728(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-752(%rbp), %r10
	pxor	%xmm0, %xmm0
	movq	-744(%rbp), %r9
	movl	$24, %edi
	movq	%r13, -80(%rbp)
	leaq	-704(%rbp), %r13
	movq	%r10, -96(%rbp)
	movq	%r9, -88(%rbp)
	movaps	%xmm0, -704(%rbp)
	movq	$0, -688(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm1
	leaq	24(%rax), %rdx
	movq	%rax, -704(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm1, (%rax)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L80
	call	_ZdlPv@PLT
.L80:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -608(%rbp)
	jne	.L148
.L81:
	cmpq	$0, -416(%rbp)
	leaq	-288(%rbp), %r15
	jne	.L149
.L84:
	movq	-728(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -688(%rbp)
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$134678535, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -704(%rbp)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-704(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L87
	call	_ZdlPv@PLT
.L87:
	movq	(%rbx), %rax
	movq	-728(%rbp), %rdi
	movq	24(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L88
	call	_ZdlPv@PLT
.L88:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L89
	.p2align 4,,10
	.p2align 3
.L93:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L90
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L93
.L91:
	movq	-280(%rbp), %r15
.L89:
	testq	%r15, %r15
	je	.L94
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L94:
	movq	-736(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L95
	call	_ZdlPv@PLT
.L95:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L96
	.p2align 4,,10
	.p2align 3
.L100:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L97
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L100
.L98:
	movq	-472(%rbp), %r15
.L96:
	testq	%r15, %r15
	je	.L101
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L101:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L102
	call	_ZdlPv@PLT
.L102:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L103
	.p2align 4,,10
	.p2align 3
.L107:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L104
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L107
.L105:
	movq	-664(%rbp), %r14
.L103:
	testq	%r14, %r14
	je	.L108
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L108:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L150
	addq	$712, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L107
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L90:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L93
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L97:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L100
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L148:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -688(%rbp)
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -704(%rbp)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-704(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L82
	call	_ZdlPv@PLT
.L82:
	movq	(%rbx), %rax
	movl	$19, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %rax
	movq	%rcx, -744(%rbp)
	movq	%rax, -752(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	-752(%rbp), %r15
	movq	-744(%rbp), %rdx
	movq	%r15, %rcx
	call	_ZN2v88internal23RegExpBuiltinsAssembler24RegExpPrototypeMatchBodyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_6StringEEEb@PLT
	movq	%r13, %rdi
	movq	%rax, -752(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm0
	movl	$32, %edi
	movq	$0, -688(%rbp)
	movhps	-744(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%r15, %xmm0
	movhps	-752(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -704(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L83
	call	_ZdlPv@PLT
.L83:
	movq	-736(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L149:
	movq	-736(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -688(%rbp)
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$134678535, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -704(%rbp)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-704(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L85
	call	_ZdlPv@PLT
.L85:
	movq	(%rbx), %rax
	movl	$17, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	16(%rax), %rbx
	movq	24(%rax), %rax
	movq	%rcx, -744(%rbp)
	movq	%rax, -752(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %xmm0
	movl	$32, %edi
	movq	$0, -688(%rbp)
	movhps	-744(%rbp), %xmm0
	leaq	-288(%rbp), %r15
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	movhps	-752(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-80(%rbp), %xmm5
	leaq	32(%rax), %rdx
	movq	%rax, -704(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L86
	call	_ZdlPv@PLT
.L86:
	movq	-728(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L84
.L150:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22420:
	.size	_ZN2v88internal32SlowRegExpPrototypeMatchBody_321EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_6StringEEE, .-_ZN2v88internal32SlowRegExpPrototypeMatchBody_321EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_6StringEEE
	.section	.text._ZN2v88internal24RegExpMatchFastAssembler27GenerateRegExpMatchFastImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal24RegExpMatchFastAssembler27GenerateRegExpMatchFastImplEv
	.type	_ZN2v88internal24RegExpMatchFastAssembler27GenerateRegExpMatchFastImplEv, @function
_ZN2v88internal24RegExpMatchFastAssembler27GenerateRegExpMatchFastImplEv:
.LFB22435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-216(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-272(%rbp), %rbx
	subq	$312, %rsp
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, -336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	leaq	-312(%rbp), %r12
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$72, %edi
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	%rax, %r14
	movq	-312(%rbp), %rax
	movq	$0, -248(%rbp)
	movq	%rax, -272(%rbp)
	call	_Znwm@PLT
	pxor	%xmm1, %xmm1
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r13, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -248(%rbp)
	movq	%rdx, -256(%rbp)
	xorl	%edx, %edx
	movq	%rax, -264(%rbp)
	movups	%xmm1, -232(%rbp)
	movq	$0, -240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm1, %xmm1
	movl	$24, %edi
	movq	-336(%rbp), %xmm0
	movq	%r14, -64(%rbp)
	leaq	-304(%rbp), %r14
	movhps	-328(%rbp), %xmm0
	movaps	%xmm1, -304(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	$0, -288(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movdqa	-80(%rbp), %xmm2
	leaq	24(%rax), %rdx
	movq	%rax, -304(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -288(%rbp)
	movq	%rdx, -296(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L152
	call	_ZdlPv@PLT
.L152:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -208(%rbp)
	jne	.L185
.L153:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L158
	call	_ZdlPv@PLT
.L158:
	movq	-256(%rbp), %rbx
	movq	-264(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L159
	.p2align 4,,10
	.p2align 3
.L163:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L160
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L163
.L161:
	movq	-264(%rbp), %r13
.L159:
	testq	%r13, %r13
	je	.L164
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L164:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L186
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L163
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L185:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$0, -344(%rbp)
	movq	$0, -328(%rbp)
	movq	$0, -336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -288(%rbp)
	movaps	%xmm0, -304(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -304(%rbp)
	movq	%rdx, -288(%rbp)
	movq	%rdx, -296(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-304(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L154
	call	_ZdlPv@PLT
.L154:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	movq	%rdx, %rbx
	movq	8(%rax), %rdx
	movq	16(%rax), %rax
	cmove	-344(%rbp), %rbx
	testq	%rdx, %rdx
	cmove	-328(%rbp), %rdx
	testq	%rax, %rax
	cmove	-336(%rbp), %rax
	movq	%rdx, -328(%rbp)
	movl	$27, %edx
	movq	%rax, -336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-336(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	-328(%rbp), %rdx
	call	_ZN2v88internal32FastRegExpPrototypeMatchBody_320EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEE
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L153
.L186:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22435:
	.size	_ZN2v88internal24RegExpMatchFastAssembler27GenerateRegExpMatchFastImplEv, .-_ZN2v88internal24RegExpMatchFastAssembler27GenerateRegExpMatchFastImplEv
	.section	.rodata._ZN2v88internal8Builtins24Generate_RegExpMatchFastEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/regexp-match-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins24Generate_RegExpMatchFastEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"RegExpMatchFast"
	.section	.text._ZN2v88internal8Builtins24Generate_RegExpMatchFastEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins24Generate_RegExpMatchFastEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins24Generate_RegExpMatchFastEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins24Generate_RegExpMatchFastEPNS0_8compiler18CodeAssemblerStateE:
.LFB22431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$220, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$867, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L191
.L188:
	movq	%r13, %rdi
	call	_ZN2v88internal24RegExpMatchFastAssembler27GenerateRegExpMatchFastImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L192
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L188
.L192:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22431:
	.size	_ZN2v88internal8Builtins24Generate_RegExpMatchFastEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins24Generate_RegExpMatchFastEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal29RegExpPrototypeMatchAssembler32GenerateRegExpPrototypeMatchImplEv.str1.1,"aMS",@progbits,1
.LC4:
	.string	"RegExp.prototype.@@match"
	.section	.text._ZN2v88internal29RegExpPrototypeMatchAssembler32GenerateRegExpPrototypeMatchImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29RegExpPrototypeMatchAssembler32GenerateRegExpPrototypeMatchImplEv
	.type	_ZN2v88internal29RegExpPrototypeMatchAssembler32GenerateRegExpPrototypeMatchImplEv, @function
_ZN2v88internal29RegExpPrototypeMatchAssembler32GenerateRegExpPrototypeMatchImplEv:
.LFB22444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1016(%rbp), %r14
	leaq	-1072(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1200(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1320, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, -1272(%rbp)
	movq	%rax, -1256(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, -1328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	leaq	-1256(%rbp), %r12
	movq	%rax, -1344(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$72, %edi
	movq	$0, -1064(%rbp)
	movq	$0, -1056(%rbp)
	movq	%rax, %rbx
	movq	-1256(%rbp), %rax
	movq	$0, -1048(%rbp)
	movq	%rax, -1072(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -1048(%rbp)
	movq	%rdx, -1056(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1032(%rbp)
	movq	%rax, -1064(%rbp)
	movq	$0, -1040(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1256(%rbp), %rax
	movl	$144, %edi
	movq	$0, -872(%rbp)
	movq	$0, -864(%rbp)
	movq	%rax, -880(%rbp)
	movq	$0, -856(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -872(%rbp)
	leaq	-824(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -856(%rbp)
	movq	%rdx, -864(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -840(%rbp)
	movq	%rax, -1304(%rbp)
	movq	$0, -848(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1256(%rbp), %rax
	movl	$168, %edi
	movq	$0, -680(%rbp)
	movq	$0, -672(%rbp)
	movq	%rax, -688(%rbp)
	movq	$0, -664(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -680(%rbp)
	leaq	-632(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -664(%rbp)
	movq	%rdx, -672(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -648(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -656(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1256(%rbp), %rax
	movl	$120, %edi
	movq	$0, -488(%rbp)
	movq	$0, -480(%rbp)
	movq	%rax, -496(%rbp)
	movq	$0, -472(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -488(%rbp)
	leaq	-440(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -472(%rbp)
	movq	%rdx, -480(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -456(%rbp)
	movq	%rax, -1296(%rbp)
	movq	$0, -464(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1256(%rbp), %rax
	movl	$144, %edi
	movq	$0, -296(%rbp)
	movq	$0, -288(%rbp)
	movq	%rax, -304(%rbp)
	movq	$0, -280(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -296(%rbp)
	leaq	-248(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -280(%rbp)
	movq	%rdx, -288(%rbp)
	xorl	%edx, %edx
	movq	%rax, -1288(%rbp)
	movups	%xmm0, -264(%rbp)
	movq	$0, -272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	-1328(%rbp), %xmm1
	movaps	%xmm0, -1200(%rbp)
	movhps	-1344(%rbp), %xmm1
	movq	%rbx, -96(%rbp)
	movaps	%xmm1, -112(%rbp)
	movq	$0, -1184(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm2
	leaq	24(%rax), %rdx
	movq	%rax, -1200(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -1184(%rbp)
	movq	%rdx, -1192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L194
	call	_ZdlPv@PLT
.L194:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1008(%rbp)
	jne	.L313
	cmpq	$0, -816(%rbp)
	jne	.L314
.L200:
	cmpq	$0, -624(%rbp)
	jne	.L315
.L203:
	cmpq	$0, -432(%rbp)
	jne	.L316
.L206:
	cmpq	$0, -240(%rbp)
	jne	.L317
.L210:
	movq	-1288(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L212
	call	_ZdlPv@PLT
.L212:
	movq	-288(%rbp), %rbx
	movq	-296(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L213
	.p2align 4,,10
	.p2align 3
.L217:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L214
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L217
.L215:
	movq	-296(%rbp), %r13
.L213:
	testq	%r13, %r13
	je	.L218
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L218:
	movq	-1296(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-464(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L219
	call	_ZdlPv@PLT
.L219:
	movq	-480(%rbp), %rbx
	movq	-488(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L220
	.p2align 4,,10
	.p2align 3
.L224:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L221
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L224
.L222:
	movq	-488(%rbp), %r13
.L220:
	testq	%r13, %r13
	je	.L225
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L225:
	movq	-1280(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L226
	call	_ZdlPv@PLT
.L226:
	movq	-672(%rbp), %rbx
	movq	-680(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L227
	.p2align 4,,10
	.p2align 3
.L231:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L228
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L231
.L229:
	movq	-680(%rbp), %r13
.L227:
	testq	%r13, %r13
	je	.L232
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L232:
	movq	-1304(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-848(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L233
	call	_ZdlPv@PLT
.L233:
	movq	-864(%rbp), %rbx
	movq	-872(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L234
	.p2align 4,,10
	.p2align 3
.L238:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L235
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L238
.L236:
	movq	-872(%rbp), %r13
.L234:
	testq	%r13, %r13
	je	.L239
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L239:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1040(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L240
	call	_ZdlPv@PLT
.L240:
	movq	-1056(%rbp), %rbx
	movq	-1064(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L241
	.p2align 4,,10
	.p2align 3
.L245:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L242
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L245
.L243:
	movq	-1064(%rbp), %r13
.L241:
	testq	%r13, %r13
	je	.L246
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L246:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L318
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L245
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L235:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L238
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L228:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L231
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L214:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L217
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L221:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L224
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L313:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1184(%rbp)
	movaps	%xmm0, -1200(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r8d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -1200(%rbp)
	movq	%rdx, -1184(%rbp)
	movq	%rdx, -1192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1200(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L196
	call	_ZdlPv@PLT
.L196:
	movq	(%rbx), %rax
	movl	$35, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %r15
	movq	16(%rax), %rax
	movq	%rax, -1344(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$34, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1272(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$62, %ecx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	leaq	.LC4(%rip), %r8
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler20ThrowIfNotJSReceiverENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS0_15MessageTemplateEPKc@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$36, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1272(%rbp), %rdi
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal27UnsafeCast10JSReceiver_1457EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$37, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -1328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1272(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1344(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ToString_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -1360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$42, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1328(%rbp), %rdx
	movq	%r13, %rcx
	movq	%rbx, %rsi
	movq	-1272(%rbp), %rdi
	call	_ZN2v88internal24Cast14ATFastJSRegExp_134EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %xmm6
	movq	%rbx, %xmm5
	movq	-1360(%rbp), %xmm3
	punpcklqdq	%xmm6, %xmm5
	pxor	%xmm0, %xmm0
	movq	%rax, -64(%rbp)
	movq	-1344(%rbp), %xmm4
	movhps	-1328(%rbp), %xmm3
	movl	$56, %edi
	movaps	%xmm5, -112(%rbp)
	leaq	-1232(%rbp), %r15
	movhps	-1328(%rbp), %xmm4
	movaps	%xmm3, -1360(%rbp)
	movaps	%xmm4, -1344(%rbp)
	movaps	%xmm5, -1328(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm0, -1232(%rbp)
	movq	$0, -1216(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm7
	movq	-64(%rbp), %rcx
	leaq	-688(%rbp), %rdi
	movdqa	-96(%rbp), %xmm6
	leaq	56(%rax), %rdx
	movq	%r15, %rsi
	movq	%rax, -1232(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-80(%rbp), %xmm7
	movq	%rcx, 48(%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -1216(%rbp)
	movq	%rdx, -1224(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L197
	call	_ZdlPv@PLT
.L197:
	movq	-1280(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1192(%rbp)
	jne	.L319
.L198:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -816(%rbp)
	je	.L200
.L314:
	movq	-1304(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-880(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -1184(%rbp)
	movaps	%xmm0, -1200(%rbp)
	call	_Znwm@PLT
	movl	$1799, %edi
	movq	%r13, %rsi
	movw	%di, 4(%rax)
	leaq	6(%rax), %rdx
	movq	%r15, %rdi
	movl	$117966855, (%rax)
	movq	%rax, -1200(%rbp)
	movq	%rdx, -1184(%rbp)
	movq	%rdx, -1192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1200(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L201
	call	_ZdlPv@PLT
.L201:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	32(%rax), %rdx
	movdqu	(%rax), %xmm6
	movq	$0, -1184(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -80(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm0, -1200(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm6
	leaq	40(%rax), %rdx
	leaq	-496(%rbp), %rdi
	movq	%rax, -1200(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -1184(%rbp)
	movq	%rdx, -1192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L202
	call	_ZdlPv@PLT
.L202:
	movq	-1296(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -624(%rbp)
	je	.L203
.L315:
	movq	-1280(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-688(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -1184(%rbp)
	movaps	%xmm0, -1200(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movq	%r15, %rdi
	movw	%si, 4(%rax)
	leaq	7(%rax), %rdx
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	movb	$7, 6(%rax)
	movq	%rax, -1200(%rbp)
	movq	%rdx, -1184(%rbp)
	movq	%rdx, -1192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1200(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L204
	call	_ZdlPv@PLT
.L204:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	16(%rax), %rdi
	movq	8(%rax), %r8
	movq	24(%rax), %rsi
	movq	32(%rax), %rcx
	movq	48(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdi, -96(%rbp)
	movl	$48, %edi
	movq	%r8, -104(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movaps	%xmm0, -1200(%rbp)
	movq	%rax, -112(%rbp)
	movq	$0, -1184(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm6
	leaq	48(%rax), %rdx
	leaq	-304(%rbp), %rdi
	movq	%rax, -1200(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -1184(%rbp)
	movq	%rdx, -1192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L205
	call	_ZdlPv@PLT
.L205:
	movq	-1288(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -432(%rbp)
	je	.L206
.L316:
	movq	-1296(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	leaq	-496(%rbp), %r8
	movq	%r8, -1328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1184(%rbp)
	movaps	%xmm0, -1200(%rbp)
	call	_Znwm@PLT
	movq	-1328(%rbp), %r8
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%r8, %rdi
	movq	%rax, -1200(%rbp)
	movq	%rdx, -1184(%rbp)
	movq	%rdx, -1192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L207
	movq	%rax, -1328(%rbp)
	call	_ZdlPv@PLT
	movq	-1328(%rbp), %rax
.L207:
	movq	(%rax), %rax
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rdx
	movq	24(%rax), %r8
	movq	32(%rax), %rax
	testq	%rdx, %rdx
	movq	%r8, -1328(%rbp)
	cmovne	%rdx, %r15
	testq	%rax, %rax
	movl	$43, %edx
	cmovne	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1328(%rbp), %r8
	movq	%r15, %rsi
	movq	%rbx, %rcx
	movq	-1272(%rbp), %rbx
	movq	%r8, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal32SlowRegExpPrototypeMatchBody_321EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_6StringEEE
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -240(%rbp)
	je	.L210
.L317:
	movq	-1288(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-304(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -1184(%rbp)
	movaps	%xmm0, -1200(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -1200(%rbp)
	movq	%rdx, -1184(%rbp)
	movq	%rdx, -1192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1200(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L211
	call	_ZdlPv@PLT
.L211:
	movq	(%rbx), %rax
	movl	$47, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	leaq	-1248(%rbp), %r15
	movq	(%rax), %r9
	movq	32(%rax), %rcx
	movq	40(%rax), %rbx
	movq	%r9, -1344(%rbp)
	movq	%rcx, -1328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1272(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$867, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-1200(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	leaq	-112(%rbp), %rcx
	xorl	%esi, %esi
	movl	$2, %ebx
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-1344(%rbp), %r9
	pushq	%rbx
	movhps	-1328(%rbp), %xmm0
	movq	%r15, %rdi
	leaq	-1232(%rbp), %rdx
	pushq	%rcx
	movl	$1, %ecx
	movq	%rax, -1232(%rbp)
	movq	-1184(%rbp), %rax
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -1224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1272(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L319:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movdqa	-1328(%rbp), %xmm7
	movdqa	-1344(%rbp), %xmm2
	movaps	%xmm0, -1232(%rbp)
	movaps	%xmm7, -112(%rbp)
	movdqa	-1360(%rbp), %xmm7
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	movq	$0, -1216(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	48(%rax), %rdx
	leaq	-880(%rbp), %rdi
	movq	%rax, -1232(%rbp)
	movups	%xmm2, (%rax)
	movdqa	-80(%rbp), %xmm2
	movups	%xmm7, 16(%rax)
	movups	%xmm2, 32(%rax)
	movq	%rdx, -1216(%rbp)
	movq	%rdx, -1224(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L199
	call	_ZdlPv@PLT
.L199:
	movq	-1304(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L198
.L318:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22444:
	.size	_ZN2v88internal29RegExpPrototypeMatchAssembler32GenerateRegExpPrototypeMatchImplEv, .-_ZN2v88internal29RegExpPrototypeMatchAssembler32GenerateRegExpPrototypeMatchImplEv
	.section	.rodata._ZN2v88internal8Builtins29Generate_RegExpPrototypeMatchEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"RegExpPrototypeMatch"
	.section	.text._ZN2v88internal8Builtins29Generate_RegExpPrototypeMatchEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins29Generate_RegExpPrototypeMatchEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins29Generate_RegExpPrototypeMatchEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins29Generate_RegExpPrototypeMatchEPNS0_8compiler18CodeAssemblerStateE:
.LFB22440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$244, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$868, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L324
.L321:
	movq	%r13, %rdi
	call	_ZN2v88internal29RegExpPrototypeMatchAssembler32GenerateRegExpPrototypeMatchImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L325
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L324:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L321
.L325:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22440:
	.size	_ZN2v88internal8Builtins29Generate_RegExpPrototypeMatchEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins29Generate_RegExpPrototypeMatchEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal32FastRegExpPrototypeMatchBody_320EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal32FastRegExpPrototypeMatchBody_320EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEE, @function
_GLOBAL__sub_I__ZN2v88internal32FastRegExpPrototypeMatchBody_320EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEE:
.LFB28989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28989:
	.size	_GLOBAL__sub_I__ZN2v88internal32FastRegExpPrototypeMatchBody_320EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEE, .-_GLOBAL__sub_I__ZN2v88internal32FastRegExpPrototypeMatchBody_320EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal32FastRegExpPrototypeMatchBody_320EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEE
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
