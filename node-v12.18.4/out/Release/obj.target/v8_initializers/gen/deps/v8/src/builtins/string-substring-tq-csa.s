	.file	"string-substring-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_3SmiENS0_6UnionTIS5_NS0_10HeapNumberEEES8_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS8_EESI_SG_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_3SmiENS0_6UnionTIS5_NS0_10HeapNumberEEES8_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS8_EESI_SG_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_3SmiENS0_6UnionTIS5_NS0_10HeapNumberEEES8_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS8_EESI_SG_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_3SmiENS0_6UnionTIS5_NS0_10HeapNumberEEES8_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS8_EESI_SG_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_3SmiENS0_6UnionTIS5_NS0_10HeapNumberEEES8_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS8_EESI_SG_:
.LFB26591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$6, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$1544, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$134612999, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2
	movq	%rax, -104(%rbp)
	call	_ZdlPv@PLT
	movq	-104(%rbp), %rax
.L2:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L3
	movq	%rdx, (%r15)
.L3:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L4
	movq	%rdx, (%r14)
.L4:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L5
	movq	%rdx, 0(%r13)
.L5:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L6
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L6:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L7
	movq	%rdx, (%rbx)
.L7:
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L1
	movq	-96(%rbp), %rbx
	movq	%rax, (%rbx)
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L33
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L33:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26591:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_3SmiENS0_6UnionTIS5_NS0_10HeapNumberEEES8_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS8_EESI_SG_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_3SmiENS0_6UnionTIS5_NS0_10HeapNumberEEES8_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS8_EESI_SG_
	.section	.rodata._ZN2v88internal23ToSmiBetweenZeroAnd_338EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_3SmiEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../../deps/v8/../../deps/v8/src/builtins/string-substring.tq"
	.section	.text._ZN2v88internal23ToSmiBetweenZeroAnd_338EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_3SmiEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23ToSmiBetweenZeroAnd_338EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_3SmiEEE
	.type	_ZN2v88internal23ToSmiBetweenZeroAnd_338EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_3SmiEEE, @function
_ZN2v88internal23ToSmiBetweenZeroAnd_338EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_3SmiEEE:
.LFB22410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-3200(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-3408(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$3576, %rsp
	movq	%rsi, -3552(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -3408(%rbp)
	movq	%rdi, -3200(%rbp)
	movl	$72, %edi
	movq	$0, -3192(%rbp)
	movq	$0, -3184(%rbp)
	movq	$0, -3176(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -3192(%rbp)
	leaq	-3144(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3176(%rbp)
	movq	%rdx, -3184(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3160(%rbp)
	movq	%rax, -3416(%rbp)
	movq	$0, -3168(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3408(%rbp), %rax
	movl	$144, %edi
	movq	$0, -3000(%rbp)
	movq	$0, -2992(%rbp)
	movq	%rax, -3008(%rbp)
	movq	$0, -2984(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -3000(%rbp)
	leaq	-2952(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2984(%rbp)
	movq	%rdx, -2992(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2968(%rbp)
	movq	%rax, -3536(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3408(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2808(%rbp)
	movq	$0, -2800(%rbp)
	movq	%rax, -2816(%rbp)
	movq	$0, -2792(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -2808(%rbp)
	leaq	-2760(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2792(%rbp)
	movq	%rdx, -2800(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2776(%rbp)
	movq	%rax, -3456(%rbp)
	movq	$0, -2784(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3408(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2616(%rbp)
	movq	$0, -2608(%rbp)
	movq	%rax, -2624(%rbp)
	movq	$0, -2600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2616(%rbp)
	leaq	-2568(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2600(%rbp)
	movq	%rdx, -2608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2584(%rbp)
	movq	%rax, -3480(%rbp)
	movq	$0, -2592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3408(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2424(%rbp)
	movq	$0, -2416(%rbp)
	movq	%rax, -2432(%rbp)
	movq	$0, -2408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2424(%rbp)
	leaq	-2376(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2408(%rbp)
	movq	%rdx, -2416(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2392(%rbp)
	movq	%rax, -3488(%rbp)
	movq	$0, -2400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3408(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2232(%rbp)
	movq	$0, -2224(%rbp)
	movq	%rax, -2240(%rbp)
	movq	$0, -2216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2232(%rbp)
	leaq	-2184(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2216(%rbp)
	movq	%rdx, -2224(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2200(%rbp)
	movq	%rax, -3472(%rbp)
	movq	$0, -2208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3408(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movq	%rax, -2048(%rbp)
	movq	$0, -2024(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2040(%rbp)
	leaq	-1992(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2024(%rbp)
	movq	%rdx, -2032(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2008(%rbp)
	movq	%rax, -3504(%rbp)
	movq	$0, -2016(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3408(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1848(%rbp)
	movq	$0, -1840(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1832(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1848(%rbp)
	leaq	-1800(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1832(%rbp)
	movq	%rdx, -1840(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1816(%rbp)
	movq	%rax, -3440(%rbp)
	movq	$0, -1824(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3408(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1656(%rbp)
	movq	$0, -1648(%rbp)
	movq	%rax, -1664(%rbp)
	movq	$0, -1640(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1656(%rbp)
	leaq	-1608(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1640(%rbp)
	movq	%rdx, -1648(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1624(%rbp)
	movq	%rax, -3528(%rbp)
	movq	$0, -1632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3408(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1464(%rbp)
	movq	$0, -1456(%rbp)
	movq	%rax, -1472(%rbp)
	movq	$0, -1448(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1464(%rbp)
	leaq	-1416(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1448(%rbp)
	movq	%rdx, -1456(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1432(%rbp)
	movq	%rax, -3464(%rbp)
	movq	$0, -1440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3408(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1272(%rbp)
	movq	$0, -1264(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -1256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1272(%rbp)
	leaq	-1224(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1256(%rbp)
	movq	%rdx, -1264(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1240(%rbp)
	movq	%rax, -3496(%rbp)
	movq	$0, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3408(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1080(%rbp)
	movq	$0, -1072(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$0, -1064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1080(%rbp)
	leaq	-1032(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1064(%rbp)
	movq	%rdx, -1072(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1048(%rbp)
	movq	%rax, -3512(%rbp)
	movq	$0, -1056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3408(%rbp), %rax
	movl	$168, %edi
	movq	$0, -888(%rbp)
	movq	$0, -880(%rbp)
	movq	%rax, -896(%rbp)
	movq	$0, -872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -888(%rbp)
	leaq	-840(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -872(%rbp)
	movq	%rdx, -880(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -856(%rbp)
	movq	%rax, -3520(%rbp)
	movq	$0, -864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3408(%rbp), %rax
	movl	$192, %edi
	movq	$0, -696(%rbp)
	movq	$0, -688(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -696(%rbp)
	leaq	-648(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -680(%rbp)
	movq	%rdx, -688(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -664(%rbp)
	movq	%rax, -3448(%rbp)
	movq	$0, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3408(%rbp), %rax
	movl	$96, %edi
	movq	$0, -504(%rbp)
	movq	$0, -496(%rbp)
	movq	%rax, -512(%rbp)
	movq	$0, -488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -504(%rbp)
	leaq	-456(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -488(%rbp)
	movq	%rdx, -496(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -472(%rbp)
	movq	%rax, -3432(%rbp)
	movq	$0, -480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3408(%rbp), %rax
	movl	$96, %edi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -312(%rbp)
	leaq	-264(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	xorl	%edx, %edx
	movq	%rax, -3424(%rbp)
	movups	%xmm0, -280(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3552(%rbp), %r9
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	%r14, -120(%rbp)
	leaq	-3328(%rbp), %r14
	movq	%r9, -128(%rbp)
	movaps	%xmm0, -3328(%rbp)
	movq	%rbx, -112(%rbp)
	movq	$0, -3312(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r13, %rdi
	movq	%r14, %rsi
	movdqa	-128(%rbp), %xmm3
	leaq	24(%rax), %rdx
	movq	%rax, -3328(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L35
	call	_ZdlPv@PLT
.L35:
	movq	-3416(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3136(%rbp)
	jne	.L386
	cmpq	$0, -2944(%rbp)
	jne	.L387
.L41:
	cmpq	$0, -2752(%rbp)
	jne	.L388
.L44:
	cmpq	$0, -2560(%rbp)
	jne	.L389
.L48:
	cmpq	$0, -2368(%rbp)
	jne	.L390
.L51:
	cmpq	$0, -2176(%rbp)
	jne	.L391
.L53:
	cmpq	$0, -1984(%rbp)
	jne	.L392
.L55:
	cmpq	$0, -1792(%rbp)
	jne	.L393
.L57:
	cmpq	$0, -1600(%rbp)
	jne	.L394
.L60:
	cmpq	$0, -1408(%rbp)
	jne	.L395
.L62:
	cmpq	$0, -1216(%rbp)
	jne	.L396
.L66:
	cmpq	$0, -1024(%rbp)
	jne	.L397
.L69:
	cmpq	$0, -832(%rbp)
	jne	.L398
.L72:
	cmpq	$0, -640(%rbp)
	jne	.L399
.L75:
	cmpq	$0, -448(%rbp)
	leaq	-320(%rbp), %r13
	jne	.L400
.L78:
	movq	-3424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -3312(%rbp)
	movaps	%xmm0, -3328(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	$101058567, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -3328(%rbp)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3328(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L81
	call	_ZdlPv@PLT
.L81:
	movq	(%rbx), %rax
	movq	-3424(%rbp), %rdi
	movq	24(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L82
	call	_ZdlPv@PLT
.L82:
	movq	-304(%rbp), %rbx
	movq	-312(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L83
	.p2align 4,,10
	.p2align 3
.L87:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L84
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L87
.L85:
	movq	-312(%rbp), %r14
.L83:
	testq	%r14, %r14
	je	.L88
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L88:
	movq	-3432(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L89
	call	_ZdlPv@PLT
.L89:
	movq	-496(%rbp), %rbx
	movq	-504(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L90
	.p2align 4,,10
	.p2align 3
.L94:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L91
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L94
.L92:
	movq	-504(%rbp), %r14
.L90:
	testq	%r14, %r14
	je	.L95
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L95:
	movq	-3448(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L96
	call	_ZdlPv@PLT
.L96:
	movq	-688(%rbp), %rbx
	movq	-696(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L97
	.p2align 4,,10
	.p2align 3
.L101:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L98
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L101
.L99:
	movq	-696(%rbp), %r14
.L97:
	testq	%r14, %r14
	je	.L102
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L102:
	movq	-3520(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-864(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L103
	call	_ZdlPv@PLT
.L103:
	movq	-880(%rbp), %rbx
	movq	-888(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L104
	.p2align 4,,10
	.p2align 3
.L108:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L105
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L108
.L106:
	movq	-888(%rbp), %r14
.L104:
	testq	%r14, %r14
	je	.L109
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L109:
	movq	-3512(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L110
	call	_ZdlPv@PLT
.L110:
	movq	-1072(%rbp), %rbx
	movq	-1080(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L111
	.p2align 4,,10
	.p2align 3
.L115:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L112
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L115
.L113:
	movq	-1080(%rbp), %r14
.L111:
	testq	%r14, %r14
	je	.L116
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L116:
	movq	-3496(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L117
	call	_ZdlPv@PLT
.L117:
	movq	-1264(%rbp), %rbx
	movq	-1272(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L118
	.p2align 4,,10
	.p2align 3
.L122:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L119
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L122
.L120:
	movq	-1272(%rbp), %r14
.L118:
	testq	%r14, %r14
	je	.L123
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L123:
	movq	-3464(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1440(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L124
	call	_ZdlPv@PLT
.L124:
	movq	-1456(%rbp), %rbx
	movq	-1464(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L125
	.p2align 4,,10
	.p2align 3
.L129:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L126
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L129
.L127:
	movq	-1464(%rbp), %r14
.L125:
	testq	%r14, %r14
	je	.L130
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L130:
	movq	-3528(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1632(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L131
	call	_ZdlPv@PLT
.L131:
	movq	-1648(%rbp), %rbx
	movq	-1656(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L132
	.p2align 4,,10
	.p2align 3
.L136:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L133
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L136
.L134:
	movq	-1656(%rbp), %r14
.L132:
	testq	%r14, %r14
	je	.L137
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L137:
	movq	-3440(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L138
	call	_ZdlPv@PLT
.L138:
	movq	-1840(%rbp), %rbx
	movq	-1848(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L139
	.p2align 4,,10
	.p2align 3
.L143:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L140
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L143
.L141:
	movq	-1848(%rbp), %r14
.L139:
	testq	%r14, %r14
	je	.L144
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L144:
	movq	-3504(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2016(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L145
	call	_ZdlPv@PLT
.L145:
	movq	-2032(%rbp), %rbx
	movq	-2040(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L146
	.p2align 4,,10
	.p2align 3
.L150:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L147
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L150
.L148:
	movq	-2040(%rbp), %r14
.L146:
	testq	%r14, %r14
	je	.L151
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L151:
	movq	-3472(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L152
	call	_ZdlPv@PLT
.L152:
	movq	-2224(%rbp), %rbx
	movq	-2232(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L153
	.p2align 4,,10
	.p2align 3
.L157:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L154
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L157
.L155:
	movq	-2232(%rbp), %r14
.L153:
	testq	%r14, %r14
	je	.L158
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L158:
	movq	-3488(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L159
	call	_ZdlPv@PLT
.L159:
	movq	-2416(%rbp), %rbx
	movq	-2424(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L160
	.p2align 4,,10
	.p2align 3
.L164:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L161
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L164
.L162:
	movq	-2424(%rbp), %r14
.L160:
	testq	%r14, %r14
	je	.L165
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L165:
	movq	-3480(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L166
	call	_ZdlPv@PLT
.L166:
	movq	-2608(%rbp), %rbx
	movq	-2616(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L167
	.p2align 4,,10
	.p2align 3
.L171:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L168
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L171
.L169:
	movq	-2616(%rbp), %r14
.L167:
	testq	%r14, %r14
	je	.L172
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L172:
	movq	-3456(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L173
	call	_ZdlPv@PLT
.L173:
	movq	-2800(%rbp), %rbx
	movq	-2808(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L174
	.p2align 4,,10
	.p2align 3
.L178:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L175
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L178
.L176:
	movq	-2808(%rbp), %r14
.L174:
	testq	%r14, %r14
	je	.L179
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L179:
	movq	-3536(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2976(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L180
	call	_ZdlPv@PLT
.L180:
	movq	-2992(%rbp), %rbx
	movq	-3000(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L181
	.p2align 4,,10
	.p2align 3
.L185:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L182
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L185
.L183:
	movq	-3000(%rbp), %r14
.L181:
	testq	%r14, %r14
	je	.L186
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L186:
	movq	-3416(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L187
	call	_ZdlPv@PLT
.L187:
	movq	-3184(%rbp), %rbx
	movq	-3192(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L188
	.p2align 4,,10
	.p2align 3
.L192:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L189
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L192
.L190:
	movq	-3192(%rbp), %r14
.L188:
	testq	%r14, %r14
	je	.L193
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L193:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L401
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L192
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L182:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L185
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L175:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L178
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L168:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L171
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L161:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L164
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L154:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L157
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L147:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L150
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L140:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L143
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L133:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L136
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L126:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L129
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L119:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L122
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L112:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L115
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L105:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L108
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L98:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L101
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L84:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L87
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L91:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L94
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L386:
	movq	-3416(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -3312(%rbp)
	movaps	%xmm0, -3328(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r9d
	movq	%r13, %rdi
	movq	%r14, %rsi
	movw	%r9w, (%rax)
	leaq	3(%rax), %rdx
	movb	$6, 2(%rax)
	movq	%rax, -3328(%rbp)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3328(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L37
	call	_ZdlPv@PLT
.L37:
	movq	(%rbx), %rax
	movl	$12, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %r13
	movq	(%rax), %rbx
	movq	16(%rax), %rax
	movq	%rax, -3568(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler16ToInteger_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_23ToIntegerTruncationModeE@PLT
	movq	%r14, %rdi
	movq	%rax, -3552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$11, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$13, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$14, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3552(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal13Cast5ATSmi_83EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm6
	pxor	%xmm0, %xmm0
	movq	-3552(%rbp), %xmm3
	movq	-3568(%rbp), %xmm5
	movl	$56, %edi
	movaps	%xmm0, -3360(%rbp)
	movdqa	%xmm3, %xmm4
	movq	%rax, -80(%rbp)
	punpcklqdq	%xmm3, %xmm5
	movq	%r13, %xmm3
	punpcklqdq	%xmm4, %xmm4
	movq	$0, -3344(%rbp)
	punpcklqdq	%xmm3, %xmm6
	movaps	%xmm5, -112(%rbp)
	leaq	-3360(%rbp), %r13
	movaps	%xmm4, -3584(%rbp)
	movaps	%xmm5, -3568(%rbp)
	movaps	%xmm6, -3552(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm4, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm4
	leaq	56(%rax), %rdx
	leaq	-2816(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movq	%rax, -3360(%rbp)
	movq	%rdx, -3344(%rbp)
	movq	%rdx, -3352(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3360(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L38
	call	_ZdlPv@PLT
.L38:
	movq	-3456(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3320(%rbp)
	jne	.L402
.L39:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2944(%rbp)
	je	.L41
.L387:
	movq	-3536(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-3008(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -3312(%rbp)
	movaps	%xmm0, -3328(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r8d
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	$134612999, (%rax)
	leaq	6(%rax), %rdx
	movw	%r8w, 4(%rax)
	movq	%rax, -3328(%rbp)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3328(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L42
	call	_ZdlPv@PLT
.L42:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	32(%rax), %rdx
	movdqu	(%rax), %xmm5
	movq	$0, -3312(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm0, -3328(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-128(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm6
	leaq	40(%rax), %rdx
	leaq	-1472(%rbp), %rdi
	movq	%rax, -3328(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L43
	call	_ZdlPv@PLT
.L43:
	movq	-3464(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2752(%rbp)
	je	.L44
.L388:
	movq	-3456(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2816(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -3312(%rbp)
	movaps	%xmm0, -3328(%rbp)
	call	_Znwm@PLT
	movl	$2056, %edi
	movq	%r14, %rsi
	movw	%di, 4(%rax)
	leaq	7(%rax), %rdx
	movq	%r13, %rdi
	movl	$134612999, (%rax)
	movb	$6, 6(%rax)
	movq	%rax, -3328(%rbp)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3328(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L45
	call	_ZdlPv@PLT
.L45:
	movq	(%rbx), %rax
	movl	$15, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -3584(%rbp)
	movq	24(%rax), %rsi
	movq	%rcx, -3568(%rbp)
	movq	%rsi, -3600(%rbp)
	movq	32(%rax), %rsi
	movq	48(%rax), %rax
	movq	%rsi, -3616(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rax, -3552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-3552(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18UintPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm2
	pxor	%xmm0, %xmm0
	movq	-3568(%rbp), %xmm1
	movq	-3616(%rbp), %xmm7
	movhps	-3600(%rbp), %xmm2
	movl	$48, %edi
	movaps	%xmm0, -3328(%rbp)
	movhps	-3584(%rbp), %xmm1
	movaps	%xmm2, -112(%rbp)
	movhps	-3552(%rbp), %xmm7
	movaps	%xmm2, -3600(%rbp)
	movaps	%xmm7, -3552(%rbp)
	movaps	%xmm1, -3568(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	$0, -3312(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	leaq	48(%rax), %rdx
	leaq	-2624(%rbp), %rdi
	movq	%rax, -3328(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L46
	call	_ZdlPv@PLT
.L46:
	movdqa	-3568(%rbp), %xmm3
	movdqa	-3600(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movdqa	-3552(%rbp), %xmm7
	movaps	%xmm0, -3328(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	$0, -3312(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm3
	leaq	48(%rax), %rdx
	leaq	-1664(%rbp), %rdi
	movq	%rax, -3328(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm3, 32(%rax)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L47
	call	_ZdlPv@PLT
.L47:
	movq	-3528(%rbp), %rcx
	movq	-3480(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2560(%rbp)
	je	.L48
.L389:
	movq	-3480(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3400(%rbp)
	leaq	-2624(%rbp), %r13
	movq	$0, -3392(%rbp)
	movq	$0, -3384(%rbp)
	movq	$0, -3376(%rbp)
	movq	$0, -3368(%rbp)
	movq	$0, -3360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r13, %rdi
	leaq	-3360(%rbp), %rax
	pushq	%rax
	leaq	-3384(%rbp), %rcx
	leaq	-3368(%rbp), %r9
	leaq	-3376(%rbp), %r8
	leaq	-3392(%rbp), %rdx
	leaq	-3400(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_3SmiENS0_6UnionTIS5_NS0_10HeapNumberEEES8_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS8_EESI_SG_
	movl	$16, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3360(%rbp), %rbx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movq	-3368(%rbp), %xmm3
	movq	-3384(%rbp), %xmm4
	movq	-3400(%rbp), %xmm5
	movaps	%xmm0, -3328(%rbp)
	movhps	-3360(%rbp), %xmm3
	movq	$0, -3312(%rbp)
	movhps	-3376(%rbp), %xmm4
	movhps	-3392(%rbp), %xmm5
	movaps	%xmm3, -3568(%rbp)
	movaps	%xmm4, -3552(%rbp)
	movaps	%xmm5, -3584(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm6
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm1
	leaq	48(%rax), %rdx
	leaq	-2432(%rbp), %rdi
	movq	%rax, -3328(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm1, 32(%rax)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3328(%rbp), %rdi
	popq	%rcx
	popq	%rsi
	testq	%rdi, %rdi
	je	.L49
	call	_ZdlPv@PLT
.L49:
	movdqa	-3584(%rbp), %xmm4
	movdqa	-3552(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movdqa	-3568(%rbp), %xmm2
	movaps	%xmm0, -3328(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	$0, -3312(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm3
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	48(%rax), %rdx
	leaq	-2240(%rbp), %rdi
	movq	%rax, -3328(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L50
	call	_ZdlPv@PLT
.L50:
	movq	-3472(%rbp), %rcx
	movq	-3488(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2368(%rbp)
	je	.L51
.L390:
	movq	-3488(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3400(%rbp)
	leaq	-2432(%rbp), %r13
	movq	$0, -3392(%rbp)
	movq	$0, -3384(%rbp)
	movq	$0, -3376(%rbp)
	movq	$0, -3368(%rbp)
	movq	$0, -3360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r13, %rdi
	leaq	-3360(%rbp), %rax
	pushq	%rax
	leaq	-3384(%rbp), %rcx
	leaq	-3392(%rbp), %rdx
	leaq	-3400(%rbp), %rsi
	leaq	-3368(%rbp), %r9
	leaq	-3376(%rbp), %r8
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_3SmiENS0_6UnionTIS5_NS0_10HeapNumberEEES8_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS8_EESI_SG_
	movl	$48, %edi
	movq	-3368(%rbp), %xmm0
	movq	-3384(%rbp), %xmm1
	movq	-3400(%rbp), %xmm2
	movq	$0, -3312(%rbp)
	movhps	-3360(%rbp), %xmm0
	movhps	-3376(%rbp), %xmm1
	movhps	-3392(%rbp), %xmm2
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3328(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm1
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	leaq	48(%rax), %rdx
	leaq	-2048(%rbp), %rdi
	movq	%rax, -3328(%rbp)
	movups	%xmm1, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3328(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L52
	call	_ZdlPv@PLT
.L52:
	movq	-3504(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2176(%rbp)
	je	.L53
.L391:
	movq	-3472(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3400(%rbp)
	leaq	-2240(%rbp), %r13
	movq	$0, -3392(%rbp)
	movq	$0, -3384(%rbp)
	movq	$0, -3376(%rbp)
	movq	$0, -3368(%rbp)
	movq	$0, -3360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r13, %rdi
	leaq	-3360(%rbp), %rax
	pushq	%rax
	leaq	-3384(%rbp), %rcx
	leaq	-3400(%rbp), %rsi
	leaq	-3368(%rbp), %r9
	leaq	-3376(%rbp), %r8
	leaq	-3392(%rbp), %rdx
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_3SmiENS0_6UnionTIS5_NS0_10HeapNumberEEES8_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS8_EESI_SG_
	movq	-3400(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	-3384(%rbp), %rax
	movl	$56, %edi
	movaps	%xmm0, -3328(%rbp)
	movq	%rdx, -128(%rbp)
	movq	-3392(%rbp), %rdx
	movq	%rax, -112(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-3376(%rbp), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -104(%rbp)
	movq	-3368(%rbp), %rdx
	movq	$0, -3312(%rbp)
	movq	%rdx, -96(%rbp)
	movq	-3360(%rbp), %rdx
	movq	%rdx, -88(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm2
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm6
	leaq	56(%rax), %rdx
	leaq	-1856(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rax, -3328(%rbp)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3328(%rbp), %rdi
	popq	%r13
	popq	%rax
	testq	%rdi, %rdi
	je	.L54
	call	_ZdlPv@PLT
.L54:
	movq	-3440(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1984(%rbp)
	je	.L55
.L392:
	movq	-3504(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3400(%rbp)
	leaq	-2048(%rbp), %r13
	movq	$0, -3392(%rbp)
	movq	$0, -3384(%rbp)
	movq	$0, -3376(%rbp)
	movq	$0, -3368(%rbp)
	movq	$0, -3360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r13, %rdi
	leaq	-3360(%rbp), %rax
	pushq	%rax
	leaq	-3384(%rbp), %rcx
	leaq	-3368(%rbp), %r9
	leaq	-3376(%rbp), %r8
	leaq	-3392(%rbp), %rdx
	leaq	-3400(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_3SmiENS0_6UnionTIS5_NS0_10HeapNumberEEES8_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS8_EESI_SG_
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	-3400(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movaps	%xmm0, -3328(%rbp)
	movq	%rdx, -128(%rbp)
	movq	-3392(%rbp), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-3384(%rbp), %rdx
	movq	$0, -3312(%rbp)
	movq	%rdx, -112(%rbp)
	movq	-3376(%rbp), %rdx
	movq	%rdx, -104(%rbp)
	movq	-3368(%rbp), %rdx
	movq	%rdx, -96(%rbp)
	movq	-3360(%rbp), %rdx
	movq	%rdx, -88(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm1
	movdqa	-96(%rbp), %xmm4
	leaq	56(%rax), %rdx
	leaq	-1856(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm4, 32(%rax)
	movq	%rax, -3328(%rbp)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3328(%rbp), %rdi
	popq	%r11
	popq	%rbx
	testq	%rdi, %rdi
	je	.L56
	call	_ZdlPv@PLT
.L56:
	movq	-3440(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1792(%rbp)
	je	.L57
.L393:
	movq	-3440(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1856(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -3312(%rbp)
	movaps	%xmm0, -3328(%rbp)
	call	_Znwm@PLT
	movl	$1544, %r10d
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	$134612999, (%rax)
	leaq	7(%rax), %rdx
	movw	%r10w, 4(%rax)
	movb	$6, 6(%rax)
	movq	%rax, -3328(%rbp)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3328(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L58
	call	_ZdlPv@PLT
.L58:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	8(%rax), %rsi
	movq	16(%rax), %rcx
	movq	48(%rax), %rdx
	movq	(%rax), %rax
	movaps	%xmm0, -3328(%rbp)
	movq	%rsi, -120(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -3312(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm2
	movq	%r14, %rsi
	leaq	32(%rax), %rdx
	leaq	-512(%rbp), %rdi
	movq	%rax, -3328(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L59
	call	_ZdlPv@PLT
.L59:
	movq	-3432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1600(%rbp)
	je	.L60
.L394:
	movq	-3528(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3400(%rbp)
	leaq	-1664(%rbp), %r13
	movq	$0, -3392(%rbp)
	movq	$0, -3384(%rbp)
	movq	$0, -3376(%rbp)
	movq	$0, -3368(%rbp)
	movq	$0, -3360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r13, %rdi
	leaq	-3360(%rbp), %rax
	pushq	%rax
	leaq	-3368(%rbp), %r9
	leaq	-3376(%rbp), %r8
	leaq	-3384(%rbp), %rcx
	leaq	-3392(%rbp), %rdx
	leaq	-3400(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectENS0_3SmiENS0_6UnionTIS5_NS0_10HeapNumberEEES8_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS8_EESI_SG_
	movl	$18, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$32, %edi
	movq	-3384(%rbp), %xmm0
	movq	-3400(%rbp), %xmm1
	movq	$0, -3312(%rbp)
	movhps	-3360(%rbp), %xmm0
	movhps	-3392(%rbp), %xmm1
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3328(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm7
	movq	%r14, %rsi
	leaq	32(%rax), %rdx
	leaq	-512(%rbp), %rdi
	movq	%rax, -3328(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3328(%rbp), %rdi
	popq	%r8
	popq	%r9
	testq	%rdi, %rdi
	je	.L61
	call	_ZdlPv@PLT
.L61:
	movq	-3432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1408(%rbp)
	je	.L62
.L395:
	movq	-3464(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1472(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -3312(%rbp)
	movaps	%xmm0, -3328(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	$134612999, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -3328(%rbp)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3328(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L63
	call	_ZdlPv@PLT
.L63:
	movq	(%rbx), %rax
	movl	$22, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	24(%rax), %rbx
	movq	%rsi, -3568(%rbp)
	movq	16(%rax), %rsi
	movq	32(%rax), %rax
	movq	%rcx, -3552(%rbp)
	movq	%rsi, -3584(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rbx, -3600(%rbp)
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$23, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rbx, -3616(%rbp)
	call	_ZN2v88internal23LoadHeapNumberValue_504EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapNumberEEE@PLT
	movl	$24, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal46FromConstexpr9ATfloat6417ATconstexpr_int31_163EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15Float64LessThanENS1_11SloppyTNodeINS0_8Float64TEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movq	-3616(%rbp), %xmm6
	movq	-3584(%rbp), %xmm7
	movaps	%xmm0, -3328(%rbp)
	movq	-3552(%rbp), %xmm2
	punpcklqdq	%xmm6, %xmm6
	movq	%rbx, -80(%rbp)
	movhps	-3600(%rbp), %xmm7
	movhps	-3568(%rbp), %xmm2
	movaps	%xmm6, -3616(%rbp)
	movaps	%xmm7, -3584(%rbp)
	movaps	%xmm2, -3552(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movq	$0, -3312(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm1
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm4
	leaq	56(%rax), %rdx
	leaq	-1280(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm1, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm4, 32(%rax)
	movq	%rax, -3328(%rbp)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L64
	call	_ZdlPv@PLT
.L64:
	movdqa	-3552(%rbp), %xmm5
	movdqa	-3584(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movdqa	-3616(%rbp), %xmm3
	movq	%rbx, -80(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -3328(%rbp)
	movq	$0, -3312(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm1
	movdqa	-96(%rbp), %xmm6
	leaq	56(%rax), %rdx
	leaq	-1088(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rax, -3328(%rbp)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L65
	call	_ZdlPv@PLT
.L65:
	movq	-3512(%rbp), %rcx
	movq	-3496(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1216(%rbp)
	je	.L66
.L396:
	movq	-3496(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1280(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -3312(%rbp)
	movaps	%xmm0, -3328(%rbp)
	call	_Znwm@PLT
	movl	$1800, %edi
	movq	%r14, %rsi
	movw	%di, 4(%rax)
	leaq	7(%rax), %rdx
	movq	%r13, %rdi
	movl	$134612999, (%rax)
	movb	$13, 6(%rax)
	movq	%rax, -3328(%rbp)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3328(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L67
	call	_ZdlPv@PLT
.L67:
	movq	(%rbx), %rax
	movl	$56, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movq	48(%rax), %rdx
	movdqu	(%rax), %xmm4
	movq	$0, -3312(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -80(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3328(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	leaq	56(%rax), %rdx
	leaq	-896(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm3, 32(%rax)
	movq	%rax, -3328(%rbp)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L68
	call	_ZdlPv@PLT
.L68:
	movq	-3520(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1024(%rbp)
	je	.L69
.L397:
	movq	-3512(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1088(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -3312(%rbp)
	movaps	%xmm0, -3328(%rbp)
	call	_Znwm@PLT
	movl	$1800, %esi
	movq	%r13, %rdi
	movw	%si, 4(%rax)
	leaq	7(%rax), %rdx
	movq	%r14, %rsi
	movl	$134612999, (%rax)
	movb	$13, 6(%rax)
	movq	%rax, -3328(%rbp)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3328(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L70
	call	_ZdlPv@PLT
.L70:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	32(%rax), %rdi
	movq	16(%rax), %rdx
	movq	8(%rax), %r9
	movq	24(%rax), %r8
	movq	40(%rax), %rsi
	movq	48(%rax), %rcx
	movq	%rdi, -96(%rbp)
	movl	$64, %edi
	movq	(%rax), %rax
	movq	%r9, -120(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movaps	%xmm0, -3328(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -3312(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm1
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm4
	leaq	64(%rax), %rdx
	leaq	-704(%rbp), %rdi
	movups	%xmm7, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm4, 48(%rax)
	movq	%rax, -3328(%rbp)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L71
	call	_ZdlPv@PLT
.L71:
	movq	-3448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -832(%rbp)
	je	.L72
.L398:
	movq	-3520(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-896(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -3312(%rbp)
	movaps	%xmm0, -3328(%rbp)
	call	_Znwm@PLT
	movl	$1800, %ecx
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	$134612999, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$13, 6(%rax)
	movq	%rax, -3328(%rbp)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3328(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L73
	call	_ZdlPv@PLT
.L73:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	40(%rax), %rbx
	movq	16(%rax), %r13
	movq	%rsi, -3568(%rbp)
	movq	24(%rax), %rsi
	movq	%rcx, -3552(%rbp)
	movq	%rsi, -3584(%rbp)
	movq	32(%rax), %rsi
	movq	%rbx, -3616(%rbp)
	movq	48(%rax), %rbx
	movq	%rsi, -3600(%rbp)
	xorl	%esi, %esi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$64, %edi
	movq	%rbx, -80(%rbp)
	movq	-3552(%rbp), %xmm0
	movq	%rax, -72(%rbp)
	movhps	-3568(%rbp), %xmm0
	movq	$0, -3312(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	%r13, %xmm0
	movhps	-3584(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-3600(%rbp), %xmm0
	movhps	-3616(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3328(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm2
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm7
	leaq	64(%rax), %rdx
	leaq	-704(%rbp), %rdi
	movups	%xmm5, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rax, -3328(%rbp)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L74
	call	_ZdlPv@PLT
.L74:
	movq	-3448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -640(%rbp)
	je	.L75
.L399:
	movq	-3448(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-704(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3312(%rbp)
	movaps	%xmm0, -3328(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movabsq	$436012470000551943, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3328(%rbp)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3328(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L76
	call	_ZdlPv@PLT
.L76:
	movq	(%rbx), %rax
	movl	$32, %edi
	movdqu	48(%rax), %xmm1
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm6
	movq	$0, -3312(%rbp)
	shufpd	$2, %xmm1, %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm0, -3328(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm4
	movq	%r14, %rsi
	leaq	32(%rax), %rdx
	leaq	-512(%rbp), %rdi
	movq	%rax, -3328(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L77
	call	_ZdlPv@PLT
.L77:
	movq	-3432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L400:
	movq	-3432(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-512(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -3312(%rbp)
	movaps	%xmm0, -3328(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	$101058567, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -3328(%rbp)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3328(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L79
	call	_ZdlPv@PLT
.L79:
	movq	(%rbx), %rax
	movl	$9, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r13
	movq	8(%rax), %rcx
	movq	16(%rax), %rbx
	movq	24(%rax), %r15
	movq	%rcx, -3552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %xmm0
	movq	%r15, %xmm2
	movl	$32, %edi
	movhps	-3552(%rbp), %xmm0
	leaq	-320(%rbp), %r13
	movq	$0, -3312(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	%rbx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3328(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm3
	movq	%r13, %rdi
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm7
	leaq	32(%rax), %rdx
	movq	%rax, -3328(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -3312(%rbp)
	movq	%rdx, -3320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L80
	call	_ZdlPv@PLT
.L80:
	movq	-3424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L402:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movdqa	-3552(%rbp), %xmm1
	movdqa	-3568(%rbp), %xmm6
	movdqa	-3584(%rbp), %xmm5
	movaps	%xmm0, -3360(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movq	$0, -3344(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm1
	movdqa	-96(%rbp), %xmm6
	leaq	48(%rax), %rdx
	leaq	-3008(%rbp), %rdi
	movq	%rax, -3360(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -3344(%rbp)
	movq	%rdx, -3352(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3360(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L40
	call	_ZdlPv@PLT
.L40:
	movq	-3536(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L39
.L401:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22410:
	.size	_ZN2v88internal23ToSmiBetweenZeroAnd_338EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_3SmiEEE, .-_ZN2v88internal23ToSmiBetweenZeroAnd_338EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_3SmiEEE
	.section	.rodata._ZN2v88internal33StringPrototypeSubstringAssembler36GenerateStringPrototypeSubstringImplEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"String.prototype.substring"
	.section	.text._ZN2v88internal33StringPrototypeSubstringAssembler36GenerateStringPrototypeSubstringImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal33StringPrototypeSubstringAssembler36GenerateStringPrototypeSubstringImplEv
	.type	_ZN2v88internal33StringPrototypeSubstringAssembler36GenerateStringPrototypeSubstringImplEv, @function
_ZN2v88internal33StringPrototypeSubstringAssembler36GenerateStringPrototypeSubstringImplEv:
.LFB22449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1688(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$1832, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -1688(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r12, %rcx
	movq	%r13, %rsi
	leaq	-1680(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-1664(%rbp), %rcx
	movl	$2, %esi
	movq	%r13, %rdi
	movq	-1680(%rbp), %r12
	movq	-1672(%rbp), %rax
	movq	%r13, -1520(%rbp)
	leaq	-1472(%rbp), %r13
	movq	%rcx, -1776(%rbp)
	movq	%rcx, -1504(%rbp)
	movq	%r12, -1488(%rbp)
	movq	%rax, -1808(%rbp)
	movq	$1, -1512(%rbp)
	movq	%rax, -1496(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rax, -1792(%rbp)
	leaq	-1520(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1760(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -1464(%rbp)
	movq	$0, -1456(%rbp)
	movq	%rax, %rbx
	movq	-1688(%rbp), %rax
	movq	$0, -1448(%rbp)
	movq	%rax, -1472(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1464(%rbp)
	leaq	-1416(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1448(%rbp)
	movq	%rdx, -1456(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1432(%rbp)
	movq	%rax, -1704(%rbp)
	movq	$0, -1440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1688(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1272(%rbp)
	movq	$0, -1264(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -1256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1272(%rbp)
	leaq	-1224(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1256(%rbp)
	movq	%rdx, -1264(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1240(%rbp)
	movq	%rax, -1736(%rbp)
	movq	$0, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1688(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1080(%rbp)
	movq	$0, -1072(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$0, -1064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1080(%rbp)
	leaq	-1032(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1064(%rbp)
	movq	%rdx, -1072(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1048(%rbp)
	movq	%rax, -1744(%rbp)
	movq	$0, -1056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1688(%rbp), %rax
	movl	$216, %edi
	movq	$0, -888(%rbp)
	movq	$0, -880(%rbp)
	movq	%rax, -896(%rbp)
	movq	$0, -872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -888(%rbp)
	leaq	-840(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -872(%rbp)
	movq	%rdx, -880(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -856(%rbp)
	movq	%rax, -1728(%rbp)
	movq	$0, -864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1688(%rbp), %rax
	movl	$216, %edi
	movq	$0, -696(%rbp)
	movq	$0, -688(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -696(%rbp)
	leaq	-648(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -680(%rbp)
	movq	%rdx, -688(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -664(%rbp)
	movq	%rax, -1712(%rbp)
	movq	$0, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1688(%rbp), %rax
	movl	$216, %edi
	movq	$0, -504(%rbp)
	movq	$0, -496(%rbp)
	movq	%rax, -512(%rbp)
	movq	$0, -488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -504(%rbp)
	leaq	-456(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -488(%rbp)
	movq	%rdx, -496(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -472(%rbp)
	movq	%rax, -1752(%rbp)
	movq	$0, -480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1688(%rbp), %rax
	movl	$216, %edi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -312(%rbp)
	leaq	-264(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	xorl	%edx, %edx
	movq	%rax, -1720(%rbp)
	movups	%xmm0, -280(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %xmm1
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movhps	-1808(%rbp), %xmm1
	movq	%rbx, -96(%rbp)
	leaq	-1552(%rbp), %r12
	movaps	%xmm1, -128(%rbp)
	movq	-1776(%rbp), %xmm1
	movaps	%xmm0, -1552(%rbp)
	movhps	-1792(%rbp), %xmm1
	movq	$0, -1536(%rbp)
	movaps	%xmm1, -112(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r13, %rdi
	movq	%r12, %rsi
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm5
	leaq	40(%rax), %rdx
	movq	%rax, -1552(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L404
	call	_ZdlPv@PLT
.L404:
	movq	-1704(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1408(%rbp)
	jne	.L571
	cmpq	$0, -1216(%rbp)
	jne	.L572
.L409:
	cmpq	$0, -1024(%rbp)
	jne	.L573
.L412:
	cmpq	$0, -832(%rbp)
	jne	.L574
.L415:
	cmpq	$0, -640(%rbp)
	jne	.L575
.L418:
	cmpq	$0, -448(%rbp)
	jne	.L576
.L422:
	cmpq	$0, -256(%rbp)
	jne	.L577
.L425:
	movq	-1720(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L428
	call	_ZdlPv@PLT
.L428:
	movq	-304(%rbp), %rbx
	movq	-312(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L429
	.p2align 4,,10
	.p2align 3
.L433:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L430
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L433
.L431:
	movq	-312(%rbp), %r12
.L429:
	testq	%r12, %r12
	je	.L434
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L434:
	movq	-1752(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L435
	call	_ZdlPv@PLT
.L435:
	movq	-496(%rbp), %rbx
	movq	-504(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L436
	.p2align 4,,10
	.p2align 3
.L440:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L437
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L440
.L438:
	movq	-504(%rbp), %r12
.L436:
	testq	%r12, %r12
	je	.L441
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L441:
	movq	-1712(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L442
	call	_ZdlPv@PLT
.L442:
	movq	-688(%rbp), %rbx
	movq	-696(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L443
	.p2align 4,,10
	.p2align 3
.L447:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L444
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L447
.L445:
	movq	-696(%rbp), %r12
.L443:
	testq	%r12, %r12
	je	.L448
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L448:
	movq	-1728(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-864(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L449
	call	_ZdlPv@PLT
.L449:
	movq	-880(%rbp), %rbx
	movq	-888(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L450
	.p2align 4,,10
	.p2align 3
.L454:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L451
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L454
.L452:
	movq	-888(%rbp), %r12
.L450:
	testq	%r12, %r12
	je	.L455
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L455:
	movq	-1744(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L456
	call	_ZdlPv@PLT
.L456:
	movq	-1072(%rbp), %rbx
	movq	-1080(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L457
	.p2align 4,,10
	.p2align 3
.L461:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L458
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L461
.L459:
	movq	-1080(%rbp), %r12
.L457:
	testq	%r12, %r12
	je	.L462
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L462:
	movq	-1736(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L463
	call	_ZdlPv@PLT
.L463:
	movq	-1264(%rbp), %rbx
	movq	-1272(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L464
	.p2align 4,,10
	.p2align 3
.L468:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L465
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L468
.L466:
	movq	-1272(%rbp), %r12
.L464:
	testq	%r12, %r12
	je	.L469
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L469:
	movq	-1704(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1440(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L470
	call	_ZdlPv@PLT
.L470:
	movq	-1456(%rbp), %rbx
	movq	-1464(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L471
	.p2align 4,,10
	.p2align 3
.L475:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L472
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L475
.L473:
	movq	-1464(%rbp), %r12
.L471:
	testq	%r12, %r12
	je	.L476
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L476:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L578
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L472:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L475
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L465:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L468
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L458:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L461
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L451:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L454
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L444:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L447
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L430:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L433
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L437:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L440
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L571:
	movq	-1704(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L406
	call	_ZdlPv@PLT
.L406:
	movq	(%rbx), %rax
	movl	$33, %edx
	movq	%r14, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	24(%rax), %rbx
	movq	%rsi, -1840(%rbp)
	movq	32(%rax), %rsi
	movq	%rcx, -1792(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -1808(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -1776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	-1808(%rbp), %rdx
	call	_ZN2v88internal17CodeStubAssembler12ToThisStringENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	%rax, -1848(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$34, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler21LoadStringLengthAsSmiENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -1824(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$37, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-1792(%rbp), %xmm3
	movq	-1776(%rbp), %rcx
	movhps	-1840(%rbp), %xmm3
	movq	%rcx, -1632(%rbp)
	movaps	%xmm3, -1648(%rbp)
	pushq	-1632(%rbp)
	pushq	-1640(%rbp)
	pushq	-1648(%rbp)
	movaps	%xmm3, -1792(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	-1824(%rbp), %rcx
	call	_ZN2v88internal23ToSmiBetweenZeroAnd_338EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_3SmiEEE
	movl	$40, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -1840(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	movdqa	-1792(%rbp), %xmm3
	movq	-1776(%rbp), %rax
	movaps	%xmm3, -1616(%rbp)
	movq	%rax, -1600(%rbp)
	pushq	-1600(%rbp)
	pushq	-1608(%rbp)
	pushq	-1616(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -1872(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1872(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm4
	pxor	%xmm0, %xmm0
	movq	-1824(%rbp), %xmm6
	movq	-1808(%rbp), %xmm7
	movdqa	-1792(%rbp), %xmm3
	movl	$64, %edi
	movaps	%xmm0, -1552(%rbp)
	movq	-1776(%rbp), %xmm2
	movhps	-1840(%rbp), %xmm6
	movq	$0, -1536(%rbp)
	movhps	-1848(%rbp), %xmm7
	movaps	%xmm6, -1824(%rbp)
	punpcklqdq	%xmm4, %xmm2
	movaps	%xmm7, -1808(%rbp)
	movaps	%xmm2, -1776(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm6, -80(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movq	%r12, %rsi
	leaq	64(%rax), %rdx
	leaq	-1280(%rbp), %rdi
	movq	%rax, -1552(%rbp)
	movups	%xmm2, (%rax)
	movdqa	-112(%rbp), %xmm2
	movups	%xmm3, 32(%rax)
	movups	%xmm2, 16(%rax)
	movdqa	-80(%rbp), %xmm2
	movq	%rdx, -1536(%rbp)
	movups	%xmm2, 48(%rax)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L407
	call	_ZdlPv@PLT
.L407:
	movdqa	-1792(%rbp), %xmm3
	movdqa	-1776(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movdqa	-1808(%rbp), %xmm2
	movaps	%xmm0, -1552(%rbp)
	movaps	%xmm3, -128(%rbp)
	movdqa	-1824(%rbp), %xmm3
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movq	$0, -1536(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm5
	movq	%r12, %rsi
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	leaq	64(%rax), %rdx
	leaq	-1088(%rbp), %rdi
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L408
	call	_ZdlPv@PLT
.L408:
	movq	-1744(%rbp), %rcx
	movq	-1736(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1216(%rbp)
	je	.L409
.L572:
	movq	-1736(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-1280(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	movabsq	$434042145146733829, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L410
	call	_ZdlPv@PLT
.L410:
	movq	(%rbx), %rax
	movl	$41, %edx
	movq	%r14, %rdi
	movq	8(%rax), %rsi
	movq	40(%rax), %rbx
	movq	(%rax), %rcx
	movq	16(%rax), %r13
	movq	%rsi, -1792(%rbp)
	movq	24(%rax), %rsi
	movq	%rbx, -1840(%rbp)
	movq	48(%rax), %rbx
	movq	%rsi, -1808(%rbp)
	movq	32(%rax), %rsi
	movq	56(%rax), %rax
	movq	%rcx, -1776(%rbp)
	movq	%rsi, -1824(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rax, -1848(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$40, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$72, %edi
	movq	%rbx, -64(%rbp)
	movq	-1776(%rbp), %xmm0
	movq	$0, -1536(%rbp)
	movhps	-1792(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%r13, %xmm0
	movhps	-1808(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-1824(%rbp), %xmm0
	movhps	-1840(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	movhps	-1848(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-128(%rbp), %xmm2
	leaq	-896(%rbp), %rdi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm4
	leaq	72(%rax), %rdx
	movq	%r12, %rsi
	movdqa	-80(%rbp), %xmm5
	movq	%rcx, 64(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L411
	call	_ZdlPv@PLT
.L411:
	movq	-1728(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1024(%rbp)
	je	.L412
.L573:
	movq	-1744(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-1088(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	movabsq	$434042145146733829, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L413
	call	_ZdlPv@PLT
.L413:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	32(%rax), %rbx
	movq	40(%rax), %rdx
	movq	%rsi, -1824(%rbp)
	movq	24(%rax), %rsi
	movq	%rcx, -1808(%rbp)
	movq	16(%rax), %rcx
	movq	%rbx, -1840(%rbp)
	movq	48(%rax), %rbx
	movq	56(%rax), %rax
	movq	%rsi, -1792(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -1848(%rbp)
	movl	$42, %edx
	movq	%rcx, -1776(%rbp)
	movq	%rax, -1872(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-1808(%rbp), %xmm0
	movq	-1776(%rbp), %rcx
	movhps	-1824(%rbp), %xmm0
	movq	%rcx, -1568(%rbp)
	movaps	%xmm0, -1584(%rbp)
	pushq	-1568(%rbp)
	pushq	-1576(%rbp)
	pushq	-1584(%rbp)
	movaps	%xmm0, -1808(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	-1792(%rbp), %rsi
	call	_ZN2v88internal23ToSmiBetweenZeroAnd_338EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_3SmiEEE
	movl	$40, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$72, %edi
	movq	%r13, -64(%rbp)
	movdqa	-1808(%rbp), %xmm0
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	-1776(%rbp), %xmm0
	movhps	-1792(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-1840(%rbp), %xmm0
	movhps	-1848(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	movhps	-1872(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-128(%rbp), %xmm6
	leaq	-704(%rbp), %rdi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm2
	leaq	72(%rax), %rdx
	movq	%r12, %rsi
	movdqa	-80(%rbp), %xmm3
	movq	%rcx, 64(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L414
	call	_ZdlPv@PLT
.L414:
	movq	-1712(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -832(%rbp)
	je	.L415
.L574:
	movq	-1728(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-896(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	movabsq	$434042145146733829, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$6, 8(%rax)
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L416
	call	_ZdlPv@PLT
.L416:
	movq	(%rbx), %rax
	movl	$72, %edi
	movdqu	48(%rax), %xmm0
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	64(%rax), %rdx
	movdqu	(%rax), %xmm4
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -64(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -1552(%rbp)
	movq	$0, -1536(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-128(%rbp), %xmm5
	leaq	-704(%rbp), %rdi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	72(%rax), %rdx
	movq	%r12, %rsi
	movdqa	-80(%rbp), %xmm2
	movq	%rcx, 64(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm2, 48(%rax)
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L417
	call	_ZdlPv@PLT
.L417:
	movq	-1712(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -640(%rbp)
	je	.L418
.L575:
	movq	-1712(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-704(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	movabsq	$434042145146733829, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$6, 8(%rax)
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L419
	call	_ZdlPv@PLT
.L419:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	8(%rax), %rsi
	movq	40(%rax), %rdx
	movq	24(%rax), %rbx
	movq	(%rax), %rcx
	movq	%rsi, -1792(%rbp)
	movq	16(%rax), %rsi
	movq	%rdx, -1848(%rbp)
	movq	48(%rax), %rdx
	movq	56(%rax), %r13
	movq	%rbx, -1824(%rbp)
	movq	32(%rax), %rbx
	movq	%rsi, -1808(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -1872(%rbp)
	movl	$43, %edx
	movq	%rcx, -1776(%rbp)
	movq	%rbx, -1840(%rbp)
	movq	64(%rax), %rbx
	movq	%r13, -1856(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movq	-1872(%rbp), %xmm4
	movq	-1840(%rbp), %xmm5
	movaps	%xmm0, -1552(%rbp)
	movq	-1808(%rbp), %xmm6
	movq	-1776(%rbp), %xmm7
	movq	%rbx, -64(%rbp)
	movhps	-1856(%rbp), %xmm4
	movhps	-1848(%rbp), %xmm5
	movhps	-1824(%rbp), %xmm6
	movaps	%xmm4, -1872(%rbp)
	movhps	-1792(%rbp), %xmm7
	movaps	%xmm5, -1840(%rbp)
	movaps	%xmm6, -1808(%rbp)
	movaps	%xmm7, -1776(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movq	$0, -1536(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-128(%rbp), %xmm3
	leaq	-512(%rbp), %rdi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	leaq	72(%rax), %rdx
	movq	%r12, %rsi
	movdqa	-80(%rbp), %xmm6
	movq	%rcx, 64(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L420
	call	_ZdlPv@PLT
.L420:
	movdqa	-1776(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movq	%rbx, -64(%rbp)
	movl	$72, %edi
	movdqa	-1808(%rbp), %xmm3
	movdqa	-1840(%rbp), %xmm2
	movaps	%xmm0, -1552(%rbp)
	movdqa	-1872(%rbp), %xmm4
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movq	$0, -1536(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-128(%rbp), %xmm5
	leaq	-320(%rbp), %rdi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	72(%rax), %rdx
	movq	%r12, %rsi
	movdqa	-80(%rbp), %xmm3
	movq	%rcx, 64(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L421
	call	_ZdlPv@PLT
.L421:
	movq	-1720(%rbp), %rcx
	movq	-1752(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -448(%rbp)
	je	.L422
.L576:
	movq	-1752(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-512(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	movabsq	$434042145146733829, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$6, 8(%rax)
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L423
	call	_ZdlPv@PLT
.L423:
	movq	(%rbx), %rax
	movl	$44, %edx
	movq	%r14, %rdi
	movq	24(%rax), %rbx
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	32(%rax), %r13
	movq	%rbx, -1824(%rbp)
	movq	40(%rax), %rbx
	movq	%rsi, -1792(%rbp)
	movq	16(%rax), %rsi
	movq	%rbx, -1840(%rbp)
	movq	48(%rax), %rbx
	movq	%rsi, -1808(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rbx, -1848(%rbp)
	movq	56(%rax), %rbx
	movq	64(%rax), %rax
	movq	%rcx, -1776(%rbp)
	movq	%rax, -1872(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$45, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$46, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$43, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$72, %edi
	movq	%rbx, -64(%rbp)
	movq	-1776(%rbp), %xmm0
	movq	$0, -1536(%rbp)
	movhps	-1792(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-1808(%rbp), %xmm0
	movhps	-1824(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%r13, %xmm0
	movhps	-1840(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	-1848(%rbp), %xmm0
	movhps	-1872(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-128(%rbp), %xmm2
	leaq	-320(%rbp), %rdi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	leaq	72(%rax), %rdx
	movq	%r12, %rsi
	movdqa	-80(%rbp), %xmm6
	movq	%rcx, 64(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L424
	call	_ZdlPv@PLT
.L424:
	movq	-1720(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -256(%rbp)
	je	.L425
.L577:
	movq	-1720(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-320(%rbp), %rbx
	xorl	%r13d, %r13d
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movabsq	$434042145146733829, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$6, 8(%rax)
	movq	%rax, -1552(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rdx, -1544(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1552(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L426
	call	_ZdlPv@PLT
.L426:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	leaq	.LC0(%rip), %rsi
	movq	40(%rax), %rdx
	movq	64(%rax), %r8
	movq	56(%rax), %rbx
	testq	%rdx, %rdx
	movq	%r8, -1776(%rbp)
	cmovne	%rdx, %r13
	movl	$48, %edx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1776(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17CodeStubAssembler8SmiUntagENS0_8compiler11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdi
	movq	%rax, -1776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	-1776(%rbp), %rcx
	call	_ZN2v88internal17CodeStubAssembler9SubStringENS0_8compiler5TNodeINS0_6StringEEENS3_INS0_7IntPtrTEEES7_@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1760(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	jmp	.L425
.L578:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22449:
	.size	_ZN2v88internal33StringPrototypeSubstringAssembler36GenerateStringPrototypeSubstringImplEv, .-_ZN2v88internal33StringPrototypeSubstringAssembler36GenerateStringPrototypeSubstringImplEv
	.section	.rodata._ZN2v88internal8Builtins33Generate_StringPrototypeSubstringEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/string-substring-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins33Generate_StringPrototypeSubstringEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"StringPrototypeSubstring"
	.section	.text._ZN2v88internal8Builtins33Generate_StringPrototypeSubstringEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins33Generate_StringPrototypeSubstringEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins33Generate_StringPrototypeSubstringEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins33Generate_StringPrototypeSubstringEPNS0_8compiler18CodeAssemblerStateE:
.LFB22445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$386, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$914, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L583
.L580:
	movq	%r13, %rdi
	call	_ZN2v88internal33StringPrototypeSubstringAssembler36GenerateStringPrototypeSubstringImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L584
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L583:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L580
.L584:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22445:
	.size	_ZN2v88internal8Builtins33Generate_StringPrototypeSubstringEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins33Generate_StringPrototypeSubstringEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal23ToSmiBetweenZeroAnd_338EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_3SmiEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal23ToSmiBetweenZeroAnd_338EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_3SmiEEE, @function
_GLOBAL__sub_I__ZN2v88internal23ToSmiBetweenZeroAnd_338EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_3SmiEEE:
.LFB29078:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29078:
	.size	_GLOBAL__sub_I__ZN2v88internal23ToSmiBetweenZeroAnd_338EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_3SmiEEE, .-_GLOBAL__sub_I__ZN2v88internal23ToSmiBetweenZeroAnd_338EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_3SmiEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal23ToSmiBetweenZeroAnd_338EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_3SmiEEE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
