	.file	"proxy-set-prototype-of-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev,"axG",@progbits,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev:
.LFB22421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$56, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L8
	call	_ZdlPv@PLT
.L8:
	movq	16(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	%r12, %r13
	je	.L9
	.p2align 4,,10
	.p2align 3
.L13:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L10
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L13
.L11:
	movq	8(%rbx), %r12
.L9:
	testq	%r12, %r12
	je	.L7
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L13
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L7:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22421:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev
	.set	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EE:
.LFB26585:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$8, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$578438803904464647, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L20
	movq	%rax, -120(%rbp)
	call	_ZdlPv@PLT
	movq	-120(%rbp), %rax
.L20:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L21
	movq	%rdx, (%r15)
.L21:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L22
	movq	%rdx, (%r14)
.L22:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L23
	movq	%rdx, 0(%r13)
.L23:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L24
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L24:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L25
	movq	%rdx, (%rbx)
.L25:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L26
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L26:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L27
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L27:
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L19
	movq	-112(%rbp), %rbx
	movq	%rax, (%rbx)
.L19:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L58
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L58:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26585:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectES8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EESM_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectES8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EESM_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectES8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EESM_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectES8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EESM_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectES8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EESM_:
.LFB26587:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$9, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$578438803904464647, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L60
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
.L60:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L61
	movq	%rdx, (%r15)
.L61:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L62
	movq	%rdx, (%r14)
.L62:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L63
	movq	%rdx, 0(%r13)
.L63:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L64
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L64:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L65
	movq	%rdx, (%rbx)
.L65:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L66
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L66:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L67
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L67:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L68
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L68:
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L59
	movq	-120(%rbp), %rbx
	movq	%rax, (%rbx)
.L59:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L102
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L102:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26587:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectES8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EESM_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectES8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EESM_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectES8_S8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EESM_SM_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectES8_S8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EESM_SM_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectES8_S8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EESM_SM_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectES8_S8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EESM_SM_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectES8_S8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EESM_SM_:
.LFB26589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$10, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$578438803904464647, %rcx
	movq	%rcx, (%rax)
	movl	$2056, %ecx
	leaq	10(%rax), %rdx
	movw	%cx, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L104
	movq	%rax, -136(%rbp)
	call	_ZdlPv@PLT
	movq	-136(%rbp), %rax
.L104:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L105
	movq	%rdx, (%r15)
.L105:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L106
	movq	%rdx, (%r14)
.L106:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L107
	movq	%rdx, 0(%r13)
.L107:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L108
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L108:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L109
	movq	%rdx, (%rbx)
.L109:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L110
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L110:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L111
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L111:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L112
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L112:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L113
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L113:
	movq	72(%rax), %rax
	testq	%rax, %rax
	je	.L103
	movq	-128(%rbp), %rsi
	movq	%rax, (%rsi)
.L103:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L150
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L150:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26589:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectES8_S8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EESM_SM_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectES8_S8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EESM_SM_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_6ObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_6ObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_6ObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_6ObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_6ObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_:
.LFB26590:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$6, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$1800, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$117901063, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L152
	movq	%rax, -104(%rbp)
	call	_ZdlPv@PLT
	movq	-104(%rbp), %rax
.L152:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L153
	movq	%rdx, (%r15)
.L153:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L154
	movq	%rdx, (%r14)
.L154:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L155
	movq	%rdx, 0(%r13)
.L155:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L156
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L156:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L157
	movq	%rdx, (%rbx)
.L157:
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L151
	movq	-96(%rbp), %rbx
	movq	%rax, (%rbx)
.L151:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L182
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L182:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26590:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_6ObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_6ObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_
	.section	.rodata._ZN2v88internal28ProxySetPrototypeOfAssembler31GenerateProxySetPrototypeOfImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../../deps/v8/../../deps/v8/src/builtins/proxy-set-prototype-of.tq"
	.section	.rodata._ZN2v88internal28ProxySetPrototypeOfAssembler31GenerateProxySetPrototypeOfImplEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	"setPrototypeOf"
	.section	.text._ZN2v88internal28ProxySetPrototypeOfAssembler31GenerateProxySetPrototypeOfImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28ProxySetPrototypeOfAssembler31GenerateProxySetPrototypeOfImplEv
	.type	_ZN2v88internal28ProxySetPrototypeOfAssembler31GenerateProxySetPrototypeOfImplEv, @function
_ZN2v88internal28ProxySetPrototypeOfAssembler31GenerateProxySetPrototypeOfImplEv:
.LFB22418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$264, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$3, %esi
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %r12
	movq	%r15, -4040(%rbp)
	leaq	-3736(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	-3544(%rbp), %rbx
	movq	%rax, -4064(%rbp)
	leaq	-3920(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -4072(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	leaq	-4040(%rbp), %r12
	movq	%rax, -4080(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$96, %edi
	movq	$0, -3784(%rbp)
	movq	%rax, -4096(%rbp)
	movq	-4040(%rbp), %rax
	movq	$0, -3776(%rbp)
	movq	%rax, -3792(%rbp)
	movq	$0, -3768(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -3784(%rbp)
	leaq	-3792(%rbp), %rax
	movq	%rdx, -3768(%rbp)
	movq	%rdx, -3776(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3752(%rbp)
	movq	%rax, -4056(%rbp)
	movq	$0, -3760(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4040(%rbp), %rax
	movl	$120, %edi
	movq	$0, -3592(%rbp)
	movq	$0, -3584(%rbp)
	movq	%rax, -3600(%rbp)
	movq	$0, -3576(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -3576(%rbp)
	movq	%rdx, -3584(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3560(%rbp)
	movq	%rax, -3592(%rbp)
	movq	$0, -3568(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4040(%rbp), %rax
	movl	$144, %edi
	movq	$0, -3400(%rbp)
	movq	$0, -3392(%rbp)
	movq	%rax, -3408(%rbp)
	movq	$0, -3384(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-3352(%rbp), %rcx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rdx, -3384(%rbp)
	movq	%rdx, -3392(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4128(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -3368(%rbp)
	movq	%rax, -3400(%rbp)
	movq	$0, -3376(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4040(%rbp), %rax
	movl	$168, %edi
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	%rax, -3216(%rbp)
	movq	$0, -3192(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-3160(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -3192(%rbp)
	movq	%rdx, -3200(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4136(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -3176(%rbp)
	movq	%rax, -3208(%rbp)
	movq	$0, -3184(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4040(%rbp), %rax
	movl	$192, %edi
	movq	$0, -3016(%rbp)
	movq	$0, -3008(%rbp)
	movq	%rax, -3024(%rbp)
	movq	$0, -3000(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2968(%rbp), %rcx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -3000(%rbp)
	movq	%rdx, -3008(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4144(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2984(%rbp)
	movq	%rax, -3016(%rbp)
	movq	$0, -2992(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4040(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2824(%rbp)
	movq	$0, -2816(%rbp)
	movq	%rax, -2832(%rbp)
	movq	$0, -2808(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2776(%rbp), %rcx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rdx, -2808(%rbp)
	movq	%rdx, -2816(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4160(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2792(%rbp)
	movq	%rax, -2824(%rbp)
	movq	$0, -2800(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4040(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2632(%rbp)
	movq	$0, -2624(%rbp)
	movq	%rax, -2640(%rbp)
	movq	$0, -2616(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2584(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -2616(%rbp)
	movq	%rdx, -2624(%rbp)
	movq	%rcx, -4208(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movups	%xmm0, -2600(%rbp)
	movq	%rax, -2632(%rbp)
	movq	$0, -2608(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4040(%rbp), %rax
	movl	$192, %edi
	movq	$0, -2440(%rbp)
	movq	$0, -2432(%rbp)
	movq	%rax, -2448(%rbp)
	movq	$0, -2424(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2392(%rbp), %rcx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -2424(%rbp)
	movq	%rdx, -2432(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4192(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2408(%rbp)
	movq	%rax, -2440(%rbp)
	movq	$0, -2416(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4040(%rbp), %rax
	movl	$192, %edi
	movq	$0, -2248(%rbp)
	movq	$0, -2240(%rbp)
	movq	%rax, -2256(%rbp)
	movq	$0, -2232(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2200(%rbp), %rcx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -2232(%rbp)
	movq	%rdx, -2240(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4232(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2216(%rbp)
	movq	%rax, -2248(%rbp)
	movq	$0, -2224(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4040(%rbp), %rax
	movl	$192, %edi
	movq	$0, -2056(%rbp)
	movq	$0, -2048(%rbp)
	movq	%rax, -2064(%rbp)
	movq	$0, -2040(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2008(%rbp), %rcx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -2040(%rbp)
	movq	%rdx, -2048(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4272(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2024(%rbp)
	movq	%rax, -2056(%rbp)
	movq	$0, -2032(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4040(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1864(%rbp)
	movq	$0, -1856(%rbp)
	movq	%rax, -1872(%rbp)
	movq	$0, -1848(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1816(%rbp), %rcx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -1848(%rbp)
	movq	%rdx, -1856(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4288(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1832(%rbp)
	movq	%rax, -1864(%rbp)
	movq	$0, -1840(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4040(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1672(%rbp)
	movq	$0, -1664(%rbp)
	movq	%rax, -1680(%rbp)
	movq	$0, -1656(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1624(%rbp), %rcx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -1656(%rbp)
	movq	%rdx, -1664(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4256(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1640(%rbp)
	movq	%rax, -1672(%rbp)
	movq	$0, -1648(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4040(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1480(%rbp)
	movq	$0, -1472(%rbp)
	movq	%rax, -1488(%rbp)
	movq	$0, -1464(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1432(%rbp), %rcx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -1464(%rbp)
	movq	%rdx, -1472(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4304(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1448(%rbp)
	movq	%rax, -1480(%rbp)
	movq	$0, -1456(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4040(%rbp), %rax
	movl	$240, %edi
	movq	$0, -1288(%rbp)
	movq	$0, -1280(%rbp)
	movq	%rax, -1296(%rbp)
	movq	$0, -1272(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1240(%rbp), %rcx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rdx, -1272(%rbp)
	movq	%rdx, -1280(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4352(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1256(%rbp)
	movq	%rax, -1288(%rbp)
	movq	$0, -1264(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4040(%rbp), %rax
	movl	$240, %edi
	movq	$0, -1096(%rbp)
	movq	$0, -1088(%rbp)
	movq	%rax, -1104(%rbp)
	movq	$0, -1080(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1048(%rbp), %rcx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rdx, -1080(%rbp)
	movq	%rdx, -1088(%rbp)
	movq	%rcx, -4360(%rbp)
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movups	%xmm0, -1064(%rbp)
	movq	%rax, -1096(%rbp)
	movq	$0, -1072(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4040(%rbp), %rax
	movl	$144, %edi
	movq	$0, -904(%rbp)
	movq	$0, -896(%rbp)
	movq	%rax, -912(%rbp)
	movq	$0, -888(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-856(%rbp), %rcx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rdx, -888(%rbp)
	movq	%rdx, -896(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4320(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -872(%rbp)
	movq	%rax, -904(%rbp)
	movq	$0, -880(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4040(%rbp), %rax
	movl	$144, %edi
	movq	$0, -712(%rbp)
	movq	$0, -704(%rbp)
	movq	%rax, -720(%rbp)
	movq	$0, -696(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-664(%rbp), %rcx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rdx, -696(%rbp)
	movq	%rdx, -704(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4328(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -680(%rbp)
	movq	%rax, -712(%rbp)
	movq	$0, -688(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4040(%rbp), %rax
	movl	$144, %edi
	movq	$0, -520(%rbp)
	movq	$0, -512(%rbp)
	movq	%rax, -528(%rbp)
	movq	$0, -504(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-472(%rbp), %rcx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rdx, -504(%rbp)
	movq	%rdx, -512(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4336(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -488(%rbp)
	movq	%rax, -520(%rbp)
	movq	$0, -496(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4040(%rbp), %rax
	movl	$96, %edi
	movq	$0, -328(%rbp)
	movq	$0, -320(%rbp)
	movq	%rax, -336(%rbp)
	movq	$0, -312(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	leaq	-280(%rbp), %rcx
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movq	%rcx, %rdi
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -312(%rbp)
	movq	%rdx, -320(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -4240(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -328(%rbp)
	movups	%xmm0, -296(%rbp)
	movq	$0, -304(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-4064(%rbp), %xmm1
	movaps	%xmm0, -3920(%rbp)
	movhps	-4072(%rbp), %xmm1
	movq	$0, -3904(%rbp)
	movaps	%xmm1, -144(%rbp)
	movq	-4080(%rbp), %xmm1
	movhps	-4096(%rbp), %xmm1
	movaps	%xmm1, -128(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm3
	movq	%r13, %rsi
	movq	-4056(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -3920(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -3904(%rbp)
	movq	%rdx, -3912(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3920(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L184
	call	_ZdlPv@PLT
.L184:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3408(%rbp), %rax
	cmpq	$0, -3728(%rbp)
	movq	%rax, -4080(%rbp)
	leaq	-3600(%rbp), %rax
	movq	%rax, -4064(%rbp)
	jne	.L332
.L185:
	leaq	-336(%rbp), %rax
	cmpq	$0, -3536(%rbp)
	movq	%rax, -4112(%rbp)
	jne	.L333
.L190:
	leaq	-3024(%rbp), %rax
	cmpq	$0, -3344(%rbp)
	movq	%rax, -4096(%rbp)
	leaq	-3216(%rbp), %rax
	movq	%rax, -4072(%rbp)
	jne	.L334
.L193:
	leaq	-2832(%rbp), %rax
	cmpq	$0, -3152(%rbp)
	movq	%rax, -4128(%rbp)
	jne	.L335
.L198:
	leaq	-2640(%rbp), %rax
	cmpq	$0, -2960(%rbp)
	movq	%rax, -4136(%rbp)
	jne	.L336
.L201:
	leaq	-912(%rbp), %rax
	cmpq	$0, -2768(%rbp)
	movq	%rax, -4176(%rbp)
	jne	.L337
.L204:
	leaq	-2448(%rbp), %rax
	cmpq	$0, -2576(%rbp)
	movq	%rax, -4144(%rbp)
	leaq	-1872(%rbp), %rax
	movq	%rax, -4160(%rbp)
	jne	.L338
.L207:
	leaq	-2256(%rbp), %rax
	cmpq	$0, -2384(%rbp)
	movq	%rax, -4208(%rbp)
	leaq	-2064(%rbp), %rax
	movq	%rax, -4224(%rbp)
	jne	.L339
	cmpq	$0, -2192(%rbp)
	jne	.L340
.L217:
	cmpq	$0, -2000(%rbp)
	jne	.L341
.L218:
	leaq	-1680(%rbp), %rax
	cmpq	$0, -1808(%rbp)
	movq	%rax, -4232(%rbp)
	leaq	-1488(%rbp), %rax
	movq	%rax, -4192(%rbp)
	jne	.L342
	cmpq	$0, -1616(%rbp)
	jne	.L343
.L222:
	leaq	-1296(%rbp), %rax
	cmpq	$0, -1424(%rbp)
	movq	%rax, -4272(%rbp)
	leaq	-1104(%rbp), %rax
	movq	%rax, -4288(%rbp)
	jne	.L344
	cmpq	$0, -1232(%rbp)
	jne	.L345
.L226:
	cmpq	$0, -1040(%rbp)
	jne	.L346
.L227:
	leaq	-720(%rbp), %rax
	cmpq	$0, -848(%rbp)
	leaq	-528(%rbp), %r14
	movq	%rax, -4256(%rbp)
	jne	.L347
	cmpq	$0, -656(%rbp)
	jne	.L348
.L231:
	cmpq	$0, -464(%rbp)
	jne	.L349
.L232:
	cmpq	$0, -272(%rbp)
	jne	.L350
.L233:
	movq	-4112(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4256(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4176(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4288(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4272(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4192(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4232(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4160(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4224(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4208(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4144(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4136(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4128(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4096(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4072(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4080(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4064(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4056(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L351
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -3904(%rbp)
	movaps	%xmm0, -3920(%rbp)
	call	_Znwm@PLT
	movq	-4056(%rbp), %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -3920(%rbp)
	movq	%rdx, -3904(%rbp)
	movq	%rdx, -3912(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3920(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L186
	call	_ZdlPv@PLT
.L186:
	movq	(%r14), %rax
	movl	$14, %edx
	movq	%r12, %rdi
	movq	16(%rax), %rsi
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	24(%rax), %rax
	movq	%rsi, -4080(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rcx, -4064(%rbp)
	movq	%rax, -4096(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$25, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -4072(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-4072(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-4064(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -4072(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4072(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal20Cast10JSReceiver_140EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-4072(%rbp), %rcx
	movq	%r14, %xmm5
	movq	-4080(%rbp), %xmm4
	movhps	-4064(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movq	%rax, -104(%rbp)
	movhps	-4096(%rbp), %xmm4
	movq	%rcx, -112(%rbp)
	leaq	-3952(%rbp), %r14
	movaps	%xmm4, -4112(%rbp)
	movaps	%xmm5, -4096(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm0, -3952(%rbp)
	movq	$0, -3936(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm5
	movq	%r14, %rsi
	leaq	48(%rax), %rdx
	movq	%rax, -3952(%rbp)
	movups	%xmm4, (%rax)
	movdqa	-128(%rbp), %xmm4
	movups	%xmm5, 32(%rax)
	movups	%xmm4, 16(%rax)
	leaq	-3408(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3936(%rbp)
	movq	%rdx, -3944(%rbp)
	movq	%rax, -4080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L187
	call	_ZdlPv@PLT
.L187:
	movq	-4128(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3600(%rbp), %rax
	cmpq	$0, -3912(%rbp)
	movq	%rax, -4064(%rbp)
	jne	.L352
.L188:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L333:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -3904(%rbp)
	movaps	%xmm0, -3920(%rbp)
	call	_Znwm@PLT
	movq	-4064(%rbp), %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -3920(%rbp)
	movq	%rdx, -3904(%rbp)
	movq	%rdx, -3912(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3920(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L191
	call	_ZdlPv@PLT
.L191:
	movq	(%rbx), %rax
	movl	$32, %edi
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm6
	movq	$0, -3904(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm0, -3920(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm4
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	movq	%rax, -3920(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	leaq	-336(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3904(%rbp)
	movq	%rdx, -3912(%rbp)
	movq	%rax, -4112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3920(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L192
	call	_ZdlPv@PLT
.L192:
	movq	-4240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L334:
	movq	-4128(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -3904(%rbp)
	movaps	%xmm0, -3920(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movq	-4080(%rbp), %rdi
	movw	%si, 4(%rax)
	leaq	6(%rax), %rdx
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	movq	%rax, -3920(%rbp)
	movq	%rdx, -3904(%rbp)
	movq	%rdx, -3912(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3920(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L194
	call	_ZdlPv@PLT
.L194:
	movq	(%rbx), %rax
	movl	$28, %edx
	movq	%r12, %rdi
	movq	24(%rax), %rsi
	movq	16(%rax), %rcx
	movq	8(%rax), %rbx
	movq	(%rax), %r14
	movq	%rsi, -4176(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rcx, -4128(%rbp)
	movq	%rbx, -4072(%rbp)
	movq	40(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -4096(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-4096(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-4072(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -4096(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$32, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r13, %r8
	movq	%rbx, %rdx
	leaq	.LC2(%rip), %rcx
	movq	%r15, %rdi
	call	_ZN2v88internal13GetMethod_245EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPKcPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm2
	movq	%rbx, %xmm6
	movq	-4128(%rbp), %xmm7
	movhps	-4096(%rbp), %xmm6
	movhps	-4072(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movhps	-4176(%rbp), %xmm7
	movaps	%xmm6, -4224(%rbp)
	leaq	-3952(%rbp), %r14
	movaps	%xmm7, -4176(%rbp)
	movaps	%xmm2, -4128(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm0, -3952(%rbp)
	movq	%rbx, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	$0, -3936(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	64(%rax), %rdx
	movq	%rax, -3952(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	leaq	-3024(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3936(%rbp)
	movq	%rdx, -3944(%rbp)
	movq	%rax, -4096(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L195
	call	_ZdlPv@PLT
.L195:
	movq	-4144(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3216(%rbp), %rax
	cmpq	$0, -3912(%rbp)
	movq	%rax, -4072(%rbp)
	jne	.L353
.L196:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L335:
	movq	-4136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -3904(%rbp)
	movaps	%xmm0, -3920(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	-4072(%rbp), %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -3920(%rbp)
	movq	%rdx, -3904(%rbp)
	movq	%rdx, -3912(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3920(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L199
	call	_ZdlPv@PLT
.L199:
	movq	(%rbx), %rax
	movl	$48, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movdqu	(%rax), %xmm4
	movq	$0, -3904(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3920(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm2
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm5
	leaq	48(%rax), %rdx
	movq	%rax, -3920(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm5, 32(%rax)
	leaq	-2832(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3904(%rbp)
	movq	%rdx, -3912(%rbp)
	movq	%rax, -4128(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3920(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L200
	call	_ZdlPv@PLT
.L200:
	movq	-4160(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L336:
	movq	-4144(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$506381209866536711, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3904(%rbp)
	movaps	%xmm0, -3920(%rbp)
	call	_Znwm@PLT
	movq	-4096(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3920(%rbp)
	movq	%rdx, -3904(%rbp)
	movq	%rdx, -3912(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3920(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L202
	call	_ZdlPv@PLT
.L202:
	movq	(%rbx), %rax
	movl	$56, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movq	56(%rax), %rdx
	movdqu	(%rax), %xmm6
	movq	$0, -3904(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3920(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm7
	leaq	56(%rax), %rdx
	movq	%rax, -3920(%rbp)
	movq	%rcx, 48(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm3, 32(%rax)
	leaq	-2640(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3904(%rbp)
	movq	%rdx, -3912(%rbp)
	movq	%rax, -4136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3920(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L203
	call	_ZdlPv@PLT
.L203:
	movq	-4208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L337:
	movq	-4160(%rbp), %rsi
	movq	%r12, %rdi
	movl	$1799, %r14d
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -3904(%rbp)
	movaps	%xmm0, -3920(%rbp)
	call	_Znwm@PLT
	movq	-4128(%rbp), %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	6(%rax), %rdx
	movw	%r14w, 4(%rax)
	movq	%rax, -3920(%rbp)
	movq	%rdx, -3904(%rbp)
	movq	%rdx, -3912(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3920(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L205
	call	_ZdlPv@PLT
.L205:
	movq	(%rbx), %rax
	movl	$33, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rbx
	movq	24(%rax), %rcx
	movq	(%rax), %r14
	movq	%rbx, -4160(%rbp)
	movq	16(%rax), %rbx
	movq	%rcx, -4176(%rbp)
	movq	%rbx, -4144(%rbp)
	movq	40(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %xmm0
	movl	$48, %edi
	movq	$0, -3904(%rbp)
	movhps	-4160(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-4144(%rbp), %xmm0
	movhps	-4176(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%rbx, %xmm0
	movhps	-4144(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3920(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm7
	leaq	48(%rax), %rdx
	movq	%rax, -3920(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	leaq	-912(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3904(%rbp)
	movq	%rdx, -3912(%rbp)
	movq	%rax, -4176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3920(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L206
	call	_ZdlPv@PLT
.L206:
	movq	-4320(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L338:
	movq	-4208(%rbp), %rsi
	movq	%r12, %rdi
	movl	$1799, %ebx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -3904(%rbp)
	movaps	%xmm0, -3920(%rbp)
	call	_Znwm@PLT
	movq	-4136(%rbp), %rdi
	movq	%r13, %rsi
	movw	%bx, 4(%rax)
	leaq	7(%rax), %rdx
	movl	$117901063, (%rax)
	movb	$7, 6(%rax)
	movq	%rax, -3920(%rbp)
	movq	%rdx, -3904(%rbp)
	movq	%rdx, -3912(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3920(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L208
	call	_ZdlPv@PLT
.L208:
	movq	(%rbx), %rax
	movl	$32, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	32(%rax), %r14
	movq	%rbx, -4144(%rbp)
	movq	8(%rax), %rbx
	movq	%rbx, -4384(%rbp)
	movq	16(%rax), %rbx
	movq	%rbx, -4208(%rbp)
	movq	24(%rax), %rbx
	movq	%rbx, -4400(%rbp)
	movq	40(%rax), %rbx
	movq	48(%rax), %rax
	movq	%rbx, -4160(%rbp)
	leaq	-3968(%rbp), %rbx
	movq	%rax, -4224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$37, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19IsUndefinedConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	je	.L209
.L211:
	movq	%r14, %xmm2
	movq	%rbx, %rdi
	movhps	-4160(%rbp), %xmm2
	movaps	%xmm2, -4160(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-3920(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, -4368(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-4160(%rbp), %xmm2
	movq	-4224(%rbp), %xmm0
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -3952(%rbp)
	movq	-3904(%rbp), %rax
	movhps	-4368(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%rax, -3944(%rbp)
	movaps	%xmm2, -128(%rbp)
.L331:
	leaq	-144(%rbp), %rsi
	movl	$5, %edi
	movq	-4144(%rbp), %r9
	movq	-4208(%rbp), %rax
	pushq	%rdi
	movl	$1, %ecx
	leaq	-3952(%rbp), %rdx
	movq	%rbx, %rdi
	pushq	%rsi
	xorl	%esi, %esi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	popq	%r10
	movq	%rbx, %rdi
	popq	%r11
	movq	-4208(%rbp), %xmm4
	movq	%rax, %r14
	movq	-4144(%rbp), %xmm3
	movhps	-4400(%rbp), %xmm4
	movhps	-4384(%rbp), %xmm3
	movaps	%xmm3, -4384(%rbp)
	movaps	%xmm4, -4208(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$40, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal13ToBoolean_240EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15Word32BinaryNotENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %xmm1
	pxor	%xmm0, %xmm0
	movdqa	-4208(%rbp), %xmm4
	movq	-4224(%rbp), %xmm5
	movdqa	-4384(%rbp), %xmm3
	movl	$64, %edi
	movaps	%xmm0, -3920(%rbp)
	movaps	%xmm4, -128(%rbp)
	movdqa	-4160(%rbp), %xmm4
	punpcklqdq	%xmm1, %xmm5
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm5, -4224(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movq	$0, -3904(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm6
	leaq	64(%rax), %rdx
	movq	%rax, -3920(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm6, 48(%rax)
	leaq	-2448(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3904(%rbp)
	movq	%rdx, -3912(%rbp)
	movq	%rax, -4144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3920(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L212
	call	_ZdlPv@PLT
.L212:
	movdqa	-4384(%rbp), %xmm7
	movdqa	-4208(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movdqa	-4160(%rbp), %xmm1
	movdqa	-4224(%rbp), %xmm4
	movaps	%xmm0, -3920(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movq	$0, -3904(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm2
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	64(%rax), %rdx
	movq	%rax, -3920(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	leaq	-1872(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3904(%rbp)
	movq	%rdx, -3912(%rbp)
	movq	%rax, -4160(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3920(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L213
	call	_ZdlPv@PLT
.L213:
	movq	-4288(%rbp), %rcx
	movq	-4192(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L339:
	movq	-4192(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -4016(%rbp)
	movq	$0, -4008(%rbp)
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	movq	$0, -3968(%rbp)
	movq	$0, -3952(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3952(%rbp), %rax
	movq	-4144(%rbp), %rdi
	pushq	%rax
	leaq	-3968(%rbp), %rax
	leaq	-4000(%rbp), %rcx
	pushq	%rax
	leaq	-3976(%rbp), %rax
	leaq	-3984(%rbp), %r9
	pushq	%rax
	leaq	-3992(%rbp), %r8
	leaq	-4008(%rbp), %rdx
	leaq	-4016(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EE
	addq	$32, %rsp
	movl	$41, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3992(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	-3968(%rbp), %xmm6
	movq	-3984(%rbp), %xmm7
	movaps	%xmm0, -3920(%rbp)
	movq	-4000(%rbp), %xmm3
	movq	-4016(%rbp), %xmm2
	movhps	-3952(%rbp), %xmm6
	movq	$0, -3904(%rbp)
	movhps	-3976(%rbp), %xmm7
	movhps	-3992(%rbp), %xmm3
	movaps	%xmm6, -4400(%rbp)
	movhps	-4008(%rbp), %xmm2
	movaps	%xmm7, -4224(%rbp)
	movaps	%xmm3, -4192(%rbp)
	movaps	%xmm2, -4384(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm1
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm3
	leaq	64(%rax), %rdx
	movq	%rax, -3920(%rbp)
	movq	-4208(%rbp), %rdi
	movups	%xmm5, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rdx, -3904(%rbp)
	movq	%rdx, -3912(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3920(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L215
	call	_ZdlPv@PLT
.L215:
	movdqa	-4384(%rbp), %xmm2
	movdqa	-4192(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movdqa	-4224(%rbp), %xmm7
	movdqa	-4400(%rbp), %xmm5
	movaps	%xmm0, -3920(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movq	$0, -3904(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm1
	movdqa	-128(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm2
	leaq	64(%rax), %rdx
	movq	%rax, -3920(%rbp)
	movups	%xmm1, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm2, 48(%rax)
	leaq	-2064(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3904(%rbp)
	movq	%rdx, -3912(%rbp)
	movq	%rax, -4224(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3920(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L216
	call	_ZdlPv@PLT
.L216:
	movq	-4272(%rbp), %rcx
	movq	-4232(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2192(%rbp)
	je	.L217
.L340:
	movq	-4232(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -4016(%rbp)
	movq	$0, -4008(%rbp)
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	movq	$0, -3968(%rbp)
	movq	$0, -3952(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3952(%rbp), %rax
	movq	-4208(%rbp), %rdi
	pushq	%rax
	leaq	-3968(%rbp), %rax
	leaq	-3984(%rbp), %r9
	pushq	%rax
	leaq	-3976(%rbp), %rax
	leaq	-4000(%rbp), %rcx
	pushq	%rax
	leaq	-3992(%rbp), %r8
	leaq	-4008(%rbp), %rdx
	leaq	-4016(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EE
	addq	$32, %rsp
	movl	$42, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	movl	$150, %edx
	movq	%r13, %rdi
	movq	-4016(%rbp), %rsi
	leaq	.LC2(%rip), %rcx
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -2000(%rbp)
	je	.L218
.L341:
	movq	-4272(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -4016(%rbp)
	movq	$0, -4008(%rbp)
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	movq	$0, -3968(%rbp)
	movq	$0, -3952(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3952(%rbp), %rax
	movq	-4224(%rbp), %rdi
	pushq	%rax
	leaq	-3968(%rbp), %rax
	leaq	-4000(%rbp), %rcx
	pushq	%rax
	leaq	-3976(%rbp), %rax
	leaq	-3984(%rbp), %r9
	pushq	%rax
	leaq	-3992(%rbp), %r8
	leaq	-4008(%rbp), %rdx
	leaq	-4016(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EE
	addq	$32, %rsp
	movl	$44, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8False_66EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L342:
	movq	-4288(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -4016(%rbp)
	movq	$0, -4008(%rbp)
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	movq	$0, -3968(%rbp)
	movq	$0, -3952(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3952(%rbp), %rax
	movq	-4160(%rbp), %rdi
	pushq	%rax
	leaq	-3968(%rbp), %rax
	leaq	-4000(%rbp), %rcx
	pushq	%rax
	leaq	-3976(%rbp), %rax
	leaq	-3984(%rbp), %r9
	pushq	%rax
	leaq	-3992(%rbp), %r8
	leaq	-4008(%rbp), %rdx
	leaq	-4016(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EE
	addq	$32, %rsp
	movl	$49, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3976(%rbp), %rdx
	movq	-4016(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal22ObjectIsExtensible_311EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$51, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movq	-3968(%rbp), %xmm4
	movq	-3984(%rbp), %xmm5
	movaps	%xmm0, -3920(%rbp)
	movq	-4000(%rbp), %xmm6
	movq	-4016(%rbp), %xmm7
	movq	%rbx, -80(%rbp)
	movhps	-3952(%rbp), %xmm4
	movhps	-3976(%rbp), %xmm5
	movhps	-3992(%rbp), %xmm6
	movaps	%xmm4, -4384(%rbp)
	movhps	-4008(%rbp), %xmm7
	movaps	%xmm5, -4288(%rbp)
	movaps	%xmm6, -4272(%rbp)
	movaps	%xmm7, -4192(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movq	$0, -3904(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm1
	leaq	72(%rax), %rdx
	movq	%rax, -3920(%rbp)
	movdqa	-144(%rbp), %xmm6
	movq	-4232(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm1, 48(%rax)
	movq	%rdx, -3904(%rbp)
	movq	%rdx, -3912(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3920(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L220
	call	_ZdlPv@PLT
.L220:
	movdqa	-4192(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movq	%rbx, -80(%rbp)
	movl	$72, %edi
	movdqa	-4272(%rbp), %xmm3
	movdqa	-4288(%rbp), %xmm2
	movaps	%xmm0, -3920(%rbp)
	movdqa	-4384(%rbp), %xmm6
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movq	$0, -3904(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm1
	movdqa	-144(%rbp), %xmm7
	leaq	72(%rax), %rdx
	movq	%rax, -3920(%rbp)
	movdqa	-96(%rbp), %xmm4
	movq	%rcx, 64(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm4, 48(%rax)
	leaq	-1488(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3904(%rbp)
	movq	%rdx, -3912(%rbp)
	movq	%rax, -4192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3920(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L221
	call	_ZdlPv@PLT
.L221:
	movq	-4304(%rbp), %rcx
	movq	-4256(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1616(%rbp)
	je	.L222
.L343:
	movq	-4256(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -4024(%rbp)
	movq	$0, -4016(%rbp)
	movq	$0, -4008(%rbp)
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	movq	$0, -3968(%rbp)
	movq	$0, -3952(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3952(%rbp), %rax
	movq	-4232(%rbp), %rdi
	leaq	-4008(%rbp), %rcx
	pushq	%rax
	leaq	-3968(%rbp), %rax
	leaq	-3992(%rbp), %r9
	pushq	%rax
	leaq	-3976(%rbp), %rax
	leaq	-4000(%rbp), %r8
	pushq	%rax
	leaq	-3984(%rbp), %rax
	leaq	-4016(%rbp), %rdx
	pushq	%rax
	leaq	-4024(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectES8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EESM_
	addq	$32, %rsp
	movl	$52, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L344:
	movq	-4304(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -4024(%rbp)
	movq	$0, -4016(%rbp)
	movq	$0, -4008(%rbp)
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	movq	$0, -3968(%rbp)
	movq	$0, -3952(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3952(%rbp), %rax
	movq	-4192(%rbp), %rdi
	leaq	-4008(%rbp), %rcx
	pushq	%rax
	leaq	-3968(%rbp), %rax
	leaq	-3992(%rbp), %r9
	pushq	%rax
	leaq	-3976(%rbp), %rax
	leaq	-4000(%rbp), %r8
	pushq	%rax
	leaq	-3984(%rbp), %rax
	leaq	-4016(%rbp), %rdx
	pushq	%rax
	leaq	-4024(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectES8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EESM_
	addq	$32, %rsp
	movl	$56, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3984(%rbp), %rdx
	movq	-4024(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal24ObjectGetPrototypeOf_314EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$61, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4008(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal13SameValue_242EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEES6_@PLT
	pxor	%xmm0, %xmm0
	movl	$80, %edi
	movq	-3976(%rbp), %xmm3
	movq	-3992(%rbp), %xmm2
	movq	%rax, %r14
	movq	-3952(%rbp), %rax
	movq	%rbx, -72(%rbp)
	movq	-4008(%rbp), %xmm4
	movhps	-3968(%rbp), %xmm3
	movq	-4024(%rbp), %xmm5
	movaps	%xmm0, -3920(%rbp)
	movhps	-3984(%rbp), %xmm2
	movaps	%xmm3, -4384(%rbp)
	movhps	-4000(%rbp), %xmm4
	movhps	-4016(%rbp), %xmm5
	movaps	%xmm2, -4304(%rbp)
	movaps	%xmm4, -4288(%rbp)
	movaps	%xmm5, -4256(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movq	%rax, -4400(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -3904(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm2
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	80(%rax), %rdx
	movq	%rax, -3920(%rbp)
	movdqa	-80(%rbp), %xmm5
	movq	-4272(%rbp), %rdi
	movups	%xmm3, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rdx, -3904(%rbp)
	movq	%rdx, -3912(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3920(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L224
	call	_ZdlPv@PLT
.L224:
	movq	-4400(%rbp), %xmm0
	movq	%rbx, %xmm6
	movdqa	-4256(%rbp), %xmm1
	movl	$80, %edi
	movdqa	-4288(%rbp), %xmm4
	movdqa	-4304(%rbp), %xmm3
	movq	$0, -3904(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movdqa	-4384(%rbp), %xmm2
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -3920(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm1
	movdqa	-96(%rbp), %xmm4
	leaq	80(%rax), %rdx
	movq	%rax, -3920(%rbp)
	movdqa	-80(%rbp), %xmm3
	movups	%xmm7, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm3, 64(%rax)
	leaq	-1104(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3904(%rbp)
	movq	%rdx, -3912(%rbp)
	movq	%rax, -4288(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3920(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L225
	call	_ZdlPv@PLT
.L225:
	movq	-4360(%rbp), %rcx
	movq	-4352(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1232(%rbp)
	je	.L226
.L345:
	movq	-4352(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -4032(%rbp)
	movq	$0, -4024(%rbp)
	movq	$0, -4016(%rbp)
	movq	$0, -4008(%rbp)
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	movq	$0, -3968(%rbp)
	movq	$0, -3952(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3952(%rbp), %rax
	movq	-4272(%rbp), %rdi
	pushq	%rax
	leaq	-3968(%rbp), %rax
	leaq	-4016(%rbp), %rcx
	pushq	%rax
	leaq	-3976(%rbp), %rax
	leaq	-4000(%rbp), %r9
	pushq	%rax
	leaq	-3984(%rbp), %rax
	leaq	-4008(%rbp), %r8
	pushq	%rax
	leaq	-3992(%rbp), %rax
	leaq	-4024(%rbp), %rdx
	pushq	%rax
	leaq	-4032(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectES8_S8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EESM_SM_
	addq	$48, %rsp
	movl	$62, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -1040(%rbp)
	je	.L227
.L346:
	movq	-4360(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -4032(%rbp)
	movq	$0, -4024(%rbp)
	movq	$0, -4016(%rbp)
	movq	$0, -4008(%rbp)
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	movq	$0, -3968(%rbp)
	movq	$0, -3952(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3952(%rbp), %rax
	movq	-4288(%rbp), %rdi
	pushq	%rax
	leaq	-3968(%rbp), %rax
	leaq	-4016(%rbp), %rcx
	pushq	%rax
	leaq	-3976(%rbp), %rax
	leaq	-4000(%rbp), %r9
	pushq	%rax
	leaq	-3984(%rbp), %rax
	leaq	-4008(%rbp), %r8
	pushq	%rax
	leaq	-3992(%rbp), %rax
	leaq	-4024(%rbp), %rdx
	pushq	%rax
	leaq	-4032(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_10JSReceiverES5_S7_NS0_6ObjectES8_S8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EESG_SK_PNSA_IS8_EESM_SM_
	addq	$48, %rsp
	movq	%r12, %rdi
	movl	$64, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	-4032(%rbp), %rsi
	movl	$148, %edx
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L347:
	movq	-4320(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	movq	$0, -3968(%rbp)
	movq	$0, -3952(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3952(%rbp), %rax
	movq	-4176(%rbp), %rdi
	pushq	%rax
	leaq	-3968(%rbp), %r9
	leaq	-3976(%rbp), %r8
	leaq	-3984(%rbp), %rcx
	leaq	-3992(%rbp), %rdx
	leaq	-4000(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_6ObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_
	movl	$68, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3976(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movq	-3968(%rbp), %xmm6
	movq	-3984(%rbp), %xmm7
	movq	-4000(%rbp), %xmm3
	movaps	%xmm0, -3920(%rbp)
	movhps	-3952(%rbp), %xmm6
	movq	$0, -3904(%rbp)
	movhps	-3976(%rbp), %xmm7
	movhps	-3992(%rbp), %xmm3
	movaps	%xmm6, -4352(%rbp)
	movaps	%xmm7, -4320(%rbp)
	movaps	%xmm3, -4304(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm2
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	movq	-4256(%rbp), %rdi
	leaq	48(%rax), %rdx
	movq	%rax, -3920(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -3904(%rbp)
	movq	%rdx, -3912(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3920(%rbp), %rdi
	popq	%r8
	popq	%r9
	testq	%rdi, %rdi
	je	.L229
	call	_ZdlPv@PLT
.L229:
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	leaq	-528(%rbp), %r14
	movdqa	-4304(%rbp), %xmm5
	movdqa	-4320(%rbp), %xmm1
	movdqa	-4352(%rbp), %xmm4
	movaps	%xmm0, -3920(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movq	$0, -3904(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm2
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-144(%rbp), %xmm3
	leaq	48(%rax), %rdx
	movq	%rax, -3920(%rbp)
	movups	%xmm2, 16(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm5, 32(%rax)
	movq	%rdx, -3904(%rbp)
	movq	%rdx, -3912(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3920(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L230
	call	_ZdlPv@PLT
.L230:
	movq	-4336(%rbp), %rcx
	movq	-4328(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -656(%rbp)
	je	.L231
.L348:
	movq	-4328(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	movq	$0, -3968(%rbp)
	movq	$0, -3952(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3952(%rbp), %rax
	movq	-4256(%rbp), %rdi
	pushq	%rax
	leaq	-3968(%rbp), %r9
	leaq	-3976(%rbp), %r8
	leaq	-3984(%rbp), %rcx
	leaq	-3992(%rbp), %rdx
	leaq	-4000(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_6ObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_
	movl	$69, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3952(%rbp), %rcx
	movq	-3968(%rbp), %rdx
	movq	%r15, %rdi
	movq	-4000(%rbp), %rsi
	call	_ZN2v88internal29ObjectSetPrototypeOfThrow_316EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_10HeapObjectEEE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -464(%rbp)
	popq	%rcx
	popq	%rsi
	je	.L232
.L349:
	movq	-4336(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3976(%rbp)
	movq	$0, -3968(%rbp)
	movq	$0, -3952(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r14, %rdi
	leaq	-3952(%rbp), %rax
	pushq	%rax
	leaq	-3968(%rbp), %r9
	leaq	-3976(%rbp), %r8
	leaq	-3984(%rbp), %rcx
	leaq	-3992(%rbp), %rdx
	leaq	-4000(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_10HeapObjectENS0_7OddballENS0_6ObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EEPNS9_IS7_EESF_
	movl	$71, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3968(%rbp), %rdx
	movq	-3952(%rbp), %rcx
	movq	%r15, %rdi
	movq	-4000(%rbp), %rsi
	call	_ZN2v88internal33ObjectSetPrototypeOfDontThrow_317EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEENS4_INS0_10HeapObjectEEE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -272(%rbp)
	popq	%rax
	popq	%rdx
	je	.L233
.L350:
	movq	-4240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -3904(%rbp)
	movaps	%xmm0, -3920(%rbp)
	call	_Znwm@PLT
	movq	-4112(%rbp), %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -3920(%rbp)
	movq	%rdx, -3904(%rbp)
	movq	%rdx, -3912(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3920(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L234
	call	_ZdlPv@PLT
.L234:
	movq	(%rbx), %rax
	movl	$74, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movl	$145, %edx
	leaq	.LC2(%rip), %rcx
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L209:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IsNullConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L211
	movq	%r14, %xmm6
	movq	%rbx, %rdi
	movhps	-4160(%rbp), %xmm6
	movaps	%xmm6, -4160(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$2, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-3920(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, -4368(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-4160(%rbp), %xmm6
	movq	-4224(%rbp), %xmm0
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, -3952(%rbp)
	movq	-3904(%rbp), %rax
	movhps	-4368(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%rax, -3944(%rbp)
	movaps	%xmm6, -128(%rbp)
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L352:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-4072(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-4096(%rbp), %xmm7
	movdqa	-4112(%rbp), %xmm1
	movl	$40, %edi
	movaps	%xmm0, -3952(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movq	%rax, -112(%rbp)
	movq	$0, -3936(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-144(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm4
	movq	-4064(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -3952(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -3936(%rbp)
	movq	%rdx, -3944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L189
	call	_ZdlPv@PLT
.L189:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L353:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movdqa	-4128(%rbp), %xmm3
	movdqa	-4176(%rbp), %xmm6
	movdqa	-4224(%rbp), %xmm1
	movaps	%xmm0, -3952(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -3936(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm7
	leaq	56(%rax), %rdx
	movq	%rax, -3952(%rbp)
	movq	%rcx, 48(%rax)
	movq	-4072(%rbp), %rdi
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm3, 32(%rax)
	movq	%rdx, -3936(%rbp)
	movq	%rdx, -3944(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3952(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L197
	call	_ZdlPv@PLT
.L197:
	movq	-4136(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L196
.L351:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22418:
	.size	_ZN2v88internal28ProxySetPrototypeOfAssembler31GenerateProxySetPrototypeOfImplEv, .-_ZN2v88internal28ProxySetPrototypeOfAssembler31GenerateProxySetPrototypeOfImplEv
	.section	.rodata._ZN2v88internal8Builtins28Generate_ProxySetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/proxy-set-prototype-of-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins28Generate_ProxySetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"ProxySetPrototypeOf"
	.section	.text._ZN2v88internal8Builtins28Generate_ProxySetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins28Generate_ProxySetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins28Generate_ProxySetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins28Generate_ProxySetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE:
.LFB22414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$146, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$860, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L358
.L355:
	movq	%r13, %rdi
	call	_ZN2v88internal28ProxySetPrototypeOfAssembler31GenerateProxySetPrototypeOfImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L359
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L355
.L359:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22414:
	.size	_ZN2v88internal8Builtins28Generate_ProxySetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins28Generate_ProxySetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_ProxySetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_ProxySetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_ProxySetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE:
.LFB29041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29041:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_ProxySetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_ProxySetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_ProxySetPrototypeOfEPNS0_8compiler18CodeAssemblerStateE
	.weak	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
