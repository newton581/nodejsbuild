	.file	"array-reduce-right-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0:
.LFB30477:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	%rsi, %r13
	pushq	%rbx
	movq	%r13, %rax
	sarq	$3, %rax
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%r13, %r13
	js	.L14
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%rdx, %r14
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.L9
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%rax, %rdi
.L9:
	leaq	(%rdi,%r13), %r15
	movq	%rdi, (%rbx)
	movq	%r15, 16(%rbx)
	cmpq	%r12, %r14
	je	.L10
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
.L10:
	movq	%r15, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE30477:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0:
.LFB30476:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	subq	%rsi, %rdx
	js	.L21
	movq	%rdi, %rbx
	je	.L17
	movq	%rdx, %rdi
	movq	%rdx, %r12
	movq	%rsi, %r13
	call	_Znwm@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	leaq	(%rax,%r12), %r14
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	%r14, 16(%rbx)
	call	memcpy@PLT
	movq	%r14, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	xorl	%r14d, %r14d
	movq	$0, (%rdi)
	movq	$0, 16(%rdi)
	movq	%r14, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE30476:
	.size	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev,"axG",@progbits,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev:
.LFB22421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$56, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L23
	call	_ZdlPv@PLT
.L23:
	movq	16(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	%r12, %r13
	je	.L24
	.p2align 4,,10
	.p2align 3
.L28:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L25
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L28
.L26:
	movq	8(%rbx), %r12
.L24:
	testq	%r12, %r12
	je	.L22
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L28
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L22:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22421:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev
	.set	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.section	.rodata._ZN2v88internal54ArrayReduceRightPreLoopEagerDeoptContinuationAssembler57GenerateArrayReduceRightPreLoopEagerDeoptContinuationImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../../deps/v8/../../deps/v8/src/builtins/array-reduce-right.tq"
	.section	.text._ZN2v88internal54ArrayReduceRightPreLoopEagerDeoptContinuationAssembler57GenerateArrayReduceRightPreLoopEagerDeoptContinuationImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal54ArrayReduceRightPreLoopEagerDeoptContinuationAssembler57GenerateArrayReduceRightPreLoopEagerDeoptContinuationImplEv
	.type	_ZN2v88internal54ArrayReduceRightPreLoopEagerDeoptContinuationAssembler57GenerateArrayReduceRightPreLoopEagerDeoptContinuationImplEv, @function
_ZN2v88internal54ArrayReduceRightPreLoopEagerDeoptContinuationAssembler57GenerateArrayReduceRightPreLoopEagerDeoptContinuationImplEv:
.LFB22418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$5, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-2624(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2752(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$2952, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -2808(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -2944(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -2960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	leaq	-2808(%rbp), %r12
	movq	%rax, -2976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$96, %edi
	movq	$0, -2616(%rbp)
	movq	%rax, -2992(%rbp)
	movq	-2808(%rbp), %rax
	movq	$0, -2608(%rbp)
	movq	%rax, -2624(%rbp)
	movq	$0, -2600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -2616(%rbp)
	leaq	-2568(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2600(%rbp)
	movq	%rdx, -2608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2584(%rbp)
	movq	%rax, -2824(%rbp)
	movq	$0, -2592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2808(%rbp), %rax
	movl	$120, %edi
	movq	$0, -2424(%rbp)
	movq	$0, -2416(%rbp)
	movq	%rax, -2432(%rbp)
	movq	$0, -2408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -2424(%rbp)
	leaq	-2376(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2408(%rbp)
	movq	%rdx, -2416(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2392(%rbp)
	movq	%rax, -2904(%rbp)
	movq	$0, -2400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2808(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2232(%rbp)
	movq	$0, -2224(%rbp)
	movq	%rax, -2240(%rbp)
	movq	$0, -2216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2232(%rbp)
	leaq	-2184(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2216(%rbp)
	movq	%rdx, -2224(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2200(%rbp)
	movq	%rax, -2840(%rbp)
	movq	$0, -2208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2808(%rbp), %rax
	movl	$96, %edi
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movq	%rax, -2048(%rbp)
	movq	$0, -2024(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -2040(%rbp)
	leaq	-1992(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2024(%rbp)
	movq	%rdx, -2032(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2008(%rbp)
	movq	%rax, -2848(%rbp)
	movq	$0, -2016(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2808(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1848(%rbp)
	movq	$0, -1840(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1832(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1848(%rbp)
	leaq	-1800(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1832(%rbp)
	movq	%rdx, -1840(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1816(%rbp)
	movq	%rax, -2832(%rbp)
	movq	$0, -1824(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2808(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1656(%rbp)
	movq	$0, -1648(%rbp)
	movq	%rax, -1664(%rbp)
	movq	$0, -1640(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1656(%rbp)
	leaq	-1608(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1640(%rbp)
	movq	%rdx, -1648(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1624(%rbp)
	movq	%rax, -2912(%rbp)
	movq	$0, -1632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2808(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1464(%rbp)
	movq	$0, -1456(%rbp)
	movq	%rax, -1472(%rbp)
	movq	$0, -1448(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1464(%rbp)
	leaq	-1416(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1448(%rbp)
	movq	%rdx, -1456(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1432(%rbp)
	movq	%rax, -2880(%rbp)
	movq	$0, -1440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2808(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1272(%rbp)
	movq	$0, -1264(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -1256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1272(%rbp)
	leaq	-1224(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1256(%rbp)
	movq	%rdx, -1264(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1240(%rbp)
	movq	%rax, -2856(%rbp)
	movq	$0, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2808(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1080(%rbp)
	movq	$0, -1072(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$0, -1064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1080(%rbp)
	leaq	-1032(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1064(%rbp)
	movq	%rdx, -1072(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1048(%rbp)
	movq	%rax, -2872(%rbp)
	movq	$0, -1056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2808(%rbp), %rax
	movl	$168, %edi
	movq	$0, -888(%rbp)
	movq	$0, -880(%rbp)
	movq	%rax, -896(%rbp)
	movq	$0, -872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -888(%rbp)
	leaq	-840(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -872(%rbp)
	movq	%rdx, -880(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -856(%rbp)
	movq	%rax, -2920(%rbp)
	movq	$0, -864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2808(%rbp), %rax
	movl	$192, %edi
	movq	$0, -696(%rbp)
	movq	$0, -688(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -696(%rbp)
	leaq	-648(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -680(%rbp)
	movq	%rdx, -688(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -664(%rbp)
	movq	%rax, -2888(%rbp)
	movq	$0, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2808(%rbp), %rax
	movl	$144, %edi
	movq	$0, -504(%rbp)
	movq	$0, -496(%rbp)
	movq	%rax, -512(%rbp)
	movq	$0, -488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -504(%rbp)
	leaq	-456(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -488(%rbp)
	movq	%rdx, -496(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -472(%rbp)
	movq	%rax, -2864(%rbp)
	movq	$0, -480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2808(%rbp), %rax
	movl	$168, %edi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -312(%rbp)
	leaq	-264(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	xorl	%edx, %edx
	movq	%rax, -2896(%rbp)
	movups	%xmm0, -280(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-2944(%rbp), %xmm1
	movaps	%xmm0, -2752(%rbp)
	movhps	-2960(%rbp), %xmm1
	movq	$0, -2736(%rbp)
	movaps	%xmm1, -128(%rbp)
	movq	-2976(%rbp), %xmm1
	movhps	-2992(%rbp), %xmm1
	movaps	%xmm1, -112(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm2
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm3
	leaq	32(%rax), %rdx
	movq	%rax, -2752(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L35
	call	_ZdlPv@PLT
.L35:
	movq	-2824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2560(%rbp)
	jne	.L326
	cmpq	$0, -2368(%rbp)
	jne	.L327
.L41:
	cmpq	$0, -2176(%rbp)
	jne	.L328
.L44:
	cmpq	$0, -1984(%rbp)
	jne	.L329
.L47:
	cmpq	$0, -1792(%rbp)
	jne	.L330
.L49:
	cmpq	$0, -1600(%rbp)
	jne	.L331
.L54:
	cmpq	$0, -1408(%rbp)
	jne	.L332
.L57:
	cmpq	$0, -1216(%rbp)
	jne	.L333
.L60:
	cmpq	$0, -1024(%rbp)
	jne	.L334
.L62:
	cmpq	$0, -832(%rbp)
	jne	.L335
.L67:
	cmpq	$0, -640(%rbp)
	jne	.L336
.L70:
	cmpq	$0, -448(%rbp)
	jne	.L337
.L73:
	cmpq	$0, -256(%rbp)
	jne	.L338
.L75:
	movq	-2896(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L77
	call	_ZdlPv@PLT
.L77:
	movq	-304(%rbp), %rbx
	movq	-312(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L78
	.p2align 4,,10
	.p2align 3
.L82:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L79
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L82
.L80:
	movq	-312(%rbp), %r13
.L78:
	testq	%r13, %r13
	je	.L83
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L83:
	movq	-2864(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L84
	call	_ZdlPv@PLT
.L84:
	movq	-496(%rbp), %rbx
	movq	-504(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L85
	.p2align 4,,10
	.p2align 3
.L89:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L86
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L89
.L87:
	movq	-504(%rbp), %r13
.L85:
	testq	%r13, %r13
	je	.L90
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L90:
	movq	-2888(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L91
	call	_ZdlPv@PLT
.L91:
	movq	-688(%rbp), %rbx
	movq	-696(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L92
	.p2align 4,,10
	.p2align 3
.L96:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L93
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L96
.L94:
	movq	-696(%rbp), %r13
.L92:
	testq	%r13, %r13
	je	.L97
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L97:
	movq	-2920(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-864(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L98
	call	_ZdlPv@PLT
.L98:
	movq	-880(%rbp), %rbx
	movq	-888(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L99
	.p2align 4,,10
	.p2align 3
.L103:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L100
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L103
.L101:
	movq	-888(%rbp), %r13
.L99:
	testq	%r13, %r13
	je	.L104
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L104:
	movq	-2872(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L105
	call	_ZdlPv@PLT
.L105:
	movq	-1072(%rbp), %rbx
	movq	-1080(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L106
	.p2align 4,,10
	.p2align 3
.L110:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L107
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L110
.L108:
	movq	-1080(%rbp), %r13
.L106:
	testq	%r13, %r13
	je	.L111
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L111:
	movq	-2856(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L112
	call	_ZdlPv@PLT
.L112:
	movq	-1264(%rbp), %rbx
	movq	-1272(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L113
	.p2align 4,,10
	.p2align 3
.L117:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L114
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L117
.L115:
	movq	-1272(%rbp), %r13
.L113:
	testq	%r13, %r13
	je	.L118
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L118:
	movq	-2880(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1440(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L119
	call	_ZdlPv@PLT
.L119:
	movq	-1456(%rbp), %rbx
	movq	-1464(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L120
	.p2align 4,,10
	.p2align 3
.L124:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L121
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L124
.L122:
	movq	-1464(%rbp), %r13
.L120:
	testq	%r13, %r13
	je	.L125
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L125:
	movq	-2912(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1632(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L126
	call	_ZdlPv@PLT
.L126:
	movq	-1648(%rbp), %rbx
	movq	-1656(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L127
	.p2align 4,,10
	.p2align 3
.L131:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L128
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L131
.L129:
	movq	-1656(%rbp), %r13
.L127:
	testq	%r13, %r13
	je	.L132
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L132:
	movq	-2832(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L133
	call	_ZdlPv@PLT
.L133:
	movq	-1840(%rbp), %rbx
	movq	-1848(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L134
	.p2align 4,,10
	.p2align 3
.L138:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L135
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L138
.L136:
	movq	-1848(%rbp), %r13
.L134:
	testq	%r13, %r13
	je	.L139
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L139:
	movq	-2848(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2016(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L140
	call	_ZdlPv@PLT
.L140:
	movq	-2032(%rbp), %rbx
	movq	-2040(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L141
	.p2align 4,,10
	.p2align 3
.L145:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L142
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L145
.L143:
	movq	-2040(%rbp), %r13
.L141:
	testq	%r13, %r13
	je	.L146
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L146:
	movq	-2840(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L147
	call	_ZdlPv@PLT
.L147:
	movq	-2224(%rbp), %rbx
	movq	-2232(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L148
	.p2align 4,,10
	.p2align 3
.L152:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L149
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L152
.L150:
	movq	-2232(%rbp), %r13
.L148:
	testq	%r13, %r13
	je	.L153
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L153:
	movq	-2904(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L154
	call	_ZdlPv@PLT
.L154:
	movq	-2416(%rbp), %rbx
	movq	-2424(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L155
	.p2align 4,,10
	.p2align 3
.L159:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L156
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L159
.L157:
	movq	-2424(%rbp), %r13
.L155:
	testq	%r13, %r13
	je	.L160
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L160:
	movq	-2824(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L161
	call	_ZdlPv@PLT
.L161:
	movq	-2608(%rbp), %rbx
	movq	-2616(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L162
	.p2align 4,,10
	.p2align 3
.L166:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L163
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L166
.L164:
	movq	-2616(%rbp), %r13
.L162:
	testq	%r13, %r13
	je	.L167
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L167:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L339
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L166
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L156:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L159
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L149:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L152
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L142:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L145
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L135:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L138
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L128:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L131
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L121:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L124
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L114:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L117
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L107:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L110
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L100:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L103
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L93:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L96
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L79:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L82
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L86:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L89
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L326:
	movq	-2824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L37
	call	_ZdlPv@PLT
.L37:
	movq	(%rbx), %rax
	movl	$17, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r14
	movq	8(%rax), %rbx
	movq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, -2944(%rbp)
	movq	%rax, -2960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal21Cast10JSReceiver_1404EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm6
	movq	%r14, %xmm5
	movq	-2944(%rbp), %xmm4
	punpcklqdq	%xmm6, %xmm5
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movq	%rbx, -96(%rbp)
	movhps	-2960(%rbp), %xmm4
	movaps	%xmm5, -128(%rbp)
	leaq	-2784(%rbp), %r14
	movaps	%xmm4, -2944(%rbp)
	movaps	%xmm5, -2960(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm0, -2784(%rbp)
	movq	%rax, -88(%rbp)
	movq	$0, -2768(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm6
	movq	%r14, %rsi
	leaq	48(%rax), %rdx
	leaq	-2240(%rbp), %rdi
	movq	%rax, -2784(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-96(%rbp), %xmm7
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -2768(%rbp)
	movq	%rdx, -2776(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L38
	call	_ZdlPv@PLT
.L38:
	movq	-2840(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2744(%rbp)
	jne	.L340
.L39:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2368(%rbp)
	je	.L41
.L327:
	movq	-2904(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2432(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L42
	call	_ZdlPv@PLT
.L42:
	movq	(%rbx), %rax
	movl	$32, %edi
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm6
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-2048(%rbp), %rdi
	movq	%rax, -2752(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L43
	call	_ZdlPv@PLT
.L43:
	movq	-2848(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2176(%rbp)
	je	.L44
.L328:
	movq	-2840(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2240(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movl	$1800, %r11d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	6(%rax), %rdx
	movw	%r11w, 4(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L45
	call	_ZdlPv@PLT
.L45:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	40(%rax), %rdx
	movdqu	(%rax), %xmm7
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm2
	leaq	40(%rax), %rdx
	leaq	-1856(%rbp), %rdi
	movq	%rax, -2752(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L46
	call	_ZdlPv@PLT
.L46:
	movq	-2832(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1984(%rbp)
	je	.L47
.L329:
	movq	-2848(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2048(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L48
	call	_ZdlPv@PLT
.L48:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -1792(%rbp)
	je	.L49
.L330:
	movq	-2832(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1856(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L50
	call	_ZdlPv@PLT
.L50:
	movq	(%rbx), %rax
	movl	$18, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	16(%rax), %rbx
	movq	%rcx, -2944(%rbp)
	movq	24(%rax), %rcx
	movq	32(%rax), %rax
	movq	%rcx, -2960(%rbp)
	movq	%rax, -2976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal82Cast71UT19ATCallableApiObject17ATCallableJSProxy15JSBoundFunction10JSFunction_1405EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm7
	movq	%r14, %xmm2
	movq	-2976(%rbp), %xmm6
	movhps	-2944(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movq	%rax, -80(%rbp)
	punpcklqdq	%xmm7, %xmm6
	movhps	-2960(%rbp), %xmm7
	movaps	%xmm2, -2944(%rbp)
	leaq	-2784(%rbp), %r14
	movaps	%xmm6, -2976(%rbp)
	movaps	%xmm7, -2960(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm0, -2784(%rbp)
	movq	$0, -2768(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm6
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm2
	leaq	56(%rax), %rdx
	leaq	-1472(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm2, 32(%rax)
	movq	%rax, -2784(%rbp)
	movq	%rdx, -2768(%rbp)
	movq	%rdx, -2776(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L51
	call	_ZdlPv@PLT
.L51:
	movq	-2880(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2744(%rbp)
	jne	.L341
.L52:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1600(%rbp)
	je	.L54
.L331:
	movq	-2912(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1664(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r10d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	6(%rax), %rdx
	movw	%r10w, 4(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L55
	call	_ZdlPv@PLT
.L55:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	32(%rax), %rdx
	movdqu	(%rax), %xmm4
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm6
	leaq	40(%rax), %rdx
	leaq	-1280(%rbp), %rdi
	movq	%rax, -2752(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L56
	call	_ZdlPv@PLT
.L56:
	movq	-2856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1408(%rbp)
	je	.L57
.L332:
	movq	-2880(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1472(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r9d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	7(%rax), %rdx
	movw	%r9w, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L58
	call	_ZdlPv@PLT
.L58:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	16(%rax), %rdi
	movq	8(%rax), %r8
	movq	24(%rax), %rsi
	movq	32(%rax), %rcx
	movq	48(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdi, -112(%rbp)
	movl	$48, %edi
	movq	%r8, -120(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movaps	%xmm0, -2752(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -2736(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm2
	leaq	48(%rax), %rdx
	leaq	-1088(%rbp), %rdi
	movq	%rax, -2752(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm2, 32(%rax)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L59
	call	_ZdlPv@PLT
.L59:
	movq	-2872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1216(%rbp)
	je	.L60
.L333:
	movq	-2856(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1280(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L61
	call	_ZdlPv@PLT
.L61:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -1024(%rbp)
	je	.L62
.L334:
	movq	-2872(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1088(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r8d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	6(%rax), %rdx
	movw	%r8w, 4(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L63
	call	_ZdlPv@PLT
.L63:
	movq	(%rbx), %rax
	movl	$19, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	24(%rax), %rbx
	movq	(%rax), %rcx
	movq	16(%rax), %r14
	movq	%rsi, -2960(%rbp)
	movq	32(%rax), %rsi
	movq	40(%rax), %rax
	movq	%rcx, -2944(%rbp)
	movq	%rsi, -2976(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rax, -2992(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal29Cast20UT5ATSmi10HeapNumber_85EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm4
	movq	%rbx, %xmm5
	movq	-2976(%rbp), %xmm3
	punpcklqdq	%xmm5, %xmm4
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	-2944(%rbp), %xmm5
	movhps	-2992(%rbp), %xmm3
	movaps	%xmm4, -112(%rbp)
	leaq	-2784(%rbp), %r14
	movhps	-2960(%rbp), %xmm5
	movaps	%xmm3, -2976(%rbp)
	movaps	%xmm4, -2992(%rbp)
	movaps	%xmm5, -2944(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -2784(%rbp)
	movq	%rbx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -2768(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm6
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm7
	movdqa	-80(%rbp), %xmm3
	leaq	64(%rax), %rdx
	leaq	-704(%rbp), %rdi
	movups	%xmm4, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rax, -2784(%rbp)
	movq	%rdx, -2768(%rbp)
	movq	%rdx, -2776(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L64
	call	_ZdlPv@PLT
.L64:
	movq	-2888(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2744(%rbp)
	jne	.L342
.L65:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -832(%rbp)
	je	.L67
.L335:
	movq	-2920(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-896(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movl	$1799, %edi
	movq	%r13, %rsi
	movw	%di, 4(%rax)
	leaq	7(%rax), %rdx
	movq	%r14, %rdi
	movl	$134744071, (%rax)
	movb	$8, 6(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L68
	call	_ZdlPv@PLT
.L68:
	movq	(%rbx), %rax
	movl	$48, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movdqu	(%rax), %xmm2
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	leaq	48(%rax), %rdx
	leaq	-512(%rbp), %rdi
	movq	%rax, -2752(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L69
	call	_ZdlPv@PLT
.L69:
	movq	-2864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -640(%rbp)
	je	.L70
.L336:
	movq	-2888(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-704(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$578720278898018311, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L71
	call	_ZdlPv@PLT
.L71:
	movq	(%rbx), %rax
	movl	$56, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movq	56(%rax), %rdx
	movdqu	(%rax), %xmm7
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -80(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm4
	leaq	56(%rax), %rdx
	leaq	-320(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm4, 32(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L72
	call	_ZdlPv@PLT
.L72:
	movq	-2896(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -448(%rbp)
	je	.L73
.L337:
	movq	-2864(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-512(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movq	%r14, %rdi
	movw	%si, 4(%rax)
	leaq	6(%rax), %rdx
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L74
	call	_ZdlPv@PLT
.L74:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -256(%rbp)
	je	.L75
.L338:
	movq	-2896(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-320(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$8, 6(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L76
	call	_ZdlPv@PLT
.L76:
	movq	(%rbx), %rax
	movl	$25, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	leaq	-2800(%rbp), %r14
	movq	(%rax), %r9
	movq	40(%rax), %rcx
	movq	32(%rax), %rbx
	movq	48(%rax), %rax
	movq	%r9, -2928(%rbp)
	movq	%rcx, -2944(%rbp)
	movq	%rax, -2960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal10TheHole_62EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$24, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -2992(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, -2976(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$769, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-2752(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	leaq	-128(%rbp), %rcx
	xorl	%esi, %esi
	movhps	-2944(%rbp), %xmm0
	movq	%rbx, %xmm5
	movl	$6, %ebx
	movq	%rax, %r8
	movaps	%xmm0, -128(%rbp)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%r14, %rdi
	movq	-2992(%rbp), %xmm0
	pushq	%rbx
	movq	-2928(%rbp), %r9
	leaq	-2784(%rbp), %rdx
	punpcklqdq	%xmm5, %xmm0
	pushq	%rcx
	movl	$1, %ecx
	movaps	%xmm0, -112(%rbp)
	movq	-2976(%rbp), %xmm0
	movq	%rax, -2784(%rbp)
	movq	-2736(%rbp), %rax
	movhps	-2960(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rax, -2776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L340:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movdqa	-2960(%rbp), %xmm6
	movdqa	-2944(%rbp), %xmm7
	movq	%rbx, -96(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm0, -2784(%rbp)
	movq	$0, -2768(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-128(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm2
	leaq	40(%rax), %rdx
	leaq	-2432(%rbp), %rdi
	movq	%rax, -2784(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -2768(%rbp)
	movq	%rdx, -2776(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L40
	call	_ZdlPv@PLT
.L40:
	movq	-2904(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L341:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movdqa	-2944(%rbp), %xmm6
	movdqa	-2960(%rbp), %xmm7
	movdqa	-2976(%rbp), %xmm3
	movaps	%xmm0, -2784(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movq	$0, -2768(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm2
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	48(%rax), %rdx
	leaq	-1664(%rbp), %rdi
	movq	%rax, -2784(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -2768(%rbp)
	movq	%rdx, -2776(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L53
	call	_ZdlPv@PLT
.L53:
	movq	-2912(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L342:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movdqa	-2944(%rbp), %xmm3
	movdqa	-2992(%rbp), %xmm2
	movdqa	-2976(%rbp), %xmm5
	movaps	%xmm0, -2784(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -2768(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	56(%rax), %rdx
	leaq	-896(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rax, -2784(%rbp)
	movq	%rdx, -2768(%rbp)
	movq	%rdx, -2776(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L66
	call	_ZdlPv@PLT
.L66:
	movq	-2920(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L65
.L339:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22418:
	.size	_ZN2v88internal54ArrayReduceRightPreLoopEagerDeoptContinuationAssembler57GenerateArrayReduceRightPreLoopEagerDeoptContinuationImplEv, .-_ZN2v88internal54ArrayReduceRightPreLoopEagerDeoptContinuationAssembler57GenerateArrayReduceRightPreLoopEagerDeoptContinuationImplEv
	.section	.rodata._ZN2v88internal8Builtins54Generate_ArrayReduceRightPreLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/array-reduce-right-tq-csa.cc"
	.align 8
.LC4:
	.string	"ArrayReduceRightPreLoopEagerDeoptContinuation"
	.section	.text._ZN2v88internal8Builtins54Generate_ArrayReduceRightPreLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins54Generate_ArrayReduceRightPreLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins54Generate_ArrayReduceRightPreLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins54Generate_ArrayReduceRightPreLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE:
.LFB22414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$146, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$766, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L347
.L344:
	movq	%r13, %rdi
	call	_ZN2v88internal54ArrayReduceRightPreLoopEagerDeoptContinuationAssembler57GenerateArrayReduceRightPreLoopEagerDeoptContinuationImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L348
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L344
.L348:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22414:
	.size	_ZN2v88internal8Builtins54Generate_ArrayReduceRightPreLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins54Generate_ArrayReduceRightPreLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_SA_SA_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_SA_SA_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_SA_SA_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_SA_SA_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_SA_SA_:
.LFB27176:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$6, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$2056, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$134744071, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L350
	movq	%rax, -104(%rbp)
	call	_ZdlPv@PLT
	movq	-104(%rbp), %rax
.L350:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L351
	movq	%rdx, (%r15)
.L351:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L352
	movq	%rdx, (%r14)
.L352:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L353
	movq	%rdx, 0(%r13)
.L353:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L354
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L354:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L355
	movq	%rdx, (%rbx)
.L355:
	movq	40(%rax), %rax
	testq	%rax, %rax
	je	.L349
	movq	-96(%rbp), %rbx
	movq	%rax, (%rbx)
.L349:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L380
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L380:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27176:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_SA_SA_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_SA_SA_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_PNS7_IS5_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_PNS7_IS5_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_PNS7_IS5_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_PNS7_IS5_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_PNS7_IS5_EE:
.LFB27182:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$7, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$2056, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$134744071, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L382
	movq	%rax, -112(%rbp)
	call	_ZdlPv@PLT
	movq	-112(%rbp), %rax
.L382:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L383
	movq	%rdx, (%r15)
.L383:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L384
	movq	%rdx, (%r14)
.L384:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L385
	movq	%rdx, 0(%r13)
.L385:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L386
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L386:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L387
	movq	%rdx, (%rbx)
.L387:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L388
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L388:
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L381
	movq	-104(%rbp), %rbx
	movq	%rax, (%rbx)
.L381:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L416
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L416:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27182:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_PNS7_IS5_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_PNS7_IS5_EE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_PNS7_IS5_EESD_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_PNS7_IS5_EESD_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_PNS7_IS5_EESD_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_PNS7_IS5_EESD_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_PNS7_IS5_EESD_:
.LFB27188:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$8, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$506382313689974791, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L418
	movq	%rax, -120(%rbp)
	call	_ZdlPv@PLT
	movq	-120(%rbp), %rax
.L418:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L419
	movq	%rdx, (%r15)
.L419:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L420
	movq	%rdx, (%r14)
.L420:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L421
	movq	%rdx, 0(%r13)
.L421:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L422
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L422:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L423
	movq	%rdx, (%rbx)
.L423:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L424
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L424:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L425
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L425:
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L417
	movq	-112(%rbp), %rbx
	movq	%rax, (%rbx)
.L417:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L456
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L456:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27188:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_PNS7_IS5_EESD_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_PNS7_IS5_EESD_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverES5_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_SF_SF_SF_PNSB_IS5_EESH_PNSB_IS9_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverES5_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_SF_SF_SF_PNSB_IS5_EESH_PNSB_IS9_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverES5_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_SF_SF_SF_PNSB_IS5_EESH_PNSB_IS9_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverES5_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_SF_SF_SF_PNSB_IS5_EESH_PNSB_IS9_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverES5_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_SF_SF_SF_PNSB_IS5_EESH_PNSB_IS9_EE:
.LFB27194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$9, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$506382313689974791, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L458
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
.L458:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L459
	movq	%rdx, (%r15)
.L459:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L460
	movq	%rdx, (%r14)
.L460:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L461
	movq	%rdx, 0(%r13)
.L461:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L462
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L462:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L463
	movq	%rdx, (%rbx)
.L463:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L464
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L464:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L465
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L465:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L466
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L466:
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L457
	movq	-120(%rbp), %rbx
	movq	%rax, (%rbx)
.L457:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L500
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L500:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27194:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverES5_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_SF_SF_SF_PNSB_IS5_EESH_PNSB_IS9_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverES5_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_SF_SF_SF_PNSB_IS5_EESH_PNSB_IS9_EE
	.section	.text._ZN2v88internal51ArrayReduceRightLoopEagerDeoptContinuationAssembler54GenerateArrayReduceRightLoopEagerDeoptContinuationImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal51ArrayReduceRightLoopEagerDeoptContinuationAssembler54GenerateArrayReduceRightLoopEagerDeoptContinuationImplEv
	.type	_ZN2v88internal51ArrayReduceRightLoopEagerDeoptContinuationAssembler54GenerateArrayReduceRightLoopEagerDeoptContinuationImplEv, @function
_ZN2v88internal51ArrayReduceRightLoopEagerDeoptContinuationAssembler54GenerateArrayReduceRightLoopEagerDeoptContinuationImplEv:
.LFB22460:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$7, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-3408(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-3536(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$3816, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -3656(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -3768(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -3808(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, -3816(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, -3824(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	leaq	-3656(%rbp), %r12
	movq	%rax, -3832(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$144, %edi
	movq	$0, -3400(%rbp)
	movq	%rax, -3840(%rbp)
	movq	-3656(%rbp), %rax
	movq	$0, -3392(%rbp)
	movq	%rax, -3408(%rbp)
	movq	$0, -3384(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -3400(%rbp)
	leaq	-3352(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3384(%rbp)
	movq	%rdx, -3392(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3368(%rbp)
	movq	%rax, -3672(%rbp)
	movq	$0, -3376(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$168, %edi
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	%rax, -3216(%rbp)
	movq	$0, -3192(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -3208(%rbp)
	leaq	-3160(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3192(%rbp)
	movq	%rdx, -3200(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3176(%rbp)
	movq	%rax, -3800(%rbp)
	movq	$0, -3184(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$192, %edi
	movq	$0, -3016(%rbp)
	movq	$0, -3008(%rbp)
	movq	%rax, -3024(%rbp)
	movq	$0, -3000(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -3016(%rbp)
	leaq	-2968(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3000(%rbp)
	movq	%rdx, -3008(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2984(%rbp)
	movq	%rax, -3760(%rbp)
	movq	$0, -2992(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2824(%rbp)
	movq	$0, -2816(%rbp)
	movq	%rax, -2832(%rbp)
	movq	$0, -2808(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2824(%rbp)
	leaq	-2776(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2808(%rbp)
	movq	%rdx, -2816(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2792(%rbp)
	movq	%rax, -3704(%rbp)
	movq	$0, -2800(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2632(%rbp)
	movq	$0, -2624(%rbp)
	movq	%rax, -2640(%rbp)
	movq	$0, -2616(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -2632(%rbp)
	leaq	-2584(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2616(%rbp)
	movq	%rdx, -2624(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2600(%rbp)
	movq	%rax, -3720(%rbp)
	movq	$0, -2608(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$192, %edi
	movq	$0, -2440(%rbp)
	movq	$0, -2432(%rbp)
	movq	%rax, -2448(%rbp)
	movq	$0, -2424(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -2440(%rbp)
	leaq	-2392(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2424(%rbp)
	movq	%rdx, -2432(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2408(%rbp)
	movq	%rax, -3792(%rbp)
	movq	$0, -2416(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$216, %edi
	movq	$0, -2248(%rbp)
	movq	$0, -2240(%rbp)
	movq	%rax, -2256(%rbp)
	movq	$0, -2232(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -2248(%rbp)
	leaq	-2200(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2232(%rbp)
	movq	%rdx, -2240(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2216(%rbp)
	movq	%rax, -3848(%rbp)
	movq	$0, -2224(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2056(%rbp)
	movq	$0, -2048(%rbp)
	movq	%rax, -2064(%rbp)
	movq	$0, -2040(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -2056(%rbp)
	leaq	-2008(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2040(%rbp)
	movq	%rdx, -2048(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2024(%rbp)
	movq	%rax, -3688(%rbp)
	movq	$0, -2032(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1864(%rbp)
	movq	$0, -1856(%rbp)
	movq	%rax, -1872(%rbp)
	movq	$0, -1848(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1864(%rbp)
	leaq	-1816(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1848(%rbp)
	movq	%rdx, -1856(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1832(%rbp)
	movq	%rax, -3712(%rbp)
	movq	$0, -1840(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1672(%rbp)
	movq	$0, -1664(%rbp)
	movq	%rax, -1680(%rbp)
	movq	$0, -1656(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1672(%rbp)
	leaq	-1624(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1656(%rbp)
	movq	%rdx, -1664(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1640(%rbp)
	movq	%rax, -3784(%rbp)
	movq	$0, -1648(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$240, %edi
	movq	$0, -1480(%rbp)
	movq	$0, -1472(%rbp)
	movq	%rax, -1488(%rbp)
	movq	$0, -1464(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -1480(%rbp)
	leaq	-1432(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1464(%rbp)
	movq	%rdx, -1472(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1448(%rbp)
	movq	%rax, -3736(%rbp)
	movq	$0, -1456(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1288(%rbp)
	movq	$0, -1280(%rbp)
	movq	%rax, -1296(%rbp)
	movq	$0, -1272(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1288(%rbp)
	leaq	-1240(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1272(%rbp)
	movq	%rdx, -1280(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1256(%rbp)
	movq	%rax, -3752(%rbp)
	movq	$0, -1264(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1096(%rbp)
	movq	$0, -1088(%rbp)
	movq	%rax, -1104(%rbp)
	movq	$0, -1080(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1096(%rbp)
	leaq	-1048(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1080(%rbp)
	movq	%rdx, -1088(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1064(%rbp)
	movq	%rax, -3696(%rbp)
	movq	$0, -1072(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$240, %edi
	movq	$0, -904(%rbp)
	movq	$0, -896(%rbp)
	movq	%rax, -912(%rbp)
	movq	$0, -888(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -904(%rbp)
	leaq	-856(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -888(%rbp)
	movq	%rdx, -896(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -872(%rbp)
	movq	%rax, -3776(%rbp)
	movq	$0, -880(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$264, %edi
	movq	$0, -712(%rbp)
	movq	$0, -704(%rbp)
	movq	%rax, -720(%rbp)
	movq	$0, -696(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -712(%rbp)
	leaq	-664(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -696(%rbp)
	movq	%rdx, -704(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -680(%rbp)
	movq	%rax, -3728(%rbp)
	movq	$0, -688(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$216, %edi
	movq	$0, -520(%rbp)
	movq	$0, -512(%rbp)
	movq	%rax, -528(%rbp)
	movq	$0, -504(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -520(%rbp)
	leaq	-472(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -504(%rbp)
	movq	%rdx, -512(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -488(%rbp)
	movq	%rax, -3744(%rbp)
	movq	$0, -496(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$240, %edi
	movq	$0, -328(%rbp)
	movq	$0, -320(%rbp)
	movq	%rax, -336(%rbp)
	movq	$0, -312(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -328(%rbp)
	leaq	-280(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -312(%rbp)
	movq	%rdx, -320(%rbp)
	xorl	%edx, %edx
	movq	%rax, -3680(%rbp)
	movups	%xmm0, -296(%rbp)
	movq	$0, -304(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movq	-3768(%rbp), %xmm1
	movaps	%xmm0, -3536(%rbp)
	movhps	-3808(%rbp), %xmm1
	movq	$0, -3520(%rbp)
	movaps	%xmm1, -144(%rbp)
	movq	-3816(%rbp), %xmm1
	movhps	-3824(%rbp), %xmm1
	movaps	%xmm1, -128(%rbp)
	movq	-3832(%rbp), %xmm1
	movhps	-3840(%rbp), %xmm1
	movaps	%xmm1, -112(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm6
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm5
	leaq	48(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movups	%xmm6, 16(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L502
	call	_ZdlPv@PLT
.L502:
	movq	-3672(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3344(%rbp)
	jne	.L838
	cmpq	$0, -3152(%rbp)
	jne	.L839
.L507:
	cmpq	$0, -2960(%rbp)
	jne	.L840
.L510:
	cmpq	$0, -2768(%rbp)
	jne	.L841
.L513:
	leaq	-2256(%rbp), %rax
	cmpq	$0, -2576(%rbp)
	movq	%rax, -3768(%rbp)
	jne	.L842
	cmpq	$0, -2384(%rbp)
	jne	.L843
.L518:
	cmpq	$0, -2192(%rbp)
	jne	.L844
.L521:
	cmpq	$0, -2000(%rbp)
	jne	.L845
.L524:
	cmpq	$0, -1808(%rbp)
	jne	.L846
.L525:
	cmpq	$0, -1616(%rbp)
	jne	.L847
.L529:
	cmpq	$0, -1424(%rbp)
	jne	.L848
.L532:
	cmpq	$0, -1232(%rbp)
	jne	.L849
.L535:
	cmpq	$0, -1040(%rbp)
	jne	.L850
.L536:
	cmpq	$0, -848(%rbp)
	jne	.L851
.L540:
	cmpq	$0, -656(%rbp)
	jne	.L852
.L543:
	cmpq	$0, -464(%rbp)
	jne	.L853
.L546:
	cmpq	$0, -272(%rbp)
	jne	.L854
.L547:
	movq	-3680(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L549
	call	_ZdlPv@PLT
.L549:
	movq	-320(%rbp), %rbx
	movq	-328(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L550
	.p2align 4,,10
	.p2align 3
.L554:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L551
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L554
.L552:
	movq	-328(%rbp), %r13
.L550:
	testq	%r13, %r13
	je	.L555
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L555:
	movq	-3744(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-496(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L556
	call	_ZdlPv@PLT
.L556:
	movq	-512(%rbp), %rbx
	movq	-520(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L557
	.p2align 4,,10
	.p2align 3
.L561:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L558
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L561
.L559:
	movq	-520(%rbp), %r13
.L557:
	testq	%r13, %r13
	je	.L562
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L562:
	movq	-3728(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L563
	call	_ZdlPv@PLT
.L563:
	movq	-704(%rbp), %rbx
	movq	-712(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L564
	.p2align 4,,10
	.p2align 3
.L568:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L565
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L568
.L566:
	movq	-712(%rbp), %r13
.L564:
	testq	%r13, %r13
	je	.L569
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L569:
	movq	-3776(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L570
	call	_ZdlPv@PLT
.L570:
	movq	-896(%rbp), %rbx
	movq	-904(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L571
	.p2align 4,,10
	.p2align 3
.L575:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L572
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L575
.L573:
	movq	-904(%rbp), %r13
.L571:
	testq	%r13, %r13
	je	.L576
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L576:
	movq	-3696(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L577
	call	_ZdlPv@PLT
.L577:
	movq	-1088(%rbp), %rbx
	movq	-1096(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L578
	.p2align 4,,10
	.p2align 3
.L582:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L579
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L582
.L580:
	movq	-1096(%rbp), %r13
.L578:
	testq	%r13, %r13
	je	.L583
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L583:
	movq	-3752(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L584
	call	_ZdlPv@PLT
.L584:
	movq	-1280(%rbp), %rbx
	movq	-1288(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L585
	.p2align 4,,10
	.p2align 3
.L589:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L586
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L589
.L587:
	movq	-1288(%rbp), %r13
.L585:
	testq	%r13, %r13
	je	.L590
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L590:
	movq	-3736(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1456(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L591
	call	_ZdlPv@PLT
.L591:
	movq	-1472(%rbp), %rbx
	movq	-1480(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L592
	.p2align 4,,10
	.p2align 3
.L596:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L593
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L596
.L594:
	movq	-1480(%rbp), %r13
.L592:
	testq	%r13, %r13
	je	.L597
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L597:
	movq	-3784(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1648(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L598
	call	_ZdlPv@PLT
.L598:
	movq	-1664(%rbp), %rbx
	movq	-1672(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L599
	.p2align 4,,10
	.p2align 3
.L603:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L600
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L603
.L601:
	movq	-1672(%rbp), %r13
.L599:
	testq	%r13, %r13
	je	.L604
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L604:
	movq	-3712(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1840(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L605
	call	_ZdlPv@PLT
.L605:
	movq	-1856(%rbp), %rbx
	movq	-1864(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L606
	.p2align 4,,10
	.p2align 3
.L610:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L607
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L610
.L608:
	movq	-1864(%rbp), %r13
.L606:
	testq	%r13, %r13
	je	.L611
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L611:
	movq	-3688(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2032(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L612
	call	_ZdlPv@PLT
.L612:
	movq	-2048(%rbp), %rbx
	movq	-2056(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L613
	.p2align 4,,10
	.p2align 3
.L617:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L614
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L617
.L615:
	movq	-2056(%rbp), %r13
.L613:
	testq	%r13, %r13
	je	.L618
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L618:
	movq	-3768(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3792(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2416(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L619
	call	_ZdlPv@PLT
.L619:
	movq	-2432(%rbp), %rbx
	movq	-2440(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L620
	.p2align 4,,10
	.p2align 3
.L624:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L621
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L624
.L622:
	movq	-2440(%rbp), %r13
.L620:
	testq	%r13, %r13
	je	.L625
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L625:
	movq	-3720(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2608(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L626
	call	_ZdlPv@PLT
.L626:
	movq	-2624(%rbp), %rbx
	movq	-2632(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L627
	.p2align 4,,10
	.p2align 3
.L631:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L628
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L631
.L629:
	movq	-2632(%rbp), %r13
.L627:
	testq	%r13, %r13
	je	.L632
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L632:
	movq	-3704(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L633
	call	_ZdlPv@PLT
.L633:
	movq	-2816(%rbp), %rbx
	movq	-2824(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L634
	.p2align 4,,10
	.p2align 3
.L638:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L635
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L638
.L636:
	movq	-2824(%rbp), %r13
.L634:
	testq	%r13, %r13
	je	.L639
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L639:
	movq	-3760(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L640
	call	_ZdlPv@PLT
.L640:
	movq	-3008(%rbp), %rbx
	movq	-3016(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L641
	.p2align 4,,10
	.p2align 3
.L645:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L642
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L645
.L643:
	movq	-3016(%rbp), %r13
.L641:
	testq	%r13, %r13
	je	.L646
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L646:
	movq	-3800(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L647
	call	_ZdlPv@PLT
.L647:
	movq	-3200(%rbp), %rbx
	movq	-3208(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L648
	.p2align 4,,10
	.p2align 3
.L652:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L649
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L652
.L650:
	movq	-3208(%rbp), %r13
.L648:
	testq	%r13, %r13
	je	.L653
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L653:
	movq	-3672(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L654
	call	_ZdlPv@PLT
.L654:
	movq	-3392(%rbp), %rbx
	movq	-3400(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L655
	.p2align 4,,10
	.p2align 3
.L659:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L656
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L659
.L657:
	movq	-3400(%rbp), %r13
.L655:
	testq	%r13, %r13
	je	.L660
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L660:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L855
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L656:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L659
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L649:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L652
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L642:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L645
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L635:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L638
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L628:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L631
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L621:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L624
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L614:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L617
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L607:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L610
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L600:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L603
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L593:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L596
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L586:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L589
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L579:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L582
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L572:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L575
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L565:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L568
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L551:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L554
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L558:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L561
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L838:
	movq	-3672(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3624(%rbp)
	leaq	-144(%rbp), %rbx
	movq	$0, -3616(%rbp)
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	movq	$0, -3592(%rbp)
	movq	$0, -3584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r14, %rdi
	leaq	-3584(%rbp), %rax
	pushq	%rax
	leaq	-3592(%rbp), %r9
	leaq	-3608(%rbp), %rcx
	leaq	-3600(%rbp), %r8
	leaq	-3616(%rbp), %rdx
	leaq	-3624(%rbp), %rsi
	leaq	-3568(%rbp), %r14
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_SA_SA_
	movl	$40, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3616(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r15, %rdi
	movq	-3624(%rbp), %rsi
	call	_ZN2v88internal21Cast10JSReceiver_1404EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-80(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-3592(%rbp), %xmm0
	movq	%rax, %xmm5
	movq	-3616(%rbp), %xmm3
	movq	$0, -3552(%rbp)
	movq	-3608(%rbp), %xmm1
	movq	-3624(%rbp), %xmm2
	movhps	-3584(%rbp), %xmm0
	punpcklqdq	%xmm5, %xmm3
	movhps	-3600(%rbp), %xmm1
	movhps	-3616(%rbp), %xmm2
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -3568(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3024(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	popq	%rsi
	popq	%r8
	testq	%rdi, %rdi
	je	.L504
	call	_ZdlPv@PLT
.L504:
	movq	-3760(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3528(%rbp)
	jne	.L856
.L505:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -3152(%rbp)
	je	.L507
.L839:
	movq	-3800(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-3216(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movl	$2056, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$8, 6(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L508
	call	_ZdlPv@PLT
.L508:
	movq	(%r14), %rax
	leaq	-144(%rbp), %rsi
	leaq	-96(%rbp), %rdx
	movq	%r13, %rdi
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm1
	movdqu	32(%rax), %xmm6
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2832(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L509
	call	_ZdlPv@PLT
.L509:
	movq	-3704(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2960(%rbp)
	je	.L510
.L840:
	movq	-3760(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-3024(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$506663788666685447, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L511
	call	_ZdlPv@PLT
.L511:
	movq	(%r14), %rax
	leaq	-144(%rbp), %rsi
	leaq	-88(%rbp), %rdx
	movq	%r13, %rdi
	movdqu	32(%rax), %xmm0
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm1
	movq	56(%rax), %rax
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2640(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L512
	call	_ZdlPv@PLT
.L512:
	movq	-3720(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2768(%rbp)
	je	.L513
.L841:
	movq	-3704(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3616(%rbp)
	leaq	-2832(%rbp), %r14
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	movq	$0, -3592(%rbp)
	movq	$0, -3584(%rbp)
	movq	$0, -3568(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r14, %rdi
	leaq	-3568(%rbp), %rax
	pushq	%rax
	leaq	-3608(%rbp), %rdx
	leaq	-3600(%rbp), %rcx
	leaq	-3584(%rbp), %r9
	leaq	-3592(%rbp), %r8
	leaq	-3616(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_SA_SA_
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L842:
	movq	-3720(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3632(%rbp)
	leaq	-2640(%rbp), %r14
	movq	$0, -3624(%rbp)
	leaq	-144(%rbp), %rbx
	movq	$0, -3616(%rbp)
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	movq	$0, -3592(%rbp)
	movq	$0, -3584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3584(%rbp), %rax
	movq	%r14, %rdi
	leaq	-3600(%rbp), %r9
	pushq	%rax
	leaq	-3592(%rbp), %rax
	leaq	-3616(%rbp), %rcx
	pushq	%rax
	leaq	-3608(%rbp), %r8
	leaq	-3624(%rbp), %rdx
	leaq	-3632(%rbp), %rsi
	leaq	-3568(%rbp), %r14
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_PNS7_IS5_EE
	movl	$41, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3616(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r15, %rdi
	movq	-3632(%rbp), %rsi
	call	_ZN2v88internal82Cast71UT19ATCallableApiObject17ATCallableJSProxy15JSBoundFunction10JSFunction_1405EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-3632(%rbp), %rdx
	movq	%rax, %r8
	movq	-3616(%rbp), %rax
	movaps	%xmm0, -3568(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rdx, -144(%rbp)
	movq	-3624(%rbp), %rdx
	movq	%rax, -128(%rbp)
	movq	%rdx, -136(%rbp)
	movq	-3608(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-3600(%rbp), %rdx
	movq	$0, -3552(%rbp)
	movq	%rdx, -112(%rbp)
	movq	-3592(%rbp), %rdx
	movq	%rdx, -104(%rbp)
	movq	-3584(%rbp), %rdx
	movq	%rdx, -96(%rbp)
	leaq	-72(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3768(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	popq	%r11
	popq	%rax
	testq	%rdi, %rdi
	je	.L515
	call	_ZdlPv@PLT
.L515:
	movq	-3848(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3528(%rbp)
	jne	.L857
.L516:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2384(%rbp)
	je	.L518
.L843:
	movq	-3792(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2448(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$578439907727902727, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L519
	call	_ZdlPv@PLT
.L519:
	movq	(%r14), %rax
	leaq	-144(%rbp), %rsi
	leaq	-88(%rbp), %rdx
	movq	%r13, %rdi
	movdqu	32(%rax), %xmm0
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm1
	movq	48(%rax), %rax
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2064(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L520
	call	_ZdlPv@PLT
.L520:
	movq	-3688(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2192(%rbp)
	je	.L521
.L844:
	movq	-3848(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	-3768(%rbp), %rdi
	movq	%r13, %rsi
	movabsq	$578439907727902727, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L522
	call	_ZdlPv@PLT
.L522:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	24(%rax), %rdi
	movq	32(%rax), %rsi
	movq	48(%rax), %rdx
	movq	(%rax), %r10
	movq	8(%rax), %r9
	movq	16(%rax), %r8
	movq	40(%rax), %rcx
	movq	64(%rax), %rax
	movq	%rdi, -120(%rbp)
	movq	%r13, %rdi
	movq	%rsi, -112(%rbp)
	leaq	-144(%rbp), %rsi
	movq	%rdx, -96(%rbp)
	leaq	-80(%rbp), %rdx
	movq	%r10, -144(%rbp)
	movq	%r9, -136(%rbp)
	movq	%r8, -128(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%rax, -88(%rbp)
	movaps	%xmm0, -3536(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1872(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L523
	call	_ZdlPv@PLT
.L523:
	movq	-3712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2000(%rbp)
	je	.L524
.L845:
	movq	-3688(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3624(%rbp)
	leaq	-2064(%rbp), %r14
	movq	$0, -3616(%rbp)
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	movq	$0, -3592(%rbp)
	movq	$0, -3584(%rbp)
	movq	$0, -3568(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3568(%rbp), %rax
	movq	%r14, %rdi
	leaq	-3592(%rbp), %r9
	pushq	%rax
	leaq	-3584(%rbp), %rax
	leaq	-3608(%rbp), %rcx
	pushq	%rax
	leaq	-3616(%rbp), %rdx
	leaq	-3600(%rbp), %r8
	leaq	-3624(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_PNS7_IS5_EE
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -1808(%rbp)
	popq	%r9
	popq	%r10
	je	.L525
.L846:
	movq	-3712(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3640(%rbp)
	leaq	-1872(%rbp), %r14
	movq	$0, -3632(%rbp)
	leaq	-144(%rbp), %rbx
	movq	$0, -3624(%rbp)
	movq	$0, -3616(%rbp)
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	movq	$0, -3592(%rbp)
	movq	$0, -3584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r14, %rdi
	leaq	-3584(%rbp), %rax
	pushq	%rax
	leaq	-3592(%rbp), %rax
	leaq	-3608(%rbp), %r9
	pushq	%rax
	leaq	-3600(%rbp), %rax
	leaq	-3624(%rbp), %rcx
	pushq	%rax
	leaq	-3616(%rbp), %r8
	leaq	-3632(%rbp), %rdx
	leaq	-3640(%rbp), %rsi
	leaq	-3568(%rbp), %r14
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_PNS7_IS5_EESD_
	addq	$32, %rsp
	movl	$42, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3616(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal29Cast20UT5ATSmi10HeapNumber_85EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-64(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-3592(%rbp), %xmm0
	movq	%rax, %xmm7
	movq	-3616(%rbp), %xmm4
	movq	$0, -3552(%rbp)
	movq	-3608(%rbp), %xmm1
	movq	-3624(%rbp), %xmm2
	movq	-3640(%rbp), %xmm3
	movhps	-3584(%rbp), %xmm0
	punpcklqdq	%xmm7, %xmm4
	movhps	-3600(%rbp), %xmm1
	movhps	-3616(%rbp), %xmm2
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3632(%rbp), %xmm3
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm0, -3568(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1488(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L526
	call	_ZdlPv@PLT
.L526:
	movq	-3736(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3528(%rbp)
	jne	.L858
.L527:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1616(%rbp)
	je	.L529
.L847:
	movq	-3784(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1680(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$506382313689974791, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L530
	call	_ZdlPv@PLT
.L530:
	movq	(%rbx), %rax
	leaq	-144(%rbp), %rsi
	leaq	-80(%rbp), %rdx
	movq	%r13, %rdi
	movdqu	32(%rax), %xmm0
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm1
	movdqu	48(%rax), %xmm4
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1296(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L531
	call	_ZdlPv@PLT
.L531:
	movq	-3752(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1424(%rbp)
	je	.L532
.L848:
	movq	-3736(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1488(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r8d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$506382313689974791, %rcx
	movw	%r8w, 8(%rax)
	leaq	10(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L533
	call	_ZdlPv@PLT
.L533:
	movq	(%rbx), %rax
	leaq	-144(%rbp), %rsi
	leaq	-72(%rbp), %rdx
	movq	%r13, %rdi
	movdqu	48(%rax), %xmm0
	movdqu	(%rax), %xmm3
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	72(%rax), %rax
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -144(%rbp)
	movq	%rax, -80(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3536(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1104(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L534
	call	_ZdlPv@PLT
.L534:
	movq	-3696(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1232(%rbp)
	je	.L535
.L849:
	movq	-3752(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3632(%rbp)
	leaq	-1296(%rbp), %r14
	movq	$0, -3624(%rbp)
	movq	$0, -3616(%rbp)
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	movq	$0, -3592(%rbp)
	movq	$0, -3584(%rbp)
	movq	$0, -3568(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r14, %rdi
	leaq	-3568(%rbp), %rax
	pushq	%rax
	leaq	-3584(%rbp), %rax
	leaq	-3616(%rbp), %rcx
	pushq	%rax
	leaq	-3592(%rbp), %rax
	leaq	-3624(%rbp), %rdx
	pushq	%rax
	leaq	-3600(%rbp), %r9
	leaq	-3608(%rbp), %r8
	leaq	-3632(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_PNS7_IS5_EESD_
	addq	$32, %rsp
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -1040(%rbp)
	je	.L536
.L850:
	movq	-3696(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3648(%rbp)
	leaq	-1104(%rbp), %r14
	movq	$0, -3640(%rbp)
	leaq	-144(%rbp), %rbx
	movq	$0, -3632(%rbp)
	movq	$0, -3624(%rbp)
	movq	$0, -3616(%rbp)
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	movq	$0, -3592(%rbp)
	movq	$0, -3584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3584(%rbp), %rax
	movq	%r14, %rdi
	leaq	-3616(%rbp), %r9
	pushq	%rax
	leaq	-3592(%rbp), %rax
	leaq	-3632(%rbp), %rcx
	pushq	%rax
	leaq	-3600(%rbp), %rax
	leaq	-3624(%rbp), %r8
	pushq	%rax
	leaq	-3608(%rbp), %rax
	leaq	-3640(%rbp), %rdx
	pushq	%rax
	leaq	-3648(%rbp), %rsi
	leaq	-3568(%rbp), %r14
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverES5_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_SF_SF_SF_PNSB_IS5_EESH_PNSB_IS9_EE
	addq	$32, %rsp
	movl	$43, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3616(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal29Cast20UT5ATSmi10HeapNumber_85EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-3648(%rbp), %rdx
	movq	%rax, %r8
	movq	-3616(%rbp), %rax
	movaps	%xmm0, -3568(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rdx, -144(%rbp)
	movq	-3640(%rbp), %rdx
	movq	%rax, -112(%rbp)
	movq	%rdx, -136(%rbp)
	movq	-3632(%rbp), %rdx
	movq	%rax, -72(%rbp)
	movq	%rdx, -128(%rbp)
	movq	-3624(%rbp), %rdx
	movq	$0, -3552(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-3608(%rbp), %rdx
	movq	%rdx, -104(%rbp)
	movq	-3600(%rbp), %rdx
	movq	%rdx, -96(%rbp)
	movq	-3592(%rbp), %rdx
	movq	%rdx, -88(%rbp)
	movq	-3584(%rbp), %rdx
	movq	%rdx, -80(%rbp)
	leaq	-56(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-720(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L537
	call	_ZdlPv@PLT
.L537:
	movq	-3728(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3528(%rbp)
	jne	.L859
.L538:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -848(%rbp)
	je	.L540
.L851:
	movq	-3776(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-912(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movl	$2056, %edi
	movq	%r13, %rsi
	movabsq	$506382313689974791, %rcx
	movw	%di, 8(%rax)
	leaq	10(%rax), %rdx
	movq	%r14, %rdi
	movq	%rcx, (%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L541
	call	_ZdlPv@PLT
.L541:
	movq	(%rbx), %rax
	leaq	-144(%rbp), %rsi
	leaq	-72(%rbp), %rdx
	movq	%r13, %rdi
	movdqu	48(%rax), %xmm0
	movdqu	(%rax), %xmm3
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	64(%rax), %rax
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -144(%rbp)
	movq	%rax, -80(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3536(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-528(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L542
	call	_ZdlPv@PLT
.L542:
	movq	-3744(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -656(%rbp)
	je	.L543
.L852:
	movq	-3728(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-720(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$11, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movl	$2056, %esi
	movq	%r14, %rdi
	movabsq	$506382313689974791, %rcx
	movw	%si, 8(%rax)
	leaq	11(%rax), %rdx
	movq	%r13, %rsi
	movq	%rcx, (%rax)
	movb	$8, 10(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L544
	call	_ZdlPv@PLT
.L544:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	40(%rax), %rdi
	movq	48(%rax), %rsi
	movq	64(%rax), %rdx
	movq	8(%rax), %r11
	movq	16(%rax), %r10
	movq	24(%rax), %r9
	movq	32(%rax), %r8
	movq	56(%rax), %rcx
	movq	(%rax), %rbx
	movq	80(%rax), %rax
	movq	%rdi, -104(%rbp)
	movq	%r13, %rdi
	movq	%rsi, -96(%rbp)
	leaq	-144(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	leaq	-64(%rbp), %rdx
	movq	%r11, -136(%rbp)
	movq	%r10, -128(%rbp)
	movq	%r9, -120(%rbp)
	movq	%r8, -112(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%rax, -72(%rbp)
	movaps	%xmm0, -3536(%rbp)
	movq	%rbx, -144(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-336(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L545
	call	_ZdlPv@PLT
.L545:
	movq	-3680(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -464(%rbp)
	je	.L546
.L853:
	movq	-3744(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3640(%rbp)
	leaq	-528(%rbp), %r14
	movq	$0, -3632(%rbp)
	movq	$0, -3624(%rbp)
	movq	$0, -3616(%rbp)
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	movq	$0, -3592(%rbp)
	movq	$0, -3584(%rbp)
	movq	$0, -3568(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3568(%rbp), %rax
	movq	%r14, %rdi
	leaq	-3624(%rbp), %rcx
	pushq	%rax
	leaq	-3584(%rbp), %rax
	leaq	-3632(%rbp), %rdx
	pushq	%rax
	leaq	-3592(%rbp), %rax
	leaq	-3608(%rbp), %r9
	pushq	%rax
	leaq	-3600(%rbp), %rax
	leaq	-3616(%rbp), %r8
	pushq	%rax
	leaq	-3640(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverES5_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_SF_SF_SF_PNSB_IS5_EESH_PNSB_IS9_EE
	addq	$32, %rsp
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -272(%rbp)
	je	.L547
.L854:
	movq	-3680(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-336(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$506382313689974791, %rcx
	movq	%rcx, (%rax)
	movl	$2056, %ecx
	leaq	10(%rax), %rdx
	movw	%cx, 8(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L548
	call	_ZdlPv@PLT
.L548:
	movq	(%rbx), %rax
	movl	$46, %edx
	movq	%r12, %rdi
	leaq	-3584(%rbp), %r14
	movq	40(%rax), %rcx
	movq	56(%rax), %rsi
	movq	(%rax), %r9
	movq	64(%rax), %rbx
	movq	%rcx, -3816(%rbp)
	movq	48(%rax), %rcx
	movq	72(%rax), %rax
	movq	%rsi, -3824(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%r9, -3840(%rbp)
	movq	%rcx, -3808(%rbp)
	movq	%rax, -3832(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$45, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$769, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-3536(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-144(%rbp), %rcx
	movq	-3840(%rbp), %r9
	xorl	%esi, %esi
	movq	-3808(%rbp), %xmm0
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -3568(%rbp)
	movq	-3520(%rbp), %rax
	leaq	-3568(%rbp), %rdx
	movhps	-3824(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-3816(%rbp), %xmm0
	movq	%rax, -3560(%rbp)
	movhps	-3808(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%rbx, %xmm0
	movl	$6, %ebx
	pushq	%rbx
	movhps	-3832(%rbp), %xmm0
	pushq	%rcx
	movl	$1, %ecx
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L856:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-3624(%rbp), %rdx
	movq	-3616(%rbp), %rax
	movaps	%xmm0, -3568(%rbp)
	movq	$0, -3552(%rbp)
	movq	%rdx, -144(%rbp)
	movq	-3608(%rbp), %rdx
	movq	%rax, -136(%rbp)
	movq	%rdx, -128(%rbp)
	movq	-3600(%rbp), %rdx
	movq	%rax, -96(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-3592(%rbp), %rdx
	movq	%rdx, -112(%rbp)
	movq	-3584(%rbp), %rdx
	movq	%rdx, -104(%rbp)
	leaq	-88(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3216(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L506
	call	_ZdlPv@PLT
.L506:
	movq	-3800(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L857:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-80(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-3616(%rbp), %xmm0
	movq	-3584(%rbp), %xmm1
	movq	$0, -3552(%rbp)
	movq	-3600(%rbp), %xmm2
	movq	-3632(%rbp), %xmm3
	movhps	-3608(%rbp), %xmm0
	movhps	-3616(%rbp), %xmm1
	movhps	-3592(%rbp), %xmm2
	movhps	-3624(%rbp), %xmm3
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -3568(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2448(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L517
	call	_ZdlPv@PLT
.L517:
	movq	-3792(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L858:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-3640(%rbp), %rdx
	movq	-3616(%rbp), %rax
	movaps	%xmm0, -3568(%rbp)
	movq	$0, -3552(%rbp)
	movq	%rdx, -144(%rbp)
	movq	-3632(%rbp), %rdx
	movq	%rax, -120(%rbp)
	movq	%rdx, -136(%rbp)
	movq	-3624(%rbp), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -128(%rbp)
	movq	-3608(%rbp), %rdx
	movq	%rdx, -112(%rbp)
	movq	-3600(%rbp), %rdx
	movq	%rdx, -104(%rbp)
	movq	-3592(%rbp), %rdx
	movq	%rdx, -96(%rbp)
	movq	-3584(%rbp), %rdx
	movq	%rdx, -88(%rbp)
	leaq	-72(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1680(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L528
	call	_ZdlPv@PLT
.L528:
	movq	-3784(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L859:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-64(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-3616(%rbp), %xmm0
	movq	-3584(%rbp), %xmm1
	movq	$0, -3552(%rbp)
	movq	-3600(%rbp), %xmm2
	movq	-3632(%rbp), %xmm3
	movq	-3648(%rbp), %xmm4
	movhps	-3608(%rbp), %xmm0
	movhps	-3616(%rbp), %xmm1
	movhps	-3592(%rbp), %xmm2
	movhps	-3624(%rbp), %xmm3
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3640(%rbp), %xmm4
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	movaps	%xmm0, -3568(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-912(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L539
	call	_ZdlPv@PLT
.L539:
	movq	-3776(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L538
.L855:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22460:
	.size	_ZN2v88internal51ArrayReduceRightLoopEagerDeoptContinuationAssembler54GenerateArrayReduceRightLoopEagerDeoptContinuationImplEv, .-_ZN2v88internal51ArrayReduceRightLoopEagerDeoptContinuationAssembler54GenerateArrayReduceRightLoopEagerDeoptContinuationImplEv
	.section	.rodata._ZN2v88internal8Builtins51Generate_ArrayReduceRightLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"ArrayReduceRightLoopEagerDeoptContinuation"
	.section	.text._ZN2v88internal8Builtins51Generate_ArrayReduceRightLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins51Generate_ArrayReduceRightLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins51Generate_ArrayReduceRightLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins51Generate_ArrayReduceRightLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE:
.LFB22456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$351, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$767, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L864
.L861:
	movq	%r13, %rdi
	call	_ZN2v88internal51ArrayReduceRightLoopEagerDeoptContinuationAssembler54GenerateArrayReduceRightLoopEagerDeoptContinuationImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L865
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L864:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L861
.L865:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22456:
	.size	_ZN2v88internal8Builtins51Generate_ArrayReduceRightLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins51Generate_ArrayReduceRightLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal50ArrayReduceRightLoopLazyDeoptContinuationAssembler53GenerateArrayReduceRightLoopLazyDeoptContinuationImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal50ArrayReduceRightLoopLazyDeoptContinuationAssembler53GenerateArrayReduceRightLoopLazyDeoptContinuationImplEv
	.type	_ZN2v88internal50ArrayReduceRightLoopLazyDeoptContinuationAssembler53GenerateArrayReduceRightLoopLazyDeoptContinuationImplEv, @function
_ZN2v88internal50ArrayReduceRightLoopLazyDeoptContinuationAssembler53GenerateArrayReduceRightLoopLazyDeoptContinuationImplEv:
.LFB22508:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$7, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-3408(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-3536(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$3816, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -3656(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -3768(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -3808(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, -3816(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, -3824(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	leaq	-3656(%rbp), %r12
	movq	%rax, -3832(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$144, %edi
	movq	$0, -3400(%rbp)
	movq	%rax, -3840(%rbp)
	movq	-3656(%rbp), %rax
	movq	$0, -3392(%rbp)
	movq	%rax, -3408(%rbp)
	movq	$0, -3384(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -3400(%rbp)
	leaq	-3352(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3384(%rbp)
	movq	%rdx, -3392(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3368(%rbp)
	movq	%rax, -3672(%rbp)
	movq	$0, -3376(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$168, %edi
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	%rax, -3216(%rbp)
	movq	$0, -3192(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -3208(%rbp)
	leaq	-3160(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3192(%rbp)
	movq	%rdx, -3200(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3176(%rbp)
	movq	%rax, -3800(%rbp)
	movq	$0, -3184(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$192, %edi
	movq	$0, -3016(%rbp)
	movq	$0, -3008(%rbp)
	movq	%rax, -3024(%rbp)
	movq	$0, -3000(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -3016(%rbp)
	leaq	-2968(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3000(%rbp)
	movq	%rdx, -3008(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2984(%rbp)
	movq	%rax, -3760(%rbp)
	movq	$0, -2992(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2824(%rbp)
	movq	$0, -2816(%rbp)
	movq	%rax, -2832(%rbp)
	movq	$0, -2808(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2824(%rbp)
	leaq	-2776(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2808(%rbp)
	movq	%rdx, -2816(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2792(%rbp)
	movq	%rax, -3704(%rbp)
	movq	$0, -2800(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2632(%rbp)
	movq	$0, -2624(%rbp)
	movq	%rax, -2640(%rbp)
	movq	$0, -2616(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -2632(%rbp)
	leaq	-2584(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2616(%rbp)
	movq	%rdx, -2624(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2600(%rbp)
	movq	%rax, -3712(%rbp)
	movq	$0, -2608(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$192, %edi
	movq	$0, -2440(%rbp)
	movq	$0, -2432(%rbp)
	movq	%rax, -2448(%rbp)
	movq	$0, -2424(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -2440(%rbp)
	leaq	-2392(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2424(%rbp)
	movq	%rdx, -2432(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2408(%rbp)
	movq	%rax, -3792(%rbp)
	movq	$0, -2416(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$216, %edi
	movq	$0, -2248(%rbp)
	movq	$0, -2240(%rbp)
	movq	%rax, -2256(%rbp)
	movq	$0, -2232(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -2248(%rbp)
	leaq	-2200(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2232(%rbp)
	movq	%rdx, -2240(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2216(%rbp)
	movq	%rax, -3744(%rbp)
	movq	$0, -2224(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2056(%rbp)
	movq	$0, -2048(%rbp)
	movq	%rax, -2064(%rbp)
	movq	$0, -2040(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -2056(%rbp)
	leaq	-2008(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2040(%rbp)
	movq	%rdx, -2048(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2024(%rbp)
	movq	%rax, -3688(%rbp)
	movq	$0, -2032(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1864(%rbp)
	movq	$0, -1856(%rbp)
	movq	%rax, -1872(%rbp)
	movq	$0, -1848(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1864(%rbp)
	leaq	-1816(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1848(%rbp)
	movq	%rdx, -1856(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1832(%rbp)
	movq	%rax, -3848(%rbp)
	movq	$0, -1840(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1672(%rbp)
	movq	$0, -1664(%rbp)
	movq	%rax, -1680(%rbp)
	movq	$0, -1656(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1672(%rbp)
	leaq	-1624(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1656(%rbp)
	movq	%rdx, -1664(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1640(%rbp)
	movq	%rax, -3784(%rbp)
	movq	$0, -1648(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$240, %edi
	movq	$0, -1480(%rbp)
	movq	$0, -1472(%rbp)
	movq	%rax, -1488(%rbp)
	movq	$0, -1464(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -1480(%rbp)
	leaq	-1432(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1464(%rbp)
	movq	%rdx, -1472(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1448(%rbp)
	movq	%rax, -3728(%rbp)
	movq	$0, -1456(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1288(%rbp)
	movq	$0, -1280(%rbp)
	movq	%rax, -1296(%rbp)
	movq	$0, -1272(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1288(%rbp)
	leaq	-1240(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1272(%rbp)
	movq	%rdx, -1280(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1256(%rbp)
	movq	%rax, -3752(%rbp)
	movq	$0, -1264(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1096(%rbp)
	movq	$0, -1088(%rbp)
	movq	%rax, -1104(%rbp)
	movq	$0, -1080(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1096(%rbp)
	leaq	-1048(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1080(%rbp)
	movq	%rdx, -1088(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1064(%rbp)
	movq	%rax, -3696(%rbp)
	movq	$0, -1072(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$240, %edi
	movq	$0, -904(%rbp)
	movq	$0, -896(%rbp)
	movq	%rax, -912(%rbp)
	movq	$0, -888(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -904(%rbp)
	leaq	-856(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -888(%rbp)
	movq	%rdx, -896(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -872(%rbp)
	movq	%rax, -3776(%rbp)
	movq	$0, -880(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$264, %edi
	movq	$0, -712(%rbp)
	movq	$0, -704(%rbp)
	movq	%rax, -720(%rbp)
	movq	$0, -696(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -712(%rbp)
	leaq	-664(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -696(%rbp)
	movq	%rdx, -704(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -680(%rbp)
	movq	%rax, -3720(%rbp)
	movq	$0, -688(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$216, %edi
	movq	$0, -520(%rbp)
	movq	$0, -512(%rbp)
	movq	%rax, -528(%rbp)
	movq	$0, -504(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -520(%rbp)
	leaq	-472(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -504(%rbp)
	movq	%rdx, -512(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -488(%rbp)
	movq	%rax, -3736(%rbp)
	movq	$0, -496(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3656(%rbp), %rax
	movl	$240, %edi
	movq	$0, -328(%rbp)
	movq	$0, -320(%rbp)
	movq	%rax, -336(%rbp)
	movq	$0, -312(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -328(%rbp)
	leaq	-280(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -312(%rbp)
	movq	%rdx, -320(%rbp)
	xorl	%edx, %edx
	movq	%rax, -3680(%rbp)
	movups	%xmm0, -296(%rbp)
	movq	$0, -304(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movq	-3768(%rbp), %xmm1
	movaps	%xmm0, -3536(%rbp)
	movhps	-3808(%rbp), %xmm1
	movq	$0, -3520(%rbp)
	movaps	%xmm1, -144(%rbp)
	movq	-3816(%rbp), %xmm1
	movhps	-3824(%rbp), %xmm1
	movaps	%xmm1, -128(%rbp)
	movq	-3832(%rbp), %xmm1
	movhps	-3840(%rbp), %xmm1
	movaps	%xmm1, -112(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm6
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm5
	leaq	48(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movups	%xmm6, 16(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L867
	call	_ZdlPv@PLT
.L867:
	movq	-3672(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3344(%rbp)
	jne	.L1203
	cmpq	$0, -3152(%rbp)
	jne	.L1204
.L872:
	cmpq	$0, -2960(%rbp)
	jne	.L1205
.L875:
	cmpq	$0, -2768(%rbp)
	jne	.L1206
.L878:
	cmpq	$0, -2576(%rbp)
	jne	.L1207
.L879:
	cmpq	$0, -2384(%rbp)
	jne	.L1208
.L883:
	leaq	-1872(%rbp), %rax
	cmpq	$0, -2192(%rbp)
	movq	%rax, -3768(%rbp)
	jne	.L1209
	cmpq	$0, -2000(%rbp)
	jne	.L1210
.L889:
	cmpq	$0, -1808(%rbp)
	jne	.L1211
.L890:
	cmpq	$0, -1616(%rbp)
	jne	.L1212
.L894:
	cmpq	$0, -1424(%rbp)
	jne	.L1213
.L897:
	cmpq	$0, -1232(%rbp)
	jne	.L1214
.L900:
	cmpq	$0, -1040(%rbp)
	jne	.L1215
.L901:
	cmpq	$0, -848(%rbp)
	jne	.L1216
.L905:
	cmpq	$0, -656(%rbp)
	jne	.L1217
.L908:
	cmpq	$0, -464(%rbp)
	jne	.L1218
.L911:
	cmpq	$0, -272(%rbp)
	jne	.L1219
.L912:
	movq	-3680(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L914
	call	_ZdlPv@PLT
.L914:
	movq	-320(%rbp), %rbx
	movq	-328(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L915
	.p2align 4,,10
	.p2align 3
.L919:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L916
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L919
.L917:
	movq	-328(%rbp), %r13
.L915:
	testq	%r13, %r13
	je	.L920
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L920:
	movq	-3736(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-496(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L921
	call	_ZdlPv@PLT
.L921:
	movq	-512(%rbp), %rbx
	movq	-520(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L922
	.p2align 4,,10
	.p2align 3
.L926:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L923
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L926
.L924:
	movq	-520(%rbp), %r13
.L922:
	testq	%r13, %r13
	je	.L927
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L927:
	movq	-3720(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L928
	call	_ZdlPv@PLT
.L928:
	movq	-704(%rbp), %rbx
	movq	-712(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L929
	.p2align 4,,10
	.p2align 3
.L933:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L930
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L933
.L931:
	movq	-712(%rbp), %r13
.L929:
	testq	%r13, %r13
	je	.L934
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L934:
	movq	-3776(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L935
	call	_ZdlPv@PLT
.L935:
	movq	-896(%rbp), %rbx
	movq	-904(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L936
	.p2align 4,,10
	.p2align 3
.L940:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L937
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L940
.L938:
	movq	-904(%rbp), %r13
.L936:
	testq	%r13, %r13
	je	.L941
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L941:
	movq	-3696(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L942
	call	_ZdlPv@PLT
.L942:
	movq	-1088(%rbp), %rbx
	movq	-1096(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L943
	.p2align 4,,10
	.p2align 3
.L947:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L944
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L947
.L945:
	movq	-1096(%rbp), %r13
.L943:
	testq	%r13, %r13
	je	.L948
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L948:
	movq	-3752(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L949
	call	_ZdlPv@PLT
.L949:
	movq	-1280(%rbp), %rbx
	movq	-1288(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L950
	.p2align 4,,10
	.p2align 3
.L954:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L951
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L954
.L952:
	movq	-1288(%rbp), %r13
.L950:
	testq	%r13, %r13
	je	.L955
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L955:
	movq	-3728(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1456(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L956
	call	_ZdlPv@PLT
.L956:
	movq	-1472(%rbp), %rbx
	movq	-1480(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L957
	.p2align 4,,10
	.p2align 3
.L961:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L958
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L961
.L959:
	movq	-1480(%rbp), %r13
.L957:
	testq	%r13, %r13
	je	.L962
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L962:
	movq	-3784(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1648(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L963
	call	_ZdlPv@PLT
.L963:
	movq	-1664(%rbp), %rbx
	movq	-1672(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L964
	.p2align 4,,10
	.p2align 3
.L968:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L965
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L968
.L966:
	movq	-1672(%rbp), %r13
.L964:
	testq	%r13, %r13
	je	.L969
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L969:
	movq	-3768(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3688(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2032(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L970
	call	_ZdlPv@PLT
.L970:
	movq	-2048(%rbp), %rbx
	movq	-2056(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L971
	.p2align 4,,10
	.p2align 3
.L975:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L972
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L975
.L973:
	movq	-2056(%rbp), %r13
.L971:
	testq	%r13, %r13
	je	.L976
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L976:
	movq	-3744(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L977
	call	_ZdlPv@PLT
.L977:
	movq	-2240(%rbp), %rbx
	movq	-2248(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L978
	.p2align 4,,10
	.p2align 3
.L982:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L979
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L982
.L980:
	movq	-2248(%rbp), %r13
.L978:
	testq	%r13, %r13
	je	.L983
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L983:
	movq	-3792(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2416(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L984
	call	_ZdlPv@PLT
.L984:
	movq	-2432(%rbp), %rbx
	movq	-2440(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L985
	.p2align 4,,10
	.p2align 3
.L989:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L986
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L989
.L987:
	movq	-2440(%rbp), %r13
.L985:
	testq	%r13, %r13
	je	.L990
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L990:
	movq	-3712(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2608(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L991
	call	_ZdlPv@PLT
.L991:
	movq	-2624(%rbp), %rbx
	movq	-2632(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L992
	.p2align 4,,10
	.p2align 3
.L996:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L993
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L996
.L994:
	movq	-2632(%rbp), %r13
.L992:
	testq	%r13, %r13
	je	.L997
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L997:
	movq	-3704(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L998
	call	_ZdlPv@PLT
.L998:
	movq	-2816(%rbp), %rbx
	movq	-2824(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L999
	.p2align 4,,10
	.p2align 3
.L1003:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1000
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L1003
.L1001:
	movq	-2824(%rbp), %r13
.L999:
	testq	%r13, %r13
	je	.L1004
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1004:
	movq	-3760(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1005
	call	_ZdlPv@PLT
.L1005:
	movq	-3008(%rbp), %rbx
	movq	-3016(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1006
	.p2align 4,,10
	.p2align 3
.L1010:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1007
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1010
.L1008:
	movq	-3016(%rbp), %r13
.L1006:
	testq	%r13, %r13
	je	.L1011
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1011:
	movq	-3800(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1012
	call	_ZdlPv@PLT
.L1012:
	movq	-3200(%rbp), %rbx
	movq	-3208(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1013
	.p2align 4,,10
	.p2align 3
.L1017:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1014
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L1017
.L1015:
	movq	-3208(%rbp), %r13
.L1013:
	testq	%r13, %r13
	je	.L1018
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1018:
	movq	-3672(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1019
	call	_ZdlPv@PLT
.L1019:
	movq	-3392(%rbp), %rbx
	movq	-3400(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1020
	.p2align 4,,10
	.p2align 3
.L1024:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1021
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1024
.L1022:
	movq	-3400(%rbp), %r13
.L1020:
	testq	%r13, %r13
	je	.L1025
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1025:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1220
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1021:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1024
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1014:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1017
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1007:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1010
	jmp	.L1008
	.p2align 4,,10
	.p2align 3
.L1000:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1003
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L993:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L996
	jmp	.L994
	.p2align 4,,10
	.p2align 3
.L986:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L989
	jmp	.L987
	.p2align 4,,10
	.p2align 3
.L979:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L982
	jmp	.L980
	.p2align 4,,10
	.p2align 3
.L972:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L975
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L965:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L968
	jmp	.L966
	.p2align 4,,10
	.p2align 3
.L958:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L961
	jmp	.L959
	.p2align 4,,10
	.p2align 3
.L951:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L954
	jmp	.L952
	.p2align 4,,10
	.p2align 3
.L944:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L947
	jmp	.L945
	.p2align 4,,10
	.p2align 3
.L937:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L940
	jmp	.L938
	.p2align 4,,10
	.p2align 3
.L930:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L933
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L916:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L919
	jmp	.L917
	.p2align 4,,10
	.p2align 3
.L923:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L926
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L1203:
	movq	-3672(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3624(%rbp)
	leaq	-144(%rbp), %rbx
	movq	$0, -3616(%rbp)
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	movq	$0, -3592(%rbp)
	movq	$0, -3584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r14, %rdi
	leaq	-3584(%rbp), %rax
	pushq	%rax
	leaq	-3592(%rbp), %r9
	leaq	-3608(%rbp), %rcx
	leaq	-3600(%rbp), %r8
	leaq	-3616(%rbp), %rdx
	leaq	-3624(%rbp), %rsi
	leaq	-3568(%rbp), %r14
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_SA_SA_
	movl	$56, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3616(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r15, %rdi
	movq	-3624(%rbp), %rsi
	call	_ZN2v88internal21Cast10JSReceiver_1404EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-80(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-3592(%rbp), %xmm0
	movq	%rax, %xmm5
	movq	-3616(%rbp), %xmm3
	movq	$0, -3552(%rbp)
	movq	-3608(%rbp), %xmm1
	movq	-3624(%rbp), %xmm2
	movhps	-3584(%rbp), %xmm0
	punpcklqdq	%xmm5, %xmm3
	movhps	-3600(%rbp), %xmm1
	movhps	-3616(%rbp), %xmm2
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -3568(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3024(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	popq	%rsi
	popq	%r8
	testq	%rdi, %rdi
	je	.L869
	call	_ZdlPv@PLT
.L869:
	movq	-3760(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3528(%rbp)
	jne	.L1221
.L870:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -3152(%rbp)
	je	.L872
.L1204:
	movq	-3800(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-3216(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movl	$2056, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$8, 6(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L873
	call	_ZdlPv@PLT
.L873:
	movq	(%r14), %rax
	leaq	-144(%rbp), %rsi
	leaq	-96(%rbp), %rdx
	movq	%r13, %rdi
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm1
	movdqu	32(%rax), %xmm6
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2832(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L874
	call	_ZdlPv@PLT
.L874:
	movq	-3704(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2960(%rbp)
	je	.L875
.L1205:
	movq	-3760(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-3024(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$506663788666685447, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L876
	call	_ZdlPv@PLT
.L876:
	movq	(%r14), %rax
	leaq	-144(%rbp), %rsi
	leaq	-88(%rbp), %rdx
	movq	%r13, %rdi
	movdqu	32(%rax), %xmm0
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm1
	movq	56(%rax), %rax
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2640(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L877
	call	_ZdlPv@PLT
.L877:
	movq	-3712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2768(%rbp)
	je	.L878
.L1206:
	movq	-3704(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3616(%rbp)
	leaq	-2832(%rbp), %r14
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	movq	$0, -3592(%rbp)
	movq	$0, -3584(%rbp)
	movq	$0, -3568(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r14, %rdi
	leaq	-3568(%rbp), %rax
	pushq	%rax
	leaq	-3608(%rbp), %rdx
	leaq	-3600(%rbp), %rcx
	leaq	-3584(%rbp), %r9
	leaq	-3592(%rbp), %r8
	leaq	-3616(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS6_IS4_EESA_SA_SA_SA_
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -2576(%rbp)
	popq	%rax
	popq	%rdx
	je	.L879
.L1207:
	movq	-3712(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3632(%rbp)
	leaq	-2640(%rbp), %r14
	movq	$0, -3624(%rbp)
	leaq	-144(%rbp), %rbx
	movq	$0, -3616(%rbp)
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	movq	$0, -3592(%rbp)
	movq	$0, -3584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3584(%rbp), %rax
	movq	%r14, %rdi
	leaq	-3600(%rbp), %r9
	pushq	%rax
	leaq	-3592(%rbp), %rax
	leaq	-3616(%rbp), %rcx
	pushq	%rax
	leaq	-3608(%rbp), %r8
	leaq	-3624(%rbp), %rdx
	leaq	-3632(%rbp), %rsi
	leaq	-3568(%rbp), %r14
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_PNS7_IS5_EE
	movl	$57, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3616(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r15, %rdi
	movq	-3632(%rbp), %rsi
	call	_ZN2v88internal82Cast71UT19ATCallableApiObject17ATCallableJSProxy15JSBoundFunction10JSFunction_1405EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-3632(%rbp), %rdx
	movq	%rax, %r8
	movq	-3616(%rbp), %rax
	movaps	%xmm0, -3568(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rdx, -144(%rbp)
	movq	-3624(%rbp), %rdx
	movq	%rax, -128(%rbp)
	movq	%rdx, -136(%rbp)
	movq	-3608(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-3600(%rbp), %rdx
	movq	$0, -3552(%rbp)
	movq	%rdx, -112(%rbp)
	movq	-3592(%rbp), %rdx
	movq	%rdx, -104(%rbp)
	movq	-3584(%rbp), %rdx
	movq	%rdx, -96(%rbp)
	leaq	-72(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2256(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	popq	%r11
	popq	%rax
	testq	%rdi, %rdi
	je	.L880
	call	_ZdlPv@PLT
.L880:
	movq	-3744(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3528(%rbp)
	jne	.L1222
.L881:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2384(%rbp)
	je	.L883
.L1208:
	movq	-3792(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2448(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$578439907727902727, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L884
	call	_ZdlPv@PLT
.L884:
	movq	(%r14), %rax
	leaq	-144(%rbp), %rsi
	leaq	-88(%rbp), %rdx
	movq	%r13, %rdi
	movdqu	32(%rax), %xmm0
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm1
	movq	48(%rax), %rax
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2064(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L885
	call	_ZdlPv@PLT
.L885:
	movq	-3688(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L1209:
	movq	-3744(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2256(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$578439907727902727, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L887
	call	_ZdlPv@PLT
.L887:
	movq	(%r14), %rax
	pxor	%xmm0, %xmm0
	movq	24(%rax), %rdi
	movq	32(%rax), %rsi
	movq	48(%rax), %rdx
	movq	(%rax), %r10
	movq	8(%rax), %r9
	movq	16(%rax), %r8
	movq	40(%rax), %rcx
	movq	64(%rax), %rax
	movq	%rdi, -120(%rbp)
	movq	%r13, %rdi
	movq	%rsi, -112(%rbp)
	leaq	-144(%rbp), %rsi
	movq	%rdx, -96(%rbp)
	leaq	-80(%rbp), %rdx
	movq	%r10, -144(%rbp)
	movq	%r9, -136(%rbp)
	movq	%r8, -128(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%rax, -88(%rbp)
	movaps	%xmm0, -3536(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1872(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -3768(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L888
	call	_ZdlPv@PLT
.L888:
	movq	-3848(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2000(%rbp)
	je	.L889
.L1210:
	movq	-3688(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3624(%rbp)
	leaq	-2064(%rbp), %r14
	movq	$0, -3616(%rbp)
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	movq	$0, -3592(%rbp)
	movq	$0, -3584(%rbp)
	movq	$0, -3568(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3568(%rbp), %rax
	movq	%r14, %rdi
	leaq	-3592(%rbp), %r9
	pushq	%rax
	leaq	-3584(%rbp), %rax
	leaq	-3608(%rbp), %rcx
	pushq	%rax
	leaq	-3616(%rbp), %rdx
	leaq	-3600(%rbp), %r8
	leaq	-3624(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_PNS7_IS5_EE
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -1808(%rbp)
	popq	%r9
	popq	%r10
	je	.L890
.L1211:
	movq	-3848(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3640(%rbp)
	leaq	-3568(%rbp), %r14
	movq	$0, -3632(%rbp)
	leaq	-144(%rbp), %rbx
	movq	$0, -3624(%rbp)
	movq	$0, -3616(%rbp)
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	movq	$0, -3592(%rbp)
	movq	$0, -3584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3584(%rbp), %rax
	movq	-3768(%rbp), %rdi
	pushq	%rax
	leaq	-3592(%rbp), %rax
	leaq	-3608(%rbp), %r9
	pushq	%rax
	leaq	-3600(%rbp), %rax
	leaq	-3624(%rbp), %rcx
	pushq	%rax
	leaq	-3616(%rbp), %r8
	leaq	-3632(%rbp), %rdx
	leaq	-3640(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_PNS7_IS5_EESD_
	addq	$32, %rsp
	movl	$58, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3616(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal29Cast20UT5ATSmi10HeapNumber_85EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-64(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-3592(%rbp), %xmm0
	movq	%rax, %xmm7
	movq	-3616(%rbp), %xmm4
	movq	$0, -3552(%rbp)
	movq	-3608(%rbp), %xmm1
	movq	-3624(%rbp), %xmm2
	movq	-3640(%rbp), %xmm3
	movhps	-3584(%rbp), %xmm0
	punpcklqdq	%xmm7, %xmm4
	movhps	-3600(%rbp), %xmm1
	movhps	-3616(%rbp), %xmm2
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3632(%rbp), %xmm3
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm0, -3568(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1488(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L891
	call	_ZdlPv@PLT
.L891:
	movq	-3728(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3528(%rbp)
	jne	.L1223
.L892:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1616(%rbp)
	je	.L894
.L1212:
	movq	-3784(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1680(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$506382313689974791, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L895
	call	_ZdlPv@PLT
.L895:
	movq	(%rbx), %rax
	leaq	-144(%rbp), %rsi
	leaq	-80(%rbp), %rdx
	movq	%r13, %rdi
	movdqu	32(%rax), %xmm0
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm1
	movdqu	48(%rax), %xmm4
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1296(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L896
	call	_ZdlPv@PLT
.L896:
	movq	-3752(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1424(%rbp)
	je	.L897
.L1213:
	movq	-3728(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1488(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r8d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$506382313689974791, %rcx
	movw	%r8w, 8(%rax)
	leaq	10(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L898
	call	_ZdlPv@PLT
.L898:
	movq	(%rbx), %rax
	leaq	-144(%rbp), %rsi
	leaq	-72(%rbp), %rdx
	movq	%r13, %rdi
	movdqu	48(%rax), %xmm0
	movdqu	(%rax), %xmm3
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	72(%rax), %rax
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -144(%rbp)
	movq	%rax, -80(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3536(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1104(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L899
	call	_ZdlPv@PLT
.L899:
	movq	-3696(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1232(%rbp)
	je	.L900
.L1214:
	movq	-3752(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3632(%rbp)
	leaq	-1296(%rbp), %r14
	movq	$0, -3624(%rbp)
	movq	$0, -3616(%rbp)
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	movq	$0, -3592(%rbp)
	movq	$0, -3584(%rbp)
	movq	$0, -3568(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r14, %rdi
	leaq	-3568(%rbp), %rax
	pushq	%rax
	leaq	-3584(%rbp), %rax
	leaq	-3616(%rbp), %rcx
	pushq	%rax
	leaq	-3592(%rbp), %rax
	leaq	-3624(%rbp), %rdx
	pushq	%rax
	leaq	-3600(%rbp), %r9
	leaq	-3608(%rbp), %r8
	leaq	-3632(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EESB_SB_SB_SB_PNS7_IS5_EESD_
	addq	$32, %rsp
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -1040(%rbp)
	je	.L901
.L1215:
	movq	-3696(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3648(%rbp)
	leaq	-1104(%rbp), %r14
	movq	$0, -3640(%rbp)
	leaq	-144(%rbp), %rbx
	movq	$0, -3632(%rbp)
	movq	$0, -3624(%rbp)
	movq	$0, -3616(%rbp)
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	movq	$0, -3592(%rbp)
	movq	$0, -3584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3584(%rbp), %rax
	movq	%r14, %rdi
	leaq	-3616(%rbp), %r9
	pushq	%rax
	leaq	-3592(%rbp), %rax
	leaq	-3632(%rbp), %rcx
	pushq	%rax
	leaq	-3600(%rbp), %rax
	leaq	-3624(%rbp), %r8
	pushq	%rax
	leaq	-3608(%rbp), %rax
	leaq	-3640(%rbp), %rdx
	pushq	%rax
	leaq	-3648(%rbp), %rsi
	leaq	-3568(%rbp), %r14
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverES5_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_SF_SF_SF_PNSB_IS5_EESH_PNSB_IS9_EE
	addq	$32, %rsp
	movl	$59, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3616(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal29Cast20UT5ATSmi10HeapNumber_85EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-3648(%rbp), %rdx
	movq	%rax, %r8
	movq	-3616(%rbp), %rax
	movaps	%xmm0, -3568(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rdx, -144(%rbp)
	movq	-3640(%rbp), %rdx
	movq	%rax, -112(%rbp)
	movq	%rdx, -136(%rbp)
	movq	-3632(%rbp), %rdx
	movq	%rax, -72(%rbp)
	movq	%rdx, -128(%rbp)
	movq	-3624(%rbp), %rdx
	movq	$0, -3552(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-3608(%rbp), %rdx
	movq	%rdx, -104(%rbp)
	movq	-3600(%rbp), %rdx
	movq	%rdx, -96(%rbp)
	movq	-3592(%rbp), %rdx
	movq	%rdx, -88(%rbp)
	movq	-3584(%rbp), %rdx
	movq	%rdx, -80(%rbp)
	leaq	-56(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-720(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L902
	call	_ZdlPv@PLT
.L902:
	movq	-3720(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3528(%rbp)
	jne	.L1224
.L903:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -848(%rbp)
	je	.L905
.L1216:
	movq	-3776(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-912(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movl	$2056, %edi
	movq	%r13, %rsi
	movabsq	$506382313689974791, %rcx
	movw	%di, 8(%rax)
	leaq	10(%rax), %rdx
	movq	%r14, %rdi
	movq	%rcx, (%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L906
	call	_ZdlPv@PLT
.L906:
	movq	(%rbx), %rax
	leaq	-144(%rbp), %rsi
	leaq	-72(%rbp), %rdx
	movq	%r13, %rdi
	movdqu	48(%rax), %xmm0
	movdqu	(%rax), %xmm3
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	64(%rax), %rax
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -144(%rbp)
	movq	%rax, -80(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3536(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-528(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L907
	call	_ZdlPv@PLT
.L907:
	movq	-3736(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -656(%rbp)
	je	.L908
.L1217:
	movq	-3720(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-720(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$11, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movl	$2056, %esi
	movq	%r14, %rdi
	movabsq	$506382313689974791, %rcx
	movw	%si, 8(%rax)
	leaq	11(%rax), %rdx
	movq	%r13, %rsi
	movq	%rcx, (%rax)
	movb	$8, 10(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L909
	call	_ZdlPv@PLT
.L909:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	40(%rax), %rdi
	movq	48(%rax), %rsi
	movq	64(%rax), %rdx
	movq	8(%rax), %r11
	movq	16(%rax), %r10
	movq	24(%rax), %r9
	movq	32(%rax), %r8
	movq	56(%rax), %rcx
	movq	(%rax), %rbx
	movq	80(%rax), %rax
	movq	%rdi, -104(%rbp)
	movq	%r13, %rdi
	movq	%rsi, -96(%rbp)
	leaq	-144(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	leaq	-64(%rbp), %rdx
	movq	%r11, -136(%rbp)
	movq	%r10, -128(%rbp)
	movq	%r9, -120(%rbp)
	movq	%r8, -112(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%rax, -72(%rbp)
	movaps	%xmm0, -3536(%rbp)
	movq	%rbx, -144(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-336(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L910
	call	_ZdlPv@PLT
.L910:
	movq	-3680(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -464(%rbp)
	je	.L911
.L1218:
	movq	-3736(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3640(%rbp)
	leaq	-528(%rbp), %r14
	movq	$0, -3632(%rbp)
	movq	$0, -3624(%rbp)
	movq	$0, -3616(%rbp)
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	movq	$0, -3592(%rbp)
	movq	$0, -3584(%rbp)
	movq	$0, -3568(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3568(%rbp), %rax
	movq	%r14, %rdi
	leaq	-3624(%rbp), %rcx
	pushq	%rax
	leaq	-3584(%rbp), %rax
	leaq	-3632(%rbp), %rdx
	pushq	%rax
	leaq	-3592(%rbp), %rax
	leaq	-3608(%rbp), %r9
	pushq	%rax
	leaq	-3600(%rbp), %rax
	leaq	-3616(%rbp), %r8
	pushq	%rax
	leaq	-3640(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_NS0_10JSReceiverES5_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_SF_SF_SF_PNSB_IS5_EESH_PNSB_IS9_EE
	addq	$32, %rsp
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -272(%rbp)
	je	.L912
.L1219:
	movq	-3680(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-336(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$506382313689974791, %rcx
	movq	%rcx, (%rax)
	movl	$2056, %ecx
	leaq	10(%rax), %rdx
	movw	%cx, 8(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L913
	call	_ZdlPv@PLT
.L913:
	movq	(%rbx), %rax
	movl	$63, %edx
	movq	%r12, %rdi
	leaq	-3584(%rbp), %r14
	movq	40(%rax), %rcx
	movq	56(%rax), %rsi
	movq	(%rax), %r9
	movq	64(%rax), %rbx
	movq	%rcx, -3816(%rbp)
	movq	48(%rax), %rcx
	movq	72(%rax), %rax
	movq	%rsi, -3824(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%r9, -3840(%rbp)
	movq	%rcx, -3808(%rbp)
	movq	%rax, -3832(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$62, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$769, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-3536(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-144(%rbp), %rcx
	movq	-3840(%rbp), %r9
	xorl	%esi, %esi
	movq	-3808(%rbp), %xmm0
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -3568(%rbp)
	movq	-3520(%rbp), %rax
	leaq	-3568(%rbp), %rdx
	movhps	-3824(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-3816(%rbp), %xmm0
	movq	%rax, -3560(%rbp)
	movhps	-3808(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%rbx, %xmm0
	movl	$6, %ebx
	pushq	%rbx
	movhps	-3832(%rbp), %xmm0
	pushq	%rcx
	movl	$1, %ecx
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$64, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L1221:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-3624(%rbp), %rdx
	movq	-3616(%rbp), %rax
	movaps	%xmm0, -3568(%rbp)
	movq	$0, -3552(%rbp)
	movq	%rdx, -144(%rbp)
	movq	-3608(%rbp), %rdx
	movq	%rax, -136(%rbp)
	movq	%rdx, -128(%rbp)
	movq	-3600(%rbp), %rdx
	movq	%rax, -96(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-3592(%rbp), %rdx
	movq	%rdx, -112(%rbp)
	movq	-3584(%rbp), %rdx
	movq	%rdx, -104(%rbp)
	leaq	-88(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3216(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L871
	call	_ZdlPv@PLT
.L871:
	movq	-3800(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L870
	.p2align 4,,10
	.p2align 3
.L1222:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-80(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-3616(%rbp), %xmm0
	movq	-3584(%rbp), %xmm1
	movq	$0, -3552(%rbp)
	movq	-3600(%rbp), %xmm2
	movq	-3632(%rbp), %xmm3
	movhps	-3608(%rbp), %xmm0
	movhps	-3616(%rbp), %xmm1
	movhps	-3592(%rbp), %xmm2
	movhps	-3624(%rbp), %xmm3
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -3568(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2448(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L882
	call	_ZdlPv@PLT
.L882:
	movq	-3792(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L1223:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-3640(%rbp), %rdx
	movq	-3616(%rbp), %rax
	movaps	%xmm0, -3568(%rbp)
	movq	$0, -3552(%rbp)
	movq	%rdx, -144(%rbp)
	movq	-3632(%rbp), %rdx
	movq	%rax, -120(%rbp)
	movq	%rdx, -136(%rbp)
	movq	-3624(%rbp), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -128(%rbp)
	movq	-3608(%rbp), %rdx
	movq	%rdx, -112(%rbp)
	movq	-3600(%rbp), %rdx
	movq	%rdx, -104(%rbp)
	movq	-3592(%rbp), %rdx
	movq	%rdx, -96(%rbp)
	movq	-3584(%rbp), %rdx
	movq	%rdx, -88(%rbp)
	leaq	-72(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1680(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L893
	call	_ZdlPv@PLT
.L893:
	movq	-3784(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L1224:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-64(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-3616(%rbp), %xmm0
	movq	-3584(%rbp), %xmm1
	movq	$0, -3552(%rbp)
	movq	-3600(%rbp), %xmm2
	movq	-3632(%rbp), %xmm3
	movq	-3648(%rbp), %xmm4
	movhps	-3608(%rbp), %xmm0
	movhps	-3616(%rbp), %xmm1
	movhps	-3592(%rbp), %xmm2
	movhps	-3624(%rbp), %xmm3
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3640(%rbp), %xmm4
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm1, -80(%rbp)
	movaps	%xmm0, -3568(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-912(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L904
	call	_ZdlPv@PLT
.L904:
	movq	-3776(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L903
.L1220:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22508:
	.size	_ZN2v88internal50ArrayReduceRightLoopLazyDeoptContinuationAssembler53GenerateArrayReduceRightLoopLazyDeoptContinuationImplEv, .-_ZN2v88internal50ArrayReduceRightLoopLazyDeoptContinuationAssembler53GenerateArrayReduceRightLoopLazyDeoptContinuationImplEv
	.section	.rodata._ZN2v88internal8Builtins50Generate_ArrayReduceRightLoopLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"ArrayReduceRightLoopLazyDeoptContinuation"
	.section	.text._ZN2v88internal8Builtins50Generate_ArrayReduceRightLoopLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins50Generate_ArrayReduceRightLoopLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins50Generate_ArrayReduceRightLoopLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins50Generate_ArrayReduceRightLoopLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE:
.LFB22504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$653, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$768, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L1229
.L1226:
	movq	%r13, %rdi
	call	_ZN2v88internal50ArrayReduceRightLoopLazyDeoptContinuationAssembler53GenerateArrayReduceRightLoopLazyDeoptContinuationImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1230
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1229:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L1226
.L1230:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22504:
	.size	_ZN2v88internal8Builtins50Generate_ArrayReduceRightLoopLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins50Generate_ArrayReduceRightLoopLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverES4_NS0_6ObjectES4_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S5_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_PNSB_IS5_EESF_PNSB_IS9_EESJ_SH_SJ_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverES4_NS0_6ObjectES4_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S5_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_PNSB_IS5_EESF_PNSB_IS9_EESJ_SH_SJ_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverES4_NS0_6ObjectES4_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S5_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_PNSB_IS5_EESF_PNSB_IS9_EESJ_SH_SJ_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverES4_NS0_6ObjectES4_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S5_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_PNSB_IS5_EESF_PNSB_IS9_EESJ_SH_SJ_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverES4_NS0_6ObjectES4_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S5_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_PNSB_IS5_EESF_PNSB_IS9_EESJ_SH_SJ_:
.LFB27205:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$9, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$578721378409580295, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1232
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
.L1232:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1233
	movq	%rdx, (%r15)
.L1233:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1234
	movq	%rdx, (%r14)
.L1234:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1235
	movq	%rdx, 0(%r13)
.L1235:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1236
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1236:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1237
	movq	%rdx, (%rbx)
.L1237:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1238
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1238:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1239
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1239:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1240
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1240:
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L1231
	movq	-120(%rbp), %rbx
	movq	%rax, (%rbx)
.L1231:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1274
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1274:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27205:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverES4_NS0_6ObjectES4_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S5_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_PNSB_IS5_EESF_PNSB_IS9_EESJ_SH_SJ_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverES4_NS0_6ObjectES4_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S5_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_PNSB_IS5_EESF_PNSB_IS9_EESJ_SH_SJ_
	.section	.rodata._ZN2v88internal41ArrayReduceRightLoopContinuationAssembler44GenerateArrayReduceRightLoopContinuationImplEv.str1.1,"aMS",@progbits,1
.LC7:
	.string	"Array.prototype.reduceRight"
	.section	.text._ZN2v88internal41ArrayReduceRightLoopContinuationAssembler44GenerateArrayReduceRightLoopContinuationImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal41ArrayReduceRightLoopContinuationAssembler44GenerateArrayReduceRightLoopContinuationImplEv
	.type	_ZN2v88internal41ArrayReduceRightLoopContinuationAssembler44GenerateArrayReduceRightLoopContinuationImplEv, @function
_ZN2v88internal41ArrayReduceRightLoopContinuationAssembler44GenerateArrayReduceRightLoopContinuationImplEv:
.LFB22517:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$6, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-2992(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-3112(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$3448, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -3112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -3136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -3152(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, -3168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, -3176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%rax, -3200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$5, %esi
	movq	%r12, %rdi
	leaq	-2864(%rbp), %r12
	movq	%rax, -3216(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$168, %edi
	movq	$0, -2856(%rbp)
	movq	$0, -2848(%rbp)
	movq	%rax, %rbx
	movq	-3112(%rbp), %rax
	movq	$0, -2840(%rbp)
	movq	%rax, -2864(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -2856(%rbp)
	leaq	-2808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2840(%rbp)
	movq	%rdx, -2848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2824(%rbp)
	movq	%rax, -3128(%rbp)
	movq	$0, -2832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3112(%rbp), %rax
	movl	$216, %edi
	movq	$0, -2664(%rbp)
	movq	$0, -2656(%rbp)
	movq	%rax, -2672(%rbp)
	movq	$0, -2648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -2664(%rbp)
	leaq	-2616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2648(%rbp)
	movq	%rdx, -2656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2632(%rbp)
	movq	%rax, -3272(%rbp)
	movq	$0, -2640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3112(%rbp), %rax
	movl	$216, %edi
	movq	$0, -2472(%rbp)
	movq	$0, -2464(%rbp)
	movq	%rax, -2480(%rbp)
	movq	$0, -2456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -2472(%rbp)
	leaq	-2424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2456(%rbp)
	movq	%rdx, -2464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2440(%rbp)
	movq	%rax, -3232(%rbp)
	movq	$0, -2448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3112(%rbp), %rax
	movl	$240, %edi
	movq	$0, -2280(%rbp)
	movq	$0, -2272(%rbp)
	movq	%rax, -2288(%rbp)
	movq	$0, -2264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -2280(%rbp)
	leaq	-2232(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2264(%rbp)
	movq	%rdx, -2272(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2248(%rbp)
	movq	%rax, -3336(%rbp)
	movq	$0, -2256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3112(%rbp), %rax
	movl	$312, %edi
	movq	$0, -2088(%rbp)
	movq	$0, -2080(%rbp)
	movq	%rax, -2096(%rbp)
	movq	$0, -2072(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rax, -2088(%rbp)
	leaq	-2040(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2072(%rbp)
	movq	%rdx, -2080(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2056(%rbp)
	movq	%rax, -3352(%rbp)
	movq	$0, -2064(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3112(%rbp), %rax
	movl	$336, %edi
	movq	$0, -1896(%rbp)
	movq	$0, -1888(%rbp)
	movq	%rax, -1904(%rbp)
	movq	$0, -1880(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -1896(%rbp)
	leaq	-1848(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1880(%rbp)
	movq	%rdx, -1888(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1864(%rbp)
	movq	%rax, -3296(%rbp)
	movq	$0, -1872(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3112(%rbp), %rax
	movl	$288, %edi
	movq	$0, -1704(%rbp)
	movq	$0, -1696(%rbp)
	movq	%rax, -1712(%rbp)
	movq	$0, -1688(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -1704(%rbp)
	leaq	-1656(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1688(%rbp)
	movq	%rdx, -1696(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1672(%rbp)
	movq	%rax, -3328(%rbp)
	movq	$0, -1680(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3112(%rbp), %rax
	movl	$288, %edi
	movq	$0, -1512(%rbp)
	movq	$0, -1504(%rbp)
	movq	%rax, -1520(%rbp)
	movq	$0, -1496(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -1512(%rbp)
	leaq	-1464(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1496(%rbp)
	movq	%rdx, -1504(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1480(%rbp)
	movq	%rax, -3264(%rbp)
	movq	$0, -1488(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3112(%rbp), %rax
	movl	$240, %edi
	movq	$0, -1320(%rbp)
	movq	$0, -1312(%rbp)
	movq	%rax, -1328(%rbp)
	movq	$0, -1304(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -1320(%rbp)
	leaq	-1272(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1304(%rbp)
	movq	%rdx, -1312(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1288(%rbp)
	movq	%rax, -3256(%rbp)
	movq	$0, -1296(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3112(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1128(%rbp)
	movq	$0, -1120(%rbp)
	movq	%rax, -1136(%rbp)
	movq	$0, -1112(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1128(%rbp)
	leaq	-1080(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1112(%rbp)
	movq	%rdx, -1120(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1096(%rbp)
	movq	%rax, -3344(%rbp)
	movq	$0, -1104(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3112(%rbp), %rax
	movl	$216, %edi
	movq	$0, -936(%rbp)
	movq	$0, -928(%rbp)
	movq	%rax, -944(%rbp)
	movq	$0, -920(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -936(%rbp)
	leaq	-888(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -920(%rbp)
	movq	%rdx, -928(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -904(%rbp)
	movq	%rax, -3312(%rbp)
	movq	$0, -912(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3112(%rbp), %rax
	movl	$240, %edi
	movq	$0, -744(%rbp)
	movq	$0, -736(%rbp)
	movq	%rax, -752(%rbp)
	movq	$0, -728(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -744(%rbp)
	leaq	-696(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -728(%rbp)
	movq	%rdx, -736(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -712(%rbp)
	movq	%rax, -3360(%rbp)
	movq	$0, -720(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3112(%rbp), %rax
	movl	$264, %edi
	movq	$0, -552(%rbp)
	movq	$0, -544(%rbp)
	movq	%rax, -560(%rbp)
	movq	$0, -536(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -552(%rbp)
	leaq	-504(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -536(%rbp)
	movq	%rdx, -544(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -520(%rbp)
	movq	%rax, -3280(%rbp)
	movq	$0, -528(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3112(%rbp), %rax
	movl	$216, %edi
	movq	$0, -360(%rbp)
	movq	$0, -352(%rbp)
	movq	%rax, -368(%rbp)
	movq	$0, -344(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -360(%rbp)
	leaq	-312(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -344(%rbp)
	movq	%rdx, -352(%rbp)
	xorl	%edx, %edx
	movq	%rax, -3304(%rbp)
	movups	%xmm0, -328(%rbp)
	movq	$0, -336(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movq	-3136(%rbp), %xmm1
	movaps	%xmm0, -2992(%rbp)
	movhps	-3152(%rbp), %xmm1
	movq	%rbx, -128(%rbp)
	movaps	%xmm1, -176(%rbp)
	movq	-3168(%rbp), %xmm1
	movq	$0, -2976(%rbp)
	movhps	-3176(%rbp), %xmm1
	movaps	%xmm1, -160(%rbp)
	movq	-3200(%rbp), %xmm1
	movhps	-3216(%rbp), %xmm1
	movaps	%xmm1, -144(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm5
	movdqa	-160(%rbp), %xmm6
	leaq	56(%rax), %rdx
	movq	%rax, -2992(%rbp)
	movdqa	-144(%rbp), %xmm7
	movq	%rcx, 48(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -2976(%rbp)
	movq	%rdx, -2984(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1276
	call	_ZdlPv@PLT
.L1276:
	movq	-3128(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2672(%rbp), %rax
	cmpq	$0, -2800(%rbp)
	movq	%rax, -3136(%rbp)
	jne	.L1430
.L1277:
	leaq	-2480(%rbp), %rax
	cmpq	$0, -2608(%rbp)
	movq	%rax, -3184(%rbp)
	leaq	-944(%rbp), %rax
	movq	%rax, -3240(%rbp)
	jne	.L1431
.L1280:
	leaq	-2288(%rbp), %rax
	cmpq	$0, -2416(%rbp)
	movq	%rax, -3248(%rbp)
	leaq	-1328(%rbp), %rax
	movq	%rax, -3152(%rbp)
	jne	.L1432
.L1283:
	leaq	-1904(%rbp), %rax
	cmpq	$0, -2224(%rbp)
	movq	%rax, -3216(%rbp)
	leaq	-2096(%rbp), %rax
	movq	%rax, -3176(%rbp)
	jne	.L1433
.L1286:
	leaq	-1712(%rbp), %rax
	cmpq	$0, -2032(%rbp)
	movq	%rax, -3232(%rbp)
	jne	.L1434
.L1291:
	leaq	-1520(%rbp), %rax
	cmpq	$0, -1840(%rbp)
	movq	%rax, -3168(%rbp)
	jne	.L1435
.L1294:
	cmpq	$0, -1648(%rbp)
	jne	.L1436
	cmpq	$0, -1456(%rbp)
	jne	.L1437
.L1303:
	cmpq	$0, -1264(%rbp)
	leaq	-1136(%rbp), %rbx
	jne	.L1438
	cmpq	$0, -1072(%rbp)
	jne	.L1439
.L1309:
	leaq	-560(%rbp), %rax
	cmpq	$0, -880(%rbp)
	movq	%rax, -3256(%rbp)
	leaq	-752(%rbp), %rax
	movq	%rax, -3200(%rbp)
	jne	.L1440
.L1311:
	leaq	-368(%rbp), %rax
	cmpq	$0, -688(%rbp)
	movq	%rax, -3264(%rbp)
	jne	.L1441
.L1315:
	cmpq	$0, -496(%rbp)
	jne	.L1442
.L1318:
	cmpq	$0, -304(%rbp)
	jne	.L1443
.L1320:
	movq	-3264(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3256(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3200(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3240(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3152(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3168(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3232(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3216(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3176(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3248(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3184(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3136(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3128(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1322
	call	_ZdlPv@PLT
.L1322:
	movq	-2848(%rbp), %rbx
	movq	-2856(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1323
	.p2align 4,,10
	.p2align 3
.L1327:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1324
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L1327
.L1325:
	movq	-2856(%rbp), %r12
.L1323:
	testq	%r12, %r12
	je	.L1328
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1328:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1444
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1324:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1327
	jmp	.L1325
	.p2align 4,,10
	.p2align 3
.L1430:
	movq	-3128(%rbp), %rsi
	movq	%r13, %rdi
	movl	$2055, %ebx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2976(%rbp)
	movaps	%xmm0, -2992(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movw	%bx, 4(%rax)
	leaq	7(%rax), %rdx
	movl	$134678279, (%rax)
	movb	$8, 6(%rax)
	movq	%rax, -2992(%rbp)
	movq	%rdx, -2976(%rbp)
	movq	%rdx, -2984(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2992(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1278
	call	_ZdlPv@PLT
.L1278:
	movq	(%rbx), %rax
	movl	$72, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rbx
	movq	8(%rax), %rcx
	movq	(%rax), %r12
	movq	%rbx, -3136(%rbp)
	movq	32(%rax), %rbx
	movq	%rcx, -3152(%rbp)
	movq	16(%rax), %rcx
	movq	%rbx, -3176(%rbp)
	movq	40(%rax), %rbx
	movq	48(%rax), %rax
	movq	%rcx, -3168(%rbp)
	movq	%rax, -3200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$75, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %xmm0
	movq	%rbx, %xmm5
	movl	$72, %edi
	movhps	-3152(%rbp), %xmm0
	movq	%rbx, -112(%rbp)
	movaps	%xmm0, -176(%rbp)
	movq	-3168(%rbp), %xmm0
	movq	$0, -2976(%rbp)
	movhps	-3136(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-3176(%rbp), %xmm0
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-3200(%rbp), %xmm0
	movhps	-3136(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2992(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm6
	movq	%r14, %rsi
	movq	-112(%rbp), %rcx
	movdqa	-160(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm5
	leaq	72(%rax), %rdx
	movq	%rax, -2992(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-128(%rbp), %xmm6
	movq	%rcx, 64(%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	leaq	-2672(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2976(%rbp)
	movq	%rdx, -2984(%rbp)
	movq	%rax, -3136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1279
	call	_ZdlPv@PLT
.L1279:
	movq	-3272(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1277
	.p2align 4,,10
	.p2align 3
.L1431:
	movq	-3272(%rbp), %rsi
	movq	%r13, %rdi
	movq	$0, -3096(%rbp)
	movq	$0, -3088(%rbp)
	movq	$0, -3080(%rbp)
	movq	$0, -3072(%rbp)
	movq	$0, -3064(%rbp)
	movq	$0, -3056(%rbp)
	movq	$0, -3048(%rbp)
	movq	$0, -3040(%rbp)
	movq	$0, -3024(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3024(%rbp), %rax
	movq	-3136(%rbp), %rdi
	leaq	-3072(%rbp), %r8
	pushq	%rax
	leaq	-3040(%rbp), %rax
	leaq	-3080(%rbp), %rcx
	pushq	%rax
	leaq	-3048(%rbp), %rax
	leaq	-3064(%rbp), %r9
	pushq	%rax
	leaq	-3056(%rbp), %rax
	leaq	-3088(%rbp), %rdx
	pushq	%rax
	leaq	-3096(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverES4_NS0_6ObjectES4_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S5_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_PNSB_IS5_EESF_PNSB_IS9_EESJ_SH_SJ_
	addq	$32, %rsp
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	-3024(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal29NumberIsGreaterThanOrEqual_78EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	movq	-3064(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	-3088(%rbp), %rcx
	movq	-3080(%rbp), %rsi
	movq	-3072(%rbp), %rdx
	movq	%rax, %r12
	movaps	%xmm0, -2992(%rbp)
	movq	-3056(%rbp), %r10
	movq	-3048(%rbp), %r11
	movq	%rdi, -3168(%rbp)
	movq	-3040(%rbp), %r9
	movq	-3096(%rbp), %rax
	movq	%rdi, -144(%rbp)
	movl	$72, %edi
	movq	-3024(%rbp), %rbx
	movq	%rcx, -3200(%rbp)
	movq	%rsi, -3176(%rbp)
	movq	%rdx, -3376(%rbp)
	movq	%r10, -3248(%rbp)
	movq	%r11, -3216(%rbp)
	movq	%r9, -3240(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%rsi, -160(%rbp)
	movq	%rdx, -152(%rbp)
	movq	%r10, -136(%rbp)
	movq	%r11, -128(%rbp)
	movq	%r9, -120(%rbp)
	movq	%rax, -3152(%rbp)
	movq	%rax, -176(%rbp)
	movq	%rbx, -112(%rbp)
	movq	$0, -2976(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm7
	movq	%r14, %rsi
	movq	-112(%rbp), %rcx
	movdqa	-160(%rbp), %xmm5
	movdqa	-144(%rbp), %xmm6
	leaq	72(%rax), %rdx
	movq	%rax, -2992(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-128(%rbp), %xmm7
	movq	-3184(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rdx, -2976(%rbp)
	movq	%rdx, -2984(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1281
	call	_ZdlPv@PLT
.L1281:
	movq	-3152(%rbp), %xmm0
	movl	$72, %edi
	movq	%rbx, -112(%rbp)
	movq	$0, -2976(%rbp)
	movhps	-3200(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-3176(%rbp), %xmm0
	movhps	-3376(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-3168(%rbp), %xmm0
	movhps	-3248(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-3216(%rbp), %xmm0
	movhps	-3240(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2992(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm6
	movdqa	-160(%rbp), %xmm4
	leaq	72(%rax), %rdx
	movq	%rax, -2992(%rbp)
	movdqa	-144(%rbp), %xmm5
	movq	%rcx, 64(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	leaq	-944(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2976(%rbp)
	movq	%rdx, -2984(%rbp)
	movq	%rax, -3240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1282
	call	_ZdlPv@PLT
.L1282:
	movq	-3312(%rbp), %rcx
	movq	-3232(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1280
	.p2align 4,,10
	.p2align 3
.L1432:
	movq	-3232(%rbp), %rsi
	movq	%r13, %rdi
	movq	$0, -3096(%rbp)
	movq	$0, -3088(%rbp)
	movq	$0, -3080(%rbp)
	movq	$0, -3072(%rbp)
	movq	$0, -3064(%rbp)
	movq	$0, -3056(%rbp)
	movq	$0, -3048(%rbp)
	movq	$0, -3040(%rbp)
	movq	$0, -3024(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3024(%rbp), %rax
	movq	-3184(%rbp), %rdi
	leaq	-3064(%rbp), %r9
	pushq	%rax
	leaq	-3040(%rbp), %rax
	leaq	-3080(%rbp), %rcx
	pushq	%rax
	leaq	-3048(%rbp), %rax
	leaq	-3072(%rbp), %r8
	pushq	%rax
	leaq	-3056(%rbp), %rax
	leaq	-3088(%rbp), %rdx
	pushq	%rax
	leaq	-3096(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverES4_NS0_6ObjectES4_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S5_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_PNSB_IS5_EESF_PNSB_IS9_EESJ_SH_SJ_
	addq	$32, %rsp
	movl	$81, %edx
	movq	%r13, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3024(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	-3064(%rbp), %rdx
	movq	-3096(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler11HasPropertyENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEES7_NS1_21HasPropertyLookupModeE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$84, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	leaq	-176(%rbp), %r12
	movq	%rax, -3168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-96(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	-3048(%rbp), %xmm4
	movq	%r14, %rdi
	movq	-3064(%rbp), %xmm5
	movq	%rdx, -3376(%rbp)
	movq	-3080(%rbp), %xmm6
	movq	-3024(%rbp), %rax
	movq	%rbx, -104(%rbp)
	movq	-3096(%rbp), %xmm7
	movhps	-3040(%rbp), %xmm4
	movhps	-3056(%rbp), %xmm5
	movaps	%xmm0, -2992(%rbp)
	movhps	-3072(%rbp), %xmm6
	movq	%rax, -3176(%rbp)
	movhps	-3088(%rbp), %xmm7
	movq	%rax, -112(%rbp)
	movaps	%xmm4, -3152(%rbp)
	movaps	%xmm5, -3200(%rbp)
	movaps	%xmm6, -3232(%rbp)
	movaps	%xmm7, -3216(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3248(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2992(%rbp), %rdi
	movq	-3376(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L1284
	call	_ZdlPv@PLT
	movq	-3376(%rbp), %rdx
.L1284:
	movq	-3176(%rbp), %xmm0
	movq	%rbx, %xmm6
	movdqa	-3216(%rbp), %xmm7
	movq	%r12, %rsi
	movdqa	-3232(%rbp), %xmm3
	movdqa	-3200(%rbp), %xmm4
	movq	%r14, %rdi
	movq	$0, -2976(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movdqa	-3152(%rbp), %xmm5
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm0, -2992(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1328(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -3152(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1285
	call	_ZdlPv@PLT
.L1285:
	movq	-3256(%rbp), %rcx
	movq	-3336(%rbp), %rdx
	movq	%r13, %rdi
	movq	-3168(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1283
	.p2align 4,,10
	.p2align 3
.L1433:
	movq	-3336(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -2976(%rbp)
	movaps	%xmm0, -2992(%rbp)
	call	_Znwm@PLT
	movl	$1800, %r11d
	movq	-3248(%rbp), %rdi
	movq	%r14, %rsi
	movabsq	$578721378409580295, %rcx
	movw	%r11w, 8(%rax)
	leaq	10(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%rax, -2992(%rbp)
	movq	%rdx, -2976(%rbp)
	movq	%rdx, -2984(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2992(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1287
	call	_ZdlPv@PLT
.L1287:
	movq	(%rbx), %rax
	movq	%r13, %rdi
	leaq	-176(%rbp), %r12
	movq	24(%rax), %rsi
	movq	8(%rax), %rbx
	movq	64(%rax), %rdx
	movq	(%rax), %rcx
	movq	%rsi, -3392(%rbp)
	movq	32(%rax), %rsi
	movq	%rbx, -3216(%rbp)
	movq	16(%rax), %rbx
	movq	%rsi, -3176(%rbp)
	movq	40(%rax), %rsi
	movq	%rbx, -3376(%rbp)
	movq	56(%rax), %rbx
	movq	%rsi, -3408(%rbp)
	movq	48(%rax), %rsi
	movq	72(%rax), %rax
	movq	%rbx, -3232(%rbp)
	leaq	-3040(%rbp), %rbx
	movq	%rsi, -3416(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -3200(%rbp)
	movl	$86, %edx
	movq	%rcx, -3168(%rbp)
	movq	%rax, -3440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-2992(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	movq	-3168(%rbp), %r9
	xorl	%esi, %esi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rdi
	movl	$1, %ecx
	movq	-3176(%rbp), %xmm0
	movq	%rax, -3024(%rbp)
	movq	%rbx, %rdi
	movq	-2976(%rbp), %rax
	pushq	%r12
	movhps	-3200(%rbp), %xmm0
	movq	%rax, -3016(%rbp)
	leaq	-3024(%rbp), %rax
	movq	%rax, %rdx
	movaps	%xmm0, -176(%rbp)
	movq	%rax, -3336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%rbx, %rdi
	movq	%rax, -3456(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$88, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$89, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3232(%rbp), %rbx
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17Cast9ATTheHole_88EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, -80(%rbp)
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%rbx, %xmm6
	movq	-3336(%rbp), %rbx
	movq	-3456(%rbp), %xmm3
	leaq	-64(%rbp), %rdx
	movq	-3416(%rbp), %xmm4
	movq	%rax, -72(%rbp)
	punpcklqdq	%xmm6, %xmm3
	movq	%rbx, %rdi
	movq	-3200(%rbp), %xmm2
	movq	-3168(%rbp), %xmm7
	punpcklqdq	%xmm6, %xmm4
	movaps	%xmm3, -3456(%rbp)
	movq	-3376(%rbp), %xmm6
	movq	-3176(%rbp), %xmm5
	movhps	-3440(%rbp), %xmm2
	movhps	-3216(%rbp), %xmm7
	movaps	%xmm4, -3200(%rbp)
	movhps	-3408(%rbp), %xmm5
	movhps	-3392(%rbp), %xmm6
	movaps	%xmm2, -3440(%rbp)
	movaps	%xmm5, -3408(%rbp)
	movaps	%xmm6, -3376(%rbp)
	movaps	%xmm7, -3168(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -3024(%rbp)
	movq	$0, -3008(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1904(%rbp), %rax
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -3216(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3024(%rbp), %rdi
	popq	%r9
	popq	%r10
	testq	%rdi, %rdi
	je	.L1288
	call	_ZdlPv@PLT
.L1288:
	movq	-3296(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2096(%rbp), %rax
	cmpq	$0, -2984(%rbp)
	movq	%rax, -3176(%rbp)
	jne	.L1445
.L1289:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L1286
	.p2align 4,,10
	.p2align 3
.L1434:
	movq	-3352(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-176(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-163(%rbp), %rdx
	movabsq	$578721378409580295, %rax
	movaps	%xmm0, -2992(%rbp)
	movq	%rax, -176(%rbp)
	movl	$134743816, -168(%rbp)
	movb	$8, -164(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3176(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2992(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1292
	call	_ZdlPv@PLT
.L1292:
	movq	(%rbx), %rax
	leaq	-80(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqu	64(%rax), %xmm0
	movdqu	(%rax), %xmm4
	movdqu	16(%rax), %xmm3
	movdqu	32(%rax), %xmm2
	movdqu	48(%rax), %xmm1
	movdqu	80(%rax), %xmm7
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -2992(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1712(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -3232(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1293
	call	_ZdlPv@PLT
.L1293:
	movq	-3328(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1291
	.p2align 4,,10
	.p2align 3
.L1436:
	movq	-3328(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$12, %edi
	movq	$0, -2976(%rbp)
	movaps	%xmm0, -2992(%rbp)
	call	_Znwm@PLT
	movq	-3232(%rbp), %rdi
	movq	%r14, %rsi
	movabsq	$578721378409580295, %rcx
	movq	%rcx, (%rax)
	leaq	12(%rax), %rdx
	movl	$134743816, 8(%rax)
	movq	%rax, -2992(%rbp)
	movq	%rdx, -2976(%rbp)
	movq	%rdx, -2984(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2992(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1298
	call	_ZdlPv@PLT
.L1298:
	movq	(%rbx), %rax
	movl	$93, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	movq	(%rax), %rcx
	movq	32(%rax), %rbx
	movq	%rcx, -3200(%rbp)
	movq	8(%rax), %rcx
	movq	%rbx, -3328(%rbp)
	movq	48(%rax), %rbx
	movq	%rcx, -3392(%rbp)
	movq	16(%rax), %rcx
	movq	%rbx, -3440(%rbp)
	movq	72(%rax), %rbx
	movq	%rcx, -3296(%rbp)
	movq	24(%rax), %rcx
	movq	%rbx, -3456(%rbp)
	leaq	-3040(%rbp), %rbx
	movq	%rcx, -3408(%rbp)
	movq	40(%rax), %rcx
	movq	%rcx, -3416(%rbp)
	movq	64(%rax), %rcx
	movq	%rcx, -3336(%rbp)
	movq	80(%rax), %rcx
	movq	88(%rax), %rax
	movq	%rcx, -3352(%rbp)
	movq	%rax, -3376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$97, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$98, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$96, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19IsUndefinedConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	je	.L1299
.L1301:
	movq	%r12, %xmm0
	movq	%rbx, %rdi
	movhps	-3376(%rbp), %xmm0
	movaps	%xmm0, -3488(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	xorl	%edx, %edx
	movq	%rax, %rsi
.L1429:
	movq	%r14, %rdi
	leaq	-176(%rbp), %r12
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$4, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-2992(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, -3472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-3488(%rbp), %xmm0
	movl	$7, %edi
	xorl	%esi, %esi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rdi
	movl	$1, %ecx
	pushq	%r12
	movq	-3200(%rbp), %r9
	movq	%rbx, %rdi
	leaq	-3024(%rbp), %rdx
	movq	-3296(%rbp), %xmm1
	movq	%rax, -3024(%rbp)
	movq	-2976(%rbp), %rax
	movaps	%xmm0, -160(%rbp)
	movq	-3352(%rbp), %xmm0
	movhps	-3472(%rbp), %xmm1
	movq	%rax, -3016(%rbp)
	movq	-3328(%rbp), %rax
	movhps	-3336(%rbp), %xmm0
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	popq	%rsi
	popq	%rdi
	movq	%rbx, %rdi
	movq	-3200(%rbp), %xmm4
	movq	%rax, -3488(%rbp)
	movq	-3296(%rbp), %xmm3
	movq	-3328(%rbp), %xmm2
	movq	-3336(%rbp), %xmm1
	movhps	-3392(%rbp), %xmm4
	movq	-3352(%rbp), %xmm0
	movhps	-3408(%rbp), %xmm3
	movhps	-3416(%rbp), %xmm2
	movaps	%xmm4, -3472(%rbp)
	movhps	-3456(%rbp), %xmm1
	movhps	-3376(%rbp), %xmm0
	movaps	%xmm3, -3392(%rbp)
	movaps	%xmm2, -3328(%rbp)
	movaps	%xmm1, -3296(%rbp)
	movaps	%xmm0, -3200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$89, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movdqa	-3328(%rbp), %xmm2
	movq	%r12, %rsi
	movdqa	-3200(%rbp), %xmm0
	movdqa	-3472(%rbp), %xmm4
	movdqa	-3392(%rbp), %xmm3
	leaq	-80(%rbp), %rdx
	movq	%r14, %rdi
	movaps	%xmm2, -144(%rbp)
	movq	-3440(%rbp), %xmm2
	movdqa	-3296(%rbp), %xmm1
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3488(%rbp), %xmm2
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -2992(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3168(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1302
	call	_ZdlPv@PLT
.L1302:
	movq	-3264(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1456(%rbp)
	je	.L1303
.L1437:
	movq	-3264(%rbp), %rsi
	movq	%r13, %rdi
	movabsq	$578721378409580295, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$12, %edi
	movq	$0, -2976(%rbp)
	movaps	%xmm0, -2992(%rbp)
	call	_Znwm@PLT
	movq	-3168(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	12(%rax), %rdx
	movl	$134743816, 8(%rax)
	movq	%rax, -2992(%rbp)
	movq	%rdx, -2976(%rbp)
	movq	%rdx, -2984(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2992(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1304
	call	_ZdlPv@PLT
.L1304:
	movq	(%rbx), %rax
	movq	%r13, %rdi
	movq	8(%rax), %rbx
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	(%rax), %rcx
	movq	%rbx, -3264(%rbp)
	movq	16(%rax), %rbx
	movq	%rsi, -3328(%rbp)
	movq	32(%rax), %rsi
	movq	%rdx, -3352(%rbp)
	movq	56(%rax), %rdx
	movq	48(%rax), %r12
	movq	%rbx, -3296(%rbp)
	movq	64(%rax), %rbx
	movq	72(%rax), %rax
	movq	%rsi, -3336(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -3376(%rbp)
	movl	$88, %edx
	movq	%rcx, -3200(%rbp)
	movq	%rax, -3392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$84, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-176(%rbp), %rsi
	leaq	-96(%rbp), %rdx
	movq	%r14, %rdi
	movq	-3200(%rbp), %xmm0
	movq	$0, -2976(%rbp)
	movhps	-3264(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-3296(%rbp), %xmm0
	movhps	-3328(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-3336(%rbp), %xmm0
	movhps	-3352(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%r12, %xmm0
	movhps	-3376(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%rbx, %xmm0
	movhps	-3392(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2992(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3152(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1305
	call	_ZdlPv@PLT
.L1305:
	movq	-3256(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1303
	.p2align 4,,10
	.p2align 3
.L1435:
	movq	-3296(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-176(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movabsq	$578721378409580295, %rax
	movl	$1800, %r8d
	leaq	-162(%rbp), %rdx
	movaps	%xmm0, -2992(%rbp)
	movq	%rax, -176(%rbp)
	movw	%r8w, -164(%rbp)
	movl	$134743816, -168(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3216(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2992(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1295
	call	_ZdlPv@PLT
.L1295:
	movq	(%rbx), %rax
	movq	8(%rax), %rbx
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	72(%rax), %rdi
	movq	%rbx, -3200(%rbp)
	movq	16(%rax), %rbx
	movq	%rsi, -3336(%rbp)
	movq	32(%rax), %rsi
	movq	%rdx, -3376(%rbp)
	movq	48(%rax), %rdx
	movq	%rdi, -3408(%rbp)
	movq	80(%rax), %rdi
	movq	(%rax), %rcx
	movq	%rbx, -3296(%rbp)
	movq	64(%rax), %rbx
	movq	88(%rax), %rax
	movq	%rsi, -3352(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -3392(%rbp)
	movl	$91, %edx
	movq	%rdi, -3416(%rbp)
	movq	%r13, %rdi
	movq	%rcx, -3168(%rbp)
	movq	%rax, -3440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$89, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-80(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-3168(%rbp), %xmm0
	movq	-3416(%rbp), %rax
	movq	$0, -2976(%rbp)
	movhps	-3200(%rbp), %xmm0
	movq	%rax, %xmm4
	movaps	%xmm0, -176(%rbp)
	movq	-3296(%rbp), %xmm0
	movhps	-3336(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-3352(%rbp), %xmm0
	movhps	-3376(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-3392(%rbp), %xmm0
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%rbx, %xmm0
	movhps	-3408(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%rax, %xmm0
	movhps	-3440(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2992(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1520(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -3168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1296
	call	_ZdlPv@PLT
.L1296:
	movq	-3264(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1294
	.p2align 4,,10
	.p2align 3
.L1438:
	movq	-3256(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -2976(%rbp)
	movaps	%xmm0, -2992(%rbp)
	call	_Znwm@PLT
	movq	-3152(%rbp), %rdi
	movq	%r14, %rsi
	movabsq	$578721378409580295, %rcx
	movq	%rcx, (%rax)
	movl	$1800, %ecx
	leaq	10(%rax), %rdx
	movw	%cx, 8(%rax)
	movq	%rax, -2992(%rbp)
	movq	%rdx, -2976(%rbp)
	movq	%rdx, -2984(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2992(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1307
	call	_ZdlPv@PLT
.L1307:
	movq	(%rbx), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rsi
	movq	8(%rax), %rbx
	movq	56(%rax), %rdx
	movq	(%rax), %rcx
	movq	%rsi, -3296(%rbp)
	movq	40(%rax), %rsi
	movq	%rbx, -3256(%rbp)
	movq	16(%rax), %rbx
	movq	%rsi, -3328(%rbp)
	movq	48(%rax), %rsi
	movq	%rdx, -3352(%rbp)
	movl	$75, %edx
	movq	32(%rax), %r12
	movq	%rsi, -3336(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -3200(%rbp)
	movq	%rbx, -3264(%rbp)
	movq	64(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$72, %edi
	movq	-3200(%rbp), %xmm0
	movq	$0, -2976(%rbp)
	movq	%rbx, -112(%rbp)
	leaq	-1136(%rbp), %rbx
	movhps	-3256(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-3264(%rbp), %xmm0
	movhps	-3296(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%r12, %xmm0
	movhps	-3328(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-3336(%rbp), %xmm0
	movhps	-3352(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2992(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movdqa	-128(%rbp), %xmm6
	leaq	72(%rax), %rdx
	movq	%rax, -2992(%rbp)
	movdqa	-176(%rbp), %xmm3
	movdqa	-160(%rbp), %xmm4
	movq	%rcx, 64(%rax)
	movdqa	-144(%rbp), %xmm5
	movups	%xmm6, 48(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rdx, -2976(%rbp)
	movq	%rdx, -2984(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1308
	call	_ZdlPv@PLT
.L1308:
	movq	-3344(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1072(%rbp)
	je	.L1309
.L1439:
	movq	-3344(%rbp), %rsi
	movq	%r13, %rdi
	movq	$0, -3096(%rbp)
	movq	$0, -3088(%rbp)
	movq	$0, -3080(%rbp)
	movq	$0, -3072(%rbp)
	movq	$0, -3064(%rbp)
	movq	$0, -3056(%rbp)
	movq	$0, -3048(%rbp)
	movq	$0, -3040(%rbp)
	movq	$0, -3024(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3024(%rbp), %rax
	movq	%rbx, %rdi
	leaq	-3080(%rbp), %rcx
	pushq	%rax
	leaq	-3040(%rbp), %rax
	leaq	-3064(%rbp), %r9
	pushq	%rax
	leaq	-3048(%rbp), %rax
	leaq	-3072(%rbp), %r8
	pushq	%rax
	leaq	-3056(%rbp), %rax
	leaq	-3088(%rbp), %rdx
	pushq	%rax
	leaq	-3096(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverES4_NS0_6ObjectES4_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S5_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_PNSB_IS5_EESF_PNSB_IS9_EESJ_SH_SJ_
	addq	$32, %rsp
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3024(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberSubENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$72, %edi
	movq	%r12, -112(%rbp)
	movq	-3048(%rbp), %xmm0
	movq	-3064(%rbp), %xmm1
	movq	-3080(%rbp), %xmm2
	movq	$0, -2976(%rbp)
	movq	-3096(%rbp), %xmm3
	movhps	-3040(%rbp), %xmm0
	movhps	-3056(%rbp), %xmm1
	movhps	-3072(%rbp), %xmm2
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3088(%rbp), %xmm3
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -2992(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm2
	movdqa	-160(%rbp), %xmm3
	leaq	72(%rax), %rdx
	movq	%rax, -2992(%rbp)
	movdqa	-144(%rbp), %xmm4
	movq	-3136(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm2, 48(%rax)
	movq	%rdx, -2976(%rbp)
	movq	%rdx, -2984(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1310
	call	_ZdlPv@PLT
.L1310:
	movq	-3272(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1309
	.p2align 4,,10
	.p2align 3
.L1440:
	movq	-3312(%rbp), %rsi
	movq	%r13, %rdi
	movq	$0, -3104(%rbp)
	leaq	-176(%rbp), %r12
	movq	$0, -3096(%rbp)
	movq	$0, -3088(%rbp)
	movq	$0, -3080(%rbp)
	movq	$0, -3072(%rbp)
	movq	$0, -3064(%rbp)
	movq	$0, -3056(%rbp)
	movq	$0, -3048(%rbp)
	movq	$0, -3040(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3040(%rbp), %rax
	movq	-3240(%rbp), %rdi
	leaq	-3072(%rbp), %r9
	pushq	%rax
	leaq	-3048(%rbp), %rax
	leaq	-3088(%rbp), %rcx
	pushq	%rax
	leaq	-3056(%rbp), %rax
	leaq	-3080(%rbp), %r8
	pushq	%rax
	leaq	-3064(%rbp), %rax
	leaq	-3096(%rbp), %rdx
	pushq	%rax
	leaq	-3104(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverES4_NS0_6ObjectES4_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_S5_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_PNSB_IS5_EESF_PNSB_IS9_EESJ_SH_SJ_
	addq	$32, %rsp
	movl	$109, %edx
	movq	%r13, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$110, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3048(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal17Cast9ATTheHole_88EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3104(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%rax, %r8
	movq	-3048(%rbp), %rax
	movaps	%xmm0, -3024(%rbp)
	movq	%rdx, -176(%rbp)
	movq	-3096(%rbp), %rdx
	movq	%rax, -120(%rbp)
	movq	%rdx, -168(%rbp)
	movq	-3088(%rbp), %rdx
	movq	%rax, -112(%rbp)
	movq	%rdx, -160(%rbp)
	movq	-3080(%rbp), %rdx
	movq	%rax, -104(%rbp)
	leaq	-3024(%rbp), %rax
	movq	%rdx, -152(%rbp)
	movq	-3072(%rbp), %rdx
	movq	%rax, %rdi
	movq	%r8, -96(%rbp)
	movq	%rdx, -144(%rbp)
	movq	-3064(%rbp), %rdx
	movq	%rax, -3336(%rbp)
	movq	%rdx, -136(%rbp)
	movq	-3056(%rbp), %rdx
	movq	$0, -3008(%rbp)
	movq	%rdx, -128(%rbp)
	leaq	-88(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3256(%rbp), %rdi
	movq	-3336(%rbp), %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1312
	call	_ZdlPv@PLT
.L1312:
	movq	-3280(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-752(%rbp), %rax
	cmpq	$0, -2984(%rbp)
	movq	%rax, -3200(%rbp)
	jne	.L1446
.L1313:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L1311
	.p2align 4,,10
	.p2align 3
.L1443:
	movq	-3304(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-176(%rbp), %rsi
	movabsq	$578721378409580295, %rax
	leaq	-167(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, -176(%rbp)
	movaps	%xmm0, -2992(%rbp)
	movb	$8, -168(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3264(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2992(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L1321
	call	_ZdlPv@PLT
.L1321:
	movq	(%r12), %rax
	movq	%r13, %rdi
	movl	$113, %edx
	leaq	.LC2(%rip), %rsi
	movq	64(%rax), %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$114, %edx
	movq	%r13, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L1320
	.p2align 4,,10
	.p2align 3
.L1442:
	movq	-3280(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-176(%rbp), %rsi
	movabsq	$578721378409580295, %rax
	movq	%rax, -176(%rbp)
	movq	%r14, %rdi
	movl	$2056, %eax
	leaq	-165(%rbp), %rdx
	movw	%ax, -168(%rbp)
	movaps	%xmm0, -2992(%rbp)
	movb	$7, -166(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3256(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2992(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L1319
	call	_ZdlPv@PLT
.L1319:
	movq	(%r12), %rax
	movl	$111, %edx
	movq	%r13, %rdi
	leaq	.LC2(%rip), %rsi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movl	$153, %edx
	leaq	.LC7(%rip), %rcx
	movq	%r12, %rsi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L1318
	.p2align 4,,10
	.p2align 3
.L1441:
	movq	-3360(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-176(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$2056, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movabsq	$578721378409580295, %rax
	pxor	%xmm0, %xmm0
	movw	%dx, -168(%rbp)
	leaq	-166(%rbp), %rdx
	movq	%rax, -176(%rbp)
	movaps	%xmm0, -2992(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3200(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1316
	movq	%rax, -3264(%rbp)
	call	_ZdlPv@PLT
	movq	-3264(%rbp), %rax
.L1316:
	movq	(%rax), %rax
	leaq	-104(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqu	48(%rax), %xmm0
	movdqu	(%rax), %xmm3
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	64(%rax), %rax
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -176(%rbp)
	movq	%rax, -112(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -2992(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-368(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -3264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1317
	call	_ZdlPv@PLT
.L1317:
	movq	-3304(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1315
	.p2align 4,,10
	.p2align 3
.L1299:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IsNullConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L1301
	movq	%r12, %xmm0
	movq	%rbx, %rdi
	movhps	-3376(%rbp), %xmm0
	movaps	%xmm0, -3488(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	movq	%rax, %rsi
	jmp	.L1429
	.p2align 4,,10
	.p2align 3
.L1445:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-3168(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movdqa	-3376(%rbp), %xmm6
	movdqa	-3408(%rbp), %xmm7
	movq	-3336(%rbp), %rbx
	leaq	-72(%rbp), %rdx
	movq	%r12, %rsi
	movq	-3232(%rbp), %rax
	movaps	%xmm5, -176(%rbp)
	movdqa	-3200(%rbp), %xmm5
	movaps	%xmm6, -160(%rbp)
	movdqa	-3440(%rbp), %xmm6
	movq	%rbx, %rdi
	movaps	%xmm7, -144(%rbp)
	movdqa	-3456(%rbp), %xmm7
	movq	%rax, -80(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm0, -3024(%rbp)
	movq	$0, -3008(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3176(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1290
	call	_ZdlPv@PLT
.L1290:
	movq	-3352(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1289
	.p2align 4,,10
	.p2align 3
.L1446:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rsi
	movq	-3336(%rbp), %r12
	leaq	-96(%rbp), %rdx
	movq	-3056(%rbp), %xmm0
	movq	-3072(%rbp), %xmm1
	movq	$0, -3008(%rbp)
	movq	-3088(%rbp), %xmm2
	movq	%r12, %rdi
	movq	-3104(%rbp), %xmm3
	movhps	-3048(%rbp), %xmm0
	movhps	-3064(%rbp), %xmm1
	movaps	%xmm0, -128(%rbp)
	movhps	-3080(%rbp), %xmm2
	movq	-3048(%rbp), %xmm0
	movhps	-3096(%rbp), %xmm3
	movaps	%xmm3, -176(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -3024(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3200(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1314
	call	_ZdlPv@PLT
.L1314:
	movq	-3360(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1313
.L1444:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22517:
	.size	_ZN2v88internal41ArrayReduceRightLoopContinuationAssembler44GenerateArrayReduceRightLoopContinuationImplEv, .-_ZN2v88internal41ArrayReduceRightLoopContinuationAssembler44GenerateArrayReduceRightLoopContinuationImplEv
	.section	.rodata._ZN2v88internal8Builtins41Generate_ArrayReduceRightLoopContinuationEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"ArrayReduceRightLoopContinuation"
	.section	.text._ZN2v88internal8Builtins41Generate_ArrayReduceRightLoopContinuationEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins41Generate_ArrayReduceRightLoopContinuationEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins41Generate_ArrayReduceRightLoopContinuationEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins41Generate_ArrayReduceRightLoopContinuationEPNS0_8compiler18CodeAssemblerStateE:
.LFB22513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$956, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$769, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L1451
.L1448:
	movq	%r13, %rdi
	call	_ZN2v88internal41ArrayReduceRightLoopContinuationAssembler44GenerateArrayReduceRightLoopContinuationImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1452
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1451:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L1448
.L1452:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22513:
	.size	_ZN2v88internal8Builtins41Generate_ArrayReduceRightLoopContinuationEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins41Generate_ArrayReduceRightLoopContinuationEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_:
.LFB27263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$13, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$120, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$506100838696486663, %rcx
	movq	%rcx, (%rax)
	leaq	13(%rax), %rdx
	movl	$67569415, 8(%rax)
	movb	$6, 12(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1454
	movq	%rax, -160(%rbp)
	call	_ZdlPv@PLT
	movq	-160(%rbp), %rax
.L1454:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1455
	movq	%rdx, (%r15)
.L1455:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1456
	movq	%rdx, (%r14)
.L1456:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1457
	movq	%rdx, 0(%r13)
.L1457:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1458
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1458:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1459
	movq	%rdx, (%rbx)
.L1459:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1460
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1460:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1461
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1461:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1462
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1462:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1463
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1463:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1464
	movq	-128(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1464:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1465
	movq	-136(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1465:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1466
	movq	-144(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1466:
	movq	96(%rax), %rax
	testq	%rax, %rax
	je	.L1453
	movq	-152(%rbp), %rsi
	movq	%rax, (%rsi)
.L1453:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1512
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1512:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27263:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_SO_SG_SO_SQ_SO_SM_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_SO_SG_SO_SQ_SO_SM_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_SO_SG_SO_SQ_SO_SM_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_SO_SG_SO_SQ_SO_SM_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_SO_SG_SO_SQ_SO_SM_:
.LFB27273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$19, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$168, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	88(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	96(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	104(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	112(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	120(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$1543, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movdqa	.LC9(%rip), %xmm0
	movw	%cx, 16(%rax)
	leaq	19(%rax), %rdx
	movb	$8, 18(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1514
	movq	%rax, -208(%rbp)
	call	_ZdlPv@PLT
	movq	-208(%rbp), %rax
.L1514:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1515
	movq	%rdx, (%r15)
.L1515:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1516
	movq	%rdx, (%r14)
.L1516:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1517
	movq	%rdx, 0(%r13)
.L1517:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1518
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1518:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1519
	movq	%rdx, (%rbx)
.L1519:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1520
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1520:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1521
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1521:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1522
	movq	-112(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1522:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1523
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1523:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1524
	movq	-128(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1524:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1525
	movq	-136(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1525:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1526
	movq	-144(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1526:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1527
	movq	-152(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1527:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1528
	movq	-160(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1528:
	movq	112(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1529
	movq	-168(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1529:
	movq	120(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1530
	movq	-176(%rbp), %rdi
	movq	%rdx, (%rdi)
.L1530:
	movq	128(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1531
	movq	-184(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1531:
	movq	136(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1532
	movq	-192(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1532:
	movq	144(%rax), %rax
	testq	%rax, %rax
	je	.L1513
	movq	-200(%rbp), %rsi
	movq	%rax, (%rsi)
.L1513:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1596
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1596:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27273:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_SO_SG_SO_SQ_SO_SM_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_SO_SG_SO_SQ_SO_SM_
	.section	.rodata._ZN2v88internal23FastArrayReduceRight_26EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_NS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableISC_EEPNSI_ISE_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"../../deps/v8/../../deps/v8/src/builtins/base.tq"
	.section	.text._ZN2v88internal23FastArrayReduceRight_26EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_NS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableISC_EEPNSI_ISE_EE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23FastArrayReduceRight_26EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_NS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableISC_EEPNSI_ISE_EE
	.type	_ZN2v88internal23FastArrayReduceRight_26EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_NS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableISC_EEPNSI_ISE_EE, @function
_ZN2v88internal23FastArrayReduceRight_26EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_NS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableISC_EEPNSI_ISE_EE:
.LFB22545:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$1464, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%rax, -9464(%rbp)
	movq	24(%rbp), %rax
	movq	%r9, %r15
	movq	%rdx, %rbx
	movq	%rdi, -8824(%rbp)
	leaq	-8816(%rbp), %r13
	leaq	-8440(%rbp), %r12
	movq	%rax, -9472(%rbp)
	movq	32(%rbp), %rax
	movq	%r13, %r14
	movq	%rsi, -8848(%rbp)
	movq	%r8, -8864(%rbp)
	movq	%rcx, -8880(%rbp)
	movq	%rax, -9480(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	%rdi, -8816(%rbp)
	movq	%rdi, -8496(%rbp)
	movl	$120, %edi
	movq	$0, -8488(%rbp)
	movq	$0, -8480(%rbp)
	movq	$0, -8472(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -8472(%rbp)
	movq	%rdx, -8480(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -8456(%rbp)
	movq	%rax, -8488(%rbp)
	movq	$0, -8464(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$168, %edi
	movq	$0, -8296(%rbp)
	movq	$0, -8288(%rbp)
	movq	%rax, -8304(%rbp)
	movq	$0, -8280(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -8296(%rbp)
	leaq	-8248(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -8280(%rbp)
	movq	%rdx, -8288(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -8264(%rbp)
	movq	%rax, -9000(%rbp)
	movq	$0, -8272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$192, %edi
	movq	$0, -8104(%rbp)
	movq	$0, -8096(%rbp)
	movq	%rax, -8112(%rbp)
	movq	$0, -8088(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -8104(%rbp)
	leaq	-8056(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -8088(%rbp)
	movq	%rdx, -8096(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -8072(%rbp)
	movq	%rax, -8952(%rbp)
	movq	$0, -8080(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$144, %edi
	movq	$0, -7912(%rbp)
	movq	$0, -7904(%rbp)
	movq	%rax, -7920(%rbp)
	movq	$0, -7896(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -7912(%rbp)
	leaq	-7864(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7896(%rbp)
	movq	%rdx, -7904(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -7880(%rbp)
	movq	%rax, -9032(%rbp)
	movq	$0, -7888(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$168, %edi
	movq	$0, -7720(%rbp)
	movq	$0, -7712(%rbp)
	movq	%rax, -7728(%rbp)
	movq	$0, -7704(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -7720(%rbp)
	leaq	-7672(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7704(%rbp)
	movq	%rdx, -7712(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -7688(%rbp)
	movq	%rax, -8912(%rbp)
	movq	$0, -7696(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$192, %edi
	movq	$0, -7528(%rbp)
	movq	$0, -7520(%rbp)
	movq	%rax, -7536(%rbp)
	movq	$0, -7512(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -7528(%rbp)
	leaq	-7480(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7512(%rbp)
	movq	%rdx, -7520(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -7496(%rbp)
	movq	%rax, -9040(%rbp)
	movq	$0, -7504(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$216, %edi
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	%rax, -7344(%rbp)
	movq	$0, -7320(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -7336(%rbp)
	leaq	-7288(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7320(%rbp)
	movq	%rdx, -7328(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -7304(%rbp)
	movq	%rax, -8944(%rbp)
	movq	$0, -7312(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$168, %edi
	movq	$0, -7144(%rbp)
	movq	$0, -7136(%rbp)
	movq	%rax, -7152(%rbp)
	movq	$0, -7128(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -7144(%rbp)
	leaq	-7096(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7128(%rbp)
	movq	%rdx, -7136(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -7112(%rbp)
	movq	%rax, -8976(%rbp)
	movq	$0, -7120(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$192, %edi
	movq	$0, -6952(%rbp)
	movq	$0, -6944(%rbp)
	movq	%rax, -6960(%rbp)
	movq	$0, -6936(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -6952(%rbp)
	leaq	-6904(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6936(%rbp)
	movq	%rdx, -6944(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6920(%rbp)
	movq	%rax, -8992(%rbp)
	movq	$0, -6928(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$312, %edi
	movq	$0, -6760(%rbp)
	movq	$0, -6752(%rbp)
	movq	%rax, -6768(%rbp)
	movq	$0, -6744(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rax, -6760(%rbp)
	leaq	-6712(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6744(%rbp)
	movq	%rdx, -6752(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6728(%rbp)
	movq	%rax, -9288(%rbp)
	movq	$0, -6736(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$312, %edi
	movq	$0, -6568(%rbp)
	movq	$0, -6560(%rbp)
	movq	%rax, -6576(%rbp)
	movq	$0, -6552(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rax, -6568(%rbp)
	leaq	-6520(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6552(%rbp)
	movq	%rdx, -6560(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6536(%rbp)
	movq	%rax, -9096(%rbp)
	movq	$0, -6544(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$312, %edi
	movq	$0, -6376(%rbp)
	movq	$0, -6368(%rbp)
	movq	%rax, -6384(%rbp)
	movq	$0, -6360(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rax, -6376(%rbp)
	leaq	-6328(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6360(%rbp)
	movq	%rdx, -6368(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6344(%rbp)
	movq	%rax, -9104(%rbp)
	movq	$0, -6352(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$312, %edi
	movq	$0, -6184(%rbp)
	movq	$0, -6176(%rbp)
	movq	%rax, -6192(%rbp)
	movq	$0, -6168(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rax, -6184(%rbp)
	leaq	-6136(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6168(%rbp)
	movq	%rdx, -6176(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6152(%rbp)
	movq	%rax, -9112(%rbp)
	movq	$0, -6160(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$312, %edi
	movq	$0, -5992(%rbp)
	movq	$0, -5984(%rbp)
	movq	%rax, -6000(%rbp)
	movq	$0, -5976(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rax, -5992(%rbp)
	leaq	-5944(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5976(%rbp)
	movq	%rdx, -5984(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5960(%rbp)
	movq	%rax, -9120(%rbp)
	movq	$0, -5968(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$312, %edi
	movq	$0, -5800(%rbp)
	movq	$0, -5792(%rbp)
	movq	%rax, -5808(%rbp)
	movq	$0, -5784(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rax, -5800(%rbp)
	leaq	-5752(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5784(%rbp)
	movq	%rdx, -5792(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5768(%rbp)
	movq	%rax, -9152(%rbp)
	movq	$0, -5776(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$312, %edi
	movq	$0, -5608(%rbp)
	movq	$0, -5600(%rbp)
	movq	%rax, -5616(%rbp)
	movq	$0, -5592(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rax, -5608(%rbp)
	leaq	-5560(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5592(%rbp)
	movq	%rdx, -5600(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5576(%rbp)
	movq	%rax, -9128(%rbp)
	movq	$0, -5584(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$312, %edi
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	%rax, -5424(%rbp)
	movq	$0, -5400(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rax, -5416(%rbp)
	leaq	-5368(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5400(%rbp)
	movq	%rdx, -5408(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5384(%rbp)
	movq	%rax, -8896(%rbp)
	movq	$0, -5392(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$312, %edi
	movq	$0, -5224(%rbp)
	movq	$0, -5216(%rbp)
	movq	%rax, -5232(%rbp)
	movq	$0, -5208(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rax, -5224(%rbp)
	leaq	-5176(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5208(%rbp)
	movq	%rdx, -5216(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5192(%rbp)
	movq	%rax, -9136(%rbp)
	movq	$0, -5200(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$360, %edi
	movq	$0, -5032(%rbp)
	movq	$0, -5024(%rbp)
	movq	%rax, -5040(%rbp)
	movq	$0, -5016(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	360(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 352(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movq	%rax, -5032(%rbp)
	leaq	-4984(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5016(%rbp)
	movq	%rdx, -5024(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5000(%rbp)
	movq	%rax, -9168(%rbp)
	movq	$0, -5008(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$312, %edi
	movq	$0, -4840(%rbp)
	movq	$0, -4832(%rbp)
	movq	%rax, -4848(%rbp)
	movq	$0, -4824(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rax, -4840(%rbp)
	leaq	-4792(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4824(%rbp)
	movq	%rdx, -4832(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4808(%rbp)
	movq	%rax, -9184(%rbp)
	movq	$0, -4816(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$312, %edi
	movq	$0, -4648(%rbp)
	movq	$0, -4640(%rbp)
	movq	%rax, -4656(%rbp)
	movq	$0, -4632(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rax, -4648(%rbp)
	leaq	-4600(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4632(%rbp)
	movq	%rdx, -4640(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4616(%rbp)
	movq	%rax, -9216(%rbp)
	movq	$0, -4624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$384, %edi
	movq	$0, -4456(%rbp)
	movq	$0, -4448(%rbp)
	movq	%rax, -4464(%rbp)
	movq	$0, -4440(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	384(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movq	%rax, -4456(%rbp)
	leaq	-4408(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4440(%rbp)
	movq	%rdx, -4448(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4424(%rbp)
	movq	%rax, -9232(%rbp)
	movq	$0, -4432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$432, %edi
	movq	$0, -4264(%rbp)
	movq	$0, -4256(%rbp)
	movq	%rax, -4272(%rbp)
	movq	$0, -4248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movups	%xmm0, (%rax)
	leaq	432(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movq	%rax, -4264(%rbp)
	leaq	-4216(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4248(%rbp)
	movq	%rdx, -4256(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4232(%rbp)
	movq	$0, -4240(%rbp)
	movq	%rax, -9440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$456, %edi
	movq	$0, -4072(%rbp)
	movq	$0, -4064(%rbp)
	movq	%rax, -4080(%rbp)
	movq	$0, -4056(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movups	%xmm0, (%rax)
	leaq	456(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movq	%rax, -4072(%rbp)
	movq	$0, 448(%rax)
	leaq	-4024(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4056(%rbp)
	movq	%rdx, -4064(%rbp)
	xorl	%edx, %edx
	movq	$0, -4048(%rbp)
	movups	%xmm0, -4040(%rbp)
	movq	%rax, -9248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$384, %edi
	movq	$0, -3880(%rbp)
	movq	$0, -3872(%rbp)
	movq	%rax, -3888(%rbp)
	movq	$0, -3864(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	384(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movq	%rax, -3880(%rbp)
	leaq	-3832(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3864(%rbp)
	movq	%rdx, -3872(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3848(%rbp)
	movq	%rax, -9264(%rbp)
	movq	$0, -3856(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$432, %edi
	movq	$0, -3688(%rbp)
	movq	$0, -3680(%rbp)
	movq	%rax, -3696(%rbp)
	movq	$0, -3672(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movups	%xmm0, (%rax)
	leaq	432(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movq	%rax, -3688(%rbp)
	leaq	-3640(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3672(%rbp)
	movq	%rdx, -3680(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3656(%rbp)
	movq	$0, -3664(%rbp)
	movq	%rax, -9456(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$456, %edi
	movq	$0, -3496(%rbp)
	movq	$0, -3488(%rbp)
	movq	%rax, -3504(%rbp)
	movq	$0, -3480(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movups	%xmm0, (%rax)
	leaq	456(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movq	%rax, -3496(%rbp)
	movq	$0, 448(%rax)
	leaq	-3448(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3480(%rbp)
	movq	%rdx, -3488(%rbp)
	xorl	%edx, %edx
	movq	$0, -3472(%rbp)
	movups	%xmm0, -3464(%rbp)
	movq	%rax, -9424(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$408, %edi
	movq	$0, -3304(%rbp)
	movq	$0, -3296(%rbp)
	movq	%rax, -3312(%rbp)
	movq	$0, -3288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	408(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 400(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movq	%rax, -3304(%rbp)
	leaq	-3256(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3288(%rbp)
	movq	%rdx, -3296(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3272(%rbp)
	movq	%rax, -8928(%rbp)
	movq	$0, -3280(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$312, %edi
	movq	$0, -3112(%rbp)
	movq	$0, -3104(%rbp)
	movq	%rax, -3120(%rbp)
	movq	$0, -3096(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rax, -3112(%rbp)
	leaq	-3064(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3096(%rbp)
	movq	%rdx, -3104(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3080(%rbp)
	movq	%rax, -8960(%rbp)
	movq	$0, -3088(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$336, %edi
	movq	$0, -2920(%rbp)
	movq	$0, -2912(%rbp)
	movq	%rax, -2928(%rbp)
	movq	$0, -2904(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -2920(%rbp)
	leaq	-2872(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2904(%rbp)
	movq	%rdx, -2912(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2888(%rbp)
	movq	%rax, -9280(%rbp)
	movq	$0, -2896(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$384, %edi
	movq	$0, -2728(%rbp)
	movq	$0, -2720(%rbp)
	movq	%rax, -2736(%rbp)
	movq	$0, -2712(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	384(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movq	%rax, -2728(%rbp)
	leaq	-2680(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2712(%rbp)
	movq	%rdx, -2720(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2696(%rbp)
	movq	%rax, -9376(%rbp)
	movq	$0, -2704(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$408, %edi
	movq	$0, -2536(%rbp)
	movq	$0, -2528(%rbp)
	movq	%rax, -2544(%rbp)
	movq	$0, -2520(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	408(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 400(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movq	%rax, -2536(%rbp)
	leaq	-2488(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2520(%rbp)
	movq	%rdx, -2528(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2504(%rbp)
	movq	%rax, -9368(%rbp)
	movq	$0, -2512(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$360, %edi
	movq	$0, -2344(%rbp)
	movq	$0, -2336(%rbp)
	movq	%rax, -2352(%rbp)
	movq	$0, -2328(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	360(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 352(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movq	%rax, -2344(%rbp)
	leaq	-2296(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2328(%rbp)
	movq	%rdx, -2336(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2312(%rbp)
	movq	%rax, -9392(%rbp)
	movq	$0, -2320(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$552, %edi
	movq	$0, -2152(%rbp)
	movq	$0, -2144(%rbp)
	movq	%rax, -2160(%rbp)
	movq	$0, -2136(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	552(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -2152(%rbp)
	movq	%rdx, -2136(%rbp)
	movups	%xmm0, 480(%rax)
	movq	$0, 544(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	leaq	-2104(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2144(%rbp)
	xorl	%edx, %edx
	movq	%rax, -9408(%rbp)
	movq	$0, -2128(%rbp)
	movups	%xmm0, -2120(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$360, %edi
	movq	$0, -1960(%rbp)
	movq	$0, -1952(%rbp)
	movq	%rax, -1968(%rbp)
	movq	$0, -1944(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	360(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 352(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movq	%rax, -1960(%rbp)
	leaq	-1912(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1944(%rbp)
	movq	%rdx, -1952(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1928(%rbp)
	movq	%rax, -9296(%rbp)
	movq	$0, -1936(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$312, %edi
	movq	$0, -1768(%rbp)
	movq	$0, -1760(%rbp)
	movq	%rax, -1776(%rbp)
	movq	$0, -1752(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rax, -1768(%rbp)
	leaq	-1720(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1752(%rbp)
	movq	%rdx, -1760(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1736(%rbp)
	movq	%rax, -9328(%rbp)
	movq	$0, -1744(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$312, %edi
	movq	$0, -1576(%rbp)
	movq	$0, -1568(%rbp)
	movq	%rax, -1584(%rbp)
	movq	$0, -1560(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rax, -1576(%rbp)
	leaq	-1528(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1560(%rbp)
	movq	%rdx, -1568(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1544(%rbp)
	movq	%rax, -9336(%rbp)
	movq	$0, -1552(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$336, %edi
	movq	$0, -1384(%rbp)
	movq	$0, -1376(%rbp)
	movq	%rax, -1392(%rbp)
	movq	$0, -1368(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -1384(%rbp)
	leaq	-1336(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1368(%rbp)
	movq	%rdx, -1376(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1352(%rbp)
	movq	%rax, -9304(%rbp)
	movq	$0, -1360(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$360, %edi
	movq	$0, -1192(%rbp)
	movq	$0, -1184(%rbp)
	movq	%rax, -1200(%rbp)
	movq	$0, -1176(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	360(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 352(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movq	%rax, -1192(%rbp)
	leaq	-1144(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1176(%rbp)
	movq	%rdx, -1184(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1160(%rbp)
	movq	%rax, -9344(%rbp)
	movq	$0, -1168(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$312, %edi
	movq	$0, -1000(%rbp)
	movq	$0, -992(%rbp)
	movq	%rax, -1008(%rbp)
	movq	$0, -984(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rax, -1000(%rbp)
	leaq	-952(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -984(%rbp)
	movq	%rdx, -992(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -968(%rbp)
	movq	%rax, -9352(%rbp)
	movq	$0, -976(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$144, %edi
	movq	$0, -808(%rbp)
	movq	$0, -800(%rbp)
	movq	%rax, -816(%rbp)
	movq	$0, -792(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -808(%rbp)
	leaq	-760(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -792(%rbp)
	movq	%rdx, -800(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -776(%rbp)
	movq	%rax, -9360(%rbp)
	movq	$0, -784(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$48, %edi
	movq	$0, -616(%rbp)
	movq	$0, -608(%rbp)
	movq	%rax, -624(%rbp)
	movq	$0, -600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -616(%rbp)
	leaq	-568(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -600(%rbp)
	movq	%rdx, -608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -584(%rbp)
	movq	%rax, -9160(%rbp)
	movq	$0, -592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8816(%rbp), %rax
	movl	$144, %edi
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	%rax, -432(%rbp)
	movq	$0, -408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -424(%rbp)
	leaq	-376(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -408(%rbp)
	movq	%rdx, -416(%rbp)
	xorl	%edx, %edx
	movq	%rax, -9320(%rbp)
	movups	%xmm0, -392(%rbp)
	movq	$0, -400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8880(%rbp), %r10
	movq	-8864(%rbp), %r11
	pxor	%xmm0, %xmm0
	movq	-8848(%rbp), %rax
	movl	$40, %edi
	movq	%r15, -208(%rbp)
	leaq	-8624(%rbp), %r15
	movq	%r10, -224(%rbp)
	movq	%r11, -216(%rbp)
	movaps	%xmm0, -8624(%rbp)
	movq	%rax, -240(%rbp)
	movq	%rbx, -232(%rbp)
	movq	$0, -8608(%rbp)
	call	_Znwm@PLT
	movq	-208(%rbp), %rcx
	movdqa	-240(%rbp), %xmm7
	movq	%r15, %rsi
	movdqa	-224(%rbp), %xmm6
	leaq	40(%rax), %rdx
	movq	%rax, -8624(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm6, 16(%rax)
	leaq	-8496(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	movq	%rax, -9312(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1598
	call	_ZdlPv@PLT
.L1598:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8112(%rbp), %rax
	cmpq	$0, -8432(%rbp)
	movq	%rax, -9008(%rbp)
	leaq	-8304(%rbp), %rax
	movq	%rax, -8832(%rbp)
	jne	.L2017
.L1599:
	leaq	-7920(%rbp), %rax
	cmpq	$0, -8240(%rbp)
	movq	%rax, -9024(%rbp)
	jne	.L2018
.L1604:
	leaq	-7728(%rbp), %rax
	cmpq	$0, -8048(%rbp)
	movq	%rax, -9000(%rbp)
	jne	.L2019
.L1607:
	leaq	-624(%rbp), %rax
	cmpq	$0, -7856(%rbp)
	movq	%rax, -8848(%rbp)
	jne	.L2020
.L1610:
	leaq	-7344(%rbp), %rax
	cmpq	$0, -7664(%rbp)
	movq	%rax, -9032(%rbp)
	leaq	-7536(%rbp), %rax
	movq	%rax, -8952(%rbp)
	jne	.L2021
.L1613:
	leaq	-7152(%rbp), %rax
	cmpq	$0, -7472(%rbp)
	movq	%rax, -9056(%rbp)
	jne	.L2022
.L1618:
	leaq	-6960(%rbp), %rax
	cmpq	$0, -7280(%rbp)
	movq	%rax, -9040(%rbp)
	jne	.L2023
.L1621:
	cmpq	$0, -7088(%rbp)
	jne	.L2024
.L1624:
	leaq	-6768(%rbp), %rax
	cmpq	$0, -6896(%rbp)
	movq	%rax, -8864(%rbp)
	jne	.L2025
.L1627:
	leaq	-6576(%rbp), %rax
	cmpq	$0, -6704(%rbp)
	movq	%rax, -9064(%rbp)
	leaq	-1584(%rbp), %rax
	movq	%rax, -9088(%rbp)
	jne	.L2026
.L1630:
	leaq	-6384(%rbp), %rax
	cmpq	$0, -6512(%rbp)
	movq	%rax, -9072(%rbp)
	leaq	-6192(%rbp), %rax
	movq	%rax, -9080(%rbp)
	jne	.L2027
.L1633:
	leaq	-5424(%rbp), %rax
	cmpq	$0, -6320(%rbp)
	movq	%rax, -8880(%rbp)
	jne	.L2028
.L1636:
	leaq	-6000(%rbp), %rax
	cmpq	$0, -6128(%rbp)
	movq	%rax, -9096(%rbp)
	leaq	-5808(%rbp), %rax
	movq	%rax, -9104(%rbp)
	jne	.L2029
	cmpq	$0, -5936(%rbp)
	jne	.L2030
.L1641:
	leaq	-5616(%rbp), %rax
	cmpq	$0, -5744(%rbp)
	movq	%rax, -9112(%rbp)
	jne	.L2031
.L1643:
	leaq	-5232(%rbp), %rax
	cmpq	$0, -5552(%rbp)
	movq	%rax, -9120(%rbp)
	jne	.L2032
	cmpq	$0, -5360(%rbp)
	jne	.L2033
.L1647:
	leaq	-5040(%rbp), %rax
	cmpq	$0, -5168(%rbp)
	movq	%rax, -9128(%rbp)
	jne	.L2034
.L1649:
	leaq	-4848(%rbp), %rax
	cmpq	$0, -4976(%rbp)
	movq	%rax, -9136(%rbp)
	leaq	-4656(%rbp), %rax
	movq	%rax, -9152(%rbp)
	jne	.L2035
	cmpq	$0, -4784(%rbp)
	jne	.L2036
.L1655:
	leaq	-4464(%rbp), %rax
	cmpq	$0, -4592(%rbp)
	movq	%rax, -9168(%rbp)
	leaq	-3888(%rbp), %rax
	movq	%rax, -9200(%rbp)
	jne	.L2037
.L1657:
	leaq	-4080(%rbp), %rax
	cmpq	$0, -4400(%rbp)
	movq	%rax, -9184(%rbp)
	leaq	-4272(%rbp), %rax
	movq	%rax, -8976(%rbp)
	jne	.L2038
.L1660:
	leaq	-3120(%rbp), %rax
	cmpq	$0, -4208(%rbp)
	movq	%rax, -8912(%rbp)
	jne	.L2039
.L1665:
	leaq	-3312(%rbp), %rax
	cmpq	$0, -4016(%rbp)
	movq	%rax, -8896(%rbp)
	jne	.L2040
.L1668:
	leaq	-3504(%rbp), %rax
	cmpq	$0, -3824(%rbp)
	movq	%rax, -9216(%rbp)
	leaq	-3696(%rbp), %rax
	movq	%rax, -8992(%rbp)
	jne	.L2041
	cmpq	$0, -3632(%rbp)
	jne	.L2042
.L1675:
	cmpq	$0, -3440(%rbp)
	jne	.L2043
.L1678:
	leaq	-2928(%rbp), %rax
	cmpq	$0, -3248(%rbp)
	movq	%rax, -9232(%rbp)
	jne	.L2044
.L1680:
	leaq	-1776(%rbp), %rax
	cmpq	$0, -3056(%rbp)
	movq	%rax, -8944(%rbp)
	jne	.L2045
.L1683:
	leaq	-2544(%rbp), %rax
	cmpq	$0, -2864(%rbp)
	movq	%rax, -9248(%rbp)
	leaq	-2736(%rbp), %rax
	movq	%rax, -8960(%rbp)
	jne	.L2046
.L1685:
	leaq	-2352(%rbp), %rax
	cmpq	$0, -2672(%rbp)
	movq	%rax, -9264(%rbp)
	jne	.L2047
.L1690:
	leaq	-1968(%rbp), %rax
	cmpq	$0, -2480(%rbp)
	movq	%rax, -8928(%rbp)
	jne	.L2048
.L1693:
	leaq	-2160(%rbp), %rax
	cmpq	$0, -2288(%rbp)
	movq	%rax, -9280(%rbp)
	jne	.L2049
	cmpq	$0, -2096(%rbp)
	jne	.L2050
.L1699:
	cmpq	$0, -1904(%rbp)
	jne	.L2051
.L1705:
	cmpq	$0, -1712(%rbp)
	jne	.L2052
.L1708:
	leaq	-1200(%rbp), %rax
	cmpq	$0, -1520(%rbp)
	movq	%rax, -9288(%rbp)
	jne	.L2053
.L1710:
	leaq	-1008(%rbp), %rax
	cmpq	$0, -1328(%rbp)
	movq	%rax, -9296(%rbp)
	jne	.L2054
.L1714:
	cmpq	$0, -1136(%rbp)
	jne	.L2055
.L1717:
	cmpq	$0, -944(%rbp)
	leaq	-816(%rbp), %r12
	jne	.L2056
.L1719:
	cmpq	$0, -752(%rbp)
	leaq	-432(%rbp), %r13
	jne	.L2057
	cmpq	$0, -560(%rbp)
	jne	.L2058
.L1725:
	movq	-9320(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -8608(%rbp)
	movaps	%xmm0, -8624(%rbp)
	call	_Znwm@PLT
	movl	$2056, %ecx
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$117966599, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -8624(%rbp)
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1727
	call	_ZdlPv@PLT
.L1727:
	movq	(%rbx), %rax
	movq	%r13, %rdi
	movq	40(%rax), %r15
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8848(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9296(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9288(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9304(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1360(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1728
	call	_ZdlPv@PLT
.L1728:
	movq	-1376(%rbp), %rbx
	movq	-1384(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1729
	.p2align 4,,10
	.p2align 3
.L1733:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1730
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1733
.L1731:
	movq	-1384(%rbp), %r12
.L1729:
	testq	%r12, %r12
	je	.L1734
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1734:
	movq	-9088(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8944(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8928(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9280(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9264(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9248(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8960(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9232(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8912(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8896(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9216(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8992(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9200(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9184(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8976(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9168(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9152(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9136(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9128(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9120(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8880(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9112(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9104(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9096(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9080(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9072(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9064(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8864(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9040(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9056(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9032(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8952(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9000(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9024(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9008(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8832(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9312(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2059
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1730:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1733
	jmp	.L1731
	.p2align 4,,10
	.p2align 3
.L2017:
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-240(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-235(%rbp), %rdx
	movaps	%xmm0, -8624(%rbp)
	movl	$117966599, -240(%rbp)
	movb	$8, -236(%rbp)
	movq	$0, -8608(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9312(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1600
	call	_ZdlPv@PLT
.L1600:
	movq	(%rbx), %rax
	movl	$123, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	32(%rax), %r12
	movq	%rcx, -8864(%rbp)
	movq	24(%rax), %rcx
	movq	%rbx, -8848(%rbp)
	movq	16(%rax), %rbx
	movq	%rcx, -8880(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$124, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8824(%rbp), %rdi
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal13Cast5ATSmi_83EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %xmm7
	pxor	%xmm0, %xmm0
	movq	-8848(%rbp), %xmm5
	leaq	-8656(%rbp), %r12
	movq	%rbx, %xmm6
	punpcklqdq	%xmm7, %xmm7
	movq	%r13, %rsi
	movhps	-8880(%rbp), %xmm6
	movhps	-8864(%rbp), %xmm5
	movq	%r12, %rdi
	movq	%rax, -184(%rbp)
	leaq	-176(%rbp), %rdx
	movaps	%xmm7, -9024(%rbp)
	movaps	%xmm6, -8880(%rbp)
	movaps	%xmm5, -8848(%rbp)
	movaps	%xmm5, -240(%rbp)
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm0, -8656(%rbp)
	movq	%rbx, -192(%rbp)
	movq	$0, -8640(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-8112(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -9008(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1601
	call	_ZdlPv@PLT
.L1601:
	movq	-8952(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8304(%rbp), %rax
	cmpq	$0, -8616(%rbp)
	movq	%rax, -8832(%rbp)
	jne	.L2060
.L1602:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L1599
	.p2align 4,,10
	.p2align 3
.L2020:
	movq	-9032(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -8608(%rbp)
	movaps	%xmm0, -8624(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r10d
	movq	-9024(%rbp), %rdi
	movq	%r15, %rsi
	movl	$117966599, (%rax)
	leaq	6(%rax), %rdx
	movw	%r10w, 4(%rax)
	movq	%rax, -8624(%rbp)
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1611
	call	_ZdlPv@PLT
.L1611:
	movq	(%rbx), %rax
	movq	-8824(%rbp), %rdi
	movl	$1, %esi
	movq	16(%rax), %r13
	movq	40(%rax), %rbx
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	-8824(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberSubENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r15, %rdi
	movq	%rax, -8848(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm6
	pxor	%xmm1, %xmm1
	movq	-8848(%rbp), %xmm0
	movl	$16, %edi
	movaps	%xmm1, -8624(%rbp)
	movq	$0, -8608(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -8848(%rbp)
	call	_Znwm@PLT
	movdqa	-8848(%rbp), %xmm0
	movq	%r15, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -8624(%rbp)
	movups	%xmm0, (%rax)
	leaq	-624(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	movq	%rax, -8848(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1612
	call	_ZdlPv@PLT
.L1612:
	movq	-9160(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1610
	.p2align 4,,10
	.p2align 3
.L2019:
	movq	-8952(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-240(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-232(%rbp), %rdx
	movabsq	$434606194611980039, %rax
	movaps	%xmm0, -8624(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -8608(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9008(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1608
	call	_ZdlPv@PLT
.L1608:
	movq	(%rbx), %rax
	leaq	-184(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movdqu	32(%rax), %xmm0
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm1
	movq	56(%rax), %rax
	movq	$0, -8608(%rbp)
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -192(%rbp)
	movaps	%xmm2, -240(%rbp)
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm0, -8624(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-7728(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -9000(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1609
	call	_ZdlPv@PLT
.L1609:
	movq	-8912(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L2018:
	movq	-9000(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-240(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	$2056, %r11d
	leaq	-233(%rbp), %rdx
	movaps	%xmm0, -8624(%rbp)
	movw	%r11w, -236(%rbp)
	movl	$117966599, -240(%rbp)
	movb	$8, -234(%rbp)
	movq	$0, -8608(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8832(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1605
	call	_ZdlPv@PLT
.L1605:
	movq	(%rbx), %rax
	leaq	-192(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm1
	movdqu	32(%rax), %xmm7
	movq	$0, -8608(%rbp)
	movaps	%xmm0, -224(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm1, -240(%rbp)
	movaps	%xmm0, -8624(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-7920(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -9024(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1606
	call	_ZdlPv@PLT
.L1606:
	movq	-9032(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1604
	.p2align 4,,10
	.p2align 3
.L2021:
	movq	-8912(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -8608(%rbp)
	movaps	%xmm0, -8624(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r9d
	movq	-9000(%rbp), %rdi
	movq	%r15, %rsi
	movl	$117966599, (%rax)
	leaq	7(%rax), %rdx
	movw	%r9w, 4(%rax)
	movb	$6, 6(%rax)
	movq	%rax, -8624(%rbp)
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1614
	call	_ZdlPv@PLT
.L1614:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	16(%rax), %rcx
	movq	8(%rax), %r13
	movq	(%rax), %rbx
	movq	48(%rax), %r12
	movq	%rsi, -8880(%rbp)
	movq	32(%rax), %rsi
	movq	%rdx, -8952(%rbp)
	movl	$125, %edx
	movq	%rcx, -8864(%rbp)
	movq	%rsi, -8912(%rbp)
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	-8824(%rbp), %rdi
	call	_ZN2v88internal30Cast20ATFastJSArrayForRead_136EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %xmm6
	movq	%r12, %xmm7
	movq	-8912(%rbp), %xmm4
	punpcklqdq	%xmm6, %xmm7
	movq	%r13, %xmm5
	movq	%rbx, %xmm6
	movq	-8864(%rbp), %xmm3
	leaq	-8656(%rbp), %r12
	leaq	-240(%rbp), %r13
	movhps	-8952(%rbp), %xmm4
	movq	%rax, -176(%rbp)
	punpcklqdq	%xmm5, %xmm6
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movhps	-8880(%rbp), %xmm3
	leaq	-168(%rbp), %rdx
	movaps	%xmm7, -9056(%rbp)
	movaps	%xmm4, -8912(%rbp)
	movaps	%xmm3, -8864(%rbp)
	movaps	%xmm6, -8880(%rbp)
	movaps	%xmm6, -240(%rbp)
	movaps	%xmm3, -224(%rbp)
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm0, -8656(%rbp)
	movq	$0, -8640(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-7344(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -9032(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1615
	call	_ZdlPv@PLT
.L1615:
	movq	-8944(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7536(%rbp), %rax
	cmpq	$0, -8616(%rbp)
	movq	%rax, -8952(%rbp)
	jne	.L2061
.L1616:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L1613
	.p2align 4,,10
	.p2align 3
.L2024:
	movq	-8976(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -8608(%rbp)
	movaps	%xmm0, -8624(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r8d
	movq	-9056(%rbp), %rdi
	movq	%r15, %rsi
	movl	$117966599, (%rax)
	leaq	7(%rax), %rdx
	movw	%r8w, 4(%rax)
	movb	$6, 6(%rax)
	movq	%rax, -8624(%rbp)
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1625
	call	_ZdlPv@PLT
.L1625:
	movq	(%rbx), %rax
	movl	$126, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	movq	16(%rax), %r13
	movq	40(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-8824(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	-8824(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberSubENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r15, %rdi
	movq	%rax, -8864(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm7
	pxor	%xmm1, %xmm1
	movq	-8864(%rbp), %xmm0
	movl	$16, %edi
	movaps	%xmm1, -8624(%rbp)
	movq	$0, -8608(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -8864(%rbp)
	call	_Znwm@PLT
	movdqa	-8864(%rbp), %xmm0
	movq	-8848(%rbp), %rdi
	movq	%r15, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -8624(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1626
	call	_ZdlPv@PLT
.L1626:
	movq	-9160(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1624
	.p2align 4,,10
	.p2align 3
.L2023:
	movq	-8944(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-240(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-231(%rbp), %rdx
	movaps	%xmm0, -8624(%rbp)
	movabsq	$506100838696486663, %rax
	movq	%rax, -240(%rbp)
	movb	$7, -232(%rbp)
	movq	$0, -8608(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9032(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1622
	call	_ZdlPv@PLT
.L1622:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	24(%rax), %rdi
	movq	32(%rax), %rsi
	movq	48(%rax), %rdx
	movq	(%rax), %r10
	movq	8(%rax), %r9
	movq	16(%rax), %r8
	movq	40(%rax), %rcx
	movq	64(%rax), %rax
	movq	%rdi, -216(%rbp)
	movq	%r15, %rdi
	movq	%rsi, -208(%rbp)
	movq	%r13, %rsi
	movq	%rdx, -192(%rbp)
	leaq	-176(%rbp), %rdx
	movq	%r10, -240(%rbp)
	movq	%r9, -232(%rbp)
	movq	%r8, -224(%rbp)
	movq	%rcx, -200(%rbp)
	movq	%rax, -184(%rbp)
	movaps	%xmm0, -8624(%rbp)
	movq	$0, -8608(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-6960(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -9040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1623
	call	_ZdlPv@PLT
.L1623:
	movq	-8992(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1621
	.p2align 4,,10
	.p2align 3
.L2022:
	movq	-9040(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-240(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-232(%rbp), %rdx
	movabsq	$506100838696486663, %rax
	movaps	%xmm0, -8624(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -8608(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8952(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1619
	call	_ZdlPv@PLT
.L1619:
	movq	(%rbx), %rax
	leaq	-184(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movdqu	32(%rax), %xmm0
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm1
	movq	48(%rax), %rax
	movq	$0, -8608(%rbp)
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -192(%rbp)
	movaps	%xmm2, -240(%rbp)
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm0, -8624(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-7152(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -9056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1620
	call	_ZdlPv@PLT
.L1620:
	movq	-8976(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1618
	.p2align 4,,10
	.p2align 3
.L2025:
	movq	-8992(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-240(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-232(%rbp), %rdx
	movabsq	$506100838696486663, %rax
	movaps	%xmm0, -8624(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -8608(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9040(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1628
	call	_ZdlPv@PLT
.L1628:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	24(%rax), %rsi
	movq	8(%rax), %rcx
	movq	40(%rax), %rdx
	movq	(%rax), %rbx
	movq	%rsi, -9072(%rbp)
	movq	32(%rax), %rsi
	movq	%rcx, -8992(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -9080(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	56(%rax), %r13
	movq	%rdx, -9088(%rbp)
	movl	$127, %edx
	movq	%rcx, -9064(%rbp)
	movq	%rbx, -8976(%rbp)
	movq	48(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-8824(%rbp), %r12
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal32NewFastJSArrayForReadWitness_237EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7JSArrayEEE@PLT
	movq	-8616(%rbp), %rdx
	movq	-8600(%rbp), %rax
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -8912(%rbp)
	movq	-8624(%rbp), %rdx
	movq	%rax, -8864(%rbp)
	movq	-8608(%rbp), %rax
	movq	%rdx, -8944(%rbp)
	movl	$130, %edx
	movq	%rax, -8880(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%r12, -8824(%rbp)
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	-8824(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %xmm7
	movq	-8976(%rbp), %xmm0
	movl	$104, %edi
	movq	%r12, -144(%rbp)
	movhps	-8992(%rbp), %xmm0
	movq	$0, -8608(%rbp)
	movaps	%xmm0, -240(%rbp)
	movq	-9064(%rbp), %xmm0
	movhps	-9072(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-9080(%rbp), %xmm0
	movhps	-9088(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	%rbx, %xmm0
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-8944(%rbp), %xmm0
	movhps	-8912(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-8880(%rbp), %xmm0
	movhps	-8864(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8624(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-224(%rbp), %xmm7
	movdqa	-208(%rbp), %xmm5
	movq	-144(%rbp), %rcx
	leaq	104(%rax), %rdx
	movq	%rax, -8624(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-192(%rbp), %xmm6
	movups	%xmm7, 16(%rax)
	movdqa	-176(%rbp), %xmm7
	movups	%xmm5, 32(%rax)
	movdqa	-160(%rbp), %xmm5
	movq	%rcx, 96(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm5, 80(%rax)
	leaq	-6768(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	movq	%rax, -8864(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1629
	call	_ZdlPv@PLT
.L1629:
	movq	-9288(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1627
	.p2align 4,,10
	.p2align 3
.L2026:
	movq	-9288(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -8760(%rbp)
	movq	$0, -8752(%rbp)
	movq	$0, -8744(%rbp)
	movq	$0, -8736(%rbp)
	movq	$0, -8728(%rbp)
	movq	$0, -8720(%rbp)
	movq	$0, -8712(%rbp)
	movq	$0, -8704(%rbp)
	movq	$0, -8696(%rbp)
	movq	$0, -8688(%rbp)
	movq	$0, -8680(%rbp)
	movq	$0, -8672(%rbp)
	movq	$0, -8656(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8656(%rbp), %rax
	movq	-8864(%rbp), %rdi
	leaq	-8744(%rbp), %rcx
	pushq	%rax
	leaq	-8672(%rbp), %rax
	leaq	-8728(%rbp), %r9
	pushq	%rax
	leaq	-8680(%rbp), %rax
	leaq	-8736(%rbp), %r8
	pushq	%rax
	leaq	-8688(%rbp), %rax
	leaq	-8752(%rbp), %rdx
	pushq	%rax
	leaq	-8696(%rbp), %rax
	leaq	-8760(%rbp), %rsi
	pushq	%rax
	leaq	-8704(%rbp), %rax
	pushq	%rax
	leaq	-8712(%rbp), %rax
	pushq	%rax
	leaq	-8720(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_
	movq	-8824(%rbp), %rbx
	addq	$64, %rsp
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8656(%rbp), %r13
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler24IntPtrGreaterThanOrEqualENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-8680(%rbp), %rbx
	pxor	%xmm0, %xmm0
	movq	-8752(%rbp), %rcx
	movq	-8744(%rbp), %rsi
	movq	-8736(%rbp), %rdx
	movq	-8712(%rbp), %r11
	movq	-8704(%rbp), %r10
	movq	%rbx, -8912(%rbp)
	movq	-8688(%rbp), %r8
	movq	-8760(%rbp), %rax
	movq	%rcx, -8992(%rbp)
	movq	-8720(%rbp), %rdi
	movq	-8696(%rbp), %r9
	movq	%rsi, -8976(%rbp)
	movq	-8672(%rbp), %rbx
	movq	-8728(%rbp), %r13
	movq	%rdx, -8944(%rbp)
	movq	%r11, -9520(%rbp)
	movq	%r10, -9504(%rbp)
	movq	%r9, -9200(%rbp)
	movq	%r8, -9088(%rbp)
	movq	%rcx, -232(%rbp)
	movq	%rsi, -224(%rbp)
	movq	%rdx, -216(%rbp)
	movq	%r11, -192(%rbp)
	movq	%r10, -184(%rbp)
	movq	%rax, -9072(%rbp)
	movq	%rdi, -9080(%rbp)
	movq	%rbx, -8880(%rbp)
	movq	-8656(%rbp), %rbx
	movq	%rax, -240(%rbp)
	movq	%rdi, -200(%rbp)
	movl	$104, %edi
	movq	%r13, -208(%rbp)
	movq	%r9, -176(%rbp)
	movq	-8912(%rbp), %rax
	movq	%r8, -168(%rbp)
	movq	%rax, -160(%rbp)
	movq	-8880(%rbp), %rax
	movaps	%xmm0, -8624(%rbp)
	movq	%rax, -152(%rbp)
	movq	%rbx, -144(%rbp)
	movq	$0, -8608(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-224(%rbp), %xmm7
	movdqa	-208(%rbp), %xmm5
	movq	-144(%rbp), %rcx
	leaq	104(%rax), %rdx
	movq	%rax, -8624(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-192(%rbp), %xmm6
	movq	-9064(%rbp), %rdi
	movups	%xmm7, 16(%rax)
	movdqa	-176(%rbp), %xmm7
	movups	%xmm5, 32(%rax)
	movdqa	-160(%rbp), %xmm5
	movq	%rcx, 96(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm5, 80(%rax)
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1631
	call	_ZdlPv@PLT
.L1631:
	movq	-9072(%rbp), %xmm0
	movl	$104, %edi
	movq	%rbx, -144(%rbp)
	movq	$0, -8608(%rbp)
	movhps	-8992(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-8976(%rbp), %xmm0
	movhps	-8944(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	%r13, %xmm0
	movhps	-9080(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-9520(%rbp), %xmm0
	movhps	-9504(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-9200(%rbp), %xmm0
	movhps	-9088(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-8912(%rbp), %xmm0
	movhps	-8880(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8624(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm6
	movdqa	-224(%rbp), %xmm7
	movdqa	-208(%rbp), %xmm5
	leaq	104(%rax), %rdx
	movq	%rax, -8624(%rbp)
	movdqa	-192(%rbp), %xmm4
	movdqa	-176(%rbp), %xmm3
	movq	%rcx, 96(%rax)
	movdqa	-160(%rbp), %xmm2
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm3, 64(%rax)
	movups	%xmm2, 80(%rax)
	leaq	-1584(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	movq	%rax, -9088(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1632
	call	_ZdlPv@PLT
.L1632:
	movq	-9336(%rbp), %rcx
	movq	-9096(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1630
	.p2align 4,,10
	.p2align 3
.L2027:
	movq	-9096(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -8760(%rbp)
	movq	$0, -8752(%rbp)
	movq	$0, -8744(%rbp)
	movq	$0, -8736(%rbp)
	movq	$0, -8728(%rbp)
	movq	$0, -8720(%rbp)
	movq	$0, -8712(%rbp)
	movq	$0, -8704(%rbp)
	movq	$0, -8696(%rbp)
	movq	$0, -8688(%rbp)
	movq	$0, -8680(%rbp)
	movq	$0, -8672(%rbp)
	movq	$0, -8656(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8656(%rbp), %rax
	movq	-9064(%rbp), %rdi
	leaq	-8728(%rbp), %r9
	pushq	%rax
	leaq	-8672(%rbp), %rax
	leaq	-8736(%rbp), %r8
	pushq	%rax
	leaq	-8680(%rbp), %rax
	leaq	-8744(%rbp), %rcx
	pushq	%rax
	leaq	-8688(%rbp), %rax
	leaq	-8752(%rbp), %rdx
	pushq	%rax
	leaq	-8696(%rbp), %rax
	leaq	-8760(%rbp), %rsi
	pushq	%rax
	leaq	-8704(%rbp), %rax
	pushq	%rax
	leaq	-8712(%rbp), %rax
	pushq	%rax
	leaq	-8720(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_
	addq	$64, %rsp
	movl	$3212, %edx
	movq	%r14, %rdi
	leaq	.LC10(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-8824(%rbp), %rbx
	movq	%r15, %rdi
	movq	%rax, %r12
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	movq	-8696(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE(%rip), %esi
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8680(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-8680(%rbp), %rbx
	pxor	%xmm0, %xmm0
	movq	-8752(%rbp), %rcx
	movq	-8744(%rbp), %rsi
	movq	-8736(%rbp), %rdx
	movq	-8712(%rbp), %r11
	movq	-8704(%rbp), %r10
	movq	%rbx, -8880(%rbp)
	movq	-8688(%rbp), %r8
	movq	-8760(%rbp), %rax
	movq	%rcx, -9080(%rbp)
	movq	-8720(%rbp), %rdi
	movq	-8696(%rbp), %r9
	movq	%rsi, -9096(%rbp)
	movq	-8672(%rbp), %rbx
	movq	-8728(%rbp), %r13
	movq	%rdx, -9200(%rbp)
	movq	%r11, -9504(%rbp)
	movq	%r10, -9520(%rbp)
	movq	%r9, -8944(%rbp)
	movq	%r8, -8976(%rbp)
	movq	%rcx, -232(%rbp)
	movq	%rsi, -224(%rbp)
	movq	%rdx, -216(%rbp)
	movq	%r11, -192(%rbp)
	movq	%r10, -184(%rbp)
	movq	%rax, -8992(%rbp)
	movq	%rdi, -9536(%rbp)
	movq	%rbx, -8912(%rbp)
	movq	-8656(%rbp), %rbx
	movq	%rax, -240(%rbp)
	movq	%rdi, -200(%rbp)
	movl	$104, %edi
	movq	%r13, -208(%rbp)
	movq	%r9, -176(%rbp)
	movq	-8880(%rbp), %rax
	movq	%r8, -168(%rbp)
	movq	%rax, -160(%rbp)
	movq	-8912(%rbp), %rax
	movaps	%xmm0, -8624(%rbp)
	movq	%rax, -152(%rbp)
	movq	%rbx, -144(%rbp)
	movq	$0, -8608(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm6
	movdqa	-224(%rbp), %xmm7
	movdqa	-208(%rbp), %xmm5
	leaq	104(%rax), %rdx
	movq	%rax, -8624(%rbp)
	movdqa	-192(%rbp), %xmm4
	movdqa	-176(%rbp), %xmm3
	movq	%rcx, 96(%rax)
	movdqa	-160(%rbp), %xmm2
	movq	-9072(%rbp), %rdi
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm3, 64(%rax)
	movups	%xmm2, 80(%rax)
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1634
	call	_ZdlPv@PLT
.L1634:
	movq	-8992(%rbp), %xmm0
	movl	$104, %edi
	movq	%rbx, -144(%rbp)
	movq	$0, -8608(%rbp)
	movhps	-9080(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-9096(%rbp), %xmm0
	movhps	-9200(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	%r13, %xmm0
	movhps	-9536(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-9504(%rbp), %xmm0
	movhps	-9520(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-8944(%rbp), %xmm0
	movhps	-8976(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-8880(%rbp), %xmm0
	movhps	-8912(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8624(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm1
	movdqa	-224(%rbp), %xmm6
	movdqa	-208(%rbp), %xmm7
	leaq	104(%rax), %rdx
	movq	%rax, -8624(%rbp)
	movdqa	-192(%rbp), %xmm5
	movdqa	-176(%rbp), %xmm4
	movq	%rcx, 96(%rax)
	movdqa	-160(%rbp), %xmm3
	movups	%xmm1, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm3, 80(%rax)
	leaq	-6192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	movq	%rax, -9080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1635
	call	_ZdlPv@PLT
.L1635:
	movq	-9112(%rbp), %rcx
	movq	-9104(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1633
	.p2align 4,,10
	.p2align 3
.L2029:
	movq	-9112(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -8760(%rbp)
	movq	$0, -8752(%rbp)
	movq	$0, -8744(%rbp)
	movq	$0, -8736(%rbp)
	movq	$0, -8728(%rbp)
	movq	$0, -8720(%rbp)
	movq	$0, -8712(%rbp)
	movq	$0, -8704(%rbp)
	movq	$0, -8696(%rbp)
	movq	$0, -8688(%rbp)
	movq	$0, -8680(%rbp)
	movq	$0, -8672(%rbp)
	movq	$0, -8656(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8656(%rbp), %rax
	movq	-9080(%rbp), %rdi
	leaq	-8744(%rbp), %rcx
	pushq	%rax
	leaq	-8672(%rbp), %rax
	leaq	-8728(%rbp), %r9
	pushq	%rax
	leaq	-8680(%rbp), %rax
	leaq	-8736(%rbp), %r8
	pushq	%rax
	leaq	-8688(%rbp), %rax
	leaq	-8752(%rbp), %rdx
	pushq	%rax
	leaq	-8696(%rbp), %rax
	leaq	-8760(%rbp), %rsi
	pushq	%rax
	leaq	-8704(%rbp), %rax
	pushq	%rax
	leaq	-8712(%rbp), %rax
	pushq	%rax
	leaq	-8720(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_
	addq	$64, %rsp
	movl	$3219, %edx
	movq	%r14, %rdi
	leaq	.LC10(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-8824(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler32IsNoElementsProtectorCellInvalidEv@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-8680(%rbp), %r13
	pxor	%xmm0, %xmm0
	movq	-8752(%rbp), %rcx
	movq	-8736(%rbp), %rsi
	movq	-8728(%rbp), %rdx
	movq	-8712(%rbp), %r11
	movq	-8704(%rbp), %r10
	movq	%r13, -8912(%rbp)
	movq	-8688(%rbp), %r8
	movq	-8760(%rbp), %rax
	movq	%rcx, -9104(%rbp)
	movq	-8720(%rbp), %rdi
	movq	-8696(%rbp), %r9
	movq	%rsi, -9112(%rbp)
	movq	-8672(%rbp), %r13
	movq	-8744(%rbp), %rbx
	movq	%rdx, -9504(%rbp)
	movq	%r11, -9200(%rbp)
	movq	%r10, -8992(%rbp)
	movq	%r9, -9520(%rbp)
	movq	%r8, -9536(%rbp)
	movq	%rcx, -232(%rbp)
	movq	%rsi, -216(%rbp)
	movq	%rdx, -208(%rbp)
	movq	%r11, -192(%rbp)
	movq	%r10, -184(%rbp)
	movq	%rax, -8976(%rbp)
	movq	%rdi, -9552(%rbp)
	movq	%r13, -8944(%rbp)
	movq	-8656(%rbp), %r13
	movq	%rax, -240(%rbp)
	movq	%rdi, -200(%rbp)
	movl	$104, %edi
	movq	%rbx, -224(%rbp)
	movq	%r9, -176(%rbp)
	movq	-8912(%rbp), %rax
	movq	%r8, -168(%rbp)
	movq	%rax, -160(%rbp)
	movq	-8944(%rbp), %rax
	movaps	%xmm0, -8624(%rbp)
	movq	%rax, -152(%rbp)
	movq	%r13, -144(%rbp)
	movq	$0, -8608(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm3
	movdqa	-224(%rbp), %xmm2
	movdqa	-208(%rbp), %xmm1
	leaq	104(%rax), %rdx
	movq	%rax, -8624(%rbp)
	movdqa	-192(%rbp), %xmm6
	movdqa	-176(%rbp), %xmm7
	movq	%rcx, 96(%rax)
	movdqa	-160(%rbp), %xmm5
	movq	-9096(%rbp), %rdi
	movups	%xmm3, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm5, 80(%rax)
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1639
	call	_ZdlPv@PLT
.L1639:
	movq	-8976(%rbp), %xmm0
	movl	$104, %edi
	movq	%r13, -144(%rbp)
	movq	$0, -8608(%rbp)
	movhps	-9104(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	%rbx, %xmm0
	movhps	-9112(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-9504(%rbp), %xmm0
	movhps	-9552(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-9200(%rbp), %xmm0
	movhps	-8992(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-9520(%rbp), %xmm0
	movhps	-9536(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-8912(%rbp), %xmm0
	movhps	-8944(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8624(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm4
	movdqa	-224(%rbp), %xmm3
	movdqa	-208(%rbp), %xmm2
	leaq	104(%rax), %rdx
	movq	%rax, -8624(%rbp)
	movdqa	-192(%rbp), %xmm1
	movdqa	-176(%rbp), %xmm6
	movq	%rcx, 96(%rax)
	movdqa	-160(%rbp), %xmm7
	movups	%xmm4, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm1, 48(%rax)
	movups	%xmm6, 64(%rax)
	movups	%xmm7, 80(%rax)
	leaq	-5808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	movq	%rax, -9104(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1640
	call	_ZdlPv@PLT
.L1640:
	movq	-9152(%rbp), %rcx
	movq	-9120(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -5936(%rbp)
	je	.L1641
.L2030:
	movq	-9120(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -8760(%rbp)
	movq	$0, -8752(%rbp)
	movq	$0, -8744(%rbp)
	movq	$0, -8736(%rbp)
	movq	$0, -8728(%rbp)
	movq	$0, -8720(%rbp)
	movq	$0, -8712(%rbp)
	movq	$0, -8704(%rbp)
	movq	$0, -8696(%rbp)
	movq	$0, -8688(%rbp)
	movq	$0, -8680(%rbp)
	movq	$0, -8672(%rbp)
	movq	$0, -8656(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8656(%rbp), %rax
	movq	-9096(%rbp), %rdi
	leaq	-8744(%rbp), %rcx
	pushq	%rax
	leaq	-8672(%rbp), %rax
	leaq	-8752(%rbp), %rdx
	pushq	%rax
	leaq	-8680(%rbp), %rax
	leaq	-8760(%rbp), %rsi
	pushq	%rax
	leaq	-8688(%rbp), %rax
	leaq	-8728(%rbp), %r9
	pushq	%rax
	leaq	-8696(%rbp), %rax
	leaq	-8736(%rbp), %r8
	pushq	%rax
	leaq	-8704(%rbp), %rax
	pushq	%rax
	leaq	-8712(%rbp), %rax
	pushq	%rax
	leaq	-8720(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_
	movq	-8760(%rbp), %rax
	pxor	%xmm0, %xmm0
	addq	$64, %rsp
	movl	$104, %edi
	movaps	%xmm0, -8624(%rbp)
	movq	%rax, -240(%rbp)
	movq	-8752(%rbp), %rax
	movq	$0, -8608(%rbp)
	movq	%rax, -232(%rbp)
	movq	-8744(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-8736(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-8728(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-8720(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-8712(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-8704(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-8696(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-8688(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-8680(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-8672(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-8656(%rbp), %rax
	movq	%rax, -144(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm5
	movdqa	-224(%rbp), %xmm4
	movdqa	-208(%rbp), %xmm3
	leaq	104(%rax), %rdx
	movq	%rax, -8624(%rbp)
	movdqa	-192(%rbp), %xmm2
	movdqa	-176(%rbp), %xmm1
	movq	%rcx, 96(%rax)
	movdqa	-160(%rbp), %xmm6
	movq	-8880(%rbp), %rdi
	movups	%xmm5, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm1, 64(%rax)
	movups	%xmm6, 80(%rax)
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1642
	call	_ZdlPv@PLT
.L1642:
	movq	-8896(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1641
	.p2align 4,,10
	.p2align 3
.L2028:
	movq	-9104(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -8760(%rbp)
	movq	$0, -8752(%rbp)
	movq	$0, -8744(%rbp)
	movq	$0, -8736(%rbp)
	movq	$0, -8728(%rbp)
	movq	$0, -8720(%rbp)
	movq	$0, -8712(%rbp)
	movq	$0, -8704(%rbp)
	movq	$0, -8696(%rbp)
	movq	$0, -8688(%rbp)
	movq	$0, -8680(%rbp)
	movq	$0, -8672(%rbp)
	movq	$0, -8656(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8656(%rbp), %rax
	movq	-9072(%rbp), %rdi
	leaq	-8744(%rbp), %rcx
	pushq	%rax
	leaq	-8672(%rbp), %rax
	leaq	-8752(%rbp), %rdx
	pushq	%rax
	leaq	-8680(%rbp), %rax
	leaq	-8760(%rbp), %rsi
	pushq	%rax
	leaq	-8688(%rbp), %rax
	leaq	-8728(%rbp), %r9
	pushq	%rax
	leaq	-8696(%rbp), %rax
	leaq	-8736(%rbp), %r8
	pushq	%rax
	leaq	-8704(%rbp), %rax
	pushq	%rax
	leaq	-8712(%rbp), %rax
	pushq	%rax
	leaq	-8720(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_
	movq	-8760(%rbp), %rax
	pxor	%xmm0, %xmm0
	addq	$64, %rsp
	movl	$104, %edi
	movaps	%xmm0, -8624(%rbp)
	movq	%rax, -240(%rbp)
	movq	-8752(%rbp), %rax
	movq	$0, -8608(%rbp)
	movq	%rax, -232(%rbp)
	movq	-8744(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-8736(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-8728(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-8720(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-8712(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-8704(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-8696(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-8688(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-8680(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-8672(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-8656(%rbp), %rax
	movq	%rax, -144(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm2
	movdqa	-224(%rbp), %xmm1
	movdqa	-208(%rbp), %xmm6
	leaq	104(%rax), %rdx
	movq	%rax, -8624(%rbp)
	movdqa	-192(%rbp), %xmm7
	movdqa	-176(%rbp), %xmm5
	movq	%rcx, 96(%rax)
	movdqa	-160(%rbp), %xmm4
	movq	-8880(%rbp), %rdi
	movups	%xmm2, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm5, 64(%rax)
	movups	%xmm4, 80(%rax)
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1637
	call	_ZdlPv@PLT
.L1637:
	movq	-8896(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1636
	.p2align 4,,10
	.p2align 3
.L2031:
	movq	-9152(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -8760(%rbp)
	movq	$0, -8752(%rbp)
	movq	$0, -8744(%rbp)
	movq	$0, -8736(%rbp)
	movq	$0, -8728(%rbp)
	movq	$0, -8720(%rbp)
	movq	$0, -8712(%rbp)
	movq	$0, -8704(%rbp)
	movq	$0, -8696(%rbp)
	movq	$0, -8688(%rbp)
	movq	$0, -8680(%rbp)
	movq	$0, -8672(%rbp)
	movq	$0, -8656(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8656(%rbp), %rax
	movq	-9104(%rbp), %rdi
	leaq	-8744(%rbp), %rcx
	pushq	%rax
	leaq	-8672(%rbp), %rax
	leaq	-8728(%rbp), %r9
	pushq	%rax
	leaq	-8680(%rbp), %rax
	leaq	-8736(%rbp), %r8
	pushq	%rax
	leaq	-8688(%rbp), %rax
	leaq	-8752(%rbp), %rdx
	pushq	%rax
	leaq	-8696(%rbp), %rax
	leaq	-8760(%rbp), %rsi
	pushq	%rax
	leaq	-8704(%rbp), %rax
	pushq	%rax
	leaq	-8712(%rbp), %rax
	pushq	%rax
	leaq	-8720(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_
	addq	$64, %rsp
	movl	$3220, %edx
	movq	%r14, %rdi
	leaq	.LC10(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$131, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	movq	-8696(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-8696(%rbp), %rax
	movl	$104, %edi
	movq	-8712(%rbp), %xmm0
	movq	-8728(%rbp), %xmm1
	movq	%rbx, -168(%rbp)
	movq	%rax, -176(%rbp)
	movq	-8680(%rbp), %rax
	movhps	-8704(%rbp), %xmm0
	movq	-8744(%rbp), %xmm2
	movhps	-8720(%rbp), %xmm1
	movq	-8760(%rbp), %xmm3
	movaps	%xmm0, -192(%rbp)
	movq	%rax, -160(%rbp)
	pxor	%xmm0, %xmm0
	movq	-8672(%rbp), %rax
	movhps	-8736(%rbp), %xmm2
	movhps	-8752(%rbp), %xmm3
	movaps	%xmm1, -208(%rbp)
	movq	%rax, -152(%rbp)
	movq	-8656(%rbp), %rax
	movaps	%xmm3, -240(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm0, -8624(%rbp)
	movq	%rax, -144(%rbp)
	movq	$0, -8608(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm7
	movdqa	-224(%rbp), %xmm5
	movdqa	-208(%rbp), %xmm4
	leaq	104(%rax), %rdx
	movq	%rax, -8624(%rbp)
	movdqa	-192(%rbp), %xmm3
	movdqa	-176(%rbp), %xmm2
	movq	%rcx, 96(%rax)
	movdqa	-160(%rbp), %xmm1
	movq	-9112(%rbp), %rdi
	movups	%xmm7, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm2, 64(%rax)
	movups	%xmm1, 80(%rax)
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1644
	call	_ZdlPv@PLT
.L1644:
	movq	-9128(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1643
	.p2align 4,,10
	.p2align 3
.L2032:
	movq	-9128(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -8760(%rbp)
	movq	$0, -8752(%rbp)
	movq	$0, -8744(%rbp)
	movq	$0, -8736(%rbp)
	movq	$0, -8728(%rbp)
	movq	$0, -8720(%rbp)
	movq	$0, -8712(%rbp)
	movq	$0, -8704(%rbp)
	movq	$0, -8696(%rbp)
	movq	$0, -8688(%rbp)
	movq	$0, -8680(%rbp)
	movq	$0, -8672(%rbp)
	movq	$0, -8656(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8656(%rbp), %rax
	movq	-9112(%rbp), %rdi
	leaq	-8744(%rbp), %rcx
	pushq	%rax
	leaq	-8672(%rbp), %rax
	leaq	-8752(%rbp), %rdx
	pushq	%rax
	leaq	-8680(%rbp), %rax
	leaq	-8760(%rbp), %rsi
	pushq	%rax
	leaq	-8688(%rbp), %rax
	leaq	-8728(%rbp), %r9
	pushq	%rax
	leaq	-8696(%rbp), %rax
	leaq	-8736(%rbp), %r8
	pushq	%rax
	leaq	-8704(%rbp), %rax
	pushq	%rax
	leaq	-8712(%rbp), %rax
	pushq	%rax
	leaq	-8720(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_
	movq	-8760(%rbp), %rax
	pxor	%xmm0, %xmm0
	addq	$64, %rsp
	movl	$104, %edi
	movaps	%xmm0, -8624(%rbp)
	movq	%rax, -240(%rbp)
	movq	-8752(%rbp), %rax
	movq	$0, -8608(%rbp)
	movq	%rax, -232(%rbp)
	movq	-8744(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-8736(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-8728(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-8720(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-8712(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-8704(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-8696(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-8688(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-8680(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-8672(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-8656(%rbp), %rax
	movq	%rax, -144(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm6
	movdqa	-224(%rbp), %xmm7
	movdqa	-208(%rbp), %xmm5
	leaq	104(%rax), %rdx
	movq	%rax, -8624(%rbp)
	movdqa	-192(%rbp), %xmm4
	movdqa	-176(%rbp), %xmm3
	movq	%rcx, 96(%rax)
	movdqa	-160(%rbp), %xmm2
	movq	-9120(%rbp), %rdi
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm3, 64(%rax)
	movups	%xmm2, 80(%rax)
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1646
	call	_ZdlPv@PLT
.L1646:
	movq	-9136(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -5360(%rbp)
	je	.L1647
.L2033:
	movq	-8896(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -8760(%rbp)
	movq	$0, -8752(%rbp)
	movq	$0, -8744(%rbp)
	movq	$0, -8736(%rbp)
	movq	$0, -8728(%rbp)
	movq	$0, -8720(%rbp)
	movq	$0, -8712(%rbp)
	movq	$0, -8704(%rbp)
	movq	$0, -8696(%rbp)
	movq	$0, -8688(%rbp)
	movq	$0, -8680(%rbp)
	movq	$0, -8672(%rbp)
	movq	$0, -8656(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8656(%rbp), %rax
	movq	-8880(%rbp), %rdi
	leaq	-8744(%rbp), %rcx
	pushq	%rax
	leaq	-8672(%rbp), %rax
	leaq	-8752(%rbp), %rdx
	pushq	%rax
	leaq	-8680(%rbp), %rax
	leaq	-8760(%rbp), %rsi
	pushq	%rax
	leaq	-8688(%rbp), %rax
	leaq	-8728(%rbp), %r9
	pushq	%rax
	leaq	-8696(%rbp), %rax
	leaq	-8736(%rbp), %r8
	pushq	%rax
	leaq	-8704(%rbp), %rax
	pushq	%rax
	leaq	-8712(%rbp), %rax
	pushq	%rax
	leaq	-8720(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_
	pxor	%xmm1, %xmm1
	addq	$64, %rsp
	movq	-8656(%rbp), %xmm0
	movl	$16, %edi
	movaps	%xmm1, -8624(%rbp)
	movhps	-8720(%rbp), %xmm0
	movq	$0, -8608(%rbp)
	movaps	%xmm0, -8896(%rbp)
	call	_Znwm@PLT
	movdqa	-8896(%rbp), %xmm0
	movq	-8848(%rbp), %rdi
	movq	%r15, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -8624(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1648
	call	_ZdlPv@PLT
.L1648:
	movq	-9160(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1647
	.p2align 4,,10
	.p2align 3
.L2035:
	movq	-9168(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1542, %edi
	pxor	%xmm0, %xmm0
	movabsq	$506100838696486663, %rax
	movw	%di, -228(%rbp)
	leaq	-240(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-225(%rbp), %rdx
	movq	%rax, -240(%rbp)
	movaps	%xmm0, -8624(%rbp)
	movl	$67569415, -232(%rbp)
	movb	$7, -226(%rbp)
	movq	$0, -8608(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9128(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1652
	call	_ZdlPv@PLT
.L1652:
	movq	(%rbx), %rax
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	8(%rax), %rcx
	movq	72(%rax), %r11
	movq	88(%rax), %r10
	movq	%rsi, -8976(%rbp)
	movq	32(%rax), %rsi
	movq	(%rax), %rbx
	movq	%rdx, -9136(%rbp)
	movq	%rdi, -9168(%rbp)
	movq	48(%rax), %rdx
	movq	64(%rax), %rdi
	movq	%rcx, -8944(%rbp)
	movq	%r11, -9504(%rbp)
	movq	16(%rax), %rcx
	movq	80(%rax), %r11
	movq	%rsi, -8992(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -9152(%rbp)
	movl	$134, %edx
	movq	104(%rax), %r12
	movq	%rdi, -9200(%rbp)
	movq	%r14, %rdi
	movq	112(%rax), %r13
	movq	%r11, -9520(%rbp)
	movq	%rcx, -8896(%rbp)
	movq	%r10, -9536(%rbp)
	movq	%rbx, -8912(%rbp)
	movq	96(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-8824(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, -9552(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-9552(%rbp), %r10
	movq	%r15, %rdi
	movq	%rax, %rdx
	movq	%r10, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE(%rip), %esi
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-8824(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r13, %rdx
	call	_ZN2v88internal29NumberIsGreaterThanOrEqual_78EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	pxor	%xmm0, %xmm0
	movq	-9520(%rbp), %xmm5
	movq	-9200(%rbp), %xmm2
	movq	-9152(%rbp), %xmm7
	movl	$104, %edi
	movq	%rax, %r12
	movq	-8992(%rbp), %xmm4
	movq	-8896(%rbp), %xmm3
	movhps	-9536(%rbp), %xmm5
	movq	-8912(%rbp), %xmm6
	movhps	-9504(%rbp), %xmm2
	movhps	-9168(%rbp), %xmm7
	movhps	-9136(%rbp), %xmm4
	movaps	%xmm5, -9520(%rbp)
	movhps	-8976(%rbp), %xmm3
	movhps	-8944(%rbp), %xmm6
	movaps	%xmm2, -9200(%rbp)
	movaps	%xmm7, -9152(%rbp)
	movaps	%xmm4, -8992(%rbp)
	movaps	%xmm3, -8896(%rbp)
	movaps	%xmm6, -8912(%rbp)
	movaps	%xmm6, -240(%rbp)
	movaps	%xmm3, -224(%rbp)
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm0, -8624(%rbp)
	movq	%rbx, -144(%rbp)
	movq	$0, -8608(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm1
	movdqa	-224(%rbp), %xmm6
	movdqa	-208(%rbp), %xmm7
	leaq	104(%rax), %rdx
	movq	%rax, -8624(%rbp)
	movdqa	-192(%rbp), %xmm5
	movdqa	-176(%rbp), %xmm4
	movq	%rcx, 96(%rax)
	movdqa	-160(%rbp), %xmm3
	movups	%xmm1, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm3, 80(%rax)
	leaq	-4848(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	movq	%rax, -9136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1653
	call	_ZdlPv@PLT
.L1653:
	movdqa	-8912(%rbp), %xmm2
	movdqa	-8896(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movl	$104, %edi
	movdqa	-8992(%rbp), %xmm6
	movdqa	-9152(%rbp), %xmm7
	movaps	%xmm0, -8624(%rbp)
	movdqa	-9200(%rbp), %xmm5
	movdqa	-9520(%rbp), %xmm4
	movaps	%xmm2, -240(%rbp)
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm6, -208(%rbp)
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movq	%rbx, -144(%rbp)
	movq	$0, -8608(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm3
	movdqa	-224(%rbp), %xmm2
	movdqa	-208(%rbp), %xmm1
	leaq	104(%rax), %rdx
	movq	%rax, -8624(%rbp)
	movdqa	-192(%rbp), %xmm6
	movdqa	-176(%rbp), %xmm7
	movq	%rcx, 96(%rax)
	movdqa	-160(%rbp), %xmm5
	movups	%xmm3, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm5, 80(%rax)
	leaq	-4656(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	movq	%rax, -9152(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1654
	call	_ZdlPv@PLT
.L1654:
	movq	-9216(%rbp), %rcx
	movq	-9184(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -4784(%rbp)
	je	.L1655
.L2036:
	movq	-9184(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -8760(%rbp)
	movq	$0, -8752(%rbp)
	movq	$0, -8744(%rbp)
	movq	$0, -8736(%rbp)
	movq	$0, -8728(%rbp)
	movq	$0, -8720(%rbp)
	movq	$0, -8712(%rbp)
	movq	$0, -8704(%rbp)
	movq	$0, -8696(%rbp)
	movq	$0, -8688(%rbp)
	movq	$0, -8680(%rbp)
	movq	$0, -8672(%rbp)
	movq	$0, -8656(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8656(%rbp), %rax
	movq	-9136(%rbp), %rdi
	leaq	-8744(%rbp), %rcx
	pushq	%rax
	leaq	-8672(%rbp), %rax
	leaq	-8752(%rbp), %rdx
	pushq	%rax
	leaq	-8680(%rbp), %rax
	leaq	-8760(%rbp), %rsi
	pushq	%rax
	leaq	-8688(%rbp), %rax
	leaq	-8728(%rbp), %r9
	pushq	%rax
	leaq	-8696(%rbp), %rax
	leaq	-8736(%rbp), %r8
	pushq	%rax
	leaq	-8704(%rbp), %rax
	pushq	%rax
	leaq	-8712(%rbp), %rax
	pushq	%rax
	leaq	-8720(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_
	pxor	%xmm1, %xmm1
	addq	$64, %rsp
	movq	-8656(%rbp), %xmm0
	movl	$16, %edi
	movaps	%xmm1, -8624(%rbp)
	movhps	-8720(%rbp), %xmm0
	movq	$0, -8608(%rbp)
	movaps	%xmm0, -8896(%rbp)
	call	_Znwm@PLT
	movdqa	-8896(%rbp), %xmm0
	movq	-8848(%rbp), %rdi
	movq	%r15, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -8624(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1656
	call	_ZdlPv@PLT
.L1656:
	movq	-9160(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1655
	.p2align 4,,10
	.p2align 3
.L2034:
	movq	-9136(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -8760(%rbp)
	movq	$0, -8752(%rbp)
	movq	$0, -8744(%rbp)
	movq	$0, -8736(%rbp)
	movq	$0, -8728(%rbp)
	movq	$0, -8720(%rbp)
	movq	$0, -8712(%rbp)
	movq	$0, -8704(%rbp)
	movq	$0, -8696(%rbp)
	movq	$0, -8688(%rbp)
	movq	$0, -8680(%rbp)
	movq	$0, -8672(%rbp)
	movq	$0, -8656(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8656(%rbp), %rax
	movq	-9120(%rbp), %rdi
	leaq	-8728(%rbp), %r9
	pushq	%rax
	leaq	-8672(%rbp), %rax
	leaq	-8736(%rbp), %r8
	pushq	%rax
	leaq	-8680(%rbp), %rax
	leaq	-8744(%rbp), %rcx
	pushq	%rax
	leaq	-8688(%rbp), %rax
	leaq	-8752(%rbp), %rdx
	pushq	%rax
	leaq	-8696(%rbp), %rax
	leaq	-8760(%rbp), %rsi
	pushq	%rax
	leaq	-8704(%rbp), %rax
	pushq	%rax
	leaq	-8712(%rbp), %rax
	pushq	%rax
	leaq	-8720(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_
	addq	$64, %rsp
	movl	$134, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$3208, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-8760(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	-8656(%rbp), %rdx
	movq	-8688(%rbp), %rax
	leaq	-240(%rbp), %rsi
	movaps	%xmm0, -8624(%rbp)
	movq	%rcx, -240(%rbp)
	movq	-8752(%rbp), %rcx
	movq	%rdx, -144(%rbp)
	movq	%rcx, -232(%rbp)
	movq	-8744(%rbp), %rcx
	movq	%rdx, -136(%rbp)
	leaq	-120(%rbp), %rdx
	movq	%rcx, -224(%rbp)
	movq	-8736(%rbp), %rcx
	movq	%rax, -168(%rbp)
	movq	%rcx, -216(%rbp)
	movq	-8728(%rbp), %rcx
	movq	%rax, -128(%rbp)
	movq	%rcx, -208(%rbp)
	movq	-8720(%rbp), %rcx
	movq	$0, -8608(%rbp)
	movq	%rcx, -200(%rbp)
	movq	-8712(%rbp), %rcx
	movq	%rcx, -192(%rbp)
	movq	-8704(%rbp), %rcx
	movq	%rcx, -184(%rbp)
	movq	-8696(%rbp), %rcx
	movq	%rcx, -176(%rbp)
	movq	-8680(%rbp), %rcx
	movq	%rcx, -160(%rbp)
	movq	-8672(%rbp), %rcx
	movq	%rcx, -152(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9128(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1650
	call	_ZdlPv@PLT
.L1650:
	movq	-9168(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1649
	.p2align 4,,10
	.p2align 3
.L2037:
	movq	-9216(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -8760(%rbp)
	leaq	-240(%rbp), %r13
	movq	$0, -8752(%rbp)
	leaq	-112(%rbp), %rbx
	movq	$0, -8744(%rbp)
	movq	$0, -8736(%rbp)
	movq	$0, -8728(%rbp)
	movq	$0, -8720(%rbp)
	movq	$0, -8712(%rbp)
	movq	$0, -8704(%rbp)
	movq	$0, -8696(%rbp)
	movq	$0, -8688(%rbp)
	movq	$0, -8680(%rbp)
	movq	$0, -8672(%rbp)
	movq	$0, -8656(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8656(%rbp), %rax
	movq	-9152(%rbp), %rdi
	leaq	-8744(%rbp), %rcx
	pushq	%rax
	leaq	-8672(%rbp), %rax
	leaq	-8728(%rbp), %r9
	pushq	%rax
	leaq	-8680(%rbp), %rax
	leaq	-8736(%rbp), %r8
	pushq	%rax
	leaq	-8688(%rbp), %rax
	leaq	-8752(%rbp), %rdx
	pushq	%rax
	leaq	-8696(%rbp), %rax
	leaq	-8760(%rbp), %rsi
	pushq	%rax
	leaq	-8704(%rbp), %rax
	pushq	%rax
	leaq	-8712(%rbp), %rax
	pushq	%rax
	leaq	-8720(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_
	addq	$64, %rsp
	movl	$136, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$3225, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	-8680(%rbp), %xmm1
	movq	-8672(%rbp), %r12
	movq	-8760(%rbp), %xmm0
	movq	-8760(%rbp), %xmm5
	movq	-8656(%rbp), %xmm2
	movq	%r12, %xmm4
	movq	-8696(%rbp), %xmm7
	punpcklqdq	%xmm4, %xmm1
	movhps	-8752(%rbp), %xmm0
	movq	-8712(%rbp), %xmm4
	movq	-8728(%rbp), %xmm3
	movq	-8744(%rbp), %xmm6
	movhps	-8656(%rbp), %xmm5
	punpcklqdq	%xmm2, %xmm2
	movhps	-8688(%rbp), %xmm7
	movhps	-8704(%rbp), %xmm4
	movhps	-8720(%rbp), %xmm3
	movaps	%xmm0, -9216(%rbp)
	movhps	-8736(%rbp), %xmm6
	movaps	%xmm0, -240(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -8976(%rbp)
	movaps	%xmm2, -9184(%rbp)
	movaps	%xmm1, -8912(%rbp)
	movaps	%xmm7, -8992(%rbp)
	movaps	%xmm4, -9200(%rbp)
	movaps	%xmm3, -8944(%rbp)
	movaps	%xmm6, -8896(%rbp)
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm0, -8624(%rbp)
	movq	$0, -8608(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9168(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1658
	call	_ZdlPv@PLT
.L1658:
	movdqa	-9216(%rbp), %xmm3
	movdqa	-8896(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdx
	movdqa	-8944(%rbp), %xmm1
	movdqa	-9200(%rbp), %xmm6
	movq	%r13, %rsi
	movq	%r15, %rdi
	movdqa	-8992(%rbp), %xmm7
	movdqa	-8912(%rbp), %xmm5
	movaps	%xmm3, -240(%rbp)
	movdqa	-9184(%rbp), %xmm4
	movdqa	-8976(%rbp), %xmm3
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm0, -8624(%rbp)
	movq	$0, -8608(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3888(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -9200(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1659
	call	_ZdlPv@PLT
.L1659:
	movq	-9264(%rbp), %rcx
	movq	-9232(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1657
	.p2align 4,,10
	.p2align 3
.L2038:
	movq	-9232(%rbp), %rsi
	movq	%r14, %rdi
	movabsq	$506100838696486663, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	$0, -8608(%rbp)
	movaps	%xmm0, -8624(%rbp)
	call	_Znwm@PLT
	movq	-9168(%rbp), %rdi
	movabsq	$434322511971682055, %rsi
	movq	%rbx, (%rax)
	leaq	16(%rax), %rdx
	movq	%rsi, 8(%rax)
	movq	%r15, %rsi
	movq	%rax, -8624(%rbp)
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1661
	call	_ZdlPv@PLT
.L1661:
	movq	(%rbx), %rax
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	8(%rax), %rcx
	movq	88(%rax), %r10
	movq	80(%rax), %r11
	movq	%rsi, -8976(%rbp)
	movq	32(%rax), %rsi
	movq	104(%rax), %r9
	movq	%rdx, -9184(%rbp)
	movq	48(%rax), %rdx
	movq	%rdi, -9232(%rbp)
	movq	64(%rax), %rdi
	movq	(%rax), %rbx
	movq	%rcx, -8912(%rbp)
	movq	16(%rax), %rcx
	movq	%r10, -9536(%rbp)
	movq	96(%rax), %r10
	movq	%rsi, -8992(%rbp)
	leaq	.LC10(%rip), %rsi
	movq	120(%rax), %r12
	movq	112(%rax), %r13
	movq	%rdx, -9216(%rbp)
	movl	$3226, %edx
	movq	%rdi, -9504(%rbp)
	movq	%r14, %rdi
	movq	%r11, -9520(%rbp)
	movq	%r10, -9552(%rbp)
	movq	%r9, -9568(%rbp)
	movq	%rbx, -8896(%rbp)
	movq	72(%rax), %rbx
	movq	%rcx, -8944(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rcx
	movq	%r15, %r8
	movq	%r13, %rsi
	movq	-8824(%rbp), %rdi
	movq	%rbx, %rdx
	call	_ZN2v88internal39LoadElementNoHole16FixedDoubleArray_235EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %xmm2
	movq	%rbx, %xmm5
	movq	-9504(%rbp), %xmm4
	punpcklqdq	%xmm2, %xmm5
	movq	%r12, %xmm1
	movq	%r13, %xmm2
	movq	-8944(%rbp), %xmm0
	movq	%rbx, %xmm6
	punpcklqdq	%xmm1, %xmm2
	movq	-9520(%rbp), %xmm7
	movq	-9552(%rbp), %xmm1
	punpcklqdq	%xmm6, %xmm4
	leaq	-88(%rbp), %rdx
	movq	%rax, -96(%rbp)
	movq	-9216(%rbp), %xmm3
	movq	-8992(%rbp), %xmm6
	movhps	-8976(%rbp), %xmm0
	movq	-8896(%rbp), %xmm8
	leaq	-8656(%rbp), %r12
	leaq	-240(%rbp), %r13
	movhps	-9568(%rbp), %xmm1
	movhps	-9536(%rbp), %xmm7
	movaps	%xmm0, -8944(%rbp)
	movhps	-9232(%rbp), %xmm3
	movq	%r13, %rsi
	movq	%r12, %rdi
	movaps	%xmm0, -224(%rbp)
	movhps	-9184(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movhps	-8912(%rbp), %xmm8
	movaps	%xmm5, -9584(%rbp)
	movaps	%xmm2, -9600(%rbp)
	movaps	%xmm1, -9552(%rbp)
	movaps	%xmm7, -9520(%rbp)
	movaps	%xmm4, -9504(%rbp)
	movaps	%xmm3, -9216(%rbp)
	movaps	%xmm6, -8992(%rbp)
	movaps	%xmm8, -8896(%rbp)
	movaps	%xmm8, -240(%rbp)
	movaps	%xmm6, -208(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm0, -8656(%rbp)
	movq	$0, -8640(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-4080(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -9184(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1662
	call	_ZdlPv@PLT
.L1662:
	movq	-9248(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4272(%rbp), %rax
	cmpq	$0, -8616(%rbp)
	movq	%rax, -8976(%rbp)
	jne	.L2062
.L1663:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L1660
	.p2align 4,,10
	.p2align 3
.L2039:
	movq	-9440(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$18, %edi
	movq	$0, -8608(%rbp)
	movaps	%xmm0, -8624(%rbp)
	call	_Znwm@PLT
	movl	$1543, %esi
	movdqa	.LC9(%rip), %xmm0
	movq	-8976(%rbp), %rdi
	movw	%si, 16(%rax)
	leaq	18(%rax), %rdx
	movq	%r15, %rsi
	movups	%xmm0, (%rax)
	movq	%rax, -8624(%rbp)
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1666
	call	_ZdlPv@PLT
.L1666:
	movq	(%rbx), %rax
	movl	$104, %edi
	movdqu	80(%rax), %xmm0
	movdqu	16(%rax), %xmm4
	movdqu	32(%rax), %xmm3
	movdqu	48(%rax), %xmm2
	movdqu	64(%rax), %xmm1
	movq	96(%rax), %rdx
	movdqu	(%rax), %xmm7
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -144(%rbp)
	movaps	%xmm7, -240(%rbp)
	movaps	%xmm4, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -8624(%rbp)
	movq	$0, -8608(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm5
	movdqa	-224(%rbp), %xmm4
	movdqa	-208(%rbp), %xmm3
	leaq	104(%rax), %rdx
	movq	%rax, -8624(%rbp)
	movdqa	-192(%rbp), %xmm2
	movdqa	-176(%rbp), %xmm1
	movq	%rcx, 96(%rax)
	movdqa	-160(%rbp), %xmm6
	movups	%xmm5, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm1, 64(%rax)
	movups	%xmm6, 80(%rax)
	leaq	-3120(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	movq	%rax, -8912(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1667
	call	_ZdlPv@PLT
.L1667:
	movq	-8960(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1665
	.p2align 4,,10
	.p2align 3
.L2041:
	movq	-9264(%rbp), %rsi
	movq	%r14, %rdi
	movabsq	$506100838696486663, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	$0, -8608(%rbp)
	movaps	%xmm0, -8624(%rbp)
	call	_Znwm@PLT
	movq	-9200(%rbp), %rdi
	movabsq	$434322511971682055, %rsi
	movq	%rbx, (%rax)
	leaq	16(%rax), %rdx
	movq	%rsi, 8(%rax)
	movq	%r15, %rsi
	movq	%rax, -8624(%rbp)
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1671
	call	_ZdlPv@PLT
.L1671:
	movq	(%rbx), %rax
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	8(%rax), %rcx
	movq	88(%rax), %r10
	movq	80(%rax), %r11
	movq	%rsi, -9232(%rbp)
	movq	32(%rax), %rsi
	movq	104(%rax), %r9
	movq	%rdx, -9264(%rbp)
	movq	48(%rax), %rdx
	movq	%rdi, -9504(%rbp)
	movq	64(%rax), %rdi
	movq	(%rax), %rbx
	movq	%rcx, -8992(%rbp)
	movq	16(%rax), %rcx
	movq	%r10, -9552(%rbp)
	movq	96(%rax), %r10
	movq	%rsi, -9248(%rbp)
	leaq	.LC10(%rip), %rsi
	movq	112(%rax), %r13
	movq	120(%rax), %r12
	movq	%rdx, -9440(%rbp)
	movl	$3229, %edx
	movq	%rdi, -9520(%rbp)
	movq	%r14, %rdi
	movq	%r11, -9536(%rbp)
	movq	%r10, -9568(%rbp)
	movq	%r9, -9584(%rbp)
	movq	%rbx, -8944(%rbp)
	movq	72(%rax), %rbx
	movq	%rcx, -9216(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rcx
	movq	%r15, %r8
	movq	%r13, %rsi
	movq	-8824(%rbp), %rdi
	movq	%rbx, %rdx
	call	_ZN2v88internal33LoadElementNoHole10FixedArray_234EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %xmm7
	movq	%r13, %xmm2
	movq	-9520(%rbp), %xmm4
	movq	%rbx, %xmm3
	punpcklqdq	%xmm7, %xmm2
	leaq	-88(%rbp), %rdx
	movq	%rax, -96(%rbp)
	movq	%rbx, %xmm5
	punpcklqdq	%xmm3, %xmm4
	movq	-9216(%rbp), %xmm0
	movq	-9568(%rbp), %xmm1
	punpcklqdq	%xmm7, %xmm5
	leaq	-8656(%rbp), %r12
	movq	-9536(%rbp), %xmm7
	movq	-9440(%rbp), %xmm3
	movq	-9248(%rbp), %xmm6
	movq	%r12, %rdi
	movaps	%xmm2, -128(%rbp)
	movq	-8944(%rbp), %xmm9
	movhps	-9232(%rbp), %xmm0
	leaq	-240(%rbp), %r13
	movhps	-9584(%rbp), %xmm1
	movaps	%xmm5, -9600(%rbp)
	movhps	-9552(%rbp), %xmm7
	movhps	-9504(%rbp), %xmm3
	movhps	-9264(%rbp), %xmm6
	movaps	%xmm0, -9232(%rbp)
	movhps	-8992(%rbp), %xmm9
	movq	%r13, %rsi
	movaps	%xmm0, -224(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -9616(%rbp)
	movaps	%xmm1, -9568(%rbp)
	movaps	%xmm7, -9536(%rbp)
	movaps	%xmm4, -9520(%rbp)
	movaps	%xmm3, -9440(%rbp)
	movaps	%xmm6, -9248(%rbp)
	movaps	%xmm9, -8944(%rbp)
	movaps	%xmm9, -240(%rbp)
	movaps	%xmm6, -208(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm0, -8656(%rbp)
	movq	$0, -8640(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3504(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -9216(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1672
	call	_ZdlPv@PLT
.L1672:
	movq	-9424(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3696(%rbp), %rax
	cmpq	$0, -8616(%rbp)
	movq	%rax, -8992(%rbp)
	jne	.L2063
.L1673:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -3632(%rbp)
	je	.L1675
.L2042:
	movq	-9456(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$18, %edi
	movq	$0, -8608(%rbp)
	movaps	%xmm0, -8624(%rbp)
	call	_Znwm@PLT
	movl	$1543, %ecx
	movdqa	.LC9(%rip), %xmm0
	movq	%r15, %rsi
	movw	%cx, 16(%rax)
	movq	-8992(%rbp), %rdi
	leaq	18(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -8624(%rbp)
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1676
	call	_ZdlPv@PLT
.L1676:
	movq	(%rbx), %rax
	movl	$104, %edi
	movdqu	80(%rax), %xmm0
	movdqu	16(%rax), %xmm4
	movdqu	32(%rax), %xmm3
	movdqu	48(%rax), %xmm2
	movdqu	64(%rax), %xmm1
	movq	96(%rax), %rdx
	movdqu	(%rax), %xmm5
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -144(%rbp)
	movaps	%xmm5, -240(%rbp)
	movaps	%xmm4, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -8624(%rbp)
	movq	$0, -8608(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm2
	movdqa	-224(%rbp), %xmm1
	movdqa	-208(%rbp), %xmm6
	leaq	104(%rax), %rdx
	movq	%rax, -8624(%rbp)
	movdqa	-192(%rbp), %xmm7
	movdqa	-176(%rbp), %xmm5
	movq	%rcx, 96(%rax)
	movdqa	-160(%rbp), %xmm4
	movq	-8912(%rbp), %rdi
	movups	%xmm2, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm5, 64(%rax)
	movups	%xmm4, 80(%rax)
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1677
	call	_ZdlPv@PLT
.L1677:
	movq	-8960(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3440(%rbp)
	je	.L1678
.L2043:
	movq	-9424(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -8808(%rbp)
	movq	$0, -8800(%rbp)
	movq	$0, -8792(%rbp)
	movq	$0, -8784(%rbp)
	movq	$0, -8776(%rbp)
	movq	$0, -8768(%rbp)
	movq	$0, -8760(%rbp)
	movq	$0, -8752(%rbp)
	movq	$0, -8744(%rbp)
	movq	$0, -8736(%rbp)
	movq	$0, -8728(%rbp)
	movq	$0, -8720(%rbp)
	movq	$0, -8712(%rbp)
	movq	$0, -8704(%rbp)
	movq	$0, -8696(%rbp)
	movq	$0, -8688(%rbp)
	movq	$0, -8680(%rbp)
	movq	$0, -8672(%rbp)
	movq	$0, -8656(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8656(%rbp), %rax
	movq	-9216(%rbp), %rdi
	leaq	-8792(%rbp), %rcx
	pushq	%rax
	leaq	-8672(%rbp), %rax
	leaq	-8776(%rbp), %r9
	pushq	%rax
	leaq	-8680(%rbp), %rax
	leaq	-8784(%rbp), %r8
	pushq	%rax
	leaq	-8688(%rbp), %rax
	leaq	-8800(%rbp), %rdx
	pushq	%rax
	leaq	-8696(%rbp), %rax
	leaq	-8808(%rbp), %rsi
	pushq	%rax
	leaq	-8704(%rbp), %rax
	pushq	%rax
	leaq	-8712(%rbp), %rax
	pushq	%rax
	leaq	-8720(%rbp), %rax
	pushq	%rax
	leaq	-8728(%rbp), %rax
	pushq	%rax
	leaq	-8736(%rbp), %rax
	pushq	%rax
	leaq	-8744(%rbp), %rax
	pushq	%rax
	leaq	-8752(%rbp), %rax
	pushq	%rax
	leaq	-8760(%rbp), %rax
	pushq	%rax
	leaq	-8768(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_SO_SG_SO_SQ_SO_SM_
	pxor	%xmm0, %xmm0
	addq	$112, %rsp
	movq	%r15, %rdi
	movq	-8808(%rbp), %rax
	leaq	-240(%rbp), %rsi
	leaq	-104(%rbp), %rdx
	movq	%rax, -240(%rbp)
	movq	-8800(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	-8792(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-8784(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-8776(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-8768(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-8760(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-8752(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-8744(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-8736(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-8728(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-8720(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-8712(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-8704(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-8696(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-8688(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-8656(%rbp), %rax
	movaps	%xmm0, -8624(%rbp)
	movq	%rax, -112(%rbp)
	movq	$0, -8608(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8896(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1679
	call	_ZdlPv@PLT
.L1679:
	movq	-8928(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1678
	.p2align 4,,10
	.p2align 3
.L2040:
	movq	-9248(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -8808(%rbp)
	movq	$0, -8800(%rbp)
	movq	$0, -8792(%rbp)
	movq	$0, -8784(%rbp)
	movq	$0, -8776(%rbp)
	movq	$0, -8768(%rbp)
	movq	$0, -8760(%rbp)
	movq	$0, -8752(%rbp)
	movq	$0, -8744(%rbp)
	movq	$0, -8736(%rbp)
	movq	$0, -8728(%rbp)
	movq	$0, -8720(%rbp)
	movq	$0, -8712(%rbp)
	movq	$0, -8704(%rbp)
	movq	$0, -8696(%rbp)
	movq	$0, -8688(%rbp)
	movq	$0, -8680(%rbp)
	movq	$0, -8672(%rbp)
	movq	$0, -8656(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8656(%rbp), %rax
	movq	-9184(%rbp), %rdi
	leaq	-8792(%rbp), %rcx
	pushq	%rax
	leaq	-8672(%rbp), %rax
	leaq	-8776(%rbp), %r9
	pushq	%rax
	leaq	-8680(%rbp), %rax
	leaq	-8784(%rbp), %r8
	pushq	%rax
	leaq	-8688(%rbp), %rax
	leaq	-8800(%rbp), %rdx
	pushq	%rax
	leaq	-8696(%rbp), %rax
	leaq	-8808(%rbp), %rsi
	pushq	%rax
	leaq	-8704(%rbp), %rax
	pushq	%rax
	leaq	-8712(%rbp), %rax
	pushq	%rax
	leaq	-8720(%rbp), %rax
	pushq	%rax
	leaq	-8728(%rbp), %rax
	pushq	%rax
	leaq	-8736(%rbp), %rax
	pushq	%rax
	leaq	-8744(%rbp), %rax
	pushq	%rax
	leaq	-8752(%rbp), %rax
	pushq	%rax
	leaq	-8760(%rbp), %rax
	pushq	%rax
	leaq	-8768(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_SO_SG_SO_SQ_SO_SM_
	pxor	%xmm0, %xmm0
	addq	$112, %rsp
	movq	%r15, %rdi
	movq	-8808(%rbp), %rax
	leaq	-240(%rbp), %rsi
	leaq	-104(%rbp), %rdx
	movq	%rax, -240(%rbp)
	movq	-8800(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	-8792(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-8784(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-8776(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-8768(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-8760(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-8752(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-8744(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-8736(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-8728(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-8720(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-8712(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-8704(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-8696(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-8688(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-8656(%rbp), %rax
	movaps	%xmm0, -8624(%rbp)
	movq	%rax, -112(%rbp)
	movq	$0, -8608(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8896(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1669
	call	_ZdlPv@PLT
.L1669:
	movq	-8928(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1668
	.p2align 4,,10
	.p2align 3
.L2045:
	movq	-8960(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -8760(%rbp)
	movq	$0, -8752(%rbp)
	movq	$0, -8744(%rbp)
	movq	$0, -8736(%rbp)
	movq	$0, -8728(%rbp)
	movq	$0, -8720(%rbp)
	movq	$0, -8712(%rbp)
	movq	$0, -8704(%rbp)
	movq	$0, -8696(%rbp)
	movq	$0, -8688(%rbp)
	movq	$0, -8680(%rbp)
	movq	$0, -8672(%rbp)
	movq	$0, -8656(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8656(%rbp), %rax
	movq	-8912(%rbp), %rdi
	leaq	-8744(%rbp), %rcx
	pushq	%rax
	leaq	-8672(%rbp), %rax
	leaq	-8752(%rbp), %rdx
	pushq	%rax
	leaq	-8680(%rbp), %rax
	leaq	-8760(%rbp), %rsi
	pushq	%rax
	leaq	-8688(%rbp), %rax
	leaq	-8728(%rbp), %r9
	pushq	%rax
	leaq	-8696(%rbp), %rax
	leaq	-8736(%rbp), %r8
	pushq	%rax
	leaq	-8704(%rbp), %rax
	pushq	%rax
	leaq	-8712(%rbp), %rax
	pushq	%rax
	leaq	-8720(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_
	movq	-8760(%rbp), %rax
	pxor	%xmm0, %xmm0
	addq	$64, %rsp
	movl	$104, %edi
	movaps	%xmm0, -8624(%rbp)
	movq	%rax, -240(%rbp)
	movq	-8752(%rbp), %rax
	movq	$0, -8608(%rbp)
	movq	%rax, -232(%rbp)
	movq	-8744(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-8736(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-8728(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-8720(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-8712(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-8704(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-8696(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-8688(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-8680(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-8672(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-8656(%rbp), %rax
	movq	%rax, -144(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm3
	movdqa	-224(%rbp), %xmm2
	movdqa	-208(%rbp), %xmm1
	leaq	104(%rax), %rdx
	movq	%rax, -8624(%rbp)
	movdqa	-192(%rbp), %xmm6
	movdqa	-176(%rbp), %xmm7
	movq	%rcx, 96(%rax)
	movdqa	-160(%rbp), %xmm5
	movq	-8944(%rbp), %rdi
	movups	%xmm3, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm5, 80(%rax)
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1684
	call	_ZdlPv@PLT
.L1684:
	movq	-9328(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1683
	.p2align 4,,10
	.p2align 3
.L2044:
	movq	-8928(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-240(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC9(%rip), %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-223(%rbp), %rdx
	movb	$8, -224(%rbp)
	movaps	%xmm0, -240(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8624(%rbp)
	movq	$0, -8608(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8896(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1681
	call	_ZdlPv@PLT
.L1681:
	movq	(%rbx), %rax
	movq	40(%rax), %rdx
	movq	24(%rax), %rsi
	movq	88(%rax), %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	48(%rax), %r12
	movq	%rdx, -9424(%rbp)
	movq	56(%rax), %rdx
	movq	%rsi, -9248(%rbp)
	movq	%rdi, -9520(%rbp)
	movq	32(%rax), %rsi
	movq	%rdx, -9440(%rbp)
	movq	72(%rax), %rdx
	movq	96(%rax), %rdi
	movq	%rbx, -8928(%rbp)
	movq	%rdx, -9456(%rbp)
	movq	80(%rax), %rdx
	movq	64(%rax), %rbx
	movq	%rcx, -8944(%rbp)
	movq	16(%rax), %rcx
	movq	128(%rax), %rax
	movq	%rsi, -9264(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -9504(%rbp)
	movl	$136, %edx
	movq	%rdi, -9536(%rbp)
	movq	%r14, %rdi
	movq	%rcx, -9232(%rbp)
	movq	%rax, -9552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-128(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	-8928(%rbp), %xmm0
	movq	$0, -8608(%rbp)
	movhps	-8944(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-9232(%rbp), %xmm0
	movhps	-9248(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-9264(%rbp), %xmm0
	movhps	-9424(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	%r12, %xmm0
	movhps	-9440(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	%rbx, %xmm0
	movhps	-9456(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-9504(%rbp), %xmm0
	movhps	-9520(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-9536(%rbp), %xmm0
	movhps	-9552(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8624(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2928(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -9232(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1682
	call	_ZdlPv@PLT
.L1682:
	movq	-9280(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1680
	.p2align 4,,10
	.p2align 3
.L2049:
	movq	-9392(%rbp), %rsi
	movq	%r14, %rdi
	movl	$2054, %r13d
	movabsq	$506100838696486663, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$15, %edi
	movq	$0, -8608(%rbp)
	movaps	%xmm0, -8624(%rbp)
	call	_Znwm@PLT
	movq	-9264(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, (%rax)
	leaq	15(%rax), %rdx
	movl	$67569415, 8(%rax)
	movw	%r13w, 12(%rax)
	movb	$8, 14(%rax)
	movq	%rax, -8624(%rbp)
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1697
	call	_ZdlPv@PLT
.L1697:
	movq	(%rbx), %rax
	movq	(%rax), %rbx
	movq	24(%rax), %rcx
	movq	40(%rax), %rsi
	movq	56(%rax), %rdx
	movq	88(%rax), %rdi
	movq	72(%rax), %r12
	movq	%rbx, -9424(%rbp)
	movq	8(%rax), %rbx
	movq	%rcx, -9280(%rbp)
	movq	32(%rax), %rcx
	movq	%rsi, -9376(%rbp)
	movq	48(%rax), %rsi
	movq	80(%rax), %r13
	movq	%rdx, -9520(%rbp)
	movq	%rdi, -9552(%rbp)
	movq	64(%rax), %rdx
	movq	104(%rax), %rdi
	movq	%rbx, -9440(%rbp)
	movq	%rcx, -9456(%rbp)
	movq	16(%rax), %rbx
	movq	96(%rax), %rcx
	movq	112(%rax), %rax
	movq	%rsi, -9504(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -9536(%rbp)
	movl	$141, %edx
	movq	%rdi, -9568(%rbp)
	movq	%r14, %rdi
	movq	%rcx, -9368(%rbp)
	movq	%rax, -9392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$143, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-8824(%rbp), %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$3208, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -9584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-56(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r12, -64(%rbp)
	movq	-9424(%rbp), %xmm1
	movq	%r12, %xmm4
	movq	-9568(%rbp), %xmm2
	leaq	-240(%rbp), %rsi
	movq	$0, -8608(%rbp)
	movdqa	%xmm1, %xmm0
	movhps	-9440(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	%rbx, %xmm0
	movhps	-9280(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-9456(%rbp), %xmm0
	movhps	-9376(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-9504(%rbp), %xmm0
	movhps	-9520(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-9536(%rbp), %xmm0
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	%r13, %xmm0
	movhps	-9552(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-9368(%rbp), %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-9392(%rbp), %xmm0
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -128(%rbp)
	movdqa	%xmm1, %xmm0
	movhps	-9280(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-9584(%rbp), %xmm0
	movhps	-9392(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	%xmm2, %xmm0
	movhps	-9368(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8624(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2160(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -9280(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1698
	call	_ZdlPv@PLT
.L1698:
	movq	-9408(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2096(%rbp)
	je	.L1699
.L2050:
	movq	-9408(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-240(%rbp), %r13
	movl	$1544, %r12d
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC11(%rip), %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-217(%rbp), %rdx
	movw	%r12w, -220(%rbp)
	movaps	%xmm0, -240(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8624(%rbp)
	movl	$134678279, -224(%rbp)
	movb	$7, -218(%rbp)
	movq	$0, -8608(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9280(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1700
	call	_ZdlPv@PLT
.L1700:
	movq	(%rbx), %rax
	movl	$142, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	leaq	-8672(%rbp), %r12
	movq	(%rax), %rbx
	movq	%rbx, -9392(%rbp)
	movq	8(%rax), %rbx
	movq	%rbx, -9408(%rbp)
	movq	16(%rax), %rbx
	movq	%rbx, -9424(%rbp)
	movq	24(%rax), %rbx
	movq	%rbx, -9440(%rbp)
	movq	32(%rax), %rbx
	movq	%rbx, -9456(%rbp)
	movq	48(%rax), %rbx
	movq	%rbx, -9376(%rbp)
	movq	56(%rax), %rbx
	movq	%rbx, -9504(%rbp)
	movq	64(%rax), %rbx
	movq	%rbx, -9520(%rbp)
	movq	72(%rax), %rbx
	movq	%rbx, -9536(%rbp)
	movq	80(%rax), %rbx
	movq	%rbx, -9552(%rbp)
	movq	88(%rax), %rbx
	movq	%rbx, -9568(%rbp)
	movq	96(%rax), %rbx
	movq	%rbx, -9584(%rbp)
	movq	104(%rax), %rbx
	movq	%rbx, -9600(%rbp)
	movq	112(%rax), %rbx
	movq	%rbx, -9616(%rbp)
	movq	128(%rax), %rbx
	movq	%rbx, -9368(%rbp)
	movq	136(%rax), %rbx
	movq	%rbx, -9632(%rbp)
	movq	152(%rax), %rcx
	movq	144(%rax), %rbx
	movq	%rcx, -9648(%rbp)
	movq	160(%rax), %rcx
	movq	%rcx, -9664(%rbp)
	movq	168(%rax), %rcx
	movq	176(%rax), %rax
	movq	%rcx, -9488(%rbp)
	movq	%rax, -9672(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-8824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19IsUndefinedConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	je	.L1701
.L1703:
	movq	-9664(%rbp), %xmm0
	movq	%rbx, %xmm1
	movq	%r12, %rdi
	movhps	-9648(%rbp), %xmm1
	movhps	-9488(%rbp), %xmm0
	movaps	%xmm1, -9696(%rbp)
	movaps	%xmm0, -9664(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-8624(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -9648(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-9696(%rbp), %xmm1
	movq	-9632(%rbp), %xmm2
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movdqa	-9664(%rbp), %xmm0
	movq	%rax, -8656(%rbp)
	movq	-8608(%rbp), %rax
	movhps	-9648(%rbp), %xmm2
	movaps	%xmm2, -240(%rbp)
	movq	%rax, -8648(%rbp)
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
.L2016:
	movl	$7, %ebx
	movl	$1, %ecx
	xorl	%esi, %esi
	movq	%r12, %rdi
	pushq	%rbx
	movq	-9368(%rbp), %r9
	leaq	-8656(%rbp), %rdx
	pushq	%r13
	movq	-9672(%rbp), %rax
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	popq	%r11
	movq	%r12, %rdi
	popq	%rbx
	movq	-9392(%rbp), %xmm5
	movq	%rax, -9368(%rbp)
	movq	-9424(%rbp), %xmm4
	movq	-9376(%rbp), %xmm3
	movq	-9520(%rbp), %xmm2
	movhps	-9408(%rbp), %xmm5
	movq	-9552(%rbp), %xmm1
	movq	-9584(%rbp), %xmm0
	movhps	-9440(%rbp), %xmm4
	movhps	-9504(%rbp), %xmm3
	movaps	%xmm5, -9648(%rbp)
	movhps	-9536(%rbp), %xmm2
	movhps	-9568(%rbp), %xmm1
	movaps	%xmm4, -9632(%rbp)
	movhps	-9600(%rbp), %xmm0
	movaps	%xmm3, -9440(%rbp)
	movaps	%xmm2, -9424(%rbp)
	movaps	%xmm1, -9408(%rbp)
	movaps	%xmm0, -9392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$138, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movdqa	-9632(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-9392(%rbp), %xmm0
	movdqa	-9648(%rbp), %xmm5
	movdqa	-9440(%rbp), %xmm3
	leaq	-120(%rbp), %rdx
	movq	%r15, %rdi
	movaps	%xmm4, -224(%rbp)
	movdqa	-9424(%rbp), %xmm2
	movq	-9456(%rbp), %xmm4
	movdqa	-9408(%rbp), %xmm1
	movq	-9616(%rbp), %rax
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-9368(%rbp), %xmm4
	movaps	%xmm5, -240(%rbp)
	movq	%rax, -128(%rbp)
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -8624(%rbp)
	movq	$0, -8608(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8928(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1704
	call	_ZdlPv@PLT
.L1704:
	movq	-9296(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1904(%rbp)
	je	.L1705
.L2051:
	movq	-9296(%rbp), %rsi
	movq	%r14, %rdi
	movabsq	$506100838696486663, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$15, %edi
	movq	$0, -8608(%rbp)
	movaps	%xmm0, -8624(%rbp)
	call	_Znwm@PLT
	movl	$2054, %r10d
	movq	-8928(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, (%rax)
	leaq	15(%rax), %rdx
	movl	$67569415, 8(%rax)
	movw	%r10w, 12(%rax)
	movb	$8, 14(%rax)
	movq	%rax, -8624(%rbp)
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1706
	call	_ZdlPv@PLT
.L1706:
	movq	(%rbx), %rax
	movq	24(%rax), %rsi
	movq	72(%rax), %rdx
	movq	8(%rax), %rcx
	movq	88(%rax), %rdi
	movq	(%rax), %rbx
	movq	32(%rax), %r13
	movq	%rsi, -9408(%rbp)
	movq	40(%rax), %rsi
	movq	%rdx, -9376(%rbp)
	movq	80(%rax), %rdx
	movq	48(%rax), %r12
	movq	%rcx, -9368(%rbp)
	movq	%rsi, -9424(%rbp)
	movq	56(%rax), %rsi
	movq	16(%rax), %rcx
	movq	%rdx, -9504(%rbp)
	movl	$137, %edx
	movq	%rsi, -9440(%rbp)
	movq	64(%rax), %rsi
	movq	%rdi, -9520(%rbp)
	movq	%r14, %rdi
	movq	%rsi, -9456(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -9392(%rbp)
	movq	%rbx, -9296(%rbp)
	movq	96(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$130, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$104, %edi
	movq	-9296(%rbp), %xmm0
	movq	%rbx, -144(%rbp)
	movq	$0, -8608(%rbp)
	movhps	-9368(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-9392(%rbp), %xmm0
	movhps	-9408(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	%r13, %xmm0
	movhps	-9424(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	%r12, %xmm0
	movhps	-9440(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-9456(%rbp), %xmm0
	movhps	-9376(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-9504(%rbp), %xmm0
	movhps	-9520(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8624(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm3
	movdqa	-224(%rbp), %xmm2
	movdqa	-208(%rbp), %xmm1
	leaq	104(%rax), %rdx
	movq	%rax, -8624(%rbp)
	movdqa	-192(%rbp), %xmm7
	movdqa	-176(%rbp), %xmm5
	movq	%rcx, 96(%rax)
	movdqa	-160(%rbp), %xmm4
	movq	-8944(%rbp), %rdi
	movups	%xmm3, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm5, 64(%rax)
	movups	%xmm4, 80(%rax)
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1707
	call	_ZdlPv@PLT
.L1707:
	movq	-9328(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1712(%rbp)
	je	.L1708
.L2052:
	movq	-9328(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -8760(%rbp)
	movq	$0, -8752(%rbp)
	movq	$0, -8744(%rbp)
	movq	$0, -8736(%rbp)
	movq	$0, -8728(%rbp)
	movq	$0, -8720(%rbp)
	movq	$0, -8712(%rbp)
	movq	$0, -8704(%rbp)
	movq	$0, -8696(%rbp)
	movq	$0, -8688(%rbp)
	movq	$0, -8680(%rbp)
	movq	$0, -8672(%rbp)
	movq	$0, -8656(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8656(%rbp), %rax
	movq	-8944(%rbp), %rdi
	leaq	-8744(%rbp), %rcx
	pushq	%rax
	leaq	-8672(%rbp), %rax
	leaq	-8728(%rbp), %r9
	pushq	%rax
	leaq	-8680(%rbp), %rax
	leaq	-8736(%rbp), %r8
	pushq	%rax
	leaq	-8688(%rbp), %rax
	leaq	-8752(%rbp), %rdx
	pushq	%rax
	leaq	-8696(%rbp), %rax
	leaq	-8760(%rbp), %rsi
	pushq	%rax
	leaq	-8704(%rbp), %rax
	pushq	%rax
	leaq	-8712(%rbp), %rax
	pushq	%rax
	leaq	-8720(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_
	movq	-8824(%rbp), %rbx
	addq	$64, %rsp
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8656(%rbp), %r13
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$104, %edi
	movq	-8680(%rbp), %xmm0
	movq	-8696(%rbp), %xmm1
	movq	-8712(%rbp), %xmm2
	movq	%rbx, -144(%rbp)
	movq	-8728(%rbp), %xmm3
	movhps	-8672(%rbp), %xmm0
	movq	-8744(%rbp), %xmm4
	movhps	-8688(%rbp), %xmm1
	movq	-8760(%rbp), %xmm5
	movhps	-8704(%rbp), %xmm2
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-8720(%rbp), %xmm3
	movhps	-8736(%rbp), %xmm4
	movaps	%xmm2, -192(%rbp)
	movhps	-8752(%rbp), %xmm5
	movaps	%xmm4, -224(%rbp)
	movaps	%xmm5, -240(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -8624(%rbp)
	movq	$0, -8608(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm3
	movdqa	-224(%rbp), %xmm2
	movdqa	-208(%rbp), %xmm1
	leaq	104(%rax), %rdx
	movq	%rax, -8624(%rbp)
	movdqa	-192(%rbp), %xmm6
	movdqa	-176(%rbp), %xmm7
	movq	%rcx, 96(%rax)
	movdqa	-160(%rbp), %xmm5
	movq	-8864(%rbp), %rdi
	movups	%xmm3, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm5, 80(%rax)
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1709
	call	_ZdlPv@PLT
.L1709:
	movq	-9288(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1708
	.p2align 4,,10
	.p2align 3
.L2048:
	movq	-9368(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-240(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC11(%rip), %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-223(%rbp), %rdx
	movb	$7, -224(%rbp)
	movaps	%xmm0, -240(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8624(%rbp)
	movq	$0, -8608(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9248(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1694
	call	_ZdlPv@PLT
.L1694:
	movq	(%rbx), %rax
	movq	24(%rax), %rsi
	movq	56(%rax), %rdx
	movq	72(%rax), %rdi
	movq	8(%rax), %rcx
	movq	88(%rax), %r11
	movq	(%rax), %rbx
	movq	%rsi, -9440(%rbp)
	movq	32(%rax), %rsi
	movq	104(%rax), %r10
	movq	%rdx, -9376(%rbp)
	movq	64(%rax), %rdx
	movq	%rdi, -9520(%rbp)
	movq	80(%rax), %rdi
	movq	%rcx, -9368(%rbp)
	movq	16(%rax), %rcx
	movq	%r11, -9552(%rbp)
	movq	96(%rax), %r11
	movq	%rsi, -9456(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -9504(%rbp)
	movl	$139, %edx
	movq	48(%rax), %r12
	movq	%rdi, -9536(%rbp)
	movq	%r14, %rdi
	movq	%rcx, -9424(%rbp)
	movq	%r11, -9568(%rbp)
	movq	%r10, -8928(%rbp)
	movq	%rbx, -9280(%rbp)
	movq	112(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$138, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-120(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	-9280(%rbp), %xmm0
	movq	%rbx, -128(%rbp)
	movq	$0, -8608(%rbp)
	movhps	-9368(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-9424(%rbp), %xmm0
	movhps	-9440(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-9456(%rbp), %xmm0
	movhps	-8928(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	%r12, %xmm0
	movhps	-9376(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-9504(%rbp), %xmm0
	movhps	-9520(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-9536(%rbp), %xmm0
	movhps	-9552(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-9568(%rbp), %xmm0
	movhps	-8928(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8624(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1968(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -8928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1695
	call	_ZdlPv@PLT
.L1695:
	movq	-9296(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1693
	.p2align 4,,10
	.p2align 3
.L2047:
	movq	-9376(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-240(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC11(%rip), %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-224(%rbp), %rdx
	movq	$0, -8608(%rbp)
	movaps	%xmm0, -240(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8624(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8960(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1691
	call	_ZdlPv@PLT
.L1691:
	movq	(%rbx), %rax
	leaq	-120(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movdqu	96(%rax), %xmm0
	movdqu	(%rax), %xmm6
	movdqu	16(%rax), %xmm5
	movdqu	32(%rax), %xmm4
	movdqu	48(%rax), %xmm3
	movdqu	64(%rax), %xmm2
	movdqu	80(%rax), %xmm1
	movq	112(%rax), %rax
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm6, -240(%rbp)
	movq	%rax, -128(%rbp)
	movaps	%xmm5, -224(%rbp)
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -8624(%rbp)
	movq	$0, -8608(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2352(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -9264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1692
	call	_ZdlPv@PLT
.L1692:
	movq	-9392(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1690
	.p2align 4,,10
	.p2align 3
.L2046:
	movq	-9280(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-240(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-226(%rbp), %rdx
	movabsq	$506100838696486663, %rax
	movaps	%xmm0, -8624(%rbp)
	movq	%rax, -240(%rbp)
	movl	$2054, %eax
	movw	%ax, -228(%rbp)
	movl	$67569415, -232(%rbp)
	movq	$0, -8608(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9232(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1686
	call	_ZdlPv@PLT
.L1686:
	movq	(%rbx), %rax
	movq	72(%rax), %r11
	movq	24(%rax), %rsi
	movq	56(%rax), %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	%r11, -9504(%rbp)
	movq	88(%rax), %r11
	movq	48(%rax), %rdx
	movq	%rsi, -9264(%rbp)
	movq	32(%rax), %rsi
	movq	%rdi, -9440(%rbp)
	movq	64(%rax), %rdi
	movq	80(%rax), %r12
	movq	%rbx, -8928(%rbp)
	movq	%rcx, -8960(%rbp)
	movq	40(%rax), %rbx
	movq	16(%rax), %rcx
	movq	%r11, -9520(%rbp)
	movq	96(%rax), %r11
	movq	104(%rax), %rax
	movq	%rsi, -9280(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -9424(%rbp)
	movl	$137, %edx
	movq	%rdi, -9456(%rbp)
	movq	%r14, %rdi
	movq	%r11, -9536(%rbp)
	movq	%rax, -9552(%rbp)
	movq	%rcx, -9248(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$138, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8824(%rbp), %rdi
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal17Cast9ATTheHole_88EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm6
	leaq	-104(%rbp), %rdx
	movq	%r13, %rsi
	movq	-9280(%rbp), %xmm3
	movq	%r12, %xmm1
	movq	-8928(%rbp), %xmm0
	leaq	-8656(%rbp), %r12
	movq	-9536(%rbp), %xmm2
	movq	%rbx, %xmm5
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	punpcklqdq	%xmm6, %xmm3
	movhps	-8960(%rbp), %xmm0
	movq	-9456(%rbp), %xmm7
	movq	-9424(%rbp), %xmm4
	movq	-9248(%rbp), %xmm6
	punpcklqdq	%xmm5, %xmm5
	movhps	-9552(%rbp), %xmm2
	movhps	-9520(%rbp), %xmm1
	movhps	-9504(%rbp), %xmm7
	movhps	-9440(%rbp), %xmm4
	movaps	%xmm0, -8928(%rbp)
	movhps	-9264(%rbp), %xmm6
	movaps	%xmm0, -240(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -9568(%rbp)
	movaps	%xmm2, -9536(%rbp)
	movaps	%xmm1, -9520(%rbp)
	movaps	%xmm7, -9456(%rbp)
	movaps	%xmm4, -9424(%rbp)
	movaps	%xmm3, -9280(%rbp)
	movaps	%xmm6, -9264(%rbp)
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm0, -8656(%rbp)
	movq	$0, -8640(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2544(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -9248(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1687
	call	_ZdlPv@PLT
.L1687:
	movq	-9368(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2736(%rbp), %rax
	cmpq	$0, -8616(%rbp)
	movq	%rax, -8960(%rbp)
	jne	.L2064
.L1688:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L1685
	.p2align 4,,10
	.p2align 3
.L2053:
	movq	-9336(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -8768(%rbp)
	leaq	-8656(%rbp), %r12
	movq	$0, -8760(%rbp)
	leaq	-240(%rbp), %r13
	movq	$0, -8752(%rbp)
	movq	$0, -8744(%rbp)
	movq	$0, -8736(%rbp)
	movq	$0, -8728(%rbp)
	movq	$0, -8720(%rbp)
	movq	$0, -8712(%rbp)
	movq	$0, -8704(%rbp)
	movq	$0, -8696(%rbp)
	movq	$0, -8688(%rbp)
	movq	$0, -8680(%rbp)
	movq	$0, -8672(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8672(%rbp), %rax
	movq	-9088(%rbp), %rdi
	leaq	-8736(%rbp), %r9
	pushq	%rax
	leaq	-8680(%rbp), %rax
	leaq	-8752(%rbp), %rcx
	pushq	%rax
	leaq	-8688(%rbp), %rax
	leaq	-8744(%rbp), %r8
	pushq	%rax
	leaq	-8696(%rbp), %rax
	leaq	-8760(%rbp), %rdx
	pushq	%rax
	leaq	-8704(%rbp), %rax
	leaq	-8768(%rbp), %rsi
	pushq	%rax
	leaq	-8712(%rbp), %rax
	pushq	%rax
	leaq	-8720(%rbp), %rax
	pushq	%rax
	leaq	-8728(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES9_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTES6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EESM_PNSE_IS6_EEPNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESO_
	addq	$64, %rsp
	movl	$148, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$149, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%ecx, %ecx
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8728(%rbp), %rsi
	movq	-8824(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZN2v88internal17Cast9ATTheHole_88EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-8768(%rbp), %rdx
	movq	%rax, %r8
	movq	-8728(%rbp), %rax
	movaps	%xmm0, -8656(%rbp)
	movq	%r8, -128(%rbp)
	movq	%rdx, -240(%rbp)
	movq	-8760(%rbp), %rdx
	movq	%rax, -200(%rbp)
	movq	%rdx, -232(%rbp)
	movq	-8752(%rbp), %rdx
	movq	%rax, -144(%rbp)
	movq	%rdx, -224(%rbp)
	movq	-8744(%rbp), %rdx
	movq	%rax, -136(%rbp)
	movq	%rdx, -216(%rbp)
	movq	-8736(%rbp), %rdx
	movq	$0, -8640(%rbp)
	movq	%rdx, -208(%rbp)
	movq	-8720(%rbp), %rdx
	movq	%rdx, -192(%rbp)
	movq	-8712(%rbp), %rdx
	movq	%rdx, -184(%rbp)
	movq	-8704(%rbp), %rdx
	movq	%rdx, -176(%rbp)
	movq	-8696(%rbp), %rdx
	movq	%rdx, -168(%rbp)
	movq	-8688(%rbp), %rdx
	movq	%rdx, -160(%rbp)
	movq	-8680(%rbp), %rdx
	movq	%rdx, -152(%rbp)
	leaq	-120(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9288(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1711
	call	_ZdlPv@PLT
.L1711:
	movq	-9344(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -8616(%rbp)
	jne	.L2065
.L1712:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L1710
	.p2align 4,,10
	.p2align 3
.L2057:
	movq	-9360(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -8608(%rbp)
	movaps	%xmm0, -8624(%rbp)
	call	_Znwm@PLT
	movl	$2056, %edi
	movq	%r15, %rsi
	movw	%di, 4(%rax)
	leaq	6(%rax), %rdx
	movq	%r12, %rdi
	movl	$117966599, (%rax)
	movq	%rax, -8624(%rbp)
	movq	%rdx, -8608(%rbp)
	movq	%rdx, -8616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1723
	call	_ZdlPv@PLT
.L1723:
	movq	(%rbx), %rax
	movl	$119, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	16(%rax), %r13
	movq	%rbx, -8824(%rbp)
	movq	32(%rax), %rbx
	movq	%rcx, -9328(%rbp)
	movq	24(%rax), %rcx
	movq	40(%rax), %rax
	movq	%rcx, -9336(%rbp)
	movq	%rax, -9344(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-240(%rbp), %rsi
	leaq	-192(%rbp), %rdx
	movq	%r15, %rdi
	movq	-8824(%rbp), %xmm0
	movq	$0, -8608(%rbp)
	movhps	-9328(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	%r13, %xmm0
	leaq	-432(%rbp), %r13
	movhps	-9336(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	%rbx, %xmm0
	movhps	-9344(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8624(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1724
	call	_ZdlPv@PLT
.L1724:
	movq	-9320(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -560(%rbp)
	je	.L1725
.L2058:
	movq	-9160(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$2056, %esi
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movw	%si, -240(%rbp)
	leaq	-238(%rbp), %rdx
	leaq	-240(%rbp), %rsi
	movaps	%xmm0, -8624(%rbp)
	movq	$0, -8608(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8848(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1726
	call	_ZdlPv@PLT
.L1726:
	movq	(%rbx), %rax
	movq	-9480(%rbp), %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-9472(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-9464(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1725
	.p2align 4,,10
	.p2align 3
.L2056:
	movq	-9352(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-240(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-227(%rbp), %rdx
	movabsq	$506100838696486663, %rax
	movaps	%xmm0, -8624(%rbp)
	movq	%rax, -240(%rbp)
	movl	$67569415, -232(%rbp)
	movb	$8, -228(%rbp)
	movq	$0, -8608(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9296(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1720
	call	_ZdlPv@PLT
.L1720:
	movq	(%rbx), %rax
	movl	$152, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	16(%rax), %r12
	movq	%rbx, -8824(%rbp)
	movq	32(%rax), %rbx
	movq	%rcx, -9328(%rbp)
	movq	24(%rax), %rcx
	movq	96(%rax), %rax
	movq	%rcx, -9336(%rbp)
	movq	%rax, -9344(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$153, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-192(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	-8824(%rbp), %xmm0
	movq	$0, -8608(%rbp)
	movhps	-9328(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	%r12, %xmm0
	leaq	-816(%rbp), %r12
	movhps	-9336(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	%rbx, %xmm0
	movhps	-9344(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8624(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1721
	call	_ZdlPv@PLT
.L1721:
	movq	-9360(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1719
	.p2align 4,,10
	.p2align 3
.L2055:
	movq	-9344(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$2056, %r8d
	pxor	%xmm0, %xmm0
	movabsq	$506100838696486663, %rax
	leaq	-240(%rbp), %rsi
	leaq	-225(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rax, -240(%rbp)
	movw	%r8w, -228(%rbp)
	movaps	%xmm0, -8624(%rbp)
	movl	$67569415, -232(%rbp)
	movb	$7, -226(%rbp)
	movq	$0, -8608(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9288(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1718
	call	_ZdlPv@PLT
.L1718:
	movq	(%rbx), %rax
	movl	$150, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	movq	(%rax), %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-8824(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movl	$153, %edx
	leaq	.LC7(%rip), %rcx
	movq	%r12, %rsi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L1717
	.p2align 4,,10
	.p2align 3
.L2054:
	movq	-9304(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-240(%rbp), %r13
	leaq	-1392(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	movabsq	$506100838696486663, %rax
	movl	$2056, %r9d
	leaq	-226(%rbp), %rdx
	movaps	%xmm0, -8624(%rbp)
	movq	%rax, -240(%rbp)
	movw	%r9w, -228(%rbp)
	movl	$67569415, -232(%rbp)
	movq	$0, -8608(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8624(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1715
	call	_ZdlPv@PLT
.L1715:
	movq	(%rbx), %rax
	leaq	-136(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movdqu	80(%rax), %xmm0
	movdqu	(%rax), %xmm5
	movdqu	16(%rax), %xmm4
	movdqu	32(%rax), %xmm3
	movdqu	48(%rax), %xmm2
	movdqu	64(%rax), %xmm1
	movq	96(%rax), %rax
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -240(%rbp)
	movq	%rax, -144(%rbp)
	movaps	%xmm4, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -8624(%rbp)
	movq	$0, -8608(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1008(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -9296(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1716
	call	_ZdlPv@PLT
.L1716:
	movq	-9352(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1714
	.p2align 4,,10
	.p2align 3
.L1701:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IsNullConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L1703
	movq	-9664(%rbp), %xmm1
	movq	%rbx, %xmm0
	movq	%r12, %rdi
	movhps	-9648(%rbp), %xmm0
	movhps	-9488(%rbp), %xmm1
	movaps	%xmm0, -9696(%rbp)
	movaps	%xmm1, -9664(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-8624(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -9648(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-9696(%rbp), %xmm0
	movq	-9632(%rbp), %xmm2
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movdqa	-9664(%rbp), %xmm1
	movq	%rax, -8656(%rbp)
	movq	-8608(%rbp), %rax
	movhps	-9648(%rbp), %xmm2
	movaps	%xmm2, -240(%rbp)
	movq	%rax, -8648(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm1, -208(%rbp)
	jmp	.L2016
	.p2align 4,,10
	.p2align 3
.L2060:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movdqa	-8848(%rbp), %xmm6
	movdqa	-8880(%rbp), %xmm7
	leaq	-184(%rbp), %rdx
	movaps	%xmm0, -8656(%rbp)
	movq	%rbx, -192(%rbp)
	movaps	%xmm6, -240(%rbp)
	movdqa	-9024(%rbp), %xmm6
	movaps	%xmm7, -224(%rbp)
	movaps	%xmm6, -208(%rbp)
	movq	$0, -8640(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8832(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1603
	call	_ZdlPv@PLT
.L1603:
	movq	-9000(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1602
	.p2align 4,,10
	.p2align 3
.L2061:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-8880(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-8864(%rbp), %xmm4
	movdqa	-8912(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movdqa	-9056(%rbp), %xmm6
	leaq	-176(%rbp), %rdx
	movaps	%xmm7, -240(%rbp)
	movaps	%xmm4, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm0, -8656(%rbp)
	movq	$0, -8640(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8952(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1617
	call	_ZdlPv@PLT
.L1617:
	movq	-9040(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1616
	.p2align 4,,10
	.p2align 3
.L2062:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-8896(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-8944(%rbp), %xmm4
	movdqa	-8992(%rbp), %xmm3
	movdqa	-9216(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	leaq	-96(%rbp), %rdx
	movdqa	-9520(%rbp), %xmm2
	movdqa	-9552(%rbp), %xmm1
	movaps	%xmm7, -240(%rbp)
	movq	%r12, %rdi
	movdqa	-9504(%rbp), %xmm7
	movdqa	-9600(%rbp), %xmm5
	movaps	%xmm4, -224(%rbp)
	movdqa	-9584(%rbp), %xmm4
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm0, -8656(%rbp)
	movq	$0, -8640(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8976(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1664
	call	_ZdlPv@PLT
.L1664:
	movq	-9440(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1663
	.p2align 4,,10
	.p2align 3
.L2064:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-8928(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-9264(%rbp), %xmm2
	movdqa	-9280(%rbp), %xmm1
	movdqa	-9424(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	leaq	-112(%rbp), %rdx
	movdqa	-9456(%rbp), %xmm4
	movdqa	-9520(%rbp), %xmm3
	movq	%r12, %rdi
	movaps	%xmm7, -240(%rbp)
	movdqa	-9536(%rbp), %xmm6
	movdqa	-9568(%rbp), %xmm7
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm5, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm0, -8656(%rbp)
	movq	$0, -8640(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8960(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1689
	call	_ZdlPv@PLT
.L1689:
	movq	-9376(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1688
	.p2align 4,,10
	.p2align 3
.L2063:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-8944(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-9232(%rbp), %xmm6
	movdqa	-9248(%rbp), %xmm7
	movdqa	-9440(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	leaq	-96(%rbp), %rdx
	movdqa	-9520(%rbp), %xmm1
	movdqa	-9536(%rbp), %xmm5
	movaps	%xmm3, -240(%rbp)
	movq	%r12, %rdi
	movdqa	-9568(%rbp), %xmm4
	movdqa	-9616(%rbp), %xmm3
	movaps	%xmm6, -224(%rbp)
	movdqa	-9600(%rbp), %xmm6
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm0, -8656(%rbp)
	movq	$0, -8640(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8992(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1674
	call	_ZdlPv@PLT
.L1674:
	movq	-9456(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1673
	.p2align 4,,10
	.p2align 3
.L2065:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-128(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-8688(%rbp), %xmm0
	movq	-8704(%rbp), %xmm1
	movq	$0, -8640(%rbp)
	movq	-8720(%rbp), %xmm2
	movq	-8736(%rbp), %xmm3
	movhps	-8680(%rbp), %xmm0
	movhps	-8696(%rbp), %xmm1
	movq	-8752(%rbp), %xmm4
	movq	-8768(%rbp), %xmm5
	movaps	%xmm0, -160(%rbp)
	movhps	-8712(%rbp), %xmm2
	movq	-8728(%rbp), %xmm0
	movhps	-8728(%rbp), %xmm3
	movhps	-8744(%rbp), %xmm4
	movhps	-8760(%rbp), %xmm5
	movaps	%xmm3, -208(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm5, -240(%rbp)
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -224(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -8656(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1392(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1713
	call	_ZdlPv@PLT
.L1713:
	movq	-9304(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1712
.L2059:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22545:
	.size	_ZN2v88internal23FastArrayReduceRight_26EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_NS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableISC_EEPNSI_ISE_EE, .-_ZN2v88internal23FastArrayReduceRight_26EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_NS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableISC_EEPNSI_ISE_EE
	.section	.text._ZN2v88internal25ArrayReduceRightAssembler28GenerateArrayReduceRightImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25ArrayReduceRightAssembler28GenerateArrayReduceRightImplEv
	.type	_ZN2v88internal25ArrayReduceRightAssembler28GenerateArrayReduceRightImplEv, @function
_ZN2v88internal25ArrayReduceRightAssembler28GenerateArrayReduceRightImplEv:
.LFB22636:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$3256, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, -3016(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r13, %rcx
	movq	%r12, %rsi
	leaq	-2976(%rbp), %rdi
	movq	%rax, %rdx
	leaq	-2672(%rbp), %r13
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-2968(%rbp), %rcx
	movl	$2, %esi
	movq	%r12, %rdi
	movq	-2976(%rbp), %rax
	movq	-2960(%rbp), %rbx
	movq	%r12, -2848(%rbp)
	leaq	-3016(%rbp), %r12
	movq	%rcx, -3168(%rbp)
	movq	%rcx, -2824(%rbp)
	movq	$1, -2840(%rbp)
	movq	%rbx, -2832(%rbp)
	movq	%rax, -3184(%rbp)
	movq	%rax, -2816(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-2848(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -3136(%rbp)
	movq	%rax, -3152(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -2664(%rbp)
	movq	$0, -2656(%rbp)
	movq	%rax, %r15
	movq	-3016(%rbp), %rax
	movq	$0, -2648(%rbp)
	movq	%rax, -2672(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2616(%rbp), %rcx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -2648(%rbp)
	movq	%rdx, -2656(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3032(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2632(%rbp)
	movq	%rax, -2664(%rbp)
	movq	$0, -2640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3016(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2472(%rbp)
	movq	$0, -2464(%rbp)
	movq	%rax, -2480(%rbp)
	movq	$0, -2456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2424(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -2456(%rbp)
	movq	%rdx, -2464(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3104(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2440(%rbp)
	movq	%rax, -2472(%rbp)
	movq	$0, -2448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3016(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2280(%rbp)
	movq	$0, -2272(%rbp)
	movq	%rax, -2288(%rbp)
	movq	$0, -2264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2232(%rbp), %rcx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -2264(%rbp)
	movq	%rdx, -2272(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3056(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2248(%rbp)
	movq	%rax, -2280(%rbp)
	movq	$0, -2256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3016(%rbp), %rax
	movl	$192, %edi
	movq	$0, -2088(%rbp)
	movq	$0, -2080(%rbp)
	movq	%rax, -2096(%rbp)
	movq	$0, -2072(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-2040(%rbp), %rcx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -2072(%rbp)
	movq	%rdx, -2080(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3120(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -2056(%rbp)
	movq	%rax, -2088(%rbp)
	movq	$0, -2064(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3016(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1896(%rbp)
	movq	$0, -1888(%rbp)
	movq	%rax, -1904(%rbp)
	movq	$0, -1880(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1848(%rbp), %rcx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -1880(%rbp)
	movq	%rdx, -1888(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3096(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1864(%rbp)
	movq	%rax, -1896(%rbp)
	movq	$0, -1872(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3016(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1704(%rbp)
	movq	$0, -1696(%rbp)
	movq	%rax, -1712(%rbp)
	movq	$0, -1688(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1656(%rbp), %rcx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -1688(%rbp)
	movq	%rdx, -1696(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3112(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1672(%rbp)
	movq	%rax, -1704(%rbp)
	movq	$0, -1680(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3016(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1512(%rbp)
	movq	$0, -1504(%rbp)
	movq	%rax, -1520(%rbp)
	movq	$0, -1496(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1464(%rbp), %rcx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -1496(%rbp)
	movq	%rdx, -1504(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3064(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1480(%rbp)
	movq	%rax, -1512(%rbp)
	movq	$0, -1488(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3016(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1320(%rbp)
	movq	$0, -1312(%rbp)
	movq	%rax, -1328(%rbp)
	movq	$0, -1304(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1272(%rbp), %rcx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -1304(%rbp)
	movq	%rdx, -1312(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3080(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1288(%rbp)
	movq	%rax, -1320(%rbp)
	movq	$0, -1296(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3016(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1128(%rbp)
	movq	$0, -1120(%rbp)
	movq	%rax, -1136(%rbp)
	movq	$0, -1112(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1080(%rbp), %rcx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -1112(%rbp)
	movq	%rdx, -1120(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3040(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1096(%rbp)
	movq	%rax, -1128(%rbp)
	movq	$0, -1104(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3016(%rbp), %rax
	movl	$360, %edi
	movq	$0, -936(%rbp)
	movq	$0, -928(%rbp)
	movq	%rax, -944(%rbp)
	movq	$0, -920(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-888(%rbp), %rcx
	movq	%r12, %rsi
	leaq	360(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 352(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movq	%rdx, -920(%rbp)
	movq	%rdx, -928(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3128(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -904(%rbp)
	movq	%rax, -936(%rbp)
	movq	$0, -912(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3016(%rbp), %rax
	movl	$336, %edi
	movq	$0, -744(%rbp)
	movq	$0, -736(%rbp)
	movq	%rax, -752(%rbp)
	movq	$0, -728(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-696(%rbp), %rcx
	movq	%r12, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rdx, -728(%rbp)
	movq	%rdx, -736(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3072(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -712(%rbp)
	movq	%rax, -744(%rbp)
	movq	$0, -720(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3016(%rbp), %rax
	movl	$264, %edi
	movq	$0, -552(%rbp)
	movq	$0, -544(%rbp)
	movq	%rax, -560(%rbp)
	movq	$0, -536(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-504(%rbp), %rcx
	movq	%r12, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rdx, -536(%rbp)
	movq	%rdx, -544(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3088(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -520(%rbp)
	movq	%rax, -552(%rbp)
	movq	$0, -528(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3016(%rbp), %rax
	movl	$120, %edi
	movq	$0, -360(%rbp)
	movq	$0, -352(%rbp)
	movq	%rax, -368(%rbp)
	movq	$0, -344(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	leaq	-312(%rbp), %rcx
	movq	$0, 112(%rax)
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -344(%rbp)
	movq	%rdx, -352(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -3048(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -360(%rbp)
	movups	%xmm0, -328(%rbp)
	movq	$0, -336(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movq	-3184(%rbp), %xmm1
	movl	$40, %edi
	movq	%r15, -144(%rbp)
	leaq	-2800(%rbp), %r15
	movhps	-3168(%rbp), %xmm1
	movaps	%xmm0, -2800(%rbp)
	movaps	%xmm1, -176(%rbp)
	movq	%rbx, %xmm1
	movhps	-3152(%rbp), %xmm1
	movq	$0, -2784(%rbp)
	movaps	%xmm1, -160(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r13, %rdi
	movq	%r15, %rsi
	movdqa	-176(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm4
	leaq	40(%rax), %rdx
	movq	%rax, -2800(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2067
	call	_ZdlPv@PLT
.L2067:
	movq	-3032(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2608(%rbp)
	jne	.L2365
	cmpq	$0, -2416(%rbp)
	jne	.L2366
.L2072:
	cmpq	$0, -2224(%rbp)
	jne	.L2367
.L2075:
	cmpq	$0, -2032(%rbp)
	jne	.L2368
.L2080:
	cmpq	$0, -1840(%rbp)
	jne	.L2369
.L2083:
	cmpq	$0, -1648(%rbp)
	jne	.L2370
.L2087:
	cmpq	$0, -1456(%rbp)
	jne	.L2371
.L2090:
	cmpq	$0, -1264(%rbp)
	jne	.L2372
.L2093:
	cmpq	$0, -1072(%rbp)
	jne	.L2373
.L2096:
	cmpq	$0, -880(%rbp)
	jne	.L2374
.L2101:
	cmpq	$0, -688(%rbp)
	jne	.L2375
.L2104:
	cmpq	$0, -496(%rbp)
	jne	.L2376
.L2106:
	cmpq	$0, -304(%rbp)
	jne	.L2377
.L2108:
	movq	-3048(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2110
	call	_ZdlPv@PLT
.L2110:
	movq	-352(%rbp), %rbx
	movq	-360(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2111
	.p2align 4,,10
	.p2align 3
.L2115:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2112
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2115
.L2113:
	movq	-360(%rbp), %r13
.L2111:
	testq	%r13, %r13
	je	.L2116
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2116:
	movq	-3088(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2117
	call	_ZdlPv@PLT
.L2117:
	movq	-544(%rbp), %rbx
	movq	-552(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2118
	.p2align 4,,10
	.p2align 3
.L2122:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2119
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2122
.L2120:
	movq	-552(%rbp), %r13
.L2118:
	testq	%r13, %r13
	je	.L2123
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2123:
	movq	-3072(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-720(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2124
	call	_ZdlPv@PLT
.L2124:
	movq	-736(%rbp), %rbx
	movq	-744(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2125
	.p2align 4,,10
	.p2align 3
.L2129:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2126
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2129
.L2127:
	movq	-744(%rbp), %r13
.L2125:
	testq	%r13, %r13
	je	.L2130
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2130:
	movq	-3128(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-912(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2131
	call	_ZdlPv@PLT
.L2131:
	movq	-928(%rbp), %rbx
	movq	-936(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2132
	.p2align 4,,10
	.p2align 3
.L2136:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2133
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2136
.L2134:
	movq	-936(%rbp), %r13
.L2132:
	testq	%r13, %r13
	je	.L2137
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2137:
	movq	-3040(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2138
	call	_ZdlPv@PLT
.L2138:
	movq	-1120(%rbp), %rbx
	movq	-1128(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2139
	.p2align 4,,10
	.p2align 3
.L2143:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2140
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2143
.L2141:
	movq	-1128(%rbp), %r13
.L2139:
	testq	%r13, %r13
	je	.L2144
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2144:
	movq	-3080(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1296(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2145
	call	_ZdlPv@PLT
.L2145:
	movq	-1312(%rbp), %rbx
	movq	-1320(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2146
	.p2align 4,,10
	.p2align 3
.L2150:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2147
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2150
.L2148:
	movq	-1320(%rbp), %r13
.L2146:
	testq	%r13, %r13
	je	.L2151
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2151:
	movq	-3064(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1488(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2152
	call	_ZdlPv@PLT
.L2152:
	movq	-1504(%rbp), %rbx
	movq	-1512(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2153
	.p2align 4,,10
	.p2align 3
.L2157:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2154
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2157
.L2155:
	movq	-1512(%rbp), %r13
.L2153:
	testq	%r13, %r13
	je	.L2158
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2158:
	movq	-3112(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1680(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2159
	call	_ZdlPv@PLT
.L2159:
	movq	-1696(%rbp), %rbx
	movq	-1704(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2160
	.p2align 4,,10
	.p2align 3
.L2164:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2161
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L2164
.L2162:
	movq	-1704(%rbp), %r13
.L2160:
	testq	%r13, %r13
	je	.L2165
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2165:
	movq	-3096(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1872(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2166
	call	_ZdlPv@PLT
.L2166:
	movq	-1888(%rbp), %rbx
	movq	-1896(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2167
	.p2align 4,,10
	.p2align 3
.L2171:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2168
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L2171
.L2169:
	movq	-1896(%rbp), %r13
.L2167:
	testq	%r13, %r13
	je	.L2172
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2172:
	movq	-3120(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2173
	call	_ZdlPv@PLT
.L2173:
	movq	-2080(%rbp), %rbx
	movq	-2088(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2174
	.p2align 4,,10
	.p2align 3
.L2178:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2175
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L2178
.L2176:
	movq	-2088(%rbp), %r13
.L2174:
	testq	%r13, %r13
	je	.L2179
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2179:
	movq	-3056(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2180
	call	_ZdlPv@PLT
.L2180:
	movq	-2272(%rbp), %rbx
	movq	-2280(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2181
	.p2align 4,,10
	.p2align 3
.L2185:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2182
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2185
.L2183:
	movq	-2280(%rbp), %r13
.L2181:
	testq	%r13, %r13
	je	.L2186
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2186:
	movq	-3104(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2187
	call	_ZdlPv@PLT
.L2187:
	movq	-2464(%rbp), %rbx
	movq	-2472(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2188
	.p2align 4,,10
	.p2align 3
.L2192:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2189
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2192
.L2190:
	movq	-2472(%rbp), %r13
.L2188:
	testq	%r13, %r13
	je	.L2193
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2193:
	movq	-3032(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2194
	call	_ZdlPv@PLT
.L2194:
	movq	-2656(%rbp), %rbx
	movq	-2664(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2195
	.p2align 4,,10
	.p2align 3
.L2199:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2196
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L2199
.L2197:
	movq	-2664(%rbp), %r13
.L2195:
	testq	%r13, %r13
	je	.L2200
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2200:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2378
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2196:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2199
	jmp	.L2197
	.p2align 4,,10
	.p2align 3
.L2189:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2192
	jmp	.L2190
	.p2align 4,,10
	.p2align 3
.L2182:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2185
	jmp	.L2183
	.p2align 4,,10
	.p2align 3
.L2175:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2178
	jmp	.L2176
	.p2align 4,,10
	.p2align 3
.L2168:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2171
	jmp	.L2169
	.p2align 4,,10
	.p2align 3
.L2161:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2164
	jmp	.L2162
	.p2align 4,,10
	.p2align 3
.L2154:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2157
	jmp	.L2155
	.p2align 4,,10
	.p2align 3
.L2147:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2150
	jmp	.L2148
	.p2align 4,,10
	.p2align 3
.L2140:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2143
	jmp	.L2141
	.p2align 4,,10
	.p2align 3
.L2133:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2136
	jmp	.L2134
	.p2align 4,,10
	.p2align 3
.L2126:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2129
	jmp	.L2127
	.p2align 4,,10
	.p2align 3
.L2112:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2115
	jmp	.L2113
	.p2align 4,,10
	.p2align 3
.L2119:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2122
	jmp	.L2120
	.p2align 4,,10
	.p2align 3
.L2365:
	movq	-3032(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2800(%rbp)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2069
	call	_ZdlPv@PLT
.L2069:
	movq	(%rbx), %rax
	movl	$163, %edx
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	movq	24(%rax), %r13
	movq	32(%rax), %rbx
	movq	%rcx, -3216(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -3232(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -3168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	.LC7(%rip), %rcx
	call	_ZN2v88internal26RequireObjectCoercible_241EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPKc@PLT
	movl	$166, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ToObject_InlineENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	movq	%rax, -3184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$169, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3184(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal21GetLengthProperty_244EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$172, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -3152(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, -3200(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3200(%rbp), %rdx
	movq	-3168(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, -3200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %xmm6
	leaq	-120(%rbp), %r13
	movq	%r15, %rdi
	movq	-3168(%rbp), %xmm3
	movq	-3152(%rbp), %rax
	movq	%rbx, %xmm2
	movq	%r13, %rdx
	movq	-3216(%rbp), %xmm7
	leaq	-176(%rbp), %rbx
	movhps	-3184(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	punpcklqdq	%xmm6, %xmm3
	movq	%rbx, %rsi
	movq	%rax, -128(%rbp)
	movhps	-3232(%rbp), %xmm7
	movaps	%xmm2, -3248(%rbp)
	movaps	%xmm3, -3168(%rbp)
	movaps	%xmm7, -3184(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm0, -2800(%rbp)
	movq	$0, -2784(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2480(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2070
	call	_ZdlPv@PLT
.L2070:
	movq	-3152(%rbp), %rax
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movdqa	-3184(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movdqa	-3168(%rbp), %xmm4
	movq	$0, -2784(%rbp)
	movq	%rax, -128(%rbp)
	movaps	%xmm6, -176(%rbp)
	movdqa	-3248(%rbp), %xmm6
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2288(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2071
	call	_ZdlPv@PLT
.L2071:
	movq	-3056(%rbp), %rcx
	movq	-3104(%rbp), %rdx
	movq	%r12, %rdi
	movq	-3200(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2416(%rbp)
	je	.L2072
.L2366:
	movq	-3104(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2480(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_Znwm@PLT
	movl	$1800, %r9d
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$117769477, (%rax)
	leaq	7(%rax), %rdx
	movw	%r9w, 4(%rax)
	movb	$8, 6(%rax)
	movq	%rax, -2800(%rbp)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2073
	call	_ZdlPv@PLT
.L2073:
	movq	(%rbx), %rax
	movl	$173, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	16(%rax), %rsi
	movq	(%rax), %r13
	movq	32(%rax), %rbx
	movq	%rcx, -3152(%rbp)
	movq	24(%rax), %rcx
	movq	%rsi, -3168(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -3184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %xmm0
	leaq	-176(%rbp), %rsi
	leaq	-136(%rbp), %rdx
	movhps	-3152(%rbp), %xmm0
	movq	%r15, %rdi
	movq	%rbx, -144(%rbp)
	movaps	%xmm0, -176(%rbp)
	movq	-3168(%rbp), %xmm0
	movq	$0, -2784(%rbp)
	movhps	-3184(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2800(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-368(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2074
	call	_ZdlPv@PLT
.L2074:
	movq	-3048(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2224(%rbp)
	je	.L2075
.L2367:
	movq	-3056(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2288(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_Znwm@PLT
	movl	$1800, %r8d
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$117769477, (%rax)
	leaq	7(%rax), %rdx
	movw	%r8w, 4(%rax)
	movb	$8, 6(%rax)
	movq	%rax, -2800(%rbp)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2076
	call	_ZdlPv@PLT
.L2076:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rsi
	movq	32(%rax), %rdx
	movq	24(%rax), %r13
	movq	40(%rax), %rbx
	movq	%rcx, -3184(%rbp)
	movq	16(%rax), %rcx
	movq	48(%rax), %rax
	movq	%rsi, -3168(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -3200(%rbp)
	movl	$175, %edx
	movq	%rcx, -3152(%rbp)
	movq	%rbx, -3216(%rbp)
	movq	%rax, -3232(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	-3168(%rbp), %xmm5
	movq	-3152(%rbp), %rcx
	movhps	-3184(%rbp), %xmm5
	movq	%rcx, -2928(%rbp)
	movaps	%xmm5, -2944(%rbp)
	pushq	-2928(%rbp)
	pushq	-2936(%rbp)
	pushq	-2944(%rbp)
	movaps	%xmm5, -3168(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal82Cast71UT19ATCallableApiObject17ATCallableJSProxy15JSBoundFunction10JSFunction_1405EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm6
	leaq	-176(%rbp), %rbx
	movq	-3232(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	leaq	-104(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%rax, -112(%rbp)
	movq	-3152(%rbp), %xmm3
	punpcklqdq	%xmm6, %xmm4
	movq	-3200(%rbp), %xmm2
	movq	%r13, %xmm6
	movdqa	-3168(%rbp), %xmm5
	leaq	-2880(%rbp), %r13
	movaps	%xmm4, -128(%rbp)
	movhps	-3216(%rbp), %xmm2
	punpcklqdq	%xmm6, %xmm3
	movq	%r13, %rdi
	movaps	%xmm4, -3232(%rbp)
	movaps	%xmm2, -3184(%rbp)
	movaps	%xmm3, -3152(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm0, -2880(%rbp)
	movq	$0, -2864(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1904(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2077
	call	_ZdlPv@PLT
.L2077:
	movq	-3096(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2792(%rbp)
	jne	.L2379
.L2078:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2032(%rbp)
	je	.L2080
.L2368:
	movq	-3120(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2096(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movabsq	$578720283176011013, %rsi
	movq	%rsi, (%rax)
	leaq	8(%rax), %rdx
	movq	%r15, %rsi
	movq	%rax, -2800(%rbp)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2081
	call	_ZdlPv@PLT
.L2081:
	movq	(%rbx), %rax
	leaq	-176(%rbp), %rsi
	leaq	-136(%rbp), %rdx
	movq	%r15, %rdi
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm1
	movq	32(%rax), %rax
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -144(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-368(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2082
	call	_ZdlPv@PLT
.L2082:
	movq	-3048(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1840(%rbp)
	je	.L2083
.L2369:
	movq	-3096(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-176(%rbp), %rbx
	leaq	-1904(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%r15, %rdi
	leaq	-167(%rbp), %rdx
	movaps	%xmm0, -2800(%rbp)
	movabsq	$578720283176011013, %rax
	movq	%rax, -176(%rbp)
	movb	$7, -168(%rbp)
	movq	$0, -2784(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L2084
	call	_ZdlPv@PLT
.L2084:
	movq	0(%r13), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	40(%rax), %rdx
	movq	(%rax), %rcx
	movq	16(%rax), %r13
	movq	%rsi, -3184(%rbp)
	movq	24(%rax), %rsi
	movq	%rdx, -3232(%rbp)
	movq	48(%rax), %rdx
	movq	%rsi, -3200(%rbp)
	movq	32(%rax), %rsi
	movq	64(%rax), %rax
	movq	%rdx, -3248(%rbp)
	movl	$182, %edx
	movq	%rsi, -3216(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -3168(%rbp)
	movq	%rax, -3256(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, -3152(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3152(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17IntPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, -3152(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %xmm7
	movq	%rbx, %rsi
	movq	%r15, %rdi
	leaq	-112(%rbp), %r13
	movhps	-3200(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movq	-3248(%rbp), %xmm5
	movq	-3216(%rbp), %xmm6
	movq	%r13, %rdx
	movq	-3168(%rbp), %xmm4
	movaps	%xmm7, -3200(%rbp)
	movhps	-3256(%rbp), %xmm5
	movaps	%xmm7, -160(%rbp)
	movhps	-3232(%rbp), %xmm6
	movhps	-3184(%rbp), %xmm4
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm5, -3248(%rbp)
	movaps	%xmm6, -3216(%rbp)
	movaps	%xmm4, -3168(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm0, -2800(%rbp)
	movq	$0, -2784(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1712(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2085
	call	_ZdlPv@PLT
.L2085:
	pxor	%xmm0, %xmm0
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movdqa	-3168(%rbp), %xmm4
	movdqa	-3200(%rbp), %xmm5
	movaps	%xmm0, -2800(%rbp)
	movdqa	-3248(%rbp), %xmm2
	movq	$0, -2784(%rbp)
	movaps	%xmm4, -176(%rbp)
	movdqa	-3216(%rbp), %xmm4
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1520(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2086
	call	_ZdlPv@PLT
.L2086:
	movq	-3064(%rbp), %rcx
	movq	-3112(%rbp), %rdx
	movq	%r12, %rdi
	movq	-3152(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1648(%rbp)
	je	.L2087
.L2370:
	movq	-3112(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1712(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movabsq	$506662689138083077, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2800(%rbp)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2088
	call	_ZdlPv@PLT
.L2088:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	8(%rax), %rsi
	movq	40(%rax), %rdx
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -3168(%rbp)
	movq	24(%rax), %rsi
	movq	%rdx, -3216(%rbp)
	movq	48(%rax), %rdx
	movq	%rsi, -3184(%rbp)
	movq	32(%rax), %rsi
	movq	56(%rax), %rax
	movq	%rcx, -3152(%rbp)
	movq	%rsi, -3200(%rbp)
	movl	$1, %esi
	movq	%rdx, -3232(%rbp)
	movq	%rax, -3248(%rbp)
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	-3152(%rbp), %xmm0
	movq	%rbx, -2896(%rbp)
	pushq	-2896(%rbp)
	movhps	-3168(%rbp), %xmm0
	movaps	%xmm0, -2912(%rbp)
	pushq	-2904(%rbp)
	pushq	-2912(%rbp)
	movaps	%xmm0, -3152(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-104(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r13, -112(%rbp)
	movdqa	-3152(%rbp), %xmm0
	leaq	-176(%rbp), %rsi
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -176(%rbp)
	movq	%rbx, %xmm0
	movhps	-3184(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-3200(%rbp), %xmm0
	movhps	-3216(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-3232(%rbp), %xmm0
	movhps	-3248(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2800(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1328(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2089
	call	_ZdlPv@PLT
.L2089:
	movq	-3080(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1456(%rbp)
	je	.L2090
.L2371:
	movq	-3064(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1520(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movabsq	$506662689138083077, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2800(%rbp)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2091
	call	_ZdlPv@PLT
.L2091:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	8(%rax), %rsi
	movq	24(%rax), %rdx
	movq	(%rax), %rcx
	movq	32(%rax), %r13
	movq	48(%rax), %rbx
	movq	%rsi, -3168(%rbp)
	movq	%rdx, -3200(%rbp)
	movq	16(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rax
	movq	%rcx, -3152(%rbp)
	movq	%rsi, -3184(%rbp)
	movq	%rdx, -3216(%rbp)
	movq	%rax, -3232(%rbp)
	call	_ZN2v88internal10TheHole_62EPNS0_8compiler18CodeAssemblerStateE@PLT
	leaq	-176(%rbp), %rsi
	leaq	-104(%rbp), %rdx
	movq	%r15, %rdi
	movq	-3152(%rbp), %xmm0
	movq	%rax, -112(%rbp)
	movq	$0, -2784(%rbp)
	movhps	-3168(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-3184(%rbp), %xmm0
	movhps	-3200(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%r13, %xmm0
	movhps	-3216(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	movhps	-3232(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2800(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1136(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2092
	call	_ZdlPv@PLT
.L2092:
	movq	-3040(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1264(%rbp)
	je	.L2093
.L2372:
	movq	-3080(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1328(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movabsq	$506662689138083077, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -2800(%rbp)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2094
	call	_ZdlPv@PLT
.L2094:
	movq	(%rbx), %rax
	leaq	-176(%rbp), %rsi
	leaq	-104(%rbp), %rdx
	movq	%r15, %rdi
	movdqu	48(%rax), %xmm0
	movdqu	(%rax), %xmm3
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	64(%rax), %rax
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -176(%rbp)
	movq	%rax, -112(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -2800(%rbp)
	movq	$0, -2784(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1136(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2095
	call	_ZdlPv@PLT
.L2095:
	movq	-3040(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1072(%rbp)
	je	.L2096
.L2373:
	movq	-3040(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1136(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movabsq	$506662689138083077, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -2800(%rbp)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2097
	call	_ZdlPv@PLT
.L2097:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	movq	40(%rax), %rdx
	movq	32(%rax), %rbx
	movq	%rcx, -3232(%rbp)
	movq	24(%rax), %rcx
	movq	%rsi, -3248(%rbp)
	movq	16(%rax), %rsi
	movq	%rcx, -3216(%rbp)
	movq	64(%rax), %rcx
	movq	%rsi, -3256(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	56(%rax), %r13
	movq	%rdx, -3152(%rbp)
	movl	$181, %edx
	movq	%rcx, -3168(%rbp)
	movq	%rbx, -3264(%rbp)
	movq	48(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$185, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-3008(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3184(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	leaq	-2992(%rbp), %r10
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r10, %rdi
	movq	%r10, -3200(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	subq	$8, %rsp
	pushq	-3200(%rbp)
	movq	-3168(%rbp), %r9
	pushq	-3184(%rbp)
	movq	%r13, %r8
	movq	%rbx, %rcx
	movq	%r14, %rdi
	movq	-3152(%rbp), %rdx
	pushq	%r15
	movq	-3216(%rbp), %rsi
	call	_ZN2v88internal23FastArrayReduceRight_26EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_NS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableISC_EEPNSI_ISE_EE
	movq	-3168(%rbp), %rcx
	movq	%r13, %xmm3
	movq	%rbx, %xmm6
	punpcklqdq	%xmm3, %xmm6
	pxor	%xmm0, %xmm0
	addq	$32, %rsp
	movq	%rax, -72(%rbp)
	movq	%rcx, %xmm5
	leaq	-2880(%rbp), %r13
	leaq	-176(%rbp), %rbx
	movq	-3232(%rbp), %xmm7
	leaq	-64(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rcx, -80(%rbp)
	movq	-3264(%rbp), %xmm2
	movhps	-3152(%rbp), %xmm5
	movaps	%xmm6, -128(%rbp)
	movq	-3256(%rbp), %xmm3
	movhps	-3248(%rbp), %xmm7
	movaps	%xmm6, -3280(%rbp)
	movhps	-3152(%rbp), %xmm2
	movhps	-3216(%rbp), %xmm3
	movaps	%xmm5, -3296(%rbp)
	movaps	%xmm2, -3152(%rbp)
	movaps	%xmm3, -3216(%rbp)
	movaps	%xmm7, -3232(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm0, -2880(%rbp)
	movq	$0, -2864(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-752(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2098
	call	_ZdlPv@PLT
.L2098:
	movq	-3072(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2792(%rbp)
	jne	.L2380
.L2099:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3200(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	movq	-3184(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	cmpq	$0, -880(%rbp)
	je	.L2101
.L2374:
	movq	-3128(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-944(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$15, %edi
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_Znwm@PLT
	movl	$2056, %edi
	movq	%r15, %rsi
	movabsq	$506662689138083077, %rcx
	movw	%di, 12(%rax)
	leaq	15(%rax), %rdx
	movq	%r13, %rdi
	movq	%rcx, (%rax)
	movl	$117966600, 8(%rax)
	movb	$8, 14(%rax)
	movq	%rax, -2800(%rbp)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2102
	call	_ZdlPv@PLT
.L2102:
	movq	(%rbx), %rax
	leaq	-176(%rbp), %rsi
	leaq	-88(%rbp), %rdx
	movq	%r15, %rdi
	movdqu	96(%rax), %xmm5
	movdqu	64(%rax), %xmm0
	movdqu	(%rax), %xmm4
	movdqu	16(%rax), %xmm3
	movdqu	32(%rax), %xmm2
	movdqu	48(%rax), %xmm1
	shufpd	$2, %xmm5, %xmm0
	movq	112(%rax), %rax
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -176(%rbp)
	movq	%rax, -96(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -2800(%rbp)
	movq	$0, -2784(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-560(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2103
	call	_ZdlPv@PLT
.L2103:
	movq	-3088(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -688(%rbp)
	je	.L2104
.L2375:
	movq	-3072(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-752(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$2056, %esi
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movw	%si, -164(%rbp)
	leaq	-162(%rbp), %rdx
	movabsq	$506662689138083077, %rax
	leaq	-176(%rbp), %rsi
	movq	%rax, -176(%rbp)
	movaps	%xmm0, -2800(%rbp)
	movl	$117966600, -168(%rbp)
	movq	$0, -2784(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2105
	call	_ZdlPv@PLT
.L2105:
	movq	(%rbx), %rax
	movq	-3136(%rbp), %rdi
	movq	104(%rax), %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	cmpq	$0, -496(%rbp)
	je	.L2106
.L2376:
	movq	-3088(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-560(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$11, %edi
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movabsq	$506662689138083077, %rcx
	movq	%rcx, (%rax)
	movl	$2056, %ecx
	leaq	11(%rax), %rdx
	movw	%cx, 8(%rax)
	movb	$8, 10(%rax)
	movq	%rax, -2800(%rbp)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2107
	call	_ZdlPv@PLT
.L2107:
	movq	(%rbx), %rax
	movl	$190, %edx
	movq	%r12, %rdi
	leaq	-2992(%rbp), %r13
	movq	48(%rax), %rcx
	movq	40(%rax), %rsi
	movq	24(%rax), %r9
	movq	72(%rax), %rbx
	movq	%rcx, -3168(%rbp)
	movq	56(%rax), %rcx
	movq	80(%rax), %rax
	movq	%rsi, -3152(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%r9, -3216(%rbp)
	movq	%rcx, -3184(%rbp)
	movq	%rax, -3200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$189, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$769, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-2800(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movq	-3216(%rbp), %r9
	xorl	%esi, %esi
	movq	-3152(%rbp), %xmm0
	movq	%rax, %r8
	movq	%r13, %rdi
	movq	%rcx, -2880(%rbp)
	leaq	-176(%rbp), %rcx
	movq	-2784(%rbp), %rax
	leaq	-2880(%rbp), %rdx
	movhps	-3184(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-3200(%rbp), %xmm0
	movq	%rax, -2872(%rbp)
	movhps	-3152(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%rbx, %xmm0
	movl	$6, %ebx
	pushq	%rbx
	movhps	-3168(%rbp), %xmm0
	pushq	%rcx
	movl	$1, %ecx
	movaps	%xmm0, -144(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3136(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	cmpq	$0, -304(%rbp)
	popq	%rax
	popq	%rdx
	je	.L2108
.L2377:
	movq	-3048(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-368(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2800(%rbp)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2109
	call	_ZdlPv@PLT
.L2109:
	movq	(%rbx), %rax
	movl	$194, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	movq	8(%rax), %rcx
	movq	16(%rax), %rbx
	movq	24(%rax), %r15
	movq	%rsi, -3152(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -3168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	leaq	-2880(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rax, %r13
	movq	%rdi, -3184(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%rbx, -2784(%rbp)
	movq	%r13, %rsi
	movq	-3152(%rbp), %xmm0
	pushq	-2784(%rbp)
	movq	-3184(%rbp), %rdi
	movhps	-3168(%rbp), %xmm0
	movaps	%xmm0, -2800(%rbp)
	pushq	-2792(%rbp)
	pushq	-2800(%rbp)
	movq	%rdi, -3152(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	movq	-3152(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3152(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	-3152(%rbp), %rdi
	movl	$25, %edx
	movq	%r15, %rsi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	-3152(%rbp), %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L2108
	.p2align 4,,10
	.p2align 3
.L2379:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-112(%rbp), %rdx
	movq	%rbx, %rsi
	movdqa	-3168(%rbp), %xmm6
	movdqa	-3184(%rbp), %xmm2
	movq	%r13, %rdi
	movaps	%xmm0, -2880(%rbp)
	movdqa	-3232(%rbp), %xmm3
	movq	$0, -2864(%rbp)
	movaps	%xmm6, -176(%rbp)
	movdqa	-3152(%rbp), %xmm6
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm3, -128(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2096(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2079
	call	_ZdlPv@PLT
.L2079:
	movq	-3120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2078
	.p2align 4,,10
	.p2align 3
.L2380:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3200(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movq	-3184(%rbp), %rdi
	movq	%rax, -3248(%rbp)
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movdqa	-3280(%rbp), %xmm4
	movq	%rbx, %rsi
	movq	-3168(%rbp), %xmm0
	movq	-3248(%rbp), %rdx
	movdqa	-3232(%rbp), %xmm5
	movq	%r13, %rdi
	movq	$0, -2864(%rbp)
	movaps	%xmm4, -128(%rbp)
	movdqa	-3216(%rbp), %xmm7
	movdqa	-3152(%rbp), %xmm6
	movaps	%xmm4, -96(%rbp)
	movq	%rax, %xmm4
	movdqa	-3296(%rbp), %xmm2
	punpcklqdq	%xmm4, %xmm0
	movq	%rdx, -64(%rbp)
	leaq	-56(%rbp), %rdx
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm0, -2880(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-944(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2100
	call	_ZdlPv@PLT
.L2100:
	movq	-3128(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2099
.L2378:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22636:
	.size	_ZN2v88internal25ArrayReduceRightAssembler28GenerateArrayReduceRightImplEv, .-_ZN2v88internal25ArrayReduceRightAssembler28GenerateArrayReduceRightImplEv
	.section	.rodata._ZN2v88internal8Builtins25Generate_ArrayReduceRightEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"ArrayReduceRight"
	.section	.text._ZN2v88internal8Builtins25Generate_ArrayReduceRightEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins25Generate_ArrayReduceRightEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins25Generate_ArrayReduceRightEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins25Generate_ArrayReduceRightEPNS0_8compiler18CodeAssemblerStateE:
.LFB22632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$2221, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$770, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L2385
.L2382:
	movq	%r13, %rdi
	call	_ZN2v88internal25ArrayReduceRightAssembler28GenerateArrayReduceRightImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2386
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2385:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L2382
.L2386:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22632:
	.size	_ZN2v88internal8Builtins25Generate_ArrayReduceRightEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins25Generate_ArrayReduceRightEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins54Generate_ArrayReduceRightPreLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins54Generate_ArrayReduceRightPreLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins54Generate_ArrayReduceRightPreLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE:
.LFB30037:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE30037:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins54Generate_ArrayReduceRightPreLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins54Generate_ArrayReduceRightPreLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins54Generate_ArrayReduceRightPreLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.weak	_ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE:
	.byte	8
	.byte	7
	.weak	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC9:
	.byte	7
	.byte	7
	.byte	8
	.byte	7
	.byte	8
	.byte	8
	.byte	6
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	4
	.byte	6
	.byte	6
	.byte	7
	.byte	6
	.align 16
.LC11:
	.byte	7
	.byte	7
	.byte	8
	.byte	7
	.byte	8
	.byte	8
	.byte	6
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	4
	.byte	6
	.byte	8
	.byte	8
	.byte	8
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
