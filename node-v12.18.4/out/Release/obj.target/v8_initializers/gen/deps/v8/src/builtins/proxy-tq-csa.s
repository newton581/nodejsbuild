	.file	"proxy-tq-csa.cc"
	.text
	.section	.rodata._ZN2v88internal18IsRevokedProxy_319EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../../deps/v8/../../deps/v8/src/builtins/proxy.tq"
	.section	.text._ZN2v88internal18IsRevokedProxy_319EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18IsRevokedProxy_319EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEE
	.type	_ZN2v88internal18IsRevokedProxy_319EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEE, @function
_ZN2v88internal18IsRevokedProxy_319EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEE:
.LFB22410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-2152(%rbp), %r15
	leaq	-2208(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2336(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-2376(%rbp), %r12
	pushq	%rbx
	subq	$2472, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -2408(%rbp)
	movq	%rsi, -2496(%rbp)
	movq	%rdx, -2480(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -2376(%rbp)
	movq	%rdi, -2208(%rbp)
	movl	$48, %edi
	movq	$0, -2200(%rbp)
	movq	$0, -2192(%rbp)
	movq	$0, -2184(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r15, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -2184(%rbp)
	movq	%rdx, -2192(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2168(%rbp)
	movq	%rax, -2200(%rbp)
	movq	$0, -2176(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2376(%rbp), %rax
	movl	$72, %edi
	movq	$0, -2008(%rbp)
	movq	$0, -2000(%rbp)
	movq	%rax, -2016(%rbp)
	movq	$0, -1992(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -2008(%rbp)
	leaq	-1960(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1992(%rbp)
	movq	%rdx, -2000(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1976(%rbp)
	movq	%rax, -2464(%rbp)
	movq	$0, -1984(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2376(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1816(%rbp)
	movq	$0, -1808(%rbp)
	movq	%rax, -1824(%rbp)
	movq	$0, -1800(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1816(%rbp)
	leaq	-1768(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1800(%rbp)
	movq	%rdx, -1808(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1784(%rbp)
	movq	%rax, -2424(%rbp)
	movq	$0, -1792(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2376(%rbp), %rax
	movl	$48, %edi
	movq	$0, -1624(%rbp)
	movq	$0, -1616(%rbp)
	movq	%rax, -1632(%rbp)
	movq	$0, -1608(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1624(%rbp)
	leaq	-1576(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1608(%rbp)
	movq	%rdx, -1616(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1592(%rbp)
	movq	%rax, -2432(%rbp)
	movq	$0, -1600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2376(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1432(%rbp)
	movq	$0, -1424(%rbp)
	movq	%rax, -1440(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1432(%rbp)
	leaq	-1384(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -2416(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2376(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -2472(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2376(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -2456(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2376(%rbp), %rax
	movl	$72, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -2440(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2376(%rbp), %rax
	movl	$96, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -2448(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2376(%rbp), %rax
	movl	$72, %edi
	movq	$0, -472(%rbp)
	movq	$0, -464(%rbp)
	movq	%rax, -480(%rbp)
	movq	$0, -456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -472(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -440(%rbp)
	movq	%rax, -2400(%rbp)
	movq	$0, -448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2376(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -280(%rbp)
	leaq	-232(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -2392(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-2496(%rbp), %xmm1
	movaps	%xmm0, -2336(%rbp)
	movhps	-2480(%rbp), %xmm1
	movq	$0, -2320(%rbp)
	movaps	%xmm1, -2496(%rbp)
	call	_Znwm@PLT
	movdqa	-2496(%rbp), %xmm1
	movq	%r14, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2
	call	_ZdlPv@PLT
.L2:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2144(%rbp)
	jne	.L257
	cmpq	$0, -1952(%rbp)
	jne	.L258
.L8:
	cmpq	$0, -1760(%rbp)
	jne	.L259
.L11:
	cmpq	$0, -1568(%rbp)
	jne	.L260
.L14:
	cmpq	$0, -1376(%rbp)
	jne	.L261
.L17:
	cmpq	$0, -1184(%rbp)
	jne	.L262
.L22:
	cmpq	$0, -992(%rbp)
	jne	.L263
.L25:
	cmpq	$0, -800(%rbp)
	jne	.L264
.L28:
	cmpq	$0, -608(%rbp)
	jne	.L265
.L31:
	cmpq	$0, -416(%rbp)
	leaq	-288(%rbp), %r14
	jne	.L266
.L34:
	movq	-2392(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$4, 2(%rax)
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L37
	call	_ZdlPv@PLT
.L37:
	movq	(%rbx), %rax
	movq	-2392(%rbp), %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L38
	call	_ZdlPv@PLT
.L38:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L39
	.p2align 4,,10
	.p2align 3
.L43:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L40
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L43
.L41:
	movq	-280(%rbp), %r14
.L39:
	testq	%r14, %r14
	je	.L44
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L44:
	movq	-2400(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L45
	call	_ZdlPv@PLT
.L45:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L46
	.p2align 4,,10
	.p2align 3
.L50:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L47
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L50
.L48:
	movq	-472(%rbp), %r14
.L46:
	testq	%r14, %r14
	je	.L51
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L51:
	movq	-2448(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L52
	call	_ZdlPv@PLT
.L52:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L53
	.p2align 4,,10
	.p2align 3
.L57:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L54
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L57
.L55:
	movq	-664(%rbp), %r14
.L53:
	testq	%r14, %r14
	je	.L58
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L58:
	movq	-2440(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L59
	call	_ZdlPv@PLT
.L59:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L60
	.p2align 4,,10
	.p2align 3
.L64:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L61
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L64
.L62:
	movq	-856(%rbp), %r14
.L60:
	testq	%r14, %r14
	je	.L65
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L65:
	movq	-2456(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L66
	call	_ZdlPv@PLT
.L66:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L67
	.p2align 4,,10
	.p2align 3
.L71:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L68
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L71
.L69:
	movq	-1048(%rbp), %r14
.L67:
	testq	%r14, %r14
	je	.L72
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L72:
	movq	-2472(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L73
	call	_ZdlPv@PLT
.L73:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L74
	.p2align 4,,10
	.p2align 3
.L78:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L75
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L78
.L76:
	movq	-1240(%rbp), %r14
.L74:
	testq	%r14, %r14
	je	.L79
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L79:
	movq	-2416(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L80
	call	_ZdlPv@PLT
.L80:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L81
	.p2align 4,,10
	.p2align 3
.L85:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L82
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L85
.L83:
	movq	-1432(%rbp), %r14
.L81:
	testq	%r14, %r14
	je	.L86
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L86:
	movq	-2432(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L87
	call	_ZdlPv@PLT
.L87:
	movq	-1616(%rbp), %rbx
	movq	-1624(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L88
	.p2align 4,,10
	.p2align 3
.L92:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L89
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L92
.L90:
	movq	-1624(%rbp), %r14
.L88:
	testq	%r14, %r14
	je	.L93
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L93:
	movq	-2424(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L94
	call	_ZdlPv@PLT
.L94:
	movq	-1808(%rbp), %rbx
	movq	-1816(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L95
	.p2align 4,,10
	.p2align 3
.L99:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L96
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L99
.L97:
	movq	-1816(%rbp), %r14
.L95:
	testq	%r14, %r14
	je	.L100
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L100:
	movq	-2464(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L101
	call	_ZdlPv@PLT
.L101:
	movq	-2000(%rbp), %rbx
	movq	-2008(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L102
	.p2align 4,,10
	.p2align 3
.L106:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L103
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L106
.L104:
	movq	-2008(%rbp), %r14
.L102:
	testq	%r14, %r14
	je	.L107
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L107:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L108
	call	_ZdlPv@PLT
.L108:
	movq	-2192(%rbp), %rbx
	movq	-2200(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L109
	.p2align 4,,10
	.p2align 3
.L113:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L110
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L113
.L111:
	movq	-2200(%rbp), %r14
.L109:
	testq	%r14, %r14
	je	.L114
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L114:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L267
	addq	$2472, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L113
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L103:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L106
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L96:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L99
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L89:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L92
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L82:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L85
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L75:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L78
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L68:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L71
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L61:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L64
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L54:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L57
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L40:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L43
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L47:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L50
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L257:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r11d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%r11w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L4
	call	_ZdlPv@PLT
.L4:
	movq	(%rbx), %rax
	movl	$13, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rbx
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rdi
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal16Cast7JSProxy_107EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm5
	movq	%r14, %xmm2
	pxor	%xmm0, %xmm0
	punpcklqdq	%xmm5, %xmm2
	movl	$32, %edi
	movaps	%xmm0, -2368(%rbp)
	leaq	-2368(%rbp), %r14
	movaps	%xmm2, -2496(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	%rbx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -2352(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%r14, %rsi
	leaq	32(%rax), %rdx
	leaq	-1824(%rbp), %rdi
	movq	%rax, -2368(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5
	call	_ZdlPv@PLT
.L5:
	movq	-2424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2328(%rbp)
	jne	.L268
.L6:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1952(%rbp)
	je	.L8
.L258:
	movq	-2464(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2016(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r10d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%r10w, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L9
	call	_ZdlPv@PLT
.L9:
	movq	(%rbx), %rax
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movdqu	(%rax), %xmm0
	movaps	%xmm1, -2336(%rbp)
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2496(%rbp)
	call	_Znwm@PLT
	movdqa	-2496(%rbp), %xmm0
	leaq	-1632(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	movq	-2432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1760(%rbp)
	je	.L11
.L259:
	movq	-2424(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1824(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L12
	call	_ZdlPv@PLT
.L12:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	24(%rax), %rdx
	movdqu	(%rax), %xmm5
	movaps	%xmm0, -2336(%rbp)
	movq	$0, -2320(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm5, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-1440(%rbp), %rdi
	movq	%rax, -2336(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L13
	call	_ZdlPv@PLT
.L13:
	movq	-2416(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1568(%rbp)
	je	.L14
.L260:
	movq	-2432(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1632(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r9d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L15
	call	_ZdlPv@PLT
.L15:
	movq	(%rbx), %rax
	movq	-2408(%rbp), %rdi
	xorl	%esi, %esi
	movq	(%rax), %rbx
	movq	8(%rax), %r14
	call	_ZN2v88internal42FromConstexpr6ATbool16ATconstexpr_bool_165EPNS0_8compiler18CodeAssemblerStateEb@PLT
	movl	$24, %edi
	movq	$0, -2320(%rbp)
	movq	%r14, %xmm7
	movq	%rbx, %xmm0
	movq	%rax, -80(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -2336(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L16
	call	_ZdlPv@PLT
.L16:
	movq	-2400(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1376(%rbp)
	je	.L17
.L261:
	movq	-2416(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1440(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r8d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L18
	call	_ZdlPv@PLT
.L18:
	movq	(%rbx), %rax
	movl	$14, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -2480(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -2496(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-2408(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE(%rip), %esi
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2408(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	call	_ZN2v88internal20Cast10JSReceiver_140EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm6
	movq	%rbx, %xmm3
	movq	-2496(%rbp), %xmm4
	punpcklqdq	%xmm6, %xmm3
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	%rax, -64(%rbp)
	movhps	-2480(%rbp), %xmm4
	movaps	%xmm3, -2512(%rbp)
	leaq	-2368(%rbp), %r14
	movaps	%xmm4, -2496(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm0, -2368(%rbp)
	movq	$0, -2352(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm7
	movdqa	-80(%rbp), %xmm6
	leaq	40(%rax), %rdx
	leaq	-1056(%rbp), %rdi
	movq	%rax, -2368(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L19
	call	_ZdlPv@PLT
.L19:
	movq	-2456(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2328(%rbp)
	jne	.L269
.L20:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1184(%rbp)
	je	.L22
.L262:
	movq	-2472(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1248(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L23
	call	_ZdlPv@PLT
.L23:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm7
	movaps	%xmm0, -2336(%rbp)
	movq	$0, -2320(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm7, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -2336(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L24
	call	_ZdlPv@PLT
.L24:
	movq	-2440(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -992(%rbp)
	je	.L25
.L263:
	movq	-2456(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L26
	call	_ZdlPv@PLT
.L26:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	8(%rax), %rsi
	movq	16(%rax), %rcx
	movq	32(%rax), %rdx
	movq	(%rax), %rax
	movaps	%xmm0, -2336(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -2320(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -2336(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L27
	call	_ZdlPv@PLT
.L27:
	movq	-2448(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L28
.L264:
	movq	-2440(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movl	$1799, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r14, %rdi
	movb	$7, 2(%rax)
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L29
	call	_ZdlPv@PLT
.L29:
	movq	(%rbx), %rax
	movq	-2408(%rbp), %rdi
	movl	$1, %esi
	movq	(%rax), %rbx
	movq	8(%rax), %r14
	call	_ZN2v88internal42FromConstexpr6ATbool16ATconstexpr_bool_165EPNS0_8compiler18CodeAssemblerStateEb@PLT
	movl	$24, %edi
	movq	$0, -2320(%rbp)
	movq	%r14, %xmm5
	movq	%rbx, %xmm0
	movq	%rax, -80(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm3
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -2336(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L30
	call	_ZdlPv@PLT
.L30:
	movq	-2400(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -608(%rbp)
	je	.L31
.L265:
	movq	-2448(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117901063, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L32
	call	_ZdlPv@PLT
.L32:
	movq	(%rbx), %rax
	movl	$15, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2408(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal42FromConstexpr6ATbool16ATconstexpr_bool_165EPNS0_8compiler18CodeAssemblerStateEb@PLT
	movq	%r14, %xmm4
	movq	%rbx, %xmm0
	movl	$24, %edi
	punpcklqdq	%xmm4, %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2336(%rbp)
	movq	$0, -2320(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-480(%rbp), %rdi
	movq	%rax, -2336(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L33
	call	_ZdlPv@PLT
.L33:
	movq	-2400(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L266:
	movq	-2400(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movq	%r14, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$4, 2(%rax)
	movq	%rax, -2336(%rbp)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2336(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L35
	call	_ZdlPv@PLT
.L35:
	movq	(%rbx), %rax
	movl	$12, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r14
	movq	%rcx, -2408(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r14, -80(%rbp)
	movhps	-2408(%rbp), %xmm0
	leaq	-288(%rbp), %r14
	movq	$0, -2320(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2336(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm7
	leaq	24(%rax), %rdx
	movq	%rax, -2336(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -2320(%rbp)
	movq	%rdx, -2328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L36
	call	_ZdlPv@PLT
.L36:
	movq	-2392(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L268:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movdqa	-2496(%rbp), %xmm5
	movaps	%xmm0, -2368(%rbp)
	movaps	%xmm5, -96(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -2352(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm2
	movq	%r14, %rsi
	leaq	24(%rax), %rdx
	leaq	-2016(%rbp), %rdi
	movq	%rax, -2368(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L7
	call	_ZdlPv@PLT
.L7:
	movq	-2464(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L269:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-2496(%rbp), %xmm5
	movdqa	-2512(%rbp), %xmm2
	movaps	%xmm0, -2368(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm2, -80(%rbp)
	movq	$0, -2352(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm2
	movq	%r14, %rsi
	leaq	32(%rax), %rdx
	leaq	-1248(%rbp), %rdi
	movq	%rax, -2368(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -2352(%rbp)
	movq	%rdx, -2360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L21
	call	_ZdlPv@PLT
.L21:
	movq	-2472(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L20
.L267:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22410:
	.size	_ZN2v88internal18IsRevokedProxy_319EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEE, .-_ZN2v88internal18IsRevokedProxy_319EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal18IsRevokedProxy_319EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal18IsRevokedProxy_319EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEE, @function
_GLOBAL__sub_I__ZN2v88internal18IsRevokedProxy_319EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEE:
.LFB28939:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28939:
	.size	_GLOBAL__sub_I__ZN2v88internal18IsRevokedProxy_319EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEE, .-_GLOBAL__sub_I__ZN2v88internal18IsRevokedProxy_319EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal18IsRevokedProxy_319EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEE
	.weak	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE:
	.byte	7
	.byte	7
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
