	.file	"array-slice-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0:
.LFB30860:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	%rsi, %r13
	pushq	%rbx
	movq	%r13, %rax
	sarq	$3, %rax
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%r13, %r13
	js	.L14
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%rdx, %r14
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.L9
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%rax, %rdi
.L9:
	leaq	(%rdi,%r13), %r15
	movq	%rdi, (%rbx)
	movq	%r15, 16(%rbx)
	cmpq	%r12, %r14
	je	.L10
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
.L10:
	movq	%r15, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE30860:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0:
.LFB30859:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	subq	%rsi, %rdx
	js	.L21
	movq	%rdi, %rbx
	je	.L17
	movq	%rdx, %rdi
	movq	%rdx, %r12
	movq	%rsi, %r13
	call	_Znwm@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	leaq	(%rax,%r12), %r14
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	%r14, 16(%rbx)
	call	memcpy@PLT
	movq	%r14, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	xorl	%r14d, %r14d
	movq	$0, (%rdi)
	movq	$0, 16(%rdi)
	movq	%r14, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE30859:
	.size	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0:
.LFB30858:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	movabsq	$384307168202282325, %rax
	cmpq	%rax, %rdx
	ja	.L39
	pxor	%xmm0, %xmm0
	leaq	(%rdx,%rdx,2), %r14
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	%rdx, %r12
	movups	%xmm0, 8(%rdi)
	salq	$3, %r14
	xorl	%esi, %esi
	movq	$0, 24(%rdi)
	testq	%rdx, %rdx
	je	.L28
	movq	%r14, %rdi
	call	_Znwm@PLT
	leaq	-1(%r12), %rdx
	leaq	(%rax,%r14), %rsi
	movq	%rax, 8(%rbx)
	movq	%rsi, 24(%rbx)
	cmpq	$2, %rdx
	jbe	.L29
	movq	%r12, %rdi
	movq	%r12, %rcx
	pxor	%xmm0, %xmm0
	movq	%rax, %rdx
	shrq	%rdi
	andq	$-2, %rcx
	addq	%rdi, %rcx
	salq	$4, %rcx
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L26:
	movups	%xmm0, (%rdx)
	addq	$48, %rdx
	movups	%xmm0, -32(%rdx)
	movups	%xmm0, -16(%rdx)
	cmpq	%rcx, %rdx
	jne	.L26
	movq	%r12, %rdx
	movq	%r12, %rcx
	andq	$-2, %rdx
	andl	$1, %ecx
	leaq	(%rdx,%rdx,2), %rdi
	leaq	(%rax,%rdi,8), %rax
	cmpq	%rdx, %r12
	je	.L28
.L25:
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	cmpq	$1, %rcx
	je	.L28
	movq	$0, 40(%rax)
	movups	%xmm0, 24(%rax)
	cmpq	$2, %rcx
	je	.L28
	movq	$0, 64(%rax)
	movups	%xmm0, 48(%rax)
.L28:
	movq	%rsi, 16(%rbx)
	pxor	%xmm0, %xmm0
	leaq	56(%rbx), %rdi
	movq	%r13, %rsi
	movups	%xmm0, 32(%rbx)
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	$0, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	%r12, %rcx
	jmp	.L25
.L39:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE30858:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	.section	.text._ZN2v88internal17CodeStubAssembler6SmiAddENS0_8compiler5TNodeINS0_3SmiEEES5_,"axG",@progbits,_ZN2v88internal17CodeStubAssembler6SmiAddENS0_8compiler5TNodeINS0_3SmiEEES5_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17CodeStubAssembler6SmiAddENS0_8compiler5TNodeINS0_3SmiEEES5_
	.type	_ZN2v88internal17CodeStubAssembler6SmiAddENS0_8compiler5TNodeINS0_3SmiEEES5_, @function
_ZN2v88internal17CodeStubAssembler6SmiAddENS0_8compiler5TNodeINS0_3SmiEEES5_:
.LFB13056:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	movq	%rdx, %rsi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	.cfi_endproc
.LFE13056:
	.size	_ZN2v88internal17CodeStubAssembler6SmiAddENS0_8compiler5TNodeINS0_3SmiEEES5_, .-_ZN2v88internal17CodeStubAssembler6SmiAddENS0_8compiler5TNodeINS0_3SmiEEES5_
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev,"axG",@progbits,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev:
.LFB22413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$56, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L43
	call	_ZdlPv@PLT
.L43:
	movq	16(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	%r12, %r13
	je	.L44
	.p2align 4,,10
	.p2align 3
.L48:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L45
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L48
.L46:
	movq	8(%rbx), %r12
.L44:
	testq	%r12, %r12
	je	.L42
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L48
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L42:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22413:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev
	.set	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.section	.rodata._ZN2v88internal29HandleSimpleArgumentsSlice_38EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_27JSArgumentsObjectWithLengthEEENS4_INS0_3SmiEEESA_PNS1_18CodeAssemblerLabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../../deps/v8/../../deps/v8/src/builtins/array-slice.tq"
	.section	.text._ZN2v88internal29HandleSimpleArgumentsSlice_38EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_27JSArgumentsObjectWithLengthEEENS4_INS0_3SmiEEESA_PNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29HandleSimpleArgumentsSlice_38EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_27JSArgumentsObjectWithLengthEEENS4_INS0_3SmiEEESA_PNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal29HandleSimpleArgumentsSlice_38EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_27JSArgumentsObjectWithLengthEEENS4_INS0_3SmiEEESA_PNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal29HandleSimpleArgumentsSlice_38EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_27JSArgumentsObjectWithLengthEEENS4_INS0_3SmiEEESA_PNS1_18CodeAssemblerLabelE:
.LFB22410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-2448(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2576(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-2616(%rbp), %r12
	pushq	%rbx
	subq	$2808, %rsp
	.cfi_offset 3, -56
	movq	%r9, -2792(%rbp)
	movq	%rsi, -2736(%rbp)
	movq	%rdx, -2752(%rbp)
	movq	%rcx, -2768(%rbp)
	movq	%r8, -2784(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -2616(%rbp)
	movq	%rdi, -2448(%rbp)
	movl	$96, %edi
	movq	$0, -2440(%rbp)
	movq	$0, -2432(%rbp)
	movq	$0, -2424(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -2440(%rbp)
	leaq	-2392(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2424(%rbp)
	movq	%rdx, -2432(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2408(%rbp)
	movq	%rax, -2640(%rbp)
	movq	$0, -2416(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2616(%rbp), %rax
	movl	$96, %edi
	movq	$0, -2248(%rbp)
	movq	$0, -2240(%rbp)
	movq	%rax, -2256(%rbp)
	movq	$0, -2232(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -2248(%rbp)
	leaq	-2200(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2232(%rbp)
	movq	%rdx, -2240(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2216(%rbp)
	movq	%rax, -2664(%rbp)
	movq	$0, -2224(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2616(%rbp), %rax
	movl	$96, %edi
	movq	$0, -2056(%rbp)
	movq	$0, -2048(%rbp)
	movq	%rax, -2064(%rbp)
	movq	$0, -2040(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -2056(%rbp)
	leaq	-2008(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2040(%rbp)
	movq	%rdx, -2048(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2024(%rbp)
	movq	%rax, -2672(%rbp)
	movq	$0, -2032(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2616(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1864(%rbp)
	movq	$0, -1856(%rbp)
	movq	%rax, -1872(%rbp)
	movq	$0, -1848(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1864(%rbp)
	leaq	-1816(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1848(%rbp)
	movq	%rdx, -1856(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1832(%rbp)
	movq	%rax, -2712(%rbp)
	movq	$0, -1840(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2616(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1672(%rbp)
	movq	$0, -1664(%rbp)
	movq	%rax, -1680(%rbp)
	movq	$0, -1656(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1672(%rbp)
	leaq	-1624(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1656(%rbp)
	movq	%rdx, -1664(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1640(%rbp)
	movq	%rax, -2656(%rbp)
	movq	$0, -1648(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2616(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1480(%rbp)
	movq	$0, -1472(%rbp)
	movq	%rax, -1488(%rbp)
	movq	$0, -1464(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1480(%rbp)
	leaq	-1432(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1464(%rbp)
	movq	%rdx, -1472(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1448(%rbp)
	movq	%rax, -2680(%rbp)
	movq	$0, -1456(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2616(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1288(%rbp)
	movq	$0, -1280(%rbp)
	movq	%rax, -1296(%rbp)
	movq	$0, -1272(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1288(%rbp)
	leaq	-1240(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1272(%rbp)
	movq	%rdx, -1280(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1256(%rbp)
	movq	%rax, -2704(%rbp)
	movq	$0, -1264(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2616(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1096(%rbp)
	movq	$0, -1088(%rbp)
	movq	%rax, -1104(%rbp)
	movq	$0, -1080(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1096(%rbp)
	leaq	-1048(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1080(%rbp)
	movq	%rdx, -1088(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1064(%rbp)
	movq	%rax, -2720(%rbp)
	movq	$0, -1072(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2616(%rbp), %rax
	movl	$240, %edi
	movq	$0, -904(%rbp)
	movq	$0, -896(%rbp)
	movq	%rax, -912(%rbp)
	movq	$0, -888(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -904(%rbp)
	leaq	-856(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -888(%rbp)
	movq	%rdx, -896(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -872(%rbp)
	movq	%rax, -2688(%rbp)
	movq	$0, -880(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2616(%rbp), %rax
	movl	$120, %edi
	movq	$0, -712(%rbp)
	movq	$0, -704(%rbp)
	movq	%rax, -720(%rbp)
	movq	$0, -696(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movups	%xmm0, (%rax)
	leaq	120(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -712(%rbp)
	leaq	-664(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -696(%rbp)
	movq	%rdx, -704(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -680(%rbp)
	movq	%rax, -2696(%rbp)
	movq	$0, -688(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2616(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	$0, -520(%rbp)
	movq	%rax, -528(%rbp)
	leaq	-472(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -2632(%rbp)
	movq	$0, -512(%rbp)
	movq	$0, -504(%rbp)
	movq	$0, -496(%rbp)
	movq	$0, -488(%rbp)
	movq	$0, -480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2616(%rbp), %rax
	movl	$120, %edi
	movq	$0, -328(%rbp)
	movq	$0, -320(%rbp)
	movq	%rax, -336(%rbp)
	movq	$0, -312(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -328(%rbp)
	leaq	-280(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -312(%rbp)
	movq	%rdx, -320(%rbp)
	xorl	%edx, %edx
	movq	%rax, -2648(%rbp)
	movups	%xmm0, -296(%rbp)
	movq	$0, -304(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-2768(%rbp), %xmm1
	movq	-2736(%rbp), %xmm2
	movaps	%xmm0, -2576(%rbp)
	movhps	-2784(%rbp), %xmm1
	movq	$0, -2560(%rbp)
	movhps	-2752(%rbp), %xmm2
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm2, -144(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-144(%rbp), %xmm3
	leaq	32(%rax), %rdx
	movq	%rax, -2576(%rbp)
	movups	%xmm4, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -2560(%rbp)
	movq	%rdx, -2568(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2576(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L55
	call	_ZdlPv@PLT
.L55:
	movq	-2640(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2384(%rbp)
	jne	.L335
	cmpq	$0, -2192(%rbp)
	jne	.L336
.L60:
	cmpq	$0, -2000(%rbp)
	jne	.L337
.L63:
	cmpq	$0, -1808(%rbp)
	jne	.L338
.L68:
	cmpq	$0, -1616(%rbp)
	jne	.L339
.L71:
	cmpq	$0, -1424(%rbp)
	jne	.L340
.L75:
	cmpq	$0, -1232(%rbp)
	jne	.L341
.L78:
	cmpq	$0, -1040(%rbp)
	jne	.L342
.L83:
	cmpq	$0, -848(%rbp)
	jne	.L343
.L86:
	cmpq	$0, -656(%rbp)
	leaq	-336(%rbp), %r14
	jne	.L344
	cmpq	$0, -464(%rbp)
	jne	.L345
.L92:
	movq	-2648(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2560(%rbp)
	movaps	%xmm0, -2576(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$101058311, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2576(%rbp)
	movq	%rdx, -2560(%rbp)
	movq	%rdx, -2568(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2576(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L94
	call	_ZdlPv@PLT
.L94:
	movq	(%rbx), %rax
	movq	-2648(%rbp), %rdi
	movq	32(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L95
	call	_ZdlPv@PLT
.L95:
	movq	-320(%rbp), %rbx
	movq	-328(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L96
	.p2align 4,,10
	.p2align 3
.L100:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L97
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L100
.L98:
	movq	-328(%rbp), %r14
.L96:
	testq	%r14, %r14
	je	.L101
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L101:
	movq	-2632(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-496(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L102
	call	_ZdlPv@PLT
.L102:
	movq	-512(%rbp), %rbx
	movq	-520(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L103
	.p2align 4,,10
	.p2align 3
.L107:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L104
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L107
.L105:
	movq	-520(%rbp), %r14
.L103:
	testq	%r14, %r14
	je	.L108
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L108:
	movq	-2696(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L109
	call	_ZdlPv@PLT
.L109:
	movq	-704(%rbp), %rbx
	movq	-712(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L110
	.p2align 4,,10
	.p2align 3
.L114:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L111
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L114
.L112:
	movq	-712(%rbp), %r14
.L110:
	testq	%r14, %r14
	je	.L115
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L115:
	movq	-2688(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L116
	call	_ZdlPv@PLT
.L116:
	movq	-896(%rbp), %rbx
	movq	-904(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L117
	.p2align 4,,10
	.p2align 3
.L121:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L118
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L121
.L119:
	movq	-904(%rbp), %r14
.L117:
	testq	%r14, %r14
	je	.L122
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L122:
	movq	-2720(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L123
	call	_ZdlPv@PLT
.L123:
	movq	-1088(%rbp), %rbx
	movq	-1096(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L124
	.p2align 4,,10
	.p2align 3
.L128:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L125
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L128
.L126:
	movq	-1096(%rbp), %r14
.L124:
	testq	%r14, %r14
	je	.L129
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L129:
	movq	-2704(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L130
	call	_ZdlPv@PLT
.L130:
	movq	-1280(%rbp), %rbx
	movq	-1288(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L131
	.p2align 4,,10
	.p2align 3
.L135:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L132
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L135
.L133:
	movq	-1288(%rbp), %r14
.L131:
	testq	%r14, %r14
	je	.L136
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L136:
	movq	-2680(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1456(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L137
	call	_ZdlPv@PLT
.L137:
	movq	-1472(%rbp), %rbx
	movq	-1480(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L138
	.p2align 4,,10
	.p2align 3
.L142:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L139
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L142
.L140:
	movq	-1480(%rbp), %r14
.L138:
	testq	%r14, %r14
	je	.L143
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L143:
	movq	-2656(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1648(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L144
	call	_ZdlPv@PLT
.L144:
	movq	-1664(%rbp), %rbx
	movq	-1672(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L145
	.p2align 4,,10
	.p2align 3
.L149:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L146
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L149
.L147:
	movq	-1672(%rbp), %r14
.L145:
	testq	%r14, %r14
	je	.L150
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L150:
	movq	-2712(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1840(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L151
	call	_ZdlPv@PLT
.L151:
	movq	-1856(%rbp), %rbx
	movq	-1864(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L152
	.p2align 4,,10
	.p2align 3
.L156:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L153
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L156
.L154:
	movq	-1864(%rbp), %r14
.L152:
	testq	%r14, %r14
	je	.L157
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L157:
	movq	-2672(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2032(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L158
	call	_ZdlPv@PLT
.L158:
	movq	-2048(%rbp), %rbx
	movq	-2056(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L159
	.p2align 4,,10
	.p2align 3
.L163:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L160
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L163
.L161:
	movq	-2056(%rbp), %r14
.L159:
	testq	%r14, %r14
	je	.L164
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L164:
	movq	-2664(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L165
	call	_ZdlPv@PLT
.L165:
	movq	-2240(%rbp), %rbx
	movq	-2248(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L166
	.p2align 4,,10
	.p2align 3
.L170:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L167
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L170
.L168:
	movq	-2248(%rbp), %r14
.L166:
	testq	%r14, %r14
	je	.L171
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L171:
	movq	-2640(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2416(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L172
	call	_ZdlPv@PLT
.L172:
	movq	-2432(%rbp), %rbx
	movq	-2440(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L173
	.p2align 4,,10
	.p2align 3
.L177:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L174
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%rbx, %r14
	jne	.L177
.L175:
	movq	-2440(%rbp), %r14
.L173:
	testq	%r14, %r14
	je	.L178
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L178:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L346
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L177
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L167:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L170
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L160:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L163
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L153:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L156
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L146:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L149
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L139:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L142
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L132:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L135
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L125:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L128
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L118:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L121
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L111:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L114
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L97:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L100
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L104:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L107
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L335:
	movq	-2640(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2560(%rbp)
	movaps	%xmm0, -2576(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$101058311, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2576(%rbp)
	movq	%rdx, -2560(%rbp)
	movq	%rdx, -2568(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2576(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L57
	call	_ZdlPv@PLT
.L57:
	movq	(%rbx), %rax
	movl	$11, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	24(%rax), %rbx
	movq	%rsi, -2752(%rbp)
	movq	16(%rax), %rsi
	movq	%rcx, -2736(%rbp)
	movq	%rsi, -2768(%rbp)
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16382, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler24IntPtrGreaterThanOrEqualENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm7
	pxor	%xmm0, %xmm0
	movq	-2768(%rbp), %xmm5
	movq	-2736(%rbp), %xmm6
	movl	$32, %edi
	movaps	%xmm0, -2576(%rbp)
	movq	$0, -2560(%rbp)
	punpcklqdq	%xmm7, %xmm5
	movhps	-2752(%rbp), %xmm6
	movaps	%xmm5, -2768(%rbp)
	movaps	%xmm6, -2736(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm1
	movdqa	-128(%rbp), %xmm2
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-2256(%rbp), %rdi
	movq	%rax, -2576(%rbp)
	movups	%xmm1, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -2560(%rbp)
	movq	%rdx, -2568(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2576(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L58
	call	_ZdlPv@PLT
.L58:
	movdqa	-2736(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-2768(%rbp), %xmm1
	movaps	%xmm0, -2576(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movq	$0, -2560(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-2064(%rbp), %rdi
	movq	%rax, -2576(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2560(%rbp)
	movq	%rdx, -2568(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2576(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L59
	call	_ZdlPv@PLT
.L59:
	movq	-2672(%rbp), %rcx
	movq	-2664(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2192(%rbp)
	je	.L60
.L336:
	movq	-2664(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2256(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2560(%rbp)
	movaps	%xmm0, -2576(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$101058311, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2576(%rbp)
	movq	%rdx, -2560(%rbp)
	movq	%rdx, -2568(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2576(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L61
	call	_ZdlPv@PLT
.L61:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-528(%rbp), %rdi
	movq	$0, -2560(%rbp)
	movaps	%xmm0, -2576(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2576(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L62
	call	_ZdlPv@PLT
.L62:
	movq	-2632(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2000(%rbp)
	je	.L63
.L337:
	movq	-2672(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2064(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2560(%rbp)
	movaps	%xmm0, -2576(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$101058311, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2576(%rbp)
	movq	%rdx, -2560(%rbp)
	movq	%rdx, -2568(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2576(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L64
	call	_ZdlPv@PLT
.L64:
	movq	(%rbx), %rax
	movl	$13, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	16(%rax), %r14
	movq	%rcx, -2752(%rbp)
	movq	8(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, -2736(%rbp)
	movq	%rax, %rbx
	movq	%rax, -2768(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -2784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$15, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-2736(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal20Cast10FixedArray_103EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm1
	movq	%r14, %xmm3
	movq	-2784(%rbp), %xmm7
	movhps	-2768(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movq	%rax, -96(%rbp)
	movq	-2752(%rbp), %xmm4
	punpcklqdq	%xmm1, %xmm7
	movl	$56, %edi
	movaps	%xmm3, -128(%rbp)
	leaq	-2608(%rbp), %r14
	movhps	-2736(%rbp), %xmm4
	movaps	%xmm7, -2784(%rbp)
	movaps	%xmm3, -2768(%rbp)
	movaps	%xmm4, -2736(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm0, -2608(%rbp)
	movq	$0, -2592(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm2
	leaq	56(%rax), %rdx
	leaq	-1680(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm3, 32(%rax)
	movq	%rax, -2608(%rbp)
	movq	%rdx, -2592(%rbp)
	movq	%rdx, -2600(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2608(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L65
	call	_ZdlPv@PLT
.L65:
	movq	-2656(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2568(%rbp)
	jne	.L347
.L66:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1808(%rbp)
	je	.L68
.L338:
	movq	-2712(%rbp), %rsi
	movq	%r12, %rdi
	movl	$1798, %ebx
	leaq	-1872(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2560(%rbp)
	movaps	%xmm0, -2576(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$101058311, (%rax)
	leaq	6(%rax), %rdx
	movw	%bx, 4(%rax)
	movq	%rax, -2576(%rbp)
	movq	%rdx, -2560(%rbp)
	movq	%rdx, -2568(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2576(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L69
	call	_ZdlPv@PLT
.L69:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-528(%rbp), %rdi
	movq	$0, -2560(%rbp)
	movaps	%xmm0, -2576(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2576(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L70
	call	_ZdlPv@PLT
.L70:
	movq	-2632(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1616(%rbp)
	je	.L71
.L339:
	movq	-2656(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1680(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2560(%rbp)
	movaps	%xmm0, -2576(%rbp)
	call	_Znwm@PLT
	movl	$1798, %r11d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$101058311, (%rax)
	leaq	7(%rax), %rdx
	movw	%r11w, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -2576(%rbp)
	movq	%rdx, -2560(%rbp)
	movq	%rdx, -2568(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2576(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L72
	call	_ZdlPv@PLT
.L72:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	24(%rax), %rdx
	movq	(%rax), %rcx
	movq	32(%rax), %rbx
	movq	%rsi, -2768(%rbp)
	movq	16(%rax), %rsi
	movq	48(%rax), %rax
	movq	%rdx, -2816(%rbp)
	movl	$14, %edx
	movq	%rsi, -2784(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -2752(%rbp)
	movq	%rax, -2736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-2736(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18UintPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm5
	pxor	%xmm0, %xmm0
	movq	-2784(%rbp), %xmm6
	movq	-2752(%rbp), %xmm7
	movhps	-2736(%rbp), %xmm5
	movl	$48, %edi
	movaps	%xmm0, -2576(%rbp)
	movhps	-2816(%rbp), %xmm6
	movaps	%xmm5, -112(%rbp)
	movhps	-2768(%rbp), %xmm7
	movaps	%xmm5, -2832(%rbp)
	movaps	%xmm6, -2784(%rbp)
	movaps	%xmm7, -2736(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movq	$0, -2560(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm1
	movdqa	-112(%rbp), %xmm2
	leaq	48(%rax), %rdx
	leaq	-1488(%rbp), %rdi
	movq	%rax, -2576(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm2, 32(%rax)
	movq	%rdx, -2560(%rbp)
	movq	%rdx, -2568(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2576(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L73
	call	_ZdlPv@PLT
.L73:
	movdqa	-2736(%rbp), %xmm7
	movdqa	-2784(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movdqa	-2832(%rbp), %xmm4
	movaps	%xmm0, -2576(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movq	$0, -2560(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm1
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm2
	movdqa	-112(%rbp), %xmm5
	leaq	48(%rax), %rdx
	leaq	-1296(%rbp), %rdi
	movq	%rax, -2576(%rbp)
	movups	%xmm1, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rdx, -2560(%rbp)
	movq	%rdx, -2568(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2576(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L74
	call	_ZdlPv@PLT
.L74:
	movq	-2704(%rbp), %rcx
	movq	-2680(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1424(%rbp)
	je	.L75
.L340:
	movq	-2680(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1488(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2560(%rbp)
	movaps	%xmm0, -2576(%rbp)
	call	_Znwm@PLT
	movl	$1798, %r10d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$101058311, (%rax)
	leaq	6(%rax), %rdx
	movw	%r10w, 4(%rax)
	movq	%rax, -2576(%rbp)
	movq	%rdx, -2560(%rbp)
	movq	%rdx, -2568(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2576(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L76
	call	_ZdlPv@PLT
.L76:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-528(%rbp), %rdi
	movq	$0, -2560(%rbp)
	movaps	%xmm0, -2576(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2576(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L77
	call	_ZdlPv@PLT
.L77:
	movq	-2632(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1232(%rbp)
	je	.L78
.L341:
	movq	-2704(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1296(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2560(%rbp)
	movaps	%xmm0, -2576(%rbp)
	call	_Znwm@PLT
	movl	$1798, %r9d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$101058311, (%rax)
	leaq	6(%rax), %rdx
	movw	%r9w, 4(%rax)
	movq	%rax, -2576(%rbp)
	movq	%rdx, -2560(%rbp)
	movq	%rdx, -2568(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2576(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L79
	call	_ZdlPv@PLT
.L79:
	movq	(%rbx), %rax
	movl	$18, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	32(%rax), %rsi
	movq	24(%rax), %r14
	movq	(%rax), %rbx
	movq	%rcx, -2768(%rbp)
	movq	16(%rax), %rcx
	movq	40(%rax), %rax
	movq	%rsi, -2816(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -2784(%rbp)
	movq	%rax, -2832(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movl	$3, %esi
	movq	%r13, %rdi
	movq	%rbx, -2752(%rbp)
	call	_ZN2v88internal17CodeStubAssembler22LoadJSArrayElementsMapENS0_12ElementsKindENS0_8compiler11SloppyTNodeINS0_13NativeContextEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$20, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	pushq	$0
	xorl	%r9d, %r9d
	movq	%r14, %r8
	pushq	$0
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movl	$3, %esi
	movq	%r13, %rdi
	movq	%rbx, -2800(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15AllocateJSArrayENS0_12ElementsKindENS0_8compiler5TNodeINS0_3MapEEEPNS3_4NodeENS4_INS0_3SmiEEES8_NS1_13ParameterModeENS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	%r13, %rdi
	movq	%rax, -2736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$19, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$22, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-2736(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal20Cast10FixedArray_103EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm6
	pxor	%xmm0, %xmm0
	movq	-2784(%rbp), %xmm5
	movl	$80, %edi
	movaps	%xmm0, -2608(%rbp)
	movq	-2816(%rbp), %xmm4
	movq	-2800(%rbp), %xmm3
	punpcklqdq	%xmm6, %xmm5
	movq	%rbx, -80(%rbp)
	movq	-2752(%rbp), %xmm6
	leaq	-2608(%rbp), %r14
	movhps	-2736(%rbp), %xmm3
	movhps	-2832(%rbp), %xmm4
	movaps	%xmm5, -2784(%rbp)
	movhps	-2768(%rbp), %xmm6
	movaps	%xmm3, -2736(%rbp)
	movaps	%xmm4, -2816(%rbp)
	movaps	%xmm6, -2752(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -2592(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm3
	leaq	-912(%rbp), %rdi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm1
	leaq	80(%rax), %rdx
	movq	%r14, %rsi
	movdqa	-80(%rbp), %xmm2
	movups	%xmm7, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm1, 48(%rax)
	movups	%xmm2, 64(%rax)
	movq	%rax, -2608(%rbp)
	movq	%rdx, -2592(%rbp)
	movq	%rdx, -2600(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2608(%rbp), %rdi
	popq	%rsi
	popq	%r8
	testq	%rdi, %rdi
	je	.L80
	call	_ZdlPv@PLT
.L80:
	movq	-2688(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2568(%rbp)
	jne	.L348
.L81:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1040(%rbp)
	je	.L83
.L342:
	movq	-2720(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1104(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -2560(%rbp)
	movaps	%xmm0, -2576(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$506381205554726663, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%rax, -2576(%rbp)
	movq	%rdx, -2560(%rbp)
	movq	%rdx, -2568(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2576(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L84
	call	_ZdlPv@PLT
.L84:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-528(%rbp), %rdi
	movq	$0, -2560(%rbp)
	movaps	%xmm0, -2576(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2576(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L85
	call	_ZdlPv@PLT
.L85:
	movq	-2632(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -848(%rbp)
	je	.L86
.L343:
	movq	-2688(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-912(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -2560(%rbp)
	movaps	%xmm0, -2576(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$506381205554726663, %rcx
	movq	%rcx, (%rax)
	movl	$1799, %ecx
	leaq	10(%rax), %rdx
	movw	%cx, 8(%rax)
	movq	%rax, -2576(%rbp)
	movq	%rdx, -2560(%rbp)
	movq	%rdx, -2568(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2576(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L87
	call	_ZdlPv@PLT
.L87:
	movq	(%rbx), %rax
	movl	$21, %edx
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	movq	40(%rax), %r8
	movq	72(%rax), %r11
	movq	16(%rax), %rbx
	movq	56(%rax), %r14
	movq	%rcx, -2752(%rbp)
	movq	24(%rax), %rcx
	movq	%rsi, -2768(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%r8, -2840(%rbp)
	movq	%rcx, -2736(%rbp)
	movq	%r11, -2800(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$25, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -2832(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2736(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$23, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -2784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -2816(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	pushq	$4
	movq	%r13, %rdi
	movq	-2800(%rbp), %r11
	pushq	-2784(%rbp)
	movq	-2832(%rbp), %r9
	movl	$2, %esi
	movq	-2840(%rbp), %r8
	movq	%r11, %rdx
	movq	-2816(%rbp), %rcx
	call	_ZN2v88internal17CodeStubAssembler12CopyElementsENS0_12ElementsKindENS0_8compiler5TNodeINS0_14FixedArrayBaseEEENS4_INS0_7IntPtrTEEES6_S8_S8_NS0_16WriteBarrierModeE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$26, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$40, %edi
	movq	%r14, -112(%rbp)
	movq	-2752(%rbp), %xmm0
	movq	$0, -2560(%rbp)
	movhps	-2768(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	movhps	-2736(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2576(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-144(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm6
	leaq	40(%rax), %rdx
	leaq	-720(%rbp), %rdi
	movq	%rax, -2576(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -2560(%rbp)
	movq	%rdx, -2568(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2576(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L88
	call	_ZdlPv@PLT
.L88:
	movq	-2696(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L344:
	movq	-2696(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-720(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2560(%rbp)
	movaps	%xmm0, -2576(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$101058311, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2576(%rbp)
	movq	%rdx, -2560(%rbp)
	movq	%rdx, -2568(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2576(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L90
	call	_ZdlPv@PLT
.L90:
	movq	(%rbx), %rax
	movl	$6, %edx
	movq	%r12, %rdi
	movq	24(%rax), %rsi
	movq	(%rax), %r14
	movq	16(%rax), %rcx
	movq	8(%rax), %r15
	movq	%rsi, -2752(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	32(%rax), %rbx
	movq	%rcx, -2736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %xmm7
	movq	%r14, %xmm0
	movl	$40, %edi
	punpcklqdq	%xmm7, %xmm0
	movq	%rbx, -112(%rbp)
	leaq	-336(%rbp), %r14
	movaps	%xmm0, -144(%rbp)
	movq	-2736(%rbp), %xmm0
	movq	$0, -2560(%rbp)
	movhps	-2752(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2576(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm3
	leaq	40(%rax), %rdx
	movq	%rax, -2576(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2560(%rbp)
	movq	%rdx, -2568(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2576(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L91
	call	_ZdlPv@PLT
.L91:
	movq	-2648(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -464(%rbp)
	je	.L92
.L345:
	movq	-2632(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-528(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	$0, -2560(%rbp)
	movaps	%xmm0, -2576(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2576(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L93
	call	_ZdlPv@PLT
.L93:
	movq	-2792(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L347:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movdqa	-2736(%rbp), %xmm1
	movdqa	-2768(%rbp), %xmm2
	movaps	%xmm0, -2608(%rbp)
	movaps	%xmm1, -144(%rbp)
	movdqa	-2784(%rbp), %xmm1
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movq	$0, -2592(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm2
	movq	%r14, %rsi
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm6
	leaq	48(%rax), %rdx
	leaq	-1872(%rbp), %rdi
	movq	%rax, -2608(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -2592(%rbp)
	movq	%rdx, -2600(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2608(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L67
	call	_ZdlPv@PLT
.L67:
	movq	-2712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L348:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-2752(%rbp), %xmm7
	movdqa	-2784(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movdqa	-2816(%rbp), %xmm4
	movl	$72, %edi
	movq	%rbx, -80(%rbp)
	movdqa	-2736(%rbp), %xmm1
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -2608(%rbp)
	movq	$0, -2592(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-144(%rbp), %xmm2
	leaq	72(%rax), %rdx
	leaq	-1104(%rbp), %rdi
	movdqa	-96(%rbp), %xmm7
	movq	%rcx, 64(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rax, -2608(%rbp)
	movq	%rdx, -2592(%rbp)
	movq	%rdx, -2600(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2608(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L82
	call	_ZdlPv@PLT
.L82:
	movq	-2720(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L81
.L346:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22410:
	.size	_ZN2v88internal29HandleSimpleArgumentsSlice_38EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_27JSArgumentsObjectWithLengthEEENS4_INS0_3SmiEEESA_PNS1_18CodeAssemblerLabelE, .-_ZN2v88internal29HandleSimpleArgumentsSlice_38EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_27JSArgumentsObjectWithLengthEEENS4_INS0_3SmiEEESA_PNS1_18CodeAssemblerLabelE
	.section	.rodata._ZN2v88internal21Cast10FixedArray_1433EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"../../deps/v8/../../deps/v8/src/builtins/base.tq"
	.section	.text._ZN2v88internal21Cast10FixedArray_1433EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Cast10FixedArray_1433EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal21Cast10FixedArray_1433EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal21Cast10FixedArray_1433EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE:
.LFB22631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	leaq	-1632(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1760(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1800(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1864, %rsp
	movq	%rsi, -1888(%rbp)
	movq	%rdx, -1904(%rbp)
	movq	%rcx, -1872(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1800(%rbp)
	movq	%rdi, -1632(%rbp)
	movl	$48, %edi
	movq	$0, -1624(%rbp)
	movq	$0, -1616(%rbp)
	movq	$0, -1608(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1624(%rbp)
	leaq	-1576(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1608(%rbp)
	movq	%rdx, -1616(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1592(%rbp)
	movq	%rax, -1816(%rbp)
	movq	$0, -1600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1432(%rbp)
	movq	$0, -1424(%rbp)
	movq	%rax, -1440(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1432(%rbp)
	leaq	-1384(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -1832(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1864(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$96, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1840(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1848(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	$0, -472(%rbp)
	movq	%rax, -480(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1824(%rbp)
	movq	$0, -464(%rbp)
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	$0, -440(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1888(%rbp), %xmm1
	movaps	%xmm0, -1760(%rbp)
	movhps	-1904(%rbp), %xmm1
	movq	$0, -1744(%rbp)
	movaps	%xmm1, -1888(%rbp)
	call	_Znwm@PLT
	movdqa	-1888(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L350
	call	_ZdlPv@PLT
.L350:
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1568(%rbp)
	jne	.L534
	cmpq	$0, -1376(%rbp)
	jne	.L535
.L356:
	cmpq	$0, -1184(%rbp)
	jne	.L536
.L359:
	cmpq	$0, -992(%rbp)
	jne	.L537
.L364:
	cmpq	$0, -800(%rbp)
	jne	.L538
.L367:
	cmpq	$0, -608(%rbp)
	leaq	-288(%rbp), %r15
	jne	.L539
	cmpq	$0, -416(%rbp)
	jne	.L540
.L373:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L375
	call	_ZdlPv@PLT
.L375:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L376
	call	_ZdlPv@PLT
.L376:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L377
	.p2align 4,,10
	.p2align 3
.L381:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L378
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L381
.L379:
	movq	-280(%rbp), %r14
.L377:
	testq	%r14, %r14
	je	.L382
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L382:
	movq	-1824(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L383
	call	_ZdlPv@PLT
.L383:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L384
	.p2align 4,,10
	.p2align 3
.L388:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L385
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L388
.L386:
	movq	-472(%rbp), %r14
.L384:
	testq	%r14, %r14
	je	.L389
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L389:
	movq	-1848(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L390
	call	_ZdlPv@PLT
.L390:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L391
	.p2align 4,,10
	.p2align 3
.L395:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L392
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L395
.L393:
	movq	-664(%rbp), %r14
.L391:
	testq	%r14, %r14
	je	.L396
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L396:
	movq	-1840(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L397
	call	_ZdlPv@PLT
.L397:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L398
	.p2align 4,,10
	.p2align 3
.L402:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L399
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L402
.L400:
	movq	-856(%rbp), %r14
.L398:
	testq	%r14, %r14
	je	.L403
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L403:
	movq	-1864(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L404
	call	_ZdlPv@PLT
.L404:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L405
	.p2align 4,,10
	.p2align 3
.L409:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L406
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L409
.L407:
	movq	-1048(%rbp), %r14
.L405:
	testq	%r14, %r14
	je	.L410
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L410:
	movq	-1832(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L411
	call	_ZdlPv@PLT
.L411:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L412
	.p2align 4,,10
	.p2align 3
.L416:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L413
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L416
.L414:
	movq	-1240(%rbp), %r14
.L412:
	testq	%r14, %r14
	je	.L417
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L417:
	movq	-1856(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L418
	call	_ZdlPv@PLT
.L418:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L419
	.p2align 4,,10
	.p2align 3
.L423:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L420
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L423
.L421:
	movq	-1432(%rbp), %r14
.L419:
	testq	%r14, %r14
	je	.L424
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L424:
	movq	-1816(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L425
	call	_ZdlPv@PLT
.L425:
	movq	-1616(%rbp), %rbx
	movq	-1624(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L426
	.p2align 4,,10
	.p2align 3
.L430:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L427
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L430
.L428:
	movq	-1624(%rbp), %r14
.L426:
	testq	%r14, %r14
	je	.L431
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L431:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L541
	addq	$1864, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L427:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L430
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L420:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L423
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L413:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L416
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L406:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L409
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L399:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L402
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L392:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L395
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L378:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L381
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L385:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L388
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L534:
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r9d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L352
	call	_ZdlPv@PLT
.L352:
	movq	(%r15), %rax
	movl	$2026, %edx
	movq	%r12, %rdi
	leaq	.LC3(%rip), %rsi
	leaq	-1792(%rbp), %r15
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rcx, -1904(%rbp)
	movq	%rax, -1888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1888(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1888(%rbp), %rax
	movl	$32, %edi
	movq	-1904(%rbp), %xmm2
	movq	$0, -1776(%rbp)
	movq	%rax, %xmm0
	movq	%rax, %xmm6
	punpcklqdq	%xmm0, %xmm0
	punpcklqdq	%xmm6, %xmm2
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -1904(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -1792(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movq	%r15, %rsi
	leaq	32(%rax), %rdx
	leaq	-1248(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L353
	call	_ZdlPv@PLT
.L353:
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1752(%rbp)
	jne	.L542
.L354:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1376(%rbp)
	je	.L356
.L535:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1440(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r8d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L357
	call	_ZdlPv@PLT
.L357:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-480(%rbp), %rdi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L358
	call	_ZdlPv@PLT
.L358:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1184(%rbp)
	je	.L359
.L536:
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1248(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L360
	call	_ZdlPv@PLT
.L360:
	movq	(%r15), %rax
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	24(%rax), %rax
	movq	%rcx, -1904(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -1888(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1888(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal20Cast10FixedArray_103EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rcx
	movq	%r15, %xmm3
	pxor	%xmm0, %xmm0
	movhps	-1904(%rbp), %xmm3
	movl	$32, %edi
	movq	%rax, -72(%rbp)
	leaq	-1792(%rbp), %r15
	movq	%rcx, -80(%rbp)
	movaps	%xmm3, -1904(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -1792(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	32(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L361
	call	_ZdlPv@PLT
.L361:
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1752(%rbp)
	jne	.L543
.L362:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -992(%rbp)
	je	.L364
.L537:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rdi
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L365
	call	_ZdlPv@PLT
.L365:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-480(%rbp), %rdi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L366
	call	_ZdlPv@PLT
.L366:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L367
.L538:
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L368
	call	_ZdlPv@PLT
.L368:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	24(%rax), %rdx
	movdqu	(%rax), %xmm4
	movaps	%xmm0, -1760(%rbp)
	movq	$0, -1744(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm4, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -1760(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L369
	call	_ZdlPv@PLT
.L369:
	movq	-1848(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L539:
	movq	-1848(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%r15, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L371
	call	_ZdlPv@PLT
.L371:
	movq	(%rbx), %rax
	movl	$46, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r15
	movq	%rcx, -1888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r15, -80(%rbp)
	movhps	-1888(%rbp), %xmm0
	leaq	-288(%rbp), %r15
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm6
	leaq	24(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L372
	call	_ZdlPv@PLT
.L372:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -416(%rbp)
	je	.L373
.L540:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L374
	call	_ZdlPv@PLT
.L374:
	movq	-1872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L542:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1904(%rbp), %xmm7
	movl	$24, %edi
	movaps	%xmm0, -1792(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-1440(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L355
	call	_ZdlPv@PLT
.L355:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L543:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1904(%rbp), %xmm7
	movl	$24, %edi
	movaps	%xmm0, -1792(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-1056(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L363
	call	_ZdlPv@PLT
.L363:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L362
.L541:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22631:
	.size	_ZN2v88internal21Cast10FixedArray_1433EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, .-_ZN2v88internal21Cast10FixedArray_1433EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal23UnsafeCast7Context_1434EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23UnsafeCast7Context_1434EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal23UnsafeCast7Context_1434EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal23UnsafeCast7Context_1434EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE:
.LFB22650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-216(%rbp), %r14
	leaq	-688(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-600(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-696(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-656(%rbp), %rbx
	subq	$696, %rsp
	movq	%rsi, -736(%rbp)
	movq	%rdx, -720(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -696(%rbp)
	movq	%rdi, -656(%rbp)
	movl	$48, %edi
	movq	$0, -648(%rbp)
	movq	$0, -640(%rbp)
	movq	$0, -632(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -632(%rbp)
	movq	%rdx, -640(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -616(%rbp)
	movq	%rax, -648(%rbp)
	movq	$0, -624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	%rax, -464(%rbp)
	movq	$0, -440(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -456(%rbp)
	leaq	-408(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -440(%rbp)
	movq	%rdx, -448(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -424(%rbp)
	movq	%rax, -712(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	%rax, -272(%rbp)
	movq	$0, -248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -248(%rbp)
	movq	%rdx, -256(%rbp)
	xorl	%edx, %edx
	movq	%rax, -264(%rbp)
	movups	%xmm0, -232(%rbp)
	movq	$0, -240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-736(%rbp), %xmm1
	movaps	%xmm0, -688(%rbp)
	movhps	-720(%rbp), %xmm1
	movq	$0, -672(%rbp)
	movaps	%xmm1, -736(%rbp)
	call	_Znwm@PLT
	movdqa	-736(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -688(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L545
	call	_ZdlPv@PLT
.L545:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -592(%rbp)
	jne	.L613
.L546:
	cmpq	$0, -400(%rbp)
	leaq	-272(%rbp), %rbx
	jne	.L614
.L549:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L552
	call	_ZdlPv@PLT
.L552:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	16(%rax), %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L553
	call	_ZdlPv@PLT
.L553:
	movq	-256(%rbp), %rbx
	movq	-264(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L554
	.p2align 4,,10
	.p2align 3
.L558:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L555
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L558
.L556:
	movq	-264(%rbp), %r14
.L554:
	testq	%r14, %r14
	je	.L559
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L559:
	movq	-712(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L560
	call	_ZdlPv@PLT
.L560:
	movq	-448(%rbp), %rbx
	movq	-456(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L561
	.p2align 4,,10
	.p2align 3
.L565:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L562
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L565
.L563:
	movq	-456(%rbp), %r14
.L561:
	testq	%r14, %r14
	je	.L566
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L566:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L567
	call	_ZdlPv@PLT
.L567:
	movq	-640(%rbp), %rbx
	movq	-648(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L568
	.p2align 4,,10
	.p2align 3
.L572:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L569
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L572
.L570:
	movq	-648(%rbp), %r13
.L568:
	testq	%r13, %r13
	je	.L573
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L573:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L615
	addq	$696, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L569:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L572
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L555:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L558
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L562:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L565
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L613:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r15, %rsi
	movw	%di, (%rax)
	leaq	2(%rax), %rdx
	movq	%rbx, %rdi
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L547
	call	_ZdlPv@PLT
.L547:
	movq	(%rbx), %rax
	movl	$2790, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	%rcx, -736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm2
	movl	$24, %edi
	movq	-736(%rbp), %xmm0
	movq	%rbx, -64(%rbp)
	movq	$0, -672(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm3
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-464(%rbp), %rdi
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L548
	call	_ZdlPv@PLT
.L548:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L614:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-464(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rsi
	movb	$7, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L550
	call	_ZdlPv@PLT
.L550:
	movq	(%rbx), %rax
	movl	$51, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -720(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %edi
	movq	%rbx, -64(%rbp)
	movq	-736(%rbp), %xmm0
	movq	$0, -672(%rbp)
	leaq	-272(%rbp), %rbx
	movhps	-720(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movdqa	-80(%rbp), %xmm4
	leaq	24(%rax), %rdx
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L551
	call	_ZdlPv@PLT
.L551:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L549
.L615:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22650:
	.size	_ZN2v88internal23UnsafeCast7Context_1434EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal23UnsafeCast7Context_1434EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal31Cast20ATFastJSArrayForCopy_1435EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Cast20ATFastJSArrayForCopy_1435EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal31Cast20ATFastJSArrayForCopy_1435EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal31Cast20ATFastJSArrayForCopy_1435EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE:
.LFB22654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	leaq	-1632(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1760(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1800(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1864, %rsp
	movq	%rsi, -1888(%rbp)
	movq	%rdx, -1904(%rbp)
	movq	%rcx, -1872(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1800(%rbp)
	movq	%rdi, -1632(%rbp)
	movl	$48, %edi
	movq	$0, -1624(%rbp)
	movq	$0, -1616(%rbp)
	movq	$0, -1608(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1624(%rbp)
	leaq	-1576(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1608(%rbp)
	movq	%rdx, -1616(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1592(%rbp)
	movq	%rax, -1816(%rbp)
	movq	$0, -1600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1432(%rbp)
	movq	$0, -1424(%rbp)
	movq	%rax, -1440(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1432(%rbp)
	leaq	-1384(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -1832(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1864(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$96, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1840(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1848(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	$0, -472(%rbp)
	movq	%rax, -480(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1824(%rbp)
	movq	$0, -464(%rbp)
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	$0, -440(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1888(%rbp), %xmm1
	movaps	%xmm0, -1760(%rbp)
	movhps	-1904(%rbp), %xmm1
	movq	$0, -1744(%rbp)
	movaps	%xmm1, -1888(%rbp)
	call	_Znwm@PLT
	movdqa	-1888(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L617
	call	_ZdlPv@PLT
.L617:
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1568(%rbp)
	jne	.L801
	cmpq	$0, -1376(%rbp)
	jne	.L802
.L623:
	cmpq	$0, -1184(%rbp)
	jne	.L803
.L626:
	cmpq	$0, -992(%rbp)
	jne	.L804
.L631:
	cmpq	$0, -800(%rbp)
	jne	.L805
.L634:
	cmpq	$0, -608(%rbp)
	leaq	-288(%rbp), %r15
	jne	.L806
	cmpq	$0, -416(%rbp)
	jne	.L807
.L640:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L642
	call	_ZdlPv@PLT
.L642:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L643
	call	_ZdlPv@PLT
.L643:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L644
	.p2align 4,,10
	.p2align 3
.L648:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L645
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L648
.L646:
	movq	-280(%rbp), %r14
.L644:
	testq	%r14, %r14
	je	.L649
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L649:
	movq	-1824(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L650
	call	_ZdlPv@PLT
.L650:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L651
	.p2align 4,,10
	.p2align 3
.L655:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L652
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L655
.L653:
	movq	-472(%rbp), %r14
.L651:
	testq	%r14, %r14
	je	.L656
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L656:
	movq	-1848(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L657
	call	_ZdlPv@PLT
.L657:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L658
	.p2align 4,,10
	.p2align 3
.L662:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L659
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L662
.L660:
	movq	-664(%rbp), %r14
.L658:
	testq	%r14, %r14
	je	.L663
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L663:
	movq	-1840(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L664
	call	_ZdlPv@PLT
.L664:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L665
	.p2align 4,,10
	.p2align 3
.L669:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L666
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L669
.L667:
	movq	-856(%rbp), %r14
.L665:
	testq	%r14, %r14
	je	.L670
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L670:
	movq	-1864(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L671
	call	_ZdlPv@PLT
.L671:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L672
	.p2align 4,,10
	.p2align 3
.L676:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L673
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L676
.L674:
	movq	-1048(%rbp), %r14
.L672:
	testq	%r14, %r14
	je	.L677
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L677:
	movq	-1832(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L678
	call	_ZdlPv@PLT
.L678:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L679
	.p2align 4,,10
	.p2align 3
.L683:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L680
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L683
.L681:
	movq	-1240(%rbp), %r14
.L679:
	testq	%r14, %r14
	je	.L684
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L684:
	movq	-1856(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L685
	call	_ZdlPv@PLT
.L685:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L686
	.p2align 4,,10
	.p2align 3
.L690:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L687
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L690
.L688:
	movq	-1432(%rbp), %r14
.L686:
	testq	%r14, %r14
	je	.L691
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L691:
	movq	-1816(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L692
	call	_ZdlPv@PLT
.L692:
	movq	-1616(%rbp), %rbx
	movq	-1624(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L693
	.p2align 4,,10
	.p2align 3
.L697:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L694
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L697
.L695:
	movq	-1624(%rbp), %r14
.L693:
	testq	%r14, %r14
	je	.L698
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L698:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L808
	addq	$1864, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L694:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L697
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L687:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L690
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L680:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L683
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L673:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L676
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L666:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L669
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L659:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L662
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L645:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L648
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L652:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L655
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L801:
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r9d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L619
	call	_ZdlPv@PLT
.L619:
	movq	(%r15), %rax
	movl	$2026, %edx
	movq	%r12, %rdi
	leaq	.LC3(%rip), %rsi
	leaq	-1792(%rbp), %r15
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rcx, -1904(%rbp)
	movq	%rax, -1888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1888(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1888(%rbp), %rax
	movl	$32, %edi
	movq	-1904(%rbp), %xmm2
	movq	$0, -1776(%rbp)
	movq	%rax, %xmm0
	movq	%rax, %xmm6
	punpcklqdq	%xmm0, %xmm0
	punpcklqdq	%xmm6, %xmm2
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -1904(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -1792(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movq	%r15, %rsi
	leaq	32(%rax), %rdx
	leaq	-1248(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L620
	call	_ZdlPv@PLT
.L620:
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1752(%rbp)
	jne	.L809
.L621:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1376(%rbp)
	je	.L623
.L802:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1440(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r8d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L624
	call	_ZdlPv@PLT
.L624:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-480(%rbp), %rdi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L625
	call	_ZdlPv@PLT
.L625:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1184(%rbp)
	je	.L626
.L803:
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1248(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L627
	call	_ZdlPv@PLT
.L627:
	movq	(%r15), %rax
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	24(%rax), %rax
	movq	%rcx, -1904(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -1888(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r13, %rcx
	movq	%rbx, %rdi
	movq	-1888(%rbp), %rdx
	call	_ZN2v88internal30Cast20ATFastJSArrayForCopy_137EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rcx
	movq	%r15, %xmm3
	pxor	%xmm0, %xmm0
	movhps	-1904(%rbp), %xmm3
	movl	$32, %edi
	movq	%rax, -72(%rbp)
	leaq	-1792(%rbp), %r15
	movq	%rcx, -80(%rbp)
	movaps	%xmm3, -1904(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -1792(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	32(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L628
	call	_ZdlPv@PLT
.L628:
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1752(%rbp)
	jne	.L810
.L629:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -992(%rbp)
	je	.L631
.L804:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rdi
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L632
	call	_ZdlPv@PLT
.L632:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-480(%rbp), %rdi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L633
	call	_ZdlPv@PLT
.L633:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L634
.L805:
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L635
	call	_ZdlPv@PLT
.L635:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	24(%rax), %rdx
	movdqu	(%rax), %xmm4
	movaps	%xmm0, -1760(%rbp)
	movq	$0, -1744(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm4, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -1760(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L636
	call	_ZdlPv@PLT
.L636:
	movq	-1848(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L806:
	movq	-1848(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%r15, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L638
	call	_ZdlPv@PLT
.L638:
	movq	(%rbx), %rax
	movl	$97, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r15
	movq	%rcx, -1888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r15, -80(%rbp)
	movhps	-1888(%rbp), %xmm0
	leaq	-288(%rbp), %r15
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm6
	leaq	24(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L639
	call	_ZdlPv@PLT
.L639:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -416(%rbp)
	je	.L640
.L807:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L641
	call	_ZdlPv@PLT
.L641:
	movq	-1872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L809:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1904(%rbp), %xmm7
	movl	$24, %edi
	movaps	%xmm0, -1792(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-1440(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L622
	call	_ZdlPv@PLT
.L622:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L810:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1904(%rbp), %xmm7
	movl	$24, %edi
	movaps	%xmm0, -1792(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-1056(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L630
	call	_ZdlPv@PLT
.L630:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L629
.L808:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22654:
	.size	_ZN2v88internal31Cast20ATFastJSArrayForCopy_1435EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, .-_ZN2v88internal31Cast20ATFastJSArrayForCopy_1435EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal38Cast27JSArgumentsObjectWithLength_1436EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal38Cast27JSArgumentsObjectWithLength_1436EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal38Cast27JSArgumentsObjectWithLength_1436EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal38Cast27JSArgumentsObjectWithLength_1436EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE:
.LFB22661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	leaq	-1632(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1760(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1800(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1864, %rsp
	movq	%rsi, -1888(%rbp)
	movq	%rdx, -1904(%rbp)
	movq	%rcx, -1872(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1800(%rbp)
	movq	%rdi, -1632(%rbp)
	movl	$48, %edi
	movq	$0, -1624(%rbp)
	movq	$0, -1616(%rbp)
	movq	$0, -1608(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1624(%rbp)
	leaq	-1576(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1608(%rbp)
	movq	%rdx, -1616(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1592(%rbp)
	movq	%rax, -1816(%rbp)
	movq	$0, -1600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1432(%rbp)
	movq	$0, -1424(%rbp)
	movq	%rax, -1440(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1432(%rbp)
	leaq	-1384(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -1832(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1864(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$96, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1840(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1848(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	$0, -472(%rbp)
	movq	%rax, -480(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1824(%rbp)
	movq	$0, -464(%rbp)
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	$0, -440(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1888(%rbp), %xmm1
	movaps	%xmm0, -1760(%rbp)
	movhps	-1904(%rbp), %xmm1
	movq	$0, -1744(%rbp)
	movaps	%xmm1, -1888(%rbp)
	call	_Znwm@PLT
	movdqa	-1888(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L812
	call	_ZdlPv@PLT
.L812:
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1568(%rbp)
	jne	.L996
	cmpq	$0, -1376(%rbp)
	jne	.L997
.L818:
	cmpq	$0, -1184(%rbp)
	jne	.L998
.L821:
	cmpq	$0, -992(%rbp)
	jne	.L999
.L826:
	cmpq	$0, -800(%rbp)
	jne	.L1000
.L829:
	cmpq	$0, -608(%rbp)
	leaq	-288(%rbp), %r15
	jne	.L1001
	cmpq	$0, -416(%rbp)
	jne	.L1002
.L835:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L837
	call	_ZdlPv@PLT
.L837:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L838
	call	_ZdlPv@PLT
.L838:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L839
	.p2align 4,,10
	.p2align 3
.L843:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L840
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L843
.L841:
	movq	-280(%rbp), %r14
.L839:
	testq	%r14, %r14
	je	.L844
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L844:
	movq	-1824(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L845
	call	_ZdlPv@PLT
.L845:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L846
	.p2align 4,,10
	.p2align 3
.L850:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L847
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L850
.L848:
	movq	-472(%rbp), %r14
.L846:
	testq	%r14, %r14
	je	.L851
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L851:
	movq	-1848(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L852
	call	_ZdlPv@PLT
.L852:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L853
	.p2align 4,,10
	.p2align 3
.L857:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L854
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L857
.L855:
	movq	-664(%rbp), %r14
.L853:
	testq	%r14, %r14
	je	.L858
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L858:
	movq	-1840(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L859
	call	_ZdlPv@PLT
.L859:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L860
	.p2align 4,,10
	.p2align 3
.L864:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L861
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L864
.L862:
	movq	-856(%rbp), %r14
.L860:
	testq	%r14, %r14
	je	.L865
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L865:
	movq	-1864(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L866
	call	_ZdlPv@PLT
.L866:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L867
	.p2align 4,,10
	.p2align 3
.L871:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L868
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L871
.L869:
	movq	-1048(%rbp), %r14
.L867:
	testq	%r14, %r14
	je	.L872
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L872:
	movq	-1832(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L873
	call	_ZdlPv@PLT
.L873:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L874
	.p2align 4,,10
	.p2align 3
.L878:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L875
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L878
.L876:
	movq	-1240(%rbp), %r14
.L874:
	testq	%r14, %r14
	je	.L879
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L879:
	movq	-1856(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L880
	call	_ZdlPv@PLT
.L880:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L881
	.p2align 4,,10
	.p2align 3
.L885:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L882
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L885
.L883:
	movq	-1432(%rbp), %r14
.L881:
	testq	%r14, %r14
	je	.L886
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L886:
	movq	-1816(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L887
	call	_ZdlPv@PLT
.L887:
	movq	-1616(%rbp), %rbx
	movq	-1624(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L888
	.p2align 4,,10
	.p2align 3
.L892:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L889
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L892
.L890:
	movq	-1624(%rbp), %r14
.L888:
	testq	%r14, %r14
	je	.L893
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L893:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1003
	addq	$1864, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L889:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L892
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L882:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L885
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L875:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L878
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L868:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L871
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L861:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L864
	jmp	.L862
	.p2align 4,,10
	.p2align 3
.L854:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L857
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L840:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L843
	jmp	.L841
	.p2align 4,,10
	.p2align 3
.L847:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L850
	jmp	.L848
	.p2align 4,,10
	.p2align 3
.L996:
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r9d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L814
	call	_ZdlPv@PLT
.L814:
	movq	(%r15), %rax
	movl	$2026, %edx
	movq	%r12, %rdi
	leaq	.LC3(%rip), %rsi
	leaq	-1792(%rbp), %r15
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rcx, -1904(%rbp)
	movq	%rax, -1888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1888(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1888(%rbp), %rax
	movl	$32, %edi
	movq	-1904(%rbp), %xmm2
	movq	$0, -1776(%rbp)
	movq	%rax, %xmm0
	movq	%rax, %xmm6
	punpcklqdq	%xmm0, %xmm0
	punpcklqdq	%xmm6, %xmm2
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -1904(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -1792(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movq	%r15, %rsi
	leaq	32(%rax), %rdx
	leaq	-1248(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L815
	call	_ZdlPv@PLT
.L815:
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1752(%rbp)
	jne	.L1004
.L816:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1376(%rbp)
	je	.L818
.L997:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1440(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r8d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L819
	call	_ZdlPv@PLT
.L819:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-480(%rbp), %rdi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L820
	call	_ZdlPv@PLT
.L820:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1184(%rbp)
	je	.L821
.L998:
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1248(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L822
	call	_ZdlPv@PLT
.L822:
	movq	(%r15), %rax
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	24(%rax), %rax
	movq	%rcx, -1904(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -1888(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r13, %rcx
	movq	%rbx, %rdi
	movq	-1888(%rbp), %rdx
	call	_ZN2v88internal37Cast27JSArgumentsObjectWithLength_133EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rcx
	movq	%r15, %xmm3
	pxor	%xmm0, %xmm0
	movhps	-1904(%rbp), %xmm3
	movl	$32, %edi
	movq	%rax, -72(%rbp)
	leaq	-1792(%rbp), %r15
	movq	%rcx, -80(%rbp)
	movaps	%xmm3, -1904(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -1792(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	32(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L823
	call	_ZdlPv@PLT
.L823:
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1752(%rbp)
	jne	.L1005
.L824:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -992(%rbp)
	je	.L826
.L999:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rdi
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L827
	call	_ZdlPv@PLT
.L827:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-480(%rbp), %rdi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L828
	call	_ZdlPv@PLT
.L828:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L829
.L1000:
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L830
	call	_ZdlPv@PLT
.L830:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	24(%rax), %rdx
	movdqu	(%rax), %xmm4
	movaps	%xmm0, -1760(%rbp)
	movq	$0, -1744(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm4, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -1760(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L831
	call	_ZdlPv@PLT
.L831:
	movq	-1848(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L1001:
	movq	-1848(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%r15, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L833
	call	_ZdlPv@PLT
.L833:
	movq	(%rbx), %rax
	movl	$107, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r15
	movq	%rcx, -1888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r15, -80(%rbp)
	movhps	-1888(%rbp), %xmm0
	leaq	-288(%rbp), %r15
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm6
	leaq	24(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L834
	call	_ZdlPv@PLT
.L834:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -416(%rbp)
	je	.L835
.L1002:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L836
	call	_ZdlPv@PLT
.L836:
	movq	-1872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L1004:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1904(%rbp), %xmm7
	movl	$24, %edi
	movaps	%xmm0, -1792(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-1440(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L817
	call	_ZdlPv@PLT
.L817:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L1005:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1904(%rbp), %xmm7
	movl	$24, %edi
	movaps	%xmm0, -1792(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-1056(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L825
	call	_ZdlPv@PLT
.L825:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L824
.L1003:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22661:
	.size	_ZN2v88internal38Cast27JSArgumentsObjectWithLength_1436EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, .-_ZN2v88internal38Cast27JSArgumentsObjectWithLength_1436EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal32UnsafeCast15ATNativeContext_1437EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32UnsafeCast15ATNativeContext_1437EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal32UnsafeCast15ATNativeContext_1437EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal32UnsafeCast15ATNativeContext_1437EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE:
.LFB22668:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-216(%rbp), %r14
	leaq	-688(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-600(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-696(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-656(%rbp), %rbx
	subq	$696, %rsp
	movq	%rsi, -736(%rbp)
	movq	%rdx, -720(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -696(%rbp)
	movq	%rdi, -656(%rbp)
	movl	$48, %edi
	movq	$0, -648(%rbp)
	movq	$0, -640(%rbp)
	movq	$0, -632(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -632(%rbp)
	movq	%rdx, -640(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -616(%rbp)
	movq	%rax, -648(%rbp)
	movq	$0, -624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	%rax, -464(%rbp)
	movq	$0, -440(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -456(%rbp)
	leaq	-408(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -440(%rbp)
	movq	%rdx, -448(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -424(%rbp)
	movq	%rax, -712(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	%rax, -272(%rbp)
	movq	$0, -248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -248(%rbp)
	movq	%rdx, -256(%rbp)
	xorl	%edx, %edx
	movq	%rax, -264(%rbp)
	movups	%xmm0, -232(%rbp)
	movq	$0, -240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-736(%rbp), %xmm1
	movaps	%xmm0, -688(%rbp)
	movhps	-720(%rbp), %xmm1
	movq	$0, -672(%rbp)
	movaps	%xmm1, -736(%rbp)
	call	_Znwm@PLT
	movdqa	-736(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -688(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1007
	call	_ZdlPv@PLT
.L1007:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -592(%rbp)
	jne	.L1075
.L1008:
	cmpq	$0, -400(%rbp)
	leaq	-272(%rbp), %rbx
	jne	.L1076
.L1011:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1014
	call	_ZdlPv@PLT
.L1014:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	16(%rax), %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1015
	call	_ZdlPv@PLT
.L1015:
	movq	-256(%rbp), %rbx
	movq	-264(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1016
	.p2align 4,,10
	.p2align 3
.L1020:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1017
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1020
.L1018:
	movq	-264(%rbp), %r14
.L1016:
	testq	%r14, %r14
	je	.L1021
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1021:
	movq	-712(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1022
	call	_ZdlPv@PLT
.L1022:
	movq	-448(%rbp), %rbx
	movq	-456(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1023
	.p2align 4,,10
	.p2align 3
.L1027:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1024
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1027
.L1025:
	movq	-456(%rbp), %r14
.L1023:
	testq	%r14, %r14
	je	.L1028
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1028:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1029
	call	_ZdlPv@PLT
.L1029:
	movq	-640(%rbp), %rbx
	movq	-648(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1030
	.p2align 4,,10
	.p2align 3
.L1034:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1031
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1034
.L1032:
	movq	-648(%rbp), %r13
.L1030:
	testq	%r13, %r13
	je	.L1035
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1035:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1077
	addq	$696, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1031:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1034
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1017:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1020
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L1024:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1027
	jmp	.L1025
	.p2align 4,,10
	.p2align 3
.L1075:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r15, %rsi
	movw	%di, (%rax)
	leaq	2(%rax), %rdx
	movq	%rbx, %rdi
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1009
	call	_ZdlPv@PLT
.L1009:
	movq	(%rbx), %rax
	movl	$2790, %edx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	%rcx, -736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm2
	movl	$24, %edi
	movq	-736(%rbp), %xmm0
	movq	%rbx, -64(%rbp)
	movq	$0, -672(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm3
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-464(%rbp), %rdi
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1010
	call	_ZdlPv@PLT
.L1010:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1008
	.p2align 4,,10
	.p2align 3
.L1076:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-464(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rsi
	movb	$7, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1012
	call	_ZdlPv@PLT
.L1012:
	movq	(%rbx), %rax
	movl	$175, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -720(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %edi
	movq	%rbx, -64(%rbp)
	movq	-736(%rbp), %xmm0
	movq	$0, -672(%rbp)
	leaq	-272(%rbp), %rbx
	movhps	-720(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movdqa	-80(%rbp), %xmm4
	leaq	24(%rax), %rdx
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1013
	call	_ZdlPv@PLT
.L1013:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1011
.L1077:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22668:
	.size	_ZN2v88internal32UnsafeCast15ATNativeContext_1437EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal32UnsafeCast15ATNativeContext_1437EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESD_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESD_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESD_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESD_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESD_:
.LFB27255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	$4, %edi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	leaq	-80(%rbp), %rsi
	movl	$101058311, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L1079
	call	_ZdlPv@PLT
.L1079:
	movq	(%r15), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1080
	movq	%rdx, (%r14)
.L1080:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1081
	movq	%rdx, 0(%r13)
.L1081:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1082
	movq	%rdx, (%r12)
.L1082:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L1078
	movq	%rax, (%rbx)
.L1078:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1101
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1101:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27255:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESD_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESD_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJEE9AddInputsEv,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJEE9AddInputsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJEE9AddInputsEv
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJEE9AddInputsEv, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJEE9AddInputsEv:
.LFB27259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rsi
	movq	$0, -16(%rbp)
	movaps	%xmm0, -32(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1102
	call	_ZdlPv@PLT
.L1102:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1109
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1109:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27259:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJEE9AddInputsEv, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJEE9AddInputsEv
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_7JSArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EESE_PNS8_IS6_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_7JSArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EESE_PNS8_IS6_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_7JSArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EESE_PNS8_IS6_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_7JSArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EESE_PNS8_IS6_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_7JSArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EESE_PNS8_IS6_EE:
.LFB27283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$5, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$56, %rsp
	movq	%r8, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$101058311, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1111
	movq	%rax, -96(%rbp)
	call	_ZdlPv@PLT
	movq	-96(%rbp), %rax
.L1111:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1112
	movq	%rdx, (%r15)
.L1112:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1113
	movq	%rdx, (%r14)
.L1113:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1114
	movq	%rdx, 0(%r13)
.L1114:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1115
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1115:
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L1110
	movq	%rax, (%rbx)
.L1110:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1137
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1137:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27283:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_7JSArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EESE_PNS8_IS6_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_7JSArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EESE_PNS8_IS6_EE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EESH_PNSB_IS6_EESH_SH_SH_SJ_SH_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESH_SJ_SH_SH_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EESH_PNSB_IS6_EESH_SH_SH_SJ_SH_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESH_SJ_SH_SH_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EESH_PNSB_IS6_EESH_SH_SH_SJ_SH_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESH_SJ_SH_SH_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EESH_PNSB_IS6_EESH_SH_SH_SJ_SH_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESH_SJ_SH_SH_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EESH_PNSB_IS6_EESH_SH_SH_SJ_SH_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESH_SJ_SH_SH_:
.LFB27340:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$17, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$152, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -88(%rbp)
	movq	%r9, -96(%rbp)
	movq	104(%rbp), %r15
	movq	%rax, -104(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	88(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	96(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movdqa	.LC4(%rip), %xmm0
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movb	$6, 16(%rax)
	leaq	17(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1139
	movq	%rax, -192(%rbp)
	call	_ZdlPv@PLT
	movq	-192(%rbp), %rax
.L1139:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1140
	movq	%rdx, (%r14)
.L1140:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1141
	movq	%rdx, 0(%r13)
.L1141:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1142
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1142:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1143
	movq	%rdx, (%rbx)
.L1143:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1144
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1144:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1145
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1145:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1146
	movq	-112(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1146:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1147
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1147:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1148
	movq	-128(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1148:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1149
	movq	-136(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1149:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1150
	movq	-144(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1150:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1151
	movq	-152(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1151:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1152
	movq	-160(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1152:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1153
	movq	-168(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1153:
	movq	112(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1154
	movq	-176(%rbp), %rdi
	movq	%rdx, (%rdi)
.L1154:
	movq	120(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1155
	movq	-184(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1155:
	movq	128(%rax), %rax
	testq	%rax, %rax
	je	.L1138
	movq	%rax, (%r15)
.L1138:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1213
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1213:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27340:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EESH_PNSB_IS6_EESH_SH_SH_SJ_SH_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESH_SJ_SH_SH_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EESH_PNSB_IS6_EESH_SH_SH_SJ_SH_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESH_SJ_SH_SH_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_S6_NS0_7IntPtrTESA_S5_S5_SA_NS0_10HeapObjectESA_SA_SA_SA_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EESJ_SJ_SJ_SL_SJ_PNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EESJ_SL_SJ_SJ_SL_PNSD_ISA_EEST_SJ_SJ_ST_PNSD_ISB_EEST_ST_ST_ST_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_S6_NS0_7IntPtrTESA_S5_S5_SA_NS0_10HeapObjectESA_SA_SA_SA_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EESJ_SJ_SJ_SL_SJ_PNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EESJ_SL_SJ_SJ_SL_PNSD_ISA_EEST_SJ_SJ_ST_PNSD_ISB_EEST_ST_ST_ST_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_S6_NS0_7IntPtrTESA_S5_S5_SA_NS0_10HeapObjectESA_SA_SA_SA_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EESJ_SJ_SJ_SL_SJ_PNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EESJ_SL_SJ_SJ_SL_PNSD_ISA_EEST_SJ_SJ_ST_PNSD_ISB_EEST_ST_ST_ST_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_S6_NS0_7IntPtrTESA_S5_S5_SA_NS0_10HeapObjectESA_SA_SA_SA_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EESJ_SJ_SJ_SL_SJ_PNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EESJ_SL_SJ_SJ_SL_PNSD_ISA_EEST_SJ_SJ_ST_PNSD_ISB_EEST_ST_ST_ST_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_S6_NS0_7IntPtrTESA_S5_S5_SA_NS0_10HeapObjectESA_SA_SA_SA_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EESJ_SJ_SJ_SL_SJ_PNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EESJ_SL_SJ_SJ_SL_PNSD_ISA_EEST_SJ_SJ_ST_PNSD_ISB_EEST_ST_ST_ST_:
.LFB27342:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$28, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$248, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	88(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	96(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	104(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	112(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	120(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	128(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	136(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	144(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	152(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	160(%rbp), %rax
	movq	%rax, -240(%rbp)
	movq	168(%rbp), %rax
	movq	%rax, -248(%rbp)
	movq	176(%rbp), %rax
	movq	%rax, -256(%rbp)
	movq	184(%rbp), %rax
	movq	%rax, -264(%rbp)
	movq	192(%rbp), %rax
	movq	%rax, -272(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movdqa	.LC4(%rip), %xmm0
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$505817156072834822, %rcx
	movl	$84215045, 24(%rax)
	leaq	28(%rax), %rdx
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1215
	movq	%rax, -280(%rbp)
	call	_ZdlPv@PLT
	movq	-280(%rbp), %rax
.L1215:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1216
	movq	%rdx, (%r15)
.L1216:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1217
	movq	%rdx, (%r14)
.L1217:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1218
	movq	%rdx, 0(%r13)
.L1218:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1219
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1219:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1220
	movq	%rdx, (%rbx)
.L1220:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1221
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1221:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1222
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1222:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1223
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1223:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1224
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1224:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1225
	movq	-128(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1225:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1226
	movq	-136(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1226:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1227
	movq	-144(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1227:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1228
	movq	-152(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1228:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1229
	movq	-160(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1229:
	movq	112(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1230
	movq	-168(%rbp), %rdi
	movq	%rdx, (%rdi)
.L1230:
	movq	120(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1231
	movq	-176(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1231:
	movq	128(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1232
	movq	-184(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1232:
	movq	136(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1233
	movq	-192(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1233:
	movq	144(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1234
	movq	-200(%rbp), %rdi
	movq	%rdx, (%rdi)
.L1234:
	movq	152(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1235
	movq	-208(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1235:
	movq	160(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1236
	movq	-216(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1236:
	movq	168(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1237
	movq	-224(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1237:
	movq	176(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1238
	movq	-232(%rbp), %rdi
	movq	%rdx, (%rdi)
.L1238:
	movq	184(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1239
	movq	-240(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1239:
	movq	192(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1240
	movq	-248(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1240:
	movq	200(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1241
	movq	-256(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1241:
	movq	208(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1242
	movq	-264(%rbp), %rdi
	movq	%rdx, (%rdi)
.L1242:
	movq	216(%rax), %rax
	testq	%rax, %rax
	je	.L1214
	movq	-272(%rbp), %rbx
	movq	%rax, (%rbx)
.L1214:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1333
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1333:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27342:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_S6_NS0_7IntPtrTESA_S5_S5_SA_NS0_10HeapObjectESA_SA_SA_SA_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EESJ_SJ_SJ_SL_SJ_PNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EESJ_SL_SJ_SJ_SL_PNSD_ISA_EEST_SJ_SJ_ST_PNSD_ISB_EEST_ST_ST_ST_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_S6_NS0_7IntPtrTESA_S5_S5_SA_NS0_10HeapObjectESA_SA_SA_SA_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EESJ_SJ_SJ_SL_SJ_PNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EESJ_SL_SJ_SJ_SL_PNSD_ISA_EEST_SJ_SJ_ST_PNSD_ISB_EEST_ST_ST_ST_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_NS0_6ObjectES6_NS0_7IntPtrTESB_S5_S5_SB_NS0_10HeapObjectESB_SB_SB_SB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS5_EESK_PNSE_IS6_EESK_SK_SK_SM_SK_PNSE_IS7_EEPNSE_IS8_EEPNSE_IS9_EESK_SM_SK_SK_PNSE_ISA_EESM_PNSE_ISB_EESW_SK_SK_SW_PNSE_ISC_EESW_SW_SW_SW_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_NS0_6ObjectES6_NS0_7IntPtrTESB_S5_S5_SB_NS0_10HeapObjectESB_SB_SB_SB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS5_EESK_PNSE_IS6_EESK_SK_SK_SM_SK_PNSE_IS7_EEPNSE_IS8_EEPNSE_IS9_EESK_SM_SK_SK_PNSE_ISA_EESM_PNSE_ISB_EESW_SK_SK_SW_PNSE_ISC_EESW_SW_SW_SW_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_NS0_6ObjectES6_NS0_7IntPtrTESB_S5_S5_SB_NS0_10HeapObjectESB_SB_SB_SB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS5_EESK_PNSE_IS6_EESK_SK_SK_SM_SK_PNSE_IS7_EEPNSE_IS8_EEPNSE_IS9_EESK_SM_SK_SK_PNSE_ISA_EESM_PNSE_ISB_EESW_SK_SK_SW_PNSE_ISC_EESW_SW_SW_SW_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_NS0_6ObjectES6_NS0_7IntPtrTESB_S5_S5_SB_NS0_10HeapObjectESB_SB_SB_SB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS5_EESK_PNSE_IS6_EESK_SK_SK_SM_SK_PNSE_IS7_EEPNSE_IS8_EEPNSE_IS9_EESK_SM_SK_SK_PNSE_ISA_EESM_PNSE_ISB_EESW_SK_SK_SW_PNSE_ISC_EESW_SW_SW_SW_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_NS0_6ObjectES6_NS0_7IntPtrTESB_S5_S5_SB_NS0_10HeapObjectESB_SB_SB_SB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS5_EESK_PNSE_IS6_EESK_SK_SK_SM_SK_PNSE_IS7_EEPNSE_IS8_EEPNSE_IS9_EESK_SM_SK_SK_PNSE_ISA_EESM_PNSE_ISB_EESW_SK_SK_SW_PNSE_ISC_EESW_SW_SW_SW_:
.LFB27358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$29, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$248, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	88(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	96(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	104(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	112(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	120(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	128(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	136(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	144(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	152(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	160(%rbp), %rax
	movq	%rax, -240(%rbp)
	movq	168(%rbp), %rax
	movq	%rax, -248(%rbp)
	movq	176(%rbp), %rax
	movq	%rax, -256(%rbp)
	movq	184(%rbp), %rax
	movq	%rax, -264(%rbp)
	movq	192(%rbp), %rax
	movq	%rax, -272(%rbp)
	movq	200(%rbp), %rax
	movq	%rax, -280(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movdqa	.LC4(%rip), %xmm0
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$361983438678853638, %rcx
	movl	$84215047, 24(%rax)
	leaq	29(%rax), %rdx
	movq	%rcx, 16(%rax)
	movb	$5, 28(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1335
	movq	%rax, -288(%rbp)
	call	_ZdlPv@PLT
	movq	-288(%rbp), %rax
.L1335:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1336
	movq	%rdx, (%r15)
.L1336:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1337
	movq	%rdx, (%r14)
.L1337:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1338
	movq	%rdx, 0(%r13)
.L1338:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1339
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1339:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1340
	movq	%rdx, (%rbx)
.L1340:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1341
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1341:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1342
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1342:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1343
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1343:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1344
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1344:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1345
	movq	-128(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1345:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1346
	movq	-136(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1346:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1347
	movq	-144(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1347:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1348
	movq	-152(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1348:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1349
	movq	-160(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1349:
	movq	112(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1350
	movq	-168(%rbp), %rdi
	movq	%rdx, (%rdi)
.L1350:
	movq	120(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1351
	movq	-176(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1351:
	movq	128(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1352
	movq	-184(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1352:
	movq	136(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1353
	movq	-192(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1353:
	movq	144(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1354
	movq	-200(%rbp), %rdi
	movq	%rdx, (%rdi)
.L1354:
	movq	152(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1355
	movq	-208(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1355:
	movq	160(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1356
	movq	-216(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1356:
	movq	168(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1357
	movq	-224(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1357:
	movq	176(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1358
	movq	-232(%rbp), %rdi
	movq	%rdx, (%rdi)
.L1358:
	movq	184(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1359
	movq	-240(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1359:
	movq	192(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1360
	movq	-248(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1360:
	movq	200(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1361
	movq	-256(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1361:
	movq	208(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1362
	movq	-264(%rbp), %rdi
	movq	%rdx, (%rdi)
.L1362:
	movq	216(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1363
	movq	-272(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1363:
	movq	224(%rax), %rax
	testq	%rax, %rax
	je	.L1334
	movq	-280(%rbp), %rsi
	movq	%rax, (%rsi)
.L1334:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1457
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1457:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27358:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_NS0_6ObjectES6_NS0_7IntPtrTESB_S5_S5_SB_NS0_10HeapObjectESB_SB_SB_SB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS5_EESK_PNSE_IS6_EESK_SK_SK_SM_SK_PNSE_IS7_EEPNSE_IS8_EEPNSE_IS9_EESK_SM_SK_SK_PNSE_ISA_EESM_PNSE_ISB_EESW_SK_SK_SW_PNSE_ISC_EESW_SW_SW_SW_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_NS0_6ObjectES6_NS0_7IntPtrTESB_S5_S5_SB_NS0_10HeapObjectESB_SB_SB_SB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS5_EESK_PNSE_IS6_EESK_SK_SK_SM_SK_PNSE_IS7_EEPNSE_IS8_EEPNSE_IS9_EESK_SM_SK_SK_PNSE_ISA_EESM_PNSE_ISB_EESW_SK_SK_SW_PNSE_ISC_EESW_SW_SW_SW_
	.section	.rodata._ZN2v88internal40HandleFastAliasedSloppyArgumentsSlice_39EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_27JSArgumentsObjectWithLengthEEENS4_INS0_3SmiEEESA_PNS1_18CodeAssemblerLabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"../../deps/v8/../../deps/v8/src/builtins/torque-internal.tq"
	.section	.text._ZN2v88internal40HandleFastAliasedSloppyArgumentsSlice_39EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_27JSArgumentsObjectWithLengthEEENS4_INS0_3SmiEEESA_PNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal40HandleFastAliasedSloppyArgumentsSlice_39EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_27JSArgumentsObjectWithLengthEEENS4_INS0_3SmiEEESA_PNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal40HandleFastAliasedSloppyArgumentsSlice_39EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_27JSArgumentsObjectWithLengthEEENS4_INS0_3SmiEEESA_PNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal40HandleFastAliasedSloppyArgumentsSlice_39EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_27JSArgumentsObjectWithLengthEEENS4_INS0_3SmiEEESA_PNS1_18CodeAssemblerLabelE:
.LFB22438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$1816, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, -9800(%rbp)
	movq	%rdi, %r13
	leaq	-9144(%rbp), %r15
	leaq	-8696(%rbp), %r12
	movq	%rsi, -9168(%rbp)
	leaq	-7928(%rbp), %rbx
	leaq	-8880(%rbp), %r14
	movq	%rdx, -9184(%rbp)
	movq	%rcx, -9200(%rbp)
	movq	%r8, -9216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -9144(%rbp)
	movq	%rdi, -8752(%rbp)
	movl	$96, %edi
	movq	$0, -8744(%rbp)
	movq	$0, -8736(%rbp)
	movq	$0, -8728(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r12, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -8728(%rbp)
	movq	%rdx, -8736(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -8712(%rbp)
	movq	%rax, -8744(%rbp)
	movq	$0, -8720(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$96, %edi
	movq	$0, -8552(%rbp)
	movq	$0, -8544(%rbp)
	movq	%rax, -8560(%rbp)
	movq	$0, -8536(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -8552(%rbp)
	leaq	-8504(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -8536(%rbp)
	movq	%rdx, -8544(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -8520(%rbp)
	movq	%rax, -9296(%rbp)
	movq	$0, -8528(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$96, %edi
	movq	$0, -8360(%rbp)
	movq	$0, -8352(%rbp)
	movq	%rax, -8368(%rbp)
	movq	$0, -8344(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -8360(%rbp)
	leaq	-8312(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -8344(%rbp)
	movq	%rdx, -8352(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -8328(%rbp)
	movq	%rax, -9312(%rbp)
	movq	$0, -8336(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$120, %edi
	movq	$0, -8168(%rbp)
	movq	$0, -8160(%rbp)
	movq	%rax, -8176(%rbp)
	movq	$0, -8152(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -8168(%rbp)
	leaq	-8120(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -8152(%rbp)
	movq	%rdx, -8160(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -8136(%rbp)
	movq	%rax, -9656(%rbp)
	movq	$0, -8144(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$144, %edi
	movq	$0, -7976(%rbp)
	movq	$0, -7968(%rbp)
	movq	%rax, -7984(%rbp)
	movq	$0, -7960(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rdx, -7960(%rbp)
	movq	%rdx, -7968(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -7944(%rbp)
	movq	%rax, -7976(%rbp)
	movq	$0, -7952(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$408, %edi
	movq	$0, -7784(%rbp)
	movq	$0, -7776(%rbp)
	movq	%rax, -7792(%rbp)
	movq	$0, -7768(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	408(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 400(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movq	%rax, -7784(%rbp)
	leaq	-7736(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7768(%rbp)
	movq	%rdx, -7776(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -7752(%rbp)
	movq	%rax, -9328(%rbp)
	movq	$0, -7760(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$408, %edi
	movq	$0, -7592(%rbp)
	movq	$0, -7584(%rbp)
	movq	%rax, -7600(%rbp)
	movq	$0, -7576(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	408(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 400(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movq	%rax, -7592(%rbp)
	leaq	-7544(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7576(%rbp)
	movq	%rdx, -7584(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -7560(%rbp)
	movq	%rax, -9536(%rbp)
	movq	$0, -7568(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$456, %edi
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	%rax, -7408(%rbp)
	movq	$0, -7384(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movups	%xmm0, (%rax)
	leaq	456(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movq	%rax, -7400(%rbp)
	movq	$0, 448(%rax)
	leaq	-7352(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7384(%rbp)
	movq	%rdx, -7392(%rbp)
	xorl	%edx, %edx
	movq	$0, -7376(%rbp)
	movups	%xmm0, -7368(%rbp)
	movq	%rax, -9568(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$288, %edi
	movq	$0, -7208(%rbp)
	movq	$0, -7200(%rbp)
	movq	%rax, -7216(%rbp)
	movq	$0, -7192(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -7208(%rbp)
	leaq	-7160(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7192(%rbp)
	movq	%rdx, -7200(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -7176(%rbp)
	movq	%rax, -9280(%rbp)
	movq	$0, -7184(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$336, %edi
	movq	$0, -7016(%rbp)
	movq	$0, -7008(%rbp)
	movq	%rax, -7024(%rbp)
	movq	$0, -7000(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -7016(%rbp)
	leaq	-6968(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7000(%rbp)
	movq	%rdx, -7008(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6984(%rbp)
	movq	%rax, -9320(%rbp)
	movq	$0, -6992(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$312, %edi
	movq	$0, -6824(%rbp)
	movq	$0, -6816(%rbp)
	movq	%rax, -6832(%rbp)
	movq	$0, -6808(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rax, -6824(%rbp)
	leaq	-6776(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6808(%rbp)
	movq	%rdx, -6816(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6792(%rbp)
	movq	%rax, -9344(%rbp)
	movq	$0, -6800(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$216, %edi
	movq	$0, -6632(%rbp)
	movq	$0, -6624(%rbp)
	movq	%rax, -6640(%rbp)
	movq	$0, -6616(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -6632(%rbp)
	leaq	-6584(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6616(%rbp)
	movq	%rdx, -6624(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6600(%rbp)
	movq	%rax, -9648(%rbp)
	movq	$0, -6608(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$240, %edi
	movq	$0, -6440(%rbp)
	movq	$0, -6432(%rbp)
	movq	%rax, -6448(%rbp)
	movq	$0, -6424(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -6440(%rbp)
	leaq	-6392(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6424(%rbp)
	movq	%rdx, -6432(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6408(%rbp)
	movq	%rax, -9360(%rbp)
	movq	$0, -6416(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$240, %edi
	movq	$0, -6248(%rbp)
	movq	$0, -6240(%rbp)
	movq	%rax, -6256(%rbp)
	movq	$0, -6232(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -6248(%rbp)
	leaq	-6200(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6232(%rbp)
	movq	%rdx, -6240(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6216(%rbp)
	movq	%rax, -9376(%rbp)
	movq	$0, -6224(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$240, %edi
	movq	$0, -6056(%rbp)
	movq	$0, -6048(%rbp)
	movq	%rax, -6064(%rbp)
	movq	$0, -6040(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -6056(%rbp)
	leaq	-6008(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6040(%rbp)
	movq	%rdx, -6048(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6024(%rbp)
	movq	%rax, -9384(%rbp)
	movq	$0, -6032(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$456, %edi
	movq	$0, -5864(%rbp)
	movq	$0, -5856(%rbp)
	movq	%rax, -5872(%rbp)
	movq	$0, -5848(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movups	%xmm0, (%rax)
	leaq	456(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movq	%rax, -5864(%rbp)
	movq	$0, 448(%rax)
	leaq	-5816(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5848(%rbp)
	movq	%rdx, -5856(%rbp)
	xorl	%edx, %edx
	movq	$0, -5840(%rbp)
	movups	%xmm0, -5832(%rbp)
	movq	%rax, -9424(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$456, %edi
	movq	$0, -5672(%rbp)
	movq	$0, -5664(%rbp)
	movq	%rax, -5680(%rbp)
	movq	$0, -5656(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movups	%xmm0, (%rax)
	leaq	456(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movq	%rax, -5672(%rbp)
	movq	$0, 448(%rax)
	leaq	-5624(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5656(%rbp)
	movq	%rdx, -5664(%rbp)
	xorl	%edx, %edx
	movq	$0, -5648(%rbp)
	movups	%xmm0, -5640(%rbp)
	movq	%rax, -9472(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$504, %edi
	movq	$0, -5480(%rbp)
	movq	$0, -5472(%rbp)
	movq	%rax, -5488(%rbp)
	movq	$0, -5464(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	504(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -5480(%rbp)
	movq	%rdx, -5464(%rbp)
	movups	%xmm0, 480(%rax)
	movq	$0, 496(%rax)
	leaq	-5432(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5472(%rbp)
	xorl	%edx, %edx
	movq	%rax, -9488(%rbp)
	movq	$0, -5456(%rbp)
	movups	%xmm0, -5448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$336, %edi
	movq	$0, -5288(%rbp)
	movq	$0, -5280(%rbp)
	movq	%rax, -5296(%rbp)
	movq	$0, -5272(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -5288(%rbp)
	leaq	-5240(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5272(%rbp)
	movq	%rdx, -5280(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5256(%rbp)
	movq	%rax, -9504(%rbp)
	movq	$0, -5264(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$384, %edi
	movq	$0, -5096(%rbp)
	movq	$0, -5088(%rbp)
	movq	%rax, -5104(%rbp)
	movq	$0, -5080(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	384(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movq	%rax, -5096(%rbp)
	leaq	-5048(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5080(%rbp)
	movq	%rdx, -5088(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5064(%rbp)
	movq	%rax, -9248(%rbp)
	movq	$0, -5072(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$360, %edi
	movq	$0, -4904(%rbp)
	movq	$0, -4896(%rbp)
	movq	%rax, -4912(%rbp)
	movq	$0, -4888(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	360(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 352(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movq	%rax, -4904(%rbp)
	leaq	-4856(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4888(%rbp)
	movq	%rdx, -4896(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4872(%rbp)
	movq	%rax, -9600(%rbp)
	movq	$0, -4880(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$408, %edi
	movq	$0, -4712(%rbp)
	movq	$0, -4704(%rbp)
	movq	%rax, -4720(%rbp)
	movq	$0, -4696(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	408(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 400(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movq	%rax, -4712(%rbp)
	leaq	-4664(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4696(%rbp)
	movq	%rdx, -4704(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4680(%rbp)
	movq	%rax, -9680(%rbp)
	movq	$0, -4688(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$408, %edi
	movq	$0, -4520(%rbp)
	movq	$0, -4512(%rbp)
	movq	%rax, -4528(%rbp)
	movq	$0, -4504(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	408(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 400(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movq	%rax, -4520(%rbp)
	leaq	-4472(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4504(%rbp)
	movq	%rdx, -4512(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4488(%rbp)
	movq	%rax, -9440(%rbp)
	movq	$0, -4496(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$672, %edi
	movq	$0, -4328(%rbp)
	movq	$0, -4320(%rbp)
	movq	%rax, -4336(%rbp)
	movq	$0, -4312(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	672(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -4328(%rbp)
	movq	%rdx, -4312(%rbp)
	movups	%xmm0, 480(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	movups	%xmm0, 544(%rax)
	movups	%xmm0, 560(%rax)
	movups	%xmm0, 576(%rax)
	movups	%xmm0, 592(%rax)
	movups	%xmm0, 608(%rax)
	movups	%xmm0, 624(%rax)
	movups	%xmm0, 640(%rax)
	movups	%xmm0, 656(%rax)
	leaq	-4280(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4320(%rbp)
	xorl	%edx, %edx
	movq	%rax, -9224(%rbp)
	movq	$0, -4304(%rbp)
	movups	%xmm0, -4296(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$672, %edi
	movq	$0, -4136(%rbp)
	movq	$0, -4128(%rbp)
	movq	%rax, -4144(%rbp)
	movq	$0, -4120(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	672(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -4136(%rbp)
	movq	%rdx, -4120(%rbp)
	movups	%xmm0, 480(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	movups	%xmm0, 544(%rax)
	movups	%xmm0, 560(%rax)
	movups	%xmm0, 576(%rax)
	movups	%xmm0, 592(%rax)
	movups	%xmm0, 608(%rax)
	movups	%xmm0, 624(%rax)
	movups	%xmm0, 640(%rax)
	movups	%xmm0, 656(%rax)
	leaq	-4088(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4128(%rbp)
	xorl	%edx, %edx
	movq	%rax, -9456(%rbp)
	movq	$0, -4112(%rbp)
	movups	%xmm0, -4104(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$720, %edi
	movq	$0, -3944(%rbp)
	movq	$0, -3936(%rbp)
	movq	%rax, -3952(%rbp)
	movq	$0, -3928(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	720(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -3944(%rbp)
	movq	%rdx, -3928(%rbp)
	movups	%xmm0, 480(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	movups	%xmm0, 544(%rax)
	movups	%xmm0, 560(%rax)
	movups	%xmm0, 576(%rax)
	movups	%xmm0, 592(%rax)
	movups	%xmm0, 608(%rax)
	movups	%xmm0, 624(%rax)
	movups	%xmm0, 640(%rax)
	movups	%xmm0, 656(%rax)
	movups	%xmm0, 672(%rax)
	movups	%xmm0, 688(%rax)
	movups	%xmm0, 704(%rax)
	leaq	-3896(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3936(%rbp)
	xorl	%edx, %edx
	movq	%rax, -9512(%rbp)
	movq	$0, -3920(%rbp)
	movups	%xmm0, -3912(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$552, %edi
	movq	$0, -3752(%rbp)
	movq	$0, -3744(%rbp)
	movq	%rax, -3760(%rbp)
	movq	$0, -3736(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	552(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -3752(%rbp)
	movq	%rdx, -3736(%rbp)
	movups	%xmm0, 480(%rax)
	movq	$0, 544(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	leaq	-3704(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3744(%rbp)
	xorl	%edx, %edx
	movq	%rax, -9520(%rbp)
	movq	$0, -3728(%rbp)
	movups	%xmm0, -3720(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$600, %edi
	movq	$0, -3560(%rbp)
	movq	$0, -3552(%rbp)
	movq	%rax, -3568(%rbp)
	movq	$0, -3544(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	600(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -3560(%rbp)
	movq	%rdx, -3544(%rbp)
	movups	%xmm0, 480(%rax)
	movq	$0, 592(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	movups	%xmm0, 544(%rax)
	movups	%xmm0, 560(%rax)
	movups	%xmm0, 576(%rax)
	leaq	-3512(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3552(%rbp)
	xorl	%edx, %edx
	movq	%rax, -9232(%rbp)
	movq	$0, -3536(%rbp)
	movups	%xmm0, -3528(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$576, %edi
	movq	$0, -3368(%rbp)
	movq	$0, -3360(%rbp)
	movq	%rax, -3376(%rbp)
	movq	$0, -3352(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	576(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -3368(%rbp)
	movq	%rdx, -3352(%rbp)
	movups	%xmm0, 480(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	movups	%xmm0, 544(%rax)
	movups	%xmm0, 560(%rax)
	leaq	-3320(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3360(%rbp)
	xorl	%edx, %edx
	movq	%rax, -9552(%rbp)
	movq	$0, -3344(%rbp)
	movups	%xmm0, -3336(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$432, %edi
	movq	$0, -3176(%rbp)
	movq	$0, -3168(%rbp)
	movq	%rax, -3184(%rbp)
	movq	$0, -3160(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movups	%xmm0, (%rax)
	leaq	432(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movq	%rax, -3176(%rbp)
	leaq	-3128(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3160(%rbp)
	movq	%rdx, -3168(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3144(%rbp)
	movq	$0, -3152(%rbp)
	movq	%rax, -9240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$432, %edi
	movq	$0, -2984(%rbp)
	movq	$0, -2976(%rbp)
	movq	%rax, -2992(%rbp)
	movq	$0, -2968(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movups	%xmm0, (%rax)
	leaq	432(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movq	%rax, -2984(%rbp)
	leaq	-2936(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2968(%rbp)
	movq	%rdx, -2976(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2952(%rbp)
	movq	$0, -2960(%rbp)
	movq	%rax, -9576(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$696, %edi
	movq	$0, -2792(%rbp)
	movq	$0, -2784(%rbp)
	movq	%rax, -2800(%rbp)
	movq	$0, -2776(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	696(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -2792(%rbp)
	movq	%rdx, -2776(%rbp)
	movups	%xmm0, 480(%rax)
	movq	$0, 688(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	movups	%xmm0, 544(%rax)
	movups	%xmm0, 560(%rax)
	movups	%xmm0, 576(%rax)
	movups	%xmm0, 592(%rax)
	movups	%xmm0, 608(%rax)
	movups	%xmm0, 624(%rax)
	movups	%xmm0, 640(%rax)
	movups	%xmm0, 656(%rax)
	movups	%xmm0, 672(%rax)
	leaq	-2744(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2784(%rbp)
	xorl	%edx, %edx
	movq	%rax, -9272(%rbp)
	movq	$0, -2768(%rbp)
	movups	%xmm0, -2760(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$696, %edi
	movq	$0, -2600(%rbp)
	movq	$0, -2592(%rbp)
	movq	%rax, -2608(%rbp)
	movq	$0, -2584(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	696(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -2600(%rbp)
	movq	%rdx, -2584(%rbp)
	movups	%xmm0, 480(%rax)
	movq	$0, 688(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	movups	%xmm0, 544(%rax)
	movups	%xmm0, 560(%rax)
	movups	%xmm0, 576(%rax)
	movups	%xmm0, 592(%rax)
	movups	%xmm0, 608(%rax)
	movups	%xmm0, 624(%rax)
	movups	%xmm0, 640(%rax)
	movups	%xmm0, 656(%rax)
	movups	%xmm0, 672(%rax)
	leaq	-2552(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2592(%rbp)
	xorl	%edx, %edx
	movq	%rax, -9776(%rbp)
	movq	$0, -2576(%rbp)
	movups	%xmm0, -2568(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$744, %edi
	movq	$0, -2408(%rbp)
	movq	$0, -2400(%rbp)
	movq	%rax, -2416(%rbp)
	movq	$0, -2392(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	744(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -2408(%rbp)
	movq	%rdx, -2392(%rbp)
	movups	%xmm0, 480(%rax)
	movq	$0, 736(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	movups	%xmm0, 544(%rax)
	movups	%xmm0, 560(%rax)
	movups	%xmm0, 576(%rax)
	movups	%xmm0, 592(%rax)
	movups	%xmm0, 608(%rax)
	movups	%xmm0, 624(%rax)
	movups	%xmm0, 640(%rax)
	movups	%xmm0, 656(%rax)
	movups	%xmm0, 672(%rax)
	movups	%xmm0, 688(%rax)
	movups	%xmm0, 704(%rax)
	movups	%xmm0, 720(%rax)
	leaq	-2360(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2400(%rbp)
	xorl	%edx, %edx
	movq	%rax, -9744(%rbp)
	movq	$0, -2384(%rbp)
	movups	%xmm0, -2376(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$576, %edi
	movq	$0, -2216(%rbp)
	movq	$0, -2208(%rbp)
	movq	%rax, -2224(%rbp)
	movq	$0, -2200(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	576(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -2216(%rbp)
	movq	%rdx, -2200(%rbp)
	movups	%xmm0, 480(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	movups	%xmm0, 544(%rax)
	movups	%xmm0, 560(%rax)
	leaq	-2168(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2208(%rbp)
	xorl	%edx, %edx
	movq	%rax, -9760(%rbp)
	movq	$0, -2192(%rbp)
	movups	%xmm0, -2184(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$624, %edi
	movq	$0, -2024(%rbp)
	movq	$0, -2016(%rbp)
	movq	%rax, -2032(%rbp)
	movq	$0, -2008(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	624(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -2024(%rbp)
	movq	%rdx, -2008(%rbp)
	movups	%xmm0, 480(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	movups	%xmm0, 544(%rax)
	movups	%xmm0, 560(%rax)
	movups	%xmm0, 576(%rax)
	movups	%xmm0, 592(%rax)
	movups	%xmm0, 608(%rax)
	leaq	-1976(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2016(%rbp)
	xorl	%edx, %edx
	movq	%rax, -9688(%rbp)
	movq	$0, -2000(%rbp)
	movups	%xmm0, -1992(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$600, %edi
	movq	$0, -1832(%rbp)
	movq	$0, -1824(%rbp)
	movq	%rax, -1840(%rbp)
	movq	$0, -1816(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	600(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -1832(%rbp)
	movq	%rdx, -1816(%rbp)
	movups	%xmm0, 480(%rax)
	movq	$0, 592(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	movups	%xmm0, 544(%rax)
	movups	%xmm0, 560(%rax)
	movups	%xmm0, 576(%rax)
	leaq	-1784(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1824(%rbp)
	xorl	%edx, %edx
	movq	%rax, -9712(%rbp)
	movq	$0, -1808(%rbp)
	movups	%xmm0, -1800(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$456, %edi
	movq	$0, -1640(%rbp)
	movq	$0, -1632(%rbp)
	movq	%rax, -1648(%rbp)
	movq	$0, -1624(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movups	%xmm0, (%rax)
	leaq	456(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movq	%rax, -1640(%rbp)
	movq	$0, 448(%rax)
	leaq	-1592(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1624(%rbp)
	movq	%rdx, -1632(%rbp)
	xorl	%edx, %edx
	movq	$0, -1616(%rbp)
	movups	%xmm0, -1608(%rbp)
	movq	%rax, -9728(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$456, %edi
	movq	$0, -1448(%rbp)
	movq	$0, -1440(%rbp)
	movq	%rax, -1456(%rbp)
	movq	$0, -1432(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movups	%xmm0, (%rax)
	leaq	456(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movq	%rax, -1448(%rbp)
	movq	$0, 448(%rax)
	leaq	-1400(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1432(%rbp)
	movq	%rdx, -1440(%rbp)
	xorl	%edx, %edx
	movq	$0, -1424(%rbp)
	movups	%xmm0, -1416(%rbp)
	movq	%rax, -9584(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$408, %edi
	movq	$0, -1256(%rbp)
	movq	$0, -1248(%rbp)
	movq	%rax, -1264(%rbp)
	movq	$0, -1240(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	408(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 400(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movq	%rax, -1256(%rbp)
	leaq	-1208(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1240(%rbp)
	movq	%rdx, -1248(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1224(%rbp)
	movq	%rax, -9784(%rbp)
	movq	$0, -1232(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$408, %edi
	movq	$0, -1064(%rbp)
	movq	$0, -1056(%rbp)
	movq	%rax, -1072(%rbp)
	movq	$0, -1048(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	408(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 400(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movq	%rax, -1064(%rbp)
	leaq	-1016(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1048(%rbp)
	movq	%rdx, -1056(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1032(%rbp)
	movq	%rax, -9696(%rbp)
	movq	$0, -1040(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$120, %edi
	movq	$0, -872(%rbp)
	movq	$0, -864(%rbp)
	movq	%rax, -880(%rbp)
	movq	$0, -856(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movups	%xmm0, (%rax)
	leaq	120(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -872(%rbp)
	leaq	-824(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -856(%rbp)
	movq	%rdx, -864(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -840(%rbp)
	movq	%rax, -9792(%rbp)
	movq	$0, -848(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r15, %rsi
	movq	$0, -680(%rbp)
	movq	%rax, -688(%rbp)
	leaq	-632(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -9640(%rbp)
	movq	$0, -672(%rbp)
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	$0, -648(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-9144(%rbp), %rax
	movl	$120, %edi
	movq	$0, -488(%rbp)
	movq	$0, -480(%rbp)
	movq	%rax, -496(%rbp)
	movq	$0, -472(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -488(%rbp)
	leaq	-440(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -472(%rbp)
	movq	%rdx, -480(%rbp)
	xorl	%edx, %edx
	movq	%rax, -9672(%rbp)
	movups	%xmm0, -456(%rbp)
	movq	$0, -464(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-9200(%rbp), %xmm1
	movq	-9168(%rbp), %xmm2
	movaps	%xmm0, -8880(%rbp)
	movhps	-9216(%rbp), %xmm1
	movq	$0, -8864(%rbp)
	movhps	-9184(%rbp), %xmm2
	movaps	%xmm1, -288(%rbp)
	movaps	%xmm2, -304(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm7
	movdqa	-288(%rbp), %xmm6
	movq	%r14, %rsi
	leaq	32(%rax), %rdx
	movq	%rax, -8880(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm6, 16(%rax)
	leaq	-8752(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -8864(%rbp)
	movq	%rdx, -8872(%rbp)
	movq	%rax, -9664(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1459
	call	_ZdlPv@PLT
.L1459:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8560(%rbp), %rax
	cmpq	$0, -8688(%rbp)
	movq	%rax, -9256(%rbp)
	leaq	-8368(%rbp), %rax
	movq	%rax, -9264(%rbp)
	jne	.L1955
.L1460:
	leaq	-688(%rbp), %rax
	cmpq	$0, -8496(%rbp)
	movq	%rax, -9168(%rbp)
	jne	.L1956
.L1463:
	leaq	-7984(%rbp), %rax
	cmpq	$0, -8304(%rbp)
	movq	%rax, -9392(%rbp)
	jne	.L1957
.L1465:
	cmpq	$0, -8112(%rbp)
	jne	.L1958
.L1469:
	leaq	-7792(%rbp), %rax
	cmpq	$0, -7920(%rbp)
	movq	%rax, -9400(%rbp)
	leaq	-7600(%rbp), %rax
	movq	%rax, -9408(%rbp)
	jne	.L1959
.L1472:
	leaq	-7408(%rbp), %rax
	cmpq	$0, -7728(%rbp)
	movq	%rax, -9416(%rbp)
	jne	.L1960
.L1476:
	leaq	-7216(%rbp), %rax
	cmpq	$0, -7536(%rbp)
	movq	%rax, -9296(%rbp)
	jne	.L1961
.L1479:
	leaq	-7024(%rbp), %rax
	cmpq	$0, -7344(%rbp)
	movq	%rax, -9312(%rbp)
	jne	.L1962
.L1482:
	cmpq	$0, -7152(%rbp)
	jne	.L1963
.L1485:
	leaq	-6832(%rbp), %rax
	cmpq	$0, -6960(%rbp)
	movq	%rax, -9280(%rbp)
	jne	.L1964
.L1487:
	leaq	-6448(%rbp), %rax
	cmpq	$0, -6768(%rbp)
	movq	%rax, -9320(%rbp)
	jne	.L1965
.L1490:
	cmpq	$0, -6576(%rbp)
	jne	.L1966
.L1495:
	leaq	-6256(%rbp), %rax
	cmpq	$0, -6384(%rbp)
	movq	%rax, -9344(%rbp)
	leaq	-6064(%rbp), %rax
	movq	%rax, -9328(%rbp)
	jne	.L1967
.L1498:
	cmpq	$0, -6192(%rbp)
	jne	.L1968
.L1502:
	leaq	-5872(%rbp), %rax
	cmpq	$0, -6000(%rbp)
	movq	%rax, -9360(%rbp)
	leaq	-5680(%rbp), %rax
	movq	%rax, -9376(%rbp)
	jne	.L1969
.L1505:
	leaq	-5488(%rbp), %rax
	cmpq	$0, -5808(%rbp)
	movq	%rax, -9384(%rbp)
	jne	.L1970
.L1509:
	leaq	-5296(%rbp), %rax
	cmpq	$0, -5616(%rbp)
	movq	%rax, -9536(%rbp)
	jne	.L1971
	cmpq	$0, -5424(%rbp)
	jne	.L1972
.L1515:
	cmpq	$0, -5232(%rbp)
	jne	.L1973
.L1518:
	leaq	-4912(%rbp), %rax
	cmpq	$0, -5040(%rbp)
	movq	%rax, -9568(%rbp)
	jne	.L1974
.L1520:
	leaq	-4720(%rbp), %rax
	cmpq	$0, -4848(%rbp)
	movq	%rax, -9216(%rbp)
	jne	.L1975
.L1523:
	leaq	-4528(%rbp), %rax
	cmpq	$0, -4656(%rbp)
	movq	%rax, -9424(%rbp)
	leaq	-1072(%rbp), %rax
	movq	%rax, -9616(%rbp)
	jne	.L1976
.L1526:
	leaq	-4144(%rbp), %rax
	cmpq	$0, -4464(%rbp)
	movq	%rax, -9472(%rbp)
	jne	.L1977
.L1529:
	leaq	-3952(%rbp), %rax
	cmpq	$0, -4272(%rbp)
	movq	%rax, -9488(%rbp)
	jne	.L1978
.L1532:
	leaq	-3760(%rbp), %rax
	cmpq	$0, -4080(%rbp)
	movq	%rax, -9504(%rbp)
	jne	.L1979
.L1534:
	cmpq	$0, -3888(%rbp)
	jne	.L1980
.L1536:
	cmpq	$0, -3696(%rbp)
	jne	.L1981
.L1539:
	leaq	-3376(%rbp), %rax
	cmpq	$0, -3504(%rbp)
	movq	%rax, -9512(%rbp)
	jne	.L1982
.L1541:
	leaq	-2992(%rbp), %rax
	cmpq	$0, -3312(%rbp)
	movq	%rax, -9520(%rbp)
	jne	.L1983
.L1544:
	leaq	-1648(%rbp), %rax
	cmpq	$0, -3120(%rbp)
	movq	%rax, -9456(%rbp)
	jne	.L1984
.L1548:
	leaq	-2608(%rbp), %rax
	cmpq	$0, -2928(%rbp)
	movq	%rax, -9632(%rbp)
	jne	.L1985
.L1551:
	leaq	-2416(%rbp), %rax
	cmpq	$0, -2736(%rbp)
	movq	%rax, -9552(%rbp)
	jne	.L1986
.L1555:
	leaq	-2224(%rbp), %rax
	cmpq	$0, -2544(%rbp)
	movq	%rax, -9576(%rbp)
	jne	.L1987
.L1557:
	leaq	-2032(%rbp), %rax
	cmpq	$0, -2352(%rbp)
	movq	%rax, -9600(%rbp)
	jne	.L1988
.L1559:
	cmpq	$0, -2160(%rbp)
	jne	.L1989
.L1562:
	leaq	-1840(%rbp), %rax
	cmpq	$0, -1968(%rbp)
	movq	%rax, -9440(%rbp)
	jne	.L1990
.L1564:
	leaq	-1456(%rbp), %rax
	cmpq	$0, -1776(%rbp)
	movq	%rax, -9200(%rbp)
	jne	.L1991
.L1567:
	cmpq	$0, -1584(%rbp)
	jne	.L1992
.L1570:
	cmpq	$0, -1392(%rbp)
	leaq	-1264(%rbp), %r12
	jne	.L1993
	cmpq	$0, -1200(%rbp)
	jne	.L1994
.L1577:
	leaq	-880(%rbp), %rax
	cmpq	$0, -1008(%rbp)
	movq	%rax, -9584(%rbp)
	jne	.L1995
.L1579:
	leaq	-496(%rbp), %rax
	cmpq	$0, -816(%rbp)
	movq	%rax, -9184(%rbp)
	jne	.L1996
	cmpq	$0, -624(%rbp)
	jne	.L1997
.L1583:
	movq	-9672(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -8864(%rbp)
	movaps	%xmm0, -8880(%rbp)
	call	_Znwm@PLT
	movq	-9184(%rbp), %rdi
	movq	%r14, %rsi
	movl	$101058311, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -8880(%rbp)
	movq	%rdx, -8864(%rbp)
	movq	%rdx, -8872(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1585
	call	_ZdlPv@PLT
.L1585:
	movq	(%rbx), %rax
	movq	-9184(%rbp), %rdi
	movq	32(%rax), %r13
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9168(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9584(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9616(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9200(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9456(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9440(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9600(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9576(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9552(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9632(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9272(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2768(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1586
	call	_ZdlPv@PLT
.L1586:
	movq	-2784(%rbp), %rbx
	movq	-2792(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1587
	.p2align 4,,10
	.p2align 3
.L1591:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1588
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1591
.L1589:
	movq	-2792(%rbp), %r12
.L1587:
	testq	%r12, %r12
	je	.L1592
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1592:
	movq	-9520(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9240(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1593
	call	_ZdlPv@PLT
.L1593:
	movq	-3168(%rbp), %rbx
	movq	-3176(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1594
	.p2align 4,,10
	.p2align 3
.L1598:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1595
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1598
.L1596:
	movq	-3176(%rbp), %r12
.L1594:
	testq	%r12, %r12
	je	.L1599
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1599:
	movq	-9512(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9232(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1600
	call	_ZdlPv@PLT
.L1600:
	movq	-3552(%rbp), %rbx
	movq	-3560(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1601
	.p2align 4,,10
	.p2align 3
.L1605:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1602
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L1605
.L1603:
	movq	-3560(%rbp), %r12
.L1601:
	testq	%r12, %r12
	je	.L1606
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1606:
	movq	-9504(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9488(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9472(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9224(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-4304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1607
	call	_ZdlPv@PLT
.L1607:
	movq	-4320(%rbp), %rbx
	movq	-4328(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1608
	.p2align 4,,10
	.p2align 3
.L1612:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1609
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1612
.L1610:
	movq	-4328(%rbp), %r12
.L1608:
	testq	%r12, %r12
	je	.L1613
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1613:
	movq	-9424(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9216(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9568(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9248(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-5072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1614
	call	_ZdlPv@PLT
.L1614:
	movq	-5088(%rbp), %rbx
	movq	-5096(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1615
	.p2align 4,,10
	.p2align 3
.L1619:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1616
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1619
.L1617:
	movq	-5096(%rbp), %r12
.L1615:
	testq	%r12, %r12
	je	.L1620
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1620:
	movq	-9536(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9384(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9376(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9360(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9328(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9344(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9320(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9648(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-6608(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1621
	call	_ZdlPv@PLT
.L1621:
	movq	-6624(%rbp), %rbx
	movq	-6632(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1622
	.p2align 4,,10
	.p2align 3
.L1626:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1623
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1626
.L1624:
	movq	-6632(%rbp), %r12
.L1622:
	testq	%r12, %r12
	je	.L1627
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1627:
	movq	-9280(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9312(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9296(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9416(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9408(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9400(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9392(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9656(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-8144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1628
	call	_ZdlPv@PLT
.L1628:
	movq	-8160(%rbp), %rbx
	movq	-8168(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1629
	.p2align 4,,10
	.p2align 3
.L1633:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1630
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1633
.L1631:
	movq	-8168(%rbp), %r12
.L1629:
	testq	%r12, %r12
	je	.L1634
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1634:
	movq	-9264(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9256(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-9664(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1998
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1630:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1633
	jmp	.L1631
	.p2align 4,,10
	.p2align 3
.L1623:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1626
	jmp	.L1624
	.p2align 4,,10
	.p2align 3
.L1616:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1619
	jmp	.L1617
	.p2align 4,,10
	.p2align 3
.L1609:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1612
	jmp	.L1610
	.p2align 4,,10
	.p2align 3
.L1602:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1605
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L1588:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1591
	jmp	.L1589
	.p2align 4,,10
	.p2align 3
.L1595:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1598
	jmp	.L1596
	.p2align 4,,10
	.p2align 3
.L1955:
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	$0, -8936(%rbp)
	movq	$0, -8928(%rbp)
	movq	$0, -8920(%rbp)
	movq	$0, -8912(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-9664(%rbp), %rdi
	leaq	-8920(%rbp), %rcx
	leaq	-8912(%rbp), %r8
	leaq	-8928(%rbp), %rdx
	leaq	-8936(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESD_
	movl	$34, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16382, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8912(%rbp), %r8
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r8, -9168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-9168(%rbp), %r8
	movq	%r14, %rdi
	movq	%rax, %r12
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler24IntPtrGreaterThanOrEqualENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-8920(%rbp), %xmm7
	movq	-8936(%rbp), %xmm3
	movaps	%xmm0, -8880(%rbp)
	movhps	-8912(%rbp), %xmm7
	movq	$0, -8864(%rbp)
	movhps	-8928(%rbp), %xmm3
	movaps	%xmm7, -9184(%rbp)
	movaps	%xmm3, -9168(%rbp)
	movaps	%xmm3, -304(%rbp)
	movaps	%xmm7, -288(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm7
	movdqa	-288(%rbp), %xmm6
	movq	%r14, %rsi
	movq	-9256(%rbp), %rdi
	leaq	32(%rax), %rdx
	movq	%rax, -8880(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -8864(%rbp)
	movq	%rdx, -8872(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1461
	call	_ZdlPv@PLT
.L1461:
	movdqa	-9168(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-9184(%rbp), %xmm4
	movaps	%xmm0, -8880(%rbp)
	movaps	%xmm2, -304(%rbp)
	movaps	%xmm4, -288(%rbp)
	movq	$0, -8864(%rbp)
	call	_Znwm@PLT
	movdqa	-304(%rbp), %xmm7
	movdqa	-288(%rbp), %xmm5
	movq	%r14, %rsi
	leaq	32(%rax), %rdx
	movq	%rax, -8880(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm5, 16(%rax)
	leaq	-8368(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -8864(%rbp)
	movq	%rdx, -8872(%rbp)
	movq	%rax, -9264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1462
	call	_ZdlPv@PLT
.L1462:
	movq	-9312(%rbp), %rcx
	movq	-9296(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1460
	.p2align 4,,10
	.p2align 3
.L1961:
	movq	-9536(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$17, %edi
	movq	$0, -8864(%rbp)
	movaps	%xmm0, -8880(%rbp)
	call	_Znwm@PLT
	movdqa	.LC6(%rip), %xmm0
	movq	-9408(%rbp), %rdi
	movq	%r14, %rsi
	movb	$5, 16(%rax)
	leaq	17(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -8880(%rbp)
	movq	%rdx, -8864(%rbp)
	movq	%rdx, -8872(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1480
	call	_ZdlPv@PLT
.L1480:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	40(%rax), %rdx
	movq	8(%rax), %rbx
	movq	24(%rax), %rsi
	movq	(%rax), %rcx
	movq	%rdx, -9328(%rbp)
	movq	56(%rax), %rdx
	movq	%rbx, -9200(%rbp)
	movq	16(%rax), %rbx
	movq	%rdx, -9536(%rbp)
	movq	72(%rax), %rdx
	movq	%rsi, -9296(%rbp)
	movq	32(%rax), %rsi
	movq	%rdx, -9616(%rbp)
	movq	80(%rax), %rdx
	movq	48(%rax), %r12
	movq	%rbx, -9216(%rbp)
	movq	64(%rax), %rbx
	movq	88(%rax), %rax
	movq	%rsi, -9312(%rbp)
	leaq	.LC5(%rip), %rsi
	movq	%rdx, -9632(%rbp)
	movl	$41, %edx
	movq	%rcx, -9184(%rbp)
	movq	%rax, -9824(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-304(%rbp), %rsi
	leaq	-208(%rbp), %rdx
	movq	%r14, %rdi
	movq	-9184(%rbp), %xmm0
	movq	$0, -8864(%rbp)
	movhps	-9200(%rbp), %xmm0
	movaps	%xmm0, -304(%rbp)
	movq	-9216(%rbp), %xmm0
	movhps	-9296(%rbp), %xmm0
	movaps	%xmm0, -288(%rbp)
	movq	-9312(%rbp), %xmm0
	movhps	-9328(%rbp), %xmm0
	movaps	%xmm0, -272(%rbp)
	movq	%r12, %xmm0
	movhps	-9536(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	%rbx, %xmm0
	movhps	-9616(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-9632(%rbp), %xmm0
	movhps	-9824(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8880(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-7216(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -9296(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1481
	call	_ZdlPv@PLT
.L1481:
	movq	-9280(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1479
	.p2align 4,,10
	.p2align 3
.L1960:
	movq	-9328(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$17, %edi
	movq	$0, -8864(%rbp)
	movaps	%xmm0, -8880(%rbp)
	call	_Znwm@PLT
	movdqa	.LC6(%rip), %xmm0
	movq	-9400(%rbp), %rdi
	movq	%r14, %rsi
	movb	$5, 16(%rax)
	leaq	17(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -8880(%rbp)
	movq	%rdx, -8864(%rbp)
	movq	%rdx, -8872(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1477
	call	_ZdlPv@PLT
.L1477:
	movq	(%rbx), %rax
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	72(%rax), %r10
	movq	%rsi, -9312(%rbp)
	movq	88(%rax), %r11
	movq	32(%rax), %rsi
	movq	%rdx, -9416(%rbp)
	movq	48(%rax), %rdx
	movq	%rdi, -9632(%rbp)
	movq	64(%rax), %rdi
	movq	120(%rax), %r9
	movq	%rcx, -9200(%rbp)
	movq	%rbx, -9216(%rbp)
	movq	104(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%r10, -9840(%rbp)
	movq	%r11, -9872(%rbp)
	movq	80(%rax), %r10
	movq	112(%rax), %r11
	movq	%rsi, -9328(%rbp)
	leaq	.LC5(%rip), %rsi
	movq	%rdx, -9616(%rbp)
	movl	$39, %edx
	movq	96(%rax), %r12
	movq	%rdi, -9824(%rbp)
	movq	%r15, %rdi
	movq	%r10, -9856(%rbp)
	movq	%r11, -9888(%rbp)
	movq	%r9, -9904(%rbp)
	movq	%rcx, -9184(%rbp)
	movq	%rbx, -9296(%rbp)
	movq	128(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -9920(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-9920(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrMulENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -9920(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-9920(%rbp), %rdx
	movq	-9184(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -9920(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$38, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-9920(%rbp), %rcx
	call	_ZN2v88internal45UnsafeNewReference20UT5ATSmi10HeapObject_1411EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	-8880(%rbp), %rax
	leaq	-304(%rbp), %rsi
	movq	-9200(%rbp), %xmm0
	leaq	-152(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rbx, -176(%rbp)
	movhps	-9216(%rbp), %xmm0
	movq	%rax, -168(%rbp)
	movq	-8872(%rbp), %rax
	movaps	%xmm0, -304(%rbp)
	movq	-9296(%rbp), %xmm0
	movq	%rax, -160(%rbp)
	movhps	-9312(%rbp), %xmm0
	movq	$0, -8864(%rbp)
	movaps	%xmm0, -288(%rbp)
	movq	-9328(%rbp), %xmm0
	movhps	-9416(%rbp), %xmm0
	movaps	%xmm0, -272(%rbp)
	movq	-9616(%rbp), %xmm0
	movhps	-9632(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	-9824(%rbp), %xmm0
	movhps	-9840(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-9856(%rbp), %xmm0
	movhps	-9872(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	%r12, %xmm0
	movhps	-9184(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-9888(%rbp), %xmm0
	movhps	-9904(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8880(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-7408(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -9416(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1478
	call	_ZdlPv@PLT
.L1478:
	movq	-9568(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1476
	.p2align 4,,10
	.p2align 3
.L1959:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	leaq	-304(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1799, %eax
	movq	%r12, %rsi
	movq	%r14, %rdi
	pxor	%xmm0, %xmm0
	leaq	-298(%rbp), %rdx
	movw	%ax, -300(%rbp)
	movaps	%xmm0, -8880(%rbp)
	movl	$101058311, -304(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9392(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1473
	call	_ZdlPv@PLT
.L1473:
	movq	(%rbx), %rax
	movl	$36, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	%rcx, -9400(%rbp)
	movq	16(%rax), %rcx
	movq	%rbx, -9616(%rbp)
	movq	24(%rax), %rbx
	movq	40(%rax), %rax
	movq	%rcx, -9296(%rbp)
	movq	%rbx, -9312(%rbp)
	movq	%rax, -9184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$38, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$8, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-9184(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -9216(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$40, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$2, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-9216(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -9632(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$39, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$44, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-9312(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-9296(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -9824(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$46, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$8, %esi
	movq	%r15, %rdi
	movq	%rax, -9408(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-9184(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$50, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal40Convert8ATintptr17ATconstexpr_int31_1415EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$51, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -9200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$37, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-9200(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, -9840(%rbp)
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -9416(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-9840(%rbp), %r8
	movq	-9416(%rbp), %rdx
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -9416(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm1
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-9184(%rbp), %rax
	movq	-9200(%rbp), %rcx
	leaq	-168(%rbp), %rbx
	pxor	%xmm0, %xmm0
	movq	-9408(%rbp), %rdx
	movaps	%xmm0, -8880(%rbp)
	movq	-9632(%rbp), %xmm4
	movq	-9296(%rbp), %xmm6
	movq	%rax, %xmm5
	movq	-9400(%rbp), %xmm7
	movq	%rcx, %xmm3
	punpcklqdq	%xmm3, %xmm1
	movhps	-9824(%rbp), %xmm4
	movhps	-9216(%rbp), %xmm5
	movq	%rdx, -232(%rbp)
	movhps	-9312(%rbp), %xmm6
	movhps	-9616(%rbp), %xmm7
	movq	%rdx, -200(%rbp)
	movq	%rbx, %rdx
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm1, -192(%rbp)
	movaps	%xmm1, -9312(%rbp)
	movq	%rax, -240(%rbp)
	movq	%rax, -208(%rbp)
	movq	%rcx, -176(%rbp)
	movaps	%xmm4, -9632(%rbp)
	movaps	%xmm5, -9824(%rbp)
	movaps	%xmm6, -9296(%rbp)
	movaps	%xmm7, -9216(%rbp)
	movaps	%xmm7, -304(%rbp)
	movaps	%xmm6, -288(%rbp)
	movaps	%xmm5, -272(%rbp)
	movaps	%xmm4, -256(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-7792(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -9400(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	movdqa	-9312(%rbp), %xmm1
	testq	%rdi, %rdi
	je	.L1474
	call	_ZdlPv@PLT
	movdqa	-9312(%rbp), %xmm1
.L1474:
	movq	-9184(%rbp), %xmm0
	movdqa	-9216(%rbp), %xmm6
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movdqa	-9296(%rbp), %xmm2
	movdqa	-9824(%rbp), %xmm4
	movq	%r14, %rdi
	movaps	%xmm1, -224(%rbp)
	movhps	-9408(%rbp), %xmm0
	movdqa	-9632(%rbp), %xmm7
	movq	-9200(%rbp), %rax
	movaps	%xmm6, -304(%rbp)
	movaps	%xmm0, -240(%rbp)
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -176(%rbp)
	movaps	%xmm2, -288(%rbp)
	movaps	%xmm4, -272(%rbp)
	movaps	%xmm7, -256(%rbp)
	movaps	%xmm1, -192(%rbp)
	movaps	%xmm0, -8880(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-7600(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -9408(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1475
	call	_ZdlPv@PLT
.L1475:
	movq	-9536(%rbp), %rcx
	movq	-9328(%rbp), %rdx
	movq	%r15, %rdi
	movq	-9416(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1472
	.p2align 4,,10
	.p2align 3
.L1958:
	movq	-9656(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-8176(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-304(%rbp), %rsi
	leaq	-299(%rbp), %rdx
	movq	%r14, %rdi
	movaps	%xmm0, -8880(%rbp)
	movl	$101058311, -304(%rbp)
	movb	$7, -300(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1470
	call	_ZdlPv@PLT
.L1470:
	movq	-9168(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	$0, -8864(%rbp)
	movaps	%xmm0, -8880(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1471
	call	_ZdlPv@PLT
.L1471:
	movq	-9640(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1957:
	movq	-9312(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -8944(%rbp)
	movq	$0, -8936(%rbp)
	movq	$0, -8928(%rbp)
	movq	$0, -8920(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-9264(%rbp), %rdi
	leaq	-8928(%rbp), %rcx
	leaq	-8920(%rbp), %r8
	leaq	-8936(%rbp), %rdx
	leaq	-8944(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESD_
	movl	$37, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8936(%rbp), %r8
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%r8, -9184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-9184(%rbp), %r8
	movzwl	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE(%rip), %esi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	movq	%rax, -9200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal35Cast25ATSloppyArgumentsElements_105EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, -272(%rbp)
	movq	-8928(%rbp), %xmm0
	leaq	-304(%rbp), %r12
	movq	-8944(%rbp), %xmm1
	movq	%rax, -264(%rbp)
	leaq	-8912(%rbp), %rax
	movq	%r12, %rsi
	movhps	-8920(%rbp), %xmm0
	movq	%rax, %rdi
	leaq	-256(%rbp), %rdx
	movq	%rax, -9184(%rbp)
	movhps	-8936(%rbp), %xmm1
	movaps	%xmm0, -288(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -304(%rbp)
	movaps	%xmm0, -8912(%rbp)
	movq	$0, -8896(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9392(%rbp), %rdi
	movq	-9184(%rbp), %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8912(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1466
	call	_ZdlPv@PLT
.L1466:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -8872(%rbp)
	jne	.L1999
.L1467:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L1465
	.p2align 4,,10
	.p2align 3
.L1956:
	movq	-9296(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -8936(%rbp)
	movq	$0, -8928(%rbp)
	movq	$0, -8920(%rbp)
	movq	$0, -8912(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-9256(%rbp), %rdi
	leaq	-8920(%rbp), %rcx
	leaq	-8928(%rbp), %rdx
	leaq	-8936(%rbp), %rsi
	leaq	-8912(%rbp), %r8
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS7_IS4_EEPNS7_IS5_EESD_
	movq	-9168(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	$0, -8864(%rbp)
	movaps	%xmm0, -8880(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1464
	call	_ZdlPv@PLT
.L1464:
	movq	-9640(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1463
	.p2align 4,,10
	.p2align 3
.L1963:
	movq	-9280(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-304(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-292(%rbp), %rdx
	movabsq	$434041041323427591, %rax
	movaps	%xmm0, -8880(%rbp)
	movq	%rax, -304(%rbp)
	movl	$84215047, -296(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9296(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1486
	call	_ZdlPv@PLT
.L1486:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L1485
	.p2align 4,,10
	.p2align 3
.L1962:
	movq	-9568(%rbp), %rsi
	movq	%r15, %rdi
	movl	$1797, %r12d
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC6(%rip), %xmm0
	movw	%r12w, -288(%rbp)
	leaq	-304(%rbp), %r12
	leaq	-285(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movb	$5, -286(%rbp)
	movaps	%xmm0, -304(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8880(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9416(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1483
	call	_ZdlPv@PLT
.L1483:
	movq	(%rbx), %rax
	movq	56(%rax), %rdi
	movq	8(%rax), %rbx
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	88(%rax), %r10
	movq	(%rax), %rcx
	movq	%rdi, -9616(%rbp)
	movq	72(%rax), %rdi
	movq	%rbx, -9200(%rbp)
	movq	16(%rax), %rbx
	movq	%rsi, -9312(%rbp)
	movq	%rdx, -9536(%rbp)
	movq	32(%rax), %rsi
	movq	48(%rax), %rdx
	movq	%rdi, -9632(%rbp)
	movq	80(%rax), %rdi
	movq	%rbx, -9216(%rbp)
	movq	%r10, -9840(%rbp)
	movq	64(%rax), %rbx
	movq	136(%rax), %r10
	movq	144(%rax), %rax
	movq	%rsi, -9328(%rbp)
	leaq	.LC5(%rip), %rsi
	movq	%rdx, -9568(%rbp)
	movl	$51, %edx
	movq	%rdi, -9824(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -9184(%rbp)
	movq	%r10, -9856(%rbp)
	movq	%rax, -9872(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-192(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-9184(%rbp), %xmm0
	movq	$0, -8864(%rbp)
	movhps	-9200(%rbp), %xmm0
	movaps	%xmm0, -304(%rbp)
	movq	-9216(%rbp), %xmm0
	movhps	-9312(%rbp), %xmm0
	movaps	%xmm0, -288(%rbp)
	movq	-9328(%rbp), %xmm0
	movhps	-9536(%rbp), %xmm0
	movaps	%xmm0, -272(%rbp)
	movq	-9568(%rbp), %xmm0
	movhps	-9616(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	%rbx, %xmm0
	movhps	-9632(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-9824(%rbp), %xmm0
	movhps	-9840(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-9856(%rbp), %xmm0
	movhps	-9872(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8880(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-7024(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -9312(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1484
	call	_ZdlPv@PLT
.L1484:
	movq	-9320(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1482
	.p2align 4,,10
	.p2align 3
.L1968:
	movq	-9376(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -8864(%rbp)
	movaps	%xmm0, -8880(%rbp)
	call	_Znwm@PLT
	movl	$1543, %r10d
	movq	-9344(%rbp), %rdi
	movq	%r14, %rsi
	movabsq	$434041041323427591, %rcx
	movw	%r10w, 8(%rax)
	leaq	10(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%rax, -8880(%rbp)
	movq	%rdx, -8864(%rbp)
	movq	%rdx, -8872(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1503
	call	_ZdlPv@PLT
.L1503:
	movq	-9168(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	$0, -8864(%rbp)
	movaps	%xmm0, -8880(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1504
	call	_ZdlPv@PLT
.L1504:
	movq	-9640(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1502
	.p2align 4,,10
	.p2align 3
.L1967:
	movq	-9360(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-304(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movabsq	$434041041323427591, %rax
	movl	$1800, %r11d
	leaq	-294(%rbp), %rdx
	movaps	%xmm0, -8880(%rbp)
	movq	%rax, -304(%rbp)
	movw	%r11w, -296(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9320(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1499
	call	_ZdlPv@PLT
.L1499:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	%rsi, -9536(%rbp)
	movq	32(%rax), %rsi
	movq	%rdx, -9616(%rbp)
	movq	48(%rax), %rdx
	movq	%rcx, -9344(%rbp)
	movq	56(%rax), %rcx
	movq	%rbx, -9328(%rbp)
	movq	16(%rax), %rbx
	movq	72(%rax), %rax
	movq	%rsi, -9568(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -9632(%rbp)
	movl	$45, %edx
	movq	%rcx, -9184(%rbp)
	movq	%rax, -9200(%rbp)
	movq	%rbx, -9360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$48, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$8, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-9200(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	movq	%rax, -9216(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$49, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-9184(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18UintPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-9200(%rbp), %xmm7
	leaq	-224(%rbp), %rdx
	movq	-9568(%rbp), %xmm4
	movaps	%xmm0, -8880(%rbp)
	movq	-9360(%rbp), %xmm5
	movq	-9632(%rbp), %xmm3
	movq	$0, -8864(%rbp)
	movq	-9344(%rbp), %xmm2
	movhps	-9216(%rbp), %xmm7
	movhps	-9616(%rbp), %xmm4
	movhps	-9184(%rbp), %xmm3
	movhps	-9536(%rbp), %xmm5
	movaps	%xmm7, -9216(%rbp)
	movhps	-9328(%rbp), %xmm2
	movaps	%xmm3, -9184(%rbp)
	movq	%rdx, -9328(%rbp)
	movaps	%xmm4, -9568(%rbp)
	movaps	%xmm5, -9360(%rbp)
	movaps	%xmm2, -9200(%rbp)
	movaps	%xmm2, -304(%rbp)
	movaps	%xmm5, -288(%rbp)
	movaps	%xmm4, -272(%rbp)
	movaps	%xmm3, -256(%rbp)
	movaps	%xmm7, -240(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-6256(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -9344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	-9328(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L1500
	call	_ZdlPv@PLT
	movq	-9328(%rbp), %rdx
.L1500:
	movdqa	-9200(%rbp), %xmm5
	movdqa	-9360(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movdqa	-9568(%rbp), %xmm4
	movdqa	-9184(%rbp), %xmm7
	movq	%r14, %rdi
	movaps	%xmm0, -8880(%rbp)
	movdqa	-9216(%rbp), %xmm3
	movaps	%xmm5, -304(%rbp)
	movaps	%xmm2, -288(%rbp)
	movaps	%xmm4, -272(%rbp)
	movaps	%xmm7, -256(%rbp)
	movaps	%xmm3, -240(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-6064(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -9328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1501
	call	_ZdlPv@PLT
.L1501:
	movq	-9384(%rbp), %rcx
	movq	-9376(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1498
	.p2align 4,,10
	.p2align 3
.L1966:
	movq	-9648(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-6640(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-304(%rbp), %rsi
	movabsq	$434041041323427591, %rax
	leaq	-295(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, -304(%rbp)
	movaps	%xmm0, -8880(%rbp)
	movb	$8, -296(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1496
	call	_ZdlPv@PLT
.L1496:
	movq	-9168(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	$0, -8864(%rbp)
	movaps	%xmm0, -8880(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1497
	call	_ZdlPv@PLT
.L1497:
	movq	-9640(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1495
	.p2align 4,,10
	.p2align 3
.L1965:
	movq	-9344(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-304(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-291(%rbp), %rdx
	movabsq	$434041041323427591, %rax
	movaps	%xmm0, -8880(%rbp)
	movq	%rax, -304(%rbp)
	movl	$117769479, -296(%rbp)
	movb	$5, -292(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9280(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1491
	call	_ZdlPv@PLT
.L1491:
	movq	(%rbx), %rax
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	8(%rax), %rbx
	movq	56(%rax), %rdi
	movq	(%rax), %rcx
	movq	96(%rax), %r8
	movq	%rsi, -9320(%rbp)
	movq	%rdx, -9328(%rbp)
	movq	32(%rax), %rsi
	movq	48(%rax), %rdx
	movq	%rbx, -9200(%rbp)
	movq	16(%rax), %rbx
	movq	%rsi, -9344(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -9536(%rbp)
	movl	$46, %edx
	movq	%rdi, -9568(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -9184(%rbp)
	movq	%r8, -9616(%rbp)
	movq	%rbx, -9216(%rbp)
	movq	88(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-9616(%rbp), %r8
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6ObjectEvE5valueE(%rip), %esi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	movq	-9184(%rbp), %rsi
	call	_ZN2v88internal21Cast10FixedArray_1433EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	-9536(%rbp), %xmm3
	movq	-9344(%rbp), %xmm4
	movq	%rax, -232(%rbp)
	leaq	-8912(%rbp), %rax
	leaq	-224(%rbp), %rdx
	movq	-9216(%rbp), %xmm5
	movhps	-9568(%rbp), %xmm3
	movq	%rax, %rdi
	movq	-9184(%rbp), %xmm6
	movhps	-9328(%rbp), %xmm4
	movq	%rax, -9184(%rbp)
	movhps	-9320(%rbp), %xmm5
	movhps	-9200(%rbp), %xmm6
	movaps	%xmm3, -9536(%rbp)
	movaps	%xmm4, -9344(%rbp)
	movaps	%xmm5, -9216(%rbp)
	movaps	%xmm6, -9200(%rbp)
	movaps	%xmm6, -304(%rbp)
	movaps	%xmm5, -288(%rbp)
	movaps	%xmm4, -272(%rbp)
	movaps	%xmm3, -256(%rbp)
	movaps	%xmm0, -8912(%rbp)
	movq	%rbx, -240(%rbp)
	movq	$0, -8896(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-6448(%rbp), %rax
	movq	-9184(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -9320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8912(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1492
	call	_ZdlPv@PLT
.L1492:
	movq	-9360(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -8872(%rbp)
	jne	.L2000
.L1493:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L1490
	.p2align 4,,10
	.p2align 3
.L1964:
	movq	-9320(%rbp), %rsi
	movq	%r15, %rdi
	movl	$1287, %ebx
	leaq	-304(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-290(%rbp), %rdx
	movw	%bx, -292(%rbp)
	movabsq	$434041041323427591, %rax
	movq	%rax, -304(%rbp)
	movaps	%xmm0, -8880(%rbp)
	movl	$84215047, -296(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9312(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1488
	call	_ZdlPv@PLT
.L1488:
	movq	(%rbx), %rax
	leaq	-200(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqu	96(%rax), %xmm6
	movdqu	80(%rax), %xmm0
	movdqu	(%rax), %xmm5
	movdqu	16(%rax), %xmm4
	movdqu	32(%rax), %xmm3
	movdqu	48(%rax), %xmm2
	punpcklqdq	%xmm6, %xmm0
	movdqu	64(%rax), %xmm1
	movq	104(%rax), %rax
	movaps	%xmm0, -224(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -304(%rbp)
	movq	%rax, -208(%rbp)
	movaps	%xmm4, -288(%rbp)
	movaps	%xmm3, -272(%rbp)
	movaps	%xmm2, -256(%rbp)
	movaps	%xmm1, -240(%rbp)
	movaps	%xmm0, -8880(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-6832(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -9280(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1489
	call	_ZdlPv@PLT
.L1489:
	movq	-9344(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1487
	.p2align 4,,10
	.p2align 3
.L1969:
	movq	-9384(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -8864(%rbp)
	movaps	%xmm0, -8880(%rbp)
	call	_Znwm@PLT
	movl	$1543, %r9d
	movq	-9328(%rbp), %rdi
	movq	%r14, %rsi
	movabsq	$434041041323427591, %rcx
	movw	%r9w, 8(%rax)
	leaq	10(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%rax, -8880(%rbp)
	movq	%rdx, -8864(%rbp)
	movq	%rdx, -8872(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1506
	call	_ZdlPv@PLT
.L1506:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rsi
	movq	56(%rax), %rdx
	movq	8(%rax), %rbx
	movq	(%rax), %rcx
	movq	%rsi, -9568(%rbp)
	movq	32(%rax), %rsi
	movq	%rdx, -9824(%rbp)
	movq	64(%rax), %rdx
	movq	%rsi, -9184(%rbp)
	movq	40(%rax), %rsi
	movq	%rbx, -9376(%rbp)
	movq	16(%rax), %rbx
	movq	%rsi, -9616(%rbp)
	movq	48(%rax), %rsi
	movq	72(%rax), %rax
	movq	%rdx, -9840(%rbp)
	movl	$52, %edx
	movq	%rsi, -9632(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -9360(%rbp)
	movq	%rbx, -9536(%rbp)
	movq	%rax, -9856(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$8, %esi
	movq	%r15, %rdi
	movq	%rax, -9216(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-9184(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$50, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -9200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal40Convert8ATintptr17ATconstexpr_int31_1415EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$51, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$37, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	-9200(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -9384(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-9384(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-304(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -9384(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-9360(%rbp), %xmm5
	movq	-9216(%rbp), %rax
	leaq	-152(%rbp), %rdx
	movaps	%xmm0, -8880(%rbp)
	movq	-9184(%rbp), %xmm2
	movq	%rbx, -160(%rbp)
	movhps	-9376(%rbp), %xmm5
	movq	%rax, -216(%rbp)
	movq	-9840(%rbp), %xmm6
	movq	-9632(%rbp), %xmm7
	movdqa	%xmm2, %xmm3
	movq	-9536(%rbp), %xmm4
	movaps	%xmm5, -9376(%rbp)
	movq	-9200(%rbp), %xmm1
	movaps	%xmm5, -304(%rbp)
	movq	%rbx, %xmm5
	movhps	-9856(%rbp), %xmm6
	movhps	-9568(%rbp), %xmm4
	movhps	-9824(%rbp), %xmm7
	movhps	-9616(%rbp), %xmm3
	movq	%rax, -184(%rbp)
	punpcklqdq	%xmm5, %xmm1
	movaps	%xmm6, -9840(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm1, -9568(%rbp)
	movaps	%xmm7, -9632(%rbp)
	movaps	%xmm3, -9616(%rbp)
	movaps	%xmm4, -9536(%rbp)
	movaps	%xmm4, -288(%rbp)
	movaps	%xmm3, -272(%rbp)
	movaps	%xmm7, -256(%rbp)
	movaps	%xmm6, -240(%rbp)
	movq	%xmm2, -224(%rbp)
	movq	%xmm2, -192(%rbp)
	movq	$0, -8864(%rbp)
	movq	%rdx, -9200(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-5872(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -9360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	-9200(%rbp), %rdx
	movdqa	-9568(%rbp), %xmm1
	testq	%rdi, %rdi
	je	.L1507
	movq	%rdx, -9568(%rbp)
	movaps	%xmm1, -9200(%rbp)
	call	_ZdlPv@PLT
	movq	-9568(%rbp), %rdx
	movdqa	-9200(%rbp), %xmm1
.L1507:
	movq	-9184(%rbp), %xmm0
	movdqa	-9376(%rbp), %xmm6
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqa	-9536(%rbp), %xmm2
	movdqa	-9616(%rbp), %xmm4
	movaps	%xmm1, -208(%rbp)
	movhps	-9216(%rbp), %xmm0
	movdqa	-9632(%rbp), %xmm7
	movdqa	-9840(%rbp), %xmm3
	movaps	%xmm6, -304(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -288(%rbp)
	movaps	%xmm4, -272(%rbp)
	movaps	%xmm7, -256(%rbp)
	movaps	%xmm3, -240(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -8880(%rbp)
	movq	%rbx, -160(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-5680(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -9376(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1508
	call	_ZdlPv@PLT
.L1508:
	movq	-9472(%rbp), %rcx
	movq	-9424(%rbp), %rdx
	movq	%r15, %rdi
	movq	-9384(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1505
	.p2align 4,,10
	.p2align 3
.L1971:
	movq	-9472(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$19, %edi
	movq	$0, -8864(%rbp)
	movaps	%xmm0, -8880(%rbp)
	call	_Znwm@PLT
	movl	$1285, %edi
	movdqa	.LC7(%rip), %xmm0
	movq	%r14, %rsi
	movw	%di, 16(%rax)
	movq	-9376(%rbp), %rdi
	leaq	19(%rax), %rdx
	movb	$5, 18(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, -8880(%rbp)
	movq	%rdx, -8864(%rbp)
	movq	%rdx, -8872(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1513
	call	_ZdlPv@PLT
.L1513:
	movq	(%rbx), %rax
	movq	40(%rax), %rdx
	movq	8(%rax), %rbx
	movq	24(%rax), %rsi
	movq	88(%rax), %rdi
	movq	(%rax), %rcx
	movq	48(%rax), %r12
	movq	%rdx, -9536(%rbp)
	movq	56(%rax), %rdx
	movq	%rbx, -9200(%rbp)
	movq	16(%rax), %rbx
	movq	%rsi, -9424(%rbp)
	movq	%rdx, -9568(%rbp)
	movq	72(%rax), %rdx
	movq	32(%rax), %rsi
	movq	%rdi, -9824(%rbp)
	movq	%rdx, -9616(%rbp)
	movq	96(%rax), %rdi
	movq	80(%rax), %rdx
	movq	%rbx, -9216(%rbp)
	movq	64(%rax), %rbx
	movq	104(%rax), %rax
	movq	%rsi, -9472(%rbp)
	leaq	.LC5(%rip), %rsi
	movq	%rdx, -9632(%rbp)
	movl	$41, %edx
	movq	%rdi, -9840(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -9184(%rbp)
	movq	%rax, -9856(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-304(%rbp), %rsi
	leaq	-192(%rbp), %rdx
	movq	%r14, %rdi
	movq	-9184(%rbp), %xmm0
	movq	$0, -8864(%rbp)
	movhps	-9200(%rbp), %xmm0
	movaps	%xmm0, -304(%rbp)
	movq	-9216(%rbp), %xmm0
	movhps	-9424(%rbp), %xmm0
	movaps	%xmm0, -288(%rbp)
	movq	-9472(%rbp), %xmm0
	movhps	-9536(%rbp), %xmm0
	movaps	%xmm0, -272(%rbp)
	movq	%r12, %xmm0
	movhps	-9568(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	%rbx, %xmm0
	movhps	-9616(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-9632(%rbp), %xmm0
	movhps	-9824(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-9840(%rbp), %xmm0
	movhps	-9856(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8880(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-5296(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -9536(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1514
	call	_ZdlPv@PLT
.L1514:
	movq	-9504(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -5424(%rbp)
	je	.L1515
.L1972:
	movq	-9488(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-304(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC7(%rip), %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-283(%rbp), %rdx
	movl	$117769477, -288(%rbp)
	movaps	%xmm0, -304(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8880(%rbp)
	movb	$5, -284(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9384(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1516
	call	_ZdlPv@PLT
.L1516:
	movq	(%rbx), %rax
	movq	56(%rax), %rdi
	movq	8(%rax), %rbx
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	88(%rax), %r10
	movq	%rdi, -9616(%rbp)
	movq	72(%rax), %rdi
	movq	104(%rax), %r11
	movq	%rbx, -9200(%rbp)
	movq	16(%rax), %rbx
	movq	(%rax), %rcx
	movq	%rsi, -9424(%rbp)
	movq	%rdx, -9488(%rbp)
	movq	32(%rax), %rsi
	movq	48(%rax), %rdx
	movq	%rdi, -9632(%rbp)
	movq	80(%rax), %rdi
	movq	%rbx, -9216(%rbp)
	movq	%r10, -9840(%rbp)
	movq	64(%rax), %rbx
	movq	96(%rax), %r10
	movq	%r11, -9872(%rbp)
	movq	152(%rax), %r11
	movq	160(%rax), %rax
	movq	%rsi, -9472(%rbp)
	leaq	.LC5(%rip), %rsi
	movq	%rdx, -9568(%rbp)
	movl	$51, %edx
	movq	%rdi, -9824(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -9184(%rbp)
	movq	%r10, -9856(%rbp)
	movq	%r11, -9888(%rbp)
	movq	%rax, -9904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-176(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-9184(%rbp), %xmm0
	movq	$0, -8864(%rbp)
	movhps	-9200(%rbp), %xmm0
	movaps	%xmm0, -304(%rbp)
	movq	-9216(%rbp), %xmm0
	movhps	-9424(%rbp), %xmm0
	movaps	%xmm0, -288(%rbp)
	movq	-9472(%rbp), %xmm0
	movhps	-9488(%rbp), %xmm0
	movaps	%xmm0, -272(%rbp)
	movq	-9568(%rbp), %xmm0
	movhps	-9616(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	%rbx, %xmm0
	movhps	-9632(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-9824(%rbp), %xmm0
	movhps	-9840(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-9856(%rbp), %xmm0
	movhps	-9872(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-9888(%rbp), %xmm0
	movhps	-9904(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8880(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-5104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1517
	call	_ZdlPv@PLT
.L1517:
	movq	-9248(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -5232(%rbp)
	je	.L1518
.L1973:
	movq	-9504(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1285, %esi
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movw	%si, -292(%rbp)
	leaq	-290(%rbp), %rdx
	movabsq	$434041041323427591, %rax
	leaq	-304(%rbp), %rsi
	movq	%rax, -304(%rbp)
	movaps	%xmm0, -8880(%rbp)
	movl	$84346375, -296(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9536(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1519
	call	_ZdlPv@PLT
.L1519:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L1518
	.p2align 4,,10
	.p2align 3
.L1970:
	movq	-9424(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$19, %edi
	movq	$0, -8864(%rbp)
	movaps	%xmm0, -8880(%rbp)
	call	_Znwm@PLT
	movl	$1285, %r8d
	movdqa	.LC7(%rip), %xmm0
	movq	%r14, %rsi
	movw	%r8w, 16(%rax)
	movq	-9360(%rbp), %rdi
	leaq	19(%rax), %rdx
	movb	$5, 18(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, -8880(%rbp)
	movq	%rdx, -8864(%rbp)
	movq	%rdx, -8872(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1510
	call	_ZdlPv@PLT
.L1510:
	movq	(%rbx), %rax
	movq	72(%rax), %r11
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	%r11, -9840(%rbp)
	movq	88(%rax), %r11
	movq	80(%rax), %r10
	movq	%rcx, -9200(%rbp)
	movq	104(%rax), %r9
	movq	120(%rax), %rcx
	movq	%rbx, -9216(%rbp)
	movq	%rsi, -9424(%rbp)
	movq	16(%rax), %rbx
	movq	32(%rax), %rsi
	movq	%rdx, -9568(%rbp)
	movq	%rdi, -9632(%rbp)
	movq	48(%rax), %rdx
	movq	64(%rax), %rdi
	movq	%r11, -9872(%rbp)
	movq	96(%rax), %r11
	movq	112(%rax), %r12
	movq	%r10, -9856(%rbp)
	movq	%rcx, -9184(%rbp)
	movq	%r11, -9888(%rbp)
	movq	%rbx, -9384(%rbp)
	movq	%rsi, -9536(%rbp)
	leaq	.LC5(%rip), %rsi
	movq	%rdx, -9616(%rbp)
	movl	$39, %edx
	movq	%rdi, -9824(%rbp)
	movq	%r15, %rdi
	movq	%r9, -9904(%rbp)
	movq	128(%rax), %r9
	movq	136(%rax), %r8
	movq	144(%rax), %rbx
	movq	%r9, -9920(%rbp)
	movq	%r8, -9936(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -9808(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-9808(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrMulENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -9808(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-9808(%rbp), %rdx
	movq	-9184(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -9808(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$38, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-9808(%rbp), %rcx
	call	_ZN2v88internal45UnsafeNewReference20UT5ATSmi10HeapObject_1411EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	-8880(%rbp), %rax
	leaq	-304(%rbp), %rsi
	movq	-9200(%rbp), %xmm0
	leaq	-136(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rbx, -160(%rbp)
	movhps	-9216(%rbp), %xmm0
	movq	%rax, -152(%rbp)
	movq	-8872(%rbp), %rax
	movaps	%xmm0, -304(%rbp)
	movq	-9384(%rbp), %xmm0
	movq	%rax, -144(%rbp)
	movhps	-9424(%rbp), %xmm0
	movaps	%xmm0, -288(%rbp)
	movq	-9536(%rbp), %xmm0
	movhps	-9568(%rbp), %xmm0
	movaps	%xmm0, -272(%rbp)
	movq	-9616(%rbp), %xmm0
	movhps	-9632(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	-9824(%rbp), %xmm0
	movhps	-9840(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-9856(%rbp), %xmm0
	movhps	-9872(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-9888(%rbp), %xmm0
	movhps	-9904(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	%r12, %xmm0
	movhps	-9184(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-9920(%rbp), %xmm0
	movhps	-9936(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8880(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-5488(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -9384(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1511
	call	_ZdlPv@PLT
.L1511:
	movq	-9488(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1509
	.p2align 4,,10
	.p2align 3
.L1984:
	movq	-9240(%rbp), %rsi
	movq	%r15, %rdi
	movl	$2054, %ebx
	leaq	-3184(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$18, %edi
	movq	$0, -8864(%rbp)
	movaps	%xmm0, -8880(%rbp)
	call	_Znwm@PLT
	movdqa	.LC4(%rip), %xmm0
	movq	%r12, %rdi
	movq	%r14, %rsi
	movw	%bx, 16(%rax)
	leaq	18(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -8880(%rbp)
	movq	%rdx, -8864(%rbp)
	movq	%rdx, -8872(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1549
	call	_ZdlPv@PLT
.L1549:
	movq	(%rbx), %rax
	movq	72(%rax), %r11
	movq	8(%rax), %rbx
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	120(%rax), %r9
	movq	%r11, -9840(%rbp)
	movq	88(%rax), %r11
	movq	96(%rax), %r10
	movq	%rbx, -9184(%rbp)
	movq	%rsi, -9440(%rbp)
	movq	16(%rax), %rbx
	movq	%r11, -9856(%rbp)
	movq	104(%rax), %r11
	movq	32(%rax), %rsi
	movq	%rdx, -9552(%rbp)
	movq	48(%rax), %rdx
	movq	(%rax), %rcx
	movq	%rdi, -9632(%rbp)
	movq	64(%rax), %rdi
	movq	%r11, -9888(%rbp)
	movq	112(%rax), %r11
	movq	80(%rax), %r12
	movq	%r9, -9920(%rbp)
	movq	128(%rax), %r9
	movq	%r10, -9872(%rbp)
	movq	%r11, -9904(%rbp)
	movq	%rbx, -9200(%rbp)
	movq	%rsi, -9456(%rbp)
	movq	%rcx, %rsi
	movq	%rdx, -9600(%rbp)
	movq	%rdi, -9824(%rbp)
	movq	%r13, %rdi
	movq	%r9, -9936(%rbp)
	movq	136(%rax), %rax
	movq	%rcx, -9952(%rbp)
	movq	%rax, %rdx
	movq	%rax, -9808(%rbp)
	call	_ZN2v88internal21UnsafeCast5ATSmi_1410EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler18LoadContextElementENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_3SmiEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-9952(%rbp), %rcx
	leaq	-304(%rbp), %rsi
	leaq	-152(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rbx, -160(%rbp)
	movq	$0, -8864(%rbp)
	movq	%rcx, %xmm0
	movhps	-9184(%rbp), %xmm0
	movaps	%xmm0, -304(%rbp)
	movq	-9200(%rbp), %xmm0
	movhps	-9440(%rbp), %xmm0
	movaps	%xmm0, -288(%rbp)
	movq	-9456(%rbp), %xmm0
	movhps	-9552(%rbp), %xmm0
	movaps	%xmm0, -272(%rbp)
	movq	-9600(%rbp), %xmm0
	movhps	-9632(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	-9824(%rbp), %xmm0
	movhps	-9840(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	%r12, %xmm0
	movhps	-9856(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-9872(%rbp), %xmm0
	movhps	-9888(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-9904(%rbp), %xmm0
	movhps	-9920(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-9936(%rbp), %xmm0
	movhps	-9808(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8880(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1648(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -9456(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1550
	call	_ZdlPv@PLT
.L1550:
	movq	-9728(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1548
	.p2align 4,,10
	.p2align 3
.L1983:
	movq	-9552(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-304(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC4(%rip), %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movabsq	$362264917950400262, %rax
	leaq	-280(%rbp), %rdx
	movq	$0, -8864(%rbp)
	movaps	%xmm0, -304(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -288(%rbp)
	movaps	%xmm0, -8880(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9512(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1545
	call	_ZdlPv@PLT
.L1545:
	movq	(%rbx), %rax
	movq	8(%rax), %rcx
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	72(%rax), %r11
	movq	88(%rax), %r10
	movq	%rcx, -9440(%rbp)
	movq	104(%rax), %r9
	movq	16(%rax), %rcx
	movq	%rsi, -9520(%rbp)
	movq	(%rax), %rbx
	movq	32(%rax), %rsi
	movq	%rdx, -9600(%rbp)
	movq	120(%rax), %r8
	movq	48(%rax), %rdx
	movq	%rdi, -9824(%rbp)
	movq	%r11, -9856(%rbp)
	movq	64(%rax), %rdi
	movq	80(%rax), %r11
	movq	%r10, -9888(%rbp)
	movq	%r9, -9920(%rbp)
	movq	96(%rax), %r10
	movq	112(%rax), %r9
	movq	%r11, -9872(%rbp)
	movq	%r10, -9904(%rbp)
	movq	%rcx, -9456(%rbp)
	movq	%rbx, -9200(%rbp)
	movq	%rsi, -9552(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -9632(%rbp)
	movl	$65, %edx
	movq	%rdi, -9840(%rbp)
	movq	%r15, %rdi
	movq	%r9, -9936(%rbp)
	movq	%r8, -9808(%rbp)
	movq	128(%rax), %r8
	movq	184(%rax), %r9
	movq	176(%rax), %rbx
	movq	%r8, -9952(%rbp)
	movq	%r9, -9184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-9184(%rbp), %r9
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6ObjectEvE5valueE(%rip), %esi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -9184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$64, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$67, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal10TheHole_62EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-9184(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-160(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-9456(%rbp), %xmm0
	movq	-9936(%rbp), %xmm7
	movq	-9904(%rbp), %xmm3
	movq	-9872(%rbp), %xmm4
	movq	-9840(%rbp), %xmm5
	movhps	-9520(%rbp), %xmm0
	movq	-9632(%rbp), %xmm1
	movhps	-9808(%rbp), %xmm7
	movq	-9552(%rbp), %xmm2
	movhps	-9920(%rbp), %xmm3
	movq	-9200(%rbp), %xmm12
	movhps	-9888(%rbp), %xmm4
	movq	-9952(%rbp), %xmm6
	movhps	-9856(%rbp), %xmm5
	movhps	-9824(%rbp), %xmm1
	movaps	%xmm0, -9456(%rbp)
	movhps	-9600(%rbp), %xmm2
	movhps	-9440(%rbp), %xmm12
	movaps	%xmm0, -288(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-9184(%rbp), %xmm6
	movaps	%xmm7, -9936(%rbp)
	movaps	%xmm6, -9184(%rbp)
	movaps	%xmm3, -9904(%rbp)
	movaps	%xmm4, -9872(%rbp)
	movaps	%xmm5, -9840(%rbp)
	movaps	%xmm1, -9632(%rbp)
	movaps	%xmm2, -9552(%rbp)
	movaps	%xmm12, -9200(%rbp)
	movaps	%xmm12, -304(%rbp)
	movaps	%xmm2, -272(%rbp)
	movaps	%xmm1, -256(%rbp)
	movaps	%xmm5, -240(%rbp)
	movaps	%xmm4, -224(%rbp)
	movq	%rdx, -9440(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm0, -8880(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3184(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	-9440(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L1546
	call	_ZdlPv@PLT
	movq	-9440(%rbp), %rdx
.L1546:
	movdqa	-9200(%rbp), %xmm1
	movdqa	-9456(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movdqa	-9552(%rbp), %xmm6
	movdqa	-9632(%rbp), %xmm2
	movq	%r14, %rdi
	movaps	%xmm0, -8880(%rbp)
	movdqa	-9840(%rbp), %xmm4
	movdqa	-9872(%rbp), %xmm7
	movaps	%xmm1, -304(%rbp)
	movdqa	-9936(%rbp), %xmm3
	movdqa	-9904(%rbp), %xmm1
	movaps	%xmm5, -288(%rbp)
	movdqa	-9184(%rbp), %xmm5
	movaps	%xmm6, -272(%rbp)
	movaps	%xmm2, -256(%rbp)
	movaps	%xmm4, -240(%rbp)
	movaps	%xmm7, -224(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2992(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -9520(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1547
	call	_ZdlPv@PLT
.L1547:
	movq	-9576(%rbp), %rcx
	movq	-9240(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1544
	.p2align 4,,10
	.p2align 3
.L1982:
	movq	-9232(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-304(%rbp), %r12
	leaq	-3568(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC4(%rip), %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-279(%rbp), %rdx
	movabsq	$505817156072834822, %rax
	movb	$5, -280(%rbp)
	movaps	%xmm0, -304(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -288(%rbp)
	movaps	%xmm0, -8880(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1542
	call	_ZdlPv@PLT
.L1542:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	16(%rax), %rsi
	movq	32(%rax), %r11
	movq	40(%rax), %r10
	movq	48(%rax), %r9
	movq	%rcx, -9184(%rbp)
	movq	80(%rax), %rcx
	movq	56(%rax), %r8
	movq	%rbx, -9200(%rbp)
	movq	64(%rax), %rdi
	movq	24(%rax), %rbx
	movq	%rsi, -9440(%rbp)
	movq	88(%rax), %rdx
	movq	72(%rax), %rsi
	movq	%rcx, -9456(%rbp)
	movq	96(%rax), %rcx
	movq	%rcx, -9512(%rbp)
	movq	104(%rax), %rcx
	movq	%rcx, -9520(%rbp)
	movq	112(%rax), %rcx
	movq	%rcx, -9600(%rbp)
	movq	120(%rax), %rcx
	movq	%rcx, -9632(%rbp)
	movq	128(%rax), %rcx
	movq	%rcx, -9824(%rbp)
	movq	136(%rax), %rcx
	movq	%rcx, -9840(%rbp)
	movq	144(%rax), %rcx
	movq	%rcx, -9856(%rbp)
	movq	152(%rax), %rcx
	movq	%rcx, -9872(%rbp)
	movq	160(%rax), %rcx
	movq	%rcx, -9888(%rbp)
	movq	168(%rax), %rcx
	movq	%rcx, -9904(%rbp)
	movq	184(%rax), %rcx
	movq	192(%rax), %rax
	movq	%rbx, -280(%rbp)
	movq	%rcx, -9920(%rbp)
	movq	-9184(%rbp), %rcx
	movq	-9456(%rbp), %rbx
	movq	%r11, -272(%rbp)
	movq	%rcx, -304(%rbp)
	movq	-9200(%rbp), %rcx
	movq	%r10, -264(%rbp)
	movq	%rcx, -296(%rbp)
	movq	-9440(%rbp), %rcx
	movq	%r9, -256(%rbp)
	movq	%rcx, -288(%rbp)
	movq	-9512(%rbp), %rcx
	movq	%r8, -248(%rbp)
	movq	%rcx, -208(%rbp)
	movq	-9520(%rbp), %rcx
	movq	%rdi, -240(%rbp)
	movq	%r14, %rdi
	movq	%rcx, -200(%rbp)
	movq	-9600(%rbp), %rcx
	movq	%rsi, -232(%rbp)
	movq	%r12, %rsi
	movq	%rcx, -192(%rbp)
	movq	-9632(%rbp), %rcx
	movq	%rdx, -216(%rbp)
	leaq	-112(%rbp), %rdx
	movq	%rcx, -184(%rbp)
	movq	-9824(%rbp), %rcx
	movq	%rbx, -224(%rbp)
	movq	%rcx, -176(%rbp)
	movq	-9840(%rbp), %rcx
	movq	%rax, -120(%rbp)
	movq	%rcx, -168(%rbp)
	movq	-9856(%rbp), %rcx
	movaps	%xmm0, -8880(%rbp)
	movq	%rcx, -160(%rbp)
	movq	-9872(%rbp), %rcx
	movq	$0, -8864(%rbp)
	movq	%rcx, -152(%rbp)
	movq	-9888(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	movq	-9904(%rbp), %rcx
	movq	%rcx, -136(%rbp)
	movq	-9920(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3376(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -9512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1543
	call	_ZdlPv@PLT
.L1543:
	movq	-9552(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1541
	.p2align 4,,10
	.p2align 3
.L1981:
	movq	-9520(%rbp), %rsi
	movq	%r15, %rdi
	movl	$1542, %r12d
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC4(%rip), %xmm0
	leaq	-304(%rbp), %rsi
	leaq	-281(%rbp), %rdx
	movq	%r14, %rdi
	movw	%r12w, -284(%rbp)
	movaps	%xmm0, -304(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8880(%rbp)
	movl	$84215558, -288(%rbp)
	movb	$5, -282(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9504(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1540
	call	_ZdlPv@PLT
.L1540:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L1539
	.p2align 4,,10
	.p2align 3
.L1980:
	movq	-9512(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-304(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC4(%rip), %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movabsq	$505817156072834822, %rax
	leaq	-274(%rbp), %rdx
	movl	$84215045, -280(%rbp)
	movq	%rax, -288(%rbp)
	movl	$1287, %eax
	movaps	%xmm0, -304(%rbp)
	pxor	%xmm0, %xmm0
	movw	%ax, -276(%rbp)
	movaps	%xmm0, -8880(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9488(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1537
	call	_ZdlPv@PLT
.L1537:
	movq	(%rbx), %rax
	movq	72(%rax), %r10
	movq	8(%rax), %rbx
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	104(%rax), %r9
	movq	%r10, -9856(%rbp)
	movq	88(%rax), %r10
	movq	(%rax), %rcx
	movq	%rbx, -9200(%rbp)
	movq	80(%rax), %r11
	movq	16(%rax), %rbx
	movq	%rsi, -9456(%rbp)
	movq	120(%rax), %r8
	movq	32(%rax), %rsi
	movq	%rdx, -9600(%rbp)
	movq	%rdi, -9824(%rbp)
	movq	48(%rax), %rdx
	movq	64(%rax), %rdi
	movq	%r10, -9888(%rbp)
	movq	%r9, -9920(%rbp)
	movq	96(%rax), %r10
	movq	112(%rax), %r9
	movq	%r11, -9872(%rbp)
	movq	%r10, -9904(%rbp)
	movq	%r9, -9936(%rbp)
	movq	%rcx, -9184(%rbp)
	movq	%rbx, -9440(%rbp)
	movq	%rsi, -9512(%rbp)
	movq	%rdx, -9632(%rbp)
	movq	%rdi, -9840(%rbp)
	movq	%r15, %rdi
	movq	%r8, -9808(%rbp)
	movq	168(%rax), %rsi
	movq	136(%rax), %rcx
	movq	152(%rax), %rbx
	movq	224(%rax), %rdx
	movq	%rsi, -10008(%rbp)
	movq	176(%rax), %rsi
	movq	128(%rax), %r8
	movq	%rcx, -9960(%rbp)
	movq	%rbx, -9968(%rbp)
	movq	144(%rax), %rcx
	movq	160(%rax), %rbx
	movq	%rsi, -10016(%rbp)
	leaq	.LC5(%rip), %rsi
	movq	%rdx, -10024(%rbp)
	movl	$56, %edx
	movq	%r8, -9952(%rbp)
	movq	%rcx, -9984(%rbp)
	movq	%rbx, -10000(%rbp)
	movq	232(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-104(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-9184(%rbp), %xmm0
	movhps	-9200(%rbp), %xmm0
	movaps	%xmm0, -304(%rbp)
	movq	-9440(%rbp), %xmm0
	movhps	-9456(%rbp), %xmm0
	movaps	%xmm0, -288(%rbp)
	movq	-9512(%rbp), %xmm0
	movhps	-9600(%rbp), %xmm0
	movaps	%xmm0, -272(%rbp)
	movq	-9632(%rbp), %xmm0
	movhps	-9824(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	-9840(%rbp), %xmm0
	movhps	-9856(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-9872(%rbp), %xmm0
	movhps	-9888(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-9904(%rbp), %xmm0
	movhps	-9920(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-9936(%rbp), %xmm0
	movhps	-9808(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-9952(%rbp), %xmm0
	movhps	-9960(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-9984(%rbp), %xmm0
	movhps	-9968(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-10000(%rbp), %xmm0
	movhps	-10008(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-10016(%rbp), %xmm0
	movq	%rbx, -112(%rbp)
	movhps	-10024(%rbp), %xmm0
	movq	$0, -8864(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8880(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3568(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1538
	call	_ZdlPv@PLT
.L1538:
	movq	-9232(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1536
	.p2align 4,,10
	.p2align 3
.L1979:
	movq	-9456(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -9128(%rbp)
	movq	$0, -9120(%rbp)
	movq	$0, -9112(%rbp)
	movq	$0, -9104(%rbp)
	movq	$0, -9096(%rbp)
	movq	$0, -9088(%rbp)
	movq	$0, -9080(%rbp)
	movq	$0, -9072(%rbp)
	movq	$0, -9064(%rbp)
	movq	$0, -9056(%rbp)
	movq	$0, -9048(%rbp)
	movq	$0, -9040(%rbp)
	movq	$0, -9032(%rbp)
	movq	$0, -9024(%rbp)
	movq	$0, -9016(%rbp)
	movq	$0, -9008(%rbp)
	movq	$0, -9000(%rbp)
	movq	$0, -8992(%rbp)
	movq	$0, -8984(%rbp)
	movq	$0, -8976(%rbp)
	movq	$0, -8968(%rbp)
	movq	$0, -8960(%rbp)
	movq	$0, -8952(%rbp)
	movq	$0, -8944(%rbp)
	movq	$0, -8936(%rbp)
	movq	$0, -8928(%rbp)
	movq	$0, -8920(%rbp)
	movq	$0, -8912(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-8912(%rbp), %rax
	movq	-9472(%rbp), %rdi
	pushq	%rax
	leaq	-8920(%rbp), %rax
	leaq	-9112(%rbp), %rcx
	pushq	%rax
	leaq	-8928(%rbp), %rax
	leaq	-9096(%rbp), %r9
	pushq	%rax
	leaq	-8936(%rbp), %rax
	leaq	-9104(%rbp), %r8
	pushq	%rax
	leaq	-8944(%rbp), %rax
	leaq	-9120(%rbp), %rdx
	pushq	%rax
	leaq	-8952(%rbp), %rax
	leaq	-9128(%rbp), %rsi
	pushq	%rax
	leaq	-8960(%rbp), %rax
	pushq	%rax
	leaq	-8968(%rbp), %rax
	pushq	%rax
	leaq	-8976(%rbp), %rax
	pushq	%rax
	leaq	-8984(%rbp), %rax
	pushq	%rax
	leaq	-8992(%rbp), %rax
	pushq	%rax
	leaq	-9000(%rbp), %rax
	pushq	%rax
	leaq	-9008(%rbp), %rax
	pushq	%rax
	leaq	-9016(%rbp), %rax
	pushq	%rax
	leaq	-9024(%rbp), %rax
	pushq	%rax
	leaq	-9032(%rbp), %rax
	pushq	%rax
	leaq	-9040(%rbp), %rax
	pushq	%rax
	leaq	-9048(%rbp), %rax
	pushq	%rax
	leaq	-9056(%rbp), %rax
	pushq	%rax
	leaq	-9064(%rbp), %rax
	pushq	%rax
	leaq	-9072(%rbp), %rax
	pushq	%rax
	leaq	-9080(%rbp), %rax
	pushq	%rax
	leaq	-9088(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_S6_NS0_7IntPtrTESA_S5_S5_SA_NS0_10HeapObjectESA_SA_SA_SA_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EESJ_SJ_SJ_SL_SJ_PNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EESJ_SL_SJ_SJ_SL_PNSD_ISA_EEST_SJ_SJ_ST_PNSD_ISB_EEST_ST_ST_ST_
	addq	$192, %rsp
	movl	$41, %edx
	movq	%r15, %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm0, %xmm0
	leaq	-120(%rbp), %rdx
	movq	%r14, %rdi
	movq	-9128(%rbp), %rax
	leaq	-304(%rbp), %rsi
	movq	%rax, -304(%rbp)
	movq	-9120(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	-9112(%rbp), %rax
	movq	%rax, -288(%rbp)
	movq	-9104(%rbp), %rax
	movq	%rax, -280(%rbp)
	movq	-9096(%rbp), %rax
	movq	%rax, -272(%rbp)
	movq	-9088(%rbp), %rax
	movq	%rax, -264(%rbp)
	movq	-9080(%rbp), %rax
	movq	%rax, -256(%rbp)
	movq	-9072(%rbp), %rax
	movq	%rax, -248(%rbp)
	movq	-9064(%rbp), %rax
	movq	%rax, -240(%rbp)
	movq	-9056(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	-9048(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-9040(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-9032(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-9024(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-9016(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-9008(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-9000(%rbp), %rax
	movaps	%xmm0, -8880(%rbp)
	movq	%rax, -176(%rbp)
	movq	-8992(%rbp), %rax
	movq	$0, -8864(%rbp)
	movq	%rax, -168(%rbp)
	movq	-8984(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-8976(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-8968(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-8960(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-8952(%rbp), %rax
	movq	%rax, -128(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9504(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1535
	call	_ZdlPv@PLT
.L1535:
	movq	-9520(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1534
	.p2align 4,,10
	.p2align 3
.L1978:
	movq	-9224(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -9128(%rbp)
	leaq	-4336(%rbp), %r12
	movq	$0, -9120(%rbp)
	movq	$0, -9112(%rbp)
	movq	$0, -9104(%rbp)
	movq	$0, -9096(%rbp)
	movq	$0, -9088(%rbp)
	movq	$0, -9080(%rbp)
	movq	$0, -9072(%rbp)
	movq	$0, -9064(%rbp)
	movq	$0, -9056(%rbp)
	movq	$0, -9048(%rbp)
	movq	$0, -9040(%rbp)
	movq	$0, -9032(%rbp)
	movq	$0, -9024(%rbp)
	movq	$0, -9016(%rbp)
	movq	$0, -9008(%rbp)
	movq	$0, -9000(%rbp)
	movq	$0, -8992(%rbp)
	movq	$0, -8984(%rbp)
	movq	$0, -8976(%rbp)
	movq	$0, -8968(%rbp)
	movq	$0, -8960(%rbp)
	movq	$0, -8952(%rbp)
	movq	$0, -8944(%rbp)
	movq	$0, -8936(%rbp)
	movq	$0, -8928(%rbp)
	movq	$0, -8920(%rbp)
	movq	$0, -8912(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-8912(%rbp), %rax
	pushq	%rax
	leaq	-8920(%rbp), %rax
	leaq	-9096(%rbp), %r9
	pushq	%rax
	leaq	-8928(%rbp), %rax
	leaq	-9104(%rbp), %r8
	pushq	%rax
	leaq	-8936(%rbp), %rax
	leaq	-9112(%rbp), %rcx
	pushq	%rax
	leaq	-8944(%rbp), %rax
	leaq	-9120(%rbp), %rdx
	pushq	%rax
	leaq	-8952(%rbp), %rax
	leaq	-9128(%rbp), %rsi
	pushq	%rax
	leaq	-8960(%rbp), %rax
	pushq	%rax
	leaq	-8968(%rbp), %rax
	pushq	%rax
	leaq	-8976(%rbp), %rax
	pushq	%rax
	leaq	-8984(%rbp), %rax
	pushq	%rax
	leaq	-8992(%rbp), %rax
	pushq	%rax
	leaq	-9000(%rbp), %rax
	pushq	%rax
	leaq	-9008(%rbp), %rax
	pushq	%rax
	leaq	-9016(%rbp), %rax
	pushq	%rax
	leaq	-9024(%rbp), %rax
	pushq	%rax
	leaq	-9032(%rbp), %rax
	pushq	%rax
	leaq	-9040(%rbp), %rax
	pushq	%rax
	leaq	-9048(%rbp), %rax
	pushq	%rax
	leaq	-9056(%rbp), %rax
	pushq	%rax
	leaq	-9064(%rbp), %rax
	pushq	%rax
	leaq	-9072(%rbp), %rax
	pushq	%rax
	leaq	-9080(%rbp), %rax
	pushq	%rax
	leaq	-9088(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_S6_NS0_7IntPtrTESA_S5_S5_SA_NS0_10HeapObjectESA_SA_SA_SA_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS5_EESJ_PNSD_IS6_EESJ_SJ_SJ_SL_SJ_PNSD_IS7_EEPNSD_IS8_EEPNSD_IS9_EESJ_SL_SJ_SJ_SL_PNSD_ISA_EEST_SJ_SJ_ST_PNSD_ISB_EEST_ST_ST_ST_
	addq	$192, %rsp
	movl	$39, %edx
	movq	%r15, %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8912(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrMulENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8936(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$38, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-8944(%rbp), %rdx
	call	_ZN2v88internal45UnsafeNewReference20UT5ATSmi10HeapObject_1411EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	leaq	-304(%rbp), %rsi
	leaq	-64(%rbp), %rdx
	movq	%r14, %rdi
	movq	-8920(%rbp), %xmm0
	movq	-9064(%rbp), %xmm9
	movq	-9080(%rbp), %xmm10
	movq	-9096(%rbp), %xmm11
	movq	-9112(%rbp), %xmm12
	movhps	-8912(%rbp), %xmm0
	movq	-9128(%rbp), %xmm13
	movhps	-9056(%rbp), %xmm9
	movq	-8936(%rbp), %xmm1
	movq	-8952(%rbp), %xmm2
	movhps	-9072(%rbp), %xmm10
	movhps	-9088(%rbp), %xmm11
	movq	-8968(%rbp), %xmm3
	movq	-8984(%rbp), %xmm4
	movhps	-9104(%rbp), %xmm12
	movhps	-9120(%rbp), %xmm13
	movq	-9000(%rbp), %xmm5
	movhps	-8928(%rbp), %xmm1
	movq	-9016(%rbp), %xmm6
	movhps	-8944(%rbp), %xmm2
	movq	-9032(%rbp), %xmm7
	movhps	-8960(%rbp), %xmm3
	movq	-9048(%rbp), %xmm8
	movhps	-8976(%rbp), %xmm4
	movhps	-8992(%rbp), %xmm5
	movhps	-9008(%rbp), %xmm6
	movaps	%xmm13, -304(%rbp)
	movhps	-9024(%rbp), %xmm7
	movhps	-9040(%rbp), %xmm8
	movaps	%xmm12, -288(%rbp)
	movaps	%xmm11, -272(%rbp)
	movaps	%xmm10, -256(%rbp)
	movaps	%xmm9, -240(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	-8880(%rbp), %xmm0
	movaps	%xmm8, -224(%rbp)
	movhps	-8872(%rbp), %xmm0
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -8880(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9488(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1533
	call	_ZdlPv@PLT
.L1533:
	movq	-9512(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1532
	.p2align 4,,10
	.p2align 3
.L1977:
	movq	-9440(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -9040(%rbp)
	movq	$0, -9032(%rbp)
	movq	$0, -9024(%rbp)
	movq	$0, -9016(%rbp)
	movq	$0, -9008(%rbp)
	movq	$0, -9000(%rbp)
	movq	$0, -8992(%rbp)
	movq	$0, -8984(%rbp)
	movq	$0, -8976(%rbp)
	movq	$0, -8968(%rbp)
	movq	$0, -8960(%rbp)
	movq	$0, -8952(%rbp)
	movq	$0, -8944(%rbp)
	movq	$0, -8936(%rbp)
	movq	$0, -8928(%rbp)
	movq	$0, -8920(%rbp)
	movq	$0, -8912(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8912(%rbp), %rax
	movq	-9424(%rbp), %rdi
	leaq	-9008(%rbp), %r9
	pushq	%rax
	leaq	-8920(%rbp), %rax
	leaq	-9016(%rbp), %r8
	pushq	%rax
	leaq	-8928(%rbp), %rax
	leaq	-9024(%rbp), %rcx
	pushq	%rax
	leaq	-8936(%rbp), %rax
	leaq	-9032(%rbp), %rdx
	pushq	%rax
	leaq	-8944(%rbp), %rax
	leaq	-9040(%rbp), %rsi
	pushq	%rax
	leaq	-8952(%rbp), %rax
	pushq	%rax
	leaq	-8960(%rbp), %rax
	pushq	%rax
	leaq	-8968(%rbp), %rax
	pushq	%rax
	leaq	-8976(%rbp), %rax
	pushq	%rax
	leaq	-8984(%rbp), %rax
	pushq	%rax
	leaq	-8992(%rbp), %rax
	pushq	%rax
	leaq	-9000(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EESH_PNSB_IS6_EESH_SH_SH_SJ_SH_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESH_SJ_SH_SH_
	addq	$96, %rsp
	movl	$65, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$8, %esi
	movq	%r15, %rdi
	movq	%rax, -9200(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	movq	-9008(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$2, %esi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8912(%rbp), %r8
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r8, -9184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-9184(%rbp), %r8
	movq	%r14, %rdi
	movq	%rax, %r12
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$55, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r12, -9888(%rbp)
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$56, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -9184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$37, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-9184(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -9440(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-9440(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-304(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -9440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-80(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-9008(%rbp), %xmm1
	movq	-8912(%rbp), %xmm2
	movq	-8928(%rbp), %xmm6
	movq	-8944(%rbp), %xmm7
	movq	-8960(%rbp), %xmm3
	movdqa	%xmm1, %xmm0
	movq	-8976(%rbp), %xmm4
	punpcklqdq	%xmm1, %xmm2
	movq	-8992(%rbp), %xmm5
	movhps	-8920(%rbp), %xmm6
	movq	-9024(%rbp), %xmm14
	movhps	-8936(%rbp), %xmm7
	movq	-9040(%rbp), %xmm15
	movhps	-8952(%rbp), %xmm3
	movhps	-8968(%rbp), %xmm4
	movhps	-9000(%rbp), %xmm0
	movhps	-8984(%rbp), %xmm5
	movhps	-9016(%rbp), %xmm14
	movaps	%xmm6, -9488(%rbp)
	movhps	-9032(%rbp), %xmm15
	movaps	%xmm7, -9504(%rbp)
	movaps	%xmm3, -9600(%rbp)
	movaps	%xmm4, -9632(%rbp)
	movaps	%xmm5, -9824(%rbp)
	movaps	%xmm14, -9856(%rbp)
	movaps	%xmm15, -9872(%rbp)
	movaps	%xmm15, -304(%rbp)
	movaps	%xmm14, -288(%rbp)
	movaps	%xmm5, -256(%rbp)
	movaps	%xmm4, -240(%rbp)
	movaps	%xmm3, -224(%rbp)
	movaps	%xmm2, -9472(%rbp)
	movaps	%xmm0, -9840(%rbp)
	movaps	%xmm0, -272(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm7, -208(%rbp)
	movq	-9200(%rbp), %rax
	movaps	%xmm2, -176(%rbp)
	movq	-9888(%rbp), %xmm2
	movq	%rax, -160(%rbp)
	movq	-9184(%rbp), %rax
	punpcklqdq	%xmm2, %xmm2
	movq	%rdx, -9904(%rbp)
	movq	%rax, -128(%rbp)
	movq	-9200(%rbp), %rax
	movq	%xmm1, -120(%rbp)
	movq	%rax, -112(%rbp)
	movq	-9184(%rbp), %rax
	movq	%xmm1, -9920(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rax, -88(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm2, -9888(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm0, -8880(%rbp)
	movq	%rbx, -152(%rbp)
	movq	%rbx, -104(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-4336(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	-9904(%rbp), %rdx
	movq	-9920(%rbp), %xmm1
	testq	%rdi, %rdi
	je	.L1530
	movq	%rdx, -9920(%rbp)
	movq	%xmm1, -9904(%rbp)
	call	_ZdlPv@PLT
	movq	-9920(%rbp), %rdx
	movq	-9904(%rbp), %xmm1
.L1530:
	movq	-9200(%rbp), %xmm0
	movq	%rbx, %xmm5
	movdqa	-9840(%rbp), %xmm4
	movq	%r12, %rsi
	movdqa	-9872(%rbp), %xmm6
	movdqa	-9856(%rbp), %xmm2
	movq	%r14, %rdi
	movq	$0, -8864(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movdqa	-9632(%rbp), %xmm5
	movdqa	-9824(%rbp), %xmm3
	movaps	%xmm4, -272(%rbp)
	movdqa	-9488(%rbp), %xmm4
	movaps	%xmm0, -160(%rbp)
	movdqa	-9472(%rbp), %xmm7
	movaps	%xmm5, -240(%rbp)
	movq	-9184(%rbp), %xmm5
	movaps	%xmm0, -112(%rbp)
	movdqa	%xmm5, %xmm0
	movaps	%xmm6, -304(%rbp)
	movdqa	-9600(%rbp), %xmm6
	movaps	%xmm2, -288(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movdqa	-9504(%rbp), %xmm2
	movaps	%xmm3, -256(%rbp)
	movdqa	-9888(%rbp), %xmm3
	movaps	%xmm4, -192(%rbp)
	movdqa	%xmm5, %xmm4
	punpcklqdq	%xmm1, %xmm4
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm2, -208(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm0, -8880(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-4144(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -9472(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1531
	call	_ZdlPv@PLT
.L1531:
	movq	-9456(%rbp), %rcx
	movq	-9224(%rbp), %rdx
	movq	%r15, %rdi
	movq	-9440(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1529
	.p2align 4,,10
	.p2align 3
.L1976:
	movq	-9680(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -9040(%rbp)
	movq	$0, -9032(%rbp)
	movq	$0, -9024(%rbp)
	movq	$0, -9016(%rbp)
	movq	$0, -9008(%rbp)
	movq	$0, -9000(%rbp)
	movq	$0, -8992(%rbp)
	movq	$0, -8984(%rbp)
	movq	$0, -8976(%rbp)
	movq	$0, -8968(%rbp)
	movq	$0, -8960(%rbp)
	movq	$0, -8952(%rbp)
	movq	$0, -8944(%rbp)
	movq	$0, -8936(%rbp)
	movq	$0, -8928(%rbp)
	movq	$0, -8920(%rbp)
	movq	$0, -8912(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8912(%rbp), %rax
	movq	-9216(%rbp), %rdi
	leaq	-9024(%rbp), %rcx
	pushq	%rax
	leaq	-8920(%rbp), %rax
	leaq	-9008(%rbp), %r9
	pushq	%rax
	leaq	-8928(%rbp), %rax
	leaq	-9016(%rbp), %r8
	pushq	%rax
	leaq	-8936(%rbp), %rax
	leaq	-9032(%rbp), %rdx
	pushq	%rax
	leaq	-8944(%rbp), %rax
	leaq	-9040(%rbp), %rsi
	pushq	%rax
	leaq	-8952(%rbp), %rax
	pushq	%rax
	leaq	-8960(%rbp), %rax
	pushq	%rax
	leaq	-8968(%rbp), %rax
	pushq	%rax
	leaq	-8976(%rbp), %rax
	pushq	%rax
	leaq	-8984(%rbp), %rax
	pushq	%rax
	leaq	-8992(%rbp), %rax
	pushq	%rax
	leaq	-9000(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EESH_PNSB_IS6_EESH_SH_SH_SJ_SH_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESH_SJ_SH_SH_
	addq	$96, %rsp
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8912(%rbp), %rbx
	movq	-8920(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -9632(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-9040(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	-9032(%rbp), %rcx
	movq	-8992(%rbp), %r10
	movq	-8984(%rbp), %r11
	movq	%rax, -9184(%rbp)
	movq	-8952(%rbp), %rax
	movq	-8976(%rbp), %r9
	movq	-8968(%rbp), %r8
	movq	%rcx, -9824(%rbp)
	movq	%rax, -9200(%rbp)
	movq	-8944(%rbp), %rax
	movq	-9024(%rbp), %rbx
	movq	-9016(%rbp), %rsi
	movq	%r10, -9904(%rbp)
	movq	%rax, -9472(%rbp)
	movq	-8936(%rbp), %rax
	movq	-9008(%rbp), %rdx
	movq	-9000(%rbp), %rdi
	movq	%r11, -9920(%rbp)
	movq	%rax, -9488(%rbp)
	movq	-8928(%rbp), %rax
	movq	-8960(%rbp), %r12
	movq	%r9, -9936(%rbp)
	movq	%rax, -9504(%rbp)
	movq	-8920(%rbp), %rax
	movq	%r8, -9808(%rbp)
	movq	%rbx, -9840(%rbp)
	movq	%rsi, -9856(%rbp)
	movq	%rdx, -9872(%rbp)
	movq	%rdi, -9888(%rbp)
	movq	%r12, -9952(%rbp)
	movq	%rax, -9600(%rbp)
	movq	-8912(%rbp), %rax
	movq	%rbx, -288(%rbp)
	leaq	-168(%rbp), %rbx
	movq	%rax, -9616(%rbp)
	movq	-9184(%rbp), %rax
	movq	%r12, -224(%rbp)
	leaq	-304(%rbp), %r12
	movq	%rax, -304(%rbp)
	movq	-9200(%rbp), %rax
	movq	%rsi, -280(%rbp)
	movq	%r12, %rsi
	movq	%rax, -216(%rbp)
	movq	-9472(%rbp), %rax
	movq	%rdx, -272(%rbp)
	movq	%rbx, %rdx
	movq	%rax, -208(%rbp)
	movq	-9488(%rbp), %rax
	movq	%rdi, -264(%rbp)
	movq	%r14, %rdi
	movq	%rax, -200(%rbp)
	movq	-9504(%rbp), %rax
	movq	%rcx, -296(%rbp)
	movq	%rax, -192(%rbp)
	movq	-9600(%rbp), %rax
	movq	%r10, -256(%rbp)
	movq	%rax, -184(%rbp)
	movq	-9616(%rbp), %rax
	movq	%r11, -248(%rbp)
	movq	%r9, -240(%rbp)
	movq	%r8, -232(%rbp)
	movq	%rax, -176(%rbp)
	movaps	%xmm0, -8880(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9424(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1527
	call	_ZdlPv@PLT
.L1527:
	movq	-9616(%rbp), %rax
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-9184(%rbp), %xmm0
	movq	$0, -8864(%rbp)
	movq	%rax, -176(%rbp)
	movhps	-9824(%rbp), %xmm0
	movaps	%xmm0, -304(%rbp)
	movq	-9840(%rbp), %xmm0
	movhps	-9856(%rbp), %xmm0
	movaps	%xmm0, -288(%rbp)
	movq	-9872(%rbp), %xmm0
	movhps	-9888(%rbp), %xmm0
	movaps	%xmm0, -272(%rbp)
	movq	-9904(%rbp), %xmm0
	movhps	-9920(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	-9936(%rbp), %xmm0
	movhps	-9808(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-9952(%rbp), %xmm0
	movhps	-9200(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-9472(%rbp), %xmm0
	movhps	-9488(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-9504(%rbp), %xmm0
	movhps	-9600(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8880(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1072(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -9616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1528
	call	_ZdlPv@PLT
.L1528:
	movq	-9696(%rbp), %rcx
	movq	-9440(%rbp), %rdx
	movq	%r15, %rdi
	movq	-9632(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1526
	.p2align 4,,10
	.p2align 3
.L1975:
	movq	-9600(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-304(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1797, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movabsq	$434041041323427591, %rax
	pxor	%xmm0, %xmm0
	leaq	-289(%rbp), %rdx
	movw	%cx, -292(%rbp)
	movq	%rax, -304(%rbp)
	movaps	%xmm0, -8880(%rbp)
	movl	$84346375, -296(%rbp)
	movb	$5, -290(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9568(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1524
	call	_ZdlPv@PLT
.L1524:
	movq	(%rbx), %rax
	movq	8(%rax), %rcx
	movq	32(%rax), %rsi
	movq	40(%rax), %rdx
	movq	64(%rax), %rdi
	movq	72(%rax), %r10
	movq	104(%rax), %r9
	movq	%rcx, -9504(%rbp)
	movq	16(%rax), %rcx
	movq	112(%rax), %r8
	movq	%rsi, -9600(%rbp)
	movq	48(%rax), %rsi
	movq	%rdx, -9616(%rbp)
	movq	56(%rax), %rdx
	movq	%rcx, -9184(%rbp)
	movq	24(%rax), %rcx
	movq	%rsi, -9216(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -9424(%rbp)
	movl	$52, %edx
	movq	(%rax), %rbx
	movq	%rdi, -9632(%rbp)
	movq	%r15, %rdi
	movq	%r10, -9824(%rbp)
	movq	%rcx, -9200(%rbp)
	movq	%r9, -9488(%rbp)
	movq	%r8, -9472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-9472(%rbp), %r8
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-9488(%rbp), %r9
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6ObjectEvE5valueE(%rip), %esi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	%r9, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -9472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$51, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-9472(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal23UnsafeCast7Context_1434EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	movl	$54, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -9888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movl	$3, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler22LoadJSArrayElementsMapENS0_12ElementsKindENS0_8compiler11SloppyTNodeINS0_13NativeContextEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -9488(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$56, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	pushq	$0
	xorl	%r9d, %r9d
	movl	$3, %esi
	pushq	$0
	movq	-9200(%rbp), %rcx
	movq	%r14, %rdi
	movq	-9488(%rbp), %rdx
	movq	%rcx, %r8
	call	_ZN2v88internal17CodeStubAssembler15AllocateJSArrayENS0_12ElementsKindENS0_8compiler5TNodeINS0_3MapEEEPNS3_4NodeENS4_INS0_3SmiEEES8_NS1_13ParameterModeENS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	%r14, %rdi
	movq	%rax, -9472(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$55, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$58, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$59, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -9872(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -9840(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-9840(%rbp), %r8
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-9472(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE(%rip), %esi
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -9840(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-9840(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal27UnsafeCast10FixedArray_1409EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$60, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -9856(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-9424(%rbp), %rdx
	movq	-9216(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler6SmiMinENS0_8compiler5TNodeINS0_3SmiEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -9840(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$63, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movhps	-9504(%rbp), %xmm0
	movq	-9184(%rbp), %rax
	leaq	-168(%rbp), %rdx
	movq	$0, -8864(%rbp)
	movaps	%xmm0, -304(%rbp)
	movq	-9184(%rbp), %xmm0
	movq	%rax, -176(%rbp)
	movhps	-9200(%rbp), %xmm0
	movaps	%xmm0, -288(%rbp)
	movq	-9600(%rbp), %xmm0
	movhps	-9616(%rbp), %xmm0
	movaps	%xmm0, -272(%rbp)
	movq	-9216(%rbp), %xmm0
	movhps	-9424(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	-9632(%rbp), %xmm0
	movhps	-9824(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-9888(%rbp), %xmm0
	movhps	-9488(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-9472(%rbp), %xmm0
	movhps	-9872(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-9856(%rbp), %xmm0
	movhps	-9840(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8880(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-4720(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -9216(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L1525
	call	_ZdlPv@PLT
.L1525:
	movq	-9680(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1523
	.p2align 4,,10
	.p2align 3
.L1974:
	movq	-9248(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-304(%rbp), %r12
	leaq	-5104(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC7(%rip), %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-288(%rbp), %rdx
	movq	$0, -8864(%rbp)
	movaps	%xmm0, -304(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8880(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1521
	call	_ZdlPv@PLT
.L1521:
	movq	(%rbx), %rax
	leaq	-184(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqu	112(%rax), %xmm7
	movdqu	96(%rax), %xmm0
	movdqu	(%rax), %xmm6
	movdqu	16(%rax), %xmm5
	movdqu	32(%rax), %xmm4
	movdqu	48(%rax), %xmm3
	punpcklqdq	%xmm7, %xmm0
	movdqu	64(%rax), %xmm2
	movdqu	80(%rax), %xmm1
	movq	120(%rax), %rax
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm6, -304(%rbp)
	movq	%rax, -192(%rbp)
	movaps	%xmm5, -288(%rbp)
	movaps	%xmm4, -272(%rbp)
	movaps	%xmm3, -256(%rbp)
	movaps	%xmm2, -240(%rbp)
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm0, -8880(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-4912(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -9568(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1522
	call	_ZdlPv@PLT
.L1522:
	movq	-9600(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1520
	.p2align 4,,10
	.p2align 3
.L1985:
	movq	-9576(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$18, %edi
	movq	$0, -8864(%rbp)
	movaps	%xmm0, -8880(%rbp)
	call	_Znwm@PLT
	movl	$2054, %r11d
	movdqa	.LC4(%rip), %xmm0
	movq	%r14, %rsi
	movw	%r11w, 16(%rax)
	movq	-9520(%rbp), %rdi
	leaq	18(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -8880(%rbp)
	movq	%rdx, -8864(%rbp)
	movq	%rdx, -8872(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1552
	call	_ZdlPv@PLT
.L1552:
	movq	(%rbx), %rax
	movq	56(%rax), %rdi
	movq	8(%rax), %rcx
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	88(%rax), %r10
	movq	104(%rax), %r11
	movq	%rdi, -9904(%rbp)
	movq	64(%rax), %rdi
	movq	120(%rax), %r9
	movq	%rcx, -9632(%rbp)
	movq	(%rax), %rbx
	movq	16(%rax), %rcx
	movq	%rsi, -9840(%rbp)
	movq	%rdi, -9184(%rbp)
	movq	72(%rax), %rdi
	movq	32(%rax), %rsi
	movq	%rdx, -9872(%rbp)
	movq	%rdi, -9920(%rbp)
	movq	48(%rax), %rdx
	movq	80(%rax), %rdi
	movq	%r10, -9808(%rbp)
	movq	%r11, -9960(%rbp)
	movq	96(%rax), %r10
	movq	112(%rax), %r11
	movq	%rcx, -9824(%rbp)
	movq	%r10, -9952(%rbp)
	movq	%r11, -9984(%rbp)
	movq	%rbx, -9600(%rbp)
	movq	%rsi, -9856(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -9888(%rbp)
	movl	$68, %edx
	movq	%rdi, -9936(%rbp)
	movq	%r15, %rdi
	movq	%r9, -9968(%rbp)
	movq	128(%rax), %rbx
	movq	136(%rax), %rax
	movq	%rbx, -9200(%rbp)
	movq	%rax, -10000(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$8, %esi
	movq	%r15, %rdi
	movq	%rax, -9552(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-9184(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$55, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -9440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$56, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$37, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	-9440(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -9576(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-9576(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-304(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -9576(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-72(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-9200(%rbp), %xmm15
	movq	-9184(%rbp), %xmm14
	movq	-9984(%rbp), %xmm7
	movq	-9952(%rbp), %xmm3
	movq	-9936(%rbp), %xmm4
	movdqa	%xmm15, %xmm6
	movq	-9888(%rbp), %xmm1
	movdqa	%xmm14, %xmm5
	movq	-9856(%rbp), %xmm2
	movhps	-10000(%rbp), %xmm6
	movq	-9824(%rbp), %xmm0
	movhps	-9968(%rbp), %xmm7
	movq	-9600(%rbp), %xmm13
	movhps	-9904(%rbp), %xmm1
	movhps	-9960(%rbp), %xmm3
	movhps	-9808(%rbp), %xmm4
	movhps	-9920(%rbp), %xmm5
	movhps	-9872(%rbp), %xmm2
	movhps	-9840(%rbp), %xmm0
	movaps	%xmm4, -9936(%rbp)
	movhps	-9632(%rbp), %xmm13
	movaps	%xmm1, -9888(%rbp)
	movaps	%xmm13, -9632(%rbp)
	movaps	%xmm13, -304(%rbp)
	movaps	%xmm1, -256(%rbp)
	movaps	%xmm6, -10000(%rbp)
	movaps	%xmm7, -9984(%rbp)
	movaps	%xmm3, -9952(%rbp)
	movaps	%xmm5, -9920(%rbp)
	movaps	%xmm2, -9856(%rbp)
	movaps	%xmm0, -9824(%rbp)
	movaps	%xmm0, -288(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -272(%rbp)
	movq	%rbx, %xmm2
	movaps	%xmm5, -240(%rbp)
	movaps	%xmm4, -224(%rbp)
	movq	-9552(%rbp), %rax
	movq	-9440(%rbp), %xmm5
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm7, -192(%rbp)
	movdqa	%xmm5, %xmm3
	movdqa	%xmm5, %xmm7
	movaps	%xmm6, -176(%rbp)
	movdqa	%xmm15, %xmm6
	punpcklqdq	%xmm15, %xmm7
	punpcklqdq	%xmm2, %xmm3
	punpcklqdq	%xmm2, %xmm6
	movq	%rdx, -9840(%rbp)
	movq	%rax, -152(%rbp)
	movq	%rax, -104(%rbp)
	movq	%xmm14, -160(%rbp)
	movaps	%xmm6, -9600(%rbp)
	movaps	%xmm7, -9440(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movq	%xmm14, -112(%rbp)
	movaps	%xmm3, -9200(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -8880(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2800(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	-9840(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L1553
	call	_ZdlPv@PLT
	movq	-9840(%rbp), %rdx
.L1553:
	movdqa	-9632(%rbp), %xmm4
	movdqa	-9824(%rbp), %xmm7
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqa	-9856(%rbp), %xmm1
	movq	%rbx, -80(%rbp)
	movdqa	-9888(%rbp), %xmm3
	movdqa	-9920(%rbp), %xmm5
	movaps	%xmm4, -304(%rbp)
	movq	-9184(%rbp), %xmm0
	movdqa	-9936(%rbp), %xmm6
	movaps	%xmm7, -288(%rbp)
	movdqa	-9952(%rbp), %xmm2
	movdqa	-9984(%rbp), %xmm4
	movaps	%xmm1, -272(%rbp)
	movhps	-9552(%rbp), %xmm0
	movdqa	-10000(%rbp), %xmm7
	movdqa	-9440(%rbp), %xmm1
	movaps	%xmm3, -256(%rbp)
	movaps	%xmm5, -240(%rbp)
	movdqa	-9600(%rbp), %xmm3
	movdqa	-9200(%rbp), %xmm5
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm2, -208(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm0, -8880(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2608(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -9632(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1554
	call	_ZdlPv@PLT
.L1554:
	movq	-9776(%rbp), %rcx
	movq	-9272(%rbp), %rdx
	movq	%r15, %rdi
	movq	-9576(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1551
	.p2align 4,,10
	.p2align 3
.L1987:
	movq	-9776(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -9136(%rbp)
	movq	$0, -9128(%rbp)
	movq	$0, -9120(%rbp)
	movq	$0, -9112(%rbp)
	movq	$0, -9104(%rbp)
	movq	$0, -9096(%rbp)
	movq	$0, -9088(%rbp)
	movq	$0, -9080(%rbp)
	movq	$0, -9072(%rbp)
	movq	$0, -9064(%rbp)
	movq	$0, -9056(%rbp)
	movq	$0, -9048(%rbp)
	movq	$0, -9040(%rbp)
	movq	$0, -9032(%rbp)
	movq	$0, -9024(%rbp)
	movq	$0, -9016(%rbp)
	movq	$0, -9008(%rbp)
	movq	$0, -9000(%rbp)
	movq	$0, -8992(%rbp)
	movq	$0, -8984(%rbp)
	movq	$0, -8976(%rbp)
	movq	$0, -8968(%rbp)
	movq	$0, -8960(%rbp)
	movq	$0, -8952(%rbp)
	movq	$0, -8944(%rbp)
	movq	$0, -8936(%rbp)
	movq	$0, -8928(%rbp)
	movq	$0, -8920(%rbp)
	movq	$0, -8912(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8912(%rbp), %rax
	movq	-9632(%rbp), %rdi
	leaq	-9120(%rbp), %rcx
	pushq	%rax
	leaq	-8920(%rbp), %rax
	leaq	-9104(%rbp), %r9
	pushq	%rax
	leaq	-8928(%rbp), %rax
	leaq	-9112(%rbp), %r8
	pushq	%rax
	leaq	-8936(%rbp), %rax
	leaq	-9128(%rbp), %rdx
	pushq	%rax
	leaq	-8944(%rbp), %rax
	leaq	-9136(%rbp), %rsi
	pushq	%rax
	leaq	-8952(%rbp), %rax
	pushq	%rax
	leaq	-8960(%rbp), %rax
	pushq	%rax
	leaq	-8968(%rbp), %rax
	pushq	%rax
	leaq	-8976(%rbp), %rax
	pushq	%rax
	leaq	-8984(%rbp), %rax
	pushq	%rax
	leaq	-8992(%rbp), %rax
	pushq	%rax
	leaq	-9000(%rbp), %rax
	pushq	%rax
	leaq	-9008(%rbp), %rax
	pushq	%rax
	leaq	-9016(%rbp), %rax
	pushq	%rax
	leaq	-9024(%rbp), %rax
	pushq	%rax
	leaq	-9032(%rbp), %rax
	pushq	%rax
	leaq	-9040(%rbp), %rax
	pushq	%rax
	leaq	-9048(%rbp), %rax
	pushq	%rax
	leaq	-9056(%rbp), %rax
	pushq	%rax
	leaq	-9064(%rbp), %rax
	pushq	%rax
	leaq	-9072(%rbp), %rax
	pushq	%rax
	leaq	-9080(%rbp), %rax
	pushq	%rax
	leaq	-9088(%rbp), %rax
	pushq	%rax
	leaq	-9096(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_NS0_6ObjectES6_NS0_7IntPtrTESB_S5_S5_SB_NS0_10HeapObjectESB_SB_SB_SB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS5_EESK_PNSE_IS6_EESK_SK_SK_SM_SK_PNSE_IS7_EEPNSE_IS8_EEPNSE_IS9_EESK_SM_SK_SK_PNSE_ISA_EESM_PNSE_ISB_EESW_SK_SK_SW_PNSE_ISC_EESW_SW_SW_SW_
	addq	$192, %rsp
	movl	$41, %edx
	movq	%r15, %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-304(%rbp), %rsi
	leaq	-112(%rbp), %rdx
	movq	%r14, %rdi
	movq	-8960(%rbp), %xmm0
	movq	-9008(%rbp), %xmm3
	movq	-9024(%rbp), %xmm4
	movq	-9040(%rbp), %xmm5
	movq	-9056(%rbp), %xmm6
	movhps	-8952(%rbp), %xmm0
	movq	-9072(%rbp), %xmm7
	movhps	-9000(%rbp), %xmm3
	movq	-9088(%rbp), %xmm8
	movhps	-9016(%rbp), %xmm4
	movq	-9104(%rbp), %xmm9
	movhps	-9032(%rbp), %xmm5
	movq	-9120(%rbp), %xmm10
	movhps	-9048(%rbp), %xmm6
	movq	-9136(%rbp), %xmm11
	movhps	-9064(%rbp), %xmm7
	movq	-8976(%rbp), %xmm1
	movhps	-9080(%rbp), %xmm8
	movq	-8992(%rbp), %xmm2
	movhps	-9096(%rbp), %xmm9
	movhps	-9112(%rbp), %xmm10
	movhps	-9128(%rbp), %xmm11
	movaps	%xmm9, -272(%rbp)
	movhps	-8968(%rbp), %xmm1
	movhps	-8984(%rbp), %xmm2
	movaps	%xmm11, -304(%rbp)
	movaps	%xmm10, -288(%rbp)
	movaps	%xmm8, -256(%rbp)
	movaps	%xmm7, -240(%rbp)
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -8880(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9576(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1558
	call	_ZdlPv@PLT
.L1558:
	movq	-9760(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1557
	.p2align 4,,10
	.p2align 3
.L1986:
	movq	-9272(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -9136(%rbp)
	leaq	-2800(%rbp), %r12
	movq	$0, -9128(%rbp)
	movq	$0, -9120(%rbp)
	movq	$0, -9112(%rbp)
	movq	$0, -9104(%rbp)
	movq	$0, -9096(%rbp)
	movq	$0, -9088(%rbp)
	movq	$0, -9080(%rbp)
	movq	$0, -9072(%rbp)
	movq	$0, -9064(%rbp)
	movq	$0, -9056(%rbp)
	movq	$0, -9048(%rbp)
	movq	$0, -9040(%rbp)
	movq	$0, -9032(%rbp)
	movq	$0, -9024(%rbp)
	movq	$0, -9016(%rbp)
	movq	$0, -9008(%rbp)
	movq	$0, -9000(%rbp)
	movq	$0, -8992(%rbp)
	movq	$0, -8984(%rbp)
	movq	$0, -8976(%rbp)
	movq	$0, -8968(%rbp)
	movq	$0, -8960(%rbp)
	movq	$0, -8952(%rbp)
	movq	$0, -8944(%rbp)
	movq	$0, -8936(%rbp)
	movq	$0, -8928(%rbp)
	movq	$0, -8920(%rbp)
	movq	$0, -8912(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8912(%rbp), %rax
	movq	%r12, %rdi
	leaq	-9104(%rbp), %r9
	pushq	%rax
	leaq	-8920(%rbp), %rax
	leaq	-9112(%rbp), %r8
	pushq	%rax
	leaq	-8928(%rbp), %rax
	leaq	-9120(%rbp), %rcx
	pushq	%rax
	leaq	-8936(%rbp), %rax
	leaq	-9128(%rbp), %rdx
	pushq	%rax
	leaq	-8944(%rbp), %rax
	leaq	-9136(%rbp), %rsi
	pushq	%rax
	leaq	-8952(%rbp), %rax
	pushq	%rax
	leaq	-8960(%rbp), %rax
	pushq	%rax
	leaq	-8968(%rbp), %rax
	pushq	%rax
	leaq	-8976(%rbp), %rax
	pushq	%rax
	leaq	-8984(%rbp), %rax
	pushq	%rax
	leaq	-8992(%rbp), %rax
	pushq	%rax
	leaq	-9000(%rbp), %rax
	pushq	%rax
	leaq	-9008(%rbp), %rax
	pushq	%rax
	leaq	-9016(%rbp), %rax
	pushq	%rax
	leaq	-9024(%rbp), %rax
	pushq	%rax
	leaq	-9032(%rbp), %rax
	pushq	%rax
	leaq	-9040(%rbp), %rax
	pushq	%rax
	leaq	-9048(%rbp), %rax
	pushq	%rax
	leaq	-9056(%rbp), %rax
	pushq	%rax
	leaq	-9064(%rbp), %rax
	pushq	%rax
	leaq	-9072(%rbp), %rax
	pushq	%rax
	leaq	-9080(%rbp), %rax
	pushq	%rax
	leaq	-9088(%rbp), %rax
	pushq	%rax
	leaq	-9096(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_NS0_6ObjectES6_NS0_7IntPtrTESB_S5_S5_SB_NS0_10HeapObjectESB_SB_SB_SB_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS5_EESK_PNSE_IS6_EESK_SK_SK_SM_SK_PNSE_IS7_EEPNSE_IS8_EEPNSE_IS9_EESK_SM_SK_SK_PNSE_ISA_EESM_PNSE_ISB_EESW_SK_SK_SW_PNSE_ISC_EESW_SW_SW_SW_
	addq	$192, %rsp
	movl	$39, %edx
	movq	%r15, %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8912(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrMulENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8936(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$38, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-8944(%rbp), %rdx
	call	_ZN2v88internal45UnsafeNewReference20UT5ATSmi10HeapObject_1411EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	pxor	%xmm0, %xmm0
	leaq	-56(%rbp), %rdx
	movq	%r14, %rdi
	movq	-9136(%rbp), %rax
	leaq	-304(%rbp), %rsi
	movq	%rax, -304(%rbp)
	movq	-9128(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	-9120(%rbp), %rax
	movq	%rax, -288(%rbp)
	movq	-9112(%rbp), %rax
	movq	%rax, -280(%rbp)
	movq	-9104(%rbp), %rax
	movq	%rax, -272(%rbp)
	movq	-9096(%rbp), %rax
	movq	%rax, -264(%rbp)
	movq	-9088(%rbp), %rax
	movq	%rax, -256(%rbp)
	movq	-9080(%rbp), %rax
	movq	%rax, -248(%rbp)
	movq	-9072(%rbp), %rax
	movq	%rax, -240(%rbp)
	movq	-9064(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	-9056(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-9048(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-9040(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-9032(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-9024(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-9016(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-9008(%rbp), %rax
	movq	$0, -8864(%rbp)
	movq	%rax, -176(%rbp)
	movq	-9000(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-8992(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-8984(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-8976(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-8968(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-8960(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-8952(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-8944(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-8936(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-8928(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-8920(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-8912(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-8880(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-8872(%rbp), %rax
	movaps	%xmm0, -8880(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9552(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1556
	call	_ZdlPv@PLT
.L1556:
	movq	-9744(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1555
	.p2align 4,,10
	.p2align 3
.L1993:
	movq	-9584(%rbp), %rsi
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$19, %edi
	movq	$0, -8864(%rbp)
	movaps	%xmm0, -8880(%rbp)
	call	_Znwm@PLT
	movl	$2054, %edi
	movdqa	.LC4(%rip), %xmm0
	movq	%r14, %rsi
	movw	%di, 16(%rax)
	movq	-9200(%rbp), %rdi
	leaq	19(%rax), %rdx
	movb	$8, 18(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, -8880(%rbp)
	movq	%rdx, -8864(%rbp)
	movq	%rdx, -8872(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1574
	call	_ZdlPv@PLT
.L1574:
	movq	(%rbx), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	112(%rax), %r11
	movq	(%rax), %r8
	movq	144(%rax), %rdx
	movq	16(%rax), %xmm4
	movq	32(%rax), %xmm3
	movq	48(%rax), %xmm2
	movq	%r11, %xmm5
	movq	%r8, %xmm6
	testq	%rdx, %rdx
	movq	96(%rax), %rbx
	movhps	8(%rax), %xmm6
	movhps	24(%rax), %xmm4
	movq	64(%rax), %xmm1
	cmovne	%rdx, %r12
	movhps	40(%rax), %xmm3
	movhps	56(%rax), %xmm2
	movq	80(%rax), %xmm0
	movq	128(%rax), %r10
	movhps	120(%rax), %xmm5
	movl	$66, %edx
	movhps	72(%rax), %xmm1
	movaps	%xmm6, -9840(%rbp)
	movhps	88(%rax), %xmm0
	movq	%r10, -9856(%rbp)
	movaps	%xmm4, -9824(%rbp)
	movaps	%xmm3, -9776(%rbp)
	movaps	%xmm2, -9760(%rbp)
	movaps	%xmm1, -9744(%rbp)
	movaps	%xmm0, -9728(%rbp)
	movaps	%xmm5, -9712(%rbp)
	movq	%r11, -9688(%rbp)
	movq	%rbx, -9184(%rbp)
	movq	104(%rax), %rbx
	movq	%r8, -9584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-9584(%rbp), %r8
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal107UnsafeCast90UT8ATBigInt7ATFalse6ATNull5ATSmi6ATTrue11ATUndefined10HeapNumber10JSReceiver6String6Symbol_1412EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$74, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -9584(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-9584(%rbp), %r8
	movq	%r14, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, -9584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-9584(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -9584(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$73, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	-9688(%rbp), %r11
	movq	%r14, %rdi
	movq	%r11, %rsi
	call	_ZN2v88internal17CodeStubAssembler21FixedArrayBoundsCheckENS0_8compiler5TNodeINS0_14FixedArrayBaseEEEPNS2_4NodeEiNS1_13ParameterModeE@PLT
	subq	$8, %rsp
	movq	%r12, %rcx
	xorl	%r9d, %r9d
	pushq	$0
	movq	-9688(%rbp), %r11
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	leaq	-1264(%rbp), %r12
	movq	%r11, %rsi
	call	_ZN2v88internal17CodeStubAssembler37StoreFixedArrayOrPropertyArrayElementEPNS0_8compiler4NodeES4_S4_NS0_16WriteBarrierModeEiNS1_13ParameterModeE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$63, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movdqa	-9728(%rbp), %xmm0
	movq	%r14, %rdi
	movdqa	-9840(%rbp), %xmm6
	movdqa	-9824(%rbp), %xmm4
	movdqa	-9776(%rbp), %xmm3
	leaq	-304(%rbp), %rsi
	leaq	-168(%rbp), %rdx
	movaps	%xmm0, -224(%rbp)
	movdqa	-9760(%rbp), %xmm2
	movq	-9184(%rbp), %xmm0
	movdqa	-9744(%rbp), %xmm1
	movdqa	-9712(%rbp), %xmm5
	movaps	%xmm6, -304(%rbp)
	movq	-9856(%rbp), %r10
	movhps	-9584(%rbp), %xmm0
	movaps	%xmm4, -288(%rbp)
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movq	%r10, -176(%rbp)
	movaps	%xmm3, -272(%rbp)
	movaps	%xmm2, -256(%rbp)
	movaps	%xmm1, -240(%rbp)
	movaps	%xmm5, -192(%rbp)
	movaps	%xmm0, -8880(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	popq	%rcx
	popq	%rsi
	testq	%rdi, %rdi
	je	.L1576
	call	_ZdlPv@PLT
.L1576:
	movq	-9784(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1200(%rbp)
	je	.L1577
.L1994:
	movq	-9784(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -9040(%rbp)
	movq	$0, -9032(%rbp)
	movq	$0, -9024(%rbp)
	movq	$0, -9016(%rbp)
	movq	$0, -9008(%rbp)
	movq	$0, -9000(%rbp)
	movq	$0, -8992(%rbp)
	movq	$0, -8984(%rbp)
	movq	$0, -8976(%rbp)
	movq	$0, -8968(%rbp)
	movq	$0, -8960(%rbp)
	movq	$0, -8952(%rbp)
	movq	$0, -8944(%rbp)
	movq	$0, -8936(%rbp)
	movq	$0, -8928(%rbp)
	movq	$0, -8920(%rbp)
	movq	$0, -8912(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8912(%rbp), %rax
	movq	%r12, %rdi
	leaq	-9024(%rbp), %rcx
	pushq	%rax
	leaq	-8920(%rbp), %rax
	leaq	-9008(%rbp), %r9
	pushq	%rax
	leaq	-8928(%rbp), %rax
	leaq	-9032(%rbp), %rdx
	pushq	%rax
	leaq	-8936(%rbp), %rax
	leaq	-9016(%rbp), %r8
	pushq	%rax
	leaq	-8944(%rbp), %rax
	leaq	-9040(%rbp), %rsi
	pushq	%rax
	leaq	-8952(%rbp), %rax
	pushq	%rax
	leaq	-8960(%rbp), %rax
	pushq	%rax
	leaq	-8968(%rbp), %rax
	pushq	%rax
	leaq	-8976(%rbp), %rax
	pushq	%rax
	leaq	-8984(%rbp), %rax
	pushq	%rax
	leaq	-8992(%rbp), %rax
	pushq	%rax
	leaq	-9000(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EESH_PNSB_IS6_EESH_SH_SH_SJ_SH_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESH_SJ_SH_SH_
	addq	$96, %rsp
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8912(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%r8, -9184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-9184(%rbp), %r8
	movq	%r14, %rdi
	movq	%rax, %rbx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-304(%rbp), %rsi
	movq	%r14, %rdi
	movq	-8928(%rbp), %xmm0
	movq	-8944(%rbp), %xmm1
	leaq	-168(%rbp), %rdx
	movq	-8960(%rbp), %xmm2
	movq	%rbx, -176(%rbp)
	movq	-8976(%rbp), %xmm3
	movhps	-8920(%rbp), %xmm0
	movq	-8992(%rbp), %xmm4
	movq	$0, -8864(%rbp)
	movq	-9008(%rbp), %xmm5
	movhps	-8936(%rbp), %xmm1
	movq	-9024(%rbp), %xmm6
	movhps	-8952(%rbp), %xmm2
	movq	-9040(%rbp), %xmm7
	movhps	-8968(%rbp), %xmm3
	movhps	-8984(%rbp), %xmm4
	movaps	%xmm0, -192(%rbp)
	movhps	-9000(%rbp), %xmm5
	movhps	-9016(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -256(%rbp)
	movhps	-9032(%rbp), %xmm7
	movaps	%xmm6, -288(%rbp)
	movaps	%xmm7, -304(%rbp)
	movaps	%xmm5, -272(%rbp)
	movaps	%xmm3, -240(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -8880(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9216(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1578
	call	_ZdlPv@PLT
.L1578:
	movq	-9680(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1577
	.p2align 4,,10
	.p2align 3
.L1992:
	movq	-9728(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$19, %edi
	movq	$0, -8864(%rbp)
	movaps	%xmm0, -8880(%rbp)
	call	_Znwm@PLT
	movl	$2054, %r8d
	movdqa	.LC4(%rip), %xmm0
	movq	%r14, %rsi
	movw	%r8w, 16(%rax)
	movq	-9456(%rbp), %rdi
	leaq	19(%rax), %rdx
	movb	$8, 18(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, -8880(%rbp)
	movq	%rdx, -8864(%rbp)
	movq	%rdx, -8872(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1571
	call	_ZdlPv@PLT
.L1571:
	movq	(%rbx), %rax
	leaq	-304(%rbp), %rsi
	leaq	-152(%rbp), %rdx
	movq	%r14, %rdi
	movdqu	128(%rax), %xmm0
	movdqu	(%rax), %xmm8
	movdqu	16(%rax), %xmm7
	movdqu	32(%rax), %xmm6
	movdqu	48(%rax), %xmm5
	movdqu	64(%rax), %xmm4
	movdqu	80(%rax), %xmm3
	movdqu	96(%rax), %xmm2
	movdqu	112(%rax), %xmm1
	movq	144(%rax), %rax
	movaps	%xmm0, -176(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm8, -304(%rbp)
	movq	%rax, -160(%rbp)
	movaps	%xmm7, -288(%rbp)
	movaps	%xmm6, -272(%rbp)
	movaps	%xmm5, -256(%rbp)
	movaps	%xmm4, -240(%rbp)
	movaps	%xmm3, -224(%rbp)
	movaps	%xmm2, -208(%rbp)
	movaps	%xmm1, -192(%rbp)
	movaps	%xmm0, -8880(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9200(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1572
	call	_ZdlPv@PLT
.L1572:
	movq	-9584(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1570
	.p2align 4,,10
	.p2align 3
.L1991:
	movq	-9712(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-304(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC4(%rip), %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-279(%rbp), %rdx
	movabsq	$506098626754709510, %rax
	movb	$5, -280(%rbp)
	movaps	%xmm0, -304(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -288(%rbp)
	movaps	%xmm0, -8880(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9440(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1568
	call	_ZdlPv@PLT
.L1568:
	movq	(%rbx), %rax
	movq	8(%rax), %rcx
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	72(%rax), %r10
	movq	88(%rax), %r11
	movq	%rcx, -9200(%rbp)
	movq	104(%rax), %r9
	movq	16(%rax), %rcx
	movq	%rsi, -9712(%rbp)
	movq	(%rax), %rbx
	movq	32(%rax), %rsi
	movq	%rdx, -9760(%rbp)
	movq	120(%rax), %r8
	movq	48(%rax), %rdx
	movq	%rdi, -9824(%rbp)
	movq	%r10, -9856(%rbp)
	movq	64(%rax), %rdi
	movq	80(%rax), %r10
	movq	%r11, -9888(%rbp)
	movq	%r9, -9920(%rbp)
	movq	96(%rax), %r11
	movq	112(%rax), %r9
	movq	%r10, -9872(%rbp)
	movq	%r11, -9904(%rbp)
	movq	%rcx, -9688(%rbp)
	movq	%rbx, -9184(%rbp)
	movq	%rsi, -9744(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -9776(%rbp)
	movl	$68, %edx
	movq	%rdi, -9840(%rbp)
	movq	%r15, %rdi
	movq	%r9, -9936(%rbp)
	movq	%r8, -9808(%rbp)
	movq	128(%rax), %r8
	movq	136(%rax), %rbx
	movq	192(%rax), %r9
	movq	%r8, -9952(%rbp)
	movq	%r9, -9984(%rbp)
	movq	%rbx, -9960(%rbp)
	movq	184(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-9984(%rbp), %r9
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6ObjectEvE5valueE(%rip), %esi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$67, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-152(%rbp), %rdx
	movq	%r12, %rsi
	movq	-9184(%rbp), %xmm0
	movq	%r14, %rdi
	movq	%rbx, -160(%rbp)
	movhps	-9200(%rbp), %xmm0
	movq	$0, -8864(%rbp)
	movaps	%xmm0, -304(%rbp)
	movq	-9688(%rbp), %xmm0
	movhps	-9712(%rbp), %xmm0
	movaps	%xmm0, -288(%rbp)
	movq	-9744(%rbp), %xmm0
	movhps	-9760(%rbp), %xmm0
	movaps	%xmm0, -272(%rbp)
	movq	-9776(%rbp), %xmm0
	movhps	-9824(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	-9840(%rbp), %xmm0
	movhps	-9856(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-9872(%rbp), %xmm0
	movhps	-9888(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-9904(%rbp), %xmm0
	movhps	-9920(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-9936(%rbp), %xmm0
	movhps	-9808(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-9952(%rbp), %xmm0
	movhps	-9960(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8880(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1456(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -9200(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1569
	call	_ZdlPv@PLT
.L1569:
	movq	-9584(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1567
	.p2align 4,,10
	.p2align 3
.L1990:
	movq	-9688(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-304(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC4(%rip), %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movabsq	$361983438678853638, %rax
	movl	$1287, %r9d
	leaq	-278(%rbp), %rdx
	movq	$0, -8864(%rbp)
	movaps	%xmm0, -304(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -288(%rbp)
	movw	%r9w, -280(%rbp)
	movaps	%xmm0, -8880(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9600(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1565
	call	_ZdlPv@PLT
.L1565:
	movq	(%rbx), %rax
	leaq	-104(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqu	192(%rax), %xmm15
	movdqu	176(%rax), %xmm0
	movdqu	(%rax), %xmm11
	movdqu	16(%rax), %xmm10
	movdqu	32(%rax), %xmm9
	movdqu	48(%rax), %xmm8
	punpcklqdq	%xmm15, %xmm0
	movdqu	64(%rax), %xmm7
	movdqu	80(%rax), %xmm6
	movdqu	96(%rax), %xmm5
	movdqu	112(%rax), %xmm4
	movdqu	128(%rax), %xmm3
	movdqu	144(%rax), %xmm2
	movdqu	160(%rax), %xmm1
	movq	200(%rax), %rax
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm11, -304(%rbp)
	movq	%rax, -112(%rbp)
	movaps	%xmm10, -288(%rbp)
	movaps	%xmm9, -272(%rbp)
	movaps	%xmm8, -256(%rbp)
	movaps	%xmm7, -240(%rbp)
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -8880(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1840(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -9440(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1566
	call	_ZdlPv@PLT
.L1566:
	movq	-9712(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1564
	.p2align 4,,10
	.p2align 3
.L1989:
	movq	-9760(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC4(%rip), %xmm0
	leaq	-304(%rbp), %rsi
	movq	%r14, %rdi
	movabsq	$361983438678853638, %rax
	leaq	-280(%rbp), %rdx
	movq	$0, -8864(%rbp)
	movaps	%xmm0, -304(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -288(%rbp)
	movaps	%xmm0, -8880(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9576(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1563
	call	_ZdlPv@PLT
.L1563:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L1562
	.p2align 4,,10
	.p2align 3
.L1988:
	movq	-9744(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-304(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC4(%rip), %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movabsq	$361983438678853638, %rax
	movl	$1797, %r10d
	leaq	-273(%rbp), %rdx
	movl	$84215047, -280(%rbp)
	movaps	%xmm0, -304(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -288(%rbp)
	movw	%r10w, -276(%rbp)
	movaps	%xmm0, -8880(%rbp)
	movb	$5, -274(%rbp)
	movq	$0, -8864(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-9552(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1560
	call	_ZdlPv@PLT
.L1560:
	movq	(%rbx), %rax
	movq	56(%rax), %rdi
	movq	8(%rax), %rcx
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	88(%rax), %r10
	movq	104(%rax), %r11
	movq	%rdi, -9840(%rbp)
	movq	72(%rax), %rdi
	movq	(%rax), %rbx
	movq	%rcx, -9200(%rbp)
	movq	120(%rax), %r9
	movq	16(%rax), %rcx
	movq	%rsi, -9600(%rbp)
	movq	%rdx, -9776(%rbp)
	movq	32(%rax), %rsi
	movq	48(%rax), %rdx
	movq	%rdi, -9856(%rbp)
	movq	%r10, -9888(%rbp)
	movq	80(%rax), %rdi
	movq	96(%rax), %r10
	movq	%r11, -9920(%rbp)
	movq	112(%rax), %r11
	movq	%rbx, -9184(%rbp)
	movq	%r10, -9904(%rbp)
	movq	64(%rax), %rbx
	movq	%r11, -9936(%rbp)
	movq	%rcx, -9440(%rbp)
	movq	%rsi, -9744(%rbp)
	movq	%rdx, -9824(%rbp)
	movq	%rdi, -9872(%rbp)
	movq	%r15, %rdi
	movq	%r9, -9808(%rbp)
	movq	128(%rax), %r9
	movq	168(%rax), %rsi
	movq	184(%rax), %rdx
	movq	136(%rax), %r8
	movq	152(%rax), %rcx
	movq	%r9, -9952(%rbp)
	movq	%rsi, -10008(%rbp)
	movq	176(%rax), %rsi
	movq	%rdx, -10024(%rbp)
	movq	232(%rax), %rdx
	movq	%r8, -9960(%rbp)
	movq	144(%rax), %r8
	movq	%rcx, -9968(%rbp)
	movq	160(%rax), %rcx
	movq	240(%rax), %rax
	movq	%rsi, -10016(%rbp)
	leaq	.LC5(%rip), %rsi
	movq	%rdx, -10032(%rbp)
	movl	$56, %edx
	movq	%r8, -9984(%rbp)
	movq	%rcx, -10000(%rbp)
	movq	%rax, -10040(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-96(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-9184(%rbp), %xmm0
	movhps	-9200(%rbp), %xmm0
	movaps	%xmm0, -304(%rbp)
	movq	-9440(%rbp), %xmm0
	movhps	-9600(%rbp), %xmm0
	movaps	%xmm0, -288(%rbp)
	movq	-9744(%rbp), %xmm0
	movhps	-9776(%rbp), %xmm0
	movaps	%xmm0, -272(%rbp)
	movq	-9824(%rbp), %xmm0
	movhps	-9840(%rbp), %xmm0
	movaps	%xmm0, -256(%rbp)
	movq	%rbx, %xmm0
	movhps	-9856(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-9872(%rbp), %xmm0
	movhps	-9888(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-9904(%rbp), %xmm0
	movhps	-9920(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-9936(%rbp), %xmm0
	movhps	-9808(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-9952(%rbp), %xmm0
	movhps	-9960(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-9984(%rbp), %xmm0
	movhps	-9968(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-10000(%rbp), %xmm0
	movhps	-10008(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-10016(%rbp), %xmm0
	movq	$0, -8864(%rbp)
	movhps	-10024(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-10032(%rbp), %xmm0
	movhps	-10040(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -8880(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2032(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -9600(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1561
	call	_ZdlPv@PLT
.L1561:
	movq	-9688(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1995:
	movq	-9696(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -9040(%rbp)
	movq	$0, -9032(%rbp)
	movq	$0, -9024(%rbp)
	movq	$0, -9016(%rbp)
	movq	$0, -9008(%rbp)
	movq	$0, -9000(%rbp)
	movq	$0, -8992(%rbp)
	movq	$0, -8984(%rbp)
	movq	$0, -8976(%rbp)
	movq	$0, -8968(%rbp)
	movq	$0, -8960(%rbp)
	movq	$0, -8952(%rbp)
	movq	$0, -8944(%rbp)
	movq	$0, -8936(%rbp)
	movq	$0, -8928(%rbp)
	movq	$0, -8920(%rbp)
	movq	$0, -8912(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-8912(%rbp), %rax
	movq	-9616(%rbp), %rdi
	leaq	-9024(%rbp), %rcx
	pushq	%rax
	leaq	-8920(%rbp), %rax
	leaq	-9008(%rbp), %r9
	pushq	%rax
	leaq	-8928(%rbp), %rax
	leaq	-9016(%rbp), %r8
	pushq	%rax
	leaq	-8936(%rbp), %rax
	leaq	-9032(%rbp), %rdx
	pushq	%rax
	leaq	-8944(%rbp), %rax
	leaq	-9040(%rbp), %rsi
	pushq	%rax
	leaq	-8952(%rbp), %rax
	pushq	%rax
	leaq	-8960(%rbp), %rax
	pushq	%rax
	leaq	-8968(%rbp), %rax
	pushq	%rax
	leaq	-8976(%rbp), %rax
	pushq	%rax
	leaq	-8984(%rbp), %rax
	pushq	%rax
	leaq	-8992(%rbp), %rax
	pushq	%rax
	leaq	-9000(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_10FixedArrayES5_S5_S5_S6_S5_NS0_7ContextENS0_3MapENS0_7JSArrayES5_S6_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EESH_PNSB_IS6_EESH_SH_SH_SJ_SH_PNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EESH_SJ_SH_SH_
	addq	$96, %rsp
	movl	$79, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-9024(%rbp), %rdx
	movq	-8992(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler6SmiMaxENS0_8compiler5TNodeINS0_3SmiEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8984(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler6SmiMinENS0_8compiler5TNodeINS0_3SmiEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$80, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8984(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%r8, -9680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-9680(%rbp), %r8
	movq	%r14, %rdi
	movq	%rax, -9184(%rbp)
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-9184(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -9184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$82, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-8936(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$83, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -9680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$84, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-9184(%rbp), %r8
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$81, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -9184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-9184(%rbp), %rax
	pushq	$4
	movq	%rbx, %r9
	movq	-8976(%rbp), %r8
	movq	-9680(%rbp), %rcx
	movl	$2, %esi
	movq	%r14, %rdi
	pushq	%rax
	movq	-8928(%rbp), %rdx
	call	_ZN2v88internal17CodeStubAssembler12CopyElementsENS0_12ElementsKindENS0_8compiler5TNodeINS0_14FixedArrayBaseEEENS4_INS0_7IntPtrTEEES6_S8_S8_NS0_16WriteBarrierModeE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$85, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-9040(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movaps	%xmm0, -8880(%rbp)
	movq	%rax, -304(%rbp)
	movq	-9032(%rbp), %rax
	movq	$0, -8864(%rbp)
	movq	%rax, -296(%rbp)
	movq	-9024(%rbp), %rax
	movq	%rax, -288(%rbp)
	movq	-9016(%rbp), %rax
	movq	%rax, -280(%rbp)
	movq	-8944(%rbp), %rax
	movq	%rax, -272(%rbp)
	call	_Znwm@PLT
	movq	-272(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-304(%rbp), %xmm6
	movdqa	-288(%rbp), %xmm2
	movq	-9584(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -8880(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -8864(%rbp)
	movq	%rdx, -8872(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L1580
	call	_ZdlPv@PLT
.L1580:
	movq	-9792(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1579
	.p2align 4,,10
	.p2align 3
.L1996:
	movq	-9792(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -8944(%rbp)
	movq	$0, -8936(%rbp)
	movq	$0, -8928(%rbp)
	movq	$0, -8920(%rbp)
	movq	$0, -8912(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-9584(%rbp), %rdi
	leaq	-8928(%rbp), %rcx
	leaq	-8912(%rbp), %r9
	leaq	-8920(%rbp), %r8
	leaq	-8936(%rbp), %rdx
	leaq	-8944(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_27JSArgumentsObjectWithLengthENS0_3SmiES5_NS0_7JSArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EESE_PNS8_IS6_EE
	movl	$29, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-8944(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movaps	%xmm0, -8880(%rbp)
	movq	%rax, -304(%rbp)
	movq	-8936(%rbp), %rax
	movq	$0, -8864(%rbp)
	movq	%rax, -296(%rbp)
	movq	-8928(%rbp), %rax
	movq	%rax, -288(%rbp)
	movq	-8920(%rbp), %rax
	movq	%rax, -280(%rbp)
	movq	-8912(%rbp), %rax
	movq	%rax, -272(%rbp)
	call	_Znwm@PLT
	movq	-272(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-304(%rbp), %xmm4
	movdqa	-288(%rbp), %xmm7
	movq	-9184(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -8880(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -8864(%rbp)
	movq	%rdx, -8872(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1582
	call	_ZdlPv@PLT
.L1582:
	movq	-9672(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -624(%rbp)
	je	.L1583
.L1997:
	movq	-9640(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-9168(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	$0, -8864(%rbp)
	movaps	%xmm0, -8880(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-8880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1584
	call	_ZdlPv@PLT
.L1584:
	movq	-9800(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1583
	.p2align 4,,10
	.p2align 3
.L1999:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %rsi
	movq	-8928(%rbp), %xmm0
	movq	-9184(%rbp), %r12
	movq	-8944(%rbp), %xmm1
	movq	-9200(%rbp), %rax
	leaq	-264(%rbp), %rdx
	movq	$0, -8896(%rbp)
	movhps	-8920(%rbp), %xmm0
	movq	%r12, %rdi
	movhps	-8936(%rbp), %xmm1
	movaps	%xmm0, -288(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -272(%rbp)
	movaps	%xmm1, -304(%rbp)
	movaps	%xmm0, -8912(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-8176(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8912(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1468
	call	_ZdlPv@PLT
.L1468:
	movq	-9656(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1467
	.p2align 4,,10
	.p2align 3
.L2000:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, -240(%rbp)
	movq	%r12, %rsi
	movq	-9184(%rbp), %rbx
	movdqa	-9200(%rbp), %xmm3
	movdqa	-9216(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	leaq	-232(%rbp), %rdx
	movdqa	-9344(%rbp), %xmm6
	movdqa	-9536(%rbp), %xmm2
	movq	%rbx, %rdi
	movaps	%xmm0, -8912(%rbp)
	movaps	%xmm3, -304(%rbp)
	movaps	%xmm5, -288(%rbp)
	movaps	%xmm6, -272(%rbp)
	movaps	%xmm2, -256(%rbp)
	movq	$0, -8896(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-6640(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-8912(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1494
	call	_ZdlPv@PLT
.L1494:
	movq	-9648(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1493
.L1998:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22438:
	.size	_ZN2v88internal40HandleFastAliasedSloppyArgumentsSlice_39EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_27JSArgumentsObjectWithLengthEEENS4_INS0_3SmiEEESA_PNS1_18CodeAssemblerLabelE, .-_ZN2v88internal40HandleFastAliasedSloppyArgumentsSlice_39EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_27JSArgumentsObjectWithLengthEEENS4_INS0_3SmiEEESA_PNS1_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS8_EESG_PNSA_IS6_EESI_SE_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS8_EESG_PNSA_IS6_EESI_SE_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS8_EESG_PNSA_IS6_EESI_SE_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS8_EESG_PNSA_IS6_EESI_SE_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS8_EESG_PNSA_IS6_EESI_SE_:
.LFB27393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$7, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$1542, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$134744071, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$8, 6(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2002
	movq	%rax, -112(%rbp)
	call	_ZdlPv@PLT
	movq	-112(%rbp), %rax
.L2002:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2003
	movq	%rdx, (%r15)
.L2003:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2004
	movq	%rdx, (%r14)
.L2004:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2005
	movq	%rdx, 0(%r13)
.L2005:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2006
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L2006:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2007
	movq	%rdx, (%rbx)
.L2007:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2008
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2008:
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L2001
	movq	-104(%rbp), %rbx
	movq	%rax, (%rbx)
.L2001:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2036
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2036:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27393:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS8_EESG_PNSA_IS6_EESI_SE_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS8_EESG_PNSA_IS6_EESI_SE_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_NS0_27JSArgumentsObjectWithLengthENS0_3MapEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS8_EESI_PNSC_IS6_EESK_SG_PNSC_IS9_EEPNSC_ISA_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_NS0_27JSArgumentsObjectWithLengthENS0_3MapEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS8_EESI_PNSC_IS6_EESK_SG_PNSC_IS9_EEPNSC_ISA_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_NS0_27JSArgumentsObjectWithLengthENS0_3MapEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS8_EESI_PNSC_IS6_EESK_SG_PNSC_IS9_EEPNSC_ISA_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_NS0_27JSArgumentsObjectWithLengthENS0_3MapEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS8_EESI_PNSC_IS6_EESK_SG_PNSC_IS9_EEPNSC_ISA_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_NS0_27JSArgumentsObjectWithLengthENS0_3MapEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS8_EESI_PNSC_IS6_EESK_SG_PNSC_IS9_EEPNSC_ISA_EE:
.LFB27398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$9, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$506661581053495303, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2038
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
.L2038:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2039
	movq	%rdx, (%r15)
.L2039:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2040
	movq	%rdx, (%r14)
.L2040:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2041
	movq	%rdx, 0(%r13)
.L2041:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2042
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L2042:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2043
	movq	%rdx, (%rbx)
.L2043:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2044
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2044:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2045
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2045:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2046
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2046:
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L2037
	movq	-120(%rbp), %rbx
	movq	%rax, (%rbx)
.L2037:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2080
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2080:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27398:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_NS0_27JSArgumentsObjectWithLengthENS0_3MapEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS8_EESI_PNSC_IS6_EESK_SG_PNSC_IS9_EEPNSC_ISA_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_NS0_27JSArgumentsObjectWithLengthENS0_3MapEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS8_EESI_PNSC_IS6_EESK_SG_PNSC_IS9_EEPNSC_ISA_EE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_NS0_27JSArgumentsObjectWithLengthENS0_3MapES3_S9_S6_S6_NS0_7JSArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS8_EESJ_PNSD_IS6_EESL_SH_PNSD_IS9_EEPNSD_ISA_EESF_SN_SL_SL_PNSD_ISB_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_NS0_27JSArgumentsObjectWithLengthENS0_3MapES3_S9_S6_S6_NS0_7JSArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS8_EESJ_PNSD_IS6_EESL_SH_PNSD_IS9_EEPNSD_ISA_EESF_SN_SL_SL_PNSD_ISB_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_NS0_27JSArgumentsObjectWithLengthENS0_3MapES3_S9_S6_S6_NS0_7JSArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS8_EESJ_PNSD_IS6_EESL_SH_PNSD_IS9_EEPNSD_ISA_EESF_SN_SL_SL_PNSD_ISB_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_NS0_27JSArgumentsObjectWithLengthENS0_3MapES3_S9_S6_S6_NS0_7JSArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS8_EESJ_PNSD_IS6_EESL_SH_PNSD_IS9_EEPNSD_ISA_EESF_SN_SL_SL_PNSD_ISB_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_NS0_27JSArgumentsObjectWithLengthENS0_3MapES3_S9_S6_S6_NS0_7JSArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS8_EESJ_PNSD_IS6_EESL_SH_PNSD_IS9_EEPNSD_ISA_EESF_SN_SL_SL_PNSD_ISB_EE:
.LFB27402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$14, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$136, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$506661581053495303, %rcx
	movq	%rcx, (%rax)
	movl	$1798, %ecx
	leaq	14(%rax), %rdx
	movl	$101123847, 8(%rax)
	movw	%cx, 12(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2082
	movq	%rax, -168(%rbp)
	call	_ZdlPv@PLT
	movq	-168(%rbp), %rax
.L2082:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2083
	movq	%rdx, (%r15)
.L2083:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2084
	movq	%rdx, (%r14)
.L2084:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2085
	movq	%rdx, 0(%r13)
.L2085:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2086
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L2086:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2087
	movq	%rdx, (%rbx)
.L2087:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2088
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2088:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2089
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2089:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2090
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2090:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2091
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2091:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2092
	movq	-128(%rbp), %rsi
	movq	%rdx, (%rsi)
.L2092:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2093
	movq	-136(%rbp), %rcx
	movq	%rdx, (%rcx)
.L2093:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2094
	movq	-144(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2094:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2095
	movq	-152(%rbp), %rsi
	movq	%rdx, (%rsi)
.L2095:
	movq	104(%rax), %rax
	testq	%rax, %rax
	je	.L2081
	movq	-160(%rbp), %rcx
	movq	%rax, (%rcx)
.L2081:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2144
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2144:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27402:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_NS0_27JSArgumentsObjectWithLengthENS0_3MapES3_S9_S6_S6_NS0_7JSArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS8_EESJ_PNSD_IS6_EESL_SH_PNSD_IS9_EEPNSD_ISA_EESF_SN_SL_SL_PNSD_ISB_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_NS0_27JSArgumentsObjectWithLengthENS0_3MapES3_S9_S6_S6_NS0_7JSArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS8_EESJ_PNSD_IS6_EESL_SH_PNSD_IS9_EEPNSD_ISA_EESF_SN_SL_SL_PNSD_ISB_EE
	.section	.text._ZN2v88internal18HandleFastSlice_40EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_6ObjectEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEESD_PNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18HandleFastSlice_40EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_6ObjectEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEESD_PNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal18HandleFastSlice_40EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_6ObjectEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEESD_PNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal18HandleFastSlice_40EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_6ObjectEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEESD_PNS1_18CodeAssemblerLabelE:
.LFB22531:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$2536, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, -6600(%rbp)
	leaq	-6216(%rbp), %r15
	leaq	-5880(%rbp), %r12
	movq	%rdi, -6232(%rbp)
	leaq	-5496(%rbp), %rbx
	leaq	-6064(%rbp), %r14
	movq	%rsi, -6240(%rbp)
	movq	%rdx, -6256(%rbp)
	movq	%rcx, -6288(%rbp)
	movq	%r8, -6272(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -6216(%rbp)
	movq	%rdi, -5936(%rbp)
	movl	$96, %edi
	movq	$0, -5928(%rbp)
	movq	$0, -5920(%rbp)
	movq	$0, -5912(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r12, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -5912(%rbp)
	movq	%rdx, -5920(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5896(%rbp)
	movq	%rax, -5928(%rbp)
	movq	$0, -5904(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6216(%rbp), %rax
	movl	$120, %edi
	movq	$0, -5736(%rbp)
	movq	$0, -5728(%rbp)
	movq	%rax, -5744(%rbp)
	movq	$0, -5720(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -5736(%rbp)
	leaq	-5688(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5720(%rbp)
	movq	%rdx, -5728(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5704(%rbp)
	movq	%rax, -6368(%rbp)
	movq	$0, -5712(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6216(%rbp), %rax
	movl	$144, %edi
	movq	$0, -5544(%rbp)
	movq	$0, -5536(%rbp)
	movq	%rax, -5552(%rbp)
	movq	$0, -5528(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rdx, -5528(%rbp)
	movq	%rdx, -5536(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5512(%rbp)
	movq	%rax, -5544(%rbp)
	movq	$0, -5520(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-5360(%rbp), %rax
	movl	$6, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -6488(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-5168(%rbp), %rax
	movl	$7, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -6312(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-4976(%rbp), %rax
	movl	$8, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -6504(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-4784(%rbp), %rax
	movl	$9, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -6328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-6216(%rbp), %rax
	movl	$192, %edi
	movq	$0, -4584(%rbp)
	movq	$0, -4576(%rbp)
	movq	%rax, -4592(%rbp)
	movq	$0, -4568(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movups	%xmm0, (%rax)
	leaq	192(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -4584(%rbp)
	leaq	-4536(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4568(%rbp)
	movq	%rdx, -4576(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4552(%rbp)
	movq	%rax, -6384(%rbp)
	movq	$0, -4560(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-4400(%rbp), %rax
	movl	$8, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -6336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-4208(%rbp), %rax
	movl	$7, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -6304(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-4016(%rbp), %rax
	movl	$8, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -6496(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3824(%rbp), %rax
	movl	$9, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -6320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-6216(%rbp), %rax
	movl	$216, %edi
	movq	$0, -3624(%rbp)
	movq	$0, -3616(%rbp)
	movq	%rax, -3632(%rbp)
	movq	$0, -3608(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movups	%xmm0, (%rax)
	leaq	216(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -3624(%rbp)
	leaq	-3576(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3608(%rbp)
	movq	%rdx, -3616(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3592(%rbp)
	movq	%rax, -6352(%rbp)
	movq	$0, -3600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-3440(%rbp), %rax
	movl	$13, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -6512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-6216(%rbp), %rax
	movl	$336, %edi
	movq	$0, -3240(%rbp)
	movq	$0, -3232(%rbp)
	movq	%rax, -3248(%rbp)
	movq	$0, -3224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -3240(%rbp)
	leaq	-3192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3224(%rbp)
	movq	%rdx, -3232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3208(%rbp)
	movq	%rax, -6400(%rbp)
	movq	$0, -3216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6216(%rbp), %rax
	movl	$216, %edi
	movq	$0, -3048(%rbp)
	movq	$0, -3040(%rbp)
	movq	%rax, -3056(%rbp)
	movq	$0, -3032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -3048(%rbp)
	leaq	-3000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3032(%rbp)
	movq	%rdx, -3040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3016(%rbp)
	movq	%rax, -6464(%rbp)
	movq	$0, -3024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6216(%rbp), %rax
	movl	$240, %edi
	movq	$0, -2856(%rbp)
	movq	$0, -2848(%rbp)
	movq	%rax, -2864(%rbp)
	movq	$0, -2840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -2856(%rbp)
	leaq	-2808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2840(%rbp)
	movq	%rdx, -2848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2824(%rbp)
	movq	%rax, -6448(%rbp)
	movq	$0, -2832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6216(%rbp), %rax
	movl	$240, %edi
	movq	$0, -2664(%rbp)
	movq	$0, -2656(%rbp)
	movq	%rax, -2672(%rbp)
	movq	$0, -2648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -2664(%rbp)
	leaq	-2616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2648(%rbp)
	movq	%rdx, -2656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2632(%rbp)
	movq	%rax, -6472(%rbp)
	movq	$0, -2640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6216(%rbp), %rax
	movl	$264, %edi
	movq	$0, -2472(%rbp)
	movq	$0, -2464(%rbp)
	movq	%rax, -2480(%rbp)
	movq	$0, -2456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -2472(%rbp)
	leaq	-2424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2456(%rbp)
	movq	%rdx, -2464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2440(%rbp)
	movq	%rax, -6296(%rbp)
	movq	$0, -2448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6216(%rbp), %rax
	movl	$216, %edi
	movq	$0, -2280(%rbp)
	movq	$0, -2272(%rbp)
	movq	%rax, -2288(%rbp)
	movq	$0, -2264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -2280(%rbp)
	leaq	-2232(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2264(%rbp)
	movq	%rdx, -2272(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2248(%rbp)
	movq	%rax, -6480(%rbp)
	movq	$0, -2256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6216(%rbp), %rax
	movl	$312, %edi
	movq	$0, -2088(%rbp)
	movq	$0, -2080(%rbp)
	movq	%rax, -2096(%rbp)
	movq	$0, -2072(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rax, -2088(%rbp)
	leaq	-2040(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2072(%rbp)
	movq	%rdx, -2080(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2056(%rbp)
	movq	%rax, -6592(%rbp)
	movq	$0, -2064(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6216(%rbp), %rax
	movl	$336, %edi
	movq	$0, -1896(%rbp)
	movq	$0, -1888(%rbp)
	movq	%rax, -1904(%rbp)
	movq	$0, -1880(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -1896(%rbp)
	leaq	-1848(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1880(%rbp)
	movq	%rdx, -1888(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1864(%rbp)
	movq	%rax, -6552(%rbp)
	movq	$0, -1872(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6216(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1704(%rbp)
	movq	$0, -1696(%rbp)
	movq	%rax, -1712(%rbp)
	movq	$0, -1688(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1704(%rbp)
	leaq	-1656(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1688(%rbp)
	movq	%rdx, -1696(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1672(%rbp)
	movq	%rax, -6560(%rbp)
	movq	$0, -1680(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6216(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1512(%rbp)
	movq	$0, -1504(%rbp)
	movq	%rax, -1520(%rbp)
	movq	$0, -1496(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1512(%rbp)
	leaq	-1464(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1496(%rbp)
	movq	%rdx, -1504(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1480(%rbp)
	movq	%rax, -6576(%rbp)
	movq	$0, -1488(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6216(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1320(%rbp)
	movq	$0, -1312(%rbp)
	movq	%rax, -1328(%rbp)
	movq	$0, -1304(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1320(%rbp)
	leaq	-1272(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1304(%rbp)
	movq	%rdx, -1312(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1288(%rbp)
	movq	%rax, -6568(%rbp)
	movq	$0, -1296(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6216(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1128(%rbp)
	movq	$0, -1120(%rbp)
	movq	%rax, -1136(%rbp)
	movq	$0, -1112(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1128(%rbp)
	leaq	-1080(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1112(%rbp)
	movq	%rdx, -1120(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1096(%rbp)
	movq	%rax, -6544(%rbp)
	movq	$0, -1104(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6216(%rbp), %rax
	movl	$168, %edi
	movq	$0, -936(%rbp)
	movq	$0, -928(%rbp)
	movq	%rax, -944(%rbp)
	movq	$0, -920(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -936(%rbp)
	leaq	-888(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -920(%rbp)
	movq	%rdx, -928(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -904(%rbp)
	movq	%rax, -6584(%rbp)
	movq	$0, -912(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6216(%rbp), %rax
	movl	$120, %edi
	movq	$0, -744(%rbp)
	movq	$0, -736(%rbp)
	movq	%rax, -752(%rbp)
	movq	$0, -728(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movups	%xmm0, (%rax)
	leaq	120(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -744(%rbp)
	leaq	-696(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -728(%rbp)
	movq	%rdx, -736(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -712(%rbp)
	movq	%rax, -6536(%rbp)
	movq	$0, -720(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6216(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r15, %rsi
	movq	$0, -552(%rbp)
	movq	%rax, -560(%rbp)
	leaq	-504(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -6264(%rbp)
	movq	$0, -544(%rbp)
	movq	$0, -536(%rbp)
	movq	$0, -528(%rbp)
	movq	$0, -520(%rbp)
	movq	$0, -512(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6216(%rbp), %rax
	movl	$120, %edi
	movq	$0, -360(%rbp)
	movq	$0, -352(%rbp)
	movq	%rax, -368(%rbp)
	movq	$0, -344(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -360(%rbp)
	leaq	-312(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -344(%rbp)
	movq	%rdx, -352(%rbp)
	xorl	%edx, %edx
	movq	%rax, -6520(%rbp)
	movups	%xmm0, -328(%rbp)
	movq	$0, -336(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-6288(%rbp), %xmm1
	movq	-6240(%rbp), %xmm2
	movaps	%xmm0, -6064(%rbp)
	movhps	-6272(%rbp), %xmm1
	movq	$0, -6048(%rbp)
	movhps	-6256(%rbp), %xmm2
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm2, -176(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm5
	movdqa	-160(%rbp), %xmm6
	movq	%r14, %rsi
	leaq	32(%rax), %rdx
	movq	%rax, -6064(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	leaq	-5936(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6048(%rbp)
	movq	%rdx, -6056(%rbp)
	movq	%rax, -6528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2146
	call	_ZdlPv@PLT
.L2146:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-5552(%rbp), %rax
	cmpq	$0, -5872(%rbp)
	movq	%rax, -6344(%rbp)
	leaq	-5744(%rbp), %rax
	movq	%rax, -6272(%rbp)
	jne	.L2412
.L2147:
	leaq	-560(%rbp), %rax
	cmpq	$0, -5680(%rbp)
	movq	%rax, -6240(%rbp)
	jne	.L2413
.L2152:
	cmpq	$0, -5488(%rbp)
	jne	.L2414
.L2154:
	cmpq	$0, -5296(%rbp)
	jne	.L2415
.L2159:
	cmpq	$0, -5104(%rbp)
	jne	.L2416
.L2161:
	cmpq	$0, -4912(%rbp)
	jne	.L2417
.L2166:
	leaq	-4592(%rbp), %rax
	cmpq	$0, -4720(%rbp)
	movq	%rax, -6368(%rbp)
	jne	.L2418
	cmpq	$0, -4528(%rbp)
	jne	.L2419
.L2173:
	leaq	-752(%rbp), %rax
	cmpq	$0, -4336(%rbp)
	movq	%rax, -6256(%rbp)
	jne	.L2420
	cmpq	$0, -4144(%rbp)
	jne	.L2421
.L2178:
	leaq	-1328(%rbp), %rax
	cmpq	$0, -3952(%rbp)
	movq	%rax, -6432(%rbp)
	jne	.L2422
.L2182:
	leaq	-3632(%rbp), %rax
	cmpq	$0, -3760(%rbp)
	movq	%rax, -6416(%rbp)
	leaq	-3056(%rbp), %rax
	movq	%rax, -6384(%rbp)
	jne	.L2423
.L2185:
	leaq	-3248(%rbp), %rax
	cmpq	$0, -3568(%rbp)
	movq	%rax, -6440(%rbp)
	jne	.L2424
	cmpq	$0, -3376(%rbp)
	jne	.L2425
.L2193:
	cmpq	$0, -3184(%rbp)
	jne	.L2426
.L2196:
	leaq	-2864(%rbp), %rax
	cmpq	$0, -2992(%rbp)
	movq	%rax, -6352(%rbp)
	leaq	-2672(%rbp), %rax
	movq	%rax, -6400(%rbp)
	jne	.L2427
.L2198:
	leaq	-2480(%rbp), %rax
	cmpq	$0, -2800(%rbp)
	movq	%rax, -6288(%rbp)
	jne	.L2428
	cmpq	$0, -2608(%rbp)
	jne	.L2429
.L2204:
	leaq	-2288(%rbp), %rax
	cmpq	$0, -2416(%rbp)
	movq	%rax, -6464(%rbp)
	leaq	-1712(%rbp), %rax
	movq	%rax, -6472(%rbp)
	jne	.L2430
.L2207:
	leaq	-1904(%rbp), %rax
	cmpq	$0, -2224(%rbp)
	movq	%rax, -6448(%rbp)
	leaq	-2096(%rbp), %rax
	movq	%rax, -6296(%rbp)
	jne	.L2431
.L2211:
	cmpq	$0, -2032(%rbp)
	jne	.L2432
	cmpq	$0, -1840(%rbp)
	jne	.L2433
.L2217:
	cmpq	$0, -1648(%rbp)
	leaq	-1520(%rbp), %r12
	jne	.L2434
.L2219:
	leaq	-1136(%rbp), %rax
	cmpq	$0, -1456(%rbp)
	movq	%rax, -6232(%rbp)
	jne	.L2435
	cmpq	$0, -1264(%rbp)
	jne	.L2436
.L2223:
	leaq	-944(%rbp), %rax
	cmpq	$0, -1072(%rbp)
	movq	%rax, -6480(%rbp)
	jne	.L2437
	cmpq	$0, -880(%rbp)
	jne	.L2438
.L2227:
	cmpq	$0, -688(%rbp)
	leaq	-368(%rbp), %r13
	jne	.L2439
	cmpq	$0, -496(%rbp)
	jne	.L2440
.L2231:
	movq	-6520(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -6048(%rbp)
	movaps	%xmm0, -6064(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	$134744071, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -6064(%rbp)
	movq	%rdx, -6048(%rbp)
	movq	%rdx, -6056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6064(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2233
	call	_ZdlPv@PLT
.L2233:
	movq	(%rbx), %rax
	movq	%r13, %rdi
	movq	32(%rax), %rbx
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6240(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6256(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6480(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6232(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6432(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6472(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6448(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6296(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6464(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6288(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6400(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6352(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6384(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6440(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6512(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6416(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6320(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6496(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6304(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6336(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6368(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6328(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6504(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6312(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6488(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6344(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6272(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6528(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2441
	leaq	-40(%rbp), %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2412:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	-176(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-172(%rbp), %rdx
	movaps	%xmm0, -6064(%rbp)
	movl	$134744071, -176(%rbp)
	movq	$0, -6048(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6528(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6064(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L2148
	call	_ZdlPv@PLT
.L2148:
	movq	(%r12), %rax
	movl	$92, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r12
	movq	%rcx, -6256(%rbp)
	movq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, -6240(%rbp)
	movq	%rax, -6288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6240(%rbp), %rsi
	movq	-6232(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal13Cast5ATSmi_83EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %rdx
	movq	%r13, %rsi
	movq	-6240(%rbp), %rcx
	movq	%r12, %xmm4
	leaq	-6096(%rbp), %r12
	movq	%rax, -136(%rbp)
	movhps	-6256(%rbp), %xmm4
	movq	%r12, %rdi
	movaps	%xmm0, -6096(%rbp)
	movq	%rcx, %xmm7
	movq	%rcx, -144(%rbp)
	movhps	-6288(%rbp), %xmm7
	movaps	%xmm4, -6256(%rbp)
	movaps	%xmm7, -6288(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm7, -160(%rbp)
	movq	$0, -6080(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-5552(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -6344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6096(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2149
	call	_ZdlPv@PLT
.L2149:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-5744(%rbp), %rax
	cmpq	$0, -6056(%rbp)
	movq	%rax, -6272(%rbp)
	jne	.L2442
.L2150:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L2147
	.p2align 4,,10
	.p2align 3
.L2417:
	leaq	-4920(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$578719175091423239, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -6048(%rbp)
	movaps	%xmm0, -6064(%rbp)
	call	_Znwm@PLT
	movq	-6504(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -6064(%rbp)
	movq	%rdx, -6048(%rbp)
	movq	%rdx, -6056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6064(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2167
	call	_ZdlPv@PLT
.L2167:
	movq	(%rbx), %rax
	movl	$56, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movq	48(%rax), %rdx
	movdqu	(%rax), %xmm7
	movq	$0, -6048(%rbp)
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -128(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -6064(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm7
	movq	-128(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-160(%rbp), %xmm6
	movq	-6304(%rbp), %rdi
	leaq	56(%rax), %rdx
	movq	%rax, -6064(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-144(%rbp), %xmm7
	movq	%rcx, 48(%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -6048(%rbp)
	movq	%rdx, -6056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2168
	call	_ZdlPv@PLT
.L2168:
	leaq	-4152(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2166
	.p2align 4,,10
	.p2align 3
.L2416:
	leaq	-5112(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-176(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$2054, %edi
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movw	%di, -172(%rbp)
	movq	%r14, %rdi
	leaq	-169(%rbp), %rdx
	movaps	%xmm0, -6064(%rbp)
	movl	$134744071, -176(%rbp)
	movb	$6, -170(%rbp)
	movq	$0, -6048(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6312(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6064(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2162
	call	_ZdlPv@PLT
.L2162:
	movq	(%rbx), %rax
	movl	$96, %edx
	movq	%r15, %rdi
	movq	24(%rax), %rsi
	movq	8(%rax), %r12
	movq	16(%rax), %rcx
	movq	(%rax), %rbx
	movq	%rsi, -6288(%rbp)
	movq	32(%rax), %rsi
	movq	48(%rax), %rax
	movq	%rcx, -6256(%rbp)
	movq	%rsi, -6368(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rax, -6416(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$97, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	-6232(%rbp), %rdi
	call	_ZN2v88internal31Cast20ATFastJSArrayForCopy_1435EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	pxor	%xmm0, %xmm0
	leaq	-104(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %xmm6
	movq	%r12, %xmm7
	movq	%rax, -112(%rbp)
	movq	-6368(%rbp), %xmm4
	leaq	-6096(%rbp), %r12
	movq	%rbx, %xmm5
	punpcklqdq	%xmm7, %xmm7
	movq	-6256(%rbp), %xmm3
	movhps	-6416(%rbp), %xmm4
	punpcklqdq	%xmm6, %xmm5
	movq	%r12, %rdi
	movaps	%xmm7, -6432(%rbp)
	movhps	-6288(%rbp), %xmm3
	movaps	%xmm4, -6368(%rbp)
	movaps	%xmm3, -6256(%rbp)
	movaps	%xmm5, -6288(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm0, -6096(%rbp)
	movq	$0, -6080(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6328(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6096(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2163
	call	_ZdlPv@PLT
.L2163:
	leaq	-4728(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -6056(%rbp)
	jne	.L2443
.L2164:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L2161
	.p2align 4,,10
	.p2align 3
.L2415:
	leaq	-5304(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$2054, %r8d
	pxor	%xmm0, %xmm0
	leaq	-176(%rbp), %rsi
	leaq	-170(%rbp), %rdx
	movq	%r14, %rdi
	movw	%r8w, -172(%rbp)
	movaps	%xmm0, -6064(%rbp)
	movl	$134744071, -176(%rbp)
	movq	$0, -6048(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6488(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2160
	call	_ZdlPv@PLT
.L2160:
	movq	-6240(%rbp), %rdi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJEE9AddInputsEv
	movq	-6264(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2159
	.p2align 4,,10
	.p2align 3
.L2414:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	leaq	-176(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	movl	$1544, %r9d
	leaq	-170(%rbp), %rdx
	movaps	%xmm0, -6064(%rbp)
	movw	%r9w, -172(%rbp)
	movl	$134744071, -176(%rbp)
	movq	$0, -6048(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6344(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6064(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2155
	call	_ZdlPv@PLT
.L2155:
	movq	(%rbx), %rax
	movl	$93, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	24(%rax), %r12
	movq	%rcx, -6288(%rbp)
	movq	16(%rax), %rcx
	movq	%rbx, -6256(%rbp)
	movq	40(%rax), %rbx
	movq	%rcx, -6368(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6232(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	call	_ZN2v88internal13Cast5ATSmi_83EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-120(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %xmm6
	movq	%rbx, %xmm3
	movq	%rax, -128(%rbp)
	movq	-6368(%rbp), %xmm5
	punpcklqdq	%xmm6, %xmm3
	leaq	-6096(%rbp), %r12
	movaps	%xmm0, -6096(%rbp)
	punpcklqdq	%xmm6, %xmm5
	movq	%r12, %rdi
	movq	-6256(%rbp), %xmm6
	movaps	%xmm3, -6416(%rbp)
	movaps	%xmm5, -6368(%rbp)
	movhps	-6288(%rbp), %xmm6
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm6, -6256(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm3, -144(%rbp)
	movq	$0, -6080(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6312(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6096(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2156
	call	_ZdlPv@PLT
.L2156:
	leaq	-5112(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -6056(%rbp)
	jne	.L2444
.L2157:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L2154
	.p2align 4,,10
	.p2align 3
.L2413:
	movq	-6368(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-176(%rbp), %rsi
	leaq	-171(%rbp), %rdx
	movq	%r14, %rdi
	movaps	%xmm0, -6064(%rbp)
	movl	$134744071, -176(%rbp)
	movb	$8, -172(%rbp)
	movq	$0, -6048(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6272(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2153
	call	_ZdlPv@PLT
.L2153:
	leaq	-560(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -6240(%rbp)
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJEE9AddInputsEv
	movq	-6264(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2152
	.p2align 4,,10
	.p2align 3
.L2418:
	leaq	-4728(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-176(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-167(%rbp), %rdx
	movaps	%xmm0, -6064(%rbp)
	movabsq	$578719175091423239, %rax
	movq	%rax, -176(%rbp)
	movb	$7, -168(%rbp)
	movq	$0, -6048(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6328(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6064(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2170
	call	_ZdlPv@PLT
.L2170:
	movq	(%rbx), %rax
	movl	$104, %edx
	movq	%r15, %rdi
	movq	24(%rax), %rsi
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	32(%rax), %r12
	movq	%rsi, -6440(%rbp)
	movq	48(%rax), %rsi
	movq	%rbx, -6368(%rbp)
	movq	40(%rax), %rbx
	movq	%rcx, -6416(%rbp)
	movq	16(%rax), %rcx
	movq	64(%rax), %rax
	movq	%rsi, -6624(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -6432(%rbp)
	movq	%rax, -6288(%rbp)
	movq	%rbx, -6256(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-6232(%rbp), %rbx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-6256(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler6SmiAddENS0_8compiler5TNodeINS0_3SmiEEES5_
	movq	%r14, %rdi
	movq	%rax, -6640(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rbx, -6232(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-6288(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler21LoadFastJSArrayLengthENS0_8compiler11SloppyTNodeINS0_7JSArrayEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-6232(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-6640(%rbp), %r8
	movq	%r14, %rdi
	movq	%rax, %rbx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler18UintPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %xmm7
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-112(%rbp), %r12
	movhps	-6256(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movq	-6624(%rbp), %xmm6
	movq	-6432(%rbp), %xmm4
	movq	%r12, %rdx
	movq	-6368(%rbp), %xmm3
	movaps	%xmm7, -6624(%rbp)
	movhps	-6288(%rbp), %xmm6
	movaps	%xmm7, -144(%rbp)
	movhps	-6440(%rbp), %xmm4
	movhps	-6416(%rbp), %xmm3
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm6, -6288(%rbp)
	movaps	%xmm4, -6432(%rbp)
	movaps	%xmm3, -6256(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm0, -6064(%rbp)
	movq	$0, -6048(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-4592(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -6368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2171
	call	_ZdlPv@PLT
.L2171:
	pxor	%xmm0, %xmm0
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movdqa	-6256(%rbp), %xmm6
	movdqa	-6432(%rbp), %xmm7
	movaps	%xmm0, -6064(%rbp)
	movdqa	-6624(%rbp), %xmm5
	movq	$0, -6048(%rbp)
	movaps	%xmm6, -176(%rbp)
	movdqa	-6288(%rbp), %xmm6
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6336(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2172
	call	_ZdlPv@PLT
.L2172:
	movq	-6384(%rbp), %rdx
	leaq	-4344(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -4528(%rbp)
	je	.L2173
.L2419:
	movq	-6384(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$506661581053495303, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -6048(%rbp)
	movaps	%xmm0, -6064(%rbp)
	call	_Znwm@PLT
	movq	-6368(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -6064(%rbp)
	movq	%rdx, -6048(%rbp)
	movq	%rdx, -6056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2174
	call	_ZdlPv@PLT
.L2174:
	movq	-6240(%rbp), %rdi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJEE9AddInputsEv
	movq	-6264(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2173
	.p2align 4,,10
	.p2align 3
.L2420:
	leaq	-4344(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$506661581053495303, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -6048(%rbp)
	movaps	%xmm0, -6064(%rbp)
	call	_Znwm@PLT
	movq	-6336(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -6064(%rbp)
	movq	%rdx, -6048(%rbp)
	movq	%rdx, -6056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6064(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2176
	call	_ZdlPv@PLT
.L2176:
	movq	(%rbx), %rax
	movl	$105, %edx
	movq	%r15, %rdi
	leaq	-6112(%rbp), %r13
	movq	24(%rax), %rsi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	40(%rax), %r12
	movq	%rsi, -6384(%rbp)
	movq	32(%rax), %rsi
	movq	%rcx, -6256(%rbp)
	movq	16(%rax), %rcx
	movq	56(%rax), %rax
	movq	%rsi, -6416(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -6288(%rbp)
	movq	%rax, -6432(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-6232(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$212, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-6064(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rsi
	movl	$3, %edi
	movq	%rbx, %r9
	pushq	%rdi
	movq	%rax, %r8
	movq	-6048(%rbp), %rax
	movl	$1, %ecx
	movq	%rsi, -6096(%rbp)
	leaq	-176(%rbp), %rsi
	leaq	-6096(%rbp), %rdx
	movq	%r13, %rdi
	movq	-6432(%rbp), %xmm0
	pushq	%rsi
	xorl	%esi, %esi
	movq	%r12, -160(%rbp)
	movhps	-6416(%rbp), %xmm0
	movq	%rax, -6088(%rbp)
	movaps	%xmm0, -176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm0
	movl	$40, %edi
	movq	%r12, -144(%rbp)
	movhps	-6256(%rbp), %xmm0
	movq	$0, -6048(%rbp)
	movaps	%xmm0, -176(%rbp)
	movq	-6288(%rbp), %xmm0
	movhps	-6384(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -6064(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movdqa	-176(%rbp), %xmm2
	movq	%r14, %rsi
	movdqa	-160(%rbp), %xmm7
	leaq	40(%rax), %rdx
	movq	%rax, -6064(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm7, 16(%rax)
	leaq	-752(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6048(%rbp)
	movq	%rdx, -6056(%rbp)
	movq	%rax, -6256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6064(%rbp), %rdi
	popq	%rcx
	popq	%rsi
	testq	%rdi, %rdi
	je	.L2177
	call	_ZdlPv@PLT
.L2177:
	movq	-6536(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -4144(%rbp)
	je	.L2178
.L2421:
	leaq	-4152(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -6160(%rbp)
	leaq	-6096(%rbp), %r12
	movq	$0, -6152(%rbp)
	leaq	-176(%rbp), %r13
	movq	$0, -6144(%rbp)
	movq	$0, -6136(%rbp)
	movq	$0, -6128(%rbp)
	movq	$0, -6120(%rbp)
	movq	$0, -6112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-6112(%rbp), %rax
	movq	-6304(%rbp), %rdi
	leaq	-6128(%rbp), %r9
	pushq	%rax
	leaq	-6120(%rbp), %rax
	leaq	-6144(%rbp), %rcx
	pushq	%rax
	leaq	-6136(%rbp), %r8
	leaq	-6152(%rbp), %rdx
	leaq	-6160(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS8_EESG_PNSA_IS6_EESI_SE_
	movl	$107, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6112(%rbp), %rdx
	movq	-6160(%rbp), %rsi
	movq	%r14, %rcx
	movq	-6232(%rbp), %rdi
	call	_ZN2v88internal38Cast27JSArgumentsObjectWithLength_1436EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-6160(%rbp), %rdx
	movq	%rax, %r8
	movq	-6112(%rbp), %rax
	movaps	%xmm0, -6096(%rbp)
	movq	%r8, -112(%rbp)
	movq	%rdx, -176(%rbp)
	movq	-6152(%rbp), %rdx
	movq	%rax, -128(%rbp)
	movq	%rdx, -168(%rbp)
	movq	-6144(%rbp), %rdx
	movq	%rax, -120(%rbp)
	movq	%rdx, -160(%rbp)
	movq	-6136(%rbp), %rdx
	movq	$0, -6080(%rbp)
	movq	%rdx, -152(%rbp)
	movq	-6128(%rbp), %rdx
	movq	%rdx, -144(%rbp)
	movq	-6120(%rbp), %rdx
	movq	%rdx, -136(%rbp)
	leaq	-104(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6320(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6096(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L2179
	call	_ZdlPv@PLT
.L2179:
	leaq	-3768(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -6056(%rbp)
	jne	.L2445
.L2180:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L2178
	.p2align 4,,10
	.p2align 3
.L2422:
	leaq	-3960(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$578719175091423239, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -6048(%rbp)
	movaps	%xmm0, -6064(%rbp)
	call	_Znwm@PLT
	movq	-6496(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -6064(%rbp)
	movq	%rdx, -6048(%rbp)
	movq	%rdx, -6056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6064(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2183
	call	_ZdlPv@PLT
.L2183:
	movq	(%rbx), %rax
	movl	$56, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movq	48(%rax), %rdx
	movdqu	(%rax), %xmm5
	movq	$0, -6048(%rbp)
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -128(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -6064(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm6
	movdqa	-160(%rbp), %xmm2
	movdqa	-144(%rbp), %xmm7
	leaq	56(%rax), %rdx
	movq	%rax, -6064(%rbp)
	movq	%rcx, 48(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm7, 32(%rax)
	leaq	-1328(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6048(%rbp)
	movq	%rdx, -6056(%rbp)
	movq	%rax, -6432(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2184
	call	_ZdlPv@PLT
.L2184:
	movq	-6568(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2182
	.p2align 4,,10
	.p2align 3
.L2424:
	movq	-6352(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -6176(%rbp)
	leaq	-6096(%rbp), %r12
	movq	$0, -6168(%rbp)
	leaq	-176(%rbp), %r13
	movq	$0, -6160(%rbp)
	movq	$0, -6152(%rbp)
	movq	$0, -6144(%rbp)
	movq	$0, -6136(%rbp)
	movq	$0, -6128(%rbp)
	movq	$0, -6120(%rbp)
	movq	$0, -6112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-6112(%rbp), %rax
	movq	-6416(%rbp), %rdi
	leaq	-6144(%rbp), %r9
	pushq	%rax
	leaq	-6120(%rbp), %rax
	leaq	-6160(%rbp), %rcx
	pushq	%rax
	leaq	-6128(%rbp), %rax
	leaq	-6152(%rbp), %r8
	pushq	%rax
	leaq	-6136(%rbp), %rax
	leaq	-6168(%rbp), %rdx
	pushq	%rax
	leaq	-6176(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_NS0_27JSArgumentsObjectWithLengthENS0_3MapEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS8_EESI_PNSC_IS6_EESK_SG_PNSC_IS9_EEPNSC_ISA_EE
	addq	$32, %rsp
	movl	$110, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6136(%rbp), %r8
	movq	%r14, %r9
	movq	-6144(%rbp), %rcx
	movq	-6120(%rbp), %rdx
	movq	-6176(%rbp), %rsi
	movq	-6232(%rbp), %rdi
	call	_ZN2v88internal40HandleFastAliasedSloppyArgumentsSlice_39EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_27JSArgumentsObjectWithLengthEEENS4_INS0_3SmiEEESA_PNS1_18CodeAssemblerLabelE
	leaq	-64(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-6176(%rbp), %xmm0
	movq	%rax, %xmm7
	movq	-6136(%rbp), %xmm4
	movq	$0, -6080(%rbp)
	movq	-6112(%rbp), %xmm1
	movq	-6128(%rbp), %xmm2
	movhps	-6168(%rbp), %xmm0
	punpcklqdq	%xmm7, %xmm4
	movq	-6160(%rbp), %xmm3
	movaps	%xmm0, -176(%rbp)
	movhps	-6176(%rbp), %xmm1
	movq	-6144(%rbp), %xmm0
	movhps	-6120(%rbp), %xmm2
	movhps	-6152(%rbp), %xmm3
	movaps	%xmm2, -128(%rbp)
	movhps	-6136(%rbp), %xmm0
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	-6120(%rbp), %xmm0
	movaps	%xmm1, -112(%rbp)
	movhps	-6144(%rbp), %xmm0
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -6096(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6440(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6096(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2190
	call	_ZdlPv@PLT
.L2190:
	movq	-6400(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -6056(%rbp)
	jne	.L2446
.L2191:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -3376(%rbp)
	je	.L2193
.L2425:
	leaq	-3384(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$506661581053495303, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$13, %edi
	movq	$0, -6048(%rbp)
	movaps	%xmm0, -6064(%rbp)
	call	_Znwm@PLT
	movq	-6512(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	13(%rax), %rdx
	movl	$101123847, 8(%rax)
	movb	$6, 12(%rax)
	movq	%rax, -6064(%rbp)
	movq	%rdx, -6048(%rbp)
	movq	%rdx, -6056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2194
	call	_ZdlPv@PLT
.L2194:
	movq	-6240(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	$0, -6048(%rbp)
	movaps	%xmm0, -6064(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2195
	call	_ZdlPv@PLT
.L2195:
	movq	-6264(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3184(%rbp)
	je	.L2196
.L2426:
	movq	-6400(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -6208(%rbp)
	movq	$0, -6200(%rbp)
	movq	$0, -6192(%rbp)
	movq	$0, -6184(%rbp)
	movq	$0, -6176(%rbp)
	movq	$0, -6168(%rbp)
	movq	$0, -6160(%rbp)
	movq	$0, -6152(%rbp)
	movq	$0, -6144(%rbp)
	movq	$0, -6136(%rbp)
	movq	$0, -6128(%rbp)
	movq	$0, -6120(%rbp)
	movq	$0, -6112(%rbp)
	movq	$0, -6096(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-6096(%rbp), %rax
	movq	-6440(%rbp), %rdi
	pushq	%rax
	leaq	-6112(%rbp), %rax
	leaq	-6192(%rbp), %rcx
	pushq	%rax
	leaq	-6120(%rbp), %rax
	leaq	-6200(%rbp), %rdx
	pushq	%rax
	leaq	-6128(%rbp), %rax
	leaq	-6208(%rbp), %rsi
	pushq	%rax
	leaq	-6136(%rbp), %rax
	leaq	-6176(%rbp), %r9
	pushq	%rax
	leaq	-6144(%rbp), %rax
	leaq	-6184(%rbp), %r8
	pushq	%rax
	leaq	-6152(%rbp), %rax
	pushq	%rax
	leaq	-6160(%rbp), %rax
	pushq	%rax
	leaq	-6168(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_NS0_27JSArgumentsObjectWithLengthENS0_3MapES3_S9_S6_S6_NS0_7JSArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS8_EESJ_PNSD_IS6_EESL_SH_PNSD_IS9_EEPNSD_ISA_EESF_SN_SL_SL_PNSD_ISB_EE
	movq	-6208(%rbp), %rax
	pxor	%xmm0, %xmm0
	addq	$80, %rsp
	movl	$40, %edi
	movaps	%xmm0, -6064(%rbp)
	movq	%rax, -176(%rbp)
	movq	-6200(%rbp), %rax
	movq	$0, -6048(%rbp)
	movq	%rax, -168(%rbp)
	movq	-6192(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-6184(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-6096(%rbp), %rax
	movq	%rax, -144(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm5
	movdqa	-160(%rbp), %xmm6
	movq	-6256(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -6064(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -6048(%rbp)
	movq	%rdx, -6056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2197
	call	_ZdlPv@PLT
.L2197:
	movq	-6536(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2196
	.p2align 4,,10
	.p2align 3
.L2423:
	leaq	-3768(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-176(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-167(%rbp), %rdx
	movaps	%xmm0, -6064(%rbp)
	movabsq	$578719175091423239, %rax
	movq	%rax, -176(%rbp)
	movb	$7, -168(%rbp)
	movq	$0, -6048(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6320(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6064(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2186
	call	_ZdlPv@PLT
.L2186:
	movq	(%rbx), %rax
	movl	$108, %edx
	movq	%r15, %rdi
	movq	40(%rax), %rsi
	movq	8(%rax), %rbx
	movq	24(%rax), %rcx
	movq	(%rax), %r12
	movq	%rsi, -6608(%rbp)
	movq	48(%rax), %rsi
	movq	%rbx, -6384(%rbp)
	movq	16(%rax), %rbx
	movq	%rcx, -6624(%rbp)
	movq	32(%rax), %rcx
	movq	64(%rax), %rax
	movq	%rsi, -6648(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -6640(%rbp)
	movq	%rbx, -6416(%rbp)
	movq	%rax, -6288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-6232(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-6288(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE(%rip), %esi
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$109, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-6232(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler25IsFastAliasedArgumentsMapENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_3MapEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -6440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %xmm7
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-104(%rbp), %r12
	movhps	-6384(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movq	%rbx, -112(%rbp)
	movq	-6640(%rbp), %xmm6
	movq	%r12, %rdx
	movq	-6648(%rbp), %xmm5
	movaps	%xmm7, -6384(%rbp)
	movq	-6416(%rbp), %xmm2
	movaps	%xmm7, -176(%rbp)
	movhps	-6288(%rbp), %xmm5
	movhps	-6608(%rbp), %xmm6
	movaps	%xmm0, -6064(%rbp)
	movhps	-6624(%rbp), %xmm2
	movaps	%xmm5, -6288(%rbp)
	movaps	%xmm6, -6640(%rbp)
	movaps	%xmm2, -6624(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movq	$0, -6048(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3632(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -6416(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2187
	call	_ZdlPv@PLT
.L2187:
	movdqa	-6384(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movq	%r12, %rdx
	movdqa	-6624(%rbp), %xmm6
	movdqa	-6640(%rbp), %xmm1
	movq	%r13, %rsi
	movq	%r14, %rdi
	movaps	%xmm0, -6064(%rbp)
	movdqa	-6288(%rbp), %xmm2
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movq	%rbx, -112(%rbp)
	movq	$0, -6048(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3056(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -6384(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2188
	call	_ZdlPv@PLT
.L2188:
	movq	-6464(%rbp), %rcx
	movq	-6352(%rbp), %rdx
	movq	%r15, %rdi
	movq	-6440(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L2185
	.p2align 4,,10
	.p2align 3
.L2428:
	movq	-6448(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$506661581053495303, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -6048(%rbp)
	movaps	%xmm0, -6064(%rbp)
	call	_Znwm@PLT
	movq	-6352(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	movl	$1031, %ebx
	leaq	10(%rax), %rdx
	movw	%bx, 8(%rax)
	movq	%rax, -6064(%rbp)
	movq	%rdx, -6048(%rbp)
	movq	%rdx, -6056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6064(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2202
	call	_ZdlPv@PLT
.L2202:
	movq	(%rbx), %rax
	movq	-6232(%rbp), %rdi
	movq	24(%rax), %rsi
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	32(%rax), %r13
	movq	%rsi, -6624(%rbp)
	movq	40(%rax), %rsi
	movq	48(%rax), %r12
	movq	%rbx, -6288(%rbp)
	movq	%rsi, -6640(%rbp)
	movq	56(%rax), %rsi
	movq	64(%rax), %rbx
	movq	%rcx, -6464(%rbp)
	movq	16(%rax), %rcx
	movq	72(%rax), %rax
	movq	%rsi, -6608(%rbp)
	movl	$1, %esi
	movq	%rcx, -6448(%rbp)
	movq	%rax, -6648(%rbp)
	call	_ZN2v88internal42FromConstexpr6ATbool16ATconstexpr_bool_165EPNS0_8compiler18CodeAssemblerStateEb@PLT
	leaq	-176(%rbp), %rsi
	leaq	-88(%rbp), %rdx
	movq	%r14, %rdi
	movq	-6288(%rbp), %xmm0
	movq	%rax, -96(%rbp)
	movq	$0, -6048(%rbp)
	movhps	-6464(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-6448(%rbp), %xmm0
	movhps	-6624(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%r13, %xmm0
	movhps	-6640(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%r12, %xmm0
	movhps	-6608(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%rbx, %xmm0
	movhps	-6648(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -6064(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2480(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -6288(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2203
	call	_ZdlPv@PLT
.L2203:
	movq	-6296(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2608(%rbp)
	je	.L2204
.L2429:
	movq	-6472(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$506661581053495303, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -6048(%rbp)
	movaps	%xmm0, -6064(%rbp)
	call	_Znwm@PLT
	movl	$1031, %r11d
	movq	-6400(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	10(%rax), %rdx
	movw	%r11w, 8(%rax)
	movq	%rax, -6064(%rbp)
	movq	%rdx, -6048(%rbp)
	movq	%rdx, -6056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6064(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2205
	call	_ZdlPv@PLT
.L2205:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	8(%rax), %rbx
	movq	40(%rax), %rsi
	movq	24(%rax), %rcx
	movq	(%rax), %r12
	movq	%rbx, -6464(%rbp)
	movq	16(%rax), %rbx
	movq	%rsi, -6640(%rbp)
	movq	48(%rax), %rsi
	movq	56(%rax), %rdx
	movq	%rbx, -6448(%rbp)
	movq	%rcx, -6472(%rbp)
	movq	64(%rax), %rbx
	movq	32(%rax), %rcx
	movq	%rsi, -6608(%rbp)
	movq	72(%rax), %rax
	movq	-6232(%rbp), %rsi
	movq	%rdx, -6648(%rbp)
	movq	%rcx, -6624(%rbp)
	movq	%rax, -6656(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler20IsSloppyArgumentsMapENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_3MapEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-88(%rbp), %rdx
	movq	%r14, %rdi
	movq	%r13, -96(%rbp)
	movq	%r12, %xmm0
	leaq	-176(%rbp), %rsi
	movq	$0, -6048(%rbp)
	movhps	-6464(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-6448(%rbp), %xmm0
	movhps	-6472(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-6624(%rbp), %xmm0
	movhps	-6640(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-6608(%rbp), %xmm0
	movhps	-6648(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%rbx, %xmm0
	movhps	-6656(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -6064(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6288(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2206
	call	_ZdlPv@PLT
.L2206:
	movq	-6296(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2204
	.p2align 4,,10
	.p2align 3
.L2427:
	movq	-6464(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -6168(%rbp)
	leaq	-176(%rbp), %r13
	movq	$0, -6160(%rbp)
	leaq	-96(%rbp), %rbx
	movq	$0, -6152(%rbp)
	movq	$0, -6144(%rbp)
	movq	$0, -6136(%rbp)
	movq	$0, -6128(%rbp)
	movq	$0, -6120(%rbp)
	movq	$0, -6112(%rbp)
	movq	$0, -6096(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-6096(%rbp), %rax
	movq	-6384(%rbp), %rdi
	leaq	-6152(%rbp), %rcx
	pushq	%rax
	leaq	-6112(%rbp), %rax
	leaq	-6136(%rbp), %r9
	pushq	%rax
	leaq	-6120(%rbp), %rax
	leaq	-6144(%rbp), %r8
	pushq	%rax
	leaq	-6128(%rbp), %rax
	leaq	-6160(%rbp), %rdx
	pushq	%rax
	leaq	-6168(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_NS0_27JSArgumentsObjectWithLengthENS0_3MapEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS8_EESI_PNSC_IS6_EESK_SG_PNSC_IS9_EEPNSC_ISA_EE
	addq	$32, %rsp
	movl	$112, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-6232(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-6096(%rbp), %rdx
	movq	-6168(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler20IsStrictArgumentsMapENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_3MapEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	-6120(%rbp), %xmm1
	movq	%r14, %rdi
	movq	-6136(%rbp), %xmm4
	movaps	%xmm0, -6064(%rbp)
	movq	-6152(%rbp), %xmm3
	movq	-6096(%rbp), %rax
	movq	%r12, -104(%rbp)
	movq	-6168(%rbp), %xmm5
	movhps	-6112(%rbp), %xmm1
	movhps	-6128(%rbp), %xmm4
	movq	$0, -6048(%rbp)
	movhps	-6144(%rbp), %xmm3
	movq	%rax, -6288(%rbp)
	movhps	-6160(%rbp), %xmm5
	movq	%rax, -112(%rbp)
	movaps	%xmm1, -6464(%rbp)
	movaps	%xmm4, -6624(%rbp)
	movaps	%xmm3, -6400(%rbp)
	movaps	%xmm5, -6640(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6352(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2199
	call	_ZdlPv@PLT
.L2199:
	movq	-6288(%rbp), %xmm0
	movq	%r12, %xmm5
	movdqa	-6640(%rbp), %xmm1
	movq	%rbx, %rdx
	movdqa	-6400(%rbp), %xmm2
	movdqa	-6624(%rbp), %xmm4
	movq	%r13, %rsi
	movq	%r14, %rdi
	punpcklqdq	%xmm5, %xmm0
	movdqa	-6464(%rbp), %xmm7
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm0, -6064(%rbp)
	movq	$0, -6048(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2672(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -6400(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2200
	call	_ZdlPv@PLT
.L2200:
	movq	-6472(%rbp), %rcx
	movq	-6448(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L2198
	.p2align 4,,10
	.p2align 3
.L2432:
	movq	-6592(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$506661581053495303, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$13, %edi
	movq	$0, -6048(%rbp)
	movaps	%xmm0, -6064(%rbp)
	call	_Znwm@PLT
	movq	-6296(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	13(%rax), %rdx
	movl	$101123847, 8(%rax)
	movb	$6, 12(%rax)
	movq	%rax, -6064(%rbp)
	movq	%rdx, -6048(%rbp)
	movq	%rdx, -6056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2216
	call	_ZdlPv@PLT
.L2216:
	movq	-6240(%rbp), %rdi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJEE9AddInputsEv
	movq	-6264(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1840(%rbp)
	je	.L2217
.L2433:
	movq	-6552(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -6208(%rbp)
	movq	$0, -6200(%rbp)
	movq	$0, -6192(%rbp)
	movq	$0, -6184(%rbp)
	movq	$0, -6176(%rbp)
	movq	$0, -6168(%rbp)
	movq	$0, -6160(%rbp)
	movq	$0, -6152(%rbp)
	movq	$0, -6144(%rbp)
	movq	$0, -6136(%rbp)
	movq	$0, -6128(%rbp)
	movq	$0, -6120(%rbp)
	movq	$0, -6112(%rbp)
	movq	$0, -6096(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-6096(%rbp), %rax
	movq	-6448(%rbp), %rdi
	pushq	%rax
	leaq	-6112(%rbp), %rax
	leaq	-6192(%rbp), %rcx
	pushq	%rax
	leaq	-6120(%rbp), %rax
	leaq	-6200(%rbp), %rdx
	pushq	%rax
	leaq	-6128(%rbp), %rax
	leaq	-6208(%rbp), %rsi
	pushq	%rax
	leaq	-6136(%rbp), %rax
	leaq	-6176(%rbp), %r9
	pushq	%rax
	leaq	-6144(%rbp), %rax
	leaq	-6184(%rbp), %r8
	pushq	%rax
	leaq	-6152(%rbp), %rax
	pushq	%rax
	leaq	-6160(%rbp), %rax
	pushq	%rax
	leaq	-6168(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_NS0_27JSArgumentsObjectWithLengthENS0_3MapES3_S9_S6_S6_NS0_7JSArrayEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSD_IS4_EEPNSD_IS8_EESJ_PNSD_IS6_EESL_SH_PNSD_IS9_EEPNSD_ISA_EESF_SN_SL_SL_PNSD_ISB_EE
	movq	-6208(%rbp), %rax
	pxor	%xmm0, %xmm0
	addq	$80, %rsp
	movl	$40, %edi
	movaps	%xmm0, -6064(%rbp)
	movq	%rax, -176(%rbp)
	movq	-6200(%rbp), %rax
	movq	$0, -6048(%rbp)
	movq	%rax, -168(%rbp)
	movq	-6192(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-6184(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-6096(%rbp), %rax
	movq	%rax, -144(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm1
	movdqa	-160(%rbp), %xmm2
	movq	-6256(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -6064(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm1, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -6048(%rbp)
	movq	%rdx, -6056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2218
	call	_ZdlPv@PLT
.L2218:
	movq	-6536(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2217
	.p2align 4,,10
	.p2align 3
.L2431:
	movq	-6480(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -6176(%rbp)
	leaq	-6096(%rbp), %r12
	movq	$0, -6168(%rbp)
	leaq	-176(%rbp), %r13
	movq	$0, -6160(%rbp)
	movq	$0, -6152(%rbp)
	movq	$0, -6144(%rbp)
	movq	$0, -6136(%rbp)
	movq	$0, -6128(%rbp)
	movq	$0, -6120(%rbp)
	movq	$0, -6112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-6112(%rbp), %rax
	movq	-6464(%rbp), %rdi
	leaq	-6144(%rbp), %r9
	pushq	%rax
	leaq	-6120(%rbp), %rax
	leaq	-6160(%rbp), %rcx
	pushq	%rax
	leaq	-6128(%rbp), %rax
	leaq	-6152(%rbp), %r8
	pushq	%rax
	leaq	-6136(%rbp), %rax
	leaq	-6168(%rbp), %rdx
	pushq	%rax
	leaq	-6176(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_NS0_27JSArgumentsObjectWithLengthENS0_3MapEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS8_EESI_PNSC_IS6_EESK_SG_PNSC_IS9_EEPNSC_ISA_EE
	addq	$32, %rsp
	movl	$113, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6136(%rbp), %r8
	movq	%r14, %r9
	movq	-6144(%rbp), %rcx
	movq	-6120(%rbp), %rdx
	movq	-6176(%rbp), %rsi
	movq	-6232(%rbp), %rdi
	call	_ZN2v88internal29HandleSimpleArgumentsSlice_38EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_27JSArgumentsObjectWithLengthEEENS4_INS0_3SmiEEESA_PNS1_18CodeAssemblerLabelE
	leaq	-64(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-6176(%rbp), %xmm0
	movq	%rax, %xmm6
	movq	-6136(%rbp), %xmm4
	movq	$0, -6080(%rbp)
	movq	-6112(%rbp), %xmm1
	movq	-6128(%rbp), %xmm2
	movhps	-6168(%rbp), %xmm0
	punpcklqdq	%xmm6, %xmm4
	movq	-6160(%rbp), %xmm3
	movaps	%xmm0, -176(%rbp)
	movhps	-6176(%rbp), %xmm1
	movq	-6144(%rbp), %xmm0
	movhps	-6120(%rbp), %xmm2
	movhps	-6152(%rbp), %xmm3
	movaps	%xmm2, -128(%rbp)
	movhps	-6136(%rbp), %xmm0
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	-6120(%rbp), %xmm0
	movaps	%xmm1, -112(%rbp)
	movhps	-6144(%rbp), %xmm0
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -6096(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6448(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6096(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2212
	call	_ZdlPv@PLT
.L2212:
	movq	-6552(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2096(%rbp), %rax
	cmpq	$0, -6056(%rbp)
	movq	%rax, -6296(%rbp)
	jne	.L2447
.L2213:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L2211
	.p2align 4,,10
	.p2align 3
.L2430:
	movq	-6296(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-176(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	movabsq	$506661581053495303, %rax
	movl	$1031, %r10d
	leaq	-165(%rbp), %rdx
	movaps	%xmm0, -6064(%rbp)
	movq	%rax, -176(%rbp)
	movw	%r10w, -168(%rbp)
	movb	$4, -166(%rbp)
	movq	$0, -6048(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6288(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6064(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2208
	call	_ZdlPv@PLT
.L2208:
	movq	(%rbx), %rax
	leaq	-104(%rbp), %r12
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	(%rax), %rbx
	movdqu	48(%rax), %xmm0
	movdqu	(%rax), %xmm3
	movdqu	16(%rax), %xmm2
	movq	%rbx, -6296(%rbp)
	movq	8(%rax), %rbx
	movdqu	32(%rax), %xmm1
	movq	%rbx, -6448(%rbp)
	movq	16(%rax), %rbx
	movq	%rbx, -6472(%rbp)
	movq	24(%rax), %rbx
	movq	%rbx, -6624(%rbp)
	movq	32(%rax), %rbx
	movq	%rbx, -6640(%rbp)
	movq	40(%rax), %rbx
	movq	%rbx, -6608(%rbp)
	movq	48(%rax), %rbx
	movq	%rbx, -6648(%rbp)
	movq	56(%rax), %rbx
	movq	%rbx, -6656(%rbp)
	movq	64(%rax), %rbx
	movq	80(%rax), %rax
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -176(%rbp)
	movq	%rax, -6664(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -6064(%rbp)
	movq	%rbx, -112(%rbp)
	movq	$0, -6048(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2288(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -6464(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2209
	call	_ZdlPv@PLT
.L2209:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rbx, -112(%rbp)
	movq	-6296(%rbp), %xmm0
	movq	$0, -6048(%rbp)
	movhps	-6448(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-6472(%rbp), %xmm0
	movhps	-6624(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-6640(%rbp), %xmm0
	movhps	-6608(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-6648(%rbp), %xmm0
	movhps	-6656(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -6064(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1712(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -6472(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2210
	call	_ZdlPv@PLT
.L2210:
	movq	-6560(%rbp), %rcx
	movq	-6480(%rbp), %rdx
	movq	%r15, %rdi
	movq	-6664(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L2207
	.p2align 4,,10
	.p2align 3
.L2434:
	movq	-6560(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -6168(%rbp)
	movq	$0, -6160(%rbp)
	movq	$0, -6152(%rbp)
	movq	$0, -6144(%rbp)
	movq	$0, -6136(%rbp)
	movq	$0, -6128(%rbp)
	movq	$0, -6120(%rbp)
	movq	$0, -6112(%rbp)
	movq	$0, -6096(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-6096(%rbp), %rax
	movq	-6472(%rbp), %rdi
	leaq	-6152(%rbp), %rcx
	pushq	%rax
	leaq	-6112(%rbp), %rax
	leaq	-6136(%rbp), %r9
	pushq	%rax
	leaq	-6120(%rbp), %rax
	leaq	-6144(%rbp), %r8
	pushq	%rax
	leaq	-6128(%rbp), %rax
	leaq	-6160(%rbp), %rdx
	pushq	%rax
	leaq	-6168(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_NS0_27JSArgumentsObjectWithLengthENS0_3MapEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS8_EESI_PNSC_IS6_EESK_SG_PNSC_IS9_EEPNSC_ISA_EE
	addq	$32, %rsp
	movl	$109, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm0, %xmm0
	leaq	-104(%rbp), %rdx
	movq	%r14, %rdi
	movq	-6168(%rbp), %rax
	leaq	-176(%rbp), %rsi
	movaps	%xmm0, -6064(%rbp)
	movq	$0, -6048(%rbp)
	movq	%rax, -176(%rbp)
	movq	-6160(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-6152(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-6144(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-6136(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-6128(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-6120(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-6112(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-6096(%rbp), %rax
	movq	%rax, -112(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2220
	call	_ZdlPv@PLT
.L2220:
	movq	-6576(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2219
	.p2align 4,,10
	.p2align 3
.L2435:
	movq	-6576(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -6168(%rbp)
	movq	$0, -6160(%rbp)
	movq	$0, -6152(%rbp)
	movq	$0, -6144(%rbp)
	movq	$0, -6136(%rbp)
	movq	$0, -6128(%rbp)
	movq	$0, -6120(%rbp)
	movq	$0, -6112(%rbp)
	movq	$0, -6096(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-6096(%rbp), %rax
	movq	%r12, %rdi
	leaq	-6152(%rbp), %rcx
	pushq	%rax
	leaq	-6112(%rbp), %rax
	leaq	-6136(%rbp), %r9
	pushq	%rax
	leaq	-6120(%rbp), %rax
	leaq	-6144(%rbp), %r8
	pushq	%rax
	leaq	-6128(%rbp), %rax
	leaq	-6160(%rbp), %rdx
	pushq	%rax
	leaq	-6168(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_NS0_27JSArgumentsObjectWithLengthENS0_3MapEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS8_EESI_PNSC_IS6_EESK_SG_PNSC_IS9_EEPNSC_ISA_EE
	addq	$32, %rsp
	movl	$107, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-6168(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movaps	%xmm0, -6064(%rbp)
	movq	%rax, -176(%rbp)
	movq	-6160(%rbp), %rax
	movq	$0, -6048(%rbp)
	movq	%rax, -168(%rbp)
	movq	-6152(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-6144(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-6136(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-6128(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-6120(%rbp), %rax
	movq	%rax, -128(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm3
	movdqa	-160(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm7
	leaq	56(%rax), %rdx
	movq	%rax, -6064(%rbp)
	movq	%rcx, 48(%rax)
	movq	-6232(%rbp), %rdi
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -6048(%rbp)
	movq	%rdx, -6056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2222
	call	_ZdlPv@PLT
.L2222:
	movq	-6544(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1264(%rbp)
	je	.L2223
.L2436:
	movq	-6568(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -6152(%rbp)
	movq	$0, -6144(%rbp)
	movq	$0, -6136(%rbp)
	movq	$0, -6128(%rbp)
	movq	$0, -6120(%rbp)
	movq	$0, -6112(%rbp)
	movq	$0, -6096(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-6096(%rbp), %rax
	movq	-6432(%rbp), %rdi
	leaq	-6120(%rbp), %r9
	pushq	%rax
	leaq	-6112(%rbp), %rax
	leaq	-6128(%rbp), %r8
	pushq	%rax
	leaq	-6136(%rbp), %rcx
	leaq	-6144(%rbp), %rdx
	leaq	-6152(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS8_EESG_PNSA_IS6_EESI_SE_
	movl	$117, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$107, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-6152(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movaps	%xmm0, -6064(%rbp)
	movq	%rax, -176(%rbp)
	movq	-6144(%rbp), %rax
	movq	$0, -6048(%rbp)
	movq	%rax, -168(%rbp)
	movq	-6136(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-6128(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-6120(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-6112(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-6096(%rbp), %rax
	movq	%rax, -128(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm5
	movdqa	-160(%rbp), %xmm6
	movdqa	-144(%rbp), %xmm1
	leaq	56(%rax), %rdx
	movq	%rax, -6064(%rbp)
	movq	%rcx, 48(%rax)
	movq	-6232(%rbp), %rdi
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm1, 32(%rax)
	movq	%rdx, -6048(%rbp)
	movq	%rdx, -6056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6064(%rbp), %rdi
	popq	%r8
	popq	%r9
	testq	%rdi, %rdi
	je	.L2224
	call	_ZdlPv@PLT
.L2224:
	movq	-6544(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2223
	.p2align 4,,10
	.p2align 3
.L2437:
	movq	-6544(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -6152(%rbp)
	movq	$0, -6144(%rbp)
	movq	$0, -6136(%rbp)
	movq	$0, -6128(%rbp)
	movq	$0, -6120(%rbp)
	movq	$0, -6112(%rbp)
	movq	$0, -6096(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-6096(%rbp), %rax
	movq	-6232(%rbp), %rdi
	leaq	-6136(%rbp), %rcx
	pushq	%rax
	leaq	-6112(%rbp), %rax
	leaq	-6120(%rbp), %r9
	pushq	%rax
	leaq	-6128(%rbp), %r8
	leaq	-6144(%rbp), %rdx
	leaq	-6152(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS8_EESG_PNSA_IS6_EESI_SE_
	movl	$97, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-6152(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movaps	%xmm0, -6064(%rbp)
	movq	%rax, -176(%rbp)
	movq	-6144(%rbp), %rax
	movq	$0, -6048(%rbp)
	movq	%rax, -168(%rbp)
	movq	-6136(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-6128(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-6120(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-6112(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-6096(%rbp), %rax
	movq	%rax, -128(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm2
	movdqa	-160(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm4
	leaq	56(%rax), %rdx
	movq	%rax, -6064(%rbp)
	movq	%rcx, 48(%rax)
	movq	-6480(%rbp), %rdi
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm4, 32(%rax)
	movq	%rdx, -6048(%rbp)
	movq	%rdx, -6056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6064(%rbp), %rdi
	popq	%rcx
	popq	%rsi
	testq	%rdi, %rdi
	je	.L2226
	call	_ZdlPv@PLT
.L2226:
	movq	-6584(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -880(%rbp)
	je	.L2227
.L2438:
	movq	-6584(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -6144(%rbp)
	movq	$0, -6136(%rbp)
	movq	$0, -6128(%rbp)
	movq	$0, -6120(%rbp)
	movq	$0, -6112(%rbp)
	movq	$0, -6096(%rbp)
	movq	$0, -6064(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-6096(%rbp), %rax
	pushq	%r14
	movq	-6480(%rbp), %rdi
	pushq	%rax
	leaq	-6128(%rbp), %rcx
	leaq	-6112(%rbp), %r9
	leaq	-6120(%rbp), %r8
	leaq	-6136(%rbp), %rdx
	leaq	-6144(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_13NativeContextENS0_6ObjectENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES8_S6_S6_S4_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS8_EESG_PNSA_IS6_EESI_SE_
	movl	$96, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$120, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-6240(%rbp), %rdi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJEE9AddInputsEv
	movq	-6264(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	popq	%rax
	popq	%rdx
	jmp	.L2227
	.p2align 4,,10
	.p2align 3
.L2439:
	movq	-6536(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -6048(%rbp)
	movaps	%xmm0, -6064(%rbp)
	call	_Znwm@PLT
	movq	-6256(%rbp), %rdi
	movq	%r14, %rsi
	movl	$134744071, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -6064(%rbp)
	movq	%rdx, -6048(%rbp)
	movq	%rdx, -6056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6064(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2229
	call	_ZdlPv@PLT
.L2229:
	movq	(%rbx), %rax
	movl	$88, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	8(%rax), %rbx
	movq	(%rax), %r13
	movq	24(%rax), %rcx
	movq	%rbx, -6536(%rbp)
	movq	16(%rax), %rbx
	movq	%rcx, -6552(%rbp)
	movq	%rbx, -6544(%rbp)
	movq	32(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %xmm0
	movl	$40, %edi
	movq	$0, -6048(%rbp)
	movhps	-6536(%rbp), %xmm0
	movq	%rbx, -144(%rbp)
	leaq	-368(%rbp), %r13
	movaps	%xmm0, -176(%rbp)
	movq	-6544(%rbp), %xmm0
	movhps	-6552(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -6064(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r13, %rdi
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm5
	leaq	40(%rax), %rdx
	movq	%rax, -6064(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -6048(%rbp)
	movq	%rdx, -6056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2230
	call	_ZdlPv@PLT
.L2230:
	movq	-6520(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -496(%rbp)
	je	.L2231
.L2440:
	movq	-6264(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-6240(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	$0, -6048(%rbp)
	movaps	%xmm0, -6064(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-6064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2232
	call	_ZdlPv@PLT
.L2232:
	movq	-6600(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2231
	.p2align 4,,10
	.p2align 3
.L2442:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movdqa	-6256(%rbp), %xmm6
	movq	-6240(%rbp), %rax
	leaq	-136(%rbp), %rdx
	movaps	%xmm0, -6096(%rbp)
	movq	$0, -6080(%rbp)
	movaps	%xmm6, -176(%rbp)
	movdqa	-6288(%rbp), %xmm6
	movq	%rax, -144(%rbp)
	movaps	%xmm6, -160(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6272(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6096(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2151
	call	_ZdlPv@PLT
.L2151:
	movq	-6368(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2150
	.p2align 4,,10
	.p2align 3
.L2444:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %rdx
	movq	%r13, %rsi
	movdqa	-6256(%rbp), %xmm6
	movdqa	-6368(%rbp), %xmm3
	movq	%r12, %rdi
	movaps	%xmm0, -6096(%rbp)
	movdqa	-6416(%rbp), %xmm4
	movq	$0, -6080(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6488(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6096(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2158
	call	_ZdlPv@PLT
.L2158:
	leaq	-5304(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2157
	.p2align 4,,10
	.p2align 3
.L2443:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-6288(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-6256(%rbp), %xmm6
	movdqa	-6368(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	leaq	-112(%rbp), %rdx
	movq	%r12, %rdi
	movdqa	-6432(%rbp), %xmm3
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm0, -6096(%rbp)
	movq	$0, -6080(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6504(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6096(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2165
	call	_ZdlPv@PLT
.L2165:
	leaq	-4920(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2164
	.p2align 4,,10
	.p2align 3
.L2445:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-112(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-6128(%rbp), %xmm0
	movq	-6144(%rbp), %xmm1
	movq	$0, -6080(%rbp)
	movq	-6160(%rbp), %xmm2
	movhps	-6120(%rbp), %xmm0
	movhps	-6136(%rbp), %xmm1
	movaps	%xmm0, -144(%rbp)
	movhps	-6152(%rbp), %xmm2
	movq	-6112(%rbp), %xmm0
	movaps	%xmm2, -176(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -6096(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6496(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6096(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2181
	call	_ZdlPv@PLT
.L2181:
	leaq	-3960(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2180
	.p2align 4,,10
	.p2align 3
.L2446:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-6168(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	-6176(%rbp), %rsi
	movq	-6144(%rbp), %rdx
	movq	-6136(%rbp), %rax
	movaps	%xmm0, -6096(%rbp)
	movq	%rdi, -168(%rbp)
	movq	-6160(%rbp), %rdi
	movq	-6120(%rbp), %rcx
	movq	%rsi, -176(%rbp)
	movq	%rdi, -160(%rbp)
	movq	-6152(%rbp), %rdi
	movq	%rdx, -144(%rbp)
	movq	%rdi, -152(%rbp)
	movq	-6128(%rbp), %rdi
	movq	%rsi, -104(%rbp)
	movq	%r13, %rsi
	movq	%rdi, -128(%rbp)
	movq	-6112(%rbp), %rdi
	movq	%rdx, -88(%rbp)
	leaq	-72(%rbp), %rdx
	movq	%rdi, -112(%rbp)
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	movq	%rcx, -120(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -6080(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6512(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6096(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2192
	call	_ZdlPv@PLT
.L2192:
	leaq	-3384(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2191
	.p2align 4,,10
	.p2align 3
.L2447:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-6168(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	-6176(%rbp), %rsi
	movq	-6144(%rbp), %rdx
	movq	-6136(%rbp), %rax
	movaps	%xmm0, -6096(%rbp)
	movq	%rdi, -168(%rbp)
	movq	-6160(%rbp), %rdi
	movq	-6120(%rbp), %rcx
	movq	%rsi, -176(%rbp)
	movq	%rdi, -160(%rbp)
	movq	-6152(%rbp), %rdi
	movq	%rdx, -144(%rbp)
	movq	%rdi, -152(%rbp)
	movq	-6128(%rbp), %rdi
	movq	%rsi, -104(%rbp)
	movq	%r13, %rsi
	movq	%rdi, -128(%rbp)
	movq	-6112(%rbp), %rdi
	movq	%rdx, -88(%rbp)
	leaq	-72(%rbp), %rdx
	movq	%rdi, -112(%rbp)
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	movq	%rcx, -120(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -6080(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6296(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-6096(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2214
	call	_ZdlPv@PLT
.L2214:
	movq	-6592(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2213
.L2441:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22531:
	.size	_ZN2v88internal18HandleFastSlice_40EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_6ObjectEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEESD_PNS1_18CodeAssemblerLabelE, .-_ZN2v88internal18HandleFastSlice_40EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_6ObjectEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEESD_PNS1_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESL_SP_SP_SL_SP_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESL_SP_SP_SL_SP_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESL_SP_SP_SL_SP_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESL_SP_SP_SL_SP_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESL_SP_SP_SL_SP_:
.LFB27431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$12, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$120, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$578720283176011013, %rcx
	movq	%rcx, (%rax)
	leaq	12(%rax), %rdx
	movl	$134744072, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2449
	movq	%rax, -152(%rbp)
	call	_ZdlPv@PLT
	movq	-152(%rbp), %rax
.L2449:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2450
	movq	%rdx, (%r15)
.L2450:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2451
	movq	%rdx, (%r14)
.L2451:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2452
	movq	%rdx, 0(%r13)
.L2452:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2453
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L2453:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2454
	movq	%rdx, (%rbx)
.L2454:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2455
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2455:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2456
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2456:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2457
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2457:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2458
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2458:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2459
	movq	-128(%rbp), %rsi
	movq	%rdx, (%rsi)
.L2459:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2460
	movq	-136(%rbp), %rcx
	movq	%rdx, (%rcx)
.L2460:
	movq	88(%rax), %rax
	testq	%rax, %rax
	je	.L2448
	movq	-144(%rbp), %rbx
	movq	%rax, (%rbx)
.L2448:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2503
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2503:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27431:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESL_SP_SP_SL_SP_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESL_SP_SP_SL_SP_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_SB_SB_S7_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESL_SP_SP_SL_SP_SP_SP_SN_SP_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_SB_SB_S7_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESL_SP_SP_SL_SP_SP_SP_SN_SP_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_SB_SB_S7_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESL_SP_SP_SL_SP_SP_SP_SN_SP_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_SB_SB_S7_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESL_SP_SP_SL_SP_SP_SP_SN_SP_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_SB_SB_S7_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESL_SP_SP_SL_SP_SP_SP_SN_SP_:
.LFB27442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$16, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$152, %rsp
	movq	16(%rbp), %rax
	movq	%rdx, -88(%rbp)
	movq	88(%rbp), %r15
	movq	96(%rbp), %r14
	movq	%r8, -96(%rbp)
	movq	%rax, -112(%rbp)
	movq	24(%rbp), %rax
	movq	%r9, -104(%rbp)
	movq	%rax, -120(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movabsq	$578720283176011013, %rsi
	movabsq	$578439907727902728, %rdi
	movq	%rsi, (%rax)
	leaq	16(%rax), %rdx
	leaq	-80(%rbp), %rsi
	movq	%rdi, 8(%rax)
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2505
	movq	%rax, -184(%rbp)
	call	_ZdlPv@PLT
	movq	-184(%rbp), %rax
.L2505:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2506
	movq	%rdx, 0(%r13)
.L2506:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2507
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L2507:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2508
	movq	%rdx, (%rbx)
.L2508:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2509
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2509:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2510
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2510:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2511
	movq	-112(%rbp), %rcx
	movq	%rdx, (%rcx)
.L2511:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2512
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2512:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2513
	movq	-128(%rbp), %rcx
	movq	%rdx, (%rcx)
.L2513:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2514
	movq	-136(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2514:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2515
	movq	-144(%rbp), %rcx
	movq	%rdx, (%rcx)
.L2515:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2516
	movq	-152(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2516:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2517
	movq	-160(%rbp), %rsi
	movq	%rdx, (%rsi)
.L2517:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2518
	movq	-168(%rbp), %rcx
	movq	%rdx, (%rcx)
.L2518:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2519
	movq	-176(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2519:
	movq	112(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2520
	movq	%rdx, (%r15)
.L2520:
	movq	120(%rax), %rax
	testq	%rax, %rax
	je	.L2504
	movq	%rax, (%r14)
.L2504:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2575
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2575:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27442:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_SB_SB_S7_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESL_SP_SP_SL_SP_SP_SP_SN_SP_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_SB_SB_S7_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESL_SP_SP_SL_SP_SP_SP_SN_SP_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_SB_SB_S7_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EESG_PNSE_IS4_EEPNSE_IS5_EEPNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EESM_SQ_SQ_SM_SQ_SQ_SQ_SO_SQ_SQ_PNSE_ISC_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_SB_SB_S7_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EESG_PNSE_IS4_EEPNSE_IS5_EEPNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EESM_SQ_SQ_SM_SQ_SQ_SQ_SO_SQ_SQ_PNSE_ISC_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_SB_SB_S7_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EESG_PNSE_IS4_EEPNSE_IS5_EEPNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EESM_SQ_SQ_SM_SQ_SQ_SQ_SO_SQ_SQ_PNSE_ISC_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_SB_SB_S7_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EESG_PNSE_IS4_EEPNSE_IS5_EEPNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EESM_SQ_SQ_SM_SQ_SQ_SQ_SO_SQ_SQ_PNSE_ISC_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_SB_SB_S7_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EESG_PNSE_IS4_EEPNSE_IS5_EEPNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EESM_SQ_SQ_SM_SQ_SQ_SQ_SO_SQ_SQ_PNSE_ISC_EE:
.LFB27446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$18, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$168, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -88(%rbp)
	movq	%r9, -96(%rbp)
	movq	%rax, -104(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	88(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	96(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	104(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	112(%rbp), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$1800, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movdqa	.LC8(%rip), %xmm0
	movw	%cx, 16(%rax)
	leaq	18(%rax), %rdx
	movq	%rax, -80(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2577
	movq	%rax, -200(%rbp)
	call	_ZdlPv@PLT
	movq	-200(%rbp), %rax
.L2577:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2578
	movq	%rdx, (%r14)
.L2578:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2579
	movq	%rdx, 0(%r13)
.L2579:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2580
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L2580:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2581
	movq	%rdx, (%rbx)
.L2581:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2582
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2582:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2583
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2583:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2584
	movq	-112(%rbp), %rcx
	movq	%rdx, (%rcx)
.L2584:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2585
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2585:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2586
	movq	-128(%rbp), %rcx
	movq	%rdx, (%rcx)
.L2586:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2587
	movq	-136(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2587:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2588
	movq	-144(%rbp), %rsi
	movq	%rdx, (%rsi)
.L2588:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2589
	movq	-152(%rbp), %rcx
	movq	%rdx, (%rcx)
.L2589:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2590
	movq	-160(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2590:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2591
	movq	-168(%rbp), %rsi
	movq	%rdx, (%rsi)
.L2591:
	movq	112(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2592
	movq	-176(%rbp), %rdi
	movq	%rdx, (%rdi)
.L2592:
	movq	120(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2593
	movq	-184(%rbp), %rcx
	movq	%rdx, (%rcx)
.L2593:
	movq	128(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2594
	movq	-192(%rbp), %rbx
	movq	%rdx, (%rbx)
.L2594:
	movq	136(%rax), %rax
	testq	%rax, %rax
	je	.L2576
	movq	%rax, (%r15)
.L2576:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2655
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2655:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27446:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_SB_SB_S7_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EESG_PNSE_IS4_EEPNSE_IS5_EEPNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EESM_SQ_SQ_SM_SQ_SQ_SQ_SO_SQ_SQ_PNSE_ISC_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_SB_SB_S7_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EESG_PNSE_IS4_EEPNSE_IS5_EEPNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EESM_SQ_SQ_SM_SQ_SQ_SQ_SO_SQ_SQ_PNSE_ISC_EE
	.section	.text._ZN2v88internal28ArrayPrototypeSliceAssembler31GenerateArrayPrototypeSliceImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28ArrayPrototypeSliceAssembler31GenerateArrayPrototypeSliceImplEv
	.type	_ZN2v88internal28ArrayPrototypeSliceAssembler31GenerateArrayPrototypeSliceImplEv, @function
_ZN2v88internal28ArrayPrototypeSliceAssembler31GenerateArrayPrototypeSliceImplEv:
.LFB22588:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$2520, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$1, %esi
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %r12
	movq	%r13, -6056(%rbp)
	leaq	-6056(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r14, %rcx
	movq	%r12, %rsi
	leaq	-5888(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-5880(%rbp), %rbx
	movl	$2, %esi
	movq	%r12, %rdi
	movq	-5872(%rbp), %rcx
	movq	-5888(%rbp), %rax
	movq	%r12, -5760(%rbp)
	leaq	-5528(%rbp), %r12
	movq	%rbx, -6112(%rbp)
	movq	%rbx, -5736(%rbp)
	leaq	-5760(%rbp), %rbx
	movq	%rcx, -6096(%rbp)
	movq	%rcx, -5744(%rbp)
	movq	$1, -5752(%rbp)
	movq	%rax, -6128(%rbp)
	movq	%rax, -5728(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rbx, %rdi
	movq	%rbx, -6424(%rbp)
	leaq	-5336(%rbp), %rbx
	movq	%rax, -6080(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -5576(%rbp)
	movq	$0, -5568(%rbp)
	movq	%rax, %r14
	movq	-6056(%rbp), %rax
	movq	$0, -5560(%rbp)
	movq	%rax, -5584(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -5560(%rbp)
	movq	%rdx, -5568(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5544(%rbp)
	movq	%rax, -5576(%rbp)
	movq	$0, -5552(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$120, %edi
	movq	$0, -5384(%rbp)
	movq	$0, -5376(%rbp)
	movq	%rax, -5392(%rbp)
	movq	$0, -5368(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -5368(%rbp)
	movq	%rdx, -5376(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5352(%rbp)
	movq	%rax, -5384(%rbp)
	movq	$0, -5360(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$168, %edi
	movq	$0, -5192(%rbp)
	movq	$0, -5184(%rbp)
	movq	%rax, -5200(%rbp)
	movq	$0, -5176(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-5144(%rbp), %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -5176(%rbp)
	movq	%rdx, -5184(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -6264(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -5160(%rbp)
	movq	%rax, -5192(%rbp)
	movq	$0, -5168(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$192, %edi
	movq	$0, -5000(%rbp)
	movq	$0, -4992(%rbp)
	movq	%rax, -5008(%rbp)
	movq	$0, -4984(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-4952(%rbp), %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -4984(%rbp)
	movq	%rdx, -4992(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -6232(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -4968(%rbp)
	movq	%rax, -5000(%rbp)
	movq	$0, -4976(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$144, %edi
	movq	$0, -4808(%rbp)
	movq	$0, -4800(%rbp)
	movq	%rax, -4816(%rbp)
	movq	$0, -4792(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-4760(%rbp), %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rdx, -4792(%rbp)
	movq	%rdx, -4800(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -6256(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -4776(%rbp)
	movq	%rax, -4808(%rbp)
	movq	$0, -4784(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$144, %edi
	movq	$0, -4616(%rbp)
	movq	$0, -4608(%rbp)
	movq	%rax, -4624(%rbp)
	movq	$0, -4600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-4568(%rbp), %rsi
	xorl	%ecx, %ecx
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rdx, -4600(%rbp)
	movq	%rdx, -4608(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -6240(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -4584(%rbp)
	movq	%rax, -4616(%rbp)
	movq	$0, -4592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$120, %edi
	movq	$0, -4424(%rbp)
	movq	$0, -4416(%rbp)
	movq	%rax, -4432(%rbp)
	movq	$0, -4408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-4376(%rbp), %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -4408(%rbp)
	movq	%rdx, -4416(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -6144(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -4392(%rbp)
	movq	%rax, -4424(%rbp)
	movq	$0, -4400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$216, %edi
	movq	$0, -4232(%rbp)
	movq	$0, -4224(%rbp)
	movq	%rax, -4240(%rbp)
	movq	$0, -4216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-4184(%rbp), %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -4216(%rbp)
	movq	%rdx, -4224(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -6296(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -4200(%rbp)
	movq	%rax, -4232(%rbp)
	movq	$0, -4208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$216, %edi
	movq	$0, -4040(%rbp)
	movq	$0, -4032(%rbp)
	movq	%rax, -4048(%rbp)
	movq	$0, -4024(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-3992(%rbp), %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -4024(%rbp)
	movq	%rdx, -4032(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -6272(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -4008(%rbp)
	movq	%rax, -4040(%rbp)
	movq	$0, -4016(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$240, %edi
	movq	$0, -3848(%rbp)
	movq	$0, -3840(%rbp)
	movq	%rax, -3856(%rbp)
	movq	$0, -3832(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-3800(%rbp), %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rdx, -3832(%rbp)
	movq	%rdx, -3840(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -6288(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -3816(%rbp)
	movq	%rax, -3848(%rbp)
	movq	$0, -3824(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$240, %edi
	movq	$0, -3656(%rbp)
	movq	$0, -3648(%rbp)
	movq	%rax, -3664(%rbp)
	movq	$0, -3640(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-3608(%rbp), %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rdx, -3640(%rbp)
	movq	%rdx, -3648(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -6160(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -3624(%rbp)
	movq	%rax, -3656(%rbp)
	movq	$0, -3632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$264, %edi
	movq	$0, -3464(%rbp)
	movq	$0, -3456(%rbp)
	movq	%rax, -3472(%rbp)
	movq	$0, -3448(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-3416(%rbp), %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rdx, -3448(%rbp)
	movq	%rdx, -3456(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -6320(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -3432(%rbp)
	movq	%rax, -3464(%rbp)
	movq	$0, -3440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$264, %edi
	movq	$0, -3272(%rbp)
	movq	$0, -3264(%rbp)
	movq	%rax, -3280(%rbp)
	movq	$0, -3256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-3224(%rbp), %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rdx, -3256(%rbp)
	movq	%rdx, -3264(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -6336(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -3240(%rbp)
	movq	%rax, -3272(%rbp)
	movq	$0, -3248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$288, %edi
	movq	$0, -3080(%rbp)
	movq	$0, -3072(%rbp)
	movq	%rax, -3088(%rbp)
	movq	$0, -3064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-3032(%rbp), %rsi
	xorl	%ecx, %ecx
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rdx, -3064(%rbp)
	movq	%rdx, -3072(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -6304(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -3048(%rbp)
	movq	%rax, -3080(%rbp)
	movq	$0, -3056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$288, %edi
	movq	$0, -2888(%rbp)
	movq	$0, -2880(%rbp)
	movq	%rax, -2896(%rbp)
	movq	$0, -2872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-2840(%rbp), %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rdx, -2872(%rbp)
	movq	%rdx, -2880(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -6192(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -2856(%rbp)
	movq	%rax, -2888(%rbp)
	movq	$0, -2864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$288, %edi
	movq	$0, -2696(%rbp)
	movq	$0, -2688(%rbp)
	movq	%rax, -2704(%rbp)
	movq	$0, -2680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-2648(%rbp), %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rdx, -2680(%rbp)
	movq	%rdx, -2688(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -6344(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -2664(%rbp)
	movq	%rax, -2696(%rbp)
	movq	$0, -2672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$288, %edi
	movq	$0, -2504(%rbp)
	movq	$0, -2496(%rbp)
	movq	%rax, -2512(%rbp)
	movq	$0, -2488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-2456(%rbp), %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rdx, -2488(%rbp)
	movq	%rdx, -2496(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -6360(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -2472(%rbp)
	movq	%rax, -2504(%rbp)
	movq	$0, -2480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$312, %edi
	movq	$0, -2312(%rbp)
	movq	$0, -2304(%rbp)
	movq	%rax, -2320(%rbp)
	movq	$0, -2296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-2264(%rbp), %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rdx, -2296(%rbp)
	movq	%rdx, -2304(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -6384(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -2280(%rbp)
	movq	%rax, -2312(%rbp)
	movq	$0, -2288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$312, %edi
	movq	$0, -2120(%rbp)
	movq	$0, -2112(%rbp)
	movq	%rax, -2128(%rbp)
	movq	$0, -2104(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-2072(%rbp), %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rdx, -2104(%rbp)
	movq	%rdx, -2112(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -6352(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -2088(%rbp)
	movq	%rax, -2120(%rbp)
	movq	$0, -2096(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$432, %edi
	movq	$0, -1928(%rbp)
	movq	$0, -1920(%rbp)
	movq	%rax, -1936(%rbp)
	movq	$0, -1912(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-1880(%rbp), %rsi
	leaq	432(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, -1896(%rbp)
	movq	%rax, -1928(%rbp)
	movq	%rdx, -1912(%rbp)
	movq	%rdx, -1920(%rbp)
	xorl	%edx, %edx
	movq	$0, -1904(%rbp)
	movq	%rsi, -6496(%rbp)
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$456, %edi
	movq	$0, -1736(%rbp)
	movq	$0, -1728(%rbp)
	movq	%rax, -1744(%rbp)
	movq	$0, -1720(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-1688(%rbp), %rsi
	leaq	456(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movq	%rax, -1736(%rbp)
	movq	$0, 448(%rax)
	movq	%rdx, -1720(%rbp)
	movq	%rdx, -1728(%rbp)
	xorl	%edx, %edx
	movq	$0, -1712(%rbp)
	movq	%rsi, -6368(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -1704(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$336, %edi
	movq	$0, -1544(%rbp)
	movq	$0, -1536(%rbp)
	movq	%rax, -1552(%rbp)
	movq	$0, -1528(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1496(%rbp), %rsi
	xorl	%ecx, %ecx
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rdx, -1528(%rbp)
	movq	%rdx, -1536(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -6400(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -1512(%rbp)
	movq	%rax, -1544(%rbp)
	movq	$0, -1520(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$336, %edi
	movq	$0, -1352(%rbp)
	movq	$0, -1344(%rbp)
	movq	%rax, -1360(%rbp)
	movq	$0, -1336(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	-1304(%rbp), %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rdx, -1336(%rbp)
	movq	%rdx, -1344(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -6480(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -1320(%rbp)
	movq	%rax, -1352(%rbp)
	movq	$0, -1328(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$384, %edi
	movq	$0, -1160(%rbp)
	movq	$0, -1152(%rbp)
	movq	%rax, -1168(%rbp)
	movq	$0, -1144(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1112(%rbp), %rsi
	xorl	%ecx, %ecx
	leaq	384(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movq	%rdx, -1144(%rbp)
	movq	%rdx, -1152(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -6432(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -1128(%rbp)
	movq	%rax, -1160(%rbp)
	movq	$0, -1136(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$384, %edi
	movq	$0, -968(%rbp)
	movq	$0, -960(%rbp)
	movq	%rax, -976(%rbp)
	movq	$0, -952(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-920(%rbp), %rsi
	xorl	%ecx, %ecx
	leaq	384(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movq	%rdx, -952(%rbp)
	movq	%rdx, -960(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -6440(%rbp)
	movq	%r15, %rsi
	movups	%xmm0, -936(%rbp)
	movq	%rax, -968(%rbp)
	movq	$0, -944(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$432, %edi
	movq	$0, -776(%rbp)
	movq	$0, -768(%rbp)
	movq	%rax, -784(%rbp)
	movq	$0, -760(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-728(%rbp), %rsi
	xorl	%ecx, %ecx
	leaq	432(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, -744(%rbp)
	movq	%rax, -776(%rbp)
	movq	%rdx, -760(%rbp)
	movq	%rdx, -768(%rbp)
	xorl	%edx, %edx
	movq	$0, -752(%rbp)
	movq	%rsi, -6448(%rbp)
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$432, %edi
	movq	$0, -584(%rbp)
	movq	$0, -576(%rbp)
	movq	%rax, -592(%rbp)
	movq	$0, -568(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-536(%rbp), %rsi
	xorl	%ecx, %ecx
	leaq	432(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, -552(%rbp)
	movq	%rax, -584(%rbp)
	movq	%rdx, -568(%rbp)
	movq	%rdx, -576(%rbp)
	xorl	%edx, %edx
	movq	$0, -560(%rbp)
	movq	%rsi, -6416(%rbp)
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-6056(%rbp), %rax
	movl	$384, %edi
	movq	$0, -392(%rbp)
	movq	$0, -384(%rbp)
	movq	%rax, -400(%rbp)
	movq	$0, -376(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-344(%rbp), %rsi
	xorl	%ecx, %ecx
	leaq	384(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movq	%rdx, -376(%rbp)
	movq	%rdx, -384(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -6456(%rbp)
	movq	%r15, %rsi
	movq	%rax, -392(%rbp)
	movups	%xmm0, -360(%rbp)
	movq	$0, -368(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movq	-6128(%rbp), %xmm1
	movl	$40, %edi
	movq	%r14, -176(%rbp)
	leaq	-5712(%rbp), %r14
	movhps	-6112(%rbp), %xmm1
	movaps	%xmm0, -5712(%rbp)
	movaps	%xmm1, -208(%rbp)
	movq	-6096(%rbp), %xmm1
	movq	$0, -5696(%rbp)
	movhps	-6080(%rbp), %xmm1
	movaps	%xmm1, -192(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rcx
	movdqa	-208(%rbp), %xmm4
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm5
	leaq	40(%rax), %rdx
	movq	%rax, -5712(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	leaq	-5584(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5696(%rbp)
	movq	%rdx, -5704(%rbp)
	movq	%rax, -6408(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2657
	call	_ZdlPv@PLT
.L2657:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-5392(%rbp), %rax
	cmpq	$0, -5520(%rbp)
	movq	%rax, -6200(%rbp)
	leaq	-4432(%rbp), %rax
	movq	%rax, -6080(%rbp)
	jne	.L2926
.L2658:
	leaq	-5008(%rbp), %rax
	cmpq	$0, -5328(%rbp)
	movq	%rax, -6208(%rbp)
	leaq	-5200(%rbp), %rax
	movq	%rax, -6176(%rbp)
	jne	.L2927
.L2662:
	leaq	-4816(%rbp), %rax
	cmpq	$0, -5136(%rbp)
	movq	%rax, -6224(%rbp)
	jne	.L2928
	cmpq	$0, -4944(%rbp)
	jne	.L2929
.L2670:
	leaq	-4624(%rbp), %rax
	cmpq	$0, -4752(%rbp)
	movq	%rax, -6232(%rbp)
	jne	.L2930
	cmpq	$0, -4560(%rbp)
	jne	.L2931
.L2675:
	leaq	-4240(%rbp), %rax
	cmpq	$0, -4368(%rbp)
	movq	%rax, -6264(%rbp)
	leaq	-4048(%rbp), %rax
	movq	%rax, -6256(%rbp)
	jne	.L2932
.L2678:
	leaq	-3856(%rbp), %rax
	cmpq	$0, -4176(%rbp)
	movq	%rax, -6240(%rbp)
	jne	.L2933
.L2682:
	leaq	-3664(%rbp), %rax
	cmpq	$0, -3984(%rbp)
	movq	%rax, -6096(%rbp)
	jne	.L2934
	cmpq	$0, -3792(%rbp)
	jne	.L2935
.L2688:
	leaq	-3472(%rbp), %rax
	cmpq	$0, -3600(%rbp)
	movq	%rax, -6272(%rbp)
	leaq	-3280(%rbp), %rax
	movq	%rax, -6288(%rbp)
	jne	.L2936
.L2691:
	leaq	-3088(%rbp), %rax
	cmpq	$0, -3408(%rbp)
	movq	%rax, -6296(%rbp)
	jne	.L2937
.L2695:
	leaq	-2896(%rbp), %rax
	cmpq	$0, -3216(%rbp)
	movq	%rax, -6112(%rbp)
	jne	.L2938
	cmpq	$0, -3024(%rbp)
	jne	.L2939
.L2701:
	leaq	-2704(%rbp), %rax
	cmpq	$0, -2832(%rbp)
	movq	%rax, -6304(%rbp)
	leaq	-2512(%rbp), %rax
	movq	%rax, -6320(%rbp)
	jne	.L2940
.L2703:
	leaq	-2320(%rbp), %rax
	cmpq	$0, -2640(%rbp)
	movq	%rax, -6336(%rbp)
	jne	.L2941
.L2706:
	leaq	-2128(%rbp), %rax
	cmpq	$0, -2448(%rbp)
	movq	%rax, -6128(%rbp)
	jne	.L2942
	cmpq	$0, -2256(%rbp)
	jne	.L2943
.L2710:
	leaq	-1744(%rbp), %rax
	cmpq	$0, -2064(%rbp)
	movq	%rax, -6344(%rbp)
	leaq	-1936(%rbp), %rax
	movq	%rax, -6192(%rbp)
	jne	.L2944
.L2713:
	leaq	-1552(%rbp), %rax
	cmpq	$0, -1872(%rbp)
	movq	%rax, -6352(%rbp)
	jne	.L2945
.L2718:
	cmpq	$0, -1680(%rbp)
	jne	.L2946
.L2721:
	leaq	-1360(%rbp), %rax
	cmpq	$0, -1488(%rbp)
	movq	%rax, -6368(%rbp)
	jne	.L2947
.L2723:
	leaq	-1168(%rbp), %rax
	cmpq	$0, -1296(%rbp)
	movq	%rax, -6160(%rbp)
	jne	.L2948
.L2726:
	leaq	-976(%rbp), %rax
	cmpq	$0, -1104(%rbp)
	movq	%rax, -6360(%rbp)
	leaq	-400(%rbp), %rax
	movq	%rax, -6400(%rbp)
	jne	.L2949
.L2729:
	leaq	-784(%rbp), %rax
	cmpq	$0, -912(%rbp)
	movq	%rax, -6384(%rbp)
	leaq	-592(%rbp), %rax
	movq	%rax, -6144(%rbp)
	jne	.L2950
	cmpq	$0, -720(%rbp)
	jne	.L2951
.L2735:
	cmpq	$0, -528(%rbp)
	jne	.L2952
.L2737:
	cmpq	$0, -336(%rbp)
	jne	.L2953
.L2740:
	movq	-6400(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6144(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6384(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6360(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6160(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6368(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6352(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6344(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6192(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6128(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6336(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6320(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6304(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6112(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6296(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6288(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6272(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6096(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6240(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6256(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6264(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6080(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6232(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6224(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6208(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6176(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6200(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-6408(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2954
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2926:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -5696(%rbp)
	movaps	%xmm0, -5712(%rbp)
	call	_Znwm@PLT
	movq	-6408(%rbp), %rdi
	movq	%r14, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -5712(%rbp)
	movq	%rdx, -5696(%rbp)
	movq	%rdx, -5704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5712(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L2659
	call	_ZdlPv@PLT
.L2659:
	movq	(%r12), %rax
	movl	$128, %edx
	movq	%r15, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	32(%rax), %r12
	movq	%rsi, -6128(%rbp)
	movq	24(%rax), %rsi
	movq	%rcx, -6112(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -6176(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -6080(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -6096(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-6096(%rbp), %rdx
	movq	-6080(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -6096(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movq	-6080(%rbp), %xmm2
	movq	-6112(%rbp), %xmm3
	movl	$40, %edi
	movaps	%xmm0, -5712(%rbp)
	movhps	-6176(%rbp), %xmm2
	movhps	-6128(%rbp), %xmm3
	movq	%r12, -176(%rbp)
	movaps	%xmm2, -6176(%rbp)
	movaps	%xmm3, -6080(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm2, -192(%rbp)
	movq	$0, -5696(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm7
	movq	-176(%rbp), %rcx
	movq	%r14, %rsi
	leaq	40(%rax), %rdx
	movq	%rax, -5712(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-192(%rbp), %xmm7
	movq	%rcx, 32(%rax)
	movups	%xmm7, 16(%rax)
	leaq	-5392(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5696(%rbp)
	movq	%rdx, -5704(%rbp)
	movq	%rax, -6200(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2660
	call	_ZdlPv@PLT
.L2660:
	pxor	%xmm0, %xmm0
	movdqa	-6080(%rbp), %xmm7
	movl	$40, %edi
	movq	%r12, -176(%rbp)
	movaps	%xmm0, -5712(%rbp)
	movaps	%xmm7, -208(%rbp)
	movdqa	-6176(%rbp), %xmm7
	movq	$0, -5696(%rbp)
	movaps	%xmm7, -192(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm7
	movq	-176(%rbp), %rcx
	movq	%r14, %rsi
	leaq	40(%rax), %rdx
	movq	%rax, -5712(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-192(%rbp), %xmm7
	movq	%rcx, 32(%rax)
	movups	%xmm7, 16(%rax)
	leaq	-4432(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5696(%rbp)
	movq	%rdx, -5704(%rbp)
	movq	%rax, -6080(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2661
	call	_ZdlPv@PLT
.L2661:
	movq	-6144(%rbp), %rcx
	movq	-6096(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L2658
	.p2align 4,,10
	.p2align 3
.L2927:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -5696(%rbp)
	movaps	%xmm0, -5712(%rbp)
	call	_Znwm@PLT
	movq	-6200(%rbp), %rdi
	movq	%r14, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -5712(%rbp)
	movq	%rdx, -5696(%rbp)
	movq	%rdx, -5704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2663
	call	_ZdlPv@PLT
.L2663:
	movq	(%rbx), %rax
	movl	$129, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	8(%rax), %rbx
	movq	32(%rax), %rcx
	movq	(%rax), %r12
	movq	%rbx, -6112(%rbp)
	movq	16(%rax), %rbx
	movq	%rcx, -6096(%rbp)
	movq	%rbx, -6128(%rbp)
	movq	24(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$130, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rcx
	movq	%r13, %rdi
	movq	-6096(%rbp), %rdx
	call	_ZN2v88internal31Cast20ATFastJSArrayForCopy_1435EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	movq	-6096(%rbp), %rcx
	movq	%rbx, %xmm3
	movq	-6128(%rbp), %xmm6
	movq	%r12, %xmm4
	leaq	-5792(%rbp), %rbx
	leaq	-208(%rbp), %r12
	movq	%rax, -152(%rbp)
	movq	%rcx, %xmm7
	punpcklqdq	%xmm3, %xmm6
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	punpcklqdq	%xmm7, %xmm7
	movhps	-6112(%rbp), %xmm4
	movq	%rbx, %rdi
	movq	%rcx, -160(%rbp)
	leaq	-144(%rbp), %rdx
	movaps	%xmm7, -6224(%rbp)
	movaps	%xmm6, -6128(%rbp)
	movaps	%xmm4, -6112(%rbp)
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm0, -5792(%rbp)
	movq	$0, -5776(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-5008(%rbp), %rax
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -6208(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2664
	call	_ZdlPv@PLT
.L2664:
	movq	-6232(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-5200(%rbp), %rax
	cmpq	$0, -5704(%rbp)
	movq	%rax, -6176(%rbp)
	jne	.L2955
.L2665:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L2662
	.p2align 4,,10
	.p2align 3
.L2928:
	movq	-6264(%rbp), %rsi
	movq	%r15, %rdi
	movl	$2056, %ebx
	leaq	-208(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-201(%rbp), %rdx
	movw	%bx, -204(%rbp)
	movaps	%xmm0, -5712(%rbp)
	movl	$117769477, -208(%rbp)
	movb	$8, -202(%rbp)
	movq	$0, -5696(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6176(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2668
	call	_ZdlPv@PLT
.L2668:
	movq	(%rbx), %rax
	leaq	-160(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm1
	movdqu	32(%rax), %xmm6
	movq	$0, -5696(%rbp)
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -5712(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-4816(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -6224(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2669
	call	_ZdlPv@PLT
.L2669:
	movq	-6256(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -4944(%rbp)
	je	.L2670
.L2929:
	movq	-6232(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-208(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-200(%rbp), %rdx
	movabsq	$506663788649710853, %rax
	movaps	%xmm0, -5712(%rbp)
	movq	%rax, -208(%rbp)
	movq	$0, -5696(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6208(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2671
	call	_ZdlPv@PLT
.L2671:
	movq	(%rbx), %rax
	movl	$131, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	24(%rax), %r9
	movq	56(%rax), %rbx
	movq	%r9, -6112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-5824(%rbp), %r10
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -6096(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-6096(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$210, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-6096(%rbp), %r10
	movq	-5712(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -208(%rbp)
	xorl	%esi, %esi
	movl	$1, %ebx
	pushq	%rbx
	movq	%rax, %r8
	movq	-6096(%rbp), %r10
	movl	$1, %ecx
	pushq	%r12
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-6112(%rbp), %r9
	leaq	-5792(%rbp), %rdx
	movq	%rax, -5792(%rbp)
	movq	-5696(%rbp), %rax
	movq	%r10, %rdi
	movq	%rax, -5784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-6096(%rbp), %r10
	movq	%rax, %r12
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-6424(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	popq	%r10
	popq	%r11
	jmp	.L2670
	.p2align 4,,10
	.p2align 3
.L2930:
	movq	-6256(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -5696(%rbp)
	movaps	%xmm0, -5712(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r9d
	movq	-6224(%rbp), %rdi
	movq	%r14, %rsi
	movl	$117769477, (%rax)
	leaq	6(%rax), %rdx
	movw	%r9w, 4(%rax)
	movq	%rax, -5712(%rbp)
	movq	%rdx, -5696(%rbp)
	movq	%rdx, -5704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2673
	call	_ZdlPv@PLT
.L2673:
	movq	(%rbx), %rax
	movl	$133, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	16(%rax), %r12
	movq	%rbx, -6096(%rbp)
	movq	32(%rax), %rbx
	movq	%rcx, -6112(%rbp)
	movq	24(%rax), %rcx
	movq	40(%rax), %rax
	movq	%rcx, -6128(%rbp)
	movq	%rax, -6232(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$130, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-208(%rbp), %rsi
	leaq	-160(%rbp), %rdx
	movq	%r14, %rdi
	movq	-6096(%rbp), %xmm0
	movq	$0, -5696(%rbp)
	movhps	-6112(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	%r12, %xmm0
	movhps	-6128(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	%rbx, %xmm0
	movhps	-6232(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -5712(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-4624(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -6232(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2674
	call	_ZdlPv@PLT
.L2674:
	movq	-6240(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -4560(%rbp)
	je	.L2675
.L2931:
	movq	-6240(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -5696(%rbp)
	movaps	%xmm0, -5712(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r8d
	movq	-6232(%rbp), %rdi
	movq	%r14, %rsi
	movl	$117769477, (%rax)
	leaq	6(%rax), %rdx
	movw	%r8w, 4(%rax)
	movq	%rax, -5712(%rbp)
	movq	%rdx, -5696(%rbp)
	movq	%rdx, -5704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2676
	call	_ZdlPv@PLT
.L2676:
	movq	(%rbx), %rax
	movl	$129, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	8(%rax), %rbx
	movq	24(%rax), %rcx
	movq	(%rax), %r12
	movq	%rbx, -6096(%rbp)
	movq	16(%rax), %rbx
	movq	%rcx, -6128(%rbp)
	movq	%rbx, -6112(%rbp)
	movq	32(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$128, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %xmm0
	movl	$40, %edi
	movq	%rbx, -176(%rbp)
	movhps	-6096(%rbp), %xmm0
	movq	$0, -5696(%rbp)
	movaps	%xmm0, -208(%rbp)
	movq	-6112(%rbp), %xmm0
	movhps	-6128(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -5712(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-208(%rbp), %xmm7
	movdqa	-192(%rbp), %xmm6
	movq	-6080(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -5712(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -5696(%rbp)
	movq	%rdx, -5704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2677
	call	_ZdlPv@PLT
.L2677:
	movq	-6144(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2675
	.p2align 4,,10
	.p2align 3
.L2932:
	movq	-6144(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -5696(%rbp)
	movaps	%xmm0, -5712(%rbp)
	call	_Znwm@PLT
	movq	-6080(%rbp), %rdi
	movq	%r14, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -5712(%rbp)
	movq	%rdx, -5696(%rbp)
	movq	%rdx, -5704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2679
	call	_ZdlPv@PLT
.L2679:
	movq	(%rbx), %rax
	movl	$139, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	24(%rax), %r12
	movq	%rbx, -6112(%rbp)
	movq	16(%rax), %rbx
	movq	%rcx, -6128(%rbp)
	movq	32(%rax), %rcx
	movq	%rbx, -6096(%rbp)
	movq	%rcx, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rbx, -6144(%rbp)
	call	_ZN2v88internal17CodeStubAssembler15ToObject_InlineENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	movq	%rax, -6264(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$142, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal21GetLengthProperty_244EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$145, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -6240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-6112(%rbp), %xmm6
	movq	-6096(%rbp), %rax
	movhps	-6128(%rbp), %xmm6
	movq	%rax, -5840(%rbp)
	movaps	%xmm6, -5856(%rbp)
	pushq	-5840(%rbp)
	pushq	-5848(%rbp)
	pushq	-5856(%rbp)
	movaps	%xmm6, -6112(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r14, %rdi
	movq	%rax, %rbx
	movq	%rax, -6256(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$146, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler16ToInteger_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_23ToIntegerTruncationModeE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$150, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal19NumberIsLessThan_75EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	movq	%r12, %xmm7
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movq	-6240(%rbp), %xmm5
	movdqa	-6112(%rbp), %xmm6
	leaq	-208(%rbp), %r12
	leaq	-136(%rbp), %rdx
	movq	-6144(%rbp), %xmm2
	movq	%r12, %rsi
	movq	-6096(%rbp), %xmm3
	movq	%rdx, -6240(%rbp)
	movhps	-6256(%rbp), %xmm5
	movq	%rax, -6128(%rbp)
	movhps	-6264(%rbp), %xmm2
	punpcklqdq	%xmm7, %xmm3
	movaps	%xmm5, -6256(%rbp)
	movaps	%xmm2, -6144(%rbp)
	movaps	%xmm3, -6096(%rbp)
	movaps	%xmm6, -208(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm0, -5712(%rbp)
	movq	%rbx, -144(%rbp)
	movq	$0, -5696(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-4240(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -6264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	movq	-6240(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L2680
	call	_ZdlPv@PLT
	movq	-6240(%rbp), %rdx
.L2680:
	movdqa	-6112(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqa	-6096(%rbp), %xmm6
	movdqa	-6256(%rbp), %xmm4
	movaps	%xmm0, -5712(%rbp)
	movaps	%xmm3, -208(%rbp)
	movdqa	-6144(%rbp), %xmm3
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movq	%rbx, -144(%rbp)
	movq	$0, -5696(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-4048(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -6256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2681
	call	_ZdlPv@PLT
.L2681:
	movq	-6272(%rbp), %rcx
	movq	-6296(%rbp), %rdx
	movq	%r15, %rdi
	movq	-6128(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L2678
	.p2align 4,,10
	.p2align 3
.L2933:
	movq	-6296(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$578720283176011013, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -5696(%rbp)
	movaps	%xmm0, -5712(%rbp)
	call	_Znwm@PLT
	movq	-6264(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -5712(%rbp)
	movq	%rdx, -5696(%rbp)
	movq	%rdx, -5704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2683
	call	_ZdlPv@PLT
.L2683:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	24(%rax), %rsi
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	40(%rax), %rdx
	movq	%rsi, -6144(%rbp)
	movq	32(%rax), %rsi
	movq	48(%rax), %r12
	movq	%rbx, -6096(%rbp)
	movq	%rcx, -6112(%rbp)
	movq	64(%rax), %rbx
	movq	16(%rax), %rcx
	movq	%rdx, -6296(%rbp)
	movq	56(%rax), %rdx
	movq	%rsi, -6240(%rbp)
	movq	%r13, %rsi
	movq	%rcx, -6128(%rbp)
	movq	%rdx, -6512(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberAddENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r14, %rdi
	movq	%rax, -6528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	-6528(%rbp), %r8
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal6Max_81EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	leaq	-208(%rbp), %rsi
	leaq	-128(%rbp), %rdx
	movq	-6096(%rbp), %xmm0
	movq	%r14, %rdi
	movq	%rax, -136(%rbp)
	movhps	-6112(%rbp), %xmm0
	movq	%rbx, -144(%rbp)
	movaps	%xmm0, -208(%rbp)
	movq	-6128(%rbp), %xmm0
	movq	$0, -5696(%rbp)
	movhps	-6144(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-6240(%rbp), %xmm0
	movhps	-6296(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	%r12, %xmm0
	movhps	-6512(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -5712(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3856(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -6240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2684
	call	_ZdlPv@PLT
.L2684:
	movq	-6288(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2682
	.p2align 4,,10
	.p2align 3
.L2934:
	movq	-6272(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$578720283176011013, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -5696(%rbp)
	movaps	%xmm0, -5712(%rbp)
	call	_Znwm@PLT
	movq	-6256(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -5712(%rbp)
	movq	%rdx, -5696(%rbp)
	movq	%rdx, -5704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2686
	call	_ZdlPv@PLT
.L2686:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	%rsi, -6144(%rbp)
	movq	32(%rax), %rsi
	movq	%rdx, -6296(%rbp)
	movq	56(%rax), %rdx
	movq	48(%rax), %r12
	movq	%rbx, -6096(%rbp)
	movq	%rcx, -6112(%rbp)
	movq	64(%rax), %rbx
	movq	16(%rax), %rcx
	movq	%rsi, -6272(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -6512(%rbp)
	movl	$151, %edx
	movq	%rcx, -6128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6Min_80EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	movl	$150, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -6528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-208(%rbp), %rsi
	leaq	-128(%rbp), %rdx
	movq	%r14, %rdi
	movq	-6096(%rbp), %xmm0
	movq	$0, -5696(%rbp)
	movhps	-6112(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-6128(%rbp), %xmm0
	movhps	-6144(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-6272(%rbp), %xmm0
	movhps	-6296(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	%r12, %xmm0
	movhps	-6512(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%rbx, %xmm0
	movhps	-6528(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -5712(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3664(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -6096(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2687
	call	_ZdlPv@PLT
.L2687:
	movq	-6160(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3792(%rbp)
	je	.L2688
.L2935:
	movq	-6288(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$578720283176011013, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -5696(%rbp)
	movaps	%xmm0, -5712(%rbp)
	call	_Znwm@PLT
	movl	$2056, %edi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	10(%rax), %rdx
	movw	%di, 8(%rax)
	movq	-6240(%rbp), %rdi
	movq	%rax, -5712(%rbp)
	movq	%rdx, -5696(%rbp)
	movq	%rdx, -5704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2689
	call	_ZdlPv@PLT
.L2689:
	movq	(%rbx), %rax
	leaq	-208(%rbp), %rsi
	leaq	-128(%rbp), %rdx
	movq	%r14, %rdi
	movdqu	48(%rax), %xmm0
	movdqu	(%rax), %xmm3
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movdqu	64(%rax), %xmm5
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -5712(%rbp)
	movq	$0, -5696(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6096(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2690
	call	_ZdlPv@PLT
.L2690:
	movq	-6160(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2688
	.p2align 4,,10
	.p2align 3
.L2936:
	movq	-6160(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$578720283176011013, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -5696(%rbp)
	movaps	%xmm0, -5712(%rbp)
	call	_Znwm@PLT
	movl	$2056, %esi
	movq	-6096(%rbp), %rdi
	movq	%rbx, (%rax)
	leaq	10(%rax), %rdx
	movw	%si, 8(%rax)
	movq	%r14, %rsi
	movq	%rax, -5712(%rbp)
	movq	%rdx, -5696(%rbp)
	movq	%rdx, -5704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2692
	call	_ZdlPv@PLT
.L2692:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	8(%rax), %rcx
	movq	40(%rax), %rsi
	movq	56(%rax), %rdx
	movq	16(%rax), %r12
	movq	%rcx, -6128(%rbp)
	movq	24(%rax), %rcx
	movq	%rsi, -6272(%rbp)
	movq	48(%rax), %rsi
	movq	%rdx, -6296(%rbp)
	movq	64(%rax), %rdx
	movq	(%rax), %rbx
	movq	%rcx, -6160(%rbp)
	movq	32(%rax), %rcx
	movq	72(%rax), %rax
	movq	%rsi, -6288(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -6512(%rbp)
	movl	$155, %edx
	movq	%rcx, -6144(%rbp)
	movq	%rbx, -6112(%rbp)
	movq	%rax, -6528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-6112(%rbp), %xmm7
	movq	%r12, -5808(%rbp)
	pushq	-5808(%rbp)
	movhps	-6128(%rbp), %xmm7
	movaps	%xmm7, -5824(%rbp)
	pushq	-5816(%rbp)
	pushq	-5824(%rbp)
	movaps	%xmm7, -6112(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$157, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -6128(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-6128(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -6128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-120(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movq	-6512(%rbp), %xmm6
	movdqa	-6112(%rbp), %xmm7
	movq	%rbx, -128(%rbp)
	movq	%r12, %xmm2
	movq	-6288(%rbp), %xmm4
	leaq	-208(%rbp), %r12
	movq	-6144(%rbp), %xmm5
	movhps	-6160(%rbp), %xmm2
	movhps	-6528(%rbp), %xmm6
	movq	%r12, %rsi
	movaps	%xmm2, -6160(%rbp)
	movhps	-6296(%rbp), %xmm4
	movhps	-6272(%rbp), %xmm5
	movq	%rdx, -6296(%rbp)
	movaps	%xmm6, -6512(%rbp)
	movaps	%xmm4, -6288(%rbp)
	movaps	%xmm5, -6144(%rbp)
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm0, -5712(%rbp)
	movq	$0, -5696(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3472(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -6272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	movq	-6296(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L2693
	call	_ZdlPv@PLT
	movq	-6296(%rbp), %rdx
.L2693:
	movdqa	-6112(%rbp), %xmm7
	movdqa	-6160(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movdqa	-6144(%rbp), %xmm3
	movdqa	-6288(%rbp), %xmm5
	movq	%r14, %rdi
	movaps	%xmm0, -5712(%rbp)
	movdqa	-6512(%rbp), %xmm4
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movq	%rbx, -128(%rbp)
	movq	$0, -5696(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3280(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -6288(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2694
	call	_ZdlPv@PLT
.L2694:
	movq	-6336(%rbp), %rcx
	movq	-6320(%rbp), %rdx
	movq	%r15, %rdi
	movq	-6128(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L2691
	.p2align 4,,10
	.p2align 3
.L2937:
	movq	-6320(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$578720283176011013, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$11, %edi
	movq	$0, -5696(%rbp)
	movaps	%xmm0, -5712(%rbp)
	call	_Znwm@PLT
	movl	$2056, %ecx
	movq	-6272(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	11(%rax), %rdx
	movw	%cx, 8(%rax)
	movb	$8, 10(%rax)
	movq	%rax, -5712(%rbp)
	movq	%rdx, -5696(%rbp)
	movq	%rdx, -5704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2696
	call	_ZdlPv@PLT
.L2696:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	64(%rax), %rdi
	movq	24(%rax), %r11
	movq	32(%rax), %r10
	movq	40(%rax), %r9
	movq	48(%rax), %rdx
	movq	56(%rax), %r8
	movq	72(%rax), %rsi
	movq	80(%rax), %rcx
	movq	8(%rax), %r12
	movq	16(%rax), %rbx
	movq	(%rax), %rax
	movq	%rdi, -144(%rbp)
	movl	$96, %edi
	movq	%r11, -184(%rbp)
	movq	%r10, -176(%rbp)
	movq	%r9, -168(%rbp)
	movq	%rdx, -160(%rbp)
	movq	%r8, -152(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movaps	%xmm0, -5712(%rbp)
	movq	%rax, -208(%rbp)
	movq	%r12, -200(%rbp)
	movq	%rbx, -192(%rbp)
	movq	$0, -5696(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm6
	movdqa	-176(%rbp), %xmm3
	movdqa	-160(%rbp), %xmm5
	leaq	96(%rax), %rdx
	movq	%rax, -5712(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-144(%rbp), %xmm4
	movdqa	-128(%rbp), %xmm7
	movups	%xmm6, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm7, 80(%rax)
	leaq	-3088(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5696(%rbp)
	movq	%rdx, -5704(%rbp)
	movq	%rax, -6296(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2697
	call	_ZdlPv@PLT
.L2697:
	movq	-6304(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2695
	.p2align 4,,10
	.p2align 3
.L2938:
	movq	-6336(%rbp), %rsi
	movq	%r15, %rdi
	movl	$2056, %r12d
	movabsq	$578720283176011013, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$11, %edi
	movq	$0, -5696(%rbp)
	movaps	%xmm0, -5712(%rbp)
	call	_Znwm@PLT
	movq	-6288(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	11(%rax), %rdx
	movw	%r12w, 8(%rax)
	movb	$8, 10(%rax)
	movq	%rax, -5712(%rbp)
	movq	%rdx, -5696(%rbp)
	movq	%rdx, -5704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2699
	call	_ZdlPv@PLT
.L2699:
	movq	(%rbx), %rax
	movq	56(%rax), %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	40(%rax), %rdx
	movq	32(%rax), %rsi
	movq	72(%rax), %r11
	movq	%rdi, -6512(%rbp)
	movq	64(%rax), %rdi
	movq	24(%rax), %r12
	movq	%rbx, -6112(%rbp)
	movq	%rcx, -6128(%rbp)
	movq	80(%rax), %rbx
	movq	16(%rax), %rcx
	movq	%rdx, -6320(%rbp)
	movq	48(%rax), %rdx
	movq	%rsi, -6160(%rbp)
	movq	%r13, %rsi
	movq	%rdi, -6528(%rbp)
	movq	%r14, %rdi
	movq	%r11, -6544(%rbp)
	movq	%rcx, -6144(%rbp)
	movq	%rdx, -6336(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler16ToInteger_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS1_23ToIntegerTruncationModeE@PLT
	movq	%r14, %rdi
	movq	%rax, -6560(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %xmm6
	movl	$96, %edi
	movq	-6112(%rbp), %xmm0
	movq	$0, -5696(%rbp)
	movhps	-6128(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-6144(%rbp), %xmm0
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-6160(%rbp), %xmm0
	movhps	-6320(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-6336(%rbp), %xmm0
	movhps	-6512(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-6528(%rbp), %xmm0
	movhps	-6544(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	movhps	-6560(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -5712(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm2
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm6
	leaq	96(%rax), %rdx
	movq	%rax, -5712(%rbp)
	movdqa	-176(%rbp), %xmm5
	movdqa	-160(%rbp), %xmm4
	movups	%xmm2, (%rax)
	movdqa	-144(%rbp), %xmm7
	movups	%xmm3, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm6, 80(%rax)
	leaq	-2896(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5696(%rbp)
	movq	%rdx, -5704(%rbp)
	movq	%rax, -6112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2700
	call	_ZdlPv@PLT
.L2700:
	movq	-6192(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3024(%rbp)
	je	.L2701
.L2939:
	movq	-6304(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -5984(%rbp)
	movq	$0, -5976(%rbp)
	movq	$0, -5968(%rbp)
	movq	$0, -5960(%rbp)
	movq	$0, -5952(%rbp)
	movq	$0, -5944(%rbp)
	movq	$0, -5936(%rbp)
	movq	$0, -5928(%rbp)
	movq	$0, -5920(%rbp)
	movq	$0, -5912(%rbp)
	movq	$0, -5904(%rbp)
	movq	$0, -5792(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5792(%rbp), %rax
	movq	-6296(%rbp), %rdi
	pushq	%rax
	leaq	-5904(%rbp), %rax
	leaq	-5968(%rbp), %rcx
	pushq	%rax
	leaq	-5912(%rbp), %rax
	leaq	-5976(%rbp), %rdx
	pushq	%rax
	leaq	-5920(%rbp), %rax
	leaq	-5984(%rbp), %rsi
	pushq	%rax
	leaq	-5928(%rbp), %rax
	leaq	-5952(%rbp), %r9
	pushq	%rax
	leaq	-5936(%rbp), %rax
	leaq	-5960(%rbp), %r8
	pushq	%rax
	leaq	-5944(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESL_SP_SP_SL_SP_
	addq	$64, %rsp
	movl	$96, %edi
	movq	-5904(%rbp), %xmm0
	movq	-5920(%rbp), %xmm1
	movq	-5936(%rbp), %xmm2
	movq	$0, -5696(%rbp)
	movq	-5952(%rbp), %xmm3
	movhps	-5792(%rbp), %xmm0
	movq	-5968(%rbp), %xmm4
	movq	-5984(%rbp), %xmm5
	movhps	-5912(%rbp), %xmm1
	movhps	-5928(%rbp), %xmm2
	movaps	%xmm0, -128(%rbp)
	movhps	-5944(%rbp), %xmm3
	movhps	-5960(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -160(%rbp)
	movhps	-5976(%rbp), %xmm5
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -5712(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm2
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm6
	movq	-6112(%rbp), %rdi
	leaq	96(%rax), %rdx
	movq	%rax, -5712(%rbp)
	movdqa	-176(%rbp), %xmm5
	movdqa	-160(%rbp), %xmm4
	movups	%xmm2, (%rax)
	movdqa	-144(%rbp), %xmm7
	movups	%xmm3, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm6, 80(%rax)
	movq	%rdx, -5696(%rbp)
	movq	%rdx, -5704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2702
	call	_ZdlPv@PLT
.L2702:
	movq	-6192(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2701
	.p2align 4,,10
	.p2align 3
.L2940:
	movq	-6192(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -5984(%rbp)
	movq	$0, -5976(%rbp)
	movq	$0, -5968(%rbp)
	movq	$0, -5960(%rbp)
	movq	$0, -5952(%rbp)
	movq	$0, -5944(%rbp)
	movq	$0, -5936(%rbp)
	movq	$0, -5928(%rbp)
	movq	$0, -5920(%rbp)
	movq	$0, -5912(%rbp)
	movq	$0, -5904(%rbp)
	movq	$0, -5792(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5792(%rbp), %rax
	movq	-6112(%rbp), %rdi
	pushq	%rax
	leaq	-5904(%rbp), %rax
	leaq	-5968(%rbp), %rcx
	pushq	%rax
	leaq	-5912(%rbp), %rax
	leaq	-5952(%rbp), %r9
	pushq	%rax
	leaq	-5920(%rbp), %rax
	leaq	-5960(%rbp), %r8
	pushq	%rax
	leaq	-5928(%rbp), %rax
	leaq	-5976(%rbp), %rdx
	pushq	%rax
	leaq	-5936(%rbp), %rax
	leaq	-5984(%rbp), %rsi
	pushq	%rax
	leaq	-5944(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESL_SP_SP_SL_SP_
	addq	$64, %rsp
	movl	$156, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$162, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	-5792(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal19NumberIsLessThan_75EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	pxor	%xmm0, %xmm0
	movl	$96, %edi
	movq	-5904(%rbp), %xmm7
	movq	-5920(%rbp), %xmm3
	movaps	%xmm0, -5712(%rbp)
	movq	%rax, %r12
	movq	-5936(%rbp), %xmm6
	movq	-5952(%rbp), %xmm4
	movhps	-5792(%rbp), %xmm7
	movq	-5968(%rbp), %xmm5
	movq	$0, -5696(%rbp)
	movq	-5984(%rbp), %xmm2
	movhps	-5912(%rbp), %xmm3
	movaps	%xmm7, -128(%rbp)
	movhps	-5928(%rbp), %xmm6
	movhps	-5944(%rbp), %xmm4
	movhps	-5960(%rbp), %xmm5
	movaps	%xmm7, -6144(%rbp)
	movhps	-5976(%rbp), %xmm2
	movaps	%xmm3, -6160(%rbp)
	movaps	%xmm6, -6192(%rbp)
	movaps	%xmm4, -6320(%rbp)
	movaps	%xmm5, -6336(%rbp)
	movaps	%xmm2, -6128(%rbp)
	movaps	%xmm2, -208(%rbp)
	movaps	%xmm5, -192(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm2
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm7
	movq	-6304(%rbp), %rdi
	leaq	96(%rax), %rdx
	movq	%rax, -5712(%rbp)
	movdqa	-176(%rbp), %xmm5
	movdqa	-160(%rbp), %xmm4
	movups	%xmm2, (%rax)
	movdqa	-144(%rbp), %xmm1
	movups	%xmm3, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm1, 64(%rax)
	movups	%xmm7, 80(%rax)
	movq	%rdx, -5696(%rbp)
	movq	%rdx, -5704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2704
	call	_ZdlPv@PLT
.L2704:
	movdqa	-6128(%rbp), %xmm6
	movdqa	-6336(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movl	$96, %edi
	movdqa	-6320(%rbp), %xmm3
	movdqa	-6192(%rbp), %xmm5
	movaps	%xmm0, -5712(%rbp)
	movdqa	-6160(%rbp), %xmm4
	movdqa	-6144(%rbp), %xmm1
	movaps	%xmm6, -208(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movq	$0, -5696(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm4
	leaq	96(%rax), %rdx
	movq	%rax, -5712(%rbp)
	movdqa	-176(%rbp), %xmm2
	movdqa	-160(%rbp), %xmm3
	movups	%xmm7, (%rax)
	movdqa	-144(%rbp), %xmm5
	movups	%xmm6, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm5, 64(%rax)
	movups	%xmm4, 80(%rax)
	leaq	-2512(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5696(%rbp)
	movq	%rdx, -5704(%rbp)
	movq	%rax, -6320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2705
	call	_ZdlPv@PLT
.L2705:
	movq	-6360(%rbp), %rcx
	movq	-6344(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L2703
	.p2align 4,,10
	.p2align 3
.L2942:
	movq	-6360(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -5984(%rbp)
	movq	$0, -5976(%rbp)
	movq	$0, -5968(%rbp)
	movq	$0, -5960(%rbp)
	movq	$0, -5952(%rbp)
	movq	$0, -5944(%rbp)
	movq	$0, -5936(%rbp)
	movq	$0, -5928(%rbp)
	movq	$0, -5920(%rbp)
	movq	$0, -5912(%rbp)
	movq	$0, -5904(%rbp)
	movq	$0, -5792(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5792(%rbp), %rax
	movq	-6320(%rbp), %rdi
	pushq	%rax
	leaq	-5904(%rbp), %rax
	leaq	-5968(%rbp), %rcx
	pushq	%rax
	leaq	-5912(%rbp), %rax
	leaq	-5952(%rbp), %r9
	pushq	%rax
	leaq	-5920(%rbp), %rax
	leaq	-5960(%rbp), %r8
	pushq	%rax
	leaq	-5928(%rbp), %rax
	leaq	-5976(%rbp), %rdx
	pushq	%rax
	leaq	-5936(%rbp), %rax
	leaq	-5984(%rbp), %rsi
	pushq	%rax
	leaq	-5944(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESL_SP_SP_SL_SP_
	movq	-5936(%rbp), %rdx
	addq	$64, %rsp
	movq	%r13, %rdi
	movq	-5792(%rbp), %rsi
	call	_ZN2v88internal6Min_80EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	movq	-5984(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	leaq	-208(%rbp), %rsi
	movq	%rax, -112(%rbp)
	movq	%rdx, -208(%rbp)
	movq	-5976(%rbp), %rdx
	movaps	%xmm0, -5712(%rbp)
	movq	%rdx, -200(%rbp)
	movq	-5968(%rbp), %rdx
	movq	$0, -5696(%rbp)
	movq	%rdx, -192(%rbp)
	movq	-5960(%rbp), %rdx
	movq	%rdx, -184(%rbp)
	movq	-5952(%rbp), %rdx
	movq	%rdx, -176(%rbp)
	movq	-5944(%rbp), %rdx
	movq	%rdx, -168(%rbp)
	movq	-5936(%rbp), %rdx
	movq	%rdx, -160(%rbp)
	movq	-5928(%rbp), %rdx
	movq	%rdx, -152(%rbp)
	movq	-5920(%rbp), %rdx
	movq	%rdx, -144(%rbp)
	movq	-5912(%rbp), %rdx
	movq	%rdx, -136(%rbp)
	movq	-5904(%rbp), %rdx
	movq	%rdx, -128(%rbp)
	movq	-5792(%rbp), %rdx
	movq	%rdx, -120(%rbp)
	leaq	-104(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6128(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2709
	call	_ZdlPv@PLT
.L2709:
	movq	-6352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2256(%rbp)
	je	.L2710
.L2943:
	movq	-6384(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$578720283176011013, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$13, %edi
	movq	$0, -5696(%rbp)
	movaps	%xmm0, -5712(%rbp)
	call	_Znwm@PLT
	movq	-6336(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	13(%rax), %rdx
	movl	$134744072, 8(%rax)
	movb	$8, 12(%rax)
	movq	%rax, -5712(%rbp)
	movq	%rdx, -5696(%rbp)
	movq	%rdx, -5704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2711
	call	_ZdlPv@PLT
.L2711:
	movq	(%rbx), %rax
	leaq	-208(%rbp), %rsi
	leaq	-104(%rbp), %rdx
	movq	%r14, %rdi
	movdqu	80(%rax), %xmm0
	movdqu	(%rax), %xmm5
	movdqu	16(%rax), %xmm4
	movdqu	32(%rax), %xmm3
	movdqu	48(%rax), %xmm2
	movdqu	64(%rax), %xmm1
	movq	96(%rax), %rax
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -208(%rbp)
	movq	%rax, -112(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -5712(%rbp)
	movq	$0, -5696(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6128(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2712
	call	_ZdlPv@PLT
.L2712:
	movq	-6352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2710
	.p2align 4,,10
	.p2align 3
.L2941:
	movq	-6344(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -5984(%rbp)
	movq	$0, -5976(%rbp)
	movq	$0, -5968(%rbp)
	movq	$0, -5960(%rbp)
	movq	$0, -5952(%rbp)
	movq	$0, -5944(%rbp)
	movq	$0, -5936(%rbp)
	movq	$0, -5928(%rbp)
	movq	$0, -5920(%rbp)
	movq	$0, -5912(%rbp)
	movq	$0, -5904(%rbp)
	movq	$0, -5792(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5792(%rbp), %rax
	movq	-6304(%rbp), %rdi
	pushq	%rax
	leaq	-5904(%rbp), %rax
	leaq	-5968(%rbp), %rcx
	pushq	%rax
	leaq	-5912(%rbp), %rax
	leaq	-5952(%rbp), %r9
	pushq	%rax
	leaq	-5920(%rbp), %rax
	leaq	-5960(%rbp), %r8
	pushq	%rax
	leaq	-5928(%rbp), %rax
	leaq	-5976(%rbp), %rdx
	pushq	%rax
	leaq	-5936(%rbp), %rax
	leaq	-5984(%rbp), %rsi
	pushq	%rax
	leaq	-5944(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESL_SP_SP_SL_SP_
	addq	$64, %rsp
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5792(%rbp), %rdx
	movq	-5936(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberAddENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal6Max_81EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	movq	-5984(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	leaq	-208(%rbp), %rsi
	movq	%rax, -112(%rbp)
	movq	%rdx, -208(%rbp)
	movq	-5976(%rbp), %rdx
	movaps	%xmm0, -5712(%rbp)
	movq	%rdx, -200(%rbp)
	movq	-5968(%rbp), %rdx
	movq	$0, -5696(%rbp)
	movq	%rdx, -192(%rbp)
	movq	-5960(%rbp), %rdx
	movq	%rdx, -184(%rbp)
	movq	-5952(%rbp), %rdx
	movq	%rdx, -176(%rbp)
	movq	-5944(%rbp), %rdx
	movq	%rdx, -168(%rbp)
	movq	-5936(%rbp), %rdx
	movq	%rdx, -160(%rbp)
	movq	-5928(%rbp), %rdx
	movq	%rdx, -152(%rbp)
	movq	-5920(%rbp), %rdx
	movq	%rdx, -144(%rbp)
	movq	-5912(%rbp), %rdx
	movq	%rdx, -136(%rbp)
	movq	-5904(%rbp), %rdx
	movq	%rdx, -128(%rbp)
	movq	-5792(%rbp), %rdx
	movq	%rdx, -120(%rbp)
	leaq	-104(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6336(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2707
	call	_ZdlPv@PLT
.L2707:
	movq	-6384(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2706
	.p2align 4,,10
	.p2align 3
.L2944:
	movq	-6352(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$578720283176011013, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$13, %edi
	movq	$0, -5696(%rbp)
	movaps	%xmm0, -5712(%rbp)
	call	_Znwm@PLT
	movq	-6128(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	13(%rax), %rdx
	movl	$134744072, 8(%rax)
	movb	$8, 12(%rax)
	movq	%rax, -5712(%rbp)
	movq	%rdx, -5696(%rbp)
	movq	%rdx, -5704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2714
	call	_ZdlPv@PLT
.L2714:
	movq	(%rbx), %rax
	movl	$161, %edx
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	56(%rax), %r11
	movq	32(%rax), %rsi
	movq	48(%rax), %rdi
	movq	88(%rax), %r10
	movq	%rbx, -6352(%rbp)
	movq	40(%rax), %rbx
	movq	80(%rax), %r9
	movq	%rcx, -6360(%rbp)
	movq	24(%rax), %r12
	movq	16(%rax), %rcx
	movq	%r11, -6544(%rbp)
	movq	%rbx, -6144(%rbp)
	movq	64(%rax), %r11
	movq	72(%rax), %rbx
	movq	96(%rax), %rax
	movq	%rsi, -6512(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdi, -6528(%rbp)
	movq	%r15, %rdi
	movq	%r11, -6560(%rbp)
	movq	%r10, -6592(%rbp)
	movq	%r9, -6576(%rbp)
	movq	%rcx, -6384(%rbp)
	movq	%rax, -6344(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$165, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-6344(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	%rbx, -6160(%rbp)
	call	_ZN2v88internal17CodeStubAssembler9NumberSubENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal6Max_81EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	movl	$175, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -6192(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal32UnsafeCast15ATNativeContext_1437EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %r9
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	-6192(%rbp), %r8
	movq	-6160(%rbp), %rcx
	movq	-6144(%rbp), %rdx
	call	_ZN2v88internal18HandleFastSlice_40EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_6ObjectEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEESD_PNS1_18CodeAssemblerLabelE
	movq	%r12, %xmm11
	movq	%rbx, %xmm3
	movq	-6384(%rbp), %xmm0
	movq	-6160(%rbp), %xmm7
	leaq	-5792(%rbp), %rbx
	movq	-6576(%rbp), %xmm4
	leaq	-208(%rbp), %r12
	movq	-6560(%rbp), %xmm5
	leaq	-56(%rbp), %rdx
	movq	%r12, %rsi
	movq	-6528(%rbp), %xmm2
	punpcklqdq	%xmm11, %xmm0
	movhps	-6192(%rbp), %xmm7
	movhps	-6144(%rbp), %xmm3
	movq	%rbx, %rdi
	movq	-6512(%rbp), %xmm1
	movhps	-6160(%rbp), %xmm5
	movq	-6344(%rbp), %xmm6
	movhps	-6592(%rbp), %xmm4
	movq	-6352(%rbp), %xmm9
	movhps	-6544(%rbp), %xmm2
	movaps	%xmm0, -192(%rbp)
	movhps	-6144(%rbp), %xmm1
	movhps	-6192(%rbp), %xmm6
	movaps	%xmm0, -6144(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-6360(%rbp), %xmm9
	movaps	%xmm7, -6608(%rbp)
	movaps	%xmm3, -6624(%rbp)
	movaps	%xmm6, -6640(%rbp)
	movaps	%xmm4, -6576(%rbp)
	movaps	%xmm5, -6560(%rbp)
	movaps	%xmm2, -6528(%rbp)
	movaps	%xmm1, -6512(%rbp)
	movaps	%xmm9, -6160(%rbp)
	movaps	%xmm9, -208(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movq	%rax, -64(%rbp)
	movaps	%xmm7, -80(%rbp)
	movaps	%xmm0, -5792(%rbp)
	movq	$0, -5776(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1744(%rbp), %rax
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -6344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2715
	call	_ZdlPv@PLT
.L2715:
	movq	-6368(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1936(%rbp), %rax
	cmpq	$0, -5704(%rbp)
	movq	%rax, -6192(%rbp)
	jne	.L2956
.L2716:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L2713
	.p2align 4,,10
	.p2align 3
.L2946:
	movq	-6368(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$19, %edi
	movq	$0, -5696(%rbp)
	movaps	%xmm0, -5712(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r11d
	movdqa	.LC9(%rip), %xmm0
	movq	%r14, %rsi
	movw	%r11w, 16(%rax)
	movq	-6344(%rbp), %rdi
	leaq	19(%rax), %rdx
	movb	$7, 18(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, -5712(%rbp)
	movq	%rdx, -5696(%rbp)
	movq	%rdx, -5704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2722
	call	_ZdlPv@PLT
.L2722:
	movq	(%rbx), %rax
	movq	-6424(%rbp), %rdi
	movq	144(%rax), %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	jmp	.L2721
	.p2align 4,,10
	.p2align 3
.L2945:
	movq	-6496(%rbp), %rsi
	movq	%r15, %rdi
	movl	$2056, %ebx
	leaq	-208(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC9(%rip), %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-190(%rbp), %rdx
	movw	%bx, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -5712(%rbp)
	movq	$0, -5696(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6192(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2719
	call	_ZdlPv@PLT
.L2719:
	movq	(%rbx), %rax
	leaq	-96(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqu	80(%rax), %xmm0
	movdqu	(%rax), %xmm5
	movdqu	16(%rax), %xmm4
	movdqu	32(%rax), %xmm3
	movdqu	48(%rax), %xmm2
	movdqu	64(%rax), %xmm1
	movdqu	96(%rax), %xmm7
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -5712(%rbp)
	movq	$0, -5696(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1552(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -6352(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2720
	call	_ZdlPv@PLT
.L2720:
	movq	-6400(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2718
	.p2align 4,,10
	.p2align 3
.L2948:
	movq	-6480(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$578720283176011013, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$14, %edi
	movq	$0, -5696(%rbp)
	movaps	%xmm0, -5712(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r9d
	movq	-6368(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	14(%rax), %rdx
	movl	$134744072, 8(%rax)
	movw	%r9w, 12(%rax)
	movq	%rax, -5712(%rbp)
	movq	%rdx, -5696(%rbp)
	movq	%rdx, -5704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2727
	call	_ZdlPv@PLT
.L2727:
	movq	(%rbx), %rax
	leaq	.LC2(%rip), %rsi
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	56(%rax), %r11
	movq	72(%rax), %r9
	movq	88(%rax), %r10
	movq	32(%rax), %rdx
	movq	%rbx, -6160(%rbp)
	movq	48(%rax), %rdi
	movq	24(%rax), %rbx
	movq	%rcx, -6360(%rbp)
	movq	%r11, -6496(%rbp)
	movq	16(%rax), %rcx
	movq	64(%rax), %r11
	movq	%r9, -6528(%rbp)
	movq	80(%rax), %r9
	movq	%r10, -6560(%rbp)
	movq	96(%rax), %r10
	movq	%rdx, -6400(%rbp)
	movl	$181, %edx
	movq	%rdi, -6480(%rbp)
	movq	%r15, %rdi
	movq	40(%rax), %r12
	movq	%r11, -6512(%rbp)
	movq	%r9, -6544(%rbp)
	movq	%r10, -6576(%rbp)
	movq	%rcx, -6384(%rbp)
	movq	%rbx, -6144(%rbp)
	movq	104(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	-6144(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler18ArraySpeciesCreateENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -6608(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$184, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$187, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -6592(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %xmm5
	leaq	-80(%rbp), %rdx
	movq	%r14, %rdi
	movq	-6160(%rbp), %xmm0
	movq	%rbx, %xmm4
	leaq	-208(%rbp), %rsi
	movq	$0, -5696(%rbp)
	movhps	-6360(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-6384(%rbp), %xmm0
	movhps	-6144(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-6400(%rbp), %xmm0
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-6480(%rbp), %xmm0
	movhps	-6496(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-6512(%rbp), %xmm0
	movhps	-6528(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-6544(%rbp), %xmm0
	movhps	-6560(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-6576(%rbp), %xmm0
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-6608(%rbp), %xmm0
	movhps	-6592(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -5712(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1168(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -6160(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2728
	call	_ZdlPv@PLT
.L2728:
	movq	-6432(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2726
	.p2align 4,,10
	.p2align 3
.L2947:
	movq	-6400(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$578720283176011013, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$14, %edi
	movq	$0, -5696(%rbp)
	movaps	%xmm0, -5712(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r10d
	movq	-6352(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	14(%rax), %rdx
	movl	$134744072, 8(%rax)
	movw	%r10w, 12(%rax)
	movq	%rax, -5712(%rbp)
	movq	%rdx, -5696(%rbp)
	movq	%rdx, -5704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2724
	call	_ZdlPv@PLT
.L2724:
	movq	(%rbx), %rax
	movq	40(%rax), %rdx
	movq	24(%rax), %rsi
	movq	88(%rax), %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	48(%rax), %r12
	movq	%rdx, -6400(%rbp)
	movq	56(%rax), %rdx
	movq	%rsi, -6384(%rbp)
	movq	%rdi, -6544(%rbp)
	movq	32(%rax), %rsi
	movq	%rdx, -6496(%rbp)
	movq	72(%rax), %rdx
	movq	96(%rax), %rdi
	movq	%rbx, -6144(%rbp)
	movq	%rdx, -6512(%rbp)
	movq	80(%rax), %rdx
	movq	64(%rax), %rbx
	movq	%rcx, -6160(%rbp)
	movq	16(%rax), %rcx
	movq	104(%rax), %rax
	movq	%rsi, -6368(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -6528(%rbp)
	movl	$174, %edx
	movq	%rdi, -6560(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -6360(%rbp)
	movq	%rax, -6576(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-208(%rbp), %rsi
	leaq	-96(%rbp), %rdx
	movq	%r14, %rdi
	movq	-6144(%rbp), %xmm0
	movq	$0, -5696(%rbp)
	movhps	-6160(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-6360(%rbp), %xmm0
	movhps	-6384(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-6368(%rbp), %xmm0
	movhps	-6400(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	%r12, %xmm0
	movhps	-6496(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%rbx, %xmm0
	movhps	-6512(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-6528(%rbp), %xmm0
	movhps	-6544(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-6560(%rbp), %xmm0
	movhps	-6576(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -5712(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1360(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -6368(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2725
	call	_ZdlPv@PLT
.L2725:
	movq	-6480(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2723
	.p2align 4,,10
	.p2align 3
.L2950:
	movq	-6440(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -6032(%rbp)
	movq	$0, -6024(%rbp)
	movq	$0, -6016(%rbp)
	movq	$0, -6008(%rbp)
	movq	$0, -6000(%rbp)
	movq	$0, -5992(%rbp)
	movq	$0, -5984(%rbp)
	movq	$0, -5976(%rbp)
	movq	$0, -5968(%rbp)
	movq	$0, -5960(%rbp)
	movq	$0, -5952(%rbp)
	movq	$0, -5944(%rbp)
	movq	$0, -5936(%rbp)
	movq	$0, -5928(%rbp)
	movq	$0, -5920(%rbp)
	movq	$0, -5912(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5912(%rbp), %rax
	movq	-6360(%rbp), %rdi
	pushq	%rax
	leaq	-5920(%rbp), %rax
	leaq	-6016(%rbp), %rcx
	pushq	%rax
	leaq	-5928(%rbp), %rax
	leaq	-6008(%rbp), %r8
	pushq	%rax
	leaq	-5936(%rbp), %rax
	leaq	-6000(%rbp), %r9
	pushq	%rax
	leaq	-5944(%rbp), %rax
	leaq	-6024(%rbp), %rdx
	pushq	%rax
	leaq	-5952(%rbp), %rax
	leaq	-6032(%rbp), %rsi
	pushq	%rax
	leaq	-5960(%rbp), %rax
	pushq	%rax
	leaq	-5968(%rbp), %rax
	pushq	%rax
	leaq	-5976(%rbp), %rax
	pushq	%rax
	leaq	-5984(%rbp), %rax
	pushq	%rax
	leaq	-5992(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_SB_SB_S7_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESL_SP_SP_SL_SP_SP_SP_SN_SP_
	addq	$96, %rsp
	movl	$189, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$192, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-5904(%rbp), %r10
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -6144(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-6144(%rbp), %r10
	movq	-6008(%rbp), %r9
	movq	-5992(%rbp), %rbx
	movq	-5960(%rbp), %r12
	movq	%r10, %rdi
	movq	%r9, -6440(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$159, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-6144(%rbp), %r10
	movq	-5712(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r12, %xmm6
	xorl	%esi, %esi
	movq	%rbx, %xmm0
	leaq	-208(%rbp), %r12
	movl	$2, %ebx
	movq	%rax, %r8
	movq	-6144(%rbp), %r10
	pushq	%rbx
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-6440(%rbp), %r9
	punpcklqdq	%xmm6, %xmm0
	pushq	%r12
	movl	$1, %ecx
	movq	%r10, %rdi
	leaq	-5792(%rbp), %rdx
	movq	%rax, -5792(%rbp)
	movq	-5696(%rbp), %rax
	movaps	%xmm0, -208(%rbp)
	movq	%rax, -5784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-6144(%rbp), %r10
	movq	%rax, %rbx
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$195, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -6144(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-6144(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -6440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-64(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-6032(%rbp), %xmm0
	movq	-5960(%rbp), %rax
	movq	-5920(%rbp), %xmm7
	movq	-5936(%rbp), %xmm3
	movq	-5952(%rbp), %xmm6
	movhps	-6024(%rbp), %xmm0
	movq	-5968(%rbp), %xmm4
	movq	%rax, %xmm10
	movq	-5984(%rbp), %xmm5
	movhps	-5928(%rbp), %xmm3
	movq	-6000(%rbp), %xmm2
	movhps	-5912(%rbp), %xmm7
	movhps	-5944(%rbp), %xmm6
	punpcklqdq	%xmm10, %xmm4
	movq	-6016(%rbp), %xmm1
	movaps	%xmm0, -6480(%rbp)
	movhps	-5976(%rbp), %xmm5
	movhps	-5992(%rbp), %xmm2
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-6008(%rbp), %xmm1
	movq	%rax, -6144(%rbp)
	movaps	%xmm7, -6592(%rbp)
	movaps	%xmm3, -6576(%rbp)
	movaps	%xmm6, -6560(%rbp)
	movaps	%xmm4, -6544(%rbp)
	movaps	%xmm5, -6528(%rbp)
	movaps	%xmm2, -6512(%rbp)
	movaps	%xmm1, -6496(%rbp)
	movaps	%xmm1, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rdx, -6608(%rbp)
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -5712(%rbp)
	movq	%rbx, -72(%rbp)
	movq	$0, -5696(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6384(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	popq	%rsi
	movq	-6608(%rbp), %rdx
	popq	%r8
	testq	%rdi, %rdi
	je	.L2733
	call	_ZdlPv@PLT
	movq	-6608(%rbp), %rdx
.L2733:
	movq	-6144(%rbp), %xmm0
	movdqa	-6496(%rbp), %xmm3
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqa	-6480(%rbp), %xmm2
	movdqa	-6512(%rbp), %xmm5
	movq	$0, -5696(%rbp)
	movaps	%xmm3, -192(%rbp)
	movq	%rbx, %xmm3
	movdqa	-6528(%rbp), %xmm4
	movdqa	-6544(%rbp), %xmm1
	punpcklqdq	%xmm3, %xmm0
	movdqa	-6560(%rbp), %xmm7
	movdqa	-6576(%rbp), %xmm6
	movaps	%xmm2, -208(%rbp)
	movdqa	-6592(%rbp), %xmm2
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -5712(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-592(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -6144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2734
	call	_ZdlPv@PLT
.L2734:
	movq	-6416(%rbp), %rcx
	movq	-6448(%rbp), %rdx
	movq	%r15, %rdi
	movq	-6440(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -720(%rbp)
	je	.L2735
.L2951:
	movq	-6448(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -6048(%rbp)
	leaq	-5904(%rbp), %r12
	movq	$0, -6040(%rbp)
	movq	$0, -6032(%rbp)
	movq	$0, -6024(%rbp)
	movq	$0, -6016(%rbp)
	movq	$0, -6008(%rbp)
	movq	$0, -6000(%rbp)
	movq	$0, -5992(%rbp)
	movq	$0, -5984(%rbp)
	movq	$0, -5976(%rbp)
	movq	$0, -5968(%rbp)
	movq	$0, -5960(%rbp)
	movq	$0, -5952(%rbp)
	movq	$0, -5944(%rbp)
	movq	$0, -5936(%rbp)
	movq	$0, -5928(%rbp)
	movq	$0, -5920(%rbp)
	movq	$0, -5912(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5912(%rbp), %rax
	movq	-6384(%rbp), %rdi
	pushq	%rax
	leaq	-5920(%rbp), %rax
	leaq	-6032(%rbp), %rcx
	pushq	%rax
	leaq	-5928(%rbp), %rax
	leaq	-6024(%rbp), %r8
	pushq	%rax
	leaq	-5936(%rbp), %rax
	leaq	-6016(%rbp), %r9
	pushq	%rax
	leaq	-5944(%rbp), %rax
	leaq	-6040(%rbp), %rdx
	pushq	%rax
	leaq	-5952(%rbp), %rax
	leaq	-6048(%rbp), %rsi
	pushq	%rax
	leaq	-5960(%rbp), %rax
	pushq	%rax
	leaq	-5968(%rbp), %rax
	pushq	%rax
	leaq	-5976(%rbp), %rax
	pushq	%rax
	leaq	-5984(%rbp), %rax
	pushq	%rax
	leaq	-5992(%rbp), %rax
	pushq	%rax
	leaq	-6000(%rbp), %rax
	pushq	%rax
	leaq	-6008(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_SB_SB_S7_SB_SB_NS0_7OddballEEE10CreatePhisEPNS1_5TNodeIS3_EESG_PNSE_IS4_EEPNSE_IS5_EEPNSE_IS6_EEPNSE_IS7_EEPNSE_ISB_EESM_SQ_SQ_SM_SQ_SQ_SQ_SO_SQ_SQ_PNSE_ISC_EE
	addq	$112, %rsp
	movl	$197, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-6024(%rbp), %r9
	movq	%r12, %rdi
	movq	-5920(%rbp), %rax
	movq	-6008(%rbp), %rbx
	movq	%r9, -6448(%rbp)
	movq	%rax, -6440(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-5712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r8
	movq	-5696(%rbp), %rax
	movl	$2, %ebx
	leaq	-5792(%rbp), %r11
	pushq	%rbx
	movq	-6448(%rbp), %r9
	movq	%r11, %rdx
	movl	$1, %ecx
	movq	%rax, -5784(%rbp)
	leaq	-208(%rbp), %rax
	movhps	-6440(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	pushq	%rax
	movq	%r10, -5792(%rbp)
	movq	%r11, -6512(%rbp)
	movaps	%xmm0, -208(%rbp)
	movq	%rax, -6440(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$200, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-6024(%rbp), %r9
	movq	-5936(%rbp), %rcx
	movq	%r12, %rdi
	movq	-5928(%rbp), %rsi
	movq	%r9, -6496(%rbp)
	movq	%rcx, -6448(%rbp)
	movq	%rsi, -6480(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$786, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-5712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-6440(%rbp), %rcx
	xorl	%esi, %esi
	movq	%rbx, -192(%rbp)
	movl	$3, %ebx
	movq	-6512(%rbp), %r11
	movq	%rax, %r8
	movq	%r12, %rdi
	pushq	%rbx
	movq	-6496(%rbp), %r9
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %r10
	pushq	%rcx
	movq	-5696(%rbp), %rax
	movq	%r11, %rdx
	movl	$1, %ecx
	movq	-6448(%rbp), %xmm0
	movq	%r10, -5792(%rbp)
	movq	%rax, -5784(%rbp)
	movhps	-6480(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	addq	$32, %rsp
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$195, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-6440(%rbp), %rsi
	leaq	-64(%rbp), %rdx
	movq	%r14, %rdi
	movq	-5920(%rbp), %xmm0
	movq	-5936(%rbp), %xmm1
	movq	$0, -5696(%rbp)
	movq	-5952(%rbp), %xmm2
	movq	-5968(%rbp), %xmm3
	movq	-5984(%rbp), %xmm4
	movhps	-5912(%rbp), %xmm0
	movq	-6000(%rbp), %xmm5
	movhps	-5928(%rbp), %xmm1
	movq	-6016(%rbp), %xmm6
	movhps	-5944(%rbp), %xmm2
	movaps	%xmm0, -80(%rbp)
	movq	-6032(%rbp), %xmm7
	movhps	-5960(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -112(%rbp)
	movq	-6048(%rbp), %xmm8
	movhps	-5976(%rbp), %xmm4
	movhps	-5992(%rbp), %xmm5
	movaps	%xmm3, -128(%rbp)
	movhps	-6008(%rbp), %xmm6
	movhps	-6024(%rbp), %xmm7
	movhps	-6040(%rbp), %xmm8
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm8, -208(%rbp)
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -5712(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6144(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2736
	call	_ZdlPv@PLT
.L2736:
	movq	-6416(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -528(%rbp)
	je	.L2737
.L2952:
	movq	-6416(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$18, %edi
	movq	$0, -5696(%rbp)
	movaps	%xmm0, -5712(%rbp)
	call	_Znwm@PLT
	movl	$1800, %ecx
	movdqa	.LC8(%rip), %xmm0
	movq	%r14, %rsi
	movw	%cx, 16(%rax)
	movq	-6144(%rbp), %rdi
	leaq	18(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -5712(%rbp)
	movq	%rdx, -5696(%rbp)
	movq	%rdx, -5704(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-5712(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2738
	call	_ZdlPv@PLT
.L2738:
	movq	(%rbx), %rax
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	8(%rax), %rcx
	movq	88(%rax), %r9
	movq	104(%rax), %r10
	movq	%rsi, -6480(%rbp)
	movq	32(%rax), %rsi
	movq	80(%rax), %r11
	movq	%rdx, -6512(%rbp)
	movq	48(%rax), %rdx
	movq	%rdi, -6544(%rbp)
	movq	64(%rax), %rdi
	movq	(%rax), %rbx
	movq	%rcx, -6440(%rbp)
	movq	%r9, -6592(%rbp)
	movq	16(%rax), %rcx
	movq	96(%rax), %r9
	movq	%r10, -6624(%rbp)
	movq	112(%rax), %r10
	movq	%rsi, -6496(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -6528(%rbp)
	movl	$204, %edx
	movq	120(%rax), %r12
	movq	%rdi, -6560(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -6448(%rbp)
	movq	%r11, -6576(%rbp)
	movq	%r9, -6608(%rbp)
	movq	%r10, -6640(%rbp)
	movq	%rbx, -6416(%rbp)
	movq	72(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -6464(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-6464(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberAddENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r14, %rdi
	movq	%rax, -6648(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$207, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberAddENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r14, %rdi
	movq	%rax, -6464(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$187, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-208(%rbp), %rsi
	leaq	-80(%rbp), %rdx
	movq	%r14, %rdi
	movq	-6416(%rbp), %xmm0
	movq	$0, -5696(%rbp)
	movhps	-6440(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-6448(%rbp), %xmm0
	movhps	-6480(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-6496(%rbp), %xmm0
	movhps	-6512(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-6528(%rbp), %xmm0
	movhps	-6544(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-6560(%rbp), %xmm0
	movhps	-6648(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-6576(%rbp), %xmm0
	movhps	-6592(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-6608(%rbp), %xmm0
	movhps	-6624(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-6640(%rbp), %xmm0
	movhps	-6464(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -5712(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6160(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2739
	call	_ZdlPv@PLT
.L2739:
	movq	-6432(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -336(%rbp)
	je	.L2740
.L2953:
	movq	-6456(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -6032(%rbp)
	leaq	-5904(%rbp), %r12
	movq	$0, -6024(%rbp)
	movq	$0, -6016(%rbp)
	movq	$0, -6008(%rbp)
	movq	$0, -6000(%rbp)
	movq	$0, -5992(%rbp)
	movq	$0, -5984(%rbp)
	movq	$0, -5976(%rbp)
	movq	$0, -5968(%rbp)
	movq	$0, -5960(%rbp)
	movq	$0, -5952(%rbp)
	movq	$0, -5944(%rbp)
	movq	$0, -5936(%rbp)
	movq	$0, -5928(%rbp)
	movq	$0, -5920(%rbp)
	movq	$0, -5912(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5912(%rbp), %rax
	movq	-6400(%rbp), %rdi
	pushq	%rax
	leaq	-5920(%rbp), %rax
	leaq	-6016(%rbp), %rcx
	pushq	%rax
	leaq	-5928(%rbp), %rax
	leaq	-6000(%rbp), %r9
	pushq	%rax
	leaq	-5936(%rbp), %rax
	leaq	-6008(%rbp), %r8
	pushq	%rax
	leaq	-5944(%rbp), %rax
	leaq	-6024(%rbp), %rdx
	pushq	%rax
	leaq	-5952(%rbp), %rax
	leaq	-6032(%rbp), %rsi
	pushq	%rax
	leaq	-5960(%rbp), %rax
	pushq	%rax
	leaq	-5968(%rbp), %rax
	pushq	%rax
	leaq	-5976(%rbp), %rax
	pushq	%rax
	leaq	-5984(%rbp), %rax
	pushq	%rax
	leaq	-5992(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_SB_SB_S7_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESL_SP_SP_SL_SP_SP_SP_SN_SP_
	addq	$96, %rsp
	movl	$211, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal16kLengthString_68EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -6432(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5920(%rbp), %rax
	movq	%r12, %rdi
	movq	-5912(%rbp), %rbx
	movq	-6008(%rbp), %r13
	movq	%rax, -6416(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$712, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-5712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r13, %r9
	xorl	%esi, %esi
	movq	%r12, %rdi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movq	%rbx, -192(%rbp)
	movl	$3, %ebx
	movq	%rax, %r8
	pushq	%rbx
	movq	-5696(%rbp), %rax
	leaq	-5792(%rbp), %rdx
	movq	-6416(%rbp), %xmm0
	movq	%rcx, -5792(%rbp)
	leaq	-208(%rbp), %rcx
	pushq	%rcx
	movl	$1, %ecx
	movhps	-6432(%rbp), %xmm0
	movq	%rax, -5784(%rbp)
	movaps	%xmm0, -208(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$214, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-5920(%rbp), %rsi
	movq	-6424(%rbp), %rdi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	popq	%rax
	popq	%rdx
	jmp	.L2740
	.p2align 4,,10
	.p2align 3
.L2949:
	movq	-6432(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -6016(%rbp)
	leaq	-208(%rbp), %r12
	movq	$0, -6008(%rbp)
	movq	$0, -6000(%rbp)
	movq	$0, -5992(%rbp)
	movq	$0, -5984(%rbp)
	movq	$0, -5976(%rbp)
	movq	$0, -5968(%rbp)
	movq	$0, -5960(%rbp)
	movq	$0, -5952(%rbp)
	movq	$0, -5944(%rbp)
	movq	$0, -5936(%rbp)
	movq	$0, -5928(%rbp)
	movq	$0, -5920(%rbp)
	movq	$0, -5912(%rbp)
	movq	$0, -5904(%rbp)
	movq	$0, -5792(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-5792(%rbp), %rax
	movq	-6160(%rbp), %rdi
	pushq	%rax
	leaq	-5904(%rbp), %rax
	leaq	-6000(%rbp), %rcx
	pushq	%rax
	leaq	-5912(%rbp), %rax
	leaq	-5984(%rbp), %r9
	pushq	%rax
	leaq	-5920(%rbp), %rax
	leaq	-5992(%rbp), %r8
	pushq	%rax
	leaq	-5928(%rbp), %rax
	leaq	-6008(%rbp), %rdx
	pushq	%rax
	leaq	-5936(%rbp), %rax
	leaq	-6016(%rbp), %rsi
	pushq	%rax
	leaq	-5944(%rbp), %rax
	pushq	%rax
	leaq	-5952(%rbp), %rax
	pushq	%rax
	leaq	-5960(%rbp), %rax
	pushq	%rax
	leaq	-5968(%rbp), %rax
	pushq	%rax
	leaq	-5976(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7RawPtrTES3_NS0_7IntPtrTENS0_7ContextENS0_6ObjectENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES6_SB_SB_S6_SB_SB_SB_S7_SB_EE10CreatePhisEPNS1_5TNodeIS3_EESF_PNSD_IS4_EEPNSD_IS5_EEPNSD_IS6_EEPNSD_IS7_EEPNSD_ISB_EESL_SP_SP_SL_SP_SP_SP_SN_SP_
	movq	-5920(%rbp), %rdx
	addq	$96, %rsp
	movq	%r13, %rdi
	movq	-5944(%rbp), %rsi
	call	_ZN2v88internal19NumberIsLessThan_75EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	leaq	-80(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-6016(%rbp), %xmm0
	movq	%rax, %rbx
	movq	-5904(%rbp), %xmm7
	movq	-5920(%rbp), %xmm3
	movq	-5936(%rbp), %xmm6
	movq	-5952(%rbp), %xmm4
	movhps	-6008(%rbp), %xmm0
	movq	-5968(%rbp), %xmm5
	movhps	-5792(%rbp), %xmm7
	movq	-5984(%rbp), %xmm2
	movhps	-5912(%rbp), %xmm3
	movq	-6000(%rbp), %xmm1
	movhps	-5928(%rbp), %xmm6
	movhps	-5944(%rbp), %xmm4
	movhps	-5960(%rbp), %xmm5
	movaps	%xmm0, -6384(%rbp)
	movhps	-5976(%rbp), %xmm2
	movhps	-5992(%rbp), %xmm1
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm7, -6528(%rbp)
	movaps	%xmm3, -6544(%rbp)
	movaps	%xmm6, -6144(%rbp)
	movaps	%xmm4, -6512(%rbp)
	movaps	%xmm5, -6496(%rbp)
	movaps	%xmm2, -6480(%rbp)
	movaps	%xmm1, -6400(%rbp)
	movaps	%xmm1, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm0, -5712(%rbp)
	movq	%rdx, -6560(%rbp)
	movq	$0, -5696(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6360(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	movq	-6560(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L2730
	call	_ZdlPv@PLT
	movq	-6560(%rbp), %rdx
.L2730:
	movdqa	-6384(%rbp), %xmm1
	movdqa	-6400(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movdqa	-6480(%rbp), %xmm2
	movdqa	-6496(%rbp), %xmm3
	movq	%r14, %rdi
	movaps	%xmm0, -5712(%rbp)
	movdqa	-6512(%rbp), %xmm5
	movdqa	-6144(%rbp), %xmm4
	movaps	%xmm1, -208(%rbp)
	movdqa	-6528(%rbp), %xmm7
	movdqa	-6544(%rbp), %xmm1
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	$0, -5696(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-400(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -6400(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5712(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2731
	call	_ZdlPv@PLT
.L2731:
	movq	-6456(%rbp), %rcx
	movq	-6440(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L2729
	.p2align 4,,10
	.p2align 3
.L2955:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-6112(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movdqa	-6128(%rbp), %xmm7
	movdqa	-6224(%rbp), %xmm6
	movq	-6096(%rbp), %rax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	-152(%rbp), %rdx
	movaps	%xmm1, -208(%rbp)
	movq	%rax, -160(%rbp)
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm0, -5792(%rbp)
	movq	$0, -5776(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6176(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2666
	call	_ZdlPv@PLT
.L2666:
	movq	-6264(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2665
	.p2align 4,,10
	.p2align 3
.L2956:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-6160(%rbp), %xmm1
	movq	%r12, %rsi
	movdqa	-6144(%rbp), %xmm7
	movdqa	-6512(%rbp), %xmm6
	movdqa	-6560(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	leaq	-64(%rbp), %rdx
	movdqa	-6576(%rbp), %xmm3
	movdqa	-6640(%rbp), %xmm5
	movaps	%xmm1, -208(%rbp)
	movq	%rbx, %rdi
	movdqa	-6528(%rbp), %xmm1
	movdqa	-6624(%rbp), %xmm4
	movaps	%xmm7, -192(%rbp)
	movdqa	-6608(%rbp), %xmm7
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	movaps	%xmm0, -5792(%rbp)
	movq	$0, -5776(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-6192(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-5792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2717
	call	_ZdlPv@PLT
.L2717:
	movq	-6496(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2716
.L2954:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22588:
	.size	_ZN2v88internal28ArrayPrototypeSliceAssembler31GenerateArrayPrototypeSliceImplEv, .-_ZN2v88internal28ArrayPrototypeSliceAssembler31GenerateArrayPrototypeSliceImplEv
	.section	.rodata._ZN2v88internal8Builtins28Generate_ArrayPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/array-slice-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins28Generate_ArrayPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"ArrayPrototypeSlice"
	.section	.text._ZN2v88internal8Builtins28Generate_ArrayPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins28Generate_ArrayPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins28Generate_ArrayPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins28Generate_ArrayPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE:
.LFB22584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$2207, %ecx
	leaq	.LC10(%rip), %rdx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$778, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L2961
.L2958:
	movq	%r13, %rdi
	call	_ZN2v88internal28ArrayPrototypeSliceAssembler31GenerateArrayPrototypeSliceImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2962
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2961:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L2958
.L2962:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22584:
	.size	_ZN2v88internal8Builtins28Generate_ArrayPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins28Generate_ArrayPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal29HandleSimpleArgumentsSlice_38EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_27JSArgumentsObjectWithLengthEEENS4_INS0_3SmiEEESA_PNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal29HandleSimpleArgumentsSlice_38EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_27JSArgumentsObjectWithLengthEEENS4_INS0_3SmiEEESA_PNS1_18CodeAssemblerLabelE, @function
_GLOBAL__sub_I__ZN2v88internal29HandleSimpleArgumentsSlice_38EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_27JSArgumentsObjectWithLengthEEENS4_INS0_3SmiEEESA_PNS1_18CodeAssemblerLabelE:
.LFB30298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE30298:
	.size	_GLOBAL__sub_I__ZN2v88internal29HandleSimpleArgumentsSlice_38EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_27JSArgumentsObjectWithLengthEEENS4_INS0_3SmiEEESA_PNS1_18CodeAssemblerLabelE, .-_GLOBAL__sub_I__ZN2v88internal29HandleSimpleArgumentsSlice_38EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_27JSArgumentsObjectWithLengthEEENS4_INS0_3SmiEEESA_PNS1_18CodeAssemblerLabelE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal29HandleSimpleArgumentsSlice_38EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_13NativeContextEEENS4_INS0_27JSArgumentsObjectWithLengthEEENS4_INS0_3SmiEEESA_PNS1_18CodeAssemblerLabelE
	.weak	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.byte	7
	.byte	7
	.byte	6
	.byte	6
	.byte	7
	.byte	6
	.byte	6
	.byte	6
	.byte	7
	.byte	6
	.byte	7
	.byte	7
	.byte	7
	.byte	6
	.byte	7
	.byte	6
	.align 16
.LC6:
	.byte	7
	.byte	7
	.byte	6
	.byte	6
	.byte	7
	.byte	6
	.byte	6
	.byte	6
	.byte	7
	.byte	5
	.byte	5
	.byte	5
	.byte	7
	.byte	5
	.byte	5
	.byte	5
	.align 16
.LC7:
	.byte	7
	.byte	7
	.byte	6
	.byte	6
	.byte	7
	.byte	6
	.byte	6
	.byte	6
	.byte	7
	.byte	6
	.byte	7
	.byte	5
	.byte	5
	.byte	5
	.byte	7
	.byte	5
	.align 16
.LC8:
	.byte	5
	.byte	5
	.byte	5
	.byte	7
	.byte	8
	.byte	7
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	7
	.byte	8
	.align 16
.LC9:
	.byte	5
	.byte	5
	.byte	5
	.byte	7
	.byte	8
	.byte	7
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	7
	.byte	7
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
