	.file	"frames-tq-csa.cc"
	.text
	.section	.rodata._ZN2v88internal53FromConstexpr11ATFrameType21ATconstexpr_FrameType_291EPNS0_8compiler18CodeAssemblerStateENS0_10StackFrame4TypeE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../../deps/v8/../../deps/v8/src/builtins/frames.tq"
	.section	.text._ZN2v88internal53FromConstexpr11ATFrameType21ATconstexpr_FrameType_291EPNS0_8compiler18CodeAssemblerStateENS0_10StackFrame4TypeE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal53FromConstexpr11ATFrameType21ATconstexpr_FrameType_291EPNS0_8compiler18CodeAssemblerStateENS0_10StackFrame4TypeE
	.type	_ZN2v88internal53FromConstexpr11ATFrameType21ATconstexpr_FrameType_291EPNS0_8compiler18CodeAssemblerStateENS0_10StackFrame4TypeE, @function
_ZN2v88internal53FromConstexpr11ATFrameType21ATconstexpr_FrameType_291EPNS0_8compiler18CodeAssemblerStateENS0_10StackFrame4TypeE:
.LFB22410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-568(%rbp), %r14
	leaq	-184(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-656(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-664(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-376(%rbp), %rbx
	subq	$664, %rsp
	movq	%rdi, -688(%rbp)
	movl	%esi, -692(%rbp)
	movq	%r12, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-624(%rbp), %rax
	movq	%rdi, -664(%rbp)
	movq	%rdi, -624(%rbp)
	movq	%r14, %rdi
	movq	%rax, -680(%rbp)
	movq	$0, -616(%rbp)
	movq	$0, -608(%rbp)
	movq	$0, -600(%rbp)
	movq	$0, -592(%rbp)
	movq	$0, -584(%rbp)
	movq	$0, -576(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$24, %edi
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	%rax, -432(%rbp)
	movq	$0, -408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movq	$0, 16(%rax)
	movq	%rdx, -408(%rbp)
	movq	%rdx, -416(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -392(%rbp)
	movq	%rax, -424(%rbp)
	movq	$0, -400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$24, %edi
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r15, %rdi
	movq	$0, 16(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-680(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2
	call	_ZdlPv@PLT
.L2:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -560(%rbp)
	jne	.L71
.L3:
	leaq	-240(%rbp), %rax
	cmpq	$0, -368(%rbp)
	movq	%rax, -680(%rbp)
	jne	.L72
.L6:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-680(%rbp), %rdi
	movq	%r13, %rsi
	movb	$6, (%rax)
	leaq	1(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L9
	call	_ZdlPv@PLT
.L9:
	movq	0(%r13), %rax
	movq	%r15, %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	movq	-224(%rbp), %rax
	movq	-232(%rbp), %r15
	cmpq	%r15, %rax
	je	.L11
	.p2align 4,,10
	.p2align 3
.L15:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L12
	movq	%rax, -680(%rbp)
	addq	$24, %r15
	call	_ZdlPv@PLT
	movq	-680(%rbp), %rax
	cmpq	%r15, %rax
	jne	.L15
.L13:
	movq	-232(%rbp), %r15
.L11:
	testq	%r15, %r15
	je	.L16
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L16:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L17
	call	_ZdlPv@PLT
.L17:
	movq	-416(%rbp), %rbx
	movq	-424(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L18
	.p2align 4,,10
	.p2align 3
.L22:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L19
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L22
.L20:
	movq	-424(%rbp), %r15
.L18:
	testq	%r15, %r15
	je	.L23
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L23:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L24
	call	_ZdlPv@PLT
.L24:
	movq	-608(%rbp), %rbx
	movq	-616(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L25
	.p2align 4,,10
	.p2align 3
.L29:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L26
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L29
.L27:
	movq	-616(%rbp), %r14
.L25:
	testq	%r14, %r14
	je	.L30
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L30:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L73
	addq	$664, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L29
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L12:
	addq	$24, %r15
	cmpq	%r15, %rax
	jne	.L15
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L19:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L22
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L71:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-680(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4
	call	_ZdlPv@PLT
.L4:
	movl	$21, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-688(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	-692(%rbp), %esi
	movq	-688(%rbp), %rdi
	addq	%rsi, %rsi
	call	_ZN2v88internal48FromConstexpr9ATuintptr19ATconstexpr_uintptr_151EPNS0_8compiler18CodeAssemblerStateEm@PLT
	movq	%r13, %rdi
	movq	%rax, -680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-688(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-680(%rbp), %r8
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-680(%rbp), %rdx
	leaq	-432(%rbp), %rdi
	movq	%r13, %rsi
	leaq	8(%rax), %rcx
	movq	%rax, -656(%rbp)
	movq	%rdx, (%rax)
	movq	%rcx, -640(%rbp)
	movq	%rcx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5
	call	_ZdlPv@PLT
.L5:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L72:
	leaq	-432(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r8, -680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-680(%rbp), %r8
	movq	%r13, %rsi
	movb	$6, (%rax)
	leaq	1(%rax), %rdx
	movq	%r8, %rdi
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L7
	movq	%rax, -680(%rbp)
	call	_ZdlPv@PLT
	movq	-680(%rbp), %rax
.L7:
	movq	(%rax), %rax
	movl	$13, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	%rcx, -680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-680(%rbp), %rcx
	movq	%r13, %rsi
	leaq	8(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rcx, (%rax)
	leaq	-240(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	movq	%rax, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L8
	call	_ZdlPv@PLT
.L8:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L6
.L73:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22410:
	.size	_ZN2v88internal53FromConstexpr11ATFrameType21ATconstexpr_FrameType_291EPNS0_8compiler18CodeAssemblerStateENS0_10StackFrame4TypeE, .-_ZN2v88internal53FromConstexpr11ATFrameType21ATconstexpr_FrameType_291EPNS0_8compiler18CodeAssemblerStateENS0_10StackFrame4TypeE
	.section	.text._ZN2v88internal21Cast11ATFrameType_292EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Cast11ATFrameType_292EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal21Cast11ATFrameType_292EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal21Cast11ATFrameType_292EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE:
.LFB22420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1144(%rbp), %r14
	leaq	-184(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1240(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1200(%rbp), %rbx
	subq	$1272, %rsp
	movq	%rdi, -1296(%rbp)
	movq	%rdx, -1304(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1240(%rbp)
	movq	%rdi, -1200(%rbp)
	movl	$24, %edi
	movq	$0, -1192(%rbp)
	movq	$0, -1184(%rbp)
	movq	$0, -1176(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	$0, 16(%rax)
	movq	%rdx, -1176(%rbp)
	movq	%rdx, -1184(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1160(%rbp)
	movq	%rax, -1192(%rbp)
	movq	$0, -1168(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1240(%rbp), %rax
	movl	$24, %edi
	movq	$0, -1000(%rbp)
	movq	$0, -992(%rbp)
	movq	%rax, -1008(%rbp)
	movq	$0, -984(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 16(%rax)
	movq	%rax, -1000(%rbp)
	leaq	-952(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -984(%rbp)
	movq	%rdx, -992(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -968(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -976(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1240(%rbp), %rax
	movl	$24, %edi
	movq	$0, -808(%rbp)
	movq	$0, -800(%rbp)
	movq	%rax, -816(%rbp)
	movq	$0, -792(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 16(%rax)
	movq	%rax, -808(%rbp)
	leaq	-760(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -792(%rbp)
	movq	%rdx, -800(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -776(%rbp)
	movq	%rax, -1272(%rbp)
	movq	$0, -784(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1240(%rbp), %rax
	movl	$48, %edi
	movq	$0, -616(%rbp)
	movq	$0, -608(%rbp)
	movq	%rax, -624(%rbp)
	movq	$0, -600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movups	%xmm0, (%rax)
	leaq	48(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -616(%rbp)
	leaq	-568(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -600(%rbp)
	movq	%rdx, -608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -584(%rbp)
	movq	%rax, -1264(%rbp)
	movq	$0, -592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1240(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	$0, -424(%rbp)
	movq	%rax, -432(%rbp)
	leaq	-376(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1256(%rbp)
	movq	$0, -416(%rbp)
	movq	$0, -408(%rbp)
	movq	$0, -400(%rbp)
	movq	$0, -392(%rbp)
	movq	$0, -384(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1240(%rbp), %rax
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r15, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -1216(%rbp)
	movaps	%xmm0, -1232(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, (%rax)
	leaq	-1232(%rbp), %r13
	leaq	8(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -1232(%rbp)
	movq	%rdx, -1216(%rbp)
	movq	%rdx, -1224(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L75
	call	_ZdlPv@PLT
.L75:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1136(%rbp)
	jne	.L209
	cmpq	$0, -944(%rbp)
	jne	.L210
.L80:
	cmpq	$0, -752(%rbp)
	jne	.L211
.L83:
	cmpq	$0, -560(%rbp)
	leaq	-240(%rbp), %rbx
	jne	.L212
	cmpq	$0, -368(%rbp)
	jne	.L213
.L89:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1216(%rbp)
	movaps	%xmm0, -1232(%rbp)
	call	_Znwm@PLT
	movl	$1544, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1232(%rbp)
	movq	%rdx, -1216(%rbp)
	movq	%rdx, -1224(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1232(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L91
	call	_ZdlPv@PLT
.L91:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	8(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L92
	call	_ZdlPv@PLT
.L92:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L93
	.p2align 4,,10
	.p2align 3
.L97:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L94
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L97
.L95:
	movq	-232(%rbp), %r15
.L93:
	testq	%r15, %r15
	je	.L98
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L98:
	movq	-1256(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L99
	call	_ZdlPv@PLT
.L99:
	movq	-416(%rbp), %rbx
	movq	-424(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L100
	.p2align 4,,10
	.p2align 3
.L104:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L101
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L104
.L102:
	movq	-424(%rbp), %r15
.L100:
	testq	%r15, %r15
	je	.L105
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L105:
	movq	-1264(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L106
	call	_ZdlPv@PLT
.L106:
	movq	-608(%rbp), %rbx
	movq	-616(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L107
	.p2align 4,,10
	.p2align 3
.L111:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L108
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L111
.L109:
	movq	-616(%rbp), %r15
.L107:
	testq	%r15, %r15
	je	.L112
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L112:
	movq	-1272(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L113
	call	_ZdlPv@PLT
.L113:
	movq	-800(%rbp), %rbx
	movq	-808(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L114
	.p2align 4,,10
	.p2align 3
.L118:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L115
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L118
.L116:
	movq	-808(%rbp), %r15
.L114:
	testq	%r15, %r15
	je	.L119
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L119:
	movq	-1280(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-976(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L120
	call	_ZdlPv@PLT
.L120:
	movq	-992(%rbp), %rbx
	movq	-1000(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L121
	.p2align 4,,10
	.p2align 3
.L125:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L122
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L125
.L123:
	movq	-1000(%rbp), %r15
.L121:
	testq	%r15, %r15
	je	.L126
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L126:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L127
	call	_ZdlPv@PLT
.L127:
	movq	-1184(%rbp), %rbx
	movq	-1192(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L128
	.p2align 4,,10
	.p2align 3
.L132:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L129
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L132
.L130:
	movq	-1192(%rbp), %r14
.L128:
	testq	%r14, %r14
	je	.L133
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L133:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L214
	addq	$1272, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L132
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L122:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L125
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L115:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L118
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L108:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L111
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L94:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L97
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L101:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L104
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L209:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -1216(%rbp)
	movaps	%xmm0, -1232(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movb	$8, (%rax)
	leaq	1(%rax), %rdx
	movq	%rax, -1232(%rbp)
	movq	%rdx, -1216(%rbp)
	movq	%rdx, -1224(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1232(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L77
	call	_ZdlPv@PLT
.L77:
	movq	(%rbx), %rax
	movl	$25, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1296(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler14TaggedIsNotSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -1296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -1216(%rbp)
	movaps	%xmm0, -1232(%rbp)
	call	_Znwm@PLT
	leaq	-1008(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -1232(%rbp)
	movq	%rdx, -1216(%rbp)
	movq	%rdx, -1224(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L78
	call	_ZdlPv@PLT
.L78:
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -1216(%rbp)
	movaps	%xmm0, -1232(%rbp)
	call	_Znwm@PLT
	leaq	-816(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -1232(%rbp)
	movq	%rdx, -1216(%rbp)
	movq	%rdx, -1224(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L79
	call	_ZdlPv@PLT
.L79:
	movq	-1272(%rbp), %rcx
	movq	-1280(%rbp), %rdx
	movq	%r12, %rdi
	movq	-1296(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -944(%rbp)
	je	.L80
.L210:
	movq	-1280(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1008(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -1216(%rbp)
	movaps	%xmm0, -1232(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movb	$8, (%rax)
	leaq	1(%rax), %rdx
	movq	%rax, -1232(%rbp)
	movq	%rdx, -1216(%rbp)
	movq	%rdx, -1224(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L81
	call	_ZdlPv@PLT
.L81:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-432(%rbp), %rdi
	movq	$0, -1216(%rbp)
	movaps	%xmm0, -1232(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L82
	call	_ZdlPv@PLT
.L82:
	movq	-1256(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -752(%rbp)
	je	.L83
.L211:
	movq	-1272(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-816(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -1216(%rbp)
	movaps	%xmm0, -1232(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movb	$8, (%rax)
	leaq	1(%rax), %rdx
	movq	%rax, -1232(%rbp)
	movq	%rdx, -1216(%rbp)
	movq	%rdx, -1224(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1232(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L84
	call	_ZdlPv@PLT
.L84:
	movq	(%rbx), %rax
	movl	$29, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movq	$0, -1216(%rbp)
	movq	%rbx, %xmm0
	movaps	%xmm1, -1232(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -1296(%rbp)
	call	_Znwm@PLT
	movdqa	-1296(%rbp), %xmm0
	leaq	-624(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1232(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -1216(%rbp)
	movq	%rdx, -1224(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L85
	call	_ZdlPv@PLT
.L85:
	movq	-1264(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L212:
	movq	-1264(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-624(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1216(%rbp)
	movaps	%xmm0, -1232(%rbp)
	call	_Znwm@PLT
	movl	$1544, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -1232(%rbp)
	movq	%rdx, -1216(%rbp)
	movq	%rdx, -1224(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1232(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L87
	call	_ZdlPv@PLT
.L87:
	movq	(%rbx), %rax
	movl	$23, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -1296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movhps	-1296(%rbp), %xmm0
	movaps	%xmm1, -1232(%rbp)
	leaq	-240(%rbp), %rbx
	movaps	%xmm0, -1296(%rbp)
	movq	$0, -1216(%rbp)
	call	_Znwm@PLT
	movdqa	-1296(%rbp), %xmm0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1232(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -1216(%rbp)
	movq	%rdx, -1224(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L88
	call	_ZdlPv@PLT
.L88:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -368(%rbp)
	je	.L89
.L213:
	movq	-1256(%rbp), %rsi
	leaq	-432(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, -1296(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1296(%rbp), %r8
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	$0, -1216(%rbp)
	movq	%r8, %rdi
	movaps	%xmm0, -1232(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L90
	call	_ZdlPv@PLT
.L90:
	movq	-1304(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L89
.L214:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22420:
	.size	_ZN2v88internal21Cast11ATFrameType_292EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, .-_ZN2v88internal21Cast11ATFrameType_292EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal23LoadObjectFromFrame_293EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEEi,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23LoadObjectFromFrame_293EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEEi
	.type	_ZN2v88internal23LoadObjectFromFrame_293EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEEi, @function
_ZN2v88internal23LoadObjectFromFrame_293EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEEi:
.LFB22427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-568(%rbp), %r14
	leaq	-184(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-664(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$664, %rsp
	movl	%edx, -684(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -664(%rbp)
	movq	%rdi, -624(%rbp)
	movl	$24, %edi
	movq	$0, -616(%rbp)
	movq	$0, -608(%rbp)
	movq	$0, -600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	$0, 16(%rax)
	movq	%rax, -616(%rbp)
	leaq	-624(%rbp), %rax
	movq	%rdx, -600(%rbp)
	movq	%rdx, -608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -584(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$48, %edi
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	%rax, -432(%rbp)
	movq	$0, -408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-376(%rbp), %rcx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -408(%rbp)
	movq	%rdx, -416(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -680(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -392(%rbp)
	movq	%rax, -424(%rbp)
	movq	$0, -400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r15, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-704(%rbp), %rdi
	movq	%r13, (%rax)
	leaq	-656(%rbp), %r13
	leaq	8(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L216
	call	_ZdlPv@PLT
.L216:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -560(%rbp)
	jne	.L284
.L217:
	cmpq	$0, -368(%rbp)
	leaq	-240(%rbp), %rbx
	jne	.L285
.L220:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movl	$2053, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L223
	call	_ZdlPv@PLT
.L223:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	8(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L224
	call	_ZdlPv@PLT
.L224:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L225
	.p2align 4,,10
	.p2align 3
.L229:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L226
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L229
.L227:
	movq	-232(%rbp), %r15
.L225:
	testq	%r15, %r15
	je	.L230
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L230:
	movq	-680(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L231
	call	_ZdlPv@PLT
.L231:
	movq	-416(%rbp), %rbx
	movq	-424(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L232
	.p2align 4,,10
	.p2align 3
.L236:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L233
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L236
.L234:
	movq	-424(%rbp), %r15
.L232:
	testq	%r15, %r15
	je	.L237
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L237:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L238
	call	_ZdlPv@PLT
.L238:
	movq	-608(%rbp), %rbx
	movq	-616(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L239
	.p2align 4,,10
	.p2align 3
.L243:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L240
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L243
.L241:
	movq	-616(%rbp), %r14
.L239:
	testq	%r14, %r14
	je	.L244
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L244:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L286
	addq	$664, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L243
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L226:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L229
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L233:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L236
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L284:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-704(%rbp), %rdi
	movq	%r13, %rsi
	movb	$5, (%rax)
	leaq	1(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L218
	movq	%rax, -704(%rbp)
	call	_ZdlPv@PLT
	movq	-704(%rbp), %rax
.L218:
	movq	(%rax), %rax
	movl	$44, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-704(%rbp), %rbx
	movl	$1800, %ecx
	movq	%r13, %rdi
	movl	-684(%rbp), %edx
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler16LoadBufferObjectEPNS0_8compiler4NodeEiNS0_11MachineTypeE@PLT
	movq	%r13, %rdi
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm0
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movhps	-704(%rbp), %xmm0
	movaps	%xmm1, -656(%rbp)
	movaps	%xmm0, -704(%rbp)
	movq	$0, -640(%rbp)
	call	_Znwm@PLT
	movdqa	-704(%rbp), %xmm0
	leaq	-432(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -656(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L219
	call	_ZdlPv@PLT
.L219:
	movq	-680(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L285:
	movq	-680(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-432(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movl	$2053, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L221
	call	_ZdlPv@PLT
.L221:
	movq	(%rbx), %rax
	movl	$43, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movhps	-704(%rbp), %xmm0
	movaps	%xmm1, -656(%rbp)
	leaq	-240(%rbp), %rbx
	movaps	%xmm0, -704(%rbp)
	movq	$0, -640(%rbp)
	call	_Znwm@PLT
	movdqa	-704(%rbp), %xmm0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -656(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L222
	call	_ZdlPv@PLT
.L222:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L220
.L286:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22427:
	.size	_ZN2v88internal23LoadObjectFromFrame_293EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEEi, .-_ZN2v88internal23LoadObjectFromFrame_293EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEEi
	.section	.text._ZN2v88internal24LoadPointerFromFrame_294EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEEi,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24LoadPointerFromFrame_294EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEEi
	.type	_ZN2v88internal24LoadPointerFromFrame_294EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEEi, @function
_ZN2v88internal24LoadPointerFromFrame_294EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEEi:
.LFB22434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-568(%rbp), %r14
	leaq	-184(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-664(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$664, %rsp
	movl	%edx, -684(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -664(%rbp)
	movq	%rdi, -624(%rbp)
	movl	$24, %edi
	movq	$0, -616(%rbp)
	movq	$0, -608(%rbp)
	movq	$0, -600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	$0, 16(%rax)
	movq	%rax, -616(%rbp)
	leaq	-624(%rbp), %rax
	movq	%rdx, -600(%rbp)
	movq	%rdx, -608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -584(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$48, %edi
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	%rax, -432(%rbp)
	movq	$0, -408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-376(%rbp), %rcx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -408(%rbp)
	movq	%rdx, -416(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -680(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -392(%rbp)
	movq	%rax, -424(%rbp)
	movq	$0, -400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r15, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-704(%rbp), %rdi
	movq	%r13, (%rax)
	leaq	-656(%rbp), %r13
	leaq	8(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L288
	call	_ZdlPv@PLT
.L288:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -560(%rbp)
	jne	.L356
.L289:
	cmpq	$0, -368(%rbp)
	leaq	-240(%rbp), %rbx
	jne	.L357
.L292:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movl	$1285, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L295
	call	_ZdlPv@PLT
.L295:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	8(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L296
	call	_ZdlPv@PLT
.L296:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L297
	.p2align 4,,10
	.p2align 3
.L301:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L298
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L301
.L299:
	movq	-232(%rbp), %r15
.L297:
	testq	%r15, %r15
	je	.L302
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L302:
	movq	-680(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L303
	call	_ZdlPv@PLT
.L303:
	movq	-416(%rbp), %rbx
	movq	-424(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L304
	.p2align 4,,10
	.p2align 3
.L308:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L305
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L308
.L306:
	movq	-424(%rbp), %r15
.L304:
	testq	%r15, %r15
	je	.L309
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L309:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L310
	call	_ZdlPv@PLT
.L310:
	movq	-608(%rbp), %rbx
	movq	-616(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L311
	.p2align 4,,10
	.p2align 3
.L315:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L312
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L315
.L313:
	movq	-616(%rbp), %r14
.L311:
	testq	%r14, %r14
	je	.L316
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L316:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L358
	addq	$664, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L312:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L315
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L298:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L301
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L305:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L308
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L356:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-704(%rbp), %rdi
	movq	%r13, %rsi
	movb	$5, (%rax)
	leaq	1(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L290
	movq	%rax, -704(%rbp)
	call	_ZdlPv@PLT
	movq	-704(%rbp), %rax
.L290:
	movq	(%rax), %rax
	movl	$47, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-704(%rbp), %rbx
	movl	$5, %ecx
	movq	%r13, %rdi
	movl	-684(%rbp), %edx
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler16LoadBufferObjectEPNS0_8compiler4NodeEiNS0_11MachineTypeE@PLT
	movq	%r13, %rdi
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm0
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movhps	-704(%rbp), %xmm0
	movaps	%xmm1, -656(%rbp)
	movaps	%xmm0, -704(%rbp)
	movq	$0, -640(%rbp)
	call	_Znwm@PLT
	movdqa	-704(%rbp), %xmm0
	leaq	-432(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -656(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L291
	call	_ZdlPv@PLT
.L291:
	movq	-680(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L357:
	movq	-680(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-432(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movl	$1285, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L293
	call	_ZdlPv@PLT
.L293:
	movq	(%rbx), %rax
	movl	$46, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movhps	-704(%rbp), %xmm0
	movaps	%xmm1, -656(%rbp)
	leaq	-240(%rbp), %rbx
	movaps	%xmm0, -704(%rbp)
	movq	$0, -640(%rbp)
	call	_Znwm@PLT
	movdqa	-704(%rbp), %xmm0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -656(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L294
	call	_ZdlPv@PLT
.L294:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L292
.L358:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22434:
	.size	_ZN2v88internal24LoadPointerFromFrame_294EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEEi, .-_ZN2v88internal24LoadPointerFromFrame_294EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEEi
	.section	.text._ZN2v88internal20LoadSmiFromFrame_295EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEEi,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20LoadSmiFromFrame_295EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEEi
	.type	_ZN2v88internal20LoadSmiFromFrame_295EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEEi, @function
_ZN2v88internal20LoadSmiFromFrame_295EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEEi:
.LFB22438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-568(%rbp), %r14
	leaq	-184(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-664(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$664, %rsp
	movl	%edx, -684(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -664(%rbp)
	movq	%rdi, -624(%rbp)
	movl	$24, %edi
	movq	$0, -616(%rbp)
	movq	$0, -608(%rbp)
	movq	$0, -600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	$0, 16(%rax)
	movq	%rax, -616(%rbp)
	leaq	-624(%rbp), %rax
	movq	%rdx, -600(%rbp)
	movq	%rdx, -608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -584(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$48, %edi
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	%rax, -432(%rbp)
	movq	$0, -408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-376(%rbp), %rcx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -408(%rbp)
	movq	%rdx, -416(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -680(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -392(%rbp)
	movq	%rax, -424(%rbp)
	movq	$0, -400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r15, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-704(%rbp), %rdi
	movq	%r13, (%rax)
	leaq	-656(%rbp), %r13
	leaq	8(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L360
	call	_ZdlPv@PLT
.L360:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -560(%rbp)
	jne	.L428
.L361:
	cmpq	$0, -368(%rbp)
	leaq	-240(%rbp), %rbx
	jne	.L429
.L364:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movl	$1541, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L367
	call	_ZdlPv@PLT
.L367:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	8(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L368
	call	_ZdlPv@PLT
.L368:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L369
	.p2align 4,,10
	.p2align 3
.L373:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L370
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L373
.L371:
	movq	-232(%rbp), %r15
.L369:
	testq	%r15, %r15
	je	.L374
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L374:
	movq	-680(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L375
	call	_ZdlPv@PLT
.L375:
	movq	-416(%rbp), %rbx
	movq	-424(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L376
	.p2align 4,,10
	.p2align 3
.L380:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L377
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L380
.L378:
	movq	-424(%rbp), %r15
.L376:
	testq	%r15, %r15
	je	.L381
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L381:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L382
	call	_ZdlPv@PLT
.L382:
	movq	-608(%rbp), %rbx
	movq	-616(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L383
	.p2align 4,,10
	.p2align 3
.L387:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L384
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L387
.L385:
	movq	-616(%rbp), %r14
.L383:
	testq	%r14, %r14
	je	.L388
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L388:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L430
	addq	$664, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L384:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L387
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L370:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L373
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L377:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L380
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L428:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-704(%rbp), %rdi
	movq	%r13, %rsi
	movb	$5, (%rax)
	leaq	1(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L362
	movq	%rax, -704(%rbp)
	call	_ZdlPv@PLT
	movq	-704(%rbp), %rax
.L362:
	movq	(%rax), %rax
	movl	$50, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-704(%rbp), %rbx
	movl	$518, %ecx
	movq	%r13, %rdi
	movl	-684(%rbp), %edx
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssembler16LoadBufferObjectEPNS0_8compiler4NodeEiNS0_11MachineTypeE@PLT
	movq	%r13, %rdi
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm0
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movhps	-704(%rbp), %xmm0
	movaps	%xmm1, -656(%rbp)
	movaps	%xmm0, -704(%rbp)
	movq	$0, -640(%rbp)
	call	_Znwm@PLT
	movdqa	-704(%rbp), %xmm0
	leaq	-432(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -656(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L363
	call	_ZdlPv@PLT
.L363:
	movq	-680(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L429:
	movq	-680(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-432(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movl	$1541, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L365
	call	_ZdlPv@PLT
.L365:
	movq	(%rbx), %rax
	movl	$49, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movhps	-704(%rbp), %xmm0
	movaps	%xmm1, -656(%rbp)
	leaq	-240(%rbp), %rbx
	movaps	%xmm0, -704(%rbp)
	movq	$0, -640(%rbp)
	call	_Znwm@PLT
	movdqa	-704(%rbp), %xmm0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -656(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L366
	call	_ZdlPv@PLT
.L366:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L364
.L430:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22438:
	.size	_ZN2v88internal20LoadSmiFromFrame_295EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEEi, .-_ZN2v88internal20LoadSmiFromFrame_295EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEEi
	.section	.text._ZN2v88internal25LoadFunctionFromFrame_296EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25LoadFunctionFromFrame_296EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEE
	.type	_ZN2v88internal25LoadFunctionFromFrame_296EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEE, @function
_ZN2v88internal25LoadFunctionFromFrame_296EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEE:
.LFB22442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-568(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-664(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$664, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -664(%rbp)
	movq	%rdi, -624(%rbp)
	movl	$24, %edi
	movq	$0, -616(%rbp)
	movq	$0, -608(%rbp)
	movq	$0, -600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movq	$0, 16(%rax)
	movq	%rax, -616(%rbp)
	leaq	-624(%rbp), %rax
	movq	%rdx, -600(%rbp)
	movq	%rdx, -608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -584(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$48, %edi
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	%rax, -432(%rbp)
	movq	$0, -408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-376(%rbp), %rcx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -408(%rbp)
	movq	%rdx, -416(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -680(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -392(%rbp)
	movq	%rax, -424(%rbp)
	movq	$0, -400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-704(%rbp), %rdi
	movq	%r15, (%rax)
	leaq	-656(%rbp), %r15
	leaq	8(%rax), %rdx
	movq	%r15, %rsi
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L432
	call	_ZdlPv@PLT
.L432:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -560(%rbp)
	jne	.L500
.L433:
	cmpq	$0, -368(%rbp)
	leaq	-240(%rbp), %rbx
	jne	.L501
.L436:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movl	$1797, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L439
	call	_ZdlPv@PLT
.L439:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	8(%rax), %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L440
	call	_ZdlPv@PLT
.L440:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L441
	.p2align 4,,10
	.p2align 3
.L445:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L442
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L445
.L443:
	movq	-232(%rbp), %r14
.L441:
	testq	%r14, %r14
	je	.L446
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L446:
	movq	-680(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L447
	call	_ZdlPv@PLT
.L447:
	movq	-416(%rbp), %rbx
	movq	-424(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L448
	.p2align 4,,10
	.p2align 3
.L452:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L449
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L452
.L450:
	movq	-424(%rbp), %r14
.L448:
	testq	%r14, %r14
	je	.L453
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L453:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L454
	call	_ZdlPv@PLT
.L454:
	movq	-608(%rbp), %rbx
	movq	-616(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L455
	.p2align 4,,10
	.p2align 3
.L459:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L456
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L459
.L457:
	movq	-616(%rbp), %r13
.L455:
	testq	%r13, %r13
	je	.L460
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L460:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L502
	addq	$664, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L456:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L459
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L442:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L445
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L449:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L452
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L500:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-704(%rbp), %rdi
	movq	%r15, %rsi
	movb	$5, (%rax)
	leaq	1(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L434
	movq	%rax, -704(%rbp)
	call	_ZdlPv@PLT
	movq	-704(%rbp), %rax
.L434:
	movq	(%rax), %rax
	movl	$59, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-704(%rbp), %rsi
	movl	$-16, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal23LoadObjectFromFrame_293EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEEi
	movl	$60, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -688(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movq	-704(%rbp), %xmm0
	movaps	%xmm1, -656(%rbp)
	movhps	-688(%rbp), %xmm0
	movq	$0, -640(%rbp)
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movdqa	-704(%rbp), %xmm0
	leaq	-432(%rbp), %rdi
	movq	%r15, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -656(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L435
	call	_ZdlPv@PLT
.L435:
	movq	-680(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L501:
	movq	-680(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-432(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movl	$1797, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r15, %rsi
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L437
	call	_ZdlPv@PLT
.L437:
	movq	(%rbx), %rax
	movl	$55, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movhps	-704(%rbp), %xmm0
	movaps	%xmm1, -656(%rbp)
	leaq	-240(%rbp), %rbx
	movaps	%xmm0, -704(%rbp)
	movq	$0, -640(%rbp)
	call	_Znwm@PLT
	movdqa	-704(%rbp), %xmm0
	movq	%rbx, %rdi
	movq	%r15, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -656(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L438
	call	_ZdlPv@PLT
.L438:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L436
.L502:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22442:
	.size	_ZN2v88internal25LoadFunctionFromFrame_296EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEE, .-_ZN2v88internal25LoadFunctionFromFrame_296EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEE
	.section	.text._ZN2v88internal23LoadCallerFromFrame_297EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23LoadCallerFromFrame_297EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEE
	.type	_ZN2v88internal23LoadCallerFromFrame_297EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEE, @function
_ZN2v88internal23LoadCallerFromFrame_297EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEE:
.LFB22446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-568(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-664(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$664, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -664(%rbp)
	movq	%rdi, -624(%rbp)
	movl	$24, %edi
	movq	$0, -616(%rbp)
	movq	$0, -608(%rbp)
	movq	$0, -600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movq	$0, 16(%rax)
	movq	%rax, -616(%rbp)
	leaq	-624(%rbp), %rax
	movq	%rdx, -600(%rbp)
	movq	%rdx, -608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -584(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$48, %edi
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	%rax, -432(%rbp)
	movq	$0, -408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-376(%rbp), %rcx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -408(%rbp)
	movq	%rdx, -416(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -680(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -392(%rbp)
	movq	%rax, -424(%rbp)
	movq	$0, -400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$48, %edi
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-704(%rbp), %rdi
	movq	%r15, (%rax)
	leaq	-656(%rbp), %r15
	leaq	8(%rax), %rdx
	movq	%r15, %rsi
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L504
	call	_ZdlPv@PLT
.L504:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -560(%rbp)
	jne	.L572
.L505:
	cmpq	$0, -368(%rbp)
	leaq	-240(%rbp), %rbx
	jne	.L573
.L508:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movl	$1285, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movw	%cx, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L511
	call	_ZdlPv@PLT
.L511:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	8(%rax), %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L512
	call	_ZdlPv@PLT
.L512:
	movq	-224(%rbp), %rbx
	movq	-232(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L513
	.p2align 4,,10
	.p2align 3
.L517:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L514
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L517
.L515:
	movq	-232(%rbp), %r14
.L513:
	testq	%r14, %r14
	je	.L518
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L518:
	movq	-680(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L519
	call	_ZdlPv@PLT
.L519:
	movq	-416(%rbp), %rbx
	movq	-424(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L520
	.p2align 4,,10
	.p2align 3
.L524:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L521
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L524
.L522:
	movq	-424(%rbp), %r14
.L520:
	testq	%r14, %r14
	je	.L525
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L525:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L526
	call	_ZdlPv@PLT
.L526:
	movq	-608(%rbp), %rbx
	movq	-616(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L527
	.p2align 4,,10
	.p2align 3
.L531:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L528
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L531
.L529:
	movq	-616(%rbp), %r13
.L527:
	testq	%r13, %r13
	je	.L532
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L532:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L574
	addq	$664, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L528:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L531
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L514:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L517
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L521:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L524
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L572:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-704(%rbp), %rdi
	movq	%r15, %rsi
	movb	$5, (%rax)
	leaq	1(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L506
	movq	%rax, -704(%rbp)
	call	_ZdlPv@PLT
	movq	-704(%rbp), %rax
.L506:
	movq	(%rax), %rax
	movl	$66, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-704(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal24LoadPointerFromFrame_294EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEEi
	movl	$67, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -688(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movq	-704(%rbp), %xmm0
	movaps	%xmm1, -656(%rbp)
	movhps	-688(%rbp), %xmm0
	movq	$0, -640(%rbp)
	movaps	%xmm0, -704(%rbp)
	call	_Znwm@PLT
	movdqa	-704(%rbp), %xmm0
	leaq	-432(%rbp), %rdi
	movq	%r15, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -656(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L507
	call	_ZdlPv@PLT
.L507:
	movq	-680(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L573:
	movq	-680(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-432(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movl	$1285, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	2(%rax), %rdx
	movq	%r15, %rsi
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L509
	call	_ZdlPv@PLT
.L509:
	movq	(%rbx), %rax
	movl	$65, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %rax
	movq	%rax, -704(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	pxor	%xmm1, %xmm1
	movl	$16, %edi
	movhps	-704(%rbp), %xmm0
	movaps	%xmm1, -656(%rbp)
	leaq	-240(%rbp), %rbx
	movaps	%xmm0, -704(%rbp)
	movq	$0, -640(%rbp)
	call	_Znwm@PLT
	movdqa	-704(%rbp), %xmm0
	movq	%rbx, %rdi
	movq	%r15, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -656(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L510
	call	_ZdlPv@PLT
.L510:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L508
.L574:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22446:
	.size	_ZN2v88internal23LoadCallerFromFrame_297EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEE, .-_ZN2v88internal23LoadCallerFromFrame_297EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEE
	.section	.text._ZN2v88internal30LoadLengthFromAdapterFrame_300EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30LoadLengthFromAdapterFrame_300EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEE
	.type	_ZN2v88internal30LoadLengthFromAdapterFrame_300EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEE, @function
_ZN2v88internal30LoadLengthFromAdapterFrame_300EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEE:
.LFB22470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-216(%rbp), %r14
	leaq	-688(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-600(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-696(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-656(%rbp), %rbx
	subq	$712, %rsp
	movq	%rdi, -744(%rbp)
	movq	%rsi, -736(%rbp)
	movq	%rdx, -720(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -696(%rbp)
	movq	%rdi, -656(%rbp)
	movl	$48, %edi
	movq	$0, -648(%rbp)
	movq	$0, -640(%rbp)
	movq	$0, -632(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -632(%rbp)
	movq	%rdx, -640(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -616(%rbp)
	movq	%rax, -648(%rbp)
	movq	$0, -624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	%rax, -464(%rbp)
	movq	$0, -440(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -456(%rbp)
	leaq	-408(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -440(%rbp)
	movq	%rdx, -448(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -424(%rbp)
	movq	%rax, -712(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	%rax, -272(%rbp)
	movq	$0, -248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -248(%rbp)
	movq	%rdx, -256(%rbp)
	xorl	%edx, %edx
	movq	%rax, -264(%rbp)
	movups	%xmm0, -232(%rbp)
	movq	$0, -240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-736(%rbp), %xmm1
	movaps	%xmm0, -688(%rbp)
	movhps	-720(%rbp), %xmm1
	movq	$0, -672(%rbp)
	movaps	%xmm1, -736(%rbp)
	call	_Znwm@PLT
	movdqa	-736(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -688(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L576
	call	_ZdlPv@PLT
.L576:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -592(%rbp)
	jne	.L644
.L577:
	cmpq	$0, -400(%rbp)
	leaq	-272(%rbp), %rbx
	jne	.L645
.L580:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$1287, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$6, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L583
	call	_ZdlPv@PLT
.L583:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	16(%rax), %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L584
	call	_ZdlPv@PLT
.L584:
	movq	-256(%rbp), %rbx
	movq	-264(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L585
	.p2align 4,,10
	.p2align 3
.L589:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L586
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L589
.L587:
	movq	-264(%rbp), %r14
.L585:
	testq	%r14, %r14
	je	.L590
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L590:
	movq	-712(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L591
	call	_ZdlPv@PLT
.L591:
	movq	-448(%rbp), %rbx
	movq	-456(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L592
	.p2align 4,,10
	.p2align 3
.L596:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L593
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L596
.L594:
	movq	-456(%rbp), %r14
.L592:
	testq	%r14, %r14
	je	.L597
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L597:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L598
	call	_ZdlPv@PLT
.L598:
	movq	-640(%rbp), %rbx
	movq	-648(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L599
	.p2align 4,,10
	.p2align 3
.L603:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L600
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L603
.L601:
	movq	-648(%rbp), %r13
.L599:
	testq	%r13, %r13
	je	.L604
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L604:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L646
	addq	$712, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L600:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L603
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L586:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L589
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L593:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L596
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L644:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$1287, %edi
	movq	%r15, %rsi
	movw	%di, (%rax)
	leaq	2(%rax), %rdx
	movq	%rbx, %rdi
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L578
	call	_ZdlPv@PLT
.L578:
	movq	(%rbx), %rax
	movl	$101, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	%rcx, -736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-744(%rbp), %rdi
	movl	$-24, %edx
	movq	%rbx, %rsi
	call	_ZN2v88internal20LoadSmiFromFrame_295EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEEi
	movq	%rbx, %xmm2
	movl	$24, %edi
	movq	-736(%rbp), %xmm0
	movq	%rax, -64(%rbp)
	movq	$0, -672(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm3
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-464(%rbp), %rdi
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L579
	call	_ZdlPv@PLT
.L579:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L645:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-464(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$1287, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rsi
	movb	$6, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L581
	call	_ZdlPv@PLT
.L581:
	movq	(%rbx), %rax
	movl	$98, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -720(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %edi
	movq	%rbx, -64(%rbp)
	movq	-736(%rbp), %xmm0
	movq	$0, -672(%rbp)
	leaq	-272(%rbp), %rbx
	movhps	-720(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movdqa	-80(%rbp), %xmm4
	leaq	24(%rax), %rdx
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L582
	call	_ZdlPv@PLT
.L582:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L580
.L646:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22470:
	.size	_ZN2v88internal30LoadLengthFromAdapterFrame_300EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEE, .-_ZN2v88internal30LoadLengthFromAdapterFrame_300EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEE
	.section	.text._ZN2v88internal19FrameTypeEquals_301EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEES6_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19FrameTypeEquals_301EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEES6_
	.type	_ZN2v88internal19FrameTypeEquals_301EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEES6_, @function
_ZN2v88internal19FrameTypeEquals_301EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEES6_:
.LFB22474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-600(%rbp), %r14
	leaq	-216(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-688(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-696(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$712, %rsp
	movq	%rsi, -736(%rbp)
	movq	%rdx, -744(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -696(%rbp)
	movq	%rdi, -656(%rbp)
	movl	$48, %edi
	movq	$0, -648(%rbp)
	movq	$0, -640(%rbp)
	movq	$0, -632(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -648(%rbp)
	leaq	-656(%rbp), %rax
	movq	%rdx, -632(%rbp)
	movq	%rdx, -640(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -616(%rbp)
	movq	%rax, -720(%rbp)
	movq	$0, -624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	%rax, -464(%rbp)
	movq	$0, -440(%rbp)
	call	_Znwm@PLT
	leaq	-408(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -440(%rbp)
	movq	%rdx, -448(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -712(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -424(%rbp)
	movq	%rax, -456(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	%rax, -272(%rbp)
	movq	$0, -248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r15, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -248(%rbp)
	movq	%rdx, -256(%rbp)
	xorl	%edx, %edx
	movq	%rax, -264(%rbp)
	movups	%xmm0, -232(%rbp)
	movq	$0, -240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-736(%rbp), %xmm1
	movaps	%xmm0, -688(%rbp)
	movhps	-744(%rbp), %xmm1
	movq	$0, -672(%rbp)
	movaps	%xmm1, -736(%rbp)
	call	_Znwm@PLT
	movdqa	-736(%rbp), %xmm1
	movq	-720(%rbp), %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -688(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L648
	call	_ZdlPv@PLT
.L648:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -592(%rbp)
	jne	.L716
.L649:
	cmpq	$0, -400(%rbp)
	leaq	-272(%rbp), %rbx
	jne	.L717
.L652:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$1542, %ecx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$4, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L655
	call	_ZdlPv@PLT
.L655:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L656
	call	_ZdlPv@PLT
.L656:
	movq	-256(%rbp), %rbx
	movq	-264(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L657
	.p2align 4,,10
	.p2align 3
.L661:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L658
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L661
.L659:
	movq	-264(%rbp), %r15
.L657:
	testq	%r15, %r15
	je	.L662
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L662:
	movq	-712(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L663
	call	_ZdlPv@PLT
.L663:
	movq	-448(%rbp), %rbx
	movq	-456(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L664
	.p2align 4,,10
	.p2align 3
.L668:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L665
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L668
.L666:
	movq	-456(%rbp), %r15
.L664:
	testq	%r15, %r15
	je	.L669
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L669:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L670
	call	_ZdlPv@PLT
.L670:
	movq	-640(%rbp), %rbx
	movq	-648(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L671
	.p2align 4,,10
	.p2align 3
.L675:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L672
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L675
.L673:
	movq	-648(%rbp), %r14
.L671:
	testq	%r14, %r14
	je	.L676
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L676:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L718
	addq	$712, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L672:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L675
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L658:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L661
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L665:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L668
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L716:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$1542, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	movq	-720(%rbp), %rdi
	leaq	2(%rax), %rdx
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L650
	movq	%rax, -720(%rbp)
	call	_ZdlPv@PLT
	movq	-720(%rbp), %rax
.L650:
	movq	(%rax), %rax
	movl	$105, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rcx, -720(%rbp)
	movq	%rax, -736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-736(%rbp), %rdx
	movq	-720(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$24, %edi
	movq	%rbx, -64(%rbp)
	movq	-720(%rbp), %xmm0
	movq	$0, -672(%rbp)
	movhps	-736(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm2
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-464(%rbp), %rdi
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L651
	call	_ZdlPv@PLT
.L651:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L649
	.p2align 4,,10
	.p2align 3
.L717:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-464(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$1542, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$4, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L653
	call	_ZdlPv@PLT
.L653:
	movq	(%rbx), %rax
	movl	$104, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -736(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -720(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %edi
	movq	%rbx, -64(%rbp)
	movq	-720(%rbp), %xmm0
	movq	$0, -672(%rbp)
	leaq	-272(%rbp), %rbx
	movhps	-736(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	-80(%rbp), %xmm3
	leaq	24(%rax), %rdx
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L654
	call	_ZdlPv@PLT
.L654:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L652
.L718:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22474:
	.size	_ZN2v88internal19FrameTypeEquals_301EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEES6_, .-_ZN2v88internal19FrameTypeEquals_301EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEES6_
	.section	.text._ZN2v88internal23LoadTargetFromFrame_304EPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23LoadTargetFromFrame_304EPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal23LoadTargetFromFrame_304EPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal23LoadTargetFromFrame_304EPNS0_8compiler18CodeAssemblerStateE:
.LFB22495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-568(%rbp), %r14
	leaq	-624(%rbp), %rax
	pushq	%r13
	leaq	-184(%rbp), %r15
	.cfi_offset 13, -40
	leaq	-656(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-664(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -56
	leaq	-376(%rbp), %rbx
	subq	$648, %rsp
	movq	%rdi, -688(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	%rdi, -664(%rbp)
	movq	%rdi, -624(%rbp)
	movq	%r14, %rdi
	movq	%rax, -680(%rbp)
	movq	$0, -616(%rbp)
	movq	$0, -608(%rbp)
	movq	$0, -600(%rbp)
	movq	$0, -592(%rbp)
	movq	$0, -584(%rbp)
	movq	$0, -576(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$24, %edi
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	%rax, -432(%rbp)
	movq	$0, -408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movq	$0, 16(%rax)
	movq	%rdx, -408(%rbp)
	movq	%rdx, -416(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -392(%rbp)
	movq	%rax, -424(%rbp)
	movq	$0, -400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$24, %edi
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r15, %rdi
	movq	$0, 16(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-680(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L720
	call	_ZdlPv@PLT
.L720:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -560(%rbp)
	jne	.L788
.L721:
	leaq	-240(%rbp), %rax
	cmpq	$0, -368(%rbp)
	movq	%rax, -680(%rbp)
	jne	.L789
.L724:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-680(%rbp), %rdi
	movq	%r13, %rsi
	movb	$7, (%rax)
	leaq	1(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L727
	call	_ZdlPv@PLT
.L727:
	movq	0(%r13), %rax
	movq	%r15, %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L728
	call	_ZdlPv@PLT
.L728:
	movq	-224(%rbp), %rax
	movq	-232(%rbp), %r15
	cmpq	%r15, %rax
	je	.L729
	.p2align 4,,10
	.p2align 3
.L733:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L730
	movq	%rax, -680(%rbp)
	addq	$24, %r15
	call	_ZdlPv@PLT
	movq	-680(%rbp), %rax
	cmpq	%r15, %rax
	jne	.L733
.L731:
	movq	-232(%rbp), %r15
.L729:
	testq	%r15, %r15
	je	.L734
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L734:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L735
	call	_ZdlPv@PLT
.L735:
	movq	-416(%rbp), %rbx
	movq	-424(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L736
	.p2align 4,,10
	.p2align 3
.L740:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L737
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L740
.L738:
	movq	-424(%rbp), %r15
.L736:
	testq	%r15, %r15
	je	.L741
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L741:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L742
	call	_ZdlPv@PLT
.L742:
	movq	-608(%rbp), %rbx
	movq	-616(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L743
	.p2align 4,,10
	.p2align 3
.L747:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L744
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L747
.L745:
	movq	-616(%rbp), %r14
.L743:
	testq	%r14, %r14
	je	.L748
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L748:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L790
	addq	$648, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L744:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L747
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L730:
	addq	$24, %r15
	cmpq	%r15, %rax
	jne	.L733
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L737:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L740
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L788:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-680(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L722
	call	_ZdlPv@PLT
.L722:
	movl	$146, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-688(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r13, %rdi
	movq	%rax, -680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-680(%rbp), %rsi
	movq	-688(%rbp), %rdi
	call	_ZN2v88internal25LoadFunctionFromFrame_296EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEE
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	movq	%rax, -680(%rbp)
	call	_Znwm@PLT
	movq	-680(%rbp), %rdx
	leaq	-432(%rbp), %rdi
	movq	%r13, %rsi
	leaq	8(%rax), %rcx
	movq	%rax, -656(%rbp)
	movq	%rdx, (%rax)
	movq	%rcx, -640(%rbp)
	movq	%rcx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L723
	call	_ZdlPv@PLT
.L723:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L721
	.p2align 4,,10
	.p2align 3
.L789:
	leaq	-432(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r8, -680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-680(%rbp), %r8
	movq	%r13, %rsi
	movb	$7, (%rax)
	leaq	1(%rax), %rdx
	movq	%r8, %rdi
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L725
	movq	%rax, -680(%rbp)
	call	_ZdlPv@PLT
	movq	-680(%rbp), %rax
.L725:
	movq	(%rax), %rax
	movl	$144, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	%rcx, -680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-680(%rbp), %rcx
	movq	%r13, %rsi
	leaq	8(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rcx, (%rax)
	leaq	-240(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	movq	%rax, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L726
	call	_ZdlPv@PLT
.L726:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L724
.L790:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22495:
	.size	_ZN2v88internal23LoadTargetFromFrame_304EPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal23LoadTargetFromFrame_304EPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal17Cast7Context_1460EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../../deps/v8/../../deps/v8/src/builtins/base.tq"
	.section	.text._ZN2v88internal17Cast7Context_1460EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal17Cast7Context_1460EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal17Cast7Context_1460EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal17Cast7Context_1460EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE:
.LFB22499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	leaq	-1632(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1760(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1800(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1864, %rsp
	movq	%rsi, -1888(%rbp)
	movq	%rdx, -1904(%rbp)
	movq	%rcx, -1872(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1800(%rbp)
	movq	%rdi, -1632(%rbp)
	movl	$48, %edi
	movq	$0, -1624(%rbp)
	movq	$0, -1616(%rbp)
	movq	$0, -1608(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1624(%rbp)
	leaq	-1576(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1608(%rbp)
	movq	%rdx, -1616(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1592(%rbp)
	movq	%rax, -1816(%rbp)
	movq	$0, -1600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1432(%rbp)
	movq	$0, -1424(%rbp)
	movq	%rax, -1440(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1432(%rbp)
	leaq	-1384(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -1832(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1864(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$96, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1840(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1848(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	$0, -472(%rbp)
	movq	%rax, -480(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1824(%rbp)
	movq	$0, -464(%rbp)
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	$0, -440(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1888(%rbp), %xmm1
	movaps	%xmm0, -1760(%rbp)
	movhps	-1904(%rbp), %xmm1
	movq	$0, -1744(%rbp)
	movaps	%xmm1, -1888(%rbp)
	call	_Znwm@PLT
	movdqa	-1888(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L792
	call	_ZdlPv@PLT
.L792:
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1568(%rbp)
	jne	.L976
	cmpq	$0, -1376(%rbp)
	jne	.L977
.L798:
	cmpq	$0, -1184(%rbp)
	jne	.L978
.L801:
	cmpq	$0, -992(%rbp)
	jne	.L979
.L806:
	cmpq	$0, -800(%rbp)
	jne	.L980
.L809:
	cmpq	$0, -608(%rbp)
	leaq	-288(%rbp), %r15
	jne	.L981
	cmpq	$0, -416(%rbp)
	jne	.L982
.L815:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L817
	call	_ZdlPv@PLT
.L817:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L818
	call	_ZdlPv@PLT
.L818:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L819
	.p2align 4,,10
	.p2align 3
.L823:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L820
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L823
.L821:
	movq	-280(%rbp), %r14
.L819:
	testq	%r14, %r14
	je	.L824
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L824:
	movq	-1824(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L825
	call	_ZdlPv@PLT
.L825:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L826
	.p2align 4,,10
	.p2align 3
.L830:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L827
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L830
.L828:
	movq	-472(%rbp), %r14
.L826:
	testq	%r14, %r14
	je	.L831
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L831:
	movq	-1848(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L832
	call	_ZdlPv@PLT
.L832:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L833
	.p2align 4,,10
	.p2align 3
.L837:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L834
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L837
.L835:
	movq	-664(%rbp), %r14
.L833:
	testq	%r14, %r14
	je	.L838
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L838:
	movq	-1840(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L839
	call	_ZdlPv@PLT
.L839:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L840
	.p2align 4,,10
	.p2align 3
.L844:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L841
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L844
.L842:
	movq	-856(%rbp), %r14
.L840:
	testq	%r14, %r14
	je	.L845
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L845:
	movq	-1864(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L846
	call	_ZdlPv@PLT
.L846:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L847
	.p2align 4,,10
	.p2align 3
.L851:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L848
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L851
.L849:
	movq	-1048(%rbp), %r14
.L847:
	testq	%r14, %r14
	je	.L852
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L852:
	movq	-1832(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L853
	call	_ZdlPv@PLT
.L853:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L854
	.p2align 4,,10
	.p2align 3
.L858:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L855
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L858
.L856:
	movq	-1240(%rbp), %r14
.L854:
	testq	%r14, %r14
	je	.L859
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L859:
	movq	-1856(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L860
	call	_ZdlPv@PLT
.L860:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L861
	.p2align 4,,10
	.p2align 3
.L865:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L862
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L865
.L863:
	movq	-1432(%rbp), %r14
.L861:
	testq	%r14, %r14
	je	.L866
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L866:
	movq	-1816(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L867
	call	_ZdlPv@PLT
.L867:
	movq	-1616(%rbp), %rbx
	movq	-1624(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L868
	.p2align 4,,10
	.p2align 3
.L872:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L869
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L872
.L870:
	movq	-1624(%rbp), %r14
.L868:
	testq	%r14, %r14
	je	.L873
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L873:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L983
	addq	$1864, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L869:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L872
	jmp	.L870
	.p2align 4,,10
	.p2align 3
.L862:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L865
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L855:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L858
	jmp	.L856
	.p2align 4,,10
	.p2align 3
.L848:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L851
	jmp	.L849
	.p2align 4,,10
	.p2align 3
.L841:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L844
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L834:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L837
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L820:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L823
	jmp	.L821
	.p2align 4,,10
	.p2align 3
.L827:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L830
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L976:
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r9d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L794
	call	_ZdlPv@PLT
.L794:
	movq	(%r15), %rax
	movl	$2026, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	leaq	-1792(%rbp), %r15
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rcx, -1904(%rbp)
	movq	%rax, -1888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1888(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler11TaggedIsSmiENS0_8compiler11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6GotoIfENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1888(%rbp), %rax
	movl	$32, %edi
	movq	-1904(%rbp), %xmm2
	movq	$0, -1776(%rbp)
	movq	%rax, %xmm0
	movq	%rax, %xmm6
	punpcklqdq	%xmm0, %xmm0
	punpcklqdq	%xmm6, %xmm2
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -1904(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -1792(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movq	%r15, %rsi
	leaq	32(%rax), %rdx
	leaq	-1248(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L795
	call	_ZdlPv@PLT
.L795:
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1752(%rbp)
	jne	.L984
.L796:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1376(%rbp)
	je	.L798
.L977:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1440(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r8d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L799
	call	_ZdlPv@PLT
.L799:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-480(%rbp), %rdi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L800
	call	_ZdlPv@PLT
.L800:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1184(%rbp)
	je	.L801
.L978:
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1248(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L802
	call	_ZdlPv@PLT
.L802:
	movq	(%r15), %rax
	movl	$1, %r8d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	24(%rax), %rax
	movq	%rcx, -1904(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -1888(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1888(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal16Cast7Context_115EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rcx
	movq	%r15, %xmm3
	pxor	%xmm0, %xmm0
	movhps	-1904(%rbp), %xmm3
	movl	$32, %edi
	movq	%rax, -72(%rbp)
	leaq	-1792(%rbp), %r15
	movq	%rcx, -80(%rbp)
	movaps	%xmm3, -1904(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -1792(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	32(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L803
	call	_ZdlPv@PLT
.L803:
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1752(%rbp)
	jne	.L985
.L804:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -992(%rbp)
	je	.L806
.L979:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rdi
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L807
	call	_ZdlPv@PLT
.L807:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-480(%rbp), %rdi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L808
	call	_ZdlPv@PLT
.L808:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L809
.L980:
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117901319, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L810
	call	_ZdlPv@PLT
.L810:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	24(%rax), %rdx
	movdqu	(%rax), %xmm4
	movaps	%xmm0, -1760(%rbp)
	movq	$0, -1744(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm4, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -1760(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L811
	call	_ZdlPv@PLT
.L811:
	movq	-1848(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L981:
	movq	-1848(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%r15, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L813
	call	_ZdlPv@PLT
.L813:
	movq	(%rbx), %rax
	movl	$75, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r15
	movq	%rcx, -1888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r15, -80(%rbp)
	movhps	-1888(%rbp), %xmm0
	leaq	-288(%rbp), %r15
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm6
	leaq	24(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L814
	call	_ZdlPv@PLT
.L814:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -416(%rbp)
	je	.L815
.L982:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L816
	call	_ZdlPv@PLT
.L816:
	movq	-1872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L984:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1904(%rbp), %xmm7
	movl	$24, %edi
	movaps	%xmm0, -1792(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-1440(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L797
	call	_ZdlPv@PLT
.L797:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L985:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1904(%rbp), %xmm7
	movl	$24, %edi
	movaps	%xmm0, -1792(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-1056(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L805
	call	_ZdlPv@PLT
.L805:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L804
.L983:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22499:
	.size	_ZN2v88internal17Cast7Context_1460EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, .-_ZN2v88internal17Cast7Context_1460EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal33Cast23UT11ATFrameType7Context_298EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Cast23UT11ATFrameType7Context_298EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal33Cast23UT11ATFrameType7Context_298EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal33Cast23UT11ATFrameType7Context_298EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE:
.LFB22447:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	leaq	-2016(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2144(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-2184(%rbp), %r12
	pushq	%rbx
	subq	$2280, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -2272(%rbp)
	movq	%rsi, -2288(%rbp)
	movq	%rdx, -2304(%rbp)
	movq	%rcx, -2312(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -2184(%rbp)
	movq	%rdi, -2016(%rbp)
	movl	$48, %edi
	movq	$0, -2008(%rbp)
	movq	$0, -2000(%rbp)
	movq	$0, -1992(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -2008(%rbp)
	leaq	-1960(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1992(%rbp)
	movq	%rdx, -2000(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1976(%rbp)
	movq	%rax, -2200(%rbp)
	movq	$0, -1984(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2184(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1816(%rbp)
	movq	$0, -1808(%rbp)
	movq	%rax, -1824(%rbp)
	movq	$0, -1800(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1816(%rbp)
	leaq	-1768(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1800(%rbp)
	movq	%rdx, -1808(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1784(%rbp)
	movq	%rax, -2256(%rbp)
	movq	$0, -1792(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2184(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1624(%rbp)
	movq	$0, -1616(%rbp)
	movq	%rax, -1632(%rbp)
	movq	$0, -1608(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1624(%rbp)
	leaq	-1576(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1608(%rbp)
	movq	%rdx, -1616(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1592(%rbp)
	movq	%rax, -2216(%rbp)
	movq	$0, -1600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2184(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1432(%rbp)
	movq	$0, -1424(%rbp)
	movq	%rax, -1440(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1432(%rbp)
	leaq	-1384(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -2224(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2184(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -2264(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2184(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -2232(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2184(%rbp), %rax
	movl	$72, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -2248(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2184(%rbp), %rax
	movl	$72, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -2208(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2184(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	$0, -472(%rbp)
	movq	%rax, -480(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -2240(%rbp)
	movq	$0, -464(%rbp)
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	$0, -440(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2184(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-2288(%rbp), %xmm1
	movaps	%xmm0, -2144(%rbp)
	movhps	-2304(%rbp), %xmm1
	movq	$0, -2128(%rbp)
	movaps	%xmm1, -2288(%rbp)
	call	_Znwm@PLT
	movdqa	-2288(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -2144(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L987
	call	_ZdlPv@PLT
.L987:
	movq	-2200(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1952(%rbp)
	jne	.L1215
	cmpq	$0, -1760(%rbp)
	jne	.L1216
.L993:
	cmpq	$0, -1568(%rbp)
	jne	.L1217
.L996:
	cmpq	$0, -1376(%rbp)
	jne	.L1218
.L999:
	cmpq	$0, -1184(%rbp)
	jne	.L1219
.L1004:
	cmpq	$0, -992(%rbp)
	jne	.L1220
.L1007:
	cmpq	$0, -800(%rbp)
	jne	.L1221
.L1010:
	cmpq	$0, -608(%rbp)
	leaq	-288(%rbp), %r15
	jne	.L1222
	cmpq	$0, -416(%rbp)
	jne	.L1223
.L1016:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -2144(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -2144(%rbp)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2144(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1018
	call	_ZdlPv@PLT
.L1018:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1019
	call	_ZdlPv@PLT
.L1019:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1020
	.p2align 4,,10
	.p2align 3
.L1024:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1021
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1024
.L1022:
	movq	-280(%rbp), %r14
.L1020:
	testq	%r14, %r14
	je	.L1025
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1025:
	movq	-2240(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1026
	call	_ZdlPv@PLT
.L1026:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1027
	.p2align 4,,10
	.p2align 3
.L1031:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1028
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1031
.L1029:
	movq	-472(%rbp), %r14
.L1027:
	testq	%r14, %r14
	je	.L1032
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1032:
	movq	-2208(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1033
	call	_ZdlPv@PLT
.L1033:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1034
	.p2align 4,,10
	.p2align 3
.L1038:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1035
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1038
.L1036:
	movq	-664(%rbp), %r14
.L1034:
	testq	%r14, %r14
	je	.L1039
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1039:
	movq	-2248(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1040
	call	_ZdlPv@PLT
.L1040:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1041
	.p2align 4,,10
	.p2align 3
.L1045:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1042
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1045
.L1043:
	movq	-856(%rbp), %r14
.L1041:
	testq	%r14, %r14
	je	.L1046
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1046:
	movq	-2232(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1047
	call	_ZdlPv@PLT
.L1047:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1048
	.p2align 4,,10
	.p2align 3
.L1052:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1049
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1052
.L1050:
	movq	-1048(%rbp), %r14
.L1048:
	testq	%r14, %r14
	je	.L1053
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1053:
	movq	-2264(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1054
	call	_ZdlPv@PLT
.L1054:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1055
	.p2align 4,,10
	.p2align 3
.L1059:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1056
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1059
.L1057:
	movq	-1240(%rbp), %r14
.L1055:
	testq	%r14, %r14
	je	.L1060
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1060:
	movq	-2224(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1061
	call	_ZdlPv@PLT
.L1061:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1062
	.p2align 4,,10
	.p2align 3
.L1066:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1063
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1066
.L1064:
	movq	-1432(%rbp), %r14
.L1062:
	testq	%r14, %r14
	je	.L1067
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1067:
	movq	-2216(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1068
	call	_ZdlPv@PLT
.L1068:
	movq	-1616(%rbp), %rbx
	movq	-1624(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1069
	.p2align 4,,10
	.p2align 3
.L1073:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1070
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1073
.L1071:
	movq	-1624(%rbp), %r14
.L1069:
	testq	%r14, %r14
	je	.L1074
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1074:
	movq	-2256(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1075
	call	_ZdlPv@PLT
.L1075:
	movq	-1808(%rbp), %rbx
	movq	-1816(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1076
	.p2align 4,,10
	.p2align 3
.L1080:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1077
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1080
.L1078:
	movq	-1816(%rbp), %r14
.L1076:
	testq	%r14, %r14
	je	.L1081
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1081:
	movq	-2200(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1984(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1082
	call	_ZdlPv@PLT
.L1082:
	movq	-2000(%rbp), %rbx
	movq	-2008(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1083
	.p2align 4,,10
	.p2align 3
.L1087:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1084
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1087
.L1085:
	movq	-2008(%rbp), %r14
.L1083:
	testq	%r14, %r14
	je	.L1088
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1088:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1224
	addq	$2280, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1084:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1087
	jmp	.L1085
	.p2align 4,,10
	.p2align 3
.L1077:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1080
	jmp	.L1078
	.p2align 4,,10
	.p2align 3
.L1070:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1073
	jmp	.L1071
	.p2align 4,,10
	.p2align 3
.L1063:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1066
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1056:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1059
	jmp	.L1057
	.p2align 4,,10
	.p2align 3
.L1049:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1052
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1042:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1045
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1035:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1038
	jmp	.L1036
	.p2align 4,,10
	.p2align 3
.L1021:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1024
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1028:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1031
	jmp	.L1029
	.p2align 4,,10
	.p2align 3
.L1215:
	movq	-2200(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -2144(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r9d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -2144(%rbp)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2144(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L989
	call	_ZdlPv@PLT
.L989:
	movq	(%rbx), %rax
	movl	$74, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %r15
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$75, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rdx
	movq	%r13, %rcx
	movq	%rbx, %rsi
	movq	-2272(%rbp), %rdi
	call	_ZN2v88internal17Cast7Context_1460EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	movq	%r15, %xmm6
	movq	%r15, %xmm2
	movq	%rbx, %xmm3
	punpcklqdq	%xmm6, %xmm3
	punpcklqdq	%xmm2, %xmm2
	pxor	%xmm0, %xmm0
	movq	%rax, -64(%rbp)
	movl	$40, %edi
	movaps	%xmm3, -96(%rbp)
	leaq	-2176(%rbp), %r15
	movaps	%xmm2, -2304(%rbp)
	movaps	%xmm3, -2288(%rbp)
	movaps	%xmm2, -80(%rbp)
	movaps	%xmm0, -2176(%rbp)
	movq	$0, -2160(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm7
	movdqa	-80(%rbp), %xmm6
	leaq	40(%rax), %rdx
	leaq	-1632(%rbp), %rdi
	movq	%rax, -2176(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -2160(%rbp)
	movq	%rdx, -2168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L990
	call	_ZdlPv@PLT
.L990:
	movq	-2216(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2136(%rbp)
	jne	.L1225
.L991:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1760(%rbp)
	je	.L993
.L1216:
	movq	-2256(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1824(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -2144(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2144(%rbp)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2144(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L994
	call	_ZdlPv@PLT
.L994:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm7
	movaps	%xmm0, -2144(%rbp)
	movq	$0, -2128(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm7, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-1440(%rbp), %rdi
	movq	%rax, -2144(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L995
	call	_ZdlPv@PLT
.L995:
	movq	-2224(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1568(%rbp)
	je	.L996
.L1217:
	movq	-2216(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1632(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -2144(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2144(%rbp)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2144(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L997
	call	_ZdlPv@PLT
.L997:
	movq	(%rbx), %rax
	movl	$76, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	32(%rax), %r15
	movq	%rcx, -2288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r15, -80(%rbp)
	movhps	-2288(%rbp), %xmm0
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2144(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -2144(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L998
	call	_ZdlPv@PLT
.L998:
	movq	-2208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1376(%rbp)
	je	.L999
.L1218:
	movq	-2224(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1440(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -2144(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r8d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -2144(%rbp)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2144(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1000
	call	_ZdlPv@PLT
.L1000:
	movq	(%rbx), %rax
	movl	$78, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r15
	movq	16(%rax), %rbx
	movq	%rcx, -2288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2272(%rbp), %rdi
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal21Cast11ATFrameType_292EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	movq	%r15, %xmm5
	movq	%rbx, %xmm4
	pxor	%xmm0, %xmm0
	punpcklqdq	%xmm4, %xmm4
	movl	$40, %edi
	movq	%rax, -64(%rbp)
	movhps	-2288(%rbp), %xmm5
	movaps	%xmm4, -2304(%rbp)
	leaq	-2176(%rbp), %r15
	movaps	%xmm5, -2288(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm0, -2176(%rbp)
	movq	$0, -2160(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	leaq	40(%rax), %rdx
	leaq	-1056(%rbp), %rdi
	movq	%rax, -2176(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2160(%rbp)
	movq	%rdx, -2168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1001
	call	_ZdlPv@PLT
.L1001:
	movq	-2232(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2136(%rbp)
	jne	.L1226
.L1002:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1184(%rbp)
	je	.L1004
.L1219:
	movq	-2264(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1248(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -2144(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2144(%rbp)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2144(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1005
	call	_ZdlPv@PLT
.L1005:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm6
	movaps	%xmm0, -2144(%rbp)
	movq	$0, -2128(%rbp)
	movq	%rdx, -80(%rbp)
	movaps	%xmm6, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm7
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -2144(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1006
	call	_ZdlPv@PLT
.L1006:
	movq	-2248(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -992(%rbp)
	je	.L1007
.L1220:
	movq	-2232(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -2144(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	5(%rax), %rdx
	movb	$6, 4(%rax)
	movq	%rax, -2144(%rbp)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2144(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1008
	call	_ZdlPv@PLT
.L1008:
	movq	(%rbx), %rax
	movl	$79, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	32(%rax), %r15
	movq	%rcx, -2288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r15, -80(%rbp)
	movhps	-2288(%rbp), %xmm0
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2144(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -2144(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1009
	call	_ZdlPv@PLT
.L1009:
	movq	-2208(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L1010
.L1221:
	movq	-2248(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -2144(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rdi
	movb	$8, 2(%rax)
	movq	%rax, -2144(%rbp)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1011
	call	_ZdlPv@PLT
.L1011:
	movl	$81, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$82, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm0, %xmm0
	leaq	-480(%rbp), %rdi
	movq	%r13, %rsi
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -2144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1012
	call	_ZdlPv@PLT
.L1012:
	movq	-2240(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L1222:
	movq	-2208(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -2144(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%r15, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$8, 2(%rax)
	movq	%rax, -2144(%rbp)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2144(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1014
	call	_ZdlPv@PLT
.L1014:
	movq	(%rbx), %rax
	movl	$71, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r15
	movq	%rcx, -2288(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r15, -80(%rbp)
	movhps	-2288(%rbp), %xmm0
	leaq	-288(%rbp), %r15
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2144(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm7
	leaq	24(%rax), %rdx
	movq	%rax, -2144(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -2128(%rbp)
	movq	%rdx, -2136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1015
	call	_ZdlPv@PLT
.L1015:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -416(%rbp)
	je	.L1016
.L1223:
	movq	-2240(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	$0, -2128(%rbp)
	movaps	%xmm0, -2144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1017
	call	_ZdlPv@PLT
.L1017:
	movq	-2312(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L1225:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-2288(%rbp), %xmm2
	movdqa	-2304(%rbp), %xmm3
	movaps	%xmm0, -2176(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movq	$0, -2160(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movq	%r15, %rsi
	leaq	32(%rax), %rdx
	leaq	-1824(%rbp), %rdi
	movq	%rax, -2176(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -2160(%rbp)
	movq	%rdx, -2168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L992
	call	_ZdlPv@PLT
.L992:
	movq	-2256(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L991
	.p2align 4,,10
	.p2align 3
.L1226:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-2288(%rbp), %xmm2
	movdqa	-2304(%rbp), %xmm3
	movaps	%xmm0, -2176(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movq	$0, -2160(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movq	%r15, %rsi
	leaq	32(%rax), %rdx
	leaq	-1248(%rbp), %rdi
	movq	%rax, -2176(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -2160(%rbp)
	movq	%rdx, -2168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1003
	call	_ZdlPv@PLT
.L1003:
	movq	-2264(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1002
.L1224:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22447:
	.size	_ZN2v88internal33Cast23UT11ATFrameType7Context_298EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE, .-_ZN2v88internal33Cast23UT11ATFrameType7Context_298EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal40UnsafeCast23UT11ATFrameType7Context_1461EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal40UnsafeCast23UT11ATFrameType7Context_1461EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal40UnsafeCast23UT11ATFrameType7Context_1461EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal40UnsafeCast23UT11ATFrameType7Context_1461EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE:
.LFB22512:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-216(%rbp), %r14
	leaq	-688(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-600(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-696(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-656(%rbp), %rbx
	subq	$696, %rsp
	movq	%rsi, -736(%rbp)
	movq	%rdx, -720(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -696(%rbp)
	movq	%rdi, -656(%rbp)
	movl	$48, %edi
	movq	$0, -648(%rbp)
	movq	$0, -640(%rbp)
	movq	$0, -632(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rdx, -632(%rbp)
	movq	%rdx, -640(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -616(%rbp)
	movq	%rax, -648(%rbp)
	movq	$0, -624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	%rax, -464(%rbp)
	movq	$0, -440(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -456(%rbp)
	leaq	-408(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -440(%rbp)
	movq	%rdx, -448(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -424(%rbp)
	movq	%rax, -712(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	%rax, -272(%rbp)
	movq	$0, -248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -248(%rbp)
	movq	%rdx, -256(%rbp)
	xorl	%edx, %edx
	movq	%rax, -264(%rbp)
	movups	%xmm0, -232(%rbp)
	movq	$0, -240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-736(%rbp), %xmm1
	movaps	%xmm0, -688(%rbp)
	movhps	-720(%rbp), %xmm1
	movq	$0, -672(%rbp)
	movaps	%xmm1, -736(%rbp)
	call	_Znwm@PLT
	movdqa	-736(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	%r15, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -688(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1228
	call	_ZdlPv@PLT
.L1228:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -592(%rbp)
	jne	.L1296
.L1229:
	cmpq	$0, -400(%rbp)
	leaq	-272(%rbp), %rbx
	jne	.L1297
.L1232:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1235
	call	_ZdlPv@PLT
.L1235:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	16(%rax), %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1236
	call	_ZdlPv@PLT
.L1236:
	movq	-256(%rbp), %rbx
	movq	-264(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1237
	.p2align 4,,10
	.p2align 3
.L1241:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1238
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1241
.L1239:
	movq	-264(%rbp), %r14
.L1237:
	testq	%r14, %r14
	je	.L1242
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1242:
	movq	-712(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1243
	call	_ZdlPv@PLT
.L1243:
	movq	-448(%rbp), %rbx
	movq	-456(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1244
	.p2align 4,,10
	.p2align 3
.L1248:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1245
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1248
.L1246:
	movq	-456(%rbp), %r14
.L1244:
	testq	%r14, %r14
	je	.L1249
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1249:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1250
	call	_ZdlPv@PLT
.L1250:
	movq	-640(%rbp), %rbx
	movq	-648(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1251
	.p2align 4,,10
	.p2align 3
.L1255:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1252
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1255
.L1253:
	movq	-648(%rbp), %r13
.L1251:
	testq	%r13, %r13
	je	.L1256
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1256:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1298
	addq	$696, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1252:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1255
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1238:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1241
	jmp	.L1239
	.p2align 4,,10
	.p2align 3
.L1245:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1248
	jmp	.L1246
	.p2align 4,,10
	.p2align 3
.L1296:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %edi
	movq	%r15, %rsi
	movw	%di, (%rax)
	leaq	2(%rax), %rdx
	movq	%rbx, %rdi
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1230
	call	_ZdlPv@PLT
.L1230:
	movq	(%rbx), %rax
	movl	$2790, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	%rcx, -736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm2
	movl	$24, %edi
	movq	-736(%rbp), %xmm0
	movq	%rbx, -64(%rbp)
	movq	$0, -672(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm3
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-464(%rbp), %rdi
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1231
	call	_ZdlPv@PLT
.L1231:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1229
	.p2align 4,,10
	.p2align 3
.L1297:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-464(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rsi
	movb	$8, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1233
	call	_ZdlPv@PLT
.L1233:
	movq	(%rbx), %rax
	movl	$92, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -720(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %edi
	movq	%rbx, -64(%rbp)
	movq	-736(%rbp), %xmm0
	movq	$0, -672(%rbp)
	leaq	-272(%rbp), %rbx
	movhps	-720(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movdqa	-80(%rbp), %xmm4
	leaq	24(%rax), %rdx
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1234
	call	_ZdlPv@PLT
.L1234:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1232
.L1298:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22512:
	.size	_ZN2v88internal40UnsafeCast23UT11ATFrameType7Context_1461EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal40UnsafeCast23UT11ATFrameType7Context_1461EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal35LoadContextOrFrameTypeFromFrame_299EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal35LoadContextOrFrameTypeFromFrame_299EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEE
	.type	_ZN2v88internal35LoadContextOrFrameTypeFromFrame_299EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEE, @function
_ZN2v88internal35LoadContextOrFrameTypeFromFrame_299EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEE:
.LFB22463:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-216(%rbp), %r14
	leaq	-688(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-600(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-696(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$712, %rsp
	movq	%rsi, -736(%rbp)
	movq	%rdx, -744(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -696(%rbp)
	movq	%rdi, -656(%rbp)
	movl	$48, %edi
	movq	$0, -648(%rbp)
	movq	$0, -640(%rbp)
	movq	$0, -632(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -648(%rbp)
	leaq	-656(%rbp), %rax
	movq	%rdx, -632(%rbp)
	movq	%rdx, -640(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -616(%rbp)
	movq	%rax, -720(%rbp)
	movq	$0, -624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	%rax, -464(%rbp)
	movq	$0, -440(%rbp)
	call	_Znwm@PLT
	leaq	-408(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -440(%rbp)
	movq	%rdx, -448(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -712(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -424(%rbp)
	movq	%rax, -456(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-696(%rbp), %rax
	movl	$72, %edi
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	%rax, -272(%rbp)
	movq	$0, -248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -248(%rbp)
	movq	%rdx, -256(%rbp)
	xorl	%edx, %edx
	movq	%rax, -264(%rbp)
	movups	%xmm0, -232(%rbp)
	movq	$0, -240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-736(%rbp), %xmm1
	movaps	%xmm0, -688(%rbp)
	movhps	-744(%rbp), %xmm1
	movq	$0, -672(%rbp)
	movaps	%xmm1, -736(%rbp)
	call	_Znwm@PLT
	movdqa	-736(%rbp), %xmm1
	movq	-720(%rbp), %rdi
	movq	%r15, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -688(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1300
	call	_ZdlPv@PLT
.L1300:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -592(%rbp)
	jne	.L1368
.L1301:
	cmpq	$0, -400(%rbp)
	leaq	-272(%rbp), %rbx
	jne	.L1369
.L1304:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$1287, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1307
	call	_ZdlPv@PLT
.L1307:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	16(%rax), %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1308
	call	_ZdlPv@PLT
.L1308:
	movq	-256(%rbp), %rbx
	movq	-264(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1309
	.p2align 4,,10
	.p2align 3
.L1313:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1310
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1313
.L1311:
	movq	-264(%rbp), %r14
.L1309:
	testq	%r14, %r14
	je	.L1314
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1314:
	movq	-712(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1315
	call	_ZdlPv@PLT
.L1315:
	movq	-448(%rbp), %rbx
	movq	-456(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1316
	.p2align 4,,10
	.p2align 3
.L1320:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1317
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1320
.L1318:
	movq	-456(%rbp), %r14
.L1316:
	testq	%r14, %r14
	je	.L1321
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1321:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1322
	call	_ZdlPv@PLT
.L1322:
	movq	-640(%rbp), %rbx
	movq	-648(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1323
	.p2align 4,,10
	.p2align 3
.L1327:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1324
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1327
.L1325:
	movq	-648(%rbp), %r13
.L1323:
	testq	%r13, %r13
	je	.L1328
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1328:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1370
	addq	$712, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1324:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1327
	jmp	.L1325
	.p2align 4,,10
	.p2align 3
.L1310:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1313
	jmp	.L1311
	.p2align 4,,10
	.p2align 3
.L1317:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1320
	jmp	.L1318
	.p2align 4,,10
	.p2align 3
.L1368:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$1287, %edi
	movq	%r15, %rsi
	movw	%di, (%rax)
	movq	-720(%rbp), %rdi
	leaq	2(%rax), %rdx
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1302
	movq	%rax, -720(%rbp)
	call	_ZdlPv@PLT
	movq	-720(%rbp), %rax
.L1302:
	movq	(%rax), %rax
	movl	$93, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	%rcx, -720(%rbp)
	movq	%rax, -736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-736(%rbp), %rsi
	movl	$-8, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal23LoadObjectFromFrame_293EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7RawPtrTEEEi
	movl	$92, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -744(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-744(%rbp), %r8
	movq	-720(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	_ZN2v88internal40UnsafeCast23UT11ATFrameType7Context_1461EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE
	movl	$24, %edi
	movq	-720(%rbp), %xmm0
	movq	$0, -672(%rbp)
	movq	%rax, -64(%rbp)
	movhps	-736(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-80(%rbp), %xmm2
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-464(%rbp), %rdi
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1303
	call	_ZdlPv@PLT
.L1303:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1369:
	movq	-712(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-464(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -672(%rbp)
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movl	$1287, %esi
	movq	%rbx, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rsi
	movb	$8, 2(%rax)
	movq	%rax, -688(%rbp)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-688(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1305
	call	_ZdlPv@PLT
.L1305:
	movq	(%rbx), %rax
	movl	$89, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -736(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -720(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %edi
	movq	%rbx, -64(%rbp)
	movq	-720(%rbp), %xmm0
	movq	$0, -672(%rbp)
	leaq	-272(%rbp), %rbx
	movhps	-736(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -688(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movdqa	-80(%rbp), %xmm3
	leaq	24(%rax), %rdx
	movq	%rax, -688(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1306
	call	_ZdlPv@PLT
.L1306:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1304
.L1370:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22463:
	.size	_ZN2v88internal35LoadContextOrFrameTypeFromFrame_299EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEE, .-_ZN2v88internal35LoadContextOrFrameTypeFromFrame_299EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEE
	.section	.text._ZN2v88internal25Cast15ATStandardFrame_302EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Cast15ATStandardFrame_302EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEEPNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal25Cast15ATStandardFrame_302EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEEPNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal25Cast15ATStandardFrame_302EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEEPNS1_18CodeAssemblerLabelE:
.LFB22481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	leaq	-1632(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1760(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1800(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1880, %rsp
	movq	%rsi, -1888(%rbp)
	movq	%rdx, -1904(%rbp)
	movq	%rcx, -1872(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1800(%rbp)
	movq	%rdi, -1632(%rbp)
	movl	$48, %edi
	movq	$0, -1624(%rbp)
	movq	$0, -1616(%rbp)
	movq	$0, -1608(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1624(%rbp)
	leaq	-1576(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1608(%rbp)
	movq	%rdx, -1616(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1592(%rbp)
	movq	%rax, -1816(%rbp)
	movq	$0, -1600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1432(%rbp)
	movq	$0, -1424(%rbp)
	movq	%rax, -1440(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1432(%rbp)
	leaq	-1384(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -1864(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -1832(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1848(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1840(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	$0, -472(%rbp)
	movq	%rax, -480(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1824(%rbp)
	movq	$0, -464(%rbp)
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	$0, -440(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1888(%rbp), %xmm1
	movaps	%xmm0, -1760(%rbp)
	movhps	-1904(%rbp), %xmm1
	movq	$0, -1744(%rbp)
	movaps	%xmm1, -1888(%rbp)
	call	_Znwm@PLT
	movdqa	-1888(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1372
	call	_ZdlPv@PLT
.L1372:
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1568(%rbp)
	jne	.L1555
	cmpq	$0, -1376(%rbp)
	jne	.L1556
.L1378:
	cmpq	$0, -1184(%rbp)
	jne	.L1557
.L1381:
	cmpq	$0, -992(%rbp)
	jne	.L1558
.L1385:
	cmpq	$0, -800(%rbp)
	jne	.L1559
.L1388:
	cmpq	$0, -608(%rbp)
	leaq	-288(%rbp), %r15
	jne	.L1560
	cmpq	$0, -416(%rbp)
	jne	.L1561
.L1394:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$1287, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$5, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1396
	call	_ZdlPv@PLT
.L1396:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1397
	call	_ZdlPv@PLT
.L1397:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1398
	.p2align 4,,10
	.p2align 3
.L1402:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1399
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1402
.L1400:
	movq	-280(%rbp), %r14
.L1398:
	testq	%r14, %r14
	je	.L1403
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1403:
	movq	-1824(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1404
	call	_ZdlPv@PLT
.L1404:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1405
	.p2align 4,,10
	.p2align 3
.L1409:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1406
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1409
.L1407:
	movq	-472(%rbp), %r14
.L1405:
	testq	%r14, %r14
	je	.L1410
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1410:
	movq	-1856(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1411
	call	_ZdlPv@PLT
.L1411:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1412
	.p2align 4,,10
	.p2align 3
.L1416:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1413
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1416
.L1414:
	movq	-664(%rbp), %r14
.L1412:
	testq	%r14, %r14
	je	.L1417
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1417:
	movq	-1840(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1418
	call	_ZdlPv@PLT
.L1418:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1419
	.p2align 4,,10
	.p2align 3
.L1423:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1420
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1423
.L1421:
	movq	-856(%rbp), %r14
.L1419:
	testq	%r14, %r14
	je	.L1424
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1424:
	movq	-1848(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1425
	call	_ZdlPv@PLT
.L1425:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1426
	.p2align 4,,10
	.p2align 3
.L1430:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1427
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1430
.L1428:
	movq	-1048(%rbp), %r14
.L1426:
	testq	%r14, %r14
	je	.L1431
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1431:
	movq	-1832(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1432
	call	_ZdlPv@PLT
.L1432:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1433
	.p2align 4,,10
	.p2align 3
.L1437:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1434
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1437
.L1435:
	movq	-1240(%rbp), %r14
.L1433:
	testq	%r14, %r14
	je	.L1438
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1438:
	movq	-1864(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1439
	call	_ZdlPv@PLT
.L1439:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1440
	.p2align 4,,10
	.p2align 3
.L1444:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1441
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1444
.L1442:
	movq	-1432(%rbp), %r14
.L1440:
	testq	%r14, %r14
	je	.L1445
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1445:
	movq	-1816(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1446
	call	_ZdlPv@PLT
.L1446:
	movq	-1616(%rbp), %rbx
	movq	-1624(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1447
	.p2align 4,,10
	.p2align 3
.L1451:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1448
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1451
.L1449:
	movq	-1624(%rbp), %r14
.L1447:
	testq	%r14, %r14
	je	.L1452
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1452:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1562
	addq	$1880, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1448:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1451
	jmp	.L1449
	.p2align 4,,10
	.p2align 3
.L1441:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1444
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L1434:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1437
	jmp	.L1435
	.p2align 4,,10
	.p2align 3
.L1427:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1430
	jmp	.L1428
	.p2align 4,,10
	.p2align 3
.L1420:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1423
	jmp	.L1421
	.p2align 4,,10
	.p2align 3
.L1413:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1416
	jmp	.L1414
	.p2align 4,,10
	.p2align 3
.L1399:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1402
	jmp	.L1400
	.p2align 4,,10
	.p2align 3
.L1406:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1409
	jmp	.L1407
	.p2align 4,,10
	.p2align 3
.L1555:
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$1287, %r10d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r10w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L1374
	call	_ZdlPv@PLT
.L1374:
	movq	(%r15), %rax
	movl	$112, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r15
	movq	8(%rax), %rax
	movq	%rax, -1904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1904(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal35LoadContextOrFrameTypeFromFrame_299EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEE
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -1888(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rsi
	movq	%r13, %rcx
	movq	%rbx, %rdi
	movq	-1888(%rbp), %rdx
	call	_ZN2v88internal21Cast10HeapObject_1441EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rcx
	movq	%r15, %xmm2
	pxor	%xmm0, %xmm0
	movhps	-1904(%rbp), %xmm2
	movl	$32, %edi
	movq	%rax, -72(%rbp)
	leaq	-1792(%rbp), %r15
	movq	%rcx, -80(%rbp)
	movaps	%xmm2, -1904(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -1792(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movq	%r15, %rsi
	leaq	32(%rax), %rdx
	leaq	-1248(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1375
	call	_ZdlPv@PLT
.L1375:
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1752(%rbp)
	jne	.L1563
.L1376:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1376(%rbp)
	je	.L1378
.L1556:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1440(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$1287, %r9d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1379
	call	_ZdlPv@PLT
.L1379:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-480(%rbp), %rdi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1380
	call	_ZdlPv@PLT
.L1380:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1184(%rbp)
	je	.L1381
.L1557:
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1248(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$117966087, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L1382
	call	_ZdlPv@PLT
.L1382:
	movq	(%r15), %rax
	movl	$111, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rsi, -1904(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -1888(%rbp)
	movq	%rax, %r15
	movq	%rax, -1912(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$122, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r15, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler9IsContextENS0_8compiler11SloppyTNodeINS0_10HeapObjectEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	-1888(%rbp), %xmm3
	movaps	%xmm0, -1760(%rbp)
	movhps	-1904(%rbp), %xmm3
	movq	%rbx, -80(%rbp)
	movaps	%xmm3, -1888(%rbp)
	movaps	%xmm3, -96(%rbp)
	movq	$0, -1744(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-1056(%rbp), %rdi
	movq	%rax, -1760(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1383
	call	_ZdlPv@PLT
.L1383:
	movq	-1912(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movdqa	-1888(%rbp), %xmm7
	movaps	%xmm0, -1760(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1744(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm4
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -1760(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1384
	call	_ZdlPv@PLT
.L1384:
	movq	-1840(%rbp), %rcx
	movq	-1848(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -992(%rbp)
	je	.L1385
.L1558:
	movq	-1848(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$1287, %r8d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	3(%rax), %rdx
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1386
	call	_ZdlPv@PLT
.L1386:
	movq	(%rbx), %rax
	movl	$123, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %r15
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %edi
	movq	$0, -1744(%rbp)
	movq	%r15, %xmm5
	movq	%rbx, %xmm0
	movq	%r15, -80(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -1760(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1387
	call	_ZdlPv@PLT
.L1387:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L1388
.L1559:
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$1287, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rdi
	movb	$7, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1389
	call	_ZdlPv@PLT
.L1389:
	movl	$125, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm0, %xmm0
	leaq	-480(%rbp), %rdi
	movq	%r13, %rsi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1390
	call	_ZdlPv@PLT
.L1390:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1388
	.p2align 4,,10
	.p2align 3
.L1560:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$1287, %esi
	movq	%r15, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$5, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1392
	call	_ZdlPv@PLT
.L1392:
	movq	(%rbx), %rax
	movl	$109, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r15
	movq	%rcx, -1888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r15, -80(%rbp)
	movhps	-1888(%rbp), %xmm0
	leaq	-288(%rbp), %r15
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm7
	leaq	24(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1393
	call	_ZdlPv@PLT
.L1393:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -416(%rbp)
	je	.L1394
.L1561:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1395
	call	_ZdlPv@PLT
.L1395:
	movq	-1872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1394
	.p2align 4,,10
	.p2align 3
.L1563:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1904(%rbp), %xmm7
	movl	$24, %edi
	movaps	%xmm0, -1792(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm4
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-1440(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1377
	call	_ZdlPv@PLT
.L1377:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1376
.L1562:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22481:
	.size	_ZN2v88internal25Cast15ATStandardFrame_302EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEEPNS1_18CodeAssemblerLabelE, .-_ZN2v88internal25Cast15ATStandardFrame_302EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEEPNS1_18CodeAssemblerLabelE
	.section	.text._ZN2v88internal33Cast23ATArgumentsAdaptorFrame_303EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEEPNS1_18CodeAssemblerLabelE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Cast23ATArgumentsAdaptorFrame_303EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEEPNS1_18CodeAssemblerLabelE
	.type	_ZN2v88internal33Cast23ATArgumentsAdaptorFrame_303EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEEPNS1_18CodeAssemblerLabelE, @function
_ZN2v88internal33Cast23ATArgumentsAdaptorFrame_303EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEEPNS1_18CodeAssemblerLabelE:
.LFB22491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-232(%rbp), %r14
	leaq	-1632(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1760(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-1800(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1880, %rsp
	movq	%rsi, -1888(%rbp)
	movq	%rdx, -1904(%rbp)
	movq	%rcx, -1872(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -1800(%rbp)
	movq	%rdi, -1632(%rbp)
	movl	$48, %edi
	movq	$0, -1624(%rbp)
	movq	$0, -1616(%rbp)
	movq	$0, -1608(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	48(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movq	%rax, -1624(%rbp)
	leaq	-1576(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1608(%rbp)
	movq	%rdx, -1616(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1592(%rbp)
	movq	%rax, -1816(%rbp)
	movq	$0, -1600(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1432(%rbp)
	movq	$0, -1424(%rbp)
	movq	%rax, -1440(%rbp)
	movq	$0, -1416(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1432(%rbp)
	leaq	-1384(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1416(%rbp)
	movq	%rdx, -1424(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1400(%rbp)
	movq	%rax, -1864(%rbp)
	movq	$0, -1408(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1240(%rbp)
	movq	$0, -1232(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1240(%rbp)
	leaq	-1192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1208(%rbp)
	movq	%rax, -1832(%rbp)
	movq	$0, -1216(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -1048(%rbp)
	movq	$0, -1040(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1032(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -1048(%rbp)
	leaq	-1000(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1016(%rbp)
	movq	%rax, -1848(%rbp)
	movq	$0, -1024(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -856(%rbp)
	movq	$0, -848(%rbp)
	movq	%rax, -864(%rbp)
	movq	$0, -840(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -856(%rbp)
	leaq	-808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -840(%rbp)
	movq	%rdx, -848(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -824(%rbp)
	movq	%rax, -1840(%rbp)
	movq	$0, -832(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -664(%rbp)
	movq	$0, -656(%rbp)
	movq	%rax, -672(%rbp)
	movq	$0, -648(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -664(%rbp)
	leaq	-616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -632(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rsi
	movq	$0, -472(%rbp)
	movq	%rax, -480(%rbp)
	leaq	-424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1824(%rbp)
	movq	$0, -464(%rbp)
	movq	$0, -456(%rbp)
	movq	$0, -448(%rbp)
	movq	$0, -440(%rbp)
	movq	$0, -432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1800(%rbp), %rax
	movl	$72, %edi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rdx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	xorl	%edx, %edx
	movq	%rax, -280(%rbp)
	movups	%xmm0, -248(%rbp)
	movq	$0, -256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	-1888(%rbp), %xmm1
	movaps	%xmm0, -1760(%rbp)
	movhps	-1904(%rbp), %xmm1
	movq	$0, -1744(%rbp)
	movaps	%xmm1, -1888(%rbp)
	call	_Znwm@PLT
	movdqa	-1888(%rbp), %xmm1
	movq	%r15, %rdi
	movq	%r13, %rsi
	leaq	16(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movups	%xmm1, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1565
	call	_ZdlPv@PLT
.L1565:
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1568(%rbp)
	jne	.L1748
	cmpq	$0, -1376(%rbp)
	jne	.L1749
.L1571:
	cmpq	$0, -1184(%rbp)
	jne	.L1750
.L1574:
	cmpq	$0, -992(%rbp)
	jne	.L1751
.L1578:
	cmpq	$0, -800(%rbp)
	jne	.L1752
.L1581:
	cmpq	$0, -608(%rbp)
	leaq	-288(%rbp), %r15
	jne	.L1753
	cmpq	$0, -416(%rbp)
	jne	.L1754
.L1587:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$1287, %ecx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$5, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1589
	call	_ZdlPv@PLT
.L1589:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	16(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1590
	call	_ZdlPv@PLT
.L1590:
	movq	-272(%rbp), %rbx
	movq	-280(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1591
	.p2align 4,,10
	.p2align 3
.L1595:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1592
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1595
.L1593:
	movq	-280(%rbp), %r14
.L1591:
	testq	%r14, %r14
	je	.L1596
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1596:
	movq	-1824(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1597
	call	_ZdlPv@PLT
.L1597:
	movq	-464(%rbp), %rbx
	movq	-472(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1598
	.p2align 4,,10
	.p2align 3
.L1602:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1599
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1602
.L1600:
	movq	-472(%rbp), %r14
.L1598:
	testq	%r14, %r14
	je	.L1603
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1603:
	movq	-1856(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1604
	call	_ZdlPv@PLT
.L1604:
	movq	-656(%rbp), %rbx
	movq	-664(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1605
	.p2align 4,,10
	.p2align 3
.L1609:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1606
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1609
.L1607:
	movq	-664(%rbp), %r14
.L1605:
	testq	%r14, %r14
	je	.L1610
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1610:
	movq	-1840(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-832(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1611
	call	_ZdlPv@PLT
.L1611:
	movq	-848(%rbp), %rbx
	movq	-856(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1612
	.p2align 4,,10
	.p2align 3
.L1616:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1613
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1616
.L1614:
	movq	-856(%rbp), %r14
.L1612:
	testq	%r14, %r14
	je	.L1617
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1617:
	movq	-1848(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1024(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1618
	call	_ZdlPv@PLT
.L1618:
	movq	-1040(%rbp), %rbx
	movq	-1048(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1619
	.p2align 4,,10
	.p2align 3
.L1623:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1620
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1623
.L1621:
	movq	-1048(%rbp), %r14
.L1619:
	testq	%r14, %r14
	je	.L1624
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1624:
	movq	-1832(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1216(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1625
	call	_ZdlPv@PLT
.L1625:
	movq	-1232(%rbp), %rbx
	movq	-1240(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1626
	.p2align 4,,10
	.p2align 3
.L1630:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1627
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1630
.L1628:
	movq	-1240(%rbp), %r14
.L1626:
	testq	%r14, %r14
	je	.L1631
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1631:
	movq	-1864(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1408(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1632
	call	_ZdlPv@PLT
.L1632:
	movq	-1424(%rbp), %rbx
	movq	-1432(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1633
	.p2align 4,,10
	.p2align 3
.L1637:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1634
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1637
.L1635:
	movq	-1432(%rbp), %r14
.L1633:
	testq	%r14, %r14
	je	.L1638
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1638:
	movq	-1816(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1600(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1639
	call	_ZdlPv@PLT
.L1639:
	movq	-1616(%rbp), %rbx
	movq	-1624(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1640
	.p2align 4,,10
	.p2align 3
.L1644:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1641
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1644
.L1642:
	movq	-1624(%rbp), %r14
.L1640:
	testq	%r14, %r14
	je	.L1645
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1645:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1755
	addq	$1880, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1641:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1644
	jmp	.L1642
	.p2align 4,,10
	.p2align 3
.L1634:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1637
	jmp	.L1635
	.p2align 4,,10
	.p2align 3
.L1627:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1630
	jmp	.L1628
	.p2align 4,,10
	.p2align 3
.L1620:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1623
	jmp	.L1621
	.p2align 4,,10
	.p2align 3
.L1613:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1616
	jmp	.L1614
	.p2align 4,,10
	.p2align 3
.L1606:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1609
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1592:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1595
	jmp	.L1593
	.p2align 4,,10
	.p2align 3
.L1599:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1602
	jmp	.L1600
	.p2align 4,,10
	.p2align 3
.L1748:
	movq	-1816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$1287, %r10d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r10w, (%rax)
	leaq	2(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L1567
	call	_ZdlPv@PLT
.L1567:
	movq	(%r15), %rax
	movl	$131, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r15
	movq	8(%rax), %rax
	movq	%rax, -1904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1904(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal35LoadContextOrFrameTypeFromFrame_299EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEE
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -1888(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1888(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal21Cast11ATFrameType_292EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE
	movq	-1888(%rbp), %rcx
	movq	%r15, %xmm2
	pxor	%xmm0, %xmm0
	movhps	-1904(%rbp), %xmm2
	movl	$32, %edi
	movq	%rax, -72(%rbp)
	leaq	-1792(%rbp), %r15
	movq	%rcx, -80(%rbp)
	movaps	%xmm2, -1904(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -1792(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movq	%r15, %rsi
	leaq	32(%rax), %rdx
	leaq	-1248(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1568
	call	_ZdlPv@PLT
.L1568:
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1752(%rbp)
	jne	.L1756
.L1569:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1376(%rbp)
	je	.L1571
.L1749:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1440(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$1287, %r9d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r9w, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1572
	call	_ZdlPv@PLT
.L1572:
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	leaq	-480(%rbp), %rdi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1573
	call	_ZdlPv@PLT
.L1573:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1184(%rbp)
	je	.L1574
.L1750:
	movq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1248(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	$101188871, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L1575
	call	_ZdlPv@PLT
.L1575:
	movq	(%r15), %rax
	movl	$130, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	24(%rax), %r15
	movq	%rsi, -1912(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -1904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$132, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$19, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal53FromConstexpr11ATFrameType21ATconstexpr_FrameType_291EPNS0_8compiler18CodeAssemblerStateENS0_10StackFrame4TypeE
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r15, -1888(%rbp)
	movq	%rax, %rdx
	call	_ZN2v88internal19FrameTypeEquals_301EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEES6_
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	-1904(%rbp), %xmm3
	movq	%rax, %r15
	movq	-1888(%rbp), %rax
	movaps	%xmm0, -1760(%rbp)
	movhps	-1912(%rbp), %xmm3
	movq	$0, -1744(%rbp)
	movaps	%xmm3, -1904(%rbp)
	movaps	%xmm3, -96(%rbp)
	movq	%rax, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-1056(%rbp), %rdi
	movq	%rax, -1760(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1576
	call	_ZdlPv@PLT
.L1576:
	movq	-1888(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movdqa	-1904(%rbp), %xmm7
	movaps	%xmm0, -1760(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1744(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm4
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-864(%rbp), %rdi
	movq	%rax, -1760(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1577
	call	_ZdlPv@PLT
.L1577:
	movq	-1840(%rbp), %rcx
	movq	-1848(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -992(%rbp)
	je	.L1578
.L1751:
	movq	-1848(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1056(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$1287, %r8d
	movq	%r15, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	3(%rax), %rdx
	movb	$6, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1579
	call	_ZdlPv@PLT
.L1579:
	movq	(%rbx), %rax
	movl	$133, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %r15
	movq	(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %edi
	movq	$0, -1744(%rbp)
	movq	%r15, %xmm5
	movq	%rbx, %xmm0
	movq	%r15, -80(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-672(%rbp), %rdi
	movq	%rax, -1760(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1580
	call	_ZdlPv@PLT
.L1580:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -800(%rbp)
	je	.L1581
.L1752:
	movq	-1840(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-864(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$1287, %edi
	movq	%r13, %rsi
	movw	%di, (%rax)
	leaq	3(%rax), %rdx
	movq	%r15, %rdi
	movb	$6, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1582
	call	_ZdlPv@PLT
.L1582:
	movl	$135, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm0, %xmm0
	leaq	-480(%rbp), %rdi
	movq	%r13, %rsi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1583
	call	_ZdlPv@PLT
.L1583:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1581
	.p2align 4,,10
	.p2align 3
.L1753:
	movq	-1856(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-672(%rbp), %r15
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movl	$1287, %esi
	movq	%r15, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$5, 2(%rax)
	movq	%rax, -1760(%rbp)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1585
	call	_ZdlPv@PLT
.L1585:
	movq	(%rbx), %rax
	movl	$128, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	16(%rax), %r15
	movq	%rcx, -1888(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm0
	movl	$24, %edi
	movq	%r15, -80(%rbp)
	movhps	-1888(%rbp), %xmm0
	leaq	-288(%rbp), %r15
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1760(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm7
	leaq	24(%rax), %rdx
	movq	%rax, -1760(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm7, (%rax)
	movq	%rdx, -1744(%rbp)
	movq	%rdx, -1752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1586
	call	_ZdlPv@PLT
.L1586:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -416(%rbp)
	je	.L1587
.L1754:
	movq	-1824(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-480(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	$0, -1744(%rbp)
	movaps	%xmm0, -1760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1760(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1588
	call	_ZdlPv@PLT
.L1588:
	movq	-1872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1587
	.p2align 4,,10
	.p2align 3
.L1756:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-1888(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1904(%rbp), %xmm7
	movl	$24, %edi
	movaps	%xmm0, -1792(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -1776(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-96(%rbp), %xmm4
	movq	%r15, %rsi
	leaq	24(%rax), %rdx
	leaq	-1440(%rbp), %rdi
	movq	%rax, -1792(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -1776(%rbp)
	movq	%rdx, -1784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1792(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1570
	call	_ZdlPv@PLT
.L1570:
	movq	-1864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1569
.L1755:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22491:
	.size	_ZN2v88internal33Cast23ATArgumentsAdaptorFrame_303EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEEPNS1_18CodeAssemblerLabelE, .-_ZN2v88internal33Cast23ATArgumentsAdaptorFrame_303EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7RawPtrTEEEPNS1_18CodeAssemblerLabelE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal53FromConstexpr11ATFrameType21ATconstexpr_FrameType_291EPNS0_8compiler18CodeAssemblerStateENS0_10StackFrame4TypeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal53FromConstexpr11ATFrameType21ATconstexpr_FrameType_291EPNS0_8compiler18CodeAssemblerStateENS0_10StackFrame4TypeE, @function
_GLOBAL__sub_I__ZN2v88internal53FromConstexpr11ATFrameType21ATconstexpr_FrameType_291EPNS0_8compiler18CodeAssemblerStateENS0_10StackFrame4TypeE:
.LFB29228:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29228:
	.size	_GLOBAL__sub_I__ZN2v88internal53FromConstexpr11ATFrameType21ATconstexpr_FrameType_291EPNS0_8compiler18CodeAssemblerStateENS0_10StackFrame4TypeE, .-_GLOBAL__sub_I__ZN2v88internal53FromConstexpr11ATFrameType21ATconstexpr_FrameType_291EPNS0_8compiler18CodeAssemblerStateENS0_10StackFrame4TypeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal53FromConstexpr11ATFrameType21ATconstexpr_FrameType_291EPNS0_8compiler18CodeAssemblerStateENS0_10StackFrame4TypeE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
