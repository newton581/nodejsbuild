	.file	"proxy-revocable-tq-csa.cc"
	.text
	.section	.rodata._ZN2v88internal23ProxyRevocableAssembler26GenerateProxyRevocableImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../../deps/v8/../../deps/v8/src/builtins/proxy-revocable.tq"
	.section	.rodata._ZN2v88internal23ProxyRevocableAssembler26GenerateProxyRevocableImplEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Proxy.revocable"
	.section	.text._ZN2v88internal23ProxyRevocableAssembler26GenerateProxyRevocableImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23ProxyRevocableAssembler26GenerateProxyRevocableImplEv
	.type	_ZN2v88internal23ProxyRevocableAssembler26GenerateProxyRevocableImplEv, @function
_ZN2v88internal23ProxyRevocableAssembler26GenerateProxyRevocableImplEv:
.LFB22418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$5, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-2224(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2352(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$2504, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, -2408(%rbp)
	movq	%rax, -2392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -2512(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	leaq	-2392(%rbp), %r12
	movq	%rax, -2528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$72, %edi
	movq	$0, -2216(%rbp)
	movq	$0, -2208(%rbp)
	movq	%rax, %rbx
	movq	-2392(%rbp), %rax
	movq	$0, -2200(%rbp)
	movq	%rax, -2224(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -2216(%rbp)
	leaq	-2168(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2200(%rbp)
	movq	%rdx, -2208(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2184(%rbp)
	movq	%rax, -2416(%rbp)
	movq	$0, -2192(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$96, %edi
	movq	$0, -2024(%rbp)
	movq	$0, -2016(%rbp)
	movq	%rax, -2032(%rbp)
	movq	$0, -2008(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -2024(%rbp)
	leaq	-1976(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2008(%rbp)
	movq	%rdx, -2016(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1992(%rbp)
	movq	%rax, -2488(%rbp)
	movq	$0, -2000(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1832(%rbp)
	movq	$0, -1824(%rbp)
	movq	%rax, -1840(%rbp)
	movq	$0, -1816(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1832(%rbp)
	leaq	-1784(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1816(%rbp)
	movq	%rdx, -1824(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1800(%rbp)
	movq	%rax, -2440(%rbp)
	movq	$0, -1808(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1640(%rbp)
	movq	$0, -1632(%rbp)
	movq	%rax, -1648(%rbp)
	movq	$0, -1624(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1640(%rbp)
	leaq	-1592(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1624(%rbp)
	movq	%rdx, -1632(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1608(%rbp)
	movq	%rax, -2448(%rbp)
	movq	$0, -1616(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$96, %edi
	movq	$0, -1448(%rbp)
	movq	$0, -1440(%rbp)
	movq	%rax, -1456(%rbp)
	movq	$0, -1432(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -1448(%rbp)
	leaq	-1400(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1432(%rbp)
	movq	%rdx, -1440(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1416(%rbp)
	movq	%rax, -2456(%rbp)
	movq	$0, -1424(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1256(%rbp)
	movq	$0, -1248(%rbp)
	movq	%rax, -1264(%rbp)
	movq	$0, -1240(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1256(%rbp)
	leaq	-1208(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1240(%rbp)
	movq	%rdx, -1248(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1224(%rbp)
	movq	%rax, -2496(%rbp)
	movq	$0, -1232(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1064(%rbp)
	movq	$0, -1056(%rbp)
	movq	%rax, -1072(%rbp)
	movq	$0, -1048(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1064(%rbp)
	leaq	-1016(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1048(%rbp)
	movq	%rdx, -1056(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1032(%rbp)
	movq	%rax, -2472(%rbp)
	movq	$0, -1040(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$120, %edi
	movq	$0, -872(%rbp)
	movq	$0, -864(%rbp)
	movq	%rax, -880(%rbp)
	movq	$0, -856(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -872(%rbp)
	leaq	-824(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -856(%rbp)
	movq	%rdx, -864(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -840(%rbp)
	movq	%rax, -2480(%rbp)
	movq	$0, -848(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$120, %edi
	movq	$0, -680(%rbp)
	movq	$0, -672(%rbp)
	movq	%rax, -688(%rbp)
	movq	$0, -664(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -680(%rbp)
	leaq	-632(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -664(%rbp)
	movq	%rdx, -672(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -648(%rbp)
	movq	%rax, -2464(%rbp)
	movq	$0, -656(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$72, %edi
	movq	$0, -488(%rbp)
	movq	$0, -480(%rbp)
	movq	%rax, -496(%rbp)
	movq	$0, -472(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r12, %rsi
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -488(%rbp)
	leaq	-440(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -472(%rbp)
	movq	%rdx, -480(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -456(%rbp)
	movq	%rax, -2424(%rbp)
	movq	$0, -464(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2392(%rbp), %rax
	movl	$72, %edi
	movq	$0, -296(%rbp)
	movq	$0, -288(%rbp)
	movq	%rax, -304(%rbp)
	movq	$0, -280(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	leaq	72(%rax), %rdx
	movq	%r12, %rsi
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	$0, 64(%rax)
	movq	%rax, -296(%rbp)
	leaq	-248(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -280(%rbp)
	movq	%rdx, -288(%rbp)
	xorl	%edx, %edx
	movq	%rax, -2432(%rbp)
	movups	%xmm0, -264(%rbp)
	movq	$0, -272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	-2512(%rbp), %xmm1
	movaps	%xmm0, -2352(%rbp)
	movhps	-2528(%rbp), %xmm1
	movq	%rbx, -96(%rbp)
	movaps	%xmm1, -112(%rbp)
	movq	$0, -2336(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm2
	leaq	24(%rax), %rdx
	movq	%rax, -2352(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm2, (%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2
	call	_ZdlPv@PLT
.L2:
	movq	-2416(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2160(%rbp)
	jne	.L266
	cmpq	$0, -1968(%rbp)
	jne	.L267
.L8:
	cmpq	$0, -1776(%rbp)
	jne	.L268
.L11:
	cmpq	$0, -1584(%rbp)
	jne	.L269
.L15:
	cmpq	$0, -1392(%rbp)
	jne	.L270
.L18:
	cmpq	$0, -1200(%rbp)
	jne	.L271
.L23:
	cmpq	$0, -1008(%rbp)
	jne	.L272
.L26:
	cmpq	$0, -816(%rbp)
	jne	.L273
.L30:
	cmpq	$0, -624(%rbp)
	jne	.L274
.L33:
	cmpq	$0, -432(%rbp)
	jne	.L275
.L38:
	cmpq	$0, -240(%rbp)
	jne	.L276
.L40:
	movq	-2432(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L42
	call	_ZdlPv@PLT
.L42:
	movq	-288(%rbp), %rbx
	movq	-296(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L43
	.p2align 4,,10
	.p2align 3
.L47:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L44
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L47
.L45:
	movq	-296(%rbp), %r13
.L43:
	testq	%r13, %r13
	je	.L48
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L48:
	movq	-2424(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-464(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L49
	call	_ZdlPv@PLT
.L49:
	movq	-480(%rbp), %rbx
	movq	-488(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L50
	.p2align 4,,10
	.p2align 3
.L54:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L51
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L54
.L52:
	movq	-488(%rbp), %r13
.L50:
	testq	%r13, %r13
	je	.L55
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L55:
	movq	-2464(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L56
	call	_ZdlPv@PLT
.L56:
	movq	-672(%rbp), %rbx
	movq	-680(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L57
	.p2align 4,,10
	.p2align 3
.L61:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L58
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L61
.L59:
	movq	-680(%rbp), %r13
.L57:
	testq	%r13, %r13
	je	.L62
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L62:
	movq	-2480(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-848(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L63
	call	_ZdlPv@PLT
.L63:
	movq	-864(%rbp), %rbx
	movq	-872(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L64
	.p2align 4,,10
	.p2align 3
.L68:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L65
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L68
.L66:
	movq	-872(%rbp), %r13
.L64:
	testq	%r13, %r13
	je	.L69
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L69:
	movq	-2472(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1040(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L70
	call	_ZdlPv@PLT
.L70:
	movq	-1056(%rbp), %rbx
	movq	-1064(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L71
	.p2align 4,,10
	.p2align 3
.L75:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L72
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L75
.L73:
	movq	-1064(%rbp), %r13
.L71:
	testq	%r13, %r13
	je	.L76
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L76:
	movq	-2496(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L77
	call	_ZdlPv@PLT
.L77:
	movq	-1248(%rbp), %rbx
	movq	-1256(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L78
	.p2align 4,,10
	.p2align 3
.L82:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L79
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L82
.L80:
	movq	-1256(%rbp), %r13
.L78:
	testq	%r13, %r13
	je	.L83
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L83:
	movq	-2456(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L84
	call	_ZdlPv@PLT
.L84:
	movq	-1440(%rbp), %rbx
	movq	-1448(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L85
	.p2align 4,,10
	.p2align 3
.L89:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L86
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L89
.L87:
	movq	-1448(%rbp), %r13
.L85:
	testq	%r13, %r13
	je	.L90
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L90:
	movq	-2448(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1616(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L91
	call	_ZdlPv@PLT
.L91:
	movq	-1632(%rbp), %rbx
	movq	-1640(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L92
	.p2align 4,,10
	.p2align 3
.L96:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L93
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L96
.L94:
	movq	-1640(%rbp), %r13
.L92:
	testq	%r13, %r13
	je	.L97
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L97:
	movq	-2440(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1808(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L98
	call	_ZdlPv@PLT
.L98:
	movq	-1824(%rbp), %rbx
	movq	-1832(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L99
	.p2align 4,,10
	.p2align 3
.L103:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L100
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L103
.L101:
	movq	-1832(%rbp), %r13
.L99:
	testq	%r13, %r13
	je	.L104
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L104:
	movq	-2488(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2000(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L105
	call	_ZdlPv@PLT
.L105:
	movq	-2016(%rbp), %rbx
	movq	-2024(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L106
	.p2align 4,,10
	.p2align 3
.L110:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L107
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L110
.L108:
	movq	-2024(%rbp), %r13
.L106:
	testq	%r13, %r13
	je	.L111
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L111:
	movq	-2416(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L112
	call	_ZdlPv@PLT
.L112:
	movq	-2208(%rbp), %rbx
	movq	-2216(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L113
	.p2align 4,,10
	.p2align 3
.L117:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L114
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L117
.L115:
	movq	-2216(%rbp), %r13
.L113:
	testq	%r13, %r13
	je	.L118
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L118:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L277
	addq	$2504, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L117
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L107:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L110
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L100:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L103
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L93:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L96
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L86:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L89
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L79:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L82
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L72:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L75
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L65:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L68
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L58:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L61
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L44:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L47
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L51:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L54
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L266:
	movq	-2416(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r8d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%r8w, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L4
	call	_ZdlPv@PLT
.L4:
	movq	(%rbx), %rax
	movl	$19, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %r15
	movq	(%rax), %rbx
	movq	16(%rax), %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rdx
	movq	%r13, %rcx
	movq	%rbx, %rsi
	movq	-2408(%rbp), %rdi
	call	_ZN2v88internal21Cast10JSReceiver_1404EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %xmm4
	movq	%r14, %xmm3
	movq	%r15, %xmm5
	punpcklqdq	%xmm4, %xmm3
	movq	%rbx, %xmm4
	pxor	%xmm0, %xmm0
	movq	%rax, -80(%rbp)
	punpcklqdq	%xmm5, %xmm4
	movl	$40, %edi
	movaps	%xmm3, -96(%rbp)
	leaq	-2384(%rbp), %r15
	movaps	%xmm3, -2528(%rbp)
	movaps	%xmm4, -2512(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm0, -2384(%rbp)
	movq	$0, -2368(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm4
	leaq	40(%rax), %rdx
	leaq	-1840(%rbp), %rdi
	movq	%rax, -2384(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2368(%rbp)
	movq	%rdx, -2376(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5
	call	_ZdlPv@PLT
.L5:
	movq	-2440(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2344(%rbp)
	jne	.L278
.L6:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1968(%rbp)
	je	.L8
.L267:
	movq	-2488(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2032(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L9
	call	_ZdlPv@PLT
.L9:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm5
	movaps	%xmm0, -2352(%rbp)
	movq	$0, -2336(%rbp)
	movq	%rdx, -96(%rbp)
	movaps	%xmm5, -112(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-112(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-496(%rbp), %rdi
	movq	%rax, -2352(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm6, (%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	movq	-2424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1776(%rbp)
	je	.L11
.L268:
	movq	-2440(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1840(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134744071, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L12
	call	_ZdlPv@PLT
.L12:
	movq	(%rbx), %rax
	movl	$20, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	32(%rax), %r15
	movq	%rcx, -2512(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -2528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2408(%rbp), %rdi
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal18IsRevokedProxy_319EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEE@PLT
	movq	%r15, %xmm4
	movq	%rbx, %xmm6
	movq	-2528(%rbp), %xmm5
	movhps	-2512(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	$0, -2336(%rbp)
	punpcklqdq	%xmm4, %xmm5
	movaps	%xmm6, -112(%rbp)
	movq	%rax, %r14
	movaps	%xmm5, -2528(%rbp)
	movaps	%xmm6, -2512(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-1648(%rbp), %rdi
	movq	%rax, -2352(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L13
	call	_ZdlPv@PLT
.L13:
	movdqa	-2512(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-2528(%rbp), %xmm5
	movaps	%xmm0, -2352(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movq	$0, -2336(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm2
	movq	%r13, %rsi
	leaq	32(%rax), %rdx
	leaq	-1456(%rbp), %rdi
	movq	%rax, -2352(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L14
	call	_ZdlPv@PLT
.L14:
	movq	-2456(%rbp), %rcx
	movq	-2448(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1584(%rbp)
	je	.L15
.L269:
	movq	-2448(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1648(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L16
	call	_ZdlPv@PLT
.L16:
	movq	(%rbx), %rax
	movl	$21, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %r14
	movq	16(%rax), %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %xmm4
	movq	%rbx, %xmm0
	movl	$24, %edi
	punpcklqdq	%xmm4, %xmm0
	movq	%r15, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2352(%rbp)
	movq	$0, -2336(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-112(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-304(%rbp), %rdi
	movq	%rax, -2352(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L17
	call	_ZdlPv@PLT
.L17:
	movq	-2432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1392(%rbp)
	je	.L18
.L270:
	movq	-2456(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1456(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L19
	call	_ZdlPv@PLT
.L19:
	movq	(%rbx), %rax
	movl	$25, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	16(%rax), %r14
	movq	(%rax), %rbx
	movq	24(%rax), %r15
	movq	%rcx, -2512(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	-2408(%rbp), %rdi
	call	_ZN2v88internal21Cast10JSReceiver_1404EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r15, %xmm6
	movq	%r14, %xmm7
	movq	%rbx, %xmm2
	punpcklqdq	%xmm6, %xmm7
	movhps	-2512(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movaps	%xmm7, -2528(%rbp)
	leaq	-2384(%rbp), %r15
	movaps	%xmm2, -2512(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm0, -2384(%rbp)
	movq	%r14, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -2368(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm2
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm7
	movdqa	-80(%rbp), %xmm3
	leaq	48(%rax), %rdx
	leaq	-1072(%rbp), %rdi
	movq	%rax, -2384(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm3, 32(%rax)
	movq	%rdx, -2368(%rbp)
	movq	%rdx, -2376(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L20
	call	_ZdlPv@PLT
.L20:
	movq	-2472(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2344(%rbp)
	jne	.L279
.L21:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1200(%rbp)
	je	.L23
.L271:
	movq	-2496(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1264(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L24
	call	_ZdlPv@PLT
.L24:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm6
	movaps	%xmm0, -2352(%rbp)
	movq	$0, -2336(%rbp)
	movq	%rdx, -96(%rbp)
	movaps	%xmm6, -112(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-112(%rbp), %xmm4
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-496(%rbp), %rdi
	movq	%rax, -2352(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm4, (%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L25
	call	_ZdlPv@PLT
.L25:
	movq	-2424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1008(%rbp)
	je	.L26
.L272:
	movq	-2472(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1072(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movl	$1800, %edi
	movq	%r13, %rsi
	movw	%di, 4(%rax)
	leaq	6(%rax), %rdx
	movq	%r14, %rdi
	movl	$117966855, (%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L27
	call	_ZdlPv@PLT
.L27:
	movq	(%rbx), %rax
	movl	$26, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	24(%rax), %rsi
	movq	(%rax), %rbx
	movq	40(%rax), %r15
	movq	%rcx, -2512(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -2536(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -2528(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2408(%rbp), %rdi
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal18IsRevokedProxy_319EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEE@PLT
	movq	%rbx, %xmm3
	pxor	%xmm0, %xmm0
	movq	-2528(%rbp), %xmm7
	movhps	-2512(%rbp), %xmm3
	movl	$40, %edi
	movq	%r15, -80(%rbp)
	movq	%rax, %r14
	movhps	-2536(%rbp), %xmm7
	movaps	%xmm3, -2512(%rbp)
	movaps	%xmm7, -2528(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm0, -2352(%rbp)
	movq	$0, -2336(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm2
	leaq	40(%rax), %rdx
	leaq	-880(%rbp), %rdi
	movq	%rax, -2352(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L28
	call	_ZdlPv@PLT
.L28:
	movdqa	-2512(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movdqa	-2528(%rbp), %xmm7
	movaps	%xmm0, -2352(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	%r15, -80(%rbp)
	movq	$0, -2336(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm6
	leaq	40(%rax), %rdx
	leaq	-688(%rbp), %rdi
	movq	%rax, -2352(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L29
	call	_ZdlPv@PLT
.L29:
	movq	-2464(%rbp), %rcx
	movq	-2480(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -816(%rbp)
	je	.L30
.L273:
	movq	-2480(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-880(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L31
	call	_ZdlPv@PLT
.L31:
	movq	(%rbx), %rax
	movl	$27, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	8(%rax), %r14
	movq	16(%rax), %r15
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %xmm4
	movq	%rbx, %xmm0
	movl	$24, %edi
	punpcklqdq	%xmm4, %xmm0
	movq	%r15, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2352(%rbp)
	movq	$0, -2336(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-112(%rbp), %xmm5
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	leaq	-304(%rbp), %rdi
	movq	%rax, -2352(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm5, (%rax)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L32
	call	_ZdlPv@PLT
.L32:
	movq	-2432(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -624(%rbp)
	je	.L33
.L274:
	movq	-2464(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	leaq	-688(%rbp), %r8
	xorl	%ebx, %ebx
	movq	%r8, -2512(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movq	-2512(%rbp), %r8
	movq	%r13, %rsi
	movl	$117966855, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%r8, %rdi
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L34
	movq	%rax, -2512(%rbp)
	call	_ZdlPv@PLT
	movq	-2512(%rbp), %rax
.L34:
	movq	(%rax), %rax
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	cmovne	%rdx, %r14
	movq	24(%rax), %rdx
	movq	32(%rax), %rax
	testq	%rdx, %rdx
	cmovne	%rdx, %r15
	testq	%rax, %rax
	movl	$31, %edx
	cmovne	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2408(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal24ProxiesCodeStubAssembler13AllocateProxyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEES7_@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$37, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2408(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal24ProxiesCodeStubAssembler27AllocateProxyRevokeFunctionENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_7JSProxyEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$43, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	-2408(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN2v88internal28NewJSProxyRevocableResult_58EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSProxyEEENS4_INS0_10JSFunctionEEE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -432(%rbp)
	je	.L38
.L275:
	movq	-2424(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-496(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movl	$2055, %esi
	movq	%r14, %rdi
	movw	%si, (%rax)
	leaq	3(%rax), %rdx
	movq	%r13, %rsi
	movb	$8, 2(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L39
	call	_ZdlPv@PLT
.L39:
	movq	(%rbx), %rax
	movl	$46, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2408(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	movl	$139, %edx
	movq	%r14, %rsi
	leaq	.LC1(%rip), %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -240(%rbp)
	je	.L40
.L276:
	movq	-2432(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-304(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$3, %edi
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_Znwm@PLT
	movl	$2055, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movw	%cx, (%rax)
	leaq	3(%rax), %rdx
	movb	$8, 2(%rax)
	movq	%rax, -2352(%rbp)
	movq	%rdx, -2336(%rbp)
	movq	%rdx, -2344(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2352(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L41
	call	_ZdlPv@PLT
.L41:
	movq	(%rbx), %rax
	movl	$49, %edx
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rsi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2408(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	xorl	%r8d, %r8d
	movl	$135, %edx
	leaq	.LC1(%rip), %rcx
	movq	%r14, %rsi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L278:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movdqa	-2512(%rbp), %xmm2
	movaps	%xmm0, -2384(%rbp)
	movaps	%xmm2, -112(%rbp)
	movdqa	-2528(%rbp), %xmm2
	movq	$0, -2368(%rbp)
	movaps	%xmm2, -96(%rbp)
	call	_Znwm@PLT
	movdqa	-112(%rbp), %xmm1
	movdqa	-96(%rbp), %xmm2
	movq	%r15, %rsi
	leaq	32(%rax), %rdx
	leaq	-2032(%rbp), %rdi
	movq	%rax, -2384(%rbp)
	movups	%xmm1, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -2368(%rbp)
	movq	%rdx, -2376(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L7
	call	_ZdlPv@PLT
.L7:
	movq	-2488(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L279:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movdqa	-2512(%rbp), %xmm1
	movdqa	-2528(%rbp), %xmm7
	movq	%r14, -80(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm0, -2384(%rbp)
	movq	$0, -2368(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm6
	leaq	40(%rax), %rdx
	leaq	-1264(%rbp), %rdi
	movq	%rax, -2384(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -2368(%rbp)
	movq	%rdx, -2376(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L22
	call	_ZdlPv@PLT
.L22:
	movq	-2496(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L21
.L277:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22418:
	.size	_ZN2v88internal23ProxyRevocableAssembler26GenerateProxyRevocableImplEv, .-_ZN2v88internal23ProxyRevocableAssembler26GenerateProxyRevocableImplEv
	.section	.rodata._ZN2v88internal8Builtins23Generate_ProxyRevocableEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/proxy-revocable-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins23Generate_ProxyRevocableEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"ProxyRevocable"
	.section	.text._ZN2v88internal8Builtins23Generate_ProxyRevocableEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins23Generate_ProxyRevocableEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins23Generate_ProxyRevocableEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins23Generate_ProxyRevocableEPNS0_8compiler18CodeAssemblerStateE:
.LFB22414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$146, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$857, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L284
.L281:
	movq	%r13, %rdi
	call	_ZN2v88internal23ProxyRevocableAssembler26GenerateProxyRevocableImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L285
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L281
.L285:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22414:
	.size	_ZN2v88internal8Builtins23Generate_ProxyRevocableEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins23Generate_ProxyRevocableEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins23Generate_ProxyRevocableEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins23Generate_ProxyRevocableEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins23Generate_ProxyRevocableEPNS0_8compiler18CodeAssemblerStateE:
.LFB28959:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28959:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins23Generate_ProxyRevocableEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins23Generate_ProxyRevocableEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins23Generate_ProxyRevocableEPNS0_8compiler18CodeAssemblerStateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
