	.file	"proxy-delete-property-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.rodata._ZN2v88internal28ProxyDeletePropertyAssembler31GenerateProxyDeletePropertyImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../../deps/v8/../../deps/v8/src/builtins/proxy-delete-property.tq"
	.section	.rodata._ZN2v88internal28ProxyDeletePropertyAssembler31GenerateProxyDeletePropertyImplEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	"deleteProperty"
	.section	.text._ZN2v88internal28ProxyDeletePropertyAssembler31GenerateProxyDeletePropertyImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28ProxyDeletePropertyAssembler31GenerateProxyDeletePropertyImplEv
	.type	_ZN2v88internal28ProxyDeletePropertyAssembler31GenerateProxyDeletePropertyImplEv, @function
_ZN2v88internal28ProxyDeletePropertyAssembler31GenerateProxyDeletePropertyImplEv:
.LFB22418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-2752(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2624(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$2984, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, -2808(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -2944(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -2960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	leaq	-2808(%rbp), %r12
	movq	%rax, -2976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$96, %edi
	movq	$0, -2616(%rbp)
	movq	%rax, -2992(%rbp)
	movq	-2808(%rbp), %rax
	movq	$0, -2608(%rbp)
	movq	%rax, -2624(%rbp)
	movq	$0, -2600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -2616(%rbp)
	leaq	-2568(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2600(%rbp)
	movq	%rdx, -2608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2584(%rbp)
	movq	%rax, -2824(%rbp)
	movq	$0, -2592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2808(%rbp), %rax
	movl	$120, %edi
	movq	$0, -2424(%rbp)
	movq	$0, -2416(%rbp)
	movq	%rax, -2432(%rbp)
	movq	$0, -2408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -2424(%rbp)
	leaq	-2376(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2408(%rbp)
	movq	%rdx, -2416(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2392(%rbp)
	movq	%rax, -2920(%rbp)
	movq	$0, -2400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2808(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2232(%rbp)
	movq	$0, -2224(%rbp)
	movq	%rax, -2240(%rbp)
	movq	$0, -2216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2232(%rbp)
	leaq	-2184(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2216(%rbp)
	movq	%rdx, -2224(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2200(%rbp)
	movq	%rax, -2832(%rbp)
	movq	$0, -2208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2808(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movq	%rax, -2048(%rbp)
	movq	$0, -2024(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -2040(%rbp)
	leaq	-1992(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2024(%rbp)
	movq	%rdx, -2032(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2008(%rbp)
	movq	%rax, -2912(%rbp)
	movq	$0, -2016(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2808(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1848(%rbp)
	movq	$0, -1840(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1832(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1848(%rbp)
	leaq	-1800(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1832(%rbp)
	movq	%rdx, -1840(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1816(%rbp)
	movq	%rax, -2840(%rbp)
	movq	$0, -1824(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2808(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1656(%rbp)
	movq	$0, -1648(%rbp)
	movq	%rax, -1664(%rbp)
	movq	$0, -1640(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1656(%rbp)
	leaq	-1608(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1640(%rbp)
	movq	%rdx, -1648(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1624(%rbp)
	movq	%rax, -2856(%rbp)
	movq	$0, -1632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2808(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1464(%rbp)
	movq	$0, -1456(%rbp)
	movq	%rax, -1472(%rbp)
	movq	$0, -1448(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1464(%rbp)
	leaq	-1416(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1448(%rbp)
	movq	%rdx, -1456(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1432(%rbp)
	movq	%rax, -2872(%rbp)
	movq	$0, -1440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2808(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1272(%rbp)
	movq	$0, -1264(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -1256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1272(%rbp)
	leaq	-1224(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1256(%rbp)
	movq	%rdx, -1264(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1240(%rbp)
	movq	%rax, -2888(%rbp)
	movq	$0, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2808(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1080(%rbp)
	movq	$0, -1072(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$0, -1064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1080(%rbp)
	leaq	-1032(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1064(%rbp)
	movq	%rdx, -1072(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1048(%rbp)
	movq	%rax, -2896(%rbp)
	movq	$0, -1056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2808(%rbp), %rax
	movl	$192, %edi
	movq	$0, -888(%rbp)
	movq	$0, -880(%rbp)
	movq	%rax, -896(%rbp)
	movq	$0, -872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -888(%rbp)
	leaq	-840(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -872(%rbp)
	movq	%rdx, -880(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -856(%rbp)
	movq	%rax, -2904(%rbp)
	movq	$0, -864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2808(%rbp), %rax
	movl	$192, %edi
	movq	$0, -696(%rbp)
	movq	$0, -688(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -696(%rbp)
	leaq	-648(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -680(%rbp)
	movq	%rdx, -688(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -664(%rbp)
	movq	%rax, -2864(%rbp)
	movq	$0, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2808(%rbp), %rax
	movl	$120, %edi
	movq	$0, -504(%rbp)
	movq	$0, -496(%rbp)
	movq	%rax, -512(%rbp)
	movq	$0, -488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -504(%rbp)
	leaq	-456(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -488(%rbp)
	movq	%rdx, -496(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -472(%rbp)
	movq	%rax, -2880(%rbp)
	movq	$0, -480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2808(%rbp), %rax
	movl	$96, %edi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%r12, %rsi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -312(%rbp)
	leaq	-264(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	xorl	%edx, %edx
	movq	%rax, -2848(%rbp)
	movups	%xmm0, -280(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-2944(%rbp), %xmm1
	movaps	%xmm0, -2752(%rbp)
	movhps	-2960(%rbp), %xmm1
	movq	$0, -2736(%rbp)
	movaps	%xmm1, -128(%rbp)
	movq	-2976(%rbp), %xmm1
	movhps	-2992(%rbp), %xmm1
	movaps	%xmm1, -112(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm2
	movq	%r13, %rdi
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm3
	leaq	32(%rax), %rdx
	movq	%rax, -2752(%rbp)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L8
	call	_ZdlPv@PLT
.L8:
	movq	-2824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2560(%rbp)
	jne	.L314
	cmpq	$0, -2368(%rbp)
	jne	.L315
.L14:
	cmpq	$0, -2176(%rbp)
	jne	.L316
.L17:
	cmpq	$0, -1984(%rbp)
	jne	.L317
.L22:
	cmpq	$0, -1792(%rbp)
	jne	.L318
.L25:
	cmpq	$0, -1600(%rbp)
	jne	.L319
.L28:
	cmpq	$0, -1408(%rbp)
	jne	.L320
.L31:
	cmpq	$0, -1216(%rbp)
	jne	.L321
.L38:
	cmpq	$0, -1024(%rbp)
	jne	.L322
.L42:
	cmpq	$0, -832(%rbp)
	jne	.L323
.L44:
	cmpq	$0, -640(%rbp)
	jne	.L324
.L46:
	cmpq	$0, -448(%rbp)
	jne	.L325
.L52:
	cmpq	$0, -256(%rbp)
	jne	.L326
.L54:
	movq	-2848(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L56
	call	_ZdlPv@PLT
.L56:
	movq	-304(%rbp), %rbx
	movq	-312(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L57
	.p2align 4,,10
	.p2align 3
.L61:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L58
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L61
.L59:
	movq	-312(%rbp), %r13
.L57:
	testq	%r13, %r13
	je	.L62
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L62:
	movq	-2880(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L63
	call	_ZdlPv@PLT
.L63:
	movq	-496(%rbp), %rbx
	movq	-504(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L64
	.p2align 4,,10
	.p2align 3
.L68:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L65
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L68
.L66:
	movq	-504(%rbp), %r13
.L64:
	testq	%r13, %r13
	je	.L69
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L69:
	movq	-2864(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L70
	call	_ZdlPv@PLT
.L70:
	movq	-688(%rbp), %rbx
	movq	-696(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L71
	.p2align 4,,10
	.p2align 3
.L75:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L72
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L75
.L73:
	movq	-696(%rbp), %r13
.L71:
	testq	%r13, %r13
	je	.L76
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L76:
	movq	-2904(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-864(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L77
	call	_ZdlPv@PLT
.L77:
	movq	-880(%rbp), %rbx
	movq	-888(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L78
	.p2align 4,,10
	.p2align 3
.L82:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L79
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L82
.L80:
	movq	-888(%rbp), %r13
.L78:
	testq	%r13, %r13
	je	.L83
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L83:
	movq	-2896(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L84
	call	_ZdlPv@PLT
.L84:
	movq	-1072(%rbp), %rbx
	movq	-1080(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L85
	.p2align 4,,10
	.p2align 3
.L89:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L86
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L89
.L87:
	movq	-1080(%rbp), %r13
.L85:
	testq	%r13, %r13
	je	.L90
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L90:
	movq	-2888(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L91
	call	_ZdlPv@PLT
.L91:
	movq	-1264(%rbp), %rbx
	movq	-1272(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L92
	.p2align 4,,10
	.p2align 3
.L96:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L93
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L96
.L94:
	movq	-1272(%rbp), %r13
.L92:
	testq	%r13, %r13
	je	.L97
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L97:
	movq	-2872(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1440(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L98
	call	_ZdlPv@PLT
.L98:
	movq	-1456(%rbp), %rbx
	movq	-1464(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L99
	.p2align 4,,10
	.p2align 3
.L103:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L100
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L103
.L101:
	movq	-1464(%rbp), %r13
.L99:
	testq	%r13, %r13
	je	.L104
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L104:
	movq	-2856(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1632(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L105
	call	_ZdlPv@PLT
.L105:
	movq	-1648(%rbp), %rbx
	movq	-1656(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L106
	.p2align 4,,10
	.p2align 3
.L110:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L107
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L110
.L108:
	movq	-1656(%rbp), %r13
.L106:
	testq	%r13, %r13
	je	.L111
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L111:
	movq	-2840(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L112
	call	_ZdlPv@PLT
.L112:
	movq	-1840(%rbp), %rbx
	movq	-1848(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L113
	.p2align 4,,10
	.p2align 3
.L117:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L114
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L117
.L115:
	movq	-1848(%rbp), %r13
.L113:
	testq	%r13, %r13
	je	.L118
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L118:
	movq	-2912(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2016(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L119
	call	_ZdlPv@PLT
.L119:
	movq	-2032(%rbp), %rbx
	movq	-2040(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L120
	.p2align 4,,10
	.p2align 3
.L124:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L121
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L124
.L122:
	movq	-2040(%rbp), %r13
.L120:
	testq	%r13, %r13
	je	.L125
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L125:
	movq	-2832(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L126
	call	_ZdlPv@PLT
.L126:
	movq	-2224(%rbp), %rbx
	movq	-2232(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L127
	.p2align 4,,10
	.p2align 3
.L131:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L128
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L131
.L129:
	movq	-2232(%rbp), %r13
.L127:
	testq	%r13, %r13
	je	.L132
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L132:
	movq	-2920(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L133
	call	_ZdlPv@PLT
.L133:
	movq	-2416(%rbp), %rbx
	movq	-2424(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L134
	.p2align 4,,10
	.p2align 3
.L138:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L135
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L138
.L136:
	movq	-2424(%rbp), %r13
.L134:
	testq	%r13, %r13
	je	.L139
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L139:
	movq	-2824(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L140
	call	_ZdlPv@PLT
.L140:
	movq	-2608(%rbp), %rbx
	movq	-2616(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L141
	.p2align 4,,10
	.p2align 3
.L145:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L142
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L145
.L143:
	movq	-2616(%rbp), %r13
.L141:
	testq	%r13, %r13
	je	.L146
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L146:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L327
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L145
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L135:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L138
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L128:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L131
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L121:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L124
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L114:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L117
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L107:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L110
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L100:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L103
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L93:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L96
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L86:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L89
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L79:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L82
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L72:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L75
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L58:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L61
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L65:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L68
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L314:
	movq	-2824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$101123847, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	movq	(%rbx), %rax
	movl	$16, %edx
	movq	%r12, %rdi
	movq	16(%rax), %rsi
	movq	8(%rax), %rcx
	movq	(%rax), %r13
	movq	24(%rax), %rax
	movq	%rsi, -2960(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rcx, -2944(%rbp)
	movq	%rax, -2976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$28, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-2944(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE(%rip), %esi
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal20Cast10JSReceiver_140EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %xmm5
	pxor	%xmm0, %xmm0
	movq	-2960(%rbp), %xmm4
	movhps	-2944(%rbp), %xmm5
	movl	$48, %edi
	movaps	%xmm0, -2784(%rbp)
	leaq	-2784(%rbp), %r13
	movhps	-2976(%rbp), %xmm4
	movaps	%xmm5, -2944(%rbp)
	movaps	%xmm4, -2960(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movq	%rbx, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	$0, -2768(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	48(%rax), %rdx
	leaq	-2240(%rbp), %rdi
	movq	%rax, -2784(%rbp)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -2768(%rbp)
	movq	%rdx, -2776(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L11
	call	_ZdlPv@PLT
.L11:
	movq	-2832(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2744(%rbp)
	jne	.L328
.L12:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2368(%rbp)
	je	.L14
.L315:
	movq	-2920(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2432(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$101123847, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L15
	call	_ZdlPv@PLT
.L15:
	movq	(%rbx), %rax
	movl	$32, %edi
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm5
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	movq	%r15, %rsi
	leaq	32(%rax), %rdx
	leaq	-320(%rbp), %rdi
	movq	%rax, -2752(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L16
	call	_ZdlPv@PLT
.L16:
	movq	-2848(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2176(%rbp)
	je	.L17
.L316:
	movq	-2832(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2240(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r10d
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$101123847, (%rax)
	leaq	6(%rax), %rdx
	movw	%r10w, 4(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L18
	call	_ZdlPv@PLT
.L18:
	movq	(%rbx), %rax
	movl	$31, %edx
	movq	8(%rax), %rcx
	movq	16(%rax), %rsi
	movq	24(%rax), %rdi
	movq	(%rax), %rbx
	movq	%rcx, -2944(%rbp)
	movq	40(%rax), %rcx
	movq	%rsi, -2976(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rdi, -2992(%rbp)
	movq	%r12, %rdi
	movq	%rcx, -2960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-2944(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE(%rip), %esi
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, -3008(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3008(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal27UnsafeCast10JSReceiver_1457EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$35, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -3008(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %r8
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-2960(%rbp), %r13
	leaq	.LC2(%rip), %rcx
	movq	%r13, %rdx
	call	_ZN2v88internal13GetMethod_245EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPKcPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %xmm6
	movq	%rbx, %xmm2
	movq	-2976(%rbp), %xmm7
	movhps	-3008(%rbp), %xmm6
	movhps	-2944(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movhps	-2992(%rbp), %xmm7
	movq	%r13, -80(%rbp)
	leaq	-2784(%rbp), %r13
	movaps	%xmm6, -3008(%rbp)
	movaps	%xmm7, -2976(%rbp)
	movaps	%xmm2, -2944(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm0, -2784(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -2768(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm6
	leaq	64(%rax), %rdx
	leaq	-1856(%rbp), %rdi
	movups	%xmm4, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movq	%rax, -2784(%rbp)
	movq	%rdx, -2768(%rbp)
	movq	%rdx, -2776(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L19
	call	_ZdlPv@PLT
.L19:
	movq	-2840(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2744(%rbp)
	jne	.L329
.L20:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1984(%rbp)
	je	.L22
.L317:
	movq	-2912(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2048(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r9d
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$101123847, (%rax)
	leaq	7(%rax), %rdx
	movw	%r9w, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L23
	call	_ZdlPv@PLT
.L23:
	movq	(%rbx), %rax
	movl	$48, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movdqu	(%rax), %xmm7
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm5
	leaq	48(%rax), %rdx
	leaq	-1664(%rbp), %rdi
	movq	%rax, -2752(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L24
	call	_ZdlPv@PLT
.L24:
	movq	-2856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1792(%rbp)
	je	.L25
.L318:
	movq	-2840(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1856(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movabsq	$506381209849759495, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L26
	call	_ZdlPv@PLT
.L26:
	movq	(%rbx), %rax
	movl	$56, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movq	56(%rax), %rdx
	movdqu	(%rax), %xmm6
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -80(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm7
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm4
	leaq	56(%rax), %rdx
	leaq	-1472(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm4, 32(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L27
	call	_ZdlPv@PLT
.L27:
	movq	-2872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1600(%rbp)
	je	.L28
.L319:
	movq	-2856(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1664(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r8d
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$101123847, (%rax)
	leaq	6(%rax), %rdx
	movw	%r8w, 4(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L29
	call	_ZdlPv@PLT
.L29:
	movq	(%rbx), %rax
	movl	$36, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	24(%rax), %rsi
	movq	(%rax), %r13
	movq	40(%rax), %rbx
	movq	%rcx, -2944(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -2976(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rcx, -2960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %xmm0
	movl	$40, %edi
	movq	%rbx, -96(%rbp)
	movhps	-2944(%rbp), %xmm0
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	-2960(%rbp), %xmm0
	movhps	-2976(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-128(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm6
	leaq	40(%rax), %rdx
	leaq	-512(%rbp), %rdi
	movq	%rax, -2752(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L30
	call	_ZdlPv@PLT
.L30:
	movq	-2880(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1408(%rbp)
	je	.L31
.L320:
	movq	-2872(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1472(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movl	$1799, %edi
	movq	%r15, %rsi
	movw	%di, 4(%rax)
	leaq	7(%rax), %rdx
	movq	%r13, %rdi
	movl	$101123847, (%rax)
	movb	$7, 6(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L32
	call	_ZdlPv@PLT
.L32:
	movq	(%rbx), %rax
	movl	$35, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	leaq	-2800(%rbp), %rbx
	movq	(%rax), %rcx
	movq	32(%rax), %r13
	movq	%rcx, -2960(%rbp)
	movq	8(%rax), %rcx
	movq	%rcx, -2976(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -2992(%rbp)
	movq	24(%rax), %rcx
	movq	%rcx, -3024(%rbp)
	movq	40(%rax), %rcx
	movq	48(%rax), %rax
	movq	%rcx, -2944(%rbp)
	movq	%rax, -3008(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$40, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19IsUndefinedConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	je	.L33
.L35:
	movq	%r13, %xmm4
	movq	%rbx, %rdi
	movhps	-2944(%rbp), %xmm4
	movaps	%xmm4, -2944(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	xorl	%edx, %edx
	movq	%rax, %rsi
.L313:
	movq	%r15, %rdi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	%rbx, %rdi
	movl	$2, %esi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-2752(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, -2928(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-128(%rbp), %rsi
	movl	$5, %edi
	movq	-3008(%rbp), %xmm0
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rdi
	movl	$1, %ecx
	pushq	%rsi
	movq	-2960(%rbp), %r9
	movhps	-2928(%rbp), %xmm0
	leaq	-2784(%rbp), %rdx
	movq	%rax, -2784(%rbp)
	movq	-2736(%rbp), %rax
	movq	%rbx, %rdi
	xorl	%esi, %esi
	movdqa	-2944(%rbp), %xmm4
	movaps	%xmm0, -128(%rbp)
	movq	%rax, -2776(%rbp)
	movq	-2992(%rbp), %rax
	movaps	%xmm4, -112(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	popq	%rcx
	movq	%rbx, %rdi
	popq	%rsi
	movq	-2960(%rbp), %xmm3
	movq	%rax, %r13
	movq	-2992(%rbp), %xmm4
	movhps	-2976(%rbp), %xmm3
	movhps	-3024(%rbp), %xmm4
	movaps	%xmm3, -2976(%rbp)
	movaps	%xmm4, -2960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$43, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal13ToBoolean_240EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15Word32BinaryNotENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %xmm7
	pxor	%xmm0, %xmm0
	movq	-3008(%rbp), %xmm5
	movdqa	-2976(%rbp), %xmm3
	movdqa	-2960(%rbp), %xmm4
	movl	$64, %edi
	movaps	%xmm0, -2752(%rbp)
	movdqa	-2944(%rbp), %xmm2
	punpcklqdq	%xmm7, %xmm5
	movq	$0, -2736(%rbp)
	movaps	%xmm5, -2992(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm4
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm6
	leaq	64(%rax), %rdx
	leaq	-1280(%rbp), %rdi
	movups	%xmm5, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm6, 48(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L36
	call	_ZdlPv@PLT
.L36:
	movdqa	-2976(%rbp), %xmm7
	movdqa	-2960(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movdqa	-2944(%rbp), %xmm2
	movdqa	-2992(%rbp), %xmm4
	movaps	%xmm0, -2752(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movq	$0, -2736(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm7
	movdqa	-80(%rbp), %xmm5
	leaq	64(%rax), %rdx
	leaq	-704(%rbp), %rdi
	movups	%xmm3, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L37
	call	_ZdlPv@PLT
.L37:
	movq	-2864(%rbp), %rcx
	movq	-2888(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1216(%rbp)
	je	.L38
.L321:
	movq	-2888(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1280(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movabsq	$578438803887687431, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L39
	call	_ZdlPv@PLT
.L39:
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	movq	40(%rax), %rdi
	movq	32(%rax), %rdx
	movq	(%rax), %rcx
	movq	%rsi, -2960(%rbp)
	movq	16(%rax), %rsi
	movq	%rdi, -3008(%rbp)
	movq	48(%rax), %rdi
	movq	24(%rax), %rbx
	movq	56(%rax), %rax
	movq	%rsi, -2976(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rdx, -2992(%rbp)
	movl	$44, %edx
	movq	%rdi, -3024(%rbp)
	movq	%r12, %rdi
	movq	%rcx, -2944(%rbp)
	movq	%rax, -2928(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11SmiConstantEi@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm2
	pxor	%xmm0, %xmm0
	movq	-2976(%rbp), %xmm3
	movq	-3024(%rbp), %xmm6
	movl	$64, %edi
	movq	-2992(%rbp), %xmm7
	movaps	%xmm0, -2752(%rbp)
	movq	$0, -2736(%rbp)
	punpcklqdq	%xmm2, %xmm3
	movq	-2944(%rbp), %xmm2
	movhps	-2928(%rbp), %xmm6
	movhps	-3008(%rbp), %xmm7
	movaps	%xmm3, -2976(%rbp)
	movhps	-2960(%rbp), %xmm2
	movaps	%xmm6, -3024(%rbp)
	movaps	%xmm7, -2992(%rbp)
	movaps	%xmm2, -2944(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm6, -80(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm3
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	leaq	64(%rax), %rdx
	leaq	-1088(%rbp), %rdi
	movups	%xmm4, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L40
	call	_ZdlPv@PLT
.L40:
	movdqa	-2944(%rbp), %xmm5
	movdqa	-2976(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movdqa	-2992(%rbp), %xmm4
	movdqa	-3024(%rbp), %xmm3
	movaps	%xmm0, -2752(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movq	$0, -2736(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm2
	leaq	64(%rax), %rdx
	leaq	-896(%rbp), %rdi
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm2, 48(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L41
	call	_ZdlPv@PLT
.L41:
	movq	-2904(%rbp), %rcx
	movq	-2896(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1024(%rbp)
	je	.L42
.L322:
	movq	-2896(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1088(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movabsq	$578438803887687431, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L43
	call	_ZdlPv@PLT
.L43:
	movq	(%rbx), %rax
	movl	$45, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r10
	movq	16(%rax), %rbx
	movq	%r10, -2944(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal59FromConstexpr20UT5ATSmi10HeapObject18ATconstexpr_string_168EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	-2944(%rbp), %r10
	movl	$150, %edx
	movq	%r15, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -832(%rbp)
	je	.L44
.L323:
	movq	-2904(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-896(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movabsq	$578438803887687431, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L45
	call	_ZdlPv@PLT
.L45:
	movl	$47, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8False_66EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -640(%rbp)
	je	.L46
.L324:
	movq	-2864(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	leaq	-704(%rbp), %r8
	movq	$0, -2944(%rbp)
	movq	%r8, -2976(%rbp)
	movq	$0, -2960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	-2976(%rbp), %r8
	movq	%r15, %rsi
	movabsq	$578438803887687431, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%r8, %rdi
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L47
	movq	%rax, -2976(%rbp)
	call	_ZdlPv@PLT
	movq	-2976(%rbp), %rax
.L47:
	movq	(%rax), %rax
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	cmovne	%rdx, %r13
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	cmove	-2944(%rbp), %rdx
	movq	%rdx, -2944(%rbp)
	movq	16(%rax), %rdx
	movq	40(%rax), %rax
	testq	%rdx, %rdx
	cmovne	%rdx, %rbx
	testq	%rax, %rax
	cmove	-2960(%rbp), %rax
	movl	$56, %edx
	movq	%rax, -2960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %r8
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	-2944(%rbp), %rcx
	movq	-2960(%rbp), %rdx
	call	_ZN2v88internal24ProxiesCodeStubAssembler21CheckDeleteTrapResultENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_7JSProxyEEENS3_INS0_4NameEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$59, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal7True_65EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -448(%rbp)
	je	.L52
.L325:
	movq	-2880(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-512(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$101123847, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L53
	call	_ZdlPv@PLT
.L53:
	movq	(%rbx), %rax
	movl	$63, %edx
	movq	%r12, %rdi
	leaq	-2800(%rbp), %r13
	leaq	.LC1(%rip), %rsi
	movq	(%rax), %r9
	movq	16(%rax), %rcx
	movq	24(%rax), %rbx
	movq	32(%rax), %rax
	movq	%r9, -2976(%rbp)
	movq	%rcx, -2944(%rbp)
	movq	%rax, -2960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$160, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-2752(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -112(%rbp)
	movl	$3, %ebx
	xorl	%esi, %esi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	pushq	%rbx
	movq	%rax, %r8
	movq	%r13, %rdi
	movq	%rcx, -2784(%rbp)
	leaq	-128(%rbp), %rcx
	movq	-2960(%rbp), %xmm0
	leaq	-2784(%rbp), %rdx
	movq	-2976(%rbp), %r9
	movq	-2736(%rbp), %rax
	pushq	%rcx
	movl	$1, %ecx
	movhps	-2944(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%rax, -2776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -256(%rbp)
	popq	%rax
	popq	%rdx
	je	.L54
.L326:
	movq	-2848(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-320(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$101123847, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L55
	call	_ZdlPv@PLT
.L55:
	movq	(%rbx), %rax
	movl	$66, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movl	$145, %edx
	leaq	.LC2(%rip), %rcx
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IsNullConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L35
	movq	%r13, %xmm4
	movq	%rbx, %rdi
	movhps	-2944(%rbp), %xmm4
	movaps	%xmm4, -2944(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	movq	%rax, %rsi
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L328:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movdqa	-2944(%rbp), %xmm3
	movaps	%xmm0, -2784(%rbp)
	movaps	%xmm3, -128(%rbp)
	movdqa	-2960(%rbp), %xmm3
	movq	%rbx, -96(%rbp)
	movaps	%xmm3, -112(%rbp)
	movq	$0, -2768(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm6
	leaq	40(%rax), %rdx
	leaq	-2432(%rbp), %rdi
	movq	%rax, -2784(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -2768(%rbp)
	movq	%rdx, -2776(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L13
	call	_ZdlPv@PLT
.L13:
	movq	-2920(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L329:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2960(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-2944(%rbp), %xmm7
	movdqa	-2976(%rbp), %xmm5
	movdqa	-3008(%rbp), %xmm2
	movl	$56, %edi
	movaps	%xmm0, -2784(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -2768(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm6
	leaq	56(%rax), %rdx
	leaq	-2048(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rax, -2784(%rbp)
	movq	%rdx, -2768(%rbp)
	movq	%rdx, -2776(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L21
	call	_ZdlPv@PLT
.L21:
	movq	-2912(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L20
.L327:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22418:
	.size	_ZN2v88internal28ProxyDeletePropertyAssembler31GenerateProxyDeletePropertyImplEv, .-_ZN2v88internal28ProxyDeletePropertyAssembler31GenerateProxyDeletePropertyImplEv
	.section	.rodata._ZN2v88internal8Builtins28Generate_ProxyDeletePropertyEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/proxy-delete-property-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins28Generate_ProxyDeletePropertyEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"ProxyDeleteProperty"
	.section	.text._ZN2v88internal8Builtins28Generate_ProxyDeletePropertyEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins28Generate_ProxyDeletePropertyEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins28Generate_ProxyDeletePropertyEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins28Generate_ProxyDeletePropertyEPNS0_8compiler18CodeAssemblerStateE:
.LFB22414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$146, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$851, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L334
.L331:
	movq	%r13, %rdi
	call	_ZN2v88internal28ProxyDeletePropertyAssembler31GenerateProxyDeletePropertyImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L335
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L331
.L335:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22414:
	.size	_ZN2v88internal8Builtins28Generate_ProxyDeletePropertyEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins28Generate_ProxyDeletePropertyEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_ProxyDeletePropertyEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_ProxyDeletePropertyEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_ProxyDeletePropertyEPNS0_8compiler18CodeAssemblerStateE:
.LFB29023:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29023:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_ProxyDeletePropertyEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_ProxyDeletePropertyEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins28Generate_ProxyDeletePropertyEPNS0_8compiler18CodeAssemblerStateE
	.weak	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
