	.file	"proxy-constructor-tq-csa.cc"
	.text
	.section	.rodata._ZN2v88internal25ProxyConstructorAssembler28GenerateProxyConstructorImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../../deps/v8/../../deps/v8/src/builtins/proxy-constructor.tq"
	.section	.rodata._ZN2v88internal25ProxyConstructorAssembler28GenerateProxyConstructorImplEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Proxy"
	.section	.text._ZN2v88internal25ProxyConstructorAssembler28GenerateProxyConstructorImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25ProxyConstructorAssembler28GenerateProxyConstructorImplEv
	.type	_ZN2v88internal25ProxyConstructorAssembler28GenerateProxyConstructorImplEv, @function
_ZN2v88internal25ProxyConstructorAssembler28GenerateProxyConstructorImplEv:
.LFB22418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$5, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-2752(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2624(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$2952, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, -2792(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -2928(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, -2944(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -2960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	leaq	-2792(%rbp), %r12
	movq	%rax, -2976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$120, %edi
	movq	$0, -2616(%rbp)
	movq	$0, -2608(%rbp)
	movq	%rax, %rbx
	movq	-2792(%rbp), %rax
	movq	$0, -2600(%rbp)
	movq	%rax, -2624(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -2616(%rbp)
	leaq	-2568(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2600(%rbp)
	movq	%rdx, -2608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2584(%rbp)
	movq	%rax, -2808(%rbp)
	movq	$0, -2592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2792(%rbp), %rax
	movl	$120, %edi
	movq	$0, -2424(%rbp)
	movq	$0, -2416(%rbp)
	movq	%rax, -2432(%rbp)
	movq	$0, -2408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -2424(%rbp)
	leaq	-2376(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2408(%rbp)
	movq	%rdx, -2416(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2392(%rbp)
	movq	%rax, -2848(%rbp)
	movq	$0, -2400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2792(%rbp), %rax
	movl	$120, %edi
	movq	$0, -2232(%rbp)
	movq	$0, -2224(%rbp)
	movq	%rax, -2240(%rbp)
	movq	$0, -2216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -2232(%rbp)
	leaq	-2184(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2216(%rbp)
	movq	%rdx, -2224(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2200(%rbp)
	movq	%rax, -2832(%rbp)
	movq	$0, -2208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2792(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movq	%rax, -2048(%rbp)
	movq	$0, -2024(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2040(%rbp)
	leaq	-1992(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2024(%rbp)
	movq	%rdx, -2032(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2008(%rbp)
	movq	%rax, -2896(%rbp)
	movq	$0, -2016(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2792(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1848(%rbp)
	movq	$0, -1840(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1832(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1848(%rbp)
	leaq	-1800(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1832(%rbp)
	movq	%rdx, -1840(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1816(%rbp)
	movq	%rax, -2840(%rbp)
	movq	$0, -1824(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2792(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1656(%rbp)
	movq	$0, -1648(%rbp)
	movq	%rax, -1664(%rbp)
	movq	$0, -1640(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1656(%rbp)
	leaq	-1608(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1640(%rbp)
	movq	%rdx, -1648(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1624(%rbp)
	movq	%rax, -2872(%rbp)
	movq	$0, -1632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2792(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1464(%rbp)
	movq	$0, -1456(%rbp)
	movq	%rax, -1472(%rbp)
	movq	$0, -1448(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1464(%rbp)
	leaq	-1416(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1448(%rbp)
	movq	%rdx, -1456(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1432(%rbp)
	movq	%rax, -2856(%rbp)
	movq	$0, -1440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2792(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1272(%rbp)
	movq	$0, -1264(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -1256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1272(%rbp)
	leaq	-1224(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1256(%rbp)
	movq	%rdx, -1264(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1240(%rbp)
	movq	%rax, -2904(%rbp)
	movq	$0, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2792(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1080(%rbp)
	movq	$0, -1072(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$0, -1064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1080(%rbp)
	leaq	-1032(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1064(%rbp)
	movq	%rdx, -1072(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1048(%rbp)
	movq	%rax, -2864(%rbp)
	movq	$0, -1056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2792(%rbp), %rax
	movl	$168, %edi
	movq	$0, -888(%rbp)
	movq	$0, -880(%rbp)
	movq	%rax, -896(%rbp)
	movq	$0, -872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -888(%rbp)
	leaq	-840(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -872(%rbp)
	movq	%rdx, -880(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -856(%rbp)
	movq	%rax, -2888(%rbp)
	movq	$0, -864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2792(%rbp), %rax
	movl	$168, %edi
	movq	$0, -696(%rbp)
	movq	$0, -688(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -696(%rbp)
	leaq	-648(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -680(%rbp)
	movq	%rdx, -688(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -664(%rbp)
	movq	%rax, -2880(%rbp)
	movq	$0, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2792(%rbp), %rax
	movl	$120, %edi
	movq	$0, -504(%rbp)
	movq	$0, -496(%rbp)
	movq	%rax, -512(%rbp)
	movq	$0, -488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	$0, 112(%rax)
	leaq	120(%rax), %rdx
	movq	%r12, %rsi
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -504(%rbp)
	leaq	-456(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -488(%rbp)
	movq	%rdx, -496(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -472(%rbp)
	movq	%rax, -2816(%rbp)
	movq	$0, -480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2792(%rbp), %rax
	movl	$120, %edi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	$0, 112(%rax)
	leaq	120(%rax), %rdx
	movq	%r12, %rsi
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -312(%rbp)
	leaq	-264(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	xorl	%edx, %edx
	movq	%rax, -2824(%rbp)
	movups	%xmm0, -280(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	-2928(%rbp), %xmm1
	movaps	%xmm0, -2752(%rbp)
	movhps	-2944(%rbp), %xmm1
	movq	%rbx, -96(%rbp)
	movaps	%xmm1, -128(%rbp)
	movq	-2960(%rbp), %xmm1
	movq	$0, -2736(%rbp)
	movhps	-2976(%rbp), %xmm1
	movaps	%xmm1, -112(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r13, %rdi
	movq	%r15, %rsi
	movdqa	-128(%rbp), %xmm2
	movdqa	-112(%rbp), %xmm3
	leaq	40(%rax), %rdx
	movq	%rax, -2752(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2
	call	_ZdlPv@PLT
.L2:
	movq	-2808(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2560(%rbp)
	jne	.L310
	cmpq	$0, -2368(%rbp)
	jne	.L311
.L7:
	cmpq	$0, -2176(%rbp)
	jne	.L312
.L9:
	cmpq	$0, -1984(%rbp)
	jne	.L313
.L14:
	cmpq	$0, -1792(%rbp)
	jne	.L314
.L17:
	cmpq	$0, -1600(%rbp)
	jne	.L315
.L21:
	cmpq	$0, -1408(%rbp)
	jne	.L316
.L24:
	cmpq	$0, -1216(%rbp)
	jne	.L317
.L29:
	cmpq	$0, -1024(%rbp)
	jne	.L318
.L32:
	cmpq	$0, -832(%rbp)
	jne	.L319
.L36:
	cmpq	$0, -640(%rbp)
	jne	.L320
.L39:
	cmpq	$0, -448(%rbp)
	jne	.L321
.L44:
	cmpq	$0, -256(%rbp)
	jne	.L322
.L46:
	movq	-2824(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L48
	call	_ZdlPv@PLT
.L48:
	movq	-304(%rbp), %rbx
	movq	-312(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L49
	.p2align 4,,10
	.p2align 3
.L53:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L50
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L53
.L51:
	movq	-312(%rbp), %r13
.L49:
	testq	%r13, %r13
	je	.L54
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L54:
	movq	-2816(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L55
	call	_ZdlPv@PLT
.L55:
	movq	-496(%rbp), %rbx
	movq	-504(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L56
	.p2align 4,,10
	.p2align 3
.L60:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L57
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L60
.L58:
	movq	-504(%rbp), %r13
.L56:
	testq	%r13, %r13
	je	.L61
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L61:
	movq	-2880(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L62
	call	_ZdlPv@PLT
.L62:
	movq	-688(%rbp), %rbx
	movq	-696(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L63
	.p2align 4,,10
	.p2align 3
.L67:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L64
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L67
.L65:
	movq	-696(%rbp), %r13
.L63:
	testq	%r13, %r13
	je	.L68
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L68:
	movq	-2888(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-864(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L69
	call	_ZdlPv@PLT
.L69:
	movq	-880(%rbp), %rbx
	movq	-888(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L70
	.p2align 4,,10
	.p2align 3
.L74:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L71
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L74
.L72:
	movq	-888(%rbp), %r13
.L70:
	testq	%r13, %r13
	je	.L75
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L75:
	movq	-2864(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L76
	call	_ZdlPv@PLT
.L76:
	movq	-1072(%rbp), %rbx
	movq	-1080(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L77
	.p2align 4,,10
	.p2align 3
.L81:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L78
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L81
.L79:
	movq	-1080(%rbp), %r13
.L77:
	testq	%r13, %r13
	je	.L82
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L82:
	movq	-2904(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L83
	call	_ZdlPv@PLT
.L83:
	movq	-1264(%rbp), %rbx
	movq	-1272(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L84
	.p2align 4,,10
	.p2align 3
.L88:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L85
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L88
.L86:
	movq	-1272(%rbp), %r13
.L84:
	testq	%r13, %r13
	je	.L89
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L89:
	movq	-2856(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1440(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L90
	call	_ZdlPv@PLT
.L90:
	movq	-1456(%rbp), %rbx
	movq	-1464(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L91
	.p2align 4,,10
	.p2align 3
.L95:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L92
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L95
.L93:
	movq	-1464(%rbp), %r13
.L91:
	testq	%r13, %r13
	je	.L96
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L96:
	movq	-2872(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1632(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L97
	call	_ZdlPv@PLT
.L97:
	movq	-1648(%rbp), %rbx
	movq	-1656(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L98
	.p2align 4,,10
	.p2align 3
.L102:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L99
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L102
.L100:
	movq	-1656(%rbp), %r13
.L98:
	testq	%r13, %r13
	je	.L103
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L103:
	movq	-2840(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L104
	call	_ZdlPv@PLT
.L104:
	movq	-1840(%rbp), %rbx
	movq	-1848(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L105
	.p2align 4,,10
	.p2align 3
.L109:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L106
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L109
.L107:
	movq	-1848(%rbp), %r13
.L105:
	testq	%r13, %r13
	je	.L110
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L110:
	movq	-2896(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2016(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L111
	call	_ZdlPv@PLT
.L111:
	movq	-2032(%rbp), %rbx
	movq	-2040(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L112
	.p2align 4,,10
	.p2align 3
.L116:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L113
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L116
.L114:
	movq	-2040(%rbp), %r13
.L112:
	testq	%r13, %r13
	je	.L117
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L117:
	movq	-2832(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L118
	call	_ZdlPv@PLT
.L118:
	movq	-2224(%rbp), %rbx
	movq	-2232(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L119
	.p2align 4,,10
	.p2align 3
.L123:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L120
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L123
.L121:
	movq	-2232(%rbp), %r13
.L119:
	testq	%r13, %r13
	je	.L124
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L124:
	movq	-2848(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L125
	call	_ZdlPv@PLT
.L125:
	movq	-2416(%rbp), %rbx
	movq	-2424(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L126
	.p2align 4,,10
	.p2align 3
.L130:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L127
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L130
.L128:
	movq	-2424(%rbp), %r13
.L126:
	testq	%r13, %r13
	je	.L131
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L131:
	movq	-2808(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L132
	call	_ZdlPv@PLT
.L132:
	movq	-2608(%rbp), %rbx
	movq	-2616(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L133
	.p2align 4,,10
	.p2align 3
.L137:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L134
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L137
.L135:
	movq	-2616(%rbp), %r13
.L133:
	testq	%r13, %r13
	je	.L138
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L138:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L323
	addq	$2952, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L137
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L127:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L130
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L120:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L123
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L113:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L116
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L106:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L109
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L99:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L102
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L92:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L95
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L85:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L88
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L78:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L81
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L71:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L74
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L64:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L67
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L50:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L53
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L57:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L60
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L310:
	movq	-2808(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$134744071, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L4
	call	_ZdlPv@PLT
.L4:
	movq	(%rbx), %rax
	movl	$17, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	32(%rax), %rbx
	movq	%rsi, -2960(%rbp)
	movq	24(%rax), %rsi
	movq	%rcx, -2944(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -2976(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -2928(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2928(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	-2928(%rbp), %xmm4
	movq	-2944(%rbp), %xmm5
	movq	%rbx, -96(%rbp)
	movhps	-2976(%rbp), %xmm4
	movaps	%xmm0, -2752(%rbp)
	movhps	-2960(%rbp), %xmm5
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm4, -2928(%rbp)
	movaps	%xmm5, -2944(%rbp)
	movaps	%xmm5, -128(%rbp)
	movq	$0, -2736(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	leaq	40(%rax), %rdx
	leaq	-2432(%rbp), %rdi
	movq	%rax, -2752(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5
	call	_ZdlPv@PLT
.L5:
	movdqa	-2944(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movdqa	-2928(%rbp), %xmm7
	movaps	%xmm0, -2752(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -2736(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-128(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm7
	leaq	40(%rax), %rdx
	leaq	-2240(%rbp), %rdi
	movq	%rax, -2752(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6
	call	_ZdlPv@PLT
.L6:
	movq	-2832(%rbp), %rcx
	movq	-2848(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2368(%rbp)
	je	.L7
.L311:
	movq	-2848(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2432(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$134744071, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L8
	call	_ZdlPv@PLT
.L8:
	movq	(%rbx), %rax
	movl	$18, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	movl	$40, %edx
	movq	%r13, %rsi
	leaq	.LC1(%rip), %rcx
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -2176(%rbp)
	je	.L9
.L312:
	movq	-2832(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2240(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$134744071, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	movq	(%rbx), %rax
	movl	$30, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	24(%rax), %r13
	movq	%rcx, -2928(%rbp)
	movq	16(%rax), %rcx
	movq	32(%rax), %rax
	movq	%rcx, -2944(%rbp)
	movq	%rax, -2960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal21Cast10JSReceiver_1404EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %xmm7
	movq	%r13, %xmm3
	movq	-2960(%rbp), %xmm6
	movq	%rbx, %xmm2
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movq	%rax, -80(%rbp)
	punpcklqdq	%xmm7, %xmm6
	movq	-2944(%rbp), %xmm7
	movhps	-2928(%rbp), %xmm2
	movaps	%xmm0, -2784(%rbp)
	movaps	%xmm6, -2960(%rbp)
	leaq	-2784(%rbp), %rbx
	punpcklqdq	%xmm3, %xmm7
	movaps	%xmm2, -2928(%rbp)
	movaps	%xmm7, -2944(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movq	$0, -2768(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm6
	movq	-80(%rbp), %rcx
	leaq	-1856(%rbp), %rdi
	movdqa	-112(%rbp), %xmm2
	leaq	56(%rax), %rdx
	movq	%rbx, %rsi
	movq	%rax, -2784(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-96(%rbp), %xmm6
	movq	%rcx, 48(%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -2768(%rbp)
	movq	%rdx, -2776(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L11
	call	_ZdlPv@PLT
.L11:
	movq	-2840(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2744(%rbp)
	jne	.L324
.L12:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1984(%rbp)
	je	.L14
.L313:
	movq	-2896(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2048(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r11d
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$134744071, (%rax)
	leaq	6(%rax), %rdx
	movw	%r11w, 4(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L15
	call	_ZdlPv@PLT
.L15:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	32(%rax), %rdx
	movdqu	(%rax), %xmm7
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-128(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm2
	leaq	40(%rax), %rdx
	leaq	-512(%rbp), %rdi
	movq	%rax, -2752(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L16
	call	_ZdlPv@PLT
.L16:
	movq	-2816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1792(%rbp)
	je	.L17
.L314:
	movq	-2840(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1856(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r10d
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$134744071, (%rax)
	leaq	7(%rax), %rdx
	movw	%r10w, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L18
	call	_ZdlPv@PLT
.L18:
	movq	(%rbx), %rax
	movl	$31, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	24(%rax), %rsi
	movq	(%rax), %rbx
	movq	%rcx, -2928(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -2960(%rbp)
	movq	32(%rax), %rsi
	movq	48(%rax), %rax
	movq	%rcx, -2944(%rbp)
	movq	%rsi, -2976(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, -2992(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18IsRevokedProxy_319EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEE@PLT
	movq	%rbx, %xmm5
	pxor	%xmm0, %xmm0
	movq	-2976(%rbp), %xmm3
	movhps	-2928(%rbp), %xmm5
	movl	$48, %edi
	movq	%rax, %r13
	movq	-2944(%rbp), %xmm4
	movhps	-2992(%rbp), %xmm3
	movaps	%xmm5, -2928(%rbp)
	movhps	-2960(%rbp), %xmm4
	movaps	%xmm3, -2976(%rbp)
	movaps	%xmm4, -2944(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -2752(%rbp)
	movq	$0, -2736(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	leaq	48(%rax), %rdx
	leaq	-1664(%rbp), %rdi
	movq	%rax, -2752(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L19
	call	_ZdlPv@PLT
.L19:
	movdqa	-2928(%rbp), %xmm7
	movdqa	-2944(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movdqa	-2976(%rbp), %xmm2
	movaps	%xmm0, -2752(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	$0, -2736(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	leaq	48(%rax), %rdx
	leaq	-1472(%rbp), %rdi
	movq	%rax, -2752(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L20
	call	_ZdlPv@PLT
.L20:
	movq	-2856(%rbp), %rcx
	movq	-2872(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1600(%rbp)
	je	.L21
.L315:
	movq	-2872(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1664(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movl	$1800, %r9d
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$134744071, (%rax)
	leaq	6(%rax), %rdx
	movw	%r9w, 4(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L22
	call	_ZdlPv@PLT
.L22:
	movq	(%rbx), %rax
	movl	$32, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	24(%rax), %rsi
	movq	(%rax), %r13
	movq	32(%rax), %rbx
	movq	%rcx, -2928(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -2960(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -2944(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %xmm0
	movl	$40, %edi
	movq	%rbx, -96(%rbp)
	movhps	-2928(%rbp), %xmm0
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	-2944(%rbp), %xmm0
	movhps	-2960(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-128(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm3
	leaq	40(%rax), %rdx
	leaq	-320(%rbp), %rdi
	movq	%rax, -2752(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L23
	call	_ZdlPv@PLT
.L23:
	movq	-2824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1408(%rbp)
	je	.L24
.L316:
	movq	-2856(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1472(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movl	$1800, %r8d
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$134744071, (%rax)
	leaq	6(%rax), %rdx
	movw	%r8w, 4(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L25
	call	_ZdlPv@PLT
.L25:
	movq	(%rbx), %rax
	movl	$36, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	24(%rax), %rsi
	movq	(%rax), %rbx
	movq	32(%rax), %r13
	movq	%rcx, -2928(%rbp)
	movq	16(%rax), %rcx
	movq	40(%rax), %rax
	movq	%rsi, -2960(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -2944(%rbp)
	movq	%rax, -2976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal21Cast10JSReceiver_1404EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm3
	movq	%r13, %xmm6
	movq	-2944(%rbp), %xmm7
	movhps	-2976(%rbp), %xmm6
	movhps	-2928(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movhps	-2960(%rbp), %xmm7
	movaps	%xmm3, -128(%rbp)
	leaq	-2784(%rbp), %rbx
	movaps	%xmm6, -2976(%rbp)
	movaps	%xmm7, -2944(%rbp)
	movaps	%xmm3, -2928(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm0, -2784(%rbp)
	movq	%r13, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -2768(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm2
	movdqa	-112(%rbp), %xmm4
	movq	%rbx, %rsi
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm6
	leaq	64(%rax), %rdx
	leaq	-1088(%rbp), %rdi
	movups	%xmm2, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movq	%rax, -2784(%rbp)
	movq	%rdx, -2768(%rbp)
	movq	%rdx, -2776(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L26
	call	_ZdlPv@PLT
.L26:
	movq	-2864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2744(%rbp)
	jne	.L325
.L27:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1216(%rbp)
	je	.L29
.L317:
	movq	-2904(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1280(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movl	$1800, %edi
	movq	%r15, %rsi
	movw	%di, 4(%rax)
	leaq	7(%rax), %rdx
	movq	%r13, %rdi
	movl	$134744071, (%rax)
	movb	$8, 6(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L30
	call	_ZdlPv@PLT
.L30:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	32(%rax), %rdx
	movdqu	(%rax), %xmm7
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-128(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm2
	leaq	40(%rax), %rdx
	leaq	-512(%rbp), %rdi
	movq	%rax, -2752(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L31
	call	_ZdlPv@PLT
.L31:
	movq	-2816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1024(%rbp)
	je	.L32
.L318:
	movq	-2864(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1088(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movabsq	$506662689155057671, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L33
	call	_ZdlPv@PLT
.L33:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rsi
	movq	8(%rax), %rcx
	movq	40(%rax), %rdx
	movq	(%rax), %r13
	movq	%rsi, -2976(%rbp)
	movq	32(%rax), %rsi
	movq	56(%rax), %rbx
	movq	%rcx, -2944(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -2992(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -2912(%rbp)
	movl	$37, %edx
	movq	%rcx, -2960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	%r13, -2928(%rbp)
	call	_ZN2v88internal18IsRevokedProxy_319EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEE@PLT
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movq	-2992(%rbp), %xmm2
	movq	-2960(%rbp), %xmm4
	movaps	%xmm0, -2752(%rbp)
	movq	%rax, %r13
	movq	-2928(%rbp), %xmm5
	movhps	-2912(%rbp), %xmm2
	movq	%rbx, -80(%rbp)
	movhps	-2976(%rbp), %xmm4
	movhps	-2944(%rbp), %xmm5
	movaps	%xmm2, -2992(%rbp)
	movaps	%xmm4, -2960(%rbp)
	movaps	%xmm5, -2928(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	$0, -2736(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	leaq	56(%rax), %rdx
	leaq	-896(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L34
	call	_ZdlPv@PLT
.L34:
	movdqa	-2928(%rbp), %xmm7
	movdqa	-2960(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movdqa	-2992(%rbp), %xmm2
	movq	%rbx, -80(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -2752(%rbp)
	movq	$0, -2736(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	leaq	56(%rax), %rdx
	leaq	-704(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L35
	call	_ZdlPv@PLT
.L35:
	movq	-2880(%rbp), %rcx
	movq	-2888(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -832(%rbp)
	je	.L36
.L319:
	movq	-2888(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-896(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movl	$1800, %esi
	movq	%r13, %rdi
	movw	%si, 4(%rax)
	leaq	7(%rax), %rdx
	movq	%r15, %rsi
	movl	$134744071, (%rax)
	movb	$7, 6(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L37
	call	_ZdlPv@PLT
.L37:
	movq	(%rbx), %rax
	movl	$38, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	24(%rax), %rsi
	movq	(%rax), %r13
	movq	32(%rax), %rbx
	movq	%rcx, -2928(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -2960(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rcx, -2944(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %xmm0
	movl	$40, %edi
	movq	%rbx, -96(%rbp)
	movhps	-2928(%rbp), %xmm0
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	-2944(%rbp), %xmm0
	movhps	-2960(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-128(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm3
	leaq	40(%rax), %rdx
	leaq	-320(%rbp), %rdi
	movq	%rax, -2752(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L38
	call	_ZdlPv@PLT
.L38:
	movq	-2824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -640(%rbp)
	je	.L39
.L320:
	movq	-2880(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -2944(%rbp)
	leaq	-704(%rbp), %rbx
	movq	$0, -2928(%rbp)
	xorl	%r13d, %r13d
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movl	$1800, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movl	$134744071, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L40
	call	_ZdlPv@PLT
.L40:
	movq	(%rbx), %rax
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	cmovne	%rdx, %r13
	movq	40(%rax), %rdx
	movq	48(%rax), %rax
	testq	%rdx, %rdx
	cmove	-2944(%rbp), %rdx
	testq	%rax, %rax
	cmove	-2928(%rbp), %rax
	movq	%rdx, %rbx
	movl	$51, %edx
	movq	%rax, -2928(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	-2928(%rbp), %rcx
	call	_ZN2v88internal24ProxiesCodeStubAssembler13AllocateProxyENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEES7_@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -448(%rbp)
	je	.L44
.L321:
	movq	-2816(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-512(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$134744071, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L45
	call	_ZdlPv@PLT
.L45:
	movq	(%rbx), %rax
	movl	$54, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$139, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -256(%rbp)
	je	.L46
.L322:
	movq	-2824(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-320(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$134744071, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L47
	call	_ZdlPv@PLT
.L47:
	movq	(%rbx), %rax
	movl	$57, %edx
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rsi
	movq	(%rax), %r13
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$135, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L324:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movdqa	-2928(%rbp), %xmm2
	movaps	%xmm0, -2784(%rbp)
	movaps	%xmm2, -128(%rbp)
	movdqa	-2944(%rbp), %xmm2
	movq	$0, -2768(%rbp)
	movaps	%xmm2, -112(%rbp)
	movdqa	-2960(%rbp), %xmm2
	movaps	%xmm2, -96(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movq	%rbx, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	leaq	48(%rax), %rdx
	leaq	-2048(%rbp), %rdi
	movq	%rax, -2784(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -2768(%rbp)
	movq	%rdx, -2776(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L13
	call	_ZdlPv@PLT
.L13:
	movq	-2896(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L325:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movdqa	-2928(%rbp), %xmm7
	movdqa	-2944(%rbp), %xmm3
	movdqa	-2976(%rbp), %xmm2
	movaps	%xmm0, -2784(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	%r13, -80(%rbp)
	movq	$0, -2768(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	movq	%rbx, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	leaq	56(%rax), %rdx
	leaq	-1280(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rax, -2784(%rbp)
	movq	%rdx, -2768(%rbp)
	movq	%rdx, -2776(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L28
	call	_ZdlPv@PLT
.L28:
	movq	-2904(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L27
.L323:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22418:
	.size	_ZN2v88internal25ProxyConstructorAssembler28GenerateProxyConstructorImplEv, .-_ZN2v88internal25ProxyConstructorAssembler28GenerateProxyConstructorImplEv
	.section	.rodata._ZN2v88internal8Builtins25Generate_ProxyConstructorEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/proxy-constructor-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins25Generate_ProxyConstructorEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"ProxyConstructor"
	.section	.text._ZN2v88internal8Builtins25Generate_ProxyConstructorEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins25Generate_ProxyConstructorEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins25Generate_ProxyConstructorEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins25Generate_ProxyConstructorEPNS0_8compiler18CodeAssemblerStateE:
.LFB22414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$146, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$850, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L330
.L327:
	movq	%r13, %rdi
	call	_ZN2v88internal25ProxyConstructorAssembler28GenerateProxyConstructorImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L331
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L330:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L327
.L331:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22414:
	.size	_ZN2v88internal8Builtins25Generate_ProxyConstructorEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins25Generate_ProxyConstructorEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins25Generate_ProxyConstructorEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins25Generate_ProxyConstructorEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins25Generate_ProxyConstructorEPNS0_8compiler18CodeAssemblerStateE:
.LFB28955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28955:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins25Generate_ProxyConstructorEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins25Generate_ProxyConstructorEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins25Generate_ProxyConstructorEPNS0_8compiler18CodeAssemblerStateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
