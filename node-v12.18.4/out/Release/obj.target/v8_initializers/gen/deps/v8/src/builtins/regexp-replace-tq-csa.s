	.file	"regexp-replace-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0:
.LFB30650:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	%rsi, %r13
	pushq	%rbx
	movq	%r13, %rax
	sarq	$3, %rax
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%r13, %r13
	js	.L14
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%rdx, %r14
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.L9
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%rax, %rdi
.L9:
	leaq	(%rdi,%r13), %r15
	movq	%rdi, (%rbx)
	movq	%r15, 16(%rbx)
	cmpq	%r12, %r14
	je	.L10
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
.L10:
	movq	%r15, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE30650:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0:
.LFB30649:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	subq	%rsi, %rdx
	js	.L21
	movq	%rdi, %rbx
	je	.L17
	movq	%rdx, %rdi
	movq	%rdx, %r12
	movq	%rsi, %r13
	call	_Znwm@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	leaq	(%rax,%r12), %r14
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	%r14, 16(%rbx)
	call	memcpy@PLT
	movq	%r14, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	xorl	%r14d, %r14d
	movq	$0, (%rdi)
	movq	$0, 16(%rdi)
	movq	%r14, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE30649:
	.size	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev,"axG",@progbits,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev:
.LFB22413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$56, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L23
	call	_ZdlPv@PLT
.L23:
	movq	16(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	%r12, %r13
	je	.L24
	.p2align 4,,10
	.p2align 3
.L28:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L25
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L28
.L26:
	movq	8(%rbx), %r12
.L24:
	testq	%r12, %r12
	je	.L22
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L28
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L22:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22413:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev
	.set	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.section	.rodata._ZN2v88internal31RegExpPrototypeReplaceAssembler34GenerateRegExpPrototypeReplaceImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../../deps/v8/../../deps/v8/src/builtins/regexp-replace.tq"
	.section	.rodata._ZN2v88internal31RegExpPrototypeReplaceAssembler34GenerateRegExpPrototypeReplaceImplEv.str1.1,"aMS",@progbits,1
.LC3:
	.string	"RegExp.prototype.@@replace"
	.section	.text._ZN2v88internal31RegExpPrototypeReplaceAssembler34GenerateRegExpPrototypeReplaceImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal31RegExpPrototypeReplaceAssembler34GenerateRegExpPrototypeReplaceImplEv
	.type	_ZN2v88internal31RegExpPrototypeReplaceAssembler34GenerateRegExpPrototypeReplaceImplEv, @function
_ZN2v88internal31RegExpPrototypeReplaceAssembler34GenerateRegExpPrototypeReplaceImplEv:
.LFB22613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$2168, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -2008(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r13, %rcx
	movq	%r12, %rsi
	leaq	-1984(%rbp), %rdi
	movq	%rax, %rdx
	leaq	-1680(%rbp), %r13
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-1968(%rbp), %rcx
	movl	$2, %esi
	movq	%r12, %rdi
	movq	-1984(%rbp), %r14
	movq	-1976(%rbp), %rax
	movq	%r12, -1856(%rbp)
	leaq	-2008(%rbp), %r12
	movq	%rcx, -2112(%rbp)
	movq	%rcx, -1840(%rbp)
	movq	%r14, -1824(%rbp)
	movq	%rax, -2128(%rbp)
	movq	$1, -1848(%rbp)
	movq	%rax, -1832(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rax, -2096(%rbp)
	leaq	-1856(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -2136(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -1672(%rbp)
	movq	$0, -1664(%rbp)
	movq	%rax, %rbx
	movq	-2008(%rbp), %rax
	movq	$0, -1656(%rbp)
	movq	%rax, -1680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1672(%rbp)
	leaq	-1624(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1656(%rbp)
	movq	%rdx, -1664(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1640(%rbp)
	movq	%rax, -2024(%rbp)
	movq	$0, -1648(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2008(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1480(%rbp)
	movq	$0, -1472(%rbp)
	movq	%rax, -1488(%rbp)
	movq	$0, -1464(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1480(%rbp)
	leaq	-1432(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1464(%rbp)
	movq	%rdx, -1472(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1448(%rbp)
	movq	%rax, -2080(%rbp)
	movq	$0, -1456(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2008(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1288(%rbp)
	movq	$0, -1280(%rbp)
	movq	%rax, -1296(%rbp)
	movq	$0, -1272(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1288(%rbp)
	leaq	-1240(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1272(%rbp)
	movq	%rdx, -1280(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1256(%rbp)
	movq	%rax, -2048(%rbp)
	movq	$0, -1264(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2008(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1096(%rbp)
	movq	$0, -1088(%rbp)
	movq	%rax, -1104(%rbp)
	movq	$0, -1080(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1096(%rbp)
	leaq	-1048(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1080(%rbp)
	movq	%rdx, -1088(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1064(%rbp)
	movq	%rax, -2032(%rbp)
	movq	$0, -1072(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2008(%rbp), %rax
	movl	$192, %edi
	movq	$0, -904(%rbp)
	movq	$0, -896(%rbp)
	movq	%rax, -912(%rbp)
	movq	$0, -888(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -904(%rbp)
	leaq	-856(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -888(%rbp)
	movq	%rdx, -896(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -872(%rbp)
	movq	%rax, -2040(%rbp)
	movq	$0, -880(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2008(%rbp), %rax
	movl	$240, %edi
	movq	$0, -712(%rbp)
	movq	$0, -704(%rbp)
	movq	%rax, -720(%rbp)
	movq	$0, -696(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -712(%rbp)
	leaq	-664(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -696(%rbp)
	movq	%rdx, -704(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -680(%rbp)
	movq	%rax, -2072(%rbp)
	movq	$0, -688(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2008(%rbp), %rax
	movl	$264, %edi
	movq	$0, -520(%rbp)
	movq	$0, -512(%rbp)
	movq	%rax, -528(%rbp)
	movq	$0, -504(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -520(%rbp)
	leaq	-472(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -504(%rbp)
	movq	%rdx, -512(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -488(%rbp)
	movq	%rax, -2056(%rbp)
	movq	$0, -496(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2008(%rbp), %rax
	movl	$216, %edi
	movq	$0, -328(%rbp)
	movq	$0, -320(%rbp)
	movq	%rax, -336(%rbp)
	movq	$0, -312(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%r12, %rsi
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -328(%rbp)
	leaq	-280(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -312(%rbp)
	movq	%rdx, -320(%rbp)
	xorl	%edx, %edx
	movq	%rax, -2064(%rbp)
	movups	%xmm0, -296(%rbp)
	movq	$0, -304(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %xmm1
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movhps	-2128(%rbp), %xmm1
	movq	%rbx, -112(%rbp)
	leaq	-1808(%rbp), %r14
	movaps	%xmm1, -144(%rbp)
	movq	-2112(%rbp), %xmm1
	movaps	%xmm0, -1808(%rbp)
	movhps	-2096(%rbp), %xmm1
	movq	$0, -1792(%rbp)
	movaps	%xmm1, -128(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r13, %rdi
	movq	%r14, %rsi
	movdqa	-128(%rbp), %xmm5
	movdqa	-144(%rbp), %xmm4
	leaq	40(%rax), %rdx
	movq	%rax, -1808(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -1792(%rbp)
	movq	%rdx, -1800(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1808(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L35
	call	_ZdlPv@PLT
.L35:
	movq	-2024(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1616(%rbp)
	jne	.L215
	cmpq	$0, -1424(%rbp)
	jne	.L216
.L41:
	cmpq	$0, -1232(%rbp)
	jne	.L217
.L44:
	cmpq	$0, -1040(%rbp)
	jne	.L218
.L47:
	cmpq	$0, -848(%rbp)
	jne	.L219
.L49:
	cmpq	$0, -656(%rbp)
	jne	.L220
.L54:
	cmpq	$0, -464(%rbp)
	jne	.L221
.L57:
	cmpq	$0, -272(%rbp)
	jne	.L222
.L59:
	movq	-2064(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L61
	call	_ZdlPv@PLT
.L61:
	movq	-320(%rbp), %rbx
	movq	-328(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L62
	.p2align 4,,10
	.p2align 3
.L66:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L63
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L66
.L64:
	movq	-328(%rbp), %r13
.L62:
	testq	%r13, %r13
	je	.L67
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L67:
	movq	-2056(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-496(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L68
	call	_ZdlPv@PLT
.L68:
	movq	-512(%rbp), %rbx
	movq	-520(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L69
	.p2align 4,,10
	.p2align 3
.L73:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L70
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L73
.L71:
	movq	-520(%rbp), %r13
.L69:
	testq	%r13, %r13
	je	.L74
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L74:
	movq	-2072(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L75
	call	_ZdlPv@PLT
.L75:
	movq	-704(%rbp), %rbx
	movq	-712(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L76
	.p2align 4,,10
	.p2align 3
.L80:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L77
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L80
.L78:
	movq	-712(%rbp), %r13
.L76:
	testq	%r13, %r13
	je	.L81
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L81:
	movq	-2040(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L82
	call	_ZdlPv@PLT
.L82:
	movq	-896(%rbp), %rbx
	movq	-904(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L83
	.p2align 4,,10
	.p2align 3
.L87:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L84
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L87
.L85:
	movq	-904(%rbp), %r13
.L83:
	testq	%r13, %r13
	je	.L88
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L88:
	movq	-2032(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L89
	call	_ZdlPv@PLT
.L89:
	movq	-1088(%rbp), %rbx
	movq	-1096(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L90
	.p2align 4,,10
	.p2align 3
.L94:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L91
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L94
.L92:
	movq	-1096(%rbp), %r13
.L90:
	testq	%r13, %r13
	je	.L95
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L95:
	movq	-2048(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L96
	call	_ZdlPv@PLT
.L96:
	movq	-1280(%rbp), %rbx
	movq	-1288(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L97
	.p2align 4,,10
	.p2align 3
.L101:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L98
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L101
.L99:
	movq	-1288(%rbp), %r13
.L97:
	testq	%r13, %r13
	je	.L102
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L102:
	movq	-2080(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1456(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L103
	call	_ZdlPv@PLT
.L103:
	movq	-1472(%rbp), %rbx
	movq	-1480(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L104
	.p2align 4,,10
	.p2align 3
.L108:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L105
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L108
.L106:
	movq	-1480(%rbp), %r13
.L104:
	testq	%r13, %r13
	je	.L109
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L109:
	movq	-2024(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1648(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L110
	call	_ZdlPv@PLT
.L110:
	movq	-1664(%rbp), %rbx
	movq	-1672(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L111
	.p2align 4,,10
	.p2align 3
.L115:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L112
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L115
.L113:
	movq	-1672(%rbp), %r13
.L111:
	testq	%r13, %r13
	je	.L116
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L116:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L223
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L115
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L105:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L108
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L98:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L101
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L91:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L94
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L84:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L87
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L77:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L80
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L63:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L66
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L70:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L73
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L215:
	movq	-2024(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1792(%rbp)
	movaps	%xmm0, -1808(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -1808(%rbp)
	movq	%rdx, -1792(%rbp)
	movq	%rdx, -1800(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1808(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L37
	call	_ZdlPv@PLT
.L37:
	movq	(%rbx), %rax
	movl	$229, %edx
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	movq	16(%rax), %rbx
	movq	%rcx, -2096(%rbp)
	movq	24(%rax), %rcx
	movq	32(%rax), %rax
	movq	%rsi, -2160(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -2112(%rbp)
	movq	%rax, -2128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-2096(%rbp), %xmm3
	movq	%rbx, -1936(%rbp)
	pushq	-1936(%rbp)
	movhps	-2160(%rbp), %xmm3
	movaps	%xmm3, -1952(%rbp)
	pushq	-1944(%rbp)
	pushq	-1952(%rbp)
	movaps	%xmm3, -2096(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r14, %rdi
	movq	%rax, -2176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$230, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r13, %rsi
	movq	%r14, %rdi
	movdqa	-2096(%rbp), %xmm3
	movq	%rbx, -1904(%rbp)
	leaq	-1888(%rbp), %r13
	pushq	-1904(%rbp)
	movaps	%xmm3, -1920(%rbp)
	pushq	-1912(%rbp)
	pushq	-1920(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r14, %rdi
	movq	%rax, -2160(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$234, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2128(%rbp), %rdx
	movq	%r14, %rcx
	movq	%r15, %rdi
	movq	-2112(%rbp), %rsi
	call	_ZN2v88internal21Cast10JSReceiver_1404EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movq	-2128(%rbp), %xmm2
	movq	-2160(%rbp), %xmm6
	movdqa	-2096(%rbp), %xmm3
	movaps	%xmm0, -1888(%rbp)
	movdqa	%xmm2, %xmm7
	movq	%rax, -80(%rbp)
	punpcklqdq	%xmm2, %xmm6
	movq	%rbx, %xmm2
	movhps	-2176(%rbp), %xmm7
	movaps	%xmm3, -144(%rbp)
	movhps	-2112(%rbp), %xmm2
	movaps	%xmm6, -2160(%rbp)
	movaps	%xmm7, -2128(%rbp)
	movaps	%xmm2, -2112(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movq	$0, -1872(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm3
	movq	-80(%rbp), %rcx
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	leaq	72(%rax), %rdx
	leaq	-1296(%rbp), %rdi
	movups	%xmm3, (%rax)
	movdqa	-128(%rbp), %xmm3
	movq	%rcx, 64(%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rax, -1888(%rbp)
	movq	%rdx, -1872(%rbp)
	movq	%rdx, -1880(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1888(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L38
	call	_ZdlPv@PLT
.L38:
	movq	-2048(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1800(%rbp)
	jne	.L224
.L39:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1424(%rbp)
	je	.L41
.L216:
	movq	-2080(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1488(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -1792(%rbp)
	movaps	%xmm0, -1808(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movabsq	$578721382687638789, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -1808(%rbp)
	movq	%rdx, -1792(%rbp)
	movq	%rdx, -1800(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1808(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L42
	call	_ZdlPv@PLT
.L42:
	movq	(%rbx), %rax
	movl	$56, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movq	48(%rax), %rdx
	movdqu	(%rax), %xmm3
	movq	$0, -1792(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -1808(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm4
	leaq	56(%rax), %rdx
	leaq	-1104(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm3, 32(%rax)
	movq	%rax, -1808(%rbp)
	movq	%rdx, -1792(%rbp)
	movq	%rdx, -1800(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1808(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L43
	call	_ZdlPv@PLT
.L43:
	movq	-2032(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1232(%rbp)
	je	.L44
.L217:
	movq	-2048(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1296(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -1792(%rbp)
	movaps	%xmm0, -1808(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movabsq	$578721382687638789, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%rax, -1808(%rbp)
	movq	%rdx, -1792(%rbp)
	movq	%rdx, -1800(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1808(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L45
	call	_ZdlPv@PLT
.L45:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	32(%rax), %rdi
	movq	8(%rax), %r10
	movq	16(%rax), %r9
	movq	24(%rax), %r8
	movq	40(%rax), %rsi
	movq	48(%rax), %rcx
	movq	64(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdi, -112(%rbp)
	movl	$64, %edi
	movq	%r10, -136(%rbp)
	movq	%r9, -128(%rbp)
	movq	%r8, -120(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movaps	%xmm0, -1808(%rbp)
	movq	%rax, -144(%rbp)
	movq	$0, -1792(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	leaq	64(%rax), %rdx
	leaq	-912(%rbp), %rdi
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rax, -1808(%rbp)
	movq	%rdx, -1792(%rbp)
	movq	%rdx, -1800(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1808(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L46
	call	_ZdlPv@PLT
.L46:
	movq	-2040(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1040(%rbp)
	je	.L47
.L218:
	movq	-2032(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1104(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -1792(%rbp)
	movaps	%xmm0, -1808(%rbp)
	call	_Znwm@PLT
	movl	$2056, %edi
	movq	%r14, %rsi
	movw	%di, 4(%rax)
	leaq	7(%rax), %rdx
	movq	%r13, %rdi
	movl	$117769477, (%rax)
	movb	$8, 6(%rax)
	movq	%rax, -1808(%rbp)
	movq	%rdx, -1792(%rbp)
	movq	%rdx, -1800(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1808(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L48
	call	_ZdlPv@PLT
.L48:
	movq	(%rbx), %rax
	movl	$235, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	24(%rax), %r13
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	movl	$62, %edx
	movq	%r13, %rsi
	leaq	.LC3(%rip), %rcx
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -848(%rbp)
	je	.L49
.L219:
	movq	-2040(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-912(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -1792(%rbp)
	movaps	%xmm0, -1808(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movabsq	$506663788649710853, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -1808(%rbp)
	movq	%rdx, -1792(%rbp)
	movq	%rdx, -1800(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1808(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L50
	call	_ZdlPv@PLT
.L50:
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	movq	32(%rax), %rdx
	movq	48(%rax), %rdi
	movq	(%rax), %rcx
	movq	24(%rax), %rbx
	movq	%rsi, -2128(%rbp)
	movq	16(%rax), %rsi
	movq	40(%rax), %r13
	movq	%rdx, -2176(%rbp)
	movl	$238, %edx
	movq	56(%rax), %rax
	movq	%rdi, -2192(%rbp)
	movq	%r12, %rdi
	movq	%rsi, -2160(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -2112(%rbp)
	movq	%rax, -2096(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ToString_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -2208(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$242, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rcx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	-2096(%rbp), %rdx
	call	_ZN2v88internal24Cast14ATFastJSRegExp_134EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %xmm2
	movq	%rbx, %xmm3
	movq	-2176(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$88, %edi
	movq	%rax, -64(%rbp)
	movq	-2208(%rbp), %xmm4
	punpcklqdq	%xmm2, %xmm6
	movaps	%xmm0, -1888(%rbp)
	movq	-2192(%rbp), %xmm5
	movq	-2160(%rbp), %xmm7
	movq	-2112(%rbp), %xmm2
	movhps	-2096(%rbp), %xmm4
	movaps	%xmm6, -112(%rbp)
	leaq	-1888(%rbp), %r13
	movhps	-2096(%rbp), %xmm5
	punpcklqdq	%xmm3, %xmm7
	movaps	%xmm4, -80(%rbp)
	movhps	-2128(%rbp), %xmm2
	movaps	%xmm4, -2208(%rbp)
	movaps	%xmm5, -2192(%rbp)
	movaps	%xmm6, -2176(%rbp)
	movaps	%xmm7, -2160(%rbp)
	movaps	%xmm2, -2096(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm5, -96(%rbp)
	movq	$0, -1872(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-128(%rbp), %xmm7
	leaq	-528(%rbp), %rdi
	movdqa	-144(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm4
	leaq	88(%rax), %rdx
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm2
	movq	%rcx, 80(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm2, 64(%rax)
	movq	%rax, -1888(%rbp)
	movq	%rdx, -1872(%rbp)
	movq	%rdx, -1880(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1888(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L51
	call	_ZdlPv@PLT
.L51:
	movq	-2056(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1800(%rbp)
	jne	.L225
.L52:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -656(%rbp)
	je	.L54
.L220:
	movq	-2072(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-720(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -1792(%rbp)
	movaps	%xmm0, -1808(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movq	%r13, %rdi
	movabsq	$506663788649710853, %rcx
	movw	%si, 8(%rax)
	leaq	10(%rax), %rdx
	movq	%r14, %rsi
	movq	%rcx, (%rax)
	movq	%rax, -1808(%rbp)
	movq	%rdx, -1792(%rbp)
	movq	%rdx, -1800(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1808(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L55
	call	_ZdlPv@PLT
.L55:
	movq	(%rbx), %rax
	movl	$72, %edi
	movdqu	48(%rax), %xmm0
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	64(%rax), %rdx
	movdqu	(%rax), %xmm3
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -80(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -1808(%rbp)
	movq	$0, -1792(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm6
	leaq	72(%rax), %rdx
	leaq	-336(%rbp), %rdi
	movdqa	-96(%rbp), %xmm5
	movq	%rcx, 64(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rax, -1808(%rbp)
	movq	%rdx, -1792(%rbp)
	movq	%rdx, -1800(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1808(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L56
	call	_ZdlPv@PLT
.L56:
	movq	-2064(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -464(%rbp)
	je	.L57
.L221:
	movq	-2056(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-528(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$11, %edi
	movq	$0, -1792(%rbp)
	movaps	%xmm0, -1808(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movabsq	$506663788649710853, %rcx
	movq	%rcx, (%rax)
	movl	$1799, %ecx
	leaq	11(%rax), %rdx
	movw	%cx, 8(%rax)
	movb	$7, 10(%rax)
	movq	%rax, -1808(%rbp)
	movq	%rdx, -1792(%rbp)
	movq	%rdx, -1800(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1808(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L58
	call	_ZdlPv@PLT
.L58:
	movq	(%rbx), %rax
	movl	$243, %edx
	movq	%r12, %rdi
	leaq	-2000(%rbp), %r13
	leaq	.LC2(%rip), %rsi
	movq	24(%rax), %r9
	movq	64(%rax), %rcx
	movq	48(%rax), %rbx
	movq	80(%rax), %rax
	movq	%r9, -2128(%rbp)
	movq	%rcx, -2096(%rbp)
	movq	%rax, -2112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$869, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-1808(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%rbx, -128(%rbp)
	movl	$3, %ebx
	xorl	%esi, %esi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	pushq	%rbx
	movq	%rax, %r8
	movq	%r13, %rdi
	movq	%rcx, -1888(%rbp)
	leaq	-144(%rbp), %rcx
	movq	-2112(%rbp), %xmm0
	leaq	-1888(%rbp), %rdx
	movq	-2128(%rbp), %r9
	movq	-1792(%rbp), %rax
	pushq	%rcx
	movl	$1, %ecx
	movhps	-2096(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%rax, -1880(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-2136(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	cmpq	$0, -272(%rbp)
	popq	%rax
	popq	%rdx
	je	.L59
.L222:
	movq	-2064(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-336(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -1792(%rbp)
	movaps	%xmm0, -1808(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movabsq	$506663788649710853, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%rax, -1808(%rbp)
	movq	%rdx, -1792(%rbp)
	movq	%rdx, -1800(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1808(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L60
	call	_ZdlPv@PLT
.L60:
	movq	(%rbx), %rax
	movl	$246, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	movq	56(%rax), %rcx
	movq	24(%rax), %r13
	movq	48(%rax), %rbx
	movq	64(%rax), %rax
	movq	%rcx, -2096(%rbp)
	movq	%rax, -2112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdx
	movl	$295, %esi
	movq	%r14, %rdi
	movq	-2096(%rbp), %xmm0
	leaq	-144(%rbp), %rcx
	movl	$3, %r8d
	movq	%rbx, -128(%rbp)
	movhps	-2112(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-2136(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L224:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-2096(%rbp), %xmm2
	movdqa	-2112(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movdqa	-2128(%rbp), %xmm6
	movdqa	-2160(%rbp), %xmm7
	movl	$64, %edi
	movaps	%xmm0, -1888(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	$0, -1872(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm4
	movdqa	-128(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	leaq	64(%rax), %rdx
	leaq	-1488(%rbp), %rdi
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rax, -1888(%rbp)
	movq	%rdx, -1872(%rbp)
	movq	%rdx, -1880(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1888(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L40
	call	_ZdlPv@PLT
.L40:
	movq	-2080(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L225:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-2096(%rbp), %xmm6
	movdqa	-2160(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movdqa	-2176(%rbp), %xmm4
	movdqa	-2192(%rbp), %xmm5
	movl	$80, %edi
	movaps	%xmm0, -1888(%rbp)
	movdqa	-2208(%rbp), %xmm2
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm2, -80(%rbp)
	movq	$0, -1872(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm6
	leaq	-720(%rbp), %rdi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm4
	leaq	80(%rax), %rdx
	movq	%r13, %rsi
	movdqa	-80(%rbp), %xmm5
	movups	%xmm3, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rax, -1888(%rbp)
	movq	%rdx, -1872(%rbp)
	movq	%rdx, -1880(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1888(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L53
	call	_ZdlPv@PLT
.L53:
	movq	-2072(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L52
.L223:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22613:
	.size	_ZN2v88internal31RegExpPrototypeReplaceAssembler34GenerateRegExpPrototypeReplaceImplEv, .-_ZN2v88internal31RegExpPrototypeReplaceAssembler34GenerateRegExpPrototypeReplaceImplEv
	.section	.rodata._ZN2v88internal8Builtins31Generate_RegExpPrototypeReplaceEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/regexp-replace-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins31Generate_RegExpPrototypeReplaceEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"RegExpPrototypeReplace"
	.section	.text._ZN2v88internal8Builtins31Generate_RegExpPrototypeReplaceEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins31Generate_RegExpPrototypeReplaceEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins31Generate_RegExpPrototypeReplaceEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins31Generate_RegExpPrototypeReplaceEPNS0_8compiler18CodeAssemblerStateE:
.LFB22609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$2447, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$870, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L230
.L227:
	movq	%r13, %rdi
	call	_ZN2v88internal31RegExpPrototypeReplaceAssembler34GenerateRegExpPrototypeReplaceImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L231
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L227
.L231:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22609:
	.size	_ZN2v88internal8Builtins31Generate_RegExpPrototypeReplaceEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins31Generate_RegExpPrototypeReplaceEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESG_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESG_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESG_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESG_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESG_:
.LFB27158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$7, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$1543, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$117769991, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$5, 6(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L233
	movq	%rax, -112(%rbp)
	call	_ZdlPv@PLT
	movq	-112(%rbp), %rax
.L233:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L234
	movq	%rdx, (%r15)
.L234:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L235
	movq	%rdx, (%r14)
.L235:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L236
	movq	%rdx, 0(%r13)
.L236:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L237
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L237:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L238
	movq	%rdx, (%rbx)
.L238:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L239
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L239:
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L232
	movq	-104(%rbp), %rbx
	movq	%rax, (%rbx)
.L232:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L267
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L267:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27158:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESG_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESG_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EESN_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EESN_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EESN_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EESN_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EESN_:
.LFB27190:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$9, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$577874754439284487, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$6, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L269
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
.L269:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L270
	movq	%rdx, (%r15)
.L270:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L271
	movq	%rdx, (%r14)
.L271:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L272
	movq	%rdx, 0(%r13)
.L272:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L273
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L273:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L274
	movq	%rdx, (%rbx)
.L274:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L275
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L275:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L276
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L276:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L277
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L277:
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L268
	movq	-120(%rbp), %rbx
	movq	%rax, (%rbx)
.L268:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L311
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L311:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27190:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EESN_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EESN_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES8_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EEPNSC_IS7_EEPNSC_IS8_EESI_PNSC_IS9_EESO_SG_SI_SI_SI_SI_PNSC_ISA_EESI_SI_SI_SI_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES8_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EEPNSC_IS7_EEPNSC_IS8_EESI_PNSC_IS9_EESO_SG_SI_SI_SI_SI_PNSC_ISA_EESI_SI_SI_SI_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES8_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EEPNSC_IS7_EEPNSC_IS8_EESI_PNSC_IS9_EESO_SG_SI_SI_SI_SI_PNSC_ISA_EESI_SI_SI_SI_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES8_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EEPNSC_IS7_EEPNSC_IS8_EESI_PNSC_IS9_EESO_SG_SI_SI_SI_SI_PNSC_ISA_EESI_SI_SI_SI_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES8_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EEPNSC_IS7_EEPNSC_IS8_EESI_PNSC_IS9_EESO_SG_SI_SI_SI_SI_PNSC_ISA_EESI_SI_SI_SI_:
.LFB27193:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$19, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$168, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	88(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	96(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	104(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	112(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	120(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$1285, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movdqa	.LC6(%rip), %xmm0
	movw	%cx, 16(%rax)
	leaq	19(%rax), %rdx
	movb	$5, 18(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L313
	movq	%rax, -208(%rbp)
	call	_ZdlPv@PLT
	movq	-208(%rbp), %rax
.L313:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L314
	movq	%rdx, (%r15)
.L314:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L315
	movq	%rdx, (%r14)
.L315:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L316
	movq	%rdx, 0(%r13)
.L316:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L317
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L317:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L318
	movq	%rdx, (%rbx)
.L318:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L319
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L319:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L320
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L320:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L321
	movq	-112(%rbp), %rcx
	movq	%rdx, (%rcx)
.L321:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L322
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L322:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L323
	movq	-128(%rbp), %rcx
	movq	%rdx, (%rcx)
.L323:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L324
	movq	-136(%rbp), %rbx
	movq	%rdx, (%rbx)
.L324:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L325
	movq	-144(%rbp), %rsi
	movq	%rdx, (%rsi)
.L325:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L326
	movq	-152(%rbp), %rcx
	movq	%rdx, (%rcx)
.L326:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L327
	movq	-160(%rbp), %rbx
	movq	%rdx, (%rbx)
.L327:
	movq	112(%rax), %rdx
	testq	%rdx, %rdx
	je	.L328
	movq	-168(%rbp), %rsi
	movq	%rdx, (%rsi)
.L328:
	movq	120(%rax), %rdx
	testq	%rdx, %rdx
	je	.L329
	movq	-176(%rbp), %rdi
	movq	%rdx, (%rdi)
.L329:
	movq	128(%rax), %rdx
	testq	%rdx, %rdx
	je	.L330
	movq	-184(%rbp), %rcx
	movq	%rdx, (%rcx)
.L330:
	movq	136(%rax), %rdx
	testq	%rdx, %rdx
	je	.L331
	movq	-192(%rbp), %rbx
	movq	%rdx, (%rbx)
.L331:
	movq	144(%rax), %rax
	testq	%rax, %rax
	je	.L312
	movq	-200(%rbp), %rsi
	movq	%rax, (%rsi)
.L312:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L395
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L395:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27193:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES8_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EEPNSC_IS7_EEPNSC_IS8_EESI_PNSC_IS9_EESO_SG_SI_SI_SI_SI_PNSC_ISA_EESI_SI_SI_SI_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES8_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EEPNSC_IS7_EEPNSC_IS8_EESI_PNSC_IS9_EESO_SG_SI_SI_SI_SI_PNSC_ISA_EESI_SI_SI_SI_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EE:
.LFB27200:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$8, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$577874754439284487, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L397
	movq	%rax, -120(%rbp)
	call	_ZdlPv@PLT
	movq	-120(%rbp), %rax
.L397:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L398
	movq	%rdx, (%r15)
.L398:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L399
	movq	%rdx, (%r14)
.L399:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L400
	movq	%rdx, 0(%r13)
.L400:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L401
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L401:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L402
	movq	%rdx, (%rbx)
.L402:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L403
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L403:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L404
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L404:
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L396
	movq	-112(%rbp), %rbx
	movq	%rax, (%rbx)
.L396:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L435
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L435:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27200:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES6_S9_S6_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EEPNSC_IS7_EEPNSC_IS8_EESI_PNSC_IS9_EESK_SQ_SK_SG_SI_SI_SI_SI_PNSC_ISA_EESI_SI_SI_SI_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES6_S9_S6_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EEPNSC_IS7_EEPNSC_IS8_EESI_PNSC_IS9_EESK_SQ_SK_SG_SI_SI_SI_SI_PNSC_ISA_EESI_SI_SI_SI_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES6_S9_S6_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EEPNSC_IS7_EEPNSC_IS8_EESI_PNSC_IS9_EESK_SQ_SK_SG_SI_SI_SI_SI_PNSC_ISA_EESI_SI_SI_SI_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES6_S9_S6_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EEPNSC_IS7_EEPNSC_IS8_EESI_PNSC_IS9_EESK_SQ_SK_SG_SI_SI_SI_SI_PNSC_ISA_EESI_SI_SI_SI_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES6_S9_S6_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EEPNSC_IS7_EEPNSC_IS8_EESI_PNSC_IS9_EESK_SQ_SK_SG_SI_SI_SI_SI_PNSC_ISA_EESI_SI_SI_SI_:
.LFB27211:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$21, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$184, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	88(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	96(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	104(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	112(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	120(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	128(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	136(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movdqa	.LC7(%rip), %xmm0
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$84215047, 16(%rax)
	leaq	21(%rax), %rdx
	movb	$5, 20(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L437
	movq	%rax, -224(%rbp)
	call	_ZdlPv@PLT
	movq	-224(%rbp), %rax
.L437:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L438
	movq	%rdx, (%r15)
.L438:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L439
	movq	%rdx, (%r14)
.L439:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L440
	movq	%rdx, 0(%r13)
.L440:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L441
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L441:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L442
	movq	%rdx, (%rbx)
.L442:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L443
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L443:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L444
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L444:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L445
	movq	-112(%rbp), %rcx
	movq	%rdx, (%rcx)
.L445:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L446
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L446:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L447
	movq	-128(%rbp), %rcx
	movq	%rdx, (%rcx)
.L447:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L448
	movq	-136(%rbp), %rbx
	movq	%rdx, (%rbx)
.L448:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L449
	movq	-144(%rbp), %rsi
	movq	%rdx, (%rsi)
.L449:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L450
	movq	-152(%rbp), %rcx
	movq	%rdx, (%rcx)
.L450:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L451
	movq	-160(%rbp), %rbx
	movq	%rdx, (%rbx)
.L451:
	movq	112(%rax), %rdx
	testq	%rdx, %rdx
	je	.L452
	movq	-168(%rbp), %rsi
	movq	%rdx, (%rsi)
.L452:
	movq	120(%rax), %rdx
	testq	%rdx, %rdx
	je	.L453
	movq	-176(%rbp), %rdi
	movq	%rdx, (%rdi)
.L453:
	movq	128(%rax), %rdx
	testq	%rdx, %rdx
	je	.L454
	movq	-184(%rbp), %rcx
	movq	%rdx, (%rcx)
.L454:
	movq	136(%rax), %rdx
	testq	%rdx, %rdx
	je	.L455
	movq	-192(%rbp), %rbx
	movq	%rdx, (%rbx)
.L455:
	movq	144(%rax), %rdx
	testq	%rdx, %rdx
	je	.L456
	movq	-200(%rbp), %rsi
	movq	%rdx, (%rsi)
.L456:
	movq	152(%rax), %rdx
	testq	%rdx, %rdx
	je	.L457
	movq	-208(%rbp), %rdi
	movq	%rdx, (%rdi)
.L457:
	movq	160(%rax), %rax
	testq	%rax, %rax
	je	.L436
	movq	-216(%rbp), %rcx
	movq	%rax, (%rcx)
.L436:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L527
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L527:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27211:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES6_S9_S6_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EEPNSC_IS7_EEPNSC_IS8_EESI_PNSC_IS9_EESK_SQ_SK_SG_SI_SI_SI_SI_PNSC_ISA_EESI_SI_SI_SI_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES6_S9_S6_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EEPNSC_IS7_EEPNSC_IS8_EESI_PNSC_IS9_EESK_SQ_SK_SG_SI_SI_SI_SI_PNSC_ISA_EESI_SI_SI_SI_
	.section	.rodata._ZN2v88internal43RegExpReplaceCallableNoExplicitCaptures_322EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_7IntPtrTEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"../../deps/v8/../../deps/v8/src/builtins/torque-internal.tq"
	.section	.text._ZN2v88internal43RegExpReplaceCallableNoExplicitCaptures_322EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_7IntPtrTEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal43RegExpReplaceCallableNoExplicitCaptures_322EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_7IntPtrTEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE
	.type	_ZN2v88internal43RegExpReplaceCallableNoExplicitCaptures_322EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_7IntPtrTEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE, @function
_ZN2v88internal43RegExpReplaceCallableNoExplicitCaptures_322EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_7IntPtrTEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE:
.LFB22410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$3960, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, -7520(%rbp)
	movq	%rcx, %r15
	movq	%rdi, %rbx
	leaq	-7488(%rbp), %r13
	movq	%rsi, -7496(%rbp)
	leaq	-7096(%rbp), %r12
	movq	%r13, %r14
	movq	%rdx, -7536(%rbp)
	movq	%r8, -7504(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	%rdi, -7488(%rbp)
	movq	%rdi, -7152(%rbp)
	movl	$120, %edi
	movq	$0, -7144(%rbp)
	movq	$0, -7136(%rbp)
	movq	$0, -7128(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -7128(%rbp)
	movq	%rdx, -7136(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -7112(%rbp)
	movq	%rax, -7144(%rbp)
	movq	$0, -7120(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$168, %edi
	movq	$0, -6952(%rbp)
	movq	$0, -6944(%rbp)
	movq	%rax, -6960(%rbp)
	movq	$0, -6936(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -6952(%rbp)
	leaq	-6904(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6936(%rbp)
	movq	%rdx, -6944(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6920(%rbp)
	movq	%rax, -7840(%rbp)
	movq	$0, -6928(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$168, %edi
	movq	$0, -6760(%rbp)
	movq	$0, -6752(%rbp)
	movq	%rax, -6768(%rbp)
	movq	$0, -6744(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -6760(%rbp)
	leaq	-6712(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6744(%rbp)
	movq	%rdx, -6752(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6728(%rbp)
	movq	%rax, -7568(%rbp)
	movq	$0, -6736(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$408, %edi
	movq	$0, -6568(%rbp)
	movq	$0, -6560(%rbp)
	movq	%rax, -6576(%rbp)
	movq	$0, -6552(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	408(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 400(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movq	%rax, -6568(%rbp)
	leaq	-6520(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6552(%rbp)
	movq	%rdx, -6560(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6536(%rbp)
	movq	%rax, -7552(%rbp)
	movq	$0, -6544(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$408, %edi
	movq	$0, -6376(%rbp)
	movq	$0, -6368(%rbp)
	movq	%rax, -6384(%rbp)
	movq	$0, -6360(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	408(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 400(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movq	%rax, -6376(%rbp)
	leaq	-6328(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6360(%rbp)
	movq	%rdx, -6368(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6344(%rbp)
	movq	%rax, -7728(%rbp)
	movq	$0, -6352(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$456, %edi
	movq	$0, -6184(%rbp)
	movq	$0, -6176(%rbp)
	movq	%rax, -6192(%rbp)
	movq	$0, -6168(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movups	%xmm0, (%rax)
	leaq	456(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movq	%rax, -6184(%rbp)
	movq	$0, 448(%rax)
	leaq	-6136(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6168(%rbp)
	movq	%rdx, -6176(%rbp)
	xorl	%edx, %edx
	movq	$0, -6160(%rbp)
	movups	%xmm0, -6152(%rbp)
	movq	%rax, -7576(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$288, %edi
	movq	$0, -5992(%rbp)
	movq	$0, -5984(%rbp)
	movq	%rax, -6000(%rbp)
	movq	$0, -5976(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -5992(%rbp)
	leaq	-5944(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5976(%rbp)
	movq	%rdx, -5984(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5960(%rbp)
	movq	%rax, -7656(%rbp)
	movq	$0, -5968(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$336, %edi
	movq	$0, -5800(%rbp)
	movq	$0, -5792(%rbp)
	movq	%rax, -5808(%rbp)
	movq	$0, -5784(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -5800(%rbp)
	leaq	-5752(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5784(%rbp)
	movq	%rdx, -5792(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5768(%rbp)
	movq	%rax, -7680(%rbp)
	movq	$0, -5776(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$336, %edi
	movq	$0, -5608(%rbp)
	movq	$0, -5600(%rbp)
	movq	%rax, -5616(%rbp)
	movq	$0, -5592(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -5608(%rbp)
	leaq	-5560(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5592(%rbp)
	movq	%rdx, -5600(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5576(%rbp)
	movq	%rax, -7696(%rbp)
	movq	$0, -5584(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$216, %edi
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	%rax, -5424(%rbp)
	movq	$0, -5400(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -5416(%rbp)
	leaq	-5368(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5400(%rbp)
	movq	%rdx, -5408(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5384(%rbp)
	movq	%rax, -7752(%rbp)
	movq	$0, -5392(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$240, %edi
	movq	$0, -5224(%rbp)
	movq	$0, -5216(%rbp)
	movq	%rax, -5232(%rbp)
	movq	$0, -5208(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -5224(%rbp)
	leaq	-5176(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5208(%rbp)
	movq	%rdx, -5216(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5192(%rbp)
	movq	%rax, -7712(%rbp)
	movq	$0, -5200(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$216, %edi
	movq	$0, -5032(%rbp)
	movq	$0, -5024(%rbp)
	movq	%rax, -5040(%rbp)
	movq	$0, -5016(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -5032(%rbp)
	leaq	-4984(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5016(%rbp)
	movq	%rdx, -5024(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5000(%rbp)
	movq	%rax, -7744(%rbp)
	movq	$0, -5008(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$216, %edi
	movq	$0, -4840(%rbp)
	movq	$0, -4832(%rbp)
	movq	%rax, -4848(%rbp)
	movq	$0, -4824(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -4840(%rbp)
	leaq	-4792(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4824(%rbp)
	movq	%rdx, -4832(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4808(%rbp)
	movq	%rax, -7600(%rbp)
	movq	$0, -4816(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$456, %edi
	movq	$0, -4648(%rbp)
	movq	$0, -4640(%rbp)
	movq	%rax, -4656(%rbp)
	movq	$0, -4632(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movups	%xmm0, (%rax)
	leaq	456(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movq	%rax, -4648(%rbp)
	movq	$0, 448(%rax)
	leaq	-4600(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4632(%rbp)
	movq	%rdx, -4640(%rbp)
	xorl	%edx, %edx
	movq	$0, -4624(%rbp)
	movups	%xmm0, -4616(%rbp)
	movq	%rax, -7616(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$456, %edi
	movq	$0, -4456(%rbp)
	movq	$0, -4448(%rbp)
	movq	%rax, -4464(%rbp)
	movq	$0, -4440(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movups	%xmm0, (%rax)
	leaq	456(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movq	%rax, -4456(%rbp)
	movq	$0, 448(%rax)
	leaq	-4408(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4440(%rbp)
	movq	%rdx, -4448(%rbp)
	xorl	%edx, %edx
	movq	$0, -4432(%rbp)
	movups	%xmm0, -4424(%rbp)
	movq	%rax, -7904(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$504, %edi
	movq	$0, -4264(%rbp)
	movq	$0, -4256(%rbp)
	movq	%rax, -4272(%rbp)
	movq	$0, -4248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	504(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -4264(%rbp)
	movq	%rdx, -4248(%rbp)
	movups	%xmm0, 480(%rax)
	movq	$0, 496(%rax)
	leaq	-4216(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4256(%rbp)
	xorl	%edx, %edx
	movq	%rax, -7784(%rbp)
	movq	$0, -4240(%rbp)
	movups	%xmm0, -4232(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$336, %edi
	movq	$0, -4072(%rbp)
	movq	$0, -4064(%rbp)
	movq	%rax, -4080(%rbp)
	movq	$0, -4056(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -4072(%rbp)
	leaq	-4024(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4056(%rbp)
	movq	%rdx, -4064(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4040(%rbp)
	movq	%rax, -7800(%rbp)
	movq	$0, -4048(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$384, %edi
	movq	$0, -3880(%rbp)
	movq	$0, -3872(%rbp)
	movq	%rax, -3888(%rbp)
	movq	$0, -3864(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	384(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movq	%rax, -3880(%rbp)
	leaq	-3832(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3864(%rbp)
	movq	%rdx, -3872(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3848(%rbp)
	movq	%rax, -7808(%rbp)
	movq	$0, -3856(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$384, %edi
	movq	$0, -3688(%rbp)
	movq	$0, -3680(%rbp)
	movq	%rax, -3696(%rbp)
	movq	$0, -3672(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	384(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movq	%rax, -3688(%rbp)
	leaq	-3640(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3672(%rbp)
	movq	%rdx, -3680(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3656(%rbp)
	movq	%rax, -7816(%rbp)
	movq	$0, -3664(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$216, %edi
	movq	$0, -3496(%rbp)
	movq	$0, -3488(%rbp)
	movq	%rax, -3504(%rbp)
	movq	$0, -3480(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -3496(%rbp)
	leaq	-3448(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3480(%rbp)
	movq	%rdx, -3488(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3464(%rbp)
	movq	%rax, -7544(%rbp)
	movq	$0, -3472(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$192, %edi
	movq	$0, -3304(%rbp)
	movq	$0, -3296(%rbp)
	movq	%rax, -3312(%rbp)
	movq	$0, -3288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -3304(%rbp)
	leaq	-3256(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3288(%rbp)
	movq	%rdx, -3296(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3272(%rbp)
	movq	%rax, -7760(%rbp)
	movq	$0, -3280(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$216, %edi
	movq	$0, -3112(%rbp)
	movq	$0, -3104(%rbp)
	movq	%rax, -3120(%rbp)
	movq	$0, -3096(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -3112(%rbp)
	leaq	-3064(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3096(%rbp)
	movq	%rdx, -3104(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3080(%rbp)
	movq	%rax, -7952(%rbp)
	movq	$0, -3088(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$240, %edi
	movq	$0, -2920(%rbp)
	movq	$0, -2912(%rbp)
	movq	%rax, -2928(%rbp)
	movq	$0, -2904(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -2920(%rbp)
	leaq	-2872(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2904(%rbp)
	movq	%rdx, -2912(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2888(%rbp)
	movq	%rax, -7824(%rbp)
	movq	$0, -2896(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$504, %edi
	movq	$0, -2728(%rbp)
	movq	$0, -2720(%rbp)
	movq	%rax, -2736(%rbp)
	movq	$0, -2712(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	504(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -2728(%rbp)
	movq	%rdx, -2712(%rbp)
	movups	%xmm0, 480(%rax)
	movq	$0, 496(%rax)
	leaq	-2680(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2720(%rbp)
	xorl	%edx, %edx
	movq	%rax, -7896(%rbp)
	movq	$0, -2704(%rbp)
	movups	%xmm0, -2696(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$504, %edi
	movq	$0, -2536(%rbp)
	movq	$0, -2528(%rbp)
	movq	%rax, -2544(%rbp)
	movq	$0, -2520(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	504(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -2536(%rbp)
	movq	%rdx, -2520(%rbp)
	movups	%xmm0, 480(%rax)
	movq	$0, 496(%rax)
	leaq	-2488(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2528(%rbp)
	xorl	%edx, %edx
	movq	%rax, -7912(%rbp)
	movq	$0, -2512(%rbp)
	movups	%xmm0, -2504(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$552, %edi
	movq	$0, -2344(%rbp)
	movq	$0, -2336(%rbp)
	movq	%rax, -2352(%rbp)
	movq	$0, -2328(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	552(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -2344(%rbp)
	movq	%rdx, -2328(%rbp)
	movups	%xmm0, 480(%rax)
	movq	$0, 544(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	leaq	-2296(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2336(%rbp)
	xorl	%edx, %edx
	movq	%rax, -7792(%rbp)
	movq	$0, -2320(%rbp)
	movups	%xmm0, -2312(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$384, %edi
	movq	$0, -2152(%rbp)
	movq	$0, -2144(%rbp)
	movq	%rax, -2160(%rbp)
	movq	$0, -2136(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	384(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movq	%rax, -2152(%rbp)
	leaq	-2104(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2136(%rbp)
	movq	%rdx, -2144(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2120(%rbp)
	movq	%rax, -7832(%rbp)
	movq	$0, -2128(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$432, %edi
	movq	$0, -1960(%rbp)
	movq	$0, -1952(%rbp)
	movq	%rax, -1968(%rbp)
	movq	$0, -1944(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movups	%xmm0, (%rax)
	leaq	432(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movq	%rax, -1960(%rbp)
	leaq	-1912(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1944(%rbp)
	movq	%rdx, -1952(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1928(%rbp)
	movq	$0, -1936(%rbp)
	movq	%rax, -7880(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$432, %edi
	movq	$0, -1768(%rbp)
	movq	$0, -1760(%rbp)
	movq	%rax, -1776(%rbp)
	movq	$0, -1752(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movups	%xmm0, (%rax)
	leaq	432(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movq	%rax, -1768(%rbp)
	leaq	-1720(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1752(%rbp)
	movq	%rdx, -1760(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1736(%rbp)
	movq	$0, -1744(%rbp)
	movq	%rax, -7584(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1576(%rbp)
	movq	$0, -1568(%rbp)
	movq	%rax, -1584(%rbp)
	movq	$0, -1560(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1576(%rbp)
	leaq	-1528(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1560(%rbp)
	movq	%rdx, -1568(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1544(%rbp)
	movq	%rax, -7888(%rbp)
	movq	$0, -1552(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1384(%rbp)
	movq	$0, -1376(%rbp)
	movq	%rax, -1392(%rbp)
	movq	$0, -1368(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1384(%rbp)
	leaq	-1336(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1368(%rbp)
	movq	%rdx, -1376(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1352(%rbp)
	movq	%rax, -7920(%rbp)
	movq	$0, -1360(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1192(%rbp)
	movq	$0, -1184(%rbp)
	movq	%rax, -1200(%rbp)
	movq	$0, -1176(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1192(%rbp)
	leaq	-1144(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1176(%rbp)
	movq	%rdx, -1184(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1160(%rbp)
	movq	%rax, -7864(%rbp)
	movq	$0, -1168(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1000(%rbp)
	movq	$0, -992(%rbp)
	movq	%rax, -1008(%rbp)
	movq	$0, -984(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1000(%rbp)
	leaq	-952(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -984(%rbp)
	movq	%rdx, -992(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -968(%rbp)
	movq	%rax, -7928(%rbp)
	movq	$0, -976(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$168, %edi
	movq	$0, -808(%rbp)
	movq	$0, -800(%rbp)
	movq	%rax, -816(%rbp)
	movq	$0, -792(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -808(%rbp)
	leaq	-760(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -792(%rbp)
	movq	%rdx, -800(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -776(%rbp)
	movq	%rax, -7872(%rbp)
	movq	$0, -784(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$120, %edi
	movq	$0, -616(%rbp)
	movq	$0, -608(%rbp)
	movq	%rax, -624(%rbp)
	movq	$0, -600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -616(%rbp)
	leaq	-568(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -600(%rbp)
	movq	%rdx, -608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -584(%rbp)
	movq	%rax, -7936(%rbp)
	movq	$0, -592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7488(%rbp), %rax
	movl	$120, %edi
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	%rax, -432(%rbp)
	movq	$0, -408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -424(%rbp)
	leaq	-376(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -408(%rbp)
	movq	%rdx, -416(%rbp)
	xorl	%edx, %edx
	movq	%rax, -7856(%rbp)
	movups	%xmm0, -392(%rbp)
	movq	$0, -400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7536(%rbp), %r11
	pxor	%xmm0, %xmm0
	movq	-7504(%rbp), %r10
	movq	-7520(%rbp), %r9
	movq	-7496(%rbp), %rax
	movl	$40, %edi
	movq	%r15, -224(%rbp)
	movq	%r11, -232(%rbp)
	leaq	-7280(%rbp), %r15
	movq	%r10, -216(%rbp)
	movq	%r9, -208(%rbp)
	movaps	%xmm0, -7280(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -7264(%rbp)
	call	_Znwm@PLT
	movq	-208(%rbp), %rcx
	movdqa	-240(%rbp), %xmm7
	movq	%r15, %rsi
	movdqa	-224(%rbp), %xmm6
	leaq	40(%rax), %rdx
	movq	%rax, -7280(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm6, 16(%rax)
	leaq	-7152(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	movq	%rax, -7848(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L529
	call	_ZdlPv@PLT
.L529:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-6960(%rbp), %rax
	cmpq	$0, -7088(%rbp)
	movq	%rax, -7496(%rbp)
	jne	.L880
.L530:
	leaq	-6768(%rbp), %rax
	cmpq	$0, -6896(%rbp)
	movq	%rax, -7592(%rbp)
	leaq	-816(%rbp), %rax
	movq	%rax, -7664(%rbp)
	jne	.L881
.L533:
	leaq	-6576(%rbp), %rax
	cmpq	$0, -6704(%rbp)
	movq	%rax, -7608(%rbp)
	leaq	-6384(%rbp), %rax
	movq	%rax, -7632(%rbp)
	jne	.L882
.L536:
	leaq	-6192(%rbp), %rax
	cmpq	$0, -6512(%rbp)
	movq	%rax, -7648(%rbp)
	jne	.L883
.L539:
	leaq	-6000(%rbp), %rax
	cmpq	$0, -6320(%rbp)
	movq	%rax, -7568(%rbp)
	jne	.L884
.L542:
	leaq	-5808(%rbp), %rax
	cmpq	$0, -6128(%rbp)
	movq	%rax, -7552(%rbp)
	jne	.L885
.L545:
	cmpq	$0, -5936(%rbp)
	jne	.L886
.L548:
	leaq	-5616(%rbp), %rax
	cmpq	$0, -5744(%rbp)
	movq	%rax, -7576(%rbp)
	jne	.L887
.L550:
	leaq	-5232(%rbp), %rax
	cmpq	$0, -5552(%rbp)
	movq	%rax, -7656(%rbp)
	leaq	-5424(%rbp), %rax
	movq	%rax, -7536(%rbp)
	jne	.L888
.L553:
	leaq	-3312(%rbp), %rax
	cmpq	$0, -5360(%rbp)
	movq	%rax, -7696(%rbp)
	jne	.L889
.L558:
	leaq	-5040(%rbp), %rax
	cmpq	$0, -5168(%rbp)
	movq	%rax, -7680(%rbp)
	jne	.L890
.L561:
	leaq	-3504(%rbp), %rax
	cmpq	$0, -4976(%rbp)
	movq	%rax, -7520(%rbp)
	jne	.L891
.L565:
	leaq	-4464(%rbp), %rax
	cmpq	$0, -4784(%rbp)
	movq	%rax, -7752(%rbp)
	jne	.L892
.L567:
	leaq	-4272(%rbp), %rax
	cmpq	$0, -4592(%rbp)
	movq	%rax, -7776(%rbp)
	jne	.L893
.L570:
	leaq	-4080(%rbp), %rax
	cmpq	$0, -4400(%rbp)
	movq	%rax, -7712(%rbp)
	jne	.L894
.L572:
	leaq	-3888(%rbp), %rax
	cmpq	$0, -4208(%rbp)
	movq	%rax, -7728(%rbp)
	jne	.L895
.L574:
	cmpq	$0, -4016(%rbp)
	jne	.L896
.L577:
	leaq	-3696(%rbp), %rax
	cmpq	$0, -3824(%rbp)
	movq	%rax, -7744(%rbp)
	jne	.L897
	cmpq	$0, -3632(%rbp)
	jne	.L898
.L582:
	leaq	-1200(%rbp), %rax
	cmpq	$0, -3440(%rbp)
	movq	%rax, -7504(%rbp)
	jne	.L899
.L585:
	leaq	-2928(%rbp), %rax
	cmpq	$0, -3248(%rbp)
	movq	%rax, -7800(%rbp)
	leaq	-3120(%rbp), %rax
	movq	%rax, -7544(%rbp)
	jne	.L900
.L587:
	leaq	-1584(%rbp), %rax
	cmpq	$0, -3056(%rbp)
	movq	%rax, -7784(%rbp)
	jne	.L901
.L591:
	leaq	-2736(%rbp), %rax
	cmpq	$0, -2864(%rbp)
	movq	%rax, -7808(%rbp)
	leaq	-2544(%rbp), %rax
	movq	%rax, -7816(%rbp)
	jne	.L902
.L594:
	leaq	-2352(%rbp), %rax
	cmpq	$0, -2672(%rbp)
	movq	%rax, -7824(%rbp)
	jne	.L903
.L601:
	leaq	-2160(%rbp), %rax
	cmpq	$0, -2480(%rbp)
	movq	%rax, -7760(%rbp)
	jne	.L904
.L603:
	cmpq	$0, -2288(%rbp)
	leaq	-1968(%rbp), %r13
	jne	.L905
.L605:
	cmpq	$0, -2096(%rbp)
	jne	.L906
.L608:
	cmpq	$0, -1904(%rbp)
	jne	.L907
.L610:
	leaq	-1392(%rbp), %rax
	cmpq	$0, -1712(%rbp)
	movq	%rax, -7792(%rbp)
	jne	.L908
	cmpq	$0, -1520(%rbp)
	jne	.L909
.L616:
	cmpq	$0, -1328(%rbp)
	jne	.L910
.L617:
	leaq	-1008(%rbp), %rax
	cmpq	$0, -1136(%rbp)
	movq	%rax, -7832(%rbp)
	jne	.L911
.L619:
	cmpq	$0, -944(%rbp)
	jne	.L912
.L621:
	leaq	-624(%rbp), %rax
	cmpq	$0, -752(%rbp)
	movq	%rax, -7840(%rbp)
	jne	.L913
.L623:
	cmpq	$0, -560(%rbp)
	leaq	-432(%rbp), %r12
	jne	.L914
.L625:
	movq	-7856(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -7264(%rbp)
	movaps	%xmm0, -7280(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	movl	$117769991, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -7280(%rbp)
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L628
	call	_ZdlPv@PLT
.L628:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7840(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7664(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7832(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7504(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7792(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7784(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7584(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L629
	call	_ZdlPv@PLT
.L629:
	movq	-1760(%rbp), %rbx
	movq	-1768(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L630
	.p2align 4,,10
	.p2align 3
.L634:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L631
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L634
.L632:
	movq	-1768(%rbp), %r12
.L630:
	testq	%r12, %r12
	je	.L635
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L635:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7760(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7824(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7816(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7808(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7800(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7544(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7696(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7520(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7744(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7728(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7712(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7776(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7752(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7616(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-4624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L636
	call	_ZdlPv@PLT
.L636:
	movq	-4640(%rbp), %rbx
	movq	-4648(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L637
	.p2align 4,,10
	.p2align 3
.L641:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L638
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L641
.L639:
	movq	-4648(%rbp), %r12
.L637:
	testq	%r12, %r12
	je	.L642
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L642:
	movq	-7600(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-4816(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L643
	call	_ZdlPv@PLT
.L643:
	movq	-4832(%rbp), %rbx
	movq	-4840(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L644
	.p2align 4,,10
	.p2align 3
.L648:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L645
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L648
.L646:
	movq	-4840(%rbp), %r12
.L644:
	testq	%r12, %r12
	je	.L649
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L649:
	movq	-7680(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7656(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7536(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7576(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7552(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7568(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7648(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7632(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7608(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7592(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7496(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7848(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L915
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L645:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L648
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L631:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L634
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L638:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L641
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L880:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -7264(%rbp)
	movaps	%xmm0, -7280(%rbp)
	call	_Znwm@PLT
	movq	-7848(%rbp), %rdi
	movq	%r15, %rsi
	movl	$117769991, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -7280(%rbp)
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7280(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L531
	call	_ZdlPv@PLT
.L531:
	movq	(%r12), %rax
	movl	$31, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r13
	movq	16(%rax), %r12
	movq	%rcx, -7496(%rbp)
	movq	24(%rax), %rcx
	movq	32(%rax), %rax
	movq	%rcx, -7520(%rbp)
	movq	%rax, -7504(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$32, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -7536(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %xmm0
	movl	$56, %edi
	movq	$0, -7264(%rbp)
	movhps	-7496(%rbp), %xmm0
	movq	%rax, -192(%rbp)
	movaps	%xmm0, -240(%rbp)
	movq	%r12, %xmm0
	movhps	-7520(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-7504(%rbp), %xmm0
	movhps	-7536(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7280(%rbp)
	call	_Znwm@PLT
	movq	-192(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm6
	movdqa	-224(%rbp), %xmm7
	movdqa	-208(%rbp), %xmm5
	leaq	56(%rax), %rdx
	movq	%rax, -7280(%rbp)
	movq	%rcx, 48(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm5, 32(%rax)
	leaq	-6960(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	movq	%rax, -7496(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L532
	call	_ZdlPv@PLT
.L532:
	movq	-7840(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L881:
	movq	-7840(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7312(%rbp), %rax
	movq	-7496(%rbp), %rdi
	leaq	-7336(%rbp), %r9
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7344(%rbp), %r8
	pushq	%rax
	leaq	-7352(%rbp), %rcx
	leaq	-7360(%rbp), %rdx
	leaq	-7368(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESG_
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7352(%rbp), %rdx
	movq	-7312(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-7336(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	-7328(%rbp), %r10
	movq	-7360(%rbp), %rcx
	movq	-7352(%rbp), %rsi
	movaps	%xmm0, -7280(%rbp)
	movq	-7344(%rbp), %rdx
	movq	-7368(%rbp), %rax
	movq	%rdi, -7648(%rbp)
	movq	-7312(%rbp), %r13
	movq	%rdi, -208(%rbp)
	movl	$56, %edi
	movq	%r10, -7632(%rbp)
	movq	%r10, -200(%rbp)
	movq	%rcx, -7536(%rbp)
	movq	%rsi, -7504(%rbp)
	movq	%rdx, -7608(%rbp)
	movq	%rcx, -232(%rbp)
	movq	%rsi, -224(%rbp)
	movq	%rdx, -216(%rbp)
	movq	%rax, -7520(%rbp)
	movq	%rax, -240(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -7264(%rbp)
	call	_Znwm@PLT
	movq	-192(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm6
	movdqa	-224(%rbp), %xmm7
	movdqa	-208(%rbp), %xmm4
	leaq	56(%rax), %rdx
	movq	%rax, -7280(%rbp)
	movq	%rcx, 48(%rax)
	movq	-7592(%rbp), %rdi
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	popq	%r10
	popq	%r11
	testq	%rdi, %rdi
	je	.L534
	call	_ZdlPv@PLT
.L534:
	movq	-7520(%rbp), %xmm0
	movl	$56, %edi
	movq	%r13, -192(%rbp)
	movq	$0, -7264(%rbp)
	movhps	-7536(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-7504(%rbp), %xmm0
	movhps	-7608(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-7648(%rbp), %xmm0
	movhps	-7632(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7280(%rbp)
	call	_Znwm@PLT
	movq	-192(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm5
	movdqa	-224(%rbp), %xmm6
	movdqa	-208(%rbp), %xmm7
	leaq	56(%rax), %rdx
	movq	%rax, -7280(%rbp)
	movq	%rcx, 48(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	leaq	-816(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	movq	%rax, -7664(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L535
	call	_ZdlPv@PLT
.L535:
	movq	-7872(%rbp), %rcx
	movq	-7568(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L882:
	movq	-7568(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7312(%rbp), %rax
	movq	-7592(%rbp), %rdi
	leaq	-7336(%rbp), %r9
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7344(%rbp), %r8
	pushq	%rax
	leaq	-7352(%rbp), %rcx
	leaq	-7360(%rbp), %rdx
	leaq	-7368(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESG_
	movl	$33, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$8, %esi
	movq	%r14, %rdi
	movq	%rax, -7504(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	movq	-7360(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$46, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -7520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$37, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7312(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	-7520(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	leaq	-240(%rbp), %r13
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-104(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	-7312(%rbp), %xmm1
	movq	-7504(%rbp), %rax
	movq	%r15, %rdi
	movaps	%xmm0, -7280(%rbp)
	movq	-7336(%rbp), %xmm7
	movq	-7352(%rbp), %xmm4
	movq	$0, -7264(%rbp)
	movdqa	%xmm1, %xmm5
	movq	%rax, -176(%rbp)
	movq	-7368(%rbp), %xmm6
	movhps	-7360(%rbp), %xmm5
	movq	-7520(%rbp), %rax
	movq	-7360(%rbp), %xmm3
	movhps	-7328(%rbp), %xmm7
	movaps	%xmm5, -7968(%rbp)
	movhps	-7344(%rbp), %xmm4
	movhps	-7360(%rbp), %xmm6
	movaps	%xmm5, -192(%rbp)
	movdqa	%xmm1, %xmm5
	movhps	-7504(%rbp), %xmm3
	punpcklqdq	%xmm5, %xmm5
	movq	%xmm1, -120(%rbp)
	movq	%xmm1, -112(%rbp)
	movq	%xmm1, -7992(%rbp)
	movq	%rax, -168(%rbp)
	movq	%rax, -128(%rbp)
	movaps	%xmm7, -7648(%rbp)
	movaps	%xmm4, -7632(%rbp)
	movaps	%xmm6, -7568(%rbp)
	movaps	%xmm6, -240(%rbp)
	movaps	%xmm4, -224(%rbp)
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm3, -7776(%rbp)
	movaps	%xmm5, -7536(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movq	%rdx, -7984(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7608(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	popq	%r8
	movq	-7984(%rbp), %rdx
	popq	%r9
	movq	-7992(%rbp), %xmm1
	testq	%rdi, %rdi
	je	.L537
	movq	%rdx, -7992(%rbp)
	movq	%xmm1, -7984(%rbp)
	call	_ZdlPv@PLT
	movq	-7992(%rbp), %rdx
	movq	-7984(%rbp), %xmm1
.L537:
	movdqa	-7968(%rbp), %xmm7
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	-7504(%rbp), %xmm0
	movdqa	-7568(%rbp), %xmm4
	movdqa	-7632(%rbp), %xmm5
	movq	%xmm1, -112(%rbp)
	movaps	%xmm7, -192(%rbp)
	movq	-7520(%rbp), %xmm7
	movdqa	-7648(%rbp), %xmm6
	movaps	%xmm4, -240(%rbp)
	movdqa	-7536(%rbp), %xmm4
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm5, -224(%rbp)
	movdqa	-7776(%rbp), %xmm5
	movaps	%xmm0, -176(%rbp)
	movdqa	%xmm7, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm6, -208(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm0, -7280(%rbp)
	movq	$0, -7264(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-6384(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -7632(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L538
	call	_ZdlPv@PLT
.L538:
	movq	-7728(%rbp), %rcx
	movq	-7552(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L883:
	movq	-7552(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$17, %edi
	movq	$0, -7264(%rbp)
	movaps	%xmm0, -7280(%rbp)
	call	_Znwm@PLT
	movdqa	.LC9(%rip), %xmm0
	movq	-7608(%rbp), %rdi
	movq	%r15, %rsi
	movb	$5, 16(%rax)
	leaq	17(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -7280(%rbp)
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7280(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L540
	call	_ZdlPv@PLT
.L540:
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	movq	24(%rax), %rdx
	movq	40(%rax), %rdi
	movq	(%rax), %rcx
	movq	56(%rax), %r11
	movq	72(%rax), %r10
	movq	%rsi, -7536(%rbp)
	movq	88(%rax), %r9
	movq	16(%rax), %rsi
	movq	%rdx, -7552(%rbp)
	movq	32(%rax), %rdx
	movq	%rdi, -7776(%rbp)
	movq	48(%rax), %rdi
	movq	120(%rax), %r8
	movq	%rcx, -7504(%rbp)
	movq	%r11, -7984(%rbp)
	movq	104(%rax), %rcx
	movq	64(%rax), %r11
	movq	%r10, -8016(%rbp)
	movq	%r9, -8000(%rbp)
	movq	80(%rax), %r10
	movq	112(%rax), %r9
	movq	%rsi, -7568(%rbp)
	leaq	.LC8(%rip), %rsi
	movq	%rdx, -7648(%rbp)
	movl	$39, %edx
	movq	96(%rax), %r13
	movq	%rdi, -7968(%rbp)
	movq	%r14, %rdi
	movq	%r11, -7992(%rbp)
	movq	%r10, -8032(%rbp)
	movq	%r9, -8048(%rbp)
	movq	%r8, -8056(%rbp)
	movq	%rcx, -7520(%rbp)
	movq	128(%rax), %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, -8064(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8064(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrMulENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, -8064(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8064(%rbp), %rdx
	movq	-7520(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, -8064(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$38, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	-8064(%rbp), %rcx
	call	_ZN2v88internal45UnsafeNewReference20UT5ATSmi10HeapObject_1411EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	leaq	-88(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r12, -112(%rbp)
	movq	-7504(%rbp), %xmm0
	movq	-7280(%rbp), %rax
	leaq	-240(%rbp), %rsi
	movq	$0, -7264(%rbp)
	movhps	-7536(%rbp), %xmm0
	movq	%rax, -104(%rbp)
	movq	-7272(%rbp), %rax
	movaps	%xmm0, -240(%rbp)
	movq	-7568(%rbp), %xmm0
	movq	%rax, -96(%rbp)
	movhps	-7552(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-7648(%rbp), %xmm0
	movhps	-7776(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-7968(%rbp), %xmm0
	movhps	-7984(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-7992(%rbp), %xmm0
	movhps	-8016(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-8032(%rbp), %xmm0
	movhps	-8000(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%r13, %xmm0
	movhps	-7520(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-8048(%rbp), %xmm0
	movhps	-8056(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7280(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-6192(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -7648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L541
	call	_ZdlPv@PLT
.L541:
	movq	-7576(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L884:
	movq	-7728(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$17, %edi
	movq	$0, -7264(%rbp)
	movaps	%xmm0, -7280(%rbp)
	call	_Znwm@PLT
	movdqa	.LC9(%rip), %xmm0
	movq	-7632(%rbp), %rdi
	movq	%r15, %rsi
	movb	$5, 16(%rax)
	leaq	17(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -7280(%rbp)
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7280(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L543
	call	_ZdlPv@PLT
.L543:
	movq	(%r12), %rax
	movq	24(%rax), %rdx
	movq	8(%rax), %rsi
	movq	72(%rax), %rdi
	movq	(%rax), %rcx
	movq	32(%rax), %r13
	movq	48(%rax), %r12
	movq	%rdx, -7568(%rbp)
	movq	40(%rax), %rdx
	movq	%rsi, -7504(%rbp)
	movq	%rdi, -7968(%rbp)
	movq	16(%rax), %rsi
	movq	%rdx, -7552(%rbp)
	movq	56(%rax), %rdx
	movq	80(%rax), %rdi
	movq	%rsi, -7536(%rbp)
	leaq	.LC8(%rip), %rsi
	movq	%rdx, -7728(%rbp)
	movq	64(%rax), %rdx
	movq	88(%rax), %rax
	movq	%rdi, -7984(%rbp)
	movq	%r14, %rdi
	movq	%rdx, -7776(%rbp)
	movl	$41, %edx
	movq	%rcx, -7520(%rbp)
	movq	%rax, -7992(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-240(%rbp), %rsi
	leaq	-144(%rbp), %rdx
	movq	%r15, %rdi
	movq	-7520(%rbp), %xmm0
	movq	$0, -7264(%rbp)
	movhps	-7504(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-7536(%rbp), %xmm0
	movhps	-7568(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	%r13, %xmm0
	movhps	-7552(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	%r12, %xmm0
	movhps	-7728(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-7776(%rbp), %xmm0
	movhps	-7968(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-7984(%rbp), %xmm0
	movhps	-7992(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7280(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-6000(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -7568(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L544
	call	_ZdlPv@PLT
.L544:
	movq	-7656(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L888:
	movq	-7696(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$14, %edi
	movq	$0, -7264(%rbp)
	movaps	%xmm0, -7280(%rbp)
	call	_Znwm@PLT
	movq	-7576(%rbp), %rdi
	movq	%r15, %rsi
	movabsq	$505817160401356551, %rcx
	movq	%rcx, (%rax)
	movl	$1287, %ecx
	leaq	14(%rax), %rdx
	movl	$84215045, 8(%rax)
	movw	%cx, 12(%rax)
	movq	%rax, -7280(%rbp)
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7280(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L554
	call	_ZdlPv@PLT
.L554:
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	movq	24(%rax), %rdx
	movq	40(%rax), %rdi
	movq	(%rax), %rcx
	movq	96(%rax), %r12
	movq	104(%rax), %r13
	movq	%rsi, -7504(%rbp)
	movq	%rdx, -7656(%rbp)
	movq	16(%rax), %rsi
	movq	32(%rax), %rdx
	movq	%rdi, -7696(%rbp)
	movq	48(%rax), %rdi
	movq	%rsi, -7536(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -7680(%rbp)
	movl	$33, %edx
	movq	%rdi, -7728(%rbp)
	movq	%r14, %rdi
	movq	%rcx, -7520(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6ObjectEvE5valueE(%rip), %esi
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%rax, %rcx
	leaq	-7312(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	movq	%rax, -7776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$35, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal13Cast5ATSmi_83EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %xmm4
	pxor	%xmm0, %xmm0
	movq	-7728(%rbp), %xmm7
	movq	-7520(%rbp), %xmm3
	movq	%r13, -176(%rbp)
	leaq	-240(%rbp), %r13
	leaq	-160(%rbp), %rdx
	punpcklqdq	%xmm4, %xmm7
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	movq	-7680(%rbp), %xmm4
	movhps	-7504(%rbp), %xmm3
	movq	-7536(%rbp), %xmm6
	movaps	%xmm7, -7728(%rbp)
	movaps	%xmm3, -7520(%rbp)
	movhps	-7696(%rbp), %xmm4
	movhps	-7656(%rbp), %xmm6
	movaps	%xmm3, -240(%rbp)
	movaps	%xmm4, -7696(%rbp)
	movaps	%xmm6, -7680(%rbp)
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm0, -7312(%rbp)
	movq	$0, -7296(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-5232(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -7656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L555
	call	_ZdlPv@PLT
.L555:
	movq	-7712(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-5424(%rbp), %rax
	cmpq	$0, -7272(%rbp)
	movq	%rax, -7536(%rbp)
	jne	.L916
.L556:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L887:
	movq	-7680(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$14, %edi
	movq	$0, -7264(%rbp)
	movaps	%xmm0, -7280(%rbp)
	call	_Znwm@PLT
	movl	$1287, %esi
	movabsq	$505817160401356551, %rcx
	movq	-7552(%rbp), %rdi
	movw	%si, 12(%rax)
	leaq	14(%rax), %rdx
	movq	%r15, %rsi
	movq	%rcx, (%rax)
	movl	$84215045, 8(%rax)
	movq	%rax, -7280(%rbp)
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7280(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L551
	call	_ZdlPv@PLT
.L551:
	movq	(%r12), %rax
	leaq	-240(%rbp), %rsi
	leaq	-128(%rbp), %rdx
	movq	%r15, %rdi
	movdqu	80(%rax), %xmm0
	movdqu	(%rax), %xmm5
	movdqu	16(%rax), %xmm4
	movdqu	32(%rax), %xmm3
	movdqu	48(%rax), %xmm2
	movdqu	64(%rax), %xmm1
	movdqu	96(%rax), %xmm6
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -240(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm4, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -7280(%rbp)
	movq	$0, -7264(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-5616(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -7576(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L552
	call	_ZdlPv@PLT
.L552:
	movq	-7696(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L886:
	movq	-7656(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-240(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-228(%rbp), %rdx
	movabsq	$505817160401356551, %rax
	movaps	%xmm0, -7280(%rbp)
	movq	%rax, -240(%rbp)
	movl	$84215045, -232(%rbp)
	movq	$0, -7264(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7568(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L549
	call	_ZdlPv@PLT
.L549:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L885:
	movq	-7576(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-240(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC9(%rip), %xmm0
	movl	$1797, %edi
	movq	%r13, %rsi
	movw	%di, -224(%rbp)
	movq	%r15, %rdi
	leaq	-221(%rbp), %rdx
	movaps	%xmm0, -240(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7280(%rbp)
	movb	$5, -222(%rbp)
	movq	$0, -7264(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7648(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7280(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L546
	call	_ZdlPv@PLT
.L546:
	movq	(%r12), %rax
	movq	40(%rax), %rdi
	movq	8(%rax), %rsi
	movq	24(%rax), %rdx
	movq	72(%rax), %r11
	movq	88(%rax), %r10
	movq	(%rax), %rcx
	movq	%rdi, -7728(%rbp)
	movq	56(%rax), %rdi
	movq	48(%rax), %r12
	movq	%rsi, -7504(%rbp)
	movq	%rdx, -7552(%rbp)
	movq	16(%rax), %rsi
	movq	32(%rax), %rdx
	movq	%rdi, -7776(%rbp)
	movq	64(%rax), %rdi
	movq	%r11, -7984(%rbp)
	movq	%r10, -8016(%rbp)
	movq	80(%rax), %r11
	movq	136(%rax), %r10
	movq	144(%rax), %rax
	movq	%rsi, -7536(%rbp)
	leaq	.LC8(%rip), %rsi
	movq	%rdx, -7576(%rbp)
	movl	$46, %edx
	movq	%rdi, -7968(%rbp)
	movq	%r14, %rdi
	movq	%rcx, -7520(%rbp)
	movq	%r11, -7992(%rbp)
	movq	%r10, -8032(%rbp)
	movq	%rax, -8000(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-128(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	-7520(%rbp), %xmm0
	movq	$0, -7264(%rbp)
	movhps	-7504(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-7536(%rbp), %xmm0
	movhps	-7552(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-7576(%rbp), %xmm0
	movhps	-7728(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	%r12, %xmm0
	movhps	-7776(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-7968(%rbp), %xmm0
	movhps	-7984(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-7992(%rbp), %xmm0
	movhps	-8016(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-8032(%rbp), %xmm0
	movhps	-8000(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7280(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-5808(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -7552(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L547
	call	_ZdlPv@PLT
.L547:
	movq	-7680(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L889:
	movq	-7752(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-240(%rbp), %rsi
	movabsq	$577874754439284487, %rax
	leaq	-231(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rax, -240(%rbp)
	movaps	%xmm0, -7280(%rbp)
	movb	$8, -232(%rbp)
	movq	$0, -7264(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7536(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7280(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L559
	call	_ZdlPv@PLT
.L559:
	movq	(%r12), %rax
	movl	$64, %edi
	movdqu	48(%rax), %xmm0
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movdqu	(%rax), %xmm7
	movq	$0, -7264(%rbp)
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm7, -240(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -7280(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm4
	movq	%r15, %rsi
	movdqa	-224(%rbp), %xmm5
	movdqa	-208(%rbp), %xmm6
	movdqa	-192(%rbp), %xmm7
	leaq	64(%rax), %rdx
	movq	%rax, -7280(%rbp)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	leaq	-3312(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	movq	%rax, -7696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L560
	call	_ZdlPv@PLT
.L560:
	movq	-7760(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L890:
	movq	-7712(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-240(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-230(%rbp), %rdx
	movabsq	$577874754439284487, %rax
	movaps	%xmm0, -7280(%rbp)
	movq	%rax, -240(%rbp)
	movl	$1544, %eax
	movw	%ax, -232(%rbp)
	movq	$0, -7264(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7656(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7280(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L562
	call	_ZdlPv@PLT
.L562:
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	movq	24(%rax), %rdx
	movq	40(%rax), %rdi
	movq	(%rax), %rcx
	movq	56(%rax), %r11
	movq	72(%rax), %r13
	movq	%rsi, -7504(%rbp)
	movq	%rdx, -7712(%rbp)
	movq	16(%rax), %rsi
	movq	32(%rax), %rdx
	movq	%rdi, -7752(%rbp)
	movq	48(%rax), %rdi
	movq	%rsi, -7680(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -7728(%rbp)
	movl	$39, %edx
	movq	%rdi, -7776(%rbp)
	movq	%r14, %rdi
	movq	%rcx, -7520(%rbp)
	movq	%r11, -7968(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17IntPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movq	-7776(%rbp), %xmm5
	movq	-7728(%rbp), %xmm7
	movq	-7520(%rbp), %xmm4
	movl	$72, %edi
	movq	-7680(%rbp), %xmm2
	movaps	%xmm0, -7280(%rbp)
	movhps	-7968(%rbp), %xmm5
	movhps	-7752(%rbp), %xmm7
	movq	%r13, -176(%rbp)
	movhps	-7712(%rbp), %xmm2
	movhps	-7504(%rbp), %xmm4
	movaps	%xmm5, -7776(%rbp)
	movaps	%xmm7, -7728(%rbp)
	movaps	%xmm2, -7712(%rbp)
	movaps	%xmm4, -7520(%rbp)
	movaps	%xmm4, -240(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm5, -192(%rbp)
	movq	$0, -7264(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm4
	movdqa	-224(%rbp), %xmm5
	movdqa	-208(%rbp), %xmm6
	leaq	72(%rax), %rdx
	movq	%rax, -7280(%rbp)
	movdqa	-192(%rbp), %xmm7
	movq	%rcx, 64(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	leaq	-5040(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	movq	%rax, -7680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L563
	call	_ZdlPv@PLT
.L563:
	movdqa	-7520(%rbp), %xmm4
	movdqa	-7712(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movdqa	-7728(%rbp), %xmm6
	movdqa	-7776(%rbp), %xmm7
	movaps	%xmm0, -7280(%rbp)
	movaps	%xmm4, -240(%rbp)
	movaps	%xmm5, -224(%rbp)
	movaps	%xmm6, -208(%rbp)
	movaps	%xmm7, -192(%rbp)
	movq	%r13, -176(%rbp)
	movq	$0, -7264(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm3
	movdqa	-224(%rbp), %xmm4
	movdqa	-208(%rbp), %xmm5
	leaq	72(%rax), %rdx
	leaq	-4848(%rbp), %rdi
	movdqa	-192(%rbp), %xmm6
	movq	%rcx, 64(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movq	%rax, -7280(%rbp)
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L564
	call	_ZdlPv@PLT
.L564:
	movq	-7600(%rbp), %rcx
	movq	-7744(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L891:
	movq	-7744(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7312(%rbp), %rax
	movq	-7680(%rbp), %rdi
	leaq	-7368(%rbp), %rcx
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7352(%rbp), %r9
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7360(%rbp), %r8
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7376(%rbp), %rdx
	pushq	%rax
	leaq	-7384(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EESN_
	addq	$32, %rsp
	movl	$43, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7312(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$44, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$11, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordSarENS1_11SloppyTNodeINS0_5WordTEEENS3_INS0_9IntegralTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$2047, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, -7504(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7504(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler7WordAndENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$45, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal26Convert5ATSmi8ATintptr_184EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movl	$39, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7352(%rbp), %rax
	movq	-7368(%rbp), %xmm0
	movl	$72, %edi
	movq	-7384(%rbp), %xmm1
	movq	%r12, -200(%rbp)
	movq	%rax, -208(%rbp)
	movq	-7336(%rbp), %rax
	movhps	-7360(%rbp), %xmm0
	movhps	-7376(%rbp), %xmm1
	movaps	%xmm0, -224(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -192(%rbp)
	movq	-7328(%rbp), %rax
	movaps	%xmm1, -240(%rbp)
	movq	%rax, -184(%rbp)
	movq	-7312(%rbp), %rax
	movaps	%xmm0, -7280(%rbp)
	movq	%rax, -176(%rbp)
	movq	$0, -7264(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm4
	movdqa	-224(%rbp), %xmm5
	movdqa	-208(%rbp), %xmm6
	leaq	72(%rax), %rdx
	movq	%rax, -7280(%rbp)
	movdqa	-192(%rbp), %xmm7
	movq	-7520(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L566
	call	_ZdlPv@PLT
.L566:
	movq	-7544(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L892:
	movq	-7600(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7384(%rbp)
	leaq	-4848(%rbp), %r12
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7312(%rbp), %rax
	movq	%r12, %rdi
	leaq	-7352(%rbp), %r9
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7360(%rbp), %r8
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7368(%rbp), %rcx
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7376(%rbp), %rdx
	pushq	%rax
	leaq	-7384(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EESN_
	addq	$32, %rsp
	movl	$49, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$8, %esi
	movq	%r14, %rdi
	movq	%rax, -7712(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	movq	-7376(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%rax, -7504(%rbp)
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7336(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, -7728(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$46, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$37, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rdi
	movq	-7728(%rbp), %xmm0
	movq	%xmm0, -7984(%rbp)
	movq	%xmm0, %rsi
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	-7504(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	leaq	-240(%rbp), %r13
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-88(%rbp), %rdx
	pxor	%xmm2, %xmm2
	movq	%r13, %rsi
	movq	-7328(%rbp), %rax
	movq	-7312(%rbp), %rcx
	movq	%r15, %rdi
	movq	-7376(%rbp), %xmm1
	movq	-7352(%rbp), %xmm6
	movq	-7368(%rbp), %xmm3
	movq	%rax, -7752(%rbp)
	movq	%rax, -184(%rbp)
	movq	-7712(%rbp), %rax
	movhps	-7344(%rbp), %xmm6
	movq	-7384(%rbp), %xmm5
	movhps	-7360(%rbp), %xmm3
	movq	-7984(%rbp), %xmm0
	movq	%xmm1, -168(%rbp)
	movq	%rax, -160(%rbp)
	movq	-7504(%rbp), %rax
	punpcklqdq	%xmm1, %xmm5
	movq	%xmm1, -128(%rbp)
	movq	%rax, -152(%rbp)
	movq	-7712(%rbp), %rax
	movq	%xmm0, -192(%rbp)
	movq	%rax, -120(%rbp)
	movq	-7504(%rbp), %rax
	movq	%xmm0, -144(%rbp)
	movq	%xmm0, -136(%rbp)
	movq	%xmm1, -8016(%rbp)
	movq	%rcx, -7776(%rbp)
	movq	%rcx, -176(%rbp)
	movaps	%xmm6, -7728(%rbp)
	movaps	%xmm3, -7968(%rbp)
	movaps	%xmm5, -7744(%rbp)
	movaps	%xmm5, -240(%rbp)
	movaps	%xmm3, -224(%rbp)
	movaps	%xmm6, -208(%rbp)
	movq	%rax, -112(%rbp)
	movq	%rdx, -7984(%rbp)
	movq	%xmm0, -104(%rbp)
	movq	%xmm0, -96(%rbp)
	movq	%xmm0, -7992(%rbp)
	movaps	%xmm2, -7280(%rbp)
	movq	$0, -7264(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-4656(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	movq	-7984(%rbp), %rdx
	movq	-7992(%rbp), %xmm0
	movq	-8016(%rbp), %xmm1
	testq	%rdi, %rdi
	je	.L568
	movq	%rdx, -8016(%rbp)
	movq	%xmm1, -7992(%rbp)
	movq	%xmm0, -7984(%rbp)
	call	_ZdlPv@PLT
	movq	-8016(%rbp), %rdx
	movq	-7992(%rbp), %xmm1
	movq	-7984(%rbp), %xmm0
.L568:
	movdqa	%xmm0, %xmm2
	movq	%xmm0, -96(%rbp)
	movq	%r13, %rsi
	movq	%r15, %rdi
	movdqa	-7744(%rbp), %xmm7
	movhps	-7752(%rbp), %xmm2
	movq	-7504(%rbp), %xmm6
	movq	$0, -7264(%rbp)
	movaps	%xmm2, -192(%rbp)
	movdqa	-7968(%rbp), %xmm3
	movq	-7776(%rbp), %xmm2
	movaps	%xmm7, -240(%rbp)
	movdqa	-7728(%rbp), %xmm4
	movq	-7712(%rbp), %xmm7
	punpcklqdq	%xmm1, %xmm2
	movaps	%xmm3, -224(%rbp)
	movaps	%xmm2, -176(%rbp)
	movdqa	%xmm7, %xmm2
	punpcklqdq	%xmm7, %xmm1
	punpcklqdq	%xmm6, %xmm2
	movaps	%xmm1, -128(%rbp)
	movdqa	%xmm6, %xmm1
	movaps	%xmm2, -160(%rbp)
	movdqa	%xmm0, %xmm2
	punpcklqdq	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	punpcklqdq	%xmm2, %xmm2
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -7280(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-4464(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -7752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L569
	call	_ZdlPv@PLT
.L569:
	movq	-7904(%rbp), %rcx
	movq	-7616(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L896:
	movq	-7800(%rbp), %rsi
	movq	%r14, %rdi
	movl	$1285, %r13d
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-240(%rbp), %rsi
	movabsq	$577874754439284487, %rax
	leaq	-226(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rax, -240(%rbp)
	movaps	%xmm0, -7280(%rbp)
	movl	$84215558, -232(%rbp)
	movw	%r13w, -228(%rbp)
	movq	$0, -7264(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7712(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L578
	call	_ZdlPv@PLT
.L578:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L895:
	movq	-7784(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-240(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC6(%rip), %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-219(%rbp), %rdx
	movl	$117769477, -224(%rbp)
	movaps	%xmm0, -240(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7280(%rbp)
	movb	$5, -220(%rbp)
	movq	$0, -7264(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7776(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7280(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L575
	call	_ZdlPv@PLT
.L575:
	movq	(%r12), %rax
	movq	40(%rax), %rdi
	movq	8(%rax), %rsi
	movq	24(%rax), %rdx
	movq	72(%rax), %r11
	movq	88(%rax), %r10
	movq	%rdi, -7968(%rbp)
	movq	56(%rax), %rdi
	movq	104(%rax), %r9
	movq	%rsi, -7728(%rbp)
	movq	16(%rax), %rsi
	movq	(%rax), %rcx
	movq	%rdx, -7784(%rbp)
	movq	32(%rax), %rdx
	movq	%rdi, -7984(%rbp)
	movq	64(%rax), %rdi
	movq	48(%rax), %r12
	movq	%r11, -8016(%rbp)
	movq	%r10, -8000(%rbp)
	movq	80(%rax), %r11
	movq	96(%rax), %r10
	movq	%r9, -8056(%rbp)
	movq	152(%rax), %r9
	movq	160(%rax), %rax
	movq	%rsi, -7744(%rbp)
	leaq	.LC8(%rip), %rsi
	movq	%rdx, -7904(%rbp)
	movl	$46, %edx
	movq	%rdi, -7992(%rbp)
	movq	%r14, %rdi
	movq	%rcx, -7504(%rbp)
	movq	%r11, -8032(%rbp)
	movq	%r10, -8048(%rbp)
	movq	%r9, -8064(%rbp)
	movq	%rax, -8072(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-112(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	-7504(%rbp), %xmm0
	movq	$0, -7264(%rbp)
	movhps	-7728(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-7744(%rbp), %xmm0
	movhps	-7784(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-7904(%rbp), %xmm0
	movhps	-7968(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	%r12, %xmm0
	movhps	-7984(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-7992(%rbp), %xmm0
	movhps	-8016(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-8032(%rbp), %xmm0
	movhps	-8000(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-8048(%rbp), %xmm0
	movhps	-8056(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-8064(%rbp), %xmm0
	movhps	-8072(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7280(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3888(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -7728(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L576
	call	_ZdlPv@PLT
.L576:
	movq	-7808(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L894:
	movq	-7904(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7464(%rbp)
	movq	$0, -7456(%rbp)
	movq	$0, -7448(%rbp)
	movq	$0, -7440(%rbp)
	movq	$0, -7432(%rbp)
	movq	$0, -7424(%rbp)
	movq	$0, -7416(%rbp)
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7312(%rbp), %rax
	movq	-7752(%rbp), %rdi
	leaq	-7448(%rbp), %rcx
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7432(%rbp), %r9
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7440(%rbp), %r8
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7456(%rbp), %rdx
	pushq	%rax
	leaq	-7352(%rbp), %rax
	leaq	-7464(%rbp), %rsi
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	leaq	-7376(%rbp), %rax
	pushq	%rax
	leaq	-7384(%rbp), %rax
	pushq	%rax
	leaq	-7392(%rbp), %rax
	pushq	%rax
	leaq	-7400(%rbp), %rax
	pushq	%rax
	leaq	-7408(%rbp), %rax
	pushq	%rax
	leaq	-7416(%rbp), %rax
	pushq	%rax
	leaq	-7424(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES8_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EEPNSC_IS7_EEPNSC_IS8_EESI_PNSC_IS9_EESO_SG_SI_SI_SI_SI_PNSC_ISA_EESI_SI_SI_SI_
	addq	$112, %rsp
	movl	$41, %edx
	movq	%r14, %rdi
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-240(%rbp), %rsi
	leaq	-128(%rbp), %rdx
	movq	%r15, %rdi
	movq	-7368(%rbp), %xmm0
	movq	-7384(%rbp), %xmm1
	movq	$0, -7264(%rbp)
	movq	-7400(%rbp), %xmm2
	movq	-7416(%rbp), %xmm3
	movq	-7432(%rbp), %xmm4
	movhps	-7360(%rbp), %xmm0
	movq	-7448(%rbp), %xmm5
	movhps	-7376(%rbp), %xmm1
	movq	-7464(%rbp), %xmm6
	movhps	-7392(%rbp), %xmm2
	movhps	-7408(%rbp), %xmm3
	movaps	%xmm0, -144(%rbp)
	movhps	-7424(%rbp), %xmm4
	movhps	-7440(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -192(%rbp)
	movhps	-7456(%rbp), %xmm6
	movaps	%xmm5, -224(%rbp)
	movaps	%xmm6, -240(%rbp)
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -7280(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7712(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L573
	call	_ZdlPv@PLT
.L573:
	movq	-7800(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L893:
	movq	-7616(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7464(%rbp)
	leaq	-4656(%rbp), %r12
	movq	$0, -7456(%rbp)
	movq	$0, -7448(%rbp)
	movq	$0, -7440(%rbp)
	movq	$0, -7432(%rbp)
	movq	$0, -7424(%rbp)
	movq	$0, -7416(%rbp)
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7312(%rbp), %rax
	movq	%r12, %rdi
	leaq	-7432(%rbp), %r9
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7440(%rbp), %r8
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7448(%rbp), %rcx
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7456(%rbp), %rdx
	pushq	%rax
	leaq	-7352(%rbp), %rax
	leaq	-7464(%rbp), %rsi
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	leaq	-7376(%rbp), %rax
	pushq	%rax
	leaq	-7384(%rbp), %rax
	pushq	%rax
	leaq	-7392(%rbp), %rax
	pushq	%rax
	leaq	-7400(%rbp), %rax
	pushq	%rax
	leaq	-7408(%rbp), %rax
	pushq	%rax
	leaq	-7416(%rbp), %rax
	pushq	%rax
	leaq	-7424(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES8_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EEPNSC_IS7_EEPNSC_IS8_EESI_PNSC_IS9_EESO_SG_SI_SI_SI_SI_PNSC_ISA_EESI_SI_SI_SI_
	addq	$112, %rsp
	movl	$39, %edx
	movq	%r14, %rdi
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7312(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrMulENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7344(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$38, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rcx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	-7352(%rbp), %rdx
	call	_ZN2v88internal45UnsafeNewReference20UT5ATSmi10HeapObject_1411EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	pxor	%xmm0, %xmm0
	leaq	-72(%rbp), %rdx
	movq	%r15, %rdi
	movq	-7464(%rbp), %rax
	leaq	-240(%rbp), %rsi
	movq	%rax, -240(%rbp)
	movq	-7456(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	-7448(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-7440(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-7432(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-7424(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-7416(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-7408(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-7400(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-7392(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-7384(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-7376(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-7368(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-7360(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-7352(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-7344(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-7336(%rbp), %rax
	movq	$0, -7264(%rbp)
	movq	%rax, -112(%rbp)
	movq	-7328(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-7312(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-7280(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-7272(%rbp), %rax
	movaps	%xmm0, -7280(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7776(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L571
	call	_ZdlPv@PLT
.L571:
	movq	-7784(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L897:
	movq	-7808(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	$0, -7264(%rbp)
	movaps	%xmm0, -7280(%rbp)
	call	_Znwm@PLT
	movabsq	$577874754439284487, %rsi
	movabsq	$362263814143805190, %rdi
	movq	%rsi, (%rax)
	leaq	16(%rax), %rdx
	movq	%r15, %rsi
	movq	%rdi, 8(%rax)
	movq	-7728(%rbp), %rdi
	movq	%rax, -7280(%rbp)
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7280(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L580
	call	_ZdlPv@PLT
.L580:
	movq	(%r12), %rax
	leaq	-240(%rbp), %rsi
	leaq	-112(%rbp), %rdx
	movq	%r15, %rdi
	movdqu	96(%rax), %xmm0
	movdqu	(%rax), %xmm6
	movdqu	16(%rax), %xmm5
	movdqu	32(%rax), %xmm4
	movdqu	48(%rax), %xmm3
	movdqu	64(%rax), %xmm2
	movdqu	80(%rax), %xmm1
	movdqu	112(%rax), %xmm7
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm6, -240(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm5, -224(%rbp)
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -7280(%rbp)
	movq	$0, -7264(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3696(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -7744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L581
	call	_ZdlPv@PLT
.L581:
	movq	-7816(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3632(%rbp)
	je	.L582
.L898:
	movq	-7816(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	$0, -7264(%rbp)
	movaps	%xmm0, -7280(%rbp)
	call	_Znwm@PLT
	movabsq	$577874754439284487, %rsi
	movabsq	$362263814143805190, %rdi
	movq	%rsi, (%rax)
	leaq	16(%rax), %rdx
	movq	%r15, %rsi
	movq	%rdi, 8(%rax)
	movq	-7744(%rbp), %rdi
	movq	%rax, -7280(%rbp)
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7280(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L583
	call	_ZdlPv@PLT
.L583:
	movq	(%r12), %rax
	movq	24(%rax), %rsi
	movq	8(%rax), %rcx
	movq	48(%rax), %rdx
	movq	56(%rax), %rdi
	movq	112(%rax), %r8
	movq	%rsi, -7800(%rbp)
	movq	32(%rax), %rsi
	movq	120(%rax), %r9
	movq	%rcx, -7504(%rbp)
	movq	16(%rax), %rcx
	movq	%rdx, -7816(%rbp)
	movl	$49, %edx
	movq	(%rax), %r13
	movq	64(%rax), %r12
	movq	%rsi, -7808(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdi, -7904(%rbp)
	movq	%r14, %rdi
	movq	%rcx, -7784(%rbp)
	movq	%r8, -7984(%rbp)
	movq	%r9, -7968(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-7968(%rbp), %r9
	movq	%r15, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-7984(%rbp), %r8
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6ObjectEvE5valueE(%rip), %esi
	movq	%r15, %rdi
	movq	%rax, %rcx
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, -7968(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-7968(%rbp), %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal21UnsafeCast5ATSmi_1410EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$50, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -7984(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-7984(%rbp), %r8
	movq	%r15, %rdi
	movq	%rax, -7968(%rbp)
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-7968(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, -7968(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$46, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$39, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %xmm0
	movl	$72, %edi
	movq	%r12, -176(%rbp)
	movhps	-7504(%rbp), %xmm0
	movq	$0, -7264(%rbp)
	movaps	%xmm0, -240(%rbp)
	movq	-7784(%rbp), %xmm0
	movhps	-7800(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-7808(%rbp), %xmm0
	movhps	-7968(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-7816(%rbp), %xmm0
	movhps	-7904(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7280(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm5
	movdqa	-224(%rbp), %xmm6
	movdqa	-208(%rbp), %xmm3
	leaq	72(%rax), %rdx
	movq	%rax, -7280(%rbp)
	movdqa	-192(%rbp), %xmm4
	movq	-7520(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm4, 48(%rax)
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L584
	call	_ZdlPv@PLT
.L584:
	movq	-7544(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L899:
	movq	-7544(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7312(%rbp), %rax
	movq	-7520(%rbp), %rdi
	leaq	-7368(%rbp), %rcx
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7352(%rbp), %r9
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7360(%rbp), %r8
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7376(%rbp), %rdx
	pushq	%rax
	leaq	-7384(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES8_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EESN_
	addq	$32, %rsp
	movl	$35, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$64, %edi
	movq	-7336(%rbp), %xmm0
	movq	-7352(%rbp), %xmm1
	movq	-7368(%rbp), %xmm2
	movq	-7384(%rbp), %xmm3
	movq	$0, -7264(%rbp)
	movhps	-7328(%rbp), %xmm0
	movhps	-7344(%rbp), %xmm1
	movhps	-7360(%rbp), %xmm2
	movhps	-7376(%rbp), %xmm3
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -240(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -7280(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm5
	movq	%r15, %rsi
	movdqa	-224(%rbp), %xmm6
	movdqa	-208(%rbp), %xmm7
	movdqa	-192(%rbp), %xmm3
	leaq	64(%rax), %rdx
	movq	%rax, -7280(%rbp)
	movq	-7504(%rbp), %rdi
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L586
	call	_ZdlPv@PLT
.L586:
	movq	-7864(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L900:
	movq	-7760(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7384(%rbp)
	leaq	-7312(%rbp), %r12
	movq	$0, -7376(%rbp)
	leaq	-240(%rbp), %r13
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7328(%rbp), %rax
	movq	-7696(%rbp), %rdi
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7352(%rbp), %r9
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7368(%rbp), %rcx
	pushq	%rax
	leaq	-7360(%rbp), %r8
	leaq	-7376(%rbp), %rdx
	leaq	-7384(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EE
	addq	$32, %rsp
	movl	$55, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7328(%rbp), %rsi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal15Cast6String_119EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-160(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-7336(%rbp), %xmm0
	movq	%rax, %xmm5
	movq	-7328(%rbp), %xmm4
	movq	$0, -7296(%rbp)
	movq	-7352(%rbp), %xmm1
	movq	-7368(%rbp), %xmm2
	movq	-7384(%rbp), %xmm3
	movhps	-7328(%rbp), %xmm0
	punpcklqdq	%xmm5, %xmm4
	movhps	-7344(%rbp), %xmm1
	movhps	-7360(%rbp), %xmm2
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-7376(%rbp), %xmm3
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm3, -240(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm0, -7312(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7800(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L588
	call	_ZdlPv@PLT
.L588:
	movq	-7824(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3120(%rbp), %rax
	cmpq	$0, -7272(%rbp)
	movq	%rax, -7544(%rbp)
	jne	.L917
.L589:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L901:
	movq	-7952(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -7264(%rbp)
	movaps	%xmm0, -7280(%rbp)
	call	_Znwm@PLT
	movq	-7544(%rbp), %rdi
	movq	%r15, %rsi
	movabsq	$577874754439284487, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%rax, -7280(%rbp)
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7280(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L592
	call	_ZdlPv@PLT
.L592:
	movq	(%r12), %rax
	movl	$64, %edi
	movdqu	48(%rax), %xmm0
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movdqu	(%rax), %xmm4
	movq	$0, -7264(%rbp)
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -240(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -7280(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-224(%rbp), %xmm7
	movdqa	-208(%rbp), %xmm3
	movdqa	-192(%rbp), %xmm4
	leaq	64(%rax), %rdx
	movq	%rax, -7280(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm4, 48(%rax)
	leaq	-1584(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	movq	%rax, -7784(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L593
	call	_ZdlPv@PLT
.L593:
	movq	-7888(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L902:
	movq	-7824(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-240(%rbp), %r13
	movl	$1799, %r12d
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-230(%rbp), %rdx
	movw	%r12w, -232(%rbp)
	movabsq	$577874754439284487, %rax
	movq	%rax, -240(%rbp)
	movaps	%xmm0, -7280(%rbp)
	movq	$0, -7264(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7800(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7280(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L595
	call	_ZdlPv@PLT
.L595:
	movq	(%r12), %rax
	movl	$57, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	leaq	-7328(%rbp), %r12
	movq	(%rax), %rcx
	movq	%rcx, -7808(%rbp)
	movq	8(%rax), %rcx
	movq	%rcx, -7824(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -8032(%rbp)
	movq	24(%rax), %rcx
	movq	%rcx, -7904(%rbp)
	movq	32(%rax), %rcx
	movq	%rcx, -7968(%rbp)
	movq	40(%rax), %rcx
	movq	%rcx, -7984(%rbp)
	movq	48(%rax), %rcx
	movq	%rcx, -7760(%rbp)
	movq	56(%rax), %rcx
	movq	72(%rax), %rax
	movq	%rcx, -8000(%rbp)
	movq	%rax, -7992(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, -7816(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19IsUndefinedConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	je	.L596
.L598:
	movq	-7816(%rbp), %xmm0
	movq	%r12, %rdi
	movhps	-7992(%rbp), %xmm0
	movaps	%xmm0, -7952(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	xorl	%edx, %edx
	movq	%rax, %rsi
.L879:
	movq	%r15, %rdi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-7280(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -7816(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-7952(%rbp), %xmm0
	movl	$6, %edi
	xorl	%esi, %esi
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	pushq	%r13
	movq	-7808(%rbp), %r9
	movq	%r12, %rdi
	leaq	-7312(%rbp), %rdx
	movq	-7968(%rbp), %xmm1
	movaps	%xmm0, -224(%rbp)
	movq	-7984(%rbp), %xmm0
	movq	%rax, -7312(%rbp)
	movq	-7264(%rbp), %rax
	movhps	-7816(%rbp), %xmm1
	movhps	-7904(%rbp), %xmm0
	movaps	%xmm1, -240(%rbp)
	movaps	%xmm0, -208(%rbp)
	movq	%rax, -7304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	popq	%r10
	movq	%r12, %rdi
	popq	%r11
	movq	-8032(%rbp), %xmm2
	movq	%rax, -7952(%rbp)
	movq	-7968(%rbp), %xmm4
	movq	-7808(%rbp), %xmm7
	movq	-7760(%rbp), %xmm6
	movhps	-7904(%rbp), %xmm2
	movhps	-7984(%rbp), %xmm4
	movhps	-7824(%rbp), %xmm7
	movaps	%xmm2, -8032(%rbp)
	movhps	-8000(%rbp), %xmm6
	movaps	%xmm7, -8016(%rbp)
	movaps	%xmm4, -7968(%rbp)
	movaps	%xmm6, -7984(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$56, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$58, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7952(%rbp), %rdx
	movq	-7808(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ToString_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	movq	%rax, -7808(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$59, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$8, %esi
	movq	%r14, %rdi
	movq	%rax, -7904(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-7824(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%r15, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$46, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r14, %rdi
	movq	%rax, -7816(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$37, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7760(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	-7816(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, -8000(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-8000(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	-72(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, -8000(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-7904(%rbp), %rax
	movq	%r12, %rdx
	movq	-7824(%rbp), %xmm5
	movq	-7808(%rbp), %xmm3
	movdqa	-8016(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%rax, -144(%rbp)
	movq	-7816(%rbp), %rax
	movq	%r15, %rdi
	movq	-7992(%rbp), %xmm1
	movdqa	-8032(%rbp), %xmm2
	punpcklqdq	%xmm5, %xmm3
	movaps	%xmm7, -240(%rbp)
	movq	%rax, -136(%rbp)
	movq	-7760(%rbp), %rax
	movdqa	-7968(%rbp), %xmm4
	movdqa	-7984(%rbp), %xmm6
	movhps	-7952(%rbp), %xmm1
	movaps	%xmm3, -8048(%rbp)
	movq	%rax, -128(%rbp)
	movq	%rax, -120(%rbp)
	movq	-7904(%rbp), %rax
	movaps	%xmm1, -7952(%rbp)
	movq	%rax, -104(%rbp)
	movq	-7816(%rbp), %rax
	movaps	%xmm2, -224(%rbp)
	movq	%rax, -96(%rbp)
	movq	-7760(%rbp), %rax
	movaps	%xmm4, -208(%rbp)
	movq	%rax, -88(%rbp)
	movq	%rax, -80(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movq	%xmm5, -112(%rbp)
	movaps	%xmm0, -7280(%rbp)
	movq	$0, -7264(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2736(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -7808(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L599
	call	_ZdlPv@PLT
.L599:
	movdqa	-8016(%rbp), %xmm5
	movdqa	-7952(%rbp), %xmm4
	movq	%r12, %rdx
	movq	%r13, %rsi
	movdqa	-7968(%rbp), %xmm7
	movdqa	-8032(%rbp), %xmm6
	movq	%r15, %rdi
	movq	$0, -7264(%rbp)
	movaps	%xmm5, -240(%rbp)
	movdqa	-8048(%rbp), %xmm5
	movdqa	-7984(%rbp), %xmm3
	movaps	%xmm4, -176(%rbp)
	movq	-7904(%rbp), %xmm4
	movaps	%xmm5, -160(%rbp)
	movq	-7816(%rbp), %xmm5
	movdqa	%xmm4, %xmm0
	movaps	%xmm7, -208(%rbp)
	movq	-7760(%rbp), %xmm7
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm0, -144(%rbp)
	movdqa	%xmm7, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	-7824(%rbp), %xmm0
	movq	%xmm7, -80(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -112(%rbp)
	movdqa	%xmm5, %xmm0
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7280(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2544(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -7816(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L600
	call	_ZdlPv@PLT
.L600:
	movq	-7912(%rbp), %rcx
	movq	-7896(%rbp), %rdx
	movq	%r14, %rdi
	movq	-8000(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L904:
	movq	-7912(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7480(%rbp)
	movq	$0, -7472(%rbp)
	movq	$0, -7464(%rbp)
	movq	$0, -7456(%rbp)
	movq	$0, -7448(%rbp)
	movq	$0, -7440(%rbp)
	movq	$0, -7432(%rbp)
	movq	$0, -7424(%rbp)
	movq	$0, -7416(%rbp)
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7312(%rbp), %rax
	movq	-7816(%rbp), %rdi
	leaq	-7464(%rbp), %rcx
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7448(%rbp), %r9
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7456(%rbp), %r8
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7472(%rbp), %rdx
	pushq	%rax
	leaq	-7352(%rbp), %rax
	leaq	-7480(%rbp), %rsi
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	leaq	-7376(%rbp), %rax
	pushq	%rax
	leaq	-7384(%rbp), %rax
	pushq	%rax
	leaq	-7392(%rbp), %rax
	pushq	%rax
	leaq	-7400(%rbp), %rax
	pushq	%rax
	leaq	-7408(%rbp), %rax
	pushq	%rax
	leaq	-7416(%rbp), %rax
	pushq	%rax
	leaq	-7424(%rbp), %rax
	pushq	%rax
	leaq	-7432(%rbp), %rax
	pushq	%rax
	leaq	-7440(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES6_S9_S6_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EEPNSC_IS7_EEPNSC_IS8_EESI_PNSC_IS9_EESK_SQ_SK_SG_SI_SI_SI_SI_PNSC_ISA_EESI_SI_SI_SI_
	subq	$-128, %rsp
	movl	$41, %edx
	movq	%r14, %rdi
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-240(%rbp), %rsi
	leaq	-112(%rbp), %rdx
	movq	%r15, %rdi
	movq	-7368(%rbp), %xmm0
	movq	-7384(%rbp), %xmm1
	movq	$0, -7264(%rbp)
	movq	-7400(%rbp), %xmm2
	movq	-7416(%rbp), %xmm3
	movq	-7432(%rbp), %xmm4
	movhps	-7360(%rbp), %xmm0
	movq	-7448(%rbp), %xmm5
	movhps	-7376(%rbp), %xmm1
	movq	-7464(%rbp), %xmm6
	movhps	-7392(%rbp), %xmm2
	movq	-7480(%rbp), %xmm7
	movhps	-7408(%rbp), %xmm3
	movhps	-7424(%rbp), %xmm4
	movhps	-7440(%rbp), %xmm5
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-7456(%rbp), %xmm6
	movhps	-7472(%rbp), %xmm7
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm7, -240(%rbp)
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -7280(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7760(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L604
	call	_ZdlPv@PLT
.L604:
	movq	-7832(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L903:
	movq	-7896(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7480(%rbp)
	movq	$0, -7472(%rbp)
	movq	$0, -7464(%rbp)
	movq	$0, -7456(%rbp)
	movq	$0, -7448(%rbp)
	movq	$0, -7440(%rbp)
	movq	$0, -7432(%rbp)
	movq	$0, -7424(%rbp)
	movq	$0, -7416(%rbp)
	movq	$0, -7408(%rbp)
	movq	$0, -7400(%rbp)
	movq	$0, -7392(%rbp)
	movq	$0, -7384(%rbp)
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7312(%rbp), %rax
	movq	-7808(%rbp), %rdi
	leaq	-7448(%rbp), %r9
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7456(%rbp), %r8
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7464(%rbp), %rcx
	pushq	%rax
	leaq	-7344(%rbp), %rax
	leaq	-7472(%rbp), %rdx
	pushq	%rax
	leaq	-7352(%rbp), %rax
	leaq	-7480(%rbp), %rsi
	pushq	%rax
	leaq	-7360(%rbp), %rax
	pushq	%rax
	leaq	-7368(%rbp), %rax
	pushq	%rax
	leaq	-7376(%rbp), %rax
	pushq	%rax
	leaq	-7384(%rbp), %rax
	pushq	%rax
	leaq	-7392(%rbp), %rax
	pushq	%rax
	leaq	-7400(%rbp), %rax
	pushq	%rax
	leaq	-7408(%rbp), %rax
	pushq	%rax
	leaq	-7416(%rbp), %rax
	pushq	%rax
	leaq	-7424(%rbp), %rax
	pushq	%rax
	leaq	-7432(%rbp), %rax
	pushq	%rax
	leaq	-7440(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectES6_S9_S6_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSC_IS4_EEPNSC_IS5_EEPNSC_IS6_EEPNSC_IS7_EEPNSC_IS8_EESI_PNSC_IS9_EESK_SQ_SK_SG_SI_SI_SI_SI_PNSC_ISA_EESI_SI_SI_SI_
	subq	$-128, %rsp
	movl	$39, %edx
	movq	%r14, %rdi
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7312(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrMulENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7344(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$38, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rcx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	-7352(%rbp), %rdx
	call	_ZN2v88internal45UnsafeNewReference20UT5ATSmi10HeapObject_1411EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	pxor	%xmm0, %xmm0
	leaq	-56(%rbp), %rdx
	movq	%r15, %rdi
	movq	-7480(%rbp), %rax
	leaq	-240(%rbp), %rsi
	movq	%rax, -240(%rbp)
	movq	-7472(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	-7464(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-7456(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-7448(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-7440(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-7432(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-7424(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-7416(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-7408(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-7400(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-7392(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-7384(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-7376(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-7368(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-7360(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-7352(%rbp), %rax
	movq	$0, -7264(%rbp)
	movq	%rax, -112(%rbp)
	movq	-7344(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-7336(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-7328(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-7312(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-7280(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-7272(%rbp), %rax
	movaps	%xmm0, -7280(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7824(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L602
	call	_ZdlPv@PLT
.L602:
	movq	-7792(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L908:
	movq	-7584(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-1776(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$18, %edi
	movq	$0, -7264(%rbp)
	movaps	%xmm0, -7280(%rbp)
	call	_Znwm@PLT
	movl	$1287, %edi
	movdqa	.LC7(%rip), %xmm0
	movq	%r15, %rsi
	movw	%di, 16(%rax)
	leaq	18(%rax), %rdx
	movq	%r12, %rdi
	movups	%xmm0, (%rax)
	movq	%rax, -7280(%rbp)
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7280(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L614
	call	_ZdlPv@PLT
.L614:
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	movq	24(%rax), %rdx
	movq	40(%rax), %r11
	movq	48(%rax), %rdi
	movq	(%rax), %rcx
	movq	80(%rax), %r8
	movq	%rsi, -7832(%rbp)
	movq	%rdx, -7896(%rbp)
	movq	16(%rax), %rsi
	movq	32(%rax), %rdx
	movq	128(%rax), %r10
	movq	%r11, -8016(%rbp)
	movq	136(%rax), %r9
	movq	56(%rax), %r11
	movq	%rsi, -7880(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -7904(%rbp)
	movl	$59, %edx
	movq	64(%rax), %r12
	movq	%rdi, -7912(%rbp)
	movq	%r14, %rdi
	movq	%r11, -7952(%rbp)
	movq	%rcx, -7792(%rbp)
	movq	%r8, -7992(%rbp)
	movq	%r10, -7984(%rbp)
	movq	%r9, -7968(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-7968(%rbp), %r9
	movq	%r15, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-7984(%rbp), %r10
	movl	$2, %r9d
	movq	%r15, %rdi
	movq	-7992(%rbp), %r8
	movq	%rax, %rcx
	movl	$7, %esi
	movq	%r10, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler13StoreToObjectENS0_21MachineRepresentationENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEEPNS1_4NodeENS0_25StoreToObjectWriteBarrierE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$60, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler21LoadStringLengthAsSmiENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-8016(%rbp), %r11
	movq	%r15, %rdi
	movq	%rax, %r12
	movq	%r11, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, -7968(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$55, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$64, %edi
	movq	-7792(%rbp), %xmm0
	movq	$0, -7264(%rbp)
	movhps	-7832(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-7880(%rbp), %xmm0
	movhps	-7896(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-7904(%rbp), %xmm0
	movhps	-7968(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-7912(%rbp), %xmm0
	movhps	-7952(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7280(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-224(%rbp), %xmm7
	movdqa	-208(%rbp), %xmm3
	movdqa	-192(%rbp), %xmm4
	leaq	64(%rax), %rdx
	movq	%rax, -7280(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm4, 48(%rax)
	leaq	-1392(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	movq	%rax, -7792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L615
	call	_ZdlPv@PLT
.L615:
	movq	-7920(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1520(%rbp)
	je	.L616
.L909:
	movq	-7888(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7312(%rbp), %rax
	movq	-7784(%rbp), %rdi
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7360(%rbp), %rcx
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7344(%rbp), %r9
	pushq	%rax
	leaq	-7352(%rbp), %r8
	leaq	-7368(%rbp), %rdx
	leaq	-7376(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EE
	addq	$32, %rsp
	movl	$62, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$63, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -1328(%rbp)
	je	.L617
.L910:
	movq	-7920(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7312(%rbp), %rax
	movq	-7792(%rbp), %rdi
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7360(%rbp), %rcx
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7344(%rbp), %r9
	pushq	%rax
	leaq	-7352(%rbp), %r8
	leaq	-7368(%rbp), %rdx
	leaq	-7376(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EE
	addq	$32, %rsp
	movl	$35, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$64, %edi
	movq	-7328(%rbp), %xmm0
	movq	-7344(%rbp), %xmm1
	movq	-7360(%rbp), %xmm2
	movq	-7376(%rbp), %xmm3
	movq	$0, -7264(%rbp)
	movhps	-7312(%rbp), %xmm0
	movhps	-7336(%rbp), %xmm1
	movhps	-7352(%rbp), %xmm2
	movhps	-7368(%rbp), %xmm3
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -240(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -7280(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm5
	movq	%r15, %rsi
	movdqa	-224(%rbp), %xmm6
	movdqa	-208(%rbp), %xmm7
	movdqa	-192(%rbp), %xmm3
	leaq	64(%rax), %rdx
	movq	%rax, -7280(%rbp)
	movq	-7504(%rbp), %rdi
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L618
	call	_ZdlPv@PLT
.L618:
	movq	-7864(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L907:
	movq	-7880(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$18, %edi
	movq	$0, -7264(%rbp)
	movaps	%xmm0, -7280(%rbp)
	call	_Znwm@PLT
	movl	$1287, %r8d
	movq	%r13, %rdi
	movq	%r15, %rsi
	movdqa	.LC7(%rip), %xmm0
	movw	%r8w, 16(%rax)
	leaq	18(%rax), %rdx
	movq	%rax, -7280(%rbp)
	movups	%xmm0, (%rax)
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7280(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L611
	call	_ZdlPv@PLT
.L611:
	movq	(%r12), %rax
	leaq	-240(%rbp), %rsi
	leaq	-96(%rbp), %rdx
	movq	%r15, %rdi
	movdqu	112(%rax), %xmm0
	movdqu	(%rax), %xmm7
	movdqu	16(%rax), %xmm6
	movdqu	32(%rax), %xmm5
	movdqu	48(%rax), %xmm4
	movdqu	64(%rax), %xmm3
	movdqu	80(%rax), %xmm2
	movdqu	96(%rax), %xmm1
	movdqu	128(%rax), %xmm8
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm7, -240(%rbp)
	movaps	%xmm8, -112(%rbp)
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -7280(%rbp)
	movq	$0, -7264(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1776(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L612
	call	_ZdlPv@PLT
.L612:
	movq	-7584(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L906:
	movq	-7832(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC7(%rip), %xmm0
	leaq	-240(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-224(%rbp), %rdx
	movq	$0, -7264(%rbp)
	movaps	%xmm0, -240(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7280(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7760(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L609
	call	_ZdlPv@PLT
.L609:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L905:
	movq	-7792(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-240(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC7(%rip), %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	$1797, %r9d
	leaq	-217(%rbp), %rdx
	movl	$84215047, -224(%rbp)
	movaps	%xmm0, -240(%rbp)
	pxor	%xmm0, %xmm0
	movw	%r9w, -220(%rbp)
	movaps	%xmm0, -7280(%rbp)
	movb	$5, -218(%rbp)
	movq	$0, -7264(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7824(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7280(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L606
	call	_ZdlPv@PLT
.L606:
	movq	(%r12), %rax
	movq	40(%rax), %rdi
	movq	8(%rax), %rsi
	movq	24(%rax), %rdx
	movq	72(%rax), %r11
	movq	88(%rax), %r10
	movq	104(%rax), %r9
	movq	%rdi, -7968(%rbp)
	movq	56(%rax), %rdi
	movq	(%rax), %rcx
	movq	%rsi, -7896(%rbp)
	movq	120(%rax), %r8
	movq	16(%rax), %rsi
	movq	%rdx, -7912(%rbp)
	movq	%rdi, -7984(%rbp)
	movq	32(%rax), %rdx
	movq	64(%rax), %rdi
	movq	%r11, -8016(%rbp)
	movq	80(%rax), %r11
	movq	%r10, -8000(%rbp)
	movq	96(%rax), %r10
	movq	48(%rax), %r12
	movq	%r9, -8056(%rbp)
	movq	112(%rax), %r9
	movq	%rcx, -7792(%rbp)
	movq	%r11, -8032(%rbp)
	movq	%r10, -8048(%rbp)
	movq	%r9, -8064(%rbp)
	movq	%rsi, -7904(%rbp)
	leaq	.LC8(%rip), %rsi
	movq	%rdx, -7952(%rbp)
	movl	$46, %edx
	movq	%rdi, -7992(%rbp)
	movq	%r14, %rdi
	movq	%r8, -8072(%rbp)
	movq	168(%rax), %r8
	movq	176(%rax), %rax
	movq	%r8, -8080(%rbp)
	movq	%rax, -8088(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	leaq	-96(%rbp), %rdx
	movq	%r15, %rdi
	movq	-7792(%rbp), %xmm0
	leaq	-1968(%rbp), %r13
	movq	$0, -7264(%rbp)
	movhps	-7896(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-7904(%rbp), %xmm0
	movhps	-7912(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-7952(%rbp), %xmm0
	movhps	-7968(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	%r12, %xmm0
	movhps	-7984(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-7992(%rbp), %xmm0
	movhps	-8016(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-8032(%rbp), %xmm0
	movhps	-8000(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-8048(%rbp), %xmm0
	movhps	-8056(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-8064(%rbp), %xmm0
	movhps	-8072(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-8080(%rbp), %xmm0
	movhps	-8088(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7280(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L607
	call	_ZdlPv@PLT
.L607:
	movq	-7880(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L912:
	movq	-7928(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7312(%rbp), %rax
	movq	-7832(%rbp), %rdi
	leaq	-7352(%rbp), %rcx
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7336(%rbp), %r9
	pushq	%rax
	leaq	-7344(%rbp), %r8
	leaq	-7360(%rbp), %rdx
	leaq	-7368(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESG_
	movq	%rbx, %rdi
	movl	$1, %esi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7312(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$56, %edi
	movq	-7336(%rbp), %xmm0
	movq	-7352(%rbp), %xmm1
	movq	-7368(%rbp), %xmm2
	movq	%rbx, -192(%rbp)
	movhps	-7328(%rbp), %xmm0
	movhps	-7344(%rbp), %xmm1
	movq	$0, -7264(%rbp)
	movhps	-7360(%rbp), %xmm2
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -240(%rbp)
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm0, -7280(%rbp)
	call	_Znwm@PLT
	movq	-192(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm7
	movdqa	-224(%rbp), %xmm3
	movdqa	-208(%rbp), %xmm4
	leaq	56(%rax), %rdx
	movq	%rax, -7280(%rbp)
	movq	%rcx, 48(%rax)
	movq	-7496(%rbp), %rdi
	movups	%xmm7, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm4, 32(%rax)
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	popq	%rcx
	popq	%rsi
	testq	%rdi, %rdi
	je	.L622
	call	_ZdlPv@PLT
.L622:
	movq	-7840(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L911:
	movq	-7864(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7376(%rbp)
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7312(%rbp), %rax
	movq	-7504(%rbp), %rdi
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7360(%rbp), %rcx
	pushq	%rax
	leaq	-7336(%rbp), %rax
	leaq	-7344(%rbp), %r9
	pushq	%rax
	leaq	-7352(%rbp), %r8
	leaq	-7368(%rbp), %rdx
	leaq	-7376(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_NS0_6ObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EESH_PNSB_IS9_EE
	addq	$32, %rsp
	movl	$33, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$32, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7376(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movaps	%xmm0, -7280(%rbp)
	movq	%rax, -240(%rbp)
	movq	-7368(%rbp), %rax
	movq	$0, -7264(%rbp)
	movq	%rax, -232(%rbp)
	movq	-7360(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-7352(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-7344(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-7336(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-7328(%rbp), %rax
	movq	%rax, -192(%rbp)
	call	_Znwm@PLT
	movq	-192(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm4
	movdqa	-224(%rbp), %xmm5
	movdqa	-208(%rbp), %xmm6
	leaq	56(%rax), %rdx
	movq	%rax, -7280(%rbp)
	movq	%rcx, 48(%rax)
	movq	-7832(%rbp), %rdi
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L620
	call	_ZdlPv@PLT
.L620:
	movq	-7928(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L913:
	movq	-7872(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -7368(%rbp)
	movq	$0, -7360(%rbp)
	movq	$0, -7352(%rbp)
	movq	$0, -7344(%rbp)
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	$0, -7312(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7312(%rbp), %rax
	movq	-7664(%rbp), %rdi
	leaq	-7352(%rbp), %rcx
	pushq	%rax
	leaq	-7328(%rbp), %rax
	leaq	-7336(%rbp), %r9
	pushq	%rax
	leaq	-7344(%rbp), %r8
	leaq	-7360(%rbp), %rdx
	leaq	-7368(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_6StringENS0_10JSReceiverENS0_3SmiES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EEPNSA_IS6_EEPNSA_IS7_EEPNSA_IS8_EESG_
	movl	$30, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$27, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7368(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movaps	%xmm0, -7280(%rbp)
	movq	%rax, -240(%rbp)
	movq	-7360(%rbp), %rax
	movq	$0, -7264(%rbp)
	movq	%rax, -232(%rbp)
	movq	-7352(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-7344(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-7336(%rbp), %rax
	movq	%rax, -208(%rbp)
	call	_Znwm@PLT
	movq	-208(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm5
	movdqa	-224(%rbp), %xmm6
	movq	-7840(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -7280(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L624
	call	_ZdlPv@PLT
.L624:
	movq	-7936(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L914:
	movq	-7936(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -7264(%rbp)
	movaps	%xmm0, -7280(%rbp)
	call	_Znwm@PLT
	movq	-7840(%rbp), %rdi
	movq	%r15, %rsi
	movl	$117769991, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -7280(%rbp)
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7280(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L626
	call	_ZdlPv@PLT
.L626:
	movq	(%rbx), %rax
	movl	$40, %edi
	leaq	-432(%rbp), %r12
	movdqu	16(%rax), %xmm0
	movq	32(%rax), %rdx
	movdqu	(%rax), %xmm7
	movq	$0, -7264(%rbp)
	movaps	%xmm0, -224(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -208(%rbp)
	movaps	%xmm7, -240(%rbp)
	movaps	%xmm0, -7280(%rbp)
	call	_Znwm@PLT
	movq	-208(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r15, %rsi
	movdqa	-240(%rbp), %xmm2
	movdqa	-224(%rbp), %xmm3
	leaq	40(%rax), %rdx
	movq	%rax, -7280(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -7264(%rbp)
	movq	%rdx, -7272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L627
	call	_ZdlPv@PLT
.L627:
	movq	-7856(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L596:
	movq	-7816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IsNullConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L598
	movq	-7816(%rbp), %xmm0
	movq	%r12, %rdi
	movhps	-7992(%rbp), %xmm0
	movaps	%xmm0, -7952(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	movq	%rax, %rsi
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L916:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-7520(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movdqa	-7696(%rbp), %xmm5
	movdqa	-7728(%rbp), %xmm6
	movq	-7776(%rbp), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movaps	%xmm4, -240(%rbp)
	movdqa	-7680(%rbp), %xmm4
	leaq	-168(%rbp), %rdx
	movq	%rax, -176(%rbp)
	movaps	%xmm4, -224(%rbp)
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm0, -7312(%rbp)
	movq	$0, -7296(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7536(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L557
	call	_ZdlPv@PLT
.L557:
	movq	-7752(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L917:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	-7384(%rbp), %rdx
	movq	-7328(%rbp), %rax
	movaps	%xmm0, -7312(%rbp)
	movq	$0, -7296(%rbp)
	movq	%rdx, -240(%rbp)
	movq	-7376(%rbp), %rdx
	movq	%rax, -184(%rbp)
	movq	%rdx, -232(%rbp)
	movq	-7368(%rbp), %rdx
	movq	%rax, -176(%rbp)
	movq	%rdx, -224(%rbp)
	movq	-7360(%rbp), %rdx
	movq	%rdx, -216(%rbp)
	movq	-7352(%rbp), %rdx
	movq	%rdx, -208(%rbp)
	movq	-7344(%rbp), %rdx
	movq	%rdx, -200(%rbp)
	movq	-7336(%rbp), %rdx
	movq	%rdx, -192(%rbp)
	leaq	-168(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7544(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L590
	call	_ZdlPv@PLT
.L590:
	movq	-7952(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L589
.L915:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22410:
	.size	_ZN2v88internal43RegExpReplaceCallableNoExplicitCaptures_322EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_7IntPtrTEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE, .-_ZN2v88internal43RegExpReplaceCallableNoExplicitCaptures_322EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_7IntPtrTEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESE_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESE_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESE_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESE_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESE_:
.LFB27221:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$5, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$56, %rsp
	movq	%r8, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$117769991, (%rax)
	leaq	5(%rax), %rdx
	movb	$5, 4(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L919
	movq	%rax, -96(%rbp)
	call	_ZdlPv@PLT
	movq	-96(%rbp), %rax
.L919:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L920
	movq	%rdx, (%r15)
.L920:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L921
	movq	%rdx, (%r14)
.L921:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L922
	movq	%rdx, 0(%r13)
.L922:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L923
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L923:
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L918
	movq	%rax, (%rbx)
.L918:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L945
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L945:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27221:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESE_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESE_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_SD_SF_SF_SF_SF_PNS9_IS7_EESF_SF_SF_SF_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_SD_SF_SF_SF_SF_PNS9_IS7_EESF_SF_SF_SF_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_SD_SF_SF_SF_SF_PNS9_IS7_EESF_SF_SF_SF_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_SD_SF_SF_SF_SF_PNS9_IS7_EESF_SF_SF_SF_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_SD_SF_SF_SF_SF_PNS9_IS7_EESF_SF_SF_SF_:
.LFB27223:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$15, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$136, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -88(%rbp)
	movq	%r9, -96(%rbp)
	movq	88(%rbp), %r15
	movq	%rax, -104(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$361703063247193863, %rcx
	movq	%rcx, (%rax)
	movl	$1285, %ecx
	leaq	15(%rax), %rdx
	movl	$84346117, 8(%rax)
	movw	%cx, 12(%rax)
	movb	$5, 14(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L947
	movq	%rax, -176(%rbp)
	call	_ZdlPv@PLT
	movq	-176(%rbp), %rax
.L947:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L948
	movq	%rdx, (%r14)
.L948:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L949
	movq	%rdx, 0(%r13)
.L949:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L950
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L950:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L951
	movq	%rdx, (%rbx)
.L951:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L952
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L952:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L953
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L953:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L954
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L954:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L955
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L955:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L956
	movq	-128(%rbp), %rsi
	movq	%rdx, (%rsi)
.L956:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L957
	movq	-136(%rbp), %rcx
	movq	%rdx, (%rcx)
.L957:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L958
	movq	-144(%rbp), %rbx
	movq	%rdx, (%rbx)
.L958:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L959
	movq	-152(%rbp), %rsi
	movq	%rdx, (%rsi)
.L959:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L960
	movq	-160(%rbp), %rcx
	movq	%rdx, (%rcx)
.L960:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L961
	movq	-168(%rbp), %rdi
	movq	%rdx, (%rdi)
.L961:
	movq	112(%rax), %rax
	testq	%rax, %rax
	je	.L946
	movq	%rax, (%r15)
.L946:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1013
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1013:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27223:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_SD_SF_SF_SF_SF_PNS9_IS7_EESF_SF_SF_SF_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_SD_SF_SF_SF_SF_PNS9_IS7_EESF_SF_SF_SF_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_NS0_7JSArrayENS0_6ObjectES4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESH_PNSB_IS7_EEPNSB_IS8_EESF_SH_SH_SH_SH_PNSB_IS9_EESH_SH_SH_SH_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_NS0_7JSArrayENS0_6ObjectES4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESH_PNSB_IS7_EEPNSB_IS8_EESF_SH_SH_SH_SH_PNSB_IS9_EESH_SH_SH_SH_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_NS0_7JSArrayENS0_6ObjectES4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESH_PNSB_IS7_EEPNSB_IS8_EESF_SH_SH_SH_SH_PNSB_IS9_EESH_SH_SH_SH_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_NS0_7JSArrayENS0_6ObjectES4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESH_PNSB_IS7_EEPNSB_IS8_EESF_SH_SH_SH_SH_PNSB_IS9_EESH_SH_SH_SH_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_NS0_7JSArrayENS0_6ObjectES4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESH_PNSB_IS7_EEPNSB_IS8_EESF_SH_SH_SH_SH_PNSB_IS9_EESH_SH_SH_SH_:
.LFB27241:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$17, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$152, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -88(%rbp)
	movq	%r9, -96(%rbp)
	movq	104(%rbp), %r15
	movq	%rax, -104(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	88(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	96(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movdqa	.LC10(%rip), %xmm0
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movb	$5, 16(%rax)
	leaq	17(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1015
	movq	%rax, -192(%rbp)
	call	_ZdlPv@PLT
	movq	-192(%rbp), %rax
.L1015:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1016
	movq	%rdx, (%r14)
.L1016:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1017
	movq	%rdx, 0(%r13)
.L1017:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1018
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1018:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1019
	movq	%rdx, (%rbx)
.L1019:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1020
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1020:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1021
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1021:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1022
	movq	-112(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1022:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1023
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1023:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1024
	movq	-128(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1024:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1025
	movq	-136(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1025:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1026
	movq	-144(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1026:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1027
	movq	-152(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1027:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1028
	movq	-160(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1028:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1029
	movq	-168(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1029:
	movq	112(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1030
	movq	-176(%rbp), %rdi
	movq	%rdx, (%rdi)
.L1030:
	movq	120(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1031
	movq	-184(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1031:
	movq	128(%rax), %rax
	testq	%rax, %rax
	je	.L1014
	movq	%rax, (%r15)
.L1014:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1089
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1089:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27241:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_NS0_7JSArrayENS0_6ObjectES4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESH_PNSB_IS7_EEPNSB_IS8_EESF_SH_SH_SH_SH_PNSB_IS9_EESH_SH_SH_SH_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_NS0_7JSArrayENS0_6ObjectES4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESH_PNSB_IS7_EEPNSB_IS8_EESF_SH_SH_SH_SH_PNSB_IS9_EESH_SH_SH_SH_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_NS0_7JSArrayENS0_6ObjectES4_S5_S5_S5_S5_NS0_10HeapObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESH_PNSB_IS7_EEPNSB_IS8_EESF_SH_SH_SH_SH_PNSB_IS9_EESH_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_NS0_7JSArrayENS0_6ObjectES4_S5_S5_S5_S5_NS0_10HeapObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESH_PNSB_IS7_EEPNSB_IS8_EESF_SH_SH_SH_SH_PNSB_IS9_EESH_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_NS0_7JSArrayENS0_6ObjectES4_S5_S5_S5_S5_NS0_10HeapObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESH_PNSB_IS7_EEPNSB_IS8_EESF_SH_SH_SH_SH_PNSB_IS9_EESH_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_NS0_7JSArrayENS0_6ObjectES4_S5_S5_S5_S5_NS0_10HeapObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESH_PNSB_IS7_EEPNSB_IS8_EESF_SH_SH_SH_SH_PNSB_IS9_EESH_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_NS0_7JSArrayENS0_6ObjectES4_S5_S5_S5_S5_NS0_10HeapObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESH_PNSB_IS7_EEPNSB_IS8_EESF_SH_SH_SH_SH_PNSB_IS9_EESH_:
.LFB27247:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$14, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$136, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$506662676253181703, %rcx
	movq	%rcx, (%rax)
	movl	$1287, %ecx
	leaq	14(%rax), %rdx
	movl	$84215045, 8(%rax)
	movw	%cx, 12(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1091
	movq	%rax, -168(%rbp)
	call	_ZdlPv@PLT
	movq	-168(%rbp), %rax
.L1091:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1092
	movq	%rdx, (%r15)
.L1092:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1093
	movq	%rdx, (%r14)
.L1093:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1094
	movq	%rdx, 0(%r13)
.L1094:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1095
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1095:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1096
	movq	%rdx, (%rbx)
.L1096:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1097
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1097:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1098
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1098:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1099
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1099:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1100
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1100:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1101
	movq	-128(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1101:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1102
	movq	-136(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1102:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1103
	movq	-144(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1103:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1104
	movq	-152(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1104:
	movq	104(%rax), %rax
	testq	%rax, %rax
	je	.L1090
	movq	-160(%rbp), %rcx
	movq	%rax, (%rcx)
.L1090:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1153
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1153:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27247:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_NS0_7JSArrayENS0_6ObjectES4_S5_S5_S5_S5_NS0_10HeapObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESH_PNSB_IS7_EEPNSB_IS8_EESF_SH_SH_SH_SH_PNSB_IS9_EESH_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_NS0_7JSArrayENS0_6ObjectES4_S5_S5_S5_S5_NS0_10HeapObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESH_PNSB_IS7_EEPNSB_IS8_EESF_SH_SH_SH_SH_PNSB_IS9_EESH_
	.section	.text._ZN2v88internal45RegExpReplaceCallableWithExplicitCaptures_323EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_7IntPtrTEEENS4_INS0_10JSReceiverEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal45RegExpReplaceCallableWithExplicitCaptures_323EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_7IntPtrTEEENS4_INS0_10JSReceiverEEE
	.type	_ZN2v88internal45RegExpReplaceCallableWithExplicitCaptures_323EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_7IntPtrTEEENS4_INS0_10JSReceiverEEE, @function
_ZN2v88internal45RegExpReplaceCallableWithExplicitCaptures_323EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_7IntPtrTEEENS4_INS0_10JSReceiverEEE:
.LFB22482:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$1208, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -4936(%rbp)
	movq	%rdi, %r13
	leaq	-4928(%rbp), %r15
	leaq	-4568(%rbp), %r12
	movq	%rdx, -4960(%rbp)
	leaq	-3032(%rbp), %rbx
	leaq	-4752(%rbp), %r14
	movq	%rcx, -4944(%rbp)
	movq	%r8, -4968(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -4928(%rbp)
	movq	%rdi, -4624(%rbp)
	movl	$96, %edi
	movq	$0, -4616(%rbp)
	movq	$0, -4608(%rbp)
	movq	$0, -4600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r12, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rdx, -4600(%rbp)
	movq	%rdx, -4608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4584(%rbp)
	movq	%rax, -4616(%rbp)
	movq	$0, -4592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4928(%rbp), %rax
	movl	$120, %edi
	movq	$0, -4424(%rbp)
	movq	$0, -4416(%rbp)
	movq	%rax, -4432(%rbp)
	movq	$0, -4408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -4424(%rbp)
	leaq	-4376(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4408(%rbp)
	movq	%rdx, -4416(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4392(%rbp)
	movq	%rax, -5152(%rbp)
	movq	$0, -4400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4928(%rbp), %rax
	movl	$120, %edi
	movq	$0, -4232(%rbp)
	movq	$0, -4224(%rbp)
	movq	%rax, -4240(%rbp)
	movq	$0, -4216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -4232(%rbp)
	leaq	-4184(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4216(%rbp)
	movq	%rdx, -4224(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4200(%rbp)
	movq	%rax, -4992(%rbp)
	movq	$0, -4208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4928(%rbp), %rax
	movl	$360, %edi
	movq	$0, -4040(%rbp)
	movq	$0, -4032(%rbp)
	movq	%rax, -4048(%rbp)
	movq	$0, -4024(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	360(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 352(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movq	%rax, -4040(%rbp)
	leaq	-3992(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4024(%rbp)
	movq	%rdx, -4032(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4008(%rbp)
	movq	%rax, -5072(%rbp)
	movq	$0, -4016(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4928(%rbp), %rax
	movl	$360, %edi
	movq	$0, -3848(%rbp)
	movq	$0, -3840(%rbp)
	movq	%rax, -3856(%rbp)
	movq	$0, -3832(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	360(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 352(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movq	%rax, -3848(%rbp)
	leaq	-3800(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3832(%rbp)
	movq	%rdx, -3840(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3816(%rbp)
	movq	%rax, -5088(%rbp)
	movq	$0, -3824(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4928(%rbp), %rax
	movl	$408, %edi
	movq	$0, -3656(%rbp)
	movq	$0, -3648(%rbp)
	movq	%rax, -3664(%rbp)
	movq	$0, -3640(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	408(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 400(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movq	%rax, -3656(%rbp)
	leaq	-3608(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3640(%rbp)
	movq	%rdx, -3648(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3624(%rbp)
	movq	%rax, -5112(%rbp)
	movq	$0, -3632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4928(%rbp), %rax
	movl	$240, %edi
	movq	$0, -3464(%rbp)
	movq	$0, -3456(%rbp)
	movq	%rax, -3472(%rbp)
	movq	$0, -3448(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -3464(%rbp)
	leaq	-3416(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3448(%rbp)
	movq	%rdx, -3456(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3432(%rbp)
	movq	%rax, -5000(%rbp)
	movq	$0, -3440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4928(%rbp), %rax
	movl	$288, %edi
	movq	$0, -3272(%rbp)
	movq	$0, -3264(%rbp)
	movq	%rax, -3280(%rbp)
	movq	$0, -3256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -3272(%rbp)
	leaq	-3224(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3256(%rbp)
	movq	%rdx, -3264(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3240(%rbp)
	movq	%rax, -5024(%rbp)
	movq	$0, -3248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4928(%rbp), %rax
	movl	$288, %edi
	movq	$0, -3080(%rbp)
	movq	$0, -3072(%rbp)
	movq	%rax, -3088(%rbp)
	movq	$0, -3064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rdx, -3064(%rbp)
	movq	%rdx, -3072(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3048(%rbp)
	movq	%rax, -3080(%rbp)
	movq	$0, -3056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4928(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2888(%rbp)
	movq	$0, -2880(%rbp)
	movq	%rax, -2896(%rbp)
	movq	$0, -2872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2888(%rbp)
	leaq	-2840(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2872(%rbp)
	movq	%rdx, -2880(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2856(%rbp)
	movq	%rax, -5224(%rbp)
	movq	$0, -2864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4928(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2696(%rbp)
	movq	$0, -2688(%rbp)
	movq	%rax, -2704(%rbp)
	movq	$0, -2680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -2696(%rbp)
	leaq	-2648(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2680(%rbp)
	movq	%rdx, -2688(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2664(%rbp)
	movq	%rax, -5104(%rbp)
	movq	$0, -2672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4928(%rbp), %rax
	movl	$120, %edi
	movq	$0, -2504(%rbp)
	movq	$0, -2496(%rbp)
	movq	%rax, -2512(%rbp)
	movq	$0, -2488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -2504(%rbp)
	leaq	-2456(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2488(%rbp)
	movq	%rdx, -2496(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2472(%rbp)
	movq	%rax, -5096(%rbp)
	movq	$0, -2480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4928(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2312(%rbp)
	movq	$0, -2304(%rbp)
	movq	%rax, -2320(%rbp)
	movq	$0, -2296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2312(%rbp)
	leaq	-2264(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2296(%rbp)
	movq	%rdx, -2304(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2280(%rbp)
	movq	%rax, -5120(%rbp)
	movq	$0, -2288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4928(%rbp), %rax
	movl	$408, %edi
	movq	$0, -2120(%rbp)
	movq	$0, -2112(%rbp)
	movq	%rax, -2128(%rbp)
	movq	$0, -2104(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	408(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 400(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movq	%rax, -2120(%rbp)
	leaq	-2072(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2104(%rbp)
	movq	%rdx, -2112(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2088(%rbp)
	movq	%rax, -5128(%rbp)
	movq	$0, -2096(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4928(%rbp), %rax
	movl	$408, %edi
	movq	$0, -1928(%rbp)
	movq	$0, -1920(%rbp)
	movq	%rax, -1936(%rbp)
	movq	$0, -1912(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	408(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 400(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movq	%rax, -1928(%rbp)
	leaq	-1880(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1912(%rbp)
	movq	%rdx, -1920(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1896(%rbp)
	movq	%rax, -5184(%rbp)
	movq	$0, -1904(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4928(%rbp), %rax
	movl	$456, %edi
	movq	$0, -1736(%rbp)
	movq	$0, -1728(%rbp)
	movq	%rax, -1744(%rbp)
	movq	$0, -1720(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movups	%xmm0, (%rax)
	leaq	456(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movq	%rax, -1736(%rbp)
	movq	$0, 448(%rax)
	leaq	-1688(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1720(%rbp)
	movq	%rdx, -1728(%rbp)
	xorl	%edx, %edx
	movq	$0, -1712(%rbp)
	movups	%xmm0, -1704(%rbp)
	movq	%rax, -5200(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4928(%rbp), %rax
	movl	$288, %edi
	movq	$0, -1544(%rbp)
	movq	$0, -1536(%rbp)
	movq	%rax, -1552(%rbp)
	movq	$0, -1528(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -1544(%rbp)
	leaq	-1496(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1528(%rbp)
	movq	%rdx, -1536(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1512(%rbp)
	movq	%rax, -5216(%rbp)
	movq	$0, -1520(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4928(%rbp), %rax
	movl	$336, %edi
	movq	$0, -1352(%rbp)
	movq	$0, -1344(%rbp)
	movq	%rax, -1360(%rbp)
	movq	$0, -1336(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -1352(%rbp)
	leaq	-1304(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1336(%rbp)
	movq	%rdx, -1344(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1320(%rbp)
	movq	%rax, -5168(%rbp)
	movq	$0, -1328(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4928(%rbp), %rax
	movl	$336, %edi
	movq	$0, -1160(%rbp)
	movq	$0, -1152(%rbp)
	movq	%rax, -1168(%rbp)
	movq	$0, -1144(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -1160(%rbp)
	leaq	-1112(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1144(%rbp)
	movq	%rdx, -1152(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1128(%rbp)
	movq	%rax, -5176(%rbp)
	movq	$0, -1136(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4928(%rbp), %rax
	movl	$120, %edi
	movq	$0, -968(%rbp)
	movq	$0, -960(%rbp)
	movq	%rax, -976(%rbp)
	movq	$0, -952(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -968(%rbp)
	leaq	-920(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -952(%rbp)
	movq	%rdx, -960(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -936(%rbp)
	movq	%rax, -5160(%rbp)
	movq	$0, -944(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4928(%rbp), %rax
	movl	$120, %edi
	movq	$0, -776(%rbp)
	movq	$0, -768(%rbp)
	movq	%rax, -784(%rbp)
	movq	$0, -760(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -776(%rbp)
	leaq	-728(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -760(%rbp)
	movq	%rdx, -768(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -744(%rbp)
	movq	%rax, -5192(%rbp)
	movq	$0, -752(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4928(%rbp), %rax
	movl	$96, %edi
	movq	$0, -584(%rbp)
	movq	$0, -576(%rbp)
	movq	%rax, -592(%rbp)
	movq	$0, -568(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -584(%rbp)
	leaq	-536(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -568(%rbp)
	movq	%rdx, -576(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -552(%rbp)
	movq	%rax, -5208(%rbp)
	movq	$0, -560(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4928(%rbp), %rax
	movl	$96, %edi
	movq	$0, -392(%rbp)
	movq	$0, -384(%rbp)
	movq	%rax, -400(%rbp)
	movq	$0, -376(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -392(%rbp)
	leaq	-344(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -376(%rbp)
	movq	%rdx, -384(%rbp)
	xorl	%edx, %edx
	movq	%rax, -5136(%rbp)
	movups	%xmm0, -360(%rbp)
	movq	$0, -368(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-4944(%rbp), %xmm1
	movq	-4936(%rbp), %xmm2
	movaps	%xmm0, -4752(%rbp)
	movhps	-4968(%rbp), %xmm1
	movq	$0, -4736(%rbp)
	movhps	-4960(%rbp), %xmm2
	movaps	%xmm1, -192(%rbp)
	movaps	%xmm2, -208(%rbp)
	call	_Znwm@PLT
	movdqa	-208(%rbp), %xmm6
	movdqa	-192(%rbp), %xmm7
	movq	%r14, %rsi
	leaq	32(%rax), %rdx
	movq	%rax, -4752(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	leaq	-4624(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4736(%rbp)
	movq	%rdx, -4744(%rbp)
	movq	%rax, -5144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1155
	call	_ZdlPv@PLT
.L1155:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4432(%rbp), %rax
	cmpq	$0, -4560(%rbp)
	movq	%rax, -4936(%rbp)
	jne	.L1355
.L1156:
	leaq	-4240(%rbp), %rax
	cmpq	$0, -4368(%rbp)
	movq	%rax, -4976(%rbp)
	leaq	-784(%rbp), %rax
	movq	%rax, -5016(%rbp)
	jne	.L1356
.L1159:
	leaq	-4048(%rbp), %rax
	cmpq	$0, -4176(%rbp)
	movq	%rax, -5008(%rbp)
	leaq	-3856(%rbp), %rax
	movq	%rax, -5040(%rbp)
	jne	.L1357
.L1162:
	leaq	-3664(%rbp), %rax
	cmpq	$0, -3984(%rbp)
	movq	%rax, -5056(%rbp)
	jne	.L1358
.L1165:
	leaq	-3472(%rbp), %rax
	cmpq	$0, -3792(%rbp)
	movq	%rax, -4968(%rbp)
	jne	.L1359
.L1167:
	leaq	-3280(%rbp), %rax
	cmpq	$0, -3600(%rbp)
	movq	%rax, -4992(%rbp)
	jne	.L1360
.L1169:
	cmpq	$0, -3408(%rbp)
	jne	.L1361
.L1172:
	leaq	-3088(%rbp), %rax
	cmpq	$0, -3216(%rbp)
	movq	%rax, -5000(%rbp)
	jne	.L1362
.L1174:
	leaq	-2704(%rbp), %rax
	cmpq	$0, -3024(%rbp)
	movq	%rax, -5024(%rbp)
	leaq	-2896(%rbp), %rax
	movq	%rax, -4944(%rbp)
	jne	.L1363
.L1177:
	leaq	-2512(%rbp), %rax
	cmpq	$0, -2832(%rbp)
	movq	%rax, -5072(%rbp)
	jne	.L1364
.L1182:
	leaq	-2320(%rbp), %rax
	cmpq	$0, -2640(%rbp)
	movq	%rax, -5088(%rbp)
	jne	.L1365
.L1185:
	leaq	-976(%rbp), %rax
	cmpq	$0, -2448(%rbp)
	movq	%rax, -4960(%rbp)
	jne	.L1366
.L1188:
	leaq	-2128(%rbp), %rax
	cmpq	$0, -2256(%rbp)
	movq	%rax, -5104(%rbp)
	leaq	-1936(%rbp), %rax
	movq	%rax, -5112(%rbp)
	jne	.L1367
.L1190:
	leaq	-1744(%rbp), %rax
	cmpq	$0, -2064(%rbp)
	movq	%rax, -5120(%rbp)
	jne	.L1368
.L1197:
	leaq	-1552(%rbp), %rax
	cmpq	$0, -1872(%rbp)
	movq	%rax, -5128(%rbp)
	jne	.L1369
.L1199:
	cmpq	$0, -1680(%rbp)
	leaq	-1360(%rbp), %rbx
	jne	.L1370
.L1201:
	cmpq	$0, -1488(%rbp)
	jne	.L1371
.L1204:
	leaq	-1168(%rbp), %rax
	cmpq	$0, -1296(%rbp)
	movq	%rax, -5096(%rbp)
	jne	.L1372
	cmpq	$0, -1104(%rbp)
	jne	.L1373
.L1209:
	cmpq	$0, -912(%rbp)
	jne	.L1374
.L1211:
	cmpq	$0, -720(%rbp)
	leaq	-592(%rbp), %r13
	jne	.L1375
.L1213:
	cmpq	$0, -528(%rbp)
	leaq	-400(%rbp), %r12
	jne	.L1376
.L1215:
	movq	-5136(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -4736(%rbp)
	movaps	%xmm0, -4752(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movl	$117769991, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -4752(%rbp)
	movq	%rdx, -4736(%rbp)
	movq	%rdx, -4744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1218
	call	_ZdlPv@PLT
.L1218:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5016(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4960(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5096(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5128(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5120(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5112(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5104(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5088(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5072(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5024(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4944(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5000(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4992(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4968(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5056(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5040(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5008(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4976(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4936(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-5144(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1377
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1355:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -4736(%rbp)
	movaps	%xmm0, -4752(%rbp)
	call	_Znwm@PLT
	movq	-5144(%rbp), %rdi
	movq	%r14, %rsi
	movl	$117769991, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -4752(%rbp)
	movq	%rdx, -4736(%rbp)
	movq	%rdx, -4744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4752(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L1157
	call	_ZdlPv@PLT
.L1157:
	movq	(%r12), %rax
	movl	$72, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r12
	movq	%rcx, -4936(%rbp)
	movq	16(%rax), %rcx
	movq	24(%rax), %rax
	movq	%rcx, -4960(%rbp)
	movq	%rax, -4944(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r12, %xmm0
	movl	$40, %edi
	movq	$0, -4736(%rbp)
	movhps	-4936(%rbp), %xmm0
	movq	%rax, -176(%rbp)
	movaps	%xmm0, -208(%rbp)
	movq	-4960(%rbp), %xmm0
	movhps	-4944(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -4752(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rcx
	movdqa	-208(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm6
	leaq	40(%rax), %rdx
	movq	%rax, -4752(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm6, 16(%rax)
	leaq	-4432(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4736(%rbp)
	movq	%rdx, -4744(%rbp)
	movq	%rax, -4936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1158
	call	_ZdlPv@PLT
.L1158:
	movq	-5152(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1156
	.p2align 4,,10
	.p2align 3
.L1356:
	movq	-5152(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4824(%rbp)
	movq	$0, -4816(%rbp)
	movq	$0, -4808(%rbp)
	movq	$0, -4800(%rbp)
	movq	$0, -4784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-4936(%rbp), %rdi
	leaq	-4784(%rbp), %r9
	leaq	-4800(%rbp), %r8
	leaq	-4808(%rbp), %rcx
	leaq	-4816(%rbp), %rdx
	leaq	-4824(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESE_
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4808(%rbp), %rdx
	movq	-4784(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -4960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-4816(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	-4808(%rbp), %rsi
	movq	-4800(%rbp), %rdx
	movq	-4824(%rbp), %rax
	movl	$40, %edi
	movaps	%xmm0, -4752(%rbp)
	movq	-4784(%rbp), %r12
	movq	%rcx, -4944(%rbp)
	movq	%rsi, -5016(%rbp)
	movq	%rdx, -4968(%rbp)
	movq	%rcx, -200(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%rdx, -184(%rbp)
	movq	%rax, -5008(%rbp)
	movq	%rax, -208(%rbp)
	movq	%r12, -176(%rbp)
	movq	$0, -4736(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-208(%rbp), %xmm7
	movdqa	-192(%rbp), %xmm3
	movq	-4976(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -4752(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -4736(%rbp)
	movq	%rdx, -4744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1160
	call	_ZdlPv@PLT
.L1160:
	movq	-5008(%rbp), %xmm0
	movl	$40, %edi
	movq	%r12, -176(%rbp)
	movq	$0, -4736(%rbp)
	movhps	-4944(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-5016(%rbp), %xmm0
	movhps	-4968(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -4752(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rcx
	movdqa	-208(%rbp), %xmm4
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm6
	leaq	40(%rax), %rdx
	movq	%rax, -4752(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm6, 16(%rax)
	leaq	-784(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4736(%rbp)
	movq	%rdx, -4744(%rbp)
	movq	%rax, -5016(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1161
	call	_ZdlPv@PLT
.L1161:
	movq	-5192(%rbp), %rcx
	movq	-4992(%rbp), %rdx
	movq	%r15, %rdi
	movq	-4960(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1159
	.p2align 4,,10
	.p2align 3
.L1357:
	movq	-4992(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4824(%rbp)
	movq	$0, -4816(%rbp)
	movq	$0, -4808(%rbp)
	movq	$0, -4800(%rbp)
	movq	$0, -4784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-4976(%rbp), %rdi
	leaq	-4784(%rbp), %r9
	leaq	-4808(%rbp), %rcx
	leaq	-4800(%rbp), %r8
	leaq	-4816(%rbp), %rdx
	leaq	-4824(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESE_
	movl	$74, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$8, %esi
	movq	%r15, %rdi
	movq	%rax, -4944(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4816(%rbp), %r8
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%r8, -4960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-4960(%rbp), %r8
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	%r8, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$46, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -4960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$37, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4784(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	-4960(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -4968(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4968(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-208(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -4968(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-88(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	-4784(%rbp), %xmm1
	movq	-4944(%rbp), %rax
	movq	%r14, %rdi
	movq	%rdx, -5280(%rbp)
	movq	-4808(%rbp), %xmm6
	movaps	%xmm0, -4752(%rbp)
	movq	-4824(%rbp), %xmm7
	movdqa	%xmm1, %xmm5
	movdqa	%xmm1, %xmm4
	movq	-4816(%rbp), %xmm3
	movq	%rax, -160(%rbp)
	movq	-4960(%rbp), %rax
	movhps	-4816(%rbp), %xmm5
	punpcklqdq	%xmm4, %xmm4
	movhps	-4800(%rbp), %xmm6
	movhps	-4816(%rbp), %xmm7
	movhps	-4944(%rbp), %xmm3
	movq	%xmm1, -104(%rbp)
	movq	%xmm1, -96(%rbp)
	movq	%xmm1, -5296(%rbp)
	movq	%rax, -152(%rbp)
	movq	%rax, -112(%rbp)
	movaps	%xmm5, -5248(%rbp)
	movaps	%xmm6, -4992(%rbp)
	movaps	%xmm7, -5056(%rbp)
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm3, -5040(%rbp)
	movaps	%xmm4, -5264(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movq	$0, -4736(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5008(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4752(%rbp), %rdi
	movq	-5280(%rbp), %rdx
	movq	-5296(%rbp), %xmm1
	testq	%rdi, %rdi
	je	.L1163
	movq	%rdx, -5296(%rbp)
	movq	%xmm1, -5280(%rbp)
	call	_ZdlPv@PLT
	movq	-5296(%rbp), %rdx
	movq	-5280(%rbp), %xmm1
.L1163:
	movq	-4944(%rbp), %xmm0
	movdqa	-5248(%rbp), %xmm3
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqa	-5056(%rbp), %xmm5
	movdqa	-4992(%rbp), %xmm7
	movq	%xmm1, -96(%rbp)
	movaps	%xmm3, -176(%rbp)
	movq	-4960(%rbp), %xmm3
	movdqa	-5264(%rbp), %xmm4
	movdqa	-5040(%rbp), %xmm6
	movaps	%xmm5, -208(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm0, -160(%rbp)
	movdqa	%xmm3, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm0, -4752(%rbp)
	movq	$0, -4736(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3856(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -5040(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1164
	call	_ZdlPv@PLT
.L1164:
	movq	-5088(%rbp), %rcx
	movq	-5072(%rbp), %rdx
	movq	%r15, %rdi
	movq	-4968(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1162
	.p2align 4,,10
	.p2align 3
.L1359:
	movq	-5088(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4904(%rbp)
	movq	$0, -4896(%rbp)
	movq	$0, -4888(%rbp)
	movq	$0, -4880(%rbp)
	movq	$0, -4872(%rbp)
	movq	$0, -4864(%rbp)
	movq	$0, -4856(%rbp)
	movq	$0, -4848(%rbp)
	movq	$0, -4840(%rbp)
	movq	$0, -4832(%rbp)
	movq	$0, -4824(%rbp)
	movq	$0, -4816(%rbp)
	movq	$0, -4808(%rbp)
	movq	$0, -4800(%rbp)
	movq	$0, -4784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4784(%rbp), %rax
	movq	-5040(%rbp), %rdi
	leaq	-4888(%rbp), %rcx
	pushq	%rax
	leaq	-4800(%rbp), %rax
	leaq	-4872(%rbp), %r9
	pushq	%rax
	leaq	-4808(%rbp), %rax
	leaq	-4880(%rbp), %r8
	pushq	%rax
	leaq	-4816(%rbp), %rax
	leaq	-4896(%rbp), %rdx
	pushq	%rax
	leaq	-4824(%rbp), %rax
	leaq	-4904(%rbp), %rsi
	pushq	%rax
	leaq	-4832(%rbp), %rax
	pushq	%rax
	leaq	-4840(%rbp), %rax
	pushq	%rax
	leaq	-4848(%rbp), %rax
	pushq	%rax
	leaq	-4856(%rbp), %rax
	pushq	%rax
	leaq	-4864(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_SD_SF_SF_SF_SF_PNS9_IS7_EESF_SF_SF_SF_
	addq	$80, %rsp
	movl	$41, %edx
	movq	%r15, %rdi
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-208(%rbp), %rsi
	leaq	-128(%rbp), %rdx
	movq	%r14, %rdi
	movq	-4840(%rbp), %xmm0
	movq	-4856(%rbp), %xmm1
	movq	$0, -4736(%rbp)
	movq	-4872(%rbp), %xmm2
	movq	-4888(%rbp), %xmm3
	movq	-4904(%rbp), %xmm4
	movhps	-4832(%rbp), %xmm0
	movhps	-4848(%rbp), %xmm1
	movhps	-4864(%rbp), %xmm2
	movhps	-4880(%rbp), %xmm3
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-4896(%rbp), %xmm4
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -4752(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4968(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1168
	call	_ZdlPv@PLT
.L1168:
	movq	-5000(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1167
	.p2align 4,,10
	.p2align 3
.L1358:
	movq	-5072(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4904(%rbp)
	movq	$0, -4896(%rbp)
	movq	$0, -4888(%rbp)
	movq	$0, -4880(%rbp)
	movq	$0, -4872(%rbp)
	movq	$0, -4864(%rbp)
	movq	$0, -4856(%rbp)
	movq	$0, -4848(%rbp)
	movq	$0, -4840(%rbp)
	movq	$0, -4832(%rbp)
	movq	$0, -4824(%rbp)
	movq	$0, -4816(%rbp)
	movq	$0, -4808(%rbp)
	movq	$0, -4800(%rbp)
	movq	$0, -4784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4784(%rbp), %rax
	movq	-5008(%rbp), %rdi
	leaq	-4872(%rbp), %r9
	pushq	%rax
	leaq	-4800(%rbp), %rax
	leaq	-4880(%rbp), %r8
	pushq	%rax
	leaq	-4808(%rbp), %rax
	leaq	-4888(%rbp), %rcx
	pushq	%rax
	leaq	-4816(%rbp), %rax
	leaq	-4896(%rbp), %rdx
	pushq	%rax
	leaq	-4824(%rbp), %rax
	leaq	-4904(%rbp), %rsi
	pushq	%rax
	leaq	-4832(%rbp), %rax
	pushq	%rax
	leaq	-4840(%rbp), %rax
	pushq	%rax
	leaq	-4848(%rbp), %rax
	pushq	%rax
	leaq	-4856(%rbp), %rax
	pushq	%rax
	leaq	-4864(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_S4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EEPNS9_IS6_EESF_SD_SF_SF_SF_SF_PNS9_IS7_EESF_SF_SF_SF_
	addq	$80, %rsp
	movl	$39, %edx
	movq	%r15, %rdi
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4784(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrMulENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4816(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$38, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-4824(%rbp), %rdx
	call	_ZN2v88internal45UnsafeNewReference20UT5ATSmi10HeapObject_1411EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	pxor	%xmm0, %xmm0
	leaq	-72(%rbp), %rdx
	movq	%r14, %rdi
	movq	-4904(%rbp), %rax
	leaq	-208(%rbp), %rsi
	movq	%rax, -208(%rbp)
	movq	-4896(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-4888(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-4880(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-4872(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-4864(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-4856(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-4848(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-4840(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-4832(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-4824(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-4816(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-4808(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-4800(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-4784(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-4752(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-4744(%rbp), %rax
	movaps	%xmm0, -4752(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -4736(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5056(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1166
	call	_ZdlPv@PLT
.L1166:
	movq	-5112(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1165
	.p2align 4,,10
	.p2align 3
.L1362:
	movq	-5024(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$12, %edi
	movq	$0, -4736(%rbp)
	movaps	%xmm0, -4752(%rbp)
	call	_Znwm@PLT
	movq	-4992(%rbp), %rdi
	movq	%r14, %rsi
	movabsq	$361703063247193863, %rcx
	movq	%rcx, (%rax)
	leaq	12(%rax), %rdx
	movl	$84346117, 8(%rax)
	movq	%rax, -4752(%rbp)
	movq	%rdx, -4736(%rbp)
	movq	%rdx, -4744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4752(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L1175
	call	_ZdlPv@PLT
.L1175:
	movq	(%r12), %rax
	leaq	-208(%rbp), %rsi
	leaq	-112(%rbp), %rdx
	movq	%r14, %rdi
	movdqu	64(%rax), %xmm0
	movdqu	(%rax), %xmm4
	movdqu	16(%rax), %xmm3
	movdqu	32(%rax), %xmm2
	movdqu	48(%rax), %xmm1
	movdqu	80(%rax), %xmm5
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -4752(%rbp)
	movq	$0, -4736(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3088(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -5000(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1176
	call	_ZdlPv@PLT
.L1176:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1174
	.p2align 4,,10
	.p2align 3
.L1361:
	movq	-5000(%rbp), %rsi
	movq	%r15, %rdi
	movl	$1285, %r12d
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-208(%rbp), %rsi
	movabsq	$361703063247193863, %rax
	leaq	-198(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, -208(%rbp)
	movaps	%xmm0, -4752(%rbp)
	movw	%r12w, -200(%rbp)
	movq	$0, -4736(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4968(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1173
	call	_ZdlPv@PLT
.L1173:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1360:
	movq	-5112(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-208(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC11(%rip), %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	-191(%rbp), %rdx
	movb	$5, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -4752(%rbp)
	movq	$0, -4736(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5056(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1170
	movq	%rax, -4960(%rbp)
	call	_ZdlPv@PLT
	movq	-4960(%rbp), %rax
.L1170:
	movq	(%rax), %rax
	movq	56(%rax), %r10
	movq	8(%rax), %rsi
	movq	24(%rax), %rdx
	movq	40(%rax), %rdi
	movq	(%rax), %rcx
	movq	64(%rax), %r11
	movq	%r10, -5264(%rbp)
	movq	72(%rax), %r10
	movq	%rsi, -4944(%rbp)
	movq	%rdx, -5072(%rbp)
	movq	16(%rax), %rsi
	movq	32(%rax), %rdx
	movq	%rdi, -5112(%rbp)
	movq	48(%rax), %rdi
	movq	%r10, -5296(%rbp)
	movq	120(%rax), %r10
	movq	128(%rax), %rax
	movq	%rsi, -4992(%rbp)
	leaq	.LC8(%rip), %rsi
	movq	%rdx, -5088(%rbp)
	movl	$46, %edx
	movq	%rdi, -5248(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -4960(%rbp)
	movq	%r11, -5280(%rbp)
	movq	%r10, -5312(%rbp)
	movq	%rax, -5232(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-112(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-4960(%rbp), %xmm0
	movq	$0, -4736(%rbp)
	movhps	-4944(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-4992(%rbp), %xmm0
	movhps	-5072(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-5088(%rbp), %xmm0
	movhps	-5112(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-5248(%rbp), %xmm0
	movhps	-5264(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-5280(%rbp), %xmm0
	movhps	-5296(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-5312(%rbp), %xmm0
	movhps	-5232(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -4752(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3280(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4992(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1171
	call	_ZdlPv@PLT
.L1171:
	movq	-5024(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1363:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movabsq	$361703063247193863, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$12, %edi
	movq	$0, -4736(%rbp)
	movaps	%xmm0, -4752(%rbp)
	call	_Znwm@PLT
	movq	-5000(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	12(%rax), %rdx
	movl	$84346117, 8(%rax)
	movq	%rax, -4752(%rbp)
	movq	%rdx, -4736(%rbp)
	movq	%rdx, -4744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1178
	call	_ZdlPv@PLT
.L1178:
	movq	(%rbx), %rax
	movl	$74, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	8(%rax), %rbx
	movq	24(%rax), %rcx
	movq	88(%rax), %r8
	movq	(%rax), %r12
	movq	%rbx, -4960(%rbp)
	movq	16(%rax), %rbx
	movq	%rcx, -5024(%rbp)
	movq	32(%rax), %rcx
	movq	%r8, -5088(%rbp)
	movq	%rcx, -5072(%rbp)
	movq	%rbx, -4944(%rbp)
	movq	80(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-5088(%rbp), %r8
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6ObjectEvE5valueE(%rip), %esi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17Cast7JSArray_1449EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm7
	pxor	%xmm0, %xmm0
	movq	-5072(%rbp), %xmm5
	movq	-4944(%rbp), %xmm6
	leaq	-4784(%rbp), %rbx
	leaq	-152(%rbp), %rdx
	movq	%rax, -160(%rbp)
	punpcklqdq	%xmm7, %xmm5
	movq	%r12, %xmm7
	leaq	-208(%rbp), %r12
	movq	%rbx, %rdi
	movhps	-5024(%rbp), %xmm6
	movhps	-4960(%rbp), %xmm7
	movq	%r12, %rsi
	movaps	%xmm5, -5088(%rbp)
	movaps	%xmm6, -5072(%rbp)
	movaps	%xmm7, -4960(%rbp)
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm0, -4784(%rbp)
	movq	$0, -4768(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2704(%rbp), %rax
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -5024(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1179
	call	_ZdlPv@PLT
.L1179:
	movq	-5104(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2896(%rbp), %rax
	cmpq	$0, -4744(%rbp)
	movq	%rax, -4944(%rbp)
	jne	.L1378
.L1180:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L1177
	.p2align 4,,10
	.p2align 3
.L1366:
	movq	-5096(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4824(%rbp)
	movq	$0, -4816(%rbp)
	movq	$0, -4808(%rbp)
	movq	$0, -4800(%rbp)
	movq	$0, -4784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-5072(%rbp), %rdi
	leaq	-4808(%rbp), %rcx
	leaq	-4816(%rbp), %rdx
	leaq	-4824(%rbp), %rsi
	leaq	-4784(%rbp), %r9
	leaq	-4800(%rbp), %r8
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESE_
	movq	-4824(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movaps	%xmm0, -4752(%rbp)
	movq	%rax, -208(%rbp)
	movq	-4816(%rbp), %rax
	movq	$0, -4736(%rbp)
	movq	%rax, -200(%rbp)
	movq	-4808(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-4800(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-4784(%rbp), %rax
	movq	%rax, -176(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-208(%rbp), %xmm5
	movdqa	-192(%rbp), %xmm7
	movq	-4960(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -4752(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -4736(%rbp)
	movq	%rdx, -4744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1189
	call	_ZdlPv@PLT
.L1189:
	movq	-5160(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1188
	.p2align 4,,10
	.p2align 3
.L1365:
	movq	-5104(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-208(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	$2053, %r10d
	leaq	-201(%rbp), %rdx
	movaps	%xmm0, -4752(%rbp)
	movw	%r10w, -204(%rbp)
	movl	$117769991, -208(%rbp)
	movb	$7, -202(%rbp)
	movq	$0, -4736(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5024(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1186
	call	_ZdlPv@PLT
.L1186:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	8(%rax), %rdi
	movq	16(%rax), %rsi
	movq	32(%rax), %rdx
	movq	(%rax), %r8
	movq	24(%rax), %rcx
	movq	48(%rax), %rax
	movq	%rdi, -200(%rbp)
	movq	%r14, %rdi
	movq	%rsi, -192(%rbp)
	movq	%r12, %rsi
	movq	%rdx, -176(%rbp)
	leaq	-160(%rbp), %rdx
	movq	%r8, -208(%rbp)
	movq	%rcx, -184(%rbp)
	movq	%rax, -168(%rbp)
	movaps	%xmm0, -4752(%rbp)
	movq	$0, -4736(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2320(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -5088(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1187
	call	_ZdlPv@PLT
.L1187:
	movq	-5120(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1185
	.p2align 4,,10
	.p2align 3
.L1364:
	movq	-5224(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$2053, %r11d
	pxor	%xmm0, %xmm0
	leaq	-208(%rbp), %rsi
	leaq	-202(%rbp), %rdx
	movq	%r14, %rdi
	movw	%r11w, -204(%rbp)
	movaps	%xmm0, -4752(%rbp)
	movl	$117769991, -208(%rbp)
	movq	$0, -4736(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4944(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1183
	call	_ZdlPv@PLT
.L1183:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	32(%rax), %rdx
	movdqu	(%rax), %xmm3
	movq	$0, -4736(%rbp)
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -176(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm0, -4752(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rcx
	movdqa	-208(%rbp), %xmm4
	movq	%r14, %rsi
	movdqa	-192(%rbp), %xmm6
	leaq	40(%rax), %rdx
	movq	%rax, -4752(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm6, 16(%rax)
	leaq	-2512(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4736(%rbp)
	movq	%rdx, -4744(%rbp)
	movq	%rax, -5072(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1184
	call	_ZdlPv@PLT
.L1184:
	movq	-5096(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1182
	.p2align 4,,10
	.p2align 3
.L1367:
	movq	-5120(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-208(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	$1797, %r9d
	leaq	-202(%rbp), %rdx
	movaps	%xmm0, -4752(%rbp)
	movw	%r9w, -204(%rbp)
	movl	$117769991, -208(%rbp)
	movq	$0, -4736(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5088(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1191
	call	_ZdlPv@PLT
.L1191:
	movq	(%rbx), %rax
	movl	$79, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	%rcx, -5120(%rbp)
	movq	16(%rax), %rcx
	movq	%rbx, -5112(%rbp)
	movq	%rcx, -5248(%rbp)
	movq	24(%rax), %rcx
	movq	%rcx, -5224(%rbp)
	movq	32(%rax), %rcx
	movq	40(%rax), %rax
	movq	%rcx, -5096(%rbp)
	movq	%rax, -5264(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	leaq	-4800(%rbp), %rbx
	call	_ZN2v88internal19GetReflectApply_227EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEE@PLT
	movq	%r13, %rdi
	movq	%rax, -5296(%rbp)
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movq	%rax, -5104(%rbp)
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$78, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -5280(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5104(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19IsUndefinedConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	je	.L1192
.L1194:
	movq	-5280(%rbp), %xmm0
	movq	%rbx, %rdi
	movq	-5104(%rbp), %xmm1
	movhps	-5264(%rbp), %xmm0
	movhps	-5224(%rbp), %xmm1
	movaps	%xmm1, -5312(%rbp)
	movaps	%xmm0, -5280(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$3, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-4752(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, -5104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-5312(%rbp), %xmm1
	movq	-5296(%rbp), %xmm2
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movdqa	-5280(%rbp), %xmm0
	movq	%rax, -4784(%rbp)
	movq	-4736(%rbp), %rax
	movhps	-5104(%rbp), %xmm2
	movaps	%xmm2, -208(%rbp)
	movq	%rax, -4776(%rbp)
	movaps	%xmm1, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
.L1354:
	movl	$6, %edi
	movq	-5112(%rbp), %r9
	movl	$1, %ecx
	xorl	%esi, %esi
	pushq	%rdi
	leaq	-4784(%rbp), %rdx
	movq	%rbx, %rdi
	pushq	%r12
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	popq	%rdi
	movq	%rbx, %rdi
	popq	%r8
	movq	-5248(%rbp), %xmm4
	movq	%rax, -5296(%rbp)
	movq	-5112(%rbp), %xmm3
	movq	-5096(%rbp), %xmm5
	movhps	-5224(%rbp), %xmm4
	movhps	-5120(%rbp), %xmm3
	movhps	-5264(%rbp), %xmm5
	movaps	%xmm4, -5248(%rbp)
	movaps	%xmm3, -5280(%rbp)
	movaps	%xmm5, -5264(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$83, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movl	$8, %esi
	movq	%r15, %rdi
	movq	%rax, -5224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-5120(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3SmiEvE5valueE(%rip), %esi
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$46, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -5112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$37, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-5096(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	-5112(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal30Convert9ATuintptr8ATintptr_203EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7IntPtrTEEE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, -5104(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-5104(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15UintPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-5224(%rbp), %rax
	movq	%r12, %rsi
	movq	-5120(%rbp), %xmm7
	movq	-5296(%rbp), %xmm6
	leaq	-72(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-5280(%rbp), %xmm3
	movq	%rax, -144(%rbp)
	movq	-5112(%rbp), %rax
	movq	%r14, %rdi
	movdqa	-5248(%rbp), %xmm4
	movdqa	-5264(%rbp), %xmm5
	punpcklqdq	%xmm7, %xmm6
	movq	%rdx, -5312(%rbp)
	movq	%rax, -136(%rbp)
	movq	-5096(%rbp), %rax
	movaps	%xmm6, -5296(%rbp)
	movq	%rax, -128(%rbp)
	movq	%rax, -120(%rbp)
	movq	-5224(%rbp), %rax
	movaps	%xmm3, -208(%rbp)
	movq	%rax, -104(%rbp)
	movq	-5112(%rbp), %rax
	movaps	%xmm4, -192(%rbp)
	movq	%rax, -96(%rbp)
	movq	-5096(%rbp), %rax
	movaps	%xmm5, -176(%rbp)
	movq	%rax, -88(%rbp)
	movq	%rax, -80(%rbp)
	movaps	%xmm6, -160(%rbp)
	movq	%xmm7, -112(%rbp)
	movaps	%xmm0, -4752(%rbp)
	movq	$0, -4736(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2128(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -5104(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4752(%rbp), %rdi
	movq	-5312(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L1195
	call	_ZdlPv@PLT
	movq	-5312(%rbp), %rdx
.L1195:
	movdqa	-5296(%rbp), %xmm5
	movq	%r12, %rsi
	movq	%r14, %rdi
	movdqa	-5248(%rbp), %xmm4
	movdqa	-5280(%rbp), %xmm3
	movq	-5096(%rbp), %xmm7
	movq	$0, -4736(%rbp)
	movaps	%xmm5, -160(%rbp)
	movdqa	-5264(%rbp), %xmm6
	movq	-5224(%rbp), %xmm5
	movaps	%xmm4, -192(%rbp)
	movq	-5112(%rbp), %xmm4
	movdqa	%xmm5, %xmm0
	movaps	%xmm3, -208(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm0, -144(%rbp)
	movdqa	%xmm7, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movq	%xmm7, -80(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	-5120(%rbp), %xmm0
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -112(%rbp)
	movdqa	%xmm4, %xmm0
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -4752(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1936(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -5112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1196
	call	_ZdlPv@PLT
.L1196:
	movq	-5184(%rbp), %rcx
	movq	-5128(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1190
	.p2align 4,,10
	.p2align 3
.L1372:
	movq	-5168(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$14, %edi
	movq	$0, -4736(%rbp)
	movaps	%xmm0, -4752(%rbp)
	call	_Znwm@PLT
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movabsq	$506662676253181703, %rcx
	movq	%rcx, (%rax)
	movl	$1287, %ecx
	leaq	14(%rax), %rdx
	movl	$84215045, 8(%rax)
	movw	%cx, 12(%rax)
	movq	%rax, -4752(%rbp)
	movq	%rdx, -4736(%rbp)
	movq	%rdx, -4744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4752(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L1207
	call	_ZdlPv@PLT
.L1207:
	movq	(%r12), %rax
	leaq	-208(%rbp), %rsi
	leaq	-96(%rbp), %rdx
	movq	%r14, %rdi
	movdqu	80(%rax), %xmm0
	movdqu	(%rax), %xmm5
	movdqu	16(%rax), %xmm4
	movdqu	32(%rax), %xmm3
	movdqu	48(%rax), %xmm2
	movdqu	64(%rax), %xmm1
	movdqu	96(%rax), %xmm7
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -4752(%rbp)
	movq	$0, -4736(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1168(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -5096(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1208
	call	_ZdlPv@PLT
.L1208:
	movq	-5176(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1104(%rbp)
	je	.L1209
.L1373:
	movq	-5176(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4896(%rbp)
	movq	$0, -4888(%rbp)
	movq	$0, -4880(%rbp)
	movq	$0, -4872(%rbp)
	movq	$0, -4864(%rbp)
	movq	$0, -4856(%rbp)
	movq	$0, -4848(%rbp)
	movq	$0, -4840(%rbp)
	movq	$0, -4832(%rbp)
	movq	$0, -4824(%rbp)
	movq	$0, -4816(%rbp)
	movq	$0, -4808(%rbp)
	movq	$0, -4800(%rbp)
	movq	$0, -4784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-4784(%rbp), %rax
	movq	-5096(%rbp), %rdi
	pushq	%rax
	leaq	-4800(%rbp), %rax
	leaq	-4880(%rbp), %rcx
	pushq	%rax
	leaq	-4808(%rbp), %rax
	leaq	-4872(%rbp), %r8
	pushq	%rax
	leaq	-4816(%rbp), %rax
	leaq	-4864(%rbp), %r9
	pushq	%rax
	leaq	-4824(%rbp), %rax
	leaq	-4888(%rbp), %rdx
	pushq	%rax
	leaq	-4832(%rbp), %rax
	leaq	-4896(%rbp), %rsi
	pushq	%rax
	leaq	-4840(%rbp), %rax
	pushq	%rax
	leaq	-4848(%rbp), %rax
	pushq	%rax
	leaq	-4856(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_NS0_7JSArrayENS0_6ObjectES4_S5_S5_S5_S5_NS0_10HeapObjectES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESH_PNSB_IS7_EEPNSB_IS8_EESF_SH_SH_SH_SH_PNSB_IS9_EESH_
	addq	$80, %rsp
	movl	$83, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4848(%rbp), %rdx
	movq	-4896(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ToString_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -5176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4784(%rbp), %r9
	movq	%r14, %rdi
	movl	$1, %esi
	movq	-4800(%rbp), %r12
	movq	%r9, -5168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-5168(%rbp), %r9
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r9, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r12, %rdx
	movl	$7, %esi
	movq	%r14, %rdi
	movq	-5176(%rbp), %r8
	movq	%rax, %rcx
	movl	$2, %r9d
	call	_ZN2v88internal8compiler13CodeAssembler13StoreToObjectENS0_21MachineRepresentationENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEEPNS1_4NodeENS0_25StoreToObjectWriteBarrierE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$72, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4896(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movaps	%xmm0, -4752(%rbp)
	movq	%rax, -208(%rbp)
	movq	-4888(%rbp), %rax
	movq	$0, -4736(%rbp)
	movq	%rax, -200(%rbp)
	movq	-4880(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-4872(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-4864(%rbp), %rax
	movq	%rax, -176(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-208(%rbp), %xmm2
	movdqa	-192(%rbp), %xmm3
	movq	-4960(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -4752(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movq	%rdx, -4736(%rbp)
	movq	%rdx, -4744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1210
	call	_ZdlPv@PLT
.L1210:
	movq	-5160(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -912(%rbp)
	je	.L1211
.L1374:
	movq	-5160(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4824(%rbp)
	movq	$0, -4816(%rbp)
	movq	$0, -4808(%rbp)
	movq	$0, -4800(%rbp)
	movq	$0, -4784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-4960(%rbp), %rdi
	leaq	-4808(%rbp), %rcx
	leaq	-4784(%rbp), %r9
	leaq	-4800(%rbp), %r8
	leaq	-4816(%rbp), %rdx
	leaq	-4824(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESE_
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4784(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$40, %edi
	movq	-4808(%rbp), %xmm0
	movq	-4824(%rbp), %xmm1
	movq	%r12, -176(%rbp)
	movhps	-4800(%rbp), %xmm0
	movhps	-4816(%rbp), %xmm1
	movq	$0, -4736(%rbp)
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -4752(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-208(%rbp), %xmm4
	movdqa	-192(%rbp), %xmm6
	movq	-4936(%rbp), %rdi
	leaq	40(%rax), %rdx
	movq	%rax, -4752(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -4736(%rbp)
	movq	%rdx, -4744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1212
	call	_ZdlPv@PLT
.L1212:
	movq	-5152(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1371:
	movq	-5216(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-208(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-196(%rbp), %rdx
	movabsq	$506662676253181703, %rax
	movaps	%xmm0, -4752(%rbp)
	movq	%rax, -208(%rbp)
	movl	$84215045, -200(%rbp)
	movq	$0, -4736(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5128(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1205
	call	_ZdlPv@PLT
.L1205:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L1204
	.p2align 4,,10
	.p2align 3
.L1370:
	movq	-5200(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-208(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC10(%rip), %xmm0
	movl	$1797, %esi
	movq	%r14, %rdi
	movw	%si, -192(%rbp)
	movq	%r12, %rsi
	leaq	-189(%rbp), %rdx
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -4752(%rbp)
	movb	$5, -190(%rbp)
	movq	$0, -4736(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5120(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1202
	call	_ZdlPv@PLT
.L1202:
	movq	(%rbx), %rax
	movq	56(%rax), %rdi
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	(%rax), %rbx
	movq	8(%rax), %rcx
	movq	%rdi, -5296(%rbp)
	movq	72(%rax), %rdi
	movq	88(%rax), %r11
	movq	%rsi, -5224(%rbp)
	movq	%rdx, -5264(%rbp)
	movq	32(%rax), %rsi
	movq	48(%rax), %rdx
	movq	%rdi, -5312(%rbp)
	movq	80(%rax), %rdi
	movq	%rbx, -5096(%rbp)
	movq	%rcx, -5184(%rbp)
	movq	64(%rax), %rbx
	movq	16(%rax), %rcx
	movq	%r11, -5320(%rbp)
	movq	136(%rax), %r11
	movq	144(%rax), %rax
	movq	%rsi, -5248(%rbp)
	leaq	.LC8(%rip), %rsi
	movq	%rdx, -5280(%rbp)
	movl	$46, %edx
	movq	%rdi, -5232(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -5200(%rbp)
	movq	%r11, -5328(%rbp)
	movq	%rax, -5336(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-96(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	-5096(%rbp), %xmm0
	movq	$0, -4736(%rbp)
	movhps	-5184(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-5200(%rbp), %xmm0
	movhps	-5224(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-5248(%rbp), %xmm0
	movhps	-5264(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-5280(%rbp), %xmm0
	movhps	-5296(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%rbx, %xmm0
	leaq	-1360(%rbp), %rbx
	movhps	-5312(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-5232(%rbp), %xmm0
	movhps	-5320(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-5328(%rbp), %xmm0
	movhps	-5336(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -4752(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1203
	call	_ZdlPv@PLT
.L1203:
	movq	-5168(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1369:
	movq	-5184(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4920(%rbp)
	movq	$0, -4912(%rbp)
	movq	$0, -4904(%rbp)
	movq	$0, -4896(%rbp)
	movq	$0, -4888(%rbp)
	movq	$0, -4880(%rbp)
	movq	$0, -4872(%rbp)
	movq	$0, -4864(%rbp)
	movq	$0, -4856(%rbp)
	movq	$0, -4848(%rbp)
	movq	$0, -4840(%rbp)
	movq	$0, -4832(%rbp)
	movq	$0, -4824(%rbp)
	movq	$0, -4816(%rbp)
	movq	$0, -4808(%rbp)
	movq	$0, -4800(%rbp)
	movq	$0, -4784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4784(%rbp), %rax
	movq	-5112(%rbp), %rdi
	leaq	-4904(%rbp), %rcx
	pushq	%rax
	leaq	-4800(%rbp), %rax
	leaq	-4888(%rbp), %r9
	pushq	%rax
	leaq	-4808(%rbp), %rax
	leaq	-4896(%rbp), %r8
	pushq	%rax
	leaq	-4816(%rbp), %rax
	leaq	-4912(%rbp), %rdx
	pushq	%rax
	leaq	-4824(%rbp), %rax
	leaq	-4920(%rbp), %rsi
	pushq	%rax
	leaq	-4832(%rbp), %rax
	pushq	%rax
	leaq	-4840(%rbp), %rax
	pushq	%rax
	leaq	-4848(%rbp), %rax
	pushq	%rax
	leaq	-4856(%rbp), %rax
	pushq	%rax
	leaq	-4864(%rbp), %rax
	pushq	%rax
	leaq	-4872(%rbp), %rax
	pushq	%rax
	leaq	-4880(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_NS0_7JSArrayENS0_6ObjectES4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESH_PNSB_IS7_EEPNSB_IS8_EESF_SH_SH_SH_SH_PNSB_IS9_EESH_SH_SH_SH_
	addq	$96, %rsp
	movl	$41, %edx
	movq	%r15, %rdi
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-208(%rbp), %rsi
	leaq	-112(%rbp), %rdx
	movq	%r14, %rdi
	movq	-4840(%rbp), %xmm0
	movq	-4856(%rbp), %xmm1
	movq	$0, -4736(%rbp)
	movq	-4872(%rbp), %xmm2
	movq	-4888(%rbp), %xmm3
	movq	-4904(%rbp), %xmm4
	movhps	-4832(%rbp), %xmm0
	movq	-4920(%rbp), %xmm5
	movhps	-4848(%rbp), %xmm1
	movhps	-4864(%rbp), %xmm2
	movhps	-4880(%rbp), %xmm3
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-4896(%rbp), %xmm4
	movhps	-4912(%rbp), %xmm5
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -4752(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5128(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1200
	call	_ZdlPv@PLT
.L1200:
	movq	-5216(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1368:
	movq	-5128(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4920(%rbp)
	movq	$0, -4912(%rbp)
	movq	$0, -4904(%rbp)
	movq	$0, -4896(%rbp)
	movq	$0, -4888(%rbp)
	movq	$0, -4880(%rbp)
	movq	$0, -4872(%rbp)
	movq	$0, -4864(%rbp)
	movq	$0, -4856(%rbp)
	movq	$0, -4848(%rbp)
	movq	$0, -4840(%rbp)
	movq	$0, -4832(%rbp)
	movq	$0, -4824(%rbp)
	movq	$0, -4816(%rbp)
	movq	$0, -4808(%rbp)
	movq	$0, -4800(%rbp)
	movq	$0, -4784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-4784(%rbp), %rax
	movq	-5104(%rbp), %rdi
	leaq	-4888(%rbp), %r9
	pushq	%rax
	leaq	-4800(%rbp), %rax
	leaq	-4896(%rbp), %r8
	pushq	%rax
	leaq	-4808(%rbp), %rax
	leaq	-4904(%rbp), %rcx
	pushq	%rax
	leaq	-4816(%rbp), %rax
	leaq	-4912(%rbp), %rdx
	pushq	%rax
	leaq	-4824(%rbp), %rax
	leaq	-4920(%rbp), %rsi
	pushq	%rax
	leaq	-4832(%rbp), %rax
	pushq	%rax
	leaq	-4840(%rbp), %rax
	pushq	%rax
	leaq	-4848(%rbp), %rax
	pushq	%rax
	leaq	-4856(%rbp), %rax
	pushq	%rax
	leaq	-4864(%rbp), %rax
	pushq	%rax
	leaq	-4872(%rbp), %rax
	pushq	%rax
	leaq	-4880(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_NS0_7JSArrayENS0_6ObjectES4_S5_S5_S5_S5_NS0_10HeapObjectES5_S5_S5_S5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EESH_PNSB_IS7_EEPNSB_IS8_EESF_SH_SH_SH_SH_PNSB_IS9_EESH_SH_SH_SH_
	addq	$96, %rsp
	movl	$39, %edx
	movq	%r15, %rdi
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal32SizeOf20UT5ATSmi10HeapObject_339EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4784(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrMulENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4816(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$38, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-4824(%rbp), %rdx
	call	_ZN2v88internal45UnsafeNewReference20UT5ATSmi10HeapObject_1411EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	pxor	%xmm0, %xmm0
	leaq	-56(%rbp), %rdx
	movq	%r14, %rdi
	movq	-4920(%rbp), %rax
	leaq	-208(%rbp), %rsi
	movq	%rax, -208(%rbp)
	movq	-4912(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-4904(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-4896(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-4888(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-4880(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-4872(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-4864(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-4856(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-4848(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-4840(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-4832(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-4824(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-4816(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-4808(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-4800(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-4784(%rbp), %rax
	movq	$0, -4736(%rbp)
	movq	%rax, -80(%rbp)
	movq	-4752(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-4744(%rbp), %rax
	movaps	%xmm0, -4752(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-5120(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1198
	call	_ZdlPv@PLT
.L1198:
	movq	-5200(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1197
	.p2align 4,,10
	.p2align 3
.L1376:
	movq	-5208(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -4736(%rbp)
	movaps	%xmm0, -4752(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	$117769991, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -4752(%rbp)
	movq	%rdx, -4736(%rbp)
	movq	%rdx, -4744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-4752(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L1216
	call	_ZdlPv@PLT
.L1216:
	movq	(%r12), %rax
	leaq	-208(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-176(%rbp), %rdx
	leaq	-400(%rbp), %r12
	movdqu	(%rax), %xmm0
	movdqu	16(%rax), %xmm5
	movq	$0, -4736(%rbp)
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -192(%rbp)
	movaps	%xmm0, -4752(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1217
	call	_ZdlPv@PLT
.L1217:
	movq	-5136(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1215
	.p2align 4,,10
	.p2align 3
.L1375:
	movq	-5192(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -4824(%rbp)
	movq	$0, -4816(%rbp)
	movq	$0, -4808(%rbp)
	movq	$0, -4800(%rbp)
	movq	$0, -4784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-5016(%rbp), %rdi
	leaq	-4808(%rbp), %rcx
	leaq	-4784(%rbp), %r9
	leaq	-4800(%rbp), %r8
	leaq	-4816(%rbp), %rdx
	leaq	-4824(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10FixedArrayENS0_7IntPtrTENS0_10JSReceiverES5_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS8_IS4_EEPNS8_IS5_EEPNS8_IS6_EESE_
	movl	$69, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-208(%rbp), %rsi
	leaq	-176(%rbp), %rdx
	movq	%r14, %rdi
	movq	-4808(%rbp), %xmm0
	movq	-4824(%rbp), %xmm1
	movq	$0, -4736(%rbp)
	movhps	-4800(%rbp), %xmm0
	movhps	-4816(%rbp), %xmm1
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -4752(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1214
	call	_ZdlPv@PLT
.L1214:
	movq	-5208(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1213
	.p2align 4,,10
	.p2align 3
.L1192:
	movq	-5104(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IsNullConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L1194
	movq	-5280(%rbp), %xmm1
	movq	%rbx, %rdi
	movq	-5104(%rbp), %xmm0
	movhps	-5264(%rbp), %xmm1
	movhps	-5224(%rbp), %xmm0
	movaps	%xmm0, -5312(%rbp)
	movaps	%xmm1, -5280(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$3, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-4752(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%rax, -5104(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-5312(%rbp), %xmm0
	movq	-5296(%rbp), %xmm2
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movdqa	-5280(%rbp), %xmm1
	movq	%rax, -4784(%rbp)
	movq	-4736(%rbp), %rax
	movhps	-5104(%rbp), %xmm2
	movaps	%xmm2, -208(%rbp)
	movq	%rax, -4776(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm1, -176(%rbp)
	jmp	.L1354
	.p2align 4,,10
	.p2align 3
.L1378:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movdqa	-4960(%rbp), %xmm7
	movdqa	-5088(%rbp), %xmm3
	leaq	-160(%rbp), %rdx
	movaps	%xmm0, -4784(%rbp)
	movq	$0, -4768(%rbp)
	movaps	%xmm7, -208(%rbp)
	movdqa	-5072(%rbp), %xmm7
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm7, -192(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4944(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-4784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1181
	call	_ZdlPv@PLT
.L1181:
	movq	-5224(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1180
.L1377:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22482:
	.size	_ZN2v88internal45RegExpReplaceCallableWithExplicitCaptures_323EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_7IntPtrTEEENS4_INS0_10JSReceiverEEE, .-_ZN2v88internal45RegExpReplaceCallableWithExplicitCaptures_323EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_7IntPtrTEEENS4_INS0_10JSReceiverEEE
	.section	.text._ZN2v88internal35RegExpReplaceFastGlobalCallable_324EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal35RegExpReplaceFastGlobalCallable_324EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE
	.type	_ZN2v88internal35RegExpReplaceFastGlobalCallable_324EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE, @function
_ZN2v88internal35RegExpReplaceFastGlobalCallable_324EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE:
.LFB22522:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-2592(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2464(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-2632(%rbp), %r12
	pushq	%rbx
	subq	$2840, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -2752(%rbp)
	movq	%rdx, -2768(%rbp)
	movq	%rcx, -2784(%rbp)
	movq	%r8, -2800(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -2632(%rbp)
	movq	%rdi, -2464(%rbp)
	movl	$96, %edi
	movq	$0, -2456(%rbp)
	movq	$0, -2448(%rbp)
	movq	$0, -2440(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -2456(%rbp)
	leaq	-2408(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2440(%rbp)
	movq	%rdx, -2448(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2424(%rbp)
	movq	%rax, -2648(%rbp)
	movq	$0, -2432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2632(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2264(%rbp)
	movq	$0, -2256(%rbp)
	movq	%rax, -2272(%rbp)
	movq	$0, -2248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -2264(%rbp)
	leaq	-2216(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2248(%rbp)
	movq	%rdx, -2256(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2232(%rbp)
	movq	%rax, -2680(%rbp)
	movq	$0, -2240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2632(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2072(%rbp)
	movq	$0, -2064(%rbp)
	movq	%rax, -2080(%rbp)
	movq	$0, -2056(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -2072(%rbp)
	leaq	-2024(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2056(%rbp)
	movq	%rdx, -2064(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2040(%rbp)
	movq	%rax, -2688(%rbp)
	movq	$0, -2048(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2632(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1880(%rbp)
	movq	$0, -1872(%rbp)
	movq	%rax, -1888(%rbp)
	movq	$0, -1864(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1880(%rbp)
	leaq	-1832(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1864(%rbp)
	movq	%rdx, -1872(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1848(%rbp)
	movq	%rax, -2736(%rbp)
	movq	$0, -1856(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2632(%rbp), %rax
	movl	$240, %edi
	movq	$0, -1688(%rbp)
	movq	$0, -1680(%rbp)
	movq	%rax, -1696(%rbp)
	movq	$0, -1672(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -1688(%rbp)
	leaq	-1640(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1672(%rbp)
	movq	%rdx, -1680(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1656(%rbp)
	movq	%rax, -2696(%rbp)
	movq	$0, -1664(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2632(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1496(%rbp)
	movq	$0, -1488(%rbp)
	movq	%rax, -1504(%rbp)
	movq	$0, -1480(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1496(%rbp)
	leaq	-1448(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1480(%rbp)
	movq	%rdx, -1488(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1464(%rbp)
	movq	%rax, -2704(%rbp)
	movq	$0, -1472(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2632(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1304(%rbp)
	movq	$0, -1296(%rbp)
	movq	%rax, -1312(%rbp)
	movq	$0, -1288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1304(%rbp)
	leaq	-1256(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1288(%rbp)
	movq	%rdx, -1296(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1272(%rbp)
	movq	%rax, -2712(%rbp)
	movq	$0, -1280(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2632(%rbp), %rax
	movl	$288, %edi
	movq	$0, -1112(%rbp)
	movq	$0, -1104(%rbp)
	movq	%rax, -1120(%rbp)
	movq	$0, -1096(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -1112(%rbp)
	leaq	-1064(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1096(%rbp)
	movq	%rdx, -1104(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1080(%rbp)
	movq	%rax, -2720(%rbp)
	movq	$0, -1088(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2632(%rbp), %rax
	movl	$288, %edi
	movq	$0, -920(%rbp)
	movq	$0, -912(%rbp)
	movq	%rax, -928(%rbp)
	movq	$0, -904(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -920(%rbp)
	leaq	-872(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -904(%rbp)
	movq	%rdx, -912(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -888(%rbp)
	movq	%rax, -2728(%rbp)
	movq	$0, -896(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2632(%rbp), %rax
	movl	$288, %edi
	movq	$0, -728(%rbp)
	movq	$0, -720(%rbp)
	movq	%rax, -736(%rbp)
	movq	$0, -712(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -728(%rbp)
	leaq	-680(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -712(%rbp)
	movq	%rdx, -720(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -696(%rbp)
	movq	%rax, -2664(%rbp)
	movq	$0, -704(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2632(%rbp), %rax
	movl	$120, %edi
	movq	$0, -536(%rbp)
	movq	$0, -528(%rbp)
	movq	%rax, -544(%rbp)
	movq	$0, -520(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -536(%rbp)
	leaq	-488(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -520(%rbp)
	movq	%rdx, -528(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -504(%rbp)
	movq	%rax, -2672(%rbp)
	movq	$0, -512(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2632(%rbp), %rax
	movl	$120, %edi
	movq	$0, -344(%rbp)
	movq	$0, -336(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -328(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -344(%rbp)
	leaq	-296(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -328(%rbp)
	movq	%rdx, -336(%rbp)
	xorl	%edx, %edx
	movq	%rax, -2656(%rbp)
	movups	%xmm0, -312(%rbp)
	movq	$0, -320(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-2784(%rbp), %xmm1
	movq	-2752(%rbp), %xmm2
	movaps	%xmm0, -2592(%rbp)
	movhps	-2800(%rbp), %xmm1
	movq	$0, -2576(%rbp)
	movhps	-2768(%rbp), %xmm2
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -160(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm3
	movq	%r13, %rdi
	movq	%r14, %rsi
	movdqa	-144(%rbp), %xmm4
	leaq	32(%rax), %rdx
	movq	%rax, -2592(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2576(%rbp)
	movq	%rdx, -2584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1380
	call	_ZdlPv@PLT
.L1380:
	movq	-2648(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2400(%rbp)
	jne	.L1655
	cmpq	$0, -2208(%rbp)
	jne	.L1656
.L1385:
	cmpq	$0, -2016(%rbp)
	jne	.L1657
.L1388:
	cmpq	$0, -1824(%rbp)
	jne	.L1658
.L1393:
	cmpq	$0, -1632(%rbp)
	jne	.L1659
.L1396:
	cmpq	$0, -1440(%rbp)
	jne	.L1660
.L1399:
	cmpq	$0, -1248(%rbp)
	jne	.L1661
.L1401:
	cmpq	$0, -1056(%rbp)
	jne	.L1662
.L1405:
	cmpq	$0, -864(%rbp)
	jne	.L1663
.L1408:
	cmpq	$0, -672(%rbp)
	jne	.L1664
.L1411:
	cmpq	$0, -480(%rbp)
	leaq	-352(%rbp), %r13
	jne	.L1665
.L1414:
	movq	-2656(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2576(%rbp)
	movaps	%xmm0, -2592(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	$117901063, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2592(%rbp)
	movq	%rdx, -2576(%rbp)
	movq	%rdx, -2584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2592(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1417
	call	_ZdlPv@PLT
.L1417:
	movq	(%rbx), %rax
	movq	-2656(%rbp), %rdi
	movq	32(%rax), %r13
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-320(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1418
	call	_ZdlPv@PLT
.L1418:
	movq	-336(%rbp), %rbx
	movq	-344(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1419
	.p2align 4,,10
	.p2align 3
.L1423:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1420
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1423
.L1421:
	movq	-344(%rbp), %r14
.L1419:
	testq	%r14, %r14
	je	.L1424
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1424:
	movq	-2672(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-512(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1425
	call	_ZdlPv@PLT
.L1425:
	movq	-528(%rbp), %rbx
	movq	-536(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1426
	.p2align 4,,10
	.p2align 3
.L1430:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1427
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1430
.L1428:
	movq	-536(%rbp), %r14
.L1426:
	testq	%r14, %r14
	je	.L1431
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1431:
	movq	-2664(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1432
	call	_ZdlPv@PLT
.L1432:
	movq	-720(%rbp), %rbx
	movq	-728(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1433
	.p2align 4,,10
	.p2align 3
.L1437:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1434
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1437
.L1435:
	movq	-728(%rbp), %r14
.L1433:
	testq	%r14, %r14
	je	.L1438
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1438:
	movq	-2728(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-896(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1439
	call	_ZdlPv@PLT
.L1439:
	movq	-912(%rbp), %rbx
	movq	-920(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1440
	.p2align 4,,10
	.p2align 3
.L1444:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1441
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1444
.L1442:
	movq	-920(%rbp), %r14
.L1440:
	testq	%r14, %r14
	je	.L1445
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1445:
	movq	-2720(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1446
	call	_ZdlPv@PLT
.L1446:
	movq	-1104(%rbp), %rbx
	movq	-1112(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1447
	.p2align 4,,10
	.p2align 3
.L1451:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1448
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1451
.L1449:
	movq	-1112(%rbp), %r14
.L1447:
	testq	%r14, %r14
	je	.L1452
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1452:
	movq	-2712(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1453
	call	_ZdlPv@PLT
.L1453:
	movq	-1296(%rbp), %rbx
	movq	-1304(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1454
	.p2align 4,,10
	.p2align 3
.L1458:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1455
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1458
.L1456:
	movq	-1304(%rbp), %r14
.L1454:
	testq	%r14, %r14
	je	.L1459
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1459:
	movq	-2704(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1472(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1460
	call	_ZdlPv@PLT
.L1460:
	movq	-1488(%rbp), %rbx
	movq	-1496(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1461
	.p2align 4,,10
	.p2align 3
.L1465:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1462
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1465
.L1463:
	movq	-1496(%rbp), %r14
.L1461:
	testq	%r14, %r14
	je	.L1466
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1466:
	movq	-2696(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1467
	call	_ZdlPv@PLT
.L1467:
	movq	-1680(%rbp), %rbx
	movq	-1688(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1468
	.p2align 4,,10
	.p2align 3
.L1472:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1469
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1472
.L1470:
	movq	-1688(%rbp), %r14
.L1468:
	testq	%r14, %r14
	je	.L1473
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1473:
	movq	-2736(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1856(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1474
	call	_ZdlPv@PLT
.L1474:
	movq	-1872(%rbp), %rbx
	movq	-1880(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1475
	.p2align 4,,10
	.p2align 3
.L1479:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1476
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%rbx, %r14
	jne	.L1479
.L1477:
	movq	-1880(%rbp), %r14
.L1475:
	testq	%r14, %r14
	je	.L1480
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1480:
	movq	-2688(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2048(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1481
	call	_ZdlPv@PLT
.L1481:
	movq	-2064(%rbp), %rbx
	movq	-2072(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1482
	.p2align 4,,10
	.p2align 3
.L1486:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1483
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%rbx, %r14
	jne	.L1486
.L1484:
	movq	-2072(%rbp), %r14
.L1482:
	testq	%r14, %r14
	je	.L1487
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1487:
	movq	-2680(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1488
	call	_ZdlPv@PLT
.L1488:
	movq	-2256(%rbp), %rbx
	movq	-2264(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1489
	.p2align 4,,10
	.p2align 3
.L1493:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1490
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1493
.L1491:
	movq	-2264(%rbp), %r14
.L1489:
	testq	%r14, %r14
	je	.L1494
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1494:
	movq	-2648(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1495
	call	_ZdlPv@PLT
.L1495:
	movq	-2448(%rbp), %rbx
	movq	-2456(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L1496
	.p2align 4,,10
	.p2align 3
.L1500:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1497
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%rbx, %r14
	jne	.L1500
.L1498:
	movq	-2456(%rbp), %r14
.L1496:
	testq	%r14, %r14
	je	.L1501
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1501:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1666
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1497:
	.cfi_restore_state
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1500
	jmp	.L1498
	.p2align 4,,10
	.p2align 3
.L1490:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1493
	jmp	.L1491
	.p2align 4,,10
	.p2align 3
.L1483:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1486
	jmp	.L1484
	.p2align 4,,10
	.p2align 3
.L1476:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1479
	jmp	.L1477
	.p2align 4,,10
	.p2align 3
.L1469:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1472
	jmp	.L1470
	.p2align 4,,10
	.p2align 3
.L1462:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1465
	jmp	.L1463
	.p2align 4,,10
	.p2align 3
.L1455:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1458
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1448:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1451
	jmp	.L1449
	.p2align 4,,10
	.p2align 3
.L1441:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1444
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L1434:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1437
	jmp	.L1435
	.p2align 4,,10
	.p2align 3
.L1420:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1423
	jmp	.L1421
	.p2align 4,,10
	.p2align 3
.L1427:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L1430
	jmp	.L1428
	.p2align 4,,10
	.p2align 3
.L1655:
	movq	-2648(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2576(%rbp)
	movaps	%xmm0, -2592(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	$117901063, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2592(%rbp)
	movq	%rdx, -2576(%rbp)
	movq	%rdx, -2584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2592(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1382
	call	_ZdlPv@PLT
.L1382:
	movq	(%rbx), %rax
	movl	$90, %edx
	movq	%r12, %rdi
	movq	16(%rax), %rsi
	movq	8(%rax), %rcx
	movq	(%rax), %r13
	movq	24(%rax), %rax
	movq	%rsi, -2768(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -2752(%rbp)
	movq	%rax, -2832(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2752(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal23RegExpBuiltinsAssembler18FastStoreLastIndexENS0_8compiler5TNodeINS0_8JSRegExpEEENS3_INS0_3SmiEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$92, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$93, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -2800(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$95, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -2784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal26GetRegExpLastMatchInfo_228EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEE@PLT
	movl	$97, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -2840(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal35GetFastPackedElementsJSArrayMap_222EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEE@PLT
	movl	$98, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$96, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	pushq	$0
	xorl	%r9d, %r9d
	movq	%rbx, %rdx
	pushq	$0
	movq	-2784(%rbp), %r8
	movl	$2, %esi
	movq	%r14, %rdi
	movq	-2800(%rbp), %rcx
	call	_ZN2v88internal17CodeStubAssembler15AllocateJSArrayENS0_12ElementsKindENS0_8compiler5TNodeINS0_3MapEEEPNS3_4NodeENS4_INS0_3SmiEEES8_NS1_13ParameterModeENS_4base5FlagsINS1_14AllocationFlagEiEE@PLT
	movq	%r14, %rdi
	movq	%rax, -2816(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$94, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdx
	movl	$293, %esi
	movq	%r14, %rdi
	movq	-2752(%rbp), %xmm0
	movl	$4, %r8d
	leaq	-160(%rbp), %rcx
	movhps	-2768(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-2840(%rbp), %xmm0
	movhps	-2816(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$100, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, -2816(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2816(%rbp), %rdx
	movq	-2752(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23RegExpBuiltinsAssembler18FastStoreLastIndexENS0_8compiler5TNodeINS0_8JSRegExpEEENS3_INS0_3SmiEEE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$103, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal7Null_63EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, -2816(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-2816(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -2816(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %xmm7
	pxor	%xmm0, %xmm0
	movq	-2768(%rbp), %xmm6
	movhps	-2752(%rbp), %xmm7
	movl	$56, %edi
	movq	%rbx, -112(%rbp)
	movq	-2800(%rbp), %xmm5
	movhps	-2832(%rbp), %xmm6
	movaps	%xmm7, -2752(%rbp)
	movhps	-2784(%rbp), %xmm5
	movaps	%xmm6, -2768(%rbp)
	movaps	%xmm5, -2784(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm0, -2592(%rbp)
	movq	$0, -2576(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm7
	movq	-112(%rbp), %rcx
	movq	%r14, %rsi
	leaq	56(%rax), %rdx
	leaq	-2272(%rbp), %rdi
	movq	%rax, -2592(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-144(%rbp), %xmm7
	movq	%rcx, 48(%rax)
	movups	%xmm7, 16(%rax)
	movdqa	-128(%rbp), %xmm7
	movq	%rdx, -2576(%rbp)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -2584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2592(%rbp), %rdi
	popq	%r8
	popq	%r9
	testq	%rdi, %rdi
	je	.L1383
	call	_ZdlPv@PLT
.L1383:
	movdqa	-2752(%rbp), %xmm2
	movdqa	-2768(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movdqa	-2784(%rbp), %xmm3
	movq	%rbx, -112(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm0, -2592(%rbp)
	movq	$0, -2576(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movdqa	-128(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-160(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm2
	leaq	56(%rax), %rdx
	leaq	-2080(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rax, -2592(%rbp)
	movq	%rdx, -2576(%rbp)
	movq	%rdx, -2584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1384
	call	_ZdlPv@PLT
.L1384:
	movq	-2688(%rbp), %rcx
	movq	-2680(%rbp), %rdx
	movq	%r12, %rdi
	movq	-2816(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2208(%rbp)
	je	.L1385
.L1656:
	movq	-2680(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2272(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2576(%rbp)
	movaps	%xmm0, -2592(%rbp)
	call	_Znwm@PLT
	movl	$1542, %edi
	movq	%r14, %rsi
	movw	%di, 4(%rax)
	leaq	7(%rax), %rdx
	movq	%r13, %rdi
	movl	$117901063, (%rax)
	movb	$7, 6(%rax)
	movq	%rax, -2592(%rbp)
	movq	%rdx, -2576(%rbp)
	movq	%rdx, -2584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2592(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1386
	call	_ZdlPv@PLT
.L1386:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	16(%rax), %rdx
	movdqu	(%rax), %xmm3
	movq	$0, -2576(%rbp)
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -128(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm0, -2592(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-160(%rbp), %xmm4
	movdqa	-144(%rbp), %xmm2
	leaq	40(%rax), %rdx
	leaq	-544(%rbp), %rdi
	movq	%rax, -2592(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -2576(%rbp)
	movq	%rdx, -2584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1387
	call	_ZdlPv@PLT
.L1387:
	movq	-2672(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2016(%rbp)
	je	.L1388
.L1657:
	movq	-2688(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2080(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2576(%rbp)
	movaps	%xmm0, -2592(%rbp)
	call	_Znwm@PLT
	movl	$1542, %esi
	movq	%r13, %rdi
	movw	%si, 4(%rax)
	leaq	7(%rax), %rdx
	movq	%r14, %rsi
	movl	$117901063, (%rax)
	movb	$7, 6(%rax)
	movq	%rax, -2592(%rbp)
	movq	%rdx, -2576(%rbp)
	movq	%rdx, -2584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2592(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1389
	call	_ZdlPv@PLT
.L1389:
	movq	(%rbx), %rax
	movl	$105, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	24(%rax), %rsi
	movq	(%rax), %r13
	movq	40(%rax), %rbx
	movq	%rcx, -2768(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -2800(%rbp)
	movq	32(%rax), %rsi
	movq	48(%rax), %rax
	movq	%rbx, -2832(%rbp)
	movq	%rsi, -2816(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rbx
	movq	%rcx, -2784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rbx, -2840(%rbp)
	call	_ZN2v88internal23UnsafeCast7JSArray_1407EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$106, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -2752(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-2752(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE(%rip), %esi
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal13Cast5ATSmi_83EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %xmm6
	pxor	%xmm0, %xmm0
	movq	-2816(%rbp), %xmm4
	movhps	-2768(%rbp), %xmm6
	movl	$80, %edi
	movaps	%xmm0, -2624(%rbp)
	movq	-2784(%rbp), %xmm5
	movq	-2840(%rbp), %xmm3
	movhps	-2832(%rbp), %xmm4
	movq	%rbx, -96(%rbp)
	leaq	-2624(%rbp), %r13
	movhps	-2800(%rbp), %xmm5
	movaps	%xmm4, -2816(%rbp)
	movhps	-2752(%rbp), %xmm3
	movaps	%xmm5, -2784(%rbp)
	movaps	%xmm3, -2752(%rbp)
	movaps	%xmm6, -2768(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movq	%rax, -88(%rbp)
	movq	$0, -2608(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm6
	leaq	80(%rax), %rdx
	leaq	-1696(%rbp), %rdi
	movdqa	-96(%rbp), %xmm4
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm4, 64(%rax)
	movq	%rax, -2624(%rbp)
	movq	%rdx, -2608(%rbp)
	movq	%rdx, -2616(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1390
	call	_ZdlPv@PLT
.L1390:
	movq	-2696(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2584(%rbp)
	jne	.L1667
.L1391:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1824(%rbp)
	je	.L1393
.L1658:
	movq	-2736(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1888(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -2576(%rbp)
	movaps	%xmm0, -2592(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movabsq	$506380106059941639, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -2592(%rbp)
	movq	%rdx, -2576(%rbp)
	movq	%rdx, -2584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2592(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1394
	call	_ZdlPv@PLT
.L1394:
	movq	(%rbx), %rax
	movl	$64, %edi
	movdqu	48(%rax), %xmm0
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movdqu	(%rax), %xmm5
	movq	$0, -2576(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -2592(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm6
	leaq	64(%rax), %rdx
	leaq	-1504(%rbp), %rdi
	movups	%xmm2, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rax, -2592(%rbp)
	movq	%rdx, -2576(%rbp)
	movq	%rdx, -2584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1395
	call	_ZdlPv@PLT
.L1395:
	movq	-2704(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1632(%rbp)
	je	.L1396
.L1659:
	movq	-2696(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1696(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -2576(%rbp)
	movaps	%xmm0, -2592(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movabsq	$506380106059941639, %rcx
	movq	%rcx, (%rax)
	movl	$1544, %ecx
	leaq	10(%rax), %rdx
	movw	%cx, 8(%rax)
	movq	%rax, -2592(%rbp)
	movq	%rdx, -2576(%rbp)
	movq	%rdx, -2584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2592(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1397
	call	_ZdlPv@PLT
.L1397:
	movq	(%rbx), %rax
	movl	$72, %edi
	movdqu	48(%rax), %xmm0
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	72(%rax), %rdx
	movdqu	(%rax), %xmm4
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -2592(%rbp)
	movq	$0, -2576(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm6
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm2
	leaq	72(%rax), %rdx
	leaq	-1312(%rbp), %rdi
	movdqa	-144(%rbp), %xmm5
	movq	%rcx, 64(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rax, -2592(%rbp)
	movq	%rdx, -2576(%rbp)
	movq	%rdx, -2584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1398
	call	_ZdlPv@PLT
.L1398:
	movq	-2712(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1440(%rbp)
	je	.L1399
.L1660:
	movq	-2704(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1504(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2576(%rbp)
	movaps	%xmm0, -2592(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movabsq	$506380106059941639, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2592(%rbp)
	movq	%rdx, -2576(%rbp)
	movq	%rdx, -2584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1400
	call	_ZdlPv@PLT
.L1400:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -1248(%rbp)
	je	.L1401
.L1661:
	movq	-2712(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1312(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -2576(%rbp)
	movaps	%xmm0, -2592(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movabsq	$506380106059941639, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$6, 8(%rax)
	movq	%rax, -2592(%rbp)
	movq	%rdx, -2576(%rbp)
	movq	%rdx, -2584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2592(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1402
	call	_ZdlPv@PLT
.L1402:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	(%rax), %rbx
	movq	%rcx, -2784(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -2816(%rbp)
	movq	32(%rax), %rsi
	movq	%rcx, -2800(%rbp)
	movq	56(%rax), %rcx
	movq	%rdx, -2840(%rbp)
	movq	48(%rax), %rdx
	movq	64(%rax), %rax
	movq	%rsi, -2832(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -2848(%rbp)
	movl	$107, %edx
	movq	%rax, %r13
	movq	%rcx, -2752(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r13, -2864(%rbp)
	call	_ZN2v88internal26Convert8ATintptr5ATSmi_186EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_3SmiEEE@PLT
	movl	$109, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -2880(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-2752(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE(%rip), %esi
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal27UnsafeCast10FixedArray_1409EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$108, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -2872(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$112, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal26GetRegExpLastMatchInfo_228EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal40_method_RegExpMatchInfo_NumberOfCapturesEPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_15RegExpMatchInfoEEE@PLT
	movl	$117, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -2768(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$2, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-2768(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm1
	pxor	%xmm0, %xmm0
	movq	-2864(%rbp), %xmm3
	movq	-2832(%rbp), %xmm5
	movhps	-2784(%rbp), %xmm1
	movq	-2800(%rbp), %xmm6
	movl	$96, %edi
	movq	-2872(%rbp), %xmm7
	movhps	-2880(%rbp), %xmm3
	movq	-2848(%rbp), %xmm4
	movaps	%xmm1, -2784(%rbp)
	movhps	-2840(%rbp), %xmm5
	movhps	-2816(%rbp), %xmm6
	movaps	%xmm3, -96(%rbp)
	movhps	-2768(%rbp), %xmm7
	movhps	-2752(%rbp), %xmm4
	movaps	%xmm3, -2864(%rbp)
	movaps	%xmm7, -2768(%rbp)
	movaps	%xmm4, -2752(%rbp)
	movaps	%xmm5, -2832(%rbp)
	movaps	%xmm6, -2800(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm7, -80(%rbp)
	movaps	%xmm0, -2592(%rbp)
	movq	$0, -2576(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm4
	movdqa	-128(%rbp), %xmm1
	leaq	-1120(%rbp), %rdi
	movdqa	-144(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm2
	leaq	96(%rax), %rdx
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm6
	movups	%xmm4, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm5, 64(%rax)
	movups	%xmm6, 80(%rax)
	movq	%rax, -2592(%rbp)
	movq	%rdx, -2576(%rbp)
	movq	%rdx, -2584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1403
	call	_ZdlPv@PLT
.L1403:
	movdqa	-2784(%rbp), %xmm7
	movdqa	-2800(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movl	$96, %edi
	movdqa	-2832(%rbp), %xmm3
	movdqa	-2752(%rbp), %xmm1
	movaps	%xmm0, -2592(%rbp)
	movdqa	-2864(%rbp), %xmm2
	movdqa	-2768(%rbp), %xmm5
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movq	$0, -2576(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm4
	leaq	-928(%rbp), %rdi
	movdqa	-144(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm3
	leaq	96(%rax), %rdx
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm1
	movdqa	-80(%rbp), %xmm2
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm1, 64(%rax)
	movups	%xmm2, 80(%rax)
	movq	%rax, -2592(%rbp)
	movq	%rdx, -2576(%rbp)
	movq	%rdx, -2584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1404
	call	_ZdlPv@PLT
.L1404:
	movq	-2728(%rbp), %rcx
	movq	-2720(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1056(%rbp)
	je	.L1405
.L1662:
	movq	-2720(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1120(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$12, %edi
	movq	$0, -2576(%rbp)
	movaps	%xmm0, -2592(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movabsq	$506380106059941639, %rcx
	movq	%rcx, (%rax)
	leaq	12(%rax), %rdx
	movl	$101123334, 8(%rax)
	movq	%rax, -2592(%rbp)
	movq	%rdx, -2576(%rbp)
	movq	%rdx, -2584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2592(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1406
	call	_ZdlPv@PLT
.L1406:
	movq	(%rbx), %rax
	movq	32(%rax), %rbx
	movq	8(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	(%rax), %rcx
	movq	16(%rax), %r13
	movq	%rbx, -2816(%rbp)
	movq	72(%rax), %rbx
	movq	%rsi, -2800(%rbp)
	movq	%rdx, -2832(%rbp)
	movq	24(%rax), %rsi
	movq	48(%rax), %rdx
	movq	%rdi, -2848(%rbp)
	movq	64(%rax), %rdi
	movq	%rbx, -2784(%rbp)
	movq	80(%rax), %rbx
	movq	88(%rax), %rax
	movq	%rsi, -2768(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -2840(%rbp)
	movl	$119, %edx
	movq	%rdi, -2864(%rbp)
	movq	%r12, %rdi
	movq	%rax, -2872(%rbp)
	movq	%rcx, -2752(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$118, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %r8
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movq	-2768(%rbp), %r9
	movq	-2784(%rbp), %rcx
	movq	-2752(%rbp), %rsi
	call	_ZN2v88internal43RegExpReplaceCallableNoExplicitCaptures_322EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_7IntPtrTEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE
	movl	$117, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$96, %edi
	movq	-2752(%rbp), %xmm0
	movq	$0, -2576(%rbp)
	movhps	-2800(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%r13, %xmm0
	movhps	-2768(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-2816(%rbp), %xmm0
	movhps	-2832(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-2840(%rbp), %xmm0
	movhps	-2848(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-2864(%rbp), %xmm0
	movhps	-2784(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	movhps	-2872(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2592(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm7
	leaq	-736(%rbp), %rdi
	movdqa	-144(%rbp), %xmm6
	movdqa	-112(%rbp), %xmm4
	leaq	96(%rax), %rdx
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm1
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm3, 64(%rax)
	movups	%xmm1, 80(%rax)
	movq	%rax, -2592(%rbp)
	movq	%rdx, -2576(%rbp)
	movq	%rdx, -2584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1407
	call	_ZdlPv@PLT
.L1407:
	movq	-2664(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -864(%rbp)
	je	.L1408
.L1663:
	movq	-2728(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-928(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$12, %edi
	movq	$0, -2576(%rbp)
	movaps	%xmm0, -2592(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movabsq	$506380106059941639, %rcx
	movq	%rcx, (%rax)
	leaq	12(%rax), %rdx
	movl	$101123334, 8(%rax)
	movq	%rax, -2592(%rbp)
	movq	%rdx, -2576(%rbp)
	movq	%rdx, -2584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2592(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1409
	call	_ZdlPv@PLT
.L1409:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	40(%rax), %rbx
	movq	32(%rax), %rsi
	movq	56(%rax), %rdx
	movq	%rcx, -2784(%rbp)
	movq	16(%rax), %rcx
	movq	%rbx, -2832(%rbp)
	movq	48(%rax), %rbx
	movq	%rsi, -2816(%rbp)
	movq	72(%rax), %rsi
	movq	%rdx, -2848(%rbp)
	movq	64(%rax), %rdx
	movq	(%rax), %r13
	movq	%rcx, -2800(%rbp)
	movq	%rbx, -2840(%rbp)
	movq	24(%rax), %rcx
	movq	80(%rax), %rbx
	movq	88(%rax), %rax
	movq	%rdx, -2864(%rbp)
	movl	$122, %edx
	movq	%rsi, -2768(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rax, -2872(%rbp)
	movq	%rcx, -2752(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$121, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	-2752(%rbp), %r8
	movq	-2768(%rbp), %rcx
	call	_ZN2v88internal45RegExpReplaceCallableWithExplicitCaptures_323EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_7IntPtrTEEENS4_INS0_10JSReceiverEEE
	movl	$117, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %xmm0
	movl	$96, %edi
	movq	$0, -2576(%rbp)
	movhps	-2784(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-2800(%rbp), %xmm0
	movhps	-2752(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-2816(%rbp), %xmm0
	movhps	-2832(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-2840(%rbp), %xmm0
	movhps	-2848(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-2864(%rbp), %xmm0
	movhps	-2768(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	movhps	-2872(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2592(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm6
	leaq	-736(%rbp), %rdi
	movdqa	-144(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm7
	leaq	96(%rax), %rdx
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm3
	movups	%xmm2, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm3, 80(%rax)
	movq	%rax, -2592(%rbp)
	movq	%rdx, -2576(%rbp)
	movq	%rdx, -2584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1410
	call	_ZdlPv@PLT
.L1410:
	movq	-2664(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -672(%rbp)
	je	.L1411
.L1664:
	movq	-2664(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-736(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$12, %edi
	movq	$0, -2576(%rbp)
	movaps	%xmm0, -2592(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movabsq	$506380106059941639, %rcx
	movq	%rcx, (%rax)
	leaq	12(%rax), %rdx
	movl	$101123334, 8(%rax)
	movq	%rax, -2592(%rbp)
	movq	%rdx, -2576(%rbp)
	movq	%rdx, -2584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2592(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1412
	call	_ZdlPv@PLT
.L1412:
	movq	(%rbx), %rax
	movl	$125, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r13
	movq	16(%rax), %rbx
	movq	%rcx, -2752(%rbp)
	movq	24(%rax), %rcx
	movq	%rcx, -2768(%rbp)
	movq	56(%rax), %rcx
	movq	64(%rax), %rax
	movq	%rcx, -2784(%rbp)
	movq	%rax, -2800(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdx
	movl	$326, %esi
	movq	%r14, %rdi
	movq	-2784(%rbp), %xmm0
	leaq	-160(%rbp), %rcx
	movl	$3, %r8d
	movq	%rbx, -144(%rbp)
	movhps	-2800(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %xmm0
	movl	$40, %edi
	movq	%r15, -128(%rbp)
	movhps	-2752(%rbp), %xmm0
	movq	$0, -2576(%rbp)
	movaps	%xmm0, -160(%rbp)
	movq	%rbx, %xmm0
	movhps	-2768(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2592(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r14, %rsi
	movdqa	-160(%rbp), %xmm1
	movdqa	-144(%rbp), %xmm2
	leaq	40(%rax), %rdx
	leaq	-544(%rbp), %rdi
	movq	%rax, -2592(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm1, (%rax)
	movups	%xmm2, 16(%rax)
	movq	%rdx, -2576(%rbp)
	movq	%rdx, -2584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1413
	call	_ZdlPv@PLT
.L1413:
	movq	-2672(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1411
	.p2align 4,,10
	.p2align 3
.L1665:
	movq	-2672(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-544(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2576(%rbp)
	movaps	%xmm0, -2592(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	$117901063, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -2592(%rbp)
	movq	%rdx, -2576(%rbp)
	movq	%rdx, -2584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2592(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1415
	call	_ZdlPv@PLT
.L1415:
	movq	(%rbx), %rax
	movl	$87, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r13
	movq	16(%rax), %rbx
	movq	32(%rax), %r15
	movq	%rcx, -2752(%rbp)
	movq	24(%rax), %rcx
	movq	%rcx, -2768(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %xmm0
	movl	$40, %edi
	movq	%r15, -128(%rbp)
	movhps	-2752(%rbp), %xmm0
	leaq	-352(%rbp), %r13
	movq	$0, -2576(%rbp)
	movaps	%xmm0, -160(%rbp)
	movq	%rbx, %xmm0
	movhps	-2768(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2592(%rbp)
	call	_Znwm@PLT
	movq	-128(%rbp), %rcx
	movq	%r13, %rdi
	movq	%r14, %rsi
	movdqa	-160(%rbp), %xmm5
	movdqa	-144(%rbp), %xmm6
	leaq	40(%rax), %rdx
	movq	%rax, -2592(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -2576(%rbp)
	movq	%rdx, -2584(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1416
	call	_ZdlPv@PLT
.L1416:
	movq	-2656(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1414
	.p2align 4,,10
	.p2align 3
.L1667:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-88(%rbp), %rdx
	movq	%r13, %rdi
	movdqa	-2768(%rbp), %xmm7
	movdqa	-2784(%rbp), %xmm4
	leaq	-160(%rbp), %rsi
	movaps	%xmm0, -2624(%rbp)
	movdqa	-2816(%rbp), %xmm3
	movq	%rbx, -96(%rbp)
	movaps	%xmm7, -160(%rbp)
	movdqa	-2752(%rbp), %xmm7
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movq	$0, -2608(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1888(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1392
	call	_ZdlPv@PLT
.L1392:
	movq	-2736(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1391
.L1666:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22522:
	.size	_ZN2v88internal35RegExpReplaceFastGlobalCallable_324EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE, .-_ZN2v88internal35RegExpReplaceFastGlobalCallable_324EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_8JSRegExpENS0_6StringES5_S5_NS0_3SmiENS0_5BoolTES6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_PNS9_IS6_EEPNS9_IS7_EESH_SJ_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_8JSRegExpENS0_6StringES5_S5_NS0_3SmiENS0_5BoolTES6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_PNS9_IS6_EEPNS9_IS7_EESH_SJ_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_8JSRegExpENS0_6StringES5_S5_NS0_3SmiENS0_5BoolTES6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_PNS9_IS6_EEPNS9_IS7_EESH_SJ_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_8JSRegExpENS0_6StringES5_S5_NS0_3SmiENS0_5BoolTES6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_PNS9_IS6_EEPNS9_IS7_EESH_SJ_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_8JSRegExpENS0_6StringES5_S5_NS0_3SmiENS0_5BoolTES6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_PNS9_IS6_EEPNS9_IS7_EESH_SJ_:
.LFB27286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$9, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$433478091386849031, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$4, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1669
	movq	%rax, -128(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
.L1669:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1670
	movq	%rdx, (%r15)
.L1670:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1671
	movq	%rdx, (%r14)
.L1671:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1672
	movq	%rdx, 0(%r13)
.L1672:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1673
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1673:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1674
	movq	%rdx, (%rbx)
.L1674:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1675
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1675:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1676
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1676:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1677
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1677:
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L1668
	movq	-120(%rbp), %rbx
	movq	%rax, (%rbx)
.L1668:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1711
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1711:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27286:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_8JSRegExpENS0_6StringES5_S5_NS0_3SmiENS0_5BoolTES6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_PNS9_IS6_EEPNS9_IS7_EESH_SJ_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_8JSRegExpENS0_6StringES5_S5_NS0_3SmiENS0_5BoolTES6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_PNS9_IS6_EEPNS9_IS7_EESH_SJ_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_8JSRegExpENS0_6StringES5_S5_NS0_3SmiENS0_5BoolTES6_S7_NS0_15RegExpMatchInfoES6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EESG_SG_PNSA_IS6_EEPNSA_IS7_EESI_SK_PNSA_IS8_EESI_SI_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_8JSRegExpENS0_6StringES5_S5_NS0_3SmiENS0_5BoolTES6_S7_NS0_15RegExpMatchInfoES6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EESG_SG_PNSA_IS6_EEPNSA_IS7_EESI_SK_PNSA_IS8_EESI_SI_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_8JSRegExpENS0_6StringES5_S5_NS0_3SmiENS0_5BoolTES6_S7_NS0_15RegExpMatchInfoES6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EESG_SG_PNSA_IS6_EEPNSA_IS7_EESI_SK_PNSA_IS8_EESI_SI_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_8JSRegExpENS0_6StringES5_S5_NS0_3SmiENS0_5BoolTES6_S7_NS0_15RegExpMatchInfoES6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EESG_SG_PNSA_IS6_EEPNSA_IS7_EESI_SK_PNSA_IS8_EESI_SI_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_8JSRegExpENS0_6StringES5_S5_NS0_3SmiENS0_5BoolTES6_S7_NS0_15RegExpMatchInfoES6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EESG_SG_PNSA_IS6_EEPNSA_IS7_EESI_SK_PNSA_IS8_EESI_SI_:
.LFB27295:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$12, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$120, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$433478091386849031, %rcx
	movq	%rcx, (%rax)
	leaq	12(%rax), %rdx
	movl	$101058308, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1713
	movq	%rax, -152(%rbp)
	call	_ZdlPv@PLT
	movq	-152(%rbp), %rax
.L1713:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1714
	movq	%rdx, (%r15)
.L1714:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1715
	movq	%rdx, (%r14)
.L1715:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1716
	movq	%rdx, 0(%r13)
.L1716:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1717
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1717:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1718
	movq	%rdx, (%rbx)
.L1718:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1719
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1719:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1720
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1720:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1721
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1721:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1722
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L1722:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1723
	movq	-128(%rbp), %rsi
	movq	%rdx, (%rsi)
.L1723:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1724
	movq	-136(%rbp), %rcx
	movq	%rdx, (%rcx)
.L1724:
	movq	88(%rax), %rax
	testq	%rax, %rax
	je	.L1712
	movq	-144(%rbp), %rbx
	movq	%rax, (%rbx)
.L1712:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1767
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1767:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27295:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_8JSRegExpENS0_6StringES5_S5_NS0_3SmiENS0_5BoolTES6_S7_NS0_15RegExpMatchInfoES6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EESG_SG_PNSA_IS6_EEPNSA_IS7_EESI_SK_PNSA_IS8_EESI_SI_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_8JSRegExpENS0_6StringES5_S5_NS0_3SmiENS0_5BoolTES6_S7_NS0_15RegExpMatchInfoES6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EESG_SG_PNSA_IS6_EEPNSA_IS7_EESI_SK_PNSA_IS8_EESI_SI_
	.section	.text._ZN2v88internal27RegExpReplaceFastString_325EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEESA_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27RegExpReplaceFastString_325EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEESA_
	.type	_ZN2v88internal27RegExpReplaceFastString_325EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEESA_, @function
_ZN2v88internal27RegExpReplaceFastString_325EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEESA_:
.LFB22547:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -3936(%rbp)
	movq	%rdi, %r14
	leaq	-3880(%rbp), %r12
	leaq	-3176(%rbp), %rbx
	movq	%rdx, -3952(%rbp)
	leaq	-3616(%rbp), %r13
	leaq	-3744(%rbp), %r15
	movq	%rcx, -3968(%rbp)
	movq	%r8, -3976(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -3880(%rbp)
	movq	%rdi, -3616(%rbp)
	movl	$96, %edi
	movq	$0, -3608(%rbp)
	movq	$0, -3600(%rbp)
	movq	$0, -3592(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -3608(%rbp)
	leaq	-3560(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3592(%rbp)
	movq	%rdx, -3600(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3576(%rbp)
	movq	%rax, -3896(%rbp)
	movq	$0, -3584(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3880(%rbp), %rax
	movl	$216, %edi
	movq	$0, -3416(%rbp)
	movq	$0, -3408(%rbp)
	movq	%rax, -3424(%rbp)
	movq	$0, -3400(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -3416(%rbp)
	leaq	-3368(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3400(%rbp)
	movq	%rdx, -3408(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3384(%rbp)
	movq	%rax, -4000(%rbp)
	movq	$0, -3392(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3880(%rbp), %rax
	movl	$216, %edi
	movq	$0, -3224(%rbp)
	movq	$0, -3216(%rbp)
	movq	%rax, -3232(%rbp)
	movq	$0, -3208(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -3208(%rbp)
	movq	%rdx, -3216(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3192(%rbp)
	movq	%rax, -3224(%rbp)
	movq	$0, -3200(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3880(%rbp), %rax
	movl	$216, %edi
	movq	$0, -3032(%rbp)
	movq	$0, -3024(%rbp)
	movq	%rax, -3040(%rbp)
	movq	$0, -3016(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -3032(%rbp)
	leaq	-2984(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3016(%rbp)
	movq	%rdx, -3024(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3000(%rbp)
	movq	%rax, -4064(%rbp)
	movq	$0, -3008(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3880(%rbp), %rax
	movl	$216, %edi
	movq	$0, -2840(%rbp)
	movq	$0, -2832(%rbp)
	movq	%rax, -2848(%rbp)
	movq	$0, -2824(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -2840(%rbp)
	leaq	-2792(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2824(%rbp)
	movq	%rdx, -2832(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2808(%rbp)
	movq	%rax, -4032(%rbp)
	movq	$0, -2816(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3880(%rbp), %rax
	movl	$264, %edi
	movq	$0, -2648(%rbp)
	movq	$0, -2640(%rbp)
	movq	%rax, -2656(%rbp)
	movq	$0, -2632(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -2648(%rbp)
	leaq	-2600(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2632(%rbp)
	movq	%rdx, -2640(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2616(%rbp)
	movq	%rax, -4056(%rbp)
	movq	$0, -2624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3880(%rbp), %rax
	movl	$288, %edi
	movq	$0, -2456(%rbp)
	movq	$0, -2448(%rbp)
	movq	%rax, -2464(%rbp)
	movq	$0, -2440(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -2456(%rbp)
	leaq	-2408(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2440(%rbp)
	movq	%rdx, -2448(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2424(%rbp)
	movq	%rax, -3920(%rbp)
	movq	$0, -2432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3880(%rbp), %rax
	movl	$216, %edi
	movq	$0, -2264(%rbp)
	movq	$0, -2256(%rbp)
	movq	%rax, -2272(%rbp)
	movq	$0, -2248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -2264(%rbp)
	leaq	-2216(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2248(%rbp)
	movq	%rdx, -2256(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2232(%rbp)
	movq	%rax, -4048(%rbp)
	movq	$0, -2240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3880(%rbp), %rax
	movl	$240, %edi
	movq	$0, -2072(%rbp)
	movq	$0, -2064(%rbp)
	movq	%rax, -2080(%rbp)
	movq	$0, -2056(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -2072(%rbp)
	leaq	-2024(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2056(%rbp)
	movq	%rdx, -2064(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2040(%rbp)
	movq	%rax, -4112(%rbp)
	movq	$0, -2048(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3880(%rbp), %rax
	movl	$288, %edi
	movq	$0, -1880(%rbp)
	movq	$0, -1872(%rbp)
	movq	%rax, -1888(%rbp)
	movq	$0, -1864(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -1880(%rbp)
	leaq	-1832(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1864(%rbp)
	movq	%rdx, -1872(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1848(%rbp)
	movq	%rax, -3984(%rbp)
	movq	$0, -1856(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3880(%rbp), %rax
	movl	$288, %edi
	movq	$0, -1688(%rbp)
	movq	$0, -1680(%rbp)
	movq	%rax, -1696(%rbp)
	movq	$0, -1672(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -1688(%rbp)
	leaq	-1640(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1672(%rbp)
	movq	%rdx, -1680(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1656(%rbp)
	movq	%rax, -4080(%rbp)
	movq	$0, -1664(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3880(%rbp), %rax
	movl	$288, %edi
	movq	$0, -1496(%rbp)
	movq	$0, -1488(%rbp)
	movq	%rax, -1504(%rbp)
	movq	$0, -1480(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -1496(%rbp)
	leaq	-1448(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1480(%rbp)
	movq	%rdx, -1488(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1464(%rbp)
	movq	%rax, -3992(%rbp)
	movq	$0, -1472(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3880(%rbp), %rax
	movl	$288, %edi
	movq	$0, -1304(%rbp)
	movq	$0, -1296(%rbp)
	movq	%rax, -1312(%rbp)
	movq	$0, -1288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -1304(%rbp)
	leaq	-1256(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1288(%rbp)
	movq	%rdx, -1296(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1272(%rbp)
	movq	%rax, -4128(%rbp)
	movq	$0, -1280(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3880(%rbp), %rax
	movl	$288, %edi
	movq	$0, -1112(%rbp)
	movq	$0, -1104(%rbp)
	movq	%rax, -1120(%rbp)
	movq	$0, -1096(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -1112(%rbp)
	leaq	-1064(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1096(%rbp)
	movq	%rdx, -1104(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1080(%rbp)
	movq	%rax, -4008(%rbp)
	movq	$0, -1088(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3880(%rbp), %rax
	movl	$288, %edi
	movq	$0, -920(%rbp)
	movq	$0, -912(%rbp)
	movq	%rax, -928(%rbp)
	movq	$0, -904(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -920(%rbp)
	leaq	-872(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -904(%rbp)
	movq	%rdx, -912(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -888(%rbp)
	movq	%rax, -4088(%rbp)
	movq	$0, -896(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3880(%rbp), %rax
	movl	$216, %edi
	movq	$0, -728(%rbp)
	movq	$0, -720(%rbp)
	movq	%rax, -736(%rbp)
	movq	$0, -712(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -728(%rbp)
	leaq	-680(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -712(%rbp)
	movq	%rdx, -720(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -696(%rbp)
	movq	%rax, -3912(%rbp)
	movq	$0, -704(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3880(%rbp), %rax
	movl	$120, %edi
	movq	$0, -536(%rbp)
	movq	$0, -528(%rbp)
	movq	%rax, -544(%rbp)
	movq	$0, -520(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -536(%rbp)
	leaq	-488(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -520(%rbp)
	movq	%rdx, -528(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -504(%rbp)
	movq	%rax, -4024(%rbp)
	movq	$0, -512(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3880(%rbp), %rax
	movl	$120, %edi
	movq	$0, -344(%rbp)
	movq	$0, -336(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -328(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -344(%rbp)
	leaq	-296(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -328(%rbp)
	movq	%rdx, -336(%rbp)
	xorl	%edx, %edx
	movq	%rax, -3904(%rbp)
	movups	%xmm0, -312(%rbp)
	movq	$0, -320(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-3968(%rbp), %xmm1
	movq	-3936(%rbp), %xmm2
	movaps	%xmm0, -3744(%rbp)
	movhps	-3976(%rbp), %xmm1
	movq	$0, -3728(%rbp)
	movhps	-3952(%rbp), %xmm2
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -160(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm6
	movq	%r13, %rdi
	movq	%r15, %rsi
	movdqa	-144(%rbp), %xmm7
	leaq	32(%rax), %rdx
	movq	%rax, -3744(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -3728(%rbp)
	movq	%rdx, -3736(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1769
	call	_ZdlPv@PLT
.L1769:
	movq	-3896(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3424(%rbp), %rax
	cmpq	$0, -3552(%rbp)
	movq	%rax, -3976(%rbp)
	leaq	-3232(%rbp), %rax
	movq	%rax, -3936(%rbp)
	jne	.L2038
	cmpq	$0, -3360(%rbp)
	jne	.L2039
.L1774:
	leaq	-3040(%rbp), %rax
	cmpq	$0, -3168(%rbp)
	movq	%rax, -3952(%rbp)
	jne	.L2040
.L1776:
	leaq	-2848(%rbp), %rax
	cmpq	$0, -2976(%rbp)
	movq	%rax, -4016(%rbp)
	jne	.L2041
	cmpq	$0, -2784(%rbp)
	jne	.L2042
.L1781:
	leaq	-2272(%rbp), %rax
	cmpq	$0, -2592(%rbp)
	movq	%rax, -4000(%rbp)
	jne	.L2043
.L1785:
	leaq	-2080(%rbp), %rax
	cmpq	$0, -2400(%rbp)
	movq	%rax, -4032(%rbp)
	jne	.L2044
.L1788:
	cmpq	$0, -2208(%rbp)
	jne	.L2045
.L1791:
	leaq	-1696(%rbp), %rax
	cmpq	$0, -2016(%rbp)
	movq	%rax, -3968(%rbp)
	jne	.L2046
	cmpq	$0, -1824(%rbp)
	jne	.L2047
.L1798:
	leaq	-1312(%rbp), %rax
	cmpq	$0, -1632(%rbp)
	movq	%rax, -4048(%rbp)
	jne	.L2048
	cmpq	$0, -1440(%rbp)
	jne	.L2049
.L1803:
	cmpq	$0, -1248(%rbp)
	leaq	-928(%rbp), %r13
	jne	.L2050
	cmpq	$0, -1056(%rbp)
	jne	.L2051
.L1808:
	cmpq	$0, -864(%rbp)
	jne	.L2052
.L1810:
	cmpq	$0, -672(%rbp)
	jne	.L2053
.L1812:
	cmpq	$0, -480(%rbp)
	leaq	-352(%rbp), %r14
	jne	.L2054
.L1814:
	movq	-3904(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -3728(%rbp)
	movaps	%xmm0, -3744(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	movl	$117901063, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -3744(%rbp)
	movq	%rdx, -3728(%rbp)
	movq	%rdx, -3736(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3744(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1817
	call	_ZdlPv@PLT
.L1817:
	movq	(%rbx), %rax
	movq	-3904(%rbp), %rdi
	movq	32(%rax), %r14
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-320(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1818
	call	_ZdlPv@PLT
.L1818:
	movq	-336(%rbp), %rbx
	movq	-344(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L1819
	.p2align 4,,10
	.p2align 3
.L1823:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1820
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%rbx, %r15
	jne	.L1823
.L1821:
	movq	-344(%rbp), %r15
.L1819:
	testq	%r15, %r15
	je	.L1824
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1824:
	movq	-4024(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-512(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1825
	call	_ZdlPv@PLT
.L1825:
	movq	-528(%rbp), %rbx
	movq	-536(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L1826
	.p2align 4,,10
	.p2align 3
.L1830:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1827
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L1830
.L1828:
	movq	-536(%rbp), %r15
.L1826:
	testq	%r15, %r15
	je	.L1831
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1831:
	movq	-3912(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-704(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1832
	call	_ZdlPv@PLT
.L1832:
	movq	-720(%rbp), %rbx
	movq	-728(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L1833
	.p2align 4,,10
	.p2align 3
.L1837:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1834
	call	_ZdlPv@PLT
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L1837
.L1835:
	movq	-728(%rbp), %r15
.L1833:
	testq	%r15, %r15
	je	.L1838
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1838:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4008(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1839
	call	_ZdlPv@PLT
.L1839:
	movq	-1104(%rbp), %rbx
	movq	-1112(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1840
	.p2align 4,,10
	.p2align 3
.L1844:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1841
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L1844
.L1842:
	movq	-1112(%rbp), %r13
.L1840:
	testq	%r13, %r13
	je	.L1845
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1845:
	movq	-4048(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3992(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1472(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1846
	call	_ZdlPv@PLT
.L1846:
	movq	-1488(%rbp), %rbx
	movq	-1496(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1847
	.p2align 4,,10
	.p2align 3
.L1851:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1848
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1851
.L1849:
	movq	-1496(%rbp), %r13
.L1847:
	testq	%r13, %r13
	je	.L1852
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1852:
	movq	-3968(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3984(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1856(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1853
	call	_ZdlPv@PLT
.L1853:
	movq	-1872(%rbp), %rbx
	movq	-1880(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1854
	.p2align 4,,10
	.p2align 3
.L1858:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1855
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L1858
.L1856:
	movq	-1880(%rbp), %r13
.L1854:
	testq	%r13, %r13
	je	.L1859
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1859:
	movq	-4032(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4000(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3920(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1860
	call	_ZdlPv@PLT
.L1860:
	movq	-2448(%rbp), %rbx
	movq	-2456(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1861
	.p2align 4,,10
	.p2align 3
.L1865:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1862
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1865
.L1863:
	movq	-2456(%rbp), %r13
.L1861:
	testq	%r13, %r13
	je	.L1866
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1866:
	movq	-4056(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1867
	call	_ZdlPv@PLT
.L1867:
	movq	-2640(%rbp), %rbx
	movq	-2648(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1868
	.p2align 4,,10
	.p2align 3
.L1872:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1869
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1872
.L1870:
	movq	-2648(%rbp), %r13
.L1868:
	testq	%r13, %r13
	je	.L1873
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1873:
	movq	-4016(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3952(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3936(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3976(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3896(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1874
	call	_ZdlPv@PLT
.L1874:
	movq	-3600(%rbp), %rbx
	movq	-3608(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1875
	.p2align 4,,10
	.p2align 3
.L1879:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1876
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L1879
.L1877:
	movq	-3608(%rbp), %r13
.L1875:
	testq	%r13, %r13
	je	.L1880
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1880:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2055
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1876:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1879
	jmp	.L1877
	.p2align 4,,10
	.p2align 3
.L1869:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1872
	jmp	.L1870
	.p2align 4,,10
	.p2align 3
.L1862:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1865
	jmp	.L1863
	.p2align 4,,10
	.p2align 3
.L1855:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1858
	jmp	.L1856
	.p2align 4,,10
	.p2align 3
.L1848:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1851
	jmp	.L1849
	.p2align 4,,10
	.p2align 3
.L1841:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1844
	jmp	.L1842
	.p2align 4,,10
	.p2align 3
.L1834:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L1837
	jmp	.L1835
	.p2align 4,,10
	.p2align 3
.L1820:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L1823
	jmp	.L1821
	.p2align 4,,10
	.p2align 3
.L1827:
	addq	$24, %r15
	cmpq	%r15, %rbx
	jne	.L1830
	jmp	.L1828
	.p2align 4,,10
	.p2align 3
.L2038:
	movq	-3896(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-160(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-156(%rbp), %rdx
	movaps	%xmm0, -3744(%rbp)
	movl	$117901063, -160(%rbp)
	movq	$0, -3728(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3744(%rbp), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L1771
	call	_ZdlPv@PLT
.L1771:
	movq	0(%r13), %rax
	movl	$133, %edx
	movq	%r12, %rdi
	movq	16(%rax), %rsi
	movq	(%rax), %rcx
	movq	8(%rax), %r13
	movq	24(%rax), %rax
	movq	%rsi, -4016(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -3968(%rbp)
	movq	%r13, -3976(%rbp)
	movq	%rax, -3936(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal15kEmptyString_67EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$134, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -4176(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$135, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -4160(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal42FromConstexpr6ATbool16ATconstexpr_bool_165EPNS0_8compiler18CodeAssemblerStateEb@PLT
	movl	$136, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -4144(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3936(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler21LoadStringLengthAsSmiENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	movq	%r15, %rdi
	movq	%rax, -3952(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$137, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movl	$1, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal23RegExpBuiltinsAssembler14FastFlagGetterENS0_8compiler5TNodeINS0_8JSRegExpEEENS4_4FlagE@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$139, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movq	-4144(%rbp), %xmm5
	movq	-3968(%rbp), %xmm3
	movaps	%xmm0, -3744(%rbp)
	movq	-4176(%rbp), %xmm6
	movq	-4016(%rbp), %xmm7
	movq	%r13, -96(%rbp)
	movhps	-3952(%rbp), %xmm5
	movhps	-4160(%rbp), %xmm6
	movhps	-3976(%rbp), %xmm3
	movaps	%xmm5, -4144(%rbp)
	movhps	-3936(%rbp), %xmm7
	movaps	%xmm6, -3952(%rbp)
	movaps	%xmm7, -3936(%rbp)
	movaps	%xmm3, -3968(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movq	$0, -3728(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm6
	movq	-96(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-144(%rbp), %xmm7
	leaq	72(%rax), %rdx
	movq	%rax, -3744(%rbp)
	movups	%xmm6, (%rax)
	movdqa	-128(%rbp), %xmm6
	movups	%xmm7, 16(%rax)
	movdqa	-112(%rbp), %xmm7
	movq	%rcx, 64(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	leaq	-3424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3728(%rbp)
	movq	%rdx, -3736(%rbp)
	movq	%rax, -3976(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1772
	call	_ZdlPv@PLT
.L1772:
	movdqa	-3968(%rbp), %xmm4
	movdqa	-3936(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movdqa	-3952(%rbp), %xmm7
	movq	%r13, -96(%rbp)
	movaps	%xmm4, -160(%rbp)
	movdqa	-4144(%rbp), %xmm4
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm0, -3744(%rbp)
	movq	$0, -3728(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm5
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm6
	leaq	72(%rax), %rdx
	movq	%rax, -3744(%rbp)
	movdqa	-144(%rbp), %xmm7
	movq	%rcx, 64(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm4, 48(%rax)
	leaq	-3232(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3728(%rbp)
	movq	%rdx, -3736(%rbp)
	movq	%rax, -3936(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1773
	call	_ZdlPv@PLT
.L1773:
	movq	-4000(%rbp), %rdx
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -3360(%rbp)
	je	.L1774
.L2039:
	movq	-4000(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3848(%rbp)
	movq	$0, -3840(%rbp)
	movq	$0, -3832(%rbp)
	movq	$0, -3824(%rbp)
	movq	$0, -3816(%rbp)
	movq	$0, -3808(%rbp)
	movq	$0, -3800(%rbp)
	movq	$0, -3792(%rbp)
	movq	$0, -3776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3776(%rbp), %rax
	movq	-3976(%rbp), %rdi
	leaq	-3816(%rbp), %r9
	pushq	%rax
	leaq	-3792(%rbp), %rax
	leaq	-3824(%rbp), %r8
	pushq	%rax
	leaq	-3800(%rbp), %rax
	leaq	-3832(%rbp), %rcx
	pushq	%rax
	leaq	-3808(%rbp), %rax
	leaq	-3840(%rbp), %rdx
	pushq	%rax
	leaq	-3848(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_8JSRegExpENS0_6StringES5_S5_NS0_3SmiENS0_5BoolTES6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_PNS9_IS6_EEPNS9_IS7_EESH_SJ_
	addq	$32, %rsp
	movl	$140, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3840(%rbp), %rsi
	movl	$16, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal23RegExpBuiltinsAssembler14FastFlagGetterENS0_8compiler5TNodeINS0_8JSRegExpEEENS4_4FlagE@PLT
	movq	%r15, %rdi
	movq	%rax, -3952(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$141, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3840(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal23RegExpBuiltinsAssembler18FastStoreLastIndexENS0_8compiler5TNodeINS0_8JSRegExpEEENS3_INS0_3SmiEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$139, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3792(%rbp), %rax
	movl	$72, %edi
	movq	-3816(%rbp), %xmm0
	movq	-3832(%rbp), %xmm1
	movq	-3848(%rbp), %xmm2
	movq	$0, -3728(%rbp)
	movq	-3952(%rbp), %rcx
	movhps	-3808(%rbp), %xmm0
	movq	%rax, -104(%rbp)
	movq	-3776(%rbp), %rax
	movhps	-3824(%rbp), %xmm1
	movhps	-3840(%rbp), %xmm2
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rcx, -112(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -3744(%rbp)
	movq	%rax, -96(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm5
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm6
	leaq	72(%rax), %rdx
	movq	%rax, -3744(%rbp)
	movdqa	-144(%rbp), %xmm7
	movq	-3936(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm4, 48(%rax)
	movq	%rdx, -3728(%rbp)
	movq	%rdx, -3736(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1775
	call	_ZdlPv@PLT
.L1775:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1774
	.p2align 4,,10
	.p2align 3
.L2040:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	$0, -3848(%rbp)
	movq	$0, -3840(%rbp)
	movq	$0, -3832(%rbp)
	movq	$0, -3824(%rbp)
	movq	$0, -3816(%rbp)
	movq	$0, -3808(%rbp)
	movq	$0, -3800(%rbp)
	movq	$0, -3792(%rbp)
	movq	$0, -3776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3776(%rbp), %rax
	movq	-3936(%rbp), %rdi
	leaq	-3832(%rbp), %rcx
	pushq	%rax
	leaq	-3792(%rbp), %rax
	leaq	-3816(%rbp), %r9
	pushq	%rax
	leaq	-3800(%rbp), %rax
	leaq	-3824(%rbp), %r8
	pushq	%rax
	leaq	-3808(%rbp), %rax
	leaq	-3840(%rbp), %rdx
	pushq	%rax
	leaq	-3848(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_8JSRegExpENS0_6StringES5_S5_NS0_3SmiENS0_5BoolTES6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_PNS9_IS6_EEPNS9_IS7_EESH_SJ_
	addq	$32, %rsp
	movl	$144, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3848(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movaps	%xmm0, -3744(%rbp)
	movq	%rax, -160(%rbp)
	movq	-3840(%rbp), %rax
	movq	$0, -3728(%rbp)
	movq	%rax, -152(%rbp)
	movq	-3832(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-3824(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-3816(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-3808(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-3800(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-3792(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-3776(%rbp), %rax
	movq	%rax, -96(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm5
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm6
	leaq	72(%rax), %rdx
	movq	%rax, -3744(%rbp)
	movdqa	-144(%rbp), %xmm7
	movq	-3952(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm4, 48(%rax)
	movq	%rdx, -3728(%rbp)
	movq	%rdx, -3736(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1777
	call	_ZdlPv@PLT
.L1777:
	movq	-4064(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1776
	.p2align 4,,10
	.p2align 3
.L2041:
	movq	-4064(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3848(%rbp)
	movq	$0, -3840(%rbp)
	movq	$0, -3832(%rbp)
	movq	$0, -3824(%rbp)
	movq	$0, -3816(%rbp)
	movq	$0, -3808(%rbp)
	movq	$0, -3800(%rbp)
	movq	$0, -3792(%rbp)
	movq	$0, -3776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3776(%rbp), %rax
	movq	-3952(%rbp), %rdi
	leaq	-3816(%rbp), %r9
	pushq	%rax
	leaq	-3792(%rbp), %rax
	leaq	-3824(%rbp), %r8
	pushq	%rax
	leaq	-3800(%rbp), %rax
	leaq	-3832(%rbp), %rcx
	pushq	%rax
	leaq	-3808(%rbp), %rax
	leaq	-3840(%rbp), %rdx
	pushq	%rax
	leaq	-3848(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_8JSRegExpENS0_6StringES5_S5_NS0_3SmiENS0_5BoolTES6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_PNS9_IS6_EEPNS9_IS7_EESH_SJ_
	addq	$32, %rsp
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal42FromConstexpr6ATbool16ATconstexpr_bool_165EPNS0_8compiler18CodeAssemblerStateEb@PLT
	movq	-3816(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	-3840(%rbp), %rcx
	movq	-3832(%rbp), %rsi
	movq	-3824(%rbp), %rdx
	movq	%rax, -3968(%rbp)
	movq	-3808(%rbp), %r11
	movq	-3792(%rbp), %r10
	movq	%rdi, -4192(%rbp)
	movq	-3848(%rbp), %rax
	movq	-3800(%rbp), %rbx
	movq	%rdi, -128(%rbp)
	movl	$72, %edi
	movq	-3776(%rbp), %r13
	movq	%rcx, -4096(%rbp)
	movq	%rsi, -4160(%rbp)
	movq	%rdx, -4144(%rbp)
	movq	%r11, -4176(%rbp)
	movq	%r10, -4200(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%rsi, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%r11, -120(%rbp)
	movq	%r10, -104(%rbp)
	movaps	%xmm0, -3744(%rbp)
	movq	%rax, -4000(%rbp)
	movq	%rax, -160(%rbp)
	movq	%rbx, -112(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -3728(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm5
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm6
	leaq	72(%rax), %rdx
	movq	%rax, -3744(%rbp)
	movdqa	-144(%rbp), %xmm7
	movq	-4016(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm4, 48(%rax)
	movq	%rdx, -3728(%rbp)
	movq	%rdx, -3736(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1779
	call	_ZdlPv@PLT
.L1779:
	movq	-4000(%rbp), %xmm0
	movl	$72, %edi
	movq	%r13, -96(%rbp)
	movq	$0, -3728(%rbp)
	movhps	-4096(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-4160(%rbp), %xmm0
	movhps	-4144(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-4192(%rbp), %xmm0
	movhps	-4176(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%rbx, %xmm0
	movhps	-4200(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3744(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm5
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm6
	leaq	72(%rax), %rdx
	leaq	-736(%rbp), %rdi
	movdqa	-144(%rbp), %xmm7
	movq	%rcx, 64(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm4, 48(%rax)
	movq	%rax, -3744(%rbp)
	movq	%rdx, -3728(%rbp)
	movq	%rdx, -3736(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1780
	call	_ZdlPv@PLT
.L1780:
	movq	-3912(%rbp), %rcx
	movq	-4032(%rbp), %rdx
	movq	%r12, %rdi
	movq	-3968(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2784(%rbp)
	je	.L1781
.L2042:
	movq	-4032(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3856(%rbp)
	leaq	-3776(%rbp), %r13
	movq	$0, -3848(%rbp)
	movq	$0, -3840(%rbp)
	movq	$0, -3832(%rbp)
	movq	$0, -3824(%rbp)
	movq	$0, -3816(%rbp)
	movq	$0, -3808(%rbp)
	movq	$0, -3800(%rbp)
	movq	$0, -3792(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3792(%rbp), %rax
	movq	-4016(%rbp), %rdi
	leaq	-3824(%rbp), %r9
	pushq	%rax
	leaq	-3800(%rbp), %rax
	leaq	-3840(%rbp), %rcx
	pushq	%rax
	leaq	-3808(%rbp), %rax
	leaq	-3832(%rbp), %r8
	pushq	%rax
	leaq	-3816(%rbp), %rax
	leaq	-3848(%rbp), %rdx
	pushq	%rax
	leaq	-3856(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_8JSRegExpENS0_6StringES5_S5_NS0_3SmiENS0_5BoolTES6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_PNS9_IS6_EEPNS9_IS7_EESH_SJ_
	addq	$32, %rsp
	movl	$146, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3840(%rbp), %rcx
	movq	%r15, %r8
	movq	%r13, %rdi
	movq	-3848(%rbp), %rdx
	movq	-3856(%rbp), %rsi
	call	_ZN2v88internal23RegExpBuiltinsAssembler40RegExpPrototypeExecBodyWithoutResultFastENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_8JSRegExpEEENS3_INS0_6StringEEEPNS2_18CodeAssemblerLabelE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-64(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rbx, -72(%rbp)
	movq	-3792(%rbp), %xmm0
	movq	-3840(%rbp), %rax
	movq	$0, -3760(%rbp)
	movq	-3808(%rbp), %xmm1
	movq	-3824(%rbp), %xmm2
	movq	-3840(%rbp), %xmm4
	movhps	-3848(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	leaq	-160(%rbp), %rax
	movq	-3856(%rbp), %xmm3
	movhps	-3800(%rbp), %xmm1
	movq	%rax, %rsi
	movaps	%xmm0, -96(%rbp)
	movhps	-3816(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movaps	%xmm1, -112(%rbp)
	movhps	-3832(%rbp), %xmm4
	movhps	-3848(%rbp), %xmm3
	movq	%rax, -3968(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm0, -3776(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2464(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3776(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1782
	call	_ZdlPv@PLT
.L1782:
	movq	-3920(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3736(%rbp)
	jne	.L2056
.L1783:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L1781
	.p2align 4,,10
	.p2align 3
.L2045:
	movq	-4048(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3848(%rbp)
	movq	$0, -3840(%rbp)
	movq	$0, -3832(%rbp)
	movq	$0, -3824(%rbp)
	movq	$0, -3816(%rbp)
	movq	$0, -3808(%rbp)
	movq	$0, -3800(%rbp)
	movq	$0, -3792(%rbp)
	movq	$0, -3776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3776(%rbp), %rax
	movq	-4000(%rbp), %rdi
	leaq	-3832(%rbp), %rcx
	pushq	%rax
	leaq	-3792(%rbp), %rax
	leaq	-3816(%rbp), %r9
	pushq	%rax
	leaq	-3800(%rbp), %rax
	leaq	-3824(%rbp), %r8
	pushq	%rax
	leaq	-3808(%rbp), %rax
	leaq	-3840(%rbp), %rdx
	pushq	%rax
	leaq	-3848(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_8JSRegExpENS0_6StringES5_S5_NS0_3SmiENS0_5BoolTES6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_PNS9_IS6_EEPNS9_IS7_EESH_SJ_
	addq	$32, %rsp
	movl	$147, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3848(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movaps	%xmm0, -3744(%rbp)
	movq	%rax, -160(%rbp)
	movq	-3840(%rbp), %rax
	movq	$0, -3728(%rbp)
	movq	%rax, -152(%rbp)
	movq	-3832(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-3824(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-3816(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-3808(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-3800(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-3792(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-3776(%rbp), %rax
	movq	%rax, -96(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm7
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-160(%rbp), %xmm4
	leaq	72(%rax), %rdx
	leaq	-736(%rbp), %rdi
	movdqa	-144(%rbp), %xmm6
	movq	%rcx, 64(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rax, -3744(%rbp)
	movq	%rdx, -3728(%rbp)
	movq	%rdx, -3736(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1792
	call	_ZdlPv@PLT
.L1792:
	movq	-3912(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1791
	.p2align 4,,10
	.p2align 3
.L2044:
	movq	-3920(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2464(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-148(%rbp), %rdx
	movq	%r15, %rdi
	movabsq	$433478091386849031, %rax
	movaps	%xmm0, -3744(%rbp)
	movq	%rax, -160(%rbp)
	leaq	-160(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -3968(%rbp)
	movl	$117901060, -152(%rbp)
	movq	$0, -3728(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3744(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1789
	call	_ZdlPv@PLT
.L1789:
	movq	(%rbx), %rax
	movq	-3968(%rbp), %rsi
	leaq	-80(%rbp), %rdx
	movq	%r15, %rdi
	movdqu	80(%rax), %xmm5
	movdqu	64(%rax), %xmm0
	movdqu	(%rax), %xmm4
	movdqu	16(%rax), %xmm3
	movdqu	32(%rax), %xmm2
	movdqu	48(%rax), %xmm1
	shufpd	$2, %xmm5, %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3744(%rbp)
	movq	$0, -3728(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2080(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -4032(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1790
	call	_ZdlPv@PLT
.L1790:
	movq	-4112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1788
	.p2align 4,,10
	.p2align 3
.L2043:
	movq	-4056(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2656(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1796, %r8d
	pxor	%xmm0, %xmm0
	movabsq	$433478091386849031, %rax
	leaq	-160(%rbp), %rsi
	leaq	-149(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rax, -160(%rbp)
	movw	%r8w, -152(%rbp)
	movaps	%xmm0, -3744(%rbp)
	movb	$7, -150(%rbp)
	movq	$0, -3728(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3744(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1786
	call	_ZdlPv@PLT
.L1786:
	movq	(%rbx), %rax
	movl	$72, %edi
	movdqu	48(%rax), %xmm0
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	64(%rax), %rdx
	movdqu	(%rax), %xmm6
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3744(%rbp)
	movq	$0, -3728(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm5
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-160(%rbp), %xmm7
	leaq	72(%rax), %rdx
	movq	%rax, -3744(%rbp)
	movdqa	-144(%rbp), %xmm3
	movq	%rcx, 64(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm2, 48(%rax)
	leaq	-2272(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3728(%rbp)
	movq	%rdx, -3736(%rbp)
	movq	%rax, -4000(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1787
	call	_ZdlPv@PLT
.L1787:
	movq	-4048(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L2046:
	movq	-4112(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -4048(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$1796, %edi
	pxor	%xmm0, %xmm0
	movabsq	$433478091386849031, %rax
	movq	%rax, -160(%rbp)
	leaq	-160(%rbp), %rax
	leaq	-150(%rbp), %rdx
	movq	%rax, %rsi
	movw	%di, -152(%rbp)
	movq	%r15, %rdi
	movq	%rax, -3968(%rbp)
	movaps	%xmm0, -3744(%rbp)
	movq	$0, -3728(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4032(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3744(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1794
	call	_ZdlPv@PLT
.L1794:
	movq	(%rbx), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	16(%rax), %rcx
	movq	32(%rax), %rdx
	movq	(%rax), %r13
	movq	64(%rax), %rbx
	movq	%rcx, -4144(%rbp)
	movq	%rcx, %xmm5
	movq	40(%rax), %rcx
	testq	%rdx, %rdx
	movq	%rbx, -4216(%rbp)
	cmove	-4048(%rbp), %rdx
	movq	%r13, %xmm4
	movhps	24(%rax), %xmm5
	movq	%rcx, -4200(%rbp)
	movq	48(%rax), %rcx
	movhps	8(%rax), %xmm4
	movq	72(%rax), %rbx
	movq	%rdx, -4048(%rbp)
	movl	$145, %edx
	movq	%rcx, -4208(%rbp)
	movq	56(%rax), %rcx
	movaps	%xmm4, -4176(%rbp)
	movaps	%xmm5, -4112(%rbp)
	movq	%rcx, -4160(%rbp)
	movq	%rbx, -4224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$148, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal41_method_RegExpMatchInfo_GetStartOfCaptureEPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_15RegExpMatchInfoEEENS0_7int31_tE@PLT
	movl	$149, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -4192(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-3792(%rbp), %rbx
	call	_ZN2v88internal39_method_RegExpMatchInfo_GetEndOfCaptureEPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_15RegExpMatchInfoEEENS0_7int31_tE@PLT
	movl	$153, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -4096(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$707, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-3744(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-3968(%rbp), %rsi
	movq	%r13, %r9
	movl	$3, %edi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rdi
	movl	$1, %ecx
	pushq	%rsi
	leaq	-3776(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	-4144(%rbp), %xmm0
	movq	%rax, -3776(%rbp)
	movq	-3728(%rbp), %rax
	movhps	-4200(%rbp), %xmm0
	movq	%rax, -3768(%rbp)
	movq	-4192(%rbp), %rax
	movaps	%xmm0, -160(%rbp)
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%rbx, %rdi
	movq	%rax, -3968(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3968(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-4048(%rbp), %rdx
	call	_ZN2v88internal12StringAdd_82EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6StringEEES8_@PLT
	movl	$154, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -3968(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$156, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-4160(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$96, %edi
	movq	-4192(%rbp), %xmm6
	movq	-3968(%rbp), %xmm1
	movdqa	-4176(%rbp), %xmm4
	movaps	%xmm0, -3744(%rbp)
	movq	-4216(%rbp), %xmm7
	movdqa	-4112(%rbp), %xmm5
	movhps	-4096(%rbp), %xmm6
	movq	$0, -3728(%rbp)
	movq	-4208(%rbp), %xmm3
	movhps	-4096(%rbp), %xmm1
	movaps	%xmm6, -80(%rbp)
	movhps	-4224(%rbp), %xmm7
	movaps	%xmm6, -4192(%rbp)
	movhps	-4160(%rbp), %xmm3
	movaps	%xmm7, -4144(%rbp)
	movaps	%xmm3, -4048(%rbp)
	movaps	%xmm1, -3968(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm4
	leaq	-1888(%rbp), %rdi
	movdqa	-144(%rbp), %xmm2
	movdqa	-112(%rbp), %xmm6
	leaq	96(%rax), %rdx
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm7
	movdqa	-80(%rbp), %xmm3
	movups	%xmm5, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm3, 80(%rax)
	movq	%rax, -3744(%rbp)
	movq	%rdx, -3728(%rbp)
	movq	%rdx, -3736(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3744(%rbp), %rdi
	popq	%rcx
	popq	%rsi
	testq	%rdi, %rdi
	je	.L1796
	call	_ZdlPv@PLT
.L1796:
	movdqa	-4176(%rbp), %xmm5
	movdqa	-4112(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movl	$96, %edi
	movdqa	-3968(%rbp), %xmm4
	movdqa	-4048(%rbp), %xmm6
	movaps	%xmm0, -3744(%rbp)
	movdqa	-4144(%rbp), %xmm7
	movdqa	-4192(%rbp), %xmm3
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movq	$0, -3728(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm4
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm7
	leaq	96(%rax), %rdx
	movq	%rax, -3744(%rbp)
	movdqa	-144(%rbp), %xmm2
	movdqa	-80(%rbp), %xmm3
	movups	%xmm5, (%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm3, 80(%rax)
	leaq	-1696(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3728(%rbp)
	movq	%rdx, -3736(%rbp)
	movq	%rax, -3968(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1797
	call	_ZdlPv@PLT
.L1797:
	movq	-4080(%rbp), %rcx
	movq	-3984(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1824(%rbp)
	je	.L1798
.L2047:
	movq	-3984(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3872(%rbp)
	leaq	-1888(%rbp), %r13
	movq	$0, -3864(%rbp)
	movq	$0, -3856(%rbp)
	movq	$0, -3848(%rbp)
	movq	$0, -3840(%rbp)
	movq	$0, -3832(%rbp)
	movq	$0, -3824(%rbp)
	movq	$0, -3816(%rbp)
	movq	$0, -3808(%rbp)
	movq	$0, -3800(%rbp)
	movq	$0, -3792(%rbp)
	movq	$0, -3776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r13, %rdi
	leaq	-3776(%rbp), %rax
	pushq	%rax
	leaq	-3792(%rbp), %rax
	leaq	-3840(%rbp), %r9
	pushq	%rax
	leaq	-3800(%rbp), %rax
	leaq	-3848(%rbp), %r8
	pushq	%rax
	leaq	-3808(%rbp), %rax
	leaq	-3856(%rbp), %rcx
	pushq	%rax
	leaq	-3816(%rbp), %rax
	leaq	-3864(%rbp), %rdx
	pushq	%rax
	leaq	-3824(%rbp), %rax
	leaq	-3872(%rbp), %rsi
	pushq	%rax
	leaq	-3832(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_8JSRegExpENS0_6StringES5_S5_NS0_3SmiENS0_5BoolTES6_S7_NS0_15RegExpMatchInfoES6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EESG_SG_PNSA_IS6_EEPNSA_IS7_EESI_SK_PNSA_IS8_EESI_SI_
	movq	-3848(%rbp), %rcx
	addq	$64, %rsp
	movq	%r14, %rdi
	movq	-3840(%rbp), %rdx
	movq	-3872(%rbp), %rsi
	call	_ZN2v88internal12StringAdd_82EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6StringEEES8_@PLT
	leaq	-160(%rbp), %rsi
	leaq	-64(%rbp), %rdx
	movq	%r15, %rdi
	movq	-3792(%rbp), %xmm0
	movq	%rax, %xmm3
	movq	-3808(%rbp), %xmm1
	movq	$0, -3728(%rbp)
	movq	-3824(%rbp), %xmm2
	movhps	-3832(%rbp), %xmm3
	movq	-3856(%rbp), %xmm4
	movq	-3872(%rbp), %xmm5
	movhps	-3776(%rbp), %xmm0
	movhps	-3800(%rbp), %xmm1
	movaps	%xmm3, -128(%rbp)
	movhps	-3816(%rbp), %xmm2
	movhps	-3848(%rbp), %xmm4
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3864(%rbp), %xmm5
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -3744(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3968(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1799
	call	_ZdlPv@PLT
.L1799:
	movq	-4080(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1798
	.p2align 4,,10
	.p2align 3
.L2048:
	movq	-4080(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3872(%rbp)
	movq	$0, -3864(%rbp)
	movq	$0, -3856(%rbp)
	movq	$0, -3848(%rbp)
	movq	$0, -3840(%rbp)
	movq	$0, -3832(%rbp)
	movq	$0, -3824(%rbp)
	movq	$0, -3816(%rbp)
	movq	$0, -3808(%rbp)
	movq	$0, -3800(%rbp)
	movq	$0, -3792(%rbp)
	movq	$0, -3776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3776(%rbp), %rax
	movq	-3968(%rbp), %rdi
	pushq	%rax
	leaq	-3792(%rbp), %rax
	leaq	-3856(%rbp), %rcx
	pushq	%rax
	leaq	-3800(%rbp), %rax
	leaq	-3840(%rbp), %r9
	pushq	%rax
	leaq	-3808(%rbp), %rax
	leaq	-3848(%rbp), %r8
	pushq	%rax
	leaq	-3816(%rbp), %rax
	leaq	-3864(%rbp), %rdx
	pushq	%rax
	leaq	-3824(%rbp), %rax
	leaq	-3872(%rbp), %rsi
	pushq	%rax
	leaq	-3832(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_8JSRegExpENS0_6StringES5_S5_NS0_3SmiENS0_5BoolTES6_S7_NS0_15RegExpMatchInfoES6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EESG_SG_PNSA_IS6_EEPNSA_IS7_EESI_SK_PNSA_IS8_EESI_SI_
	addq	$64, %rsp
	movl	$159, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3808(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler15Word32BinaryNotENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$96, %edi
	movq	-3792(%rbp), %xmm4
	movq	-3808(%rbp), %xmm5
	movaps	%xmm0, -3744(%rbp)
	movq	-3824(%rbp), %xmm6
	movq	-3840(%rbp), %xmm7
	movhps	-3776(%rbp), %xmm4
	movq	-3856(%rbp), %xmm3
	movq	$0, -3728(%rbp)
	movq	-3872(%rbp), %xmm2
	movhps	-3800(%rbp), %xmm5
	movaps	%xmm4, -80(%rbp)
	movhps	-3816(%rbp), %xmm6
	movhps	-3832(%rbp), %xmm7
	movhps	-3848(%rbp), %xmm3
	movaps	%xmm4, -4176(%rbp)
	movhps	-3864(%rbp), %xmm2
	movaps	%xmm5, -4160(%rbp)
	movaps	%xmm6, -4144(%rbp)
	movaps	%xmm7, -4112(%rbp)
	movaps	%xmm3, -4080(%rbp)
	movaps	%xmm2, -4048(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm4
	leaq	-1504(%rbp), %rdi
	movdqa	-144(%rbp), %xmm2
	movdqa	-112(%rbp), %xmm6
	leaq	96(%rax), %rdx
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm7
	movdqa	-80(%rbp), %xmm3
	movups	%xmm5, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm3, 80(%rax)
	movq	%rax, -3744(%rbp)
	movq	%rdx, -3728(%rbp)
	movq	%rdx, -3736(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1801
	call	_ZdlPv@PLT
.L1801:
	movdqa	-4048(%rbp), %xmm5
	movdqa	-4080(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movl	$96, %edi
	movdqa	-4112(%rbp), %xmm2
	movdqa	-4144(%rbp), %xmm4
	movaps	%xmm0, -3744(%rbp)
	movdqa	-4160(%rbp), %xmm6
	movdqa	-4176(%rbp), %xmm7
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	movq	$0, -3728(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm1
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm4
	leaq	96(%rax), %rdx
	movq	%rax, -3744(%rbp)
	movdqa	-144(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm6
	movups	%xmm5, (%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm6, 80(%rax)
	leaq	-1312(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3728(%rbp)
	movq	%rdx, -3736(%rbp)
	movq	%rax, -4048(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1802
	call	_ZdlPv@PLT
.L1802:
	movq	-4128(%rbp), %rcx
	movq	-3992(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1440(%rbp)
	je	.L1803
.L2049:
	movq	-3992(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3872(%rbp)
	leaq	-1504(%rbp), %r13
	movq	$0, -3864(%rbp)
	movq	$0, -3856(%rbp)
	movq	$0, -3848(%rbp)
	movq	$0, -3840(%rbp)
	movq	$0, -3832(%rbp)
	movq	$0, -3824(%rbp)
	movq	$0, -3816(%rbp)
	movq	$0, -3808(%rbp)
	movq	$0, -3800(%rbp)
	movq	$0, -3792(%rbp)
	movq	$0, -3776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r13, %rdi
	leaq	-3776(%rbp), %rax
	pushq	%rax
	leaq	-3792(%rbp), %rax
	leaq	-3856(%rbp), %rcx
	pushq	%rax
	leaq	-3800(%rbp), %rax
	leaq	-3864(%rbp), %rdx
	pushq	%rax
	leaq	-3808(%rbp), %rax
	leaq	-3872(%rbp), %rsi
	pushq	%rax
	leaq	-3816(%rbp), %rax
	leaq	-3840(%rbp), %r9
	pushq	%rax
	leaq	-3824(%rbp), %rax
	leaq	-3848(%rbp), %r8
	pushq	%rax
	leaq	-3832(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_8JSRegExpENS0_6StringES5_S5_NS0_3SmiENS0_5BoolTES6_S7_NS0_15RegExpMatchInfoES6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EESG_SG_PNSA_IS6_EEPNSA_IS7_EESI_SK_PNSA_IS8_EESI_SI_
	movq	-3872(%rbp), %rax
	pxor	%xmm0, %xmm0
	addq	$64, %rsp
	movl	$72, %edi
	movaps	%xmm0, -3744(%rbp)
	movq	%rax, -160(%rbp)
	movq	-3864(%rbp), %rax
	movq	$0, -3728(%rbp)
	movq	%rax, -152(%rbp)
	movq	-3856(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-3848(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-3840(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-3832(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-3824(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-3816(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-3808(%rbp), %rax
	movq	%rax, -96(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm3
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm1
	movdqa	-160(%rbp), %xmm7
	leaq	72(%rax), %rdx
	leaq	-736(%rbp), %rdi
	movdqa	-144(%rbp), %xmm5
	movq	%rcx, 64(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm1, 48(%rax)
	movq	%rax, -3744(%rbp)
	movq	%rdx, -3728(%rbp)
	movq	%rdx, -3736(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1804
	call	_ZdlPv@PLT
.L1804:
	movq	-3912(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1803
	.p2align 4,,10
	.p2align 3
.L2050:
	movq	-4128(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3872(%rbp)
	movq	$0, -3864(%rbp)
	movq	$0, -3856(%rbp)
	movq	$0, -3848(%rbp)
	movq	$0, -3840(%rbp)
	movq	$0, -3832(%rbp)
	movq	$0, -3824(%rbp)
	movq	$0, -3816(%rbp)
	movq	$0, -3808(%rbp)
	movq	$0, -3800(%rbp)
	movq	$0, -3792(%rbp)
	movq	$0, -3776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3776(%rbp), %rax
	movq	-4048(%rbp), %rdi
	pushq	%rax
	leaq	-3792(%rbp), %rax
	leaq	-3856(%rbp), %rcx
	pushq	%rax
	leaq	-3800(%rbp), %rax
	leaq	-3840(%rbp), %r9
	pushq	%rax
	leaq	-3808(%rbp), %rax
	leaq	-3848(%rbp), %r8
	pushq	%rax
	leaq	-3816(%rbp), %rax
	leaq	-3864(%rbp), %rdx
	pushq	%rax
	leaq	-3824(%rbp), %rax
	leaq	-3872(%rbp), %rsi
	pushq	%rax
	leaq	-3832(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_8JSRegExpENS0_6StringES5_S5_NS0_3SmiENS0_5BoolTES6_S7_NS0_15RegExpMatchInfoES6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EESG_SG_PNSA_IS6_EEPNSA_IS7_EESI_SK_PNSA_IS8_EESI_SI_
	addq	$64, %rsp
	movl	$162, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3776(%rbp), %rbx
	movq	-3792(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$96, %edi
	movq	-3792(%rbp), %xmm4
	movq	-3808(%rbp), %xmm5
	movaps	%xmm0, -3744(%rbp)
	movq	-3824(%rbp), %xmm6
	movq	-3840(%rbp), %xmm7
	movhps	-3776(%rbp), %xmm4
	movq	-3856(%rbp), %xmm3
	movq	$0, -3728(%rbp)
	movq	-3872(%rbp), %xmm1
	movhps	-3800(%rbp), %xmm5
	movaps	%xmm4, -80(%rbp)
	movhps	-3816(%rbp), %xmm6
	movhps	-3832(%rbp), %xmm7
	movhps	-3848(%rbp), %xmm3
	movaps	%xmm4, -4176(%rbp)
	movhps	-3864(%rbp), %xmm1
	movaps	%xmm5, -4160(%rbp)
	movaps	%xmm6, -4144(%rbp)
	movaps	%xmm7, -4128(%rbp)
	movaps	%xmm3, -4112(%rbp)
	movaps	%xmm1, -4080(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm6
	leaq	-1120(%rbp), %rdi
	movdqa	-144(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm7
	leaq	96(%rax), %rdx
	movq	%r15, %rsi
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm3
	movups	%xmm2, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm5, 64(%rax)
	movups	%xmm3, 80(%rax)
	movq	%rax, -3744(%rbp)
	movq	%rdx, -3728(%rbp)
	movq	%rdx, -3736(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1806
	call	_ZdlPv@PLT
.L1806:
	movdqa	-4080(%rbp), %xmm1
	movdqa	-4112(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movl	$96, %edi
	movdqa	-4128(%rbp), %xmm4
	movdqa	-4144(%rbp), %xmm6
	movaps	%xmm0, -3744(%rbp)
	leaq	-928(%rbp), %r13
	movdqa	-4160(%rbp), %xmm7
	movdqa	-4176(%rbp), %xmm5
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movq	$0, -3728(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm2
	movq	%r13, %rdi
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm6
	leaq	96(%rax), %rdx
	movq	%rax, -3744(%rbp)
	movdqa	-160(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm1
	movups	%xmm2, 32(%rax)
	movdqa	-80(%rbp), %xmm7
	movups	%xmm4, 48(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm1, 16(%rax)
	movups	%xmm6, 64(%rax)
	movups	%xmm7, 80(%rax)
	movq	%rdx, -3728(%rbp)
	movq	%rdx, -3736(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1807
	call	_ZdlPv@PLT
.L1807:
	movq	-4088(%rbp), %rcx
	movq	-4008(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1056(%rbp)
	je	.L1808
.L2051:
	movq	-4008(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3872(%rbp)
	leaq	-1120(%rbp), %rbx
	movq	$0, -3864(%rbp)
	movq	$0, -3856(%rbp)
	movq	$0, -3848(%rbp)
	movq	$0, -3840(%rbp)
	movq	$0, -3832(%rbp)
	movq	$0, -3824(%rbp)
	movq	$0, -3816(%rbp)
	movq	$0, -3808(%rbp)
	movq	$0, -3800(%rbp)
	movq	$0, -3792(%rbp)
	movq	$0, -3776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%rbx, %rdi
	leaq	-3776(%rbp), %rax
	pushq	%rax
	leaq	-3792(%rbp), %rax
	leaq	-3840(%rbp), %r9
	pushq	%rax
	leaq	-3800(%rbp), %rax
	leaq	-3856(%rbp), %rcx
	pushq	%rax
	leaq	-3808(%rbp), %rax
	leaq	-3848(%rbp), %r8
	pushq	%rax
	leaq	-3816(%rbp), %rax
	leaq	-3864(%rbp), %rdx
	pushq	%rax
	leaq	-3824(%rbp), %rax
	leaq	-3872(%rbp), %rsi
	pushq	%rax
	leaq	-3832(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_8JSRegExpENS0_6StringES5_S5_NS0_3SmiENS0_5BoolTES6_S7_NS0_15RegExpMatchInfoES6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EESG_SG_PNSA_IS6_EEPNSA_IS7_EESI_SK_PNSA_IS8_EESI_SI_
	addq	$64, %rsp
	movl	$163, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$164, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3864(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal23RegExpBuiltinsAssembler31FastLoadLastIndexBeforeSmiCheckENS0_8compiler5TNodeINS0_8JSRegExpEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3824(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movq	-3856(%rbp), %rsi
	movl	$1, %r8d
	call	_ZN2v88internal23RegExpBuiltinsAssembler18AdvanceStringIndexENS0_8compiler11SloppyTNodeINS0_6StringEEENS3_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEENS3_INS0_5BoolTEEEb@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$163, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3864(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal23RegExpBuiltinsAssembler18FastStoreLastIndexENS0_8compiler5TNodeINS0_8JSRegExpEEENS3_INS0_3SmiEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$162, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-160(%rbp), %rsi
	leaq	-64(%rbp), %rdx
	movq	%r15, %rdi
	movq	-3792(%rbp), %xmm0
	movq	-3808(%rbp), %xmm1
	movq	$0, -3728(%rbp)
	movq	-3824(%rbp), %xmm2
	movq	-3840(%rbp), %xmm3
	movq	-3856(%rbp), %xmm4
	movhps	-3776(%rbp), %xmm0
	movq	-3872(%rbp), %xmm5
	movhps	-3800(%rbp), %xmm1
	movhps	-3816(%rbp), %xmm2
	movhps	-3832(%rbp), %xmm3
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-3848(%rbp), %xmm4
	movhps	-3864(%rbp), %xmm5
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -3744(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1809
	call	_ZdlPv@PLT
.L1809:
	movq	-4088(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -864(%rbp)
	je	.L1810
.L2052:
	movq	-4088(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3872(%rbp)
	movq	$0, -3864(%rbp)
	movq	$0, -3856(%rbp)
	movq	$0, -3848(%rbp)
	movq	$0, -3840(%rbp)
	movq	$0, -3832(%rbp)
	movq	$0, -3824(%rbp)
	movq	$0, -3816(%rbp)
	movq	$0, -3808(%rbp)
	movq	$0, -3800(%rbp)
	movq	$0, -3792(%rbp)
	movq	$0, -3776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r13, %rdi
	leaq	-3776(%rbp), %rax
	pushq	%rax
	leaq	-3792(%rbp), %rax
	leaq	-3856(%rbp), %rcx
	pushq	%rax
	leaq	-3800(%rbp), %rax
	leaq	-3840(%rbp), %r9
	pushq	%rax
	leaq	-3808(%rbp), %rax
	leaq	-3848(%rbp), %r8
	pushq	%rax
	leaq	-3816(%rbp), %rax
	leaq	-3864(%rbp), %rdx
	pushq	%rax
	leaq	-3824(%rbp), %rax
	leaq	-3872(%rbp), %rsi
	pushq	%rax
	leaq	-3832(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_8JSRegExpENS0_6StringES5_S5_NS0_3SmiENS0_5BoolTES6_S7_NS0_15RegExpMatchInfoES6_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSA_IS4_EEPNSA_IS5_EESG_SG_PNSA_IS6_EEPNSA_IS7_EESI_SK_PNSA_IS8_EESI_SI_
	addq	$64, %rsp
	movl	$144, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3872(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movaps	%xmm0, -3744(%rbp)
	movq	%rax, -160(%rbp)
	movq	-3864(%rbp), %rax
	movq	$0, -3728(%rbp)
	movq	%rax, -152(%rbp)
	movq	-3856(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-3848(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-3840(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-3832(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-3824(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-3816(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-3808(%rbp), %rax
	movq	%rax, -96(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm1
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-160(%rbp), %xmm5
	leaq	72(%rax), %rdx
	movq	%rax, -3744(%rbp)
	movdqa	-144(%rbp), %xmm3
	movq	-3952(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm2, 48(%rax)
	movq	%rdx, -3728(%rbp)
	movq	%rdx, -3736(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1811
	call	_ZdlPv@PLT
.L1811:
	movq	-4064(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -672(%rbp)
	je	.L1812
.L2053:
	movq	-3912(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -3864(%rbp)
	leaq	-736(%rbp), %rbx
	movq	$0, -3856(%rbp)
	movq	$0, -3848(%rbp)
	movq	$0, -3840(%rbp)
	movq	$0, -3832(%rbp)
	movq	$0, -3824(%rbp)
	movq	$0, -3816(%rbp)
	movq	$0, -3808(%rbp)
	movq	$0, -3800(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3800(%rbp), %rax
	movq	%rbx, %rdi
	leaq	-3840(%rbp), %r8
	pushq	%rax
	leaq	-3808(%rbp), %rax
	leaq	-3848(%rbp), %rcx
	pushq	%rax
	leaq	-3816(%rbp), %rax
	leaq	-3832(%rbp), %r9
	pushq	%rax
	leaq	-3824(%rbp), %rax
	leaq	-3856(%rbp), %rdx
	pushq	%rax
	leaq	-3864(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_8JSRegExpENS0_6StringES5_S5_NS0_3SmiENS0_5BoolTES6_S7_EE10CreatePhisEPNS1_5TNodeIS3_EEPNS9_IS4_EEPNS9_IS5_EESF_SF_PNS9_IS6_EEPNS9_IS7_EESH_SJ_
	addq	$32, %rsp
	movl	$168, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3848(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler21LoadStringLengthAsSmiENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-3792(%rbp), %r10
	movq	%r14, %rsi
	movq	%r10, %rdi
	movq	%r10, -4088(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4088(%rbp), %r10
	movq	-3864(%rbp), %r9
	movq	-3824(%rbp), %rcx
	movq	-3848(%rbp), %rax
	movq	%r10, %rdi
	movq	%r9, -4112(%rbp)
	movq	%rcx, -4080(%rbp)
	movq	%rax, -4064(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$707, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-4088(%rbp), %r10
	movq	-3744(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-160(%rbp), %r11
	xorl	%esi, %esi
	movq	%rbx, -144(%rbp)
	movl	$3, %ebx
	movq	-4088(%rbp), %r10
	movq	%rax, %r8
	movq	-4064(%rbp), %xmm0
	pushq	%rbx
	movq	-4112(%rbp), %r9
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	leaq	-3776(%rbp), %rdx
	pushq	%r11
	movq	-3728(%rbp), %rax
	movhps	-4080(%rbp), %xmm0
	movq	%r10, %rdi
	movq	%rcx, -3776(%rbp)
	movl	$1, %ecx
	movq	%r11, -4080(%rbp)
	movaps	%xmm0, -160(%rbp)
	movq	%r10, -4064(%rbp)
	movq	%rax, -3768(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-4064(%rbp), %r10
	movq	%rax, %rbx
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3832(%rbp), %rdx
	movq	%rbx, %rcx
	movq	%r14, %rdi
	movq	-3864(%rbp), %rsi
	call	_ZN2v88internal12StringAdd_82EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6StringEEES8_@PLT
	movq	-3864(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movq	-4080(%rbp), %r11
	movq	%rax, -128(%rbp)
	movq	%rdx, -160(%rbp)
	movq	-3856(%rbp), %rdx
	movq	%r11, %rsi
	movaps	%xmm0, -3744(%rbp)
	movq	%rdx, -152(%rbp)
	movq	-3848(%rbp), %rdx
	movq	$0, -3728(%rbp)
	movq	%rdx, -144(%rbp)
	movq	-3840(%rbp), %rdx
	movq	%rdx, -136(%rbp)
	leaq	-120(%rbp), %rdx
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-544(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3744(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L1813
	call	_ZdlPv@PLT
.L1813:
	movq	-4024(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1812
	.p2align 4,,10
	.p2align 3
.L2054:
	movq	-4024(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-544(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -3728(%rbp)
	movaps	%xmm0, -3744(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	movl	$117901063, (%rax)
	leaq	5(%rax), %rdx
	movb	$7, 4(%rax)
	movq	%rax, -3744(%rbp)
	movq	%rdx, -3728(%rbp)
	movq	%rdx, -3736(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3744(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1815
	call	_ZdlPv@PLT
.L1815:
	movq	(%rbx), %rax
	movl	$128, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	24(%rax), %rbx
	movq	%rcx, -4064(%rbp)
	movq	16(%rax), %rcx
	movq	%rbx, -4088(%rbp)
	movq	32(%rax), %rbx
	movq	%rcx, -4080(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-120(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rbx, -128(%rbp)
	movq	%r14, %xmm0
	leaq	-160(%rbp), %rsi
	leaq	-352(%rbp), %r14
	movq	$0, -3728(%rbp)
	movhps	-4064(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-4080(%rbp), %xmm0
	movhps	-4088(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -3744(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3744(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1816
	call	_ZdlPv@PLT
.L1816:
	movq	-3904(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1814
	.p2align 4,,10
	.p2align 3
.L2056:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3856(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	-3848(%rbp), %rdx
	movq	-3840(%rbp), %rax
	movq	-3968(%rbp), %rsi
	movq	%r13, %rdi
	movaps	%xmm0, -3776(%rbp)
	movq	%rcx, -160(%rbp)
	movq	-3832(%rbp), %rcx
	movq	%rdx, -152(%rbp)
	movq	%rcx, -136(%rbp)
	movq	-3824(%rbp), %rcx
	movq	%rdx, -88(%rbp)
	leaq	-72(%rbp), %rdx
	movq	%rcx, -128(%rbp)
	movq	-3816(%rbp), %rcx
	movq	%rax, -144(%rbp)
	movq	%rcx, -120(%rbp)
	movq	-3808(%rbp), %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -112(%rbp)
	movq	-3800(%rbp), %rcx
	movq	$0, -3760(%rbp)
	movq	%rcx, -104(%rbp)
	movq	-3792(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2656(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3776(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1784
	call	_ZdlPv@PLT
.L1784:
	movq	-4056(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1783
.L2055:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22547:
	.size	_ZN2v88internal27RegExpReplaceFastString_325EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEESA_, .-_ZN2v88internal27RegExpReplaceFastString_325EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEESA_
	.section	.text._ZN2v88internal22RegExpReplaceAssembler25GenerateRegExpReplaceImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22RegExpReplaceAssembler25GenerateRegExpReplaceImplEv
	.type	_ZN2v88internal22RegExpReplaceAssembler25GenerateRegExpReplaceImplEv, @function
_ZN2v88internal22RegExpReplaceAssembler25GenerateRegExpReplaceImplEv:
.LFB22577:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-2768(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2640(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$3032, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, -2824(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -2960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -2976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$2, %esi
	movq	%r12, %rdi
	leaq	-2824(%rbp), %r12
	movq	%rax, -2992(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$96, %edi
	movq	$0, -2632(%rbp)
	movq	%rax, -3008(%rbp)
	movq	-2824(%rbp), %rax
	movq	$0, -2624(%rbp)
	movq	%rax, -2640(%rbp)
	movq	$0, -2616(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	96(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movq	%rax, -2632(%rbp)
	leaq	-2584(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2616(%rbp)
	movq	%rdx, -2624(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2600(%rbp)
	movq	%rax, -2840(%rbp)
	movq	$0, -2608(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2824(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2440(%rbp)
	movq	$0, -2432(%rbp)
	movq	%rax, -2448(%rbp)
	movq	$0, -2424(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2440(%rbp)
	leaq	-2392(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2424(%rbp)
	movq	%rdx, -2432(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2408(%rbp)
	movq	%rax, -2928(%rbp)
	movq	$0, -2416(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2824(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2248(%rbp)
	movq	$0, -2240(%rbp)
	movq	%rax, -2256(%rbp)
	movq	$0, -2232(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -2248(%rbp)
	leaq	-2200(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2232(%rbp)
	movq	%rdx, -2240(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2216(%rbp)
	movq	%rax, -2864(%rbp)
	movq	$0, -2224(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2824(%rbp), %rax
	movl	$144, %edi
	movq	$0, -2056(%rbp)
	movq	$0, -2048(%rbp)
	movq	%rax, -2064(%rbp)
	movq	$0, -2040(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -2056(%rbp)
	leaq	-2008(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2040(%rbp)
	movq	%rdx, -2048(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2024(%rbp)
	movq	%rax, -2880(%rbp)
	movq	$0, -2032(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2824(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1864(%rbp)
	movq	$0, -1856(%rbp)
	movq	%rax, -1872(%rbp)
	movq	$0, -1848(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1864(%rbp)
	leaq	-1816(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1848(%rbp)
	movq	%rdx, -1856(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1832(%rbp)
	movq	%rax, -2888(%rbp)
	movq	$0, -1840(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2824(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1672(%rbp)
	movq	$0, -1664(%rbp)
	movq	%rax, -1680(%rbp)
	movq	$0, -1656(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1672(%rbp)
	leaq	-1624(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1656(%rbp)
	movq	%rdx, -1664(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1640(%rbp)
	movq	%rax, -2904(%rbp)
	movq	$0, -1648(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2824(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1480(%rbp)
	movq	$0, -1472(%rbp)
	movq	%rax, -1488(%rbp)
	movq	$0, -1464(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1480(%rbp)
	leaq	-1432(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1464(%rbp)
	movq	%rdx, -1472(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1448(%rbp)
	movq	%rax, -2848(%rbp)
	movq	$0, -1456(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2824(%rbp), %rax
	movl	$120, %edi
	movq	$0, -1288(%rbp)
	movq	$0, -1280(%rbp)
	movq	%rax, -1296(%rbp)
	movq	$0, -1272(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -1288(%rbp)
	leaq	-1240(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1272(%rbp)
	movq	%rdx, -1280(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1256(%rbp)
	movq	%rax, -2872(%rbp)
	movq	$0, -1264(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2824(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1096(%rbp)
	movq	$0, -1088(%rbp)
	movq	%rax, -1104(%rbp)
	movq	$0, -1080(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1096(%rbp)
	leaq	-1048(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1080(%rbp)
	movq	%rdx, -1088(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1064(%rbp)
	movq	%rax, -2936(%rbp)
	movq	$0, -1072(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2824(%rbp), %rax
	movl	$240, %edi
	movq	$0, -904(%rbp)
	movq	$0, -896(%rbp)
	movq	%rax, -912(%rbp)
	movq	$0, -888(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -904(%rbp)
	leaq	-856(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -888(%rbp)
	movq	%rdx, -896(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -872(%rbp)
	movq	%rax, -2896(%rbp)
	movq	$0, -880(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2824(%rbp), %rax
	movl	$216, %edi
	movq	$0, -712(%rbp)
	movq	$0, -704(%rbp)
	movq	%rax, -720(%rbp)
	movq	$0, -696(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -712(%rbp)
	leaq	-664(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -696(%rbp)
	movq	%rdx, -704(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -680(%rbp)
	movq	%rax, -2920(%rbp)
	movq	$0, -688(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2824(%rbp), %rax
	movl	$216, %edi
	movq	$0, -520(%rbp)
	movq	$0, -512(%rbp)
	movq	%rax, -528(%rbp)
	movq	$0, -504(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -520(%rbp)
	leaq	-472(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -504(%rbp)
	movq	%rdx, -512(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -488(%rbp)
	movq	%rax, -2912(%rbp)
	movq	$0, -496(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2824(%rbp), %rax
	movl	$192, %edi
	movq	$0, -328(%rbp)
	movq	$0, -320(%rbp)
	movq	%rax, -336(%rbp)
	movq	$0, -312(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%r12, %rsi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -328(%rbp)
	leaq	-280(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -312(%rbp)
	movq	%rdx, -320(%rbp)
	xorl	%edx, %edx
	movq	%rax, -2856(%rbp)
	movups	%xmm0, -296(%rbp)
	movq	$0, -304(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movq	-2960(%rbp), %xmm1
	movaps	%xmm0, -2768(%rbp)
	movhps	-2976(%rbp), %xmm1
	movq	$0, -2752(%rbp)
	movaps	%xmm1, -144(%rbp)
	movq	-2992(%rbp), %xmm1
	movhps	-3008(%rbp), %xmm1
	movaps	%xmm1, -128(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movq	%r13, %rdi
	movq	%r15, %rsi
	movdqa	-144(%rbp), %xmm3
	leaq	32(%rax), %rdx
	movq	%rax, -2768(%rbp)
	movups	%xmm4, 16(%rax)
	movups	%xmm3, (%rax)
	movq	%rdx, -2752(%rbp)
	movq	%rdx, -2760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2768(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2058
	call	_ZdlPv@PLT
.L2058:
	movq	-2840(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2576(%rbp)
	jne	.L2368
	cmpq	$0, -2384(%rbp)
	jne	.L2369
.L2064:
	cmpq	$0, -2192(%rbp)
	jne	.L2370
.L2067:
	cmpq	$0, -2000(%rbp)
	jne	.L2371
.L2071:
	cmpq	$0, -1808(%rbp)
	jne	.L2372
.L2074:
	cmpq	$0, -1616(%rbp)
	jne	.L2373
.L2077:
	cmpq	$0, -1424(%rbp)
	jne	.L2374
.L2080:
	cmpq	$0, -1232(%rbp)
	jne	.L2375
.L2082:
	cmpq	$0, -1040(%rbp)
	jne	.L2376
.L2087:
	cmpq	$0, -848(%rbp)
	jne	.L2377
.L2090:
	cmpq	$0, -656(%rbp)
	jne	.L2378
.L2094:
	cmpq	$0, -464(%rbp)
	jne	.L2379
.L2097:
	cmpq	$0, -272(%rbp)
	jne	.L2380
.L2103:
	movq	-2856(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2105
	call	_ZdlPv@PLT
.L2105:
	movq	-320(%rbp), %rbx
	movq	-328(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2106
	.p2align 4,,10
	.p2align 3
.L2110:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2107
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2110
.L2108:
	movq	-328(%rbp), %r13
.L2106:
	testq	%r13, %r13
	je	.L2111
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2111:
	movq	-2912(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-496(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2112
	call	_ZdlPv@PLT
.L2112:
	movq	-512(%rbp), %rbx
	movq	-520(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2113
	.p2align 4,,10
	.p2align 3
.L2117:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2114
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2117
.L2115:
	movq	-520(%rbp), %r13
.L2113:
	testq	%r13, %r13
	je	.L2118
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2118:
	movq	-2920(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2119
	call	_ZdlPv@PLT
.L2119:
	movq	-704(%rbp), %rbx
	movq	-712(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2120
	.p2align 4,,10
	.p2align 3
.L2124:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2121
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2124
.L2122:
	movq	-712(%rbp), %r13
.L2120:
	testq	%r13, %r13
	je	.L2125
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2125:
	movq	-2896(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2126
	call	_ZdlPv@PLT
.L2126:
	movq	-896(%rbp), %rbx
	movq	-904(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2127
	.p2align 4,,10
	.p2align 3
.L2131:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2128
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2131
.L2129:
	movq	-904(%rbp), %r13
.L2127:
	testq	%r13, %r13
	je	.L2132
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2132:
	movq	-2936(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2133
	call	_ZdlPv@PLT
.L2133:
	movq	-1088(%rbp), %rbx
	movq	-1096(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2134
	.p2align 4,,10
	.p2align 3
.L2138:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2135
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2138
.L2136:
	movq	-1096(%rbp), %r13
.L2134:
	testq	%r13, %r13
	je	.L2139
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2139:
	movq	-2872(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2140
	call	_ZdlPv@PLT
.L2140:
	movq	-1280(%rbp), %rbx
	movq	-1288(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2141
	.p2align 4,,10
	.p2align 3
.L2145:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2142
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2145
.L2143:
	movq	-1288(%rbp), %r13
.L2141:
	testq	%r13, %r13
	je	.L2146
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2146:
	movq	-2848(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1456(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2147
	call	_ZdlPv@PLT
.L2147:
	movq	-1472(%rbp), %rbx
	movq	-1480(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2148
	.p2align 4,,10
	.p2align 3
.L2152:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2149
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2152
.L2150:
	movq	-1480(%rbp), %r13
.L2148:
	testq	%r13, %r13
	je	.L2153
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2153:
	movq	-2904(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1648(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2154
	call	_ZdlPv@PLT
.L2154:
	movq	-1664(%rbp), %rbx
	movq	-1672(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2155
	.p2align 4,,10
	.p2align 3
.L2159:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2156
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2159
.L2157:
	movq	-1672(%rbp), %r13
.L2155:
	testq	%r13, %r13
	je	.L2160
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2160:
	movq	-2888(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1840(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2161
	call	_ZdlPv@PLT
.L2161:
	movq	-1856(%rbp), %rbx
	movq	-1864(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2162
	.p2align 4,,10
	.p2align 3
.L2166:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2163
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2166
.L2164:
	movq	-1864(%rbp), %r13
.L2162:
	testq	%r13, %r13
	je	.L2167
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2167:
	movq	-2880(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2032(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2168
	call	_ZdlPv@PLT
.L2168:
	movq	-2048(%rbp), %rbx
	movq	-2056(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2169
	.p2align 4,,10
	.p2align 3
.L2173:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2170
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2173
.L2171:
	movq	-2056(%rbp), %r13
.L2169:
	testq	%r13, %r13
	je	.L2174
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2174:
	movq	-2864(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2175
	call	_ZdlPv@PLT
.L2175:
	movq	-2240(%rbp), %rbx
	movq	-2248(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2176
	.p2align 4,,10
	.p2align 3
.L2180:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2177
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2180
.L2178:
	movq	-2248(%rbp), %r13
.L2176:
	testq	%r13, %r13
	je	.L2181
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2181:
	movq	-2928(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2416(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2182
	call	_ZdlPv@PLT
.L2182:
	movq	-2432(%rbp), %rbx
	movq	-2440(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2183
	.p2align 4,,10
	.p2align 3
.L2187:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2184
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2187
.L2185:
	movq	-2440(%rbp), %r13
.L2183:
	testq	%r13, %r13
	je	.L2188
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2188:
	movq	-2840(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2608(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2189
	call	_ZdlPv@PLT
.L2189:
	movq	-2624(%rbp), %rbx
	movq	-2632(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2190
	.p2align 4,,10
	.p2align 3
.L2194:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2191
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2194
.L2192:
	movq	-2632(%rbp), %r13
.L2190:
	testq	%r13, %r13
	je	.L2195
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2195:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2381
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2191:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2194
	jmp	.L2192
	.p2align 4,,10
	.p2align 3
.L2184:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2187
	jmp	.L2185
	.p2align 4,,10
	.p2align 3
.L2177:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2180
	jmp	.L2178
	.p2align 4,,10
	.p2align 3
.L2170:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2173
	jmp	.L2171
	.p2align 4,,10
	.p2align 3
.L2163:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2166
	jmp	.L2164
	.p2align 4,,10
	.p2align 3
.L2156:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2159
	jmp	.L2157
	.p2align 4,,10
	.p2align 3
.L2149:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2152
	jmp	.L2150
	.p2align 4,,10
	.p2align 3
.L2142:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2145
	jmp	.L2143
	.p2align 4,,10
	.p2align 3
.L2135:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2138
	jmp	.L2136
	.p2align 4,,10
	.p2align 3
.L2128:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2131
	jmp	.L2129
	.p2align 4,,10
	.p2align 3
.L2121:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2124
	jmp	.L2122
	.p2align 4,,10
	.p2align 3
.L2107:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2110
	jmp	.L2108
	.p2align 4,,10
	.p2align 3
.L2114:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L2117
	jmp	.L2115
	.p2align 4,,10
	.p2align 3
.L2368:
	movq	-2840(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -2752(%rbp)
	movaps	%xmm0, -2768(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$134678279, (%rax)
	leaq	4(%rax), %rdx
	movq	%rax, -2768(%rbp)
	movq	%rdx, -2752(%rbp)
	movq	%rdx, -2760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2768(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2060
	call	_ZdlPv@PLT
.L2060:
	movq	(%rbx), %rax
	movl	$178, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	24(%rax), %r13
	movq	(%rax), %rbx
	movq	%rcx, -2960(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -2976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$179, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rdx
	movq	%r15, %rcx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal82Cast71UT19ATCallableApiObject17ATCallableJSProxy15JSBoundFunction10JSFunction_1405EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %xmm7
	movq	%r13, %xmm5
	movq	-2976(%rbp), %xmm6
	punpcklqdq	%xmm5, %xmm5
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movq	%rax, -96(%rbp)
	punpcklqdq	%xmm7, %xmm6
	movq	%rbx, %xmm7
	movaps	%xmm5, -2992(%rbp)
	leaq	-2800(%rbp), %r13
	movhps	-2960(%rbp), %xmm7
	movaps	%xmm6, -2976(%rbp)
	movaps	%xmm7, -2960(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm0, -2800(%rbp)
	movq	$0, -2784(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm2
	movq	-96(%rbp), %rcx
	leaq	-2256(%rbp), %rdi
	movdqa	-128(%rbp), %xmm7
	leaq	56(%rax), %rdx
	movq	%r13, %rsi
	movq	%rax, -2800(%rbp)
	movups	%xmm2, (%rax)
	movdqa	-112(%rbp), %xmm2
	movq	%rcx, 48(%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm2, 32(%rax)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2061
	call	_ZdlPv@PLT
.L2061:
	movq	-2864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2760(%rbp)
	jne	.L2382
.L2062:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2384(%rbp)
	je	.L2064
.L2369:
	movq	-2928(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2448(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2752(%rbp)
	movaps	%xmm0, -2768(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r11d
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$134678279, (%rax)
	leaq	6(%rax), %rdx
	movw	%r11w, 4(%rax)
	movq	%rax, -2768(%rbp)
	movq	%rdx, -2752(%rbp)
	movq	%rdx, -2760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2768(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2065
	call	_ZdlPv@PLT
.L2065:
	movq	(%rbx), %rax
	movl	$40, %edi
	movdqu	16(%rax), %xmm0
	movq	32(%rax), %rdx
	movdqu	(%rax), %xmm7
	movq	$0, -2752(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -112(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm0, -2768(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r15, %rsi
	movdqa	-144(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm7
	leaq	40(%rax), %rdx
	leaq	-1296(%rbp), %rdi
	movq	%rax, -2768(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm7, 16(%rax)
	movq	%rdx, -2752(%rbp)
	movq	%rdx, -2760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2768(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2066
	call	_ZdlPv@PLT
.L2066:
	movq	-2872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2192(%rbp)
	je	.L2067
.L2370:
	movq	-2864(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2256(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2752(%rbp)
	movaps	%xmm0, -2768(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r10d
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$134678279, (%rax)
	leaq	7(%rax), %rdx
	movw	%r10w, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -2768(%rbp)
	movq	%rdx, -2752(%rbp)
	movq	%rdx, -2760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2768(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2068
	call	_ZdlPv@PLT
.L2068:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rdx
	movq	16(%rax), %rsi
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	%rdx, -2992(%rbp)
	movq	32(%rax), %rdx
	movq	48(%rax), %rax
	movq	%rsi, -2976(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -3008(%rbp)
	movl	$180, %edx
	movq	%rcx, -2960(%rbp)
	movq	%rax, -3024(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal23RegExpBuiltinsAssembler14FastFlagGetterENS0_8compiler5TNodeINS0_8JSRegExpEEENS4_4FlagE@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm7
	pxor	%xmm0, %xmm0
	movq	-3008(%rbp), %xmm2
	movq	-2976(%rbp), %xmm3
	movl	$48, %edi
	movq	-2960(%rbp), %xmm4
	movaps	%xmm0, -2768(%rbp)
	movhps	-3024(%rbp), %xmm2
	movq	$0, -2752(%rbp)
	punpcklqdq	%xmm7, %xmm4
	movhps	-2992(%rbp), %xmm3
	movaps	%xmm2, -3008(%rbp)
	movaps	%xmm3, -2976(%rbp)
	movaps	%xmm4, -2960(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm3
	movq	%r15, %rsi
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm2
	leaq	48(%rax), %rdx
	leaq	-2064(%rbp), %rdi
	movq	%rax, -2768(%rbp)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm2, 32(%rax)
	movq	%rdx, -2752(%rbp)
	movq	%rdx, -2760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2768(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2069
	call	_ZdlPv@PLT
.L2069:
	movdqa	-2960(%rbp), %xmm3
	movdqa	-2976(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movdqa	-3008(%rbp), %xmm5
	movaps	%xmm0, -2768(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movq	$0, -2752(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-128(%rbp), %xmm2
	movdqa	-112(%rbp), %xmm7
	leaq	48(%rax), %rdx
	leaq	-1872(%rbp), %rdi
	movq	%rax, -2768(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rdx, -2752(%rbp)
	movq	%rdx, -2760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2768(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2070
	call	_ZdlPv@PLT
.L2070:
	movq	-2888(%rbp), %rcx
	movq	-2880(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2000(%rbp)
	je	.L2071
.L2371:
	movq	-2880(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2064(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2752(%rbp)
	movaps	%xmm0, -2768(%rbp)
	call	_Znwm@PLT
	movl	$1800, %r9d
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$134678279, (%rax)
	leaq	6(%rax), %rdx
	movw	%r9w, 4(%rax)
	movq	%rax, -2768(%rbp)
	movq	%rdx, -2752(%rbp)
	movq	%rdx, -2760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2768(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2072
	call	_ZdlPv@PLT
.L2072:
	movq	(%rbx), %rax
	movl	$181, %edx
	movq	%r12, %rdi
	movq	24(%rax), %rsi
	movq	(%rax), %r13
	movq	8(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -2992(%rbp)
	movq	32(%rax), %rsi
	movq	40(%rax), %rax
	movq	%rcx, -2960(%rbp)
	movq	%rsi, -3008(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rax, -2976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-2976(%rbp), %r8
	movq	-2960(%rbp), %rdx
	call	_ZN2v88internal35RegExpReplaceFastGlobalCallable_324EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE
	movl	$180, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -3024(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %xmm0
	movl	$56, %edi
	movq	-3024(%rbp), %rax
	movhps	-2960(%rbp), %xmm0
	movq	$0, -2752(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	movhps	-2992(%rbp), %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	-3008(%rbp), %xmm0
	movhps	-2976(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2768(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-144(%rbp), %xmm3
	leaq	56(%rax), %rdx
	leaq	-1680(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rax, -2768(%rbp)
	movq	%rdx, -2752(%rbp)
	movq	%rdx, -2760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2768(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2073
	call	_ZdlPv@PLT
.L2073:
	movq	-2904(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1808(%rbp)
	je	.L2074
.L2372:
	movq	-2888(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1872(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2752(%rbp)
	movaps	%xmm0, -2768(%rbp)
	call	_Znwm@PLT
	movl	$1800, %r8d
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$134678279, (%rax)
	leaq	6(%rax), %rdx
	movw	%r8w, 4(%rax)
	movq	%rax, -2768(%rbp)
	movq	%rdx, -2752(%rbp)
	movq	%rdx, -2760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2768(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2075
	call	_ZdlPv@PLT
.L2075:
	movq	(%rbx), %rax
	movl	$182, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	40(%rax), %rbx
	movq	(%rax), %r13
	movq	%rcx, -2960(%rbp)
	movq	16(%rax), %rcx
	movq	%rbx, -3024(%rbp)
	movq	%rcx, -2976(%rbp)
	movq	24(%rax), %rcx
	movq	%rcx, -2992(%rbp)
	movq	32(%rax), %rcx
	movq	%rcx, -3008(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdx
	movl	$297, %esi
	movq	%r15, %rdi
	movq	-2976(%rbp), %xmm0
	leaq	-144(%rbp), %rcx
	movl	$3, %r8d
	movq	%rbx, -128(%rbp)
	movhps	-2960(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$180, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %xmm0
	movl	$56, %edi
	movq	%rbx, -96(%rbp)
	movhps	-2960(%rbp), %xmm0
	movq	$0, -2752(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	-2976(%rbp), %xmm0
	movhps	-2992(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-3008(%rbp), %xmm0
	movhps	-3024(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2768(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm2
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm6
	leaq	56(%rax), %rdx
	leaq	-1488(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm7, 32(%rax)
	movq	%rax, -2768(%rbp)
	movq	%rdx, -2752(%rbp)
	movq	%rdx, -2760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2768(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2076
	call	_ZdlPv@PLT
.L2076:
	movq	-2848(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1616(%rbp)
	je	.L2077
.L2373:
	movq	-2904(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1680(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2752(%rbp)
	movaps	%xmm0, -2768(%rbp)
	call	_Znwm@PLT
	movl	$1800, %edi
	movq	%r15, %rsi
	movw	%di, 4(%rax)
	leaq	7(%rax), %rdx
	movq	%r13, %rdi
	movl	$134678279, (%rax)
	movb	$7, 6(%rax)
	movq	%rax, -2768(%rbp)
	movq	%rdx, -2752(%rbp)
	movq	%rdx, -2760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2768(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2078
	call	_ZdlPv@PLT
.L2078:
	movq	(%rbx), %rax
	movl	$56, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movq	48(%rax), %rdx
	movdqu	(%rax), %xmm3
	movq	$0, -2752(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -96(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -2768(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movdqa	-128(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-144(%rbp), %xmm4
	leaq	56(%rax), %rdx
	leaq	-1488(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rax, -2768(%rbp)
	movq	%rdx, -2752(%rbp)
	movq	%rdx, -2760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2768(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2079
	call	_ZdlPv@PLT
.L2079:
	movq	-2848(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1424(%rbp)
	je	.L2080
.L2374:
	movq	-2848(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1488(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2752(%rbp)
	movaps	%xmm0, -2768(%rbp)
	call	_Znwm@PLT
	movl	$1800, %esi
	movq	%r13, %rdi
	movw	%si, 4(%rax)
	leaq	7(%rax), %rdx
	movq	%r15, %rsi
	movl	$134678279, (%rax)
	movb	$7, 6(%rax)
	movq	%rax, -2768(%rbp)
	movq	%rdx, -2752(%rbp)
	movq	%rdx, -2760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2768(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2081
	call	_ZdlPv@PLT
.L2081:
	movq	(%rbx), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	48(%rax), %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -1232(%rbp)
	je	.L2082
.L2375:
	movq	-2872(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1296(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2752(%rbp)
	movaps	%xmm0, -2768(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$134678279, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2768(%rbp)
	movq	%rdx, -2752(%rbp)
	movq	%rdx, -2760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2768(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2083
	call	_ZdlPv@PLT
.L2083:
	movq	(%rbx), %rax
	movl	$184, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	24(%rax), %r13
	movq	%rcx, -2960(%rbp)
	movq	16(%rax), %rcx
	movq	32(%rax), %rax
	movq	%rcx, -2976(%rbp)
	movq	%rax, -2992(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$185, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$186, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ToString_InlineENS0_8compiler11SloppyTNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	movq	%rax, -3008(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$192, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rcx
	movq	%r14, %rdi
	movq	-2960(%rbp), %rdx
	call	_ZN2v88internal24Cast14ATFastJSRegExp_134EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %xmm2
	pxor	%xmm0, %xmm0
	movq	-2976(%rbp), %xmm7
	movq	-2960(%rbp), %rcx
	movl	$80, %edi
	movq	%rax, -72(%rbp)
	leaq	-2800(%rbp), %r13
	movq	-2992(%rbp), %xmm6
	punpcklqdq	%xmm2, %xmm7
	movq	-2960(%rbp), %xmm5
	movq	%rbx, %xmm2
	movhps	-2960(%rbp), %xmm2
	movq	%rcx, -80(%rbp)
	movhps	-3008(%rbp), %xmm5
	punpcklqdq	%xmm6, %xmm6
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm5, -3024(%rbp)
	movaps	%xmm6, -2992(%rbp)
	movaps	%xmm7, -2976(%rbp)
	movaps	%xmm2, -3008(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm0, -2800(%rbp)
	movq	$0, -2784(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm3
	leaq	-912(%rbp), %rdi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm6
	leaq	80(%rax), %rdx
	movq	%r13, %rsi
	movdqa	-80(%rbp), %xmm5
	movups	%xmm7, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm5, 64(%rax)
	movq	%rax, -2800(%rbp)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2084
	call	_ZdlPv@PLT
.L2084:
	movq	-2896(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2760(%rbp)
	jne	.L2383
.L2085:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1040(%rbp)
	je	.L2087
.L2376:
	movq	-2936(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1104(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -2752(%rbp)
	movaps	%xmm0, -2768(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movabsq	$506382313689908999, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%rax, -2768(%rbp)
	movq	%rdx, -2752(%rbp)
	movq	%rdx, -2760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2768(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2088
	call	_ZdlPv@PLT
.L2088:
	movq	(%rbx), %rax
	movl	$64, %edi
	movdqu	48(%rax), %xmm0
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movdqu	(%rax), %xmm7
	movq	$0, -2752(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -2768(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm3
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm6
	leaq	64(%rax), %rdx
	leaq	-336(%rbp), %rdi
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm6, 48(%rax)
	movq	%rax, -2768(%rbp)
	movq	%rdx, -2752(%rbp)
	movq	%rdx, -2760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2768(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2089
	call	_ZdlPv@PLT
.L2089:
	movq	-2856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -848(%rbp)
	je	.L2090
.L2377:
	movq	-2896(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-912(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -2752(%rbp)
	movaps	%xmm0, -2768(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movabsq	$506382313689908999, %rcx
	movq	%rcx, (%rax)
	movl	$1799, %ecx
	leaq	10(%rax), %rdx
	movw	%cx, 8(%rax)
	movq	%rax, -2768(%rbp)
	movq	%rdx, -2752(%rbp)
	movq	%rdx, -2760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2768(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2091
	call	_ZdlPv@PLT
.L2091:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	leaq	-2816(%rbp), %r13
	movq	8(%rax), %rsi
	movq	40(%rax), %rdx
	movq	24(%rax), %rbx
	movq	(%rax), %rcx
	movq	%rsi, -2992(%rbp)
	movq	16(%rax), %rsi
	movq	%rdx, -2944(%rbp)
	movq	48(%rax), %rdx
	movq	%rsi, -3008(%rbp)
	movq	56(%rax), %rsi
	movq	%rbx, -3024(%rbp)
	movq	32(%rax), %rbx
	movq	%rdx, -3056(%rbp)
	movl	$194, %edx
	movq	%rsi, -2976(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -2960(%rbp)
	movq	%rbx, -3040(%rbp)
	movq	72(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$36, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	%r15, %rdi
	movq	%rax, -3064(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$193, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -3072(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$52, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-2768(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-144(%rbp), %rsi
	movl	$3, %edi
	movq	-3072(%rbp), %rcx
	pushq	%rdi
	movq	%rax, %r8
	movq	%r13, %rdi
	movq	-2976(%rbp), %xmm0
	pushq	%rsi
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-2960(%rbp), %r9
	xorl	%esi, %esi
	movq	%rax, -2800(%rbp)
	movq	-2752(%rbp), %rax
	movhps	-3064(%rbp), %xmm0
	leaq	-2800(%rbp), %rdx
	movq	%rcx, -128(%rbp)
	movl	$1, %ecx
	movaps	%xmm0, -144(%rbp)
	movq	%rax, -2792(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	movq	%rax, -3064(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$-1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	-3064(%rbp), %r8
	movq	%r15, %rdi
	movq	%rax, %r13
	movq	%r8, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movq	-3056(%rbp), %xmm3
	movq	-3040(%rbp), %xmm4
	movaps	%xmm0, -2768(%rbp)
	movq	-2960(%rbp), %xmm6
	movq	-3008(%rbp), %xmm5
	movq	%rbx, -80(%rbp)
	movhps	-2976(%rbp), %xmm3
	movhps	-2944(%rbp), %xmm4
	movhps	-2992(%rbp), %xmm6
	movaps	%xmm3, -3056(%rbp)
	movhps	-3024(%rbp), %xmm5
	movaps	%xmm4, -3040(%rbp)
	movaps	%xmm5, -2976(%rbp)
	movaps	%xmm6, -2960(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movq	$0, -2752(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm2
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm5
	leaq	72(%rax), %rdx
	leaq	-720(%rbp), %rdi
	movq	%rcx, 64(%rax)
	movdqa	-96(%rbp), %xmm3
	movups	%xmm5, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rax, -2768(%rbp)
	movq	%rdx, -2752(%rbp)
	movq	%rdx, -2760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2768(%rbp), %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L2092
	call	_ZdlPv@PLT
.L2092:
	movdqa	-2960(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movq	%rbx, -80(%rbp)
	movl	$72, %edi
	movdqa	-2976(%rbp), %xmm6
	movdqa	-3040(%rbp), %xmm5
	movaps	%xmm0, -2768(%rbp)
	movdqa	-3056(%rbp), %xmm2
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	$0, -2752(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm7
	leaq	72(%rax), %rdx
	leaq	-528(%rbp), %rdi
	movdqa	-96(%rbp), %xmm6
	movq	%rcx, 64(%rax)
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm6, 48(%rax)
	movq	%rax, -2768(%rbp)
	movq	%rdx, -2752(%rbp)
	movq	%rdx, -2760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2768(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2093
	call	_ZdlPv@PLT
.L2093:
	movq	-2912(%rbp), %rcx
	movq	-2920(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -656(%rbp)
	je	.L2094
.L2378:
	movq	-2920(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-720(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -2752(%rbp)
	movaps	%xmm0, -2768(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movabsq	$506382313689908999, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%rax, -2768(%rbp)
	movq	%rdx, -2752(%rbp)
	movq	%rdx, -2760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2768(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2095
	call	_ZdlPv@PLT
.L2095:
	movq	(%rbx), %rax
	movl	$195, %edx
	movq	%r12, %rdi
	movq	24(%rax), %rbx
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	32(%rax), %r13
	movq	%rbx, -3008(%rbp)
	movq	40(%rax), %rbx
	movq	%rsi, -2976(%rbp)
	movq	16(%rax), %rsi
	movq	%rbx, -3024(%rbp)
	movq	48(%rax), %rbx
	movq	56(%rax), %rax
	movq	%rsi, -2992(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -2960(%rbp)
	movq	%rax, -3040(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$64, %edi
	movq	-2960(%rbp), %xmm0
	movq	$0, -2752(%rbp)
	movhps	-2976(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-2992(%rbp), %xmm0
	movhps	-3008(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%r13, %xmm0
	movhps	-3024(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%rbx, %xmm0
	movhps	-3040(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2768(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm2
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm4
	leaq	64(%rax), %rdx
	leaq	-336(%rbp), %rdi
	movups	%xmm5, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm4, 48(%rax)
	movq	%rax, -2768(%rbp)
	movq	%rdx, -2752(%rbp)
	movq	%rdx, -2760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2768(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2096
	call	_ZdlPv@PLT
.L2096:
	movq	-2856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -464(%rbp)
	je	.L2097
.L2379:
	movq	-2912(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	leaq	-528(%rbp), %r8
	movq	$0, -2960(%rbp)
	movq	%r8, -2992(%rbp)
	movq	$0, -2976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -2752(%rbp)
	movaps	%xmm0, -2768(%rbp)
	call	_Znwm@PLT
	movq	-2992(%rbp), %r8
	movq	%r15, %rsi
	movabsq	$506382313689908999, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%r8, %rdi
	movq	%rax, -2768(%rbp)
	movq	%rdx, -2752(%rbp)
	movq	%rdx, -2760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2768(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2098
	movq	%rax, -2992(%rbp)
	call	_ZdlPv@PLT
	movq	-2992(%rbp), %rax
.L2098:
	movq	(%rax), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	cmovne	%rdx, %r13
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	cmove	-2960(%rbp), %rdx
	movq	%rdx, -2960(%rbp)
	movq	56(%rax), %rdx
	movq	64(%rax), %rax
	testq	%rdx, %rdx
	cmovne	%rdx, %rbx
	testq	%rax, %rax
	cmove	-2976(%rbp), %rax
	movl	$198, %edx
	movq	%rax, -2976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %r8
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-2960(%rbp), %rcx
	movq	-2976(%rbp), %rdx
	call	_ZN2v88internal27RegExpReplaceFastString_325EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_8JSRegExpEEENS4_INS0_6StringEEESA_
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -272(%rbp)
	je	.L2103
.L2380:
	movq	-2856(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-336(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2752(%rbp)
	movaps	%xmm0, -2768(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movabsq	$506382313689908999, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2768(%rbp)
	movq	%rdx, -2752(%rbp)
	movq	%rdx, -2760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2768(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2104
	call	_ZdlPv@PLT
.L2104:
	movq	(%rbx), %rax
	movl	$201, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	movq	16(%rax), %rcx
	movq	(%rax), %r13
	movq	56(%rax), %rbx
	movq	%rcx, -2960(%rbp)
	movq	48(%rax), %rcx
	movq	%rcx, -2976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdx
	movl	$295, %esi
	movq	%r15, %rdi
	movq	-2976(%rbp), %xmm0
	leaq	-144(%rbp), %rcx
	movl	$3, %r8d
	movq	%rbx, -128(%rbp)
	movhps	-2960(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler15CallRuntimeImplENS0_7Runtime10FunctionIdENS1_5TNodeINS0_6ObjectEEESt16initializer_listIS7_E@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L2103
	.p2align 4,,10
	.p2align 3
.L2382:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movdqa	-2960(%rbp), %xmm3
	movaps	%xmm0, -2800(%rbp)
	movaps	%xmm3, -144(%rbp)
	movdqa	-2976(%rbp), %xmm3
	movq	$0, -2784(%rbp)
	movaps	%xmm3, -128(%rbp)
	movdqa	-2992(%rbp), %xmm3
	movaps	%xmm3, -112(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm2
	leaq	48(%rax), %rdx
	leaq	-2448(%rbp), %rdi
	movq	%rax, -2800(%rbp)
	movups	%xmm6, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm2, 32(%rax)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2063
	call	_ZdlPv@PLT
.L2063:
	movq	-2928(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2062
	.p2align 4,,10
	.p2align 3
.L2383:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-3008(%rbp), %xmm7
	movdqa	-2976(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movdqa	-2992(%rbp), %xmm4
	movdqa	-3024(%rbp), %xmm6
	movl	$72, %edi
	movaps	%xmm0, -2800(%rbp)
	movq	-2960(%rbp), %rax
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movq	%rax, -80(%rbp)
	movq	$0, -2784(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm2
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm5
	leaq	72(%rax), %rdx
	leaq	-1104(%rbp), %rdi
	movdqa	-96(%rbp), %xmm3
	movq	%rcx, 64(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rax, -2800(%rbp)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2086
	call	_ZdlPv@PLT
.L2086:
	movq	-2936(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L2085
.L2381:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22577:
	.size	_ZN2v88internal22RegExpReplaceAssembler25GenerateRegExpReplaceImplEv, .-_ZN2v88internal22RegExpReplaceAssembler25GenerateRegExpReplaceImplEv
	.section	.rodata._ZN2v88internal8Builtins22Generate_RegExpReplaceEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"RegExpReplace"
	.section	.text._ZN2v88internal8Builtins22Generate_RegExpReplaceEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins22Generate_RegExpReplaceEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins22Generate_RegExpReplaceEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins22Generate_RegExpReplaceEPNS0_8compiler18CodeAssemblerStateE:
.LFB22573:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$2198, %ecx
	leaq	.LC4(%rip), %rdx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$869, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L2388
.L2385:
	movq	%r13, %rdi
	call	_ZN2v88internal22RegExpReplaceAssembler25GenerateRegExpReplaceImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2389
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2388:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L2385
.L2389:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22573:
	.size	_ZN2v88internal8Builtins22Generate_RegExpReplaceEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins22Generate_RegExpReplaceEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal43RegExpReplaceCallableNoExplicitCaptures_322EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_7IntPtrTEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal43RegExpReplaceCallableNoExplicitCaptures_322EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_7IntPtrTEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE, @function
_GLOBAL__sub_I__ZN2v88internal43RegExpReplaceCallableNoExplicitCaptures_322EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_7IntPtrTEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE:
.LFB30128:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE30128:
	.size	_GLOBAL__sub_I__ZN2v88internal43RegExpReplaceCallableNoExplicitCaptures_322EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_7IntPtrTEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE, .-_GLOBAL__sub_I__ZN2v88internal43RegExpReplaceCallableNoExplicitCaptures_322EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_7IntPtrTEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal43RegExpReplaceCallableNoExplicitCaptures_322EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10FixedArrayEEENS4_INS0_7IntPtrTEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE
	.weak	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_14FixedArrayBaseEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEvE5valueE:
	.byte	8
	.byte	7
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC6:
	.byte	7
	.byte	7
	.byte	5
	.byte	7
	.byte	7
	.byte	6
	.byte	5
	.byte	8
	.byte	6
	.byte	7
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	7
	.byte	5
	.align 16
.LC7:
	.byte	7
	.byte	7
	.byte	5
	.byte	7
	.byte	7
	.byte	6
	.byte	5
	.byte	8
	.byte	7
	.byte	8
	.byte	7
	.byte	7
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.align 16
.LC9:
	.byte	7
	.byte	7
	.byte	5
	.byte	7
	.byte	7
	.byte	6
	.byte	5
	.byte	7
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	7
	.byte	5
	.byte	5
	.byte	5
	.align 16
.LC10:
	.byte	7
	.byte	7
	.byte	5
	.byte	7
	.byte	5
	.byte	7
	.byte	8
	.byte	7
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	7
	.byte	5
	.byte	5
	.byte	5
	.align 16
.LC11:
	.byte	7
	.byte	7
	.byte	5
	.byte	7
	.byte	5
	.byte	7
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	7
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.byte	7
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
