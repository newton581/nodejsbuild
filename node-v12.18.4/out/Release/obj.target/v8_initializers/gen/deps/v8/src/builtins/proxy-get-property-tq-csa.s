	.file	"proxy-get-property-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectENS0_3SmiENS0_10JSReceiverENS0_10HeapObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EE,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectENS0_3SmiENS0_10JSReceiverENS0_10HeapObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectENS0_3SmiENS0_10JSReceiverENS0_10HeapObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EE
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectENS0_3SmiENS0_10JSReceiverENS0_10HeapObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EE, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectENS0_3SmiENS0_10JSReceiverENS0_10HeapObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EE:
.LFB26559:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$7, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$1798, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movl	$134678279, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L8
	movq	%rax, -112(%rbp)
	call	_ZdlPv@PLT
	movq	-112(%rbp), %rax
.L8:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L9
	movq	%rdx, (%r15)
.L9:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L10
	movq	%rdx, (%r14)
.L10:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L11
	movq	%rdx, 0(%r13)
.L11:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L12
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L12:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L13
	movq	%rdx, (%rbx)
.L13:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L14
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L14:
	movq	48(%rax), %rax
	testq	%rax, %rax
	je	.L7
	movq	-104(%rbp), %rbx
	movq	%rax, (%rbx)
.L7:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L42
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L42:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26559:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectENS0_3SmiENS0_10JSReceiverENS0_10HeapObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EE, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectENS0_3SmiENS0_10JSReceiverENS0_10HeapObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EE
	.section	.rodata._ZN2v88internal25ProxyGetPropertyAssembler28GenerateProxyGetPropertyImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../../deps/v8/../../deps/v8/src/builtins/proxy-get-property.tq"
	.section	.rodata._ZN2v88internal25ProxyGetPropertyAssembler28GenerateProxyGetPropertyImplEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	"get"
	.section	.text._ZN2v88internal25ProxyGetPropertyAssembler28GenerateProxyGetPropertyImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25ProxyGetPropertyAssembler28GenerateProxyGetPropertyImplEv
	.type	_ZN2v88internal25ProxyGetPropertyAssembler28GenerateProxyGetPropertyImplEv, @function
_ZN2v88internal25ProxyGetPropertyAssembler28GenerateProxyGetPropertyImplEv:
.LFB22418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-2624(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2752(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$3032, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -2848(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -2976(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -2992(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, -3008(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	leaq	-2848(%rbp), %r12
	movq	%rax, -3024(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$120, %edi
	movq	$0, -2616(%rbp)
	movq	$0, -2608(%rbp)
	movq	%rax, %rbx
	movq	-2848(%rbp), %rax
	movq	$0, -2600(%rbp)
	movq	%rax, -2624(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -2616(%rbp)
	leaq	-2568(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2600(%rbp)
	movq	%rdx, -2608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2584(%rbp)
	movq	%rax, -2856(%rbp)
	movq	$0, -2592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2848(%rbp), %rax
	movl	$192, %edi
	movq	$0, -2424(%rbp)
	movq	$0, -2416(%rbp)
	movq	%rax, -2432(%rbp)
	movq	$0, -2408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -2424(%rbp)
	leaq	-2376(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2408(%rbp)
	movq	%rdx, -2416(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2392(%rbp)
	movq	%rax, -2944(%rbp)
	movq	$0, -2400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2848(%rbp), %rax
	movl	$216, %edi
	movq	$0, -2232(%rbp)
	movq	$0, -2224(%rbp)
	movq	%rax, -2240(%rbp)
	movq	$0, -2216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -2232(%rbp)
	leaq	-2184(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2216(%rbp)
	movq	%rdx, -2224(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2200(%rbp)
	movq	%rax, -2864(%rbp)
	movq	$0, -2208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2848(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2040(%rbp)
	movq	$0, -2032(%rbp)
	movq	%rax, -2048(%rbp)
	movq	$0, -2024(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -2040(%rbp)
	leaq	-1992(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2024(%rbp)
	movq	%rdx, -2032(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2008(%rbp)
	movq	%rax, -2872(%rbp)
	movq	$0, -2016(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2848(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1848(%rbp)
	movq	$0, -1840(%rbp)
	movq	%rax, -1856(%rbp)
	movq	$0, -1832(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1848(%rbp)
	leaq	-1800(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1832(%rbp)
	movq	%rdx, -1840(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1816(%rbp)
	movq	%rax, -2880(%rbp)
	movq	$0, -1824(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2848(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1656(%rbp)
	movq	$0, -1648(%rbp)
	movq	%rax, -1664(%rbp)
	movq	$0, -1640(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1656(%rbp)
	leaq	-1608(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1640(%rbp)
	movq	%rdx, -1648(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1624(%rbp)
	movq	%rax, -2936(%rbp)
	movq	$0, -1632(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2848(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1464(%rbp)
	movq	$0, -1456(%rbp)
	movq	%rax, -1472(%rbp)
	movq	$0, -1448(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1464(%rbp)
	leaq	-1416(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1448(%rbp)
	movq	%rdx, -1456(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1432(%rbp)
	movq	%rax, -2888(%rbp)
	movq	$0, -1440(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2848(%rbp), %rax
	movl	$144, %edi
	movq	$0, -1272(%rbp)
	movq	$0, -1264(%rbp)
	movq	%rax, -1280(%rbp)
	movq	$0, -1256(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -1272(%rbp)
	leaq	-1224(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1256(%rbp)
	movq	%rdx, -1264(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1240(%rbp)
	movq	%rax, -2912(%rbp)
	movq	$0, -1248(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2848(%rbp), %rax
	movl	$168, %edi
	movq	$0, -1080(%rbp)
	movq	$0, -1072(%rbp)
	movq	%rax, -1088(%rbp)
	movq	$0, -1064(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1080(%rbp)
	leaq	-1032(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1064(%rbp)
	movq	%rdx, -1072(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1048(%rbp)
	movq	%rax, -2928(%rbp)
	movq	$0, -1056(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2848(%rbp), %rax
	movl	$192, %edi
	movq	$0, -888(%rbp)
	movq	$0, -880(%rbp)
	movq	%rax, -896(%rbp)
	movq	$0, -872(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -888(%rbp)
	leaq	-840(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -872(%rbp)
	movq	%rdx, -880(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -856(%rbp)
	movq	%rax, -2952(%rbp)
	movq	$0, -864(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2848(%rbp), %rax
	movl	$216, %edi
	movq	$0, -696(%rbp)
	movq	$0, -688(%rbp)
	movq	%rax, -704(%rbp)
	movq	$0, -680(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -696(%rbp)
	leaq	-648(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -680(%rbp)
	movq	%rdx, -688(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -664(%rbp)
	movq	%rax, -2896(%rbp)
	movq	$0, -672(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2848(%rbp), %rax
	movl	$168, %edi
	movq	$0, -504(%rbp)
	movq	$0, -496(%rbp)
	movq	%rax, -512(%rbp)
	movq	$0, -488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -504(%rbp)
	leaq	-456(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -488(%rbp)
	movq	%rdx, -496(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -472(%rbp)
	movq	%rax, -2904(%rbp)
	movq	$0, -480(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-2848(%rbp), %rax
	movl	$192, %edi
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -296(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -312(%rbp)
	leaq	-264(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	xorl	%edx, %edx
	movq	%rax, -2920(%rbp)
	movups	%xmm0, -280(%rbp)
	movq	$0, -288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	-2976(%rbp), %xmm1
	movaps	%xmm0, -2752(%rbp)
	movhps	-2992(%rbp), %xmm1
	movq	%rbx, -96(%rbp)
	movaps	%xmm1, -128(%rbp)
	movq	-3008(%rbp), %xmm1
	movq	$0, -2736(%rbp)
	movhps	-3024(%rbp), %xmm1
	movaps	%xmm1, -112(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movdqa	-128(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm4
	leaq	40(%rax), %rdx
	movq	%rax, -2752(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L44
	call	_ZdlPv@PLT
.L44:
	movq	-2856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2560(%rbp)
	jne	.L334
	cmpq	$0, -2368(%rbp)
	jne	.L335
.L50:
	cmpq	$0, -2176(%rbp)
	jne	.L336
.L53:
	cmpq	$0, -1984(%rbp)
	jne	.L337
.L55:
	cmpq	$0, -1792(%rbp)
	jne	.L338
.L57:
	cmpq	$0, -1600(%rbp)
	jne	.L339
.L62:
	cmpq	$0, -1408(%rbp)
	jne	.L340
.L64:
	cmpq	$0, -1216(%rbp)
	jne	.L341
.L67:
	cmpq	$0, -1024(%rbp)
	jne	.L342
.L69:
	cmpq	$0, -832(%rbp)
	jne	.L343
.L74:
	cmpq	$0, -640(%rbp)
	jne	.L344
.L77:
	cmpq	$0, -448(%rbp)
	jne	.L345
.L80:
	cmpq	$0, -256(%rbp)
	jne	.L346
.L82:
	movq	-2920(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L88
	call	_ZdlPv@PLT
.L88:
	movq	-304(%rbp), %rbx
	movq	-312(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L89
	.p2align 4,,10
	.p2align 3
.L93:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L90
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L93
.L91:
	movq	-312(%rbp), %r13
.L89:
	testq	%r13, %r13
	je	.L94
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L94:
	movq	-2904(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-480(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L95
	call	_ZdlPv@PLT
.L95:
	movq	-496(%rbp), %rbx
	movq	-504(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L96
	.p2align 4,,10
	.p2align 3
.L100:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L97
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L100
.L98:
	movq	-504(%rbp), %r13
.L96:
	testq	%r13, %r13
	je	.L101
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L101:
	movq	-2896(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-672(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L102
	call	_ZdlPv@PLT
.L102:
	movq	-688(%rbp), %rbx
	movq	-696(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L103
	.p2align 4,,10
	.p2align 3
.L107:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L104
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L107
.L105:
	movq	-696(%rbp), %r13
.L103:
	testq	%r13, %r13
	je	.L108
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L108:
	movq	-2952(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-864(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L109
	call	_ZdlPv@PLT
.L109:
	movq	-880(%rbp), %rbx
	movq	-888(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L110
	.p2align 4,,10
	.p2align 3
.L114:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L111
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L114
.L112:
	movq	-888(%rbp), %r13
.L110:
	testq	%r13, %r13
	je	.L115
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L115:
	movq	-2928(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L116
	call	_ZdlPv@PLT
.L116:
	movq	-1072(%rbp), %rbx
	movq	-1080(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L117
	.p2align 4,,10
	.p2align 3
.L121:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L118
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L121
.L119:
	movq	-1080(%rbp), %r13
.L117:
	testq	%r13, %r13
	je	.L122
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L122:
	movq	-2912(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L123
	call	_ZdlPv@PLT
.L123:
	movq	-1264(%rbp), %rbx
	movq	-1272(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L124
	.p2align 4,,10
	.p2align 3
.L128:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L125
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L128
.L126:
	movq	-1272(%rbp), %r13
.L124:
	testq	%r13, %r13
	je	.L129
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L129:
	movq	-2888(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1440(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L130
	call	_ZdlPv@PLT
.L130:
	movq	-1456(%rbp), %rbx
	movq	-1464(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L131
	.p2align 4,,10
	.p2align 3
.L135:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L132
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L135
.L133:
	movq	-1464(%rbp), %r13
.L131:
	testq	%r13, %r13
	je	.L136
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L136:
	movq	-2936(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1632(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L137
	call	_ZdlPv@PLT
.L137:
	movq	-1648(%rbp), %rbx
	movq	-1656(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L138
	.p2align 4,,10
	.p2align 3
.L142:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L139
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L142
.L140:
	movq	-1656(%rbp), %r13
.L138:
	testq	%r13, %r13
	je	.L143
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L143:
	movq	-2880(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1824(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L144
	call	_ZdlPv@PLT
.L144:
	movq	-1840(%rbp), %rbx
	movq	-1848(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L145
	.p2align 4,,10
	.p2align 3
.L149:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L146
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L149
.L147:
	movq	-1848(%rbp), %r13
.L145:
	testq	%r13, %r13
	je	.L150
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L150:
	movq	-2872(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2016(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L151
	call	_ZdlPv@PLT
.L151:
	movq	-2032(%rbp), %rbx
	movq	-2040(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L152
	.p2align 4,,10
	.p2align 3
.L156:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L153
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L156
.L154:
	movq	-2040(%rbp), %r13
.L152:
	testq	%r13, %r13
	je	.L157
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L157:
	movq	-2864(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L158
	call	_ZdlPv@PLT
.L158:
	movq	-2224(%rbp), %rbx
	movq	-2232(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L159
	.p2align 4,,10
	.p2align 3
.L163:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L160
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L163
.L161:
	movq	-2232(%rbp), %r13
.L159:
	testq	%r13, %r13
	je	.L164
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L164:
	movq	-2944(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L165
	call	_ZdlPv@PLT
.L165:
	movq	-2416(%rbp), %rbx
	movq	-2424(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L166
	.p2align 4,,10
	.p2align 3
.L170:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L167
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L170
.L168:
	movq	-2424(%rbp), %r13
.L166:
	testq	%r13, %r13
	je	.L171
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L171:
	movq	-2856(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L172
	call	_ZdlPv@PLT
.L172:
	movq	-2608(%rbp), %rbx
	movq	-2616(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L173
	.p2align 4,,10
	.p2align 3
.L177:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L174
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L177
.L175:
	movq	-2616(%rbp), %r13
.L173:
	testq	%r13, %r13
	je	.L178
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L178:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L347
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L177
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L167:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L170
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L160:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L163
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L153:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L156
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L146:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L149
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L139:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L142
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L132:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L135
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L125:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L128
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L118:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L121
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L111:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L114
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L104:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L107
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L90:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L93
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L97:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L100
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L334:
	movq	-2856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134678279, (%rax)
	leaq	5(%rax), %rdx
	movb	$6, 4(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L46
	call	_ZdlPv@PLT
.L46:
	movq	(%rbx), %rax
	movl	$18, %edx
	movq	%r12, %rdi
	movq	16(%rax), %rsi
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	24(%rax), %rbx
	movq	32(%rax), %rax
	movq	%rsi, -3008(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rcx, -2976(%rbp)
	movq	%rax, -2992(%rbp)
	movq	%rbx, -3024(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$27, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$28, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	-2976(%rbp), %rdx
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE(%rip), %esi
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$29, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal15Cast6ATNull_101EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2992(%rbp), %rcx
	movq	%r14, %xmm6
	movq	-3008(%rbp), %xmm5
	movq	%rbx, %xmm7
	movhps	-2976(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movhps	-3024(%rbp), %xmm5
	punpcklqdq	%xmm7, %xmm7
	movq	%rcx, -96(%rbp)
	leaq	-2784(%rbp), %r14
	movaps	%xmm5, -3008(%rbp)
	movaps	%xmm6, -2976(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm7, -3024(%rbp)
	movaps	%xmm7, -80(%rbp)
	movaps	%xmm0, -2784(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -64(%rbp)
	movq	$0, -2768(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm3
	movq	-64(%rbp), %rcx
	leaq	-2240(%rbp), %rdi
	movdqa	-112(%rbp), %xmm4
	leaq	72(%rax), %rdx
	movq	%r14, %rsi
	movq	%rax, -2784(%rbp)
	movups	%xmm3, (%rax)
	movdqa	-96(%rbp), %xmm3
	movups	%xmm4, 16(%rax)
	movdqa	-80(%rbp), %xmm4
	movq	%rcx, 64(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm4, 48(%rax)
	movq	%rdx, -2768(%rbp)
	movq	%rdx, -2776(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L47
	call	_ZdlPv@PLT
.L47:
	movq	-2864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2744(%rbp)
	jne	.L348
.L48:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2368(%rbp)
	je	.L50
.L335:
	movq	-2944(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2432(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$506381205588346631, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L51
	call	_ZdlPv@PLT
.L51:
	movq	(%rbx), %rax
	movl	$56, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movq	48(%rax), %rdx
	movdqu	(%rax), %xmm5
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -80(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm3
	leaq	56(%rax), %rdx
	leaq	-2048(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm3, 32(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L52
	call	_ZdlPv@PLT
.L52:
	movq	-2872(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2176(%rbp)
	je	.L53
.L336:
	movq	-2864(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2240(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$506381205588346631, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L54
	call	_ZdlPv@PLT
.L54:
	movq	(%rbx), %rax
	movl	$30, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	xorl	%r8d, %r8d
	movl	$145, %edx
	movq	%r14, %rsi
	leaq	.LC2(%rip), %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateEPKcS7_@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -1984(%rbp)
	je	.L55
.L337:
	movq	-2872(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -2840(%rbp)
	leaq	-2048(%rbp), %r14
	movq	$0, -2832(%rbp)
	movq	$0, -2824(%rbp)
	movq	$0, -2816(%rbp)
	movq	$0, -2808(%rbp)
	movq	$0, -2800(%rbp)
	movq	$0, -2784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2784(%rbp), %rax
	movq	%r14, %rdi
	leaq	-2816(%rbp), %r8
	pushq	%rax
	leaq	-2800(%rbp), %rax
	leaq	-2824(%rbp), %rcx
	pushq	%rax
	leaq	-2808(%rbp), %r9
	leaq	-2832(%rbp), %rdx
	leaq	-2840(%rbp), %rsi
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectENS0_3SmiENS0_10JSReceiverENS0_10HeapObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EE
	movl	$32, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$33, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	.LC1(%rip), %rsi
	movl	$29, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-2840(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	-2784(%rbp), %rax
	movl	$56, %edi
	movaps	%xmm0, -2752(%rbp)
	movq	%rdx, -128(%rbp)
	movq	-2832(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-2824(%rbp), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -112(%rbp)
	movq	-2816(%rbp), %rdx
	movq	$0, -2736(%rbp)
	movq	%rdx, -104(%rbp)
	movq	-2808(%rbp), %rdx
	movq	%rdx, -96(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	leaq	56(%rax), %rdx
	leaq	-1856(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	popq	%rsi
	popq	%r8
	testq	%rdi, %rdi
	je	.L56
	call	_ZdlPv@PLT
.L56:
	movq	-2880(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1792(%rbp)
	je	.L57
.L338:
	movq	-2880(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1856(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movl	$1798, %ecx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134678279, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L58
	call	_ZdlPv@PLT
.L58:
	movq	(%rbx), %rax
	movl	$28, %edx
	movq	%r12, %rdi
	movq	24(%rax), %rbx
	movq	16(%rax), %rsi
	movq	(%rax), %rcx
	movq	8(%rax), %r14
	movq	%rbx, -3008(%rbp)
	movq	32(%rax), %rbx
	movq	40(%rax), %rax
	movq	%rsi, -2992(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rcx, -2976(%rbp)
	movq	%rax, -3040(%rbp)
	movq	%rbx, -3024(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$38, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE(%rip), %esi
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal20Cast10JSReceiver_140EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm7
	pxor	%xmm0, %xmm0
	movq	-3024(%rbp), %xmm2
	movl	$64, %edi
	movaps	%xmm0, -2784(%rbp)
	movq	-2992(%rbp), %xmm3
	movq	-2976(%rbp), %xmm4
	movhps	-3040(%rbp), %xmm2
	movq	%rbx, -80(%rbp)
	leaq	-2784(%rbp), %r14
	punpcklqdq	%xmm7, %xmm4
	movhps	-3008(%rbp), %xmm3
	movaps	%xmm2, -3024(%rbp)
	movaps	%xmm3, -2992(%rbp)
	movaps	%xmm4, -2976(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -2768(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm4
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm6
	leaq	64(%rax), %rdx
	leaq	-1472(%rbp), %rdi
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movq	%rax, -2784(%rbp)
	movq	%rdx, -2768(%rbp)
	movq	%rdx, -2776(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L59
	call	_ZdlPv@PLT
.L59:
	movq	-2888(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2744(%rbp)
	jne	.L349
.L60:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -1600(%rbp)
	je	.L62
.L339:
	movq	-2936(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -2840(%rbp)
	leaq	-1664(%rbp), %r14
	movq	$0, -2832(%rbp)
	movq	$0, -2824(%rbp)
	movq	$0, -2816(%rbp)
	movq	$0, -2808(%rbp)
	movq	$0, -2800(%rbp)
	movq	$0, -2784(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2784(%rbp), %rax
	movq	%r14, %rdi
	leaq	-2824(%rbp), %rcx
	pushq	%rax
	leaq	-2800(%rbp), %rax
	leaq	-2832(%rbp), %rdx
	pushq	%rax
	leaq	-2840(%rbp), %rsi
	leaq	-2808(%rbp), %r9
	leaq	-2816(%rbp), %r8
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_7JSProxyENS0_4NameENS0_6ObjectENS0_3SmiENS0_10JSReceiverENS0_10HeapObjectEEE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EEPNSB_IS5_EEPNSB_IS6_EEPNSB_IS7_EEPNSB_IS8_EEPNSB_IS9_EE
	movl	$48, %edi
	movq	-2808(%rbp), %xmm0
	movq	-2824(%rbp), %xmm1
	movq	-2840(%rbp), %xmm2
	movq	$0, -2736(%rbp)
	movhps	-2800(%rbp), %xmm0
	movhps	-2816(%rbp), %xmm1
	movhps	-2832(%rbp), %xmm2
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm4
	leaq	48(%rax), %rdx
	leaq	-1280(%rbp), %rdi
	movq	%rax, -2752(%rbp)
	movups	%xmm7, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm4, 32(%rax)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	popq	%rbx
	popq	%r14
	testq	%rdi, %rdi
	je	.L63
	call	_ZdlPv@PLT
.L63:
	movq	-2912(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1408(%rbp)
	je	.L64
.L340:
	movq	-2888(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1472(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$506381205588346631, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L65
	call	_ZdlPv@PLT
.L65:
	movq	(%rbx), %rax
	movl	$56, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movq	56(%rax), %rdx
	movdqu	(%rax), %xmm5
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -80(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm3
	leaq	56(%rax), %rdx
	leaq	-1088(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm3, 32(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L66
	call	_ZdlPv@PLT
.L66:
	movq	-2928(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1216(%rbp)
	je	.L67
.L341:
	movq	-2912(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1280(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movl	$1798, %r11d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134678279, (%rax)
	leaq	6(%rax), %rdx
	movw	%r11w, 4(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L68
	call	_ZdlPv@PLT
.L68:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -1024(%rbp)
	je	.L69
.L342:
	movq	-2928(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1088(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movl	$1798, %r10d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134678279, (%rax)
	leaq	7(%rax), %rdx
	movw	%r10w, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L70
	call	_ZdlPv@PLT
.L70:
	movq	(%rbx), %rax
	movl	$43, %edx
	movq	%r12, %rdi
	movq	24(%rax), %rsi
	movq	8(%rax), %rcx
	movq	40(%rax), %r14
	movq	(%rax), %rbx
	movq	%rsi, -3008(%rbp)
	movq	32(%rax), %rsi
	movq	%rcx, -2976(%rbp)
	movq	16(%rax), %rcx
	movq	48(%rax), %rax
	movq	%rsi, -3024(%rbp)
	leaq	.LC1(%rip), %rsi
	movq	%rcx, -2992(%rbp)
	movq	%rax, -3040(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rdx
	movq	%r13, %r8
	movq	%rbx, %rsi
	leaq	.LC2(%rip), %rcx
	movq	%r15, %rdi
	call	_ZN2v88internal13GetMethod_245EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPKcPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm4
	movq	%rbx, %xmm2
	movq	-3040(%rbp), %xmm5
	movhps	-2976(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movl	$72, %edi
	movq	-3024(%rbp), %xmm6
	punpcklqdq	%xmm4, %xmm5
	movaps	%xmm2, -128(%rbp)
	leaq	-2784(%rbp), %r14
	movq	-2992(%rbp), %xmm7
	punpcklqdq	%xmm4, %xmm6
	movaps	%xmm5, -3040(%rbp)
	movhps	-3008(%rbp), %xmm7
	movaps	%xmm6, -3024(%rbp)
	movaps	%xmm7, -2992(%rbp)
	movaps	%xmm2, -2976(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm0, -2784(%rbp)
	movq	%rax, -64(%rbp)
	movq	$0, -2768(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movdqa	-128(%rbp), %xmm5
	leaq	-704(%rbp), %rdi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm3
	leaq	72(%rax), %rdx
	movq	%r14, %rsi
	movdqa	-80(%rbp), %xmm4
	movq	%rcx, 64(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm4, 48(%rax)
	movq	%rax, -2784(%rbp)
	movq	%rdx, -2768(%rbp)
	movq	%rdx, -2776(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L71
	call	_ZdlPv@PLT
.L71:
	movq	-2896(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2744(%rbp)
	jne	.L350
.L72:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -832(%rbp)
	je	.L74
.L343:
	movq	-2952(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-896(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$506381205588346631, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L75
	call	_ZdlPv@PLT
.L75:
	movq	(%rbx), %rax
	movl	$56, %edi
	movdqu	32(%rax), %xmm0
	movdqu	16(%rax), %xmm1
	movq	48(%rax), %rdx
	movdqu	(%rax), %xmm5
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -80(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm6
	movq	%r13, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm3
	leaq	56(%rax), %rdx
	leaq	-512(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm6, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm3, 32(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L76
	call	_ZdlPv@PLT
.L76:
	movq	-2904(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -640(%rbp)
	je	.L77
.L344:
	movq	-2896(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-704(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$506381205588346631, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L78
	call	_ZdlPv@PLT
.L78:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	32(%rax), %rdi
	movq	8(%rax), %r10
	movq	16(%rax), %r9
	movq	24(%rax), %r8
	movq	40(%rax), %rsi
	movq	48(%rax), %rcx
	movq	64(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdi, -96(%rbp)
	movl	$64, %edi
	movq	%r10, -120(%rbp)
	movq	%r9, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movaps	%xmm0, -2752(%rbp)
	movq	%rax, -128(%rbp)
	movq	$0, -2736(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movdqa	-112(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	leaq	64(%rax), %rdx
	leaq	-320(%rbp), %rdi
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2752(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L79
	call	_ZdlPv@PLT
.L79:
	movq	-2920(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -448(%rbp)
	je	.L80
.L345:
	movq	-2904(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-512(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movl	$1798, %r9d
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	$134678279, (%rax)
	leaq	7(%rax), %rdx
	movw	%r9w, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L81
	call	_ZdlPv@PLT
.L81:
	movq	(%rbx), %rax
	movl	$45, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	leaq	-2800(%rbp), %r14
	movq	16(%rax), %rcx
	movq	(%rax), %r9
	movq	24(%rax), %rbx
	movq	%rcx, -2976(%rbp)
	movq	32(%rax), %rcx
	movq	48(%rax), %rax
	movq	%r9, -3024(%rbp)
	movq	%rcx, -2992(%rbp)
	movq	%rax, -3008(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$44, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$711, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-2752(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	movq	-3024(%rbp), %r9
	xorl	%esi, %esi
	movq	-3008(%rbp), %xmm0
	movq	%rax, %r8
	movq	%r14, %rdi
	movq	%rcx, -2784(%rbp)
	leaq	-128(%rbp), %rcx
	movq	-2736(%rbp), %rax
	leaq	-2784(%rbp), %rdx
	movhps	-2976(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%rbx, %xmm0
	movl	$4, %ebx
	pushq	%rbx
	movhps	-2992(%rbp), %xmm0
	pushq	%rcx
	movl	$1, %ecx
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -2776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -256(%rbp)
	popq	%rdi
	popq	%r8
	je	.L82
.L346:
	movq	-2920(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -2976(%rbp)
	leaq	-320(%rbp), %r14
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2736(%rbp)
	movaps	%xmm0, -2752(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movabsq	$506381205588346631, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2752(%rbp)
	movq	%rdx, -2736(%rbp)
	movq	%rdx, -2744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2752(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L83
	call	_ZdlPv@PLT
.L83:
	movq	(%rbx), %rax
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	leaq	-2800(%rbp), %r14
	movq	(%rax), %rcx
	movq	8(%rax), %rdx
	movq	40(%rax), %rbx
	movq	%rcx, -2992(%rbp)
	movq	16(%rax), %rcx
	testq	%rdx, %rdx
	cmove	-2976(%rbp), %rdx
	movq	%rcx, -3008(%rbp)
	movq	24(%rax), %rcx
	movq	%rdx, -2976(%rbp)
	movl	$43, %edx
	movq	%rcx, -3040(%rbp)
	movq	48(%rax), %rcx
	movq	56(%rax), %rax
	movq	%rcx, -3024(%rbp)
	movq	%rax, -2960(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$49, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19IsUndefinedConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	je	.L85
.L87:
	movq	-3008(%rbp), %xmm0
	movq	%rbx, %xmm1
	movq	%r14, %rdi
	movhps	-3024(%rbp), %xmm1
	movhps	-3040(%rbp), %xmm0
	movaps	%xmm1, -3072(%rbp)
	movaps	%xmm0, -3056(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$3, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-2752(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -3040(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-3072(%rbp), %xmm1
	movq	-2960(%rbp), %xmm2
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movdqa	-3056(%rbp), %xmm0
	movq	%rax, -2784(%rbp)
	movq	-2736(%rbp), %rax
	movhps	-3040(%rbp), %xmm2
	movaps	%xmm2, -128(%rbp)
	movq	%rax, -2776(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
.L333:
	leaq	-128(%rbp), %rcx
	movl	$6, %ebx
	xorl	%esi, %esi
	movq	%r14, %rdi
	pushq	%rbx
	movq	-2992(%rbp), %r9
	leaq	-2784(%rbp), %rdx
	pushq	%rcx
	movl	$1, %ecx
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	popq	%rax
	popq	%rdx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$62, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%rbx, %r9
	movq	-2976(%rbp), %rcx
	pushq	$0
	movq	-3008(%rbp), %r8
	movq	%r13, %rdi
	movq	-3024(%rbp), %rdx
	movq	-2992(%rbp), %rsi
	call	_ZN2v88internal24ProxiesCodeStubAssembler21CheckGetSetTrapResultENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_10JSReceiverEEENS3_INS0_7JSProxyEEENS3_INS0_4NameEEENS3_INS0_6ObjectEEENS8_10AccessKindE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$63, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IsNullConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L87
	movq	-3008(%rbp), %xmm2
	movq	%rbx, %xmm0
	movq	%r14, %rdi
	movhps	-3024(%rbp), %xmm0
	movhps	-3040(%rbp), %xmm2
	movaps	%xmm0, -3072(%rbp)
	movaps	%xmm2, -3056(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$3, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-2752(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -3040(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-3072(%rbp), %xmm0
	movq	-2960(%rbp), %xmm1
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movdqa	-3056(%rbp), %xmm2
	movq	%rax, -2784(%rbp)
	movq	-2736(%rbp), %rax
	movhps	-3040(%rbp), %xmm1
	movaps	%xmm1, -128(%rbp)
	movq	%rax, -2776(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L348:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-2992(%rbp), %rax
	pxor	%xmm0, %xmm0
	movdqa	-2976(%rbp), %xmm3
	movdqa	-3008(%rbp), %xmm4
	movl	$64, %edi
	movaps	%xmm0, -2784(%rbp)
	movaps	%xmm3, -128(%rbp)
	movdqa	-3024(%rbp), %xmm3
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm3, -80(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -2768(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm4
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm5
	leaq	64(%rax), %rdx
	leaq	-2432(%rbp), %rdi
	movq	%rax, -2784(%rbp)
	movups	%xmm4, (%rax)
	movdqa	-96(%rbp), %xmm4
	movups	%xmm3, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rdx, -2768(%rbp)
	movq	%rdx, -2776(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L49
	call	_ZdlPv@PLT
.L49:
	movq	-2944(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L349:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movdqa	-2976(%rbp), %xmm6
	movdqa	-2992(%rbp), %xmm7
	movdqa	-3024(%rbp), %xmm3
	movaps	%xmm0, -2784(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -2768(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm4
	movq	%r14, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm5
	leaq	56(%rax), %rdx
	leaq	-1664(%rbp), %rdi
	movq	%rcx, 48(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm5, 32(%rax)
	movq	%rax, -2784(%rbp)
	movq	%rdx, -2768(%rbp)
	movq	%rdx, -2776(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L61
	call	_ZdlPv@PLT
.L61:
	movq	-2936(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L350:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-2976(%rbp), %xmm6
	movdqa	-2992(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movdqa	-3024(%rbp), %xmm3
	movdqa	-3040(%rbp), %xmm4
	movl	$64, %edi
	movaps	%xmm0, -2784(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movq	$0, -2768(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm2
	movdqa	-112(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-96(%rbp), %xmm6
	movdqa	-80(%rbp), %xmm7
	leaq	64(%rax), %rdx
	leaq	-896(%rbp), %rdi
	movups	%xmm2, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rax, -2784(%rbp)
	movq	%rdx, -2768(%rbp)
	movq	%rdx, -2776(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L73
	call	_ZdlPv@PLT
.L73:
	movq	-2952(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L72
.L347:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22418:
	.size	_ZN2v88internal25ProxyGetPropertyAssembler28GenerateProxyGetPropertyImplEv, .-_ZN2v88internal25ProxyGetPropertyAssembler28GenerateProxyGetPropertyImplEv
	.section	.rodata._ZN2v88internal8Builtins25Generate_ProxyGetPropertyEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/proxy-get-property-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins25Generate_ProxyGetPropertyEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"ProxyGetProperty"
	.section	.text._ZN2v88internal8Builtins25Generate_ProxyGetPropertyEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins25Generate_ProxyGetPropertyEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins25Generate_ProxyGetPropertyEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins25Generate_ProxyGetPropertyEPNS0_8compiler18CodeAssemblerStateE:
.LFB22414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$146, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$852, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L355
.L352:
	movq	%r13, %rdi
	call	_ZN2v88internal25ProxyGetPropertyAssembler28GenerateProxyGetPropertyImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L356
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L355:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L352
.L356:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22414:
	.size	_ZN2v88internal8Builtins25Generate_ProxyGetPropertyEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins25Generate_ProxyGetPropertyEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins25Generate_ProxyGetPropertyEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins25Generate_ProxyGetPropertyEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins25Generate_ProxyGetPropertyEPNS0_8compiler18CodeAssemblerStateE:
.LFB29034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29034:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins25Generate_ProxyGetPropertyEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins25Generate_ProxyGetPropertyEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins25Generate_ProxyGetPropertyEPNS0_8compiler18CodeAssemblerStateE
	.weak	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_10HeapObjectEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
