	.file	"array-findindex-tq-csa.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.rodata._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cannot create std::vector larger than max_size()"
	.section	.text._ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0:
.LFB30373:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	%rsi, %r13
	pushq	%rbx
	movq	%r13, %rax
	sarq	$3, %rax
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%r13, %r13
	js	.L14
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%rdx, %r14
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.L9
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%rax, %rdi
.L9:
	leaq	(%rdi,%r13), %r15
	movq	%rdi, (%rbx)
	movq	%r15, 16(%rbx)
	cmpq	%r12, %r14
	je	.L10
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
.L10:
	movq	%r15, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE30373:
	.size	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0, @function
_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0:
.LFB30372:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	subq	%rsi, %rdx
	js	.L21
	movq	%rdi, %rbx
	je	.L17
	movq	%rdx, %rdi
	movq	%rdx, %r12
	movq	%rsi, %r13
	call	_Znwm@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	leaq	(%rax,%r12), %r14
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	%r14, 16(%rbx)
	call	memcpy@PLT
	movq	%r14, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	xorl	%r14d, %r14d
	movq	$0, (%rdi)
	movq	$0, 16(%rdi)
	movq	%r14, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE30372:
	.size	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0, .-_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0:
.LFB30371:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	movabsq	$384307168202282325, %rax
	cmpq	%rax, %rdx
	ja	.L39
	pxor	%xmm0, %xmm0
	leaq	(%rdx,%rdx,2), %r14
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	%rdx, %r12
	movups	%xmm0, 8(%rdi)
	salq	$3, %r14
	xorl	%esi, %esi
	movq	$0, 24(%rdi)
	testq	%rdx, %rdx
	je	.L28
	movq	%r14, %rdi
	call	_Znwm@PLT
	leaq	-1(%r12), %rdx
	leaq	(%rax,%r14), %rsi
	movq	%rax, 8(%rbx)
	movq	%rsi, 24(%rbx)
	cmpq	$2, %rdx
	jbe	.L29
	movq	%r12, %rdi
	movq	%r12, %rcx
	pxor	%xmm0, %xmm0
	movq	%rax, %rdx
	shrq	%rdi
	andq	$-2, %rcx
	addq	%rdi, %rcx
	salq	$4, %rcx
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L26:
	movups	%xmm0, (%rdx)
	addq	$48, %rdx
	movups	%xmm0, -32(%rdx)
	movups	%xmm0, -16(%rdx)
	cmpq	%rcx, %rdx
	jne	.L26
	movq	%r12, %rdx
	movq	%r12, %rcx
	andq	$-2, %rdx
	andl	$1, %ecx
	leaq	(%rdx,%rdx,2), %rdi
	leaq	(%rax,%rdi,8), %rax
	cmpq	%rdx, %r12
	je	.L28
.L25:
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	cmpq	$1, %rcx
	je	.L28
	movq	$0, 40(%rax)
	movups	%xmm0, 24(%rax)
	cmpq	$2, %rcx
	je	.L28
	movq	$0, 64(%rax)
	movups	%xmm0, 48(%rax)
.L28:
	movq	%rsi, 16(%rbx)
	pxor	%xmm0, %xmm0
	leaq	56(%rbx), %rdi
	movq	%r13, %rsi
	movups	%xmm0, 32(%rbx)
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	$0, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	%r12, %rcx
	jmp	.L25
.L39:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE30371:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	.section	.text._ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev,"axG",@progbits,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.type	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, @function
_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev:
.LFB22421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$56, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L41
	call	_ZdlPv@PLT
.L41:
	movq	16(%rbx), %r13
	movq	8(%rbx), %r12
	cmpq	%r12, %r13
	je	.L42
	.p2align 4,,10
	.p2align 3
.L46:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L43
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L46
.L44:
	movq	8(%rbx), %r12
.L42:
	testq	%r12, %r12
	je	.L40
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %r13
	jne	.L46
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L40:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22421:
	.size	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev, .-_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.weak	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev
	.set	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD1Ev,_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	.section	.rodata._ZN2v88internal49ArrayFindIndexLoopEagerDeoptContinuationAssembler52GenerateArrayFindIndexLoopEagerDeoptContinuationImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"../../deps/v8/../../deps/v8/src/builtins/array-findindex.tq"
	.section	.text._ZN2v88internal49ArrayFindIndexLoopEagerDeoptContinuationAssembler52GenerateArrayFindIndexLoopEagerDeoptContinuationImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal49ArrayFindIndexLoopEagerDeoptContinuationAssembler52GenerateArrayFindIndexLoopEagerDeoptContinuationImplEv
	.type	_ZN2v88internal49ArrayFindIndexLoopEagerDeoptContinuationAssembler52GenerateArrayFindIndexLoopEagerDeoptContinuationImplEv, @function
_ZN2v88internal49ArrayFindIndexLoopEagerDeoptContinuationAssembler52GenerateArrayFindIndexLoopEagerDeoptContinuationImplEv:
.LFB22418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$7, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-3408(%rbp), %r14
	leaq	-3536(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-3352(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-3160(%rbp), %rbx
	subq	$3848, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, -3608(%rbp)
	movq	%rax, -3592(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -3616(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -3624(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, -3648(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, -3664(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$4, %esi
	movq	%r12, %rdi
	leaq	-3592(%rbp), %r12
	movq	%rax, -3712(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$144, %edi
	movq	$0, -3400(%rbp)
	movq	%rax, -3728(%rbp)
	movq	-3592(%rbp), %rax
	movq	$0, -3392(%rbp)
	movq	%rax, -3408(%rbp)
	movq	$0, -3384(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rdx, -3384(%rbp)
	movq	%rdx, -3392(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3368(%rbp)
	movq	%rax, -3400(%rbp)
	movq	$0, -3376(%rbp)
	movq	%r14, -3768(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3592(%rbp), %rax
	movl	$168, %edi
	movq	$0, -3208(%rbp)
	movq	$0, -3200(%rbp)
	movq	%rax, -3216(%rbp)
	movq	$0, -3192(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -3192(%rbp)
	movq	%rdx, -3200(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3176(%rbp)
	movq	%rax, -3208(%rbp)
	movq	$0, -3184(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-3024(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3672(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2832(%rbp), %rax
	movl	$6, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3688(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2640(%rbp), %rax
	movl	$7, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3696(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-3592(%rbp), %rax
	movl	$192, %edi
	movq	$0, -2440(%rbp)
	movq	$0, -2432(%rbp)
	movq	%rax, -2448(%rbp)
	movq	$0, -2424(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movups	%xmm0, (%rax)
	leaq	192(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -2440(%rbp)
	leaq	-2392(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2424(%rbp)
	movq	%rdx, -2432(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2408(%rbp)
	movq	%rax, -3840(%rbp)
	movq	$0, -2416(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-2256(%rbp), %rax
	movl	$9, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-3592(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2056(%rbp)
	movq	$0, -2048(%rbp)
	movq	%rax, -2064(%rbp)
	movq	$0, -2040(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -2056(%rbp)
	leaq	-2008(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2040(%rbp)
	movq	%rdx, -2048(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2024(%rbp)
	movq	%rax, -3752(%rbp)
	movq	$0, -2032(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3592(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1864(%rbp)
	movq	$0, -1856(%rbp)
	movq	%rax, -1872(%rbp)
	movq	$0, -1848(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1864(%rbp)
	leaq	-1816(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1848(%rbp)
	movq	%rdx, -1856(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1832(%rbp)
	movq	%rax, -3744(%rbp)
	movq	$0, -1840(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3592(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1672(%rbp)
	movq	$0, -1664(%rbp)
	movq	%rax, -1680(%rbp)
	movq	$0, -1656(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1672(%rbp)
	leaq	-1624(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1656(%rbp)
	movq	%rdx, -1664(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1640(%rbp)
	movq	%rax, -3856(%rbp)
	movq	$0, -1648(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3592(%rbp), %rax
	movl	$240, %edi
	movq	$0, -1480(%rbp)
	movq	$0, -1472(%rbp)
	movq	%rax, -1488(%rbp)
	movq	$0, -1464(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -1480(%rbp)
	leaq	-1432(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1464(%rbp)
	movq	%rdx, -1472(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1448(%rbp)
	movq	%rax, -3824(%rbp)
	movq	$0, -1456(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3592(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1288(%rbp)
	movq	$0, -1280(%rbp)
	movq	%rax, -1296(%rbp)
	movq	$0, -1272(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1288(%rbp)
	leaq	-1240(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1272(%rbp)
	movq	%rdx, -1280(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1256(%rbp)
	movq	%rax, -3792(%rbp)
	movq	$0, -1264(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3592(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1096(%rbp)
	movq	$0, -1088(%rbp)
	movq	%rax, -1104(%rbp)
	movq	$0, -1080(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1096(%rbp)
	leaq	-1048(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1080(%rbp)
	movq	%rdx, -1088(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1064(%rbp)
	movq	%rax, -3808(%rbp)
	movq	$0, -1072(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3592(%rbp), %rax
	movl	$240, %edi
	movq	$0, -904(%rbp)
	movq	$0, -896(%rbp)
	movq	%rax, -912(%rbp)
	movq	$0, -888(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movups	%xmm0, (%rax)
	leaq	240(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -904(%rbp)
	leaq	-856(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -888(%rbp)
	movq	%rdx, -896(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -872(%rbp)
	movq	%rax, -3864(%rbp)
	movq	$0, -880(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-720(%rbp), %rax
	movl	$11, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3632(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-3592(%rbp), %rax
	movl	$216, %edi
	movq	$0, -520(%rbp)
	movq	$0, -512(%rbp)
	movq	%rax, -528(%rbp)
	movq	$0, -504(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -520(%rbp)
	leaq	-472(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -504(%rbp)
	movq	%rdx, -512(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -488(%rbp)
	movq	%rax, -3776(%rbp)
	movq	$0, -496(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3592(%rbp), %rax
	movl	$240, %edi
	movq	$0, -328(%rbp)
	movq	$0, -320(%rbp)
	movq	%rax, -336(%rbp)
	movq	$0, -312(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -328(%rbp)
	leaq	-280(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -312(%rbp)
	movq	%rdx, -320(%rbp)
	xorl	%edx, %edx
	movq	%rax, -3816(%rbp)
	movups	%xmm0, -296(%rbp)
	movq	$0, -304(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$48, %edi
	movq	-3616(%rbp), %xmm1
	movaps	%xmm0, -3536(%rbp)
	movhps	-3624(%rbp), %xmm1
	movq	$0, -3520(%rbp)
	movaps	%xmm1, -144(%rbp)
	movq	-3648(%rbp), %xmm1
	movhps	-3664(%rbp), %xmm1
	movaps	%xmm1, -128(%rbp)
	movq	-3712(%rbp), %xmm1
	movhps	-3728(%rbp), %xmm1
	movaps	%xmm1, -112(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm5
	movq	%r14, %rdi
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-144(%rbp), %xmm4
	leaq	48(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movups	%xmm5, 16(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm6, 32(%rax)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L53
	call	_ZdlPv@PLT
.L53:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3216(%rbp), %rax
	cmpq	$0, -3344(%rbp)
	movq	%rax, -3616(%rbp)
	jne	.L226
	cmpq	$0, -3152(%rbp)
	jne	.L227
.L59:
	cmpq	$0, -2960(%rbp)
	jne	.L228
.L62:
	cmpq	$0, -2768(%rbp)
	jne	.L229
.L65:
	leaq	-2448(%rbp), %rax
	cmpq	$0, -2576(%rbp)
	movq	%rax, -3624(%rbp)
	jne	.L230
.L67:
	leaq	-2064(%rbp), %rax
	cmpq	$0, -2384(%rbp)
	movq	%rax, -3760(%rbp)
	jne	.L231
.L72:
	leaq	-1872(%rbp), %rax
	cmpq	$0, -2192(%rbp)
	movq	%rax, -3728(%rbp)
	jne	.L232
.L75:
	cmpq	$0, -2000(%rbp)
	jne	.L233
.L78:
	leaq	-1488(%rbp), %rax
	cmpq	$0, -1808(%rbp)
	movq	%rax, -3752(%rbp)
	leaq	-1680(%rbp), %rax
	movq	%rax, -3648(%rbp)
	jne	.L234
.L80:
	leaq	-1296(%rbp), %rax
	cmpq	$0, -1616(%rbp)
	movq	%rax, -3712(%rbp)
	jne	.L235
.L85:
	leaq	-1104(%rbp), %rax
	cmpq	$0, -1424(%rbp)
	movq	%rax, -3744(%rbp)
	jne	.L236
.L88:
	cmpq	$0, -1232(%rbp)
	jne	.L237
.L91:
	leaq	-912(%rbp), %rax
	cmpq	$0, -1040(%rbp)
	movq	%rax, -3664(%rbp)
	jne	.L238
.L93:
	cmpq	$0, -848(%rbp)
	leaq	-528(%rbp), %r14
	jne	.L239
.L98:
	cmpq	$0, -656(%rbp)
	leaq	-336(%rbp), %rbx
	jne	.L240
.L101:
	cmpq	$0, -464(%rbp)
	jne	.L241
	cmpq	$0, -272(%rbp)
	jne	.L242
.L106:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3632(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3664(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3744(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3712(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3752(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3648(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3728(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3760(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3680(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3624(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3696(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3688(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3672(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3616(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-3768(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L243
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$2056, %r13d
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	-3768(%rbp), %rdi
	movq	%r15, %rsi
	movw	%r13w, 4(%rax)
	leaq	6(%rax), %rdx
	movl	$134744071, (%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L55
	call	_ZdlPv@PLT
.L55:
	movq	0(%r13), %rax
	movl	$17, %edx
	movq	%r12, %rdi
	movq	24(%rax), %rsi
	movq	(%rax), %r13
	movq	8(%rax), %rcx
	movq	16(%rax), %r14
	movq	%rsi, -3616(%rbp)
	movq	32(%rax), %rsi
	movq	40(%rax), %rax
	movq	%rcx, -3624(%rbp)
	movq	%rsi, -3648(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rax, -3664(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3624(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rcx
	movq	-3608(%rbp), %rdi
	call	_ZN2v88internal21Cast10JSReceiver_1404EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3624(%rbp), %rcx
	movq	%r14, %xmm3
	movq	-3648(%rbp), %xmm7
	movq	%r13, %xmm4
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movq	%rcx, %xmm6
	leaq	-3568(%rbp), %r14
	leaq	-144(%rbp), %r13
	movq	%rcx, -96(%rbp)
	movhps	-3664(%rbp), %xmm7
	movhps	-3616(%rbp), %xmm3
	movq	%r13, %rsi
	movq	%r14, %rdi
	punpcklqdq	%xmm6, %xmm4
	movaps	%xmm7, -3664(%rbp)
	movaps	%xmm3, -3648(%rbp)
	movaps	%xmm4, -3712(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm0, -3568(%rbp)
	movq	$0, -3552(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3672(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L56
	call	_ZdlPv@PLT
.L56:
	leaq	-2968(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3216(%rbp), %rax
	cmpq	$0, -3528(%rbp)
	movq	%rax, -3616(%rbp)
	jne	.L244
.L57:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -3152(%rbp)
	je	.L59
.L227:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movl	$2056, %ebx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	-3616(%rbp), %rdi
	movq	%r15, %rsi
	movw	%bx, 4(%rax)
	leaq	7(%rax), %rdx
	movl	$134744071, (%rax)
	movb	$8, 6(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L60
	call	_ZdlPv@PLT
.L60:
	movq	(%rbx), %rax
	leaq	-144(%rbp), %rsi
	leaq	-96(%rbp), %rdx
	movq	%r15, %rdi
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm1
	movdqu	32(%rax), %xmm6
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3688(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L61
	call	_ZdlPv@PLT
.L61:
	leaq	-2776(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2960(%rbp)
	je	.L62
.L228:
	leaq	-2968(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-144(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-136(%rbp), %rdx
	movabsq	$506663788666685447, %rax
	movaps	%xmm0, -3536(%rbp)
	movq	%rax, -144(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3672(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L63
	call	_ZdlPv@PLT
.L63:
	movq	(%rbx), %rax
	leaq	-88(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movdqu	32(%rax), %xmm0
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm1
	movq	56(%rax), %rax
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3696(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L64
	call	_ZdlPv@PLT
.L64:
	leaq	-2584(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2768(%rbp)
	je	.L65
.L229:
	leaq	-2776(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r11d
	movq	-3688(%rbp), %rdi
	movq	%r15, %rsi
	movl	$134744071, (%rax)
	leaq	6(%rax), %rdx
	movw	%r11w, 4(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L66
	call	_ZdlPv@PLT
.L66:
	movq	-3608(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L230:
	leaq	-2584(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r10d
	movq	-3696(%rbp), %rdi
	movq	%r15, %rsi
	movl	$134744071, (%rax)
	leaq	7(%rax), %rdx
	movw	%r10w, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L68
	call	_ZdlPv@PLT
.L68:
	movq	(%rbx), %rax
	movl	$18, %edx
	movq	%r12, %rdi
	movq	24(%rax), %rcx
	movq	40(%rax), %rsi
	movq	8(%rax), %rbx
	movq	(%rax), %r13
	movq	%rcx, -3648(%rbp)
	movq	32(%rax), %rcx
	movq	%rsi, -3712(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	48(%rax), %r14
	movq	%rbx, -3624(%rbp)
	movq	16(%rax), %rbx
	movq	%rcx, -3664(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	-3608(%rbp), %rdi
	call	_ZN2v88internal82Cast71UT19ATCallableApiObject17ATCallableJSProxy15JSBoundFunction10JSFunction_1405EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm5
	movq	%rbx, %xmm6
	movq	%r13, %xmm7
	punpcklqdq	%xmm6, %xmm5
	movq	%rbx, %xmm2
	movq	-3664(%rbp), %xmm6
	leaq	-3568(%rbp), %r14
	pxor	%xmm0, %xmm0
	leaq	-72(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, -80(%rbp)
	leaq	-144(%rbp), %r13
	movhps	-3712(%rbp), %xmm6
	movhps	-3648(%rbp), %xmm2
	movaps	%xmm5, -3728(%rbp)
	movhps	-3624(%rbp), %xmm7
	movq	%r13, %rsi
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm6, -3712(%rbp)
	movaps	%xmm2, -3648(%rbp)
	movaps	%xmm7, -3664(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm0, -3568(%rbp)
	movq	$0, -3552(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3680(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L69
	call	_ZdlPv@PLT
.L69:
	leaq	-2200(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2448(%rbp), %rax
	cmpq	$0, -3528(%rbp)
	movq	%rax, -3624(%rbp)
	jne	.L245
.L70:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L233:
	movq	-3752(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movl	$2056, %r9d
	movq	-3760(%rbp), %rdi
	movq	%r15, %rsi
	movl	$134744071, (%rax)
	leaq	7(%rax), %rdx
	movw	%r9w, 4(%rax)
	movb	$7, 6(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L79
	call	_ZdlPv@PLT
.L79:
	movq	-3608(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L232:
	leaq	-2200(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-144(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-135(%rbp), %rdx
	movaps	%xmm0, -3536(%rbp)
	movabsq	$578439907727902727, %rax
	movq	%rax, -144(%rbp)
	movb	$7, -136(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3680(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L76
	call	_ZdlPv@PLT
.L76:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	24(%rax), %rdi
	movq	32(%rax), %rsi
	movq	48(%rax), %rdx
	movq	(%rax), %r10
	movq	8(%rax), %r9
	movq	16(%rax), %r8
	movq	40(%rax), %rcx
	movq	64(%rax), %rax
	movq	%rdi, -120(%rbp)
	movq	%r15, %rdi
	movq	%rsi, -112(%rbp)
	movq	%r13, %rsi
	movq	%rdx, -96(%rbp)
	leaq	-80(%rbp), %rdx
	movq	%r10, -144(%rbp)
	movq	%r9, -136(%rbp)
	movq	%r8, -128(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%rax, -88(%rbp)
	movaps	%xmm0, -3536(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1872(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -3728(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L77
	call	_ZdlPv@PLT
.L77:
	movq	-3744(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L231:
	movq	-3840(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-144(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-136(%rbp), %rdx
	movabsq	$578439907727902727, %rax
	movaps	%xmm0, -3536(%rbp)
	movq	%rax, -144(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3624(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L73
	call	_ZdlPv@PLT
.L73:
	movq	(%rbx), %rax
	leaq	-88(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movdqu	32(%rax), %xmm0
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm1
	movq	48(%rax), %rax
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -96(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2064(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -3760(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L74
	call	_ZdlPv@PLT
.L74:
	movq	-3752(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L234:
	movq	-3744(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$506382313689974791, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	-3728(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L81
	call	_ZdlPv@PLT
.L81:
	movq	(%rbx), %rax
	movl	$19, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	8(%rax), %rbx
	movq	40(%rax), %rcx
	movq	(%rax), %r14
	movq	16(%rax), %r13
	movq	%rbx, -3648(%rbp)
	movq	24(%rax), %rbx
	movq	%rcx, -3712(%rbp)
	movq	48(%rax), %rcx
	movq	%rbx, -3664(%rbp)
	movq	32(%rax), %rbx
	movq	56(%rax), %rax
	movq	%rcx, -3744(%rbp)
	movq	%rax, -3752(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3608(%rbp), %rdi
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal29Cast20UT5ATSmi10HeapNumber_85EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %xmm5
	movq	%r14, %xmm6
	movq	-3744(%rbp), %xmm3
	leaq	-3568(%rbp), %r14
	movq	%rbx, %xmm4
	leaq	-144(%rbp), %r13
	movq	%rax, -72(%rbp)
	movhps	-3664(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	leaq	-64(%rbp), %rdx
	movq	%r13, %rsi
	movhps	-3752(%rbp), %xmm3
	movhps	-3712(%rbp), %xmm4
	movq	%r14, %rdi
	movaps	%xmm5, -3840(%rbp)
	movhps	-3648(%rbp), %xmm6
	movaps	%xmm3, -3744(%rbp)
	movaps	%xmm4, -3712(%rbp)
	movaps	%xmm6, -3664(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -3568(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -3552(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1488(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -3752(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L82
	call	_ZdlPv@PLT
.L82:
	movq	-3824(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1680(%rbp), %rax
	cmpq	$0, -3528(%rbp)
	movq	%rax, -3648(%rbp)
	jne	.L246
.L83:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L238:
	movq	-3808(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$506382313689974791, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	-3744(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L94
	call	_ZdlPv@PLT
.L94:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rsi
	movq	8(%rax), %rcx
	movq	56(%rax), %rdx
	movq	(%rax), %rbx
	movq	40(%rax), %r14
	movq	48(%rax), %r13
	movq	%rsi, -3824(%rbp)
	movq	32(%rax), %rsi
	movq	%rcx, -3792(%rbp)
	movq	16(%rax), %rcx
	movq	%rdx, -3856(%rbp)
	movl	$20, %edx
	movq	%rsi, -3840(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rbx, -3664(%rbp)
	movq	64(%rax), %rbx
	movq	%rcx, -3808(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3608(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	call	_ZN2v88internal29Cast20UT5ATSmi10HeapNumber_85EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm6
	movq	%r13, %xmm7
	movq	-3840(%rbp), %xmm3
	leaq	-3568(%rbp), %r14
	movq	%rbx, %xmm2
	leaq	-144(%rbp), %r13
	movq	-3808(%rbp), %xmm4
	punpcklqdq	%xmm6, %xmm2
	punpcklqdq	%xmm6, %xmm3
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	-3664(%rbp), %xmm5
	leaq	-56(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	movhps	-3856(%rbp), %xmm7
	movhps	-3824(%rbp), %xmm4
	movaps	%xmm3, -112(%rbp)
	movhps	-3792(%rbp), %xmm5
	movaps	%xmm2, -3888(%rbp)
	movaps	%xmm7, -3856(%rbp)
	movaps	%xmm3, -3840(%rbp)
	movaps	%xmm4, -3808(%rbp)
	movaps	%xmm5, -3792(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm2, -80(%rbp)
	movaps	%xmm0, -3568(%rbp)
	movq	$0, -3552(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3632(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L95
	call	_ZdlPv@PLT
.L95:
	leaq	-664(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-912(%rbp), %rax
	cmpq	$0, -3528(%rbp)
	movq	%rax, -3664(%rbp)
	jne	.L247
.L96:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L237:
	movq	-3792(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$506382313689974791, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	-3712(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L92
	call	_ZdlPv@PLT
.L92:
	movq	-3608(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L236:
	movq	-3824(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-144(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	movabsq	$506382313689974791, %rax
	movl	$2056, %r8d
	leaq	-134(%rbp), %rdx
	movaps	%xmm0, -3536(%rbp)
	movq	%rax, -144(%rbp)
	movw	%r8w, -136(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3752(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L89
	call	_ZdlPv@PLT
.L89:
	movq	(%rbx), %rax
	leaq	-72(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movdqu	48(%rax), %xmm0
	movdqu	(%rax), %xmm3
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	72(%rax), %rax
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -144(%rbp)
	movq	%rax, -80(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3536(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1104(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -3744(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L90
	call	_ZdlPv@PLT
.L90:
	movq	-3808(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L235:
	movq	-3856(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-144(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-135(%rbp), %rdx
	movaps	%xmm0, -3536(%rbp)
	movabsq	$506382313689974791, %rax
	movq	%rax, -144(%rbp)
	movb	$8, -136(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3648(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L86
	call	_ZdlPv@PLT
.L86:
	movq	(%rbx), %rax
	leaq	-80(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movdqu	32(%rax), %xmm0
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm1
	movdqu	48(%rax), %xmm7
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1296(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -3712(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L87
	call	_ZdlPv@PLT
.L87:
	movq	-3792(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L241:
	movq	-3776(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -3520(%rbp)
	movaps	%xmm0, -3536(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	movabsq	$506382313689974791, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -3536(%rbp)
	movq	%rdx, -3520(%rbp)
	movq	%rdx, -3528(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L105
	call	_ZdlPv@PLT
.L105:
	movq	-3608(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -272(%rbp)
	je	.L106
.L242:
	movq	-3816(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-144(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$2056, %ecx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movabsq	$506382313689974791, %rax
	pxor	%xmm0, %xmm0
	leaq	-134(%rbp), %rdx
	movw	%cx, -136(%rbp)
	movq	%rax, -144(%rbp)
	movaps	%xmm0, -3536(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L107
	movq	%rax, -3792(%rbp)
	call	_ZdlPv@PLT
	movq	-3792(%rbp), %rax
.L107:
	movq	(%rax), %rax
	movl	$23, %edx
	movq	%r12, %rdi
	movq	56(%rax), %rsi
	movq	24(%rax), %rcx
	movq	(%rax), %r9
	movq	%rsi, -3808(%rbp)
	movq	64(%rax), %rsi
	movq	%rcx, -3776(%rbp)
	movq	48(%rax), %rcx
	movq	72(%rax), %rax
	movq	%rsi, -3816(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%r9, -3856(%rbp)
	movq	%rcx, -3792(%rbp)
	movq	%rax, -3824(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$22, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-3584(%rbp), %r10
	movq	-3608(%rbp), %rsi
	movq	%r10, %rdi
	movq	%r10, -3840(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3840(%rbp), %r10
	movq	%r10, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$743, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-3840(%rbp), %r10
	movq	-3536(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$6, %edi
	xorl	%esi, %esi
	movq	-3792(%rbp), %xmm0
	pushq	%rdi
	movq	%rax, %r8
	movq	-3840(%rbp), %r10
	movl	$1, %ecx
	movhps	-3808(%rbp), %xmm0
	pushq	%r13
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	-3856(%rbp), %r9
	movaps	%xmm0, -144(%rbp)
	leaq	-3568(%rbp), %rdx
	movq	%r10, %rdi
	movq	-3776(%rbp), %xmm0
	movq	%rax, -3568(%rbp)
	movq	-3520(%rbp), %rax
	movhps	-3792(%rbp), %xmm0
	movq	%r10, -3792(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	-3816(%rbp), %xmm0
	movq	%rax, -3560(%rbp)
	movhps	-3824(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-3792(%rbp), %r10
	movq	%rax, %r13
	movq	%r10, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3608(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L240:
	leaq	-664(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-144(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$2056, %esi
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movw	%si, -136(%rbp)
	leaq	-133(%rbp), %rdx
	movq	%r13, %rsi
	movabsq	$506382313689974791, %rax
	movq	%rax, -144(%rbp)
	movaps	%xmm0, -3536(%rbp)
	movb	$8, -134(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3632(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L102
	call	_ZdlPv@PLT
.L102:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	(%rax), %rbx
	movq	40(%rax), %rdi
	movq	48(%rax), %rsi
	movq	64(%rax), %rdx
	movq	8(%rax), %r11
	movq	16(%rax), %r10
	movq	24(%rax), %r9
	movq	32(%rax), %r8
	movq	56(%rax), %rcx
	movq	80(%rax), %rax
	movq	%rbx, -144(%rbp)
	leaq	-336(%rbp), %rbx
	movq	%rdi, -104(%rbp)
	movq	%r15, %rdi
	movq	%rsi, -96(%rbp)
	movq	%r13, %rsi
	movq	%rdx, -80(%rbp)
	leaq	-64(%rbp), %rdx
	movq	%r11, -136(%rbp)
	movq	%r10, -128(%rbp)
	movq	%r9, -120(%rbp)
	movq	%r8, -112(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%rax, -72(%rbp)
	movaps	%xmm0, -3536(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L103
	call	_ZdlPv@PLT
.L103:
	movq	-3816(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L239:
	movq	-3864(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-144(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movl	$2056, %edi
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movw	%di, -136(%rbp)
	leaq	-134(%rbp), %rdx
	movq	%r15, %rdi
	movabsq	$506382313689974791, %rax
	movq	%rax, -144(%rbp)
	movaps	%xmm0, -3536(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3664(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3536(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L99
	call	_ZdlPv@PLT
.L99:
	movq	(%rbx), %rax
	leaq	-72(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-528(%rbp), %r14
	movdqu	48(%rax), %xmm0
	movdqu	(%rax), %xmm3
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	64(%rax), %rax
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -144(%rbp)
	movq	%rax, -80(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3536(%rbp)
	movq	$0, -3520(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3536(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L100
	call	_ZdlPv@PLT
.L100:
	movq	-3776(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L244:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-88(%rbp), %rdx
	movq	%r13, %rsi
	movdqa	-3712(%rbp), %xmm2
	movq	-3624(%rbp), %rax
	movq	%r14, %rdi
	movaps	%xmm0, -3568(%rbp)
	movq	$0, -3552(%rbp)
	movaps	%xmm2, -144(%rbp)
	movdqa	-3648(%rbp), %xmm2
	movq	%rax, -96(%rbp)
	movaps	%xmm2, -128(%rbp)
	movdqa	-3664(%rbp), %xmm2
	movaps	%xmm2, -112(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3616(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L58
	call	_ZdlPv@PLT
.L58:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L245:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rdx
	movq	%r13, %rsi
	movdqa	-3664(%rbp), %xmm2
	movdqa	-3648(%rbp), %xmm7
	movq	%r14, %rdi
	movaps	%xmm0, -3568(%rbp)
	movq	$0, -3552(%rbp)
	movaps	%xmm2, -144(%rbp)
	movdqa	-3712(%rbp), %xmm2
	movaps	%xmm7, -128(%rbp)
	movdqa	-3728(%rbp), %xmm7
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3624(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L71
	call	_ZdlPv@PLT
.L71:
	movq	-3840(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L246:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-72(%rbp), %rdx
	movq	%r13, %rsi
	movdqa	-3664(%rbp), %xmm2
	movq	%r14, %rdi
	movq	%rbx, -80(%rbp)
	movdqa	-3840(%rbp), %xmm7
	movaps	%xmm0, -3568(%rbp)
	movaps	%xmm2, -144(%rbp)
	movdqa	-3712(%rbp), %xmm2
	movaps	%xmm7, -128(%rbp)
	movdqa	-3744(%rbp), %xmm7
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	$0, -3552(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3648(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L84
	call	_ZdlPv@PLT
.L84:
	movq	-3856(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L247:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-3792(%rbp), %xmm3
	movq	%r13, %rsi
	movdqa	-3808(%rbp), %xmm6
	movdqa	-3840(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	leaq	-64(%rbp), %rdx
	movq	%r14, %rdi
	movdqa	-3856(%rbp), %xmm7
	movaps	%xmm3, -144(%rbp)
	movdqa	-3888(%rbp), %xmm3
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm0, -3568(%rbp)
	movq	$0, -3552(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-3664(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L97
	call	_ZdlPv@PLT
.L97:
	movq	-3864(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L96
.L243:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22418:
	.size	_ZN2v88internal49ArrayFindIndexLoopEagerDeoptContinuationAssembler52GenerateArrayFindIndexLoopEagerDeoptContinuationImplEv, .-_ZN2v88internal49ArrayFindIndexLoopEagerDeoptContinuationAssembler52GenerateArrayFindIndexLoopEagerDeoptContinuationImplEv
	.section	.rodata._ZN2v88internal8Builtins49Generate_ArrayFindIndexLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/array-findindex-tq-csa.cc"
	.align 8
.LC4:
	.string	"ArrayFindIndexLoopEagerDeoptContinuation"
	.section	.text._ZN2v88internal8Builtins49Generate_ArrayFindIndexLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins49Generate_ArrayFindIndexLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins49Generate_ArrayFindIndexLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins49Generate_ArrayFindIndexLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE:
.LFB22414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$146, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$740, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L252
.L249:
	movq	%r13, %rdi
	call	_ZN2v88internal49ArrayFindIndexLoopEagerDeoptContinuationAssembler52GenerateArrayFindIndexLoopEagerDeoptContinuationImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L253
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L249
.L253:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22414:
	.size	_ZN2v88internal8Builtins49Generate_ArrayFindIndexLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins49Generate_ArrayFindIndexLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal48ArrayFindIndexLoopLazyDeoptContinuationAssembler51GenerateArrayFindIndexLoopLazyDeoptContinuationImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal48ArrayFindIndexLoopLazyDeoptContinuationAssembler51GenerateArrayFindIndexLoopLazyDeoptContinuationImplEv
	.type	_ZN2v88internal48ArrayFindIndexLoopLazyDeoptContinuationAssembler51GenerateArrayFindIndexLoopLazyDeoptContinuationImplEv, @function
_ZN2v88internal48ArrayFindIndexLoopLazyDeoptContinuationAssembler51GenerateArrayFindIndexLoopLazyDeoptContinuationImplEv:
.LFB22469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-344(%rbp), %r14
	leaq	-304(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r13, -344(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, -400(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -392(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, -384(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%rax, -360(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$5, %esi
	movq	%r12, %rdi
	leaq	-248(%rbp), %r12
	movq	%rax, -368(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$168, %edi
	movq	$0, -296(%rbp)
	movq	$0, -288(%rbp)
	movq	%rax, %rbx
	movq	-344(%rbp), %rax
	movq	$0, -280(%rbp)
	movq	%rax, -304(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rdx, -280(%rbp)
	movq	%rdx, -288(%rbp)
	xorl	%edx, %edx
	movq	%rax, -296(%rbp)
	movups	%xmm0, -264(%rbp)
	movq	$0, -272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movq	-400(%rbp), %xmm1
	movq	%rbx, -64(%rbp)
	leaq	-336(%rbp), %rbx
	movhps	-392(%rbp), %xmm1
	movaps	%xmm0, -336(%rbp)
	movaps	%xmm1, -112(%rbp)
	movq	-384(%rbp), %xmm1
	movq	$0, -320(%rbp)
	movhps	-376(%rbp), %xmm1
	movaps	%xmm1, -96(%rbp)
	movq	-360(%rbp), %xmm1
	movhps	-368(%rbp), %xmm1
	movaps	%xmm1, -80(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	leaq	56(%rax), %rdx
	movq	%rax, -336(%rbp)
	movdqa	-80(%rbp), %xmm4
	movq	%rcx, 48(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm4, 32(%rax)
	movq	%rdx, -320(%rbp)
	movq	%rdx, -328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L255
	call	_ZdlPv@PLT
.L255:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -240(%rbp)
	jne	.L279
.L256:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L258
	call	_ZdlPv@PLT
.L258:
	movq	-288(%rbp), %rbx
	movq	-296(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L259
	.p2align 4,,10
	.p2align 3
.L263:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L260
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L263
.L261:
	movq	-296(%rbp), %r12
.L259:
	testq	%r12, %r12
	je	.L264
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L264:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L280
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L263
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L279:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -320(%rbp)
	movaps	%xmm0, -336(%rbp)
	call	_Znwm@PLT
	movl	$2056, %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movl	$134744071, (%rax)
	leaq	7(%rax), %rdx
	movw	%cx, 4(%rax)
	movb	$8, 6(%rax)
	movq	%rax, -336(%rbp)
	movq	%rdx, -320(%rbp)
	movq	%rdx, -328(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L257
	call	_ZdlPv@PLT
.L257:
	movl	$34, %edx
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L256
.L280:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22469:
	.size	_ZN2v88internal48ArrayFindIndexLoopLazyDeoptContinuationAssembler51GenerateArrayFindIndexLoopLazyDeoptContinuationImplEv, .-_ZN2v88internal48ArrayFindIndexLoopLazyDeoptContinuationAssembler51GenerateArrayFindIndexLoopLazyDeoptContinuationImplEv
	.section	.rodata._ZN2v88internal8Builtins48Generate_ArrayFindIndexLoopLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"ArrayFindIndexLoopLazyDeoptContinuation"
	.section	.text._ZN2v88internal8Builtins48Generate_ArrayFindIndexLoopLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins48Generate_ArrayFindIndexLoopLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins48Generate_ArrayFindIndexLoopLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins48Generate_ArrayFindIndexLoopLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE:
.LFB22465:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$448, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$741, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L285
.L282:
	movq	%r13, %rdi
	call	_ZN2v88internal48ArrayFindIndexLoopLazyDeoptContinuationAssembler51GenerateArrayFindIndexLoopLazyDeoptContinuationImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L286
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L282
.L286:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22465:
	.size	_ZN2v88internal8Builtins48Generate_ArrayFindIndexLoopLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins48Generate_ArrayFindIndexLoopLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.section	.rodata._ZN2v88internal37Convert5ATSmi17ATconstexpr_int31_1406EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"../../deps/v8/../../deps/v8/src/builtins/base.tq"
	.section	.text._ZN2v88internal37Convert5ATSmi17ATconstexpr_int31_1406EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal37Convert5ATSmi17ATconstexpr_int31_1406EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE
	.type	_ZN2v88internal37Convert5ATSmi17ATconstexpr_int31_1406EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE, @function
_ZN2v88internal37Convert5ATSmi17ATconstexpr_int31_1406EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE:
.LFB22649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-184(%rbp), %r14
	leaq	-656(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-568(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-664(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-376(%rbp), %rbx
	subq	$664, %rsp
	movq	%rdi, -688(%rbp)
	movl	%esi, -692(%rbp)
	movq	%r12, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-624(%rbp), %rax
	movq	%rdi, -664(%rbp)
	movq	%rdi, -624(%rbp)
	movq	%r13, %rdi
	movq	%rax, -680(%rbp)
	movq	$0, -616(%rbp)
	movq	$0, -608(%rbp)
	movq	$0, -600(%rbp)
	movq	$0, -592(%rbp)
	movq	$0, -584(%rbp)
	movq	$0, -576(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$24, %edi
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	%rax, -432(%rbp)
	movq	$0, -408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%rbx, %rdi
	movq	$0, 16(%rax)
	movq	%rdx, -408(%rbp)
	movq	%rdx, -416(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -392(%rbp)
	movq	%rax, -424(%rbp)
	movq	$0, -400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-664(%rbp), %rax
	movl	$24, %edi
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -216(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r14, %rdi
	movq	$0, 16(%rax)
	movq	%rdx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	movups	%xmm0, -200(%rbp)
	movq	$0, -208(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-680(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r15, %rsi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L288
	call	_ZdlPv@PLT
.L288:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -560(%rbp)
	jne	.L356
.L289:
	leaq	-240(%rbp), %rax
	cmpq	$0, -368(%rbp)
	movq	%rax, -680(%rbp)
	jne	.L357
.L292:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-680(%rbp), %rdi
	movq	%r15, %rsi
	movb	$6, (%rax)
	leaq	1(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L295
	call	_ZdlPv@PLT
.L295:
	movq	(%r15), %rax
	movq	%r14, %rdi
	movq	(%rax), %r15
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L296
	call	_ZdlPv@PLT
.L296:
	movq	-224(%rbp), %rax
	movq	-232(%rbp), %r14
	cmpq	%r14, %rax
	je	.L297
	.p2align 4,,10
	.p2align 3
.L301:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L298
	movq	%rax, -680(%rbp)
	addq	$24, %r14
	call	_ZdlPv@PLT
	movq	-680(%rbp), %rax
	cmpq	%r14, %rax
	jne	.L301
.L299:
	movq	-232(%rbp), %r14
.L297:
	testq	%r14, %r14
	je	.L302
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L302:
	movq	%rbx, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L303
	call	_ZdlPv@PLT
.L303:
	movq	-416(%rbp), %rbx
	movq	-424(%rbp), %r14
	cmpq	%r14, %rbx
	je	.L304
	.p2align 4,,10
	.p2align 3
.L308:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L305
	call	_ZdlPv@PLT
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L308
.L306:
	movq	-424(%rbp), %r14
.L304:
	testq	%r14, %r14
	je	.L309
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L309:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L310
	call	_ZdlPv@PLT
.L310:
	movq	-608(%rbp), %rbx
	movq	-616(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L311
	.p2align 4,,10
	.p2align 3
.L315:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L312
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L315
.L313:
	movq	-616(%rbp), %r13
.L311:
	testq	%r13, %r13
	je	.L316
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L316:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L358
	addq	$664, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L312:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L315
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L298:
	addq	$24, %r14
	cmpq	%r14, %rax
	jne	.L301
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L305:
	addq	$24, %r14
	cmpq	%r14, %rbx
	jne	.L308
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L356:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-680(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r15, %rsi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L290
	call	_ZdlPv@PLT
.L290:
	movl	$2650, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	-692(%rbp), %esi
	movq	-688(%rbp), %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	movq	%rax, -680(%rbp)
	call	_Znwm@PLT
	movq	-680(%rbp), %rdx
	leaq	-432(%rbp), %rdi
	movq	%r15, %rsi
	leaq	8(%rax), %rcx
	movq	%rax, -656(%rbp)
	movq	%rdx, (%rax)
	movq	%rcx, -640(%rbp)
	movq	%rcx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L291
	call	_ZdlPv@PLT
.L291:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L357:
	leaq	-432(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r8, -680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-680(%rbp), %r8
	movq	%r15, %rsi
	movb	$6, (%rax)
	leaq	1(%rax), %rdx
	movq	%r8, %rdi
	movq	%rax, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L293
	movq	%rax, -680(%rbp)
	call	_ZdlPv@PLT
	movq	-680(%rbp), %rax
.L293:
	movq	(%rax), %rax
	movl	$91, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	%rcx, -680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -640(%rbp)
	movaps	%xmm0, -656(%rbp)
	call	_Znwm@PLT
	movq	-680(%rbp), %rcx
	movq	%r15, %rsi
	leaq	8(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%rcx, (%rax)
	leaq	-240(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	movq	%rax, -680(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-656(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L294
	call	_ZdlPv@PLT
.L294:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L292
.L358:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22649:
	.size	_ZN2v88internal37Convert5ATSmi17ATconstexpr_int31_1406EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE, .-_ZN2v88internal37Convert5ATSmi17ATconstexpr_int31_1406EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE
	.section	.text._ZN2v88internal39ArrayFindIndexLoopContinuationAssembler42GenerateArrayFindIndexLoopContinuationImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal39ArrayFindIndexLoopContinuationAssembler42GenerateArrayFindIndexLoopContinuationImplEv
	.type	_ZN2v88internal39ArrayFindIndexLoopContinuationAssembler42GenerateArrayFindIndexLoopContinuationImplEv, @function
_ZN2v88internal39ArrayFindIndexLoopContinuationAssembler42GenerateArrayFindIndexLoopContinuationImplEv:
.LFB22526:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$6, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-1520(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1488(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1720, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, -1560(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -1632(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -1648(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, -1664(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, -1680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%rax, -1696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$5, %esi
	movq	%r12, %rdi
	leaq	-1560(%rbp), %r12
	movq	%rax, -1712(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$168, %edi
	movq	$0, -1480(%rbp)
	movq	$0, -1472(%rbp)
	movq	%rax, %rbx
	movq	-1560(%rbp), %rax
	movq	$0, -1464(%rbp)
	movq	%rax, -1488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -1480(%rbp)
	leaq	-1432(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1464(%rbp)
	movq	%rdx, -1472(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1448(%rbp)
	movq	%rax, -1576(%rbp)
	movq	$0, -1456(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1560(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1288(%rbp)
	movq	$0, -1280(%rbp)
	movq	%rax, -1296(%rbp)
	movq	$0, -1272(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1288(%rbp)
	leaq	-1240(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1272(%rbp)
	movq	%rdx, -1280(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1256(%rbp)
	movq	%rax, -1584(%rbp)
	movq	$0, -1264(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1560(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1096(%rbp)
	movq	$0, -1088(%rbp)
	movq	%rax, -1104(%rbp)
	movq	$0, -1080(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1096(%rbp)
	leaq	-1048(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1080(%rbp)
	movq	%rdx, -1088(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1064(%rbp)
	movq	%rax, -1616(%rbp)
	movq	$0, -1072(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1560(%rbp), %rax
	movl	$240, %edi
	movq	$0, -904(%rbp)
	movq	$0, -896(%rbp)
	movq	%rax, -912(%rbp)
	movq	$0, -888(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -904(%rbp)
	leaq	-856(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -888(%rbp)
	movq	%rdx, -896(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -872(%rbp)
	movq	%rax, -1600(%rbp)
	movq	$0, -880(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1560(%rbp), %rax
	movl	$240, %edi
	movq	$0, -712(%rbp)
	movq	$0, -704(%rbp)
	movq	%rax, -720(%rbp)
	movq	$0, -696(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -712(%rbp)
	leaq	-664(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -696(%rbp)
	movq	%rdx, -704(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -680(%rbp)
	movq	%rax, -1608(%rbp)
	movq	$0, -688(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1560(%rbp), %rax
	movl	$192, %edi
	movq	$0, -520(%rbp)
	movq	$0, -512(%rbp)
	movq	%rax, -528(%rbp)
	movq	$0, -504(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -520(%rbp)
	leaq	-472(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -504(%rbp)
	movq	%rdx, -512(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -488(%rbp)
	movq	%rax, -1592(%rbp)
	movq	$0, -496(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1560(%rbp), %rax
	movl	$192, %edi
	movq	$0, -328(%rbp)
	movq	$0, -320(%rbp)
	movq	%rax, -336(%rbp)
	movq	$0, -312(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -328(%rbp)
	leaq	-280(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -312(%rbp)
	movq	%rdx, -320(%rbp)
	xorl	%edx, %edx
	movq	%rax, -1624(%rbp)
	movups	%xmm0, -296(%rbp)
	movq	$0, -304(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$56, %edi
	movq	-1632(%rbp), %xmm1
	movaps	%xmm0, -1520(%rbp)
	movhps	-1648(%rbp), %xmm1
	movq	%rbx, -96(%rbp)
	movaps	%xmm1, -144(%rbp)
	movq	-1664(%rbp), %xmm1
	movq	$0, -1504(%rbp)
	movhps	-1680(%rbp), %xmm1
	movaps	%xmm1, -128(%rbp)
	movq	-1696(%rbp), %xmm1
	movhps	-1712(%rbp), %xmm1
	movaps	%xmm1, -112(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rcx
	movq	%r13, %rdi
	movq	%r15, %rsi
	movdqa	-128(%rbp), %xmm3
	movdqa	-144(%rbp), %xmm2
	leaq	56(%rax), %rdx
	movq	%rax, -1520(%rbp)
	movdqa	-112(%rbp), %xmm4
	movq	%rcx, 48(%rax)
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm4, 32(%rax)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L360
	call	_ZdlPv@PLT
.L360:
	movq	-1576(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1424(%rbp)
	jne	.L524
	cmpq	$0, -1232(%rbp)
	jne	.L525
.L364:
	cmpq	$0, -1040(%rbp)
	jne	.L526
.L368:
	cmpq	$0, -848(%rbp)
	jne	.L527
.L375:
	cmpq	$0, -656(%rbp)
	jne	.L528
.L377:
	cmpq	$0, -464(%rbp)
	jne	.L529
.L380:
	cmpq	$0, -272(%rbp)
	jne	.L530
.L383:
	movq	-1624(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L385
	call	_ZdlPv@PLT
.L385:
	movq	-320(%rbp), %rbx
	movq	-328(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L386
	.p2align 4,,10
	.p2align 3
.L390:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L387
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L390
.L388:
	movq	-328(%rbp), %r13
.L386:
	testq	%r13, %r13
	je	.L391
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L391:
	movq	-1592(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-496(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L392
	call	_ZdlPv@PLT
.L392:
	movq	-512(%rbp), %rbx
	movq	-520(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L393
	.p2align 4,,10
	.p2align 3
.L397:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L394
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L397
.L395:
	movq	-520(%rbp), %r13
.L393:
	testq	%r13, %r13
	je	.L398
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L398:
	movq	-1608(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L399
	call	_ZdlPv@PLT
.L399:
	movq	-704(%rbp), %rbx
	movq	-712(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L400
	.p2align 4,,10
	.p2align 3
.L404:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L401
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L404
.L402:
	movq	-712(%rbp), %r13
.L400:
	testq	%r13, %r13
	je	.L405
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L405:
	movq	-1600(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L406
	call	_ZdlPv@PLT
.L406:
	movq	-896(%rbp), %rbx
	movq	-904(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L407
	.p2align 4,,10
	.p2align 3
.L411:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L408
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L411
.L409:
	movq	-904(%rbp), %r13
.L407:
	testq	%r13, %r13
	je	.L412
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L412:
	movq	-1616(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L413
	call	_ZdlPv@PLT
.L413:
	movq	-1088(%rbp), %rbx
	movq	-1096(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L414
	.p2align 4,,10
	.p2align 3
.L418:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L415
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L418
.L416:
	movq	-1096(%rbp), %r13
.L414:
	testq	%r13, %r13
	je	.L419
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L419:
	movq	-1584(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L420
	call	_ZdlPv@PLT
.L420:
	movq	-1280(%rbp), %rbx
	movq	-1288(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L421
	.p2align 4,,10
	.p2align 3
.L425:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L422
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L425
.L423:
	movq	-1288(%rbp), %r13
.L421:
	testq	%r13, %r13
	je	.L426
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L426:
	movq	-1576(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1456(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L427
	call	_ZdlPv@PLT
.L427:
	movq	-1472(%rbp), %rbx
	movq	-1480(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L428
	.p2align 4,,10
	.p2align 3
.L432:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L429
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L432
.L430:
	movq	-1480(%rbp), %r13
.L428:
	testq	%r13, %r13
	je	.L433
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L433:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L531
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L429:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L432
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L422:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L425
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L415:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L418
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L408:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L411
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L401:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L404
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L387:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L390
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L394:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L397
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L524:
	movq	-1576(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -1504(%rbp)
	movaps	%xmm0, -1520(%rbp)
	call	_Znwm@PLT
	movl	$2055, %r11d
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	$134678279, (%rax)
	leaq	7(%rax), %rdx
	movw	%r11w, 4(%rax)
	movb	$8, 6(%rax)
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1520(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L362
	call	_ZdlPv@PLT
.L362:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rsi
	movq	8(%rax), %rcx
	movq	40(%rax), %rdx
	movq	(%rax), %r13
	movq	%rsi, -1696(%rbp)
	movq	32(%rax), %rsi
	movq	%rcx, -1664(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -1712(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	48(%rax), %rbx
	movq	%rdx, -1648(%rbp)
	movl	$72, %edx
	movq	%rcx, -1680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %xmm0
	movl	$64, %edi
	movq	$0, -1504(%rbp)
	movhps	-1664(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-1680(%rbp), %xmm0
	movhps	-1696(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-1712(%rbp), %xmm0
	movhps	-1648(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%rbx, %xmm0
	movhps	-1648(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1520(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm4
	movq	%r15, %rsi
	leaq	64(%rax), %rdx
	leaq	-1296(%rbp), %rdi
	movq	%rax, -1520(%rbp)
	movups	%xmm3, (%rax)
	movdqa	-112(%rbp), %xmm3
	movups	%xmm4, 16(%rax)
	movdqa	-96(%rbp), %xmm4
	movups	%xmm3, 32(%rax)
	movups	%xmm4, 48(%rax)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L363
	call	_ZdlPv@PLT
.L363:
	movq	-1584(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1232(%rbp)
	je	.L364
.L525:
	movq	-1584(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1296(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -1504(%rbp)
	movaps	%xmm0, -1520(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movabsq	$578721378409580295, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1520(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L365
	call	_ZdlPv@PLT
.L365:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	8(%rax), %rsi
	movq	24(%rax), %rdx
	movq	32(%rax), %rbx
	movq	(%rax), %rcx
	movq	%rsi, -1664(%rbp)
	movq	16(%rax), %rsi
	movq	%rdx, -1696(%rbp)
	movq	40(%rax), %rdx
	movq	%rbx, -1712(%rbp)
	movq	48(%rax), %rbx
	movq	56(%rax), %rax
	movq	%rsi, -1680(%rbp)
	movq	%rdx, -1632(%rbp)
	movq	%rbx, %rdx
	movq	%rax, %rsi
	movq	%rcx, -1648(%rbp)
	movq	%rax, -1728(%rbp)
	call	_ZN2v88internal19NumberIsLessThan_75EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES9_@PLT
	movq	%rbx, %xmm5
	pxor	%xmm0, %xmm0
	movq	-1712(%rbp), %xmm6
	movhps	-1728(%rbp), %xmm5
	movl	$64, %edi
	movaps	%xmm0, -1520(%rbp)
	movq	%rax, %r13
	movq	-1680(%rbp), %xmm7
	movhps	-1632(%rbp), %xmm6
	movaps	%xmm5, -96(%rbp)
	movq	-1648(%rbp), %xmm2
	movaps	%xmm5, -1728(%rbp)
	movhps	-1696(%rbp), %xmm7
	movhps	-1664(%rbp), %xmm2
	movaps	%xmm6, -1712(%rbp)
	movaps	%xmm7, -1680(%rbp)
	movaps	%xmm2, -1648(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movq	$0, -1504(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm5
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm6
	movdqa	-96(%rbp), %xmm3
	leaq	64(%rax), %rdx
	leaq	-1104(%rbp), %rdi
	movups	%xmm2, (%rax)
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L366
	call	_ZdlPv@PLT
.L366:
	movdqa	-1648(%rbp), %xmm4
	movdqa	-1680(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movdqa	-1712(%rbp), %xmm5
	movdqa	-1728(%rbp), %xmm6
	movaps	%xmm0, -1520(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movq	$0, -1504(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm4
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm7
	movdqa	-96(%rbp), %xmm2
	leaq	64(%rax), %rdx
	leaq	-336(%rbp), %rdi
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm7, 32(%rax)
	movups	%xmm2, 48(%rax)
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L367
	call	_ZdlPv@PLT
.L367:
	movq	-1624(%rbp), %rcx
	movq	-1616(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1040(%rbp)
	je	.L368
.L526:
	movq	-1616(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1104(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -1504(%rbp)
	movaps	%xmm0, -1520(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movabsq	$578721378409580295, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1520(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L369
	call	_ZdlPv@PLT
.L369:
	movq	(%rbx), %rax
	leaq	-1552(%rbp), %r13
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	32(%rax), %rdx
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	%rdx, -1664(%rbp)
	movq	40(%rax), %rdx
	movq	%rcx, -1696(%rbp)
	movq	16(%rax), %rcx
	movq	%rdx, -1736(%rbp)
	movq	48(%rax), %rdx
	movq	%rcx, -1712(%rbp)
	movq	24(%rax), %rcx
	movq	56(%rax), %rax
	movq	%rdx, -1744(%rbp)
	movl	$78, %edx
	movq	%rcx, -1648(%rbp)
	movq	%rax, -1680(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$710, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-1520(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	$2, %edi
	movq	%rbx, %r9
	movq	-1664(%rbp), %xmm0
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	pushq	%rdi
	movl	$1, %ecx
	movq	%rax, -1536(%rbp)
	movq	-1504(%rbp), %rax
	movhps	-1680(%rbp), %xmm0
	movq	%r13, %rdi
	movaps	%xmm0, -144(%rbp)
	movq	%rax, -1528(%rbp)
	leaq	-144(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -1752(%rbp)
	leaq	-1536(%rbp), %rax
	pushq	%rsi
	movq	%rax, %rdx
	xorl	%esi, %esi
	movq	%rax, -1728(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	movq	%rax, -1632(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$82, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1648(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19IsUndefinedConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	popq	%r9
	popq	%r10
	testb	%al, %al
	je	.L370
.L372:
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	xorl	%edx, %edx
	movq	%rax, %rsi
.L523:
	movq	%r15, %rdi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movq	%r13, %rdi
	movl	$3, %esi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-1520(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -1760(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movq	-1752(%rbp), %rsi
	movl	$6, %edi
	movq	%rbx, %r9
	pushq	%rdi
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movl	$1, %ecx
	movq	-1712(%rbp), %xmm0
	pushq	%rsi
	movq	%r13, %rdi
	xorl	%esi, %esi
	movq	-1728(%rbp), %rdx
	movq	%rax, -1536(%rbp)
	movhps	-1760(%rbp), %xmm0
	movq	-1504(%rbp), %rax
	movaps	%xmm0, -144(%rbp)
	movq	-1648(%rbp), %xmm0
	movq	%rax, -1528(%rbp)
	movhps	-1632(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-1680(%rbp), %xmm0
	movhps	-1664(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%rbx, %xmm3
	popq	%rdi
	movq	%r13, %rdi
	popq	%r8
	movhps	-1696(%rbp), %xmm3
	movq	%rax, -1728(%rbp)
	movq	-1712(%rbp), %xmm4
	movaps	%xmm3, -1696(%rbp)
	movq	-1664(%rbp), %xmm5
	movq	-1744(%rbp), %xmm6
	movhps	-1648(%rbp), %xmm4
	movhps	-1736(%rbp), %xmm5
	movhps	-1680(%rbp), %xmm6
	movaps	%xmm4, -1712(%rbp)
	movaps	%xmm5, -1664(%rbp)
	movaps	%xmm6, -1648(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$85, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-1728(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal13ToBoolean_240EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEE@PLT
	movdqa	-1696(%rbp), %xmm3
	movq	-1632(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movdqa	-1712(%rbp), %xmm4
	movdqa	-1664(%rbp), %xmm5
	movq	%rax, %r13
	movl	$80, %edi
	movdqa	-1648(%rbp), %xmm6
	movhps	-1728(%rbp), %xmm7
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm7, -1680(%rbp)
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	movaps	%xmm0, -1520(%rbp)
	movq	$0, -1504(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm6
	leaq	-912(%rbp), %rdi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm4
	leaq	80(%rax), %rdx
	movq	%r15, %rsi
	movdqa	-80(%rbp), %xmm7
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm7, 64(%rax)
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L373
	call	_ZdlPv@PLT
.L373:
	movdqa	-1696(%rbp), %xmm2
	movdqa	-1712(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movl	$80, %edi
	movdqa	-1664(%rbp), %xmm6
	movdqa	-1648(%rbp), %xmm3
	movaps	%xmm0, -1520(%rbp)
	movdqa	-1680(%rbp), %xmm4
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movq	$0, -1504(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm2
	leaq	-720(%rbp), %rdi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	leaq	80(%rax), %rdx
	movq	%r15, %rsi
	movdqa	-80(%rbp), %xmm3
	movups	%xmm7, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm3, 64(%rax)
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L374
	call	_ZdlPv@PLT
.L374:
	movq	-1608(%rbp), %rcx
	movq	-1600(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -848(%rbp)
	je	.L375
.L527:
	movq	-1600(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-912(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -1504(%rbp)
	movaps	%xmm0, -1520(%rbp)
	call	_Znwm@PLT
	movl	$2056, %esi
	movq	%r13, %rdi
	movabsq	$578721378409580295, %rcx
	movw	%si, 8(%rax)
	leaq	10(%rax), %rdx
	movq	%r15, %rsi
	movq	%rcx, (%rax)
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1520(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L376
	call	_ZdlPv@PLT
.L376:
	movq	(%rbx), %rax
	movl	$86, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	56(%rax), %r13
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -656(%rbp)
	je	.L377
.L528:
	movq	-1608(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-720(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -1504(%rbp)
	movaps	%xmm0, -1520(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movabsq	$578721378409580295, %rcx
	movq	%rcx, (%rax)
	movl	$2056, %ecx
	leaq	10(%rax), %rdx
	movw	%cx, 8(%rax)
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1520(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L378
	call	_ZdlPv@PLT
.L378:
	movq	(%rbx), %rax
	movl	$72, %edx
	movq	%r12, %rdi
	movq	24(%rax), %rbx
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	32(%rax), %r13
	movq	%rbx, -1696(%rbp)
	movq	40(%rax), %rbx
	movq	%rsi, -1664(%rbp)
	movq	16(%rax), %rsi
	movq	%rbx, -1712(%rbp)
	movq	48(%rax), %rbx
	movq	56(%rax), %rax
	movq	%rsi, -1680(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -1648(%rbp)
	movq	%rax, -1632(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$64, %edi
	movq	-1648(%rbp), %xmm0
	movq	$0, -1504(%rbp)
	movhps	-1664(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-1680(%rbp), %xmm0
	movhps	-1696(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	%r13, %xmm0
	movhps	-1712(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%rbx, %xmm0
	movhps	-1632(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1520(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm4
	movdqa	-128(%rbp), %xmm7
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm5
	leaq	64(%rax), %rdx
	leaq	-528(%rbp), %rdi
	movups	%xmm4, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm5, 48(%rax)
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L379
	call	_ZdlPv@PLT
.L379:
	movq	-1592(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -464(%rbp)
	je	.L380
.L529:
	movq	-1592(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-528(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -1504(%rbp)
	movaps	%xmm0, -1520(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movabsq	$578721378409580295, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1520(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L381
	call	_ZdlPv@PLT
.L381:
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	8(%rax), %rsi
	movq	24(%rax), %rbx
	movq	40(%rax), %rdx
	movq	(%rax), %rcx
	movq	%rsi, -1664(%rbp)
	movq	16(%rax), %rsi
	movq	%rbx, -1696(%rbp)
	movq	32(%rax), %rbx
	movq	%rdx, -1632(%rbp)
	movq	48(%rax), %rdx
	movq	%rsi, -1680(%rbp)
	movl	$1, %esi
	movq	56(%rax), %r13
	movq	%rcx, -1648(%rbp)
	movq	%rdx, -1728(%rbp)
	movq	%rbx, -1712(%rbp)
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler9NumberAddENS0_8compiler11SloppyTNodeINS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_@PLT
	movq	%r15, %rdi
	movq	%rax, -1736(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$64, %edi
	movq	-1648(%rbp), %xmm0
	movq	$0, -1504(%rbp)
	movhps	-1664(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-1680(%rbp), %xmm0
	movhps	-1696(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-1712(%rbp), %xmm0
	movhps	-1632(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	-1728(%rbp), %xmm0
	movhps	-1736(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1520(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm4
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm7
	leaq	64(%rax), %rdx
	leaq	-1296(%rbp), %rdi
	movups	%xmm6, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm7, 48(%rax)
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L382
	call	_ZdlPv@PLT
.L382:
	movq	-1584(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -272(%rbp)
	je	.L383
.L530:
	movq	-1624(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-336(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -1504(%rbp)
	movaps	%xmm0, -1520(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	movabsq	$578721378409580295, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -1520(%rbp)
	movq	%rdx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L384
	call	_ZdlPv@PLT
.L384:
	movl	$91, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r14, %rdi
	movl	$-1, %esi
	call	_ZN2v88internal37Convert5ATSmi17ATconstexpr_int31_1406EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L370:
	movq	-1648(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IsNullConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L372
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	movq	%rax, %rsi
	jmp	.L523
.L531:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22526:
	.size	_ZN2v88internal39ArrayFindIndexLoopContinuationAssembler42GenerateArrayFindIndexLoopContinuationImplEv, .-_ZN2v88internal39ArrayFindIndexLoopContinuationAssembler42GenerateArrayFindIndexLoopContinuationImplEv
	.section	.rodata._ZN2v88internal8Builtins39Generate_ArrayFindIndexLoopContinuationEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"ArrayFindIndexLoopContinuation"
	.section	.text._ZN2v88internal8Builtins39Generate_ArrayFindIndexLoopContinuationEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins39Generate_ArrayFindIndexLoopContinuationEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins39Generate_ArrayFindIndexLoopContinuationEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins39Generate_ArrayFindIndexLoopContinuationEPNS0_8compiler18CodeAssemblerStateE:
.LFB22522:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$862, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$743, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L536
.L533:
	movq	%r13, %rdi
	call	_ZN2v88internal39ArrayFindIndexLoopContinuationAssembler42GenerateArrayFindIndexLoopContinuationImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L537
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L536:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L533
.L537:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22522:
	.size	_ZN2v88internal8Builtins39Generate_ArrayFindIndexLoopContinuationEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins39Generate_ArrayFindIndexLoopContinuationEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_S4_NS0_10JSReceiverES5_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_SF_SF_SF_SF_SF_PNSB_IS5_EESH_PNSB_IS9_EESJ_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_S4_NS0_10JSReceiverES5_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_SF_SF_SF_SF_SF_PNSB_IS5_EESH_PNSB_IS9_EESJ_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_S4_NS0_10JSReceiverES5_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_SF_SF_SF_SF_SF_PNSB_IS5_EESH_PNSB_IS9_EESJ_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_S4_NS0_10JSReceiverES5_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_SF_SF_SF_SF_SF_PNSB_IS5_EESH_PNSB_IS9_EESJ_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_S4_NS0_10JSReceiverES5_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_SF_SF_SF_SF_SF_PNSB_IS5_EESH_PNSB_IS9_EESJ_:
.LFB27179:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$12, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$120, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$578721382704613383, %rcx
	movq	%rcx, (%rax)
	leaq	12(%rax), %rdx
	movl	$134743815, 8(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L539
	movq	%rax, -152(%rbp)
	call	_ZdlPv@PLT
	movq	-152(%rbp), %rax
.L539:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L540
	movq	%rdx, (%r15)
.L540:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L541
	movq	%rdx, (%r14)
.L541:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L542
	movq	%rdx, 0(%r13)
.L542:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L543
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L543:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L544
	movq	%rdx, (%rbx)
.L544:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L545
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L545:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L546
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L546:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L547
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L547:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L548
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L548:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L549
	movq	-128(%rbp), %rsi
	movq	%rdx, (%rsi)
.L549:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L550
	movq	-136(%rbp), %rcx
	movq	%rdx, (%rcx)
.L550:
	movq	88(%rax), %rax
	testq	%rax, %rax
	je	.L538
	movq	-144(%rbp), %rbx
	movq	%rax, (%rbx)
.L538:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L593
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L593:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27179:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_S4_NS0_10JSReceiverES5_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_SF_SF_SF_SF_SF_PNSB_IS5_EESH_PNSB_IS9_EESJ_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_S4_NS0_10JSReceiverES5_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_SF_SF_SF_SF_SF_PNSB_IS5_EESH_PNSB_IS9_EESJ_
	.section	.text._ZN2v88internal61ArrayFindIndexLoopAfterCallbackLazyDeoptContinuationAssembler64GenerateArrayFindIndexLoopAfterCallbackLazyDeoptContinuationImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal61ArrayFindIndexLoopAfterCallbackLazyDeoptContinuationAssembler64GenerateArrayFindIndexLoopAfterCallbackLazyDeoptContinuationImplEv
	.type	_ZN2v88internal61ArrayFindIndexLoopAfterCallbackLazyDeoptContinuationAssembler64GenerateArrayFindIndexLoopAfterCallbackLazyDeoptContinuationImplEv, @function
_ZN2v88internal61ArrayFindIndexLoopAfterCallbackLazyDeoptContinuationAssembler64GenerateArrayFindIndexLoopAfterCallbackLazyDeoptContinuationImplEv:
.LFB22478:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$312, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	$9, %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rdi, %r12
	leaq	-3752(%rbp), %r13
	leaq	-3808(%rbp), %rbx
	leaq	-3936(%rbp), %r15
	movq	%rax, -4104(%rbp)
	movq	%rax, -4088(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -4128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	movq	%rax, -4192(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$2, %esi
	movq	%rax, -4208(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$3, %esi
	movq	%rax, -4224(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$4, %esi
	movq	%rax, -4240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movl	$5, %esi
	movq	%rax, -4256(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$6, %esi
	movq	%r12, %rdi
	leaq	-4088(%rbp), %r12
	movq	%rax, -4304(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movl	$192, %edi
	movq	$0, -3800(%rbp)
	movq	%rax, -4320(%rbp)
	movq	-4088(%rbp), %rax
	movq	$0, -3792(%rbp)
	movq	%rax, -3808(%rbp)
	movq	$0, -3784(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r13, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rdx, -3784(%rbp)
	movq	%rdx, -3792(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3768(%rbp)
	movq	%rax, -3800(%rbp)
	movq	$0, -3776(%rbp)
	movq	%rbx, -4288(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-3616(%rbp), %rax
	movl	$9, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -4264(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3424(%rbp), %rax
	movl	$10, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -4160(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3232(%rbp), %rax
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -4176(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-3040(%rbp), %rax
	movl	$9, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -4144(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2848(%rbp), %rax
	movl	$10, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -4272(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-4088(%rbp), %rax
	movl	$264, %edi
	movq	$0, -2648(%rbp)
	movq	$0, -2640(%rbp)
	movq	%rax, -2656(%rbp)
	movq	$0, -2632(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movups	%xmm0, (%rax)
	leaq	264(%rax), %rdx
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -2648(%rbp)
	leaq	-2600(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2632(%rbp)
	movq	%rdx, -2640(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2616(%rbp)
	movq	%rax, -4416(%rbp)
	movq	$0, -2624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	leaq	-2464(%rbp), %rax
	movl	$9, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -4112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2272(%rbp), %rax
	movl	$10, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -4152(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-2080(%rbp), %rax
	movl	$11, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -4280(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1888(%rbp), %rax
	movl	$12, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -4168(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	leaq	-1696(%rbp), %rax
	movl	$10, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -4136(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseC2EPNS1_13CodeAssemblerEmNS1_18CodeAssemblerLabel4TypeE.constprop.0
	movq	-4088(%rbp), %rax
	movl	$264, %edi
	movq	$0, -1496(%rbp)
	movq	$0, -1488(%rbp)
	movq	%rax, -1504(%rbp)
	movq	$0, -1480(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -1496(%rbp)
	leaq	-1448(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1480(%rbp)
	movq	%rdx, -1488(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1464(%rbp)
	movq	%rax, -4384(%rbp)
	movq	$0, -1472(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4088(%rbp), %rax
	movl	$288, %edi
	movq	$0, -1304(%rbp)
	movq	$0, -1296(%rbp)
	movq	%rax, -1312(%rbp)
	movq	$0, -1288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -1304(%rbp)
	leaq	-1256(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1288(%rbp)
	movq	%rdx, -1296(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1272(%rbp)
	movq	%rax, -4424(%rbp)
	movq	$0, -1280(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4088(%rbp), %rax
	movl	$312, %edi
	movq	$0, -1112(%rbp)
	movq	$0, -1104(%rbp)
	movq	%rax, -1120(%rbp)
	movq	$0, -1096(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	312(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 304(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movq	%rax, -1112(%rbp)
	leaq	-1064(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1096(%rbp)
	movq	%rdx, -1104(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1080(%rbp)
	movq	%rax, -4336(%rbp)
	movq	$0, -1088(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4088(%rbp), %rax
	movl	$264, %edi
	movq	$0, -920(%rbp)
	movq	$0, -912(%rbp)
	movq	%rax, -928(%rbp)
	movq	$0, -904(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	264(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 256(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movq	%rax, -920(%rbp)
	leaq	-872(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -904(%rbp)
	movq	%rdx, -912(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -888(%rbp)
	movq	%rax, -4368(%rbp)
	movq	$0, -896(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4088(%rbp), %rax
	movl	$288, %edi
	movq	$0, -728(%rbp)
	movq	$0, -720(%rbp)
	movq	%rax, -736(%rbp)
	movq	$0, -712(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -728(%rbp)
	leaq	-680(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -712(%rbp)
	movq	%rdx, -720(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -696(%rbp)
	movq	%rax, -4400(%rbp)
	movq	$0, -704(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4088(%rbp), %rax
	movl	$288, %edi
	movq	$0, -536(%rbp)
	movq	$0, -528(%rbp)
	movq	%rax, -544(%rbp)
	movq	$0, -520(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -536(%rbp)
	leaq	-488(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -520(%rbp)
	movq	%rdx, -528(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -504(%rbp)
	movq	%rax, -4352(%rbp)
	movq	$0, -512(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4088(%rbp), %rax
	movl	$288, %edi
	movq	$0, -344(%rbp)
	movq	$0, -336(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -328(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	288(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movq	%rax, -344(%rbp)
	leaq	-296(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -328(%rbp)
	movq	%rdx, -336(%rbp)
	xorl	%edx, %edx
	movq	%rax, -4344(%rbp)
	movups	%xmm0, -312(%rbp)
	movq	$0, -320(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	-4128(%rbp), %xmm1
	movaps	%xmm0, -3936(%rbp)
	movhps	-4192(%rbp), %xmm1
	movq	$0, -3920(%rbp)
	movaps	%xmm1, -160(%rbp)
	movq	-4208(%rbp), %xmm1
	movhps	-4224(%rbp), %xmm1
	movaps	%xmm1, -144(%rbp)
	movq	-4240(%rbp), %xmm1
	movhps	-4256(%rbp), %xmm1
	movaps	%xmm1, -128(%rbp)
	movq	-4304(%rbp), %xmm1
	movhps	-4320(%rbp), %xmm1
	movaps	%xmm1, -112(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm7
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm5
	leaq	64(%rax), %rdx
	movq	%rax, -3936(%rbp)
	movdqa	-144(%rbp), %xmm6
	movups	%xmm7, 32(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm4, 48(%rax)
	movq	%rdx, -3920(%rbp)
	movq	%rdx, -3928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L595
	call	_ZdlPv@PLT
.L595:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3744(%rbp)
	jne	.L767
	cmpq	$0, -3552(%rbp)
	jne	.L768
.L601:
	cmpq	$0, -3360(%rbp)
	jne	.L769
.L604:
	cmpq	$0, -3168(%rbp)
	jne	.L770
.L607:
	leaq	-2656(%rbp), %rax
	cmpq	$0, -2976(%rbp)
	movq	%rax, -4208(%rbp)
	jne	.L771
	cmpq	$0, -2784(%rbp)
	jne	.L772
.L614:
	cmpq	$0, -2592(%rbp)
	jne	.L773
.L617:
	cmpq	$0, -2400(%rbp)
	jne	.L774
.L620:
	cmpq	$0, -2208(%rbp)
	jne	.L775
.L622:
	cmpq	$0, -2016(%rbp)
	jne	.L776
.L627:
	leaq	-1504(%rbp), %rax
	cmpq	$0, -1824(%rbp)
	movq	%rax, -4192(%rbp)
	jne	.L777
.L630:
	cmpq	$0, -1632(%rbp)
	jne	.L778
.L633:
	leaq	-1120(%rbp), %rax
	cmpq	$0, -1440(%rbp)
	movq	%rax, -4224(%rbp)
	leaq	-1312(%rbp), %rax
	movq	%rax, -4128(%rbp)
	jne	.L779
.L635:
	cmpq	$0, -1248(%rbp)
	leaq	-928(%rbp), %r14
	jne	.L780
.L640:
	leaq	-736(%rbp), %rax
	cmpq	$0, -1056(%rbp)
	movq	%rax, -4240(%rbp)
	jne	.L781
.L643:
	cmpq	$0, -864(%rbp)
	jne	.L782
.L646:
	leaq	-544(%rbp), %rax
	cmpq	$0, -672(%rbp)
	leaq	-352(%rbp), %r13
	movq	%rax, -4256(%rbp)
	jne	.L783
.L648:
	cmpq	$0, -480(%rbp)
	jne	.L784
.L651:
	cmpq	$0, -288(%rbp)
	jne	.L785
.L652:
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4256(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4240(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4224(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4128(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4192(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4136(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4168(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4280(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4152(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4112(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4208(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4272(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4144(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4176(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4160(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4264(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-4288(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L786
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L767:
	.cfi_restore_state
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3920(%rbp)
	movaps	%xmm0, -3936(%rbp)
	call	_Znwm@PLT
	movq	-4288(%rbp), %rdi
	movq	%r15, %rsi
	movabsq	$578721382704613383, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3936(%rbp)
	movq	%rdx, -3920(%rbp)
	movq	%rdx, -3928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3936(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L597
	call	_ZdlPv@PLT
.L597:
	movq	(%rbx), %rax
	movl	$48, %edx
	movq	%r12, %rdi
	movq	40(%rax), %rsi
	movq	24(%rax), %rcx
	movq	(%rax), %r13
	movq	8(%rax), %rbx
	movq	%rsi, -4208(%rbp)
	movq	48(%rax), %rsi
	movq	16(%rax), %r14
	movq	%rcx, -4128(%rbp)
	movq	32(%rax), %rcx
	movq	56(%rax), %rax
	movq	%rsi, -4224(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rax, -4240(%rbp)
	movq	%rcx, -4192(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	-4104(%rbp), %rdi
	call	_ZN2v88internal21Cast10JSReceiver_1404EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm4
	movq	%r14, %xmm7
	movq	-4224(%rbp), %xmm5
	movq	%r13, %xmm3
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rdx
	movq	-4192(%rbp), %xmm6
	leaq	-3968(%rbp), %r14
	punpcklqdq	%xmm4, %xmm3
	movq	%rax, -88(%rbp)
	leaq	-160(%rbp), %r13
	movhps	-4208(%rbp), %xmm6
	movhps	-4240(%rbp), %xmm5
	movq	%r13, %rsi
	movq	%r14, %rdi
	movhps	-4128(%rbp), %xmm7
	movaps	%xmm5, -4224(%rbp)
	movaps	%xmm6, -4192(%rbp)
	movaps	%xmm7, -4128(%rbp)
	movaps	%xmm3, -4208(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm0, -3968(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -3952(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4160(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L598
	call	_ZdlPv@PLT
.L598:
	leaq	-3368(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3928(%rbp)
	jne	.L787
.L599:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -3552(%rbp)
	je	.L601
.L768:
	leaq	-3560(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-160(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-151(%rbp), %rdx
	movaps	%xmm0, -3936(%rbp)
	movabsq	$578721382704613383, %rax
	movq	%rax, -160(%rbp)
	movb	$8, -152(%rbp)
	movq	$0, -3920(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4264(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3936(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L602
	call	_ZdlPv@PLT
.L602:
	movq	(%rbx), %rax
	leaq	-96(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movdqu	32(%rax), %xmm0
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm1
	movdqu	48(%rax), %xmm7
	movq	$0, -3920(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -3936(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4176(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L603
	call	_ZdlPv@PLT
.L603:
	leaq	-3176(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3360(%rbp)
	je	.L604
.L769:
	leaq	-3368(%rbp), %rsi
	movq	%r12, %rdi
	movl	$1800, %ebx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-160(%rbp), %r13
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	movabsq	$578721382704613383, %rax
	leaq	-150(%rbp), %rdx
	movq	%r13, %rsi
	movw	%bx, -152(%rbp)
	movq	%rax, -160(%rbp)
	movaps	%xmm0, -3936(%rbp)
	movq	$0, -3920(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4160(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3936(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L605
	call	_ZdlPv@PLT
.L605:
	movq	(%rbx), %rax
	leaq	-88(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movdqu	48(%rax), %xmm0
	movdqu	(%rax), %xmm3
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	72(%rax), %rax
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -160(%rbp)
	movq	%rax, -96(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3936(%rbp)
	movq	$0, -3920(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4144(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L606
	call	_ZdlPv@PLT
.L606:
	leaq	-2984(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3168(%rbp)
	je	.L607
.L770:
	leaq	-3176(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -3920(%rbp)
	movaps	%xmm0, -3936(%rbp)
	call	_Znwm@PLT
	movq	-4176(%rbp), %rdi
	movq	%r15, %rsi
	movabsq	$578721382704613383, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -3936(%rbp)
	movq	%rdx, -3920(%rbp)
	movq	%rdx, -3928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L608
	call	_ZdlPv@PLT
.L608:
	movq	-4104(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L771:
	leaq	-2984(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -3920(%rbp)
	movaps	%xmm0, -3936(%rbp)
	call	_Znwm@PLT
	movq	-4144(%rbp), %rdi
	movq	%r15, %rsi
	movabsq	$578721382704613383, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%rax, -3936(%rbp)
	movq	%rdx, -3920(%rbp)
	movq	%rdx, -3928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3936(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L610
	call	_ZdlPv@PLT
.L610:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	40(%rax), %rsi
	movq	56(%rax), %rdx
	movq	(%rax), %r13
	movq	16(%rax), %rbx
	movq	64(%rax), %r14
	movq	%rcx, -4128(%rbp)
	movq	24(%rax), %rcx
	movq	%rsi, -4224(%rbp)
	movq	48(%rax), %rsi
	movq	%rdx, -4256(%rbp)
	movl	$49, %edx
	movq	%rcx, -4192(%rbp)
	movq	32(%rax), %rcx
	movq	%rsi, -4240(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -4208(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	-4104(%rbp), %rdi
	call	_ZN2v88internal82Cast71UT19ATCallableApiObject17ATCallableJSProxy15JSBoundFunction10JSFunction_1405EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm4
	movq	%rbx, %xmm7
	movq	-4240(%rbp), %xmm2
	punpcklqdq	%xmm7, %xmm4
	pxor	%xmm0, %xmm0
	leaq	-72(%rbp), %rdx
	movq	%rax, -80(%rbp)
	movq	%r13, %xmm7
	leaq	-3968(%rbp), %r14
	movaps	%xmm4, -96(%rbp)
	movq	-4208(%rbp), %xmm5
	movq	%rbx, %xmm6
	leaq	-160(%rbp), %r13
	movq	%r14, %rdi
	movaps	%xmm4, -4304(%rbp)
	movhps	-4256(%rbp), %xmm2
	movhps	-4224(%rbp), %xmm5
	movq	%r13, %rsi
	movaps	%xmm0, -3968(%rbp)
	movhps	-4192(%rbp), %xmm6
	movhps	-4128(%rbp), %xmm7
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm2, -4240(%rbp)
	movaps	%xmm5, -4224(%rbp)
	movaps	%xmm6, -4192(%rbp)
	movaps	%xmm7, -4128(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm2, -112(%rbp)
	movq	$0, -3952(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2656(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4208(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L611
	call	_ZdlPv@PLT
.L611:
	movq	-4416(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3928(%rbp)
	jne	.L788
.L612:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2784(%rbp)
	je	.L614
.L772:
	leaq	-2792(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-160(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	movabsq	$578721382704613383, %rax
	movl	$2055, %r11d
	leaq	-150(%rbp), %rdx
	movaps	%xmm0, -3936(%rbp)
	movq	%rax, -160(%rbp)
	movw	%r11w, -152(%rbp)
	movq	$0, -3920(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4272(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3936(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L615
	call	_ZdlPv@PLT
.L615:
	movq	(%rbx), %rax
	leaq	-88(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movdqu	48(%rax), %xmm0
	movdqu	(%rax), %xmm3
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	64(%rax), %rax
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -160(%rbp)
	movq	%rax, -96(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3936(%rbp)
	movq	$0, -3920(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4112(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L616
	call	_ZdlPv@PLT
.L616:
	leaq	-2408(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2592(%rbp)
	je	.L617
.L773:
	movq	-4416(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-160(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	movabsq	$578721382704613383, %rax
	movl	$2055, %r10d
	leaq	-149(%rbp), %rdx
	movaps	%xmm0, -3936(%rbp)
	movq	%rax, -160(%rbp)
	movw	%r10w, -152(%rbp)
	movb	$7, -150(%rbp)
	movq	$0, -3920(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4208(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3936(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L618
	call	_ZdlPv@PLT
.L618:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	40(%rax), %rdi
	movq	48(%rax), %rsi
	movq	64(%rax), %rdx
	movq	8(%rax), %r11
	movq	16(%rax), %r10
	movq	24(%rax), %r9
	movq	32(%rax), %r8
	movq	56(%rax), %rcx
	movq	(%rax), %rbx
	movq	80(%rax), %rax
	movq	%rdi, -120(%rbp)
	movq	%r15, %rdi
	movq	%rsi, -112(%rbp)
	movq	%r13, %rsi
	movq	%rdx, -96(%rbp)
	leaq	-80(%rbp), %rdx
	movq	%r11, -152(%rbp)
	movq	%r10, -144(%rbp)
	movq	%r9, -136(%rbp)
	movq	%r8, -128(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%rax, -88(%rbp)
	movaps	%xmm0, -3936(%rbp)
	movq	%rbx, -160(%rbp)
	movq	$0, -3920(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4152(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L619
	call	_ZdlPv@PLT
.L619:
	leaq	-2216(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2400(%rbp)
	je	.L620
.L774:
	leaq	-2408(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$578721382704613383, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -3920(%rbp)
	movaps	%xmm0, -3936(%rbp)
	call	_Znwm@PLT
	movq	-4112(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%rax, -3936(%rbp)
	movq	%rdx, -3920(%rbp)
	movq	%rdx, -3928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L621
	call	_ZdlPv@PLT
.L621:
	movq	-4104(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	cmpq	$0, -2208(%rbp)
	je	.L622
.L775:
	leaq	-2216(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$578721382704613383, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -3920(%rbp)
	movaps	%xmm0, -3936(%rbp)
	call	_Znwm@PLT
	movl	$1799, %r9d
	movq	-4152(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, (%rax)
	leaq	10(%rax), %rdx
	movw	%r9w, 8(%rax)
	movq	%rax, -3936(%rbp)
	movq	%rdx, -3920(%rbp)
	movq	%rdx, -3928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3936(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L623
	call	_ZdlPv@PLT
.L623:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rbx
	movq	40(%rax), %rsi
	movq	56(%rax), %rdx
	movq	(%rax), %rcx
	movq	16(%rax), %r14
	movq	64(%rax), %r13
	movq	%rbx, -4192(%rbp)
	movq	24(%rax), %rbx
	movq	%rsi, -4240(%rbp)
	movq	48(%rax), %rsi
	movq	%rdx, -4304(%rbp)
	movl	$50, %edx
	movq	%rbx, -4224(%rbp)
	movq	32(%rax), %rbx
	movq	72(%rax), %rax
	movq	%rsi, -4256(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -4128(%rbp)
	movq	%rax, -4320(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4104(%rbp), %rdi
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal29Cast20UT5ATSmi10HeapNumber_85EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %xmm3
	movq	%r14, %xmm5
	movq	-4256(%rbp), %xmm4
	leaq	-3968(%rbp), %r14
	movq	%rbx, %xmm2
	movq	-4128(%rbp), %xmm6
	leaq	-160(%rbp), %r13
	movhps	-4320(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	leaq	-64(%rbp), %rdx
	movq	%r13, %rsi
	movhps	-4304(%rbp), %xmm4
	movq	%r14, %rdi
	movq	%rax, -72(%rbp)
	movhps	-4240(%rbp), %xmm2
	movhps	-4224(%rbp), %xmm5
	movhps	-4192(%rbp), %xmm6
	movaps	%xmm3, -4320(%rbp)
	movaps	%xmm4, -4256(%rbp)
	movaps	%xmm2, -4240(%rbp)
	movaps	%xmm5, -4224(%rbp)
	movaps	%xmm6, -4128(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm0, -3968(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -3952(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4168(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L624
	call	_ZdlPv@PLT
.L624:
	leaq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3928(%rbp)
	jne	.L789
.L625:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2016(%rbp)
	je	.L627
.L776:
	leaq	-2024(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-160(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	movabsq	$578721382704613383, %rax
	movl	$1799, %r8d
	leaq	-149(%rbp), %rdx
	movaps	%xmm0, -3936(%rbp)
	movq	%rax, -160(%rbp)
	movw	%r8w, -152(%rbp)
	movb	$8, -150(%rbp)
	movq	$0, -3920(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4280(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3936(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L628
	call	_ZdlPv@PLT
.L628:
	movq	(%rbx), %rax
	leaq	-80(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movdqu	48(%rax), %xmm0
	movdqu	(%rax), %xmm3
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movdqu	64(%rax), %xmm7
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -3936(%rbp)
	movq	$0, -3920(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4136(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L629
	call	_ZdlPv@PLT
.L629:
	leaq	-1640(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L778:
	leaq	-1640(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -3920(%rbp)
	movaps	%xmm0, -3936(%rbp)
	call	_Znwm@PLT
	movl	$1799, %edi
	movq	%r15, %rsi
	movabsq	$578721382704613383, %rcx
	movw	%di, 8(%rax)
	movq	-4136(%rbp), %rdi
	leaq	10(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%rax, -3936(%rbp)
	movq	%rdx, -3920(%rbp)
	movq	%rdx, -3928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L634
	call	_ZdlPv@PLT
.L634:
	movq	-4104(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L777:
	leaq	-1832(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-160(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-148(%rbp), %rdx
	movabsq	$578721382704613383, %rax
	movaps	%xmm0, -3936(%rbp)
	movq	%rax, -160(%rbp)
	movl	$134743815, -152(%rbp)
	movq	$0, -3920(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4168(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3936(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L631
	call	_ZdlPv@PLT
.L631:
	movq	(%rbx), %rax
	leaq	-72(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movdqu	64(%rax), %xmm0
	movdqu	(%rax), %xmm4
	movdqu	16(%rax), %xmm3
	movdqu	32(%rax), %xmm2
	movdqu	48(%rax), %xmm1
	movq	88(%rax), %rax
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -160(%rbp)
	movq	%rax, -80(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3936(%rbp)
	movq	$0, -3920(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1504(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -4192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L632
	call	_ZdlPv@PLT
.L632:
	movq	-4384(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L779:
	movq	-4384(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$11, %edi
	movq	$0, -3920(%rbp)
	movaps	%xmm0, -3936(%rbp)
	call	_Znwm@PLT
	movl	$1799, %esi
	movabsq	$578721382704613383, %rcx
	movq	-4192(%rbp), %rdi
	movw	%si, 8(%rax)
	leaq	11(%rax), %rdx
	movq	%r15, %rsi
	movq	%rcx, (%rax)
	movb	$8, 10(%rax)
	movq	%rax, -3936(%rbp)
	movq	%rdx, -3920(%rbp)
	movq	%rdx, -3928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3936(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L636
	call	_ZdlPv@PLT
.L636:
	movq	(%rbx), %rax
	movq	24(%rax), %rsi
	movq	56(%rax), %rdi
	movq	8(%rax), %rcx
	movq	48(%rax), %rdx
	movq	(%rax), %rbx
	movq	40(%rax), %r14
	movq	%rsi, -4256(%rbp)
	movq	32(%rax), %rsi
	movq	%rdi, -4384(%rbp)
	movq	72(%rax), %rdi
	movq	64(%rax), %r13
	movq	%rcx, -4224(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -4304(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -4320(%rbp)
	movl	$51, %edx
	movq	%rdi, -4416(%rbp)
	movq	%r12, %rdi
	movq	%rbx, -4128(%rbp)
	movq	80(%rax), %rbx
	movq	%rcx, -4240(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-4104(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	call	_ZN2v88internal29Cast20UT5ATSmi10HeapNumber_85EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r14, %xmm5
	movq	%r14, %xmm3
	movq	-4304(%rbp), %xmm2
	movq	%rbx, %xmm7
	pxor	%xmm0, %xmm0
	leaq	-56(%rbp), %rdx
	movq	-4320(%rbp), %xmm4
	punpcklqdq	%xmm5, %xmm2
	punpcklqdq	%xmm3, %xmm7
	movq	-4240(%rbp), %xmm5
	movq	-4128(%rbp), %xmm6
	movq	%r13, %xmm3
	leaq	-3968(%rbp), %r14
	leaq	-160(%rbp), %r13
	movq	%rax, -64(%rbp)
	movhps	-4256(%rbp), %xmm5
	movq	%r13, %rsi
	movq	%r14, %rdi
	movaps	%xmm7, -4448(%rbp)
	movhps	-4416(%rbp), %xmm3
	movhps	-4384(%rbp), %xmm4
	movhps	-4224(%rbp), %xmm6
	movaps	%xmm2, -4304(%rbp)
	movaps	%xmm3, -4416(%rbp)
	movaps	%xmm4, -4320(%rbp)
	movaps	%xmm5, -4240(%rbp)
	movaps	%xmm6, -4256(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	movaps	%xmm0, -3968(%rbp)
	movq	$0, -3952(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1120(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -4224(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L637
	call	_ZdlPv@PLT
.L637:
	movq	-4336(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-1312(%rbp), %rax
	cmpq	$0, -3928(%rbp)
	movq	%rax, -4128(%rbp)
	jne	.L790
.L638:
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L782:
	movq	-4368(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$578721382704613383, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$11, %edi
	movq	$0, -3920(%rbp)
	movaps	%xmm0, -3936(%rbp)
	call	_Znwm@PLT
	movl	$1799, %ecx
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbx, (%rax)
	leaq	11(%rax), %rdx
	movw	%cx, 8(%rax)
	movb	$8, 10(%rax)
	movq	%rax, -3936(%rbp)
	movq	%rdx, -3920(%rbp)
	movq	%rdx, -3928(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L647
	call	_ZdlPv@PLT
.L647:
	movq	-4104(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler11UnreachableEv@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L781:
	movq	-4336(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-160(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-147(%rbp), %rdx
	movabsq	$578721382704613383, %rax
	movaps	%xmm0, -3936(%rbp)
	movq	%rax, -160(%rbp)
	movl	$134743815, -152(%rbp)
	movb	$8, -148(%rbp)
	movq	$0, -3920(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4224(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3936(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L644
	call	_ZdlPv@PLT
.L644:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	8(%rax), %rcx
	movq	(%rax), %rbx
	movq	24(%rax), %r11
	movq	32(%rax), %r10
	movq	40(%rax), %r9
	movq	48(%rax), %r8
	movq	%rcx, -4240(%rbp)
	movq	16(%rax), %rcx
	movq	56(%rax), %rdi
	movq	64(%rax), %rsi
	movq	80(%rax), %rdx
	movq	%rcx, -4256(%rbp)
	movq	72(%rax), %rcx
	movq	96(%rax), %rax
	movq	%rbx, -160(%rbp)
	movq	-4240(%rbp), %rbx
	movq	%rdi, -104(%rbp)
	movq	%r15, %rdi
	movq	%rsi, -96(%rbp)
	movq	%r13, %rsi
	movq	%rbx, -152(%rbp)
	movq	-4256(%rbp), %rbx
	movq	%rdx, -80(%rbp)
	leaq	-64(%rbp), %rdx
	movq	%r11, -136(%rbp)
	movq	%r10, -128(%rbp)
	movq	%r9, -120(%rbp)
	movq	%r8, -112(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%rax, -72(%rbp)
	movaps	%xmm0, -3936(%rbp)
	movq	%rbx, -144(%rbp)
	movq	$0, -3920(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-736(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -4240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L645
	call	_ZdlPv@PLT
.L645:
	movq	-4400(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L780:
	movq	-4424(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-160(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-148(%rbp), %rdx
	movabsq	$578721382704613383, %rax
	movaps	%xmm0, -3936(%rbp)
	movq	%rax, -160(%rbp)
	movl	$134743815, -152(%rbp)
	movq	$0, -3920(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4128(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-3936(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L641
	call	_ZdlPv@PLT
.L641:
	movq	(%rbx), %rax
	leaq	-72(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-928(%rbp), %r14
	movdqu	64(%rax), %xmm0
	movdqu	(%rax), %xmm4
	movdqu	16(%rax), %xmm3
	movdqu	32(%rax), %xmm2
	movdqu	48(%rax), %xmm1
	movq	80(%rax), %rax
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -160(%rbp)
	movq	%rax, -80(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm0, -3936(%rbp)
	movq	$0, -3920(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L642
	call	_ZdlPv@PLT
.L642:
	movq	-4368(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L785:
	movq	-4344(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -4080(%rbp)
	movq	$0, -4072(%rbp)
	movq	$0, -4064(%rbp)
	movq	$0, -4056(%rbp)
	movq	$0, -4048(%rbp)
	movq	$0, -4040(%rbp)
	movq	$0, -4032(%rbp)
	movq	$0, -4024(%rbp)
	movq	$0, -4016(%rbp)
	movq	$0, -4008(%rbp)
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r13, %rdi
	leaq	-3992(%rbp), %rax
	pushq	%rax
	leaq	-4000(%rbp), %rax
	leaq	-4056(%rbp), %r8
	pushq	%rax
	leaq	-4008(%rbp), %rax
	leaq	-4064(%rbp), %rcx
	pushq	%rax
	leaq	-4016(%rbp), %rax
	leaq	-4048(%rbp), %r9
	pushq	%rax
	leaq	-4024(%rbp), %rax
	leaq	-4072(%rbp), %rdx
	pushq	%rax
	leaq	-4032(%rbp), %rax
	leaq	-4080(%rbp), %rsi
	pushq	%rax
	leaq	-4040(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_S4_NS0_10JSReceiverES5_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_SF_SF_SF_SF_SF_PNSB_IS5_EESH_PNSB_IS9_EESJ_
	addq	$64, %rsp
	movl	$63, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$62, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-3984(%rbp), %r11
	movq	-4104(%rbp), %rsi
	movq	%r11, %rdi
	movq	%r11, -4368(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4008(%rbp), %rcx
	movq	-4368(%rbp), %r11
	movq	-4080(%rbp), %r9
	movq	-4056(%rbp), %rbx
	movq	%rcx, -4320(%rbp)
	movq	-4016(%rbp), %rax
	movq	%r11, %rdi
	movq	-3992(%rbp), %rcx
	movq	%r9, -4384(%rbp)
	movq	%rbx, -4336(%rbp)
	movq	-4000(%rbp), %rbx
	movq	%rcx, -4344(%rbp)
	movq	%rax, -4304(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$743, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-4368(%rbp), %r11
	movq	-3936(%rbp), %rsi
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rcx
	xorl	%esi, %esi
	movq	-4304(%rbp), %xmm0
	movq	%rcx, -3968(%rbp)
	leaq	-160(%rbp), %rcx
	movq	-4368(%rbp), %r11
	movq	%rax, %r8
	movhps	-4320(%rbp), %xmm0
	movq	-4384(%rbp), %r9
	movq	-3920(%rbp), %rax
	leaq	-3968(%rbp), %rdx
	movaps	%xmm0, -160(%rbp)
	movq	%r11, %rdi
	movq	-4336(%rbp), %xmm0
	movq	%rax, -3960(%rbp)
	movhps	-4304(%rbp), %xmm0
	movq	%r11, -4304(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	movl	$6, %ebx
	pushq	%rbx
	movhps	-4344(%rbp), %xmm0
	pushq	%rcx
	movl	$1, %ecx
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	-4304(%rbp), %r11
	movq	%rax, %rbx
	movq	%r11, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-4104(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	popq	%rax
	popq	%rdx
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L784:
	movq	-4352(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -4064(%rbp)
	movq	$0, -4056(%rbp)
	movq	$0, -4048(%rbp)
	movq	$0, -4040(%rbp)
	movq	$0, -4032(%rbp)
	movq	$0, -4024(%rbp)
	movq	$0, -4016(%rbp)
	movq	$0, -4008(%rbp)
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3968(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3968(%rbp), %rax
	movq	-4256(%rbp), %rdi
	pushq	%rax
	leaq	-3984(%rbp), %rax
	leaq	-4048(%rbp), %rcx
	pushq	%rax
	leaq	-3992(%rbp), %rax
	leaq	-4032(%rbp), %r9
	pushq	%rax
	leaq	-4000(%rbp), %rax
	leaq	-4040(%rbp), %r8
	pushq	%rax
	leaq	-4008(%rbp), %rax
	leaq	-4056(%rbp), %rdx
	pushq	%rax
	leaq	-4016(%rbp), %rax
	leaq	-4064(%rbp), %rsi
	pushq	%rax
	leaq	-4024(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_S4_NS0_10JSReceiverES5_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_SF_SF_SF_SF_SF_PNSB_IS5_EESH_PNSB_IS9_EESJ_
	addq	$64, %rsp
	movq	%r12, %rdi
	movl	$59, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4104(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-4016(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6ReturnENS1_11SloppyTNodeINS0_6ObjectEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L783:
	movq	-4400(%rbp), %rsi
	movq	%r12, %rdi
	movq	$0, -4064(%rbp)
	leaq	-160(%rbp), %r13
	movq	$0, -4056(%rbp)
	leaq	-64(%rbp), %rbx
	movq	$0, -4048(%rbp)
	movq	$0, -4040(%rbp)
	movq	$0, -4032(%rbp)
	movq	$0, -4024(%rbp)
	movq	$0, -4016(%rbp)
	movq	$0, -4008(%rbp)
	movq	$0, -4000(%rbp)
	movq	$0, -3992(%rbp)
	movq	$0, -3984(%rbp)
	movq	$0, -3968(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-3968(%rbp), %rax
	movq	-4240(%rbp), %rdi
	pushq	%rax
	leaq	-3984(%rbp), %rax
	leaq	-4048(%rbp), %rcx
	pushq	%rax
	leaq	-3992(%rbp), %rax
	leaq	-4032(%rbp), %r9
	pushq	%rax
	leaq	-4000(%rbp), %rax
	leaq	-4040(%rbp), %r8
	pushq	%rax
	leaq	-4008(%rbp), %rax
	leaq	-4056(%rbp), %rdx
	pushq	%rax
	leaq	-4016(%rbp), %rax
	leaq	-4064(%rbp), %rsi
	pushq	%rax
	leaq	-4024(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_6ObjectES4_S4_S4_S4_S4_S4_NS0_10JSReceiverES5_NS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSB_IS4_EESF_SF_SF_SF_SF_SF_PNSB_IS5_EESH_PNSB_IS9_EESJ_
	addq	$64, %rsp
	movl	$58, %edx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-4008(%rbp), %rsi
	movq	-4104(%rbp), %rdi
	call	_ZN2v88internal13ToBoolean_240EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEE@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	-3984(%rbp), %xmm7
	movq	%r15, %rdi
	movq	-4000(%rbp), %xmm3
	movq	%rax, -4304(%rbp)
	movq	-4016(%rbp), %xmm4
	movaps	%xmm0, -3936(%rbp)
	movq	-4032(%rbp), %xmm2
	movq	-4048(%rbp), %xmm5
	movhps	-3968(%rbp), %xmm7
	movq	-4064(%rbp), %xmm6
	movhps	-3992(%rbp), %xmm3
	movhps	-4008(%rbp), %xmm4
	movhps	-4024(%rbp), %xmm2
	movaps	%xmm3, -96(%rbp)
	movhps	-4040(%rbp), %xmm5
	movhps	-4056(%rbp), %xmm6
	movaps	%xmm7, -4320(%rbp)
	movaps	%xmm3, -4336(%rbp)
	movaps	%xmm4, -4368(%rbp)
	movaps	%xmm2, -4384(%rbp)
	movaps	%xmm5, -4400(%rbp)
	movaps	%xmm6, -4416(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm7, -80(%rbp)
	movq	$0, -3920(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4256(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L649
	call	_ZdlPv@PLT
.L649:
	movdqa	-4416(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movdqa	-4400(%rbp), %xmm3
	movdqa	-4384(%rbp), %xmm4
	movq	%rbx, %rdx
	movq	%r15, %rdi
	leaq	-352(%rbp), %r13
	movdqa	-4368(%rbp), %xmm7
	movaps	%xmm2, -160(%rbp)
	movdqa	-4336(%rbp), %xmm2
	movaps	%xmm3, -144(%rbp)
	movdqa	-4320(%rbp), %xmm3
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm0, -3936(%rbp)
	movq	$0, -3920(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L650
	call	_ZdlPv@PLT
.L650:
	movq	-4344(%rbp), %rcx
	movq	-4352(%rbp), %rdx
	movq	%r12, %rdi
	movq	-4304(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L787:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-88(%rbp), %rdx
	movq	%r13, %rsi
	movdqa	-4208(%rbp), %xmm4
	movq	%r14, %rdi
	movq	%rbx, -96(%rbp)
	movdqa	-4128(%rbp), %xmm7
	movaps	%xmm0, -3968(%rbp)
	movaps	%xmm4, -160(%rbp)
	movdqa	-4192(%rbp), %xmm4
	movaps	%xmm7, -144(%rbp)
	movdqa	-4224(%rbp), %xmm7
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movq	$0, -3952(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4264(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L600
	call	_ZdlPv@PLT
.L600:
	leaq	-3560(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L788:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-4128(%rbp), %xmm4
	movq	%r13, %rsi
	movdqa	-4192(%rbp), %xmm7
	movdqa	-4224(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rdx
	movq	%r14, %rdi
	movdqa	-4240(%rbp), %xmm3
	movaps	%xmm4, -160(%rbp)
	movdqa	-4304(%rbp), %xmm4
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm0, -3968(%rbp)
	movq	$0, -3952(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4272(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L613
	call	_ZdlPv@PLT
.L613:
	leaq	-2792(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L789:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-4128(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-4224(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	leaq	-72(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rbx, -80(%rbp)
	movdqa	-4240(%rbp), %xmm2
	movdqa	-4256(%rbp), %xmm3
	movaps	%xmm7, -160(%rbp)
	movdqa	-4320(%rbp), %xmm4
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm0, -3968(%rbp)
	movq	$0, -3952(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4280(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L626
	call	_ZdlPv@PLT
.L626:
	leaq	-2024(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L790:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-4256(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-4240(%rbp), %xmm6
	movdqa	-4304(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	leaq	-64(%rbp), %rdx
	movq	%r14, %rdi
	movdqa	-4320(%rbp), %xmm3
	movdqa	-4416(%rbp), %xmm4
	movaps	%xmm7, -160(%rbp)
	movdqa	-4448(%rbp), %xmm7
	movaps	%xmm6, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	movaps	%xmm0, -3968(%rbp)
	movq	$0, -3952(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-4128(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-3968(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L639
	call	_ZdlPv@PLT
.L639:
	movq	-4424(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L638
.L786:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22478:
	.size	_ZN2v88internal61ArrayFindIndexLoopAfterCallbackLazyDeoptContinuationAssembler64GenerateArrayFindIndexLoopAfterCallbackLazyDeoptContinuationImplEv, .-_ZN2v88internal61ArrayFindIndexLoopAfterCallbackLazyDeoptContinuationAssembler64GenerateArrayFindIndexLoopAfterCallbackLazyDeoptContinuationImplEv
	.section	.rodata._ZN2v88internal8Builtins61Generate_ArrayFindIndexLoopAfterCallbackLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"ArrayFindIndexLoopAfterCallbackLazyDeoptContinuation"
	.section	.text._ZN2v88internal8Builtins61Generate_ArrayFindIndexLoopAfterCallbackLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins61Generate_ArrayFindIndexLoopAfterCallbackLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins61Generate_ArrayFindIndexLoopAfterCallbackLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins61Generate_ArrayFindIndexLoopAfterCallbackLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE:
.LFB22474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$481, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC8(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$742, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L795
.L792:
	movq	%r13, %rdi
	call	_ZN2v88internal61ArrayFindIndexLoopAfterCallbackLazyDeoptContinuationAssembler64GenerateArrayFindIndexLoopAfterCallbackLazyDeoptContinuationImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L796
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L795:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L792
.L796:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22474:
	.size	_ZN2v88internal8Builtins61Generate_ArrayFindIndexLoopAfterCallbackLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins61Generate_ArrayFindIndexLoopAfterCallbackLazyDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_:
.LFB27235:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$14, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$136, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movabsq	$506098639673231111, %rcx
	movq	%rcx, (%rax)
	movl	$1028, %ecx
	leaq	14(%rax), %rdx
	movl	$67569415, 8(%rax)
	movw	%cx, 12(%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L798
	movq	%rax, -168(%rbp)
	call	_ZdlPv@PLT
	movq	-168(%rbp), %rax
.L798:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L799
	movq	%rdx, (%r15)
.L799:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L800
	movq	%rdx, (%r14)
.L800:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L801
	movq	%rdx, 0(%r13)
.L801:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L802
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L802:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L803
	movq	%rdx, (%rbx)
.L803:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L804
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L804:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L805
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L805:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L806
	movq	-112(%rbp), %rbx
	movq	%rdx, (%rbx)
.L806:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L807
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L807:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L808
	movq	-128(%rbp), %rsi
	movq	%rdx, (%rsi)
.L808:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L809
	movq	-136(%rbp), %rcx
	movq	%rdx, (%rcx)
.L809:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L810
	movq	-144(%rbp), %rbx
	movq	%rdx, (%rbx)
.L810:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L811
	movq	-152(%rbp), %rsi
	movq	%rdx, (%rsi)
.L811:
	movq	104(%rax), %rax
	testq	%rax, %rax
	je	.L797
	movq	-160(%rbp), %rcx
	movq	%rax, (%rcx)
.L797:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L860
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L860:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27235:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_S6_S3_S6_SA_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SO_SG_SO_SQ_SO_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_S6_S3_S6_SA_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SO_SG_SO_SQ_SO_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_S6_S3_S6_SA_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SO_SG_SO_SQ_SO_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_S6_S3_S6_SA_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SO_SG_SO_SQ_SO_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_S6_S3_S6_SA_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SO_SG_SO_SQ_SO_:
.LFB27244:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$22, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	88(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	96(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	104(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	112(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	120(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	128(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	136(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	144(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$1543, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movdqa	.LC9(%rip), %xmm0
	movl	$101123590, 16(%rax)
	leaq	22(%rax), %rdx
	movw	%cx, 20(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L862
	movq	%rax, -232(%rbp)
	call	_ZdlPv@PLT
	movq	-232(%rbp), %rax
.L862:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L863
	movq	%rdx, (%r15)
.L863:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L864
	movq	%rdx, (%r14)
.L864:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L865
	movq	%rdx, 0(%r13)
.L865:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L866
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L866:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L867
	movq	%rdx, (%rbx)
.L867:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L868
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L868:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L869
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L869:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L870
	movq	-112(%rbp), %rcx
	movq	%rdx, (%rcx)
.L870:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L871
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L871:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L872
	movq	-128(%rbp), %rcx
	movq	%rdx, (%rcx)
.L872:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L873
	movq	-136(%rbp), %rbx
	movq	%rdx, (%rbx)
.L873:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L874
	movq	-144(%rbp), %rsi
	movq	%rdx, (%rsi)
.L874:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L875
	movq	-152(%rbp), %rcx
	movq	%rdx, (%rcx)
.L875:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L876
	movq	-160(%rbp), %rbx
	movq	%rdx, (%rbx)
.L876:
	movq	112(%rax), %rdx
	testq	%rdx, %rdx
	je	.L877
	movq	-168(%rbp), %rsi
	movq	%rdx, (%rsi)
.L877:
	movq	120(%rax), %rdx
	testq	%rdx, %rdx
	je	.L878
	movq	-176(%rbp), %rdi
	movq	%rdx, (%rdi)
.L878:
	movq	128(%rax), %rdx
	testq	%rdx, %rdx
	je	.L879
	movq	-184(%rbp), %rcx
	movq	%rdx, (%rcx)
.L879:
	movq	136(%rax), %rdx
	testq	%rdx, %rdx
	je	.L880
	movq	-192(%rbp), %rbx
	movq	%rdx, (%rbx)
.L880:
	movq	144(%rax), %rdx
	testq	%rdx, %rdx
	je	.L881
	movq	-200(%rbp), %rsi
	movq	%rdx, (%rsi)
.L881:
	movq	152(%rax), %rdx
	testq	%rdx, %rdx
	je	.L882
	movq	-208(%rbp), %rdi
	movq	%rdx, (%rdi)
.L882:
	movq	160(%rax), %rdx
	testq	%rdx, %rdx
	je	.L883
	movq	-216(%rbp), %rcx
	movq	%rdx, (%rcx)
.L883:
	movq	168(%rax), %rax
	testq	%rax, %rax
	je	.L861
	movq	-224(%rbp), %rbx
	movq	%rax, (%rbx)
.L861:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L956
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L956:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27244:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_S6_S3_S6_SA_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SO_SG_SO_SQ_SO_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_S6_S3_S6_SA_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SO_SG_SO_SQ_SO_
	.section	.text._ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SO_SG_SO_SQ_SO_SM_,"axG",@progbits,_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SO_SG_SO_SQ_SO_SM_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SO_SG_SO_SQ_SO_SM_
	.type	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SO_SG_SO_SQ_SO_SM_, @function
_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SO_SG_SO_SQ_SO_SM_:
.LFB27246:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$23, %edi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	16(%rbp), %rax
	movq	%r8, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	56(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	64(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	72(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	80(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	88(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	96(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	104(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	112(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	120(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	128(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	136(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	144(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	152(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movl	$1543, %ecx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rsi
	movdqa	.LC9(%rip), %xmm0
	movl	$101123590, 16(%rax)
	leaq	23(%rax), %rdx
	movw	%cx, 20(%rax)
	movb	$8, 22(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L958
	movq	%rax, -240(%rbp)
	call	_ZdlPv@PLT
	movq	-240(%rbp), %rax
.L958:
	movq	(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L959
	movq	%rdx, (%r15)
.L959:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L960
	movq	%rdx, (%r14)
.L960:
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L961
	movq	%rdx, 0(%r13)
.L961:
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L962
	movq	-88(%rbp), %rcx
	movq	%rdx, (%rcx)
.L962:
	movq	32(%rax), %rdx
	testq	%rdx, %rdx
	je	.L963
	movq	%rdx, (%rbx)
.L963:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L964
	movq	-96(%rbp), %rbx
	movq	%rdx, (%rbx)
.L964:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L965
	movq	-104(%rbp), %rbx
	movq	%rdx, (%rbx)
.L965:
	movq	56(%rax), %rdx
	testq	%rdx, %rdx
	je	.L966
	movq	-112(%rbp), %rcx
	movq	%rdx, (%rcx)
.L966:
	movq	64(%rax), %rdx
	testq	%rdx, %rdx
	je	.L967
	movq	-120(%rbp), %rbx
	movq	%rdx, (%rbx)
.L967:
	movq	72(%rax), %rdx
	testq	%rdx, %rdx
	je	.L968
	movq	-128(%rbp), %rcx
	movq	%rdx, (%rcx)
.L968:
	movq	80(%rax), %rdx
	testq	%rdx, %rdx
	je	.L969
	movq	-136(%rbp), %rbx
	movq	%rdx, (%rbx)
.L969:
	movq	88(%rax), %rdx
	testq	%rdx, %rdx
	je	.L970
	movq	-144(%rbp), %rsi
	movq	%rdx, (%rsi)
.L970:
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L971
	movq	-152(%rbp), %rcx
	movq	%rdx, (%rcx)
.L971:
	movq	104(%rax), %rdx
	testq	%rdx, %rdx
	je	.L972
	movq	-160(%rbp), %rbx
	movq	%rdx, (%rbx)
.L972:
	movq	112(%rax), %rdx
	testq	%rdx, %rdx
	je	.L973
	movq	-168(%rbp), %rsi
	movq	%rdx, (%rsi)
.L973:
	movq	120(%rax), %rdx
	testq	%rdx, %rdx
	je	.L974
	movq	-176(%rbp), %rdi
	movq	%rdx, (%rdi)
.L974:
	movq	128(%rax), %rdx
	testq	%rdx, %rdx
	je	.L975
	movq	-184(%rbp), %rcx
	movq	%rdx, (%rcx)
.L975:
	movq	136(%rax), %rdx
	testq	%rdx, %rdx
	je	.L976
	movq	-192(%rbp), %rbx
	movq	%rdx, (%rbx)
.L976:
	movq	144(%rax), %rdx
	testq	%rdx, %rdx
	je	.L977
	movq	-200(%rbp), %rsi
	movq	%rdx, (%rsi)
.L977:
	movq	152(%rax), %rdx
	testq	%rdx, %rdx
	je	.L978
	movq	-208(%rbp), %rdi
	movq	%rdx, (%rdi)
.L978:
	movq	160(%rax), %rdx
	testq	%rdx, %rdx
	je	.L979
	movq	-216(%rbp), %rcx
	movq	%rdx, (%rcx)
.L979:
	movq	168(%rax), %rdx
	testq	%rdx, %rdx
	je	.L980
	movq	-224(%rbp), %rbx
	movq	%rdx, (%rbx)
.L980:
	movq	176(%rax), %rax
	testq	%rax, %rax
	je	.L957
	movq	-232(%rbp), %rsi
	movq	%rax, (%rsi)
.L957:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1056
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1056:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27246:
	.size	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SO_SG_SO_SQ_SO_SM_, .-_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SO_SG_SO_SQ_SO_SM_
	.section	.text._ZN2v88internal20FastArrayFindIndex_6EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_NS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableISA_EE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20FastArrayFindIndex_6EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_NS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableISA_EE
	.type	_ZN2v88internal20FastArrayFindIndex_6EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_NS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableISA_EE, @function
_ZN2v88internal20FastArrayFindIndex_6EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_NS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableISA_EE:
.LFB22536:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$440, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%rax, -8504(%rbp)
	movq	24(%rbp), %rax
	movq	%r9, %r14
	movq	%rdx, %rbx
	movq	%rdi, -7896(%rbp)
	leaq	-7888(%rbp), %r13
	leaq	-7480(%rbp), %r12
	movq	%rsi, -8000(%rbp)
	movq	%r13, %r15
	movq	%r8, -7952(%rbp)
	movq	%rcx, -7984(%rbp)
	movq	%rax, -8512(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	%rdi, -7888(%rbp)
	movq	%rdi, -7536(%rbp)
	movl	$120, %edi
	movq	$0, -7528(%rbp)
	movq	$0, -7520(%rbp)
	movq	$0, -7512(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -7512(%rbp)
	movq	%rdx, -7520(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -7496(%rbp)
	movq	%rax, -7528(%rbp)
	movq	$0, -7504(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$168, %edi
	movq	$0, -7336(%rbp)
	movq	$0, -7328(%rbp)
	movq	%rax, -7344(%rbp)
	movq	$0, -7320(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -7336(%rbp)
	leaq	-7288(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7320(%rbp)
	movq	%rdx, -7328(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -7304(%rbp)
	movq	%rax, -8224(%rbp)
	movq	$0, -7312(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$192, %edi
	movq	$0, -7144(%rbp)
	movq	$0, -7136(%rbp)
	movq	%rax, -7152(%rbp)
	movq	$0, -7128(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -7144(%rbp)
	leaq	-7096(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7128(%rbp)
	movq	%rdx, -7136(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -7112(%rbp)
	movq	%rax, -8032(%rbp)
	movq	$0, -7120(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$144, %edi
	movq	$0, -6952(%rbp)
	movq	$0, -6944(%rbp)
	movq	%rax, -6960(%rbp)
	movq	$0, -6936(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -6952(%rbp)
	leaq	-6904(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6936(%rbp)
	movq	%rdx, -6944(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6920(%rbp)
	movq	%rax, -8064(%rbp)
	movq	$0, -6928(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$168, %edi
	movq	$0, -6760(%rbp)
	movq	$0, -6752(%rbp)
	movq	%rax, -6768(%rbp)
	movq	$0, -6744(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -6760(%rbp)
	leaq	-6712(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6744(%rbp)
	movq	%rdx, -6752(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6728(%rbp)
	movq	%rax, -8080(%rbp)
	movq	$0, -6736(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$192, %edi
	movq	$0, -6568(%rbp)
	movq	$0, -6560(%rbp)
	movq	%rax, -6576(%rbp)
	movq	$0, -6552(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -6568(%rbp)
	leaq	-6520(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6552(%rbp)
	movq	%rdx, -6560(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6536(%rbp)
	movq	%rax, -8112(%rbp)
	movq	$0, -6544(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$216, %edi
	movq	$0, -6376(%rbp)
	movq	$0, -6368(%rbp)
	movq	%rax, -6384(%rbp)
	movq	$0, -6360(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -6376(%rbp)
	leaq	-6328(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6360(%rbp)
	movq	%rdx, -6368(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6344(%rbp)
	movq	%rax, -8144(%rbp)
	movq	$0, -6352(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$168, %edi
	movq	$0, -6184(%rbp)
	movq	$0, -6176(%rbp)
	movq	%rax, -6192(%rbp)
	movq	$0, -6168(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -6184(%rbp)
	leaq	-6136(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -6168(%rbp)
	movq	%rdx, -6176(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -6152(%rbp)
	movq	%rax, -8160(%rbp)
	movq	$0, -6160(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$192, %edi
	movq	$0, -5992(%rbp)
	movq	$0, -5984(%rbp)
	movq	%rax, -6000(%rbp)
	movq	$0, -5976(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -5992(%rbp)
	leaq	-5944(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5976(%rbp)
	movq	%rdx, -5984(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5960(%rbp)
	movq	%rax, -8176(%rbp)
	movq	$0, -5968(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$336, %edi
	movq	$0, -5800(%rbp)
	movq	$0, -5792(%rbp)
	movq	%rax, -5808(%rbp)
	movq	$0, -5784(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -5800(%rbp)
	leaq	-5752(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5784(%rbp)
	movq	%rdx, -5792(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5768(%rbp)
	movq	%rax, -8368(%rbp)
	movq	$0, -5776(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$336, %edi
	movq	$0, -5608(%rbp)
	movq	$0, -5600(%rbp)
	movq	%rax, -5616(%rbp)
	movq	$0, -5592(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -5608(%rbp)
	leaq	-5560(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5592(%rbp)
	movq	%rdx, -5600(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5576(%rbp)
	movq	%rax, -8016(%rbp)
	movq	$0, -5584(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$336, %edi
	movq	$0, -5416(%rbp)
	movq	$0, -5408(%rbp)
	movq	%rax, -5424(%rbp)
	movq	$0, -5400(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -5416(%rbp)
	leaq	-5368(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5400(%rbp)
	movq	%rdx, -5408(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5384(%rbp)
	movq	%rax, -8192(%rbp)
	movq	$0, -5392(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$336, %edi
	movq	$0, -5224(%rbp)
	movq	$0, -5216(%rbp)
	movq	%rax, -5232(%rbp)
	movq	$0, -5208(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -5224(%rbp)
	leaq	-5176(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5208(%rbp)
	movq	%rdx, -5216(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5192(%rbp)
	movq	%rax, -8208(%rbp)
	movq	$0, -5200(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$336, %edi
	movq	$0, -5032(%rbp)
	movq	$0, -5024(%rbp)
	movq	%rax, -5040(%rbp)
	movq	$0, -5016(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -5032(%rbp)
	leaq	-4984(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -5016(%rbp)
	movq	%rdx, -5024(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -5000(%rbp)
	movq	%rax, -8216(%rbp)
	movq	$0, -5008(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$336, %edi
	movq	$0, -4840(%rbp)
	movq	$0, -4832(%rbp)
	movq	%rax, -4848(%rbp)
	movq	$0, -4824(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -4840(%rbp)
	leaq	-4792(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4824(%rbp)
	movq	%rdx, -4832(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4808(%rbp)
	movq	%rax, -8240(%rbp)
	movq	$0, -4816(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$336, %edi
	movq	$0, -4648(%rbp)
	movq	$0, -4640(%rbp)
	movq	%rax, -4656(%rbp)
	movq	$0, -4632(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -4648(%rbp)
	leaq	-4600(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4632(%rbp)
	movq	%rdx, -4640(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4616(%rbp)
	movq	%rax, -8096(%rbp)
	movq	$0, -4624(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$336, %edi
	movq	$0, -4456(%rbp)
	movq	$0, -4448(%rbp)
	movq	%rax, -4464(%rbp)
	movq	$0, -4440(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -4456(%rbp)
	leaq	-4408(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4440(%rbp)
	movq	%rdx, -4448(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4424(%rbp)
	movq	%rax, -7928(%rbp)
	movq	$0, -4432(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$336, %edi
	movq	$0, -4264(%rbp)
	movq	$0, -4256(%rbp)
	movq	%rax, -4272(%rbp)
	movq	$0, -4248(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -4264(%rbp)
	leaq	-4216(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4248(%rbp)
	movq	%rdx, -4256(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4232(%rbp)
	movq	%rax, -8256(%rbp)
	movq	$0, -4240(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$384, %edi
	movq	$0, -4072(%rbp)
	movq	$0, -4064(%rbp)
	movq	%rax, -4080(%rbp)
	movq	$0, -4056(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	384(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movq	%rax, -4072(%rbp)
	leaq	-4024(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -4056(%rbp)
	movq	%rdx, -4064(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -4040(%rbp)
	movq	%rax, -8432(%rbp)
	movq	$0, -4048(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$336, %edi
	movq	$0, -3880(%rbp)
	movq	$0, -3872(%rbp)
	movq	%rax, -3888(%rbp)
	movq	$0, -3864(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -3880(%rbp)
	leaq	-3832(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3864(%rbp)
	movq	%rdx, -3872(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3848(%rbp)
	movq	%rax, -8464(%rbp)
	movq	$0, -3856(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$336, %edi
	movq	$0, -3688(%rbp)
	movq	$0, -3680(%rbp)
	movq	%rax, -3696(%rbp)
	movq	$0, -3672(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -3688(%rbp)
	leaq	-3640(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3672(%rbp)
	movq	%rdx, -3680(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3656(%rbp)
	movq	%rax, -8480(%rbp)
	movq	$0, -3664(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$480, %edi
	movq	$0, -3496(%rbp)
	movq	$0, -3488(%rbp)
	movq	%rax, -3504(%rbp)
	movq	$0, -3480(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movups	%xmm0, (%rax)
	leaq	480(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -3496(%rbp)
	leaq	-3448(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3480(%rbp)
	movq	%rdx, -3488(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -3464(%rbp)
	movq	%rax, -8336(%rbp)
	movq	$0, -3472(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$528, %edi
	movq	$0, -3304(%rbp)
	movq	$0, -3296(%rbp)
	movq	%rax, -3312(%rbp)
	movq	$0, -3288(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	528(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -3304(%rbp)
	movq	%rdx, -3288(%rbp)
	movups	%xmm0, 480(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	leaq	-3256(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3296(%rbp)
	xorl	%edx, %edx
	movq	%rax, -8496(%rbp)
	movq	$0, -3280(%rbp)
	movups	%xmm0, -3272(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$552, %edi
	movq	$0, -3112(%rbp)
	movq	$0, -3104(%rbp)
	movq	%rax, -3120(%rbp)
	movq	$0, -3096(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	552(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -3112(%rbp)
	movq	%rdx, -3096(%rbp)
	movups	%xmm0, 480(%rax)
	movq	$0, 544(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	leaq	-3064(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -3104(%rbp)
	xorl	%edx, %edx
	movq	%rax, -8352(%rbp)
	movq	$0, -3088(%rbp)
	movups	%xmm0, -3080(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$480, %edi
	movq	$0, -2920(%rbp)
	movq	$0, -2912(%rbp)
	movq	%rax, -2928(%rbp)
	movq	$0, -2904(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movups	%xmm0, (%rax)
	leaq	480(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -2920(%rbp)
	leaq	-2872(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2904(%rbp)
	movq	%rdx, -2912(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2888(%rbp)
	movq	%rax, -8400(%rbp)
	movq	$0, -2896(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$528, %edi
	movq	$0, -2728(%rbp)
	movq	$0, -2720(%rbp)
	movq	%rax, -2736(%rbp)
	movq	$0, -2712(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	528(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -2728(%rbp)
	movq	%rdx, -2712(%rbp)
	movups	%xmm0, 480(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	leaq	-2680(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2720(%rbp)
	xorl	%edx, %edx
	movq	%rax, -8384(%rbp)
	movq	$0, -2704(%rbp)
	movups	%xmm0, -2696(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$552, %edi
	movq	$0, -2536(%rbp)
	movq	$0, -2528(%rbp)
	movq	%rax, -2544(%rbp)
	movq	$0, -2520(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	552(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -2536(%rbp)
	movq	%rdx, -2520(%rbp)
	movups	%xmm0, 480(%rax)
	movq	$0, 544(%rax)
	movups	%xmm0, 496(%rax)
	movups	%xmm0, 512(%rax)
	movups	%xmm0, 528(%rax)
	leaq	-2488(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2528(%rbp)
	xorl	%edx, %edx
	movq	%rax, -8416(%rbp)
	movq	$0, -2512(%rbp)
	movups	%xmm0, -2504(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$504, %edi
	movq	$0, -2344(%rbp)
	movq	$0, -2336(%rbp)
	movq	%rax, -2352(%rbp)
	movq	$0, -2328(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	504(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -2344(%rbp)
	movq	%rdx, -2328(%rbp)
	movups	%xmm0, 480(%rax)
	movq	$0, 496(%rax)
	leaq	-2296(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2336(%rbp)
	xorl	%edx, %edx
	movq	%rax, -7920(%rbp)
	movq	$0, -2320(%rbp)
	movups	%xmm0, -2312(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$408, %edi
	movq	$0, -2152(%rbp)
	movq	$0, -2144(%rbp)
	movq	%rax, -2160(%rbp)
	movq	$0, -2136(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	408(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 400(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movq	%rax, -2152(%rbp)
	leaq	-2104(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2136(%rbp)
	movq	%rdx, -2144(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2120(%rbp)
	movq	%rax, -8320(%rbp)
	movq	$0, -2128(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$432, %edi
	movq	$0, -1960(%rbp)
	movq	$0, -1952(%rbp)
	movq	%rax, -1968(%rbp)
	movq	$0, -1944(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movups	%xmm0, (%rax)
	leaq	432(%rax), %rdx
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movq	%rax, -1960(%rbp)
	leaq	-1912(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1944(%rbp)
	movq	%rdx, -1952(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1928(%rbp)
	movq	$0, -1936(%rbp)
	movq	%rax, -7936(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$504, %edi
	movq	$0, -1768(%rbp)
	movq	$0, -1760(%rbp)
	movq	%rax, -1776(%rbp)
	movq	$0, -1752(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	504(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movups	%xmm0, 384(%rax)
	movups	%xmm0, 400(%rax)
	movups	%xmm0, 416(%rax)
	movups	%xmm0, 432(%rax)
	movups	%xmm0, 448(%rax)
	movups	%xmm0, 464(%rax)
	movq	%rax, -1768(%rbp)
	movq	%rdx, -1752(%rbp)
	movups	%xmm0, 480(%rax)
	movq	$0, 496(%rax)
	leaq	-1720(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1760(%rbp)
	xorl	%edx, %edx
	movq	%rax, -8448(%rbp)
	movq	$0, -1744(%rbp)
	movups	%xmm0, -1736(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$384, %edi
	movq	$0, -1576(%rbp)
	movq	$0, -1568(%rbp)
	movq	%rax, -1584(%rbp)
	movq	$0, -1560(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	384(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movq	%rax, -1576(%rbp)
	leaq	-1528(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1560(%rbp)
	movq	%rdx, -1568(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1544(%rbp)
	movq	%rax, -8120(%rbp)
	movq	$0, -1552(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$384, %edi
	movq	$0, -1384(%rbp)
	movq	$0, -1376(%rbp)
	movq	%rax, -1392(%rbp)
	movq	$0, -1368(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	384(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movups	%xmm0, 336(%rax)
	movups	%xmm0, 352(%rax)
	movups	%xmm0, 368(%rax)
	movq	%rax, -1384(%rbp)
	leaq	-1336(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1368(%rbp)
	movq	%rdx, -1376(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1352(%rbp)
	movq	%rax, -8376(%rbp)
	movq	$0, -1360(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$336, %edi
	movq	$0, -1192(%rbp)
	movq	$0, -1184(%rbp)
	movq	%rax, -1200(%rbp)
	movq	$0, -1176(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -1192(%rbp)
	leaq	-1144(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1176(%rbp)
	movq	%rdx, -1184(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1160(%rbp)
	movq	%rax, -8128(%rbp)
	movq	$0, -1168(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$336, %edi
	movq	$0, -1000(%rbp)
	movq	$0, -992(%rbp)
	movq	%rax, -1008(%rbp)
	movq	$0, -984(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -1000(%rbp)
	leaq	-952(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -984(%rbp)
	movq	%rdx, -992(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -968(%rbp)
	movq	%rax, -8088(%rbp)
	movq	$0, -976(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$144, %edi
	movq	$0, -808(%rbp)
	movq	$0, -800(%rbp)
	movq	%rax, -816(%rbp)
	movq	$0, -792(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -808(%rbp)
	leaq	-760(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -792(%rbp)
	movq	%rdx, -800(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -776(%rbp)
	movq	%rax, -7960(%rbp)
	movq	$0, -784(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$24, %edi
	movq	$0, -616(%rbp)
	movq	$0, -608(%rbp)
	movq	%rax, -624(%rbp)
	movq	$0, -600(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 16(%rax)
	movq	%rax, -616(%rbp)
	leaq	-568(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -600(%rbp)
	movq	%rdx, -608(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -584(%rbp)
	movq	%rax, -7904(%rbp)
	movq	$0, -592(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7888(%rbp), %rax
	movl	$144, %edi
	movq	$0, -424(%rbp)
	movq	$0, -416(%rbp)
	movq	%rax, -432(%rbp)
	movq	$0, -408(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	leaq	144(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movq	%rax, -424(%rbp)
	leaq	-376(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -408(%rbp)
	movq	%rdx, -416(%rbp)
	xorl	%edx, %edx
	movq	%rax, -7912(%rbp)
	movups	%xmm0, -392(%rbp)
	movq	$0, -400(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-8000(%rbp), %r10
	movq	-7984(%rbp), %r11
	pxor	%xmm0, %xmm0
	movq	-7952(%rbp), %rax
	movl	$40, %edi
	movq	%r14, -208(%rbp)
	leaq	-7664(%rbp), %r14
	movq	%r10, -240(%rbp)
	movq	%r11, -224(%rbp)
	movaps	%xmm0, -7664(%rbp)
	movq	%rbx, -232(%rbp)
	movq	%rax, -216(%rbp)
	movq	$0, -7648(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm7
	movq	-208(%rbp), %rcx
	movq	%r14, %rsi
	leaq	40(%rax), %rdx
	movq	%rax, -7664(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-224(%rbp), %xmm7
	movq	%rcx, 32(%rax)
	movups	%xmm7, 16(%rax)
	leaq	-7536(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	movq	%rax, -8360(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1058
	call	_ZdlPv@PLT
.L1058:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7152(%rbp), %rax
	cmpq	$0, -7472(%rbp)
	movq	%rax, -7968(%rbp)
	jne	.L1543
.L1059:
	leaq	-6960(%rbp), %rax
	cmpq	$0, -7280(%rbp)
	movq	%rax, -8048(%rbp)
	jne	.L1544
.L1064:
	leaq	-6768(%rbp), %rax
	cmpq	$0, -7088(%rbp)
	movq	%rax, -8056(%rbp)
	jne	.L1545
.L1067:
	cmpq	$0, -6896(%rbp)
	jne	.L1546
.L1070:
	leaq	-6384(%rbp), %rax
	cmpq	$0, -6704(%rbp)
	movq	%rax, -8064(%rbp)
	leaq	-6576(%rbp), %rax
	movq	%rax, -8000(%rbp)
	jne	.L1547
.L1073:
	leaq	-6192(%rbp), %rax
	cmpq	$0, -6512(%rbp)
	movq	%rax, -8104(%rbp)
	jne	.L1548
.L1078:
	leaq	-6000(%rbp), %rax
	cmpq	$0, -6320(%rbp)
	movq	%rax, -8112(%rbp)
	jne	.L1549
.L1081:
	cmpq	$0, -6128(%rbp)
	jne	.L1550
.L1084:
	leaq	-5808(%rbp), %rax
	cmpq	$0, -5936(%rbp)
	movq	%rax, -7952(%rbp)
	jne	.L1551
.L1087:
	leaq	-5616(%rbp), %rax
	cmpq	$0, -5744(%rbp)
	movq	%rax, -8080(%rbp)
	jne	.L1552
.L1090:
	leaq	-5424(%rbp), %rax
	cmpq	$0, -5552(%rbp)
	movq	%rax, -8144(%rbp)
	leaq	-5232(%rbp), %rax
	movq	%rax, -8160(%rbp)
	jne	.L1553
	cmpq	$0, -5360(%rbp)
	jne	.L1554
.L1096:
	leaq	-5040(%rbp), %rax
	cmpq	$0, -5168(%rbp)
	movq	%rax, -8176(%rbp)
	leaq	-4848(%rbp), %rax
	movq	%rax, -8192(%rbp)
	jne	.L1555
	cmpq	$0, -4976(%rbp)
	jne	.L1556
.L1101:
	cmpq	$0, -4784(%rbp)
	jne	.L1557
.L1103:
	leaq	-4272(%rbp), %rax
	cmpq	$0, -4592(%rbp)
	movq	%rax, -8208(%rbp)
	jne	.L1558
.L1105:
	cmpq	$0, -4400(%rbp)
	jne	.L1559
.L1107:
	leaq	-4080(%rbp), %rax
	cmpq	$0, -4208(%rbp)
	movq	%rax, -8272(%rbp)
	jne	.L1560
.L1109:
	leaq	-3888(%rbp), %rax
	cmpq	$0, -4016(%rbp)
	movq	%rax, -8288(%rbp)
	leaq	-3696(%rbp), %rax
	movq	%rax, -8304(%rbp)
	jne	.L1561
	cmpq	$0, -3824(%rbp)
	jne	.L1562
.L1115:
	leaq	-3504(%rbp), %rax
	cmpq	$0, -3632(%rbp)
	movq	%rax, -8216(%rbp)
	leaq	-2928(%rbp), %rax
	movq	%rax, -8256(%rbp)
	jne	.L1563
.L1117:
	leaq	-3120(%rbp), %rax
	cmpq	$0, -3440(%rbp)
	movq	%rax, -8240(%rbp)
	leaq	-3312(%rbp), %rax
	movq	%rax, -8016(%rbp)
	jne	.L1564
.L1120:
	leaq	-2160(%rbp), %rax
	cmpq	$0, -3248(%rbp)
	movq	%rax, -7984(%rbp)
	jne	.L1565
	cmpq	$0, -3056(%rbp)
	jne	.L1566
.L1127:
	leaq	-2544(%rbp), %rax
	cmpq	$0, -2864(%rbp)
	movq	%rax, -8336(%rbp)
	leaq	-2736(%rbp), %rax
	movq	%rax, -8032(%rbp)
	jne	.L1567
	cmpq	$0, -2672(%rbp)
	jne	.L1568
.L1134:
	cmpq	$0, -2480(%rbp)
	jne	.L1569
.L1136:
	cmpq	$0, -2288(%rbp)
	jne	.L1570
.L1138:
	cmpq	$0, -2096(%rbp)
	jne	.L1571
.L1141:
	leaq	-1776(%rbp), %rax
	cmpq	$0, -1904(%rbp)
	movq	%rax, -8352(%rbp)
	jne	.L1572
.L1144:
	leaq	-1392(%rbp), %rax
	cmpq	$0, -1712(%rbp)
	movq	%rax, -8320(%rbp)
	jne	.L1573
.L1147:
	cmpq	$0, -1520(%rbp)
	jne	.L1574
	cmpq	$0, -1328(%rbp)
	jne	.L1575
.L1157:
	cmpq	$0, -1136(%rbp)
	jne	.L1576
.L1160:
	cmpq	$0, -944(%rbp)
	jne	.L1577
.L1162:
	cmpq	$0, -752(%rbp)
	leaq	-432(%rbp), %r12
	jne	.L1578
	cmpq	$0, -560(%rbp)
	jne	.L1579
.L1167:
	movq	-7912(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -7648(%rbp)
	movaps	%xmm0, -7664(%rbp)
	call	_Znwm@PLT
	movl	$2056, %ecx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movl	$117966599, (%rax)
	leaq	6(%rax), %rdx
	movw	%cx, 4(%rax)
	movq	%rax, -7664(%rbp)
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7664(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1169
	call	_ZdlPv@PLT
.L1169:
	movq	(%rbx), %rax
	movq	-7912(%rbp), %rdi
	movq	40(%rax), %r12
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1170
	call	_ZdlPv@PLT
.L1170:
	movq	-416(%rbp), %rbx
	movq	-424(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1171
	.p2align 4,,10
	.p2align 3
.L1175:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1172
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1175
.L1173:
	movq	-424(%rbp), %r13
.L1171:
	testq	%r13, %r13
	je	.L1176
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1176:
	movq	-7904(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-592(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1177
	call	_ZdlPv@PLT
.L1177:
	movq	-608(%rbp), %rbx
	movq	-616(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1178
	.p2align 4,,10
	.p2align 3
.L1182:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1179
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1182
.L1180:
	movq	-616(%rbp), %r13
.L1178:
	testq	%r13, %r13
	je	.L1183
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1183:
	movq	-7960(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-784(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1184
	call	_ZdlPv@PLT
.L1184:
	movq	-800(%rbp), %rbx
	movq	-808(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1185
	.p2align 4,,10
	.p2align 3
.L1189:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1186
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1189
.L1187:
	movq	-808(%rbp), %r13
.L1185:
	testq	%r13, %r13
	je	.L1190
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1190:
	movq	-8088(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-976(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1191
	call	_ZdlPv@PLT
.L1191:
	movq	-992(%rbp), %rbx
	movq	-1000(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1192
	.p2align 4,,10
	.p2align 3
.L1196:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1193
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1196
.L1194:
	movq	-1000(%rbp), %r13
.L1192:
	testq	%r13, %r13
	je	.L1197
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1197:
	movq	-8128(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1198
	call	_ZdlPv@PLT
.L1198:
	movq	-1184(%rbp), %rbx
	movq	-1192(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1199
	.p2align 4,,10
	.p2align 3
.L1203:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1200
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1203
.L1201:
	movq	-1192(%rbp), %r13
.L1199:
	testq	%r13, %r13
	je	.L1204
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1204:
	movq	-8320(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8120(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1552(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1205
	call	_ZdlPv@PLT
.L1205:
	movq	-1568(%rbp), %rbx
	movq	-1576(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1206
	.p2align 4,,10
	.p2align 3
.L1210:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1207
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1210
.L1208:
	movq	-1576(%rbp), %r13
.L1206:
	testq	%r13, %r13
	je	.L1211
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1211:
	movq	-8352(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7936(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1936(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1212
	call	_ZdlPv@PLT
.L1212:
	movq	-1952(%rbp), %rbx
	movq	-1960(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1213
	.p2align 4,,10
	.p2align 3
.L1217:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1214
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1217
.L1215:
	movq	-1960(%rbp), %r13
.L1213:
	testq	%r13, %r13
	je	.L1218
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1218:
	movq	-7984(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7920(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2320(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1219
	call	_ZdlPv@PLT
.L1219:
	movq	-2336(%rbp), %rbx
	movq	-2344(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1220
	.p2align 4,,10
	.p2align 3
.L1224:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1221
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1224
.L1222:
	movq	-2344(%rbp), %r13
.L1220:
	testq	%r13, %r13
	je	.L1225
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1225:
	movq	-8336(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8032(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8256(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8240(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8016(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8216(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8304(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8288(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8272(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8208(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7928(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-4432(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1226
	call	_ZdlPv@PLT
.L1226:
	movq	-4448(%rbp), %rbx
	movq	-4456(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1227
	.p2align 4,,10
	.p2align 3
.L1231:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1228
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L1231
.L1229:
	movq	-4456(%rbp), %r13
.L1227:
	testq	%r13, %r13
	je	.L1232
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1232:
	movq	-8096(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-4624(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1233
	call	_ZdlPv@PLT
.L1233:
	movq	-4640(%rbp), %rbx
	movq	-4648(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1234
	.p2align 4,,10
	.p2align 3
.L1238:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1235
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1238
.L1236:
	movq	-4648(%rbp), %r13
.L1234:
	testq	%r13, %r13
	je	.L1239
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1239:
	movq	-8192(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8176(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8160(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8144(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8080(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7952(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8112(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8104(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8064(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8000(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8056(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8048(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-7968(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	-8224(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-7312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1240
	call	_ZdlPv@PLT
.L1240:
	movq	-7328(%rbp), %rbx
	movq	-7336(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1241
	.p2align 4,,10
	.p2align 3
.L1245:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1242
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1245
.L1243:
	movq	-7336(%rbp), %r13
.L1241:
	testq	%r13, %r13
	je	.L1246
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1246:
	movq	-8360(%rbp), %rdi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBaseD2Ev
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1580
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1242:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1245
	jmp	.L1243
	.p2align 4,,10
	.p2align 3
.L1235:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1238
	jmp	.L1236
	.p2align 4,,10
	.p2align 3
.L1228:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1231
	jmp	.L1229
	.p2align 4,,10
	.p2align 3
.L1221:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1224
	jmp	.L1222
	.p2align 4,,10
	.p2align 3
.L1214:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1217
	jmp	.L1215
	.p2align 4,,10
	.p2align 3
.L1207:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1210
	jmp	.L1208
	.p2align 4,,10
	.p2align 3
.L1200:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1203
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1193:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1196
	jmp	.L1194
	.p2align 4,,10
	.p2align 3
.L1186:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1189
	jmp	.L1187
	.p2align 4,,10
	.p2align 3
.L1172:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1175
	jmp	.L1173
	.p2align 4,,10
	.p2align 3
.L1179:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1182
	jmp	.L1180
	.p2align 4,,10
	.p2align 3
.L1543:
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	-240(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-235(%rbp), %rdx
	movaps	%xmm0, -7664(%rbp)
	movl	$117966599, -240(%rbp)
	movb	$8, -236(%rbp)
	movq	$0, -7648(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8360(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7664(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1060
	call	_ZdlPv@PLT
.L1060:
	movq	(%rbx), %rax
	movl	$97, %edx
	movq	%r15, %rdi
	movq	24(%rax), %rsi
	movq	(%rax), %rcx
	movq	8(%rax), %rbx
	movq	32(%rax), %r12
	movq	%rsi, -8000(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -7952(%rbp)
	movq	%rbx, -7984(%rbp)
	movq	16(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7896(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movl	$98, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -7968(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-7896(%rbp), %rdi
	movq	%r14, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal13Cast5ATSmi_83EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r12, %xmm4
	pxor	%xmm0, %xmm0
	movq	-7952(%rbp), %xmm6
	leaq	-7696(%rbp), %r12
	movq	%rbx, %xmm5
	movq	%r13, %rsi
	movq	%rax, -184(%rbp)
	movhps	-7968(%rbp), %xmm4
	movhps	-8000(%rbp), %xmm5
	movq	%r12, %rdi
	movaps	%xmm0, -7696(%rbp)
	movhps	-7984(%rbp), %xmm6
	leaq	-176(%rbp), %rdx
	movaps	%xmm4, -8048(%rbp)
	movaps	%xmm5, -8000(%rbp)
	movaps	%xmm6, -7952(%rbp)
	movaps	%xmm6, -240(%rbp)
	movaps	%xmm5, -224(%rbp)
	movaps	%xmm4, -208(%rbp)
	movq	%rbx, -192(%rbp)
	movq	$0, -7680(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-7152(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -7968(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7696(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1061
	call	_ZdlPv@PLT
.L1061:
	movq	-8032(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -7656(%rbp)
	jne	.L1581
.L1062:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1546:
	movq	-8064(%rbp), %rsi
	movq	%r15, %rdi
	movl	$1544, %ebx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-240(%rbp), %rsi
	leaq	-234(%rbp), %rdx
	movq	%r14, %rdi
	movw	%bx, -236(%rbp)
	movaps	%xmm0, -7664(%rbp)
	movl	$117966599, -240(%rbp)
	movq	$0, -7648(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8048(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7664(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1071
	call	_ZdlPv@PLT
.L1071:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	40(%rax), %rbx
	movaps	%xmm0, -7664(%rbp)
	movq	$0, -7648(%rbp)
	call	_Znwm@PLT
	leaq	-624(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -7664(%rbp)
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1072
	call	_ZdlPv@PLT
.L1072:
	movq	-7904(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1070
	.p2align 4,,10
	.p2align 3
.L1545:
	movq	-8032(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-240(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-232(%rbp), %rdx
	movabsq	$434603995588724487, %rax
	movaps	%xmm0, -7664(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -7648(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7968(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7664(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1068
	call	_ZdlPv@PLT
.L1068:
	movq	(%rbx), %rax
	leaq	-184(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movdqu	32(%rax), %xmm0
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm1
	movq	56(%rax), %rax
	movq	$0, -7648(%rbp)
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -192(%rbp)
	movaps	%xmm2, -240(%rbp)
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm0, -7664(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-6768(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -8056(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1069
	call	_ZdlPv@PLT
.L1069:
	movq	-8080(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1544:
	movq	-8224(%rbp), %rsi
	movq	%r15, %rdi
	movl	$1544, %r13d
	leaq	-7344(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movw	%r13w, -236(%rbp)
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	leaq	-240(%rbp), %r13
	leaq	-233(%rbp), %rdx
	movaps	%xmm0, -7664(%rbp)
	movq	%r13, %rsi
	movl	$117966599, -240(%rbp)
	movb	$8, -234(%rbp)
	movq	$0, -7648(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7664(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1065
	call	_ZdlPv@PLT
.L1065:
	movq	(%rbx), %rax
	leaq	-192(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm1
	movdqu	32(%rax), %xmm7
	movq	$0, -7648(%rbp)
	movaps	%xmm0, -224(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm1, -240(%rbp)
	movaps	%xmm0, -7664(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-6960(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -8048(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1066
	call	_ZdlPv@PLT
.L1066:
	movq	-8064(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1547:
	movq	-8080(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -7648(%rbp)
	movaps	%xmm0, -7664(%rbp)
	call	_Znwm@PLT
	movl	$1544, %r11d
	movq	-8056(%rbp), %rdi
	movq	%r14, %rsi
	movl	$117966599, (%rax)
	leaq	7(%rax), %rdx
	movw	%r11w, 4(%rax)
	movb	$6, 6(%rax)
	movq	%rax, -7664(%rbp)
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7664(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1074
	call	_ZdlPv@PLT
.L1074:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	16(%rax), %rcx
	movq	8(%rax), %r13
	movq	(%rax), %rbx
	movq	48(%rax), %r12
	movq	%rsi, -7984(%rbp)
	movq	32(%rax), %rsi
	movq	%rdx, -8032(%rbp)
	movl	$99, %edx
	movq	%rcx, -7952(%rbp)
	movq	%rsi, -8000(%rbp)
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	-7896(%rbp), %rdi
	call	_ZN2v88internal23Cast13ATFastJSArray_135EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10HeapObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%r13, %xmm7
	movq	%r12, %xmm3
	movq	-7952(%rbp), %xmm2
	punpcklqdq	%xmm7, %xmm3
	movq	%r13, %xmm5
	leaq	-7696(%rbp), %r12
	movq	-8000(%rbp), %xmm7
	movq	%rbx, %xmm4
	leaq	-240(%rbp), %r13
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movhps	-8032(%rbp), %xmm7
	punpcklqdq	%xmm5, %xmm4
	movq	%r13, %rsi
	movq	%rax, -176(%rbp)
	movhps	-7984(%rbp), %xmm2
	leaq	-168(%rbp), %rdx
	movaps	%xmm3, -8080(%rbp)
	movaps	%xmm7, -8032(%rbp)
	movaps	%xmm2, -7984(%rbp)
	movaps	%xmm4, -7952(%rbp)
	movaps	%xmm4, -240(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm0, -7696(%rbp)
	movq	$0, -7680(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-6384(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -8064(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7696(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1075
	call	_ZdlPv@PLT
.L1075:
	movq	-8144(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-6576(%rbp), %rax
	cmpq	$0, -7656(%rbp)
	movq	%rax, -8000(%rbp)
	jne	.L1582
.L1076:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L1073
	.p2align 4,,10
	.p2align 3
.L1550:
	movq	-8160(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -7648(%rbp)
	movaps	%xmm0, -7664(%rbp)
	call	_Znwm@PLT
	movl	$1544, %r10d
	movq	-8104(%rbp), %rdi
	movq	%r14, %rsi
	movl	$117966599, (%rax)
	leaq	7(%rax), %rdx
	movw	%r10w, 4(%rax)
	movb	$6, 6(%rax)
	movq	%rax, -7664(%rbp)
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7664(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1085
	call	_ZdlPv@PLT
.L1085:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	40(%rax), %rbx
	movaps	%xmm0, -7664(%rbp)
	movq	$0, -7648(%rbp)
	call	_Znwm@PLT
	leaq	-624(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -7664(%rbp)
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1086
	call	_ZdlPv@PLT
.L1086:
	movq	-7904(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1084
	.p2align 4,,10
	.p2align 3
.L1549:
	movq	-8144(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-240(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-231(%rbp), %rdx
	movaps	%xmm0, -7664(%rbp)
	movabsq	$506098639673231111, %rax
	movq	%rax, -240(%rbp)
	movb	$7, -232(%rbp)
	movq	$0, -7648(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8064(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7664(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1082
	call	_ZdlPv@PLT
.L1082:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	24(%rax), %rdi
	movq	32(%rax), %rsi
	movq	48(%rax), %rdx
	movq	(%rax), %r10
	movq	8(%rax), %r9
	movq	16(%rax), %r8
	movq	40(%rax), %rcx
	movq	64(%rax), %rax
	movq	%rdi, -216(%rbp)
	movq	%r14, %rdi
	movq	%rsi, -208(%rbp)
	movq	%r13, %rsi
	movq	%rdx, -192(%rbp)
	leaq	-176(%rbp), %rdx
	movq	%r10, -240(%rbp)
	movq	%r9, -232(%rbp)
	movq	%r8, -224(%rbp)
	movq	%rcx, -200(%rbp)
	movq	%rax, -184(%rbp)
	movaps	%xmm0, -7664(%rbp)
	movq	$0, -7648(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-6000(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -8112(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1083
	call	_ZdlPv@PLT
.L1083:
	movq	-8176(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1081
	.p2align 4,,10
	.p2align 3
.L1548:
	movq	-8112(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-240(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-232(%rbp), %rdx
	movabsq	$506098639673231111, %rax
	movaps	%xmm0, -7664(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -7648(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8000(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7664(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1079
	call	_ZdlPv@PLT
.L1079:
	movq	(%rbx), %rax
	leaq	-184(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movdqu	32(%rax), %xmm0
	movdqu	(%rax), %xmm2
	movdqu	16(%rax), %xmm1
	movq	48(%rax), %rax
	movq	$0, -7648(%rbp)
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -192(%rbp)
	movaps	%xmm2, -240(%rbp)
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm0, -7664(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-6192(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -8104(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1080
	call	_ZdlPv@PLT
.L1080:
	movq	-8160(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1078
	.p2align 4,,10
	.p2align 3
.L1551:
	movq	-8176(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-240(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-232(%rbp), %rdx
	movabsq	$506098639673231111, %rax
	movaps	%xmm0, -7664(%rbp)
	movq	%rax, -240(%rbp)
	movq	$0, -7648(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8112(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7664(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1088
	call	_ZdlPv@PLT
.L1088:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	8(%rax), %rbx
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	(%rax), %rcx
	movq	%rbx, -8160(%rbp)
	movq	16(%rax), %rbx
	movq	%rsi, -8272(%rbp)
	movq	32(%rax), %rsi
	movq	%rdx, -8304(%rbp)
	movq	48(%rax), %rdx
	movq	%rbx, -8176(%rbp)
	movq	56(%rax), %rbx
	movq	%rsi, -8288(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -8528(%rbp)
	movl	$100, %edx
	movq	%rcx, -8144(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7896(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal25NewFastJSArrayWitness_236EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7JSArrayEEE@PLT
	movq	-7640(%rbp), %rdi
	movq	-7624(%rbp), %rax
	movl	$103, %edx
	leaq	.LC2(%rip), %rsi
	movq	-7648(%rbp), %r12
	movq	-7664(%rbp), %r13
	movq	%rdi, -8032(%rbp)
	movq	-7656(%rbp), %rdi
	movq	%rax, -7952(%rbp)
	movq	-7632(%rbp), %rax
	movq	%rdi, -8080(%rbp)
	movq	%r15, %rdi
	movq	%rax, -7984(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %xmm7
	movl	$112, %edi
	movq	-8144(%rbp), %xmm0
	movq	$0, -7648(%rbp)
	movhps	-8160(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-8176(%rbp), %xmm0
	movhps	-8272(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-8288(%rbp), %xmm0
	movhps	-8304(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-8528(%rbp), %xmm0
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	%r13, %xmm0
	movhps	-8080(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	%r12, %xmm0
	movhps	-8032(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-7984(%rbp), %xmm0
	movhps	-7952(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7664(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm6
	movq	%r14, %rsi
	leaq	112(%rax), %rdx
	movq	%rax, -7664(%rbp)
	movups	%xmm7, (%rax)
	movdqa	-224(%rbp), %xmm7
	movups	%xmm6, 80(%rax)
	movups	%xmm7, 16(%rax)
	movdqa	-208(%rbp), %xmm7
	movq	%rdx, -7648(%rbp)
	movups	%xmm7, 32(%rax)
	movdqa	-192(%rbp), %xmm7
	movq	%rdx, -7656(%rbp)
	movups	%xmm7, 48(%rax)
	movdqa	-176(%rbp), %xmm7
	movups	%xmm7, 64(%rax)
	movdqa	-144(%rbp), %xmm7
	movups	%xmm7, 96(%rax)
	leaq	-5808(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -7952(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1089
	call	_ZdlPv@PLT
.L1089:
	movq	-8368(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1087
	.p2align 4,,10
	.p2align 3
.L1552:
	movq	-8368(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7808(%rbp)
	movq	$0, -7800(%rbp)
	movq	$0, -7792(%rbp)
	movq	$0, -7784(%rbp)
	movq	$0, -7776(%rbp)
	movq	$0, -7768(%rbp)
	movq	$0, -7760(%rbp)
	movq	$0, -7752(%rbp)
	movq	$0, -7744(%rbp)
	movq	$0, -7736(%rbp)
	movq	$0, -7728(%rbp)
	movq	$0, -7720(%rbp)
	movq	$0, -7712(%rbp)
	movq	$0, -7696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7696(%rbp), %rax
	movq	-7952(%rbp), %rdi
	pushq	%rax
	leaq	-7712(%rbp), %rax
	leaq	-7792(%rbp), %rcx
	pushq	%rax
	leaq	-7720(%rbp), %rax
	leaq	-7776(%rbp), %r9
	pushq	%rax
	leaq	-7728(%rbp), %rax
	leaq	-7784(%rbp), %r8
	pushq	%rax
	leaq	-7736(%rbp), %rax
	leaq	-7800(%rbp), %rdx
	pushq	%rax
	leaq	-7744(%rbp), %rax
	leaq	-7808(%rbp), %rsi
	pushq	%rax
	leaq	-7752(%rbp), %rax
	pushq	%rax
	leaq	-7760(%rbp), %rax
	pushq	%rax
	leaq	-7768(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	movq	-7896(%rbp), %rsi
	addq	$80, %rsp
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7768(%rbp), %r13
	movq	-7760(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrLessThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$112, %edi
	movq	-7712(%rbp), %xmm5
	movq	-7728(%rbp), %xmm6
	movaps	%xmm0, -7664(%rbp)
	movq	-7744(%rbp), %xmm3
	movq	-7760(%rbp), %xmm7
	movhps	-7696(%rbp), %xmm5
	movq	-7776(%rbp), %xmm1
	movq	$0, -7648(%rbp)
	movq	-7792(%rbp), %xmm2
	movhps	-7720(%rbp), %xmm6
	movq	-7808(%rbp), %xmm4
	movhps	-7736(%rbp), %xmm3
	movhps	-7752(%rbp), %xmm7
	movhps	-7768(%rbp), %xmm1
	movaps	%xmm5, -8176(%rbp)
	movhps	-7784(%rbp), %xmm2
	movhps	-7800(%rbp), %xmm4
	movaps	%xmm6, -8160(%rbp)
	movaps	%xmm3, -7984(%rbp)
	movaps	%xmm7, -8288(%rbp)
	movaps	%xmm1, -8144(%rbp)
	movaps	%xmm2, -8032(%rbp)
	movaps	%xmm4, -8272(%rbp)
	movaps	%xmm4, -240(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-224(%rbp), %xmm6
	movdqa	-208(%rbp), %xmm7
	movq	-8080(%rbp), %rdi
	leaq	112(%rax), %rdx
	movq	%rax, -7664(%rbp)
	movups	%xmm5, (%rax)
	movdqa	-192(%rbp), %xmm5
	movups	%xmm6, 16(%rax)
	movdqa	-176(%rbp), %xmm6
	movups	%xmm7, 32(%rax)
	movdqa	-160(%rbp), %xmm7
	movups	%xmm5, 48(%rax)
	movdqa	-144(%rbp), %xmm5
	movups	%xmm6, 64(%rax)
	movups	%xmm7, 80(%rax)
	movups	%xmm5, 96(%rax)
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1091
	call	_ZdlPv@PLT
.L1091:
	movdqa	-8272(%rbp), %xmm6
	movdqa	-8032(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movl	$112, %edi
	movdqa	-8144(%rbp), %xmm3
	movdqa	-8288(%rbp), %xmm5
	movaps	%xmm0, -7664(%rbp)
	movaps	%xmm6, -240(%rbp)
	movdqa	-7984(%rbp), %xmm6
	movaps	%xmm7, -224(%rbp)
	movdqa	-8160(%rbp), %xmm7
	movaps	%xmm3, -208(%rbp)
	movdqa	-8176(%rbp), %xmm3
	movaps	%xmm5, -192(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm7, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movq	$0, -7648(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm4
	movq	%r14, %rsi
	movdqa	-224(%rbp), %xmm5
	movdqa	-208(%rbp), %xmm6
	movdqa	-192(%rbp), %xmm7
	leaq	112(%rax), %rdx
	leaq	-1008(%rbp), %rdi
	movdqa	-176(%rbp), %xmm3
	movups	%xmm4, (%rax)
	movdqa	-160(%rbp), %xmm4
	movups	%xmm5, 16(%rax)
	movdqa	-144(%rbp), %xmm5
	movups	%xmm6, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm3, 64(%rax)
	movups	%xmm4, 80(%rax)
	movups	%xmm5, 96(%rax)
	movq	%rax, -7664(%rbp)
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1092
	call	_ZdlPv@PLT
.L1092:
	movq	-8088(%rbp), %rcx
	movq	-8016(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1090
	.p2align 4,,10
	.p2align 3
.L1553:
	movq	-8016(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7808(%rbp)
	movq	$0, -7800(%rbp)
	movq	$0, -7792(%rbp)
	movq	$0, -7784(%rbp)
	movq	$0, -7776(%rbp)
	movq	$0, -7768(%rbp)
	movq	$0, -7760(%rbp)
	movq	$0, -7752(%rbp)
	movq	$0, -7744(%rbp)
	movq	$0, -7736(%rbp)
	movq	$0, -7728(%rbp)
	movq	$0, -7720(%rbp)
	movq	$0, -7712(%rbp)
	movq	$0, -7696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7696(%rbp), %rax
	movq	-8080(%rbp), %rdi
	pushq	%rax
	leaq	-7712(%rbp), %rax
	leaq	-7776(%rbp), %r9
	pushq	%rax
	leaq	-7720(%rbp), %rax
	leaq	-7784(%rbp), %r8
	pushq	%rax
	leaq	-7728(%rbp), %rax
	leaq	-7792(%rbp), %rcx
	pushq	%rax
	leaq	-7736(%rbp), %rax
	leaq	-7800(%rbp), %rdx
	pushq	%rax
	leaq	-7744(%rbp), %rax
	leaq	-7808(%rbp), %rsi
	pushq	%rax
	leaq	-7752(%rbp), %rax
	pushq	%rax
	leaq	-7760(%rbp), %rax
	pushq	%rax
	leaq	-7768(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	addq	$80, %rsp
	movl	$3097, %edx
	movq	%r15, %rdi
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	-7896(%rbp), %rbx
	movq	%r14, %rdi
	movq	%rax, %r12
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movl	$1, %esi
	movq	%r14, %rdi
	movq	-7744(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler14IntPtrConstantEl@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrSubENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movzwl	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE(%rip), %esi
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v88internal8compiler13CodeAssembler14LoadFromObjectENS0_11MachineTypeENS1_5TNodeINS0_10HeapObjectEEENS4_INS0_7IntPtrTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7728(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler12WordNotEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$112, %edi
	movq	-7712(%rbp), %xmm5
	movq	-7728(%rbp), %xmm6
	movaps	%xmm0, -7664(%rbp)
	movq	-7744(%rbp), %xmm3
	movq	-7760(%rbp), %xmm7
	movhps	-7696(%rbp), %xmm5
	movq	-7776(%rbp), %xmm1
	movq	$0, -7648(%rbp)
	movq	-7792(%rbp), %xmm2
	movhps	-7720(%rbp), %xmm6
	movq	-7808(%rbp), %xmm4
	movhps	-7736(%rbp), %xmm3
	movhps	-7752(%rbp), %xmm7
	movhps	-7768(%rbp), %xmm1
	movaps	%xmm5, -8176(%rbp)
	movhps	-7784(%rbp), %xmm2
	movhps	-7800(%rbp), %xmm4
	movaps	%xmm6, -8160(%rbp)
	movaps	%xmm3, -7984(%rbp)
	movaps	%xmm7, -8032(%rbp)
	movaps	%xmm1, -8016(%rbp)
	movaps	%xmm2, -8288(%rbp)
	movaps	%xmm4, -8272(%rbp)
	movaps	%xmm4, -240(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm6
	movq	%r14, %rsi
	movdqa	-224(%rbp), %xmm7
	movdqa	-208(%rbp), %xmm3
	movdqa	-192(%rbp), %xmm4
	leaq	112(%rax), %rdx
	movq	%rax, -7664(%rbp)
	movdqa	-176(%rbp), %xmm5
	movups	%xmm6, (%rax)
	movdqa	-160(%rbp), %xmm6
	movups	%xmm7, 16(%rax)
	movdqa	-144(%rbp), %xmm7
	movq	-8144(%rbp), %rdi
	movups	%xmm3, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm5, 64(%rax)
	movups	%xmm6, 80(%rax)
	movups	%xmm7, 96(%rax)
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1094
	call	_ZdlPv@PLT
.L1094:
	movdqa	-8272(%rbp), %xmm3
	movdqa	-8288(%rbp), %xmm4
	pxor	%xmm0, %xmm0
	movl	$112, %edi
	movdqa	-8016(%rbp), %xmm5
	movdqa	-8032(%rbp), %xmm6
	movaps	%xmm0, -7664(%rbp)
	movdqa	-7984(%rbp), %xmm7
	movaps	%xmm3, -240(%rbp)
	movdqa	-8160(%rbp), %xmm3
	movaps	%xmm4, -224(%rbp)
	movdqa	-8176(%rbp), %xmm4
	movaps	%xmm5, -208(%rbp)
	movaps	%xmm6, -192(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movq	$0, -7648(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm5
	movq	%r14, %rsi
	movdqa	-224(%rbp), %xmm6
	movdqa	-208(%rbp), %xmm7
	movdqa	-192(%rbp), %xmm3
	leaq	112(%rax), %rdx
	movq	%rax, -7664(%rbp)
	movups	%xmm5, (%rax)
	movdqa	-176(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm5
	movups	%xmm6, 16(%rax)
	movdqa	-144(%rbp), %xmm6
	movups	%xmm7, 32(%rax)
	movups	%xmm3, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm5, 80(%rax)
	movups	%xmm6, 96(%rax)
	leaq	-5232(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	movq	%rax, -8160(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1095
	call	_ZdlPv@PLT
.L1095:
	movq	-8208(%rbp), %rcx
	movq	-8192(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -5360(%rbp)
	je	.L1096
.L1554:
	movq	-8192(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7808(%rbp)
	movq	$0, -7800(%rbp)
	movq	$0, -7792(%rbp)
	movq	$0, -7784(%rbp)
	movq	$0, -7776(%rbp)
	movq	$0, -7768(%rbp)
	movq	$0, -7760(%rbp)
	movq	$0, -7752(%rbp)
	movq	$0, -7744(%rbp)
	movq	$0, -7736(%rbp)
	movq	$0, -7728(%rbp)
	movq	$0, -7720(%rbp)
	movq	$0, -7712(%rbp)
	movq	$0, -7696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7696(%rbp), %rax
	movq	-8144(%rbp), %rdi
	pushq	%rax
	leaq	-7712(%rbp), %rax
	leaq	-7792(%rbp), %rcx
	pushq	%rax
	leaq	-7720(%rbp), %rax
	leaq	-7800(%rbp), %rdx
	pushq	%rax
	leaq	-7728(%rbp), %rax
	leaq	-7808(%rbp), %rsi
	pushq	%rax
	leaq	-7736(%rbp), %rax
	leaq	-7776(%rbp), %r9
	pushq	%rax
	leaq	-7744(%rbp), %rax
	leaq	-7784(%rbp), %r8
	pushq	%rax
	leaq	-7752(%rbp), %rax
	pushq	%rax
	leaq	-7760(%rbp), %rax
	pushq	%rax
	leaq	-7768(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	addq	$80, %rsp
	movl	$112, %edi
	movq	-7712(%rbp), %xmm0
	movq	-7728(%rbp), %xmm1
	movq	-7744(%rbp), %xmm2
	movq	$0, -7648(%rbp)
	movq	-7760(%rbp), %xmm3
	movhps	-7696(%rbp), %xmm0
	movq	-7776(%rbp), %xmm4
	movq	-7792(%rbp), %xmm5
	movhps	-7720(%rbp), %xmm1
	movq	-7808(%rbp), %xmm6
	movhps	-7736(%rbp), %xmm2
	movhps	-7752(%rbp), %xmm3
	movhps	-7768(%rbp), %xmm4
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-7784(%rbp), %xmm5
	movhps	-7800(%rbp), %xmm6
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm6, -240(%rbp)
	movaps	%xmm5, -224(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -7664(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm7
	movq	%r14, %rsi
	movdqa	-224(%rbp), %xmm3
	movdqa	-208(%rbp), %xmm4
	movdqa	-192(%rbp), %xmm5
	leaq	112(%rax), %rdx
	leaq	-4464(%rbp), %rdi
	movdqa	-176(%rbp), %xmm6
	movups	%xmm7, (%rax)
	movdqa	-160(%rbp), %xmm7
	movups	%xmm3, 16(%rax)
	movdqa	-144(%rbp), %xmm3
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	movups	%xmm7, 80(%rax)
	movups	%xmm3, 96(%rax)
	movq	%rax, -7664(%rbp)
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1097
	call	_ZdlPv@PLT
.L1097:
	movq	-7928(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1096
	.p2align 4,,10
	.p2align 3
.L1555:
	movq	-8208(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7808(%rbp)
	movq	$0, -7800(%rbp)
	movq	$0, -7792(%rbp)
	movq	$0, -7784(%rbp)
	movq	$0, -7776(%rbp)
	movq	$0, -7768(%rbp)
	movq	$0, -7760(%rbp)
	movq	$0, -7752(%rbp)
	movq	$0, -7744(%rbp)
	movq	$0, -7736(%rbp)
	movq	$0, -7728(%rbp)
	movq	$0, -7720(%rbp)
	movq	$0, -7712(%rbp)
	movq	$0, -7696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7696(%rbp), %rax
	movq	-8160(%rbp), %rdi
	pushq	%rax
	leaq	-7712(%rbp), %rax
	leaq	-7792(%rbp), %rcx
	pushq	%rax
	leaq	-7720(%rbp), %rax
	leaq	-7776(%rbp), %r9
	pushq	%rax
	leaq	-7728(%rbp), %rax
	leaq	-7784(%rbp), %r8
	pushq	%rax
	leaq	-7736(%rbp), %rax
	leaq	-7800(%rbp), %rdx
	pushq	%rax
	leaq	-7744(%rbp), %rax
	leaq	-7808(%rbp), %rsi
	pushq	%rax
	leaq	-7752(%rbp), %rax
	pushq	%rax
	leaq	-7760(%rbp), %rax
	pushq	%rax
	leaq	-7768(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	addq	$80, %rsp
	movl	$3104, %edx
	movq	%r15, %rdi
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7896(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler32IsNoElementsProtectorCellInvalidEv@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$112, %edi
	movq	-7712(%rbp), %xmm5
	movq	-7728(%rbp), %xmm6
	movaps	%xmm0, -7664(%rbp)
	movq	-7744(%rbp), %xmm3
	movq	-7760(%rbp), %xmm7
	movhps	-7696(%rbp), %xmm5
	movq	-7776(%rbp), %xmm1
	movq	$0, -7648(%rbp)
	movq	-7792(%rbp), %xmm2
	movhps	-7720(%rbp), %xmm6
	movq	-7808(%rbp), %xmm4
	movhps	-7736(%rbp), %xmm3
	movhps	-7752(%rbp), %xmm7
	movhps	-7768(%rbp), %xmm1
	movaps	%xmm5, -8032(%rbp)
	movhps	-7784(%rbp), %xmm2
	movhps	-7800(%rbp), %xmm4
	movaps	%xmm6, -7984(%rbp)
	movaps	%xmm3, -8288(%rbp)
	movaps	%xmm7, -8272(%rbp)
	movaps	%xmm1, -8016(%rbp)
	movaps	%xmm2, -8208(%rbp)
	movaps	%xmm4, -8192(%rbp)
	movaps	%xmm4, -240(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm4
	movq	%r14, %rsi
	movdqa	-224(%rbp), %xmm5
	movdqa	-208(%rbp), %xmm6
	movdqa	-192(%rbp), %xmm2
	leaq	112(%rax), %rdx
	movq	%rax, -7664(%rbp)
	movdqa	-176(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm3
	movups	%xmm4, (%rax)
	movdqa	-144(%rbp), %xmm1
	movq	-8176(%rbp), %rdi
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm3, 80(%rax)
	movups	%xmm1, 96(%rax)
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1099
	call	_ZdlPv@PLT
.L1099:
	movdqa	-8192(%rbp), %xmm4
	movdqa	-8208(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movl	$112, %edi
	movdqa	-8016(%rbp), %xmm6
	movdqa	-8272(%rbp), %xmm2
	movaps	%xmm0, -7664(%rbp)
	movdqa	-8288(%rbp), %xmm7
	movdqa	-7984(%rbp), %xmm3
	movaps	%xmm4, -240(%rbp)
	movdqa	-8032(%rbp), %xmm1
	movaps	%xmm5, -224(%rbp)
	movaps	%xmm6, -208(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movq	$0, -7648(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm4
	movq	%r14, %rsi
	movdqa	-224(%rbp), %xmm5
	movdqa	-208(%rbp), %xmm6
	movdqa	-192(%rbp), %xmm2
	leaq	112(%rax), %rdx
	movq	%rax, -7664(%rbp)
	movdqa	-176(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm3
	movups	%xmm4, (%rax)
	movdqa	-144(%rbp), %xmm1
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm3, 80(%rax)
	movups	%xmm1, 96(%rax)
	leaq	-4848(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	movq	%rax, -8192(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1100
	call	_ZdlPv@PLT
.L1100:
	movq	-8240(%rbp), %rcx
	movq	-8216(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -4976(%rbp)
	je	.L1101
.L1556:
	movq	-8216(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7808(%rbp)
	movq	$0, -7800(%rbp)
	movq	$0, -7792(%rbp)
	movq	$0, -7784(%rbp)
	movq	$0, -7776(%rbp)
	movq	$0, -7768(%rbp)
	movq	$0, -7760(%rbp)
	movq	$0, -7752(%rbp)
	movq	$0, -7744(%rbp)
	movq	$0, -7736(%rbp)
	movq	$0, -7728(%rbp)
	movq	$0, -7720(%rbp)
	movq	$0, -7712(%rbp)
	movq	$0, -7696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7696(%rbp), %rax
	movq	-8176(%rbp), %rdi
	pushq	%rax
	leaq	-7712(%rbp), %rax
	leaq	-7792(%rbp), %rcx
	pushq	%rax
	leaq	-7720(%rbp), %rax
	leaq	-7800(%rbp), %rdx
	pushq	%rax
	leaq	-7728(%rbp), %rax
	leaq	-7808(%rbp), %rsi
	pushq	%rax
	leaq	-7736(%rbp), %rax
	leaq	-7776(%rbp), %r9
	pushq	%rax
	leaq	-7744(%rbp), %rax
	leaq	-7784(%rbp), %r8
	pushq	%rax
	leaq	-7752(%rbp), %rax
	pushq	%rax
	leaq	-7760(%rbp), %rax
	pushq	%rax
	leaq	-7768(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	addq	$80, %rsp
	movl	$112, %edi
	movq	-7712(%rbp), %xmm0
	movq	-7728(%rbp), %xmm1
	movq	-7744(%rbp), %xmm2
	movq	$0, -7648(%rbp)
	movq	-7760(%rbp), %xmm3
	movhps	-7696(%rbp), %xmm0
	movq	-7776(%rbp), %xmm4
	movq	-7792(%rbp), %xmm5
	movhps	-7720(%rbp), %xmm1
	movq	-7808(%rbp), %xmm6
	movhps	-7736(%rbp), %xmm2
	movhps	-7752(%rbp), %xmm3
	movhps	-7768(%rbp), %xmm4
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-7784(%rbp), %xmm5
	movhps	-7800(%rbp), %xmm6
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm6, -240(%rbp)
	movaps	%xmm5, -224(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -7664(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm4
	movq	%r14, %rsi
	movdqa	-224(%rbp), %xmm5
	movdqa	-208(%rbp), %xmm6
	movdqa	-192(%rbp), %xmm2
	leaq	112(%rax), %rdx
	leaq	-4464(%rbp), %rdi
	movdqa	-176(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm3
	movups	%xmm4, (%rax)
	movdqa	-144(%rbp), %xmm1
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm3, 80(%rax)
	movups	%xmm1, 96(%rax)
	movq	%rax, -7664(%rbp)
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1102
	call	_ZdlPv@PLT
.L1102:
	movq	-7928(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -4784(%rbp)
	je	.L1103
.L1557:
	movq	-8240(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7808(%rbp)
	movq	$0, -7800(%rbp)
	movq	$0, -7792(%rbp)
	movq	$0, -7784(%rbp)
	movq	$0, -7776(%rbp)
	movq	$0, -7768(%rbp)
	movq	$0, -7760(%rbp)
	movq	$0, -7752(%rbp)
	movq	$0, -7744(%rbp)
	movq	$0, -7736(%rbp)
	movq	$0, -7728(%rbp)
	movq	$0, -7720(%rbp)
	movq	$0, -7712(%rbp)
	movq	$0, -7696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7696(%rbp), %rax
	movq	-8192(%rbp), %rdi
	pushq	%rax
	leaq	-7712(%rbp), %rax
	leaq	-7792(%rbp), %rcx
	pushq	%rax
	leaq	-7720(%rbp), %rax
	leaq	-7776(%rbp), %r9
	pushq	%rax
	leaq	-7728(%rbp), %rax
	leaq	-7784(%rbp), %r8
	pushq	%rax
	leaq	-7736(%rbp), %rax
	leaq	-7800(%rbp), %rdx
	pushq	%rax
	leaq	-7744(%rbp), %rax
	leaq	-7808(%rbp), %rsi
	pushq	%rax
	leaq	-7752(%rbp), %rax
	pushq	%rax
	leaq	-7760(%rbp), %rax
	pushq	%rax
	leaq	-7768(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	addq	$80, %rsp
	movl	$3105, %edx
	movq	%r15, %rdi
	leaq	.LC6(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$104, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	movq	-7744(%rbp), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7744(%rbp), %rax
	movl	$112, %edi
	movq	-7760(%rbp), %xmm0
	movq	-7776(%rbp), %xmm1
	movq	%rbx, -168(%rbp)
	movhps	-7752(%rbp), %xmm0
	movq	%rax, -176(%rbp)
	movq	-7792(%rbp), %xmm2
	movaps	%xmm0, -192(%rbp)
	movq	-7728(%rbp), %xmm0
	movq	-7808(%rbp), %xmm3
	movhps	-7768(%rbp), %xmm1
	movhps	-7784(%rbp), %xmm2
	movaps	%xmm1, -208(%rbp)
	movhps	-7720(%rbp), %xmm0
	movhps	-7800(%rbp), %xmm3
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm0, -160(%rbp)
	movq	-7712(%rbp), %xmm0
	movaps	%xmm3, -240(%rbp)
	movhps	-7696(%rbp), %xmm0
	movq	$0, -7648(%rbp)
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7664(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm4
	movq	%r14, %rsi
	movdqa	-224(%rbp), %xmm5
	movdqa	-208(%rbp), %xmm6
	movdqa	-192(%rbp), %xmm2
	leaq	112(%rax), %rdx
	leaq	-4656(%rbp), %rdi
	movdqa	-176(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm3
	movups	%xmm4, (%rax)
	movdqa	-144(%rbp), %xmm1
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm3, 80(%rax)
	movups	%xmm1, 96(%rax)
	movq	%rax, -7664(%rbp)
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1104
	call	_ZdlPv@PLT
.L1104:
	movq	-8096(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1103
	.p2align 4,,10
	.p2align 3
.L1559:
	movq	-7928(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7808(%rbp)
	leaq	-4464(%rbp), %r12
	movq	$0, -7800(%rbp)
	movq	$0, -7792(%rbp)
	movq	$0, -7784(%rbp)
	movq	$0, -7776(%rbp)
	movq	$0, -7768(%rbp)
	movq	$0, -7760(%rbp)
	movq	$0, -7752(%rbp)
	movq	$0, -7744(%rbp)
	movq	$0, -7736(%rbp)
	movq	$0, -7728(%rbp)
	movq	$0, -7720(%rbp)
	movq	$0, -7712(%rbp)
	movq	$0, -7696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-7696(%rbp), %rax
	pushq	%rax
	leaq	-7712(%rbp), %rax
	leaq	-7792(%rbp), %rcx
	pushq	%rax
	leaq	-7720(%rbp), %rax
	leaq	-7800(%rbp), %rdx
	pushq	%rax
	leaq	-7728(%rbp), %rax
	leaq	-7808(%rbp), %rsi
	pushq	%rax
	leaq	-7736(%rbp), %rax
	leaq	-7776(%rbp), %r9
	pushq	%rax
	leaq	-7744(%rbp), %rax
	leaq	-7784(%rbp), %r8
	pushq	%rax
	leaq	-7752(%rbp), %rax
	pushq	%rax
	leaq	-7760(%rbp), %rax
	pushq	%rax
	leaq	-7768(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	pxor	%xmm0, %xmm0
	addq	$80, %rsp
	movq	-7768(%rbp), %rbx
	movl	$8, %edi
	movaps	%xmm0, -7664(%rbp)
	movq	$0, -7648(%rbp)
	call	_Znwm@PLT
	leaq	-624(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -7664(%rbp)
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1108
	call	_ZdlPv@PLT
.L1108:
	movq	-7904(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1558:
	movq	-8096(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7808(%rbp)
	leaq	-4656(%rbp), %r12
	movq	$0, -7800(%rbp)
	movq	$0, -7792(%rbp)
	movq	$0, -7784(%rbp)
	movq	$0, -7776(%rbp)
	movq	$0, -7768(%rbp)
	movq	$0, -7760(%rbp)
	movq	$0, -7752(%rbp)
	movq	$0, -7744(%rbp)
	movq	$0, -7736(%rbp)
	movq	$0, -7728(%rbp)
	movq	$0, -7720(%rbp)
	movq	$0, -7712(%rbp)
	movq	$0, -7696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-7696(%rbp), %rax
	pushq	%rax
	leaq	-7712(%rbp), %rax
	leaq	-7792(%rbp), %rcx
	pushq	%rax
	leaq	-7720(%rbp), %rax
	leaq	-7800(%rbp), %rdx
	pushq	%rax
	leaq	-7728(%rbp), %rax
	leaq	-7808(%rbp), %rsi
	pushq	%rax
	leaq	-7736(%rbp), %rax
	leaq	-7776(%rbp), %r9
	pushq	%rax
	leaq	-7744(%rbp), %rax
	leaq	-7784(%rbp), %r8
	pushq	%rax
	leaq	-7752(%rbp), %rax
	pushq	%rax
	leaq	-7760(%rbp), %rax
	pushq	%rax
	leaq	-7768(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	addq	$80, %rsp
	movl	$112, %edi
	movq	-7712(%rbp), %xmm0
	movq	-7728(%rbp), %xmm1
	movq	-7744(%rbp), %xmm2
	movq	$0, -7648(%rbp)
	movq	-7760(%rbp), %xmm3
	movhps	-7696(%rbp), %xmm0
	movq	-7776(%rbp), %xmm4
	movq	-7792(%rbp), %xmm5
	movhps	-7720(%rbp), %xmm1
	movq	-7808(%rbp), %xmm6
	movhps	-7736(%rbp), %xmm2
	movhps	-7752(%rbp), %xmm3
	movhps	-7768(%rbp), %xmm4
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-7784(%rbp), %xmm5
	movhps	-7800(%rbp), %xmm6
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm6, -240(%rbp)
	movaps	%xmm5, -224(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -7664(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm4
	movq	%r14, %rsi
	movdqa	-224(%rbp), %xmm5
	movdqa	-208(%rbp), %xmm6
	movdqa	-192(%rbp), %xmm2
	leaq	112(%rax), %rdx
	movq	%rax, -7664(%rbp)
	movdqa	-176(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm3
	movups	%xmm4, (%rax)
	movdqa	-144(%rbp), %xmm1
	movq	-8208(%rbp), %rdi
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm3, 80(%rax)
	movups	%xmm1, 96(%rax)
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1106
	call	_ZdlPv@PLT
.L1106:
	movq	-8256(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1561:
	movq	-8432(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC9(%rip), %xmm0
	leaq	-240(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-224(%rbp), %rdx
	movq	$0, -7648(%rbp)
	movaps	%xmm0, -240(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7664(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8272(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7664(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1112
	call	_ZdlPv@PLT
.L1112:
	movq	(%rbx), %rax
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	8(%rax), %rbx
	movq	72(%rax), %r11
	movq	88(%rax), %r10
	movq	%rsi, -8216(%rbp)
	movq	32(%rax), %rsi
	movq	(%rax), %rcx
	movq	%rdx, -8256(%rbp)
	movq	%rdi, -8304(%rbp)
	movq	48(%rax), %rdx
	movq	64(%rax), %rdi
	movq	%rbx, -8016(%rbp)
	movq	%r11, -8528(%rbp)
	movq	16(%rax), %rbx
	movq	80(%rax), %r11
	movq	%r10, -8552(%rbp)
	movq	104(%rax), %r10
	movq	%rsi, -8240(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -8288(%rbp)
	movl	$107, %edx
	movq	120(%rax), %r12
	movq	%rdi, -8432(%rbp)
	movq	%r15, %rdi
	movq	112(%rax), %r13
	movq	%rcx, -7984(%rbp)
	movq	%r11, -8544(%rbp)
	movq	%r10, -8576(%rbp)
	movq	%rbx, -8032(%rbp)
	movq	96(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7896(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler21LoadFastJSArrayLengthENS0_8compiler11SloppyTNodeINS0_7JSArrayEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-7896(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler24IntPtrGreaterThanOrEqualENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm5
	pxor	%xmm0, %xmm0
	movq	-8544(%rbp), %xmm6
	movq	-8432(%rbp), %xmm3
	movhps	-8576(%rbp), %xmm5
	movq	-8240(%rbp), %xmm1
	movl	$112, %edi
	movq	-8032(%rbp), %xmm2
	movhps	-8552(%rbp), %xmm6
	movq	-7984(%rbp), %xmm4
	movaps	%xmm5, -8576(%rbp)
	movq	-8288(%rbp), %xmm7
	movhps	-8528(%rbp), %xmm3
	movhps	-8256(%rbp), %xmm1
	movaps	%xmm6, -8544(%rbp)
	movhps	-8216(%rbp), %xmm2
	movhps	-8016(%rbp), %xmm4
	movaps	%xmm3, -8432(%rbp)
	movhps	-8304(%rbp), %xmm7
	movaps	%xmm1, -8240(%rbp)
	movaps	%xmm7, -8304(%rbp)
	movaps	%xmm2, -8032(%rbp)
	movaps	%xmm4, -7984(%rbp)
	movaps	%xmm4, -240(%rbp)
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm0, -7664(%rbp)
	movq	$0, -7648(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm4
	movq	%r14, %rsi
	movdqa	-224(%rbp), %xmm5
	movdqa	-208(%rbp), %xmm6
	movdqa	-192(%rbp), %xmm2
	leaq	112(%rax), %rdx
	movq	%rax, -7664(%rbp)
	movdqa	-176(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm3
	movups	%xmm4, (%rax)
	movdqa	-144(%rbp), %xmm1
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm3, 80(%rax)
	movups	%xmm1, 96(%rax)
	leaq	-3888(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	movq	%rax, -8288(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1113
	call	_ZdlPv@PLT
.L1113:
	movdqa	-7984(%rbp), %xmm4
	movdqa	-8032(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movl	$112, %edi
	movdqa	-8240(%rbp), %xmm6
	movdqa	-8304(%rbp), %xmm2
	movaps	%xmm0, -7664(%rbp)
	movdqa	-8432(%rbp), %xmm7
	movdqa	-8544(%rbp), %xmm3
	movaps	%xmm4, -240(%rbp)
	movdqa	-8576(%rbp), %xmm1
	movaps	%xmm5, -224(%rbp)
	movaps	%xmm6, -208(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movq	$0, -7648(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm4
	movq	%r14, %rsi
	movdqa	-224(%rbp), %xmm5
	movdqa	-208(%rbp), %xmm6
	movdqa	-192(%rbp), %xmm2
	leaq	112(%rax), %rdx
	movq	%rax, -7664(%rbp)
	movdqa	-176(%rbp), %xmm7
	movdqa	-160(%rbp), %xmm3
	movups	%xmm4, (%rax)
	movdqa	-144(%rbp), %xmm1
	movups	%xmm5, 16(%rax)
	movups	%xmm6, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm7, 64(%rax)
	movups	%xmm3, 80(%rax)
	movups	%xmm1, 96(%rax)
	leaq	-3696(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	movq	%rax, -8304(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1114
	call	_ZdlPv@PLT
.L1114:
	movq	-8480(%rbp), %rcx
	movq	-8464(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -3824(%rbp)
	je	.L1115
.L1562:
	movq	-8464(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7808(%rbp)
	movq	$0, -7800(%rbp)
	movq	$0, -7792(%rbp)
	movq	$0, -7784(%rbp)
	movq	$0, -7776(%rbp)
	movq	$0, -7768(%rbp)
	movq	$0, -7760(%rbp)
	movq	$0, -7752(%rbp)
	movq	$0, -7744(%rbp)
	movq	$0, -7736(%rbp)
	movq	$0, -7728(%rbp)
	movq	$0, -7720(%rbp)
	movq	$0, -7712(%rbp)
	movq	$0, -7696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7696(%rbp), %rax
	movq	-8288(%rbp), %rdi
	pushq	%rax
	leaq	-7712(%rbp), %rax
	leaq	-7792(%rbp), %rcx
	pushq	%rax
	leaq	-7720(%rbp), %rax
	leaq	-7800(%rbp), %rdx
	pushq	%rax
	leaq	-7728(%rbp), %rax
	leaq	-7808(%rbp), %rsi
	pushq	%rax
	leaq	-7736(%rbp), %rax
	leaq	-7776(%rbp), %r9
	pushq	%rax
	leaq	-7744(%rbp), %rax
	leaq	-7784(%rbp), %r8
	pushq	%rax
	leaq	-7752(%rbp), %rax
	pushq	%rax
	leaq	-7760(%rbp), %rax
	pushq	%rax
	leaq	-7768(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	pxor	%xmm0, %xmm0
	addq	$80, %rsp
	movq	-7768(%rbp), %rbx
	movl	$8, %edi
	movaps	%xmm0, -7664(%rbp)
	movq	$0, -7648(%rbp)
	call	_Znwm@PLT
	leaq	-624(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -7664(%rbp)
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1116
	call	_ZdlPv@PLT
.L1116:
	movq	-7904(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1560:
	movq	-8256(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7808(%rbp)
	movq	$0, -7800(%rbp)
	movq	$0, -7792(%rbp)
	movq	$0, -7784(%rbp)
	movq	$0, -7776(%rbp)
	movq	$0, -7768(%rbp)
	movq	$0, -7760(%rbp)
	movq	$0, -7752(%rbp)
	movq	$0, -7744(%rbp)
	movq	$0, -7736(%rbp)
	movq	$0, -7728(%rbp)
	movq	$0, -7720(%rbp)
	movq	$0, -7712(%rbp)
	movq	$0, -7696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7696(%rbp), %rax
	movq	-8208(%rbp), %rdi
	pushq	%rax
	leaq	-7712(%rbp), %rax
	leaq	-7792(%rbp), %rcx
	pushq	%rax
	leaq	-7720(%rbp), %rax
	leaq	-7776(%rbp), %r9
	pushq	%rax
	leaq	-7728(%rbp), %rax
	leaq	-7784(%rbp), %r8
	pushq	%rax
	leaq	-7736(%rbp), %rax
	leaq	-7800(%rbp), %rdx
	pushq	%rax
	leaq	-7744(%rbp), %rax
	leaq	-7808(%rbp), %rsi
	pushq	%rax
	leaq	-7752(%rbp), %rax
	pushq	%rax
	leaq	-7760(%rbp), %rax
	pushq	%rax
	leaq	-7768(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	addq	$80, %rsp
	movl	$107, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$3093, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-240(%rbp), %rsi
	leaq	-112(%rbp), %rdx
	movq	%r14, %rdi
	movq	-7712(%rbp), %xmm0
	movq	-7728(%rbp), %xmm1
	movq	$0, -7648(%rbp)
	movq	-7744(%rbp), %xmm2
	movq	-7760(%rbp), %xmm3
	movhps	-7696(%rbp), %xmm0
	movhps	-7720(%rbp), %xmm1
	movq	-7776(%rbp), %xmm4
	movq	-7792(%rbp), %xmm5
	movaps	%xmm0, -144(%rbp)
	movhps	-7736(%rbp), %xmm2
	movq	-7768(%rbp), %xmm0
	movq	-7808(%rbp), %xmm6
	movhps	-7752(%rbp), %xmm3
	movhps	-7768(%rbp), %xmm4
	movhps	-7784(%rbp), %xmm5
	movaps	%xmm2, -176(%rbp)
	movhps	-7736(%rbp), %xmm0
	movhps	-7800(%rbp), %xmm6
	movaps	%xmm5, -224(%rbp)
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm6, -240(%rbp)
	movaps	%xmm4, -208(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm0, -7664(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8272(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1110
	call	_ZdlPv@PLT
.L1110:
	movq	-8432(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1109
	.p2align 4,,10
	.p2align 3
.L1563:
	movq	-8480(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7808(%rbp)
	leaq	-240(%rbp), %r13
	movq	$0, -7800(%rbp)
	leaq	-80(%rbp), %rbx
	movq	$0, -7792(%rbp)
	movq	$0, -7784(%rbp)
	movq	$0, -7776(%rbp)
	movq	$0, -7768(%rbp)
	movq	$0, -7760(%rbp)
	movq	$0, -7752(%rbp)
	movq	$0, -7744(%rbp)
	movq	$0, -7736(%rbp)
	movq	$0, -7728(%rbp)
	movq	$0, -7720(%rbp)
	movq	$0, -7712(%rbp)
	movq	$0, -7696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7696(%rbp), %rax
	movq	-8304(%rbp), %rdi
	pushq	%rax
	leaq	-7712(%rbp), %rax
	leaq	-7792(%rbp), %rcx
	pushq	%rax
	leaq	-7720(%rbp), %rax
	leaq	-7776(%rbp), %r9
	pushq	%rax
	leaq	-7728(%rbp), %rax
	leaq	-7784(%rbp), %r8
	pushq	%rax
	leaq	-7736(%rbp), %rax
	leaq	-7800(%rbp), %rdx
	pushq	%rax
	leaq	-7744(%rbp), %rax
	leaq	-7808(%rbp), %rsi
	pushq	%rax
	leaq	-7752(%rbp), %rax
	pushq	%rax
	leaq	-7760(%rbp), %rax
	pushq	%rax
	leaq	-7768(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	addq	$80, %rsp
	movl	$109, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$3133, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$3110, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-7728(%rbp), %xmm1
	movq	-7720(%rbp), %r12
	movq	-7768(%rbp), %xmm0
	movq	-7808(%rbp), %xmm5
	movq	-7712(%rbp), %xmm7
	movq	%r12, %xmm4
	movq	-7744(%rbp), %xmm2
	punpcklqdq	%xmm4, %xmm1
	movdqa	%xmm0, %xmm6
	movq	-7760(%rbp), %xmm4
	movq	-7776(%rbp), %xmm8
	movdqa	%xmm0, %xmm3
	punpcklqdq	%xmm0, %xmm5
	punpcklqdq	%xmm6, %xmm6
	movq	-7792(%rbp), %xmm9
	movq	-7808(%rbp), %xmm10
	movhps	-7808(%rbp), %xmm3
	movhps	-7736(%rbp), %xmm2
	movhps	-7752(%rbp), %xmm4
	punpcklqdq	%xmm0, %xmm8
	movhps	-7784(%rbp), %xmm9
	movhps	-7696(%rbp), %xmm7
	movaps	%xmm5, -8544(%rbp)
	movhps	-7800(%rbp), %xmm10
	pxor	%xmm0, %xmm0
	movaps	%xmm6, -8528(%rbp)
	movaps	%xmm3, -8480(%rbp)
	movaps	%xmm7, -8464(%rbp)
	movaps	%xmm1, -8432(%rbp)
	movaps	%xmm2, -8256(%rbp)
	movaps	%xmm4, -8240(%rbp)
	movaps	%xmm8, -8032(%rbp)
	movaps	%xmm9, -8016(%rbp)
	movaps	%xmm10, -7984(%rbp)
	movaps	%xmm10, -240(%rbp)
	movaps	%xmm9, -224(%rbp)
	movaps	%xmm8, -208(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm2, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm0, -7664(%rbp)
	movq	$0, -7648(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8216(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1118
	call	_ZdlPv@PLT
.L1118:
	movdqa	-7984(%rbp), %xmm5
	movdqa	-8016(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdx
	movdqa	-8032(%rbp), %xmm2
	movdqa	-8240(%rbp), %xmm7
	movq	%r13, %rsi
	movq	%r14, %rdi
	movdqa	-8256(%rbp), %xmm3
	movdqa	-8432(%rbp), %xmm1
	movaps	%xmm5, -240(%rbp)
	movdqa	-8464(%rbp), %xmm4
	movdqa	-8480(%rbp), %xmm5
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm2, -208(%rbp)
	movdqa	-8528(%rbp), %xmm6
	movdqa	-8544(%rbp), %xmm2
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm5, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm0, -7664(%rbp)
	movq	$0, -7648(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2928(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -8256(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1119
	call	_ZdlPv@PLT
.L1119:
	movq	-8400(%rbp), %rcx
	movq	-8336(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1117
	.p2align 4,,10
	.p2align 3
.L1564:
	movq	-8336(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$20, %edi
	movq	$0, -7648(%rbp)
	movaps	%xmm0, -7664(%rbp)
	call	_Znwm@PLT
	movdqa	.LC9(%rip), %xmm0
	movq	-8216(%rbp), %rdi
	movq	%r14, %rsi
	movl	$101123590, 16(%rax)
	leaq	20(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -7664(%rbp)
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7664(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1121
	call	_ZdlPv@PLT
.L1121:
	movq	(%rbx), %rax
	movq	8(%rax), %rbx
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	88(%rax), %r10
	movq	104(%rax), %r9
	movq	%rbx, -8016(%rbp)
	movq	80(%rax), %r11
	movq	(%rax), %rcx
	movq	%rsi, -8240(%rbp)
	movq	16(%rax), %rbx
	movq	32(%rax), %rsi
	movq	%rdx, -8432(%rbp)
	movq	120(%rax), %r8
	movq	48(%rax), %rdx
	movq	%rdi, -8480(%rbp)
	movq	%r10, -8552(%rbp)
	movq	64(%rax), %rdi
	movq	96(%rax), %r10
	movq	%r9, -8560(%rbp)
	movq	112(%rax), %r9
	movq	%r11, -8544(%rbp)
	movq	%r10, -8576(%rbp)
	movq	%r9, -8592(%rbp)
	movq	%rcx, -7984(%rbp)
	movq	%rbx, -8032(%rbp)
	movq	72(%rax), %rbx
	movq	%rsi, -8336(%rbp)
	leaq	.LC6(%rip), %rsi
	movq	%rdx, -8464(%rbp)
	movl	$3111, %edx
	movq	%rdi, -8528(%rbp)
	movq	%r15, %rdi
	movq	%r8, -8600(%rbp)
	movq	128(%rax), %r8
	movq	136(%rax), %r12
	movq	144(%rax), %r13
	movq	%r8, -8624(%rbp)
	movq	%r12, -8608(%rbp)
	movq	152(%rax), %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rcx
	movq	%r14, %r8
	movq	%r13, %rsi
	movq	-7896(%rbp), %rdi
	movq	%rbx, %rdx
	call	_ZN2v88internal39LoadElementNoHole16FixedDoubleArray_235EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm0
	movq	%r12, %xmm7
	movq	-8528(%rbp), %xmm4
	movq	%r13, %xmm6
	movq	%rbx, %xmm5
	movq	-8624(%rbp), %xmm3
	movq	-8576(%rbp), %xmm1
	punpcklqdq	%xmm0, %xmm4
	punpcklqdq	%xmm7, %xmm5
	movq	-8464(%rbp), %xmm0
	punpcklqdq	%xmm7, %xmm6
	movq	-8544(%rbp), %xmm2
	leaq	-7696(%rbp), %r12
	movq	-8592(%rbp), %xmm7
	leaq	-240(%rbp), %r13
	movq	-8336(%rbp), %xmm11
	leaq	-56(%rbp), %rdx
	movq	%r13, %rsi
	movq	-8032(%rbp), %xmm12
	movq	-7984(%rbp), %xmm13
	movhps	-8480(%rbp), %xmm0
	movhps	-8608(%rbp), %xmm3
	movhps	-8600(%rbp), %xmm7
	movhps	-8560(%rbp), %xmm1
	movhps	-8552(%rbp), %xmm2
	movhps	-8432(%rbp), %xmm11
	movaps	%xmm0, -8464(%rbp)
	movhps	-8240(%rbp), %xmm12
	movhps	-8016(%rbp), %xmm13
	movq	%r12, %rdi
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -8656(%rbp)
	movaps	%xmm6, -8640(%rbp)
	movaps	%xmm3, -8624(%rbp)
	movaps	%xmm7, -8592(%rbp)
	movaps	%xmm1, -8576(%rbp)
	movaps	%xmm2, -8544(%rbp)
	movaps	%xmm4, -8528(%rbp)
	movaps	%xmm11, -8336(%rbp)
	movaps	%xmm12, -8032(%rbp)
	movaps	%xmm13, -7984(%rbp)
	movaps	%xmm13, -240(%rbp)
	movaps	%xmm12, -224(%rbp)
	movaps	%xmm11, -208(%rbp)
	movaps	%xmm4, -176(%rbp)
	movq	%rax, -64(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm0, -7696(%rbp)
	movq	$0, -7680(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-3120(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -8240(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7696(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1122
	call	_ZdlPv@PLT
.L1122:
	movq	-8352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-3312(%rbp), %rax
	cmpq	$0, -7656(%rbp)
	movq	%rax, -8016(%rbp)
	jne	.L1583
.L1123:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L1565:
	movq	-8496(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7872(%rbp)
	movq	$0, -7864(%rbp)
	movq	$0, -7856(%rbp)
	movq	$0, -7848(%rbp)
	movq	$0, -7840(%rbp)
	movq	$0, -7832(%rbp)
	movq	$0, -7824(%rbp)
	movq	$0, -7816(%rbp)
	movq	$0, -7808(%rbp)
	movq	$0, -7800(%rbp)
	movq	$0, -7792(%rbp)
	movq	$0, -7784(%rbp)
	movq	$0, -7776(%rbp)
	movq	$0, -7768(%rbp)
	movq	$0, -7760(%rbp)
	movq	$0, -7752(%rbp)
	movq	$0, -7744(%rbp)
	movq	$0, -7736(%rbp)
	movq	$0, -7728(%rbp)
	movq	$0, -7720(%rbp)
	movq	$0, -7712(%rbp)
	movq	$0, -7696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7696(%rbp), %rax
	movq	-8016(%rbp), %rdi
	pushq	%rax
	leaq	-7712(%rbp), %rax
	leaq	-7856(%rbp), %rcx
	pushq	%rax
	leaq	-7720(%rbp), %rax
	leaq	-7840(%rbp), %r9
	pushq	%rax
	leaq	-7728(%rbp), %rax
	leaq	-7848(%rbp), %r8
	pushq	%rax
	leaq	-7736(%rbp), %rax
	leaq	-7864(%rbp), %rdx
	pushq	%rax
	leaq	-7744(%rbp), %rax
	leaq	-7872(%rbp), %rsi
	pushq	%rax
	leaq	-7752(%rbp), %rax
	pushq	%rax
	leaq	-7760(%rbp), %rax
	pushq	%rax
	leaq	-7768(%rbp), %rax
	pushq	%rax
	leaq	-7776(%rbp), %rax
	pushq	%rax
	leaq	-7784(%rbp), %rax
	pushq	%rax
	leaq	-7792(%rbp), %rax
	pushq	%rax
	leaq	-7800(%rbp), %rax
	pushq	%rax
	leaq	-7808(%rbp), %rax
	pushq	%rax
	leaq	-7816(%rbp), %rax
	pushq	%rax
	leaq	-7824(%rbp), %rax
	pushq	%rax
	leaq	-7832(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_S6_S3_S6_SA_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SO_SG_SO_SQ_SO_
	pxor	%xmm0, %xmm0
	leaq	-104(%rbp), %rdx
	movq	%r14, %rdi
	movq	-7872(%rbp), %rax
	addq	$144, %rsp
	leaq	-240(%rbp), %rsi
	movq	%rax, -240(%rbp)
	movq	-7864(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	-7856(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-7848(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-7840(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-7832(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-7824(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-7816(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-7808(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-7800(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-7792(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-7784(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-7776(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-7768(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-7760(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-7752(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-7744(%rbp), %rax
	movaps	%xmm0, -7664(%rbp)
	movq	%rax, -112(%rbp)
	movq	$0, -7648(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7984(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1126
	call	_ZdlPv@PLT
.L1126:
	movq	-8320(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -3056(%rbp)
	je	.L1127
.L1566:
	movq	-8352(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7880(%rbp)
	movq	$0, -7872(%rbp)
	movq	$0, -7864(%rbp)
	movq	$0, -7856(%rbp)
	movq	$0, -7848(%rbp)
	movq	$0, -7840(%rbp)
	movq	$0, -7832(%rbp)
	movq	$0, -7824(%rbp)
	movq	$0, -7816(%rbp)
	movq	$0, -7808(%rbp)
	movq	$0, -7800(%rbp)
	movq	$0, -7792(%rbp)
	movq	$0, -7784(%rbp)
	movq	$0, -7776(%rbp)
	movq	$0, -7768(%rbp)
	movq	$0, -7760(%rbp)
	movq	$0, -7752(%rbp)
	movq	$0, -7744(%rbp)
	movq	$0, -7736(%rbp)
	movq	$0, -7728(%rbp)
	movq	$0, -7720(%rbp)
	movq	$0, -7712(%rbp)
	movq	$0, -7696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7696(%rbp), %rax
	movq	-8240(%rbp), %rdi
	leaq	-7864(%rbp), %rcx
	pushq	%rax
	leaq	-7712(%rbp), %rax
	leaq	-7848(%rbp), %r9
	pushq	%rax
	leaq	-7720(%rbp), %rax
	leaq	-7856(%rbp), %r8
	pushq	%rax
	leaq	-7728(%rbp), %rax
	leaq	-7872(%rbp), %rdx
	pushq	%rax
	leaq	-7736(%rbp), %rax
	leaq	-7880(%rbp), %rsi
	pushq	%rax
	leaq	-7744(%rbp), %rax
	pushq	%rax
	leaq	-7752(%rbp), %rax
	pushq	%rax
	leaq	-7760(%rbp), %rax
	pushq	%rax
	leaq	-7768(%rbp), %rax
	pushq	%rax
	leaq	-7776(%rbp), %rax
	pushq	%rax
	leaq	-7784(%rbp), %rax
	pushq	%rax
	leaq	-7792(%rbp), %rax
	pushq	%rax
	leaq	-7800(%rbp), %rax
	pushq	%rax
	leaq	-7808(%rbp), %rax
	pushq	%rax
	leaq	-7816(%rbp), %rax
	pushq	%rax
	leaq	-7824(%rbp), %rax
	pushq	%rax
	leaq	-7832(%rbp), %rax
	pushq	%rax
	leaq	-7840(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SO_SG_SO_SQ_SO_SM_
	pxor	%xmm0, %xmm0
	leaq	-72(%rbp), %rdx
	movq	%r14, %rdi
	movq	-7880(%rbp), %rax
	addq	$144, %rsp
	leaq	-240(%rbp), %rsi
	movq	%rax, -240(%rbp)
	movq	-7872(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	-7864(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-7856(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-7848(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-7840(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-7832(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-7824(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-7816(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-7808(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-7800(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-7792(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-7784(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-7776(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-7768(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-7760(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-7752(%rbp), %rax
	movaps	%xmm0, -7664(%rbp)
	movq	%rax, -112(%rbp)
	movq	-7744(%rbp), %rax
	movq	$0, -7648(%rbp)
	movq	%rax, -104(%rbp)
	movq	-7736(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-7728(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-7696(%rbp), %rax
	movq	%rax, -80(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2352(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1128
	call	_ZdlPv@PLT
.L1128:
	movq	-7920(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1567:
	movq	-8400(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$20, %edi
	movq	$0, -7648(%rbp)
	movaps	%xmm0, -7664(%rbp)
	call	_Znwm@PLT
	movdqa	.LC9(%rip), %xmm0
	movq	-8256(%rbp), %rdi
	movq	%r14, %rsi
	movl	$101123590, 16(%rax)
	leaq	20(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rax, -7664(%rbp)
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7664(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1130
	call	_ZdlPv@PLT
.L1130:
	movq	(%rbx), %rax
	movq	8(%rax), %rbx
	movq	24(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rdi
	movq	88(%rax), %r10
	movq	104(%rax), %r9
	movq	%rbx, -8336(%rbp)
	movq	80(%rax), %r11
	movq	(%rax), %rcx
	movq	%rsi, -8400(%rbp)
	movq	16(%rax), %rbx
	movq	32(%rax), %rsi
	movq	%rdx, -8464(%rbp)
	movq	120(%rax), %r8
	movq	48(%rax), %rdx
	movq	%rdi, -8496(%rbp)
	movq	%r10, -8552(%rbp)
	movq	64(%rax), %rdi
	movq	96(%rax), %r10
	movq	%r9, -8560(%rbp)
	movq	112(%rax), %r9
	movq	%r11, -8544(%rbp)
	movq	%r10, -8576(%rbp)
	movq	%r9, -8592(%rbp)
	movq	%rcx, -8032(%rbp)
	movq	%rbx, -8352(%rbp)
	movq	72(%rax), %rbx
	movq	%rsi, -8432(%rbp)
	leaq	.LC6(%rip), %rsi
	movq	%rdx, -8480(%rbp)
	movl	$3114, %edx
	movq	%rdi, -8528(%rbp)
	movq	%r15, %rdi
	movq	%r8, -8600(%rbp)
	movq	128(%rax), %r8
	movq	136(%rax), %r12
	movq	144(%rax), %r13
	movq	%r8, -8624(%rbp)
	movq	%r12, -8608(%rbp)
	movq	152(%rax), %r12
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r12, %rcx
	movq	%r14, %r8
	movq	%r13, %rsi
	movq	-7896(%rbp), %rdi
	movq	%rbx, %rdx
	call	_ZN2v88internal33LoadElementNoHole10FixedArray_234EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_7JSArrayEEENS4_INS0_3SmiEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm0
	movq	%r12, %xmm1
	movq	-8528(%rbp), %xmm4
	movq	%r13, %xmm6
	movq	%rbx, %xmm5
	movq	-8624(%rbp), %xmm3
	movq	-8592(%rbp), %xmm7
	punpcklqdq	%xmm0, %xmm4
	punpcklqdq	%xmm1, %xmm5
	movq	-8480(%rbp), %xmm0
	punpcklqdq	%xmm1, %xmm6
	movq	-8544(%rbp), %xmm2
	leaq	-7696(%rbp), %r12
	movq	-8576(%rbp), %xmm1
	leaq	-240(%rbp), %r13
	movq	-8432(%rbp), %xmm14
	leaq	-56(%rbp), %rdx
	movq	%r13, %rsi
	movq	-8352(%rbp), %xmm15
	movq	-8032(%rbp), %xmm8
	movhps	-8496(%rbp), %xmm0
	movhps	-8608(%rbp), %xmm3
	movhps	-8600(%rbp), %xmm7
	movhps	-8560(%rbp), %xmm1
	movhps	-8552(%rbp), %xmm2
	movhps	-8464(%rbp), %xmm14
	movaps	%xmm0, -8480(%rbp)
	movhps	-8400(%rbp), %xmm15
	movhps	-8336(%rbp), %xmm8
	movq	%r12, %rdi
	movaps	%xmm0, -192(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -8656(%rbp)
	movaps	%xmm6, -8640(%rbp)
	movaps	%xmm3, -8624(%rbp)
	movaps	%xmm7, -8592(%rbp)
	movaps	%xmm1, -8576(%rbp)
	movaps	%xmm2, -8544(%rbp)
	movaps	%xmm4, -8528(%rbp)
	movaps	%xmm14, -8432(%rbp)
	movaps	%xmm15, -8400(%rbp)
	movaps	%xmm8, -8352(%rbp)
	movaps	%xmm8, -240(%rbp)
	movaps	%xmm15, -224(%rbp)
	movaps	%xmm14, -208(%rbp)
	movaps	%xmm4, -176(%rbp)
	movq	%rax, -64(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm0, -7696(%rbp)
	movq	$0, -7680(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2544(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -8336(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7696(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1131
	call	_ZdlPv@PLT
.L1131:
	movq	-8416(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-2736(%rbp), %rax
	cmpq	$0, -7656(%rbp)
	movq	%rax, -8032(%rbp)
	jne	.L1584
.L1132:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2672(%rbp)
	je	.L1134
.L1568:
	movq	-8384(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7872(%rbp)
	movq	$0, -7864(%rbp)
	movq	$0, -7856(%rbp)
	movq	$0, -7848(%rbp)
	movq	$0, -7840(%rbp)
	movq	$0, -7832(%rbp)
	movq	$0, -7824(%rbp)
	movq	$0, -7816(%rbp)
	movq	$0, -7808(%rbp)
	movq	$0, -7800(%rbp)
	movq	$0, -7792(%rbp)
	movq	$0, -7784(%rbp)
	movq	$0, -7776(%rbp)
	movq	$0, -7768(%rbp)
	movq	$0, -7760(%rbp)
	movq	$0, -7752(%rbp)
	movq	$0, -7744(%rbp)
	movq	$0, -7736(%rbp)
	movq	$0, -7728(%rbp)
	movq	$0, -7720(%rbp)
	movq	$0, -7712(%rbp)
	movq	$0, -7696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	leaq	-7696(%rbp), %rax
	movq	-8032(%rbp), %rdi
	pushq	%rax
	leaq	-7712(%rbp), %rax
	leaq	-7856(%rbp), %rcx
	pushq	%rax
	leaq	-7720(%rbp), %rax
	leaq	-7840(%rbp), %r9
	pushq	%rax
	leaq	-7728(%rbp), %rax
	leaq	-7848(%rbp), %r8
	pushq	%rax
	leaq	-7736(%rbp), %rax
	leaq	-7864(%rbp), %rdx
	pushq	%rax
	leaq	-7744(%rbp), %rax
	leaq	-7872(%rbp), %rsi
	pushq	%rax
	leaq	-7752(%rbp), %rax
	pushq	%rax
	leaq	-7760(%rbp), %rax
	pushq	%rax
	leaq	-7768(%rbp), %rax
	pushq	%rax
	leaq	-7776(%rbp), %rax
	pushq	%rax
	leaq	-7784(%rbp), %rax
	pushq	%rax
	leaq	-7792(%rbp), %rax
	pushq	%rax
	leaq	-7800(%rbp), %rax
	pushq	%rax
	leaq	-7808(%rbp), %rax
	pushq	%rax
	leaq	-7816(%rbp), %rax
	pushq	%rax
	leaq	-7824(%rbp), %rax
	pushq	%rax
	leaq	-7832(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_S6_S3_S6_SA_S6_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SO_SG_SO_SQ_SO_
	pxor	%xmm0, %xmm0
	leaq	-104(%rbp), %rdx
	movq	%r14, %rdi
	movq	-7872(%rbp), %rax
	addq	$144, %rsp
	leaq	-240(%rbp), %rsi
	movq	%rax, -240(%rbp)
	movq	-7864(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	-7856(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-7848(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-7840(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-7832(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-7824(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-7816(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-7808(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-7800(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-7792(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-7784(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-7776(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-7768(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-7760(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-7752(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-7744(%rbp), %rax
	movaps	%xmm0, -7664(%rbp)
	movq	%rax, -112(%rbp)
	movq	$0, -7648(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7984(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1135
	call	_ZdlPv@PLT
.L1135:
	movq	-8320(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2480(%rbp)
	je	.L1136
.L1569:
	movq	-8416(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7880(%rbp)
	movq	$0, -7872(%rbp)
	movq	$0, -7864(%rbp)
	movq	$0, -7856(%rbp)
	movq	$0, -7848(%rbp)
	movq	$0, -7840(%rbp)
	movq	$0, -7832(%rbp)
	movq	$0, -7824(%rbp)
	movq	$0, -7816(%rbp)
	movq	$0, -7808(%rbp)
	movq	$0, -7800(%rbp)
	movq	$0, -7792(%rbp)
	movq	$0, -7784(%rbp)
	movq	$0, -7776(%rbp)
	movq	$0, -7768(%rbp)
	movq	$0, -7760(%rbp)
	movq	$0, -7752(%rbp)
	movq	$0, -7744(%rbp)
	movq	$0, -7736(%rbp)
	movq	$0, -7728(%rbp)
	movq	$0, -7720(%rbp)
	movq	$0, -7712(%rbp)
	movq	$0, -7696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	leaq	-7696(%rbp), %rax
	movq	-8336(%rbp), %rdi
	leaq	-7864(%rbp), %rcx
	pushq	%rax
	leaq	-7712(%rbp), %rax
	leaq	-7848(%rbp), %r9
	pushq	%rax
	leaq	-7720(%rbp), %rax
	leaq	-7856(%rbp), %r8
	pushq	%rax
	leaq	-7728(%rbp), %rax
	leaq	-7872(%rbp), %rdx
	pushq	%rax
	leaq	-7736(%rbp), %rax
	leaq	-7880(%rbp), %rsi
	pushq	%rax
	leaq	-7744(%rbp), %rax
	pushq	%rax
	leaq	-7752(%rbp), %rax
	pushq	%rax
	leaq	-7760(%rbp), %rax
	pushq	%rax
	leaq	-7768(%rbp), %rax
	pushq	%rax
	leaq	-7776(%rbp), %rax
	pushq	%rax
	leaq	-7784(%rbp), %rax
	pushq	%rax
	leaq	-7792(%rbp), %rax
	pushq	%rax
	leaq	-7800(%rbp), %rax
	pushq	%rax
	leaq	-7808(%rbp), %rax
	pushq	%rax
	leaq	-7816(%rbp), %rax
	pushq	%rax
	leaq	-7824(%rbp), %rax
	pushq	%rax
	leaq	-7832(%rbp), %rax
	pushq	%rax
	leaq	-7840(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_S6_S3_S6_S6_S3_S6_SA_S6_S9_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_SO_SG_SO_SO_SG_SO_SQ_SO_SM_
	pxor	%xmm0, %xmm0
	leaq	-72(%rbp), %rdx
	movq	%r14, %rdi
	movq	-7880(%rbp), %rax
	addq	$144, %rsp
	leaq	-240(%rbp), %rsi
	movq	%rax, -240(%rbp)
	movq	-7872(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	-7864(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-7856(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-7848(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-7840(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-7832(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-7824(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-7816(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-7808(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-7800(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-7792(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-7784(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-7776(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-7768(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-7760(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-7752(%rbp), %rax
	movaps	%xmm0, -7664(%rbp)
	movq	%rax, -112(%rbp)
	movq	-7744(%rbp), %rax
	movq	$0, -7648(%rbp)
	movq	%rax, -104(%rbp)
	movq	-7736(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-7728(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-7696(%rbp), %rax
	movq	%rax, -80(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2352(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1137
	call	_ZdlPv@PLT
.L1137:
	movq	-7920(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2288(%rbp)
	je	.L1138
.L1570:
	movq	-7920(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-240(%rbp), %r13
	leaq	-2352(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC9(%rip), %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-219(%rbp), %rdx
	movl	$101123590, -224(%rbp)
	movaps	%xmm0, -240(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7664(%rbp)
	movb	$8, -220(%rbp)
	movq	$0, -7648(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7664(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1139
	call	_ZdlPv@PLT
.L1139:
	movq	(%rbx), %rax
	movq	40(%rax), %rdx
	movq	8(%rax), %rbx
	movq	24(%rax), %rsi
	movq	88(%rax), %rdi
	movq	104(%rax), %r11
	movq	120(%rax), %r10
	movq	%rdx, -8480(%rbp)
	movq	56(%rax), %rdx
	movq	(%rax), %rcx
	movq	%rbx, -8400(%rbp)
	movq	%rsi, -8432(%rbp)
	movq	16(%rax), %rbx
	movq	%rdx, -8496(%rbp)
	movq	72(%rax), %rdx
	movq	32(%rax), %rsi
	movq	%rdi, -8544(%rbp)
	movq	%rdx, -8384(%rbp)
	movq	96(%rax), %rdi
	movq	80(%rax), %rdx
	movq	%r11, -8576(%rbp)
	movq	112(%rax), %r11
	movq	48(%rax), %r12
	movq	%r10, -8592(%rbp)
	movq	128(%rax), %r10
	movq	%rcx, -8352(%rbp)
	movq	%r11, -8560(%rbp)
	movq	%rbx, -8416(%rbp)
	movq	64(%rax), %rbx
	movq	%rsi, -8464(%rbp)
	leaq	.LC6(%rip), %rsi
	movq	%rdx, -8528(%rbp)
	movl	$3133, %edx
	movq	%rdi, -8552(%rbp)
	movq	%r15, %rdi
	movq	%r10, -8600(%rbp)
	movq	160(%rax), %rax
	movq	%rax, -8624(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-96(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-8352(%rbp), %xmm0
	movq	$0, -7648(%rbp)
	movhps	-8400(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-8416(%rbp), %xmm0
	movhps	-8432(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-8464(%rbp), %xmm0
	movhps	-8480(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	%r12, %xmm0
	movhps	-8496(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	%rbx, %xmm0
	movhps	-8384(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-8528(%rbp), %xmm0
	movhps	-8544(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-8552(%rbp), %xmm0
	movhps	-8576(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-8560(%rbp), %xmm0
	movhps	-8592(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-8600(%rbp), %xmm0
	movhps	-8624(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7664(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1968(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1140
	call	_ZdlPv@PLT
.L1140:
	movq	-7936(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2096(%rbp)
	je	.L1141
.L1571:
	movq	-8320(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-240(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC9(%rip), %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-223(%rbp), %rdx
	movb	$6, -224(%rbp)
	movaps	%xmm0, -240(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7664(%rbp)
	movq	$0, -7648(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-7984(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7664(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1142
	call	_ZdlPv@PLT
.L1142:
	movq	(%rbx), %rax
	movq	40(%rax), %rdx
	movq	24(%rax), %rsi
	movq	72(%rax), %rdi
	movq	8(%rax), %rbx
	movq	88(%rax), %r11
	movq	%rdx, -8464(%rbp)
	movq	56(%rax), %rdx
	movq	104(%rax), %r10
	movq	%rsi, -8416(%rbp)
	movq	32(%rax), %rsi
	movq	(%rax), %rcx
	movq	%rdi, -8384(%rbp)
	movq	80(%rax), %rdi
	movq	%rdx, -8480(%rbp)
	movq	64(%rax), %rdx
	movq	120(%rax), %r9
	movq	%rbx, -8352(%rbp)
	movq	%r11, -8544(%rbp)
	movq	16(%rax), %rbx
	movq	96(%rax), %r11
	movq	%r10, -8576(%rbp)
	movq	112(%rax), %r10
	movq	%rsi, -8432(%rbp)
	leaq	.LC6(%rip), %rsi
	movq	%rdx, -8496(%rbp)
	movl	$3136, %edx
	movq	48(%rax), %r12
	movq	%rdi, -8528(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -8320(%rbp)
	movq	%r11, -8552(%rbp)
	movq	%r10, -8560(%rbp)
	movq	%r9, -8592(%rbp)
	movq	%rbx, -8400(%rbp)
	movq	128(%rax), %rbx
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7896(%rbp), %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	leaq	-96(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-8320(%rbp), %xmm0
	movq	%rax, -104(%rbp)
	movq	%rbx, -112(%rbp)
	movhps	-8352(%rbp), %xmm0
	movq	$0, -7648(%rbp)
	movaps	%xmm0, -240(%rbp)
	movq	-8400(%rbp), %xmm0
	movhps	-8416(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-8432(%rbp), %xmm0
	movhps	-8464(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	%r12, %xmm0
	movhps	-8480(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-8496(%rbp), %xmm0
	movhps	-8384(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-8528(%rbp), %xmm0
	movhps	-8544(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-8552(%rbp), %xmm0
	movhps	-8576(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-8560(%rbp), %xmm0
	movhps	-8592(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7664(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1968(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1143
	call	_ZdlPv@PLT
.L1143:
	movq	-7936(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1141
	.p2align 4,,10
	.p2align 3
.L1574:
	movq	-8120(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$578716967411058439, %rbx
	leaq	-1584(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	$0, -7648(%rbp)
	movaps	%xmm0, -7664(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	movabsq	$506098639673231111, %rcx
	movq	%rbx, 8(%rax)
	leaq	16(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%rax, -7664(%rbp)
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7664(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1155
	call	_ZdlPv@PLT
.L1155:
	movq	(%rbx), %rax
	movl	$113, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r13
	movq	16(%rax), %r12
	movq	32(%rax), %rbx
	movq	%rcx, -8400(%rbp)
	movq	24(%rax), %rcx
	movq	40(%rax), %rax
	movq	%rcx, -8416(%rbp)
	movq	%rax, -8432(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %xmm0
	leaq	-240(%rbp), %rsi
	movq	%r14, %rdi
	movhps	-8400(%rbp), %xmm0
	leaq	-192(%rbp), %rdx
	movq	$0, -7648(%rbp)
	movaps	%xmm0, -240(%rbp)
	movq	%r12, %xmm0
	movhps	-8416(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	%rbx, %xmm0
	movhps	-8432(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7664(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-816(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1156
	call	_ZdlPv@PLT
.L1156:
	movq	-7960(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1328(%rbp)
	je	.L1157
.L1575:
	movq	-8376(%rbp), %rsi
	movq	%r15, %rdi
	movabsq	$578716967411058439, %rbx
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$16, %edi
	movq	$0, -7648(%rbp)
	movaps	%xmm0, -7664(%rbp)
	call	_Znwm@PLT
	movq	-8320(%rbp), %rdi
	movq	%r14, %rsi
	movabsq	$506098639673231111, %rcx
	movq	%rbx, 8(%rax)
	leaq	16(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%rax, -7664(%rbp)
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7664(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1158
	call	_ZdlPv@PLT
.L1158:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rsi
	movq	8(%rax), %rbx
	movq	88(%rax), %rdx
	movq	(%rax), %rcx
	movq	32(%rax), %r13
	movq	48(%rax), %r12
	movq	%rsi, -8432(%rbp)
	movq	40(%rax), %rsi
	movq	%rbx, -8400(%rbp)
	movq	16(%rax), %rbx
	movq	%rdx, -8384(%rbp)
	movq	%rsi, -8448(%rbp)
	movq	56(%rax), %rsi
	movq	96(%rax), %rdx
	movq	%rbx, -8416(%rbp)
	movq	%rsi, -8464(%rbp)
	movq	72(%rax), %rsi
	movq	64(%rax), %rbx
	movq	%rdx, -8528(%rbp)
	movl	$103, %edx
	movq	%rsi, -8480(%rbp)
	movq	80(%rax), %rsi
	movq	104(%rax), %rax
	movq	%rcx, -8376(%rbp)
	movq	%rsi, -8496(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rax, -8544(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$112, %edi
	movq	-8376(%rbp), %xmm0
	movq	$0, -7648(%rbp)
	movhps	-8400(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-8416(%rbp), %xmm0
	movhps	-8432(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	%r13, %xmm0
	movhps	-8448(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	%r12, %xmm0
	movhps	-8464(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	%rbx, %xmm0
	movhps	-8480(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-8496(%rbp), %xmm0
	movhps	-8384(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-8528(%rbp), %xmm0
	movhps	-8544(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7664(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm2
	movq	%r14, %rsi
	movdqa	-224(%rbp), %xmm7
	movdqa	-208(%rbp), %xmm3
	movdqa	-192(%rbp), %xmm1
	leaq	112(%rax), %rdx
	leaq	-1200(%rbp), %rdi
	movdqa	-176(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm5
	movups	%xmm2, (%rax)
	movdqa	-144(%rbp), %xmm6
	movups	%xmm7, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm1, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm5, 80(%rax)
	movups	%xmm6, 96(%rax)
	movq	%rax, -7664(%rbp)
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1159
	call	_ZdlPv@PLT
.L1159:
	movq	-8128(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1136(%rbp)
	je	.L1160
.L1576:
	movq	-8128(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7808(%rbp)
	leaq	-1200(%rbp), %r12
	movq	$0, -7800(%rbp)
	movq	$0, -7792(%rbp)
	movq	$0, -7784(%rbp)
	movq	$0, -7776(%rbp)
	movq	$0, -7768(%rbp)
	movq	$0, -7760(%rbp)
	movq	$0, -7752(%rbp)
	movq	$0, -7744(%rbp)
	movq	$0, -7736(%rbp)
	movq	$0, -7728(%rbp)
	movq	$0, -7720(%rbp)
	movq	$0, -7712(%rbp)
	movq	$0, -7696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-7696(%rbp), %rax
	pushq	%rax
	leaq	-7712(%rbp), %rax
	leaq	-7792(%rbp), %rcx
	pushq	%rax
	leaq	-7720(%rbp), %rax
	leaq	-7776(%rbp), %r9
	pushq	%rax
	leaq	-7728(%rbp), %rax
	leaq	-7784(%rbp), %r8
	pushq	%rax
	leaq	-7736(%rbp), %rax
	leaq	-7800(%rbp), %rdx
	pushq	%rax
	leaq	-7744(%rbp), %rax
	leaq	-7808(%rbp), %rsi
	pushq	%rax
	leaq	-7752(%rbp), %rax
	pushq	%rax
	leaq	-7760(%rbp), %rax
	pushq	%rax
	leaq	-7768(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	movq	-7896(%rbp), %rbx
	addq	$80, %rsp
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal42FromConstexpr5ATSmi17ATconstexpr_int31_152EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-7768(%rbp), %r13
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastTaggedSignedToWordENS1_11SloppyTNodeINS0_3SmiEEE@PLT
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler9IntPtrAddENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler25BitcastWordToTaggedSignedENS1_11SloppyTNodeINS0_5WordTEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-7776(%rbp), %rax
	movq	-7792(%rbp), %xmm0
	movl	$112, %edi
	movq	-7808(%rbp), %xmm1
	movq	%rbx, -200(%rbp)
	movhps	-7784(%rbp), %xmm0
	movq	%rax, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movq	-7760(%rbp), %xmm0
	movhps	-7800(%rbp), %xmm1
	movaps	%xmm1, -240(%rbp)
	movhps	-7752(%rbp), %xmm0
	movq	$0, -7648(%rbp)
	movaps	%xmm0, -192(%rbp)
	movq	-7744(%rbp), %xmm0
	movhps	-7736(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-7728(%rbp), %xmm0
	movhps	-7720(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-7712(%rbp), %xmm0
	movhps	-7696(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7664(%rbp)
	call	_Znwm@PLT
	movdqa	-240(%rbp), %xmm2
	movq	%r14, %rsi
	movdqa	-224(%rbp), %xmm7
	movdqa	-208(%rbp), %xmm3
	movdqa	-192(%rbp), %xmm1
	leaq	112(%rax), %rdx
	movq	%rax, -7664(%rbp)
	movdqa	-176(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm5
	movups	%xmm2, (%rax)
	movdqa	-144(%rbp), %xmm6
	movq	-7952(%rbp), %rdi
	movups	%xmm7, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm1, 48(%rax)
	movups	%xmm4, 64(%rax)
	movups	%xmm5, 80(%rax)
	movups	%xmm6, 96(%rax)
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1161
	call	_ZdlPv@PLT
.L1161:
	movq	-8368(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -944(%rbp)
	je	.L1162
.L1577:
	movq	-8088(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, -7808(%rbp)
	leaq	-1008(%rbp), %r12
	movq	$0, -7800(%rbp)
	movq	$0, -7792(%rbp)
	movq	$0, -7784(%rbp)
	movq	$0, -7776(%rbp)
	movq	$0, -7768(%rbp)
	movq	$0, -7760(%rbp)
	movq	$0, -7752(%rbp)
	movq	$0, -7744(%rbp)
	movq	$0, -7736(%rbp)
	movq	$0, -7728(%rbp)
	movq	$0, -7720(%rbp)
	movq	$0, -7712(%rbp)
	movq	$0, -7696(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	subq	$8, %rsp
	movq	%r12, %rdi
	leaq	-7696(%rbp), %rax
	pushq	%rax
	leaq	-7712(%rbp), %rax
	leaq	-7792(%rbp), %rcx
	pushq	%rax
	leaq	-7720(%rbp), %rax
	leaq	-7776(%rbp), %r9
	pushq	%rax
	leaq	-7728(%rbp), %rax
	leaq	-7784(%rbp), %r8
	pushq	%rax
	leaq	-7736(%rbp), %rax
	leaq	-7800(%rbp), %rdx
	pushq	%rax
	leaq	-7744(%rbp), %rax
	leaq	-7808(%rbp), %rsi
	pushq	%rax
	leaq	-7752(%rbp), %rax
	pushq	%rax
	leaq	-7760(%rbp), %rax
	pushq	%rax
	leaq	-7768(%rbp), %rax
	pushq	%rax
	call	_ZN2v88internal8compiler31CodeAssemblerParameterizedLabelIJNS0_7ContextENS0_10JSReceiverENS0_6UnionTINS0_3SmiENS0_10HeapNumberEEES4_NS0_6ObjectES6_S6_NS0_7JSArrayESA_SA_NS0_3MapENS0_5BoolTESC_SC_EE10CreatePhisEPNS1_5TNodeIS3_EEPNSE_IS4_EEPNSE_IS8_EESI_PNSE_IS9_EEPNSE_IS6_EESO_PNSE_ISA_EESQ_SQ_PNSE_ISB_EEPNSE_ISC_EESU_SU_
	addq	$80, %rsp
	movl	$116, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7896(%rbp), %rdi
	movl	$-1, %esi
	call	_ZN2v88internal58FromConstexpr20UT5ATSmi10HeapNumber17ATconstexpr_int31_158EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	leaq	-240(%rbp), %rsi
	leaq	-192(%rbp), %rdx
	movq	%r14, %rdi
	movq	-7776(%rbp), %xmm0
	movq	%rax, %xmm2
	movq	-7792(%rbp), %xmm1
	movq	$0, -7648(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movhps	-7784(%rbp), %xmm1
	movq	-7808(%rbp), %xmm2
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movhps	-7800(%rbp), %xmm2
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm2, -240(%rbp)
	movaps	%xmm0, -7664(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-816(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1163
	call	_ZdlPv@PLT
.L1163:
	movq	-7960(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1162
	.p2align 4,,10
	.p2align 3
.L1573:
	movq	-8448(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-240(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC10(%rip), %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	-219(%rbp), %rdx
	movl	$101189639, -224(%rbp)
	movaps	%xmm0, -240(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7664(%rbp)
	movb	$7, -220(%rbp)
	movq	$0, -7648(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8352(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7664(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1148
	call	_ZdlPv@PLT
.L1148:
	movq	(%rbx), %rax
	movl	$111, %edx
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rsi
	leaq	-7712(%rbp), %r12
	movq	(%rax), %rcx
	movq	%rcx, -8320(%rbp)
	movq	8(%rax), %rcx
	movq	%rcx, -8400(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -8416(%rbp)
	movq	24(%rax), %rcx
	movq	%rcx, -8432(%rbp)
	movq	32(%rax), %rcx
	movq	%rcx, -8448(%rbp)
	movq	40(%rax), %rcx
	movq	%rcx, -8464(%rbp)
	movq	48(%rax), %rcx
	movq	%rcx, -8480(%rbp)
	movq	56(%rax), %rcx
	movq	%rcx, -8496(%rbp)
	movq	64(%rax), %rcx
	movq	%rcx, -8384(%rbp)
	movq	72(%rax), %rcx
	movq	%rcx, -8528(%rbp)
	movq	80(%rax), %rcx
	movq	%rcx, -8544(%rbp)
	movq	88(%rax), %rcx
	movq	%rcx, -8552(%rbp)
	movq	96(%rax), %rcx
	movq	%rcx, -8576(%rbp)
	movq	104(%rax), %rcx
	movq	%rcx, -8560(%rbp)
	movq	112(%rax), %rcx
	movq	%rcx, -8592(%rbp)
	movq	120(%rax), %rcx
	movq	%rcx, -8600(%rbp)
	movq	128(%rax), %rcx
	movq	136(%rax), %rbx
	movq	%rcx, -8624(%rbp)
	movq	144(%rax), %rcx
	movq	%rcx, -8608(%rbp)
	movq	152(%rax), %rcx
	movq	160(%rax), %rax
	movq	%rcx, -8640(%rbp)
	movq	%rax, -8656(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7896(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19IsUndefinedConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	je	.L1149
.L1151:
	movq	-8640(%rbp), %xmm0
	movq	%rbx, %xmm1
	movq	%r12, %rdi
	movhps	-8608(%rbp), %xmm1
	movhps	-8656(%rbp), %xmm0
	movaps	%xmm1, -8672(%rbp)
	movaps	%xmm0, -8640(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-7664(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -8608(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-8672(%rbp), %xmm1
	movq	-8624(%rbp), %xmm2
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movdqa	-8640(%rbp), %xmm0
	movq	%rax, -7696(%rbp)
	movq	-7648(%rbp), %rax
	movhps	-8608(%rbp), %xmm2
	movaps	%xmm2, -240(%rbp)
	movq	%rax, -7688(%rbp)
	movaps	%xmm1, -224(%rbp)
	movaps	%xmm0, -208(%rbp)
.L1542:
	movl	$6, %ebx
	movl	$1, %ecx
	xorl	%esi, %esi
	movq	%r12, %rdi
	pushq	%rbx
	movq	-8600(%rbp), %r9
	leaq	-7696(%rbp), %rdx
	pushq	%r13
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	popq	%rdi
	movq	%r12, %rdi
	popq	%r8
	movq	-8320(%rbp), %xmm5
	movq	%rax, %rbx
	movq	-8416(%rbp), %xmm6
	movq	-8448(%rbp), %xmm3
	movq	-8480(%rbp), %xmm7
	movq	-8384(%rbp), %xmm1
	movhps	-8400(%rbp), %xmm5
	movq	-8544(%rbp), %xmm2
	movhps	-8432(%rbp), %xmm6
	movq	-8576(%rbp), %xmm4
	movhps	-8464(%rbp), %xmm3
	movhps	-8496(%rbp), %xmm7
	movaps	%xmm5, -8320(%rbp)
	movhps	-8528(%rbp), %xmm1
	movhps	-8552(%rbp), %xmm2
	movaps	%xmm6, -8400(%rbp)
	movhps	-8560(%rbp), %xmm4
	movaps	%xmm3, -8416(%rbp)
	movaps	%xmm7, -8432(%rbp)
	movaps	%xmm1, -8448(%rbp)
	movaps	%xmm2, -8464(%rbp)
	movaps	%xmm4, -8480(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$110, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$112, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-7896(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal13ToBoolean_240EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_6ObjectEEE@PLT
	movq	%rbx, %xmm9
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-8592(%rbp), %xmm0
	movdqa	-8320(%rbp), %xmm5
	leaq	-112(%rbp), %rbx
	movq	%rax, %r12
	movdqa	-8400(%rbp), %xmm6
	movdqa	-8416(%rbp), %xmm3
	movq	%rbx, %rdx
	movq	$0, -7648(%rbp)
	punpcklqdq	%xmm9, %xmm0
	movdqa	-8432(%rbp), %xmm7
	movdqa	-8448(%rbp), %xmm1
	movaps	%xmm5, -240(%rbp)
	movdqa	-8464(%rbp), %xmm2
	movdqa	-8480(%rbp), %xmm4
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -8496(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm0, -7664(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1584(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1152
	call	_ZdlPv@PLT
.L1152:
	movdqa	-8320(%rbp), %xmm6
	movdqa	-8400(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdx
	movdqa	-8416(%rbp), %xmm7
	movdqa	-8432(%rbp), %xmm3
	movq	%r13, %rsi
	movq	%r14, %rdi
	movdqa	-8448(%rbp), %xmm1
	movdqa	-8464(%rbp), %xmm4
	movaps	%xmm6, -240(%rbp)
	movdqa	-8480(%rbp), %xmm5
	movdqa	-8496(%rbp), %xmm6
	movaps	%xmm2, -224(%rbp)
	movaps	%xmm7, -208(%rbp)
	movaps	%xmm3, -192(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm0, -7664(%rbp)
	movq	$0, -7648(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1392(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -8320(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1153
	call	_ZdlPv@PLT
.L1153:
	movq	-8376(%rbp), %rcx
	movq	-8120(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	jmp	.L1147
	.p2align 4,,10
	.p2align 3
.L1572:
	movq	-7936(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-240(%rbp), %r13
	leaq	-1968(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	.LC9(%rip), %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	movl	$2054, %r9d
	leaq	-222(%rbp), %rdx
	movq	$0, -7648(%rbp)
	movaps	%xmm0, -240(%rbp)
	pxor	%xmm0, %xmm0
	movw	%r9w, -224(%rbp)
	movaps	%xmm0, -7664(%rbp)
	call	_ZNSt6vectorIN2v88internal21MachineRepresentationESaIS2_EE19_M_range_initializeIPKS2_EEvT_S8_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7664(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1145
	call	_ZdlPv@PLT
.L1145:
	movq	(%rbx), %rax
	movq	8(%rax), %rbx
	movq	24(%rax), %rsi
	movq	56(%rax), %rdx
	movq	88(%rax), %r11
	movq	80(%rax), %rdi
	movq	(%rax), %rcx
	movq	%rbx, -8416(%rbp)
	movq	16(%rax), %rbx
	movq	72(%rax), %r12
	movq	%rsi, -8464(%rbp)
	movq	40(%rax), %rsi
	movq	%rdx, -8496(%rbp)
	movq	%rbx, -8432(%rbp)
	movq	32(%rax), %rbx
	movq	64(%rax), %rdx
	movq	%rsi, -8400(%rbp)
	movq	48(%rax), %rsi
	movq	%rbx, -8352(%rbp)
	movq	%r11, -8544(%rbp)
	movq	96(%rax), %rbx
	movq	104(%rax), %r11
	movq	136(%rax), %rax
	movq	%rsi, -8480(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -8384(%rbp)
	movl	$109, %edx
	movq	%rdi, -8528(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -8320(%rbp)
	movq	%r11, -8552(%rbp)
	movq	%rax, -8576(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$111, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$3093, %edx
	leaq	.LC6(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r12, %xmm5
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-8576(%rbp), %rax
	leaq	-72(%rbp), %rdx
	movq	%r12, -80(%rbp)
	movq	-8320(%rbp), %xmm0
	movq	-8464(%rbp), %xmm7
	movq	$0, -7648(%rbp)
	movhps	-8416(%rbp), %xmm0
	movaps	%xmm0, -240(%rbp)
	movq	-8432(%rbp), %xmm0
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	-8352(%rbp), %xmm0
	movhps	-8400(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	movq	-8480(%rbp), %xmm0
	movhps	-8496(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	movq	-8384(%rbp), %xmm0
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-8528(%rbp), %xmm0
	movhps	-8544(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%rbx, %xmm0
	movhps	-8552(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%rax, %xmm0
	movhps	-8320(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movdqa	%xmm7, %xmm0
	movhps	-8352(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%rax, %xmm0
	movhps	-8400(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7664(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1776(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -8352(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1146
	call	_ZdlPv@PLT
.L1146:
	movq	-8448(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1578:
	movq	-7960(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-816(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$6, %edi
	movq	$0, -7648(%rbp)
	movaps	%xmm0, -7664(%rbp)
	call	_Znwm@PLT
	movl	$2056, %esi
	movq	%r12, %rdi
	movw	%si, 4(%rax)
	leaq	6(%rax), %rdx
	movq	%r14, %rsi
	movl	$117966599, (%rax)
	movq	%rax, -7664(%rbp)
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7664(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1165
	call	_ZdlPv@PLT
.L1165:
	movq	(%rbx), %rax
	movl	$94, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	8(%rax), %rcx
	movq	16(%rax), %r12
	movq	(%rax), %r13
	movq	32(%rax), %rbx
	movq	%rcx, -7896(%rbp)
	movq	24(%rax), %rcx
	movq	40(%rax), %rax
	movq	%rcx, -8368(%rbp)
	movq	%rax, -8376(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %xmm0
	leaq	-240(%rbp), %rsi
	movq	%r14, %rdi
	movhps	-7896(%rbp), %xmm0
	leaq	-192(%rbp), %rdx
	movq	$0, -7648(%rbp)
	movaps	%xmm0, -240(%rbp)
	movq	%r12, %xmm0
	leaq	-432(%rbp), %r12
	movhps	-8368(%rbp), %xmm0
	movaps	%xmm0, -224(%rbp)
	movq	%rbx, %xmm0
	movhps	-8376(%rbp), %xmm0
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -7664(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7664(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1166
	call	_ZdlPv@PLT
.L1166:
	movq	-7912(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -560(%rbp)
	je	.L1167
.L1579:
	movq	-7904(%rbp), %rsi
	movq	%r15, %rdi
	leaq	-624(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %edi
	movq	$0, -7648(%rbp)
	movaps	%xmm0, -7664(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movb	$6, (%rax)
	leaq	1(%rax), %rdx
	movq	%rax, -7664(%rbp)
	movq	%rdx, -7648(%rbp)
	movq	%rdx, -7656(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-7664(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1168
	call	_ZdlPv@PLT
.L1168:
	movq	(%rbx), %rax
	movq	-8512(%rbp), %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal8compiler21CodeAssemblerVariable4BindEPNS1_4NodeE@PLT
	movq	-8504(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1167
	.p2align 4,,10
	.p2align 3
.L1149:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler14IsNullConstantENS1_5TNodeINS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L1151
	movq	-8640(%rbp), %xmm1
	movq	%rbx, %xmm0
	movq	%r12, %rdi
	movhps	-8608(%rbp), %xmm0
	movhps	-8656(%rbp), %xmm1
	movaps	%xmm0, -8672(%rbp)
	movaps	%xmm1, -8640(%rbp)
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$2, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE@PLT
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler13Int32ConstantEi@PLT
	movq	-7664(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -8608(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	movdqa	-8672(%rbp), %xmm0
	movq	-8624(%rbp), %xmm2
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movdqa	-8640(%rbp), %xmm1
	movq	%rax, -7696(%rbp)
	movq	-7648(%rbp), %rax
	movhps	-8608(%rbp), %xmm2
	movaps	%xmm2, -240(%rbp)
	movq	%rax, -7688(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm1, -208(%rbp)
	jmp	.L1542
	.p2align 4,,10
	.p2align 3
.L1581:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	movdqa	-7952(%rbp), %xmm7
	movdqa	-8048(%rbp), %xmm3
	leaq	-184(%rbp), %rdx
	movaps	%xmm0, -7696(%rbp)
	movq	%rbx, -192(%rbp)
	movaps	%xmm7, -240(%rbp)
	movdqa	-8000(%rbp), %xmm7
	movaps	%xmm3, -208(%rbp)
	movaps	%xmm7, -224(%rbp)
	movq	$0, -7680(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-7344(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7696(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1063
	call	_ZdlPv@PLT
.L1063:
	movq	-8224(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1582:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-7952(%rbp), %xmm7
	movq	%r13, %rsi
	movdqa	-7984(%rbp), %xmm3
	movdqa	-8032(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movdqa	-8080(%rbp), %xmm4
	leaq	-176(%rbp), %rdx
	movaps	%xmm7, -240(%rbp)
	movaps	%xmm3, -224(%rbp)
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm4, -192(%rbp)
	movaps	%xmm0, -7696(%rbp)
	movq	$0, -7680(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8000(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7696(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1077
	call	_ZdlPv@PLT
.L1077:
	movq	-8112(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1076
	.p2align 4,,10
	.p2align 3
.L1583:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-7984(%rbp), %xmm5
	movq	%r13, %rsi
	movdqa	-8032(%rbp), %xmm6
	movdqa	-8336(%rbp), %xmm7
	movdqa	-8464(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	leaq	-64(%rbp), %rdx
	movdqa	-8528(%rbp), %xmm1
	movdqa	-8544(%rbp), %xmm4
	movaps	%xmm5, -240(%rbp)
	movq	%r12, %rdi
	movdqa	-8576(%rbp), %xmm5
	movdqa	-8640(%rbp), %xmm2
	movaps	%xmm6, -224(%rbp)
	movaps	%xmm7, -208(%rbp)
	movdqa	-8592(%rbp), %xmm6
	movdqa	-8624(%rbp), %xmm7
	movaps	%xmm3, -192(%rbp)
	movdqa	-8656(%rbp), %xmm3
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm0, -7696(%rbp)
	movq	$0, -7680(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8016(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7696(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1124
	call	_ZdlPv@PLT
.L1124:
	movq	-8496(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1123
	.p2align 4,,10
	.p2align 3
.L1584:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movdqa	-8352(%rbp), %xmm1
	movq	%r13, %rsi
	movdqa	-8400(%rbp), %xmm4
	movdqa	-8432(%rbp), %xmm5
	movdqa	-8480(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	leaq	-64(%rbp), %rdx
	movdqa	-8528(%rbp), %xmm7
	movdqa	-8544(%rbp), %xmm2
	movaps	%xmm1, -240(%rbp)
	movq	%r12, %rdi
	movdqa	-8576(%rbp), %xmm3
	movdqa	-8592(%rbp), %xmm1
	movaps	%xmm4, -224(%rbp)
	movaps	%xmm5, -208(%rbp)
	movdqa	-8624(%rbp), %xmm4
	movdqa	-8640(%rbp), %xmm5
	movaps	%xmm6, -192(%rbp)
	movdqa	-8656(%rbp), %xmm6
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm6, -80(%rbp)
	movaps	%xmm0, -7696(%rbp)
	movq	$0, -7680(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	movq	-8032(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-7696(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1133
	call	_ZdlPv@PLT
.L1133:
	movq	-8384(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1132
.L1580:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22536:
	.size	_ZN2v88internal20FastArrayFindIndex_6EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_NS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableISA_EE, .-_ZN2v88internal20FastArrayFindIndex_6EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_NS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableISA_EE
	.section	.rodata._ZN2v88internal32ArrayPrototypeFindIndexAssembler35GenerateArrayPrototypeFindIndexImplEv.str1.1,"aMS",@progbits,1
.LC11:
	.string	"Array.prototype.findIndex"
	.section	.text._ZN2v88internal32ArrayPrototypeFindIndexAssembler35GenerateArrayPrototypeFindIndexImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal32ArrayPrototypeFindIndexAssembler35GenerateArrayPrototypeFindIndexImplEv
	.type	_ZN2v88internal32ArrayPrototypeFindIndexAssembler35GenerateArrayPrototypeFindIndexImplEv, @function
_ZN2v88internal32ArrayPrototypeFindIndexAssembler35GenerateArrayPrototypeFindIndexImplEv:
.LFB22618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$3240, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, -3000(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r13, %rcx
	movq	%r12, %rsi
	leaq	-2976(%rbp), %rdi
	movq	%rax, %rdx
	leaq	-2672(%rbp), %r13
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-2960(%rbp), %rcx
	movl	$2, %esi
	movq	%r12, %rdi
	movq	-2976(%rbp), %r14
	movq	-2968(%rbp), %rax
	movq	%r12, -2848(%rbp)
	leaq	-3000(%rbp), %r12
	movq	%rcx, -3152(%rbp)
	movq	%rcx, -2832(%rbp)
	movq	%r14, -2816(%rbp)
	movq	%rax, -3168(%rbp)
	movq	$1, -2840(%rbp)
	movq	%rax, -2824(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%rax, -3136(%rbp)
	leaq	-2848(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -3120(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -2664(%rbp)
	movq	$0, -2656(%rbp)
	movq	%rax, %rbx
	movq	-3000(%rbp), %rax
	movq	$0, -2648(%rbp)
	movq	%rax, -2672(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -2664(%rbp)
	leaq	-2616(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2648(%rbp)
	movq	%rdx, -2656(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2632(%rbp)
	movq	%rax, -3016(%rbp)
	movq	$0, -2640(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3000(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2472(%rbp)
	movq	$0, -2464(%rbp)
	movq	%rax, -2480(%rbp)
	movq	$0, -2456(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -2472(%rbp)
	leaq	-2424(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2456(%rbp)
	movq	%rdx, -2464(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2440(%rbp)
	movq	%rax, -3080(%rbp)
	movq	$0, -2448(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3000(%rbp), %rax
	movl	$168, %edi
	movq	$0, -2280(%rbp)
	movq	$0, -2272(%rbp)
	movq	%rax, -2288(%rbp)
	movq	$0, -2264(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	168(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 160(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movq	%rax, -2280(%rbp)
	leaq	-2232(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2264(%rbp)
	movq	%rdx, -2272(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2248(%rbp)
	movq	%rax, -3040(%rbp)
	movq	$0, -2256(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3000(%rbp), %rax
	movl	$192, %edi
	movq	$0, -2088(%rbp)
	movq	$0, -2080(%rbp)
	movq	%rax, -2096(%rbp)
	movq	$0, -2072(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -2088(%rbp)
	leaq	-2040(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -2072(%rbp)
	movq	%rdx, -2080(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -2056(%rbp)
	movq	%rax, -3112(%rbp)
	movq	$0, -2064(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3000(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1896(%rbp)
	movq	$0, -1888(%rbp)
	movq	%rax, -1904(%rbp)
	movq	$0, -1880(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1896(%rbp)
	leaq	-1848(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1880(%rbp)
	movq	%rdx, -1888(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1864(%rbp)
	movq	%rax, -3072(%rbp)
	movq	$0, -1872(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3000(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1704(%rbp)
	movq	$0, -1696(%rbp)
	movq	%rax, -1712(%rbp)
	movq	$0, -1688(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1704(%rbp)
	leaq	-1656(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1688(%rbp)
	movq	%rdx, -1696(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1672(%rbp)
	movq	%rax, -3088(%rbp)
	movq	$0, -1680(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3000(%rbp), %rax
	movl	$192, %edi
	movq	$0, -1512(%rbp)
	movq	$0, -1504(%rbp)
	movq	%rax, -1520(%rbp)
	movq	$0, -1496(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	192(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movq	%rax, -1512(%rbp)
	leaq	-1464(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1496(%rbp)
	movq	%rdx, -1504(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1480(%rbp)
	movq	%rax, -3048(%rbp)
	movq	$0, -1488(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3000(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1320(%rbp)
	movq	$0, -1312(%rbp)
	movq	%rax, -1328(%rbp)
	movq	$0, -1304(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1320(%rbp)
	leaq	-1272(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1304(%rbp)
	movq	%rdx, -1312(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1288(%rbp)
	movq	%rax, -3064(%rbp)
	movq	$0, -1296(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3000(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1128(%rbp)
	movq	$0, -1120(%rbp)
	movq	%rax, -1136(%rbp)
	movq	$0, -1112(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rax, -1128(%rbp)
	leaq	-1080(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -1112(%rbp)
	movq	%rdx, -1120(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -1096(%rbp)
	movq	%rax, -3024(%rbp)
	movq	$0, -1104(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3000(%rbp), %rax
	movl	$336, %edi
	movq	$0, -936(%rbp)
	movq	$0, -928(%rbp)
	movq	%rax, -944(%rbp)
	movq	$0, -920(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -936(%rbp)
	leaq	-888(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -920(%rbp)
	movq	%rdx, -928(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -904(%rbp)
	movq	%rax, -3104(%rbp)
	movq	$0, -912(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3000(%rbp), %rax
	movl	$336, %edi
	movq	$0, -744(%rbp)
	movq	$0, -736(%rbp)
	movq	%rax, -752(%rbp)
	movq	$0, -728(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	leaq	336(%rax), %rdx
	movups	%xmm0, (%rax)
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movups	%xmm0, 240(%rax)
	movups	%xmm0, 256(%rax)
	movups	%xmm0, 272(%rax)
	movups	%xmm0, 288(%rax)
	movups	%xmm0, 304(%rax)
	movups	%xmm0, 320(%rax)
	movq	%rax, -744(%rbp)
	leaq	-696(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -728(%rbp)
	movq	%rdx, -736(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -712(%rbp)
	movq	%rax, -3056(%rbp)
	movq	$0, -720(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3000(%rbp), %rax
	movl	$240, %edi
	movq	$0, -552(%rbp)
	movq	$0, -544(%rbp)
	movq	%rax, -560(%rbp)
	movq	$0, -536(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%r12, %rsi
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rax, -552(%rbp)
	leaq	-504(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -536(%rbp)
	movq	%rdx, -544(%rbp)
	xorl	%edx, %edx
	movups	%xmm0, -520(%rbp)
	movq	%rax, -3096(%rbp)
	movq	$0, -528(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-3000(%rbp), %rax
	movl	$120, %edi
	movq	$0, -360(%rbp)
	movq	$0, -352(%rbp)
	movq	%rax, -368(%rbp)
	movq	$0, -344(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	$0, 112(%rax)
	leaq	120(%rax), %rdx
	movq	%r12, %rsi
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rax, -360(%rbp)
	leaq	-312(%rbp), %rax
	movq	%rax, %rdi
	movq	%rdx, -344(%rbp)
	movq	%rdx, -352(%rbp)
	xorl	%edx, %edx
	movq	%rax, -3032(%rbp)
	movups	%xmm0, -328(%rbp)
	movq	$0, -336(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %xmm1
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movhps	-3168(%rbp), %xmm1
	movaps	%xmm0, -2800(%rbp)
	leaq	-2800(%rbp), %r14
	movaps	%xmm1, -176(%rbp)
	movq	-3152(%rbp), %xmm1
	movq	%rbx, -144(%rbp)
	movhps	-3136(%rbp), %xmm1
	movq	$0, -2784(%rbp)
	movaps	%xmm1, -160(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movq	%r13, %rdi
	movq	%r14, %rsi
	movdqa	-176(%rbp), %xmm4
	movdqa	-160(%rbp), %xmm5
	leaq	40(%rax), %rdx
	movq	%rax, -2800(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm4, (%rax)
	movups	%xmm5, 16(%rax)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1586
	call	_ZdlPv@PLT
.L1586:
	movq	-3016(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2608(%rbp)
	jne	.L1884
	cmpq	$0, -2416(%rbp)
	jne	.L1885
.L1591:
	cmpq	$0, -2224(%rbp)
	jne	.L1886
.L1594:
	cmpq	$0, -2032(%rbp)
	jne	.L1887
.L1599:
	cmpq	$0, -1840(%rbp)
	jne	.L1888
.L1602:
	cmpq	$0, -1648(%rbp)
	jne	.L1889
.L1606:
	cmpq	$0, -1456(%rbp)
	jne	.L1890
.L1609:
	cmpq	$0, -1264(%rbp)
	jne	.L1891
.L1612:
	cmpq	$0, -1072(%rbp)
	jne	.L1892
.L1615:
	cmpq	$0, -880(%rbp)
	jne	.L1893
.L1620:
	cmpq	$0, -688(%rbp)
	jne	.L1894
.L1623:
	cmpq	$0, -496(%rbp)
	jne	.L1895
.L1625:
	cmpq	$0, -304(%rbp)
	jne	.L1896
.L1627:
	movq	-3032(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1629
	call	_ZdlPv@PLT
.L1629:
	movq	-352(%rbp), %rbx
	movq	-360(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1630
	.p2align 4,,10
	.p2align 3
.L1634:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1631
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1634
.L1632:
	movq	-360(%rbp), %r13
.L1630:
	testq	%r13, %r13
	je	.L1635
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1635:
	movq	-3096(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1636
	call	_ZdlPv@PLT
.L1636:
	movq	-544(%rbp), %rbx
	movq	-552(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1637
	.p2align 4,,10
	.p2align 3
.L1641:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1638
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1641
.L1639:
	movq	-552(%rbp), %r13
.L1637:
	testq	%r13, %r13
	je	.L1642
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1642:
	movq	-3056(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-720(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1643
	call	_ZdlPv@PLT
.L1643:
	movq	-736(%rbp), %rbx
	movq	-744(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1644
	.p2align 4,,10
	.p2align 3
.L1648:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1645
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1648
.L1646:
	movq	-744(%rbp), %r13
.L1644:
	testq	%r13, %r13
	je	.L1649
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1649:
	movq	-3104(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-912(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1650
	call	_ZdlPv@PLT
.L1650:
	movq	-928(%rbp), %rbx
	movq	-936(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1651
	.p2align 4,,10
	.p2align 3
.L1655:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1652
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1655
.L1653:
	movq	-936(%rbp), %r13
.L1651:
	testq	%r13, %r13
	je	.L1656
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1656:
	movq	-3024(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1657
	call	_ZdlPv@PLT
.L1657:
	movq	-1120(%rbp), %rbx
	movq	-1128(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1658
	.p2align 4,,10
	.p2align 3
.L1662:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1659
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1662
.L1660:
	movq	-1128(%rbp), %r13
.L1658:
	testq	%r13, %r13
	je	.L1663
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1663:
	movq	-3064(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1296(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1664
	call	_ZdlPv@PLT
.L1664:
	movq	-1312(%rbp), %rbx
	movq	-1320(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1665
	.p2align 4,,10
	.p2align 3
.L1669:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1666
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1669
.L1667:
	movq	-1320(%rbp), %r13
.L1665:
	testq	%r13, %r13
	je	.L1670
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1670:
	movq	-3048(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1488(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1671
	call	_ZdlPv@PLT
.L1671:
	movq	-1504(%rbp), %rbx
	movq	-1512(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1672
	.p2align 4,,10
	.p2align 3
.L1676:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1673
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1676
.L1674:
	movq	-1512(%rbp), %r13
.L1672:
	testq	%r13, %r13
	je	.L1677
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1677:
	movq	-3088(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1680(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1678
	call	_ZdlPv@PLT
.L1678:
	movq	-1696(%rbp), %rbx
	movq	-1704(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1679
	.p2align 4,,10
	.p2align 3
.L1683:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1680
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L1683
.L1681:
	movq	-1704(%rbp), %r13
.L1679:
	testq	%r13, %r13
	je	.L1684
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1684:
	movq	-3072(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1872(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1685
	call	_ZdlPv@PLT
.L1685:
	movq	-1888(%rbp), %rbx
	movq	-1896(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1686
	.p2align 4,,10
	.p2align 3
.L1690:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1687
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1690
.L1688:
	movq	-1896(%rbp), %r13
.L1686:
	testq	%r13, %r13
	je	.L1691
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1691:
	movq	-3112(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2064(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1692
	call	_ZdlPv@PLT
.L1692:
	movq	-2080(%rbp), %rbx
	movq	-2088(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1693
	.p2align 4,,10
	.p2align 3
.L1697:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1694
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L1697
.L1695:
	movq	-2088(%rbp), %r13
.L1693:
	testq	%r13, %r13
	je	.L1698
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1698:
	movq	-3040(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1699
	call	_ZdlPv@PLT
.L1699:
	movq	-2272(%rbp), %rbx
	movq	-2280(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1700
	.p2align 4,,10
	.p2align 3
.L1704:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1701
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1704
.L1702:
	movq	-2280(%rbp), %r13
.L1700:
	testq	%r13, %r13
	je	.L1705
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1705:
	movq	-3080(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1706
	call	_ZdlPv@PLT
.L1706:
	movq	-2464(%rbp), %rbx
	movq	-2472(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1707
	.p2align 4,,10
	.p2align 3
.L1711:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1708
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1711
.L1709:
	movq	-2472(%rbp), %r13
.L1707:
	testq	%r13, %r13
	je	.L1712
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1712:
	movq	-3016(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-2640(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1713
	call	_ZdlPv@PLT
.L1713:
	movq	-2656(%rbp), %rbx
	movq	-2664(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L1714
	.p2align 4,,10
	.p2align 3
.L1718:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1715
	call	_ZdlPv@PLT
	addq	$24, %r13
	cmpq	%rbx, %r13
	jne	.L1718
.L1716:
	movq	-2664(%rbp), %r13
.L1714:
	testq	%r13, %r13
	je	.L1719
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1719:
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1897
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1715:
	.cfi_restore_state
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1718
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1708:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1711
	jmp	.L1709
	.p2align 4,,10
	.p2align 3
.L1701:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1704
	jmp	.L1702
	.p2align 4,,10
	.p2align 3
.L1694:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1697
	jmp	.L1695
	.p2align 4,,10
	.p2align 3
.L1687:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1690
	jmp	.L1688
	.p2align 4,,10
	.p2align 3
.L1680:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1683
	jmp	.L1681
	.p2align 4,,10
	.p2align 3
.L1673:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1676
	jmp	.L1674
	.p2align 4,,10
	.p2align 3
.L1666:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1669
	jmp	.L1667
	.p2align 4,,10
	.p2align 3
.L1659:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1662
	jmp	.L1660
	.p2align 4,,10
	.p2align 3
.L1652:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1655
	jmp	.L1653
	.p2align 4,,10
	.p2align 3
.L1645:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1648
	jmp	.L1646
	.p2align 4,,10
	.p2align 3
.L1631:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1634
	jmp	.L1632
	.p2align 4,,10
	.p2align 3
.L1638:
	addq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L1641
	jmp	.L1639
	.p2align 4,,10
	.p2align 3
.L1884:
	movq	-3016(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2800(%rbp)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1588
	call	_ZdlPv@PLT
.L1588:
	movq	(%rbx), %rax
	movl	$124, %edx
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	movq	24(%rax), %r13
	movq	32(%rax), %rbx
	movq	%rcx, -3192(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -3216(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -3152(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	.LC11(%rip), %rcx
	call	_ZN2v88internal26RequireObjectCoercible_241EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPKc@PLT
	movl	$127, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssembler15ToObject_InlineENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -3168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$130, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	-3168(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal21GetLengthProperty_244EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEE@PLT
	movl	$133, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -3136(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, -3184(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-3184(%rbp), %rdx
	movq	-3152(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, -3184(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %xmm7
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movq	-3152(%rbp), %xmm3
	movq	-3136(%rbp), %rax
	movq	%rbx, %xmm2
	leaq	-120(%rbp), %rbx
	movq	-3192(%rbp), %xmm4
	leaq	-176(%rbp), %r13
	movq	%rbx, %rdx
	movhps	-3168(%rbp), %xmm2
	punpcklqdq	%xmm7, %xmm3
	movq	%r13, %rsi
	movq	%rax, -128(%rbp)
	movhps	-3216(%rbp), %xmm4
	movaps	%xmm2, -3232(%rbp)
	movaps	%xmm3, -3152(%rbp)
	movaps	%xmm4, -3168(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm0, -2800(%rbp)
	movq	$0, -2784(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2480(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1589
	call	_ZdlPv@PLT
.L1589:
	movq	-3136(%rbp), %rax
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movdqa	-3168(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	movdqa	-3152(%rbp), %xmm5
	movq	$0, -2784(%rbp)
	movq	%rax, -128(%rbp)
	movaps	%xmm7, -176(%rbp)
	movdqa	-3232(%rbp), %xmm7
	movaps	%xmm5, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2288(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1590
	call	_ZdlPv@PLT
.L1590:
	movq	-3040(%rbp), %rcx
	movq	-3080(%rbp), %rdx
	movq	%r12, %rdi
	movq	-3184(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -2416(%rbp)
	je	.L1591
.L1885:
	movq	-3080(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2480(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_Znwm@PLT
	movl	$1800, %r11d
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	$117769477, (%rax)
	leaq	7(%rax), %rdx
	movw	%r11w, 4(%rax)
	movb	$8, 6(%rax)
	movq	%rax, -2800(%rbp)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1592
	call	_ZdlPv@PLT
.L1592:
	movq	(%rbx), %rax
	movl	$134, %edx
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	24(%rax), %rsi
	movq	(%rax), %r13
	movq	32(%rax), %rbx
	movq	%rcx, -3136(%rbp)
	movq	16(%rax), %rcx
	movq	%rsi, -3168(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -3152(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %xmm0
	leaq	-176(%rbp), %rsi
	leaq	-136(%rbp), %rdx
	movhps	-3136(%rbp), %xmm0
	movq	%r14, %rdi
	movq	%rbx, -144(%rbp)
	movaps	%xmm0, -176(%rbp)
	movq	-3152(%rbp), %xmm0
	movq	$0, -2784(%rbp)
	movhps	-3168(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2800(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-368(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1593
	call	_ZdlPv@PLT
.L1593:
	movq	-3032(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2224(%rbp)
	je	.L1594
.L1886:
	movq	-3040(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2288(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$7, %edi
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_Znwm@PLT
	movl	$1800, %r10d
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	$117769477, (%rax)
	leaq	7(%rax), %rdx
	movw	%r10w, 4(%rax)
	movb	$8, 6(%rax)
	movq	%rax, -2800(%rbp)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1595
	call	_ZdlPv@PLT
.L1595:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	40(%rax), %rdx
	movq	24(%rax), %r13
	movq	%rsi, -3168(%rbp)
	movq	32(%rax), %rsi
	movq	%rcx, -3152(%rbp)
	movq	16(%rax), %rcx
	movq	48(%rax), %rax
	movq	%rsi, -3184(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rdx, -3192(%rbp)
	movl	$137, %edx
	movq	%rcx, -3136(%rbp)
	movq	%rax, -3216(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	-3152(%rbp), %xmm6
	movq	-3136(%rbp), %rcx
	movhps	-3168(%rbp), %xmm6
	movq	%rcx, -2928(%rbp)
	movaps	%xmm6, -2944(%rbp)
	pushq	-2928(%rbp)
	pushq	-2936(%rbp)
	pushq	-2944(%rbp)
	movaps	%xmm6, -3152(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal82Cast71UT19ATCallableApiObject17ATCallableJSProxy15JSBoundFunction10JSFunction_1405EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelE@PLT
	movq	%rbx, %xmm7
	leaq	-2880(%rbp), %rbx
	movq	-3216(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	leaq	-104(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%rax, -112(%rbp)
	movq	-3184(%rbp), %xmm2
	punpcklqdq	%xmm7, %xmm5
	movq	-3136(%rbp), %xmm3
	movq	%r13, %xmm7
	movdqa	-3152(%rbp), %xmm6
	leaq	-176(%rbp), %r13
	movaps	%xmm5, -128(%rbp)
	movhps	-3192(%rbp), %xmm2
	punpcklqdq	%xmm7, %xmm3
	movq	%r13, %rsi
	movaps	%xmm5, -3168(%rbp)
	movaps	%xmm2, -3184(%rbp)
	movaps	%xmm3, -3136(%rbp)
	movaps	%xmm6, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm0, -2880(%rbp)
	movq	$0, -2864(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1904(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1596
	call	_ZdlPv@PLT
.L1596:
	movq	-3072(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2792(%rbp)
	jne	.L1898
.L1597:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	cmpq	$0, -2032(%rbp)
	je	.L1599
.L1887:
	movq	-3112(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-2096(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movabsq	$578720283176011013, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2800(%rbp)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1600
	call	_ZdlPv@PLT
.L1600:
	movq	(%rbx), %rax
	leaq	-176(%rbp), %rsi
	leaq	-136(%rbp), %rdx
	movq	%r14, %rdi
	movdqu	16(%rax), %xmm0
	movdqu	(%rax), %xmm1
	movq	32(%rax), %rax
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -144(%rbp)
	movaps	%xmm1, -176(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-368(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1601
	call	_ZdlPv@PLT
.L1601:
	movq	-3032(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1840(%rbp)
	je	.L1602
.L1888:
	movq	-3072(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1904(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movabsq	$578720283176011013, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$7, 8(%rax)
	movq	%rax, -2800(%rbp)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1603
	call	_ZdlPv@PLT
.L1603:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	movq	40(%rax), %rdx
	movq	(%rax), %rcx
	movq	16(%rax), %r13
	movq	%rsi, -3152(%rbp)
	movq	24(%rax), %rsi
	movq	%rdx, -3192(%rbp)
	movq	48(%rax), %rdx
	movq	%rsi, -3168(%rbp)
	movq	32(%rax), %rsi
	movq	64(%rax), %rax
	movq	%rdx, -3216(%rbp)
	movl	$140, %edx
	movq	%rsi, -3184(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -3136(%rbp)
	movq	%rax, -3232(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17IntPtrGreaterThanENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-112(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movq	-3216(%rbp), %xmm6
	movq	%r13, %xmm4
	movq	-3184(%rbp), %xmm7
	leaq	-176(%rbp), %r13
	movq	-3136(%rbp), %xmm5
	movhps	-3168(%rbp), %xmm4
	movq	%r13, %rsi
	movaps	%xmm0, -2800(%rbp)
	movhps	-3232(%rbp), %xmm6
	movhps	-3192(%rbp), %xmm7
	movaps	%xmm4, -3168(%rbp)
	movhps	-3152(%rbp), %xmm5
	movaps	%xmm6, -3216(%rbp)
	movq	%rdx, -3152(%rbp)
	movaps	%xmm7, -3184(%rbp)
	movaps	%xmm5, -3136(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movq	$0, -2784(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1712(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	-3152(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L1604
	call	_ZdlPv@PLT
	movq	-3152(%rbp), %rdx
.L1604:
	movdqa	-3136(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movq	%r13, %rsi
	movq	%r14, %rdi
	movdqa	-3168(%rbp), %xmm6
	movdqa	-3216(%rbp), %xmm2
	movaps	%xmm0, -2800(%rbp)
	movaps	%xmm5, -176(%rbp)
	movdqa	-3184(%rbp), %xmm5
	movaps	%xmm6, -160(%rbp)
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movq	$0, -2784(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1520(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1605
	call	_ZdlPv@PLT
.L1605:
	movq	-3048(%rbp), %rcx
	movq	-3088(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1648(%rbp)
	je	.L1606
.L1889:
	movq	-3088(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1712(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movabsq	$506662689138083077, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2800(%rbp)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1607
	call	_ZdlPv@PLT
.L1607:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	8(%rax), %rsi
	movq	40(%rax), %rdx
	movq	(%rax), %rcx
	movq	16(%rax), %rbx
	movq	%rsi, -3152(%rbp)
	movq	24(%rax), %rsi
	movq	%rdx, -3192(%rbp)
	movq	48(%rax), %rdx
	movq	%rsi, -3168(%rbp)
	movq	32(%rax), %rsi
	movq	56(%rax), %rax
	movq	%rcx, -3136(%rbp)
	movq	%rsi, -3184(%rbp)
	movl	$1, %esi
	movq	%rdx, -3216(%rbp)
	movq	%rax, -3232(%rbp)
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-3136(%rbp), %xmm0
	movq	%rbx, -2896(%rbp)
	pushq	-2896(%rbp)
	movhps	-3152(%rbp), %xmm0
	movaps	%xmm0, -2912(%rbp)
	pushq	-2904(%rbp)
	pushq	-2912(%rbp)
	movaps	%xmm0, -3136(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	leaq	-104(%rbp), %rdx
	movq	%r14, %rdi
	movq	%r13, -112(%rbp)
	movdqa	-3136(%rbp), %xmm0
	leaq	-176(%rbp), %rsi
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -176(%rbp)
	movq	%rbx, %xmm0
	movhps	-3168(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	-3184(%rbp), %xmm0
	movhps	-3192(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-3216(%rbp), %xmm0
	movhps	-3232(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2800(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1328(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1608
	call	_ZdlPv@PLT
.L1608:
	movq	-3064(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1456(%rbp)
	je	.L1609
.L1890:
	movq	-3048(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1520(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movabsq	$506662689138083077, %rcx
	movq	%rcx, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -2800(%rbp)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1610
	call	_ZdlPv@PLT
.L1610:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	8(%rax), %rsi
	movq	24(%rax), %rdx
	movq	(%rax), %rcx
	movq	32(%rax), %r13
	movq	48(%rax), %rbx
	movq	%rsi, -3152(%rbp)
	movq	%rdx, -3184(%rbp)
	movq	16(%rax), %rsi
	movq	40(%rax), %rdx
	movq	56(%rax), %rax
	movq	%rcx, -3136(%rbp)
	movq	%rsi, -3168(%rbp)
	movq	%rdx, -3192(%rbp)
	movq	%rax, -3216(%rbp)
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	leaq	-176(%rbp), %rsi
	leaq	-104(%rbp), %rdx
	movq	%r14, %rdi
	movq	-3136(%rbp), %xmm0
	movq	%rax, -112(%rbp)
	movq	$0, -2784(%rbp)
	movhps	-3152(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-3168(%rbp), %xmm0
	movhps	-3184(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%r13, %xmm0
	movhps	-3192(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	movhps	-3216(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -2800(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1136(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1611
	call	_ZdlPv@PLT
.L1611:
	movq	-3024(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1264(%rbp)
	je	.L1612
.L1891:
	movq	-3064(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1328(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movabsq	$506662689138083077, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -2800(%rbp)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1613
	call	_ZdlPv@PLT
.L1613:
	movq	(%rbx), %rax
	leaq	-176(%rbp), %rsi
	leaq	-104(%rbp), %rdx
	movq	%r14, %rdi
	movdqu	48(%rax), %xmm0
	movdqu	(%rax), %xmm3
	movdqu	16(%rax), %xmm2
	movdqu	32(%rax), %xmm1
	movq	64(%rax), %rax
	movaps	%xmm0, -128(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -176(%rbp)
	movq	%rax, -112(%rbp)
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm0, -2800(%rbp)
	movq	$0, -2784(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-1136(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1614
	call	_ZdlPv@PLT
.L1614:
	movq	-3024(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1072(%rbp)
	je	.L1615
.L1892:
	movq	-3024(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-1136(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movabsq	$506662689138083077, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -2800(%rbp)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1616
	call	_ZdlPv@PLT
.L1616:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	movq	32(%rax), %rdx
	movq	48(%rax), %rbx
	movq	%rcx, -3192(%rbp)
	movq	24(%rax), %rcx
	movq	%rsi, -3216(%rbp)
	movq	16(%rax), %rsi
	movq	%rdx, -3248(%rbp)
	movq	40(%rax), %rdx
	movq	%rcx, -3184(%rbp)
	movq	64(%rax), %rcx
	movq	%rsi, -3232(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	56(%rax), %r13
	movq	%rdx, -3136(%rbp)
	movl	$144, %edx
	movq	%rcx, -3152(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	-2992(%rbp), %rax
	movl	$6, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -3168(%rbp)
	call	_ZN2v88internal8compiler21CodeAssemblerVariableC2EPNS1_13CodeAssemblerENS0_21MachineRepresentationE@PLT
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pushq	-3168(%rbp)
	movq	%r13, %r8
	movq	-3152(%rbp), %r9
	movq	-3136(%rbp), %rdx
	pushq	%r14
	movq	%rbx, %rcx
	movq	%r15, %rdi
	movq	-3184(%rbp), %rsi
	call	_ZN2v88internal20FastArrayFindIndex_6EPNS0_8compiler18CodeAssemblerStateENS1_5TNodeINS0_7ContextEEENS4_INS0_10JSReceiverEEENS4_INS0_6UnionTINS0_3SmiENS0_10HeapNumberEEEEES8_NS4_INS0_6ObjectEEEPNS1_18CodeAssemblerLabelEPNS1_26TypedCodeAssemblerVariableISA_EE
	movq	-3152(%rbp), %rcx
	movq	%r13, %xmm3
	movq	%rbx, %xmm7
	punpcklqdq	%xmm3, %xmm7
	movq	%rax, -72(%rbp)
	leaq	-64(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	-3248(%rbp), %xmm2
	movq	%rcx, %xmm6
	movq	%rax, %rdx
	movq	%rcx, -80(%rbp)
	movq	-3192(%rbp), %xmm4
	leaq	-2880(%rbp), %rbx
	movq	-3232(%rbp), %xmm3
	leaq	-176(%rbp), %r13
	movhps	-3136(%rbp), %xmm6
	movhps	-3136(%rbp), %xmm2
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movhps	-3184(%rbp), %xmm3
	movhps	-3216(%rbp), %xmm4
	movq	%rax, -3192(%rbp)
	movaps	%xmm7, -3264(%rbp)
	movaps	%xmm6, -3280(%rbp)
	movaps	%xmm2, -3248(%rbp)
	movaps	%xmm3, -3136(%rbp)
	movaps	%xmm4, -3184(%rbp)
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm7, -128(%rbp)
	movaps	%xmm6, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movaps	%xmm0, -2880(%rbp)
	movq	$0, -2864(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-752(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2880(%rbp), %rdi
	popq	%r8
	popq	%r9
	testq	%rdi, %rdi
	je	.L1617
	call	_ZdlPv@PLT
.L1617:
	movq	-3056(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -2792(%rbp)
	jne	.L1899
.L1618:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-3168(%rbp), %rdi
	call	_ZN2v88internal8compiler21CodeAssemblerVariableD2Ev@PLT
	cmpq	$0, -880(%rbp)
	je	.L1620
.L1893:
	movq	-3104(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-944(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$14, %edi
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_Znwm@PLT
	movl	$1544, %edi
	movq	%r14, %rsi
	movabsq	$506662689138083077, %rcx
	movw	%di, 12(%rax)
	leaq	14(%rax), %rdx
	movq	%r13, %rdi
	movq	%rcx, (%rax)
	movl	$117966600, 8(%rax)
	movq	%rax, -2800(%rbp)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1621
	call	_ZdlPv@PLT
.L1621:
	movq	(%rbx), %rax
	leaq	-176(%rbp), %rsi
	leaq	-96(%rbp), %rdx
	movq	%r14, %rdi
	movdqu	64(%rax), %xmm0
	movdqu	96(%rax), %xmm5
	movdqu	(%rax), %xmm4
	movdqu	16(%rax), %xmm3
	movdqu	32(%rax), %xmm2
	movdqu	48(%rax), %xmm1
	shufpd	$2, %xmm5, %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm4, -176(%rbp)
	movaps	%xmm3, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -2800(%rbp)
	movq	$0, -2784(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-560(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2800(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1622
	call	_ZdlPv@PLT
.L1622:
	movq	-3096(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -688(%rbp)
	je	.L1623
.L1894:
	movq	-3056(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-752(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$14, %edi
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_Znwm@PLT
	movl	$2056, %esi
	movq	%r13, %rdi
	movabsq	$506662689138083077, %rcx
	movw	%si, 12(%rax)
	leaq	14(%rax), %rdx
	movq	%r14, %rsi
	movq	%rcx, (%rax)
	movl	$117966600, 8(%rax)
	movq	%rax, -2800(%rbp)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1624
	call	_ZdlPv@PLT
.L1624:
	movq	(%rbx), %rax
	movq	-3120(%rbp), %rdi
	movq	104(%rax), %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	cmpq	$0, -496(%rbp)
	je	.L1625
.L1895:
	movq	-3096(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-560(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movabsq	$506662689138083077, %rcx
	movq	%rcx, (%rax)
	movl	$1544, %ecx
	leaq	10(%rax), %rdx
	movw	%cx, 8(%rax)
	movq	%rax, -2800(%rbp)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1626
	call	_ZdlPv@PLT
.L1626:
	movq	(%rbx), %rax
	movl	$149, %edx
	movq	%r12, %rdi
	leaq	-2992(%rbp), %r13
	movq	48(%rax), %rsi
	movq	24(%rax), %r9
	movq	40(%rax), %rcx
	movq	72(%rax), %rbx
	movq	%rsi, -3152(%rbp)
	movq	56(%rax), %rsi
	movq	%r9, -3192(%rbp)
	movq	%rsi, -3168(%rbp)
	movq	64(%rax), %rsi
	movq	%rcx, -3136(%rbp)
	movq	%rsi, -3184(%rbp)
	leaq	.LC2(%rip), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$148, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal8compiler13CodeAssembler7isolateEv@PLT
	movl	$743, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-2800(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler19UntypedHeapConstantENS0_6HandleINS0_10HeapObjectEEE@PLT
	leaq	-176(%rbp), %rcx
	movq	-3192(%rbp), %r9
	xorl	%esi, %esi
	movq	-3136(%rbp), %xmm0
	movq	%rax, %r8
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -2880(%rbp)
	movq	-2784(%rbp), %rax
	leaq	-2880(%rbp), %rdx
	movhps	-3168(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	movq	-3184(%rbp), %xmm0
	movq	%rax, -2872(%rbp)
	movhps	-3136(%rbp), %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	%rbx, %xmm0
	movl	$6, %ebx
	pushq	%rbx
	movhps	-3152(%rbp), %xmm0
	pushq	%rcx
	movl	$1, %ecx
	movaps	%xmm0, -144(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler13CallStubRImplENS0_12StubCallModeERKNS0_23CallInterfaceDescriptorEmPNS1_4NodeENS1_11SloppyTNodeINS0_6ObjectEEESt16initializer_listIS8_E@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-3120(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	cmpq	$0, -304(%rbp)
	popq	%rax
	popq	%rdx
	je	.L1627
.L1896:
	movq	-3032(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-368(%rbp), %r13
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -2784(%rbp)
	movaps	%xmm0, -2800(%rbp)
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -2800(%rbp)
	movq	%rdx, -2784(%rbp)
	movq	%rdx, -2792(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-2800(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1628
	call	_ZdlPv@PLT
.L1628:
	movq	(%rbx), %rax
	movl	$153, %edx
	movq	%r12, %rdi
	leaq	-2880(%rbp), %r14
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	24(%rax), %r10
	movq	16(%rax), %rbx
	movq	%rsi, -3152(%rbp)
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -3136(%rbp)
	movq	%r10, -3168(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	subq	$8, %rsp
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	-3136(%rbp), %xmm0
	movq	%rbx, -2784(%rbp)
	pushq	-2784(%rbp)
	movhps	-3152(%rbp), %xmm0
	movaps	%xmm0, -2800(%rbp)
	pushq	-2792(%rbp)
	pushq	-2800(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r14, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	-3168(%rbp), %r10
	movq	%r13, %rcx
	movl	$25, %edx
	movq	%r10, %rsi
	call	_ZN2v88internal17CodeStubAssembler14ThrowTypeErrorEPNS0_8compiler4NodeENS0_15MessageTemplateES4_S4_S4_@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	jmp	.L1627
	.p2align 4,,10
	.p2align 3
.L1898:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	leaq	-112(%rbp), %rdx
	movq	%r13, %rsi
	movdqa	-3152(%rbp), %xmm5
	movdqa	-3184(%rbp), %xmm7
	movq	%rbx, %rdi
	movaps	%xmm0, -2880(%rbp)
	movq	$0, -2864(%rbp)
	movaps	%xmm5, -176(%rbp)
	movdqa	-3136(%rbp), %xmm5
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm5, -160(%rbp)
	movdqa	-3168(%rbp), %xmm5
	movaps	%xmm5, -128(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-2096(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1598
	call	_ZdlPv@PLT
.L1598:
	movq	-3112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1597
	.p2align 4,,10
	.p2align 3
.L1899:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	movq	-3168(%rbp), %rdi
	call	_ZNK2v88internal8compiler21CodeAssemblerVariable5valueEv@PLT
	movdqa	-3264(%rbp), %xmm6
	movq	%r13, %rsi
	movq	-3152(%rbp), %xmm0
	movdqa	-3184(%rbp), %xmm7
	movdqa	-3136(%rbp), %xmm2
	movq	%rbx, %rdi
	movq	$0, -2864(%rbp)
	movaps	%xmm6, -128(%rbp)
	movdqa	-3248(%rbp), %xmm3
	movdqa	-3280(%rbp), %xmm5
	movaps	%xmm6, -96(%rbp)
	movq	%rax, %xmm6
	movq	-3192(%rbp), %rdx
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm7, -176(%rbp)
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm2, -160(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm0, -2880(%rbp)
	call	_ZNSt6vectorIPN2v88internal8compiler4NodeESaIS4_EE19_M_range_initializeIPKS4_EEvT_SA_St20forward_iterator_tag.isra.0.constprop.0
	leaq	-944(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-2880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1619
	call	_ZdlPv@PLT
.L1619:
	movq	-3104(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	jmp	.L1618
.L1897:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22618:
	.size	_ZN2v88internal32ArrayPrototypeFindIndexAssembler35GenerateArrayPrototypeFindIndexImplEv, .-_ZN2v88internal32ArrayPrototypeFindIndexAssembler35GenerateArrayPrototypeFindIndexImplEv
	.section	.rodata._ZN2v88internal8Builtins32Generate_ArrayPrototypeFindIndexEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"ArrayPrototypeFindIndex"
	.section	.text._ZN2v88internal8Builtins32Generate_ArrayPrototypeFindIndexEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins32Generate_ArrayPrototypeFindIndexEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins32Generate_ArrayPrototypeFindIndexEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins32Generate_ArrayPrototypeFindIndexEPNS0_8compiler18CodeAssemblerStateE:
.LFB22614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$1873, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$744, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L1904
.L1901:
	movq	%r13, %rdi
	call	_ZN2v88internal32ArrayPrototypeFindIndexAssembler35GenerateArrayPrototypeFindIndexImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1905
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1904:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L1901
.L1905:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22614:
	.size	_ZN2v88internal8Builtins32Generate_ArrayPrototypeFindIndexEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins32Generate_ArrayPrototypeFindIndexEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins49Generate_ArrayFindIndexLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins49Generate_ArrayFindIndexLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins49Generate_ArrayFindIndexLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE:
.LFB29974:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29974:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins49Generate_ArrayFindIndexLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins49Generate_ArrayFindIndexLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins49Generate_ArrayFindIndexLoopEagerDeoptContinuationEPNS0_8compiler18CodeAssemblerStateE
	.weak	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE
	.section	.rodata._ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE,"aG",@progbits,_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE,comdat
	.type	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE, @gnu_unique_object
	.size	_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE, 2
_ZN2v88internal13MachineTypeOfINS0_3MapEvE5valueE:
	.byte	7
	.byte	7
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC9:
	.byte	7
	.byte	7
	.byte	8
	.byte	7
	.byte	8
	.byte	6
	.byte	6
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	4
	.byte	4
	.byte	4
	.byte	6
	.byte	7
	.align 16
.LC10:
	.byte	7
	.byte	7
	.byte	8
	.byte	7
	.byte	8
	.byte	6
	.byte	6
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	4
	.byte	4
	.byte	4
	.byte	8
	.byte	7
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
