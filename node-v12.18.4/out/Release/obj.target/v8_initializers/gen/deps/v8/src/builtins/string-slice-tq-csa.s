	.file	"string-slice-tq-csa.cc"
	.text
	.section	.rodata._ZN2v88internal29StringPrototypeSliceAssembler32GenerateStringPrototypeSliceImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../../deps/v8/../../deps/v8/src/builtins/string-slice.tq"
	.section	.rodata._ZN2v88internal29StringPrototypeSliceAssembler32GenerateStringPrototypeSliceImplEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"String.prototype.slice"
	.section	.text._ZN2v88internal29StringPrototypeSliceAssembler32GenerateStringPrototypeSliceImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal29StringPrototypeSliceAssembler32GenerateStringPrototypeSliceImplEv
	.type	_ZN2v88internal29StringPrototypeSliceAssembler32GenerateStringPrototypeSliceImplEv, @function
_ZN2v88internal29StringPrototypeSliceAssembler32GenerateStringPrototypeSliceImplEv:
.LFB22418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1832, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r13, -1672(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal8compiler13CodeAssembler19ChangeInt32ToIntPtrENS1_11SloppyTNodeINS0_7Word32TEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal8compiler13CodeAssembler16LoadFramePointerEv@PLT
	movq	%r14, %rcx
	movq	%r12, %rsi
	leaq	-1664(%rbp), %rdi
	movq	%rax, %rdx
	leaq	-1672(%rbp), %r14
	call	_ZN2v88internal17CodeStubAssembler17GetFrameArgumentsENS0_8compiler5TNodeINS0_7RawPtrTEEENS3_INS0_7IntPtrTEEE@PLT
	movq	-1656(%rbp), %rcx
	movl	$2, %esi
	movq	%r12, %rdi
	movq	-1664(%rbp), %rax
	movq	-1648(%rbp), %rbx
	movq	%r12, -1536(%rbp)
	leaq	-1488(%rbp), %r12
	movq	%rcx, -1776(%rbp)
	movq	%rcx, -1512(%rbp)
	movq	$1, -1528(%rbp)
	movq	%rbx, -1520(%rbp)
	movq	%rax, -1792(%rbp)
	movq	%rax, -1504(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler9ParameterEi@PLT
	leaq	-1536(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -1744(%rbp)
	movq	%rax, -1760(%rbp)
	call	_ZNK2v88internal17CodeStubArguments11GetReceiverEv@PLT
	movl	$120, %edi
	movq	$0, -1480(%rbp)
	movq	$0, -1472(%rbp)
	movq	%rax, %r15
	movq	-1672(%rbp), %rax
	movq	$0, -1464(%rbp)
	movq	%rax, -1488(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1432(%rbp), %rcx
	movq	%r14, %rsi
	leaq	120(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 112(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rdx, -1464(%rbp)
	movq	%rdx, -1472(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1688(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1448(%rbp)
	movq	%rax, -1480(%rbp)
	movq	$0, -1456(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1672(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1288(%rbp)
	movq	$0, -1280(%rbp)
	movq	%rax, -1296(%rbp)
	movq	$0, -1272(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1240(%rbp), %rcx
	movq	%r14, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -1272(%rbp)
	movq	%rdx, -1280(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1712(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1256(%rbp)
	movq	%rax, -1288(%rbp)
	movq	$0, -1264(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1672(%rbp), %rax
	movl	$216, %edi
	movq	$0, -1096(%rbp)
	movq	$0, -1088(%rbp)
	movq	%rax, -1104(%rbp)
	movq	$0, -1080(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-1048(%rbp), %rcx
	movq	%r14, %rsi
	leaq	216(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movq	$0, 208(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movq	%rdx, -1080(%rbp)
	movq	%rdx, -1088(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1720(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -1064(%rbp)
	movq	%rax, -1096(%rbp)
	movq	$0, -1072(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1672(%rbp), %rax
	movl	$240, %edi
	movq	$0, -904(%rbp)
	movq	$0, -896(%rbp)
	movq	%rax, -912(%rbp)
	movq	$0, -888(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-856(%rbp), %rcx
	movq	%r14, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rdx, -888(%rbp)
	movq	%rdx, -896(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1704(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -872(%rbp)
	movq	%rax, -904(%rbp)
	movq	$0, -880(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1672(%rbp), %rax
	movl	$240, %edi
	movq	$0, -712(%rbp)
	movq	$0, -704(%rbp)
	movq	%rax, -720(%rbp)
	movq	$0, -696(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-664(%rbp), %rcx
	movq	%r14, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rdx, -696(%rbp)
	movq	%rdx, -704(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1696(%rbp)
	xorl	%ecx, %ecx
	movups	%xmm0, -680(%rbp)
	movq	%rax, -712(%rbp)
	movq	$0, -688(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1672(%rbp), %rax
	movl	$240, %edi
	movq	$0, -520(%rbp)
	movq	$0, -512(%rbp)
	movq	%rax, -528(%rbp)
	movq	$0, -504(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-472(%rbp), %rsi
	xorl	%ecx, %ecx
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rdx, -504(%rbp)
	movq	%rdx, -512(%rbp)
	xorl	%edx, %edx
	movq	%rsi, -1728(%rbp)
	movq	%r14, %rsi
	movups	%xmm0, -488(%rbp)
	movq	%rax, -520(%rbp)
	movq	$0, -496(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	movq	-1672(%rbp), %rax
	movl	$240, %edi
	movq	$0, -328(%rbp)
	movq	$0, -320(%rbp)
	movq	%rax, -336(%rbp)
	movq	$0, -312(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	-280(%rbp), %rcx
	movq	%r14, %rsi
	leaq	240(%rax), %rdx
	movups	%xmm0, (%rax)
	movq	%rcx, %rdi
	movl	$1, %r8d
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 112(%rax)
	movups	%xmm0, 128(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 160(%rax)
	movups	%xmm0, 176(%rax)
	movups	%xmm0, 192(%rax)
	movups	%xmm0, 208(%rax)
	movups	%xmm0, 224(%rax)
	movq	%rdx, -312(%rbp)
	movq	%rdx, -320(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -1736(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -328(%rbp)
	movups	%xmm0, -296(%rbp)
	movq	$0, -304(%rbp)
	call	_ZN2v88internal8compiler18CodeAssemblerLabelC1EPNS1_13CodeAssemblerEmPKPNS1_21CodeAssemblerVariableENS2_4TypeE@PLT
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	-1792(%rbp), %xmm1
	movq	%r15, -112(%rbp)
	leaq	-1568(%rbp), %r15
	movhps	-1776(%rbp), %xmm1
	movaps	%xmm0, -1568(%rbp)
	movaps	%xmm1, -144(%rbp)
	movq	%rbx, %xmm1
	movhps	-1760(%rbp), %xmm1
	movq	$0, -1552(%rbp)
	movaps	%xmm1, -128(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rcx
	movq	%r12, %rdi
	movq	%r15, %rsi
	movdqa	-128(%rbp), %xmm6
	movdqa	-144(%rbp), %xmm5
	leaq	40(%rax), %rdx
	movq	%rax, -1568(%rbp)
	movq	%rcx, 32(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2
	call	_ZdlPv@PLT
.L2:
	movq	-1688(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1424(%rbp)
	jne	.L172
	cmpq	$0, -1232(%rbp)
	jne	.L173
.L7:
	cmpq	$0, -1040(%rbp)
	jne	.L174
.L10:
	cmpq	$0, -848(%rbp)
	jne	.L175
.L13:
	cmpq	$0, -656(%rbp)
	jne	.L176
.L16:
	cmpq	$0, -464(%rbp)
	jne	.L177
.L20:
	cmpq	$0, -272(%rbp)
	jne	.L178
.L22:
	movq	-1736(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L27
	call	_ZdlPv@PLT
.L27:
	movq	-320(%rbp), %rbx
	movq	-328(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L28
	.p2align 4,,10
	.p2align 3
.L32:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L29
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L32
.L30:
	movq	-328(%rbp), %r12
.L28:
	testq	%r12, %r12
	je	.L33
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L33:
	movq	-1728(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-496(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L34
	call	_ZdlPv@PLT
.L34:
	movq	-512(%rbp), %rbx
	movq	-520(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L35
	.p2align 4,,10
	.p2align 3
.L39:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L36
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L39
.L37:
	movq	-520(%rbp), %r12
.L35:
	testq	%r12, %r12
	je	.L40
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L40:
	movq	-1696(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-688(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L41
	call	_ZdlPv@PLT
.L41:
	movq	-704(%rbp), %rbx
	movq	-712(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L42
	.p2align 4,,10
	.p2align 3
.L46:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L43
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L46
.L44:
	movq	-712(%rbp), %r12
.L42:
	testq	%r12, %r12
	je	.L47
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L47:
	movq	-1704(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-880(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L48
	call	_ZdlPv@PLT
.L48:
	movq	-896(%rbp), %rbx
	movq	-904(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L49
	.p2align 4,,10
	.p2align 3
.L53:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L50
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L53
.L51:
	movq	-904(%rbp), %r12
.L49:
	testq	%r12, %r12
	je	.L54
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L54:
	movq	-1720(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L55
	call	_ZdlPv@PLT
.L55:
	movq	-1088(%rbp), %rbx
	movq	-1096(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L56
	.p2align 4,,10
	.p2align 3
.L60:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L57
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L60
.L58:
	movq	-1096(%rbp), %r12
.L56:
	testq	%r12, %r12
	je	.L61
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L61:
	movq	-1712(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L62
	call	_ZdlPv@PLT
.L62:
	movq	-1280(%rbp), %rbx
	movq	-1288(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L63
	.p2align 4,,10
	.p2align 3
.L67:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L64
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L67
.L65:
	movq	-1288(%rbp), %r12
.L63:
	testq	%r12, %r12
	je	.L68
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L68:
	movq	-1688(%rbp), %rdi
	call	_ZN2v88internal8compiler18CodeAssemblerLabelD1Ev@PLT
	movq	-1456(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L69
	call	_ZdlPv@PLT
.L69:
	movq	-1472(%rbp), %rbx
	movq	-1480(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L70
	.p2align 4,,10
	.p2align 3
.L74:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L71
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%rbx, %r12
	jne	.L74
.L72:
	movq	-1480(%rbp), %r12
.L70:
	testq	%r12, %r12
	je	.L75
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L75:
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L179
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L74
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L64:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L67
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L57:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L60
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L50:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L53
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L43:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L46
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L29:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L32
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L36:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L39
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L172:
	movq	-1688(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$5, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	movl	$117769477, (%rax)
	leaq	5(%rax), %rdx
	movb	$8, 4(%rax)
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L4
	call	_ZdlPv@PLT
.L4:
	movq	(%rbx), %rax
	movl	$15, %edx
	movq	%r14, %rdi
	movq	8(%rax), %rsi
	movq	(%rax), %rcx
	movq	24(%rax), %rbx
	movq	%rsi, -1824(%rbp)
	movq	16(%rax), %rsi
	movq	32(%rax), %rax
	movq	%rcx, -1760(%rbp)
	movq	%rsi, -1776(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rax, -1792(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal44FromConstexpr6String18ATconstexpr_string_154EPNS0_8compiler18CodeAssemblerStateEPKc@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rcx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	-1792(%rbp), %rdx
	call	_ZN2v88internal17CodeStubAssembler12ToThisStringENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_6StringEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	movq	%rax, -1856(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$18, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssembler22LoadStringLengthAsWordENS0_8compiler11SloppyTNodeINS0_6StringEEE@PLT
	movq	%r15, %rdi
	movq	%rax, -1808(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$21, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1776(%rbp), %rsi
	subq	$8, %rsp
	movq	%r15, %rdi
	movq	-1760(%rbp), %xmm4
	movq	%rsi, -1616(%rbp)
	movq	%r12, %rsi
	movhps	-1824(%rbp), %xmm4
	pushq	-1616(%rbp)
	movaps	%xmm4, -1632(%rbp)
	pushq	-1624(%rbp)
	pushq	-1632(%rbp)
	movaps	%xmm4, -1760(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	-1808(%rbp), %rcx
	call	_ZN2v88internal17CodeStubAssembler22ConvertToRelativeIndexENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_7IntPtrTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, -1832(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$25, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal45FromConstexpr8ATintptr17ATconstexpr_int31_148EPNS0_8compiler18CodeAssemblerStateENS0_7int31_tE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movdqa	-1760(%rbp), %xmm4
	subq	$8, %rsp
	movq	%r15, %rdi
	movq	-1776(%rbp), %rsi
	movaps	%xmm4, -1600(%rbp)
	movq	%rsi, -1584(%rbp)
	movq	%r12, %rsi
	pushq	-1584(%rbp)
	pushq	-1592(%rbp)
	pushq	-1600(%rbp)
	call	_ZN2v88internal17CodeStubAssembler16GetArgumentValueENS0_21TorqueStructArgumentsENS0_8compiler5TNodeINS0_7IntPtrTEEE@PLT
	addq	$32, %rsp
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$27, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal12Undefined_64EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, -1824(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1824(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler9WordEqualENS1_5TNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, -1824(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm5
	pxor	%xmm0, %xmm0
	movq	-1808(%rbp), %xmm7
	movq	-1792(%rbp), %xmm2
	movl	$72, %edi
	movaps	%xmm0, -1568(%rbp)
	movq	-1776(%rbp), %xmm3
	movdqa	-1760(%rbp), %xmm4
	movq	%r12, -80(%rbp)
	movhps	-1832(%rbp), %xmm7
	movhps	-1856(%rbp), %xmm2
	punpcklqdq	%xmm5, %xmm3
	movaps	%xmm7, -1808(%rbp)
	movaps	%xmm2, -1792(%rbp)
	movaps	%xmm3, -1776(%rbp)
	movaps	%xmm4, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm7, -96(%rbp)
	movq	$0, -1552(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm4
	movq	-80(%rbp), %rcx
	leaq	-1296(%rbp), %rdi
	movdqa	-112(%rbp), %xmm6
	leaq	72(%rax), %rdx
	movq	%r15, %rsi
	movq	%rax, -1568(%rbp)
	movups	%xmm4, (%rax)
	movdqa	-128(%rbp), %xmm4
	movq	%rcx, 64(%rax)
	movups	%xmm4, 16(%rax)
	movdqa	-96(%rbp), %xmm4
	movups	%xmm6, 32(%rax)
	movups	%xmm4, 48(%rax)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5
	call	_ZdlPv@PLT
.L5:
	movdqa	-1760(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movq	%r12, -80(%rbp)
	movl	$72, %edi
	movdqa	-1776(%rbp), %xmm6
	movdqa	-1792(%rbp), %xmm7
	movaps	%xmm0, -1568(%rbp)
	movdqa	-1808(%rbp), %xmm4
	movaps	%xmm5, -144(%rbp)
	movaps	%xmm6, -128(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movq	$0, -1552(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rcx
	movdqa	-128(%rbp), %xmm6
	movq	%r15, %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-144(%rbp), %xmm5
	leaq	72(%rax), %rdx
	leaq	-1104(%rbp), %rdi
	movdqa	-96(%rbp), %xmm3
	movq	%rcx, 64(%rax)
	movups	%xmm5, (%rax)
	movups	%xmm6, 16(%rax)
	movups	%xmm2, 32(%rax)
	movups	%xmm3, 48(%rax)
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6
	call	_ZdlPv@PLT
.L6:
	movq	-1720(%rbp), %rcx
	movq	-1712(%rbp), %rdx
	movq	%r14, %rdi
	movq	-1824(%rbp), %rsi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -1232(%rbp)
	je	.L7
.L173:
	movq	-1712(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-1296(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	movabsq	$361703076132095237, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L8
	call	_ZdlPv@PLT
.L8:
	movq	(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	40(%rax), %rdi
	movq	48(%rax), %rdx
	movq	8(%rax), %r11
	movq	16(%rax), %r10
	movq	24(%rax), %r9
	movq	32(%rax), %r8
	movq	%rdi, -104(%rbp)
	movl	$80, %edi
	movq	56(%rax), %rsi
	movq	64(%rax), %rcx
	movq	%r11, -136(%rbp)
	movq	(%rax), %rax
	movq	%r10, -128(%rbp)
	movq	%r9, -120(%rbp)
	movq	%r8, -112(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movaps	%xmm0, -1568(%rbp)
	movq	%rax, -144(%rbp)
	movq	$0, -1552(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm7
	movdqa	-128(%rbp), %xmm4
	leaq	-912(%rbp), %rdi
	movdqa	-112(%rbp), %xmm5
	movdqa	-96(%rbp), %xmm6
	leaq	80(%rax), %rdx
	movq	%r15, %rsi
	movdqa	-80(%rbp), %xmm2
	movups	%xmm7, (%rax)
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm2, 64(%rax)
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L9
	call	_ZdlPv@PLT
.L9:
	movq	-1704(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -1040(%rbp)
	je	.L10
.L174:
	movq	-1720(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-1104(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$9, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	movabsq	$361703076132095237, %rcx
	movq	%rcx, (%rax)
	leaq	9(%rax), %rdx
	movb	$8, 8(%rax)
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L11
	call	_ZdlPv@PLT
.L11:
	movq	(%rbx), %rax
	movq	%r15, %rdi
	movq	8(%rax), %rcx
	movq	40(%rax), %rdx
	movq	(%rax), %rsi
	movq	32(%rax), %rbx
	movq	%rcx, -1792(%rbp)
	movq	16(%rax), %rcx
	movq	%rdx, -1832(%rbp)
	movq	56(%rax), %rdx
	movq	%rcx, -1808(%rbp)
	movq	24(%rax), %rcx
	movq	48(%rax), %r12
	movq	%rsi, -1776(%rbp)
	movq	%r13, %rsi
	movq	%rbx, -1824(%rbp)
	movq	64(%rax), %rbx
	movq	%rcx, -1760(%rbp)
	movq	%rdx, -1856(%rbp)
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rcx
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movq	-1760(%rbp), %rsi
	call	_ZN2v88internal17CodeStubAssembler22ConvertToRelativeIndexENS0_8compiler5TNodeINS0_7ContextEEENS3_INS0_6ObjectEEENS3_INS0_7IntPtrTEEE@PLT
	movq	%r15, %rdi
	movq	%rax, -1872(%rbp)
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movl	$80, %edi
	movq	-1776(%rbp), %xmm0
	movq	$0, -1552(%rbp)
	movhps	-1792(%rbp), %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	-1808(%rbp), %xmm0
	movhps	-1760(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-1824(%rbp), %xmm0
	movhps	-1832(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	%r12, %xmm0
	movhps	-1856(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	%rbx, %xmm0
	movhps	-1872(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm7
	leaq	-720(%rbp), %rdi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	leaq	80(%rax), %rdx
	movq	%r15, %rsi
	movdqa	-80(%rbp), %xmm6
	movups	%xmm3, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L12
	call	_ZdlPv@PLT
.L12:
	movq	-1696(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -848(%rbp)
	je	.L13
.L175:
	movq	-1704(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-912(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movl	$1288, %r8d
	movq	%r12, %rdi
	movabsq	$361703076132095237, %rsi
	movq	%rsi, (%rax)
	leaq	10(%rax), %rdx
	movq	%r15, %rsi
	movw	%r8w, 8(%rax)
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L14
	call	_ZdlPv@PLT
.L14:
	movq	(%rbx), %rax
	movl	$80, %edi
	movdqu	64(%rax), %xmm0
	movdqu	16(%rax), %xmm3
	movdqu	32(%rax), %xmm2
	movdqu	48(%rax), %xmm1
	movdqu	(%rax), %xmm7
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm7, -144(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	movaps	%xmm0, -1568(%rbp)
	movq	$0, -1552(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm3
	leaq	-720(%rbp), %rdi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	leaq	80(%rax), %rdx
	movq	%r15, %rsi
	movdqa	-80(%rbp), %xmm6
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L15
	call	_ZdlPv@PLT
.L15:
	movq	-1696(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler4GotoEPNS1_18CodeAssemblerLabelE@PLT
	cmpq	$0, -656(%rbp)
	je	.L16
.L176:
	movq	-1696(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-720(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movl	$1288, %edi
	movabsq	$361703076132095237, %rsi
	movq	%rsi, (%rax)
	leaq	10(%rax), %rdx
	movq	%r15, %rsi
	movw	%di, 8(%rax)
	movq	%r12, %rdi
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L17
	call	_ZdlPv@PLT
.L17:
	movq	(%rbx), %rax
	movq	24(%rax), %rbx
	movq	8(%rax), %rsi
	movq	40(%rax), %rdx
	movq	64(%rax), %rdi
	movq	%rbx, -1808(%rbp)
	movq	32(%rax), %rbx
	movq	%rsi, -1776(%rbp)
	movq	16(%rax), %rsi
	movq	%rdx, -1832(%rbp)
	movq	48(%rax), %rdx
	movq	(%rax), %rcx
	movq	%rbx, -1824(%rbp)
	movq	56(%rax), %rbx
	movq	72(%rax), %rax
	movq	%rsi, -1792(%rbp)
	leaq	.LC0(%rip), %rsi
	movq	%rdx, -1856(%rbp)
	movl	$26, %edx
	movq	%rdi, -1872(%rbp)
	movq	%r14, %rdi
	movq	%rax, %r12
	movq	%rcx, -1760(%rbp)
	movq	%rax, -1840(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movl	$29, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21IntPtrLessThanOrEqualENS1_11SloppyTNodeINS0_5WordTEEES5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	%rbx, %xmm2
	pxor	%xmm0, %xmm0
	movq	-1856(%rbp), %xmm6
	movq	-1872(%rbp), %xmm5
	movl	$80, %edi
	movq	-1824(%rbp), %xmm7
	movaps	%xmm0, -1568(%rbp)
	movq	-1760(%rbp), %xmm3
	punpcklqdq	%xmm2, %xmm6
	movq	-1792(%rbp), %xmm2
	movq	$0, -1552(%rbp)
	movhps	-1840(%rbp), %xmm5
	movhps	-1832(%rbp), %xmm7
	movaps	%xmm6, -1856(%rbp)
	movhps	-1808(%rbp), %xmm2
	movhps	-1776(%rbp), %xmm3
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm5, -1872(%rbp)
	movaps	%xmm7, -1824(%rbp)
	movaps	%xmm2, -1792(%rbp)
	movaps	%xmm3, -1760(%rbp)
	movaps	%xmm3, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm3
	movdqa	-128(%rbp), %xmm7
	leaq	-528(%rbp), %rdi
	movdqa	-112(%rbp), %xmm4
	movdqa	-96(%rbp), %xmm5
	leaq	80(%rax), %rdx
	movq	%r15, %rsi
	movdqa	-80(%rbp), %xmm6
	movups	%xmm3, (%rax)
	movups	%xmm7, 16(%rax)
	movups	%xmm4, 32(%rax)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L18
	call	_ZdlPv@PLT
.L18:
	movdqa	-1760(%rbp), %xmm2
	movdqa	-1792(%rbp), %xmm3
	pxor	%xmm0, %xmm0
	movl	$80, %edi
	movdqa	-1824(%rbp), %xmm7
	movdqa	-1856(%rbp), %xmm4
	movaps	%xmm0, -1568(%rbp)
	movdqa	-1872(%rbp), %xmm5
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm7, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movq	$0, -1552(%rbp)
	call	_Znwm@PLT
	movdqa	-144(%rbp), %xmm6
	movdqa	-128(%rbp), %xmm2
	leaq	-336(%rbp), %rdi
	movdqa	-112(%rbp), %xmm3
	movdqa	-96(%rbp), %xmm7
	leaq	80(%rax), %rdx
	movq	%r15, %rsi
	movdqa	-80(%rbp), %xmm4
	movups	%xmm6, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm7, 48(%rax)
	movups	%xmm4, 64(%rax)
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase9AddInputsESt6vectorIPNS1_4NodeESaIS5_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L19
	call	_ZdlPv@PLT
.L19:
	movq	-1736(%rbp), %rcx
	movq	-1728(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler6BranchENS1_11SloppyTNodeINS0_9IntegralTEEEPNS1_18CodeAssemblerLabelES7_@PLT
	cmpq	$0, -464(%rbp)
	je	.L20
.L177:
	movq	-1728(%rbp), %rsi
	movq	%r14, %rdi
	leaq	-528(%rbp), %r12
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movl	$1288, %esi
	movq	%r12, %rdi
	movabsq	$361703076132095237, %rcx
	movw	%si, 8(%rax)
	leaq	10(%rax), %rdx
	movq	%r15, %rsi
	movq	%rcx, (%rax)
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L21
	call	_ZdlPv@PLT
.L21:
	movl	$30, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal15kEmptyString_67EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	-1744(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	cmpq	$0, -272(%rbp)
	je	.L22
.L178:
	movq	-1736(%rbp), %rsi
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	leaq	-336(%rbp), %r8
	movq	$0, -1760(%rbp)
	movq	%r8, -1776(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler4BindEPNS1_18CodeAssemblerLabelE@PLT
	pxor	%xmm0, %xmm0
	movl	$10, %edi
	movq	$0, -1552(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_Znwm@PLT
	movq	-1776(%rbp), %r8
	movq	%r15, %rsi
	movabsq	$361703076132095237, %rcx
	movq	%rcx, (%rax)
	movl	$1288, %ecx
	leaq	10(%rax), %rdx
	movw	%cx, 8(%rax)
	movq	%r8, %rdi
	movq	%rax, -1568(%rbp)
	movq	%rdx, -1552(%rbp)
	movq	%rdx, -1560(%rbp)
	call	_ZN2v88internal8compiler35CodeAssemblerParameterizedLabelBase10CreatePhisESt6vectorINS0_21MachineRepresentationESaIS4_EE@PLT
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L23
	movq	%rax, -1776(%rbp)
	call	_ZdlPv@PLT
	movq	-1776(%rbp), %rax
.L23:
	movq	(%rax), %rax
	movq	%r14, %rdi
	leaq	.LC0(%rip), %rsi
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	cmovne	%rdx, %r12
	movq	56(%rax), %rdx
	movq	72(%rax), %rax
	testq	%rdx, %rdx
	cmovne	%rdx, %rbx
	testq	%rax, %rax
	cmove	-1760(%rbp), %rax
	movl	$33, %edx
	movq	%rax, -1760(%rbp)
	call	_ZN2v88internal8compiler13CodeAssembler17SetSourcePositionEPKci@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17CodeStubAssemblerC1EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movq	-1760(%rbp), %rcx
	call	_ZN2v88internal17CodeStubAssembler9SubStringENS0_8compiler5TNodeINS0_6StringEEENS3_INS0_7IntPtrTEEES7_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-1744(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal17CodeStubArguments12PopAndReturnEPNS0_8compiler4NodeE@PLT
	jmp	.L22
.L179:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22418:
	.size	_ZN2v88internal29StringPrototypeSliceAssembler32GenerateStringPrototypeSliceImplEv, .-_ZN2v88internal29StringPrototypeSliceAssembler32GenerateStringPrototypeSliceImplEv
	.section	.rodata._ZN2v88internal8Builtins29Generate_StringPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out/Release/obj/gen/torque-output-root/torque-generated/../../deps/v8/src/builtins/string-slice-tq-csa.cc"
	.section	.rodata._ZN2v88internal8Builtins29Generate_StringPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"StringPrototypeSlice"
	.section	.text._ZN2v88internal8Builtins29Generate_StringPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8Builtins29Generate_StringPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE
	.type	_ZN2v88internal8Builtins29Generate_StringPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE, @function
_ZN2v88internal8Builtins29Generate_StringPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE:
.LFB22414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17CodeStubAssemblerC2EPNS0_8compiler18CodeAssemblerStateE@PLT
	movq	%r12, %rdi
	movl	$146, %ecx
	leaq	.LC2(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v88internal8compiler18CodeAssemblerState26SetInitialDebugInformationEPKcS4_i@PLT
	movl	$912, %edi
	call	_ZN2v88internal8Builtins6KindOfEi@PLT
	cmpl	$1, %eax
	je	.L184
.L181:
	movq	%r13, %rdi
	call	_ZN2v88internal29StringPrototypeSliceAssembler32GenerateStringPrototypeSliceImplEv
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssemblerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L185
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal8compiler13CodeAssembler21GetJSContextParameterEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17CodeStubAssembler17PerformStackCheckENS0_8compiler5TNodeINS0_7ContextEEE@PLT
	jmp	.L181
.L185:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22414:
	.size	_ZN2v88internal8Builtins29Generate_StringPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE, .-_ZN2v88internal8Builtins29Generate_StringPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8Builtins29Generate_StringPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8Builtins29Generate_StringPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE, @function
_GLOBAL__sub_I__ZN2v88internal8Builtins29Generate_StringPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE:
.LFB28905:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE28905:
	.size	_GLOBAL__sub_I__ZN2v88internal8Builtins29Generate_StringPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE, .-_GLOBAL__sub_I__ZN2v88internal8Builtins29Generate_StringPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8Builtins29Generate_StringPrototypeSliceEPNS0_8compiler18CodeAssemblerStateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
