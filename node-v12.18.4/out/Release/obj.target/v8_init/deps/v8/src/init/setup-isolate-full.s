	.file	"setup-isolate-full.cc"
	.text
	.section	.text._ZN2v88internal20SetupIsolateDelegateD2Ev,"axG",@progbits,_ZN2v88internal20SetupIsolateDelegateD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20SetupIsolateDelegateD2Ev
	.type	_ZN2v88internal20SetupIsolateDelegateD2Ev, @function
_ZN2v88internal20SetupIsolateDelegateD2Ev:
.LFB25183:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE25183:
	.size	_ZN2v88internal20SetupIsolateDelegateD2Ev, .-_ZN2v88internal20SetupIsolateDelegateD2Ev
	.weak	_ZN2v88internal20SetupIsolateDelegateD1Ev
	.set	_ZN2v88internal20SetupIsolateDelegateD1Ev,_ZN2v88internal20SetupIsolateDelegateD2Ev
	.section	.text._ZN2v88internal20SetupIsolateDelegateD0Ev,"axG",@progbits,_ZN2v88internal20SetupIsolateDelegateD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20SetupIsolateDelegateD0Ev
	.type	_ZN2v88internal20SetupIsolateDelegateD0Ev, @function
_ZN2v88internal20SetupIsolateDelegateD0Ev:
.LFB25185:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE25185:
	.size	_ZN2v88internal20SetupIsolateDelegateD0Ev, .-_ZN2v88internal20SetupIsolateDelegateD0Ev
	.section	.rodata._ZN2v88internal20SetupIsolateDelegate13SetupBuiltinsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"isolate->snapshot_available()"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal20SetupIsolateDelegate13SetupBuiltinsEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20SetupIsolateDelegate13SetupBuiltinsEPNS0_7IsolateE
	.type	_ZN2v88internal20SetupIsolateDelegate13SetupBuiltinsEPNS0_7IsolateE, @function
_ZN2v88internal20SetupIsolateDelegate13SetupBuiltinsEPNS0_7IsolateE:
.LFB20105:
	.cfi_startproc
	endbr64
	cmpb	$0, 8(%rdi)
	jne	.L13
	movq	41792(%rsi), %rax
	testq	%rax, %rax
	je	.L6
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L4
.L6:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	%rsi, %rdi
	jmp	_ZN2v88internal20SetupIsolateDelegate21SetupBuiltinsInternalEPNS0_7IsolateE@PLT
	.cfi_endproc
.LFE20105:
	.size	_ZN2v88internal20SetupIsolateDelegate13SetupBuiltinsEPNS0_7IsolateE, .-_ZN2v88internal20SetupIsolateDelegate13SetupBuiltinsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal20SetupIsolateDelegate9SetupHeapEPNS0_4HeapE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"heap->isolate()->snapshot_available()"
	.section	.text._ZN2v88internal20SetupIsolateDelegate9SetupHeapEPNS0_4HeapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20SetupIsolateDelegate9SetupHeapEPNS0_4HeapE
	.type	_ZN2v88internal20SetupIsolateDelegate9SetupHeapEPNS0_4HeapE, @function
_ZN2v88internal20SetupIsolateDelegate9SetupHeapEPNS0_4HeapE:
.LFB20106:
	.cfi_startproc
	endbr64
	cmpb	$0, 8(%rdi)
	jne	.L23
	movq	4200(%rsi), %rax
	testq	%rax, %rax
	je	.L16
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L17
.L16:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%rsi, %rdi
	jmp	_ZN2v88internal20SetupIsolateDelegate17SetupHeapInternalEPNS0_4HeapE@PLT
	.cfi_endproc
.LFE20106:
	.size	_ZN2v88internal20SetupIsolateDelegate9SetupHeapEPNS0_4HeapE, .-_ZN2v88internal20SetupIsolateDelegate9SetupHeapEPNS0_4HeapE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal20SetupIsolateDelegate13SetupBuiltinsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal20SetupIsolateDelegate13SetupBuiltinsEPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal20SetupIsolateDelegate13SetupBuiltinsEPNS0_7IsolateE:
.LFB25235:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25235:
	.size	_GLOBAL__sub_I__ZN2v88internal20SetupIsolateDelegate13SetupBuiltinsEPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal20SetupIsolateDelegate13SetupBuiltinsEPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal20SetupIsolateDelegate13SetupBuiltinsEPNS0_7IsolateE
	.weak	_ZTVN2v88internal20SetupIsolateDelegateE
	.section	.data.rel.ro.local._ZTVN2v88internal20SetupIsolateDelegateE,"awG",@progbits,_ZTVN2v88internal20SetupIsolateDelegateE,comdat
	.align 8
	.type	_ZTVN2v88internal20SetupIsolateDelegateE, @object
	.size	_ZTVN2v88internal20SetupIsolateDelegateE, 48
_ZTVN2v88internal20SetupIsolateDelegateE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal20SetupIsolateDelegateD1Ev
	.quad	_ZN2v88internal20SetupIsolateDelegateD0Ev
	.quad	_ZN2v88internal20SetupIsolateDelegate13SetupBuiltinsEPNS0_7IsolateE
	.quad	_ZN2v88internal20SetupIsolateDelegate9SetupHeapEPNS0_4HeapE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
