	.file	"test_linked_binding.cc"
	.text
	.section	.text._ZN7testing4Test5SetupEv,"axG",@progbits,_ZN7testing4Test5SetupEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing4Test5SetupEv
	.type	_ZN7testing4Test5SetupEv, @function
_ZN7testing4Test5SetupEv:
.LFB4725:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4725:
	.size	_ZN7testing4Test5SetupEv, .-_ZN7testing4Test5SetupEv
	.section	.text._ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestED2Ev:
.LFB11351:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11351:
	.size	_ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestED1Ev,_ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestED2Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestED2Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestED2Ev
	.type	_ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestED2Ev, @function
_ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestED2Ev:
.LFB11359:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11359:
	.size	_ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestED2Ev, .-_ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestED2Ev
	.weak	_ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestED1Ev
	.set	_ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestED1Ev,_ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestED2Ev
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"value"
.LC1:
	.string	"key"
	.text
	.p2align 4
	.globl	_Z17InitializeBindingN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPv
	.type	_Z17InitializeBindingN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPv, @function
_Z17InitializeBindingN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPv:
.LFB8500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rdx, %rdi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	subq	$16, %rsp
	call	_ZN2v87Context10GetIsolateEv@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L10
.L6:
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L11
.L7:
	movq	%r13, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L12
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L11:
	movq	%rax, -40(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-40(%rbp), %rdx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L12:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
.LFE8500:
	.size	_Z17InitializeBindingN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPv, .-_Z17InitializeBindingN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPv
	.p2align 4
	.globl	_Z22InitializeLocalBindingN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPv
	.type	_Z22InitializeLocalBindingN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPv, @function
_Z22InitializeLocalBindingN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPv:
.LFB8533:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rdx, %rdi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	subq	$16, %rsp
	addl	$1, (%rcx)
	call	_ZN2v87Context10GetIsolateEv@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L18
.L14:
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L19
.L15:
	movq	%r13, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L20
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%rax, -40(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-40(%rbp), %rdx
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L20:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
.LFE8533:
	.size	_Z22InitializeLocalBindingN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPv, .-_Z22InitializeLocalBindingN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPv
	.section	.rodata.str1.1
.LC2:
	.string	"node"
.LC3:
	.string	"-p"
.LC4:
	.string	"process.version"
.LC5:
	.string	"%s"
.LC6:
	.string	"local_linked"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"process._linkedBinding('local_linked').key"
	.text
	.align 2
	.p2align 4
	.globl	_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test8TestBodyEv
	.type	_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test8TestBodyEv, @function
_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test8TestBodyEv:
.LFB8540:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$8, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	.LC5(%rip), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-144(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	.LC2(%rip), %rdx
	leaq	.LC3(%rip), %rax
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	leaq	.LC4(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	-72(%rbp), %rdi
	call	strlen@PLT
	movl	$24, %edi
	movq	%rax, %r13
	call	malloc@PLT
	leal	22(%r13), %edi
	movl	$5, %r13d
	movslq	%edi, %rdi
	movq	%rax, %r12
	call	malloc@PLT
	leaq	-80(%rbp), %r10
	movq	%rax, (%r12)
	movl	$1701080942, (%rax)
	movb	$0, 4(%rax)
	.p2align 4,,10
	.p2align 3
.L22:
	movq	(%r10,%r15), %rcx
	movq	%r10, -208(%rbp)
	movq	%rcx, %rdi
	movq	%rcx, -200(%rbp)
	call	strlen@PLT
	movslq	%r13d, %r9
	movq	-200(%rbp), %rcx
	movq	%r14, %rdx
	leal	1(%rax), %r8d
	movq	%r9, %rdi
	xorl	%eax, %eax
	addq	(%r12), %rdi
	movslq	%r8d, %rsi
	movl	%r8d, -192(%rbp)
	movq	%r9, -184(%rbp)
	call	snprintf@PLT
	movq	-184(%rbp), %r9
	addq	(%r12), %r9
	movl	-192(%rbp), %r8d
	movq	%r9, (%r12,%r15)
	addq	$8, %r15
	movq	-208(%rbp), %r10
	addl	%r8d, %r13d
	cmpq	$24, %r15
	jne	.L22
	movq	-144(%rbp), %r14
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE@PLT
	movq	%rax, -112(%rbp)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L35
	movq	%rax, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	movq	%r14, %rdi
	xorl	%ecx, %ecx
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	call	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L36
	movq	-112(%rbp), %rsi
	movq	%r12, %r9
	movl	$3, %r8d
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%rax, %rdi
	call	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L37
	leaq	-164(%rbp), %rcx
	leaq	_Z22InitializeLocalBindingN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPv(%rip), %rdx
	movq	%rax, %rdi
	movl	$0, -164(%rbp)
	leaq	.LC6(%rip), %rsi
	call	_ZN4node16AddLinkedBindingEPNS_11EnvironmentEPKcPFvN2v85LocalINS4_6ObjectEEENS5_INS4_5ValueEEENS5_INS4_7ContextEEEPvESC_@PLT
	movq	16(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	16(%rbx), %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC7(%rip), %rsi
	movq	%rax, -184(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L38
.L26:
	movq	-184(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZN2v86Script7CompileENS_5LocalINS_7ContextEEENS1_INS_6StringEEEPNS_12ScriptOriginE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L39
.L27:
	movq	-184(%rbp), %rsi
	call	_ZN2v86Script3RunENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L40
.L28:
	movq	16(%rbx), %rsi
	leaq	-160(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN2v86String9Utf8ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movq	-160(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L41
	movl	$6, %ecx
	leaq	.LC0(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L42
	cmpl	$1, -164(%rbp)
	jne	.L43
	movq	%rbx, %rdi
	call	_ZN2v86String9Utf8ValueD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE@PLT
	movq	%r14, %rdi
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	(%r12), %rdi
	call	free@PLT
	movq	%r12, %rdi
	call	free@PLT
	movq	-216(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L44
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L38:
	movq	%rax, -192(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-192(%rbp), %rsi
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%rax, -192(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-192(%rbp), %rdi
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%rax, -184(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-184(%rbp), %rdx
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L41:
	leaq	_ZZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test8TestBodyEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L42:
	leaq	_ZZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test8TestBodyEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L43:
	leaq	_ZZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test8TestBodyEvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L44:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8540:
	.size	_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test8TestBodyEv, .-_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test8TestBodyEv
	.section	.text._ZN15NodeTestFixture8TearDownEv,"axG",@progbits,_ZN15NodeTestFixture8TearDownEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN15NodeTestFixture8TearDownEv
	.type	_ZN15NodeTestFixture8TearDownEv, @function
_ZN15NodeTestFixture8TearDownEv:
.LFB8491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdi
	movq	16(%rbx), %rsi
	movq	(%rdi), %rax
	call	*176(%rax)
	movq	16(%rbx), %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdi
	movq	16(%rbx), %rsi
	movq	(%rdi), %rax
	call	*200(%rax)
	movq	16(%rbx), %rdi
	call	_ZN2v87Isolate7DisposeEv@PLT
	movq	$0, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8491:
	.size	_ZN15NodeTestFixture8TearDownEv, .-_ZN15NodeTestFixture8TearDownEv
	.section	.text._ZN15NodeTestFixture5SetUpEv,"axG",@progbits,_ZN15NodeTestFixture5SetUpEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN15NodeTestFixture5SetUpEv
	.type	_ZN15NodeTestFixture5SetUpEv, @function
_ZN15NodeTestFixture5SetUpEv:
.LFB8488:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN4node26CreateArrayBufferAllocatorEv@PLT
	movq	8+_ZN15NodeTestFixture9allocatorE(%rip), %r8
	movq	%rax, %rdi
	movq	%rax, 8+_ZN15NodeTestFixture9allocatorE(%rip)
	testq	%r8, %r8
	je	.L48
	movq	%r8, %rdi
	call	*_ZN15NodeTestFixture9allocatorE(%rip)
	movq	8+_ZN15NodeTestFixture9allocatorE(%rip), %rdi
.L48:
	movq	_ZN4node24FreeArrayBufferAllocatorEPNS_20ArrayBufferAllocatorE@GOTPCREL(%rip), %rax
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	movq	%rax, _ZN15NodeTestFixture9allocatorE(%rip)
	call	_ZN4node10NewIsolateEPNS_20ArrayBufferAllocatorEP9uv_loop_sPNS_20MultiIsolatePlatformE@PLT
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L55
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate5EnterEv@PLT
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	leaq	_ZZN15NodeTestFixture5SetUpEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8488:
	.size	_ZN15NodeTestFixture5SetUpEv, .-_ZN15NodeTestFixture5SetUpEv
	.section	.text._ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestED0Ev:
.LFB11361:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11361:
	.size	_ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestED0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestED0Ev,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestED0Ev
	.type	_ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestED0Ev, @function
_ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestED0Ev:
.LFB11353:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11353:
	.size	_ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestED0Ev, .-_ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestED0Ev
	.section	.text._ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestD2Ev,"axG",@progbits,_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestD2Ev
	.type	_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestD2Ev, @function
_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestD2Ev:
.LFB11347:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11347:
	.size	_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestD2Ev, .-_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestD2Ev
	.weak	_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestD1Ev
	.set	_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestD1Ev,_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestD2Ev
	.section	.text._ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestD0Ev,"axG",@progbits,_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestD0Ev
	.type	_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestD0Ev, @function
_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestD0Ev:
.LFB11349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11349:
	.size	_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestD0Ev, .-_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestD0Ev
	.section	.text._ZN33LinkedBindingTest_SimpleTest_TestD2Ev,"axG",@progbits,_ZN33LinkedBindingTest_SimpleTest_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN33LinkedBindingTest_SimpleTest_TestD2Ev
	.type	_ZN33LinkedBindingTest_SimpleTest_TestD2Ev, @function
_ZN33LinkedBindingTest_SimpleTest_TestD2Ev:
.LFB11355:
	.cfi_startproc
	endbr64
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN7testing4TestD2Ev@PLT
	.cfi_endproc
.LFE11355:
	.size	_ZN33LinkedBindingTest_SimpleTest_TestD2Ev, .-_ZN33LinkedBindingTest_SimpleTest_TestD2Ev
	.weak	_ZN33LinkedBindingTest_SimpleTest_TestD1Ev
	.set	_ZN33LinkedBindingTest_SimpleTest_TestD1Ev,_ZN33LinkedBindingTest_SimpleTest_TestD2Ev
	.section	.text._ZN33LinkedBindingTest_SimpleTest_TestD0Ev,"axG",@progbits,_ZN33LinkedBindingTest_SimpleTest_TestD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN33LinkedBindingTest_SimpleTest_TestD0Ev
	.type	_ZN33LinkedBindingTest_SimpleTest_TestD0Ev, @function
_ZN33LinkedBindingTest_SimpleTest_TestD0Ev:
.LFB11357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV15NodeTestFixture(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN7testing4TestD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11357:
	.size	_ZN33LinkedBindingTest_SimpleTest_TestD0Ev, .-_ZN33LinkedBindingTest_SimpleTest_TestD0Ev
	.section	.text._ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestE10CreateTestEv:
.LFB11436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV33LinkedBindingTest_SimpleTest_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11436:
	.size	_ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestE10CreateTestEv
	.section	.text._ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestE10CreateTestEv,"axG",@progbits,_ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestE10CreateTestEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestE10CreateTestEv
	.type	_ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestE10CreateTestEv, @function
_ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestE10CreateTestEv:
.LFB11435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN7testing4TestC2Ev@PLT
	leaq	16+_ZTV54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test(%rip), %rax
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11435:
	.size	_ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestE10CreateTestEv, .-_ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestE10CreateTestEv
	.section	.text._ZN15NodeTestFixture16TearDownTestCaseEv,"axG",@progbits,_ZN15NodeTestFixture16TearDownTestCaseEv,comdat
	.p2align 4
	.weak	_ZN15NodeTestFixture16TearDownTestCaseEv
	.type	_ZN15NodeTestFixture16TearDownTestCaseEv, @function
_ZN15NodeTestFixture16TearDownTestCaseEv:
.LFB8487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rbx
	subq	$8, %rsp
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdi
	call	_ZN4node12NodePlatform8ShutdownEv@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L73:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	uv_run@PLT
.L70:
	movq	%rbx, %rdi
	call	uv_loop_alive@PLT
	testl	%eax, %eax
	jne	.L73
	call	_ZN2v82V816ShutdownPlatformEv@PLT
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rdi
	call	uv_loop_close@PLT
	testl	%eax, %eax
	jne	.L74
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	leaq	_ZZN15NodeTestFixture16TearDownTestCaseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8487:
	.size	_ZN15NodeTestFixture16TearDownTestCaseEv, .-_ZN15NodeTestFixture16TearDownTestCaseEv
	.section	.rodata.str1.1
.LC8:
	.string	"../test/cctest/gtest/gtest.h"
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"Condition !test_case_fp || !test_suite_fp failed. "
	.align 8
.LC10:
	.string	"Test can not provide both SetUpTestSuite and SetUpTestCase, please make sure there is only one present "
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_ZN7testing8internal16SuiteApiResolverI17LinkedBindingTestE19GetSetUpCaseOrSuiteEv.part.0, @function
_ZN7testing8internal16SuiteApiResolverI17LinkedBindingTestE19GetSetUpCaseOrSuiteEv.part.0:
.LFB11539:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$6946, %ecx
	movl	$3, %esi
	leaq	.LC8(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-28(%rbp), %r12
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN7testing8internal8GTestLogC1ENS0_16GTestLogSeverityEPKci@PLT
	movl	$50, %edx
	leaq	.LC9(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC10(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal8GTestLogD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L78
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L78:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11539:
	.size	_ZN7testing8internal16SuiteApiResolverI17LinkedBindingTestE19GetSetUpCaseOrSuiteEv.part.0, .-_ZN7testing8internal16SuiteApiResolverI17LinkedBindingTestE19GetSetUpCaseOrSuiteEv.part.0
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"Test can not provide both TearDownTestSuite and TearDownTestCase, please make sure there is only one present "
	.section	.text.startup
	.p2align 4
	.type	_ZN7testing8internal16SuiteApiResolverI17LinkedBindingTestE22GetTearDownCaseOrSuiteEv.part.0, @function
_ZN7testing8internal16SuiteApiResolverI17LinkedBindingTestE22GetTearDownCaseOrSuiteEv.part.0:
.LFB11540:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$6959, %ecx
	movl	$3, %esi
	leaq	.LC8(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-28(%rbp), %r12
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN7testing8internal8GTestLogC1ENS0_16GTestLogSeverityEPKci@PLT
	movl	$50, %edx
	leaq	.LC9(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	.LC11(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc@PLT
	movq	%r12, %rdi
	call	_ZN7testing8internal8GTestLogD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L82
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L82:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11540:
	.size	_ZN7testing8internal16SuiteApiResolverI17LinkedBindingTestE22GetTearDownCaseOrSuiteEv.part.0, .-_ZN7testing8internal16SuiteApiResolverI17LinkedBindingTestE22GetTearDownCaseOrSuiteEv.part.0
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"basic_string::_M_construct null not valid"
	.section	.text.startup
	.align 2
	.p2align 4
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0:
.LFB11563:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	16(%rdi), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r14, (%rdi)
	testq	%rsi, %rsi
	je	.L94
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rsi, %r13
	call	strlen@PLT
	movq	%rax, -48(%rbp)
	movq	%rax, %r12
	cmpq	$15, %rax
	ja	.L95
	cmpq	$1, %rax
	jne	.L87
	movzbl	0(%r13), %eax
	movb	%al, 16(%rbx)
.L88:
	movq	-48(%rbp), %rax
	movq	(%rbx), %rdx
	movq	%rax, 8(%rbx)
	movb	$0, (%rdx,%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L96
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L87:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L88
	jmp	.L86
.L95:
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %r14
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L86:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	memcpy@PLT
	jmp	.L88
.L94:
	leaq	.LC12(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L96:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11563:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	.section	.text._ZN4node12NodePlatformD0Ev,"axG",@progbits,_ZN4node12NodePlatformD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12NodePlatformD0Ev
	.type	_ZN4node12NodePlatformD0Ev, @function
_ZN4node12NodePlatformD0Ev:
.LFB9978:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	120(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L99
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L100
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L126
	.p2align 4,,10
	.p2align 3
.L99:
	movq	64(%r12), %rbx
	testq	%rbx, %rbx
	je	.L105
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L111
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	jne	.L127
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L105
.L106:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L113
.L127:
	lock subl	$1, 8(%r13)
	jne	.L113
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L113
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L106
	.p2align 4,,10
	.p2align 3
.L105:
	movq	56(%r12), %rax
	movq	48(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	movq	$0, 72(%r12)
	movq	$0, 64(%r12)
	cmpq	%rax, %rdi
	je	.L115
	call	_ZdlPv@PLT
.L115:
	leaq	8(%r12), %rdi
	call	uv_mutex_destroy@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L109
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L109:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L105
.L111:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L109
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L109
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L100:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L99
.L126:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L103
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L104:
	cmpl	$1, %eax
	jne	.L99
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L103:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L104
	.cfi_endproc
.LFE9978:
	.size	_ZN4node12NodePlatformD0Ev, .-_ZN4node12NodePlatformD0Ev
	.section	.rodata._ZN15NodeTestFixture13SetUpTestCaseEv.str1.1,"aMS",@progbits,1
.LC13:
	.string	"NODE_OPTIONS"
.LC14:
	.string	"cctest"
	.section	.text._ZN15NodeTestFixture13SetUpTestCaseEv,"axG",@progbits,_ZN15NodeTestFixture13SetUpTestCaseEv,comdat
	.p2align 4
	.weak	_ZN15NodeTestFixture13SetUpTestCaseEv
	.type	_ZN15NodeTestFixture13SetUpTestCaseEv, @function
_ZN15NodeTestFixture13SetUpTestCaseEv:
.LFB8482:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN15NodeTestFixture16node_initializedE(%rip)
	je	.L170
.L129:
	movl	$1312, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node7tracing5AgentC1Ev@PLT
	movq	_ZN15NodeTestFixture13tracing_agentE(%rip), %r13
	movq	%r12, _ZN15NodeTestFixture13tracing_agentE(%rip)
	testq	%r13, %r13
	je	.L130
	movq	%r13, %rdi
	call	_ZN4node7tracing5AgentD1Ev@PLT
	movl	$1312, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	movq	_ZN15NodeTestFixture13tracing_agentE(%rip), %r12
.L130:
	movq	%r12, %rdi
	call	_ZN4node7tracing16TraceEventHelper8SetAgentEPNS0_5AgentE@PLT
	movq	_ZN15NodeTestFixture13tracing_agentE(%rip), %rax
	movq	976(%rax), %r12
	testq	%r12, %r12
	je	.L171
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rdi
	call	uv_loop_init@PLT
	testl	%eax, %eax
	jne	.L172
	movl	$128, %edi
	call	_Znwm@PLT
	movq	%r12, %rdx
	movl	$4, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN4node12NodePlatformC1EiPNS_7tracing17TracingControllerE@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %r12
	movq	%r13, _ZN15NodeTestFixture8platformE(%rip)
	testq	%r12, %r12
	je	.L133
	movq	(%r12), %rax
	leaq	_ZN4node12NodePlatformD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L134
	movq	120(%r12), %r13
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L136
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L137
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L138:
	cmpl	$1, %eax
	jne	.L136
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L140
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L141:
	cmpl	$1, %eax
	jne	.L136
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L136:
	movq	64(%r12), %rbx
	testq	%rbx, %rbx
	je	.L155
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	jne	.L145
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L153:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L155
.L145:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L153
	lock subl	$1, 8(%r13)
	jne	.L153
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L153
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L145
	.p2align 4,,10
	.p2align 3
.L155:
	movq	56(%r12), %rax
	movq	48(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	movq	$0, 72(%r12)
	movq	$0, 64(%r12)
	cmpq	%rax, %rdi
	je	.L143
	call	_ZdlPv@PLT
.L143:
	leaq	8(%r12), %rdi
	call	uv_mutex_destroy@PLT
	movl	$128, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %r13
.L133:
	movq	%r13, %rdi
	call	_ZN2v82V818InitializePlatformEPNS_8PlatformE@PLT
	call	_ZN2v82V810InitializeEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L173
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L148
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L148:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L155
.L151:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L148
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L148
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L170:
	leaq	.LC13(%rip), %rdi
	call	uv_os_unsetenv@PLT
	leaq	.LC14(%rip), %rax
	leaq	-48(%rbp), %rcx
	movl	$1, -64(%rbp)
	leaq	-60(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	movq	%rax, -56(%rbp)
	leaq	-64(%rbp), %rdi
	movb	$1, _ZN15NodeTestFixture16node_initializedE(%rip)
	call	_ZN4node4InitEPiPPKcS0_PS3_@PLT
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L134:
	movq	%r12, %rdi
	call	*%rax
	movq	_ZN15NodeTestFixture8platformE(%rip), %r13
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L137:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L171:
	leaq	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L172:
	leaq	_ZZN15NodeTestFixture13SetUpTestCaseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L140:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L141
.L173:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8482:
	.size	_ZN15NodeTestFixture13SetUpTestCaseEv, .-_ZN15NodeTestFixture13SetUpTestCaseEv
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"process._linkedBinding('cctest_linkedbinding').key"
	.text
	.align 2
	.p2align 4
	.globl	_ZN33LinkedBindingTest_SimpleTest_Test8TestBodyEv
	.type	_ZN33LinkedBindingTest_SimpleTest_Test8TestBodyEv, @function
_ZN33LinkedBindingTest_SimpleTest_Test8TestBodyEv:
.LFB8532:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-144(%rbp), %r15
	leaq	.LC5(%rip), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	16(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	.LC2(%rip), %rdx
	leaq	.LC3(%rip), %rax
	movq	%rax, %xmm1
	movq	%rdx, %xmm0
	leaq	.LC4(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	-72(%rbp), %rdi
	call	strlen@PLT
	movl	$24, %edi
	movq	%rax, %r13
	call	malloc@PLT
	leal	22(%r13), %edi
	movl	$5, %r13d
	movslq	%edi, %rdi
	movq	%rax, %r12
	call	malloc@PLT
	movl	$8, %r8d
	leaq	-80(%rbp), %r11
	movq	%rax, (%r12)
	movl	$1701080942, (%rax)
	movb	$0, 4(%rax)
	.p2align 4,,10
	.p2align 3
.L175:
	movq	(%r11,%r8), %rcx
	movq	%r8, -200(%rbp)
	movq	%r11, -192(%rbp)
	movq	%rcx, %rdi
	movq	%rcx, -184(%rbp)
	call	strlen@PLT
	movslq	%r13d, %r10
	movq	-184(%rbp), %rcx
	movq	%r14, %rdx
	leal	1(%rax), %r9d
	movq	%r10, %rdi
	xorl	%eax, %eax
	addq	(%r12), %rdi
	movslq	%r9d, %rsi
	movl	%r9d, -176(%rbp)
	movq	%r10, -168(%rbp)
	call	snprintf@PLT
	movq	-200(%rbp), %r8
	movq	-168(%rbp), %r10
	movl	-176(%rbp), %r9d
	addq	(%r12), %r10
	movq	%r10, (%r12,%r8)
	addq	$8, %r8
	movq	-192(%rbp), %r11
	addl	%r9d, %r13d
	cmpq	$24, %r8
	jne	.L175
	movq	-144(%rbp), %r14
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE@PLT
	movq	%rax, -112(%rbp)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L187
	movq	%rax, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	_ZN15NodeTestFixture8platformE(%rip), %rdx
	movq	%r14, %rdi
	xorl	%ecx, %ecx
	leaq	_ZN15NodeTestFixture12current_loopE(%rip), %rsi
	call	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L188
	movq	-112(%rbp), %rsi
	movq	%r12, %r9
	movl	$3, %r8d
	movq	%r12, %rcx
	movl	$1, %edx
	movq	%rax, %rdi
	call	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_@PLT
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L189
	movq	16(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	16(%rbx), %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC15(%rip), %rsi
	movq	%rax, -168(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L190
.L179:
	movq	-168(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZN2v86Script7CompileENS_5LocalINS_7ContextEEENS1_INS_6StringEEEPNS_12ScriptOriginE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L191
.L180:
	movq	-168(%rbp), %rsi
	call	_ZN2v86Script3RunENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L192
.L181:
	movq	16(%rbx), %rsi
	leaq	-160(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN2v86String9Utf8ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movq	-160(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L193
	movl	$6, %ecx
	leaq	.LC0(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L194
	movq	%rbx, %rdi
	call	_ZN2v86String9Utf8ValueD1Ev@PLT
	movq	-176(%rbp), %rdi
	call	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE@PLT
	movq	%r14, %rdi
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	(%r12), %rdi
	call	free@PLT
	movq	%r12, %rdi
	call	free@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L195
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L188:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L189:
	leaq	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L190:
	movq	%rax, -184(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-184(%rbp), %rsi
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L191:
	movq	%rax, -184(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-184(%rbp), %rdi
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L192:
	movq	%rax, -168(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-168(%rbp), %rdx
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L193:
	leaq	_ZZN33LinkedBindingTest_SimpleTest_Test8TestBodyEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L194:
	leaq	_ZZN33LinkedBindingTest_SimpleTest_Test8TestBodyEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L195:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8532:
	.size	_ZN33LinkedBindingTest_SimpleTest_Test8TestBodyEv, .-_ZN33LinkedBindingTest_SimpleTest_Test8TestBodyEv
	.section	.text.startup
	.p2align 4
	.type	_register_cctest_linkedbinding, @function
_register_cctest_linkedbinding:
.LFB8501:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE8501:
	.size	_register_cctest_linkedbinding, .-_register_cctest_linkedbinding
	.section	.init_array,"aw"
	.align 8
	.quad	_register_cctest_linkedbinding
	.section	.text._ZN4node12NodePlatformD2Ev,"axG",@progbits,_ZN4node12NodePlatformD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12NodePlatformD2Ev
	.type	_ZN4node12NodePlatformD2Ev, @function
_ZN4node12NodePlatformD2Ev:
.LFB9976:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	120(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L199
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L200
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L226
	.p2align 4,,10
	.p2align 3
.L199:
	movq	64(%rbx), %r12
	testq	%r12, %r12
	je	.L205
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L211
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	jne	.L227
	.p2align 4,,10
	.p2align 3
.L213:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L205
.L206:
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L213
.L227:
	lock subl	$1, 8(%r13)
	jne	.L213
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L213
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L206
	.p2align 4,,10
	.p2align 3
.L205:
	movq	56(%rbx), %rax
	movq	48(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%rbx), %rdi
	leaq	96(%rbx), %rax
	movq	$0, 72(%rbx)
	movq	$0, 64(%rbx)
	cmpq	%rax, %rdi
	je	.L215
	call	_ZdlPv@PLT
.L215:
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_destroy@PLT
	.p2align 4,,10
	.p2align 3
.L210:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L209
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L209:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L205
.L211:
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L209
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L209
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L200:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L199
.L226:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L203
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L204:
	cmpl	$1, %eax
	jne	.L199
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L203:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L204
	.cfi_endproc
.LFE9976:
	.size	_ZN4node12NodePlatformD2Ev, .-_ZN4node12NodePlatformD2Ev
	.weak	_ZN4node12NodePlatformD1Ev
	.set	_ZN4node12NodePlatformD1Ev,_ZN4node12NodePlatformD2Ev
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag:
.LFB10488:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L229
	testq	%rsi, %rsi
	je	.L245
.L229:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L246
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L232
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L233:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L247
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L233
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L246:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L231:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L233
.L245:
	leaq	.LC12(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L247:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10488:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	.section	.rodata.str1.8
	.align 8
.LC16:
	.string	"../test/cctest/test_linked_binding.cc"
	.section	.rodata.str1.1
.LC17:
	.string	"SimpleTest"
.LC18:
	.string	"LinkedBindingTest"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"LocallyDefinedLinkedBindingTest"
	.section	.text.startup
	.p2align 4
	.type	_GLOBAL__sub_I__Z17InitializeBindingN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPv, @function
_GLOBAL__sub_I__Z17InitializeBindingN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPv:
.LFB11515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZStL8__ioinit(%rip), %rsi
	call	__cxa_atexit@PLT
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r13
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestEE(%rip), %rax
	movq	%rax, 0(%r13)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L259
.L249:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L260
.L250:
	leaq	-128(%rbp), %r15
	leaq	.LC16(%rip), %rsi
	movq	%r15, %rdi
	leaq	-96(%rbp), %r12
	leaq	-80(%rbp), %rbx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	leaq	_ZN15NodeTestFixture16TearDownTestCaseEv(%rip), %r14
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	subq	$8, %rsp
	movq	%r12, %r8
	xorl	%ecx, %ecx
	leaq	_ZN15NodeTestFixture13SetUpTestCaseEv(%rip), %rax
	pushq	%r13
	leaq	.LC18(%rip), %rdi
	xorl	%edx, %edx
	pushq	%r14
	leaq	_ZN7testing8internal12TypeIdHelperI17LinkedBindingTestE6dummy_E(%rip), %r9
	leaq	.LC17(%rip), %rsi
	pushq	%rax
	movl	$24, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN33LinkedBindingTest_SimpleTest_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L251
	call	_ZdlPv@PLT
.L251:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	movq	%rax, -136(%rbp)
	cmpq	%rax, %rdi
	je	.L252
	call	_ZdlPv@PLT
.L252:
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$1, %edi
	movq	%rax, %r13
	leaq	16+_ZTVN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestEE(%rip), %rax
	movq	%rax, 0(%r13)
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L261
.L253:
	movl	$1, %edi
	call	_ZN7testing8internal6IsTrueEb@PLT
	testb	%al, %al
	je	.L262
.L254:
	leaq	.LC16(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	addq	%rsi, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	subq	$8, %rsp
	movq	%r12, %r8
	xorl	%ecx, %ecx
	leaq	_ZN15NodeTestFixture13SetUpTestCaseEv(%rip), %rax
	pushq	%r13
	leaq	.LC18(%rip), %rdi
	xorl	%edx, %edx
	pushq	%r14
	leaq	_ZN7testing8internal12TypeIdHelperI17LinkedBindingTestE6dummy_E(%rip), %r9
	leaq	.LC19(%rip), %rsi
	pushq	%rax
	movl	$62, -64(%rbp)
	call	_ZN7testing8internal23MakeAndRegisterTestInfoEPKcS2_S2_S2_NS0_12CodeLocationEPKvPFvvES7_PNS0_15TestFactoryBaseE@PLT
	movq	-96(%rbp), %rdi
	addq	$32, %rsp
	movq	%rax, _ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test10test_info_E(%rip)
	cmpq	%rbx, %rdi
	je	.L255
	call	_ZdlPv@PLT
.L255:
	movq	-128(%rbp), %rdi
	cmpq	-136(%rbp), %rdi
	je	.L248
	call	_ZdlPv@PLT
.L248:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L263
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L259:
	.cfi_restore_state
	call	_ZN7testing8internal16SuiteApiResolverI17LinkedBindingTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L249
.L262:
	call	_ZN7testing8internal16SuiteApiResolverI17LinkedBindingTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L254
.L261:
	call	_ZN7testing8internal16SuiteApiResolverI17LinkedBindingTestE22GetTearDownCaseOrSuiteEv.part.0
	jmp	.L253
.L260:
	call	_ZN7testing8internal16SuiteApiResolverI17LinkedBindingTestE19GetSetUpCaseOrSuiteEv.part.0
	jmp	.L250
.L263:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11515:
	.size	_GLOBAL__sub_I__Z17InitializeBindingN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPv, .-_GLOBAL__sub_I__Z17InitializeBindingN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPv
	.section	.init_array
	.align 8
	.quad	_GLOBAL__sub_I__Z17InitializeBindingN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPv
	.weak	_ZN7testing8internal12TypeIdHelperI17LinkedBindingTestE6dummy_E
	.section	.bss._ZN7testing8internal12TypeIdHelperI17LinkedBindingTestE6dummy_E,"awG",@nobits,_ZN7testing8internal12TypeIdHelperI17LinkedBindingTestE6dummy_E,comdat
	.type	_ZN7testing8internal12TypeIdHelperI17LinkedBindingTestE6dummy_E, @gnu_unique_object
	.size	_ZN7testing8internal12TypeIdHelperI17LinkedBindingTestE6dummy_E, 1
_ZN7testing8internal12TypeIdHelperI17LinkedBindingTestE6dummy_E:
	.zero	1
	.weak	_ZTV15NodeTestFixture
	.section	.data.rel.ro._ZTV15NodeTestFixture,"awG",@progbits,_ZTV15NodeTestFixture,comdat
	.align 8
	.type	_ZTV15NodeTestFixture, @object
	.size	_ZTV15NodeTestFixture, 64
_ZTV15NodeTestFixture:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	__cxa_pure_virtual
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI33LinkedBindingTest_SimpleTest_TestE10CreateTestEv
	.weak	_ZTV33LinkedBindingTest_SimpleTest_Test
	.section	.data.rel.ro.local._ZTV33LinkedBindingTest_SimpleTest_Test,"awG",@progbits,_ZTV33LinkedBindingTest_SimpleTest_Test,comdat
	.align 8
	.type	_ZTV33LinkedBindingTest_SimpleTest_Test, @object
	.size	_ZTV33LinkedBindingTest_SimpleTest_Test, 64
_ZTV33LinkedBindingTest_SimpleTest_Test:
	.quad	0
	.quad	0
	.quad	_ZN33LinkedBindingTest_SimpleTest_TestD1Ev
	.quad	_ZN33LinkedBindingTest_SimpleTest_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN33LinkedBindingTest_SimpleTest_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.weak	_ZTVN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestEE
	.section	.data.rel.ro.local._ZTVN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestEE,"awG",@progbits,_ZTVN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestEE,comdat
	.align 8
	.type	_ZTVN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestEE, @object
	.size	_ZTVN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestEE, 40
_ZTVN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestEE:
	.quad	0
	.quad	0
	.quad	_ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestED1Ev
	.quad	_ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestED0Ev
	.quad	_ZN7testing8internal15TestFactoryImplI54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestE10CreateTestEv
	.weak	_ZTV54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test
	.section	.data.rel.ro.local._ZTV54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test,"awG",@progbits,_ZTV54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test,comdat
	.align 8
	.type	_ZTV54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test, @object
	.size	_ZTV54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test, 64
_ZTV54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test:
	.quad	0
	.quad	0
	.quad	_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestD1Ev
	.quad	_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_TestD0Ev
	.quad	_ZN15NodeTestFixture5SetUpEv
	.quad	_ZN15NodeTestFixture8TearDownEv
	.quad	_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test8TestBodyEv
	.quad	_ZN7testing4Test5SetupEv
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"../test/cctest/test_linked_binding.cc:84"
	.section	.rodata.str1.1
.LC21:
	.string	"(calls) == (1)"
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"virtual void LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test::TestBody()"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test8TestBodyEvE4args_1, @object
	.size	_ZZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test8TestBodyEvE4args_1, 24
_ZZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test8TestBodyEvE4args_1:
	.quad	.LC20
	.quad	.LC21
	.quad	.LC22
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"../test/cctest/test_linked_binding.cc:83"
	.align 8
.LC24:
	.string	"(strcmp(*utf8val, \"value\")) == (0)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test8TestBodyEvE4args_0, @object
	.size	_ZZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test8TestBodyEvE4args_0, 24
_ZZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test8TestBodyEvE4args_0:
	.quad	.LC23
	.quad	.LC24
	.quad	.LC22
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"../test/cctest/test_linked_binding.cc:82"
	.section	.rodata.str1.1
.LC26:
	.string	"(*utf8val) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test8TestBodyEvE4args, @object
	.size	_ZZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test8TestBodyEvE4args, 24
_ZZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test8TestBodyEvE4args:
	.quad	.LC25
	.quad	.LC26
	.quad	.LC22
	.globl	_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test10test_info_E
	.bss
	.align 8
	.type	_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test10test_info_E, @object
	.size	_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test10test_info_E, 8
_ZN54LinkedBindingTest_LocallyDefinedLinkedBindingTest_Test10test_info_E:
	.zero	8
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"../test/cctest/test_linked_binding.cc:42"
	.align 8
.LC28:
	.string	"virtual void LinkedBindingTest_SimpleTest_Test::TestBody()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN33LinkedBindingTest_SimpleTest_Test8TestBodyEvE4args_0, @object
	.size	_ZZN33LinkedBindingTest_SimpleTest_Test8TestBodyEvE4args_0, 24
_ZZN33LinkedBindingTest_SimpleTest_Test8TestBodyEvE4args_0:
	.quad	.LC27
	.quad	.LC24
	.quad	.LC28
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"../test/cctest/test_linked_binding.cc:41"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN33LinkedBindingTest_SimpleTest_Test8TestBodyEvE4args, @object
	.size	_ZZN33LinkedBindingTest_SimpleTest_Test8TestBodyEvE4args, 24
_ZZN33LinkedBindingTest_SimpleTest_Test8TestBodyEvE4args:
	.quad	.LC29
	.quad	.LC26
	.quad	.LC28
	.globl	_ZN33LinkedBindingTest_SimpleTest_Test10test_info_E
	.bss
	.align 8
	.type	_ZN33LinkedBindingTest_SimpleTest_Test10test_info_E, @object
	.size	_ZN33LinkedBindingTest_SimpleTest_Test10test_info_E, 8
_ZN33LinkedBindingTest_SimpleTest_Test10test_info_E:
	.zero	8
	.section	.rodata.str1.1
.LC30:
	.string	"cctest_linkedbinding"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	2
	.quad	0
	.quad	.LC16
	.quad	0
	.quad	_Z17InitializeBindingN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPv
	.quad	.LC30
	.quad	0
	.quad	0
	.weak	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"../test/cctest/node_test_fixture.h:140"
	.section	.rodata.str1.1
.LC32:
	.string	"(nullptr) != (environment_)"
	.section	.rodata.str1.8
	.align 8
.LC33:
	.string	"EnvironmentTestFixture::Env::Env(const v8::HandleScope&, const Argv&)"
	.section	.data.rel.ro.local._ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1,"awG",@progbits,_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1,comdat
	.align 16
	.type	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1, @gnu_unique_object
	.size	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1, 24
_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_1:
	.quad	.LC31
	.quad	.LC32
	.quad	.LC33
	.weak	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0
	.section	.rodata.str1.8
	.align 8
.LC34:
	.string	"../test/cctest/node_test_fixture.h:135"
	.section	.rodata.str1.1
.LC35:
	.string	"(nullptr) != (isolate_data_)"
	.section	.data.rel.ro.local._ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0,"awG",@progbits,_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0,comdat
	.align 16
	.type	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0, @gnu_unique_object
	.size	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0, 24
_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args_0:
	.quad	.LC34
	.quad	.LC35
	.quad	.LC33
	.weak	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args
	.section	.rodata.str1.8
	.align 8
.LC36:
	.string	"../test/cctest/node_test_fixture.h:129"
	.section	.rodata.str1.1
.LC37:
	.string	"!context_.IsEmpty()"
	.section	.data.rel.ro.local._ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args,"awG",@progbits,_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args,comdat
	.align 16
	.type	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args, @gnu_unique_object
	.size	_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args, 24
_ZZN22EnvironmentTestFixture3EnvC4ERKN2v811HandleScopeERK4ArgvE4args:
	.quad	.LC36
	.quad	.LC37
	.quad	.LC33
	.weak	_ZZN15NodeTestFixture5SetUpEvE4args
	.section	.rodata.str1.8
	.align 8
.LC38:
	.string	"../test/cctest/node_test_fixture.h:108"
	.section	.rodata.str1.1
.LC39:
	.string	"(isolate_) != (nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"virtual void NodeTestFixture::SetUp()"
	.section	.data.rel.ro.local._ZZN15NodeTestFixture5SetUpEvE4args,"awG",@progbits,_ZZN15NodeTestFixture5SetUpEvE4args,comdat
	.align 16
	.type	_ZZN15NodeTestFixture5SetUpEvE4args, @gnu_unique_object
	.size	_ZZN15NodeTestFixture5SetUpEvE4args, 24
_ZZN15NodeTestFixture5SetUpEvE4args:
	.quad	.LC38
	.quad	.LC39
	.quad	.LC40
	.weak	_ZZN15NodeTestFixture16TearDownTestCaseEvE4args
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"../test/cctest/node_test_fixture.h:101"
	.align 8
.LC42:
	.string	"(0) == (uv_loop_close(&current_loop))"
	.align 8
.LC43:
	.string	"static void NodeTestFixture::TearDownTestCase()"
	.section	.data.rel.ro.local._ZZN15NodeTestFixture16TearDownTestCaseEvE4args,"awG",@progbits,_ZZN15NodeTestFixture16TearDownTestCaseEvE4args,comdat
	.align 16
	.type	_ZZN15NodeTestFixture16TearDownTestCaseEvE4args, @gnu_unique_object
	.size	_ZZN15NodeTestFixture16TearDownTestCaseEvE4args, 24
_ZZN15NodeTestFixture16TearDownTestCaseEvE4args:
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.weak	_ZZN15NodeTestFixture13SetUpTestCaseEvE4args
	.section	.rodata.str1.8
	.align 8
.LC44:
	.string	"../test/cctest/node_test_fixture.h:87"
	.align 8
.LC45:
	.string	"(0) == (uv_loop_init(&current_loop))"
	.align 8
.LC46:
	.string	"static void NodeTestFixture::SetUpTestCase()"
	.section	.data.rel.ro.local._ZZN15NodeTestFixture13SetUpTestCaseEvE4args,"awG",@progbits,_ZZN15NodeTestFixture13SetUpTestCaseEvE4args,comdat
	.align 16
	.type	_ZZN15NodeTestFixture13SetUpTestCaseEvE4args, @gnu_unique_object
	.size	_ZZN15NodeTestFixture13SetUpTestCaseEvE4args, 24
_ZZN15NodeTestFixture13SetUpTestCaseEvE4args:
	.quad	.LC44
	.quad	.LC45
	.quad	.LC46
	.weak	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args
	.section	.rodata.str1.1
.LC47:
	.string	"../src/tracing/agent.h:91"
.LC48:
	.string	"(controller) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC49:
	.string	"node::tracing::TracingController* node::tracing::Agent::GetTracingController()"
	.section	.data.rel.ro.local._ZZN4node7tracing5Agent20GetTracingControllerEvE4args,"awG",@progbits,_ZZN4node7tracing5Agent20GetTracingControllerEvE4args,comdat
	.align 16
	.type	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args, @gnu_unique_object
	.size	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args, 24
_ZZN4node7tracing5Agent20GetTracingControllerEvE4args:
	.quad	.LC47
	.quad	.LC48
	.quad	.LC49
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
